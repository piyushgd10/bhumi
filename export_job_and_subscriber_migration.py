#!/usr/bin/python

import MySQLdb
import sys
import csv
from pymongo import MongoClient
import json
import os
import subprocess

arguments = sys.argv


arg = arguments[1]
p = subprocess.Popen(["/usr/local/bin/scriptscommon/scriptscommon/getSpecificServerDetails", "-s", arg, "-d"], stdout=subprocess.PIPE)

uniware_server_name, err = p.communicate()

print 'Server is :' + uniware_server_name
uniware_server_name = uniware_server_name.strip()
mysql_ip = uniware_server_name
uniware_db = MySQLdb.connect(host=mysql_ip, user="root", passwd="uniware", db="uniware")
uniware_cursor = uniware_db.cursor()
uniware_cursor.execute(
    "select GROUP_CONCAT(u.username), p.code, ejt.name, ej.frequency, ej.cron_expression, ej.export_job_request_json, ej.file_path, ej.scheduled_time, ej.task_duration_in_sec, ej.enabled, ej.description, ej.notification_email, ej.created, ej.updated , es.created, es.updated, es.subscribed, t.code, ej.id from export_subscriber es, export_job ej, export_job_type ejt, user u, party p, tenant t where u.id = es.user_id and ej.id = es.export_job_id and p.id = ej.facility_id and ej.export_job_type_id = ejt.id and u.tenant_id = t.id group by ej.id;")
f = open("/tmp/temp_export_job_and_subscriber_table_data.csv", "w+")

uniware_writer = csv.writer(f)
for wrow in uniware_cursor.fetchall():
    uniware_writer.writerow(wrow)
f.close()
print "Done writing csv file /tmp/temp_export_job_and_subscriber_table_data.csv"
print "Data fetched from MySQL, inserting in mongo"

linuxCMD = 'cat /tmp/temp_export_job_and_subscriber_table_data.csv >> /tmp/temp_export_job_and_subscriber_table_data_mongo.csv'
os.system(linuxCMD)

p = subprocess.Popen(["/usr/local/bin/scriptscommon/scriptscommon/getSpecificServerDetails", "-s", arg, "-tm"], stdout=subprocess.PIPE)
mongo_server_name, err = p.communicate()
mongo_server_name = mongo_server_name.strip()
mysql_ip = mongo_server_name

flag = '1';
uniware_cursor.execute(
    "select  value from environment_property where name = 'tenant.specific.mongo.enabled';")
try:
    flag =  uniware_cursor.fetchone()[0]
except :
    print 'tenantspecific mongo not enabled on this cloud'


if (flag == "true"):
    print 'Tenant Specific primary mongo is :' + mongo_server_name
else:
    mongo_server_name = "common2.mongo.unicommerce.infra:27017"
    print 'Tenant Specific primary mongo is :' + mongo_server_name

mongo_connection = MongoClient(mongo_server_name)
f = open("/tmp/temp_export_job_and_subscriber_table_data_mongo.csv", "r+")
export_reader = csv.reader(f)
count = 0
old_export_job_id = 0

for row in export_reader:
    usernames = row[0].split(",")
    facility_code = row[1]
    export_job_type_name = row[2]
    frequency = row[3]
    cron_expression = row[4]
    export_job_request_json = row[5]
    file_path = row[6]
    scheduled_time = row[7]
    task_duration_in_sec = row[8]
    enabled = False
    if (row[9] == '1'):
        enabled = True
    description = row[10]
    notification_email = row[11]
    ej_created = row[12]
    ej_updated = row[13]
    es_created = row[14]
    es_updated = row[15]
    subscribed = False
    if (row[16] == '1'):
        subscribed = True
    tenant_code = row[17]
    #query = "select u.username from user u, export_job ej where ej.user_id = u.id and ej.id = " + row[18] + ";"
    #uniware_cursor.execute(query)
    #temp = uniware_cursor.fetchone()
    #ej_username = temp[0]

    #export_job_request = json.loads(export_job_request_json)

    if (flag == "true"):
        db = mongo_connection[tenant_code]
        db.eval("db.exportJob.find({}).forEach(function(item) { for(i = 0; i != item.request.exportFilters.length; ++i) { if(item.request.exportFilters[i]._id == null){ item.request.exportFilters[i]._id = item.request.exportFilters[i].id; delete item.request.exportFilters[i].id;}}db.exportJob.update({_id: item._id}, item);});")
        #db.eval("db.subscribedExport.update( {}, { $rename: { 'username': 'usernames' } },{multi:true} )")
        #db.eval("function (){db.subscribedExport.find().forEach(function(doc){db.subscribedExport.update({_id:doc._id}, {$set:{exportJobId:doc.exportJobId.valueOf()}})})}")
        #db.eval("update()")
    else:
        #print "update not needed here"
        db = mongo_connection["uniware"]
        db.eval("db.exportJob.find({}).forEach(function(item) { for(i = 0; i != item.request.exportFilters.length; ++i) { if(item.request.exportFilters[i]._id == null){ item.request.exportFilters[i]._id = item.request.exportFilters[i].id; delete item.request.exportFilters[i].id;}}db.exportJob.update({_id: item._id}, item);});")
        # exportJobCollection = db.exportJob
        # exportSubsCriptionCollection = db.subscribedExport
        #
        # export_job = {
        #     'username': ej_username,
        #     'exportJobTypeName': export_job_type_name,
        #     'tenantCode': tenant_code,
        #     'facilityCode': facility_code,
        #     'scheduledTime': scheduled_time,
        #     'frequency': frequency,
        #     'cronExpression': cron_expression,
        #     'enabled': enabled,
        #     'notificationEmail': notification_email,
        #     'description': description,
        #     'request': export_job_request,
        #     'taskDurationInSec': task_duration_in_sec,
        #     'exportFilePath': file_path,
        #     'created': ej_created,
        #     'updated': ej_updated
        # }
        # id = exportJobCollection.insert(export_job)
        # db.eval("function (id) {db.exportJob.find({_id:id}).forEach(function(doc){db.exportJob.update({_id:doc._id},{$set:{created:new ISODate(doc.created), updated:new ISODate(doc.updated), scheduledTime:new ISODate(doc.scheduledTime)}},{upsert:false , multi:true})})}",id)
        #
        # export_subscriber = {
        #     'usernames': usernames,
        #     'exportJobId': id,
        #     'created': es_created,
        #     'updated': es_updated,
        #     'subscribed': subscribed
        # }
        #
        # id = exportSubsCriptionCollection.insert(export_subscriber)
        # db.eval("function (id) {db.subscribedExport.find({_id:id}).forEach(function(doc){db.subscribedExport.update({_id:doc._id},{$set:{created:new ISODate(doc.created), updated:new ISODate(doc.updated)}},{upsert:false , multi:true})})}",id)
        # count = count + 1

f.close()

#print 'total rows: ' + str(count)

linuxCMD = 'rm /tmp/temp_export_job_and_subscriber_table_data.csv'
os.system(linuxCMD)
linuxCMD = 'rm /tmp/temp_export_job_and_subscriber_table_data_mongo.csv'
os.system(linuxCMD)

print 'Done! '
