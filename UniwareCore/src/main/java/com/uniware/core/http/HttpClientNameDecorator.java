/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 12-Jun-2013
 *  @author unicom
 */
package com.uniware.core.http;

import com.unifier.core.transport.http.IHttpClientNameDecorator;
import com.uniware.core.utils.UserContext;

public class HttpClientNameDecorator implements IHttpClientNameDecorator {

    @Override
    public String decorate(String name) {
        if (name != null) {
            name = name + "_" + UserContext.current().getTenantId();
        }
        return name;
    }

}
