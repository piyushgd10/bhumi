/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 13, 2012
 *  @author praveeng
 */
package com.uniware.core.vo;

import com.uniware.core.entity.Facility;
import org.springframework.data.mongodb.core.mapping.Document;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author praveeng
 */
@Document(collection = "saleOrderFlowSummary")
public class SaleOrderFlowSummaryVO {

    @Id
    private String  id;
    private String  tenantCode;
    private String  facilityCode;
    private Long    totalNoOfOrders;
    private Long    ordersDispatched;
    private Date    created;


    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    public String getTenantCode() {
        return tenantCode;
    }

    public void setTenantCode(String tenantCode) {
        this.tenantCode = tenantCode;
    }

    public String getFacilityCode() {
        return facilityCode;
    }

    public void setFacilityCode(String facilityCode) {
        this.facilityCode = facilityCode;
    }

    /**
     * @return the totalNoOfOrders
     */
    public Long getTotalNoOfOrders() {
        return totalNoOfOrders;
    }

    /**
     * @param totalNoOfOrders the totalNoOfOrders to set
     */
    public void setTotalNoOfOrders(Long totalNoOfOrders) {
        this.totalNoOfOrders = totalNoOfOrders;
    }

    /**
     * @return the ordersDispatched
     */
    public Long getOrdersDispatched() {
        return ordersDispatched;
    }

    /**
     * @param ordersDispatched the ordersDispatched to set
     */
    public void setOrdersDispatched(Long ordersDispatched) {
        this.ordersDispatched = ordersDispatched;
    }

    /**
     * @return the created
     */
    public Date getCreated() {
        return created;
    }

    /**
     * @param created the created to set
     */
    public void setCreated(Date created) {
        this.created = created;
    }

}
