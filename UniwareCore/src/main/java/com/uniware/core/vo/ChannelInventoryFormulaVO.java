package com.uniware.core.vo;

import com.google.gson.Gson;
import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.EncryptionUtils;
import com.unifier.core.utils.StringUtils;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document(collection = "channelInventoryFormula")
public class ChannelInventoryFormulaVO {

    @Id
    private String id;

    @Indexed(unique = true)
    private String code;

    @Indexed(unique = true)
    private String checksum;

    private String formula;
    private Date   created;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getChecksum() {
        return checksum;
    }

    public void setChecksum(String checksum) {
        this.checksum = checksum;
    }

    public String getFormula() {
        return formula;
    }

    public void setFormula(String formula) {
        this.formula = formula;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public static void main(String[] args) {
        String checksum = EncryptionUtils.md5Encode(
                "#{#inventorySnapshot.inventory - #inventorySnapshot.openSale - #pendency - (#failedOrderInventory?:0) - #inventoryBlockedOnOtherChannels - #inventorySnapshot.pendingInventoryAssessment + #unprocessedOrderInventory}");
        ChannelInventoryFormulaVO channelInventoryFormula = new ChannelInventoryFormulaVO();
        channelInventoryFormula.setCode(StringUtils.getRandomAlphaNumeric(8));
        channelInventoryFormula.setChecksum(checksum);
        channelInventoryFormula.setFormula("#{#inventorySnapshot.inventory - #inventorySnapshot.openSale - #pendency - (#failedOrderInventory?:0) - #inventoryBlockedOnOtherChannels - #inventorySnapshot.pendingInventoryAssessment + #unprocessedOrderInventory}");
        channelInventoryFormula.setCreated(DateUtils.getCurrentTime());
        System.out.println(new Gson().toJson(channelInventoryFormula));
    }
}
