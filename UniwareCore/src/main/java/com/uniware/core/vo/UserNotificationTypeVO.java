package com.uniware.core.vo;

// Generated Aug 27, 2013 5:28:44 PM by Hibernate Tools 3.4.0.CR1

import java.io.Serializable;
import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "userNotificationType")
public class UserNotificationTypeVO implements Serializable {

    public enum NotificationType {
        INVALID_VENDOR_EMAIL,
        CHANNEL_CONNECTOR_BROKEN
    }

    @Id
    private String  id;
    @Indexed(unique = true, dropDups = true)
    private String  type;
    private String  accessResourceName;
    private boolean enabled;
    private Date    created;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAccessResourceName() {
        return accessResourceName;
    }

    public void setAccessResourceName(String accessResourceName) {
        this.accessResourceName = accessResourceName;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

}
