/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 05-Mar-2014
 *  @author amit
 */
package com.uniware.core.vo;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "globalPrintTemplate")
/*
@CompoundIndexes({
        @CompoundIndex(def = "{'tenantCode' :  1, 'type' :  1, 'name' : 1}", unique = true),
        @CompoundIndex(def = "{'tenantCode' :  1, 'enabled' :  1}"),
        @CompoundIndex(def = "{'tenantCode' :1, 'code' : 1}", unique = true) })
*/
public class GlobalPrintTemplateVO {

    public enum Type {
        INVOICE,
        SHIPMENT_LABEL,
        SHIPPING_MANIFEST,
        SHIPPING_MANIFEST_CSV,
        PURCHASE_ORDER,
        PICKLIST,
        PICKLIST_PREVIEW,
        ITEM_LABEL,
        ITEM_LABEL_PRN,
        ITEM_TYPE_LABEL_CSV,
        ITEM_TYPE_LABEL_PRN,
        SHELF,
        SHELF_CSV,
        RETURN_MANIFEST,
        RETURN_MANIFEST_CSV,
        QC_REJECTION_REPORT,
        SHIPMENT_LABEL_CSV,
        PURCHASE_ORDER_CSV,
        OUTBOUND_GATE_PASS,
        WORKORDER,
        REVERSE_PICKUP,
        STATE_REGULATORY_FORM,
        BOM_PICKLIST_TEMPLATE,
        VENDOR_INVOICE,
        SHIPMENT_LABEL_PRN,
        PICKLIST_ITEM_CSV
    }

    @Id
    private String                     id;

    private String                     tenantCode;

    private String                     code;

    private String                     type;

    private String                     name;

    private String                     description;

    private String                     template;

    private boolean                    autoConfig;

    private boolean                    enabled;

    private boolean                    useAsDefault;

    private GlobalTemplatePrintOptions globalTemplatePrintOptions;

    private boolean                    openlyAccessible;

    private boolean                    isCustomizable;

    /**
     * Prefix of file name in which data for this type of template will be downloaded. This will be used by
     * UniwarePrintUtility for fetching the template.
     */
    private String                     dataFilenamePrefix;

    private Date                       created;

    private Date                       updated;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTenantCode() {
        return tenantCode;
    }

    public void setTenantCode(String tenantCode) {
        this.tenantCode = tenantCode;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    public boolean isAutoConfig() {
        return autoConfig;
    }

    public void setAutoConfig(boolean autoConfig) {
        this.autoConfig = autoConfig;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public boolean isUseAsDefault() {
        return useAsDefault;
    }

    public void setUseAsDefault(boolean useAsDefault) {
        this.useAsDefault = useAsDefault;
    }

    public GlobalTemplatePrintOptions getGlobalTemplatePrintOptions() {
        return globalTemplatePrintOptions;
    }

    public void setGlobalTemplatePrintOptions(GlobalTemplatePrintOptions globalTemplatePrintOptions) {
        this.globalTemplatePrintOptions = globalTemplatePrintOptions;
    }

    public boolean isOpenlyAccessible() {
        return openlyAccessible;
    }

    public void setOpenlyAccessible(boolean openlyAccessible) {
        this.openlyAccessible = openlyAccessible;
    }

    public String getDataFilenamePrefix() {
        return dataFilenamePrefix;
    }

    public void setDataFilenamePrefix(String dataFilenamePrefix) {
        this.dataFilenamePrefix = dataFilenamePrefix;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public String generateCode() {
        return type.concat("_").concat(name.replace(" ", "_").toUpperCase());
    }

    public boolean isCustomizable() {
        return isCustomizable;
    }

    public void setCustomizable(boolean isCustomizable) {
        this.isCustomizable = isCustomizable;
    }

    public class GlobalTemplatePrintOptions {
        private int     numberOfCopies;
        private String  printDialog;
        private String  pageSize;
        private String  pageMargins;
        private boolean landscape;
        private String  previewImagePath;
        private String  previewBigImagePath;
        private boolean xml;

        public GlobalTemplatePrintOptions() {
        }

        public int getNumberOfCopies() {
            return numberOfCopies;
        }

        public void setNumberOfCopies(int numberOfCopies) {
            this.numberOfCopies = numberOfCopies;
        }

        public String getPrintDialog() {
            return printDialog;
        }

        public void setPrintDialog(String printDialog) {
            this.printDialog = printDialog;
        }

        public String getPageSize() {
            return pageSize;
        }

        public void setPageSize(String pageSize) {
            this.pageSize = pageSize;
        }

        public String getPageMargins() {
            return pageMargins;
        }

        public void setPageMargins(String pageMargins) {
            this.pageMargins = pageMargins;
        }

        public boolean isLandscape() {
            return landscape;
        }

        public void setLandscape(boolean landscape) {
            this.landscape = landscape;
        }

        public String getPreviewImagePath() {
            return previewImagePath;
        }

        public void setPreviewImagePath(String previewImagePath) {
            this.previewImagePath = previewImagePath;
        }

        public String getPreviewBigImagePath() {
            return previewBigImagePath;
        }

        public void setPreviewBigImagePath(String previewBigImagePath) {
            this.previewBigImagePath = previewBigImagePath;
        }

        public boolean isXml() {
            return xml;
        }

        public void setXml(boolean xml) {
            this.xml = xml;
        }

    }
}