/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 16/04/14
 *  @author amit
 */

package com.uniware.core.vo;

import java.util.HashMap;
import java.util.Map;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "contentNamespace")
// @CompoundIndexes(@CompoundIndex(def = "{'identifier': 1}", unique = true))
public class ContentNamespaceVO {

    @Id
    private String                               id;

    private String                               identifier;

    private Map<ContentVO.LanguageCode, Integer> versionMap = new HashMap<>();

    public ContentNamespaceVO() {
        super();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public Map<ContentVO.LanguageCode, Integer> getVersionMap() {
        return versionMap;
    }

    public void setVersionMap(Map<ContentVO.LanguageCode, Integer> versionMap) {
        this.versionMap = versionMap;
    }
}
