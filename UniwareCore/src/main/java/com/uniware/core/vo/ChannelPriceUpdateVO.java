package com.uniware.core.vo;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "channelPriceUpdate")
public class ChannelPriceUpdateVO {
    
    public enum Status {
        SUCCESS,
        FAILED,
    }
    
    public enum Op {
        LOCAL_EDIT,
        CHANNEL_PULL,
        CHANNEL_PUSH
    }

    @Id
    private String     id;
    private String     tenantCode;
    
    private String     channelCode;
    private String     channelProductId;
    
    private String     who;
    private Date       created;
    
    private String     operation;
    private String     message;
    private String     statusCode;
    private String     uniwareSkuCode;
    private String     sellerSkuCode;
    
    private BigDecimal sellingPrice;
    private BigDecimal msp;
    private BigDecimal transferPrice;
    private BigDecimal mrp;
    private BigDecimal competitivePrice;
    private String     currencyCode;
    

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTenantCode() {
        return tenantCode;
    }

    public void setTenantCode(String tenantCode) {
        this.tenantCode = tenantCode;
    }

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    public String getChannelProductId() {
        return channelProductId;
    }

    public void setChannelProductId(String channelProductId) {
        this.channelProductId = channelProductId;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }
    
    public String getUniwareSkuCode() {
        return uniwareSkuCode;
    }

    public void setUniwareSkuCode(String uniwareSkuCode) {
        this.uniwareSkuCode = uniwareSkuCode;
    }

    public String getSellerSkuCode() {
        return sellerSkuCode;
    }

    public void setSellerSkuCode(String sellerSkuCode) {
        this.sellerSkuCode = sellerSkuCode;
    }

    public BigDecimal getSellingPrice() {
        return sellingPrice;
    }

    public void setSellingPrice(BigDecimal sellingPrice) {
        this.sellingPrice = sellingPrice;
    }

    public BigDecimal getMsp() {
        return msp;
    }

    public void setMsp(BigDecimal msp) {
        this.msp = msp;
    }

    public BigDecimal getTransferPrice() {
        return transferPrice;
    }

    public void setTransferPrice(BigDecimal transferPrice) {
        this.transferPrice = transferPrice;
    }

    public BigDecimal getMrp() {
        return mrp;
    }

    public void setMrp(BigDecimal mrp) {
        this.mrp = mrp;
    }

    public BigDecimal getCompetitivePrice() {
        return competitivePrice;
    }

    public void setCompetitivePrice(BigDecimal competitivePrice) {
        this.competitivePrice = competitivePrice;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public String getWho() {
        return who;
    }

    public void setWho(String who) {
        this.who = who;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }
    
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }
}
