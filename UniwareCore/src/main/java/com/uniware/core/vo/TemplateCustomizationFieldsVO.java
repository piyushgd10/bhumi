/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 06/05/14
 *  @author amit
 */

package com.uniware.core.vo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.google.gson.Gson;

/**
 * Defines all the customization parameters applicable on a template type. These parameters are grouped into
 * {@link com.uniware.core.vo.TemplateCustomizationFieldsVO.CustomizationFieldGroup}.
 */
@Document(collection = "templateCustomizationFields")
/*
@CompoundIndexes({
        @CompoundIndex(def = "{'templateType' : 1, 'customizationFieldGroupList.displayName': 1}", unique = true),
        @CompoundIndex(def = "{'templateType' : 1, 'customizationFieldGroupList.code': 1}", unique = true),
        @CompoundIndex(def = "{'templateType' : 1, 'customizationFieldGroupList.fieldList.name': 1}", unique = true) })
*/
public class TemplateCustomizationFieldsVO {

    @Id
    private String                        id;

    private PrintTemplateVO.Type          templateType;

    private List<CustomizationFieldGroup> customizationFieldGroupList;

    public TemplateCustomizationFieldsVO() {
        super();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public PrintTemplateVO.Type getTemplateType() {
        return templateType;
    }

    public void setTemplateType(PrintTemplateVO.Type templateType) {
        this.templateType = templateType;
    }

    public List<CustomizationFieldGroup> getCustomizationFieldGroupList() {
        return customizationFieldGroupList;
    }

    public void setCustomizationFieldGroupList(List<CustomizationFieldGroup> customizationFieldGroupList) {
        this.customizationFieldGroupList = customizationFieldGroupList;
    }

    /**
     * Represents a group of fields that show up together.
     */
    public static class CustomizationFieldGroup {

        /**
         * Primary key.
         */
        private String                   code;

        /**
         * Order of display of this group
         */
        private int                      position;

        private String                   displayName;

        private List<CustomizationField> fieldList;

        public CustomizationFieldGroup() {
            super();
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public int getPosition() {
            return position;
        }

        public void setPosition(int position) {
            this.position = position;
        }

        public String getDisplayName() {
            return displayName;
        }

        public void setDisplayName(String displayName) {
            this.displayName = displayName;
        }

        public List<CustomizationField> getFieldList() {
            return fieldList;
        }

        public void setFieldList(List<CustomizationField> fieldList) {
            this.fieldList = fieldList;
        }

        @Override
        public String toString() {
            return "CustomizationFieldGroup{" + "code='" + code + '\'' + ", position=" + position + ", displayName='" + displayName + '\'' + ", fieldList=" + fieldList + '}';
        }
    }

    public static class CustomizationField {

        public enum Type {
            CHECKBOX,
            TEXT_AREA,
            IMAGE,
            DROPDOWN
        }

        /**
         * Name as displayed on the page
         */
        private String              name;

        /**
         * Unique code of each field. Values of these customization parameters are stored in the {@code PrintTemplateVO}
         * with this code as key.
         */
        private String              code;

        /**
         * On which field does this field depend.
         */
        private String              parentFieldCode;

        /**
         * Is mandatory?
         */
        private boolean             required;

        /**
         * Type of the field. Checkbox fields are ones whose values are present in the system and client has to give a
         * Yes/No answer depending on which the field will show up in the template.
         */
        private Type                type;

        /**
         * CMS key of the content to be shown on tooltip.
         */
        private String              tooltipKey;

        /**
         * Additional info for UI
         */
        private Map<String, Object> uiConfigMetadata;

        /**
         * List of possible values (mostly used for dropdown type).
         */
        private List<String>        possibleValues;

        /**
         * List of product types for which this field is applicable
         */
        private List<String>        applicableForProductFeatures;

        /**
         * Is enabled? Only enabled fields will show up on the UI (and will be visible in the evaluated template)
         */
        private boolean             enabled;

        public CustomizationField() {
            super();
        }

        public Type getType() {
            return type;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public Map<String, Object> getUiConfigMetadata() {
            return uiConfigMetadata;
        }

        public String getParentFieldCode() {
            return parentFieldCode;
        }

        public void setParentFieldCode(String parentFieldCode) {
            this.parentFieldCode = parentFieldCode;
        }

        public Type isType() {
            return type;
        }

        public void setType(Type type) {
            this.type = type;
        }

        public String getTooltipKey() {
            return tooltipKey;
        }

        public void setTooltipKey(String tooltipKey) {
            this.tooltipKey = tooltipKey;
        }

        public boolean isRequired() {
            return required;
        }

        public void setRequired(boolean required) {
            this.required = required;
        }

        public void setUiConfigMetadata(Map<String, Object> uiConfigMetadata) {
            this.uiConfigMetadata = uiConfigMetadata;
        }

        public List<String> getPossibleValues() {
            return possibleValues;
        }

        public void setPossibleValues(List<String> possibleValues) {
            this.possibleValues = possibleValues;
        }

        public boolean isEnabled() {
            return enabled;
        }

        public void setEnabled(boolean enabled) {
            this.enabled = enabled;
        }

        public List<String> getApplicableForProductFeatures() {
            return applicableForProductFeatures;
        }

        public void setApplicableForProductFeatures(List<String> applicableForProductFeatures) {
            this.applicableForProductFeatures = applicableForProductFeatures;
        }

        @Override
        public String toString() {
            return "CustomizationField{" + "name='" + name + '\'' + ", code='" + code + '\'' + ", required=" + required + ", type=" + type + ", uiConfigMetadata="
                    + uiConfigMetadata + ", enabled=" + enabled + '}';
        }
    }

    @Override
    public String toString() {
        return "TemplateCustomizationFieldsVO{" + "id='" + id + '\'' + ", templateType=" + templateType + ", customizationFieldGroupList=" + customizationFieldGroupList + '}';
    }

    public static void main(String[] args) {
        TemplateCustomizationFieldsVO vo = new TemplateCustomizationFieldsVO();
        List<CustomizationFieldGroup> customizationFieldGroupList = new ArrayList<>();
        // C1
        CustomizationFieldGroup c1 = new CustomizationFieldGroup();
        c1.setDisplayName("Item Details");
        c1.setCode("ITEM_DETAILS");
        c1.setPosition(1);
        List<CustomizationField> cfl1 = new ArrayList<>();
        CustomizationField cf11 = new CustomizationField();
        cf11.setCode("DISPLAY_BRAND");
        cf11.setName("Brand");
        cf11.setEnabled(true);
        cf11.setRequired(false);
        cf11.setType(CustomizationField.Type.CHECKBOX);

        CustomizationField cf12 = new CustomizationField();
        cf12.setCode("DISPLAY_PRODUCT_NAME");
        cf12.setName("Product Name");
        cf12.setEnabled(true);
        cf12.setRequired(false);
        cf12.setType(CustomizationField.Type.CHECKBOX);

        CustomizationField cf13 = new CustomizationField();
        cf13.setCode("DISPLAY_PRODUCT_DESCRIPTION");
        cf13.setName("Product Description");
        cf13.setEnabled(true);
        cf13.setRequired(false);
        cf13.setType(CustomizationField.Type.CHECKBOX);

        CustomizationField cf14 = new CustomizationField();
        cf14.setCode("DISPLAY_PRODUCT_SKU_CODE");
        cf14.setName("Product SKU Code");
        cf14.setEnabled(true);
        cf14.setRequired(false);
        cf14.setType(CustomizationField.Type.CHECKBOX);

        CustomizationField cf15 = new CustomizationField();
        cf15.setCode("DISPLAY_CHANNEL_SKU_CODE");
        cf15.setName("Channel SKU Code");
        cf15.setEnabled(true);
        cf15.setRequired(false);
        cf15.setType(CustomizationField.Type.CHECKBOX);

        CustomizationField cf16 = new CustomizationField();
        cf16.setCode("DISPLAY_COLOR");
        cf16.setName("Color");
        cf16.setEnabled(true);
        cf16.setRequired(false);
        cf16.setType(CustomizationField.Type.CHECKBOX);

        CustomizationField cf17 = new CustomizationField();
        cf17.setCode("DISPLAY_SIZE");
        cf17.setName("SIZE");
        cf17.setEnabled(true);
        cf17.setRequired(false);
        cf17.setType(CustomizationField.Type.CHECKBOX);

        CustomizationField cf18 = new CustomizationField();
        cf18.setCode("DISPLAY_MRP");
        cf18.setName("MRP");
        cf18.setEnabled(true);
        cf18.setRequired(false);
        cf18.setType(CustomizationField.Type.CHECKBOX);

        CustomizationField cf19 = new CustomizationField();
        cf19.setCode("DISPLAY_DISCOUNT");
        cf19.setName("Discount");
        cf19.setEnabled(true);
        cf19.setRequired(false);
        cf19.setType(CustomizationField.Type.CHECKBOX);

        CustomizationField cf110 = new CustomizationField();
        cf110.setCode("DISPLAY_TAX");
        cf110.setName("Tax");
        cf110.setEnabled(true);
        cf110.setRequired(false);
        cf110.setType(CustomizationField.Type.CHECKBOX);

        cfl1.addAll(Arrays.asList(cf11, cf12, cf13, cf14, cf15, cf16, cf17, cf18, cf19, cf110));
        c1.setFieldList(cfl1);
        customizationFieldGroupList.add(c1);

        // C2
        CustomizationFieldGroup c2 = new CustomizationFieldGroup();
        c2.setDisplayName("Order Details");
        c2.setCode("ORDER_DETAILS");
        c2.setPosition(2);
        List<CustomizationField> cfl2 = new ArrayList<>();
        CustomizationField cf21 = new CustomizationField();
        cf21.setCode("DISPLAY_SHIP_DATE");
        cf21.setName("Ship Date");
        cf21.setEnabled(true);
        cf21.setRequired(false);
        cf21.setType(CustomizationField.Type.CHECKBOX);

        CustomizationField cf22 = new CustomizationField();
        cf22.setCode("DISPLAY_CARRIER_NAME");
        cf22.setName("Carrier Name");
        cf22.setEnabled(true);
        cf22.setRequired(false);
        cf22.setType(CustomizationField.Type.CHECKBOX);

        CustomizationField cf23 = new CustomizationField();
        cf23.setCode("DISPLAY_AWB_NUM");
        cf23.setName("AWB Number");
        cf23.setEnabled(true);
        cf23.setRequired(false);
        cf23.setType(CustomizationField.Type.CHECKBOX);

        CustomizationField cf24 = new CustomizationField();
        cf24.setCode("DISPLAY_INVOICE_BARCODE");
        cf24.setName("Invoice Barcode");
        cf24.setEnabled(true);
        cf24.setRequired(false);
        cf24.setType(CustomizationField.Type.CHECKBOX);

        CustomizationField cf25 = new CustomizationField();
        cf25.setCode("DISPLAY_ORDER_NUM_BARCODE");
        cf25.setName("Order Number Barcode");
        cf25.setEnabled(true);
        cf25.setRequired(false);
        cf25.setType(CustomizationField.Type.CHECKBOX);

        CustomizationField cf26 = new CustomizationField();
        cf26.setCode("DISPLAY_CHANNEL_NAME");
        cf26.setName("Channel Name");
        cf26.setEnabled(true);
        cf26.setRequired(false);
        cf26.setType(CustomizationField.Type.CHECKBOX);

        CustomizationField cf27 = new CustomizationField();
        cf27.setCode("DISPLAY_SHIPPING_CHARGES");
        cf27.setName("Shipping Charges");
        cf27.setEnabled(true);
        cf27.setRequired(false);
        cf27.setType(CustomizationField.Type.CHECKBOX);

        CustomizationField cf28 = new CustomizationField();
        cf28.setCode("DISPLAY_COD_CHARGES");
        cf28.setName("COD Charges");
        cf28.setEnabled(true);
        cf28.setRequired(false);
        cf28.setType(CustomizationField.Type.CHECKBOX);

        CustomizationField cf29 = new CustomizationField();
        cf29.setCode("DISPLAY_PREPAID_AMOUNT");
        cf29.setName("Prepaid Amount");
        cf29.setEnabled(true);
        cf29.setRequired(false);
        cf29.setType(CustomizationField.Type.CHECKBOX);

        CustomizationField cf210 = new CustomizationField();
        cf210.setCode("DISPLAY_TOTAL_VAT_CST");
        cf210.setName("Total VAT/CST On Sale");
        cf210.setEnabled(true);
        cf210.setRequired(false);
        cf210.setType(CustomizationField.Type.CHECKBOX);

        CustomizationField cf211 = new CustomizationField();
        cf211.setCode("DISPLAY_TRADE_DISCOUNT");
        cf211.setName("TRADE_DISCOUNT");
        cf211.setEnabled(false);
        cf211.setRequired(false);
        cf211.setType(CustomizationField.Type.CHECKBOX);

        CustomizationField cf212 = new CustomizationField();
        cf212.setCode("DISPLAY_ADDITIONAL_DISCOUNT");
        cf212.setName("Additional Discount");
        cf212.setEnabled(false);
        cf212.setRequired(false);
        cf212.setType(CustomizationField.Type.CHECKBOX);

        CustomizationField cf213 = new CustomizationField();
        cf213.setCode("DISPLAY_ADDITIONAL_TAX");
        cf213.setName("Additional Tax");
        cf213.setEnabled(false);
        cf213.setRequired(false);
        cf213.setType(CustomizationField.Type.CHECKBOX);

        CustomizationField cf214 = new CustomizationField();
        cf214.setCode("DISPLAY_AMOUNT_IN_WORDS");
        cf214.setName("Amount In Words");
        cf214.setEnabled(true);
        cf214.setRequired(false);
        cf214.setType(CustomizationField.Type.CHECKBOX);

        cfl2.addAll(Arrays.asList(cf21, cf22, cf23, cf24, cf25, cf26, cf27, cf28, cf29, cf210, cf211, cf212, cf213, cf214));
        c2.setFieldList(cfl2);
        customizationFieldGroupList.add(c2);

        // C3
        CustomizationFieldGroup c3 = new CustomizationFieldGroup();
        c3.setDisplayName("Seller-Byer Information");
        c3.setCode("SELLER_BUYER_INFORMATION");
        c3.setPosition(3);
        List<CustomizationField> cfl3 = new ArrayList<>();

        CustomizationField cf31 = new CustomizationField();
        cf31.setCode("SELLER_LOGO");
        cf31.setName("Seller Logo");
        cf31.setEnabled(true);
        cf31.setRequired(false);
        cf31.setType(CustomizationField.Type.IMAGE);

        CustomizationField cf32 = new CustomizationField();
        cf32.setCode("DISPLAY_SELLER_LOGO");
        cf32.setName("Display Seller Logo");
        cf32.setEnabled(true);
        cf32.setRequired(false);
        cf32.setType(CustomizationField.Type.CHECKBOX);

        CustomizationField cf33 = new CustomizationField();
        cf33.setCode("DISPLAY_CHANNEL_LOGO");
        cf33.setName("Channel Logo");
        cf33.setEnabled(true);
        cf33.setRequired(false);
        cf33.setType(CustomizationField.Type.CHECKBOX);

        CustomizationField cf34 = new CustomizationField();
        cf34.setCode("DISPLAY_WAREHOUSE_NAME");
        cf34.setName("Warehouse Name");
        cf34.setEnabled(true);
        cf34.setRequired(false);
        cf34.setType(CustomizationField.Type.CHECKBOX);

        CustomizationField cf35 = new CustomizationField();
        cf35.setCode("DISPLAY_WAREHOUSE_ADDRESS");
        cf35.setName("Warehouse Address");
        cf35.setEnabled(true);
        cf35.setRequired(false);
        cf35.setType(CustomizationField.Type.CHECKBOX);

        CustomizationField cf36 = new CustomizationField();
        cf36.setCode("DISPLAY_BUYER_VAT_TIN_NUM");
        cf36.setName("Buyer VAT TIN Number");
        cf36.setEnabled(false);
        cf36.setRequired(false);
        cf36.setType(CustomizationField.Type.CHECKBOX);

        CustomizationField cf37 = new CustomizationField();
        cf37.setCode("DISPLAY_BUYER_CST_TIN_NUM");
        cf37.setName("Buyer CST TIN Number");
        cf37.setEnabled(false);
        cf37.setRequired(false);
        cf37.setType(CustomizationField.Type.CHECKBOX);

        CustomizationField cf38 = new CustomizationField();
        cf38.setCode("DISPLAY_BUYER_CIN_NUM");
        cf38.setName("Buyer CIN Number");
        cf38.setEnabled(false);
        cf38.setRequired(false);
        cf38.setType(CustomizationField.Type.CHECKBOX);

        CustomizationField cf39 = new CustomizationField();
        cf39.setCode("DISPLAY_SELLER_VAT_TIN_NUM");
        cf39.setName("Seller VAT TIN Number");
        cf39.setEnabled(true);
        cf39.setRequired(false);
        cf39.setType(CustomizationField.Type.CHECKBOX);

        CustomizationField cf310 = new CustomizationField();
        cf310.setCode("DISPLAY_SELLER_CST_TIN_NUM");
        cf310.setName("Seller CST TIN Number");
        cf310.setEnabled(true);
        cf310.setRequired(false);
        cf310.setType(CustomizationField.Type.CHECKBOX);

        CustomizationField cf311 = new CustomizationField();
        cf311.setCode("DISPLAY_SELLER_CIN_NUM");
        cf311.setName("Seller CIN Number");
        cf311.setEnabled(false);
        cf311.setRequired(false);
        cf311.setType(CustomizationField.Type.CHECKBOX);

        CustomizationField cf312 = new CustomizationField();
        cf312.setCode("DISPLAY_RECIPIENT_VAT_TIN_NUM");
        cf312.setName("Recipient VAT TIN Number");
        cf312.setEnabled(false);
        cf312.setRequired(false);
        cf312.setType(CustomizationField.Type.CHECKBOX);

        CustomizationField cf313 = new CustomizationField();
        cf313.setCode("DISPLAY_RECIPIENT_CST_TIN_NUM");
        cf313.setName("Recipient CST TIN Number");
        cf313.setEnabled(false);
        cf313.setRequired(false);
        cf313.setType(CustomizationField.Type.CHECKBOX);

        CustomizationField cf314 = new CustomizationField();
        cf314.setCode("DISPLAY_RECIPIENT_CIN_NUM");
        cf314.setName("Recipient CIN Number");
        cf314.setEnabled(false);
        cf314.setRequired(false);
        cf314.setType(CustomizationField.Type.CHECKBOX);

        CustomizationField cf315 = new CustomizationField();
        cf315.setCode("DISPLAY_CUSTOMER_CARE_NUM");
        cf315.setName("Customer Care Number");
        cf315.setEnabled(false);
        cf315.setRequired(false);
        cf315.setType(CustomizationField.Type.CHECKBOX);

        CustomizationField cf316 = new CustomizationField();
        cf316.setCode("DISPLAY_CUSTOMER_CARE_EMAIL");
        cf316.setName("Customer Care Email");
        cf316.setEnabled(false);
        cf316.setRequired(false);
        cf316.setType(CustomizationField.Type.CHECKBOX);

        CustomizationField cf317 = new CustomizationField();
        cf317.setCode("NOTES");
        cf317.setName("NOTES");
        cf317.setEnabled(true);
        cf317.setRequired(false);
        cf317.setType(CustomizationField.Type.TEXT_AREA);

        CustomizationField cf318 = new CustomizationField();
        cf318.setCode("DIGITAL_SIGNATURE");
        cf318.setName("Digital Signature");
        cf318.setPossibleValues(Arrays.asList("A", "B"));
        cf318.setEnabled(false);
        cf318.setRequired(false);
        cf318.setType(CustomizationField.Type.IMAGE);

        CustomizationField cf319 = new CustomizationField();
        cf319.setCode("SIGNATURE");
        cf319.setName("Signature");
        cf319.setEnabled(true);
        cf319.setRequired(false);
        cf319.setType(CustomizationField.Type.TEXT_AREA);

        CustomizationField cf320 = new CustomizationField();
        cf320.setCode("TERMS_AND_CONDITIONS ");
        cf320.setName("Terms And Conditions");
        cf320.setEnabled(true);
        cf320.setRequired(false);
        cf320.setType(CustomizationField.Type.TEXT_AREA);

        cfl3.addAll(Arrays.asList(cf31, cf32, cf33, cf34, cf35, cf36, cf37, cf38, cf39, cf310, cf311, cf312, cf313, cf314, cf315, cf316, cf317, cf318, cf319, cf320));
        c3.setFieldList(cfl3);
        customizationFieldGroupList.add(c3);

        //vo.setTemplateType(Type.TAX_INVOICE);
        vo.setCustomizationFieldGroupList(customizationFieldGroupList);
        System.out.println(new Gson().toJson(vo));
    }
}
