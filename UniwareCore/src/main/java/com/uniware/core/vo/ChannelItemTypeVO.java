package com.uniware.core.vo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;

import com.unifier.core.utils.DateUtils;
import com.uniware.core.api.channel.WsChannelItemType;
import com.uniware.core.api.channel.WsChannelItemTypeAttribute;

@Document(collection = "channelItemType")
// @CompoundIndexes({ @CompoundIndex(def = "{'tenantCode' :  1, 'channelCode' :  1, 'channelProductId' : 1}", unique = true) })
public class ChannelItemTypeVO {

    @Id
    private String          id;
    private String          tenantCode;
    private String          channelCode;
    private String          channelProductId;
    private String          brand;
    private String          productDescription;
    private Set<String>     imageUrls;
    private List<Attribute> attributes;
    private Date            created;
    private Date            updated;

    public static class Attribute {
        private String name;
        private String value;

        public Attribute(String name, String value) {
            super();
            this.name = name;
            this.value = value;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

    }

    public ChannelItemTypeVO() {
    }

    public ChannelItemTypeVO(WsChannelItemType channelItemType) {
        channelCode = channelItemType.getChannelCode();
        channelProductId = channelItemType.getChannelProductId();
        brand = channelItemType.getBrand();
        productDescription = channelItemType.getProductDescription();
        imageUrls = channelItemType.getImageUrls();
        if (channelItemType.getAttributes() != null && !channelItemType.getAttributes().isEmpty()) {
            attributes = new ArrayList<Attribute>(channelItemType.getAttributes().size());
            for (WsChannelItemTypeAttribute cita : channelItemType.getAttributes()) {
                attributes.add(new Attribute(cita.getName(), cita.getValue()));
            }
        }
        created = DateUtils.getCurrentTime();
        updated = DateUtils.getCurrentTime();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTenantCode() {
        return tenantCode;
    }

    public void setTenantCode(String tenantCode) {
        this.tenantCode = tenantCode;
    }

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    public String getChannelProductId() {
        return channelProductId;
    }

    public void setChannelProductId(String channelProductId) {
        this.channelProductId = channelProductId;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public Set<String> getImageUrls() {
        return imageUrls;
    }

    public void setImageUrls(Set<String> imageUrls) {
        this.imageUrls = imageUrls;
    }

    public List<Attribute> getAttributes() {
        return attributes;
    }

    public void setAttributes(List<Attribute> attributes) {
        this.attributes = attributes;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

}
