package com.uniware.core.vo;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by akshayag on 9/12/16.
 */

@Document(collection = "paymentReconciliation")
public class PaymentReconciliation {

    public enum ReconciliationStatus {
        RECONCILED,
        AWAITING_PAYMENT,
        DISPUTED,
        UNRECONCILED,
        UNRECONCILABLE
    };

    public enum ReconcileType {
        CREDIT_INITIATE,
        DEBIT_INITIATE
    };

    public enum UnReconcileXML {
        UnreonciliedItem,
        ReconciliationIdentifier,
        SellingPrice,
        ShippingCharges,
        ExpectedEarning,
        OrderTransactionDate,
        StatusCode
    }

    @Id
    private String     id;
    private String     orderReferenceId;
    private Integer settlementReferenceId;
    private String     tenantCode;
    private String     statusCode     = ReconciliationStatus.AWAITING_PAYMENT.name();
    private String     currencyCode;
    private BigDecimal receivedAmount = BigDecimal.ZERO;
    private Date       settlementDate;
    private Pricing    pricing;
    private Date       created;
    private Date       updated;

    public static class Pricing {
        private BigDecimal transferPrice;
        private BigDecimal costPrice;
        private BigDecimal channelTransferPrice;
        private BigDecimal commissionPercentage;
        private BigDecimal paymentGatewayCharge;
        private BigDecimal logisticsCost;
        private BigDecimal dropshipShippingCharges = BigDecimal.ZERO;
        private BigDecimal dropshipCommission      = BigDecimal.ZERO;
        private BigDecimal giftWrapCharges         = BigDecimal.ZERO;
        private BigDecimal totalPrice;
        private BigDecimal sellingPrice;
        private BigDecimal discount                = BigDecimal.ZERO;
        private BigDecimal prepaidAmount           = BigDecimal.ZERO;
        private BigDecimal shippingCharges         = BigDecimal.ZERO;
        private BigDecimal shippingMethodCharges   = BigDecimal.ZERO;
        private BigDecimal cashOnDeliveryCharges   = BigDecimal.ZERO;

        public BigDecimal getTransferPrice() {
            return transferPrice;
        }

        public void setTransferPrice(BigDecimal transferPrice) {
            this.transferPrice = transferPrice;
        }

        public BigDecimal getCostPrice() {
            return costPrice;
        }

        public void setCostPrice(BigDecimal costPrice) {
            this.costPrice = costPrice;
        }

        public BigDecimal getChannelTransferPrice() {
            return channelTransferPrice;
        }

        public void setChannelTransferPrice(BigDecimal channelTransferPrice) {
            this.channelTransferPrice = channelTransferPrice;
        }

        public BigDecimal getCommissionPercentage() {
            return commissionPercentage;
        }

        public void setCommissionPercentage(BigDecimal commissionPercentage) {
            this.commissionPercentage = commissionPercentage;
        }

        public BigDecimal getPaymentGatewayCharge() {
            return paymentGatewayCharge;
        }

        public void setPaymentGatewayCharge(BigDecimal paymentGatewayCharge) {
            this.paymentGatewayCharge = paymentGatewayCharge;
        }

        public BigDecimal getLogisticsCost() {
            return logisticsCost;
        }

        public void setLogisticsCost(BigDecimal logisticsCost) {
            this.logisticsCost = logisticsCost;
        }

        public BigDecimal getDropshipShippingCharges() {
            return dropshipShippingCharges;
        }

        public void setDropshipShippingCharges(BigDecimal dropshipShippingCharges) {
            this.dropshipShippingCharges = dropshipShippingCharges;
        }

        public BigDecimal getDropshipCommission() {
            return dropshipCommission;
        }

        public void setDropshipCommission(BigDecimal dropshipCommission) {
            this.dropshipCommission = dropshipCommission;
        }

        public BigDecimal getGiftWrapCharges() {
            return giftWrapCharges;
        }

        public void setGiftWrapCharges(BigDecimal giftWrapCharges) {
            this.giftWrapCharges = giftWrapCharges;
        }

        public BigDecimal getTotalPrice() {
            return totalPrice;
        }

        public void setTotalPrice(BigDecimal totalPrice) {
            this.totalPrice = totalPrice;
        }

        public BigDecimal getSellingPrice() {
            return sellingPrice;
        }

        public void setSellingPrice(BigDecimal sellingPrice) {
            this.sellingPrice = sellingPrice;
        }

        public BigDecimal getDiscount() {
            return discount;
        }

        public void setDiscount(BigDecimal discount) {
            this.discount = discount;
        }

        public BigDecimal getPrepaidAmount() {
            return prepaidAmount;
        }

        public void setPrepaidAmount(BigDecimal prepaidAmount) {
            this.prepaidAmount = prepaidAmount;
        }

        public BigDecimal getShippingCharges() {
            return shippingCharges;
        }

        public void setShippingCharges(BigDecimal shippingCharges) {
            this.shippingCharges = shippingCharges;
        }

        public BigDecimal getShippingMethodCharges() {
            return shippingMethodCharges;
        }

        public void setShippingMethodCharges(BigDecimal shippingMethodCharges) {
            this.shippingMethodCharges = shippingMethodCharges;
        }

        public BigDecimal getCashOnDeliveryCharges() {
            return cashOnDeliveryCharges;
        }

        public void setCashOnDeliveryCharges(BigDecimal cashOnDeliveryCharges) {
            this.cashOnDeliveryCharges = cashOnDeliveryCharges;
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOrderReferenceId() {
        return orderReferenceId;
    }

    public void setOrderReferenceId(String orderReferenceId) {
        this.orderReferenceId = orderReferenceId;
    }

    public Integer getSettlementReferenceId() {
        return settlementReferenceId;
    }

    public void setSettlementReferenceId(Integer settlementReferenceId) {
        this.settlementReferenceId = settlementReferenceId;
    }

    public String getTenantCode() {
        return tenantCode;
    }

    public void setTenantCode(String tenantCode) {
        this.tenantCode = tenantCode;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public BigDecimal getReceivedAmount() {
        return receivedAmount;
    }

    public void setReceivedAmount(BigDecimal receivedAmount) {
        this.receivedAmount = receivedAmount;
    }

    public Date getSettlementDate() {
        return settlementDate;
    }

    public void setSettlementDate(Date settlementDate) {
        this.settlementDate = settlementDate;
    }

    public Pricing getPricing() {
        return pricing;
    }

    public void setPricing(Pricing pricing) {
        this.pricing = pricing;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }
}
