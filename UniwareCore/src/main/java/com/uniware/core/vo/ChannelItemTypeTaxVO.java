package com.uniware.core.vo;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "channelItemTypeTax")
@CompoundIndexes({ @CompoundIndex(name = "tenant_channel_product", def = "{'tenantCode' :  1, 'channelCode' :  1, 'channelProductId' : 1}", unique = true) })
public class ChannelItemTypeTaxVO {

    @Id
    private String     id;
    private String     tenantCode;
    private String     channelCode;
    private String     channelProductId;
    private String     hsnCode;
    private BigDecimal vatPercentage;
    private BigDecimal cstPercentage;
    private BigDecimal centralGstPercentage;
    private BigDecimal stateGstPercentage;
    private BigDecimal unionTerritoryGstPercentage;
    private BigDecimal integratedGstPercentage;
    private BigDecimal compensationCessPercentage;
    private Date       created;
    private Date       updated;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTenantCode() {
        return tenantCode;
    }

    public void setTenantCode(String tenantCode) {
        this.tenantCode = tenantCode;
    }

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    public String getChannelProductId() {
        return channelProductId;
    }

    public void setChannelProductId(String channelProductId) {
        this.channelProductId = channelProductId;
    }

    public String getHsnCode() {
        return hsnCode;
    }

    public void setHsnCode(String hsnCode) {
        this.hsnCode = hsnCode;
    }

    public BigDecimal getVatPercentage() {
        return vatPercentage;
    }

    public void setVatPercentage(BigDecimal vatPercentage) {
        this.vatPercentage = vatPercentage;
    }

    public BigDecimal getCstPercentage() {
        return cstPercentage;
    }

    public void setCstPercentage(BigDecimal cstPercentage) {
        this.cstPercentage = cstPercentage;
    }

    public BigDecimal getCentralGstPercentage() {
        return centralGstPercentage;
    }

    public void setCentralGstPercentage(BigDecimal centralGstPercentage) {
        this.centralGstPercentage = centralGstPercentage;
    }

    public BigDecimal getStateGstPercentage() {
        return stateGstPercentage;
    }

    public void setStateGstPercentage(BigDecimal stateGstPercentage) {
        this.stateGstPercentage = stateGstPercentage;
    }

    public BigDecimal getUnionTerritoryGstPercentage() {
        return unionTerritoryGstPercentage;
    }

    public void setUnionTerritoryGstPercentage(BigDecimal unionTerritoryGstPercentage) {
        this.unionTerritoryGstPercentage = unionTerritoryGstPercentage;
    }

    public BigDecimal getIntegratedGstPercentage() {
        return integratedGstPercentage;
    }

    public void setIntegratedGstPercentage(BigDecimal integratedGstPercentage) {
        this.integratedGstPercentage = integratedGstPercentage;
    }

    public BigDecimal getCompensationCessPercentage() {
        return compensationCessPercentage;
    }

    public void setCompensationCessPercentage(BigDecimal compensationCessPercentage) {
        this.compensationCessPercentage = compensationCessPercentage;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

}
