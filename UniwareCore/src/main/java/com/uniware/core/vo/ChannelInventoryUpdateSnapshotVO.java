package com.uniware.core.vo;

import java.util.Date;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.unifier.core.utils.DateUtils;
import com.uniware.core.api.inventory.SearchInventorySnapshotResponse.InventorySnapshotDTO;
import com.uniware.core.entity.ChannelItemType;

@Document(collection = "channelInventoryUpdateSnapshot")
public class ChannelInventoryUpdateSnapshotVO {

    @Id
    private String                           id;
    private String                           tenantCode;
    private long                             calculatedInventory;
    private String                           formulaCode;
    private long                             pendingInventoryAssessment;
    private long                             openSale;
    private long                             inventory;
    private long                             openPurchase;
    private long                             putawayPending;
    private long                             pendingStockTransfer;
    private long                             vendorInventory;
    private long                             inventoryBlocked;
    private int                              inventoryBlockedOnOtherChannels;
    private long                             unprocessedOrderInventory;
    private int                              pendencyOnOtherChannels;
    private int                              totalPendency;
    private int                              pendency;
    private int                              blockedInventory;
    private int                              failedOrderInventory;
    private Integer                          lastInventoryUpdate;
    private List<ComponentItemTypeInventory> componentItemTypeInventories;
    private Date                             created;

    public ChannelInventoryUpdateSnapshotVO() {
        super();
    }

    public ChannelInventoryUpdateSnapshotVO(InventorySnapshotDTO is, ChannelItemType cit, String formulaCode) {
        this.formulaCode = formulaCode;
        this.pendency = cit.getPendency();
        this.blockedInventory = cit.getBlockedInventory();
        this.failedOrderInventory = cit.getFailedOrderInventory();
        this.lastInventoryUpdate = cit.getLastInventoryUpdate();
        this.created = DateUtils.getCurrentTime();

        if (is != null) {
            this.pendingInventoryAssessment = is.getPendingInventoryAssessment();
            this.openSale = is.getOpenSale();
            this.inventory = is.getInventory();
            this.openPurchase = is.getOpenPurchase();
            this.putawayPending = is.getPutawayPending();
            this.pendingStockTransfer = is.getPendingStockTransfer();
            this.vendorInventory = is.getVendorInventory();
            this.inventoryBlocked = is.getInventoryBlocked();
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTenantCode() {
        return tenantCode;
    }

    public void setTenantCode(String tenantCode) {
        this.tenantCode = tenantCode;
    }

    public long getCalculatedInventory() {
        return calculatedInventory;
    }

    public void setCalculatedInventory(long calculatedInventory) {
        this.calculatedInventory = calculatedInventory;
    }

    public String getFormulaCode() {
        return formulaCode;
    }

    public void setFormulaCode(String formulaCode) {
        this.formulaCode = formulaCode;
    }

    public long getOpenSale() {
        return openSale;
    }

    public void setOpenSale(long openSale) {
        this.openSale = openSale;
    }

    public long getInventory() {
        return inventory;
    }

    public void setInventory(long inventory) {
        this.inventory = inventory;
    }

    public long getOpenPurchase() {
        return openPurchase;
    }

    public void setOpenPurchase(long openPurchase) {
        this.openPurchase = openPurchase;
    }

    public long getPutawayPending() {
        return putawayPending;
    }

    public void setPutawayPending(long putawayPending) {
        this.putawayPending = putawayPending;
    }

    public long getPendingStockTransfer() {
        return pendingStockTransfer;
    }

    public void setPendingStockTransfer(long pendingStockTransfer) {
        this.pendingStockTransfer = pendingStockTransfer;
    }

    public long getVendorInventory() {
        return vendorInventory;
    }

    public void setVendorInventory(long vendorInventory) {
        this.vendorInventory = vendorInventory;
    }

    public long getInventoryBlocked() {
        return inventoryBlocked;
    }

    public void setInventoryBlocked(long inventoryBlocked) {
        this.inventoryBlocked = inventoryBlocked;
    }

    public int getInventoryBlockedOnOtherChannels() {
        return inventoryBlockedOnOtherChannels;
    }

    public void setInventoryBlockedOnOtherChannels(int inventoryBlockedOnOtherChannels) {
        this.inventoryBlockedOnOtherChannels = inventoryBlockedOnOtherChannels;
    }

    public int getPendencyOnOtherChannels() {
        return pendencyOnOtherChannels;
    }

    public void setPendencyOnOtherChannels(int pendencyOnOtherChannels) {
        this.pendencyOnOtherChannels = pendencyOnOtherChannels;
    }

    public int getTotalPendency() {
        return totalPendency;
    }

    public void setTotalPendency(int totalPendency) {
        this.totalPendency = totalPendency;
    }

    public int getPendency() {
        return pendency;
    }

    public void setPendency(int pendency) {
        this.pendency = pendency;
    }

    public int getBlockedInventory() {
        return blockedInventory;
    }

    public void setBlockedInventory(int blockedInventory) {
        this.blockedInventory = blockedInventory;
    }

    public int getFailedOrderInventory() {
        return failedOrderInventory;
    }

    public void setFailedOrderInventory(int failedOrderInventory) {
        this.failedOrderInventory = failedOrderInventory;
    }

    public long getUnprocessedOrderInventory() {
        return unprocessedOrderInventory;
    }

    public void setUnprocessedOrderInventory(long unprocessedOrderInventory) {
        this.unprocessedOrderInventory = unprocessedOrderInventory;
    }

    public Integer getLastInventoryUpdate() {
        return lastInventoryUpdate;
    }

    public void setLastInventoryUpdate(Integer lastInventoryUpdate) {
        this.lastInventoryUpdate = lastInventoryUpdate;
    }

    public List<ComponentItemTypeInventory> getComponentItemTypeInventories() {
        return componentItemTypeInventories;
    }

    public void setComponentItemTypeInventories(List<ComponentItemTypeInventory> componentItemTypeInventories) {
        this.componentItemTypeInventories = componentItemTypeInventories;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public long getPendingInventoryAssessment() {
        return pendingInventoryAssessment;
    }

    public void setPendingInventoryAssessment(long pendingInventoryAssessment) {
        this.pendingInventoryAssessment = pendingInventoryAssessment;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ChannelInventoryUpdateSnapshotVO that = (ChannelInventoryUpdateSnapshotVO) o;

        if (blockedInventory != that.blockedInventory) {
            return false;
        }
        if (calculatedInventory != that.calculatedInventory) {
            return false;
        }
        if (failedOrderInventory != that.failedOrderInventory) {
            return false;
        }
        if (inventory != that.inventory) {
            return false;
        }
        if (inventoryBlocked != that.inventoryBlocked) {
            return false;
        }
        if (inventoryBlockedOnOtherChannels != that.inventoryBlockedOnOtherChannels) {
            return false;
        }
        if (openPurchase != that.openPurchase) {
            return false;
        }
        if (openSale != that.openSale) {
            return false;
        }
        if (pendency != that.pendency) {
            return false;
        }
        if (pendencyOnOtherChannels != that.pendencyOnOtherChannels) {
            return false;
        }
        if (pendingInventoryAssessment != that.pendingInventoryAssessment) {
            return false;
        }
        if (pendingStockTransfer != that.pendingStockTransfer) {
            return false;
        }
        if (putawayPending != that.putawayPending) {
            return false;
        }
        if (totalPendency != that.totalPendency) {
            return false;
        }
        if (vendorInventory != that.vendorInventory) {
            return false;
        }
        if (componentItemTypeInventories != null ? !componentItemTypeInventories.equals(that.componentItemTypeInventories) : that.componentItemTypeInventories != null) {
            return false;
        }
        if (formulaCode != null ? !formulaCode.equals(that.formulaCode) : that.formulaCode != null) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (calculatedInventory ^ (calculatedInventory >>> 32));
        result = 31 * result + (formulaCode != null ? formulaCode.hashCode() : 0);
        result = 31 * result + (int) (pendingInventoryAssessment ^ (pendingInventoryAssessment >>> 32));
        result = 31 * result + (int) (openSale ^ (openSale >>> 32));
        result = 31 * result + (int) (inventory ^ (inventory >>> 32));
        result = 31 * result + (int) (openPurchase ^ (openPurchase >>> 32));
        result = 31 * result + (int) (putawayPending ^ (putawayPending >>> 32));
        result = 31 * result + (int) (pendingStockTransfer ^ (pendingStockTransfer >>> 32));
        result = 31 * result + (int) (vendorInventory ^ (vendorInventory >>> 32));
        result = 31 * result + (int) (inventoryBlocked ^ (inventoryBlocked >>> 32));
        result = 31 * result + inventoryBlockedOnOtherChannels;
        result = 31 * result + pendencyOnOtherChannels;
        result = 31 * result + totalPendency;
        result = 31 * result + pendency;
        result = 31 * result + blockedInventory;
        result = 31 * result + failedOrderInventory;
        result = 31 * result + (componentItemTypeInventories != null ? componentItemTypeInventories.hashCode() : 0);
        return result;
    }

    public static class ComponentItemTypeInventory {
        private String skuCode;
        private String channelProductId;
        private long   calculatedInventory;

        public ComponentItemTypeInventory() {
            super();
        }

        public ComponentItemTypeInventory(String skuCode, String channelProductId, long calculatedInventory) {
            super();
            this.skuCode = skuCode;
            this.channelProductId = channelProductId;
            this.calculatedInventory = calculatedInventory;
        }

        public String getSkuCode() {
            return skuCode;
        }

        public void setSkuCode(String skuCode) {
            this.skuCode = skuCode;
        }

        public String getChannelProductId() {
            return channelProductId;
        }

        public void setChannelProductId(String channelProductId) {
            this.channelProductId = channelProductId;
        }

        public long getCalculatedInventory() {
            return calculatedInventory;
        }

        public void setCalculatedInventory(long calculatedInventory) {
            this.calculatedInventory = calculatedInventory;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }

            ComponentItemTypeInventory that = (ComponentItemTypeInventory) o;

            if (calculatedInventory != that.calculatedInventory) {
                return false;
            }
            if (channelProductId != null ? !channelProductId.equals(that.channelProductId) : that.channelProductId != null) {
                return false;
            }
            if (!skuCode.equals(that.skuCode)) {
                return false;
            }

            return true;
        }

        @Override
        public int hashCode() {
            int result = skuCode.hashCode();
            result = 31 * result + (channelProductId != null ? channelProductId.hashCode() : 0);
            result = 31 * result + (int) (calculatedInventory ^ (calculatedInventory >>> 32));
            return result;
        }
    }

}
