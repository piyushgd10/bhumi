/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 05-Mar-2014
 *  @author amit
 */
package com.uniware.core.vo;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Represents selected templates.
 * 
 * @author amit
 */
@Document(collection = "printTemplate")
// @CompoundIndexes({ @CompoundIndex(def = "{'tenantCode' :  1, 'type' :  1}", unique = true) })
public class PrintTemplateVO implements Serializable {

    public enum Type {
        INVOICE,
        SHIPMENT_LABEL,
        SHIPPING_MANIFEST,
        SHIPPING_MANIFEST_CSV,
        PURCHASE_ORDER,
        PICKLIST,
        PICKLIST_PREVIEW,
        ITEM_LABEL,
        ITEM_LABEL_PRN,
        ITEM_TYPE_LABEL_CSV,
        ITEM_TYPE_LABEL_PRN,
        SHELF,
        SHELF_CSV,
        RETURN_MANIFEST,
        RETURN_MANIFEST_CSV,
        QC_REJECTION_REPORT,
        SHIPMENT_LABEL_CSV,
        PURCHASE_ORDER_CSV,
        OUTBOUND_GATE_PASS,
        WORKORDER,
        REVERSE_PICKUP,
        STATE_REGULATORY_FORM,
        BOM_PICKLIST_TEMPLATE,
        VENDOR_INVOICE,
        SHIPMENT_LABEL_PRN,
        PICKLIST_ITEM_CSV,
        INFLOW_RECEIPT,
        PICK_BUCKET_CSV
    }

    @Id
    private String              id;

    private String              tenantCode;

    private String              samplePrintTemplateCode;

    private String              type;

    private Map<String, String> customizationParameters = new HashMap<>();

    private boolean             enabled;
    
    private boolean             userCustomized;

    private Date                created;

    private Date                updated;

    public PrintTemplateVO() {
        super();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTenantCode() {
        return tenantCode;
    }

    public void setTenantCode(String tenantCode) {
        this.tenantCode = tenantCode;
    }

    public String getSamplePrintTemplateCode() {
        return samplePrintTemplateCode;
    }

    public void setSamplePrintTemplateCode(String samplePrintTemplateCode) {
        this.samplePrintTemplateCode = samplePrintTemplateCode;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public Map<String, String> getCustomizationParameters() {
        return customizationParameters;
    }

    public void setCustomizationParameters(Map<String, String> customizationParameters) {
        this.customizationParameters = customizationParameters;
    }
    
    public boolean isUserCustomized() {
        return userCustomized;
    }

    public void setUserCustomized(boolean userCustomized) {
        this.userCustomized = userCustomized;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    @Override
    public String toString() {
        return "PrintTemplateVO{" + "id='" + id + '\'' + ", tenantCode='" + tenantCode + '\'' + ", samplePrintTemplateCode='" + samplePrintTemplateCode + '\'' + ", type='" + type
                + '\'' + ", customizationParameters=" + customizationParameters + ", enabled=" + enabled + ", created=" + created + ", updated=" + updated + '}';
    }
}
