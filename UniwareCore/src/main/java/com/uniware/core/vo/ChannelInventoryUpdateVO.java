package com.uniware.core.vo;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "channelInventoryUpdate_O2")
/*
@CompoundIndexes({
        @CompoundIndex(def = "{'tenantCode' :  1, 'channelCode' :  1, 'channelProductId' : 1}"),
        @CompoundIndex(def = "{'tenantCode' :  1, 'channelCode' :  1, 'statusCode' :  1}") })
        */
public class ChannelInventoryUpdateVO {

    public enum Status {
        SUCCESS,
        FAILED,
        SKIPPED
    }

    @Id
    private String id;
    private String tenantCode;
    private String channelCode;
    private String channelProductId;
    private String skuCode;
    private String sellerSkuCode;
    private String productName;
    private int    calculatedInventory;
    private String statusCode;
    private String message;
    private String snapshotReferenceId;
    private Date   created;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTenantCode() {
        return tenantCode;
    }

    public void setTenantCode(String tenantCode) {
        this.tenantCode = tenantCode;
    }

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    public String getSkuCode() {
        return skuCode;
    }

    public void setSkuCode(String skuCode) {
        this.skuCode = skuCode;
    }

    public String getChannelProductId() {
        return channelProductId;
    }

    public void setChannelProductId(String channelProductId) {
        this.channelProductId = channelProductId;
    }

    public String getSellerSkuCode() {
        return sellerSkuCode;
    }

    public void setSellerSkuCode(String channelSkuCode) {
        this.sellerSkuCode = channelSkuCode;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getCalculatedInventory() {
        return calculatedInventory;
    }

    public void setCalculatedInventory(int calculatedInventory) {
        this.calculatedInventory = calculatedInventory;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSnapshotReferenceId() {
        return snapshotReferenceId;
    }

    public void setSnapshotReferenceId(String snapshotReferenceId) {
        this.snapshotReferenceId = snapshotReferenceId;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

}
