package com.uniware.core.vo;

import com.uniware.core.entity.Tenant;
import org.springframework.data.mongodb.core.mapping.Document;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Document(collection = "uiCustomList")
public class UICustomListVO {

    /**
     *
     */
    @Id
    private String id;
    private String tenantCode;
    private String name;
    private String options;
    private Date   created;
    private Date   updated;

    public UICustomListVO() {
    }

    public UICustomListVO(String tenantCode, String name, String options, Date created, Date updated) {
        this.tenantCode = tenantCode;
        this.name = name;
        this.options = options;
        this.created = created;
        this.updated = updated;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTenantCode() {
        return tenantCode;
    }

    public void setTenantCode(String tenantCode) {
        this.tenantCode = tenantCode;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOptions() {
        return this.options;
    }

    public void setOptions(String options) {
        this.options = options;
    }

    public Date getCreated() {
        return this.created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return this.updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

}
