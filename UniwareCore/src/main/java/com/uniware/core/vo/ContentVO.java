/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 16/04/14
 *  @author amit
 */

package com.uniware.core.vo;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "content")
/*
@CompoundIndexes({
        @CompoundIndex(def = "{'nsIdentifier' :  1, 'languageCode' : 1}"),
        @CompoundIndex(def = "{'nsIdentifier': 1, 'languageCode':1, 'key':1}", unique = true) })
*/
public class ContentVO {

    // ISO 639-2.
    public enum LanguageCode {
        ENG,
        HIN
    }

    @Id
    private String       id;

    private String       nsIdentifier;
    private String       key;
    private LanguageCode languageCode;
    private String       content;

    public ContentVO() {
        super();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNsIdentifier() {
        return nsIdentifier;
    }

    public void setNsIdentifier(String nsIdentifier) {
        this.nsIdentifier = nsIdentifier;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }

    public void setLanguageCode(LanguageCode languageCode) {
        this.languageCode = languageCode;
    }

    public LanguageCode getLanguageCode() {
        return languageCode;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    @Override
    public String toString() {
        return "ContentVO{" + "nsIdentifier='" + nsIdentifier + '\'' + ", key='" + key + '\'' + ", languageCode=" + languageCode + ", content='" + content + '\'' + '}';
    }

}
