/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 02-July-2015
 *  @author akshay
 */
package com.uniware.core.vo;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Id;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "userProfile")
//@CompoundIndexes({ @CompoundIndex(name = "tenant_username", def = "{'tenantCode' : 1, 'username' : 1}", unique = true) })
public class UserProfileVO {

    @Id
    private String id;
    private String username;
    private String tenantCode;
    private boolean verified = false;
    private Date    lastReadTime;
    private Date    notificationReadTime;
    private Integer lastReleaseUpdateVersionRead;
    private boolean referrerNotificationRead;
    private Date    referrerNotificationReadTime;
    private Set<String> visitedUrls = new HashSet<>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getTenantCode() {
        return tenantCode;
    }

    public void setTenantCode(String tenantCode) {
        this.tenantCode = tenantCode;
    }

    public boolean isVerified() {
        return verified;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }

    public Date getLastReadTime() {
        return lastReadTime;
    }

    public void setLastReadTime(Date lastReadTime) {
        this.lastReadTime = lastReadTime;
    }

    public Date getNotificationReadTime() {
        return notificationReadTime;
    }

    public void setNotificationReadTime(Date notificationReadTime) {
        this.notificationReadTime = notificationReadTime;
    }

    public Integer getLastReleaseUpdateVersionRead() {
        return lastReleaseUpdateVersionRead;
    }

    public void setLastReleaseUpdateVersionRead(Integer lastReleaseUpdateVersionRead) {
        this.lastReleaseUpdateVersionRead = lastReleaseUpdateVersionRead;
    }

    public boolean isReferrerNotificationRead() {
        return referrerNotificationRead;
    }

    public void setReferrerNotificationRead(boolean referrerNotificationRead) {
        this.referrerNotificationRead = referrerNotificationRead;
    }

    public Date getReferrerNotificationReadTime() {
        return referrerNotificationReadTime;
    }

    public void setReferrerNotificationReadTime(Date referrerNotificationReadTime) {
        this.referrerNotificationReadTime = referrerNotificationReadTime;
    }

    public Set<String> getVisitedUrls() {
        return visitedUrls;
    }

    public void setVisitedUrls(Set<String> visitedUrls) {
        this.visitedUrls = visitedUrls;
    }

}
