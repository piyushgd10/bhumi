/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 25-Apr-2014
 *  @author unicom
 */
package com.uniware.core.vo;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "apiLimits")
public class ApiLimitsVO {
    private String tenantCode;
    private String apiName;
    private int    limit;
    private int    timeInSeconds;

    public String getApiName() {
        return apiName;
    }

    public String getTenantCode() {
        return tenantCode;
    }

    public void setTenantCode(String tenantCode) {
        this.tenantCode = tenantCode;
    }

    public void setApiName(String apiName) {
        this.apiName = apiName;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getTimeInSeconds() {
        return timeInSeconds;
    }

    public void setTimeInSeconds(int timeInSeconds) {
        this.timeInSeconds = timeInSeconds;
    }

}
