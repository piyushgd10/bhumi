/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 22-Aug-2013
 *  @author sunny
 */
package com.uniware.core.vo;

import com.google.gson.Gson;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "dataSourceConfiguration")
@CompoundIndexes({ @CompoundIndex(name = "serverNameAndType", def = "{'serverName' :  1, 'type' :  1}", unique = true) })
public class DataSourceConfigurationVO {

    // <Resource name="jdbc/UniwareDS"
    // auth="Container"
    // type="javax.sql.DataSource"
    // maxActive="100"
    // maxIdle="20"
    // maxWaitMillis="15000"
    // username="root"
    // password="uniware"
    // driverClassName="com.mysql.jdbc.Driver"
    // removeAbandoned="true"
    // removeAbandonedTimeout="30"
    // url="jdbc:mysql://localhost:3306/uniware?autoReconnect=true&amp;characterEncoding=UTF-8"/>

    public enum Type {
        MAIN,
        REPLICATION
    }

    @Id
    private String  id;

    private String  serverName;
    private Type    type;

    private String  driverClassName;
    private String  url;
    private String  username;
    private String  password;

    private String  defaultCatalog;
    private String  evictionPolicyClassName;

    private boolean abandonedUsageTracking;
    private boolean cacheState;
    private boolean enableAutoCommitOnReturn;

    private int     initialSize;
    private boolean lifo;
    private boolean logAbandoned;
    private int     maxConnLifetimeMillis;
    private int     maxIdle;
    private int     maxOpenPreparedStatements;
    private int     maxTotal;
    private int     maxWaitMillis;
    private int     minIdle;
    private int     numTestsPerEvictionRun;
    private boolean poolPreparedStatements;
    private boolean removeAbandonedOnBorrow;
    private boolean removeAbandonedOnMaintenance;
    private int     removeAbandonedTimeout;
    private boolean rollbackOnReturn;
    private int     softMinEvictableIdleTimeMillis;
    private boolean testOnBorrow;
    private boolean testOnCreate;
    private boolean testOnReturn;
    private boolean testWhileIdle;
    private int     timeBetweenEvictionRunsMillis;

    private String  validationQuery;
    private int     validationQueryTimeout;
    private boolean accessToUnderlyingConnectionAllowed;
    private boolean defaultReadOnly;
    private String  connectionProperties;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getServerName() {
        return serverName;
    }

    public void setServerName(String serverName) {
        this.serverName = serverName;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public String getDriverClassName() {
        return driverClassName;
    }

    public void setDriverClassName(String driverClassName) {
        this.driverClassName = driverClassName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDefaultCatalog() {
        return defaultCatalog;
    }

    public void setDefaultCatalog(String defaultCatalog) {
        this.defaultCatalog = defaultCatalog;
    }

    public String getEvictionPolicyClassName() {
        return evictionPolicyClassName;
    }

    public void setEvictionPolicyClassName(String evictionPolicyClassName) {
        this.evictionPolicyClassName = evictionPolicyClassName;
    }

    public boolean isAbandonedUsageTracking() {
        return abandonedUsageTracking;
    }

    public void setAbandonedUsageTracking(boolean abandonedUsageTracking) {
        this.abandonedUsageTracking = abandonedUsageTracking;
    }

    public boolean isCacheState() {
        return cacheState;
    }

    public void setCacheState(boolean cacheState) {
        this.cacheState = cacheState;
    }

    public boolean isEnableAutoCommitOnReturn() {
        return enableAutoCommitOnReturn;
    }

    public void setEnableAutoCommitOnReturn(boolean enableAutoCommitOnReturn) {
        this.enableAutoCommitOnReturn = enableAutoCommitOnReturn;
    }

    public int getInitialSize() {
        return initialSize;
    }

    public void setInitialSize(int initialSize) {
        this.initialSize = initialSize;
    }

    public boolean isLifo() {
        return lifo;
    }

    public void setLifo(boolean lifo) {
        this.lifo = lifo;
    }

    public boolean isLogAbandoned() {
        return logAbandoned;
    }

    public void setLogAbandoned(boolean logAbandoned) {
        this.logAbandoned = logAbandoned;
    }

    public int getMaxConnLifetimeMillis() {
        return maxConnLifetimeMillis;
    }

    public void setMaxConnLifetimeMillis(int maxConnLifetimeMillis) {
        this.maxConnLifetimeMillis = maxConnLifetimeMillis;
    }

    public int getMaxIdle() {
        return maxIdle;
    }

    public void setMaxIdle(int maxIdle) {
        this.maxIdle = maxIdle;
    }

    public int getMaxOpenPreparedStatements() {
        return maxOpenPreparedStatements;
    }

    public void setMaxOpenPreparedStatements(int maxOpenPreparedStatements) {
        this.maxOpenPreparedStatements = maxOpenPreparedStatements;
    }

    public int getMaxTotal() {
        return maxTotal;
    }

    public void setMaxTotal(int maxTotal) {
        this.maxTotal = maxTotal;
    }

    public int getMaxWaitMillis() {
        return maxWaitMillis;
    }

    public void setMaxWaitMillis(int maxWaitMillis) {
        this.maxWaitMillis = maxWaitMillis;
    }

    public int getMinIdle() {
        return minIdle;
    }

    public void setMinIdle(int minIdle) {
        this.minIdle = minIdle;
    }

    public int getNumTestsPerEvictionRun() {
        return numTestsPerEvictionRun;
    }

    public void setNumTestsPerEvictionRun(int numTestsPerEvictionRun) {
        this.numTestsPerEvictionRun = numTestsPerEvictionRun;
    }

    public boolean isPoolPreparedStatements() {
        return poolPreparedStatements;
    }

    public void setPoolPreparedStatements(boolean poolPreparedStatements) {
        this.poolPreparedStatements = poolPreparedStatements;
    }

    public boolean isRemoveAbandonedOnBorrow() {
        return removeAbandonedOnBorrow;
    }

    public void setRemoveAbandonedOnBorrow(boolean removeAbandonedOnBorrow) {
        this.removeAbandonedOnBorrow = removeAbandonedOnBorrow;
    }

    public boolean isRemoveAbandonedOnMaintenance() {
        return removeAbandonedOnMaintenance;
    }

    public void setRemoveAbandonedOnMaintenance(boolean removeAbandonedOnMaintenance) {
        this.removeAbandonedOnMaintenance = removeAbandonedOnMaintenance;
    }

    public int getRemoveAbandonedTimeout() {
        return removeAbandonedTimeout;
    }

    public void setRemoveAbandonedTimeout(int removeAbandonedTimeout) {
        this.removeAbandonedTimeout = removeAbandonedTimeout;
    }

    public boolean isRollbackOnReturn() {
        return rollbackOnReturn;
    }

    public void setRollbackOnReturn(boolean rollbackOnReturn) {
        this.rollbackOnReturn = rollbackOnReturn;
    }

    public int getSoftMinEvictableIdleTimeMillis() {
        return softMinEvictableIdleTimeMillis;
    }

    public void setSoftMinEvictableIdleTimeMillis(int softMinEvictableIdleTimeMillis) {
        this.softMinEvictableIdleTimeMillis = softMinEvictableIdleTimeMillis;
    }

    public boolean isTestOnBorrow() {
        return testOnBorrow;
    }

    public void setTestOnBorrow(boolean testOnBorrow) {
        this.testOnBorrow = testOnBorrow;
    }

    public boolean isTestOnCreate() {
        return testOnCreate;
    }

    public void setTestOnCreate(boolean testOnCreate) {
        this.testOnCreate = testOnCreate;
    }

    public boolean isTestOnReturn() {
        return testOnReturn;
    }

    public void setTestOnReturn(boolean testOnReturn) {
        this.testOnReturn = testOnReturn;
    }

    public boolean isTestWhileIdle() {
        return testWhileIdle;
    }

    public void setTestWhileIdle(boolean testWhileIdle) {
        this.testWhileIdle = testWhileIdle;
    }

    public int getTimeBetweenEvictionRunsMillis() {
        return timeBetweenEvictionRunsMillis;
    }

    public void setTimeBetweenEvictionRunsMillis(int timeBetweenEvictionRunsMillis) {
        this.timeBetweenEvictionRunsMillis = timeBetweenEvictionRunsMillis;
    }

    public String getValidationQuery() {
        return validationQuery;
    }

    public void setValidationQuery(String validationQuery) {
        this.validationQuery = validationQuery;
    }

    public int getValidationQueryTimeout() {
        return validationQueryTimeout;
    }

    public void setValidationQueryTimeout(int validationQueryTimeout) {
        this.validationQueryTimeout = validationQueryTimeout;
    }

    public boolean isAccessToUnderlyingConnectionAllowed() {
        return accessToUnderlyingConnectionAllowed;
    }

    public void setAccessToUnderlyingConnectionAllowed(boolean accessToUnderlyingConnectionAllowed) {
        this.accessToUnderlyingConnectionAllowed = accessToUnderlyingConnectionAllowed;
    }

    public boolean isDefaultReadOnly() {
        return defaultReadOnly;
    }

    public void setDefaultReadOnly(boolean defaultReadOnly) {
        this.defaultReadOnly = defaultReadOnly;
    }

    public String getConnectionProperties() {
        return connectionProperties;
    }

    public void setConnectionProperties(String connectionProperties) {
        this.connectionProperties = connectionProperties;
    }

    @Override
    public String toString() {
        return "DataSourceConfigurationVO{" + "id='" + id + '\'' + ", serverName='" + serverName + '\'' + ", type=" + type + ", driverClassName='" + driverClassName + '\''
                + ", url='" + url + '\'' + ", username='" + username + '\'' + ", password='" + password + '\'' + ", defaultCatalog='" + defaultCatalog + '\''
                + ", evictionPolicyClassName='" + evictionPolicyClassName + '\'' + ", abandonedUsageTracking=" + abandonedUsageTracking + ", cacheState=" + cacheState
                + ", enableAutoCommitOnReturn=" + enableAutoCommitOnReturn + ", initialSize=" + initialSize + ", lifo=" + lifo + ", logAbandoned=" + logAbandoned
                + ", maxConnLifetimeMillis=" + maxConnLifetimeMillis + ", maxIdle=" + maxIdle + ", maxOpenPreparedStatements=" + maxOpenPreparedStatements + ", maxTotal="
                + maxTotal + ", maxWaitMillis=" + maxWaitMillis + ", minIdle=" + minIdle + ", numTestsPerEvictionRun=" + numTestsPerEvictionRun + ", poolPreparedStatements="
                + poolPreparedStatements + ", removeAbandonedOnBorrow=" + removeAbandonedOnBorrow + ", removeAbandonedOnMaintenance=" + removeAbandonedOnMaintenance
                + ", removeAbandonedTimeout=" + removeAbandonedTimeout + ", rollbackOnReturn=" + rollbackOnReturn + ", softMinEvictableIdleTimeMillis='"
                + softMinEvictableIdleTimeMillis + '\'' + ", testOnBorrow=" + testOnBorrow + ", testOnCreate=" + testOnCreate + ", testOnReturn=" + testOnReturn
                + ", testWhileIdle=" + testWhileIdle + ", timeBetweenEvictionRunsMillis=" + timeBetweenEvictionRunsMillis + ", validationQuery='" + validationQuery + '\''
                + ", validationQueryTimeout=" + validationQueryTimeout + ", accessToUnderlyingConnectionAllowed=" + accessToUnderlyingConnectionAllowed + ", defaultReadOnly="
                + defaultReadOnly + ", connectionProperties='" + connectionProperties + '\'' + '}';
    }

    public static void main(String[] args) {
        DataSourceConfigurationVO d = new DataSourceConfigurationVO();
        d.setType(Type.MAIN);
        d.setServerName("StgEnterprise1");
        d.setDriverClassName("com.mysql.jdbc.Driver");
        d.setUrl("jdbc:mysql://localhost:3306/uniware?autoReconnect=true");
        d.setUsername("root");
        d.setPassword("uniware");
        d.setDefaultCatalog("uniware");
        d.setEvictionPolicyClassName("org.apache.tomcat.dbcp.pool2.impl.DefaultEvictionPolicy");
        d.setAbandonedUsageTracking(false);
        d.setCacheState(true);
        d.setEnableAutoCommitOnReturn(true);
        d.setInitialSize(1);
        d.setLifo(true);
        d.setLogAbandoned(false);
        d.setMaxConnLifetimeMillis(-1);
        d.setMaxIdle(20);
        d.setMaxOpenPreparedStatements(-1);
        d.setMaxTotal(-1);
        d.setMaxWaitMillis(15000);
        d.setMinIdle(0);
        d.setNumTestsPerEvictionRun(3);
        d.setPoolPreparedStatements(false);
        d.setRemoveAbandonedOnBorrow(false);
        d.setRemoveAbandonedOnMaintenance(false);
        d.setRemoveAbandonedTimeout(300);
        d.setRollbackOnReturn(true);
        d.setSoftMinEvictableIdleTimeMillis(-1);
        d.setTestOnBorrow(true);
        d.setTestOnCreate(false);
        d.setTestOnReturn(false);
        d.setTestWhileIdle(false);
        d.setTimeBetweenEvictionRunsMillis(-1);
        d.setValidationQuery("select 1");
        d.setValidationQueryTimeout(100);
        d.setAccessToUnderlyingConnectionAllowed(false);
        d.setDefaultReadOnly(false);
        d.setConnectionProperties("");
        System.out.println(new Gson().toJson(d));
    }
}
