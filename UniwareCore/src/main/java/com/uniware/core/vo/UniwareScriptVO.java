package com.uniware.core.vo;

import java.util.Date;

import javax.persistence.Id;

import org.springframework.data.mongodb.core.mapping.Document;

import com.unifier.core.transport.http.HttpProxy;

@Document(collection = "uniwareScript")
/*
@CompoundIndexes({
        @CompoundIndex(def = "{'tenantCode' : 1, 'name' : 1}", unique = true),
        @CompoundIndex(def = "{'loadEagerly': 1,'tenantCode': 1}")})
*/
public class UniwareScriptVO {

    public enum Name {
        THIRD_PARTY_SHIPPING_PROVIDER_ALLOCATION_SCRIPT,
        FACILITY_ALLOCATION_SCRIPT,
        EVALUATE_TEMPLATE_ON_DUMMY_ORDER_SCRIPT,
        FETCH_SELLER_DETAILS_USING_TIN
    }

    @Id
    private String    id;

    private String    name;
    private String    script;
    private boolean   loadEagerly = false;
    private HttpProxy httpProxy;
    private String    httpProxyServer;
    private String    version;
    private Date      created;
    private Date      updated;

    public UniwareScriptVO() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getScript() {
        return script;
    }

    public void setScript(String script) {
        this.script = script;
    }

    public HttpProxy getHttpProxy() {
        return httpProxy;
    }

    public void setHttpProxy(HttpProxy httpProxy) {
        this.httpProxy = httpProxy;
    }

    public String getHttpProxyServer() {
        return httpProxyServer;
    }

    public void setHttpProxyServer(String httpProxyServer) {
        this.httpProxyServer = httpProxyServer;
    }

    public boolean isLoadEagerly() {
        return loadEagerly;
    }

    public void setLoadEagerly(boolean loadEagerly) {
        this.loadEagerly = loadEagerly;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

}
