/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 22-Aug-2013
 *  @author sunny
 */
package com.uniware.core.vo;

import java.util.Date;

import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;

import com.uniware.core.api.saleorder.CreateSaleOrderRequest;
import com.uniware.core.api.saleorder.CreateSaleOrderResponse;

@Document(collection = "saleOrder")
//@CompoundIndexes({ @CompoundIndex(def = "{'tenantCode' :  1, 'code' :  1}", unique = true) })
public class SaleOrderVO {

    public enum StatusCode {
        PARTIALLY_CREATED,
        FAILED
    }

    private String                  code;
    private String                  tenantCode;
    private Date                    created;
    private Date                    updated;
    private CreateSaleOrderRequest  request;
    private CreateSaleOrderResponse response;
    private StatusCode              statusCode;
    private boolean                 fixable;

    /**
     * 
     */
    public SaleOrderVO() {
    }

    /**
     * @param code
     * @param tenantCode
     * @param created
     * @param request
     * @param response
     * @param statusCode TODO
     */
    public SaleOrderVO(String code, String tenantCode, Date created, CreateSaleOrderRequest request, CreateSaleOrderResponse response, StatusCode statusCode) {
        this.code = code;
        this.tenantCode = tenantCode;
        this.created = created;
        this.request = request;
        this.response = response;
        this.statusCode = statusCode;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTenantCode() {
        return tenantCode;
    }

    public void setTenantCode(String tenantCode) {
        this.tenantCode = tenantCode;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public CreateSaleOrderRequest getRequest() {
        return request;
    }

    public void setRequest(CreateSaleOrderRequest request) {
        this.request = request;
    }

    public CreateSaleOrderResponse getResponse() {
        return response;
    }

    public void setResponse(CreateSaleOrderResponse response) {
        this.response = response;
    }

    public StatusCode getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(StatusCode statusCode) {
        this.statusCode = statusCode;
    }

    public boolean isFixable() {
        return fixable;
    }

    public void setFixable(boolean fixable) {
        this.fixable = fixable;
    }

    @Override
    public String toString() {
        return "SaleOrderVO [code=" + code + ", tenantCode=" + tenantCode + ", created=" + created + ", request=" + request + ", response=" + response + ", statusCode="
                + statusCode + "]";
    }

}
