/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 23-Apr-2014
 *  @author parijat
 */
package com.uniware.core.vo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;

import com.unifier.core.api.audit.AuditItem;
import com.unifier.core.api.audit.AuditItemField;
import com.unifier.core.utils.StringUtils;

@Document(collection = "entityAuditLogs")
// @CompoundIndexes({ @CompoundIndex(def = "{'identifier':1,'entityName':1,'tenantCode':1,'facilityCode':1}") })
public class EntityAuditLogVO {

    @Id
    private String             id;
    private String             entityId;
    private String             identifier;
    private String             groupIdentifier;
    private String             entityName;
    private String             tenantCode;
    private String             facilityCode;
    private String             username;
    private String             apiUsername;
    private List<FieldAuditVO> fields = new ArrayList<>();
    private String             methodName;
    private String             auditLog;
    private Date               created;

    public EntityAuditLogVO() {

    }

    public EntityAuditLogVO(AuditItem auditItem) {
        this.entityId = auditItem.getEntityId();
        this.identifier = auditItem.getIdentifier();
        this.groupIdentifier = auditItem.getGroupIdentifier();
        this.entityName = auditItem.getEntityName();
        for (AuditItemField field : auditItem.getAuditFields().values()) {
            if ((StringUtils.isNotBlank(field.getOldValue()) && StringUtils.isNotBlank(field.getNewValue()) && !field.getOldValue().equalsIgnoreCase(field.getNewValue()) || (StringUtils.isBlank(field.getOldValue()) && StringUtils.isNotBlank(field.getNewValue())))) {
                FieldAuditVO fieldVO = new FieldAuditVO(field.getFieldName(), field.getOldValue(), field.getNewValue());
                this.fields.add(fieldVO);
            }
        }
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the entityId
     */
    public String getEntityId() {
        return entityId;
    }

    /**
     * @param entityId the entityId to set
     */
    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    /**
     * @return the identifier
     */
    public String getIdentifier() {
        return identifier;
    }

    /**
     * @param identifier the identifier to set
     */
    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    /**
     * @return the groupIdentifier
     */
    public String getGroupIdentifier() {
        return groupIdentifier;
    }

    /**
     * @param groupIdentifier the groupIdentifier to set
     */
    public void setGroupIdentifier(String groupIdentifier) {
        this.groupIdentifier = groupIdentifier;
    }

    /**
     * @return the entityName
     */
    public String getEntityName() {
        return entityName;
    }

    /**
     * @param entityName the entityName to set
     */
    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    /**
     * @return the tenantCode
     */
    public String getTenantCode() {
        return tenantCode;
    }

    /**
     * @param tenantCode the tenantCode to set
     */
    public void setTenantCode(String tenantCode) {
        this.tenantCode = tenantCode;
    }

    /**
     * @return the facilityCode
     */
    public String getFacilityCode() {
        return facilityCode;
    }

    /**
     * @param facilityCode the facilityCode to set
     */
    public void setFacilityCode(String facilityCode) {
        this.facilityCode = facilityCode;
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the apiUsername
     */
    public String getApiUsername() {
        return apiUsername;
    }

    /**
     * @param apiUsername the apiUsername to set
     */
    public void setApiUsername(String apiUsername) {
        this.apiUsername = apiUsername;
    }

    /**
     * @return the methodName
     */
    public String getMethodName() {
        return methodName;
    }

    /**
     * @param methodName the methodName to set
     */
    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    /**
     * @return the auditLog
     */
    public String getAuditLog() {
        return auditLog;
    }

    /**
     * @param auditLog the auditLog to set
     */
    public void setAuditLog(String auditLog) {
        this.auditLog = auditLog;
    }

    /**
     * @return the fields
     */
    public List<FieldAuditVO> getFields() {
        return fields;
    }

    /**
     * @param fields the fields to set
     */
    public void setFields(List<FieldAuditVO> fields) {
        this.fields = fields;
    }

    /**
     * @return the created
     */
    public Date getCreated() {
        return created;
    }

    /**
     * @param created the created to set
     */
    public void setCreated(Date created) {
        this.created = created;
    }

    public class FieldAuditVO {
        private String fieldName;
        private String oldValue;
        private String newValue;

        public FieldAuditVO(String fieldName, String oldValue, String newValue) {
            this.fieldName = fieldName;
            this.oldValue = oldValue;
            this.newValue = newValue;
        }

        /**
         * @return the fieldName
         */
        public String getFieldName() {
            return fieldName;
        }

        /**
         * @return the oldValue
         */
        public String getOldValue() {
            return oldValue;
        }

        /**
         * @return the newValue
         */
        public String getNewValue() {
            return newValue;
        }

    }
}
