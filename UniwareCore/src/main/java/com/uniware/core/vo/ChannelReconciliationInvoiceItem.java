/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 13-Oct-2014
 *  @author parijat
 */
package com.uniware.core.vo;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Id;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "channelReconciliationInvoiceItem")
/*
@CompoundIndexes({
        @CompoundIndex(def = "{invoiceId:1, reconciliationIdentifier:1}"),
        @CompoundIndex(def = "{tenantCode:1, isProcessed:1}") })
*/
public class ChannelReconciliationInvoiceItem {

    public enum InvoiceItemProcessedStatus {
        UNPROCESSED,
        PROCESSED,
        ORDER_NOT_RECONCILABLE,
        InvoiceItemProcessedStatus, ORDER_NOT_FOUND
    }

    @Id
    private String     id;
    private String     invoiceCode;
    private String     reconciliationIdentifier;
    private String     channelCode;
    private String     tenantCode;
    private String     channelSaleOrderCode;
    private Date       orderDate;
    private String     invoiceItemOrderStatus;
    private String     channelProductName;
    private String     additionalInfo;
    private double     orderItemValue          = 0;
    private double     settledValue            = 0;
    private Date       depositDate;
    private String     statusCode              = InvoiceItemProcessedStatus.UNPROCESSED.name();
    private int        quantity;
    private BigDecimal totalRewards            = BigDecimal.ZERO;
    private BigDecimal totalDiscounts          = BigDecimal.ZERO;
    private BigDecimal totalOtherIncentive     = BigDecimal.ZERO;
    private BigDecimal totalSellerEarning      = BigDecimal.ZERO;
    private BigDecimal totalCommission         = BigDecimal.ZERO;
    private BigDecimal totalFixedFee           = BigDecimal.ZERO;
    private BigDecimal totalShippingCharge     = BigDecimal.ZERO;
    private BigDecimal totalShippingFee        = BigDecimal.ZERO;
    private BigDecimal totalReverseShippingFee = BigDecimal.ZERO;
    private BigDecimal totalAdditionalFee      = BigDecimal.ZERO;
    private BigDecimal totalChannelPenalty     = BigDecimal.ZERO;
    private double     totalChannelRecovery    = 0;
    private BigDecimal totalTax                = BigDecimal.ZERO;
    private String     additionalFeeDescription;
    private Date       created;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getInvoiceCode() {
        return invoiceCode;
    }

    public void setInvoiceCode(String invoiceCode) {
        this.invoiceCode = invoiceCode;
    }

    public String getReconciliationIdentifier() {
        return reconciliationIdentifier;
    }

    public void setReconciliationIdentifier(String reconciliationIdentifier) {
        this.reconciliationIdentifier = reconciliationIdentifier;
    }

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    public String getTenantCode() {
        return tenantCode;
    }

    public void setTenantCode(String tenantCode) {
        this.tenantCode = tenantCode;
    }

    public String getChannelSaleOrderCode() {
        return channelSaleOrderCode;
    }

    public void setChannelSaleOrderCode(String channelSaleOrderCode) {
        this.channelSaleOrderCode = channelSaleOrderCode;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public String getInvoiceItemOrderStatus() {
        return invoiceItemOrderStatus;
    }

    public void setInvoiceItemOrderStatus(String invoiceItemOrderStatus) {
        this.invoiceItemOrderStatus = invoiceItemOrderStatus;
    }

    public String getChannelProductName() {
        return channelProductName;
    }

    public void setChannelProductName(String channelProductName) {
        this.channelProductName = channelProductName;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    public double getOrderItemValue() {
        return orderItemValue;
    }

    public void setOrderItemValue(double orderItemValue) {
        this.orderItemValue = orderItemValue;
    }

    public void setTotalChannelRecovery(double totalChannelRecovery) {
        this.totalChannelRecovery = totalChannelRecovery;
    }

    public double getSettledValue() {
        return settledValue;
    }

    public void setSettledValue(double settledValue) {
        this.settledValue = settledValue;
    }

    public Date getDepositDate() {
        return depositDate;
    }

    public void setDepositDate(Date depositDate) {
        this.depositDate = depositDate;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getTotalRewards() {
        return totalRewards;
    }

    public void setTotalRewards(BigDecimal totalRewards) {
        this.totalRewards = totalRewards;
    }

    public BigDecimal getTotalDiscounts() {
        return totalDiscounts;
    }

    public void setTotalDiscounts(BigDecimal totalDiscounts) {
        this.totalDiscounts = totalDiscounts;
    }

    public BigDecimal getTotalOtherIncentive() {
        return totalOtherIncentive;
    }

    public void setTotalOtherIncentive(BigDecimal totalOtherIncentive) {
        this.totalOtherIncentive = totalOtherIncentive;
    }

    public BigDecimal getTotalSellerEarning() {
        return totalSellerEarning;
    }

    public void setTotalSellerEarning(BigDecimal totalSellerEarning) {
        this.totalSellerEarning = totalSellerEarning;
    }

    public BigDecimal getTotalCommission() {
        return totalCommission;
    }

    public void setTotalCommission(BigDecimal totalCommission) {
        this.totalCommission = totalCommission;
    }

    public BigDecimal getTotalFixedFee() {
        return totalFixedFee;
    }

    public void setTotalFixedFee(BigDecimal totalFixedFee) {
        this.totalFixedFee = totalFixedFee;
    }

    public BigDecimal getTotalShippingCharge() {
        return totalShippingCharge;
    }

    public void setTotalShippingCharge(BigDecimal totalShippingCharge) {
        this.totalShippingCharge = totalShippingCharge;
    }

    public BigDecimal getTotalShippingFee() {
        return totalShippingFee;
    }

    public void setTotalShippingFee(BigDecimal totalShippingFee) {
        this.totalShippingFee = totalShippingFee;
    }

    public BigDecimal getTotalReverseShippingFee() {
        return totalReverseShippingFee;
    }

    public void setTotalReverseShippingFee(BigDecimal totalReverseShippingFee) {
        this.totalReverseShippingFee = totalReverseShippingFee;
    }

    public BigDecimal getTotalAdditionalFee() {
        return totalAdditionalFee;
    }

    public void setTotalAdditionalFee(BigDecimal totalAdditionalFee) {
        this.totalAdditionalFee = totalAdditionalFee;
    }

    public BigDecimal getTotalChannelPenalty() {
        return totalChannelPenalty;
    }

    public void setTotalChannelPenalty(BigDecimal totalChannelPenalty) {
        this.totalChannelPenalty = totalChannelPenalty;
    }

    public double getTotalChannelRecovery() {
        return totalChannelRecovery;
    }

    public BigDecimal getTotalTax() {
        return totalTax;
    }

    public void setTotalTax(BigDecimal totalTax) {
        this.totalTax = totalTax;
    }

    public String getAdditionalFeeDescription() {
        return additionalFeeDescription;
    }

    public void setAdditionalFeeDescription(String additionalFeeDescription) {
        this.additionalFeeDescription = additionalFeeDescription;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }
}
