/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 10-Oct-2014
 *  @author parijat
 */
package com.uniware.core.vo;

import java.util.Date;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author parijat
 */
@Document(collection = "channelReconciliationInvoice")
// @CompoundIndexes({ @CompoundIndex(def = "{tenantCode:1,channelCode:1,invoiceCode:1}", unique = true) })
public class ChannelReconciliationInvoice {

    public enum StatusCode {
        SUCCESS,
        FAILED
    }

    private String tenantCode;
    private String channelCode;
    private String invoiceCode;
    private String settlementDate;
    private Date   depositDate;
    private double totalSettlementValue = 0;
    private String downloadUrl;
    private int    failedCount;
    private int    successCount;
    private int    processedCount;
    private int    unprocessedCount;
    private String failedReason;
    private Date   created;
    private Date   updated;

    public String getTenantCode() {
        return tenantCode;
    }

    public void setTenantCode(String tenantCode) {
        this.tenantCode = tenantCode;
    }

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    public String getInvoiceCode() {
        return invoiceCode;
    }

    public void setInvoiceCode(String invoiceCode) {
        this.invoiceCode = invoiceCode;
    }

    public String getSettlementDate() {
        return settlementDate;
    }

    public void setSettlementDate(String settlementDate) {
        this.settlementDate = settlementDate;
    }

    public Date getDepositDate() {
        return depositDate;
    }

    public void setDepositDate(Date depositDate) {
        this.depositDate = depositDate;
    }

    public double getTotalSettlementValue() {
        return totalSettlementValue;
    }

    public void setTotalSettlementValue(double totalSettlementValue) {
        this.totalSettlementValue = totalSettlementValue;
    }

    public String getDownloadUrl() {
        return downloadUrl;
    }

    public void setDownloadUrl(String downloadUrl) {
        this.downloadUrl = downloadUrl;
    }

    public int getFailedCount() {
        return failedCount;
    }

    public void setFailedCount(int failedCount) {
        this.failedCount = failedCount;
    }

    public int getSuccessCount() {
        return successCount;
    }

    public void setSuccessCount(int successCount) {
        this.successCount = successCount;
    }

    public int getProcessedCount() {
        return processedCount;
    }

    public void setProcessedCount(int processedCount) {
        this.processedCount = processedCount;
    }

    public int getUnprocessedCount() {
        return unprocessedCount;
    }

    public void setUnprocessedCount(int unprocessedCount) {
        this.unprocessedCount = unprocessedCount;
    }

    public String getFailedReason() {
        return failedReason;
    }

    public void setFailedReason(String failedReason) {
        this.failedReason = failedReason;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    @Override
    public String toString() {
        return "ChannelReconciliationInvoiceVO{" + "updated=" + updated + ", created=" + created + ", failedReason='" + failedReason + '\'' + ", unprocessedCount="
                + unprocessedCount + ", processedCount=" + processedCount + ", successCount=" + successCount + ", failedCount=" + failedCount + ", totalSettlementValue="
                + totalSettlementValue + ", depositDate=" + depositDate + ", settlementDate='" + settlementDate + '\'' + ", invoiceCode='" + invoiceCode + '\'' + ", channelCode='"
                + channelCode + '\'' + ", tenantCode='" + tenantCode + '\'' + '}';
    }
}
