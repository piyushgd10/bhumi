/*
 * Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 10/23/15 11:57 AM
 * @author harshpal
 */

package com.uniware.core.vo;

import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * Created by harshpal on 10/23/15.
 */

@Document(collection = "dashboardWidget")
public class DashboardWidgetVO {

    private String tenantCode;

    private String cacheWidgetIdentifier;

    private String jsonData;

    private Date expireAt;

    public String getTenantCode() {
        return tenantCode;
    }

    public void setTenantCode(String tenantCode) {
        this.tenantCode = tenantCode;
    }

    public String getCacheWidgetIdentifier() {
        return cacheWidgetIdentifier;
    }

    public void setCacheWidgetIdentifier(String cacheWidgetIdentifier) {
        this.cacheWidgetIdentifier = cacheWidgetIdentifier;
    }

    public String getJsonData() {
        return jsonData;
    }

    public void setJsonData(String jsonData) {
        this.jsonData = jsonData;
    }

    public Date getExpireAt() {
        return expireAt;
    }

    public void setExpireAt(Date expireAt) {
        this.expireAt = expireAt;
    }
}
