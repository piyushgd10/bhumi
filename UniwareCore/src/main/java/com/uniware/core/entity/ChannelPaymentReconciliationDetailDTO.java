/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 15-Oct-2014
 *  @author parijat
 */
package com.uniware.core.entity;

import com.unifier.core.entity.SaleOrderReconciliation;
import java.util.Date;

/**
 * @author parijat
 */
public class ChannelPaymentReconciliationDetailDTO {

    private String  channelCode;
    private String  channelSaleOrderCode;
    private String  description;
    private String  statusCode;
    private String  reconciliationIdentifier;
    private String  currencyCode;
    private double  totalReconcileAmount = 0;
    private Integer quantity;
    private Date    lastSettledDate;
    private Date    orderTransactionDate;
    private Date    created;
    private Date    updated;

    public ChannelPaymentReconciliationDetailDTO(SaleOrderReconciliation paymentReconciliation) {
        this.channelCode = paymentReconciliation.getChannelCode();
        this.channelSaleOrderCode = paymentReconciliation.getChannelSaleOrderCode();
        this.statusCode = paymentReconciliation.getStatusCode();
        this.reconciliationIdentifier = paymentReconciliation.getReconciliationIdentifier();
        this.currencyCode = paymentReconciliation.getCurrencyCode();
        this.totalReconcileAmount = paymentReconciliation.getTotalReconcileAmount();
        this.lastSettledDate = paymentReconciliation.getLastSettledDate();
        this.orderTransactionDate = paymentReconciliation.getOrderTransactionDate();
        this.created = paymentReconciliation.getCreated();
        this.updated = paymentReconciliation.getUpdated();
    }

    /**
     * @return the channelCode
     */
    public String getChannelCode() {
        return channelCode;
    }

    /**
     * @param channelCode the channelCode to set
     */
    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    public String getChannelSaleOrderCode() {
        return channelSaleOrderCode;
    }

    public void setChannelSaleOrderCode(String channelSaleOrderCode) {
        this.channelSaleOrderCode = channelSaleOrderCode;
    }

    /**
     * @return the saleOrderCode
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the statusCode
     */
    public String getStatusCode() {
        return statusCode;
    }

    /**
     * @param statusCode the statusCode to set
     */
    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    /**
     * @return the reconciliationIdentifier
     */
    public String getReconciliationIdentifier() {
        return reconciliationIdentifier;
    }

    /**
     * @param reconciliationIdentifier the reconciliationIdentifier to set
     */
    public void setReconciliationIdentifier(String reconciliationIdentifier) {
        this.reconciliationIdentifier = reconciliationIdentifier;
    }

    /**
     * @return the currencyCode
     */
    public String getCurrencyCode() {
        return currencyCode;
    }

    /**
     * @param currencyCode the currencyCode to set
     */
    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    /**
     * @return the totalReconcileAmount
     */
    public double getTotalReconcileAmount() {
        return totalReconcileAmount;
    }

    public void setTotalReconcileAmount(double totalReconcileAmount) {
        this.totalReconcileAmount = totalReconcileAmount;
    }

    /**
     * @return the quantity
     */
    public Integer getQuantity() {
        return quantity;
    }

    /**
     * @param quantity the quantity to set
     */
    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    /**
     * @return the lastSettledDate
     */
    public Date getLastSettledDate() {
        return lastSettledDate;
    }

    /**
     * @param lastSettledDate the lastSettledDate to set
     */
    public void setLastSettledDate(Date lastSettledDate) {
        this.lastSettledDate = lastSettledDate;
    }

    /**
     * @return the orderTransactionDate
     */
    public Date getOrderTransactionDate() {
        return orderTransactionDate;
    }

    /**
     * @param orderTransactionDate the orderTransactionDate to set
     */
    public void setOrderTransactionDate(Date orderTransactionDate) {
        this.orderTransactionDate = orderTransactionDate;
    }

    /**
     * @return the created
     */
    public Date getCreated() {
        return created;
    }

    /**
     * @param created the created to set
     */
    public void setCreated(Date created) {
        this.created = created;
    }

    /**
     * @return the updated
     */
    public Date getUpdated() {
        return updated;
    }

    /**
     * @param updated the updated to set
     */
    public void setUpdated(Date updated) {
        this.updated = updated;
    }

}
