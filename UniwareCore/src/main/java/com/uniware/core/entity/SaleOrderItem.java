package com.uniware.core.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.DynamicUpdate;

import com.unifier.core.annotation.Customizable;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.StringUtils;
import com.uniware.core.cache.FacilityCache;

/**
 * SaleOrderItem generated by hbm2java
 */
@Entity
@DynamicUpdate
@Table(name = "sale_order_item")
@Customizable(displayName = "Sale Order Item")
public class SaleOrderItem implements java.io.Serializable {

    public enum PackageType {
        FIXED,
        FLEXIBLE
    }

    public enum StatusCode {
        CANCELLED,
        CREATED,
        FULFILLABLE,
        UNFULFILLABLE,
        DISPATCHED,
        DELIVERED,
        REPLACED,
        RESHIPPED,
        ALTERNATE_SUGGESTED,
        ALTERNATE_ACCEPTED,
        LOCATION_NOT_SERVICEABLE,
        MANIFESTED,
        STAGED,
        PICKING_FOR_STAGING,
        PICKING_FOR_INVOICING
    }

    public enum ItemDetailingStatus {
        NOT_REQUIRED,
        PENDING,
        COMPLETE
    }

    /**
     *
     */
    private static final long      serialVersionUID        = 4228187375149781349L;
    private Integer                id;
    private AddressDetail          shippingAddress;
    private Item                   item;
    private SaleOrder              saleOrder;
    private ShippingMethod         shippingMethod;
    private ItemTypeInventory      itemTypeInventory;
    private ShippingPackage        shippingPackage;
    private ItemType               itemType;
    private ShippingProvider       shippingProvider;
    private String                 trackingNumber;
    private String                 statusCode;
    private Facility               facility;
    private Section                section;
    private String                 code;
    private BigDecimal             totalPrice;
    private BigDecimal             sellingPrice;
    private BigDecimal             discount                = BigDecimal.ZERO;
    private BigDecimal             prepaidAmount           = BigDecimal.ZERO;

    private BigDecimal             shippingCharges         = BigDecimal.ZERO;
    private BigDecimal             shippingMethodCharges   = BigDecimal.ZERO;
    private BigDecimal             cashOnDeliveryCharges   = BigDecimal.ZERO;
    private String                 giftMessage;
    private BigDecimal             giftWrapCharges         = BigDecimal.ZERO;
    private boolean                giftWrap;
    private String                 voucherCode;
    private BigDecimal             voucherValue            = BigDecimal.ZERO;
    private BigDecimal             storeCredit             = BigDecimal.ZERO;
    private BigDecimal             dropshipShippingCharges = BigDecimal.ZERO;
    private BigDecimal             dropshipCommission      = BigDecimal.ZERO;
    private BigDecimal             transferPrice;
    private BigDecimal             costPrice;
    private BigDecimal             channelTransferPrice;
    private BigDecimal             commissionPercentage;
    private BigDecimal             paymentGatewayCharge;
    private BigDecimal             logisticsCost;
    private int                    packetNumber;
    private String                 reversePickupReason;
    private ReversePickup          reversePickup;
    private SaleOrderItemAlternate saleOrderItemAlternate;
    private SaleOrderItem          alteredSaleOrderItem;
    private String                 combinationIdentifier;
    private String                 combinationDescription;
    private String                 bundleSkuCode;
    private boolean                requiresCustomization;
    private String                 cancellationReason;
    private String                 itemDetailFields;
    private String                 itemDetails;
    private ItemDetailingStatus    itemDetailingStatus;
    private String                 channelProductId;
    private String                 sellerSkuCode;
    private String                 channelSaleOrderItemCode;
    private String                 channelProductName;
    private Date                   created;
    private Date                   updated;
    private boolean                onHold;
    private InvoiceItem            invoiceItem;
    private InvoiceItem            returnInvoiceItem;
    private Date                   cancellationTime;

    public SaleOrderItem() {
    }

    public SaleOrderItem(AddressDetail shippingAddress, SaleOrder saleOrder, ShippingMethod shippingMethod, ItemType itemType, String statusCode, String code,
            BigDecimal totalPrice, BigDecimal sellingPrice, BigDecimal discount, BigDecimal shippingCharges, BigDecimal shippingMethodCharges, BigDecimal cashOnDeliveryCharges,
            BigDecimal giftWrapCharges, boolean giftWrap, int packetNumber, Date created, Date updated) {
        this.shippingAddress = shippingAddress;
        this.saleOrder = saleOrder;
        this.shippingMethod = shippingMethod;
        this.itemType = itemType;
        this.statusCode = statusCode;
        this.code = code;
        this.totalPrice = totalPrice;
        this.discount = discount;
        this.sellingPrice = sellingPrice;
        this.shippingCharges = shippingCharges;
        this.shippingMethodCharges = shippingMethodCharges;
        this.cashOnDeliveryCharges = cashOnDeliveryCharges;
        this.giftWrapCharges = giftWrapCharges;
        this.giftWrap = giftWrap;
        this.packetNumber = packetNumber;
        this.created = created;
        this.updated = updated;
    }

    public SaleOrderItem(AddressDetail shippingAddress, Item item, SaleOrder saleOrder, ShippingMethod shippingMethod, ItemTypeInventory itemTypeInventory,
            ShippingPackage shippingPackage, ItemType itemType, String statusCode, String code, BigDecimal totalPrice, BigDecimal sellingPrice, BigDecimal discount,
            BigDecimal shippingCharges, BigDecimal shippingMethodCharges, BigDecimal cashOnDeliveryCharges, String giftMessage, BigDecimal giftWrapCharges, boolean giftWrap,
            int packetNumber, Date created, Date updated) {
        this.shippingAddress = shippingAddress;
        this.item = item;
        this.saleOrder = saleOrder;
        this.shippingMethod = shippingMethod;
        this.itemTypeInventory = itemTypeInventory;
        this.shippingPackage = shippingPackage;
        this.itemType = itemType;
        this.statusCode = statusCode;
        this.code = code;
        this.totalPrice = totalPrice;
        this.sellingPrice = sellingPrice;
        this.discount = discount;
        this.shippingCharges = shippingCharges;
        this.shippingMethodCharges = shippingMethodCharges;
        this.cashOnDeliveryCharges = cashOnDeliveryCharges;
        this.giftMessage = giftMessage;
        this.giftWrapCharges = giftWrapCharges;
        this.giftWrap = giftWrap;
        this.packetNumber = packetNumber;
        this.created = created;
        this.updated = updated;
    }

    public SaleOrderItem(SaleOrderItem saleOrderItem) {
        setCashOnDeliveryCharges(saleOrderItem.getCashOnDeliveryCharges());
        setCode(saleOrderItem.getCode());
        setGiftMessage(saleOrderItem.getGiftMessage());
        setGiftWrap(saleOrderItem.isGiftWrap());
        setGiftWrapCharges(saleOrderItem.getGiftWrapCharges());
        setItemType(saleOrderItem.getItemType());
        setChannelProductId(saleOrderItem.getChannelProductId());
        setSellerSkuCode(saleOrderItem.getSellerSkuCode());
        setChannelSaleOrderItemCode(saleOrderItem.getChannelSaleOrderItemCode());
        setChannelProductName(saleOrderItem.getChannelProductName());
        setTransferPrice(saleOrderItem.getTransferPrice());
        setCostPrice(saleOrderItem.getCostPrice());
        setPacketNumber(saleOrderItem.getPacketNumber());
        setSellingPrice(saleOrderItem.getSellingPrice());
        setShippingAddress(saleOrderItem.getShippingAddress());
        setShippingCharges(saleOrderItem.getShippingCharges());
        setShippingMethod(saleOrderItem.getShippingMethod());
        setShippingMethodCharges(saleOrderItem.getShippingMethodCharges());
        setTotalPrice(saleOrderItem.getTotalPrice());
        setPrepaidAmount(saleOrderItem.getPrepaidAmount());
        setDiscount(saleOrderItem.getDiscount());
        setVoucherCode(saleOrderItem.getVoucherCode());
        setVoucherValue(saleOrderItem.getVoucherValue());
        setStoreCredit(saleOrderItem.getStoreCredit());
        setCombinationIdentifier(saleOrderItem.getCombinationIdentifier());
        setCombinationDescription(saleOrderItem.getCombinationDescription());
        setBundleSkuCode(saleOrderItem.getBundleSkuCode());
        setItemDetailFields(saleOrderItem.getItemDetailFields());
        setItemDetailingStatus(saleOrderItem.getItemDetailingStatus() == ItemDetailingStatus.NOT_REQUIRED ? ItemDetailingStatus.NOT_REQUIRED : ItemDetailingStatus.PENDING);
        setStatusCode(SaleOrderItem.StatusCode.CREATED.name());
        setFacility(saleOrderItem.getFacility());
        setOnHold(saleOrderItem.isOnHold());
        setCreated(DateUtils.getCurrentTime());
        setUpdated(DateUtils.getCurrentTime());
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "cancellation_time", nullable = true, length = 19)
    public Date getCancellationTime() {
        return cancellationTime;
    }

    public void setCancellationTime(Date cancellationTime) {
        this.cancellationTime = cancellationTime;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "shipping_address_id", nullable = false)
    public AddressDetail getShippingAddress() {
        return this.shippingAddress;
    }

    public void setShippingAddress(AddressDetail shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "item_id")
    public Item getItem() {
        return this.item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "sale_order_id", nullable = false)
    public SaleOrder getSaleOrder() {
        return this.saleOrder;
    }

    public void setSaleOrder(SaleOrder saleOrder) {
        this.saleOrder = saleOrder;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "item_type_inventory_id")
    public ItemTypeInventory getItemTypeInventory() {
        return this.itemTypeInventory;
    }

    public void setItemTypeInventory(ItemTypeInventory itemTypeInventory) {
        this.itemTypeInventory = itemTypeInventory;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "shipping_provider_id")
    public ShippingProvider getShippingProvider() {
        return this.shippingProvider;
    }

    public void setShippingProvider(ShippingProvider shippingProvider) {
        this.shippingProvider = shippingProvider;
    }

    @Column(name = "tracking_number", length = 45)
    public String getTrackingNumber() {
        return this.trackingNumber;
    }

    public void setTrackingNumber(String trackingNumber) {
        this.trackingNumber = trackingNumber;
    }

    @Column(name = "transfer_price", precision = 12)
    public BigDecimal getTransferPrice() {
        return this.transferPrice;
    }

    public void setTransferPrice(BigDecimal transferPrice) {
        this.transferPrice = transferPrice;
    }
    
    @Column(name = "cost_price", precision = 12)
    public BigDecimal getCostPrice() {
        return this.costPrice;
    }

    public void setCostPrice(BigDecimal costPrice) {
        this.costPrice = costPrice;
    }

    @Column(name = "channel_transfer_price", precision = 12)
    public BigDecimal getChannelTransferPrice() {
        return channelTransferPrice;
    }

    public void setChannelTransferPrice(BigDecimal channelTransferPrice) {
        this.channelTransferPrice = channelTransferPrice;
    }

    @Column(name = "commission_percentage")
    public BigDecimal getCommissionPercentage() {
        return commissionPercentage;
    }

    public void setCommissionPercentage(BigDecimal commissionPercentage) {
        this.commissionPercentage = commissionPercentage;
    }

    @Column(name = "payment_gateway_charge")
    public BigDecimal getPaymentGatewayCharge() {
        return paymentGatewayCharge;
    }

    public void setPaymentGatewayCharge(BigDecimal paymentGatewayCharge) {
        this.paymentGatewayCharge = paymentGatewayCharge;
    }

    @Column(name = "logistics_cost")
    public BigDecimal getLogisticsCost() {
        return logisticsCost;
    }

    public void setLogisticsCost(BigDecimal logisticsCost) {
        this.logisticsCost = logisticsCost;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "shipping_method_id", nullable = false)
    public ShippingMethod getShippingMethod() {
        return this.shippingMethod;
    }

    public void setShippingMethod(ShippingMethod shippingMethod) {
        this.shippingMethod = shippingMethod;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "shipping_package_id")
    public ShippingPackage getShippingPackage() {
        return this.shippingPackage;
    }

    public void setShippingPackage(ShippingPackage shippingPackage) {
        this.shippingPackage = shippingPackage;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "item_type_id", nullable = false)
    public ItemType getItemType() {
        return this.itemType;
    }

    public void setItemType(ItemType itemType) {
        this.itemType = itemType;
    }

    @Column(name = "status_code", nullable = false, length = 45)
    public String getStatusCode() {
        return this.statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "facility_id")
    public Facility getFacility() {
        return this.facility;
    }

    public void setFacility(Facility facility) {
        this.facility = facility;
    }

    @Column(name = "code", nullable = false, length = 45)
    public String getCode() {
        return this.code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Column(name = "total_price", nullable = false, precision = 12)
    public BigDecimal getTotalPrice() {
        return this.totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    @Column(name = "selling_price", nullable = false, precision = 12)
    public BigDecimal getSellingPrice() {
        return this.sellingPrice;
    }

    public void setSellingPrice(BigDecimal sellingPrice) {
        this.sellingPrice = sellingPrice;
    }

    @Column(name = "discount", nullable = false, precision = 12)
    public BigDecimal getDiscount() {
        return this.discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    @Column(name = "prepaid_amount", nullable = false, precision = 12)
    public BigDecimal getPrepaidAmount() {
        return this.prepaidAmount;
    }

    public void setPrepaidAmount(BigDecimal prepaidAmount) {
        this.prepaidAmount = prepaidAmount;
    }

    @Column(name = "shipping_charges", nullable = false, precision = 12)
    public BigDecimal getShippingCharges() {
        return this.shippingCharges;
    }

    public void setShippingCharges(BigDecimal shippingCharges) {
        this.shippingCharges = shippingCharges;
    }

    @Column(name = "shipping_method_charges", nullable = false, precision = 12)
    public BigDecimal getShippingMethodCharges() {
        return this.shippingMethodCharges;
    }

    public void setShippingMethodCharges(BigDecimal shippingMethodCharges) {
        this.shippingMethodCharges = shippingMethodCharges;
    }

    @Column(name = "cash_on_delivery_charges", nullable = false, precision = 12)
    public BigDecimal getCashOnDeliveryCharges() {
        return this.cashOnDeliveryCharges;
    }

    public void setCashOnDeliveryCharges(BigDecimal cashOnDeliveryCharges) {
        this.cashOnDeliveryCharges = cashOnDeliveryCharges;
    }

    @Column(name = "gift_message", length = 256)
    public String getGiftMessage() {
        return this.giftMessage;
    }

    public void setGiftMessage(String giftMessage) {
        this.giftMessage = giftMessage;
    }

    @Column(name = "gift_wrap_charges", nullable = false, precision = 12)
    public BigDecimal getGiftWrapCharges() {
        return this.giftWrapCharges;
    }

    public void setGiftWrapCharges(BigDecimal giftWrapCharges) {
        this.giftWrapCharges = giftWrapCharges;
    }

    @Column(name = "gift_wrap", nullable = false)
    public boolean isGiftWrap() {
        return this.giftWrap;
    }

    public void setGiftWrap(boolean giftWrap) {
        this.giftWrap = giftWrap;
    }

    @Column(name = "voucher_code", length = 45)
    public String getVoucherCode() {
        return this.voucherCode;
    }

    public void setVoucherCode(String voucherCode) {
        this.voucherCode = voucherCode;
    }

    @Column(name = "voucher_value", nullable = false, precision = 12)
    public BigDecimal getVoucherValue() {
        return this.voucherValue;
    }

    public void setVoucherValue(BigDecimal voucherValue) {
        this.voucherValue = voucherValue;
    }

    @Column(name = "store_credit", nullable = false, precision = 12)
    public BigDecimal getStoreCredit() {
        return this.storeCredit;
    }

    public void setStoreCredit(BigDecimal storeCredit) {
        this.storeCredit = storeCredit;
    }

    @Column(name = "dropship_shipping_charges", nullable = false, precision = 12)
    public BigDecimal getDropshipShippingCharges() {
        return this.dropshipShippingCharges;
    }

    public void setDropshipShippingCharges(BigDecimal dropshipShippingCharges) {
        this.dropshipShippingCharges = dropshipShippingCharges;
    }

    @Column(name = "dropship_commission", nullable = false, precision = 12)
    public BigDecimal getDropshipCommission() {
        return this.dropshipCommission;
    }

    public void setDropshipCommission(BigDecimal dropshipCommission) {
        this.dropshipCommission = dropshipCommission;
    }

    @Column(name = "packet_number", nullable = false)
    public int getPacketNumber() {
        return this.packetNumber;
    }

    public void setPacketNumber(int packetNumber) {
        this.packetNumber = packetNumber;
    }

    @Column(name = "reverse_pickup_reason", length = 500)
    public String getReversePickupReason() {
        return reversePickupReason;
    }

    public void setReversePickupReason(String reversePickupReason) {
        this.reversePickupReason = reversePickupReason;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "reverse_pickup_id")
    public ReversePickup getReversePickup() {
        return this.reversePickup;
    }

    public void setReversePickup(ReversePickup reversePickup) {
        this.reversePickup = reversePickup;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "sale_order_item_alternate_id")
    public SaleOrderItemAlternate getSaleOrderItemAlternate() {
        return this.saleOrderItemAlternate;
    }

    public void setSaleOrderItemAlternate(SaleOrderItemAlternate saleOrderItemAlternate) {
        this.saleOrderItemAlternate = saleOrderItemAlternate;
    }

    @Column(name = "requires_customization", nullable = false)
    public boolean isRequiresCustomization() {
        return this.requiresCustomization;
    }

    public void setRequiresCustomization(boolean requiresCustomization) {
        this.requiresCustomization = requiresCustomization;
    }

    @Column(name = "item_detail_fields", length = 100)
    public String getItemDetailFields() {
        return this.itemDetailFields;
    }

    public void setItemDetailFields(String itemDetailFields) {
        this.itemDetailFields = itemDetailFields;
    }

    @Column(name = "item_details", length = 256)
    public String getItemDetails() {
        return this.itemDetails;
    }

    public void setItemDetails(String itemDetails) {
        this.itemDetails = itemDetails;
    }

    @Column(name = "item_detailing_status", nullable = false)
    @Enumerated(EnumType.STRING)
    public ItemDetailingStatus getItemDetailingStatus() {
        return this.itemDetailingStatus;
    }

    public void setItemDetailingStatus(ItemDetailingStatus itemDetailingStatus) {
        this.itemDetailingStatus = itemDetailingStatus;
    }

    @Column(name = "channel_product_id", length = 128)
    public String getChannelProductId() {
        return channelProductId;
    }

    public void setChannelProductId(String channelProductId) {
        this.channelProductId = channelProductId;
    }

    @Column(name = "seller_sku_code", length = 128)
    public String getSellerSkuCode() {
        return sellerSkuCode;
    }

    public void setSellerSkuCode(String sellerSkuCode) {
        this.sellerSkuCode = sellerSkuCode;
    }

    @Column(name = "channel_sale_order_item_code", nullable = false, length = 45)
    public String getChannelSaleOrderItemCode() {
        return channelSaleOrderItemCode;
    }

    public void setChannelSaleOrderItemCode(String channelSaleOrderItemCode) {
        this.channelSaleOrderItemCode = channelSaleOrderItemCode;
    }

    @Column(name = "channel_product_name", nullable = false, length = 256)
    public String getChannelProductName() {
        return channelProductName;
    }

    public void setChannelProductName(String channelProductName) {
        this.channelProductName = channelProductName;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created", nullable = false, length = 19)
    public Date getCreated() {
        return this.created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated", nullable = false, length = 19, insertable = false, updatable = false)
    public Date getUpdated() {
        return this.updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    @Transient
    public boolean isCancellable() {
        return StringUtils.equalsAny(getStatusCode(), StatusCode.CREATED.name(), StatusCode.UNFULFILLABLE.name(), StatusCode.ALTERNATE_SUGGESTED.name(),
                StatusCode.FULFILLABLE.name(), StatusCode.LOCATION_NOT_SERVICEABLE.name(), StatusCode.PICKING_FOR_INVOICING.name(), StatusCode.PICKING_FOR_STAGING.name(), StatusCode.STAGED.name());
    }

    @Transient
    public boolean isReversePickable() {
        return getReversePickup() == null
                && getShippingPackage() != null
                && StringUtils.equalsAny(getStatusCode(), StatusCode.DELIVERED.name(), StatusCode.DISPATCHED.name())
                && StringUtils.equalsAny(getShippingPackage().getStatusCode(), ShippingPackage.StatusCode.DISPATCHED.name(), ShippingPackage.StatusCode.SHIPPED.name(),
                        ShippingPackage.StatusCode.DELIVERED.name());
    }

    @Transient
    public boolean isPacketConfigurable() {
        return this.shippingPackage == null && this.packetNumber != 0;
    }

    @Transient
    public boolean isInventoryReleasable() {
        return SaleOrderItem.StatusCode.FULFILLABLE.name().equals(this.getStatusCode())
                && (this.getShippingPackage() == null || ShippingPackage.StatusCode.CREATED.name().equals(this.getShippingPackage().getStatusCode()) && !this.onHold);
    }

    @Transient
    public boolean isPackageModifiable() {
        return this.isPacketConfigurable() && !SaleOrderItem.StatusCode.CANCELLED.name().equals(this.getStatusCode());
    }

    @Transient
    public boolean isPackageCreatable() {
        return SaleOrderItem.StatusCode.FULFILLABLE.name().equals(this.getStatusCode()) && this.getShippingPackage() == null && !this.onHold;
    }

    @Transient
    public boolean isFacilitySwitchable() {
        Facility currentFacility = CacheManager.getInstance().getCache(FacilityCache.class).getCurrentFacility();
        String currentFacilityCode = currentFacility != null ? currentFacility.getCode() : null;
        return this.getFacility() == null
                || (this.getFacility().getCode().equals(currentFacilityCode) && (this.getShippingPackage() == null || ShippingPackage.StatusCode.CREATED.name().equals(
                        this.getShippingPackage().getStatusCode())));
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "SaleOrderItem [id=" + id + ", shippingAddress=" + shippingAddress + ", item=" + item + ", shippingMethod=" + shippingMethod + ", itemType=" + itemType.getId()
                + ", statusCode=" + statusCode + ", code=" + code + ", totalPrice=" + totalPrice + ", sellingPrice=" + sellingPrice + ", shippingCharges=" + shippingCharges
                + ", shippingMethodCharges=" + shippingMethodCharges + ", cashOnDeliveryCharges=" + cashOnDeliveryCharges + ", giftMessage=" + giftMessage + ", giftWrapCharges="
                + giftWrapCharges + ", giftWrap=" + giftWrap + ", packetNumber=" + packetNumber + ", created=" + created + ", updated=" + updated + "]";
    }

    @Column(name = "combination_identifier", length = 45)
    public String getCombinationIdentifier() {
        return this.combinationIdentifier;
    }

    public void setCombinationIdentifier(String combinationIdentifier) {
        this.combinationIdentifier = combinationIdentifier;
    }

    @Column(name = "combination_description", length = 200)
    public String getCombinationDescription() {
        return this.combinationDescription;
    }

    public void setCombinationDescription(String combinationDescription) {
        this.combinationDescription = combinationDescription;
    }

    @Column(name = "bundle_sku_code", length = 45)
    public String getBundleSkuCode() {
        return bundleSkuCode;
    }

    public void setBundleSkuCode(String bundleSkuCode) {
        this.bundleSkuCode = bundleSkuCode;
    }

    @Column(name = "on_hold", nullable = false)
    public boolean isOnHold() {
        return this.onHold;
    }

    public void setOnHold(boolean onHold) {
        this.onHold = onHold;
    }

    /**
     * @return the alteredSaleOrderItem
     */
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "altered_sale_order_item_id")
    public SaleOrderItem getAlteredSaleOrderItem() {
        return alteredSaleOrderItem;
    }

    /**
     * @param alteredSaleOrderItem the alteredSaleOrderItem to set
     */
    public void setAlteredSaleOrderItem(SaleOrderItem alteredSaleOrderItem) {
        this.alteredSaleOrderItem = alteredSaleOrderItem;
    }

    @Column(name = "cancellation_reason", length = 100)
    public String getCancellationReason() {
        return cancellationReason;
    }

    public void setCancellationReason(String cancellationReason) {
        this.cancellationReason = cancellationReason;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "invoice_item_id")
    public InvoiceItem getInvoiceItem() {
        return invoiceItem;
    }

    public void setInvoiceItem(InvoiceItem invoiceItem) {
        this.invoiceItem = invoiceItem;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "return_invoice_item_id")
    public InvoiceItem getReturnInvoiceItem() {
        return returnInvoiceItem;
    }

    public void setReturnInvoiceItem(InvoiceItem returnInvoiceItem) {
        this.returnInvoiceItem = returnInvoiceItem;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "section_id")
    public Section getSection() {
        return section;
    }

    public void setSection(Section section) {
        this.section = section;
    }

    @Transient
    public String getChannelSkuCode() {
        return channelProductId;
    }
}
