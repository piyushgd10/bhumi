package com.uniware.core.entity;

import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.Date;

@Document(collection = "regulatoryForm")
public class RegulatoryForm implements Serializable {

    public enum GOODS_MOVEMENT_TYPE {
        INBOUND,
        OUTBOUND
    }

    private String  id;
    private String  countryCode;
    private String  stateCode;
    @Indexed(unique = true)
    private String  code;
    private String  name;
    private String  goodsMovementType;
    private String  conditionalExpression;
    private boolean enabled;
    private Date    created;
    private Date    updated;

    public RegulatoryForm() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getStateCode() {
        return stateCode;
    }

    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGoodsMovementType() {
        return goodsMovementType;
    }

    public void setGoodsMovementType(String goodsMovementType) {
        this.goodsMovementType = goodsMovementType;
    }

    public String getConditionalExpression() {
        return conditionalExpression;
    }

    public void setConditionalExpression(String conditionalExpression) {
        this.conditionalExpression = conditionalExpression;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }
}
