package com.uniware.core.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.unifier.core.entity.User;

/**
 * Created by harshpal on 2/24/16.
 */
@Entity
@Table(name = "cycle_count_shelf_summary")
public class CycleCountShelfSummary implements Serializable {

    private static final long serialVersionUID = -1825650523355738999L;
    private Integer           id;
    private SubCycleCount     subCycleCount;
    private Shelf             shelf;
    private User              user;
    private boolean           active;
    private Integer           nonBarcodedItemCount;
    private String            itemCodes;
    private Date              created;
    private Date              updated;

    public CycleCountShelfSummary() {
    }

    public CycleCountShelfSummary(SubCycleCount subCycleCount, Shelf shelf, User user, boolean active, Date created, Date updated) {
        this.subCycleCount = subCycleCount;
        this.shelf = shelf;
        this.user = user;
        this.active = active;
        this.created = created;
        this.updated = updated;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "sub_cycle_count_id", nullable = false)
    public SubCycleCount getSubCycleCount() {
        return subCycleCount;
    }

    public void setSubCycleCount(SubCycleCount subCycleCount) {
        this.subCycleCount = subCycleCount;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "shelf_id", nullable = false)
    public Shelf getShelf() {
        return shelf;
    }

    public void setShelf(Shelf shelf) {
        this.shelf = shelf;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false)
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Column(name = "active", nullable = false)
    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Column(name = "non_barcoded_item_count", nullable = false)
    public Integer getNonBarcodedItemCount() {
        return nonBarcodedItemCount;
    }

    public void setNonBarcodedItemCount(Integer nonBarcodedItemCount) {
        this.nonBarcodedItemCount = nonBarcodedItemCount;
    }

    @Column(name = "item_codes", nullable = false)
    public String getItemCodes() {
        return itemCodes;
    }

    @Column(name = "item_Codes")
    public void setItemCodes(String itemCodes) {
        this.itemCodes = itemCodes;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created", nullable = false, length = 19)
    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated", nullable = false, length = 19, insertable = false, updatable = false)
    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    @Override public String toString() {
        return "CycleCountShelfSummary{" + "id=" + id + ", subCycleCount=" + subCycleCount + ", shelf=" + shelf
                + ", active=" + active + '}';
    }
}
