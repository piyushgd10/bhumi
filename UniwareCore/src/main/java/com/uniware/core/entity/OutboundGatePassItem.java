package com.uniware.core.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.uniware.core.entity.ItemTypeInventory.Type;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

/**
 * OutboundGatePassItem generated by hbm2java
 */
@Entity
@Table(name = "outbound_gate_pass_item")
public class OutboundGatePassItem implements java.io.Serializable {
    public enum StatusCode {
        CREATED,
        RETURN_AWAITED,
        CLOSED
    }

    /**
     * 
     */
    private static final long serialVersionUID = -5576898911413029917L;
    private Integer           id;
    private String            statusCode;
    private ItemType          itemType;
    private Integer           quantity;
    private int               receivedQuantity;
    private Item              item;
    private InvoiceItem       invoiceItem;
    private InvoiceItem       returnInvoiceItem;
    private BigDecimal        unitPrice;
    private Type              inventoryType;
    private OutboundGatePass  outboundGatePass;
    private Shelf             shelf;
    private String            reason;
    private Date              created;
    private Date              updated;

    public OutboundGatePassItem() {
    }

    public OutboundGatePassItem(String statusCode, Item item, OutboundGatePass outboundGatePass, Date created, Date updated) {
        this.statusCode = statusCode;
        this.item = item;
        this.outboundGatePass = outboundGatePass;
        this.created = created;
        this.updated = updated;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "status_code", nullable = false, length = 45)
    public String getStatusCode() {
        return this.statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "item_type_id", nullable = false)
    public ItemType getItemType() {
        return this.itemType;
    }

    public void setItemType(ItemType itemType) {
        this.itemType = itemType;
    }

    @Column(name = "quantity", nullable = false)
    public int getQuantity() {
        return this.quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Column(name = "received_quantity", nullable = false)
    public int getReceivedQuantity() {
        return this.receivedQuantity;
    }

    public void setReceivedQuantity(int receivedQuantity) {
        this.receivedQuantity = receivedQuantity;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "item_id")
    @BatchSize(size = 1000)
    public Item getItem() {
        return this.item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "invoice_item_id", unique = true)
    public InvoiceItem getInvoiceItem() {
        return invoiceItem;
    }

    public void setInvoiceItem(InvoiceItem invoiceItem) {
        this.invoiceItem = invoiceItem;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "return_invoice_item_id", unique = true)
    public InvoiceItem getReturnInvoiceItem() {
        return returnInvoiceItem;
    }

    public void setReturnInvoiceItem(InvoiceItem returnInvoiceItem) {
        this.returnInvoiceItem = returnInvoiceItem;
    }

    @Column(name = "unit_price", nullable = false, precision = 12)
    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    /*
     *  Unit Price used for generating gatepass invoice. It should always be tax inclusive as when generating invoice we
     *  are back calculating taxes based on tax type configuration ranges which are assumed to be defined on tax
     *  inclusive prices (BY DEFAULT).
     */
    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }

    @Column(name = "inventory_type", nullable = false)
    @Enumerated(EnumType.STRING)
    public Type getInventoryType() {
        return this.inventoryType;
    }

    public void setInventoryType(Type inventoryType) {
        this.inventoryType = inventoryType;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "outbound_gate_pass_id", nullable = false)
    public OutboundGatePass getOutboundGatePass() {
        return this.outboundGatePass;
    }

    public void setOutboundGatePass(OutboundGatePass outboundGatePass) {
        this.outboundGatePass = outboundGatePass;
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "shelf_id", nullable = false, referencedColumnName = "id")
    public Shelf getShelf() {
        return shelf;
    }

    public void setShelf(Shelf shelf) {
        this.shelf = shelf;
    }

    /**
     * @return the reason
     */
    @Column(name = "reason", nullable = true, length = 100)
    public String getReason() {
        return reason;
    }

    /**
     * @param reason the reason to set
     */
    public void setReason(String reason) {
        this.reason = reason;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created", nullable = false, length = 19)
    public Date getCreated() {
        return this.created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated", nullable = false, length = 19, insertable = false, updatable = false)
    public Date getUpdated() {
        return this.updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

}
