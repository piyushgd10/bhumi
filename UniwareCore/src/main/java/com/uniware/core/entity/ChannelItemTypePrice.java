/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Oct 19, 2015
 *  @author akshay
 */
package com.uniware.core.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "channel_item_type_price")
public class ChannelItemTypePrice implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1806557976351849499L;

    private Integer           id;
    private Tenant            tenant;
    private Channel           channel;
    private String            channelProductId;
    private ChannelItemType   channelItemType;
    private BigDecimal        sellingPrice;
    private BigDecimal        msp;
    private BigDecimal        transferPrice;
    private BigDecimal        mrp;
    private BigDecimal        competitivePrice;
    private String            currencyCode;
    private BigDecimal        channelSellingPrice;
    private BigDecimal        channelMsp;
    private BigDecimal        channelTransferPrice;
    private BigDecimal        chanenlMrp;
    private BigDecimal        channelCompetitivePrice;
    private String            channelCurrencyCode;
    private Date              lastUpdateAttemptAt;
    private String            lastPushStatus;
    private String            lastPushMessage;
    private boolean           dirty;
    private boolean           disabled;
    private boolean           disabledDueToPushErrors;
    private int               pushFailedCount;
    private Date              lastPullAt;
    private Date              created;
    private Date              updated;

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "tenant_id", nullable = false)
    public Tenant getTenant() {
        return this.tenant;
    }

    public void setTenant(Tenant tenant) {
        this.tenant = tenant;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "channel_id", nullable = false)
    public Channel getChannel() {
        return this.channel;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }

    @Column(name = "channel_product_id", nullable = false, length = 128)
    public String getChannelProductId() {
        return channelProductId;
    }

    public void setChannelProductId(String channelProductId) {
        this.channelProductId = channelProductId;
    }


    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "channel_item_type_id")
    public ChannelItemType getChannelItemType() {
        return channelItemType;
    }

    public void setChannelItemType(ChannelItemType channelItemType) {
        this.channelItemType = channelItemType;
    }

    @Column(name = "selling_price",  precision = 12)
    public BigDecimal getSellingPrice() {
        return sellingPrice;
    }

    public void setSellingPrice(BigDecimal sellingPrice) {
        this.sellingPrice = sellingPrice;
    }

    @Column(name = "msp",  precision = 12)
    public BigDecimal getMsp() {
        return msp;
    }

    public void setMsp(BigDecimal msp) {
        this.msp = msp;
    }

    @Column(name = "transfer_price",  precision = 12)
    public BigDecimal getTransferPrice() {
        return transferPrice;
    }

    public void setTransferPrice(BigDecimal transferPrice) {
        this.transferPrice = transferPrice;
    }

    @Column(name = "mrp",  precision = 12)
    public BigDecimal getMrp() {
        return mrp;
    }

    public void setMrp(BigDecimal mrp) {
        this.mrp = mrp;
    }

    @Column(name = "competitive_price",  precision = 12)
    public BigDecimal getCompetitivePrice() {
        return competitivePrice;
    }

    public void setCompetitivePrice(BigDecimal competitivePrice) {
        this.competitivePrice = competitivePrice;
    }
    
    @Column(name = "currency_code")
    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    @Column(name = "channel_selling_price",  precision = 12)
    public BigDecimal getChannelSellingPrice() {
        return channelSellingPrice;
    }

    public void setChannelSellingPrice(BigDecimal channelSellingPrice) {
        this.channelSellingPrice = channelSellingPrice;
    }

    @Column(name = "channel_msp",  precision = 12)
    public BigDecimal getChannelMsp() {
        return channelMsp;
    }

    public void setChannelMsp(BigDecimal channelMsp) {
        this.channelMsp = channelMsp;
    }

    @Column(name = "channel_transfer_price",  precision = 12)
    public BigDecimal getChannelTransferPrice() {
        return channelTransferPrice;
    }

    public void setChannelTransferPrice(BigDecimal channelTransferPrice) {
        this.channelTransferPrice = channelTransferPrice;
    }

    @Column(name = "channel_mrp",  precision = 12)
    public BigDecimal getChannelMrp() {
        return chanenlMrp;
    }

    public void setChannelMrp(BigDecimal chanenlMrp) {
        this.chanenlMrp = chanenlMrp;
    }

    @Column(name = "channel_competitive_price",  precision = 12)
    public BigDecimal getChannelCompetitivePrice() {
        return channelCompetitivePrice;
    }

    public void setChannelCompetitivePrice(BigDecimal channelCompetitivePrice) {
        this.channelCompetitivePrice = channelCompetitivePrice;
    }

    @Column(name = "channel_currency_code")
    public String getChannelCurrencyCode() {
        return channelCurrencyCode;
    }

    public void setChannelCurrencyCode(String channelCurrencyCode) {
        this.channelCurrencyCode = channelCurrencyCode;
    }

    @Column(name = "dirty", nullable = false)
    public boolean isDirty() {
        return dirty;
    }

    public void setDirty(boolean dirty) {
        this.dirty = dirty;
    }

    @Column(name = "disabled", nullable = false)
    public boolean isDisabled() {
        return disabled;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }

    @Column(name = "disabled_due_to_push_errors", nullable = false)
    public boolean isDisabledDueToPushErrors() {
        return disabledDueToPushErrors;
    }

    public void setDisabledDueToPushErrors(boolean disabledDueToPushErrors) {
        this.disabledDueToPushErrors = disabledDueToPushErrors;
    }

    @Column(name = "last_update_attempt_at")
    public Date getLastUpdateAttemptAt() {
        return lastUpdateAttemptAt;
    }

    public void setLastUpdateAttemptAt(Date lastUpdateAttemptAt) {
        this.lastUpdateAttemptAt = lastUpdateAttemptAt;
    }
    
    @Column(name = "last_push_status")
    public String getLastPushStatus() {
        return lastPushStatus;
    }

    public void setLastPushStatus(String lastPushStatus) {
        this.lastPushStatus = lastPushStatus;
    }
    
    @Column(name = "last_push_message")
    public String getLastPushMessage() {
        return lastPushMessage;
    }

    public void setLastPushMessage(String lastPushMessage) {
        this.lastPushMessage = lastPushMessage;
    }

    @Column(name = "push_failed_count")
    public int getPushFailedCount() {
        return pushFailedCount;
    }

    public void setPushFailedCount(int pushFailedCount) {
        this.pushFailedCount = pushFailedCount;
    }

    @Column(name = "last_pull_at")
    public Date getLastPullAt() {
        return lastPullAt;
    }

    public void setLastPullAt(Date lastPullAt) {
        this.lastPullAt = lastPullAt;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created", nullable = false, length = 19)
    public Date getCreated() {
        return this.created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated", nullable = false, length = 19, insertable = false, updatable = false)
    public Date getUpdated() {
        return this.updated;
    }
    
    public void setUpdated(Date updated) {
        this.updated = updated;
    }
}
