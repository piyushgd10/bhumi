package com.uniware.core.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class SourceConnector implements Serializable {

    private String  sourceCode;
    private String  name;
    private String  displayName;
    private String  helpText;
    private int     priority;
    private boolean thirdParty;
    private boolean requiredInOrderSync;
    private boolean requiredInInventorySync;
    private boolean requiredInReconciliationSync;
    private boolean requiredInChannelWarehouseInventorySync;
    private String  verificationScriptName;
    private Set<SourceConnectorParameter> sourceConnectorParameters = new HashSet<SourceConnectorParameter>();

    public String getSourceCode() {
        return sourceCode;
    }

    public void setSourceCode(String sourceCode) {
        this.sourceCode = sourceCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getHelpText() {
        return helpText;
    }

    public void setHelpText(String helpText) {
        this.helpText = helpText;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public boolean isThirdParty() {
        return thirdParty;
    }

    public void setThirdParty(boolean thirdParty) {
        this.thirdParty = thirdParty;
    }

    public boolean isRequiredInOrderSync() {
        return requiredInOrderSync;
    }

    public void setRequiredInOrderSync(boolean requiredInOrderSync) {
        this.requiredInOrderSync = requiredInOrderSync;
    }

    public boolean isRequiredInInventorySync() {
        return requiredInInventorySync;
    }

    public void setRequiredInInventorySync(boolean requiredInInventorySync) {
        this.requiredInInventorySync = requiredInInventorySync;
    }

    public boolean isRequiredInReconciliationSync() {
        return requiredInReconciliationSync;
    }

    public boolean isRequiredInChannelWarehouseInventorySync() {
        return requiredInChannelWarehouseInventorySync;
    }

    public void setRequiredInChannelWarehouseInventorySync(boolean requiredInChannelWarehouseInventorySync) {
        this.requiredInChannelWarehouseInventorySync = requiredInChannelWarehouseInventorySync;
    }

    public void setRequiredInReconciliationSync(boolean requiredInReconciliationSync) {
        this.requiredInReconciliationSync = requiredInReconciliationSync;
    }
    
    public String getVerificationScriptName() {
        return verificationScriptName;
    }

    public void setVerificationScriptName(String verificationScriptName) {
        this.verificationScriptName = verificationScriptName;
    }

    public Set<SourceConnectorParameter> getSourceConnectorParameters() {
        return sourceConnectorParameters;
    }

    public void setSourceConnectorParameters(Set<SourceConnectorParameter> sourceConnectorParameters) {
        this.sourceConnectorParameters = sourceConnectorParameters;
    }
}
