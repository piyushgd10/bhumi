package com.uniware.core.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.unifier.core.utils.DateUtils;

/**
 * Created by harshpal on 2/12/16.
 */
@Entity
@Table(name = "cycle_count_item")
public class CycleCountItem implements Serializable {

    private static final long      serialVersionUID = -7695578829039898076L;
    private Integer                id;
    private Shelf                  shelf;
    private String                 skuCode;
    private String                 itemTypeName;
    private CycleCountShelfSummary cycleCountShelfSummary;
    private ItemTypeInventory.Type inventoryType;
    private Date                   ageingStartDate;
    private int                    expectedInventory;
    private int                    inventoryDifference;
    private int                    pendingReconciliation;
    private boolean                discarded;
    private Date                   created;
    private Date                   updated;

    public CycleCountItem() {
    }

    public CycleCountItem(Shelf shelf, String skuCode, String itemTypeName, ItemTypeInventory.Type inventoryType, CycleCountShelfSummary cycleCountShelfSummary,
            Date ageingStartDate, int expectedInventory, int inventoryDifference, int pendingReconciliation) {
        this.shelf = shelf;
        this.skuCode = skuCode;
        this.itemTypeName = itemTypeName;
        this.inventoryType = inventoryType;
        this.cycleCountShelfSummary = cycleCountShelfSummary;
        this.expectedInventory = expectedInventory;
        this.inventoryDifference = inventoryDifference;
        this.ageingStartDate = ageingStartDate;
        this.pendingReconciliation = pendingReconciliation;
        this.created = DateUtils.getCurrentTime();
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "shelf_id", nullable = false)
    public Shelf getShelf() {
        return shelf;
    }

    public void setShelf(Shelf shelf) {
        this.shelf = shelf;
    }

    @Column(name = "sku_code", nullable = false)
    public String getSkuCode() {
        return skuCode;
    }

    public void setSkuCode(String skuCode) {
        this.skuCode = skuCode;
    }

    @Column(name = "inventory_type", nullable = false)
    @Enumerated(EnumType.STRING)
    public ItemTypeInventory.Type getInventoryType() {
        return inventoryType;
    }

    public void setInventoryType(ItemTypeInventory.Type inventoryType) {
        this.inventoryType = inventoryType;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "ageing_start_date", length = 10)
    public Date getAgeingStartDate() {
        return ageingStartDate;
    }

    public void setAgeingStartDate(Date ageingStartDate) {
        this.ageingStartDate = ageingStartDate;
    }

    @Column(name = "item_type_name", nullable = false)
    public String getItemTypeName() {
        return itemTypeName;
    }

    public void setItemTypeName(String itemTypeName) {
        this.itemTypeName = itemTypeName;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cycle_count_shelf_summary_id", nullable = false)
    public CycleCountShelfSummary getCycleCountShelfSummary() {
        return cycleCountShelfSummary;
    }

    public void setCycleCountShelfSummary(CycleCountShelfSummary cycleCountShelfSummary) {
        this.cycleCountShelfSummary = cycleCountShelfSummary;
    }

    @Column(name = "expected_inventory", nullable = false)
    public int getExpectedInventory() {
        return expectedInventory;
    }

    public void setExpectedInventory(int expectedInventory) {
        this.expectedInventory = expectedInventory;
    }

    @Column(name = "inventory_difference", nullable = false)
    public int getInventoryDifference() {
        return inventoryDifference;
    }

    public void setInventoryDifference(int inventoryDifference) {
        this.inventoryDifference = inventoryDifference;
    }

    @Column(name = "pending_reconciliation", nullable = false)
    public int getPendingReconciliation() {
        return pendingReconciliation;
    }

    public void setPendingReconciliation(int pendingReconciliation) {
        this.pendingReconciliation = pendingReconciliation;
    }

    @Column(name = "discarded", nullable = false)
    public boolean isDiscarded() {
        return discarded;
    }

    public void setDiscarded(boolean discarded) {
        this.discarded = discarded;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created", nullable = false, length = 19)
    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated", nullable = false, length = 19, insertable = false, updatable = false)
    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }
}
