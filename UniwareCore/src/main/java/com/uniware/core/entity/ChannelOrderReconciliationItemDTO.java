package com.uniware.core.entity;

import java.math.BigDecimal;

/**
 * Created by akshayag on 10/4/16.
 */
public class ChannelOrderReconciliationItemDTO {

    private String     saleOrderCode;
    private BigDecimal sellingPrice    = BigDecimal.ZERO;
    private BigDecimal shippingCharges = BigDecimal.ZERO;

    public String getSaleOrderCode() {
        return saleOrderCode;
    }

    public void setSaleOrderCode(String saleOrderCode) {
        this.saleOrderCode = saleOrderCode;
    }

    public BigDecimal getSellingPrice() {
        return sellingPrice;
    }

    public void setSellingPrice(BigDecimal sellingPrice) {
        this.sellingPrice = sellingPrice;
    }

    public BigDecimal getShippingCharges() {
        return shippingCharges;
    }

    public void setShippingCharges(BigDecimal shippingCharges) {
        this.shippingCharges = shippingCharges;
    }

}
