package com.uniware.core.entity;

import java.io.Serializable;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.Transient;

import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import com.unifier.core.utils.StringUtils;

@Document(collection = "source")
public class Source implements Serializable {

    public enum Code {
        CUSTOM,
    }

    public enum Type {
        B2B,
        CART,
        MARKETPLACE
    }

    public enum Localization {
        NATIONAL,
        INTERNATIONAL
    }

    public enum ShipmentLabelFormat {
        CSV,
        HTML,
        PDF,
        PNG
    }

    public enum ShipmentLabelAggregationFormat {
        NONE("A4", "A4", false),
        A6_TO_A4("A6", "A4", false),
        A5_TO_A4("A5", "A4", false),
        A6_TO_A4_LANDSCAPE("A6", "A4", true),
        A5_TO_A4_LANDSCAPE("A5", "A4", true);

        private String  fromSize;
        private String  toSize;
        private boolean landscape;

        private ShipmentLabelAggregationFormat(String fromSize, String toSize, boolean landscape) {
            this.fromSize = fromSize;
            this.toSize = toSize;
            this.landscape = landscape;
        }

        public String getFromSize() {
            return fromSize;
        }

        public void setFromSize(String fromSize) {
            this.fromSize = fromSize;
        }

        public String getToSize() {
            return toSize;
        }

        public void setToSize(String toSize) {
            this.toSize = toSize;
        }

        public boolean isLandscape() {
            return landscape;
        }

        public void setLandscape(boolean landscape) {
            this.landscape = landscape;
        }

    }

    public enum ConfigurationStatus {
        YES,
        NO,
        PROMPT
    }

    // Boolean Properties
    public static final String                        ACTIVE                                                       = "active";
    public static final String                        USE_SELLER_SKU_FOR_INVENTORY_UPDATE                          = "use.seller.sku.for.inventory.update";
    public static final String                        CHANNEL_VERIFICATION_SUPPORTED                               = "channel.verification.supported";
    public static final String                        NOTIFICATIONS_ENABLED                                        = "notifications.enabled";
    public static final String                        THIRD_PARTY_CONFIGURATION_REQUIRED                           = "third.party.configuration.required";
    public static final String                        INVENTORY_SYNC_CONFIGURED                                    = "inventory.sync.configured";
    public static final String                        ORDER_SYNC_CONFIGURED                                        = "order.sync.configured";
    public static final String                        ORDER_STATUS_SYNC_CONFIGURED                                 = "order.status.sync.configured";
    private static final String                       CATALOG_SYNC_CONFIGURED                                      = "catalog.sync.supported";
    private static final String                       CHANNEL_WAREHOUSE_INVENTORY_SYNC_CONFIGURED                  = "channel.warehouse.inventory.sync.configured";
    public static final String                        ALLOW_MULTIPLE_CHANNEL                                       = "allow.multiple.channel";
    public static final String                        PENDENCY_CONFIGURATION_ENABLED                               = "pendency.configuration.enabled";
    public static final String                        COMMIT_PENDENCY_ON_CHANNEL                                   = "commit.pendency.on.channel";
    public static final String                        ALLOW_COMBINED_MANIFEST                                      = "allow.combined.manifest";
    public static final String                        PROMPT_FOR_SHIPMENT_LABEL_FORMAT                             = "prompt.for.shipment.label.format";
    public static final String                        AUTO_VERIFY_ORDERS                                           = "auto.verify.orders";
    public static final String                        ALLOW_ANY_SHIPPING_METHOD                                    = "allow.any.shipping.method";
    public static final String                        ALLOW_VENDOR_SELF                                            = "allow.vendor.self";
    public static final String                        MANIFEST_ALL_READY_TO_SHIP_ORDERS_TOGETHER                   = "manifest.all.ready.to.ship.orders.together";
    public static final String                        DISPLAY_ORDER_CODE_UNIQUE                                    = "display.order.code.unique";
    public static final String                        PAYMENT_RECONCILIATION_ENABLED                               = "payment.reconciliation.enabled";
    public static final String                        PACKAGE_TYPE_CONFIGURED                                      = "package.type.configured";
    public static final String                        THIRD_PARTY_SHIPPING_AVAILABLE                               = "third.party.shipping.available";
    public static final String                        ORDER_RECONCILIATION_ENABLED                                 = "order.reconciliation.enabled";
    public static final String                        PRODUCT_DELISTING_CONFIGURED                                 = "product.delisting.configured";
    public static final String                        SHIPPING_LABEL_AGGREGATION_CONFIGURED                        = "shipping.label.aggregation.enabled";
    public static final String                        HTTP_PROXY_REQUIRED                                          = "http.proxy.required";
    public static final String                        DISPATCH_PACKAGE_ON_MANIFEST_CLOSE                           = "dispatch.package.on.manifest.close";
    public static final String                        RESTRICT_CUSTOMER_NOTIFICATION_ON_DISPATCH                   = "restrict.customer.notification.on.dispatch";
    public static final String                        REFETCH_SHIPPING_LABEL_ALLOWED                               = "refetch.shipping.label.allowed";
    public static final String                        HEARTBEAT_SYNC_ENABLED                                       = "heartbeat.sync.enabled";
    public static final String                        ORDER_REFRESH_ENABLED                                        = "order.refresh.enabled";
    public static final String                        DISPLAY_ORDER_CODE_TO_SALE_ORDER_CODE_UNIQUE                 = "display.order.code.to.sale.order.code.unique";
    public static final String                        FETCH_CURRENT_CHANNEL_MANIFEST_ENABLED                       = "fetch.current.channel.manifest.enabled";
    public static final String                        SELECTIVE_CATALOG_SYNC_ENABLED                               = "selective.catalog.sync.enabled";
    public static final String                        FREQUENT_ORDER_SYNC_ENABLED                                  = "frequent.order.sync.enabled";
    public static final String                        DISALLOW_PACKAGE_SPLIT                                       = "disallow.package.split";
    public static final String                        TAT_AVAILABLE_FROM_CHANNEL                                   = "tat.available.from.channel";
    public static final String                        POD_REQUIRED                                                 = "pod.required";
    public static final String                        FACILITY_ASSOCIATION_REQUIRED                                = "facility.association.required";
    public static final String                        FETCH_COMPLETE_ORDERS                                        = "fetch.complete.orders";
    public static final String                        FULFILLMENT_BY_CHANNEL                                       = "fulfillment.by.channel";
    public static final String                        PRICING_SYNC_CONFIGURED                                      = "pricing.sync.configured";
    public static final String                        PRICE_COMPUTATION_SUPPORTED                                  = "price.computation.supported";
    public static final String                        FRACTIONAL_PRICE_SUPPORTED                                   = "fractional.price.supported";
    public static final String                        PERIODIC_SYNC_DISABLED                                       = "periodic.sync.disabled";
    public static final String                        ASYNC_CATALOG_SYNC_SUPPORTED                                 = "async.catalog.sync.supported";
    private static final String                       ASYNC_RECONCILIATION_SYNC_SUPPORTED                          = "async.reconciliation.sync.supported";
    private static final String                       AUTO_DISABLE_STALE_CATALOG                                   = "auto.disable.stale.catalog";
    public static final String                        CHANNEL_INVOICING                                            = "channel.invoicing";
    public static final String                        DEDUCT_FIXED_GST_ON_OTHER_CHARGES                            = "deduct.fixed.gst.on.other.charges";

    // Integer Properties
    public static final String                        PROCESS_INVENTORY_COUNT                                      = "process.inventory.count";
    public static final String                        PROCESS_NOTIFICATION_COUNT                                   = "process.notification.count";
    public static final String                        PROCESS_PRICE_COUNT                                          = "process.price.count";
    public static final String                        HTTP_PROXY_THRESHOLD                                         = "http.proxy.threshold";
    public static final String                        GST_TAX_CLASS_FOR_OTHER_CHARGES                              = "gst.tax.class.for.other.charges";

    // String properties
    public static final String                        ASSOCIATED_FACILITY                                          = "associated.facility";
    public static final String                        SHIPMENT_LABEL_FORMAT                                        = "shipment.label.format";
    public static final String                        SALE_ORDER_PRE_PROCESSING_SCRIPT_NAME                        = "sale.order.pre.processing.script.name";
    public static final String                        SALE_ORDER_LIST_SCRIPT_NAME                                  = "sale.order.list.script.name";
    public static final String                        SALE_ORDER_DETAILS_SCRIPT_NAME                               = "sale.order.details.script.name";
    public static final String                        MANIFEST_VERIFICATION_SCRIPT_NAME                            = "manifest.verification.script.name";
    public static final String                        NOTIFICATION_SCRIPT_NAME                                     = "notification.script.name";
    public static final String                        PRE_CONFIGURATION_SCRIPT_NAME                                = "pre.configuration.script.name";
    public static final String                        POST_CONFIGURATION_SCRIPT_NAME                               = "post.configuration.script.name";
    public static final String                        POST_INVOICE_SCRIPT_NAME                                     = "post.invoice.script.name";
    public static final String                        SHIPPING_PROVIDER_ALLOCATION_SCRIPT_NAME                     = "shipping.provider.allocation.script.name";
    public static final String                        SALE_ORDER_STATUS_SYNC_SCRIPT_NAME                           = "sale.order.status.sync.script.name";
    public static final String                        DISPATCH_VERIFICATION_SCRIPT_NAME                            = "dispatch.verification.script.name";
    public static final String                        PRE_MANIFEST_SCRIPT_NAME                                     = "pre.manifest.script.name";
    public static final String                        POST_MANIFEST_SCRIPT_NAME                                    = "post.manifest.script.name";
    public static final String                        CHANNEL_WAREHOUSE_INVENTORY_SCRIPT_NAME                      = "channel.warehouse.inventory.script.name";
    public static final String                        CHANNEL_ITEM_TYPE_LIST_SCRIPT_NAME                           = "channel.item.type.list.script.name";
    public static final String                        CHANNEL_ITEM_TYPE_DETAILS_SCRIPT_NAME                        = "channel.item.type.details.script.name";
    public static final String                        INVENTORY_UPDATE_SCRIPT_NAME                                 = "inventory.update.script.name";
    public static final String                        FETCH_PENDENCY_SCRIPT_NAME                                   = "fetch.pendency.script.name";
    public static final String                        PROCESS_PENDENCY_SCRIPT_NAME                                 = "process.pendency.script.name";
    public static final String                        FETCH_SHIPPING_LABEL_SCRIPT_NAME                             = "fetch.shipping.label.script.name";
    public static final String                        VERIFY_ORDER_SCRIPT_NAME                                     = "verify.order.script.name";
    public static final String                        INVENTORY_UPDATE_CRON_EXPRESSION                             = "inventory.update.cron.expression";
    public static final String                        NOTIFICATION_UPDATE_CRON_EXPRESSION                          = "notification.update.cron.expression";
    public static final String                        STOCKOUT_INVENTORY_ACKNOWLEDGE_CRON_EXPRESSION               = "stockout.inventory.acknowledge.cron.expression";
    public static final String                        LANDING_PAGE_SCRIPT_NAME                                     = "landing.page.script.name";
    public static final String                        SALE_ORDER_CANCELLATION_SCRIPT_NAME                          = "sale.order.cancellation.script.name";
    public static final String                        INSTRUCTIONS_TEMPLATE_NAME                                   = "instructions.template.name";
    public static final String                        ORDER_ITEM_PRERECONCILIATION_SCRIPT_NAME                     = "order.item.pre.reconciliation.script.name";
    public static final String                        UNRECONCILIED_ORDER_ITEM_SCRIPT_NAME                         = "unreconcilied.order.item.script.name";
    public static final String                        RECONCILIATION_TRANSACTION_SCRIPT_NAME                       = "reconciliation.transaction.script.name";
    public static final String                        RECONCILIATION_PAYMENT_SCRIPT_NAME                           = "reconciliation.payment.script.name";
    public static final String                        SALE_ORDER_ACKNOWLEDGEMENT_SCRIPT_NAME                       = "sale.order.acknowledgement.script.name";
    public static final String                        CANCEL_SALE_ORDER_ACKNOWLEDGEMENT_SCRIPT_NAME                = "cancel.sale.order.acknowledgement.script.name";
    public static final String                        MARK_SALE_ORDER_RETURNED_SCRIPT_NAME                         = "mark.sale.order.returned.script.name";
    public static final String                        SHIPPING_LABEL_SIZE                                          = "shipping.label.size";
    public static final String                        SHIPPING_LABEL_X_OFFSET                                      = "shipping.label.x.offset";
    public static final String                        SHIPPING_LABEL_Y_OFFSET                                      = "shipping.label.y.offset";
    public static final String                        THIRD_PARTY_SHIPPING_DEFAULT                                 = "third.party.shipping.default";
    public static final String                        PRODUCT_RELISTING_CONFIGURED                                 = "product.relisting.configured";
    public static final String                        CHANNEL_CATALOG_SYNC_PREPROCESSOR_SCRIPT_NAME                = "channel.catalog.sync.preprocessor.script.name";
    public static final String                        CHANNEL_REFRESH_SALE_ORDER_SCRIPT_NAME                       = "channel.refresh.sale.order.script.name";
    public static final String                        HEARTBEAT_SYNC_SCRIPT_NAME                                   = "heartbeat.sync.script.name";
    public static final String                        FORCE_DISPATCH_SHIPPING_PACKAGE_SCRIPT_NAME                  = "force.dispatch.shipping.package.script.name";
    public static final String                        FETCH_CURRENT_CHANNEL_MANIFEST_SCRIPT_NAME                   = "fetch.current.channel.manifest.script.name";
    public static final String                        REFRESH_SHIPPING_LABEL_SCRIPT_NAME                           = "refresh.shipping.label.script.name";
    public static final String                        SALE_ORDER_STATUS_SYNC_METADATA                              = "sale.order.status.sync.metadata.script.name";
    public static final String                        CHANNEL_RECONCILIATION_INVOICE_LIST_SCRIPT_NAME              = "reconciliation.invoice.list.script.name";
    public static final String                        CHANNEL_RECONCILIATION_INVOICE_DETAIL_SCRIPT_NAME            = "reconciliation.invoice.detail.script.name";
    public static final String                        CHANNEL_RECONCILIATION_INVOICE_SYNC_PREPROCESSOR_SCRIPT_NAME = "reconciliation.invoice.sync.preprocessor.script.name";
    public static final String                        RESERVED_KEYWORDS_FOR_SHORT_NAME                             = "reserved.keywords.for.short.name";
    public static final String                        UPDATE_PRICE_SCRIPT_NAME                                     = "price.update.script.name";
    public static final String                        CHECK_PRICE_SCRIPT_NAME                                      = "check.price.script.name";
    public static final String                        POST_SHIPPING_PACKAGE_CREATE_SCRIPT                          = "post.shipping.package.create.script";
    public static final String                        DEFAULT_INVENTORY_UPDATE_FORMULA                             = "default.inventory.update.formula";
    public static final String                        DEFAULT_INVENTORY_UPDATE_FORMULA_CODE                        = "default.inventory.update.formula.code";
    public static final String                        CHANNEL_WAREHOUSE_INVENTORY_SYNC_PREPROCESSOR_SCRIPT_NAME    = "channel.warehouse.inventory.sync.preprocessor.script.name";
    private static final String                       ASYNC_CHANNEL_WAREHOUSE_INVENTORY_SYNC_SUPPORTED             = "async.channel.warehouse.inventory.sync.supported";
    private static final String                       GENERIC_CHANNEL_API_URL                                      = "generic.channel.api.url";
    public static final String                        FETCH_INVOICE_SCRIPT_NAME                                    = "fetch.invoice.script.name";

    // List properties
    public static final String                        SALE_ORDER_CANCELLATION_REASONS                              = "sale.order.cancellation.reasons";
    public static final String                        SALE_ORDER_RETURN_REASONS                                    = "sale.order.return.reasons";
    public static final String                        AVAILABLE_PRICE_FIELDS                                       = "available.price.fields";
    public static final String                        EDITABLE_PRICE_FIELDS                                        = "editable.price.fields";
    public static final String                        SUPPORTED_CURRENCIES                                         = "supported.currencies";
    public static final String                        INVENTORY_SYNC_DISABLING_AFTER_MAX_RETRY_OFF                 = "inventory.sync.disabling.after.max.retries.off";

    private String                                    id;

    @Indexed(unique = true)
    private String                                    code;

    private String                                    name;
    private boolean                                   enabled;
    private int                                       priority;
    private Type                                      type;
    private Localization                              localization;
    private Date                                      created;
    private Date                                      updated;
    private Set<SourceProperty>                       sourceProperties                                             = new HashSet<>();
    private Set<SourceConnector>                      sourceConnectors                                             = new HashSet<>();
    private Set<SourceConfigurationParameter>         sourceConfigurationParameters                                = new HashSet<>();

    private Map<String, Object>                       sourcePropertyToValue;
    private Map<String, SourceConfigurationParameter> paramterNameToParameter;

    public Source() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public Localization getLocalization() {
        return localization;
    }

    public void setLocalization(Localization localization) {
        this.localization = localization;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public Set<SourceProperty> getSourceProperties() {
        return sourceProperties;
    }

    public void setSourceProperties(Set<SourceProperty> sourceProperties) {
        this.sourceProperties = sourceProperties;
    }

    public Set<SourceConnector> getSourceConnectors() {
        return sourceConnectors;
    }

    public void setSourceConnectors(Set<SourceConnector> sourceConnectors) {
        this.sourceConnectors = sourceConnectors;
    }

    public Set<SourceConfigurationParameter> getSourceConfigurationParameters() {
        return sourceConfigurationParameters;
    }

    public void setSourceConfigurationParameters(Set<SourceConfigurationParameter> sourceConfigurationParameters) {
        this.sourceConfigurationParameters = sourceConfigurationParameters;
    }

    @Transient
    public boolean isActive() {
        return Boolean.parseBoolean(getFieldValue(ACTIVE, "true"));
    }

    @Transient
    public boolean isInventorySyncConfigured() {
        return Boolean.parseBoolean(getStringifiedFieldValue(INVENTORY_SYNC_CONFIGURED));
    }

    @Transient
    public boolean isOrderSyncConfigured() {
        return Boolean.parseBoolean(getStringifiedFieldValue(ORDER_SYNC_CONFIGURED));
    }

    @Transient
    public boolean isCatalogSyncConfigured() {
        if (StringUtils.isNotBlank(getStringifiedFieldValue(CATALOG_SYNC_CONFIGURED))) {
            return Boolean.parseBoolean(getStringifiedFieldValue(CATALOG_SYNC_CONFIGURED));
        }
        return true;
    }

    @Transient
    public boolean isChannelWarehouseInventorySyncConfigured() {
        if (StringUtils.isNotBlank(getStringifiedFieldValue(CHANNEL_WAREHOUSE_INVENTORY_SYNC_CONFIGURED))) {
            return Boolean.parseBoolean(getStringifiedFieldValue(CHANNEL_WAREHOUSE_INVENTORY_SYNC_CONFIGURED));
        }
        return true;
    }

    @Transient
    public boolean isInventorySyncDisablingAfterMaxRetryOff() {
        String value = getStringifiedFieldValue(INVENTORY_SYNC_DISABLING_AFTER_MAX_RETRY_OFF);
        return StringUtils.isBlank(value) ? false : Boolean.parseBoolean(value);
    }

    @Transient
    public boolean isInventorySyncDisablingAfterMaxRetryOn() {
        return !isInventorySyncDisablingAfterMaxRetryOff();
    }

    @Transient
    public boolean isPricingSyncConfigured() {
        return Boolean.parseBoolean(getStringifiedFieldValue(PRICING_SYNC_CONFIGURED));
    }

    @Transient
    public boolean isPriceComputationSupported() {
        return Boolean.parseBoolean(getStringifiedFieldValue(PRICE_COMPUTATION_SUPPORTED));
    }

    @Transient
    public boolean isFractionalPriceSupported() {
        return Boolean.parseBoolean(getStringifiedFieldValue(FRACTIONAL_PRICE_SUPPORTED));
    }

    @Transient
    public boolean isPeriodicSyncDisabled() {
        return Boolean.parseBoolean(getStringifiedFieldValue(PERIODIC_SYNC_DISABLED));
    }

    @Transient
    public boolean isInvoicingOfChannel() {
        return Boolean.parseBoolean(getStringifiedFieldValue(CHANNEL_INVOICING));
    }

    @Transient
    public boolean isAsyncCatalogSyncSupported() {
        return Boolean.parseBoolean(getStringifiedFieldValue(ASYNC_CATALOG_SYNC_SUPPORTED));
    }

    @Transient
    public boolean isAsyncChannelWarehouseInventorySyncSupported() {
        return Boolean.parseBoolean(getStringifiedFieldValue(ASYNC_CHANNEL_WAREHOUSE_INVENTORY_SYNC_SUPPORTED));
    }

    @Transient
    public boolean isAllowMultipleChannel() {
        return Boolean.parseBoolean(getStringifiedFieldValue(ALLOW_MULTIPLE_CHANNEL));
    }

    @Transient
    public boolean isThirdPartyConfigurationRequired() {
        return Boolean.parseBoolean(getStringifiedFieldValue(THIRD_PARTY_CONFIGURATION_REQUIRED));
    }

    @Transient
    public boolean isFulfillmentByChannel() {
        return isFetchCompleteOrders();
    }

    @Transient
    public boolean isAsyncReconciliationSyncSupported() {
        return Boolean.parseBoolean(getStringifiedFieldValue(ASYNC_RECONCILIATION_SYNC_SUPPORTED));
    }

    @Transient
    public boolean isFetchCompleteOrders() {
        return Boolean.parseBoolean(getStringifiedFieldValue(FETCH_COMPLETE_ORDERS));
    }

    @Transient
    public boolean isNotificationsEnabled() {
        return Boolean.parseBoolean(getStringifiedFieldValue(NOTIFICATIONS_ENABLED));
    }

    @Transient
    public int getProcessNotificationCount() {
        return Integer.parseInt(getFieldValue(PROCESS_NOTIFICATION_COUNT, "1"));
    }

    @Transient
    public int getProcessInventoryCount() {
        return Integer.parseInt(getFieldValue(PROCESS_INVENTORY_COUNT, "1"));
    }

    @Transient
    public int getProcessPriceCount() {
        return Integer.parseInt(getFieldValue(PROCESS_PRICE_COUNT, "1"));
    }

    @Transient
    public int getHttpProxyThreshold() {
        return Integer.parseInt(getFieldValue(HTTP_PROXY_THRESHOLD, "10"));
    }

    @Transient
    public String getInventoryUpdateCronExpression() {
        return getFieldValue(INVENTORY_UPDATE_CRON_EXPRESSION, "0 */30 * * * *");
    }

    @Transient
    public String getNotificationUpdateCronExpression() {
        return getStringifiedFieldValue(NOTIFICATION_UPDATE_CRON_EXPRESSION);
    }

    @Transient
    public String getStockoutInventoryAcknowledgeCronExpression() {
        return getStringifiedFieldValue(STOCKOUT_INVENTORY_ACKNOWLEDGE_CRON_EXPRESSION);
    }

    @Transient
    public boolean isUseSellerSkuForInventoryUpdate() {
        return Boolean.parseBoolean(getStringifiedFieldValue(USE_SELLER_SKU_FOR_INVENTORY_UPDATE));
    }

    @Transient
    public boolean isChannelVerificationSupported() {
        return Boolean.parseBoolean(getStringifiedFieldValue(CHANNEL_VERIFICATION_SUPPORTED));
    }

    @Transient
    public boolean isPendencyConfigurationEnabled() {
        return Boolean.parseBoolean(getStringifiedFieldValue(PENDENCY_CONFIGURATION_ENABLED));
    }

    @Transient
    public boolean isCommitPendencyOnChannel() {
        return Boolean.parseBoolean(getStringifiedFieldValue(COMMIT_PENDENCY_ON_CHANNEL));
    }

    @Transient
    public boolean isAutoDisableStaleCatalog() {
        String value = getStringifiedFieldValue(AUTO_DISABLE_STALE_CATALOG);
        if (value != null) {
            return Boolean.parseBoolean(value);
        }
        return true;
    }

    @Transient
    public ConfigurationStatus getProductRelistingConfigured() {
        ConfigurationStatus status;
        String value = getStringifiedFieldValue(PRODUCT_RELISTING_CONFIGURED);
        if (StringUtils.isBlank(value)) {
            status = ConfigurationStatus.NO;
        } else {
            switch (value) {
                case "PROMPT":
                    status = ConfigurationStatus.PROMPT;
                    break;
                case "YES":
                    status = ConfigurationStatus.YES;
                    break;
                case "NO":
                    status = ConfigurationStatus.NO;
                    break;
                default:
                    status = ConfigurationStatus.NO;
                    break;
            }
        }
        return status;
    }

    @Transient
    public float getShippingLabelXOffset() {
        String value = getStringifiedFieldValue(SHIPPING_LABEL_X_OFFSET);
        if (StringUtils.isNotBlank(value)) {
            try {
                return Float.parseFloat(value);
            } catch (Exception ex) {
            }
        }
        return 0f;
    }

    @Transient
    public float getShippingLabelYOffset() {
        String value = getStringifiedFieldValue(SHIPPING_LABEL_Y_OFFSET);
        if (StringUtils.isNotBlank(value)) {
            try {
                return Float.parseFloat(value);
            } catch (Exception ex) {
            }
        }
        return 0f;
    }

    @Transient
    public Boolean getThirdPartyShippingDefault() {
        String value = getStringifiedFieldValue(THIRD_PARTY_SHIPPING_DEFAULT);
        if (StringUtils.isNotBlank(value)) {
            try {
                return Boolean.parseBoolean(value);
            } catch (Exception ex) {
            }
        }
        return null;
    }

    @Transient
    public boolean isOrderRefreshEnabled() {
        return Boolean.parseBoolean(getFieldValue(ORDER_REFRESH_ENABLED, "false"));
    }

    @Transient
    public boolean isHeartbeatSyncEnabled() {
        return Boolean.parseBoolean(getFieldValue(HEARTBEAT_SYNC_ENABLED, "false"));
    }

    @Transient
    public boolean isOrderReconciliationEnabled() {
        return Boolean.parseBoolean(getFieldValue(ORDER_RECONCILIATION_ENABLED, "false"));
    }

    @Transient
    public boolean isProductDelistingConfigured() {
        return Boolean.parseBoolean(getFieldValue(PRODUCT_DELISTING_CONFIGURED, "false"));
    }

    @Transient
    public boolean isShippingLabelAggregationConfigured() {
        return Boolean.parseBoolean(getFieldValue(SHIPPING_LABEL_AGGREGATION_CONFIGURED, "false"));
    }

    @Transient
    public boolean isHttpProxyRequired() {
        return Boolean.parseBoolean(getFieldValue(HTTP_PROXY_REQUIRED, "false"));
    }

    @Transient
    public boolean isDispatchPackageOnManifestClose() {
        return Boolean.parseBoolean(getFieldValue(DISPATCH_PACKAGE_ON_MANIFEST_CLOSE, "false"));
    }

    @Transient
    public boolean isPaymentReconciliationEnabled() {
        return Boolean.parseBoolean(getStringifiedFieldValue(PAYMENT_RECONCILIATION_ENABLED));
    }

    @Transient
    public String getLandingPageScriptName() {
        return getStringifiedFieldValue(LANDING_PAGE_SCRIPT_NAME);
    }

    @Transient
    public String getSaleOrderCancellationScriptName() {
        return getStringifiedFieldValue(SALE_ORDER_CANCELLATION_SCRIPT_NAME);
    }

    @Transient
    public String getShipmentLabelFormat() {
        if (!Boolean.parseBoolean(getStringifiedFieldValue(PROMPT_FOR_SHIPMENT_LABEL_FORMAT))) {
            return getStringifiedFieldValue(SHIPMENT_LABEL_FORMAT);
        }
        return null;
    }

    @Transient
    public String getDefaultShipmentLabelFormat() {
        return getStringifiedFieldValue(SHIPMENT_LABEL_FORMAT);
    }

    @Transient
    public boolean isAllowCombinedManifest() {
        return Boolean.parseBoolean(getStringifiedFieldValue(ALLOW_COMBINED_MANIFEST));
    }

    @Transient
    public boolean isAutoVerifyOrders() {
        return Boolean.parseBoolean(getStringifiedFieldValue(AUTO_VERIFY_ORDERS));
    }

    @Transient
    public boolean deductFixedGstOnOtherCharges() {
        return Boolean.parseBoolean(getStringifiedFieldValue(DEDUCT_FIXED_GST_ON_OTHER_CHARGES));
    }

    @Transient
    public boolean isPackageTypeConfigured() {
        return Boolean.parseBoolean(getStringifiedFieldValue(PACKAGE_TYPE_CONFIGURED));
    }

    @Transient
    public boolean isThirdPartyShippingAvailable() {
        String value = getStringifiedFieldValue(THIRD_PARTY_SHIPPING_AVAILABLE);
        if (StringUtils.isNotBlank(value)) {
            return Boolean.parseBoolean(value);
        }
        return true;
    }

    @Transient
    public boolean isAllowAnyShippingMethod() {
        String value = getStringifiedFieldValue(ALLOW_ANY_SHIPPING_METHOD);
        if (StringUtils.isNotBlank(value)) {
            return Boolean.parseBoolean(value);
        }
        return true;
    }

    @Transient
    public boolean isAllowVendorSelf() {
        String value = getStringifiedFieldValue(ALLOW_VENDOR_SELF);
        if (StringUtils.isNotBlank(value)) {
            return Boolean.parseBoolean(value);
        }
        return false;
    }

    @Transient
    public boolean isManifestAllReadyToShipOrdersTogether() {
        return Boolean.parseBoolean(getStringifiedFieldValue(MANIFEST_ALL_READY_TO_SHIP_ORDERS_TOGETHER));
    }

    @Transient
    public boolean isDisplayOrderCodeUnique() {
        return Boolean.parseBoolean(getStringifiedFieldValue(DISPLAY_ORDER_CODE_UNIQUE));
    }

    @Transient
    public boolean isDisplayOrderCodeToSaleOrderCodeUnique() {
        return Boolean.parseBoolean(getStringifiedFieldValue(DISPLAY_ORDER_CODE_TO_SALE_ORDER_CODE_UNIQUE));
    }

    @Transient
    public boolean isFetchCurrentChannelManifestEnabled() {
        return Boolean.parseBoolean(getStringifiedFieldValue(FETCH_CURRENT_CHANNEL_MANIFEST_ENABLED));
    }

    @Transient
    public boolean isSelectiveCatalogSyncEnabled() {
        return Boolean.parseBoolean(getStringifiedFieldValue(SELECTIVE_CATALOG_SYNC_ENABLED));
    }

    @Transient
    public boolean isFrequentOrderSyncEnabled() {
        return Boolean.parseBoolean(getStringifiedFieldValue(FREQUENT_ORDER_SYNC_ENABLED));
    }

    @Transient
    public boolean isDisallowPackageSplit() {
        return Boolean.parseBoolean(getStringifiedFieldValue(DISALLOW_PACKAGE_SPLIT));
    }

    @Transient
    public boolean isTatAvailableFromChannel() {
        return Boolean.parseBoolean(getStringifiedFieldValue(TAT_AVAILABLE_FROM_CHANNEL));
    }

    @Transient
    public boolean isPODRequired() {
        return Boolean.parseBoolean(getStringifiedFieldValue(POD_REQUIRED));
    }

    @Transient
    public boolean isFacilityAssociationRequired() {
        return Boolean.parseBoolean(getStringifiedFieldValue(FACILITY_ASSOCIATION_REQUIRED));
    }

    @Transient
    public boolean isRestrictCustomerNotificationOnDispatch() {
        return Boolean.parseBoolean(getStringifiedFieldValue(RESTRICT_CUSTOMER_NOTIFICATION_ON_DISPATCH));
    }

    @Transient
    public boolean isRefetchShippingLabelAllowed() {
        return Boolean.parseBoolean(getStringifiedFieldValue(REFETCH_SHIPPING_LABEL_ALLOWED));
    }

    @Transient
    public boolean isOrderStatusSyncAvailable() {
        return getStringifiedFieldValue(SALE_ORDER_STATUS_SYNC_SCRIPT_NAME) != null;
    }

    @SuppressWarnings("unchecked")
    @Transient
    public List<String> getSaleOrderCancellationReasons() {
        Object value = getFieldValue(SALE_ORDER_CANCELLATION_REASONS);
        if (value != null) {
            return (List<String>) value;
        }
        return Collections.emptyList();
    }

    @SuppressWarnings("unchecked")
    @Transient
    public List<String> getSaleOrderReturnReasons() {
        Object value = getFieldValue(SALE_ORDER_RETURN_REASONS);
        if (value != null) {
            return (List<String>) value;
        }
        return Collections.emptyList();
    }

    @SuppressWarnings("unchecked")
    @Transient
    public List<String> getAvailablePriceFields() {
        Object value = getFieldValue(AVAILABLE_PRICE_FIELDS);
        if (value != null) {
            return (List<String>) value;
        }
        return Collections.emptyList();
    }

    @SuppressWarnings("unchecked")
    @Transient
    public List<String> getEditablePriceFields() {
        Object value = getFieldValue(EDITABLE_PRICE_FIELDS);
        if (value != null) {
            return (List<String>) value;
        }
        return Collections.emptyList();
    }

    @SuppressWarnings("unchecked")
    @Transient
    public List<String> getSupportedCurrencies() {
        Object value = getFieldValue(SUPPORTED_CURRENCIES);
        if (value != null) {
            return (List<String>) value;
        }
        return Collections.EMPTY_LIST;
    }

    @SuppressWarnings("unchecked")
    @Transient
    public String getGenericChannelApiUrl() {
        Object value = getFieldValue(GENERIC_CHANNEL_API_URL);
        if (value != null) {
            return (String) value;
        }

        return null;
    }

    @Transient
    public String getStringifiedFieldValue(String fieldName) {
        if (sourcePropertyToValue == null) {
            sourcePropertyToValue = new HashMap<>();
            for (SourceProperty property : getSourceProperties()) {
                sourcePropertyToValue.put(property.getName(), property.getValue());
            }
        }
        Object value = sourcePropertyToValue.get(fieldName);
        return value != null ? String.valueOf(value) : null;
    }

    @Transient
    public Object getFieldValue(String fieldName) {
        if (sourcePropertyToValue == null) {
            sourcePropertyToValue = new HashMap<>();
            for (SourceProperty property : getSourceProperties()) {
                sourcePropertyToValue.put(property.getName(), property.getValue());
            }
        }
        return sourcePropertyToValue.get(fieldName);
    }

    @Transient
    public String getFieldValue(String fieldName, String defaultValue) {
        String value = getStringifiedFieldValue(fieldName);
        return StringUtils.isBlank(value) ? defaultValue : value;
    }

    @Transient
    public String getReservedKeywordsForShortName() {
        return getStringifiedFieldValue(RESERVED_KEYWORDS_FOR_SHORT_NAME);
    }

    @Transient
    public String getDefaultInventoryUpdateFormula() {
        return getStringifiedFieldValue(DEFAULT_INVENTORY_UPDATE_FORMULA);
    }

    @Transient
    public String getDefaultInventoryUpdateFormulaCode() {
        return getStringifiedFieldValue(DEFAULT_INVENTORY_UPDATE_FORMULA_CODE);
    }

    @Transient
    public SourceConfigurationParameter getSourceConfigutaionParameter(String fieldName) {
        if (paramterNameToParameter == null) {
            paramterNameToParameter = new HashMap<>();
            for (SourceConfigurationParameter param : getSourceConfigurationParameters()) {
                paramterNameToParameter.put(param.getName(), param);
            }
        }
        return paramterNameToParameter.get(fieldName);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        Source source = (Source) o;

        return code.equals(source.code);
    }

    @Override
    public int hashCode() {
        return code.hashCode();
    }

    public static void main(String args[]) {
        System.out.println(Boolean.parseBoolean(null));
    }
}