package com.uniware.core.entity;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * @author parijat
 */
@Entity
@Table(name = "shipping_provider_parameter", uniqueConstraints = @UniqueConstraint(columnNames = { "shipping_provider_id", "name" }))
public class ShippingProviderParameterOld implements java.io.Serializable {

    private static final long serialVersionUID = 5124339782441994459L;
    private Integer          id;
    private ShippingProvider shippingProvider;
    private String           name;
    private String           value;

    public ShippingProviderParameterOld() {
    }

    public ShippingProviderParameterOld(ShippingProvider shippingProvider, String name, String value) {
        this.shippingProvider = shippingProvider;
        this.name = name;
        this.value = value;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "shipping_provider_id", nullable = false)
    public ShippingProvider getShippingProvider() {
        return this.shippingProvider;
    }

    public void setShippingProvider(ShippingProvider shippingProvider) {
        this.shippingProvider = shippingProvider;
    }

    @Column(name = "name", nullable = false, length = 45)
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "value", nullable = false, length = 256)
    public String getValue() {
        return this.value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
