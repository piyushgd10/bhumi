/**
 * Copyright 2017 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version 1.0, 18/09/17
 * @author aditya
 */
package com.uniware.core.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

import com.uniware.core.entity.Putaway.Type;

/**
 * An inventory ledger is used to keep track of inventory leaving or entering the warehouse. It contains the skuCode as
 * identifier of inventory, and keeps the old balance and new balance to show the change in inventory. On {@code ITEM}
 * level traceability, item codes are also kept.
 */
@Entity
@Table(name = "inventory_ledger", uniqueConstraints = { @UniqueConstraint(columnNames = { "facility_id", "sku_code" }) })
public class InventoryLedger implements Serializable {
    private static final long serialVersionUID = -6025247728451462591L;
    private Integer           id;
    private Facility          facility;
    private String            skuCode;
    /**
     * Contains info about transaction type for which the ledger entry was created.
     * 
     * @see TransactionType
     */
    private TransactionType   transactionType;
    /**
     * <ul>
     * Used to keep track of transactions in which the ledger entry was made. Based on {@link TransactionType},
     * following combinations can happen </b>
     * <li>{@link TransactionType#OPENING_BALANCE}: null</li>
     * <li>{@link TransactionType#MONTHLY_OPENING_BALANCE}: null</li>
     * <li>{@link TransactionType#GRN}: GRN code</li>
     * <li>{@link TransactionType#SALE_ORDER_DISPATCH}: sale order item code</li>
     * <li>{@link TransactionType#RETURN}: sale order item code</li>
     * <li>{@link TransactionType#OUTBOUND_GATE_PASS}: gate pass code</li>
     * <li>{@link TransactionType#OUTBOUND_GATE_PASS_RETURN} gate pass code</li>
     * <li>{@link TransactionType#INVENTORY_ADJUSTMENT}: inventory adjustment id</li>
     * <li>{@link TransactionType#CROSSDOCK}: item code</>
     * </ul>
     */
    private String            transactionIdentifier;
    private int               oldBalance;
    private int               newBalance;
    /**
     * <ul>
     * </b>
     * <li>{@link TransactionType#OPENING_BALANCE}: This field should contain the missing inventory (from cycle count)
     * that was at opening. (Only used in migration queries, nowhere in code.)</li>
     * <li>{@link TransactionType#MONTHLY_OPENING_BALANCE}: Same as {@code OPENING_BALANCE}</li>
     * <li>{@link TransactionType#GRN}: putaway code of PUTAWAY_GRN_ITEM type putaway.</li>
     * <li>{@link TransactionType#SALE_ORDER_DISPATCH}: manifest code</li>
     * <li>{@link TransactionType#RETURN}: putaway code</li>
     * <li>{@link TransactionType#OUTBOUND_GATE_PASS}: gate pass type</li>
     * <li>{@link TransactionType#OUTBOUND_GATE_PASS_RETURN}: gate pass code</li>
     * <li>{@link TransactionType#INVENTORY_ADJUSTMENT}: adjustment type</li>
     * <li>{@link TransactionType#CROSSDOCK}: Empty string</li>
     * </ul>
     */
    private String            additionalInfo;
    private StatusCode        statusCode;
    private String            itemCodes;
    private Date              created;

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "facility_id", nullable = false)
    public Facility getFacility() {
        return facility;
    }

    public void setFacility(Facility facility) {
        this.facility = facility;
    }

    @Column(name = "sku_code", nullable = false)
    public String getSkuCode() {
        return skuCode;
    }

    public void setSkuCode(String skuCode) {
        this.skuCode = skuCode;
    }

    @Enumerated(EnumType.STRING)
    @Column(name = "transaction_type", nullable = false)
    public TransactionType getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(TransactionType transactionType) {
        this.transactionType = transactionType;
    }

    @Column(name = "transaction_identifier", nullable = false)
    public String getTransactionIdentifier() {
        return transactionIdentifier;
    }

    public void setTransactionIdentifier(String transactionIdentifier) {
        this.transactionIdentifier = transactionIdentifier;
    }

    @Column(name = "old_balance", nullable = false)
    public int getOldBalance() {
        return oldBalance;
    }

    public void setOldBalance(int oldBalance) {
        this.oldBalance = oldBalance;
    }

    @Column(name = "new_balance", nullable = false)
    public int getNewBalance() {
        return newBalance;
    }

    public void setNewBalance(int newBalance) {
        this.newBalance = newBalance;
    }

    @Column(name = "additional_info")
    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    @Column(name = "status_code")
    @Enumerated(EnumType.STRING)
    public StatusCode getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(StatusCode statusCode) {
        this.statusCode = statusCode;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created", nullable = false, length = 19)
    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    @Override
    public String toString() {
        return "InventoryLedger{" + "facility=" + facility + ", skuCode='" + skuCode + '\'' + ", transactionType=" + transactionType + ", transactionIdentifier='"
                + transactionIdentifier + '\'' + ", oldBalance=" + oldBalance + ", newBalance=" + newBalance + ", additionalInfo='" + additionalInfo + '\'' + ", created=" + created + '}';
    }

    /**
     * <b>INCREASE</b> denotes that inventory is increased. Ex: If {@code INCREASE} by 5, then
     * {@code newQuantity = oldQuantity + 5}<br/>
     * <b>DECREASE</b> denotes that inventory is decreased. Ex: If {@code DECREASE} by 4, then
     * {@code newQuantity = oldQuantity - 4}<br/>
     */
    public enum ChangeType {
        INCREASE,
        DECREASE
    }

    public enum StatusCode {
        LIVE
    }

    /**
     * Contains various transaction types. Any addition and deletion should be accompanied by a change in enum types of
     * {@link #transactionType} by an sql query.
     * <ul>
     * <li>{@link TransactionType#OPENING_BALANCE}: Used for first time migration. No use should be there in code, only
     * in sql queries.</li>
     * <li>{@link TransactionType#MONTHLY_OPENING_BALANCE}: Balance at the start of the month. Populated by
     * {@code MonthlyOpeningBalancePopulatorJob}</li>
     * <li>{@link TransactionType#GRN}: Used when adding inflow recipt items are added to putaway.</li>
     * <li>{@link TransactionType#SALE_ORDER_DISPATCH}: When shipping package is marked dispatched.</li>
     * <li>{@link TransactionType#RETURN}: Used when adding return items, either of type {@link ReversePickup} or of
     * {@link ReturnManifest}, to putaway.(Used for putaway types {@link Type#PUTAWAY_COURIER_RETURNED_ITEMS},
     * {@link Type#PUTAWAY_REVERSE_PICKUP_ITEM} and {@link Type#PUTAWAY_RECEIVED_RETURNS})</li>
     * <li>{@link TransactionType#OUTBOUND_GATE_PASS}: When outbound gate pass is to be marked complete.</li>
     * <li>{@link TransactionType#OUTBOUND_GATE_PASS_RETURN}: Used when adding returnable gatepass items to
     * putaway.</li>
     * <li>{@link TransactionType#INVENTORY_ADJUSTMENT}: When inventory is changed without any due process. See
     * {@link InventoryAdjustment}</li>
     * <li>{@link TransactionType#CROSSDOCK}: When inventory is added via jabong service, used only for Myntra.</li>
     * </ul>
     */
    public enum TransactionType {
        OPENING_BALANCE,
        MONTHLY_OPENING_BALANCE,
        GRN,
        SALE_ORDER_DISPATCH,
        RETURN,
        OUTBOUND_GATE_PASS,
        OUTBOUND_GATE_PASS_RETURN,
        INVENTORY_ADJUSTMENT,
        CROSSDOCK
    }
}
