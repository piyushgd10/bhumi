package com.uniware.core.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 * Imported by Aditya Harit on 12/06/2017 from SdUniware
 */
@Entity
@Table(name = "cycle_count")
public class CycleCount implements Serializable {
    private static final long serialVersionUID = 3655577728112310188L;
    private Integer           id;
    private String            code;
    private Facility          facility;
    private String            statusCode;
    private Date              targetCompletion;
    private Date              completed;
    private Date              created;
    private Date              updated;
    private Long              completedShelves;
    private Long              totalShelves;

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "code", unique = true, nullable = false, length = 20)
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "facility_id", nullable = false)
    public Facility getFacility() {
        return facility;
    }

    public void setFacility(Facility facility) {
        this.facility = facility;
    }

    @Column(name = "status_code", nullable = false, length = 20)
    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "target_completion", nullable = false, length = 19)
    public Date getTargetCompletion() {
        return targetCompletion;
    }

    public void setTargetCompletion(Date targetCompletion) {
        this.targetCompletion = targetCompletion;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "completed", length = 19)
    public Date getCompleted() {
        return completed;
    }

    public void setCompleted(Date completed) {
        this.completed = completed;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created", nullable = false, length = 19)
    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated", nullable = false, length = 19, insertable = false, updatable = false)
    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    @Transient
    public boolean isActive() {
        return statusCode.equals(StatusCode.CREATED.name());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || !(o instanceof CycleCount))
            return false;
        CycleCount that = (CycleCount) o;
        if (getId() == null) {
            if (that.getId() != null)
                return false;
        } else if (!getId().equals(that.getId()))
            return false;
        return true;
    }

    @Override
    public int hashCode() {
        return getId() != null ? getId().hashCode() : 0;
    }

    @Column(name = "completed_shelves", nullable = true)
    public Long getCompletedShelves() {
        return completedShelves;
    }

    public void setCompletedShelves(Long completedShelves) {
        this.completedShelves = completedShelves;
    }

    @Column(name = "total_shelves", nullable = true)
    public Long getTotalShelves() {
        return totalShelves;
    }

    public void setTotalShelves(Long allShelves) {
        this.totalShelves = allShelves;
    }

    @Override public String toString() {
        return "CycleCount{" + "id=" + id + ", code='" + code + '\'' + ", facility=" + facility + ", statusCode='"
                + statusCode + '\'' + '}';
    }

    public enum StatusCode {
        CREATED,
        IN_PROGRESS,
        COMPLETED,
        ABORTED
    }
}
