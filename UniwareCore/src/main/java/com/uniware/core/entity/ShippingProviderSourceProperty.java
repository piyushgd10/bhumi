package com.uniware.core.entity;


/**
 * @author parijat
 */
public class ShippingProviderSourceProperty implements java.io.Serializable {

    /**
     * 
     */
    private static final long      serialVersionUID = -8699641030604320018L;

    private String            shippingSourceCode;
    private String                 name;
    private String                 value;

    public ShippingProviderSourceProperty() {
    }


    public String getShippingSourceCode() {
        return this.shippingSourceCode;
    }

    public void setShippingSourceCode(String shippingSourceCode) {
        this.shippingSourceCode = shippingSourceCode;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return this.value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
