package com.uniware.core.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.unifier.core.annotation.audit.AuditClass;
import com.unifier.core.annotation.audit.AuditField;

/**
 * SourceParameter generated by hbm2java
 */
@Entity
@Table(name = "channel_connector_parameter")
@AuditClass(identifier = "#{#entity.name}", groupIdentifier = "#{#entity.channelConnector.channel.code}")
public class ChannelConnectorParameter implements java.io.Serializable {

    private static final long serialVersionUID = -890439463901095477L;

    private Integer           id;
    private ChannelConnector  channelConnector;
    private String            name;
    @AuditField(fieldName = "name")
    private String            value;
    private Date              created;
    private Date              updated;

    public ChannelConnectorParameter() {
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "channel_connector_id", nullable = false)
    public ChannelConnector getChannelConnector() {
        return channelConnector;
    }

    public void setChannelConnector(ChannelConnector channelConnector) {
        this.channelConnector = channelConnector;
    }

    @Column(name = "name", nullable = false, length = 256)
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "value")
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created", nullable = false, length = 19)
    public Date getCreated() {
        return this.created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated", nullable = false, length = 19, updatable = false, insertable = false)
    public Date getUpdated() {
        return this.updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ChannelConnectorParameter other = (ChannelConnectorParameter) obj;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        return true;
    }

}
