/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 18-Nov-2014
 *  @author parijat
 */
package com.uniware.core.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author parijat
 */
@Entity
@Table(name = "tax_type_configuration_range")
public class TaxTypeConfigurationRange implements Serializable, Comparable<TaxTypeConfigurationRange> {

    /**
     * 
     */
    private static final long      serialVersionUID   = 4424317960472578735L;

    public static final BigDecimal TAX_RANGE_INFINITY = BigDecimal.valueOf(9999999999.99);

    private Integer                id;
    private TaxTypeConfiguration   taxTypeConfiguration;
    private BigDecimal             vat                = BigDecimal.ZERO;
    private BigDecimal             cst                = BigDecimal.ZERO;
    private BigDecimal             cstFormc           = BigDecimal.ZERO;
    private BigDecimal             additionalTax      = BigDecimal.ZERO;
    private BigDecimal             startPrice         = BigDecimal.ZERO.setScale(2);
    private BigDecimal             endPrice           = TaxTypeConfigurationRange.TAX_RANGE_INFINITY;
    private BigDecimal             centralGst         = BigDecimal.ZERO;
    private BigDecimal             stateGst           = BigDecimal.ZERO;
    private BigDecimal             unionTerritoryGst  = BigDecimal.ZERO;
    private BigDecimal             integratedGst      = BigDecimal.ZERO;
    private BigDecimal             compensationCess   = BigDecimal.ZERO;
    private Date                   created;
    private Date                   updated;

    public TaxTypeConfigurationRange() {

    }

    public TaxTypeConfigurationRange(TaxTypeConfiguration taxTypeConfiguration) {
        this.taxTypeConfiguration = taxTypeConfiguration;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "tax_type_configuration_id", nullable = false)
    public TaxTypeConfiguration getTaxTypeConfiguration() {
        return taxTypeConfiguration;
    }

    public void setTaxTypeConfiguration(TaxTypeConfiguration taxTypeConfiguration) {
        this.taxTypeConfiguration = taxTypeConfiguration;
    }

    @Column(name = "vat", nullable = false, precision = 6)
    public BigDecimal getVat() {
        return this.vat;
    }

    public void setVat(BigDecimal vat) {
        this.vat = vat;
    }

    @Column(name = "cst", nullable = false, precision = 6)
    public BigDecimal getCst() {
        return this.cst;
    }

    public void setCst(BigDecimal cst) {
        this.cst = cst;
    }

    @Column(name = "cst_formc", nullable = false, precision = 6)
    public BigDecimal getCstFormc() {
        return this.cstFormc;
    }

    public void setCstFormc(BigDecimal cstFormc) {
        this.cstFormc = cstFormc;
    }

    @Column(name = "additional_tax", nullable = false, precision = 6)
    public BigDecimal getAdditionalTax() {
        return this.additionalTax;
    }

    public void setAdditionalTax(BigDecimal additionalTax) {
        this.additionalTax = additionalTax;
    }

    @Column(name = "start_price", nullable = false, precision = 6)
    public BigDecimal getStartPrice() {
        return startPrice;
    }

    public void setStartPrice(BigDecimal startPrice) {
        this.startPrice = startPrice;
    }

    @Column(name = "end_price", nullable = false, precision = 6)
    public BigDecimal getEndPrice() {
        return endPrice;
    }

    public void setEndPrice(BigDecimal endPrice) {
        this.endPrice = endPrice;
    }

    @Column(name = "central_gst", precision = 6)
    public BigDecimal getCentralGst() {
        return centralGst;
    }

    public void setCentralGst(BigDecimal centralGst) {
        this.centralGst = centralGst;
    }

    @Column(name = "state_gst", precision = 6)
    public BigDecimal getStateGst() {
        return stateGst;
    }

    public void setStateGst(BigDecimal stateGst) {
        this.stateGst = stateGst;
    }

    @Column(name = "union_territory_gst", precision = 6)
    public BigDecimal getUnionTerritoryGst() {
        return unionTerritoryGst;
    }

    public void setUnionTerritoryGst(BigDecimal unionTerritoryGst) {
        this.unionTerritoryGst = unionTerritoryGst;
    }

    @Column(name = "integrated_gst", precision = 6)
    public BigDecimal getIntegratedGst() {
        return integratedGst;
    }

    public void setIntegratedGst(BigDecimal integratedGst) {
        this.integratedGst = integratedGst;
    }

    @Column(name = "compensation_cess", precision = 6)
    public BigDecimal getCompensationCess() {
        return compensationCess;
    }

    public void setCompensationCess(BigDecimal compensationCess) {
        this.compensationCess = compensationCess;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created", nullable = false, length = 19)
    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated", nullable = false, length = 19, insertable = false, updatable = false)
    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    @Override
    public int compareTo(TaxTypeConfigurationRange o) {
        if (o == null) {
            return 0;
        }
        return this.startPrice.compareTo(o.getStartPrice());
    }

    @Override public String toString() {
        return "TaxTypeConfigurationRange{" +
                "id=" + id +
                ", vat=" + vat +
                ", cst=" + cst +
                ", cstFormc=" + cstFormc +
                ", additionalTax=" + additionalTax +
                ", startPrice=" + startPrice +
                ", endPrice=" + endPrice +
                ", centralGst=" + centralGst +
                ", stateGst=" + stateGst +
                ", unionTerritoryGst=" + unionTerritoryGst +
                ", integratedGst=" + integratedGst +
                ", compensationCess=" + compensationCess +
                '}';
    }
}
