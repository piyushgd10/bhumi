package com.uniware.core.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;


/**
 * @author parijat
 */
@Entity
@Table(name = "shipping_provider_connector_parameter", uniqueConstraints = @UniqueConstraint(columnNames = { "shipping_provider_connector_id", "name" }))
public class ShippingProviderConnectorParameter implements java.io.Serializable {

    /**
     * 
     */
    private static final long         serialVersionUID = 4645148855851994553L;
    private Integer                   id;
    private ShippingProviderConnector shippingProviderConnector;
    private String                    name;
    private String                    value;
    private Date                      created;
    private Date                      updated;

    public ShippingProviderConnectorParameter() {
    }

    public ShippingProviderConnectorParameter(ShippingProviderConnector shippingProviderConnector, String name, Date created, Date updated) {
        this.shippingProviderConnector = shippingProviderConnector;
        this.name = name;
        this.created = created;
        this.updated = updated;
    }

    public ShippingProviderConnectorParameter(ShippingProviderConnector shippingProviderConnector, String name, String value, Date created, Date updated) {
        this.shippingProviderConnector = shippingProviderConnector;
        this.name = name;
        this.value = value;
        this.created = created;
        this.updated = updated;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "shipping_provider_connector_id", nullable = false)
    public ShippingProviderConnector getShippingProviderConnector() {
        return this.shippingProviderConnector;
    }

    public void setShippingProviderConnector(ShippingProviderConnector shippingProviderConnector) {
        this.shippingProviderConnector = shippingProviderConnector;
    }

    @Column(name = "name", nullable = false, length = 256)
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "value", length = 65535)
    public String getValue() {
        return this.value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created", nullable = false, length = 19)
    public Date getCreated() {
        return this.created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated", nullable = false, length = 19)
    public Date getUpdated() {
        return this.updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

}
