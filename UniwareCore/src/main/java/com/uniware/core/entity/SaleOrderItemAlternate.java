package com.uniware.core.entity;

import static javax.persistence.GenerationType.IDENTITY;

import com.unifier.core.entity.User;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * SaleOrderItemAlternate generated by hbm2java
 */
@Entity
@Table(name = "sale_order_item_alternate")
public class SaleOrderItemAlternate implements java.io.Serializable {

    public enum StatusCode {
        CREATED,
        ACCEPTED,
        APPROVED
    }

    /**
     * 
     */
    private static final long                     serialVersionUID                  = -3959856842545090554L;
    private Integer                               id;
    private User                                  user;
    private String                                statusCode;
    private Date                                  created;
    private Date                                  updated;
    private Set<SaleOrderItemAlternateSuggestion> saleOrderItemAlternateSuggestions = new HashSet<SaleOrderItemAlternateSuggestion>(0);

    public SaleOrderItemAlternate() {
    }

    public SaleOrderItemAlternate(User user, String statusCode, Date created, Date updated) {
        this.user = user;
        this.statusCode = statusCode;
        this.created = created;
        this.updated = updated;
    }

    public SaleOrderItemAlternate(User user, String statusCode, Date created, Date updated, Set<SaleOrderItemAlternateSuggestion> saleOrderItemAlternateSuggestions) {
        this.user = user;
        this.statusCode = statusCode;
        this.created = created;
        this.updated = updated;
        this.saleOrderItemAlternateSuggestions = saleOrderItemAlternateSuggestions;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false)
    public User getUser() {
        return this.user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Column(name = "status_code", nullable = false, length = 45)
    public String getStatusCode() {
        return this.statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created", nullable = false, length = 19)
    public Date getCreated() {
        return this.created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated", nullable = false, length = 19, updatable = false, insertable = false)
    public Date getUpdated() {
        return this.updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "saleOrderItemAlternate", cascade = { CascadeType.PERSIST })
    public Set<SaleOrderItemAlternateSuggestion> getSaleOrderItemAlternateSuggestions() {
        return this.saleOrderItemAlternateSuggestions;
    }

    public void setSaleOrderItemAlternateSuggestions(Set<SaleOrderItemAlternateSuggestion> saleOrderItemAlternateSuggestions) {
        this.saleOrderItemAlternateSuggestions = saleOrderItemAlternateSuggestions;
    }

}
