package com.uniware.core.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 * @author parijat
 */
@Entity
@Table(name = "shipping_provider_connector")
public class ShippingProviderConnector implements java.io.Serializable {

    /**
     * 
     */
    private static final long                       serialVersionUID                    = 160065984753240438L;

    public enum Status {
        NOT_CONFIGURED,
        ACTIVE,
        BROKEN
    }

    private Integer                                 id;
    private String                                  shippingSourceConnectorName;
    private ShippingProvider                        shippingProvider;
    private Status                                  statusCode;
    private int                                     failedCount;
    private String                                  errorMessage;
    private Date                                    created;
    private Date                                    updated;
    private Set<ShippingProviderConnectorParameter> shippingProviderConnectorParameters = new HashSet<ShippingProviderConnectorParameter>(0);

    public ShippingProviderConnector() {
    }

    public ShippingProviderConnector(String shippingSourceConnectorName, ShippingProvider shippingProvider, int failedCount, Date created, Date updated) {
        this.shippingSourceConnectorName = shippingSourceConnectorName;
        this.shippingProvider = shippingProvider;
        this.failedCount = failedCount;
        this.created = created;
        this.updated = updated;
    }

    public ShippingProviderConnector(String shippingSourceConnectorName, ShippingProvider shippingProvider, Status statusCode, int failedCount, String errorMessage,
            Date created, Date updated, Set<ShippingProviderConnectorParameter> shippingProviderConnectorParameters) {
        this.shippingSourceConnectorName = shippingSourceConnectorName;
        this.shippingProvider = shippingProvider;
        this.statusCode = statusCode;
        this.failedCount = failedCount;
        this.errorMessage = errorMessage;
        this.created = created;
        this.updated = updated;
        this.shippingProviderConnectorParameters = shippingProviderConnectorParameters;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "shipping_source_connector_name", length = 200)
    public String getShippingSourceConnectorName() {
        return shippingSourceConnectorName;
    }

    public void setShippingSourceConnectorName(String shippingSourceConnectorName) {
        this.shippingSourceConnectorName = shippingSourceConnectorName;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "shipping_provider_id", nullable = false)
    public ShippingProvider getShippingProvider() {
        return this.shippingProvider;
    }

    public void setShippingProvider(ShippingProvider shippingProvider) {
        this.shippingProvider = shippingProvider;
    }

    @Column(name = "status_code", length = 15)
    @Enumerated(EnumType.STRING)
    public Status getStatusCode() {
        return this.statusCode;
    }

    public void setStatusCode(Status statusCode) {
        this.statusCode = statusCode;
    }

    @Column(name = "failed_count", nullable = false)
    public int getFailedCount() {
        return this.failedCount;
    }

    public void setFailedCount(int failedCount) {
        this.failedCount = failedCount;
    }

    @Column(name = "error_message", length = 65535)
    public String getErrorMessage() {
        return this.errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created", nullable = false, length = 19)
    public Date getCreated() {
        return this.created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated", nullable = false, length = 19)
    public Date getUpdated() {
        return this.updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "shippingProviderConnector", cascade = { CascadeType.PERSIST, CascadeType.MERGE })
    public Set<ShippingProviderConnectorParameter> getShippingProviderConnectorParameters() {
        return this.shippingProviderConnectorParameters;
    }

    public void setShippingProviderConnectorParameters(Set<ShippingProviderConnectorParameter> shippingProviderConnectorParameters) {
        this.shippingProviderConnectorParameters = shippingProviderConnectorParameters;
    }

    @Transient
    public void resetCount() {
        setFailedCount(0);
    }

}
