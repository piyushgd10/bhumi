package com.uniware.core.entity;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import com.unifier.core.utils.StringUtils;

/**
 * @author parijat
 */
@Document(collection = "shippingSource")
public class ShippingProviderSource implements java.io.Serializable {

    /**
     *
     */
    public enum Code {
        CUSTOM
    }

    private static final long                   serialVersionUID                     = -4877622487983631402L;

    public static final String                  HANDLER_CLASS                        = "handler.class";
    public static final String                  SCRAPER_SCRIPT                       = "scraper.script";
    public static final String                  TRACKING_NUMBER_ALLOCATION_SCRIPT    = "tracking.number.allocation.script";
    public static final String                  TRACKING_LINK                        = "tracking.link";
    public static final String                  TRACKING_ENABLED                     = "tracking.enabled";
    public static final String                  MAX_TRACKING_NUMBER_PER_REQUEST      = "max.tracking.number.per.request";
    public static final String                  POST_MANIFEST_SCRIPT                 = "post.manifest.script";
    public static final String                  SHIPPING_PAYMENT_ENABLED_METHODS_CSV = "shipping.payment.method";
    public static final String                  AVAILABLE_SERVICEABILITIES           = "available.serviceabilities";
    private static final String                 RESERVED_KEYWORDS_FOR_SHORT_NAME     = "reserved.keywords.for.short.name";

    private String                              id;

    @Indexed(unique = true)
    private String                              code;
    private String                              name;
    private boolean                             enabled;
    private Date                                created;
    private Date                                updated;

    private Set<ShippingSourceConnector>        shippingSourceConnectors             = new HashSet<ShippingSourceConnector>(0);
    private Set<ShippingProviderSourceProperty> shippingSourceProperties     = new HashSet<ShippingProviderSourceProperty>(0);
    private final Map<String, String>           propertyToValueMap                   = new HashMap<>();

    public ShippingProviderSource() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public String getCode() {
        return this.code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isEnabled() {
        return this.enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public Date getCreated() {
        return this.created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return this.updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "shippingProviderSource")
    public Set<ShippingSourceConnector> getShippingSourceConnectors() {
        return this.shippingSourceConnectors;
    }

    public void setShippingSourceConnectors(Set<ShippingSourceConnector> shippingSourceConnectors) {
        this.shippingSourceConnectors = shippingSourceConnectors;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "shippingProviderSource")
    public Set<ShippingProviderSourceProperty> getShippingProviderSourceProperties() {
        return this.shippingSourceProperties;
    }

    public void setShippingProviderSourceProperties(Set<ShippingProviderSourceProperty> shippingProviderSourceProperties) {
        this.shippingSourceProperties = shippingProviderSourceProperties;
    }

    @Transient
    public Map<String, String> getSourcePropertyToValue() {
        return propertyToValueMap;
    }

    public String getPropertyValue(String propertyName) {
        if (propertyToValueMap.get(propertyName) == null) {
            for (ShippingProviderSourceProperty property : getShippingProviderSourceProperties()) {
                propertyToValueMap.put(property.getName(), property.getValue());
            }
        }
        return propertyToValueMap.get(propertyName);
    }

    @Transient
    public String getPropertyValue(String propertyName, String defaultValue) {
        String value = getPropertyValue(propertyName);
        return StringUtils.isBlank(value) ? defaultValue : value;
    }

    @Transient
    public String getHandlerClassName() {
        return getPropertyValue(HANDLER_CLASS);
    }

    @Transient
    public Integer getMaxTrackingNumberPerRequest() {
        return Integer.parseInt(getPropertyValue(MAX_TRACKING_NUMBER_PER_REQUEST, "1"));
    }

    @Transient
    public String getPostManifestScript() {
        return getPropertyValue(POST_MANIFEST_SCRIPT);
    }

    @Transient
    public String getScraperScript() {
        return getPropertyValue(SCRAPER_SCRIPT);
    }

    @Transient
    public String getTrackingAllocationScript() {
        return getPropertyValue(TRACKING_NUMBER_ALLOCATION_SCRIPT);
    }

    @Transient
    public String getTrackingLink() {
        return getPropertyValue(TRACKING_LINK);
    }

    @Transient
    public boolean isTrackingEnabled() {
        return Integer.parseInt(getPropertyValue(TRACKING_ENABLED, "0")) == 1 ? true : false;
    }

    @Transient
    public String getShippingPaymentEnabledMethodsCSV() {
        return getPropertyValue(SHIPPING_PAYMENT_ENABLED_METHODS_CSV);
    }

    @Transient
    public String getAvailableServiceabilitiesCSV() {
        return getPropertyValue(AVAILABLE_SERVICEABILITIES);
    }

    @Transient
    public String getReservedKeywordsForShortName() {
        return getPropertyValue(RESERVED_KEYWORDS_FOR_SHORT_NAME);
    }
}
