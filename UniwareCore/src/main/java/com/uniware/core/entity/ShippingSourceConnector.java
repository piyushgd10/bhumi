package com.uniware.core.entity;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * @author parijat
 */
public class ShippingSourceConnector implements java.io.Serializable {

    private static final long                     serialVersionUID                  = -8574468695446616995L;
    private String                                shippingSourceCode;
    private String                                name;
    private String                                displayName;
    private String                                helpText;
    private int                                   priority;
    private boolean                               requiredInAwbFetch;
    private boolean                               requiredInTracking;
    private String                                verificationScriptName;
    private Date                                  created;
    private Date                                  updated;
    private Set<ShippingSourceConnectorParameter> shippingSourceConnectorParameters = new HashSet<ShippingSourceConnectorParameter>(0);

    public ShippingSourceConnector() {
    }

    public String getShippingSourceCode() {
        return this.shippingSourceCode;
    }

    public void setShippingSourceCode(String shippingProviderSource) {
        this.shippingSourceCode = shippingProviderSource;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDisplayName() {
        return this.displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getHelpText() {
        return this.helpText;
    }

    public void setHelpText(String helpText) {
        this.helpText = helpText;
    }

    public int getPriority() {
        return this.priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public boolean isRequiredInAwbFetch() {
        return this.requiredInAwbFetch;
    }

    public void setRequiredInAwbFetch(boolean requiredInAwbFetch) {
        this.requiredInAwbFetch = requiredInAwbFetch;
    }

    public boolean isRequiredInTracking() {
        return this.requiredInTracking;
    }

    public void setRequiredInTracking(boolean requiredInTracking) {
        this.requiredInTracking = requiredInTracking;
    }

    public String getVerificationScriptName() {
        return this.verificationScriptName;
    }

    public void setVerificationScriptName(String verificationScriptName) {
        this.verificationScriptName = verificationScriptName;
    }

    public Date getCreated() {
        return this.created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return this.updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public Set<ShippingSourceConnectorParameter> getShippingSourceConnectorParameters() {
        return this.shippingSourceConnectorParameters;
    }

    public void setShippingSourceConnectorParameters(Set<ShippingSourceConnectorParameter> shippingSourceConnectorParameters) {
        this.shippingSourceConnectorParameters = shippingSourceConnectorParameters;
    }

}