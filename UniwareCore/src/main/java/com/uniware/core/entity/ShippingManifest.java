package com.uniware.core.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

import com.unifier.core.annotation.Customizable;
import com.unifier.core.entity.User;
import com.uniware.core.entity.ShippingPackage.ShippingManager;

/**
 * ShippingManifest generated by hbm2java
 */
@Entity
@Table(name = "shipping_manifest", uniqueConstraints = @UniqueConstraint(columnNames = { "code", "facility_id" }))
@Customizable(displayName = "Shipping Manifest")
public class ShippingManifest implements java.io.Serializable {
    public enum StatusCode {
        DISCARDED,
        CREATED,
        CLOSED
    }

    /**
     * 
     */
    private static final long         serialVersionUID      = 45486560105401108L;
    private Integer                   id;
    private String                    code;
    private String                    statusCode;
    private String                    shippingProviderCode;
    private String                    shippingProviderName;
    private ShippingMethod            shippingMethod;
    private Channel                   channel;
    private Facility                  facility;
    private String                    comments;
    private User                      user;
    private String                    manifestLink;
    private boolean                   manifestSynced;
    private String                    signatureLink;
    private String                    signeeName;
    private String                    signeeMobile;
    private ShippingManager           shippingManager;
    private Date                      closed;
    private Date                      created;
    private Date                      updated;
    private Set<ShippingManifestItem> shippingManifestItems = new HashSet<ShippingManifestItem>(0);

    public ShippingManifest() {
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "facility_id", nullable = false)
    public Facility getFacility() {
        return this.facility;
    }

    public void setFacility(Facility facility) {
        this.facility = facility;
    }

    @Column(name = "code", unique = true, length = 45)
    public String getCode() {
        return this.code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false)
    public User getUser() {
        return this.user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Column(name = "status_code", nullable = false, length = 45)
    public String getStatusCode() {
        return this.statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    @Column(name = "shipping_provider_code", length = 128)
    public String getShippingProviderCode() {
        return shippingProviderCode;
    }

    public void setShippingProviderCode(String shippingProviderCode) {
        this.shippingProviderCode = shippingProviderCode;
    }

    @Column(name = "shipping_provider_name", length = 128)
    public String getShippingProviderName() {
        return shippingProviderName;
    }

    public void setShippingProviderName(String shippingProviderName) {
        this.shippingProviderName = shippingProviderName;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "shipping_method_id")
    public ShippingMethod getShippingMethod() {
        return this.shippingMethod;
    }

    public void setShippingMethod(ShippingMethod shippingMethod) {
        this.shippingMethod = shippingMethod;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "channel_id", nullable = false)
    public Channel getChannel() {
        return channel;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }

    @Column(name = "comments", length = 100)
    public String getComments() {
        return this.comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "closed", nullable = false, length = 19)
    public Date getClosed() {
        return closed;
    }

    public void setClosed(Date closed) {
        this.closed = closed;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created", nullable = false, length = 19)
    public Date getCreated() {
        return this.created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated", nullable = false, length = 19, insertable = false, updatable = false)
    public Date getUpdated() {
        return this.updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "shippingManifest")
    @OrderBy
    public Set<ShippingManifestItem> getShippingManifestItems() {
        return this.shippingManifestItems;
    }

    public void setShippingManifestItems(Set<ShippingManifestItem> shippingManifestItems) {
        this.shippingManifestItems = shippingManifestItems;
    }

    @Column(name = "manifest_link", length = 255)
    public String getManifestLink() {
        return manifestLink;
    }

    public void setManifestLink(String manifestLink) {
        this.manifestLink = manifestLink;
    }

    @Column(name = "manifest_synced", nullable = false)
    public boolean isManifestSynced() {
        return manifestSynced;
    }

    public void setManifestSynced(boolean manifestSynced) {
        this.manifestSynced = manifestSynced;
    }

    @Transient
    public boolean isThirdPartyShipping() {
        return ShippingManager.CHANNEL.equals(shippingManager);
    }

    @Column(name = "shipping_manager", nullable = false)
    @Enumerated(EnumType.STRING)
    public ShippingManager getShippingManager() {
        return shippingManager;
    }

    @Column(name = "signature_link", length = 255)
    public String getSignatureLink() {
        return signatureLink;
    }

    public void setSignatureLink(String signatureLink) {
        this.signatureLink = signatureLink;
    }

    @Column(name = "signee_name", length = 100)
    public String getSigneeName() {
        return signeeName;
    }

    public void setSigneeName(String signeeName) {
        this.signeeName = signeeName;
    }

    @Column(name = "signee_mobile", length = 50)
    public String getSigneeMobile() {
        return signeeMobile;
    }

    public void setSigneeMobile(String signeeMobile) {
        this.signeeMobile = signeeMobile;
    }

    public void setShippingManager(ShippingManager shippingManager) {
        this.shippingManager = shippingManager;
    }

    @Transient
    public ShippingProvider getShippingProvider() {
        ShippingProvider shippingProvider = new ShippingProvider();
        shippingProvider.setCode(shippingProviderCode);
        shippingProvider.setName(shippingProviderName);
        return shippingProvider;
    }
}
