package com.uniware.core.entity;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

import com.unifier.core.entity.User;

/**
 * Putaway is a process by which we put inventory on shelf. There are 7 different types of putaway at present.
 * See {@link Type} for description of each.
 */
@Entity
@Table(name = "putaway", uniqueConstraints = @UniqueConstraint(columnNames = { "code", "facility_id" }))
public class Putaway implements java.io.Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -1302562534956478405L;
    private Integer           id;
    private Facility          facility;
    private String            code;
    private String            type;
    private User              user;
    private String            statusCode;
    private Date              created;
    private Date              updated;
    private Set<PutawayItem>  putawayItems     = new HashSet<PutawayItem>(0);

    public Putaway() {
    }

    public Putaway(int id, User user, String statusCode, Date created, Date updated) {
        this.id = id;
        this.user = user;
        this.statusCode = statusCode;
        this.created = created;
        this.updated = updated;
    }

    public Putaway(int id, String code, String type, User user, String statusCode, Date created, Date updated, Set<PutawayItem> putawayItems) {
        this.id = id;
        this.code = code;
        this.type = type;
        this.user = user;
        this.statusCode = statusCode;
        this.created = created;
        this.updated = updated;
        this.putawayItems = putawayItems;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "facility_id", nullable = false)
    public Facility getFacility() {
        return this.facility;
    }

    public void setFacility(Facility facility) {
        this.facility = facility;
    }

    @Column(name = "code", unique = true, length = 45)
    public String getCode() {
        return this.code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Column(name = "type", nullable = false, length = 45)
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false)
    public User getUser() {
        return this.user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Column(name = "status_code", nullable = false, length = 45)
    public String getStatusCode() {
        return this.statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created", nullable = false, length = 19)
    public Date getCreated() {
        return this.created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated", nullable = false, length = 19, insertable = false, updatable = false)
    public Date getUpdated() {
        return this.updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "putaway")
    public Set<PutawayItem> getPutawayItems() {
        return this.putawayItems;
    }

    public void setPutawayItems(Set<PutawayItem> putawayItems) {
        this.putawayItems = putawayItems;
    }

    public enum Type {
        PUTAWAY_CANCELLED_ITEM(true),
        PUTAWAY_GRN_ITEM(true),
        PUTAWAY_COURIER_RETURNED_ITEMS(true),
        PUTAWAY_REVERSE_PICKUP_ITEM(true),
        PUTAWAY_GATEPASS_ITEM(true),
        PUTAWAY_INSPECTED_NOT_BAD_ITEM(true),
        PUTAWAY_SHELF_TRANSFER(false),
        PUTAWAY_PICKLIST_ITEM(true),
        PUTAWAY_RECEIVED_RETURNS(false);
        private boolean webOnly;

        Type(boolean webOnly) {
            this.webOnly = webOnly;
        }

        public boolean isWebOnly() {
            return webOnly;
        }
    }

    public enum StatusCode {
        DISCARDED,
        CREATED,
        PENDING,
        COMPLETE
    }

    public enum ActivityType {
        CREATION,
        DISCARD,
        EDIT,
        COMPLETE
    }
}
