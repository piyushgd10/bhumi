/*
 * Copyright 2017 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 11/07/17
 * @author piyush
 */
package com.uniware.core.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.StringUtils;

@Entity
@Table(name = "picklist_item")
public class PicklistItem implements java.io.Serializable {

    private static final long serialVersionUID = 7604845885922992820L;

    public enum StatusCode {
        CREATED,
        SUBMITTED,
        RECEIVED,
        COMPLETE,
        PUTBACK_PENDING,
        PUTBACK_ACCEPTED,
        PUTBACK_COMPLETE,
        INVENTORY_NOT_FOUND
    }

    public enum QCStatus {
        PENDING,
        ACCEPTED,
        REJECTED
    }

    private Integer       id;
    private Picklist      picklist;
    private PickBatch     pickBatch;
    private String        itemCode;
    private String        saleOrderItemCode;
    private String        saleOrderCode;
    private String        shippingPackageCode;
    private String        skuCode;
    private String        shelfCode;
    private String        stagingShelfCode;
    private StatusCode    statusCode   = StatusCode.CREATED;
    private QCStatus      qcStatusCode = QCStatus.PENDING;
    private Date          created;
    private Date          updated;

    public PicklistItem() {
    }

    public PicklistItem(Picklist picklist, SaleOrderItem saleOrderItem) {
        this.picklist = picklist;
        this.saleOrderCode = saleOrderItem.getSaleOrder().getCode();
        this.saleOrderItemCode = saleOrderItem.getCode();
        this.shippingPackageCode = saleOrderItem.getShippingPackage().getCode();
        this.skuCode = saleOrderItem.getItemType().getSkuCode();
        this.created = DateUtils.getCurrentTime();
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "picklist_id")
    public Picklist getPicklist() {
        return picklist;
    }

    public void setPicklist(Picklist picklist) {
        this.picklist = picklist;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "pick_batch_id")
    public PickBatch getPickBatch() {
        return pickBatch;
    }

    public void setPickBatch(PickBatch pickBatch) {
        this.pickBatch = pickBatch;
    }

    @Column(name = "item_code")
    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    @Column(name = "sale_order_item_code", nullable = false)
    public String getSaleOrderItemCode() {
        return saleOrderItemCode;
    }

    public void setSaleOrderItemCode(String saleOrderItemCode) {
        this.saleOrderItemCode = saleOrderItemCode;
    }

    @Column(name = "sale_order_code", nullable = false)
    public String getSaleOrderCode() {
        return saleOrderCode;
    }

    public void setSaleOrderCode(String saleOrderCode) {
        this.saleOrderCode = saleOrderCode;
    }

    @Column(name = "shipping_package_code", nullable = false)
    public String getShippingPackageCode() {
        return shippingPackageCode;
    }

    public void setShippingPackageCode(String shippingPackageCode) {
        this.shippingPackageCode = shippingPackageCode;
    }

    @Column(name = "sku_code", nullable = false)
    public String getSkuCode() {
        return skuCode;
    }

    public void setSkuCode(String skuCode) {
        this.skuCode = skuCode;
    }

    @Column(name = "shelf_code")
    public String getShelfCode() {
        return shelfCode;
    }

    public void setShelfCode(String shelfCode) {
        this.shelfCode = shelfCode;
    }

    @Column(name = "staging_shelf_code")
    public String getStagingShelfCode() {
        return stagingShelfCode;
    }

    public void setStagingShelfCode(String stagingShelfCode) {
        this.stagingShelfCode = stagingShelfCode;
    }

    @Enumerated(EnumType.STRING)
    @Column(name = "status_code", nullable = false)
    public StatusCode getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(StatusCode statusCode) {
        this.statusCode = statusCode;
    }

    @Enumerated(EnumType.STRING)
    @Column(name = "qc_status_code", nullable = false)
    public QCStatus getQcStatusCode() {
        return qcStatusCode;
    }

    public void setQcStatusCode(QCStatus qcStatusCode) {
        this.qcStatusCode = qcStatusCode;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created", nullable = false, length = 19)
    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated", nullable = false, length = 19, insertable = false, updatable = false)
    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    @Transient
    public boolean isAssessed() {
        return StringUtils.equalsAny(getStatusCode(), StatusCode.INVENTORY_NOT_FOUND, StatusCode.PUTBACK_ACCEPTED, StatusCode.PUTBACK_COMPLETE, StatusCode.COMPLETE);
    }

    @Transient
    public boolean isRescannedPutbackItem() {
        return PicklistItem.StatusCode.PUTBACK_PENDING.equals(getStatusCode()) && !PicklistItem.QCStatus.PENDING.equals(getQcStatusCode());
    }

    @Transient
    public boolean isPutbackAcknowledged() {
        return StatusCode.PUTBACK_ACCEPTED.equals(getStatusCode());
    }
}
