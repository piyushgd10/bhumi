package com.uniware.core.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 * Created by harshpal on 2/12/16.
 */
@Entity
@Table(name = "sub_cycle_count")
public class SubCycleCount implements Serializable {
    private Integer    id;
    private String     code;
    private CycleCount cycleCount;
    private String     statusCode;
    private Date       targetCompletion;
    private Date       completed;
    private Date       created;
    private Date       updated;

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "code", unique = true, nullable = false, length = 20)
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cycle_count_id", nullable = false)
    public CycleCount getCycleCount() {
        return cycleCount;
    }

    public void setCycleCount(CycleCount cycleCount) {
        this.cycleCount = cycleCount;
    }

    @Column(name = "status_code", nullable = false, length = 20)
    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "target_completion", nullable = false, length = 19)
    public Date getTargetCompletion() {
        return targetCompletion;
    }

    public void setTargetCompletion(Date targetCompletion) {
        this.targetCompletion = targetCompletion;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "completed", length = 19)
    public Date getCompleted() {
        return completed;
    }

    public void setCompleted(Date completed) {
        this.completed = completed;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created", nullable = false, length = 19)
    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated", nullable = false, length = 19, insertable = false, updatable = false)
    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    @Transient
    public boolean isStatusCreated() {
        return CycleCount.StatusCode.CREATED.name().equals(statusCode);
    }

    @Transient
    public boolean isIncomplete() {
        return !isComplete();
    }

    @Transient
    public boolean isComplete() {
        return CycleCount.StatusCode.COMPLETED.name().equals(statusCode);
    }

    @Override public String toString() {
        return "SubCycleCount{" + "id=" + id + ", code='" + code + '\'' + ", cycleCount=" + cycleCount
                + ", statusCode='" + statusCode + '\'' + ", targetCompletion=" + targetCompletion + ", completed="
                + completed + ", created=" + created + ", updated=" + updated + '}';
    }

    @Override
    public boolean equals(Object that) {
        if (this == that)
            return true;
        if (that == null || SubCycleCount.class != that.getClass())
            return false;
        SubCycleCount subCycleCount = (SubCycleCount) that;
        return id != null || (subCycleCount.getId() == null && id.equals(subCycleCount.id));
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + code.hashCode();
        result = 31 * result + cycleCount.hashCode();
        return result;
    }
}
