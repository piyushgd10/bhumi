package com.uniware.core.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Created by harshpal on 22/04/16.
 */
@Entity
@Table(name = "cycle_count_error_item")
public class CycleCountErrorItem implements Serializable {

    private static final long serialVersionUID = -6174032598507568840L;
    private Integer           id;
    private SubCycleCount     subCycleCount;
    private Shelf shelf;
    private String            itemCode;
    private String            reasonCode;
    private String            comment;
    private Date              created;

    public CycleCountErrorItem() {
    }

    public CycleCountErrorItem(SubCycleCount subCycleCount, Shelf shelf, String itemCode, String reasonCode, String comment, Date created) {
        this.subCycleCount = subCycleCount;
        this.shelf = shelf;
        this.itemCode = itemCode;
        this.reasonCode = reasonCode;
        this.comment = comment;
        this.created = created;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "sub_cycle_count_id", nullable = false)
    public SubCycleCount getSubCycleCount() {
        return subCycleCount;
    }

    public void setSubCycleCount(SubCycleCount subCycleCount) {
        this.subCycleCount = subCycleCount;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "shelf_id", nullable = false)
    public Shelf getShelf() {
        return shelf;
    }

    public void setShelf(Shelf shelf) {
        this.shelf = shelf;
    }

    @Column(name = "item_code", length = 100)
    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    @Column(name = "reason_code", nullable = false)
    public String getReasonCode() {
        return reasonCode;
    }

    public void setReasonCode(String reasonCode) {
        this.reasonCode = reasonCode;
    }

    @Column(name = "comment")
    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created", nullable = false, length = 19)
    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }
}
