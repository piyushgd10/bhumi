package com.uniware.core.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

import com.unifier.core.annotation.Customizable;
import com.unifier.core.utils.StringUtils;

/**
 * ReversePickup generated by hbm2java
 */
@Entity
@Table(name = "reverse_pickup", uniqueConstraints = @UniqueConstraint(columnNames = { "code", "facility_id" }))
@Customizable(displayName = "Reverse Pickup")
public class ReversePickup implements java.io.Serializable {

    /**
     * 
     */
    private static final long           serialVersionUID        = 6924407624039988803L;
    private Integer                     id;
    private ShipmentTracking            shipmentTracking;
    private AddressDetail               shippingAddress;
    private String                      statusCode;
    private SaleOrder                   replacementSaleOrder;
    private SaleOrder                   saleOrder;
    private String                      reversePickupAction;
    private String                      trackingNumber;
    private ShippingProvider            shippingProvider;
    private Facility                    facility;
    private String                      code;
    private Invoice                     returnInvoice;
    private ShippingProvider            redispatchShippingProvider;
    private String                      redispatchTrackingNumber;
    private String                      redispatchReason;
    private String                      redispatchManifestCode;
    private Date                        created;
    private Date                        updated;
    private Set<SaleOrderItem>          saleOrderItems          = new HashSet<SaleOrderItem>(0);
    private Set<ReversePickupAlternate> reversePickupAlternates = new HashSet<ReversePickupAlternate>(0);

    public ReversePickup() {
    }

    public ReversePickup(AddressDetail shippingAddress, String statusCode, SaleOrder saleOrder, String reversePickupAction, Date created, Date updated) {
        this.shippingAddress = shippingAddress;
        this.statusCode = statusCode;
        this.saleOrder = saleOrder;
        this.reversePickupAction = reversePickupAction;
        this.created = created;
        this.updated = updated;
    }

    public ReversePickup(AddressDetail shippingAddress, ShipmentTracking shipmentTracking, String statusCode, SaleOrder saleOrder, String reversePickupAction, String code,
            Date created, Date updated, Set<SaleOrderItem> saleOrderItems) {
        this.shipmentTracking = shipmentTracking;
        this.shippingAddress = shippingAddress;
        this.statusCode = statusCode;
        this.saleOrder = saleOrder;
        this.reversePickupAction = reversePickupAction;
        this.code = code;
        this.created = created;
        this.updated = updated;
        this.saleOrderItems = saleOrderItems;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "facility_id", nullable = false)
    public Facility getFacility() {
        return this.facility;
    }

    public void setFacility(Facility facility) {
        this.facility = facility;
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "shipment_tracking_id")
    public ShipmentTracking getShipmentTracking() {
        return this.shipmentTracking;
    }

    public void setShipmentTracking(ShipmentTracking shipmentTracking) {
        this.shipmentTracking = shipmentTracking;
    }

    @Column(name = "status_code", nullable = false, length = 45)
    public String getStatusCode() {
        return this.statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "shipping_provider_id")
    public ShippingProvider getShippingProvider() {
        return this.shippingProvider;
    }

    public void setShippingProvider(ShippingProvider shippingProvider) {
        this.shippingProvider = shippingProvider;
    }

    @Column(name = "tracking_number", length = 45)
    public String getTrackingNumber() {
        return this.trackingNumber;
    }

    public void setTrackingNumber(String trackingNumber) {
        this.trackingNumber = trackingNumber;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "shipping_address_id")
    public AddressDetail getShippingAddress() {
        return this.shippingAddress;
    }

    public void setShippingAddress(AddressDetail shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "replacement_sale_order_id")
    public SaleOrder getReplacementSaleOrder() {
        return this.replacementSaleOrder;
    }

    public void setReplacementSaleOrder(SaleOrder replacementSaleOrder) {
        this.replacementSaleOrder = replacementSaleOrder;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "sale_order_id", nullable = false)
    public SaleOrder getSaleOrder() {
        return this.saleOrder;
    }

    public void setSaleOrder(SaleOrder saleOrder) {
        this.saleOrder = saleOrder;
    }

    @Column(name = "action_code", nullable = false, length = 45)
    public String getReversePickupAction() {
        return this.reversePickupAction;
    }

    public void setReversePickupAction(String reversePickupAction) {
        this.reversePickupAction = reversePickupAction;
    }

    @Column(name = "code", unique = true, length = 45)
    public String getCode() {
        return this.code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created", nullable = false, length = 19)
    public Date getCreated() {
        return this.created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated", nullable = false, length = 19, insertable = false, updatable = false)
    public Date getUpdated() {
        return this.updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "reversePickup")
    public Set<SaleOrderItem> getSaleOrderItems() {
        return this.saleOrderItems;
    }

    public void setSaleOrderItems(Set<SaleOrderItem> saleOrderItems) {
        this.saleOrderItems = saleOrderItems;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "reversePickup", cascade = { CascadeType.PERSIST })
    public Set<ReversePickupAlternate> getReversePickupAlternates() {
        return this.reversePickupAlternates;
    }

    public void setReversePickupAlternates(Set<ReversePickupAlternate> reversePickupAlternates) {
        this.reversePickupAlternates = reversePickupAlternates;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "redispatch_shipping_provider_id")
    public ShippingProvider getRedispatchShippingProvider() {
        return this.redispatchShippingProvider;
    }

    public void setRedispatchShippingProvider(ShippingProvider redispatchShippingProvider) {
        this.redispatchShippingProvider = redispatchShippingProvider;
    }

    @Column(name = "redispatch_tracking_number", length = 45)
    public String getRedispatchTrackingNumber() {
        return this.redispatchTrackingNumber;
    }

    public void setRedispatchTrackingNumber(String redispatchTrackingNumber) {
        this.redispatchTrackingNumber = redispatchTrackingNumber;
    }

    @Column(name = "redispatch_reason", length = 255)
    public String getRedispatchReason() {
        return this.redispatchReason;
    }

    public void setRedispatchReason(String redispatchReason) {
        this.redispatchReason = redispatchReason;
    }

    @Column(name = "redispatch_manifest_code", length = 45)
    public String getRedispatchManifestCode() {
        return this.redispatchManifestCode;
    }

    public void setRedispatchManifestCode(String redispatchManifestCode) {
        this.redispatchManifestCode = redispatchManifestCode;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "return_invoice_id", unique = true)
    public Invoice getReturnInvoice() {
        return this.returnInvoice;
    }

    public void setReturnInvoice(Invoice invoice) {
        this.returnInvoice = invoice;
    }

    @Transient
    public List<SaleOrderItem> getPendingActionableSaleOrderItems() {
        List<SaleOrderItem> pendingSaleOrderItems = new ArrayList<>();
        for(SaleOrderItem saleOrderItem : getSaleOrderItems()){
            if(!StringUtils.equalsAny(saleOrderItem.getStatusCode(), SaleOrderItem.StatusCode.CANCELLED.name(),
                    SaleOrderItem.StatusCode.REPLACED.name())){
                pendingSaleOrderItems.add(saleOrderItem);
            }
        }
        return pendingSaleOrderItems;
    }

    public enum StatusCode {
        CREATED,
        COMPLETE,
        CANCELLED,
        REDISPATCH_PENDING,
        REDISPATCHED
    }

    public enum Action {
        REPLACE_IMMEDIATELY_EXPECT_RETURN("RIER"),
        REPLACE_IMMEDIATELY_DONT_EXPECT_RETURN("RIDER"),
        WAIT_FOR_RETURN_AND_REPLACE("WAR"),
        WAIT_FOR_RETURN_AND_CANCEL("WAC");

        private String actionCode;

        private Action(String actionCode) {
            this.actionCode = actionCode;
        }

        public static Action valueOfCode(String code) {
            for (Action action : values()) {
                if (action.code().equalsIgnoreCase(code)) {
                    return action;
                }
            }
            return null;
        }

        public String code() {
            return this.actionCode;
        }
    }

}
