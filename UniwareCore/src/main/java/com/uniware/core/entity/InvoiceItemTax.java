/*
 *  Copyright 2017 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *  @version     1.0, July 04, 2017
 *  @author amit
 */

package com.uniware.core.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "invoice_item_tax")
public class InvoiceItemTax implements java.io.Serializable {

    private static final long serialVersionUID = -7890750023189178212L;

    /**
     * On which cost head is this tax applicable
     */
    public enum CostHead {
        SELLING_PRICE,
        SHIPPING_CHARGES,
        GIFT_WRAP_CHARGES,
        SHIPPING_METHOD_CHARGES,
        CASH_ON_DELIVERY_CHARGES
    }

    private Integer     id;
    private InvoiceItem invoiceItem;
    private CostHead    costHead;
    private BigDecimal  integratedGst               = BigDecimal.ZERO;
    private BigDecimal  integratedGstPercentage     = BigDecimal.ZERO;
    private BigDecimal  unionTerritoryGst           = BigDecimal.ZERO;
    private BigDecimal  unionTerritoryGstPercentage = BigDecimal.ZERO;
    private BigDecimal  stateGst                    = BigDecimal.ZERO;
    private BigDecimal  stateGstPercentage          = BigDecimal.ZERO;
    private BigDecimal  centralGst                  = BigDecimal.ZERO;
    private BigDecimal  centralGstPercentage        = BigDecimal.ZERO;
    private BigDecimal  compensationCess            = BigDecimal.ZERO;
    private BigDecimal  compensationCessPercentage  = BigDecimal.ZERO;
    private String      taxTypeCode;
    private BigDecimal  additionalTax               = BigDecimal.ZERO;
    private BigDecimal  additionalTaxPercentage     = BigDecimal.ZERO;
    private BigDecimal  taxPercentage               = BigDecimal.ZERO;
    private BigDecimal  vat                         = BigDecimal.ZERO;
    private BigDecimal  cst                         = BigDecimal.ZERO;
    private BigDecimal  serviceTax                  = BigDecimal.ZERO;

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "invoice_item_id", nullable = false)
    public InvoiceItem getInvoiceItem() {
        return invoiceItem;
    }

    public void setInvoiceItem(InvoiceItem invoiceItem) {
        this.invoiceItem = invoiceItem;
    }

    @Column(name = "cost_head", nullable = false)
    @Enumerated(EnumType.STRING)
    public CostHead getCostHead() {
        return costHead;
    }

    public void setCostHead(CostHead costHead) {
        this.costHead = costHead;
    }

    @Column(name = "integrated_gst", nullable = false, precision = 12)
    public BigDecimal getIntegratedGst() {
        return integratedGst;
    }

    public void setIntegratedGst(BigDecimal integratedGst) {
        this.integratedGst = integratedGst;
    }

    @Column(name = "union_territory_gst", nullable = false, precision = 12)
    public BigDecimal getUnionTerritoryGst() {
        return unionTerritoryGst;
    }

    public void setUnionTerritoryGst(BigDecimal unionTerritoryGst) {
        this.unionTerritoryGst = unionTerritoryGst;
    }

    @Column(name = "state_gst", nullable = false, precision = 12)
    public BigDecimal getStateGst() {
        return stateGst;
    }

    public void setStateGst(BigDecimal stateGst) {
        this.stateGst = stateGst;
    }

    @Column(name = "central_gst", nullable = false, precision = 12)
    public BigDecimal getCentralGst() {
        return centralGst;
    }

    public void setCentralGst(BigDecimal centralGst) {
        this.centralGst = centralGst;
    }

    @Column(name = "compensation_cess", nullable = false, precision = 12)
    public BigDecimal getCompensationCess() {
        return compensationCess;
    }

    public void setCompensationCess(BigDecimal compensationCess) {
        this.compensationCess = compensationCess;
    }

    @Column(name = "service_tax", nullable = false, precision = 12)
    public BigDecimal getServiceTax() {
        return this.serviceTax;
    }

    public void setServiceTax(BigDecimal serviceTax) {
        this.serviceTax = serviceTax;
    }

    @Column(name = "tax_type_code", nullable = false, length = 45)
    public String getTaxTypeCode() {
        return taxTypeCode;
    }

    public void setTaxTypeCode(String taxTypeCode) {
        this.taxTypeCode = taxTypeCode;
    }

    @Column(name = "additional_tax", nullable = false, precision = 12)
    public BigDecimal getAdditionalTax() {
        return this.additionalTax;
    }

    public void setAdditionalTax(BigDecimal additionalTax) {
        this.additionalTax = additionalTax;
    }

    @Column(name = "additional_tax_percentage", nullable = false, precision = 6)
    public BigDecimal getAdditionalTaxPercentage() {
        return this.additionalTaxPercentage;
    }

    public void setAdditionalTaxPercentage(BigDecimal additionalTaxPercentage) {
        this.additionalTaxPercentage = additionalTaxPercentage;
    }

    @Column(name = "integrated_gst_percentage", nullable = false, precision = 12)
    public BigDecimal getIntegratedGstPercentage() {
        return integratedGstPercentage;
    }

    public void setIntegratedGstPercentage(BigDecimal integratedGstPercentage) {
        this.integratedGstPercentage = integratedGstPercentage;
    }

    @Column(name = "union_territory_gst_percentage", nullable = false, precision = 12)
    public BigDecimal getUnionTerritoryGstPercentage() {
        return unionTerritoryGstPercentage;
    }

    public void setUnionTerritoryGstPercentage(BigDecimal unionTerritoryGstPercentage) {
        this.unionTerritoryGstPercentage = unionTerritoryGstPercentage;
    }

    @Column(name = "state_gst_percentage", nullable = false, precision = 12)
    public BigDecimal getStateGstPercentage() {
        return stateGstPercentage;
    }

    public void setStateGstPercentage(BigDecimal stateGstPercentage) {
        this.stateGstPercentage = stateGstPercentage;
    }

    @Column(name = "central_gst_percentage", nullable = false, precision = 12)
    public BigDecimal getCentralGstPercentage() {
        return centralGstPercentage;
    }

    public void setCentralGstPercentage(BigDecimal centralGstPercentage) {
        this.centralGstPercentage = centralGstPercentage;
    }

    @Column(name = "compensation_cess_percentage", nullable = false, precision = 12)
    public BigDecimal getCompensationCessPercentage() {
        return compensationCessPercentage;
    }

    public void setCompensationCessPercentage(BigDecimal compensationCessPercentage) {
        this.compensationCessPercentage = compensationCessPercentage;
    }

    @Column(name = "tax_percentage", nullable = false, precision = 6)
    public BigDecimal getTaxPercentage() {
        return taxPercentage;
    }

    public void setTaxPercentage(BigDecimal taxPercentage) {
        this.taxPercentage = taxPercentage;
    }

    @Column(name = "vat", nullable = false, precision = 12)
    public BigDecimal getVat() {
        return this.vat;
    }

    public void setVat(BigDecimal vat) {
        this.vat = vat;
    }

    @Column(name = "cst", nullable = false, precision = 12)
    public BigDecimal getCst() {
        return this.cst;
    }

    public void setCst(BigDecimal cst) {
        this.cst = cst;
    }

    @Transient
    public BigDecimal getTotalTaxAmount() {
        return (this.getVat()).add(this.getCst()).add(this.getCompensationCess()).add(this.getCentralGst()).add(this.getStateGst()).add(this.getUnionTerritoryGst()).add(
                this.getIntegratedGst()).add(this.getAdditionalTax());
    }
}
