package com.uniware.core.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

/**
 * ShippingPackageType generated by hbm2java
 */
@Entity
@Table(name = "shipping_package_type", uniqueConstraints = @UniqueConstraint(columnNames = { "code", "facility_id" }))
public class ShippingPackageType implements java.io.Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1346631003907586892L;
    private Integer           id;
    private Facility          facility;
    private String            code;
    private int               boxLength;
    private int               boxWidth;
    private int               boxHeight;
    private int               boxWeight;
    private BigDecimal        packingCost      = BigDecimal.ZERO;
    private boolean           enabled;
    private boolean           editable;

    public ShippingPackageType() {
    }

    public ShippingPackageType(String code, int boxLength, int boxWidth, int boxHeight, int boxWeight, boolean enabled, BigDecimal packingCost) {
        this.code = code;
        this.boxLength = boxLength;
        this.boxWidth = boxWidth;
        this.boxHeight = boxHeight;
        this.boxWeight = boxWeight;
        this.enabled = enabled;
        this.packingCost = packingCost;
    }

    /**
     * @param shippingPackageTypeId
     */
    public ShippingPackageType(Integer shippingPackageTypeId) {
        this.id = shippingPackageTypeId;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "facility_id")
    public Facility getFacility() {
        return this.facility;
    }

    public void setFacility(Facility facility) {
        this.facility = facility;
    }

    @Column(name = "code", nullable = false, length = 45)
    public String getCode() {
        return this.code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Column(name = "box_length", nullable = false)
    public int getBoxLength() {
        return this.boxLength;
    }

    public void setBoxLength(int boxLength) {
        this.boxLength = boxLength;
    }

    @Column(name = "box_width", nullable = false)
    public int getBoxWidth() {
        return this.boxWidth;
    }

    public void setBoxWidth(int boxWidth) {
        this.boxWidth = boxWidth;
    }

    @Column(name = "box_height", nullable = false)
    public int getBoxHeight() {
        return this.boxHeight;
    }

    public void setBoxHeight(int boxHeight) {
        this.boxHeight = boxHeight;
    }

    @Column(name = "box_weight", nullable = false)
    public int getBoxWeight() {
        return this.boxWeight;
    }

    public void setBoxWeight(int boxWeight) {
        this.boxWeight = boxWeight;
    }

    @Column(name = "enabled", nullable = false)
    public boolean isEnabled() {
        return this.enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Column(name = "packing_cost", nullable = false)
    public BigDecimal getPackingCost() {
        return packingCost;
    }

    public void setPackingCost(BigDecimal packingCost) {
        this.packingCost = packingCost;
    }

    @Transient
    public int getVolume() {
        return this.boxHeight * this.boxLength * this.boxWidth;
    }

    @Column(name = "editable", nullable = false)
    public boolean isEditable() {
        return this.editable;
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
    }
}
