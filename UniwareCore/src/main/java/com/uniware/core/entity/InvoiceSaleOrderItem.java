package com.uniware.core.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.unifier.core.utils.DateUtils;

/**
 * InvoiceSaleOrderItem generated by hbm2java
 */
@Entity
@Table(name = "invoice_sale_order_item")
public class InvoiceSaleOrderItem implements java.io.Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -7828550023144278212L;
    private Integer           id;
    private InvoiceItem       invoiceItem;
    private SaleOrderItem     saleOrderItem;
    private Date              created;
    private Date              updated;

    public InvoiceSaleOrderItem() {
    }

    public InvoiceSaleOrderItem(InvoiceItem invoiceItem, SaleOrderItem saleOrderItem) {
        this.invoiceItem = invoiceItem;
        this.saleOrderItem = saleOrderItem;
        this.created = DateUtils.getCurrentTime();
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "invoice_item_id", nullable = false)
    public InvoiceItem getInvoiceItem() {
        return invoiceItem;
    }

    public void setInvoiceItem(InvoiceItem invoiceItem) {
        this.invoiceItem = invoiceItem;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "sale_order_item_id", nullable = false)
    public SaleOrderItem getSaleOrderItem() {
        return saleOrderItem;
    }

    public void setSaleOrderItem(SaleOrderItem saleOrderItem) {
        this.saleOrderItem = saleOrderItem;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created", nullable = false, length = 19)
    public Date getCreated() {
        return this.created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated", nullable = false, length = 19, insertable = false, updatable = false)
    public Date getUpdated() {
        return this.updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

}
