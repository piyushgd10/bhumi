package com.uniware.core.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

/**
 * @author parijat
 */
@Entity
@Table(name = "shipping_provider", uniqueConstraints = @UniqueConstraint(columnNames = { "code", "tenant_id" }))
public class ShippingProvider implements java.io.Serializable {

    public enum Code {
        SELF_PICKUP("SELF_PICKUP", "Self Pickup"),
        SELF_STORE("SELF_STORE", "Self Store");
        private String code;
        private String name;

        Code(String code, String name) {
            this.code = code;
            this.name = name;
        }

        public String getCode() {
            return code;
        }

        public String getName() {
            return name;
        }
    }

    public enum SyncStatus {
        ON,
        OFF,
        NOT_AVAILABLE
    }

    public enum ShippingServiceablity {
        GLOBAL_SERVICEABLITY("Any facility to Any Place"),
        LOCATION_AGNOSTIC("Any facility to Selected Pincodes"),
        LIMITED_SERVICEABILITY("This Facility to selected pincodes");

        private String name;

        ShippingServiceablity(String name) {
            this.name = name;
        }

        /**
         * @return the name
         */
        public String getName() {
            return name;
        }

    }

    /**
     *
     */
    private static final long serialVersionUID = 8487949423024363352L;
    private Integer               id;
    private Tenant                tenant;
    private ShippingServiceablity shippingServiceablity;
    private String                shippingProviderSourceCode;
    private String                code;
    private String                name;
    private boolean               enabled;
    private boolean               configured;
    private boolean               hidden;
    private int                   preference;
    private Date                  created;
    private Date                  updated;
    private boolean               integrated;
    private String                shortName;
    private String                colorCode;
    private SyncStatus            trackingSyncStatus;
    private Set<ShippingProviderConnector> shippingProviderConnectors = new HashSet<ShippingProviderConnector>();

    public ShippingProvider() {
    }

    public ShippingProvider(Tenant tenant, ShippingServiceablity shippingServiceablity, String shippingProviderSourceCode, String code, String name, boolean enabled,
            boolean configured, int preference, Date created, Date updated, boolean integrated, String shortName, SyncStatus trackingSyncStatus) {
        this.tenant = tenant;
        this.shippingServiceablity = shippingServiceablity;
        this.shippingProviderSourceCode = shippingProviderSourceCode;
        this.code = code;
        this.name = name;
        this.enabled = enabled;
        this.configured = configured;
        this.preference = preference;
        this.created = created;
        this.updated = updated;
        this.integrated = integrated;
        this.shortName = shortName;
        this.trackingSyncStatus = trackingSyncStatus;
    }

    public ShippingProvider(Tenant tenant, ShippingServiceablity shippingServiceablity, String shippingProviderSourceCode, String code, String name, boolean enabled,
            boolean configured, int preference, Date created, Date updated, boolean integrated, String shortName, String colorCode, SyncStatus trackingSyncStatus,
            Set<ShippingProviderConnector> shippingProviderConnectors) {
        this.tenant = tenant;
        this.shippingServiceablity = shippingServiceablity;
        this.shippingProviderSourceCode = shippingProviderSourceCode;
        this.code = code;
        this.name = name;
        this.enabled = enabled;
        this.configured = configured;
        this.preference = preference;
        this.created = created;
        this.updated = updated;
        this.integrated = integrated;
        this.shortName = shortName;
        this.colorCode = colorCode;
        this.trackingSyncStatus = trackingSyncStatus;
        this.shippingProviderConnectors = shippingProviderConnectors;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "tenant_id", nullable = false)
    public Tenant getTenant() {
        return this.tenant;
    }

    public void setTenant(Tenant tenant) {
        this.tenant = tenant;
    }

    @Column(name = "serviceability", nullable = false, length = 128)
    @Enumerated(EnumType.STRING)
    public ShippingServiceablity getShippingServiceablity() {
        return this.shippingServiceablity;
    }

    public void setShippingServiceablity(ShippingServiceablity shippingServiceablity) {
        this.shippingServiceablity = shippingServiceablity;
    }

    @Column(name = "shipping_source_code", nullable = false, length = 45)
    public String getShippingProviderSourceCode() {
        return shippingProviderSourceCode;
    }

    public void setShippingProviderSourceCode(String shippingProviderSourceCode) {
        this.shippingProviderSourceCode = shippingProviderSourceCode;
    }

    @Column(name = "code", nullable = false, length = 45)
    public String getCode() {
        return this.code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Column(name = "name", nullable = false, length = 45)
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "enabled", nullable = false)
    public boolean isEnabled() {
        return this.enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Column(name = "configured", nullable = false)
    public boolean isConfigured() {
        return this.configured;
    }

    public void setConfigured(boolean configured) {
        this.configured = configured;
    }

    @Column(name = "hidden", nullable = false)
    public boolean isHidden() {
        return hidden;
    }

    public void setHidden(boolean hidden) {
        this.hidden = hidden;
    }

    @Column(name = "preference", nullable = false)
    public int getPreference() {
        return this.preference;
    }

    public void setPreference(int preference) {
        this.preference = preference;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created", nullable = false, length = 19)
    public Date getCreated() {
        return this.created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated", nullable = false, length = 19)
    public Date getUpdated() {
        return this.updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    @Column(name = "integrated", nullable = false)
    public boolean isIntegrated() {
        return this.integrated;
    }

    public void setIntegrated(boolean integrated) {
        this.integrated = integrated;
    }

    @Column(name = "short_name", nullable = false, length = 2)
    public String getShortName() {
        return this.shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    @Column(name = "color_code", length = 20)
    public String getColorCode() {
        return this.colorCode;
    }

    public void setColorCode(String colorCode) {
        this.colorCode = colorCode;
    }

    @Column(name = "tracking_sync_status", nullable = false, length = 14)
    @Enumerated(EnumType.STRING)
    public SyncStatus getTrackingSyncStatus() {
        return this.trackingSyncStatus;
    }

    public void setTrackingSyncStatus(SyncStatus trackingSyncStatus) {
        this.trackingSyncStatus = trackingSyncStatus;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "shippingProvider", cascade = { CascadeType.PERSIST, CascadeType.MERGE })
    public Set<ShippingProviderConnector> getShippingProviderConnectors() {
        return this.shippingProviderConnectors;
    }

    public void setShippingProviderConnectors(Set<ShippingProviderConnector> shippingProviderConnectors) {
        this.shippingProviderConnectors = shippingProviderConnectors;
    }

}
