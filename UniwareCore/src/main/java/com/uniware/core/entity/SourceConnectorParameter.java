package com.uniware.core.entity;

import java.io.Serializable;

public class SourceConnectorParameter implements Serializable {

    public enum Type {
        TEXT("text"),
        PASSWORD("password"),
        HIDDEN("hidden"),
        CHECKBOX("checkbox"),
        READONLY("readonly");
        private String type;

        private Type(String type) {
            this.type = type;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }
    }

    private String sourceConnectorName;
    private String name;
    private String displayName;
    private String displayPlaceHolder;
    private int    priority;
    private Type   type;

    public String getSourceConnectorName() {
        return sourceConnectorName;
    }

    public void setSourceConnectorName(String sourceConnectorName) {
        this.sourceConnectorName = sourceConnectorName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayPlaceHolder() {
        return displayPlaceHolder;
    }

    public void setDisplayPlaceHolder(String displayPlaceHolder) {
        this.displayPlaceHolder = displayPlaceHolder;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }
}
