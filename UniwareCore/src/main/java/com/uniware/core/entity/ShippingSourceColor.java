/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 24-Feb-2015
 *  @author parijat
 */
package com.uniware.core.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * @author parijat
 */
@Entity
@Table(name = "shipping_source_color", uniqueConstraints = @UniqueConstraint(columnNames = { "color" }))
public class ShippingSourceColor implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 7310223190063116625L;

    private Integer           id;
    private String            shippingSourceCode;
    private String            color;
    private Integer           priority;

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "shipping_source_code", nullable = false, length = 45)
    public String getShippingSourceCode() {
        return shippingSourceCode;
    }

    public void setShippingSourceCode(String source) {
        this.shippingSourceCode = source;
    }

    @Column(name = "color", nullable = false, length = 45)
    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Column(name = "priority", nullable = false, length = 256)
    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

}
