/*
 * Copyright 2017 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 11/07/17
 * @author piyush
 */
package com.uniware.core.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import java.util.Date;
import java.util.Set;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "pick_batch")
public class PickBatch {

    private Integer           id;
    private StatusCode        statusCode;
    private Picklist          picklist;
    private PickBucket        pickBucket;
    private Date              created;
    private Date              updated;
    private Set<PicklistItem> picklistItems;

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Enumerated(EnumType.STRING)
    @Column(name = "status_code", nullable = false, length = 45)
    public StatusCode getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(StatusCode statusCode) {
        this.statusCode = statusCode;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "picklist_id")
    public Picklist getPicklist() {
        return picklist;
    }

    public void setPicklist(Picklist picklist) {
        this.picklist = picklist;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "pick_bucket_id", nullable = false)
    public PickBucket getPickBucket() {
        return pickBucket;
    }

    public void setPickBucket(PickBucket pickBucket) {
        this.pickBucket = pickBucket;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created", nullable = false, length = 19)
    public Date getCreated() {
        return this.created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated", nullable = false, length = 19, insertable = false, updatable = false)
    public Date getUpdated() {
        return this.updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "pickBatch", cascade = { CascadeType.PERSIST })
    public Set<PicklistItem> getPicklistItems() {
        return picklistItems;
    }

    public void setPicklistItems(Set<PicklistItem> picklistItems) {
        this.picklistItems = picklistItems;
    }


    @Transient
    public boolean isClosed() {
        return StatusCode.CLOSED.equals(this.statusCode);
    }
    public enum StatusCode {
        PICKING,
        SUBMITTED,
        RECEIVED,
        CLOSED
    }
}
