package com.uniware.core.entity;

import java.sql.Timestamp;
import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

/**
 * ItemTypeInventory generated by hbm2java
 */
@Entity
@Table(name = "item_type_inventory", uniqueConstraints = @UniqueConstraint(columnNames = { "facility_id", "item_type_id", "shelf_id", "type" }))
public class ItemTypeInventory implements java.io.Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 133583774737053176L;
    private Date              ageingStartDate;
    private Integer           id;
    private Facility          facility;
    private Shelf             shelf;
    private ItemType          itemType;
    private Type              type;
    private int               quantity;
    private int               quantityBlocked;
    private int               quantityNotFound;
    private int               quantityDamaged;
    private int               quantityLost;
    private int               priority;
    private Integer           sla;
    private Date              created;
    private Date              updated;

    public ItemTypeInventory() {
    }

    public ItemTypeInventory(Shelf shelf, ItemType itemType, Type inventoryType, Date ageingStartDate, int quantity, int priority, Date created, Date updated) {
        this.shelf = shelf;
        this.itemType = itemType;
        this.type = inventoryType;
        this.ageingStartDate = ageingStartDate;
        this.quantity = quantity;
        this.priority = priority;
        this.created = created;
        this.updated = new Timestamp(updated.getTime());

    }

    public ItemTypeInventory(Shelf shelf, ItemType itemType, Type inventoryType, Date ageingStartDate, int quantity, int priority, Integer sla, Date created, Date updated) {
        this.shelf = shelf;
        this.itemType = itemType;
        this.type = inventoryType;
        this.ageingStartDate = ageingStartDate;
        this.quantity = quantity;
        this.priority = priority;
        this.sla = sla;
        this.created = created;
        this.updated = updated;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "ageing_start_date", length = 10, nullable = false)
    public Date getAgeingStartDate() {
        return ageingStartDate;
    }

    public void setAgeingStartDate(Date ageingStartDate) {
        this.ageingStartDate = ageingStartDate;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "shelf_id", nullable = false)
    public Shelf getShelf() {
        return this.shelf;
    }

    public void setShelf(Shelf shelf) {
        this.shelf = shelf;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "facility_id", nullable = false)
    public Facility getFacility() {
        return this.facility;
    }

    public void setFacility(Facility facility) {
        this.facility = facility;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "item_type_id", nullable = false)
    public ItemType getItemType() {
        return this.itemType;
    }

    public void setItemType(ItemType itemType) {
        this.itemType = itemType;
    }

    @Column(name = "type", nullable = false)
    @Enumerated(EnumType.STRING)
    public Type getType() {
        return this.type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    @Column(name = "quantity", nullable = false)
    public int getQuantity() {
        return this.quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Column(name = "quantity_not_found", nullable = false)
    public int getQuantityNotFound() {
        return this.quantityNotFound;
    }

    public void setQuantityNotFound(int quantityNotFound) {
        this.quantityNotFound = quantityNotFound;
    }

    @Column(name = "quantity_damaged", nullable = false)
    public int getQuantityDamaged() {
        return this.quantityDamaged;
    }

    public void setQuantityDamaged(int quantityDamaged) {
        this.quantityDamaged = quantityDamaged;
    }

    @Column(name = "quantity_lost", nullable = false)
    public int getQuantityLost() {
        return this.quantityLost;
    }

    public void setQuantityLost(int quantityLost) {
        this.quantityLost = quantityLost;
    }

    @Column(name = "priority", nullable = false)
    public int getPriority() {
        return this.priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    @Column(name = "sla", nullable = false)
    public Integer getSla() {
        return sla;
    }

    public void setSla(Integer sla) {
        this.sla = sla;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created", nullable = false, length = 19)
    public Date getCreated() {
        return this.created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated", nullable = false, length = 19, insertable = false, updatable = false)
    public Date getUpdated() {
        return this.updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    @Column(name = "quantity_blocked", nullable = false)
    public int getQuantityBlocked() {
        return quantityBlocked;
    }

    public void setQuantityBlocked(int quantityBlocked) {
        this.quantityBlocked = quantityBlocked;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ItemTypeInventory other = (ItemTypeInventory) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "ItemTypeInventory{" + "id=" + id + ", facility=" + facility + ", shelf=" + shelf + ", itemType=" + itemType + ", type=" + type + ", ageingDate=" + ageingStartDate
                + ", quantity=" + quantity + ", quantityBlocked=" + quantityBlocked + ", quantityNotFound=" + quantityNotFound + ", quantityDamaged=" + quantityDamaged
                + ", quantityLost=" + quantityLost + ", priority=" + priority + ", sla=" + sla + ", created=" + created + ", updated=" + updated + '}';
    }

    /**
     * Contains {@code GOOD_INVENTORY, BAD_INVENTORY, QC_REJECTED, VIRTUAL_INVENTORY}
     */
    public enum Type {
        GOOD_INVENTORY,
        BAD_INVENTORY,
        QC_REJECTED,
        VIRTUAL_INVENTORY
    }
}
