package com.uniware.core.entity;

import java.io.Serializable;
import java.util.List;

public class SourceConfigurationParameter implements Serializable {

    public enum Group {
        GENERAL("general"),
        ORDER("order"),
        INVENTORY("inventory"),
        RECONCILIATION("reconciliation");
        private String group;

        private Group(String group) {
            this.group = group;
        }

        public String getGroup() {
            return group;
        }

        public void setGroup(String group) {
            this.group = group;
        }

    }

    public enum Type {
        TEXT("text"),
        HIDDEN("hidden"),
        CHECKBOX("checkbox"),
        SELECT("select"),
        FORMULA("formula");
        private String type;

        private Type(String type) {
            this.type = type;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }
    }

    private String              sourceCode;
    private String              name;
    private String              displayNameKey;
    private String              displayPlaceHolder;
    private List<PossibleValue> possibleValues;
    private String              defaultValue;
    private boolean             required;
    private Group               groupName;
    private String              tooltipKey;
    private String              javaType;
    private Type                type;

    public static class PossibleValue implements Serializable {
        private String value;
        private String displayValue;

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public String getDisplayValue() {
            return displayValue;
        }

        public void setDisplayValue(String displayValue) {
            this.displayValue = displayValue;
        }
    }

    public SourceConfigurationParameter() {
    }

    public String getSourceCode() {
        return sourceCode;
    }

    public void setSourceCode(String sourceCode) {
        this.sourceCode = sourceCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDisplayNameKey() {
        return displayNameKey;
    }

    public void setDisplayNameKey(String displayNameKey) {
        this.displayNameKey = displayNameKey;
    }

    public String getDisplayPlaceHolder() {
        return displayPlaceHolder;
    }

    public void setDisplayPlaceHolder(String displayPlaceHolder) {
        this.displayPlaceHolder = displayPlaceHolder;
    }

    public List<PossibleValue> getPossibleValues() {
        return possibleValues;
    }

    public void setPossibleValues(List<PossibleValue> possibleValues) {
        this.possibleValues = possibleValues;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public Group getGroupName() {
        return groupName;
    }

    public void setGroupName(Group groupName) {
        this.groupName = groupName;
    }

    public String getTooltipKey() {
        return tooltipKey;
    }

    public void setTooltipKey(String tooltipKey) {
        this.tooltipKey = tooltipKey;
    }

    public String getJavaType() {
        return javaType;
    }

    public void setJavaType(String javaType) {
        this.javaType = javaType;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

}
