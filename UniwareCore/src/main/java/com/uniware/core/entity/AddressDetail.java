package com.uniware.core.entity;

import com.unifier.core.cache.CacheManager;
import com.uniware.core.cache.LocationCache;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import java.util.Date;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * AddressDetail generated by hbm2java
 */
@Entity
@Table(name = "address_detail")
public class AddressDetail implements java.io.Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -7422322271730400689L;
    private Integer           id;
    private Tenant            tenant;
    private String            name;
    private String            addressLine1;
    private String            addressLine2;
    private String            city;
    private String            stateCode;
    private String            countryCode;
    private String            pincode;
    private String            phone;
    private String            email;
    private Date              created;
    private Date              updated;

    public AddressDetail() {
    }

    public AddressDetail(String name, String addressLine1, String city, String stateCode, String countryCode, String pincode, String phone, Date created, Date updated) {
        this.name = name;
        this.addressLine1 = addressLine1;
        this.city = city;
        this.stateCode = stateCode;
        this.countryCode = countryCode;
        this.pincode = pincode;
        this.phone = phone;
        this.created = created;
        this.updated = updated;
    }

    public AddressDetail(String name, String addressLine1, String addressLine2, String city, String stateCode, String countryCode, String pincode, String phone, String email,
            Date created, Date updated) {
        this.name = name;
        this.addressLine1 = addressLine1;
        this.addressLine2 = addressLine2;
        this.city = city;
        this.stateCode = stateCode;
        this.countryCode = countryCode;
        this.pincode = pincode;
        this.phone = phone;
        this.email = email;
        this.created = created;
        this.updated = updated;
    }

    public AddressDetail(String id, String name, String addressLine1, String addressLine2, String city, String stateCode, String countryCode, String pincode, String phone,
            String email, Date created, Date updated) {
        this.id = Integer.valueOf(id);
        this.name = name;
        this.addressLine1 = addressLine1;
        this.addressLine2 = addressLine2;
        this.city = city;
        this.stateCode = stateCode;
        this.countryCode = countryCode;
        this.pincode = pincode;
        this.phone = phone;
        this.email = email;
        this.created = created;
        this.updated = updated;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "tenant_id", nullable = false)
    public Tenant getTenant() {
        return this.tenant;
    }

    public void setTenant(Tenant tenant) {
        this.tenant = tenant;
    }

    @Column(name = "name", nullable = false, length = 100)
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "address_line1", nullable = false, length = 500)
    public String getAddressLine1() {
        return this.addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    @Column(name = "address_line2", length = 500)
    public String getAddressLine2() {
        return this.addressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    @Column(name = "city", nullable = false, length = 100)
    public String getCity() {
        return this.city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Column(name = "state_code", nullable = false, length = 45)
    public String getStateCode() {
        return this.stateCode;
    }

    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }

    @Column(name = "country_code", nullable = false, length = 2)
    public String getCountryCode() {
        return this.countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    @Column(name = "pincode", nullable = false, length = 45)
    public String getPincode() {
        return this.pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    @Column(name = "phone", nullable = false, length = 50)
    public String getPhone() {
        return this.phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Column(name = "email", nullable = false, length = 100)
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created", nullable = false, length = 19)
    public Date getCreated() {
        return this.created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated", nullable = false, length = 19, insertable = false, updatable = false)
    public Date getUpdated() {
        return this.updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    @Transient
    public State getState() {
        State state = CacheManager.getInstance().getCache(LocationCache.class).getStateByCode(this.stateCode, this.countryCode);
        if (state == null) {
            Country country = CacheManager.getInstance().getCache(LocationCache.class).getCountryByCode(this.countryCode);
            state = new State(country, this.stateCode, this.stateCode);
        }
        return state;
    }
}
