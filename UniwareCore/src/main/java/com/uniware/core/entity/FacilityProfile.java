/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 30-Jul-2013
 *  @author Sunny
 */
package com.uniware.core.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Document(collection = "facilityProfile")
public class FacilityProfile implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -1895042443903018863L;

    private String id;
    private String tenantCode;
    private String facilityCode;
    private String sellerCode;
    private String sellerName;
    private String contactEmail;
    private String mobile;
    private String alternatePhone;

    private double[]     position;
    private boolean      isAutoSetupServiceability;
    private boolean      isAutoSetupPickupServiceability;
    private boolean      messageOnOrderCreation;
    private List<String> pickupShippingMethods;
    private List<String> selfShippingMethods;
    private double deliveryRadius = 0.0;
    private double pickupRadius   = 0.0;
    private boolean storePickupEnabled;
    private boolean storeDeliveryEnabled;
    private int dispatchSLA = 24;
    private int deliverySLA = 72;
    private StoreTiming           storeTimings;
    private List<FacilityHoliday> holidays;

    public static class FacilityHoliday implements Serializable {
        private String name;
        private Date   date;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Date getDate() {
            return date;
        }

        public void setDate(Date date) {
            this.date = date;
        }

    }

    public static class StoreTiming implements Serializable {

        private List<DayTiming> dayTimings;

        public List<DayTiming> getDayTimings() {
            return dayTimings;
        }

        public void setDayTimings(List<DayTiming> dayTimings) {
            this.dayTimings = dayTimings;
        }
    }

    public static class DayTiming implements Serializable {
        public enum DayOfWeek {
            MONDAY,
            TUESDAY,
            WEDNESDAY,
            THURSDAY,
            FRIDAY,
            SATURDAY,
            SUNDAY
        }

        private DayOfWeek dayOfWeek;
        private boolean   closed;
        private String    openingTime;
        private String    closingTime;

        public DayTiming() {
        }

        public DayTiming(DayOfWeek dayOfWeek, boolean closed, String openingTime, String closingTime) {
            this.dayOfWeek = dayOfWeek;
            this.closed = closed;
            this.openingTime = openingTime;
            this.closingTime = closingTime;
        }

        public DayOfWeek getDayOfWeek() {
            return dayOfWeek;
        }

        public void setDayOfWeek(DayOfWeek dayOfWeek) {
            this.dayOfWeek = dayOfWeek;
        }

        public boolean isClosed() {
            return closed;
        }

        public void setClosed(boolean closed) {
            this.closed = closed;
        }

        public String getOpeningTime() {
            return openingTime;
        }

        public void setOpeningTime(String openingTime) {
            this.openingTime = openingTime;
        }

        public String getClosingTime() {
            return closingTime;
        }

        public void setClosingTime(String closingTime) {
            this.closingTime = closingTime;
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTenantCode() {
        return tenantCode;
    }

    public void setTenantCode(String tenantCode) {
        this.tenantCode = tenantCode;
    }

    public String getFacilityCode() {
        return facilityCode;
    }

    public void setFacilityCode(String facilityCode) {
        this.facilityCode = facilityCode;
    }

    public String getSellerName() {
        return sellerName;
    }

    public void setSellerName(String sellerName) {
        this.sellerName = sellerName;
    }

    public String getContactEmail() {
        return contactEmail;
    }

    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getAlternatePhone() {
        return alternatePhone;
    }

    public void setAlternatePhone(String alternatePhone) {
        this.alternatePhone = alternatePhone;
    }

    public double[] getPosition() {
        return position;
    }

    public void setPosition(double[] position) {
        this.position = position;
    }

    public int getDispatchSLA() {
        return dispatchSLA;
    }

    public void setDispatchSLA(int dispatchSLA) {
        this.dispatchSLA = dispatchSLA;
    }

    public int getDeliverySLA() {
        return deliverySLA;
    }

    public void setDeliverySLA(int deliverySLA) {
        this.deliverySLA = deliverySLA;
    }

    public boolean isAutoSetupServiceability() {
        return isAutoSetupServiceability;
    }

    public boolean isMessageOnOrderCreation() {
        return messageOnOrderCreation;
    }

    public void setMessageOnOrderCreation(boolean messageOnOrderCreation) {
        this.messageOnOrderCreation = messageOnOrderCreation;
    }

    public void setAutoSetupServiceability(boolean isAutoSetupServiceability) {
        this.isAutoSetupServiceability = isAutoSetupServiceability;
    }

    public List<String> getPickupShippingMethods() {
        return pickupShippingMethods;
    }

    public void setPickupShippingMethods(List<String> pickupShippingMethods) {
        this.pickupShippingMethods = pickupShippingMethods;
    }

    public List<String> getSelfShippingMethods() {
        return selfShippingMethods;
    }

    public void setSelfShippingMethods(List<String> selfShippingMethods) {
        this.selfShippingMethods = selfShippingMethods;
    }

    public double getDeliveryRadius() {
        return deliveryRadius;
    }

    public void setDeliveryRadius(double deliveryRadius) {
        this.deliveryRadius = deliveryRadius;
    }

    public boolean isStorePickupEnabled() {
        return storePickupEnabled;
    }

    public void setStorePickupEnabled(boolean storePickupEnabled) {
        this.storePickupEnabled = storePickupEnabled;
    }

    public boolean isStoreDeliveryEnabled() {
        return storeDeliveryEnabled;
    }

    public void setStoreDeliveryEnabled(boolean storeDeliveryEnabled) {
        this.storeDeliveryEnabled = storeDeliveryEnabled;
    }

    public StoreTiming getStoreTimings() {
        return storeTimings;
    }

    public void setStoreTimings(StoreTiming storeTimings) {
        this.storeTimings = storeTimings;
    }

    public List<FacilityHoliday> getHolidays() {
        return holidays;
    }

    public void setHolidays(List<FacilityHoliday> holidays) {
        this.holidays = holidays;
    }
    public boolean isAutoSetupPickupServiceability() {
        return isAutoSetupPickupServiceability;
    }

    public void setAutoSetupPickupServiceability(boolean isAutoSetupPickupServiceability) {
        this.isAutoSetupPickupServiceability = isAutoSetupPickupServiceability;
    }

    public double getPickupRadius() {
        return pickupRadius;
    }

    public void setPickupRadius(double pickupRadius) {
        this.pickupRadius = pickupRadius;
    }

    public String getSellerCode() {
        return sellerCode;
    }

    public void setSellerCode(String sellerCode) {
        this.sellerCode = sellerCode;
    }
}
