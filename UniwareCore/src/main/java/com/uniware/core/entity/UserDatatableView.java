package com.uniware.core.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

import com.google.gson.annotations.Expose;
import com.unifier.core.entity.User;

/**
 * UserDatatableView generated by hbm2java
 */
@Entity
@Table(name = "user_datatable_view", uniqueConstraints = @UniqueConstraint(columnNames = { "datatable_type", "user_id", "name" }))
public class UserDatatableView implements java.io.Serializable, Comparable<UserDatatableView> {

    /**
     * 
     */
    private static final long serialVersionUID = -8820919354856681245L;

    private Integer           id;
    private User              user;
    private Tenant            tenant;

    @Expose
    private String            datatableType;
    
    private String            accessResourceName;

    @Expose
    private String            name;
    
    @Expose
    private String            code;

    @Expose
    private String            configJson;
    private int               displayOrder     = 10;

    @Expose
    private boolean           systemView;
    private Date              created;
    private Date              updated;

    public UserDatatableView() {
    }

    public UserDatatableView(Tenant tenant, String datatableType, String name, String configJson, Date created, Date updated) {
        this.tenant = tenant;
        this.datatableType = datatableType;
        this.name = name;
        this.configJson = configJson;
        this.created = created;
        this.updated = updated;
    }

    public UserDatatableView(User user, Tenant tenant, String datatableType, String name, String configJson, Date created, Date updated) {
        this.user = user;
        this.tenant = tenant;
        this.datatableType = datatableType;
        this.name = name;
        this.configJson = configJson;
        this.created = created;
        this.updated = updated;
    }
    
    public UserDatatableView(User user, Tenant tenant, String datatableType, String name, String code, String configJson, Date created, Date updated) {
        this.user = user;
        this.tenant = tenant;
        this.datatableType = datatableType;
        this.name = name;
        this.code = code;
        this.configJson = configJson;
        this.created = created;
        this.updated = updated;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    public User getUser() {
        return this.user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "tenant_id", nullable = false)
    public Tenant getTenant() {
        return this.tenant;
    }

    public void setTenant(Tenant tenant) {
        this.tenant = tenant;
    }

    @Column(name = "datatable_type", nullable = false, length = 45)
    public String getDatatableType() {
        return this.datatableType;
    }

    public void setDatatableType(String datatableType) {
        this.datatableType = datatableType;
    }

    @Column(name = "name", nullable = false, length = 45)
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    @Column(name = "access_resource_name", length = 45)
    public String getAccessResourceName() {
        return accessResourceName;
    }

    public void setAccessResourceName(String accessResourceName) {
        this.accessResourceName = accessResourceName;
    }

    @Column(name = "config_json", nullable = false, length = 65535)
    public String getConfigJson() {
        return this.configJson;
    }

    public void setConfigJson(String configJson) {
        this.configJson = configJson;
    }
    
    @Column(name = "code", nullable = false, length = 45)
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created", nullable = false, length = 19)
    public Date getCreated() {
        return this.created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated", nullable = false, length = 19, updatable = false, insertable = false)
    public Date getUpdated() {
        return this.updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    @Column(name = "display_order", nullable = false)
    public int getDisplayOrder() {
        return displayOrder;
    }

    public void setDisplayOrder(int displayOrder) {
        this.displayOrder = displayOrder;
    }

    @Column(name = "system_view", nullable = false)
    public boolean isSystemView() {
        return systemView;
    }

    public void setSystemView(boolean systemView) {
        this.systemView = systemView;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((tenant == null) ? 0 : tenant.getId().hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof UserDatatableView))
            return false;
        UserDatatableView other = (UserDatatableView) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        if (tenant == null) {
            if (other.tenant != null)
                return false;
        } else if (!tenant.getId().equals(other.tenant.getId()))
            return false;
        return true;
    }

    @Override
    public int compareTo(UserDatatableView o) {
        return displayOrder > o.getDisplayOrder() ? 1 : -1;
    }

}
