package com.uniware.core.entity;


/**
 * @author parijat
 */
public class ShippingSourceConnectorParameter implements java.io.Serializable {

    public enum Type {
        TEXT("text"),
        PASSWORD("password"),
        HIDDEN("hidden"),
        CHECKBOX("checkbox"),
        READONLY("readonly"),
        LAST_3_CHARS("last3chars");
        private String type;

        private Type(String type) {
            this.type = type;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }
    }

    /**
     * 
     */
    private static final long serialVersionUID = 7056581919749501958L;
    private String            shippingSourceConnectorName;
    private String            name;
    private String            displayName;
    private String            displayPlaceHolder;
    private Type              type;
    private byte              priority;

    public ShippingSourceConnectorParameter() {

    }

    public String getShippingSourceConnectorName() {
        return this.shippingSourceConnectorName;
    }

    public void setShippingSourceConnectorName(String shippingSourceConnectorName) {
        this.shippingSourceConnectorName = shippingSourceConnectorName;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDisplayName() {
        return this.displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayPlaceHolder() {
        return this.displayPlaceHolder;
    }

    public void setDisplayPlaceHolder(String displayPlaceHolder) {
        this.displayPlaceHolder = displayPlaceHolder;
    }

    public Type getType() {
        return this.type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public byte getPriority() {
        return this.priority;
    }

    public void setPriority(byte priority) {
        this.priority = priority;
    }

}
