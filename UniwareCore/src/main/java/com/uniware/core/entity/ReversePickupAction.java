package com.uniware.core.entity;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * ReversePickupAction generated by hbm2java
 */
@Entity
@Table(name = "reverse_pickup_action", uniqueConstraints = @UniqueConstraint(columnNames = "code"))
public class ReversePickupAction implements java.io.Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 6814415791144591549L;
    private Integer           id;
    private String            code;
    private String            description;

    public ReversePickupAction() {
    }

    public ReversePickupAction(String code) {
        this.code = code;
    }

    public ReversePickupAction(String code, String description) {
        this.code = code;
        this.description = description;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "code", unique = true, nullable = false, length = 45)
    public String getCode() {
        return this.code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Column(name = "description", length = 45)
    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
