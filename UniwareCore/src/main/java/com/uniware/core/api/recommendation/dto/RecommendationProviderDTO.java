/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Nov 25, 2015
 *  @author akshay
 */
package com.uniware.core.api.recommendation.dto;

import java.util.Date;

import com.uniware.core.vo.RecommendationProviderVO;

public class RecommendationProviderDTO {

    private String recommendationProvider;
    private String recommendationType;
    private Date created;
    
    public RecommendationProviderDTO() {
    }
    
    public RecommendationProviderDTO(RecommendationProviderVO rp) {
        this.recommendationProvider = rp.getProvider();
        this.recommendationType = rp.getRecommendationType();
        this.created = rp.getCreated();
    }
    
    public RecommendationProviderDTO(String recommendationType, String recommendationProvider) {
        this.recommendationProvider = recommendationProvider;
        this.recommendationType = recommendationType;
    }

    public String getRecommendationProvider() {
        return recommendationProvider;
    }

    public void setRecommendationProvider(String recommendationProvider) {
        this.recommendationProvider = recommendationProvider;
    }

    public String getRecommendationType() {
        return recommendationType;
    }

    public void setRecommendationType(String recommendationType) {
        this.recommendationType = recommendationType;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

}
