/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 01/05/14
 *  @author amit
 */

package com.uniware.core.api.cms;

import java.util.HashMap;
import java.util.Map;

import com.unifier.core.api.base.ServiceResponse;

public class GetVersionForNamespaceResponse extends ServiceResponse {

    private static final long    serialVersionUID = 3304336091080174909L;

    private Map<String, Integer> nsToVersionMap   = new HashMap<>();

    public GetVersionForNamespaceResponse() {
        super();
    }

    public Map<String, Integer> getNsToVersionMap() {
        return nsToVersionMap;
    }

    public void setNsToVersionMap(Map<String, Integer> nsToVersionMap) {
        this.nsToVersionMap = nsToVersionMap;
    }

    @Override
    public String toString() {
        return "GetVersionForNamespaceResponse{" + "nsToVersionMap=" + nsToVersionMap + '}';
    }
}
