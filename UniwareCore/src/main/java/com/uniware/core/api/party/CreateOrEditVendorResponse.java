/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 21-Oct-2013
 *  @author unicom
 */
package com.uniware.core.api.party;

import com.unifier.core.api.base.ServiceResponse;

public class CreateOrEditVendorResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -507271360058912979L;

}
