/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Nov 25, 2015
 *  @author akshay
 */
package com.uniware.core.api.catalog;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

public class GetListingDetailsRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -7043006481176163249L;
    
    @NotBlank
    private String channelProductId;

    public String getChannelProductId() {
        return channelProductId;
    }

    public void setChannelProductId(String channelProductId) {
        this.channelProductId = channelProductId;
    }
}
