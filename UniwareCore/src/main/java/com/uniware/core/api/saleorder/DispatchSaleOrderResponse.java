package com.uniware.core.api.saleorder;

import com.unifier.core.api.base.ServiceResponse;
import com.uniware.core.api.shipping.ShippingManifestStatusDTO;

/**
 * Created by akshayag on 9/30/15.
 */
public class DispatchSaleOrderResponse extends ServiceResponse {

    private String shippingManifestCode;

    private ShippingManifestStatusDTO shippingManifestStatus;

    public ShippingManifestStatusDTO getShippingManifestStatus() {
        return shippingManifestStatus;
    }

    public void setShippingManifestStatus(ShippingManifestStatusDTO shippingManifestStatus) {
        this.shippingManifestStatus = shippingManifestStatus;
    }

    public String getShippingManifestCode() {
        return shippingManifestCode;
    }

    public void setShippingManifestCode(String shippingManifestCode) {
        this.shippingManifestCode = shippingManifestCode;
    }
}
