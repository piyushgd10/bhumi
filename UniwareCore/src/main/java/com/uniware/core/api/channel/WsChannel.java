/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 18-Apr-2013
 *  @author unicom
 */
package com.uniware.core.api.channel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.customfields.WsCustomFieldValue;
import com.unifier.core.utils.StringUtils;
import com.uniware.core.entity.Channel;
import com.uniware.core.entity.Source.ShipmentLabelAggregationFormat;

/**
 * @author Sunny Agarwal
 */
public class WsChannel {

    @NotBlank
    private String                                       sourceCode;

    @NotBlank
    private String                                       channelName;

    private boolean                                      enabled;
    private boolean                                      thirdPartyShipping;
    private boolean                                      reSyncInventory;
    private String                                       customerCode;
    private String                                       billingPartyCode;

    private String                                       orderSyncStatus;
    private String                                       inventorySyncStatus;
    private String                                       orderReconciliationSyncStatus;
    private String                                       pricingSyncStatus;

    @Length(max = 255)
    private String                                       ledgerName;

    @Valid
    private List<WsChannelConfigurationParameter>        channelConfigurationParameters = new ArrayList<>(0);
    @Valid
    private List<WsCustomFieldValue>                     customFieldValues;
    private Map<String, WsChannelConfigurationParameter> parameterNameToParameter       = new HashMap<>();

    public String getSourceCode() {
        return sourceCode;
    }

    public void setSourceCode(String channelCode) {
        this.sourceCode = channelCode;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public boolean isThirdPartyShipping() {
        return thirdPartyShipping;
    }

    public void setThirdPartyShipping(boolean thirdPartyShipping) {
        this.thirdPartyShipping = thirdPartyShipping;
    }

    public String getLedgerName() {
        return ledgerName;
    }

    public void setLedgerName(String ledgerName) {
        this.ledgerName = ledgerName;
    }

    public boolean isReSyncInventory() {
        return reSyncInventory;
    }

    public void setReSyncInventory(boolean reSyncInventory) {
        this.reSyncInventory = reSyncInventory;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    public String getBillingPartyCode() {
        return billingPartyCode;
    }

    public void setBillingPartyCode(String billingPartyCode) {
        this.billingPartyCode = billingPartyCode;
    }

    public String getOrderSyncStatus() {
        return orderSyncStatus;
    }

    public void setOrderSyncStatus(String orderSyncStatus) {
        this.orderSyncStatus = orderSyncStatus;
    }

    public String getInventorySyncStatus() {
        return inventorySyncStatus;
    }

    public void setInventorySyncStatus(String inventorySyncStatus) {
        this.inventorySyncStatus = inventorySyncStatus;
    }

    public String getOrderReconciliationSyncStatus() {
        return orderReconciliationSyncStatus;
    }

    public void setOrderReconciliationSyncStatus(String orderReconciliationSyncStatus) {
        this.orderReconciliationSyncStatus = orderReconciliationSyncStatus;
    }
    
    public String getPricingSyncStatus() {
        return pricingSyncStatus;
    }

    public void setPricingSyncStatus(String pricingSyncStatus) {
        this.pricingSyncStatus = pricingSyncStatus;
    }

    public List<WsChannelConfigurationParameter> getChannelConfigurationParameters() {
        return channelConfigurationParameters;
    }

    public void setChannelConfigurationParameters(List<WsChannelConfigurationParameter> channelConfigurationParameters) {
        this.channelConfigurationParameters = channelConfigurationParameters;
    }

    public List<WsCustomFieldValue> getCustomFieldValues() {
        return customFieldValues;
    }

    public void setCustomFieldValues(List<WsCustomFieldValue> customFieldValues) {
        this.customFieldValues = customFieldValues;
    }

    // HELPERS
    public Integer getTat() {
        String tatStr = getParameterValue(Channel.TAT);
        return StringUtils.isNotEmpty(tatStr) ? Integer.parseInt(tatStr) : null;
    }

    public boolean isNotificationsEnabled() {
        return Boolean.parseBoolean(getParameterValue(Channel.NOTIFICATIONS_ENABLED));
    }

    public String getInventoryUpdateFormula() {
        return getParameterValue(Channel.INVENTORY_UPDATE_FORMULA);
    }

    public Integer getInventoryAllocationPriority() {
        String priorityStr = getParameterValue(Channel.INVENTORY_ALLOCATION_PRIORITY);
        return StringUtils.isNotEmpty(priorityStr) ? Integer.parseInt(priorityStr) : null;
    }

    public boolean isAllowCombinedManifest() {
        return Boolean.parseBoolean(getParameterValue(Channel.ALLOW_COMBINED_MANIFEST));
    }

    public boolean isAutoVerifyOrders() {
        return Boolean.parseBoolean(getParameterValue(Channel.AUTO_VERIFY_ORDERS));
    }

    public String getPackageType() {
        return getParameterValue(Channel.PACKAGE_TYPE);
    }

    public boolean isProductDelistingEnabled() {
        return Boolean.parseBoolean(getParameterValue(Channel.PRODUCT_DELISTING_ENABLED));
    }

    public boolean isProductRelistingEnabled() {
        return Boolean.parseBoolean(getParameterValue(Channel.PRODUCT_RELISTING_ENABLED));
    }

    public ShipmentLabelAggregationFormat getShippingLabelAggregationFormat() {
        String labelAggregationStr = getParameterValue(Channel.SHIPPING_LABEL_AGGREGATION_FORMAT);
        return StringUtils.isNotEmpty(labelAggregationStr) ? ShipmentLabelAggregationFormat.valueOf(labelAggregationStr) : ShipmentLabelAggregationFormat.NONE;
    }

    public String getShipmentLabelFormat() {
        return getParameterValue(Channel.SHIPMENT_LABEL_FORMAT);
    }

    public String getParameterValue(String parameterName) {
        WsChannelConfigurationParameter param = parameterNameToParameter.get(parameterName);
        if (param != null) {
            return param.getValue();
        } else {
            for (WsChannelConfigurationParameter p : channelConfigurationParameters) {
                parameterNameToParameter.put(p.getName(), p);
            }
            param = parameterNameToParameter.get(parameterName);
        }
        if (param != null) {
            return param.getValue();
        }
        return null;
    }
}
