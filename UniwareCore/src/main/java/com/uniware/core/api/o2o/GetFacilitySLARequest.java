/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 *  @version     1.0, Sep 18, 2015
 *  @author akshay
 */
package com.uniware.core.api.o2o;

import javax.validation.constraints.NotNull;

import com.unifier.core.api.base.ServiceRequest;

public class GetFacilitySLARequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 7156080959403347000L;
    
    @NotNull
    private Integer facilityId;

    public Integer getFacilityId() {
        return facilityId;
    }

    public void setFacilityId(Integer facilityId) {
        this.facilityId = facilityId;
    }
}
