/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jan 12, 2012
 *  @author singla
 */
package com.uniware.core.api.packer;

import java.util.List;

import com.unifier.core.api.base.ServiceResponse;

public class GetPacklistRemainingQuantityResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -5007937678874543614L;

    private List<PickBucketRemainingQuantityDTO> pickBucketRemainingQuantity;

    public List<PickBucketRemainingQuantityDTO> getPickBucketRemainingQuantity() {
        return pickBucketRemainingQuantity;
    }

    public void setPickBucketRemainingQuantity(List<PickBucketRemainingQuantityDTO> pickBucketRemainingQuantity) {
        this.pickBucketRemainingQuantity = pickBucketRemainingQuantity;
    }

    public static class PickBucketRemainingQuantityDTO {

        private String  pickbucketCode;
        private Integer quantity;

        public PickBucketRemainingQuantityDTO(String pickbucketCode, Integer quantity) {
            this.pickbucketCode = pickbucketCode;
            this.quantity = quantity;
        }

        public String getPickbucketCode() {
            return pickbucketCode;
        }

        public void setPickbucketCode(String pickbucketCode) {
            this.pickbucketCode = pickbucketCode;
        }

        public Integer getQuantity() {
            return quantity;
        }

        public void setQuantity(Integer quantity) {
            this.quantity = quantity;
        }
    }
}
