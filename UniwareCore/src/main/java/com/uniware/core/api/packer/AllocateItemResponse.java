/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jan 16, 2012
 *  @author singla
 */
package com.uniware.core.api.packer;

import java.util.List;

import com.unifier.core.api.base.ServiceResponse;
import com.uniware.core.api.item.ItemDetailFieldDTO;

/**
 * @author singla
 */
public class AllocateItemResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long        serialVersionUID = -8266371053142184563L;

    private String                   itemCode;
    private String                   saleOrderItemCode;
    private String                   shippingPackageCode;
    private String                   skuCode;
    private String                   productName;
    private List<ItemDetailFieldDTO> itemDetailFields;

    /**
     * @return the customFields
     */
    public List<ItemDetailFieldDTO> getItemDetailFields() {
        return itemDetailFields;
    }

    /**
     * @param customFields the customFields to set
     */
    public void setItemDetailFields(List<ItemDetailFieldDTO> itemDetailFields) {
        this.itemDetailFields = itemDetailFields;
    }

    public String getShippingPackageCode() {
        return shippingPackageCode;
    }

    public void setShippingPackageCode(String shippingPackageCode) {
        this.shippingPackageCode = shippingPackageCode;
    }
    
    public String getSkuCode() {
        return skuCode;
    }

    public void setSkuCode(String skuCode) {
        this.skuCode = skuCode;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    /**
     * @return the itemCode
     */
    public String getItemCode() {
        return itemCode;
    }

    /**
     * @param itemCode the itemCode to set
     */
    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getSaleOrderItemCode() {
        return saleOrderItemCode;
    }

    public void setSaleOrderItemCode(String saleOrderItemCode) {
        this.saleOrderItemCode = saleOrderItemCode;
    }

}
