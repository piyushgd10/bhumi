/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 12-Jun-2012
 *  @author vibhu
 */
package com.uniware.core.api.inflow;

import com.unifier.core.api.base.ServiceResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * @author vibhu
 */
public class CreateItemLabelsResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -3026049804142564471L;

    private List<String>      itemCodes        = new ArrayList<String>();

    /**
     * @return the itemCodes
     */
    public List<String> getItemCodes() {
        return itemCodes;
    }

    /**
     * @param itemCodes the itemCodes to set
     */
    public void setItemCodes(List<String> itemCodes) {
        this.itemCodes = itemCodes;
    }

    public void addItemCode(String itemCode) {
        this.itemCodes.add(itemCode);
    }

}