/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 25-Mar-2014
 *  @author karunsingla
 */
package com.uniware.core.api.inflow;

import java.math.BigDecimal;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

public class WsVendorInvoiceItem {

    private String     itemSkuCode;

    @NotBlank
    private String     description;

    @NotNull
    private BigDecimal unitPrice;

    private BigDecimal discount                = BigDecimal.ZERO;

    @Min(value = 1)
    @NotNull
    private Integer    quantity;

    private BigDecimal integratedGstPercentage     = BigDecimal.ZERO;

    private BigDecimal unionTerritoryGstPercentage = BigDecimal.ZERO;

    private BigDecimal stateGstPercentage          = BigDecimal.ZERO;

    private BigDecimal centralGstPercentage        = BigDecimal.ZERO;

    private BigDecimal compensationCessPercentage  = BigDecimal.ZERO;

    private BigDecimal taxPercentage               = BigDecimal.ZERO;

    private BigDecimal additionalTaxPercentage = BigDecimal.ZERO;

    public String getItemSkuCode() {
        return itemSkuCode;
    }

    public void setItemSkuCode(String itemSkuCode) {
        this.itemSkuCode = itemSkuCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }

    public int getQuantity() {
        return quantity;
    }

    public BigDecimal getTaxPercentage() {
        return taxPercentage;
    }

    public void setTaxPercentage(BigDecimal taxPercentage) {
        this.taxPercentage = taxPercentage;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getAdditionalTaxPercentage() {
        return additionalTaxPercentage;
    }

    public void setAdditionalTaxPercentage(BigDecimal additionalTaxPercentage) {
        this.additionalTaxPercentage = additionalTaxPercentage;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    public BigDecimal getIntegratedGstPercentage() {
        return integratedGstPercentage;
    }

    public void setIntegratedGstPercentage(BigDecimal integratedGstPercentage) {
        this.integratedGstPercentage = integratedGstPercentage;
    }

    public BigDecimal getUnionTerritoryGstPercentage() {
        return unionTerritoryGstPercentage;
    }

    public void setUnionTerritoryGstPercentage(BigDecimal unionTerritoryGstPercentage) {
        this.unionTerritoryGstPercentage = unionTerritoryGstPercentage;
    }

    public BigDecimal getStateGstPercentage() {
        return stateGstPercentage;
    }

    public void setStateGstPercentage(BigDecimal stateGstPercentage) {
        this.stateGstPercentage = stateGstPercentage;
    }

    public BigDecimal getCentralGstPercentage() {
        return centralGstPercentage;
    }

    public void setCentralGstPercentage(BigDecimal centralGstPercentage) {
        this.centralGstPercentage = centralGstPercentage;
    }

    public BigDecimal getCompensationCessPercentage() {
        return compensationCessPercentage;
    }

    public void setCompensationCessPercentage(BigDecimal compensationCessPercentage) {
        this.compensationCessPercentage = compensationCessPercentage;
    }
}
