package com.uniware.core.api.invoice;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created by akshayag on 9/1/15.
 */
public class InvoiceDTO {
    private String               code;
    private String               displayCode;
    private String               shippingPackageCode;
    private String               reversePickupCode;
    private int                  totalQuantity;
    private String               paymentMode;
    private BigDecimal           sellingPrice          = BigDecimal.ZERO;
    private BigDecimal           subtotal              = BigDecimal.ZERO;
    private BigDecimal           total                 = BigDecimal.ZERO;
    private BigDecimal           vat                   = BigDecimal.ZERO;
    private BigDecimal           cst                   = BigDecimal.ZERO;
    private BigDecimal           integratedGst         = BigDecimal.ZERO;
    private BigDecimal           unionTerritoryGst     = BigDecimal.ZERO;
    private BigDecimal           stateGst              = BigDecimal.ZERO;
    private BigDecimal           centralGst            = BigDecimal.ZERO;
    private BigDecimal           compensationCess      = BigDecimal.ZERO;
    private BigDecimal           additionalTax         = BigDecimal.ZERO;
    private BigDecimal           prepaidAmount         = BigDecimal.ZERO;
    private BigDecimal           storeCredit           = BigDecimal.ZERO;
    private BigDecimal           discount              = BigDecimal.ZERO;
    private BigDecimal           shippingCharges       = BigDecimal.ZERO;
    private BigDecimal           shippingMethodCharges = BigDecimal.ZERO;
    private BigDecimal           cashOnDeliveryCharges = BigDecimal.ZERO;
    private BigDecimal           giftWrapCharges       = BigDecimal.ZERO;
    private Date                 channelCreated;
    private Date                 created;
    private boolean              isReturn;
    private List<InvoiceItemDTO> invoiceItems;

    public String getDisplayCode() {
        return displayCode;
    }

    public void setDisplayCode(String displayCode) {
        this.displayCode = displayCode;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getShippingPackageCode() {
        return shippingPackageCode;
    }

    public void setShippingPackageCode(String shippingPackageCode) {
        this.shippingPackageCode = shippingPackageCode;
    }

    public int getTotalQuantity() {
        return totalQuantity;
    }

    public void setTotalQuantity(int totalQuantity) {
        this.totalQuantity = totalQuantity;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public BigDecimal getSellingPrice() {
        return sellingPrice;
    }

    public void setSellingPrice(BigDecimal sellingPrice) {
        this.sellingPrice = sellingPrice;
    }

    public BigDecimal getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(BigDecimal subtotal) {
        this.subtotal = subtotal;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public BigDecimal getVat() {
        return vat;
    }

    public void setVat(BigDecimal vat) {
        this.vat = vat;
    }

    public BigDecimal getCst() {
        return cst;
    }

    public void setCst(BigDecimal cst) {
        this.cst = cst;
    }

    public BigDecimal getIntegratedGst() {
        return integratedGst;
    }

    public void setIntegratedGst(BigDecimal integratedGst) {
        this.integratedGst = integratedGst;
    }

    public BigDecimal getUnionTerritoryGst() {
        return unionTerritoryGst;
    }

    public void setUnionTerritoryGst(BigDecimal unionTerritoryGst) {
        this.unionTerritoryGst = unionTerritoryGst;
    }

    public BigDecimal getStateGst() {
        return stateGst;
    }

    public void setStateGst(BigDecimal stateGst) {
        this.stateGst = stateGst;
    }

    public BigDecimal getCentralGst() {
        return centralGst;
    }

    public void setCentralGst(BigDecimal centralGst) {
        this.centralGst = centralGst;
    }

    public BigDecimal getCompensationCess() {
        return compensationCess;
    }

    public void setCompensationCess(BigDecimal compensationCess) {
        this.compensationCess = compensationCess;
    }

    public BigDecimal getAdditionalTax() {
        return additionalTax;
    }

    public void setAdditionalTax(BigDecimal additionalTax) {
        this.additionalTax = additionalTax;
    }

    public BigDecimal getPrepaidAmount() {
        return prepaidAmount;
    }

    public void setPrepaidAmount(BigDecimal prepaidAmount) {
        this.prepaidAmount = prepaidAmount;
    }

    public BigDecimal getStoreCredit() {
        return storeCredit;
    }

    public void setStoreCredit(BigDecimal storeCredit) {
        this.storeCredit = storeCredit;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    public BigDecimal getShippingCharges() {
        return shippingCharges;
    }

    public void setShippingCharges(BigDecimal shippingCharges) {
        this.shippingCharges = shippingCharges;
    }

    public BigDecimal getShippingMethodCharges() {
        return shippingMethodCharges;
    }

    public void setShippingMethodCharges(BigDecimal shippingMethodCharges) {
        this.shippingMethodCharges = shippingMethodCharges;
    }

    public BigDecimal getGiftWrapCharges() {
        return giftWrapCharges;
    }

    public void setGiftWrapCharges(BigDecimal giftWrapCharges) {
        this.giftWrapCharges = giftWrapCharges;
    }

    public BigDecimal getCashOnDeliveryCharges() {
        return cashOnDeliveryCharges;
    }

    public void setCashOnDeliveryCharges(BigDecimal cashOnDeliveryCharges) {
        this.cashOnDeliveryCharges = cashOnDeliveryCharges;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public List<InvoiceItemDTO> getInvoiceItems() {
        return invoiceItems;
    }

    public void setInvoiceItems(List<InvoiceItemDTO> invoiceItems) {
        this.invoiceItems = invoiceItems;
    }

    public boolean isReturn() {
        return isReturn;
    }

    public void setReturn(boolean aReturn) {
        isReturn = aReturn;
    }

    public String getReversePickupCode() {
        return reversePickupCode;
    }

    public void setReversePickupCode(String reversePickupCode) {
        this.reversePickupCode = reversePickupCode;
    }

    public Date getChannelCreated() {
        return channelCreated;
    }

    public void setChannelCreated(Date channelCreated) {
        this.channelCreated = channelCreated;
    }
}
