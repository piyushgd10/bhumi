/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 30-Jan-2014
 *  @author akshay
 */
package com.uniware.core.api.party;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.unifier.core.api.base.ServiceRequest;

public class EditVendorDetailsRequest extends ServiceRequest{

    /**
     * 
     */
    private static final long serialVersionUID = -2679458674499196813L;

    @NotNull
    @Valid
    private WsVendor          wsVendor;

    @Valid
    private WsPartyAddress    shippingAddress;

    @Valid
    private WsPartyAddress    billingAddress;

    @Valid
    private List<WsPartyContact> contacts;

    @Valid
    private WsVendorAgreement agreement;


    public WsVendor getWsVendor() {
        return wsVendor;
    }

    public void setWsVendor(WsVendor wsVendor) {
        this.wsVendor = wsVendor;
    }
    
    public WsPartyAddress getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(WsPartyAddress shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    public WsPartyAddress getBillingAddress() {
        return billingAddress;
    }

    public void setBillingAddress(WsPartyAddress billingAddress) {
        this.billingAddress = billingAddress;
    }


    public WsVendorAgreement getAgreement() {
        return agreement;
    }

    public void setAgreement(WsVendorAgreement agreement) {
        this.agreement = agreement;
    }

    public List<WsPartyContact> getContacts() {
        return contacts;
    }

    public void setContacts(List<WsPartyContact> contacts) {
        this.contacts = contacts;
    }

}
