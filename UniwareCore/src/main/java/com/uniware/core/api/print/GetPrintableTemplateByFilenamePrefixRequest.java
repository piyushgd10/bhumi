/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 25-Mar-2014
 *  @author amit
 */
package com.uniware.core.api.print;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotNull;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author amit
 */
public class GetPrintableTemplateByFilenamePrefixRequest extends ServiceRequest {

    private static final long serialVersionUID   = -631420436168218426L;

    @NotNull
    private List<String>      fileNamePrefixList = new ArrayList<String>();

    private String            licenseKey;

    public GetPrintableTemplateByFilenamePrefixRequest() {
        super();
    }

    public GetPrintableTemplateByFilenamePrefixRequest(List<String> fileNamePrefixList) {
        this.fileNamePrefixList = fileNamePrefixList;
    }

    public List<String> getFileNamePrefixList() {
        return fileNamePrefixList;
    }

    public void setFileNamePrefixList(List<String> fileNamePrefixList) {
        this.fileNamePrefixList = fileNamePrefixList;
    }

    public String getLicenseKey() {
        return licenseKey;
    }

    public void setLicenseKey(String licenseKey) {
        this.licenseKey = licenseKey;
    }
}
