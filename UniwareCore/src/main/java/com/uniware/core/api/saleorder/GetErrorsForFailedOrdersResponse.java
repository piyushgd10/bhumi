/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 02-Jun-2014
 *  @author unicom
 */
package com.uniware.core.api.saleorder;

import java.util.ArrayList;
import java.util.List;

import com.unifier.core.api.base.ServiceResponse;

public class GetErrorsForFailedOrdersResponse extends ServiceResponse {

    private static final long           serialVersionUID = -4815681473536983330L;

    private List<SkuMappingErrorDTO>    skuErrors        = new ArrayList<>();

    private List<AddressDetailErrorDTO> addressErrors    = new ArrayList<>();

    public List<SkuMappingErrorDTO> getSkuErrors() {
        return skuErrors;
    }

    public void setSkuErrors(List<SkuMappingErrorDTO> skuErrors) {
        this.skuErrors = skuErrors;
    }

    public List<AddressDetailErrorDTO> getAddressErrors() {
        return addressErrors;
    }

    public void setAddressErrors(List<AddressDetailErrorDTO> addressErrors) {
        this.addressErrors = addressErrors;
    }

}