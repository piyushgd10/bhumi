/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 16/04/14
 *  @author amit
 */

package com.uniware.core.api.cms;

import java.util.Map;

import com.unifier.core.api.base.ServiceResponse;

public class GetContentForNamespaceResponse extends ServiceResponse {

    private static final long   serialVersionUID = 7841627148051704694L;

    private NamespaceContentDTO namespaceContentDTO;

    public NamespaceContentDTO getNamespaceContentDTO() {
        return namespaceContentDTO;
    }

    public void setNamespaceContentDTO(NamespaceContentDTO namespaceContentDTO) {
        this.namespaceContentDTO = namespaceContentDTO;
    }

    public static class NamespaceContentDTO {
        private String              nsIdentifier;
        private Integer             version;
        private Map<String, String> nsContentMap;

        public NamespaceContentDTO() {
            super();
        }

        public NamespaceContentDTO(String nsIdentifier, Integer version, Map<String, String> nsContentMap) {
            this.nsIdentifier = nsIdentifier;
            this.version = version;
            this.nsContentMap = nsContentMap;
        }

        public String getNsIdentifier() {
            return nsIdentifier;
        }

        public void setNsIdentifier(String nsIdentifier) {
            this.nsIdentifier = nsIdentifier;
        }

        public Integer getVersion() {
            return version;
        }

        public void setVersion(Integer version) {
            this.version = version;
        }

        public Map<String, String> getNsContentMap() {
            return nsContentMap;
        }

        public void setNsContentMap(Map<String, String> nsContentMap) {
            this.nsContentMap = nsContentMap;
        }

    }
}
