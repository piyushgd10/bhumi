/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Oct 19, 2015
 *  @author akshay
 */
package com.uniware.core.api.prices;

import com.unifier.core.api.base.ServiceRequest;

public class PushAllPricesOnChannelRequest extends ServiceRequest {

    private static final long serialVersionUID = -7591725873200786611L;
    
    public PushAllPricesOnChannelRequest() {
        
    }
    
    public PushAllPricesOnChannelRequest(String channelCode) {
        this.channelCode = channelCode;
    }

    private boolean forcePush = false;

    private String channelCode;
    
    private boolean localExecution = false;

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    public boolean isForcePush() {
        return forcePush;
    }

    public void setForcePush(boolean forcePush) {
        this.forcePush = forcePush;
    }

    public boolean isLocalExecution() {
        return localExecution;
    }

    public void setLocalExecution(boolean localExecution) {
        this.localExecution = localExecution;
    }
    
}
