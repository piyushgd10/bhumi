package com.uniware.core.api.channel;

import com.unifier.core.api.base.ServiceRequest;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Created by bhuvneshwarkumar on 04/08/15.
 */
public class RelistChannelItemTypeRequest extends ServiceRequest{

    @NotBlank
    private String            channelCode;

    @NotBlank
    private String            channelProductId;

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    public String getChannelProductId() {
        return channelProductId;
    }

    public void setChannelProductId(String channelProductId) {
        this.channelProductId = channelProductId;
    }

}
