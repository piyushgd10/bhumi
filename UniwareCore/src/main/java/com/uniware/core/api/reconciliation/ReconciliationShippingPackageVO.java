/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 07-Jan-2014
 *  @author parijat
 */
package com.uniware.core.api.reconciliation;


/**
 * @author parijat
 *
 */
public class ReconciliationShippingPackageVO {

    private String                         shippingPackageCode;

    /**
     * @return the shippingPackageCode
     */
    public String getShippingPackageCode() {
        return shippingPackageCode;
    }

    /**
     * @param shippingPackageCode the shippingPackageCode to set
     */
    public void setShippingPackageCode(String shippingPackageCode) {
        this.shippingPackageCode = shippingPackageCode;
    }

    @Override
    public String toString() {
        return "ReconciliationShippingPackageVO [shippingPackageCode=" + shippingPackageCode + "]";
    }

}
