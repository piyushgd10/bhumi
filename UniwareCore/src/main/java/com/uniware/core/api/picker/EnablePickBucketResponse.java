/*
 * Copyright 2016 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 19/9/16 5:06 PM
 * @author bhuvneshwarkumar
 */

package com.uniware.core.api.picker;

import com.unifier.core.api.base.ServiceResponse;

/**
 * Created by bhuvneshwarkumar on 19/09/16.
 */
public class EnablePickBucketResponse extends ServiceResponse {
}
