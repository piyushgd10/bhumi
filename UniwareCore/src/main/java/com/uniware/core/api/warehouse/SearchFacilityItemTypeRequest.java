/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 28, 2012
 *  @author Pankaj
 */
package com.uniware.core.api.warehouse;

import com.unifier.core.api.base.ServiceRequest;
import com.unifier.core.pagination.SearchOptions;

/**
 * @author Pankaj
 */
public class SearchFacilityItemTypeRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 3487208997913837644L;

    private String            facilityCode;
    private String            itemTypeKeyword;
    private SearchOptions     searchOptions;

    /**
     * @return the facilityCode
     */
    public String getFacilityCode() {
        return facilityCode;
    }

    /**
     * @param facilityCode the facilityCode to set
     */
    public void setFacilityCode(String facilityCode) {
        this.facilityCode = facilityCode;
    }

    /**
     * @return the itemTypeKeyword
     */
    public String getItemTypeKeyword() {
        return itemTypeKeyword;
    }

    /**
     * @param itemTypeKeyword the itemTypeKeyword to set
     */
    public void setItemTypeKeyword(String itemTypeKeyword) {
        this.itemTypeKeyword = itemTypeKeyword;
    }

    /**
     * @return the searchOptions
     */
    public SearchOptions getSearchOptions() {
        return searchOptions;
    }

    /**
     * @param searchOptions the searchOptions to set
     */
    public void setSearchOptions(SearchOptions searchOptions) {
        this.searchOptions = searchOptions;
    }

}
