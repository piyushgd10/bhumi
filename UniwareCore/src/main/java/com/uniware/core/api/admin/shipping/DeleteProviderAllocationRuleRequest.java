/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 11-Aug-2012
 *  @author praveeng
 */
package com.uniware.core.api.admin.shipping;

import com.unifier.core.api.base.ServiceRequest;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author praveeng
 */
public class DeleteProviderAllocationRuleRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 4711966651042112007L;

    @NotEmpty
    private String            providerAllocationRuleName;

    /**
     * @return the providerAllocationRuleName
     */
    public String getProviderAllocationRuleName() {
        return providerAllocationRuleName;
    }

    /**
     * @param providerAllocationRuleName the providerAllocationRuleName to set
     */
    public void setProviderAllocationRuleName(String providerAllocationRuleName) {
        this.providerAllocationRuleName = providerAllocationRuleName;
    }

}
