/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 11-Apr-2012
 *  @author vibhu
 */
package com.uniware.core.api.purchase;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.unifier.core.api.base.ServiceResponse;
import com.uniware.core.entity.ItemType;

/**
 * @author vibhu
 */
public class GetBackOrderItemsResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long      serialVersionUID = -854614202565543468L;

    private List<BackOrderItemDTO> elements         = new ArrayList<BackOrderItemDTO>();
    private Long                   totalRecords;

    /**
     * @return the elements
     */
    public List<BackOrderItemDTO> getElements() {
        return elements;
    }

    /**
     * @param elements the elements to set
     */
    public void setElements(List<BackOrderItemDTO> elements) {
        this.elements = elements;
    }

    /**
     * @return the totalRecords
     */
    public Long getTotalRecords() {
        return totalRecords;
    }

    /**
     * @param totalRecords the totalRecords to set
     */
    public void setTotalRecords(Long totalRecords) {
        this.totalRecords = totalRecords;
    }

    public static class BackOrderItemDTO {
        private String                           skuCode;
        private String                           name;
        private String                           itemTypeImageUrl;
        private String                           itemTypePageUrl;
        private int                              waitingQuantity;
        private String                           color;
        private String                           brand;
        private String                           size;
        private List<BackOrderVendorItemTypeDTO> vendorItemTypes = new ArrayList<BackOrderVendorItemTypeDTO>();

        public BackOrderItemDTO() {
        }

        /**
         * @param itemType
         */
        public BackOrderItemDTO(ItemType itemType, int quantity) {
            this.itemTypeImageUrl = itemType.getImageUrl();
            this.itemTypePageUrl = itemType.getProductPageUrl();
            this.name = itemType.getName();
            this.skuCode = itemType.getSkuCode();
            this.waitingQuantity = quantity;
            this.brand = itemType.getBrand();
            this.color = itemType.getColor();
            this.size = itemType.getSize();
        }

        /**
         * @return the skuCode
         */
        public String getSkuCode() {
            return skuCode;
        }

        /**
         * @param skuCode the skuCode to set
         */
        public void setSkuCode(String skuCode) {
            this.skuCode = skuCode;
        }

        /**
         * @return the name
         */
        public String getName() {
            return name;
        }

        /**
         * @param name the name to set
         */
        public void setName(String name) {
            this.name = name;
        }

        /**
         * @return the waitingQuantity
         */
        public int getWaitingQuantity() {
            return waitingQuantity;
        }

        /**
         * @param waitingQuantity the waitingQuantity to set
         */
        public void setWaitingQuantity(int waitingQuantity) {
            this.waitingQuantity = waitingQuantity;
        }

        /**
         * @return the vendorItemTypes
         */
        public List<BackOrderVendorItemTypeDTO> getVendorItemTypes() {
            return vendorItemTypes;
        }

        /**
         * @param vendorItemTypes the vendorItemTypes to set
         */
        public void setVendorItemTypes(List<BackOrderVendorItemTypeDTO> vendorItemTypes) {
            this.vendorItemTypes = vendorItemTypes;
        }

        /**
         * @return the itemTypeImageUrl
         */
        public String getItemTypeImageUrl() {
            return itemTypeImageUrl;
        }

        /**
         * @param itemTypeImageUrl the itemTypeImageUrl to set
         */
        public void setItemTypeImageUrl(String itemTypeImageUrl) {
            this.itemTypeImageUrl = itemTypeImageUrl;
        }

        /**
         * @return the itemTypePageUrl
         */
        public String getItemTypePageUrl() {
            return itemTypePageUrl;
        }

        /**
         * @param itemTypePageUrl the itemTypePageUrl to set
         */
        public void setItemTypePageUrl(String itemTypePageUrl) {
            this.itemTypePageUrl = itemTypePageUrl;
        }

        /**
         * @return the color
         */
        public String getColor() {
            return color;
        }

        /**
         * @param color the color to set
         */
        public void setColor(String color) {
            this.color = color;
        }

        /**
         * @return the brand
         */
        public String getBrand() {
            return brand;
        }

        /**
         * @param brand the brand to set
         */
        public void setBrand(String brand) {
            this.brand = brand;
        }

        /**
         * @return the size
         */
        public String getSize() {
            return size;
        }

        /**
         * @param size the size to set
         */
        public void setSize(String size) {
            this.size = size;
        }

    }

    public static class BackOrderVendorItemTypeDTO {

        private Integer    vendorId;
        private String     vendorCode;
        private String     vendorName;
        private String     vendorSkuCode;
        private BigDecimal unitPrice;

        /**
         * @return the vendorId
         */
        public Integer getVendorId() {
            return vendorId;
        }

        /**
         * @param vendorId the vendorId to set
         */
        public void setVendorId(Integer vendorId) {
            this.vendorId = vendorId;
        }

        /**
         * @return the vendorName
         */
        public String getVendorName() {
            return vendorName;
        }

        /**
         * @param vendorName the vendorName to set
         */
        public void setVendorName(String vendorName) {
            this.vendorName = vendorName;
        }

        /**
         * @return the unitPrice
         */
        public BigDecimal getUnitPrice() {
            return unitPrice;
        }

        /**
         * @param unitPrice the unitPrice to set
         */
        public void setUnitPrice(BigDecimal unitPrice) {
            this.unitPrice = unitPrice;
        }

        /**
         * @return the vendorCode
         */
        public String getVendorCode() {
            return vendorCode;
        }

        /**
         * @param vendorCode the vendorCode to set
         */
        public void setVendorCode(String vendorCode) {
            this.vendorCode = vendorCode;
        }

        /**
         * @return the vendorSkuCode
         */
        public String getVendorSkuCode() {
            return vendorSkuCode;
        }

        /**
         * @param vendorSkuCode the vendorSkuCode to set
         */
        public void setVendorSkuCode(String vendorSkuCode) {
            this.vendorSkuCode = vendorSkuCode;
        }

    }

}
