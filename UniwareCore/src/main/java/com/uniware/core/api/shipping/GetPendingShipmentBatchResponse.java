/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 04-Apr-2014
 *  @author karunsingla
 */
package com.uniware.core.api.shipping;

import java.util.List;

import com.unifier.core.api.base.ServiceResponse;

public class GetPendingShipmentBatchResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 5085465603532160022L;

    private List<String>      shipmentBatchCodes;

    public List<String> getShipmentBatchCodes() {
        return shipmentBatchCodes;
    }

    public void setShipmentBatchCodes(List<String> shipmentBatchCodes) {
        this.shipmentBatchCodes = shipmentBatchCodes;
    }
}
