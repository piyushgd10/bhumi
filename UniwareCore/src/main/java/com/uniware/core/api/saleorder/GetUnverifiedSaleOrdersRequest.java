package com.uniware.core.api.saleorder;

import com.unifier.core.api.base.ServiceRequest;

/**
 * Created by akshayag on 9/16/15.
 */
public class GetUnverifiedSaleOrdersRequest extends ServiceRequest {

    private PageRequest pageRequest;

    public PageRequest getPageRequest() {
        return pageRequest;
    }

    public void setPageRequest(PageRequest pageRequest) {
        this.pageRequest = pageRequest;
    }
}
