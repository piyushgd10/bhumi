/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 29-Sep-2014
 *  @author parijat
 */
package com.uniware.core.api.channel;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author parijat
 *
 */
public class SyncChannelReconciliationInvoiceRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 1224069856115810320L;

    @NotBlank
    private String            channelCode;

    public SyncChannelReconciliationInvoiceRequest() {
        super();
    }

    public SyncChannelReconciliationInvoiceRequest(String channelCode) {
        this.channelCode = channelCode;
    }

    /**
     * @return the channelCode
     */
    public String getChannelCode() {
        return channelCode;
    }

    /**
     * @param channelCode the channelCode to set
     */
    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

}
