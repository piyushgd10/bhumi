/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Oct 6, 2012
 *  @author Pankaj
 */
package com.uniware.core.api.purchase;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author Pankaj
 */
public class EmailPurchaseOrderResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 4800759169305881133L;

}
