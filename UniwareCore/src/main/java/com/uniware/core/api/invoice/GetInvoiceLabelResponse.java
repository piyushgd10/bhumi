/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 30-Jun-2014
 *  @author akshay
 */
package com.uniware.core.api.invoice;

import com.unifier.core.api.base.ServiceResponse;

public class GetInvoiceLabelResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private String            invoiceCode;

    private String            invoiceDisplayCode;

    private String            label;

    public String getInvoiceDisplayCode() {
        return invoiceDisplayCode;
    }

    public void setInvoiceDisplayCode(String invoiceDisplayCode) {
        this.invoiceDisplayCode = invoiceDisplayCode;
    }

    public String getInvoiceCode() {
        return invoiceCode;
    }

    public void setInvoiceCode(String invoiceCode) {
        this.invoiceCode = invoiceCode;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

}
