/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Aug 10, 2012
 *  @author singla
 */
package com.uniware.core.api.saleorder;

import com.unifier.core.api.base.ServiceRequest;

import java.math.BigDecimal;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

public class EditSaleOrderItemRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID      = -437542490936423235L;

    @NotBlank
    private String            saleOrderCode;
    @NotBlank
    private String            saleOrderItemCode;
    @NotNull
    private BigDecimal        totalPrice;
    @NotNull
    private BigDecimal        sellingPrice;
    private BigDecimal        discount              = BigDecimal.ZERO;
    private BigDecimal        shippingCharges       = BigDecimal.ZERO;
    private BigDecimal        shippingMethodCharges = BigDecimal.ZERO;
    private BigDecimal        cashOnDeliveryCharges = BigDecimal.ZERO;
    private BigDecimal        giftWrapCharges       = BigDecimal.ZERO;
    private String            voucherCode;
    private BigDecimal        voucherValue          = BigDecimal.ZERO;
    private BigDecimal        storeCredit           = BigDecimal.ZERO;

    /**
     * @return the saleOrderCode
     */
    public String getSaleOrderCode() {
        return saleOrderCode;
    }

    /**
     * @param saleOrderCode the saleOrderCode to set
     */
    public void setSaleOrderCode(String saleOrderCode) {
        this.saleOrderCode = saleOrderCode;
    }

    /**
     * @return the saleOrderItemCode
     */
    public String getSaleOrderItemCode() {
        return saleOrderItemCode;
    }

    /**
     * @param saleOrderItemCode the saleOrderItemCode to set
     */
    public void setSaleOrderItemCode(String saleOrderItemCode) {
        this.saleOrderItemCode = saleOrderItemCode;
    }

    /**
     * @return the totalPrice
     */
    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    /**
     * @param totalPrice the totalPrice to set
     */
    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    /**
     * @return the sellingPrice
     */
    public BigDecimal getSellingPrice() {
        return sellingPrice;
    }

    /**
     * @param sellingPrice the sellingPrice to set
     */
    public void setSellingPrice(BigDecimal sellingPrice) {
        this.sellingPrice = sellingPrice;
    }

    /**
     * @return the voucherCode
     */
    public String getVoucherCode() {
        return voucherCode;
    }

    /**
     * @param voucherCode the voucherCode to set
     */
    public void setVoucherCode(String voucherCode) {
        this.voucherCode = voucherCode;
    }

    /**
     * @return the discount
     */
    public BigDecimal getDiscount() {
        return discount;
    }

    /**
     * @return the shippingCharges
     */
    public BigDecimal getShippingCharges() {
        return shippingCharges;
    }

    /**
     * @return the shippingMethodCharges
     */
    public BigDecimal getShippingMethodCharges() {
        return shippingMethodCharges;
    }

    /**
     * @return the cashOnDeliveryCharges
     */
    public BigDecimal getCashOnDeliveryCharges() {
        return cashOnDeliveryCharges;
    }

    /**
     * @return the giftWrapCharges
     */
    public BigDecimal getGiftWrapCharges() {
        return giftWrapCharges;
    }

    /**
     * @return the voucherValue
     */
    public BigDecimal getVoucherValue() {
        return voucherValue;
    }

    /**
     * @return the storeCredit
     */
    public BigDecimal getStoreCredit() {
        return storeCredit;
    }

    /**
     * @param discount the discount to set
     */
    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    /**
     * @param shippingCharges the shippingCharges to set
     */
    public void setShippingCharges(BigDecimal shippingCharges) {
        this.shippingCharges = shippingCharges;
    }

    /**
     * @param shippingMethodCharges the shippingMethodCharges to set
     */
    public void setShippingMethodCharges(BigDecimal shippingMethodCharges) {
        this.shippingMethodCharges = shippingMethodCharges;
    }

    /**
     * @param cashOnDeliveryCharges the cashOnDeliveryCharges to set
     */
    public void setCashOnDeliveryCharges(BigDecimal cashOnDeliveryCharges) {
        this.cashOnDeliveryCharges = cashOnDeliveryCharges;
    }

    /**
     * @param giftWrapCharges the giftWrapCharges to set
     */
    public void setGiftWrapCharges(BigDecimal giftWrapCharges) {
        this.giftWrapCharges = giftWrapCharges;
    }

    /**
     * @param voucherValue the voucherValue to set
     */
    public void setVoucherValue(BigDecimal voucherValue) {
        this.voucherValue = voucherValue;
    }

    /**
     * @param storeCredit the storeCredit to set
     */
    public void setStoreCredit(BigDecimal storeCredit) {
        this.storeCredit = storeCredit;
    }
}
