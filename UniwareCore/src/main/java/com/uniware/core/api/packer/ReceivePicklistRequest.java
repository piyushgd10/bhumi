package com.uniware.core.api.packer;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

/**
 * Created by karunsingla on 28/06/14.
 */
public class ReceivePicklistRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 4725933798115406275L;

    @NotBlank
    private String            picklistCode;

    private String            destination;

    public String getPicklistCode() {
        return picklistCode;
    }

    public void setPicklistCode(String picklistCode) {
        this.picklistCode = picklistCode;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }
}
