/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Nov 25, 2015
 *  @author akshay
 */
package com.uniware.core.api.recommendation;

import java.math.BigDecimal;

import javax.validation.constraints.Digits;

import org.hibernate.validator.constraints.NotBlank;

public class PriceRecommendationBody extends RecommendationBody {

    private static final long serialVersionUID = 5275488808420174126L;

    @NotBlank
    private String            currencyCode;

    @NotBlank
    private String            channelCode;

    @NotBlank
    private String            channelProductId;
    
    @Digits(integer = 10, fraction = 2, message = "requestJson.max msp value cannot have more than 10 digits and 2 decimal places")
    private BigDecimal        msp;
    @Digits(integer = 10, fraction = 2, message = "requestJson.max mrp value cannot have more than 10 digits and 2 decimal places")
    private BigDecimal        mrp;
    @Digits(integer = 10, fraction = 2, message = "requestJson.max sellingPrice value cannot have more than 10 digits and 2 decimal places")
    private BigDecimal        sellingPrice;
    
    public BigDecimal getMsp() {
        return msp;
    }

    public void setMsp(BigDecimal msp) {
        this.msp = msp;
    }

    public BigDecimal getMrp() {
        return mrp;
    }

    public void setMrp(BigDecimal mrp) {
        this.mrp = mrp;
    }

    public BigDecimal getSellingPrice() {
        return sellingPrice;
    }

    public void setSellingPrice(BigDecimal sellingPrice) {
        this.sellingPrice = sellingPrice;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    public String getChannelProductId() {
        return channelProductId;
    }

    public void setChannelProductId(String channelProductId) {
        this.channelProductId = channelProductId;
    }
}
