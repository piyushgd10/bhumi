/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Nov 16, 2012
 *  @author singla
 */
package com.uniware.core.api.catalog;

import com.unifier.core.api.base.ServiceRequest;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.hibernate.validator.constraints.NotEmpty;

public class CreateOrEditFacilityItemTypeRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long        serialVersionUID = -5620911375650604009L;

    @NotEmpty
    @Valid
    private List<WsFacilityItemType> facilityItemTypes;

    public List<WsFacilityItemType> getFacilityItemTypes() {
        return facilityItemTypes;
    }

    public void setFacilityItemTypes(List<WsFacilityItemType> facilityItemTypes) {
        this.facilityItemTypes = facilityItemTypes;
    }

    public void addFacilityItemType(WsFacilityItemType facilityItemType) {
        if (facilityItemTypes == null) {
            facilityItemTypes = new ArrayList<WsFacilityItemType>();
        }
        facilityItemTypes.add(facilityItemType);
    }

}
