/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Oct 22, 2012
 *  @author Pankaj
 */
package com.uniware.core.api.email;

import java.util.List;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author Sunny
 */
public class SendEmailResponse extends ServiceResponse {

    private static final long serialVersionUID = 6697651353632817219L;

    private List<String>      invalidEmails;

    public List<String> getInvalidEmails() {
        return invalidEmails;
    }

    public void setInvalidEmails(List<String> invalidEmails) {
        this.invalidEmails = invalidEmails;
    }

}
