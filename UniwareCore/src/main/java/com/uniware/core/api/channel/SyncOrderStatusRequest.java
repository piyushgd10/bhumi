/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 03-Jun-2012
 *  @author vibhu
 */
package com.uniware.core.api.channel;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author Sunny
 */
public class SyncOrderStatusRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 1963404117545473225L;

    @NotBlank
    private String            channelCode;

    private Integer           numOfDays        = 60;

    private boolean           syncPendingOrdersOnly;

    public SyncOrderStatusRequest() {
        super();
    }

    public SyncOrderStatusRequest(String channelCode, Integer numberOfDays, boolean syncPendingOrdersOnly) {
        super();
        this.channelCode = channelCode;
        this.numOfDays = numberOfDays;
        this.syncPendingOrdersOnly = syncPendingOrdersOnly;
    }

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    public Integer getNumOfDays() {
        return numOfDays;
    }

    public void setNumOfDays(Integer numOfDays) {
        this.numOfDays = numOfDays;
    }

    public boolean isSyncPendingOrdersOnly() {
        return syncPendingOrdersOnly;
    }

    public void setSyncPendingOrdersOnly(boolean syncPendingOrdersOnly) {
        this.syncPendingOrdersOnly = syncPendingOrdersOnly;
    }

}
