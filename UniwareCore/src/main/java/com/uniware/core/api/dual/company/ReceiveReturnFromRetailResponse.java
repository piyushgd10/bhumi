/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Nov 7, 2012
 *  @author singla
 */
package com.uniware.core.api.dual.company;

import com.unifier.core.api.base.ServiceResponse;

public class ReceiveReturnFromRetailResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 6200457334651823870L;

}
