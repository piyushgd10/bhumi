/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 07-Apr-2014
 *  @author harsh
 */
package com.uniware.core.api.channel;

import java.util.Date;
import java.util.List;

import com.unifier.core.api.base.ServiceResponse;
import com.uniware.core.vo.ChannelInventoryUpdateSnapshotVO;

public class GetChannelInventoryUpdateSnapshotResponse extends ServiceResponse {

    private static final long                 serialVersionUID = -4730601468319587328L;

    private ChannelInventoryUpdateSnapshotDTO inventoryUpdate;

    public ChannelInventoryUpdateSnapshotDTO getInventoryUpdate() {
        return inventoryUpdate;
    }

    public void setInventoryUpdate(ChannelInventoryUpdateSnapshotDTO inventoryUpdate) {
        this.inventoryUpdate = inventoryUpdate;
    }

    public static class ChannelInventoryUpdateSnapshotDTO {
        private String                                                                                id;
        private String                                                                                tenantCode;
        private long                                                                                  calculatedInventory;
        private String                                                                                formula;
        private long                                                                                  pendingInventoryAssessment;
        private long                                                                                  openSale;
        private long                                                                                  inventory;
        private long                                                                                  openPurchase;
        private long                                                                                  putawayPending;
        private long                                                                                  pendingStockTransfer;
        private long                                                                                  vendorInventory;
        private long                                                                                  inventoryBlocked;
        private long                                                                                  unprocessedOrderInventory;
        private int                                                                                   inventoryBlockedOnOtherChannels;
        private int                                                                                   pendencyOnOtherChannels;
        private int                                                                                   totalPendency;
        private int                                                                                   pendency;
        private int                                                                                   blockedInventory;
        private int                                                                                   failedOrderInventory;
        private Integer                                                                               lastInventoryUpdate;
        private List<com.uniware.core.vo.ChannelInventoryUpdateSnapshotVO.ComponentItemTypeInventory> componentItemTypeInventories;
        private Date                                                                                  created;

        public ChannelInventoryUpdateSnapshotDTO() {
        }

        public ChannelInventoryUpdateSnapshotDTO(ChannelInventoryUpdateSnapshotVO cit) {
            this.id = cit.getId();
            this.calculatedInventory = cit.getCalculatedInventory();
            this.pendingInventoryAssessment = cit.getPendingInventoryAssessment();
            this.openSale = cit.getOpenSale();
            this.inventory = cit.getInventory();
            this.openPurchase = cit.getOpenPurchase();
            this.putawayPending = cit.getPutawayPending();
            this.pendency = cit.getPendency();
            this.pendingStockTransfer = cit.getPendingStockTransfer();
            this.vendorInventory = cit.getVendorInventory();
            this.inventoryBlocked = cit.getInventoryBlocked();
            this.inventoryBlockedOnOtherChannels = cit.getInventoryBlockedOnOtherChannels();
            this.pendencyOnOtherChannels = cit.getPendencyOnOtherChannels();
            this.totalPendency = cit.getTotalPendency();
            this.blockedInventory = cit.getBlockedInventory();
            this.failedOrderInventory = cit.getFailedOrderInventory();
            this.unprocessedOrderInventory = cit.getUnprocessedOrderInventory();
            this.lastInventoryUpdate = cit.getLastInventoryUpdate();
            this.componentItemTypeInventories = cit.getComponentItemTypeInventories();
            this.created = cit.getCreated();
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTenantCode() {
            return tenantCode;
        }

        public void setTenantCode(String tenantCode) {
            this.tenantCode = tenantCode;
        }

        public long getCalculatedInventory() {
            return calculatedInventory;
        }

        public void setCalculatedInventory(long calculatedInventory) {
            this.calculatedInventory = calculatedInventory;
        }

        public String getFormula() {
            return formula;
        }

        public void setFormula(String formula) {
            this.formula = formula;
        }

        public long getPendingInventoryAssessment() {
            return pendingInventoryAssessment;
        }

        public void setPendingInventoryAssessment(long pendingInventoryAssessment) {
            this.pendingInventoryAssessment = pendingInventoryAssessment;
        }

        public long getOpenSale() {
            return openSale;
        }

        public void setOpenSale(long openSale) {
            this.openSale = openSale;
        }

        public long getInventory() {
            return inventory;
        }

        public void setInventory(long inventory) {
            this.inventory = inventory;
        }

        public long getOpenPurchase() {
            return openPurchase;
        }

        public void setOpenPurchase(long openPurchase) {
            this.openPurchase = openPurchase;
        }

        public long getPutawayPending() {
            return putawayPending;
        }

        public void setPutawayPending(long putawayPending) {
            this.putawayPending = putawayPending;
        }

        public long getPendingStockTransfer() {
            return pendingStockTransfer;
        }

        public void setPendingStockTransfer(long pendingStockTransfer) {
            this.pendingStockTransfer = pendingStockTransfer;
        }

        public long getVendorInventory() {
            return vendorInventory;
        }

        public void setVendorInventory(long vendorInventory) {
            this.vendorInventory = vendorInventory;
        }

        public long getUnprocessedOrderInventory() {
            return unprocessedOrderInventory;
        }

        public void setUnprocessedOrderInventory(long unprocessedOrderInventory) {
            this.unprocessedOrderInventory = unprocessedOrderInventory;
        }

        public long getInventoryBlocked() {
            return inventoryBlocked;
        }

        public void setInventoryBlocked(long inventoryBlocked) {
            this.inventoryBlocked = inventoryBlocked;
        }

        public int getInventoryBlockedOnOtherChannels() {
            return inventoryBlockedOnOtherChannels;
        }

        public void setInventoryBlockedOnOtherChannels(int inventoryBlockedOnOtherChannels) {
            this.inventoryBlockedOnOtherChannels = inventoryBlockedOnOtherChannels;
        }

        public int getPendencyOnOtherChannels() {
            return pendencyOnOtherChannels;
        }

        public void setPendencyOnOtherChannels(int pendencyOnOtherChannels) {
            this.pendencyOnOtherChannels = pendencyOnOtherChannels;
        }

        public int getTotalPendency() {
            return totalPendency;
        }

        public void setTotalPendency(int totalPendency) {
            this.totalPendency = totalPendency;
        }

        public int getPendency() {
            return pendency;
        }

        public void setPendency(int pendency) {
            this.pendency = pendency;
        }

        public int getBlockedInventory() {
            return blockedInventory;
        }

        public void setBlockedInventory(int blockedInventory) {
            this.blockedInventory = blockedInventory;
        }

        public int getFailedOrderInventory() {
            return failedOrderInventory;
        }

        public void setFailedOrderInventory(int failedOrderInventory) {
            this.failedOrderInventory = failedOrderInventory;
        }

        public Integer getLastInventoryUpdate() {
            return lastInventoryUpdate;
        }

        public void setLastInventoryUpdate(Integer lastInventoryUpdate) {
            this.lastInventoryUpdate = lastInventoryUpdate;
        }

        public List<ChannelInventoryUpdateSnapshotVO.ComponentItemTypeInventory> getComponentItemTypeInventories() {
            return componentItemTypeInventories;
        }

        public void setComponentItemTypeInventories(List<ChannelInventoryUpdateSnapshotVO.ComponentItemTypeInventory> componentItemTypeInventories) {
            this.componentItemTypeInventories = componentItemTypeInventories;
        }

        public Date getCreated() {
            return created;
        }

        public void setCreated(Date created) {
            this.created = created;
        }
    }
}
