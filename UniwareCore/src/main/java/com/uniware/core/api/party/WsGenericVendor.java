/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jul 27, 2012
 *  @author singla
 */
package com.uniware.core.api.party;

import java.util.List;

import javax.validation.Valid;

import com.unifier.core.api.customfields.WsCustomFieldValue;

/**
 * @author singla
 */

public class WsGenericVendor extends WsGenericParty {

    private Integer                  purchaseExpiryPeriod;

    private boolean                  acceptsCForm;

    @Valid
    private List<WsCustomFieldValue> customFieldValues;

    @Valid
    private List<WsVendorAgreement>  vendorAgreements;

    public WsGenericVendor(WsVendor vendor) {
        setCode(vendor.getCode());
        setName(vendor.getName());
        setPan(vendor.getPan());
        setTin(vendor.getTin());
        setCstNumber(vendor.getCstNumber());
        setStNumber(vendor.getStNumber());
        setGstNumber(vendor.getGstNumber());
        setWebsite(vendor.getWebsite());
        setEnabled(vendor.getEnabled());
        setRegisteredDealer(vendor.isRegisteredDealer());
        setTaxExempted(vendor.isTaxExempted());
        this.purchaseExpiryPeriod = vendor.getPurchaseExpiryPeriod();
        this.acceptsCForm = vendor.isAcceptsCForm();
        this.customFieldValues = vendor.getCustomFieldValues();
    }

    public WsGenericVendor() {

    }

    public Integer getPurchaseExpiryPeriod() {
        return purchaseExpiryPeriod;
    }

    public void setPurchaseExpiryPeriod(Integer purchaseExpiryPeriod) {
        this.purchaseExpiryPeriod = purchaseExpiryPeriod;
    }

    public boolean isAcceptsCForm() {
        return acceptsCForm;
    }

    public void setAcceptsCForm(boolean acceptsCForm) {
        this.acceptsCForm = acceptsCForm;
    }

    public List<WsCustomFieldValue> getCustomFieldValues() {
        return customFieldValues;
    }

    public void setCustomFieldValues(List<WsCustomFieldValue> customFieldValues) {
        this.customFieldValues = customFieldValues;
    }

    public List<WsVendorAgreement> getVendorAgreements() {
        return vendorAgreements;
    }

    public void setVendorAgreements(List<WsVendorAgreement> vendorAgreements) {
        this.vendorAgreements = vendorAgreements;
    }
    
}
