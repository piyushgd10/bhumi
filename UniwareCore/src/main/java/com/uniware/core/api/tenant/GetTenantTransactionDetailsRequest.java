/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 07-May-2014
 *  @author akshay
 */
package com.uniware.core.api.tenant;

import javax.validation.constraints.NotNull;

import com.unifier.core.api.base.ServiceRequest;
import com.unifier.core.utils.DateUtils.DateRange;

public class GetTenantTransactionDetailsRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @NotNull
    private String            tenantCode;

    @NotNull
    private DateRange         daterange;

    @NotNull
    private Integer           perOrderItems;

    @NotNull
    private String            transactionType;

    public String getTenantCode() {
        return tenantCode;
    }

    public void setTenantCode(String tenantCode) {
        this.tenantCode = tenantCode;
    }

    public DateRange getDaterange() {
        return daterange;
    }

    public void setDaterange(DateRange daterange) {
        this.daterange = daterange;
    }

    public Integer getPerOrderItems() {
        return perOrderItems;
    }

    public void setPerOrderItems(Integer perOrderItems) {
        this.perOrderItems = perOrderItems;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

}
