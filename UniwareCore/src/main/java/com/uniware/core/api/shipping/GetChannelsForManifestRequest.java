/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 7, 2012
 *  @author singla
 */
package com.uniware.core.api.shipping;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author Sunny
 */
public class GetChannelsForManifestRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 5090362284101726468L;

}
