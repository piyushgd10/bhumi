/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 25-Mar-2014
 *  @author karunsingla
 */
package com.uniware.core.api.inflow;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

public class CreateDebitForCreditVendorInvoiceRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -7443054773127393570L;

    @NotBlank
    private String            creditVendorInvoiceCode;

    private boolean           ignorePriceDiscrepancy;

    private boolean           addQCRejectedItems;

    @NotNull
    private Integer           userId;

    public String getCreditVendorInvoiceCode() {
        return creditVendorInvoiceCode;
    }

    public void setCreditVendorInvoiceCode(String creditVendorInvoiceCode) {
        this.creditVendorInvoiceCode = creditVendorInvoiceCode;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public boolean isIgnorePriceDiscrepancy() {
        return ignorePriceDiscrepancy;
    }

    public void setIgnorePriceDiscrepancy(boolean ignorePriceDiscrepancy) {
        this.ignorePriceDiscrepancy = ignorePriceDiscrepancy;
    }

    public boolean isAddQCRejectedItems() {
        return addQCRejectedItems;
    }

    public void setAddQCRejectedItems(boolean addQCRejectedItems) {
        this.addQCRejectedItems = addQCRejectedItems;
    }
}
