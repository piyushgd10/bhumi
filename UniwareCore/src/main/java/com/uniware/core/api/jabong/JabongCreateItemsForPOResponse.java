/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jul 4, 2012
 *  @author singla
 */
package com.uniware.core.api.jabong;

import com.unifier.core.api.base.ServiceResponse;

import java.util.ArrayList;
import java.util.List;

import com.uniware.core.entity.Item;

public class JabongCreateItemsForPOResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long   serialVersionUID = 9093288963623918324L;
    private List<JabongItemDTO> items            = new ArrayList<JabongItemDTO>();

    /**
     * @return the items
     */
    public List<JabongItemDTO> getItems() {
        return items;
    }

    /**
     * @param items the items to set
     */
    public void setItems(List<JabongItemDTO> items) {
        this.items = items;
    }

    public static class JabongItemDTO {
        private Item   item;
        private String sequence;

        /**
         * @param item2
         * @param i
         */
        public JabongItemDTO(Item item, String sequence) {
            this.item = item;
            this.sequence = sequence;
        }

        /**
         * @return the item
         */
        public Item getItem() {
            return item;
        }

        /**
         * @param item the item to set
         */
        public void setItem(Item item) {
            this.item = item;
        }

        /**
         * @return the sequence
         */
        public String getSequence() {
            return sequence;
        }

        /**
         * @param sequence the sequence to set
         */
        public void setSequence(String sequence) {
            this.sequence = sequence;
        }
    }

}
