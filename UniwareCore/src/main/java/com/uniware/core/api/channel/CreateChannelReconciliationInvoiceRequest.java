/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 09-Oct-2014
 *  @author parijat
 */
package com.uniware.core.api.channel;

import javax.validation.Valid;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author parijat
 */
public class CreateChannelReconciliationInvoiceRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long               serialVersionUID = 3733474998629845309L;

    @Valid
    private WsChannelReconciliationInvoice channelReconciliationInvoice;

    private String                         invoiceFailedLog;

    /**
     * @return the channelReconciliationIInvoice
     */
    public WsChannelReconciliationInvoice getChannelReconciliationInvoice() {
        return channelReconciliationInvoice;
    }

    public void setChannelReconciliationInvoice(WsChannelReconciliationInvoice channelReconciliationInvoice) {
        this.channelReconciliationInvoice = channelReconciliationInvoice;
    }

    /**
     * @return the invoiceFailedLog
     */
    public String getInvoiceFailedLog() {
        return invoiceFailedLog;
    }

    /**
     * @param invoiceFailedLog the invoiceFailedLog to set
     */
    public void setInvoiceFailedLog(String invoiceFailedLog) {
        this.invoiceFailedLog = invoiceFailedLog;
    }

}
