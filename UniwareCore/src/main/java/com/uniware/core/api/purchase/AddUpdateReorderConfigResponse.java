/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 10-Oct-2013
 *  @author parijat
 */
package com.uniware.core.api.purchase;

import javax.validation.constraints.NotNull;

import com.unifier.core.api.base.ServiceResponse;

public class AddUpdateReorderConfigResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 4728748928372897L;
    
    @NotNull
    private WsReorderConfig wsReorderConfig;

    /**
     * @return the wsReorderConfig
     */
    public WsReorderConfig getWsReorderConfig() {
        return wsReorderConfig;
    }

    /**
     * @param wsReorderConfig the wsReorderConfig to set
     */
    public void setWsReorderConfig(WsReorderConfig wsReorderConfig) {
        this.wsReorderConfig = wsReorderConfig;
    }

}
