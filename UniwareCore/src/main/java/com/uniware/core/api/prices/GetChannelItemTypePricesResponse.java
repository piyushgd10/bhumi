/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Nov 25, 2015
 *  @author akshay
 */
package com.uniware.core.api.prices;

import java.util.ArrayList;
import java.util.List;

import com.unifier.core.api.base.ServiceResponse;
import com.uniware.core.api.prices.dto.ListingPricesDTO;

public class GetChannelItemTypePricesResponse extends ServiceResponse {

    private static final long serialVersionUID = 5970156023999882782L;
    
    private List<ListingPricesDTO> listingPrices = new ArrayList<>();

    public List<ListingPricesDTO> getListingPrices() {
        return listingPrices;
    }

    public void setListingPrices(List<ListingPricesDTO> listingPrices) {
        this.listingPrices = listingPrices;
    }
}
