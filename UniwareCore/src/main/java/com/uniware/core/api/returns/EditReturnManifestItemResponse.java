/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 02-Apr-2013
 *  @author pankaj
 */
package com.uniware.core.api.returns;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author pankaj
 */
public class EditReturnManifestItemResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long     serialVersionUID = -7165097865317589844L;

    private ReturnManifestItemDetailDTO returnManifestItem;

    /**
     * @return the returnManifestItem
     */
    public ReturnManifestItemDetailDTO getReturnManifestItem() {
        return returnManifestItem;
    }

    /**
     * @param returnManifestItem the returnManifestItem to set
     */
    public void setReturnManifestItem(ReturnManifestItemDetailDTO returnManifestItem) {
        this.returnManifestItem = returnManifestItem;
    }

}
