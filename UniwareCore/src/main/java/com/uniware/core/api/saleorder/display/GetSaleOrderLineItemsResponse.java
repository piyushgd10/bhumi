/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jan 10, 2012
 *  @author singla
 */
package com.uniware.core.api.saleorder.display;

import java.math.BigDecimal;
import java.util.List;
import java.util.Set;

import com.unifier.core.api.base.ServiceResponse;

public class GetSaleOrderLineItemsResponse extends ServiceResponse {

    /**
     *
     */
    private static final long serialVersionUID = 6284414547113409023L;

    public GetSaleOrderLineItemsResponse() {
    }

    private List<SaleOrderLineItemDTO> lineItems;
    private boolean                    invalidFacility;
    private Set<String>                validFacilities;
    private boolean                    cancellable;
    private boolean                    holdable;
    private boolean                    unholdable;
    private boolean                    inventoryReleasable;
    private boolean                    reversePickable;
    private boolean                    packageModifiable;
    private boolean                    facilitySwitchable;
    private boolean                    packageCreatable;
    private boolean                    alternateAcceptable;
    private boolean                    alternateSuggestible;

    public boolean isAlternateSuggestible() {
        return alternateSuggestible;
    }

    public void setAlternateSuggestible(boolean alternateSuggestible) {
        this.alternateSuggestible = alternateSuggestible;
    }

    public List<SaleOrderLineItemDTO> getLineItems() {
        return lineItems;
    }

    public void setLineItems(List<SaleOrderLineItemDTO> lineItems) {
        this.lineItems = lineItems;
    }

    public boolean isInvalidFacility() {
        return invalidFacility;
    }

    public void setInvalidFacility(boolean invalidFacility) {
        this.invalidFacility = invalidFacility;
    }

    public Set<String> getValidFacilities() {
        return validFacilities;
    }

    public void setValidFacilities(Set<String> validFacilities) {
        this.validFacilities = validFacilities;
    }

    public boolean isCancellable() {
        return cancellable;
    }

    public void setCancellable(boolean cancellable) {
        this.cancellable = cancellable;
    }

    public boolean isHoldable() {
        return holdable;
    }

    public void setHoldable(boolean holdable) {
        this.holdable = holdable;
    }

    public boolean isUnholdable() {
        return unholdable;
    }

    public void setUnholdable(boolean unholdable) {
        this.unholdable = unholdable;
    }

    public boolean isInventoryReleasable() {
        return inventoryReleasable;
    }

    public void setInventoryReleasable(boolean inventoryReleasable) {
        this.inventoryReleasable = inventoryReleasable;
    }

    public boolean isReversePickable() {
        return reversePickable;
    }

    public void setReversePickable(boolean reversePickable) {
        this.reversePickable = reversePickable;
    }

    public boolean isPackageModifiable() {
        return packageModifiable;
    }

    public void setPackageModifiable(boolean packageModifiable) {
        this.packageModifiable = packageModifiable;
    }

    public boolean isFacilitySwitchable() {
        return facilitySwitchable;
    }

    public void setFacilitySwitchable(boolean facilitySwitchable) {
        this.facilitySwitchable = facilitySwitchable;
    }

    public boolean isPackageCreatable() {
        return packageCreatable;
    }

    public void setPackageCreatable(boolean packageCreatable) {
        this.packageCreatable = packageCreatable;
    }

    public boolean isAlternateAcceptable() {
        return alternateAcceptable;
    }

    public void setAlternateAcceptable(boolean alternateSuggestable) {
        this.alternateAcceptable = alternateSuggestable;
    }

    public static class SaleOrderLineItemDTO {
        private String  lineItemName;
        private String  lineItemIdentifier;
        private String  channelProductId;
        private boolean combo;
        private String  itemSku;
        private String  channelProductName;
        private String  facilityCode;
        private BigDecimal totalPrice            = BigDecimal.ZERO;
        private BigDecimal sellingPrice          = BigDecimal.ZERO;
        private BigDecimal shippingCharges       = BigDecimal.ZERO;
        private BigDecimal cashOnDeliveryCharges = BigDecimal.ZERO;
        private BigDecimal giftWrapCharges       = BigDecimal.ZERO;
        private BigDecimal discount              = BigDecimal.ZERO;
        private BigDecimal prepaidAmount         = BigDecimal.ZERO;
        private BigDecimal shippingMethodCharges = BigDecimal.ZERO;
        private BigDecimal totalCharges          = BigDecimal.ZERO;
        private int totalItems;
        private int inProcess;
        private int onHold;
        private int cancellable;
        private int unfulfillable;
        private int dispatched;
        private int cancelled;
        private int returned;
        private int replaced;
        private int reshipped;

        public String getLineItemName() {
            return lineItemName;
        }

        public void setLineItemName(String lineItemName) {
            this.lineItemName = lineItemName;
        }

        public String getLineItemIdentifier() {
            return lineItemIdentifier;
        }

        public void setLineItemIdentifier(String lineItemIdentifier) {
            this.lineItemIdentifier = lineItemIdentifier;
        }

        public String getChannelProductId() {
            return channelProductId;
        }

        public void setChannelProductId(String channelProductId) {
            this.channelProductId = channelProductId;
        }

        public String getItemSku() {
            return itemSku;
        }

        public void setItemSku(String itemSku) {
            this.itemSku = itemSku;
        }

        public String getChannelProductName() {
            return channelProductName;
        }

        public void setChannelProductName(String channelProductName) {
            this.channelProductName = channelProductName;
        }

        public boolean isCombo() {
            return combo;
        }

        public void setCombo(boolean combo) {
            this.combo = combo;
        }

        public String getFacilityCode() {
            return facilityCode;
        }

        public void setFacilityCode(String facilityCode) {
            this.facilityCode = facilityCode;
        }

        public BigDecimal getTotalPrice() {
            return totalPrice;
        }

        public void setTotalPrice(BigDecimal totalPrice) {
            this.totalPrice = totalPrice;
        }

        public BigDecimal getSellingPrice() {
            return sellingPrice;
        }

        public void setSellingPrice(BigDecimal sellingPrice) {
            this.sellingPrice = sellingPrice;
        }

        public BigDecimal getShippingCharges() {
            return shippingCharges;
        }

        public void setShippingCharges(BigDecimal shippingCharges) {
            this.shippingCharges = shippingCharges;
        }

        public BigDecimal getCashOnDeliveryCharges() {
            return cashOnDeliveryCharges;
        }

        public void setCashOnDeliveryCharges(BigDecimal cashOnDeliveryCharges) {
            this.cashOnDeliveryCharges = cashOnDeliveryCharges;
        }

        public BigDecimal getGiftWrapCharges() {
            return giftWrapCharges;
        }

        public void setGiftWrapCharges(BigDecimal giftWrapCharges) {
            this.giftWrapCharges = giftWrapCharges;
        }

        public BigDecimal getDiscount() {
            return discount;
        }

        public void setDiscount(BigDecimal discount) {
            this.discount = discount;
        }

        public BigDecimal getPrepaidAmount() {
            return prepaidAmount;
        }

        public void setPrepaidAmount(BigDecimal prepaidAmount) {
            this.prepaidAmount = prepaidAmount;
        }

        public BigDecimal getShippingMethodCharges() {
            return shippingMethodCharges;
        }

        public void setShippingMethodCharges(BigDecimal shippingMethodCharges) {
            this.shippingMethodCharges = shippingMethodCharges;
        }

        public BigDecimal getTotalCharges() {
            return totalCharges;
        }

        public void setTotalCharges(BigDecimal totalCharges) {
            this.totalCharges = totalCharges;
        }

        public int getTotalItems() {
            return totalItems;
        }

        public void setTotalItems(int totalItems) {
            this.totalItems = totalItems;
        }

        public int getInProcess() {
            return inProcess;
        }

        public void setInProcess(int inProgress) {
            this.inProcess = inProgress;
        }

        public int getCancellable() {
            return cancellable;
        }

        public void setCancellable(int cancellable) {
            this.cancellable = cancellable;
        }

        public int getOnHold() {
            return onHold;
        }

        public void setOnHold(int onHold) {
            this.onHold = onHold;
        }

        public int getUnfulfillable() {
            return unfulfillable;
        }

        public void setUnfulfillable(int unfulfillable) {
            this.unfulfillable = unfulfillable;
        }

        public int getDispatched() {
            return dispatched;
        }

        public void setDispatched(int dispatched) {
            this.dispatched = dispatched;
        }

        public int getCancelled() {
            return cancelled;
        }

        public void setCancelled(int cancelled) {
            this.cancelled = cancelled;
        }

        public int getReturned() {
            return returned;
        }

        public void setReturned(int returned) {
            this.returned = returned;
        }

        public int getReplaced() {
            return replaced;
        }

        public void setReplaced(int replaced) {
            this.replaced = replaced;
        }

        public int getReshipped() {
            return reshipped;
        }

        public void setReshipped(int reshipped) {
            this.reshipped = reshipped;
        }
    }
}
