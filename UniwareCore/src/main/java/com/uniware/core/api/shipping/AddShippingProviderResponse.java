/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 12-Feb-2015
 *  @author parijat
 */
package com.uniware.core.api.shipping;

import com.unifier.core.api.base.ServiceResponse;
import com.uniware.core.api.admin.shipping.ShippingProviderDetailDTO;

/**
 * @author parijat
 */
public class AddShippingProviderResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long         serialVersionUID = 2470285083535604894L;

    private ShippingProviderDetailDTO shippingProvider;

    /**
     * @return the shippingProvider
     */
    public ShippingProviderDetailDTO getShippingProvider() {
        return shippingProvider;
    }

    /**
     * @param shippingProvider the shippingProvider to set
     */
    public void setShippingProvider(ShippingProviderDetailDTO shippingProvider) {
        this.shippingProvider = shippingProvider;
    }

}
