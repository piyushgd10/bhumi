/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 18-Apr-2013
 *  @author unicom
 */
package com.uniware.core.api.channel;

import com.uniware.core.entity.Channel;
import com.uniware.core.entity.Source;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Sunny Agarwal
 */
public class ChannelSyncDTO implements Serializable {
    
    
    /**
     * 
     */
    private static final long serialVersionUID = 4075187091377231231L;
    
    private int                           channelId;
    private String                        code;
    private String                        name;
    private boolean                       enabled;
    private String                        colorCode;
    private String                        shortName;
    private String                        orderSyncStatus;
    private String                        inventorySyncStatus;
    private String                        pricingSyncStatus;
    private SourceDTO                     sourceDTO;
    private List<ChannelConnectorDTO>     channelConnectors;
    private ChannelOrderSyncStatusDTO     lastOrderSyncResult;
    private ChannelInventorySyncStatusDTO lastInventorySyncResult;
    private ChannelPricingPushStatusDTO   lastPricingSyncResult;

    public ChannelSyncDTO() {
    }

    public ChannelSyncDTO(Channel channel, Source source) {
        setChannelId(channel.getId());
        code = channel.getCode();
        name = channel.getName();
        enabled = channel.isEnabled();
        colorCode = channel.getColorCode();
        shortName = channel.getShortName();
        orderSyncStatus = channel.getOrderSyncStatus().name();
        inventorySyncStatus = channel.getInventorySyncStatus().name();
        pricingSyncStatus = channel.getPricingSyncStatus().name();
        channelConnectors = new ArrayList<ChannelConnectorDTO>(channel.getChannelConnectors().size());
        sourceDTO = new SourceDTO(source);
    }

    public int getChannelId() {
        return channelId;
    }

    public void setChannelId(int channelId) {
        this.channelId = channelId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getOrderSyncStatus() {
        return orderSyncStatus;
    }

    public void setOrderSyncStatus(String orderSyncStatus) {
        this.orderSyncStatus = orderSyncStatus;
    }

    public String getInventorySyncStatus() {
        return inventorySyncStatus;
    }

    public void setInventorySyncStatus(String inventorySyncStatus) {
        this.inventorySyncStatus = inventorySyncStatus;
    }
    
    public String getPricingSyncStatus() {
        return pricingSyncStatus;
    }

    public void setPricingSyncStatus(String pricingSyncStatus) {
        this.pricingSyncStatus = pricingSyncStatus;
    }

    public ChannelOrderSyncStatusDTO getLastOrderSyncResult() {
        return lastOrderSyncResult;
    }

    public void setLastOrderSyncResult(ChannelOrderSyncStatusDTO lastOrderSyncResult) {
        this.lastOrderSyncResult = lastOrderSyncResult;
    }

    public ChannelInventorySyncStatusDTO getLastInventorySyncResult() {
        return lastInventorySyncResult;
    }

    public void setLastInventorySyncResult(ChannelInventorySyncStatusDTO lastInventorySyncResult) {
        this.lastInventorySyncResult = lastInventorySyncResult;
    }
    
    public ChannelPricingPushStatusDTO getLastPricingSyncResult() {
        return lastPricingSyncResult;
    }

    public void setLastPricingSyncResult(ChannelPricingPushStatusDTO lastPricingSyncResult) {
        this.lastPricingSyncResult = lastPricingSyncResult;
    }

    public SourceDTO getSourceDTO() {
        return sourceDTO;
    }

    public void setSourceDTO(SourceDTO sourceDTO) {
        this.sourceDTO = sourceDTO;
    }

    public List<ChannelConnectorDTO> getChannelConnectors() {
        return channelConnectors;
    }

    public void setChannelConnectors(List<ChannelConnectorDTO> channelConnectors) {
        this.channelConnectors = channelConnectors;
    }

    public String getColorCode() {
        return colorCode;
    }

    public void setColorCode(String colorCode) {
        this.colorCode = colorCode;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

}
