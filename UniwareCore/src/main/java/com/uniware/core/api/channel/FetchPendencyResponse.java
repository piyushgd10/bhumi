/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jun 20, 2012
 *  @author singla
 */
package com.uniware.core.api.channel;

import java.util.ArrayList;
import java.util.List;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author Sunny
 */
public class FetchPendencyResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long      serialVersionUID = 5136222167598910731L;
    private String                 channelCode;
    private List<FetchPendencyDTO> pendencyDTOs     = new ArrayList<FetchPendencyDTO>();

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    public List<FetchPendencyDTO> getPendencyDTOs() {
        return pendencyDTOs;
    }

    public void setPendencyDTOs(List<FetchPendencyDTO> pendencyDTOs) {
        this.pendencyDTOs = pendencyDTOs;
    }

}
