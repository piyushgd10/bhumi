package com.uniware.core.api.saleorder;

import com.unifier.core.api.base.ServiceResponse;
import com.uniware.core.entity.SaleOrderItem;
import com.uniware.core.entity.ShippingPackage;

import java.util.Date;
import java.util.List;

/**
 * Created by akshayag on 9/16/15.
 */
public class GetShippingPackagesByStatusCodeResponse extends ServiceResponse {

    private List<ShippingPackageDTO> shippingPackages;

    public List<ShippingPackageDTO> getShippingPackages() {
        return shippingPackages;
    }

    public void setShippingPackages(List<ShippingPackageDTO> shippingPackages) {
        this.shippingPackages = shippingPackages;
    }

    public static class ShippingPackageDTO {

        private String                 saleOrderCode;
        private String                 displaySaleOrderCode;
        private Date                   fulfillmentTat;
        private Date                   displayOrderDateTime;
        private String                 shippingPackageCode;
        private String                 shippingPackageStatus;
        private List<SaleOrderItemDTO> saleOrderItems;

        public ShippingPackageDTO() {
        }

        public ShippingPackageDTO(ShippingPackage shippingPackage, List<SaleOrderItemDTO> saleOrderItemDTOs) {
            this.saleOrderCode = shippingPackage.getSaleOrder().getCode();
            this.displaySaleOrderCode = shippingPackage.getSaleOrder().getDisplayOrderCode();
            this.fulfillmentTat = shippingPackage.getSaleOrder().getFulfillmentTat();
            this.displayOrderDateTime = shippingPackage.getSaleOrder().getDisplayOrderDateTime();
            this.shippingPackageCode = shippingPackage.getCode();
            this.shippingPackageStatus = shippingPackage.getStatusCode();
            this.saleOrderItems = saleOrderItemDTOs;
        }

        public String getShippingPackageStatus() {
            return shippingPackageStatus;
        }

        public void setShippingPackageStatus(String shippingPackageStatus) {
            this.shippingPackageStatus = shippingPackageStatus;
        }

        public String getSaleOrderCode() {
            return saleOrderCode;
        }

        public void setSaleOrderCode(String saleOrderCode) {
            this.saleOrderCode = saleOrderCode;
        }

        public String getDisplaySaleOrderCode() {
            return displaySaleOrderCode;
        }

        public void setDisplaySaleOrderCode(String displaySaleOrderCode) {
            this.displaySaleOrderCode = displaySaleOrderCode;
        }

        public Date getFulfillmentTat() {
            return fulfillmentTat;
        }

        public void setFulfillmentTat(Date fulfillmentTat) {
            this.fulfillmentTat = fulfillmentTat;
        }

        public Date getDisplayOrderDateTime() {
            return displayOrderDateTime;
        }

        public void setDisplayOrderDateTime(Date displayOrderDateTime) {
            this.displayOrderDateTime = displayOrderDateTime;
        }

        public String getShippingPackageCode() {
            return shippingPackageCode;
        }

        public void setShippingPackageCode(String shippingPackageCode) {
            this.shippingPackageCode = shippingPackageCode;
        }

        public List<SaleOrderItemDTO> getSaleOrderItems() {
            return saleOrderItems;
        }

        public void setSaleOrderItems(List<SaleOrderItemDTO> saleOrderItems) {
            this.saleOrderItems = saleOrderItems;
        }
    }

    public static class SaleOrderItemDTO {
        private String saleOrderItemCode;
        private String channelProductId;
        private String skuCode;
        private String productDescription;
        private String imageUrl;

        public SaleOrderItemDTO() {
        }

        public SaleOrderItemDTO(SaleOrderItem saleOrderItem, String imageUrl) {
            this.saleOrderItemCode = saleOrderItem.getCode();
            this.skuCode = saleOrderItem.getSellerSkuCode();
            this.productDescription = saleOrderItem.getChannelProductName();
            this.imageUrl = imageUrl;
            this.channelProductId = saleOrderItem.getChannelProductId();
        }

        public String getChannelProductId() {
            return channelProductId;
        }

        public void setChannelProductId(String channelProductId) {
            this.channelProductId = channelProductId;
        }

        public String getSaleOrderItemCode() {
            return saleOrderItemCode;
        }

        public void setSaleOrderItemCode(String saleOrderItemCode) {
            this.saleOrderItemCode = saleOrderItemCode;
        }

        public String getSkuCode() {
            return skuCode;
        }

        public void setSkuCode(String skuCode) {
            this.skuCode = skuCode;
        }

        public String getProductDescription() {
            return productDescription;
        }

        public void setProductDescription(String productDescription) {
            this.productDescription = productDescription;
        }

        public String getImageUrl() {
            return imageUrl;
        }

        public void setImageUrl(String imageUrl) {
            this.imageUrl = imageUrl;
        }
    }
}
