/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 17-Dec-2013
 *  @author akshay
 */
package com.uniware.core.api.shipping;

import java.util.ArrayList;
import java.util.List;

import com.uniware.core.api.saleorder.SaleOrderItemDTO;

public class ShippingPackageDetailDTO {

    private String                 code;
    private String                 saleOrderCode;
    private String                 statusCode;
    private String                 shippingManifestCode;
    private SaleOrderDetailDTO     saleOrderDetails;
    private List<SaleOrderItemDTO> saleOrderItems = new ArrayList<SaleOrderItemDTO>();

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getSaleOrderCode() {
        return saleOrderCode;
    }

    public void setSaleOrderCode(String saleOrderCode) {
        this.saleOrderCode = saleOrderCode;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getShippingManifestCode() {
        return shippingManifestCode;
    }

    public void setShippingManifestCode(String shippingManifestCode) {
        this.shippingManifestCode = shippingManifestCode;
    }

    public SaleOrderDetailDTO getSaleOrderDetails() {
        return saleOrderDetails;
    }

    public void setSaleOrderDetails(SaleOrderDetailDTO saleOrderDetails) {
        this.saleOrderDetails = saleOrderDetails;
    }

    public List<SaleOrderItemDTO> getSaleOrderItems() {
        return saleOrderItems;
    }

    public void setSaleOrderItems(List<SaleOrderItemDTO> saleOrderItems) {
        this.saleOrderItems = saleOrderItems;
    }

}
