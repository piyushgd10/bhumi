/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 21, 2012
 *  @author praveeng
 */
package com.uniware.core.api.party;

/**
 * @author praveeng
 */
public class CreateVendorAgreementResponse extends VendorAgreementResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 8758274969934625530L;

}
