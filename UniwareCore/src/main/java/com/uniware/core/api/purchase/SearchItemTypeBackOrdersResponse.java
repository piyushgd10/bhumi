/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 30-Jul-2012
 *  @author praveeng
 */
package com.uniware.core.api.purchase;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author praveeng
 */
public class SearchItemTypeBackOrdersResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long      serialVersionUID = 7421997200511457902L;
    private List<SaleOrderItemDTO> saleOrderItems   = new ArrayList<SaleOrderItemDTO>();

    /**
     * @return the saleOrderItems
     */
    public List<SaleOrderItemDTO> getSaleOrderItems() {
        return saleOrderItems;
    }

    /**
     * @param saleOrderItems the saleOrderItems to set
     */
    public void setSaleOrderItems(List<SaleOrderItemDTO> saleOrderItems) {
        this.saleOrderItems = saleOrderItems;
    }

    public static class SaleOrderItemDTO {
        private Integer    saleOrderItemId;
        private String     saleOrderItemCode;
        private String     itemName;
        private String     itemSku;
        private String     imageUrl;
        private BigDecimal sellingPrice;
        private String     status;
        private String     saleOrderCode;

        public Integer getSaleOrderItemId() {
            return saleOrderItemId;
        }

        public void setSaleOrderItemId(Integer saleOrderItemId) {
            this.saleOrderItemId = saleOrderItemId;
        }

        public String getSaleOrderItemCode() {
            return saleOrderItemCode;
        }

        public void setSaleOrderItemCode(String saleOrderItemCode) {
            this.saleOrderItemCode = saleOrderItemCode;
        }

        public String getItemName() {
            return itemName;
        }

        public void setItemName(String itemName) {
            this.itemName = itemName;
        }

        public String getItemSku() {
            return itemSku;
        }

        public void setItemSku(String itemSku) {
            this.itemSku = itemSku;
        }

        public String getImageUrl() {
            return imageUrl;
        }

        public void setImageUrl(String imageUrl) {
            this.imageUrl = imageUrl;
        }

        public BigDecimal getSellingPrice() {
            return sellingPrice;
        }

        public void setSellingPrice(BigDecimal sellingPrice) {
            this.sellingPrice = sellingPrice;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getSaleOrderCode() {
            return saleOrderCode;
        }

        public void setSaleOrderCode(String saleOrderCode) {
            this.saleOrderCode = saleOrderCode;
        }

    }

}
