/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, May 6, 2015
 *  @author harsh
 */
package com.uniware.core.api.purchase;

import java.util.List;

import com.unifier.core.api.base.ServiceResponse;
import com.uniware.core.configuration.TaxConfiguration.TaxTypeVO;

/**
 * @author harsh
 */
public class LookupPurchaseOrderLineItemsResponse extends ServiceResponse {

    private static final long           serialVersionUID = -1330662922657441000L;

    private List<PurchaseOrderLineItem> lineItems;

    public List<PurchaseOrderLineItem> getLineItems() {
        return lineItems;
    }

    public void setLineItems(List<PurchaseOrderLineItem> lineItems) {
        this.lineItems = lineItems;
    }

    public static class PurchaseOrderLineItem {
        private String    itemSKU;
        private Integer   quantity;
        private Double    sellingPrice;
        private String    vendorSkuCode;
        private Double    mrp;
        private TaxTypeVO taxType;
        private Double    discount;

        public String getItemSKU() {
            return itemSKU;
        }

        public void setItemSKU(String itemSKU) {
            this.itemSKU = itemSKU;
        }

        public Integer getQuantity() {
            return quantity;
        }

        public void setQuantity(Integer quantity) {
            this.quantity = quantity;
        }

        public Double getSellingPrice() {
            return sellingPrice;
        }

        public void setSellingPrice(Double sellingPrice) {
            this.sellingPrice = sellingPrice;
        }

        public String getVendorSkuCode() {
            return vendorSkuCode;
        }

        public void setVendorSkuCode(String vendorSkuCode) {
            this.vendorSkuCode = vendorSkuCode;
        }

        public Double getMrp() {
            return mrp;
        }

        public void setMrp(Double mrp) {
            this.mrp = mrp;
        }

        public TaxTypeVO getTaxType() {
            return taxType;
        }

        public void setTaxType(TaxTypeVO taxType) {
            this.taxType = taxType;
        }

        public Double getDiscount() {
            return discount;
        }

        public void setDiscount(Double discount) {
            this.discount = discount;
        }

    }

}
