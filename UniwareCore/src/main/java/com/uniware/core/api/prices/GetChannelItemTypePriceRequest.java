/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Oct 13, 2015
 *  @author bhupi
 */
package com.uniware.core.api.prices;


public class GetChannelItemTypePriceRequest extends AbstractChannelItemTypeRequest {

    private static final long serialVersionUID = 3341528689438847422L;

}
