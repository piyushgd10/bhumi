/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 20/08/14
 *  @author amit
 */

package com.uniware.core.api.packer;

import com.unifier.core.api.base.ServiceResponse;
import com.uniware.core.entity.SaleOrder;
import com.uniware.core.entity.SaleOrderItem;

public class PrintPicklistItemResponse extends ServiceResponse {

    private static final long     serialVersionUID = 9030945051345914779L;

    private SaleOrder     saleOrder;

    private SaleOrderItem saleOrderItem;

    private int           serialNumberInOrder;

    private int           serialNumberInPackage;

    public PrintPicklistItemResponse() {
        super();
    }

    public SaleOrder getSaleOrder() {
        return saleOrder;
    }

    public void setSaleOrder(SaleOrder saleOrder) {
        this.saleOrder = saleOrder;
    }

    public SaleOrderItem getSaleOrderItem() {
        return saleOrderItem;
    }

    public void setSaleOrderItem(SaleOrderItem saleOrderItem) {
        this.saleOrderItem = saleOrderItem;
    }

    public int getSerialNumberInOrder() {
        return serialNumberInOrder;
    }

    public void setSerialNumberInOrder(int serialNumberInOrder) {
        this.serialNumberInOrder = serialNumberInOrder;
    }

    public int getSerialNumberInPackage() {
        return serialNumberInPackage;
    }

    public void setSerialNumberInPackage(int serialNumberInPackage) {
        this.serialNumberInPackage = serialNumberInPackage;
    }

    @Override
    public String toString() {
        return "PrintPicklistItemResponse{" + "saleOrder=" + saleOrder + ", saleOrderItem=" + saleOrderItem + ", serialNumberInOrder=" + serialNumberInOrder
                + ", serialNumberInPackage=" + serialNumberInPackage + '}';
    }
}