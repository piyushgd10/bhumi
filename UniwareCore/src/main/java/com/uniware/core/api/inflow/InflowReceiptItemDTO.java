/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 3, 2012
 *  @author praveeng
 */
package com.uniware.core.api.inflow;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author praveeng
 */
public class InflowReceiptItemDTO {

    private int        id;
    private int        purchaseOrderItemId;
    private String     itemSKU;
    private String     itemTypeName;
    private String     itemTypeImageUrl;
    private String     itemTypePageUrl;
    private String     vendorSkuCode;
    private int        quantity;
    private boolean    itemsLabelled;

    private int        pendingQuantity;
    private int        rejectedQuantity;
    // null if not applicable
    private Integer    detailedQuantity;

    private String     rejectionComments;
    private Date       expiry;
    private String     status;
    private BigDecimal unitPrice      = BigDecimal.ZERO;
    private BigDecimal maxRetailPrice;
    private BigDecimal additionalCost = BigDecimal.ZERO;
    private String     batchCode;
    private BigDecimal discount;
    private BigDecimal discountPercentage;
    private Date       manufacturingDate;
    private boolean    expirable;
    private Integer    shelfLife;
    private Integer    grnExpiryTolerance;

    /**
     * @return the itemSKU
     */
    public String getItemSKU() {
        return itemSKU;
    }

    /**
     * @param itemSKU the itemSKU to set
     */
    public void setItemSKU(String itemSKU) {
        this.itemSKU = itemSKU;
    }

    /**
     * @return the itemTypeName
     */
    public String getItemTypeName() {
        return itemTypeName;
    }

    /**
     * @param itemTypeName the itemTypeName to set
     */
    public void setItemTypeName(String itemTypeName) {
        this.itemTypeName = itemTypeName;
    }

    /**
     * @return the vendorSkuCode
     */
    public String getVendorSkuCode() {
        return vendorSkuCode;
    }

    /**
     * @param vendorSkuCode the vendorSkuCode to set
     */
    public void setVendorSkuCode(String vendorSkuCode) {
        this.vendorSkuCode = vendorSkuCode;
    }

    /**
     * @return the quantity
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     * @param quantity the quantity to set
     */
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    /**
     * @return the rejectedQuantity
     */
    public int getRejectedQuantity() {
        return rejectedQuantity;
    }

    /**
     * @param rejectedQuantity the rejectedQuantity to set
     */
    public void setRejectedQuantity(int rejectedQuantity) {
        this.rejectedQuantity = rejectedQuantity;
    }

    /**
     * @return the unitPrice
     */
    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    /**
     * @param unitPrice the unitPrice to set
     */
    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }

    /**
     * @return the itemsLabelled
     */
    public boolean isItemsLabelled() {
        return itemsLabelled;
    }

    /**
     * @param itemsLabelled the itemsLabelled to set
     */
    public void setItemsLabelled(boolean itemsLabelled) {
        this.itemsLabelled = itemsLabelled;
    }

    /**
     * @return the rejectionComments
     */
    public String getRejectionComments() {
        return rejectionComments;
    }

    /**
     * @param rejectionComments the rejectionComments to set
     */
    public void setRejectionComments(String rejectionComments) {
        this.rejectionComments = rejectionComments;
    }

    /**
     * @return the expiry
     */
    public Date getExpiry() {
        return expiry;
    }

    /**
     * @param expiry the expiry to set
     */
    public void setExpiry(Date expiry) {
        this.expiry = expiry;
    }

    /**
     * @return the pendingQuantity
     */
    public int getPendingQuantity() {
        return pendingQuantity;
    }

    /**
     * @param pendingQuantity the pendingQuantity to set
     */
    public void setPendingQuantity(int pendingQuantity) {
        this.pendingQuantity = pendingQuantity;
    }

    /**
     * @return the itemTypeImageUrl
     */
    public String getItemTypeImageUrl() {
        return itemTypeImageUrl;
    }

    /**
     * @param itemTypeImageUrl the itemTypeImageUrl to set
     */
    public void setItemTypeImageUrl(String itemTypeImageUrl) {
        this.itemTypeImageUrl = itemTypeImageUrl;
    }

    /**
     * @return the itemTypePageUrl
     */
    public String getItemTypePageUrl() {
        return itemTypePageUrl;
    }

    /**
     * @param itemTypePageUrl the itemTypePageUrl to set
     */
    public void setItemTypePageUrl(String itemTypePageUrl) {
        this.itemTypePageUrl = itemTypePageUrl;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the detailedQuantity
     */
    public Integer getDetailedQuantity() {
        return detailedQuantity;
    }

    /**
     * @param detailedQuantity the detailedQuantity to set
     */
    public void setDetailedQuantity(Integer detailedQuantity) {
        this.detailedQuantity = detailedQuantity;
    }

    /**
     * @return the maxRetailPrice
     */
    public BigDecimal getMaxRetailPrice() {
        return maxRetailPrice;
    }

    /**
     * @param maxRetailPrice the maxRetailPrice to set
     */
    public void setMaxRetailPrice(BigDecimal maxRetailPrice) {
        this.maxRetailPrice = maxRetailPrice;
    }

    /**
     * @return the discount
     */
    public BigDecimal getDiscount() {
        return discount;
    }

    /**
     * @param discount the discount to set
     */
    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    /**
     * @return the discountPercentage
     */
    public BigDecimal getDiscountPercentage() {
        return discountPercentage;
    }

    /**
     * @param discountPercentage the discountPercentage to set
     */
    public void setDiscountPercentage(BigDecimal discountPercentage) {
        this.discountPercentage = discountPercentage;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the purchaseOrderItemId
     */
    public int getPurchaseOrderItemId() {
        return purchaseOrderItemId;
    }

    /**
     * @param purchaseOrderItemId the purchaseOrderItemId to set
     */
    public void setPurchaseOrderItemId(int purchaseOrderItemId) {
        this.purchaseOrderItemId = purchaseOrderItemId;
    }

    /**
     * @return the additionalCost
     */
    public BigDecimal getAdditionalCost() {
        return additionalCost;
    }

    /**
     * @param additionalCost the additionalCost to set
     */
    public void setAdditionalCost(BigDecimal additionalCost) {
        this.additionalCost = additionalCost;
    }

    /**
     * @return the batchCode
     */
    public String getBatchCode() {
        return batchCode;
    }

    /**
     * @param batchCode the batchCode to set
     */
    public void setBatchCode(String batchCode) {
        this.batchCode = batchCode;
    }

    public Date getManufacturingDate() {
        return manufacturingDate;
    }

    public void setManufacturingDate(Date manufacturingDate) {
        this.manufacturingDate = manufacturingDate;
    }

    public boolean isExpirable() {
        return expirable;
    }

    public void setExpirable(boolean expirable) {
        this.expirable = expirable;
    }

    public Integer getShelfLife() {
        return shelfLife;
    }

    public void setShelfLife(Integer shelfLife) {
        this.shelfLife = shelfLife;
    }

    public Integer getGrnExpiryTolerance() {
        return grnExpiryTolerance;
    }

    public void setGrnExpiryTolerance(Integer grnExpiryTolerance) {
        this.grnExpiryTolerance = grnExpiryTolerance;
    }

}