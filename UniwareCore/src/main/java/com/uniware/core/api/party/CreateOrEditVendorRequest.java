/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 21-Oct-2013
 *  @author akshay
 */
package com.uniware.core.api.party;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.unifier.core.api.base.ServiceRequest;

public class CreateOrEditVendorRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 9104743695524691610L;

    @NotNull
    @Valid
    private WsVendor          vendor;

    /**
     * @return the vendor
     */
    public WsVendor getVendor() {
        return vendor;
    }

    /**
     * @param vendor the vendor to set
     */
    public void setVendor(WsVendor vendor) {
        this.vendor = vendor;
    }

}
