/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 20, 2012
 *  @author praveeng
 */
package com.uniware.core.api.party;

import com.unifier.core.api.base.ServiceRequest;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * @author praveeng
 */
public class EditVendorRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -8699411146004803918L;

    @NotNull
    @Valid
    private WsGenericVendor          vendor;

    /**
     * @return the vendor
     */
    public WsGenericVendor getVendor() {
        return vendor;
    }

    /**
     * @param vendor the vendor to set
     */
    public void setVendor(WsGenericVendor vendor) {
        this.vendor = vendor;
    }

}
