/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 12, 2012
 *  @author praveeng
 */
package com.uniware.core.api.shipping;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author praveeng
 */
public class PaymentReconciliationResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 5342607491296587884L;

}
