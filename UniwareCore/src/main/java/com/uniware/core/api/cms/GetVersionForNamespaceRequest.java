/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 01/05/14
 *  @author amit
 */

package com.uniware.core.api.cms;

import java.util.Set;

import javax.validation.constraints.NotNull;

import com.unifier.core.api.base.ServiceRequest;
import com.uniware.core.vo.ContentVO;

public class GetVersionForNamespaceRequest extends ServiceRequest {

    private static final long      serialVersionUID = 7260847264312534031L;

    @NotNull
    private Set<String>            nsIdentifierSet;

    private ContentVO.LanguageCode languageCode;

    public GetVersionForNamespaceRequest() {
        super();
    }

    public ContentVO.LanguageCode getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(ContentVO.LanguageCode languageCode) {
        this.languageCode = languageCode;
    }

    public Set<String> getNsIdentifierSet() {
        return nsIdentifierSet;
    }

    public void setNsIdentifierSet(Set<String> nsIdentifierSet) {
        this.nsIdentifierSet = nsIdentifierSet;
    }

    @Override
    public String toString() {
        return "GetVersionForNamespaceRequest{" + "languageCode=" + languageCode + ", nsIdentifierSet=" + nsIdentifierSet + '}';
    }
}
