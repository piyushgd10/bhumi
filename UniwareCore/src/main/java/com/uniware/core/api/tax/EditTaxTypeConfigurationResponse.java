/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jun 20, 2012
 *  @author ankit
 */
package com.uniware.core.api.tax;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author ankit
 *
 */
public class EditTaxTypeConfigurationResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -4747152853048677706L;

}
