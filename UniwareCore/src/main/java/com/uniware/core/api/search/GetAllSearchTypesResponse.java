/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 17-Apr-2013
 *  @author unicom
 */
package com.uniware.core.api.search;

import java.util.List;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author Sunny
 */
public class GetAllSearchTypesResponse extends ServiceResponse {

    private static final long serialVersionUID = 2434232323L;

    private List<SearchTypeDTO> searchTypes;

    public List<SearchTypeDTO> getSearchTypes() {
        return searchTypes;
    }

    public void setSearchTypes(List<SearchTypeDTO> searchTypes) {
        this.searchTypes = searchTypes;
    }

}
