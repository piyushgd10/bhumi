/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 21, 2012
 *  @author praveeng
 */
package com.uniware.core.api.party;

import com.unifier.core.api.base.ServiceResponse;

import com.uniware.core.api.party.dto.VendorAgreementDTO;

/**
 * @author praveeng
 */
public class VendorAgreementResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long  serialVersionUID = 8496809010567736197L;
    private VendorAgreementDTO vendorAgreementDTO;
    /**
     * @return the vendorAgreementDTO
     */
    public VendorAgreementDTO getVendorAgreementDTO() {
        return vendorAgreementDTO;
    }
    /**
     * @param vendorAgreementDTO the vendorAgreementDTO to set
     */
    public void setVendorAgreementDTO(VendorAgreementDTO vendorAgreementDTO) {
        this.vendorAgreementDTO = vendorAgreementDTO;
    }

}
