/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Apr 7, 2012
 *  @author singla
 */
package com.uniware.core.api.purchase;

import com.unifier.core.api.base.ServiceRequest;

import javax.validation.constraints.NotNull;

/**
 * @author singla
 */
public class PurgePurchaseCartRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -1523488893672159472L;

    @NotNull
    private Integer           userId;

    public PurgePurchaseCartRequest() {

    }

    /**
     * @param userId
     */
    public PurgePurchaseCartRequest(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }
}
