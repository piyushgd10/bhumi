/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Oct 19, 2015
 *  @author bhupi
 */
package com.uniware.core.api.prices;

import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import com.unifier.core.api.base.ServiceResponse;

public class PullAllPricesFromChannelAsyncResponse extends ServiceResponse {

    private static final long serialVersionUID = 5864186440119842271L;
    
    private String channelCode;
    
    private Future<PullAllPricesFromChannelResponse> asyncTaskResult;

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }
    
    public void setAsyncTaskResult(final Future<PullAllPricesFromChannelResponse> asyncTaskResult) {
        this.asyncTaskResult = asyncTaskResult;
    }
    
    /**
     * Waits if necessary for the pull to complete and returns a {@code PullAllPricesFromChannelResponse}
     *
     * @throws CancellationException if the pull was cancelled
     * @throws ExecutionException if the pull threw an exception
     * @throws InterruptedException if the current thread was interrupted while waiting
     */
    public PullAllPricesFromChannelResponse sync() throws InterruptedException, ExecutionException {
        if (null != asyncTaskResult) {
            return asyncTaskResult.get();
        }
        PullAllPricesFromChannelResponse response = new PullAllPricesFromChannelResponse();
        response.setResponse(this);
        return response;
    }
}
