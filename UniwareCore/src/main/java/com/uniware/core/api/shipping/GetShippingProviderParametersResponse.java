/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jan 23, 2012
 *  @author singla
 */
package com.uniware.core.api.shipping;

import java.util.List;

import com.unifier.core.api.base.ServiceResponse;
import com.uniware.core.entity.ShippingProviderConnectorParameter;

/**
 * @author Sunny
 */
public class GetShippingProviderParametersResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long                  serialVersionUID = -5104527497740336170L;

    private String                             shippingProviderCode;
    private List<ShippingProviderConnectorParameterDTO> shippingProviderParameters;

    public String getShippingProviderCode() {
        return shippingProviderCode;
    }

    public void setShippingProviderCode(String shippingProviderCode) {
        this.shippingProviderCode = shippingProviderCode;
    }

    public List<ShippingProviderConnectorParameterDTO> getShippingProviderParameters() {
        return shippingProviderParameters;
    }

    public void setShippingProviderParameters(List<ShippingProviderConnectorParameterDTO> shippingProviderParameters) {
        this.shippingProviderParameters = shippingProviderParameters;
    }

    public static class ShippingProviderConnectorParameterDTO {
        private String name;
        private String value;

        public ShippingProviderConnectorParameterDTO() {
        }

        public ShippingProviderConnectorParameterDTO(ShippingProviderConnectorParameter spp) {
            this.name = spp.getName();
            this.value = spp.getValue();
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

    }

}
