/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 02-Feb-2015
 *  @author akshaykochhar
 */
package com.uniware.core.api.returns;

import com.unifier.core.api.base.ServiceResponse;

public class GetReturnManifestItemDetailsResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -3142039469805229599L;
    
    private ReturnManifestItemDetailDTO returnManifestItemDetail;
    
    public ReturnManifestItemDetailDTO getReturnManifestItemDetail() {
        return returnManifestItemDetail;
    }

    public void setReturnManifestItemDetail(ReturnManifestItemDetailDTO returnManifestItemDetail) {
        this.returnManifestItemDetail = returnManifestItemDetail;
    }

}
