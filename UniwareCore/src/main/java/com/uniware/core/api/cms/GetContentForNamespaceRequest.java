/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 16/04/14
 *  @author amit
 */

package com.uniware.core.api.cms;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;
import com.uniware.core.vo.ContentVO;

public class GetContentForNamespaceRequest extends ServiceRequest {

    private static final long      serialVersionUID = 3866843747010862854L;

    @NotBlank
    private String                 nsIdentifier;

    private ContentVO.LanguageCode languageCode;

    public GetContentForNamespaceRequest() {
        super();
    }

    public String getNsIdentifier() {
        return nsIdentifier;
    }

    public void setNsIdentifier(String nsIdentifier) {
        this.nsIdentifier = nsIdentifier;
    }

    public ContentVO.LanguageCode getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(ContentVO.LanguageCode languageCode) {
        this.languageCode = languageCode;
    }

    @Override
    public String toString() {
        return "GetContentForNamespaceRequest{" + "nsIdentifier=" + nsIdentifier + ", languageCode=" + languageCode + '}';
    }

}
