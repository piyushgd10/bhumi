/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, May 9, 2012
 *  @author singla
 */
package com.uniware.core.api.inflow;

import java.util.List;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author singla
 */
public class CompleteInflowReceiptItemsQCRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -6767871325768460858L;

    @NotNull
    private String            inflowReceiptCode;

    @NotEmpty
    private List<Integer>     inflowReceiptItemIds;

    private String            userEmail;

    /**
     * @return the inflowReceiptItemIds
     */
    public List<Integer> getInflowReceiptItemIds() {
        return inflowReceiptItemIds;
    }

    /**
     * @param inflowReceiptItemIds the inflowReceiptItemIds to set
     */
    public void setInflowReceiptItemIds(List<Integer> inflowReceiptItemIds) {
        this.inflowReceiptItemIds = inflowReceiptItemIds;
    }

    /**
     * @return the userEmail
     */
    public String getUserEmail() {
        return userEmail;
    }

    /**
     * @param userEmail the userEmail to set
     */
    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    /**
     * @return the inflowReceiptCode
     */
    public String getInflowReceiptCode() {
        return inflowReceiptCode;
    }

    /**
     * @param inflowReceiptCode the inflowReceiptCode to set
     */
    public void setInflowReceiptCode(String inflowReceiptCode) {
        this.inflowReceiptCode = inflowReceiptCode;
    }

}
