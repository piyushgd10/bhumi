/*
 * Copyright 2016 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 2/9/16 5:19 PM
 * @author bhuvneshwarkumar
 */

package com.uniware.core.api.picker;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.unifier.core.api.base.ServiceResponse;
import com.uniware.core.entity.PickSet;

/**
 * Created by bhuvneshwarkumar on 02/09/16.
 */
public class GetPicklistDetailResponse extends ServiceResponse {

    private static final long                               serialVersionUID          = -9110911982136751354L;

    private String                                          pickSetName;
    private String                                          picker;
    private PickSet.Type                                    pickSetType;
    private String                                          statusCode;
    private Map<String, Integer>                            pickBucketScannedQuantity = new HashMap<>();
    // shelf to sku to slot item
    private Map<String, Map<String, ItemTypeShelfQuantity>> pickInstructions;

    public String getPickSetName() {
        return pickSetName;
    }

    public void setPickSetName(String pickSetName) {
        this.pickSetName = pickSetName;
    }

    public String getPicker() {
        return picker;
    }

    public void setPicker(String picker) {
        this.picker = picker;
    }

    public PickSet.Type getPickSetType() {
        return pickSetType;
    }

    public void setPickSetType(PickSet.Type pickSetType) {
        this.pickSetType = pickSetType;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public Map<String, Integer> getPickBucketScannedQuantity() {
        return pickBucketScannedQuantity;
    }

    public void setPickBucketScannedQuantity(Map<String, Integer> pickBucketScannedQuantity) {
        this.pickBucketScannedQuantity = pickBucketScannedQuantity;
    }

    public Map<String, Map<String, ItemTypeShelfQuantity>> getPickInstructions() {
        return pickInstructions;
    }

    public void setPickInstructions(Map<String, Map<String, ItemTypeShelfQuantity>> pickInstructions) {
        this.pickInstructions = pickInstructions;
    }

    public static class ItemTypeShelfQuantity {

        private Integer     quantity       = 0;

        private String      productName;

        private Set<String> validItemCodes = new HashSet<>();

        public void incrementQuantity() {
            quantity++;
        }

        public Integer getQuantity() {
            return quantity;
        }

        public void setQuantity(Integer quantity) {
            this.quantity = quantity;
        }

        public String getProductName() {
            return productName;
        }

        public void setProductName(String productName) {
            this.productName = productName;
        }

        public Set<String> getValidItemCodes() {
            return validItemCodes;
        }

        public void setValidItemCodes(Set<String> validItemCodes) {
            this.validItemCodes = validItemCodes;
        }
    }
}
