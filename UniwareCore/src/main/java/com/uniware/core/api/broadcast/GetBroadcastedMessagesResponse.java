/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 20-Jan-2014
 *  @author akshay
 */
package com.uniware.core.api.broadcast;

import java.util.ArrayList;
import java.util.List;

import com.unifier.core.api.base.ServiceResponse;

public class GetBroadcastedMessagesResponse extends ServiceResponse{

    /**
     * 
     */
    private static final long serialVersionUID = 1560673680801570030L;

    List<BroadcastedMessageDTO> broadcastedMessages = new ArrayList<BroadcastedMessageDTO>();

    public List<BroadcastedMessageDTO> getBroadcastedMessages() {
        return broadcastedMessages;
    }

    public void setBroadcastedMessages(List<BroadcastedMessageDTO> broadcastedMessages) {
        this.broadcastedMessages = broadcastedMessages;
    }
}
