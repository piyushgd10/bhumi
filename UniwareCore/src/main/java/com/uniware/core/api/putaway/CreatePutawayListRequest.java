/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, May 9, 2012
 *  @author singla
 */
package com.uniware.core.api.putaway;

import com.unifier.core.api.base.ServiceRequest;

import javax.validation.constraints.NotNull;

/**
 * @author singla
 */
public class CreatePutawayListRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 6747500947804007909L;

    @NotNull
    private Integer           userId;

    @NotNull
    private String            putawayCode;

    /**
     * @return the putawayId
     */
    public String getPutawayCode() {
        return putawayCode;
    }

    /**
     * @param putawayId the putawayId to set
     */
    public void setPutawayCode(String putawayCode) {
        this.putawayCode = putawayCode;
    }

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

}
