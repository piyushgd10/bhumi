/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 15, 2011
 *  @author singla
 */
package com.uniware.core.api.saleorder;

import com.unifier.core.api.base.ServiceRequest;
import com.uniware.core.api.model.WsInvoice;
import com.uniware.core.api.model.WsSaleOrder;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author singla
 */
public class CreateSaleOrderRequest extends ServiceRequest {
    /**
     * 
     */
    private static final long serialVersionUID = 3148768119801793617L;
    @NotNull
    @Valid
    private WsSaleOrder       saleOrder;

    @Valid
    private List<WsInvoice>   invoices;

    public void setSaleOrder(WsSaleOrder saleOrder) {
        this.saleOrder = saleOrder;
    }

    public WsSaleOrder getSaleOrder() {
        return saleOrder;
    }

    @Override
    public String toString() {
        return "CreateSaleOrderRequest [saleOrder=" + saleOrder + ",invoices=" + invoices + "]";
    }

    public List<WsInvoice> getInvoices() {
        return invoices;
    }

    public void setInvoices(List<WsInvoice> invoices) {
        this.invoices = invoices;
    }
}
