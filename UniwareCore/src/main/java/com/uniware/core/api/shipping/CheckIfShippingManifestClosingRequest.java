/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 7, 2012
 *  @author singla
 */
package com.uniware.core.api.shipping;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author Sunny
 */
public class CheckIfShippingManifestClosingRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 1655278751824968276L;

    @NotBlank
    private String            shippingManifestCode;

    public String getShippingManifestCode() {
        return shippingManifestCode;
    }

    public void setShippingManifestCode(String shippingManifestCode) {
        this.shippingManifestCode = shippingManifestCode;
    }

}
