/*
 * Copyright 2016 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 9/3/16 1:35 PM
 * @author amdalal
 */

package com.uniware.core.api.admin.pickset;

import com.uniware.core.entity.PickArea;

public class PickAreaDTO {

    private int    id;

    private String name;

    public PickAreaDTO(PickArea pickArea) {
        setId(pickArea.getId());
        setName(pickArea.getName());
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
