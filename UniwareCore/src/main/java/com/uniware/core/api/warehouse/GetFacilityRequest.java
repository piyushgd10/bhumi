/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 24-Jan-2014
 *  @author parijat
 */
package com.uniware.core.api.warehouse;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

public class GetFacilityRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -3215784307744382978L;

    @NotBlank
    private String            code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
