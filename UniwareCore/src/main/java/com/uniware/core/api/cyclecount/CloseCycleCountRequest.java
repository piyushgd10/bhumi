/**
 * Copyright 2017 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version 1.0, 08/02/17
 * @author aditya
 */
package com.uniware.core.api.cyclecount;

import com.unifier.core.api.base.ServiceRequest;

public class
CloseCycleCountRequest extends ServiceRequest {
}
