/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 24-Dec-2011
 *  @author vibhu
 */
package com.uniware.core.api.warehouse;

import java.math.BigDecimal;

import javax.validation.constraints.Min;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author vibhu
 */
public class EditShippingPackageTypeRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -8441687950733318686L;

    private String            code;

    private boolean           enabled;
    @Min(0)
    private int               boxLength;
    @Min(0)
    private int               boxWidth;
    @Min(0)
    private int               boxHeight;
    @Min(0)
    private int               boxWeight;
    @Min(0)
    private BigDecimal        packingCost;

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the enabled
     */
    public boolean isEnabled() {
        return enabled;
    }

    /**
     * @param enabled the enabled to set
     */
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    /**
     * @return the boxLength
     */
    public int getBoxLength() {
        return boxLength;
    }

    /**
     * @param boxLength the boxLength to set
     */
    public void setBoxLength(int boxLength) {
        this.boxLength = boxLength;
    }

    /**
     * @return the boxWidth
     */
    public int getBoxWidth() {
        return boxWidth;
    }

    /**
     * @param boxWidth the boxWidth to set
     */
    public void setBoxWidth(int boxWidth) {
        this.boxWidth = boxWidth;
    }

    /**
     * @return the boxHeight
     */
    public int getBoxHeight() {
        return boxHeight;
    }

    /**
     * @param boxHeight the boxHeight to set
     */
    public void setBoxHeight(int boxHeight) {
        this.boxHeight = boxHeight;
    }

    /**
     * @return the boxWeight
     */
    public int getBoxWeight() {
        return boxWeight;
    }

    /**
     * @param boxWeight the boxWeight to set
     */
    public void setBoxWeight(int boxWeight) {
        this.boxWeight = boxWeight;
    }

    /**
     * @return the packingCost
     */
    public BigDecimal getPackingCost() {
        return packingCost;
    }

    /**
     * @param packingCost the packingCost to set
     */
    public void setPackingCost(BigDecimal packingCost) {
        this.packingCost = packingCost;
    }

}