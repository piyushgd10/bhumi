/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 10-Oct-2013
 *  @author parijat
 */
package com.uniware.core.api.purchase;

import javax.validation.constraints.NotNull;

public class WsReorderConfig {
    
    @NotNull
    private Integer itemTypeId;
    
    @NotNull
    private String itemName;
    
    @NotNull
    private String facilityName;
    
    @NotNull
    private Integer reorderQuantity;
    
    @NotNull
    private Integer reorderThreshold;
    
    /**
     * @param itemTypeId
     * @param itemName
     * @param facilityName
     * @param reorderQuantity
     * @param reorderThreshold
     */
    public WsReorderConfig(Integer itemTypeId, String itemName, String facilityName, Integer reorderQuantity, Integer reorderThreshold) {
        this.itemTypeId = itemTypeId;
        this.itemName = itemName;
        this.facilityName = facilityName;
        this.reorderQuantity = reorderQuantity;
        this.reorderThreshold = reorderThreshold;
    }

    /**
     * @return the itemTypeId
     */
    public Integer getItemTypeId() {
        return itemTypeId;
    }

    /**
     * @param itemTypeId the itemTypeId to set
     */
    public void setItemTypeId(Integer itemTypeId) {
        this.itemTypeId = itemTypeId;
    }

    /**
     * @return the itemName
     */
    public String getItemName() {
        return itemName;
    }

    /**
     * @param itemName the itemName to set
     */
    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    /**
     * @return the reorderQuantity
     */
    public Integer getReorderQuantity() {
        return reorderQuantity;
    }

    /**
     * @param reorderQuantity the reorderQuantity to set
     */
    public void setReorderQuantity(Integer reorderQuantity) {
        this.reorderQuantity = reorderQuantity;
    }

    /**
     * @return the reorderThreshold
     */
    public Integer getReorderThreshold() {
        return reorderThreshold;
    }

    /**
     * @param reorderThreshold the reorderThreshold to set
     */
    public void setReorderThreshold(Integer reorderThreshold) {
        this.reorderThreshold = reorderThreshold;
    }

    /**
     * @return the facilityName
     */
    public String getFacilityName() {
        return facilityName;
    }

    /**
     * @param facilityName the facilityName to set
     */
    public void setFacilityName(String facilityName) {
        this.facilityName = facilityName;
    }

}
