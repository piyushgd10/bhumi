/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Apr 12, 2015
 *  @author akshay
 */
package com.uniware.core.api.material;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;
import com.uniware.core.entity.ItemTypeInventory.Type;

public class EditNonTraceableGatePassItemRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 2176009574868572572L;
    
    @NotBlank
    private String            gatePassCode;

    @NotBlank
    private String            itemSKU;

    @NotNull
    private Type              inventoryType;

    @Min(value = 1)
    @NotNull
    private Integer           quantity;
    
    @NotBlank
    private String            shelfCode;

    /**
     * @return the gatePassCode
     */
    public String getGatePassCode() {
        return gatePassCode;
    }

    /**
     * @param gatePassCode the gatePassCode to set
     */
    public void setGatePassCode(String gatePassCode) {
        this.gatePassCode = gatePassCode;
    }

    /**
     * @return the itemSKU
     */
    public String getItemSKU() {
        return itemSKU;
    }

    /**
     * @param itemSKU the itemSKU to set
     */
    public void setItemSKU(String itemSKU) {
        this.itemSKU = itemSKU;
    }

    /**
     * @return the quantity
     */
    public Integer getQuantity() {
        return quantity;
    }

    /**
     * @param quantity the quantity to set
     */
    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Type getInventoryType() {
        return inventoryType;
    }

    public void setInventoryType(Type inventoryType) {
        this.inventoryType = inventoryType;
    }

    public String getShelfCode() {
        return shelfCode;
    }

    public void setShelfCode(String shelfCode) {
        this.shelfCode = shelfCode;
    }

}
