/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jan 23, 2012
 *  @author singla
 */
package com.uniware.core.api.shipping;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author Sunny
 */
public class RefreshShippingLabelResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -5104527497740336170L;

    private String            shippingLabelLink;

    public String getShippingLabelLink() {
        return shippingLabelLink;
    }

    public void setShippingLabelLink(String shippingLabelLink) {
        this.shippingLabelLink = shippingLabelLink;
    }

}
