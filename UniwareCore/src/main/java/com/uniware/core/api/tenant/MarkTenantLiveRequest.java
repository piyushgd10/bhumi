/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 14, 2012
 *  @author singla
 */
package com.uniware.core.api.tenant;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author Sunny
 */
public class MarkTenantLiveRequest extends ServiceRequest {

    private static final long serialVersionUID = -3749078213185359215L;

    private boolean           clearData;
    private boolean           clearAWBs;

    public boolean isClearData() {
        return clearData;
    }

    public void setClearData(boolean clearData) {
        this.clearData = clearData;
    }

    public boolean isClearAWBs() {
        return clearAWBs;
    }

    public void setClearAWBs(boolean clearAWBs) {
        this.clearAWBs = clearAWBs;
    }

}
