/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 25-Dec-2013
 *  @author sunny
 */
package com.uniware.core.api.systemnotification.sandbox;

import com.uniware.core.entity.Channel;
import com.uniware.core.entity.SaleOrder;
import com.uniware.core.entity.ShippingManifest;
import com.uniware.core.entity.ShippingPackage;
import com.uniware.core.entity.Source;

import java.util.Map;

public class SandboxParams {

    private Map<String, String>       channelConnectorParameters;
    private SandboxSaleOrderVO        saleOrder;
    private SandboxShippingPackageVO  shippingPackage;
    private SandboxChannelVO          channel;
    private SandboxSourceVO           source;
    private SandboxShippingManifestVO shippingManifest;
    private String                    saleOrderCode;

    public Map<String, String> getChannelConnectorParameters() {
        return channelConnectorParameters;
    }

    public void setChannelConnectorParameters(Map<String, String> channelParameters) {
        this.channelConnectorParameters = channelParameters;
    }

    public SandboxSaleOrderVO getSaleOrder() {
        return saleOrder;
    }

    public void setSaleOrder(SaleOrder so) {
        this.saleOrder = new SandboxSaleOrderVO(so);
    }

    public SandboxShippingPackageVO getShippingPackage() {
        return shippingPackage;
    }

    public void setShippingPackage(ShippingPackage sp) {
        this.shippingPackage = new SandboxShippingPackageVO(sp);
    }

    public SandboxChannelVO getChannel() {
        return channel;
    }

    public void setChannel(Channel channel) {
        this.channel = new SandboxChannelVO(channel);
    }

    public SandboxSourceVO getSource() {
        return source;
    }

    public void setSource(Source s) {
        this.source = new SandboxSourceVO(s);
    }

    public SandboxShippingManifestVO getShippingManifest() {
        return shippingManifest;
    }

    public void setShippingManifest(ShippingManifest sm) {
        this.shippingManifest = new SandboxShippingManifestVO(sm);
    }

    public String getSaleOrderCode() {
        return saleOrderCode;
    }

    public void setSaleOrderCode(String saleOrderCode) {
        this.saleOrderCode = saleOrderCode;
    }

}
