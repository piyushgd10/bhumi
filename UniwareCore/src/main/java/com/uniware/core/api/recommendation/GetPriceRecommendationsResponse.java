/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Nov 25, 2015
 *  @author akshay
 */
package com.uniware.core.api.recommendation;

import java.util.ArrayList;
import java.util.List;

import com.unifier.core.api.base.ServiceResponse;
import com.uniware.core.api.recommendation.dto.PriceRecommendationDTO;

public class GetPriceRecommendationsResponse extends ServiceResponse {

    private static final long serialVersionUID = 3813184303548645128L;

    private List<PriceRecommendationDTO> recommendations = new ArrayList<>();

    public List<PriceRecommendationDTO> getRecommendations() {
        return recommendations;
    }

    public void setRecommendations(List<PriceRecommendationDTO> recommendations) {
        this.recommendations = recommendations;
    }
    
}
