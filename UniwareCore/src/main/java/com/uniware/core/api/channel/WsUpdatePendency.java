/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 18-Jun-2013
 *  @author unicom
 */
package com.uniware.core.api.channel;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

public class WsUpdatePendency {

    @NotBlank
    private String  sellerSkuCode;

    @NotBlank
    private String  channelProductId;

    private String  channelProductIdentifier;

    @NotNull
    private Integer promisedQuantity;

    @NotNull
    private Integer outOfStockQuantity;

    public String getSellerSkuCode() {
        return sellerSkuCode;
    }

    public void setSellerSkuCode(String sellerSkuCode) {
        this.sellerSkuCode = sellerSkuCode;
    }

    public String getChannelProductId() {
        return channelProductId;
    }

    public void setChannelProductId(String channelProductId) {
        this.channelProductId = channelProductId;
    }

    public String getChannelProductIdentifier() {
        return channelProductIdentifier;
    }

    public void setChannelProductIdentifier(String channelProductIdentifier) {
        this.channelProductIdentifier = channelProductIdentifier;
    }

    public Integer getPromisedQuantity() {
        return promisedQuantity;
    }

    public void setPromisedQuantity(Integer promisedQuantity) {
        this.promisedQuantity = promisedQuantity;
    }

    public Integer getOutOfStockQuantity() {
        return outOfStockQuantity;
    }

    public void setOutOfStockQuantity(Integer outOfStockQuantity) {
        this.outOfStockQuantity = outOfStockQuantity;
    }

    @Override
    public String toString() {
        return "WsUpdatePendency [sellerSkuCode=" + sellerSkuCode + ", channelProductId=" + channelProductId + ", channelProductIdentifier=" + channelProductIdentifier
                + ", promisedQuantity=" + promisedQuantity + ", outOfStockQuantity=" + outOfStockQuantity + "]";
    }

}
