/*
 * Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 6/11/15 2:01 PM
 * @author unicom
 */

package com.uniware.core.api.channel;

import com.unifier.core.api.base.ServiceResponse;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by unicom on 06/11/15.
 */
public class GetConnectorStatusResponse extends ServiceResponse{
    String tenantCode;
    String tenantId;
    List<ChannelConnectorStatus> channelConnectorStatus;

    public String getTenantId() {
        return tenantId;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    public String getTenantCode() {
        return tenantCode;
    }

    public void setTenantCode(String tenantCode) {
        this.tenantCode = tenantCode;
    }

    public List<ChannelConnectorStatus> getChannelConnectorStatus() {
        return channelConnectorStatus;
    }

    public void setChannelConnectorStatus(List<ChannelConnectorStatus> channelConnectorStatus) {
        this.channelConnectorStatus = channelConnectorStatus;
    }

    public static class ChannelConnectorStatus {

        String code;
        String name;
        String sourceCode;
        String sourceConnectorName;
        String statusCode;

        public String getSourceCode() {
            return sourceCode;
        }

        public void setSourceCode(String sourceCode) {
            this.sourceCode = sourceCode;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getSourceConnectorName() {
            return sourceConnectorName;
        }

        public void setSourceConnectorName(String sourceConnectorName) {
            this.sourceConnectorName = sourceConnectorName;
        }

        public String getStatusCode() {
            return statusCode;
        }

        public void setStatusCode(String statusCode) {
            this.statusCode = statusCode;
        }
    }

}
