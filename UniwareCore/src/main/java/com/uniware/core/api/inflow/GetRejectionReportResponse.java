/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, May 3, 2012
 *  @author singla
 */
package com.uniware.core.api.inflow;

import com.unifier.core.api.base.ServiceResponse;
import com.unifier.core.api.email.EmailMessageDTO;

/**
 * @author singla
 */
public class GetRejectionReportResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 141215731302397118L;

    private EmailMessageDTO   emailMessage;

    /**
     * @return the emailMessage
     */
    public EmailMessageDTO getEmailMessage() {
        return emailMessage;
    }

    /**
     * @param emailMessage the emailMessage to set
     */
    public void setEmailMessage(EmailMessageDTO emailMessage) {
        this.emailMessage = emailMessage;
    }
}
