/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 03-Sep-2015
 *  @author parijat
 */
package com.uniware.core.api.facility;

import java.util.List;

import javax.validation.constraints.NotNull;

import com.unifier.core.api.base.ServiceRequest;
import com.uniware.core.api.channel.AddChannelConnectorRequest;
import com.uniware.core.api.channel.AddChannelRequest;

public class CreateFacilityChannelRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 8587656789L;

    @NotNull
    private AddChannelRequest                channelRequest;

    private List<AddChannelConnectorRequest> channelConnectorRequests;

    @NotNull
    private CreateFacilityRequest            facilityRequest;

    @NotNull
    private CreateFacilityProfileRequest     facilityProfileRequest;

    /**
     * @return the channelRequest
     */
    public AddChannelRequest getChannelRequest() {
        return channelRequest;
    }

    /**
     * @param channelRequest the channelRequest to set
     */
    public void setChannelRequest(AddChannelRequest channelRequest) {
        this.channelRequest = channelRequest;
    }

    /**
     * @return the channelConnectorRequests
     */
    public List<AddChannelConnectorRequest> getChannelConnectorRequests() {
        return channelConnectorRequests;
    }

    /**
     * @param channelConnectorRequests the channelConnectorRequests to set
     */
    public void setChannelConnectorRequests(List<AddChannelConnectorRequest> channelConnectorRequests) {
        this.channelConnectorRequests = channelConnectorRequests;
    }

    /**
     * @return the facilityRequest
     */
    public CreateFacilityRequest getFacilityRequest() {
        return facilityRequest;
    }

    /**
     * @param facilityRequest the facilityRequest to set
     */
    public void setFacilityRequest(CreateFacilityRequest facilityRequest) {
        this.facilityRequest = facilityRequest;
    }

    /**
     * @return the facilityProfileRequest
     */
    public CreateFacilityProfileRequest getFacilityProfileRequest() {
        return facilityProfileRequest;
    }

    /**
     * @param facilityProfileRequest the facilityProfileRequest to set
     */
    public void setFacilityProfileRequest(CreateFacilityProfileRequest facilityProfileRequest) {
        this.facilityProfileRequest = facilityProfileRequest;
    }

}
