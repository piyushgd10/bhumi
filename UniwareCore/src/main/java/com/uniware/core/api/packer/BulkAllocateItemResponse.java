/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 30-Jun-2013
 *  @author unicom
 */
package com.uniware.core.api.packer;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.unifier.core.api.base.ServiceResponse;
import com.unifier.core.api.validation.WsError;
import com.uniware.core.api.item.ItemDetailFieldDTO;

public class BulkAllocateItemResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long                           serialVersionUID = 234243234242L;
    private final Map<String, Integer>                  itemCodes        = new HashMap<String, Integer>();
    private final Map<String, List<WsError>>            failures         = new HashMap<String, List<WsError>>();
    private final Map<String, List<ItemDetailFieldDTO>> itemDetailFields = new HashMap<String, List<ItemDetailFieldDTO>>();

    /**
     * @return the itemDetailFields
     */
    public Map<String, List<ItemDetailFieldDTO>> getItemDetailFields() {
        return itemDetailFields;
    }

    /**
     * @return the itemCodes
     */
    public Map<String, Integer> getItemCodes() {
        return itemCodes;
    }

    /**
     * @return the failures
     */
    public Map<String, List<WsError>> getFailures() {
        return failures;
    }

}
