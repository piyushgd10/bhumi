/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 8, 2012
 *  @author singla
 */
package com.uniware.core.api.shipping;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.unifier.core.api.customfields.CustomFieldMetadataDTO;
import com.uniware.core.entity.ShippingManifest;

/**
 * @author singla
 */
public class ManifestDTO implements Serializable {
    /**
     * 
     */
    private static final long            serialVersionUID = 251059107113771397L;
    private Integer                      id;
    private String                       code;
    private String                       username;
    private String                       shippingProvider;
    private String                       shippingProviderCode;
    private String                       shippingManfestLink;
    private String                       shippingMethod;
    private String                       signatureLink;
    private String                       channel;
    private boolean                      cashOnDelivery;
    private Date                         created;
    private String                       status;
    private List<ManifestItemDTO>        manifestItems    = new ArrayList<ManifestItemDTO>();
    private List<CustomFieldMetadataDTO> customFieldValues;
    private ShippingManifestStatusDTO    shippingManifestStatus;
    private boolean                      fetchCurrentChannelManifestEnabled;

    public ManifestDTO() {
    }

    public ManifestDTO(ShippingManifest shippingManifest) {
        super();
        this.id = shippingManifest.getId();
        this.code = shippingManifest.getCode();
        this.username = shippingManifest.getUser().getUsername();
        this.signatureLink = shippingManifest.getSignatureLink();
        this.shippingProvider = shippingManifest.getShippingProviderCode() != null ? shippingManifest.getShippingProviderCode() : "ALL";
        this.shippingProviderCode = shippingManifest.getShippingProviderCode() != null ? shippingManifest.getShippingProviderCode() : "ALL";
        this.shippingManfestLink = shippingManifest.getManifestLink();
        this.created = shippingManifest.getCreated();
        this.status = shippingManifest.getStatusCode();
    }

    public String getSignatureLink() {
        return signatureLink;
    }

    public void setSignatureLink(String signatureLink) {
        this.signatureLink = signatureLink;
    }

    /**
     * @return the shippingProviderCode
     */
    public String getShippingProviderCode() {
        return shippingProviderCode;
    }

    /**
     * @param shippingProviderCode the shippingProviderCode to set
     */
    public void setShippingProviderCode(String shippingProviderCode) {
        this.shippingProviderCode = shippingProviderCode;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the shippingProvider
     */
    public String getShippingProvider() {
        return shippingProvider;
    }

    /**
     * @param shippingProvider the shippingProvider to set
     */
    public void setShippingProvider(String shippingProvider) {
        this.shippingProvider = shippingProvider;
    }

    /**
     * @return the shippingMethod
     */
    public String getShippingMethod() {
        return shippingMethod;
    }

    /**
     * @param shippingMethod the shippingMethod to set
     */
    public void setShippingMethod(String shippingMethod) {
        this.shippingMethod = shippingMethod;
    }

    /**
     * @return the created
     */
    public Date getCreated() {
        return created;
    }

    /**
     * @param created the created to set
     */
    public void setCreated(Date created) {
        this.created = created;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the manifestItems
     */
    public List<ManifestItemDTO> getManifestItems() {
        return manifestItems;
    }

    /**
     * @param manifestItems the manifestItems to set
     */
    public void setManifestItems(List<ManifestItemDTO> manifestItems) {
        this.manifestItems = manifestItems;
    }

    /**
     * @return the cashOnDelivery
     */
    public boolean isCashOnDelivery() {
        return cashOnDelivery;
    }

    /**
     * @param cashOnDelivery the cashOnDelivery to set
     */
    public void setCashOnDelivery(boolean cashOnDelivery) {
        this.cashOnDelivery = cashOnDelivery;
    }

    /**
     * @return the customFieldValues
     */
    public List<CustomFieldMetadataDTO> getCustomFieldValues() {
        return customFieldValues;
    }

    /**
     * @param customFieldValues the customFieldValues to set
     */
    public void setCustomFieldValues(List<CustomFieldMetadataDTO> customFieldValues) {
        this.customFieldValues = customFieldValues;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    /**
     * @return the shippingManfestLink
     */
    public String getShippingManfestLink() {
        return shippingManfestLink;
    }

    /**
     * @param shippingManfestLink the shippingManfestLink to set
     */
    public void setShippingManfestLink(String shippingManfestLink) {
        this.shippingManfestLink = shippingManfestLink;
    }

    public ShippingManifestStatusDTO getShippingManifestStatus() {
        return shippingManifestStatus;
    }

    public void setShippingManifestStatus(ShippingManifestStatusDTO shippingManifestStatus) {
        this.shippingManifestStatus = shippingManifestStatus;
    }

    public boolean isFetchCurrentChannelManifestEnabled() {
        return fetchCurrentChannelManifestEnabled;
    }

    public void setFetchCurrentChannelManifestEnabled(boolean fetchCurrentChannelManifestEnabled) {
        this.fetchCurrentChannelManifestEnabled = fetchCurrentChannelManifestEnabled;
    }

}
