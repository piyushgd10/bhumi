/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jan 10, 2012
 *  @author singla
 */
package com.uniware.core.api.saleorder.display;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.unifier.core.api.base.ServiceResponse;
import com.unifier.core.api.customfields.CustomFieldMetadataDTO;
import com.uniware.core.api.saleorder.AddressDTO;
import com.uniware.core.entity.SaleOrderItem;
import com.uniware.core.entity.ShippingPackage.ShippingManager;

public class GetSaleOrderShippingPackagesResponse extends ServiceResponse {

    /**
     *
     */
    private static final long serialVersionUID = 6284414547113409023L;

    public GetSaleOrderShippingPackagesResponse() {
    }

    private List<ShippingPackageFullDTO> shippingPackages;

    public List<ShippingPackageFullDTO> getShippingPackages() {
        return shippingPackages;
    }

    public void setShippingPackages(List<ShippingPackageFullDTO> shippingPackages) {
        this.shippingPackages = shippingPackages;
    }

    public static class ShippingPackageFullDTO {
        private String                        code;
        private String                        shippingProviderCode;
        private boolean                       providerEditable;
        private String                        shippingMethodCode;
        private String                        shippingPackageType;
        private AddressDTO                    shippingAddress;
        private String                        trackingStatus;
        private String                        courierStatus;
        private String                        zone;
        private BigDecimal                    estimatedWeight;
        private BigDecimal                    actualWeight;
        private Date                          created;
        private String                        reshipmentCode;
        private String                        parentPackageCode;
        private Integer                       noOfItems;
        private Integer                       noOfBoxes;
        private String                        invoiceCode;
        private String                        invoiceDisplayCode;
        private String                        returnInvoiceDisplayCode;
        private String                        returnInvoiceCode;
        private String                        picklistNumber;
        private Integer                       length;
        private Integer                       width;
        private Integer                       height;
        private String                        trackingNumber;
        private String                        shippingLabelLink;
        private String                        statusCode;
        private BigDecimal                    totalPrice;
        private BigDecimal                    collectableAmount;
        private Date                          dispatchTime;
        private Date                          deliveryTime;
        private boolean                       repackageable;
        private boolean                       requiresCustomization;
        private boolean                       splittable;
        private boolean                       podVerified;
        private List<ShippingPackageLineItem> shippingPackageLineItems = new ArrayList<ShippingPackageLineItem>();
        private List<CustomFieldMetadataDTO>  customFieldValues;
        private List<CustomFieldMetadataDTO>  saleOrderCustomFieldValues;
        private List<StateRegulatoryForm>     stateRegulatoryForms;
        private String                        reshipmentSaleOrderCode;
        private String                        shippingManifestCode;
        private String                        returnManifestCode;
        private String                        channelName;
        private String                        channelCode;
        private boolean                       thirdPartyShipping;
        private boolean                       refetchShippingLabelAllowed;
        private boolean                       orderRefreshEnabled;
        private int                           itemDetailingPending;
        private boolean                       productManagementSwitchedOff;
        private ShippingManager               shippingManager;

        public ShippingPackageFullDTO() {
        }

        public String getInvoiceDisplayCode() {
            return invoiceDisplayCode;
        }

        public void setInvoiceDisplayCode(String invoiceDisplayCode) {
            this.invoiceDisplayCode = invoiceDisplayCode;
        }

        public String getReturnInvoiceDisplayCode() {
            return returnInvoiceDisplayCode;
        }

        public void setReturnInvoiceDisplayCode(String returnInvoiceDisplayCode) {
            this.returnInvoiceDisplayCode = returnInvoiceDisplayCode;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getShippingProviderCode() {
            return shippingProviderCode;
        }

        public void setShippingProviderCode(String shippingProviderCode) {
            this.shippingProviderCode = shippingProviderCode;
        }

        public boolean isProviderEditable() {
            return providerEditable;
        }

        public void setProviderEditable(boolean providerEditable) {
            this.providerEditable = providerEditable;
        }

        public String getShippingMethodCode() {
            return shippingMethodCode;
        }

        public void setShippingMethodCode(String shippingMethodCode) {
            this.shippingMethodCode = shippingMethodCode;
        }

        public String getShippingPackageType() {
            return shippingPackageType;
        }

        public void setShippingPackageType(String shippingPackageType) {
            this.shippingPackageType = shippingPackageType;
        }

        public AddressDTO getShippingAddress() {
            return shippingAddress;
        }

        public void setShippingAddress(AddressDTO shippingAddress) {
            this.shippingAddress = shippingAddress;
        }

        public String getTrackingStatus() {
            return trackingStatus;
        }

        public void setTrackingStatus(String trackingStatus) {
            this.trackingStatus = trackingStatus;
        }

        public String getCourierStatus() {
            return courierStatus;
        }

        public void setCourierStatus(String courierStatus) {
            this.courierStatus = courierStatus;
        }

        public String getZone() {
            return zone;
        }

        public void setZone(String zone) {
            this.zone = zone;
        }

        public BigDecimal getEstimatedWeight() {
            return estimatedWeight;
        }

        public void setEstimatedWeight(BigDecimal estimatedWeight) {
            this.estimatedWeight = estimatedWeight;
        }

        public BigDecimal getActualWeight() {
            return actualWeight;
        }

        public void setActualWeight(BigDecimal actualWeight) {
            this.actualWeight = actualWeight;
        }

        public Date getCreated() {
            return created;
        }

        public void setCreated(Date created) {
            this.created = created;
        }

        public String getReshipmentCode() {
            return reshipmentCode;
        }

        public void setReshipmentCode(String reshipmentCode) {
            this.reshipmentCode = reshipmentCode;
        }

        public String getParentPackageCode() {
            return parentPackageCode;
        }

        public void setParentPackageCode(String parentPackageCode) {
            this.parentPackageCode = parentPackageCode;
        }

        public Integer getNoOfItems() {
            return noOfItems;
        }

        public void setNoOfItems(Integer noOfItems) {
            this.noOfItems = noOfItems;
        }

        public Integer getNoOfBoxes() {
            return noOfBoxes;
        }

        public void setNoOfBoxes(Integer noOfBoxes) {
            this.noOfBoxes = noOfBoxes;
        }

        public String getInvoiceCode() {
            return invoiceCode;
        }

        public void setInvoiceCode(String invoiceCode) {
            this.invoiceCode = invoiceCode;
        }

        public String getReturnInvoiceCode() {
            return returnInvoiceCode;
        }

        public void setReturnInvoiceCode(String returnInvoiceCode) {
            this.returnInvoiceCode = returnInvoiceCode;
        }

        public String getPicklistNumber() {
            return picklistNumber;
        }

        public void setPicklistNumber(String picklistNumber) {
            this.picklistNumber = picklistNumber;
        }

        public Integer getLength() {
            return length;
        }

        public void setLength(Integer length) {
            this.length = length;
        }

        public Integer getWidth() {
            return width;
        }

        public void setWidth(Integer width) {
            this.width = width;
        }

        public Integer getHeight() {
            return height;
        }

        public void setHeight(Integer height) {
            this.height = height;
        }

        public String getTrackingNumber() {
            return trackingNumber;
        }

        public void setTrackingNumber(String trackingNumber) {
            this.trackingNumber = trackingNumber;
        }

        public String getShippingLabelLink() {
            return shippingLabelLink;
        }

        public void setShippingLabelLink(String shippingLabelLink) {
            this.shippingLabelLink = shippingLabelLink;
        }

        public String getStatusCode() {
            return statusCode;
        }

        public void setStatusCode(String statusCode) {
            this.statusCode = statusCode;
        }

        public BigDecimal getTotalPrice() {
            return totalPrice;
        }

        public void setTotalPrice(BigDecimal totalPrice) {
            this.totalPrice = totalPrice;
        }

        public BigDecimal getCollectableAmount() {
            return collectableAmount;
        }

        public void setCollectableAmount(BigDecimal collectableAmount) {
            this.collectableAmount = collectableAmount;
        }

        public Date getDispatchTime() {
            return dispatchTime;
        }

        public void setDispatchTime(Date dispatchTime) {
            this.dispatchTime = dispatchTime;
        }

        public Date getDeliveryTime() {
            return deliveryTime;
        }

        public void setDeliveryTime(Date deliveryTime) {
            this.deliveryTime = deliveryTime;
        }

        public boolean isRepackageable() {
            return repackageable;
        }

        public void setRepackageable(boolean repackageable) {
            this.repackageable = repackageable;
        }

        public boolean isRequiresCustomization() {
            return requiresCustomization;
        }

        public void setRequiresCustomization(boolean requiresCustomization) {
            this.requiresCustomization = requiresCustomization;
        }

        public boolean isSplittable() {
            return splittable;
        }

        public void setSplittable(boolean splittable) {
            this.splittable = splittable;
        }

        public boolean isPodVerified() {
            return podVerified;
        }

        public void setPodVerified(boolean podVerified) {
            this.podVerified = podVerified;
        }

        public List<ShippingPackageLineItem> getShippingPackageLineItems() {
            return shippingPackageLineItems;
        }

        public void setShippingPackageLineItems(List<ShippingPackageLineItem> shippingPackageLineItems) {
            this.shippingPackageLineItems = shippingPackageLineItems;
        }

        public List<CustomFieldMetadataDTO> getCustomFieldValues() {
            return customFieldValues;
        }

        public void setCustomFieldValues(List<CustomFieldMetadataDTO> customFieldValues) {
            this.customFieldValues = customFieldValues;
        }

        public List<CustomFieldMetadataDTO> getSaleOrderCustomFieldValues() {
            return saleOrderCustomFieldValues;
        }

        public void setSaleOrderCustomFieldValues(List<CustomFieldMetadataDTO> saleOrderCustomFieldValues) {
            this.saleOrderCustomFieldValues = saleOrderCustomFieldValues;
        }

        public List<StateRegulatoryForm> getStateRegulatoryForms() {
            return stateRegulatoryForms;
        }

        public void setStateRegulatoryForms(List<StateRegulatoryForm> stateRegulatoryForms) {
            this.stateRegulatoryForms = stateRegulatoryForms;
        }

        public String getReshipmentSaleOrderCode() {
            return reshipmentSaleOrderCode;
        }

        public void setReshipmentSaleOrderCode(String reshipmentSaleOrderCode) {
            this.reshipmentSaleOrderCode = reshipmentSaleOrderCode;
        }

        public String getShippingManifestCode() {
            return shippingManifestCode;
        }

        public void setShippingManifestCode(String shippingManifestCode) {
            this.shippingManifestCode = shippingManifestCode;
        }

        public String getReturnManifestCode() {
            return returnManifestCode;
        }

        public void setReturnManifestCode(String returnManifestCode) {
            this.returnManifestCode = returnManifestCode;
        }

        public String getChannelName() {
            return channelName;
        }

        public void setChannelName(String channelName) {
            this.channelName = channelName;
        }

        public String getChannelCode() {
            return channelCode;
        }

        public void setChannelCode(String channelCode) {
            this.channelCode = channelCode;
        }

        public boolean isThirdPartyShipping() {
            return thirdPartyShipping;
        }

        public boolean isRefetchShippingLabelAllowed() {
            return refetchShippingLabelAllowed;
        }

        public void setRefetchShippingLabelAllowed(boolean refetchShippingLabelAllowed) {
            this.refetchShippingLabelAllowed = refetchShippingLabelAllowed;
        }

        public boolean isOrderRefreshEnabled() {
            return orderRefreshEnabled;
        }

        public void setOrderRefreshEnabled(boolean orderRefreshEnabled) {
            this.orderRefreshEnabled = orderRefreshEnabled;
        }

        public int getItemDetailingPending() {
            return itemDetailingPending;
        }

        public void setItemDetailingPending(int itemDetailingPendingCount) {
            this.itemDetailingPending = itemDetailingPendingCount;
        }

        public boolean isProductManagementSwitchedOff() {
            return productManagementSwitchedOff;
        }

        public void setProductManagementSwitchedOff(boolean productManagementSwitchedOff) {
            this.productManagementSwitchedOff = productManagementSwitchedOff;
        }

        public void setThirdPartyShipping(boolean thirdPartyShipping) {
            this.thirdPartyShipping = thirdPartyShipping;
        }

        public ShippingManager getShippingManager() {
            return shippingManager;
        }

        public void setShippingManager(ShippingManager shippingManager) {
            this.shippingManager = shippingManager;
        }

    }

    public static class ShippingPackageLineItem {
        private String                lineItemIdentifier;
        private List<WsSaleOrderItem> saleOrderItems;
        private String                itemName;
        private int                   quantity;
        private int                   cancelled;
        private String                hsnCode;
        private String                taxType;
        private String                picksetName;
        private String                picklistCode;
        private BigDecimal            taxPercentage;

        public String getHsnCode() {
            return hsnCode;
        }

        public void setHsnCode(String hsnCode) {
            this.hsnCode = hsnCode;
        }

        public String getLineItemIdentifier() {
            return lineItemIdentifier;
        }

        public void setLineItemIdentifier(String lineItemIdentifier) {
            this.lineItemIdentifier = lineItemIdentifier;
        }

        public String getItemName() {
            return itemName;
        }

        public void setItemName(String itemName) {
            this.itemName = itemName;
        }

        public int getQuantity() {
            return quantity;
        }

        public void setQuantity(int quantity) {
            this.quantity = quantity;
        }

        public List<WsSaleOrderItem> getSaleOrderItems() {
            return saleOrderItems;
        }

        public void setSaleOrderItems(List<WsSaleOrderItem> saleOrderItems) {
            this.saleOrderItems = saleOrderItems;
        }

        public int getCancelled() {
            return cancelled;
        }

        public void setCancelled(int cancelled) {
            this.cancelled = cancelled;
        }

        public String getTaxType() {
            return taxType;
        }

        public void setTaxType(String taxType) {
            this.taxType = taxType;
        }

        public String getPicksetName() {
            return picksetName;
        }

        public void setPicksetName(String picksetName) {
            this.picksetName = picksetName;
        }

        public String getPicklistCode() {
            return picklistCode;
        }

        public void setPicklistCode(String picklistCode) {
            this.picklistCode = picklistCode;
        }

        public BigDecimal getTaxPercentage() {
            return taxPercentage;
        }

        public void setTaxPercentage(BigDecimal taxPercentage) {
            this.taxPercentage = taxPercentage;
        }

        public static class WsSaleOrderItem {
            private String  code;
            private boolean cancelled;
            private boolean onHold;

            public WsSaleOrderItem() {
            }

            public WsSaleOrderItem(SaleOrderItem soi) {
                this.code = soi.getCode();
                this.cancelled = SaleOrderItem.StatusCode.CANCELLED.name().equals(soi.getStatusCode());
                this.onHold = soi.isOnHold();
            }

            public String getCode() {
                return code;
            }

            public void setCode(String code) {
                this.code = code;
            }

            public boolean isOnHold() {
                return onHold;
            }

            public void setOnHold(boolean onHold) {
                this.onHold = onHold;
            }

            public boolean isCancelled() {
                return cancelled;
            }

            public void setCancelled(boolean cancelled) {
                this.cancelled = cancelled;
            }

        }

    }

    public static class StateRegulatoryForm {
        private String name;
        private String code;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

    }

    public static class LineItemKey {
        private String identifier;
        private String picksetName;
        private String picklistCode;

        public LineItemKey(String identifier, String picksetName, String picklistCode) {
            this.identifier = identifier;
            this.picksetName = picksetName;
            this.picklistCode = picklistCode;
        }

        public String getIdentifier() {
            return identifier;
        }

        public void setIdentifier(String identifier) {
            this.identifier = identifier;
        }

        public String getPicksetName() {
            return picksetName;
        }

        public void setPicksetName(String picksetName) {
            this.picksetName = picksetName;
        }

        public String getPicklistCode() {
            return picklistCode;
        }

        public void setPicklistCode(String picklistCode) {
            this.picklistCode = picklistCode;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o)
                return true;
            if (!(o instanceof LineItemKey))
                return false;

            LineItemKey lineItemKey = (LineItemKey) o;

            if (!identifier.equals(lineItemKey.identifier))
                return false;
            if (picksetName != null ? !picksetName.equals(lineItemKey.picksetName) : lineItemKey.picksetName != null)
                return false;
            return picklistCode != null ? picklistCode.equals(lineItemKey.picklistCode) : lineItemKey.picklistCode == null;
        }

        @Override
        public int hashCode() {
            int result = identifier.hashCode();
            result = 31 * result + (picksetName != null ? picksetName.hashCode() : 0);
            result = 31 * result + (picklistCode != null ? picklistCode.hashCode() : 0);
            return result;
        }
    }
}
