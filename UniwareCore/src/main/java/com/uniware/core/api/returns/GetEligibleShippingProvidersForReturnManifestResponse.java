/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 05-Feb-2015
 *  @author akshaykochhar
 */
package com.uniware.core.api.returns;

import java.util.List;

import com.unifier.core.api.base.ServiceResponse;
import com.uniware.core.api.admin.shipping.ShippingProviderSearchDTO;

public class GetEligibleShippingProvidersForReturnManifestResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 2429703731025989412L;
    
    private List<ShippingProviderSearchDTO> shippingProviders;

    public List<ShippingProviderSearchDTO> getShippingProviders() {
        return shippingProviders;
    }

    public void setShippingProviders(List<ShippingProviderSearchDTO> shippingProviders) {
        this.shippingProviders = shippingProviders;
    }
}
