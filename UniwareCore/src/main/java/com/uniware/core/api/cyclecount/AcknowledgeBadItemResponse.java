package com.uniware.core.api.cyclecount;

import com.unifier.core.api.base.ServiceResponse;

/**
 * Created by harshpal on 22/04/16.
 */
public class AcknowledgeBadItemResponse extends ServiceResponse {

    public GetItemResponse.ItemDTO item;

    public GetItemResponse.ItemDTO getItem() {
        return item;
    }

    public void setItem(GetItemResponse.ItemDTO item) {
        this.item = item;
    }
}
