/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 03-Jun-2012
 *  @author vibhu
 */
package com.uniware.core.api.admin.shipping;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author Pankaj
 */
public class DeleteAwbNumberRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -4618695161133805711L;

    @NotBlank
    private String            shippingProviderCode;

    @NotBlank
    private String            shippingMethodName;

    /**
     * @return the shippingProviderCode
     */
    public String getShippingProviderCode() {
        return shippingProviderCode;
    }

    /**
     * @param shippingProviderCode the shippingProviderCode to set
     */
    public void setShippingProviderCode(String shippingProviderCode) {
        this.shippingProviderCode = shippingProviderCode;
    }

    /**
     * @return the shippingMethodName
     */
    public String getShippingMethodName() {
        return shippingMethodName;
    }

    /**
     * @param shippingMethodName the shippingMethodName to set
     */
    public void setShippingMethodName(String shippingMethodName) {
        this.shippingMethodName = shippingMethodName;
    }

}
