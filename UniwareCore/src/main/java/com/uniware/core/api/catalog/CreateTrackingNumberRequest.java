/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 26, 2012
 *  @author praveeng
 */
package com.uniware.core.api.catalog;

import com.unifier.core.api.base.ServiceRequest;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author praveeng
 */
public class CreateTrackingNumberRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 6231618579254763556L;

    @NotEmpty
    private String            shippingMethodCode;

    private boolean           COD;

    @NotNull
    private String            trackingNumber;

    @NotEmpty
    private String            shippingProviderCode;

    public String getShippingMethodCode() {
        return shippingMethodCode;
    }

    public void setShippingMethodCode(String shippingMethodCode) {
        this.shippingMethodCode = shippingMethodCode;
    }

    public String getTrackingNumber() {
        return trackingNumber;
    }

    public void setTrackingNumber(String trackingNumber) {
        this.trackingNumber = trackingNumber;
    }

    public String getShippingProviderCode() {
        return shippingProviderCode;
    }

    public void setShippingProviderCode(String shippingProviderCode) {
        this.shippingProviderCode = shippingProviderCode;
    }

    public boolean isCOD() {
        return COD;
    }

    public void setCOD(boolean cOD) {
        COD = cOD;
    }

}
