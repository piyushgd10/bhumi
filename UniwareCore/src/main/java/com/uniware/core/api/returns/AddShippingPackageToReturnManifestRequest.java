/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, May 26, 2012
 *  @author singla
 */
package com.uniware.core.api.returns;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author singla
 */
public class AddShippingPackageToReturnManifestRequest extends ServiceRequest {

    /**
     *
     */
    private static final long serialVersionUID = 6785610901653956311L;

    @NotBlank
    private String returnManifestCode;

    @NotNull
    private String awbOrShipmentCode;

    /**
     * @return the awbOrShipmentCode
     */
    public String getAwbOrShipmentCode() {
        return awbOrShipmentCode;
    }

    /**
     * @param awbOrShipmentCode the awbOrShipmentCode to set
     */
    public void setAwbOrShipmentCode(String awbOrShipmentCode) {
        this.awbOrShipmentCode = awbOrShipmentCode;
    }

    /**
     * @return the returnManifestCode
     */
    public String getReturnManifestCode() {
        return returnManifestCode;
    }

    /**
     * @param returnManifestCode the returnManifestCode to set
     */
    public void setReturnManifestCode(String returnManifestCode) {
        this.returnManifestCode = returnManifestCode;
    }

}
