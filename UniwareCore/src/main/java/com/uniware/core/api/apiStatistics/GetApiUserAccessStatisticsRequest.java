/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 24-Mar-2014
 *  @author parijat
 */
package com.uniware.core.api.apiStatistics;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author parijat
 */
public class GetApiUserAccessStatisticsRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -4472901761887415014L;

    @NotBlank
    private String            apiUserName;

    public String getApiUserName() {
        return apiUserName;
    }

    public void setApiUserName(String apiUserName) {
        this.apiUserName = apiUserName;
    }

}
