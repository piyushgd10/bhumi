/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 30-Jul-2012
 *  @author praveeng
 */
package com.uniware.core.api.purchase;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author praveeng
 */
public class SearchItemTypeBackOrdersRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 6962467505713914948L;
    private String            itemSKU;

    /**
     * @return the itemSKU
     */
    public String getItemSKU() {
        return itemSKU;
    }

    /**
     * @param itemSKU the itemSKU to set
     */
    public void setItemSKU(String itemSKU) {
        this.itemSKU = itemSKU;
    }

}
