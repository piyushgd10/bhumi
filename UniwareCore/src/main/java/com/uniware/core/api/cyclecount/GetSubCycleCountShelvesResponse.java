package com.uniware.core.api.cyclecount;

import com.unifier.core.api.base.ServiceResponse;

/**
 * Created by harshpal on 20/04/16.
 */
public class GetSubCycleCountShelvesResponse extends ServiceResponse {

    private long             resultCount;

    private SubCycleCountDTO subCycleCount;

    public SubCycleCountDTO getSubCycleCount() {
        return subCycleCount;
    }

    public void setSubCycleCount(SubCycleCountDTO subCycleCount) {
        this.subCycleCount = subCycleCount;
    }

    public long getResultCount() {
        return resultCount;
    }

    public void setResultCount(long resultCount) {
        this.resultCount = resultCount;
    }
}
