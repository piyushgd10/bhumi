/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 26-Apr-2013
 *  @author pankaj
 */
package com.uniware.core.api.shipping;

import java.math.BigDecimal;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;
import com.uniware.core.api.shipping.WsShippingBox;

/**
 * @author pankaj
 */
public class EditShippingPackageDetailRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 5483329287181658392L;

    @NotBlank
    private String            shippingPackageCode;

    @NotNull
    private String            shippingPackageTypeCode;

    private BigDecimal        actualWeight;

    @Valid
    private WsShippingBox     shippingBox;

    /**
     * @return the shippingPackageCode
     */
    public String getShippingPackageCode() {
        return shippingPackageCode;
    }

    /**
     * @param shippingPackageCode the shippingPackageCode to set
     */
    public void setShippingPackageCode(String shippingPackageCode) {
        this.shippingPackageCode = shippingPackageCode;
    }

    /**
     * @return the shippingPackageTypeCode
     */
    public String getShippingPackageTypeCode() {
        return shippingPackageTypeCode;
    }

    /**
     * @param shippingPackageTypeCode the shippingPackageTypeCode to set
     */
    public void setShippingPackageTypeCode(String shippingPackageTypeCode) {
        this.shippingPackageTypeCode = shippingPackageTypeCode;
    }

    /**
     * @return the actualWeight
     */
    public BigDecimal getActualWeight() {
        return actualWeight;
    }

    /**
     * @param actualWeight the actualWeight to set
     */
    public void setActualWeight(BigDecimal actualWeight) {
        this.actualWeight = actualWeight;
    }

    /**
     * @return the shippingBox
     */
    public WsShippingBox getShippingBox() {
        return shippingBox;
    }

    /**
     * @param shippingBox the shippingBox to set
     */
    public void setShippingBox(WsShippingBox shippingBox) {
        this.shippingBox = shippingBox;
    }

}
