/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 05-Feb-2015
 *  @author akshaykochhar
 */
package com.uniware.core.api.returns;

import com.unifier.core.api.base.ServiceRequest;

public class GetEligibleShippingProvidersForReturnManifestRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -6250166586700374282L;

}
