/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 23-Jul-2013
 *  @author unicom
 */
package com.uniware.core.api.party;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.unifier.core.api.base.ServiceRequest;

public class CreateOrEditVendorItemTypeRequest extends ServiceRequest {

    /**
     * unicom
     */
    private static final long serialVersionUID = 6663920833110937379L;

    @NotNull
    Integer                   UserId;
    @NotNull
    @Valid
    private WsVendorItemType  vendorItemType;

    public Integer getUserId() {
        return UserId;
    }

    public void setUserId(Integer userId) {
        UserId = userId;
    }

    /**
     * @return the vendorItemType
     */
    public WsVendorItemType getVendorItemType() {
        return vendorItemType;
    }

    /**
     * @param vendorItemType the vendorItemType to set
     */
    public void setVendorItemType(WsVendorItemType vendorItemType) {
        this.vendorItemType = vendorItemType;
    }

}
