package com.uniware.core.api.saleorder;

import java.util.ArrayList;
import java.util.List;

import com.unifier.core.api.base.ServiceResponse;

public class AddOrEditSaleOrderItemsResponse extends ServiceResponse {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9027840875559708477L;
	
	private List<SaleOrderItemLightDTO> saleOrderItemLightDTOs = new ArrayList<SaleOrderItemLightDTO>();

	/**
	 * @return the saleOrderItemLightDTOs
	 */
	public List<SaleOrderItemLightDTO> getSaleOrderItemLightDTOs() {
		return saleOrderItemLightDTOs;
	}

	/**
	 * @param saleOrderItemLightDTOs the saleOrderItemLightDTOs to set
	 */
	public void setSaleOrderItemLightDTOs(
			List<SaleOrderItemLightDTO> saleOrderItemLightDTOs) {
		this.saleOrderItemLightDTOs = saleOrderItemLightDTOs;
	}
	
}
