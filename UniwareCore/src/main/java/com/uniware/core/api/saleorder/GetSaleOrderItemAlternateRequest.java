/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 10-Aug-2012
 *  @author vibhu
 */
package com.uniware.core.api.saleorder;

import com.unifier.core.api.base.ServiceRequest;

import javax.validation.constraints.NotNull;

/**
 * @author vibhu
 */
public class GetSaleOrderItemAlternateRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -2258216951736826020L;

    @NotNull
    private Integer           saleOrderItemAlternateId;

    /**
     * @return the saleOrderItemAlternateId
     */
    public Integer getSaleOrderItemAlternateId() {
        return saleOrderItemAlternateId;
    }

    /**
     * @param saleOrderItemAlternateId the saleOrderItemAlternateId to set
     */
    public void setSaleOrderItemAlternateId(Integer saleOrderItemAlternateId) {
        this.saleOrderItemAlternateId = saleOrderItemAlternateId;
    }

}
