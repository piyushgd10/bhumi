/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 19, 2012
 *  @author praveeng
 */
package com.uniware.core.api.material;

import com.unifier.core.api.base.ServiceResponse;

public class CreateInboundGatePassResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long  serialVersionUID = 1238808324906830823L;

    private InboundGatePassDTO inboundGatePassDTO;

    /**
     * @return the inboundGatePassDTO
     */
    public InboundGatePassDTO getInboundGatePassDTO() {
        return inboundGatePassDTO;
    }

    /**
     * @param inboundGatePassDTO the inboundGatePassDTO to set
     */
    public void setInboundGatePassDTO(InboundGatePassDTO inboundGatePassDTO) {
        this.inboundGatePassDTO = inboundGatePassDTO;
    }

}
