/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 15-May-2014
 *  @author unicom
 */
package com.uniware.core.api.saleorder;

import com.unifier.core.api.base.ServiceResponse;

public class RemoveMappingErrorRetrySaleOrderResponse extends ServiceResponse {

    private static final long serialVersionUID = -1625715386633175087L;

    private int               numOfOrdersUpdated;

    private int               successfulOrderCount;

    public int getNumOfOrdersUpdated() {
        return numOfOrdersUpdated;
    }

    public void setNumOfOrdersUpdated(int numOfOrdersUpdated) {
        this.numOfOrdersUpdated = numOfOrdersUpdated;
    }

    public int getSuccessfulOrderCount() {
        return successfulOrderCount;
    }

    public void setSuccessfulOrderCount(int successfulOrderCount) {
        this.successfulOrderCount = successfulOrderCount;
    }

}
