package com.uniware.core.api.invoice;

import com.unifier.core.api.base.ServiceRequest;
import com.uniware.core.api.model.WsInvoice;

import org.hibernate.validator.constraints.NotBlank;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created by piyush on 8/26/15.
 */
public class CreateInvoiceWithDetailsRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 8990556112434344157L;

    @NotBlank
    private String                  saleOrderCode;

    @Valid
    private WsInvoice               invoice;

    @NotNull
    private Integer                 userId;

    private String                  shippingProviderCode;

    private String                  trackingNumber;

    public String getSaleOrderCode() {
        return saleOrderCode;
    }

    public void setSaleOrderCode(String saleOrderCode) {
        this.saleOrderCode = saleOrderCode;
    }

    public WsInvoice getInvoice() {
        return invoice;
    }

    public void setInvoice(WsInvoice invoice) {
        this.invoice = invoice;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getShippingProviderCode() {
        return shippingProviderCode;
    }

    public void setShippingProviderCode(String shippingProviderCode) {
        this.shippingProviderCode = shippingProviderCode;
    }

    public String getTrackingNumber() {
        return trackingNumber;
    }

    public void setTrackingNumber(String trackingNumber) {
        this.trackingNumber = trackingNumber;
    }
}
