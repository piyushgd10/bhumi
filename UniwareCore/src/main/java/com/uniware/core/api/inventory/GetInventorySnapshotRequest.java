/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Nov 27, 2012
 *  @author singla
 */
package com.uniware.core.api.inventory;

import java.util.List;

import com.unifier.core.api.base.ServiceRequest;

public class GetInventorySnapshotRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -7511882452414977884L;

    private List<String>      itemTypeSKUs;

    private Integer           updatedSinceInMinutes;

    /**
     * @return the itemTypeSKUs
     */
    public List<String> getItemTypeSKUs() {
        return itemTypeSKUs;
    }

    /**
     * @param itemTypeSKUs the itemTypeSKUs to set
     */
    public void setItemTypeSKUs(List<String> itemTypeSKUs) {
        this.itemTypeSKUs = itemTypeSKUs;
    }

    public Integer getUpdatedSinceInMinutes() {
        return updatedSinceInMinutes;
    }

    public void setUpdatedSinceInMinutes(Integer updatedSinceInMinutes) {
        this.updatedSinceInMinutes = updatedSinceInMinutes;
    }

}
