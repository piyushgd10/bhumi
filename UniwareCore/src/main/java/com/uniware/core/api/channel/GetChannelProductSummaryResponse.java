/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jun 20, 2012
 *  @author singla
 */
package com.uniware.core.api.channel;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author Sunny
 */
public class GetChannelProductSummaryResponse extends ServiceResponse {

    /**
     *
     */
    private static final long serialVersionUID = -8331014310944611193L;
    private long liveProductCount;
    private long nonLiveProductCount;
    private long mappedProductCount;
    private long unmappedProductCount;
    private long ignoredProductCount;

    public long getLiveProductCount() {
        return liveProductCount;
    }

    public void setLiveProductCount(long liveProductCount) {
        this.liveProductCount = liveProductCount;
    }

    public long getNonLiveProductCount() {
        return nonLiveProductCount;
    }

    public void setNonLiveProductCount(long nonLiveProductCount) {
        this.nonLiveProductCount = nonLiveProductCount;
    }

    public long getMappedProductCount() {
        return mappedProductCount;
    }

    public void setMappedProductCount(long mappedProductCount) {
        this.mappedProductCount = mappedProductCount;
    }

    public long getUnmappedProductCount() {
        return unmappedProductCount;
    }

    public void setUnmappedProductCount(long unmappedProductCount) {
        this.unmappedProductCount = unmappedProductCount;
    }

    public long getIgnoredProductCount() {
        return ignoredProductCount;
    }

    public void setIgnoredProductCount(long ignoredProductCount) {
        this.ignoredProductCount = ignoredProductCount;
    }
}
