/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 06-Jun-2012
 *  @author vibhu
 */
package com.uniware.core.api.item;

import com.unifier.core.api.base.ServiceRequest;

import org.hibernate.validator.constraints.NotBlank;

/**
 * @author vibhu
 */
public class GetItemTypeDetailRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -8743329813684020354L;

    @NotBlank
    private String            itemSKU;

    /**
     * @return the itemSKU
     */
    public String getItemSKU() {
        return itemSKU;
    }

    /**
     * @param itemSKU the itemSKU to set
     */
    public void setItemSKU(String itemSKU) {
        this.itemSKU = itemSKU;
    }

}
