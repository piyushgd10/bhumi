/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 09-Oct-2014
 *  @author parijat
 */
package com.uniware.core.api.channel;

import java.util.List;

import javax.validation.Valid;

import org.hibernate.validator.constraints.NotBlank;

public class WsChannelReconciliationInvoice {

    @NotBlank
    private String                                   channelCode;
    @NotBlank
    private String                                   invoiceCode;
    @NotBlank
    private String                                   settlementDate;

    private String                                   downloadUrl;

    @NotBlank
    private String                                   depositDate;

    private double                                   totalSettlemenValue = 0;

    @Valid
    private List<WsChannelReconciliationInvoiceItem> invoiceItems;

    /**
     * @return the channelCode
     */
    public String getChannelCode() {
        return channelCode;
    }

    /**
     * @param channelCode the channelCode to set
     */
    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    /**
     * @return the invoiceCode
     */
    public String getInvoiceCode() {
        return invoiceCode;
    }

    /**
     * @param invoiceCode the invoiceCode to set
     */
    public void setInvoiceCode(String invoiceCode) {
        this.invoiceCode = invoiceCode;
    }

    /**
     * @return the settlementDate
     */
    public String getSettlementDate() {
        return settlementDate;
    }

    /**
     * @param settlementDate the settlementDate to set
     */
    public void setSettlementDate(String settlementDate) {
        this.settlementDate = settlementDate;
    }

    public String getDownloadUrl() {
        return downloadUrl;
    }

    public void setDownloadUrl(String downloadUrl) {
        this.downloadUrl = downloadUrl;
    }

    /**
     * @return the depositDate
     */
    public String getDepositDate() {
        return depositDate;
    }

    /**
     * @param depositDate the depositDate to set
     */
    public void setDepositDate(String depositDate) {
        this.depositDate = depositDate;
    }

    public double getTotalSettlemenValue() {
        return totalSettlemenValue;
    }

    public void setTotalSettlemenValue(double totalSettlemenValue) {
        this.totalSettlemenValue = totalSettlemenValue;
    }

    /**
     * @return the invoiceItems
     */
    public List<WsChannelReconciliationInvoiceItem> getInvoiceItems() {
        return invoiceItems;
    }

    /**
     * @param invoiceItems the invoiceItems to set
     */
    public void setInvoiceItems(List<WsChannelReconciliationInvoiceItem> invoiceItems) {
        this.invoiceItems = invoiceItems;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */

    @Override
    public String toString() {
        return "WsChannelReconciliationInvoice{" + "channelCode='" + channelCode + '\'' + ", invoiceCode='" + invoiceCode + '\'' + ", settlementDate='" + settlementDate + '\''
                + ", depositDate='" + depositDate + '\'' + ", totalSettlemenValue=" + totalSettlemenValue + ", invoiceItems=" + invoiceItems + '}';
    }
}
