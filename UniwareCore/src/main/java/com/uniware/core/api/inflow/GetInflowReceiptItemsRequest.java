/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Apr 14, 2012
 *  @author praveeng
 */
package com.uniware.core.api.inflow;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author praveeng
 */
public class GetInflowReceiptItemsRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -523651226286981473L;
    private String            inflowReceiptCode;

    /**
     * @return the inflowReceiptCode
     */
    public String getInflowReceiptCode() {
        return inflowReceiptCode;
    }

    /**
     * @param inflowReceiptCode the inflowReceiptCode to set
     */
    public void setInflowReceiptCode(String inflowReceiptCode) {
        this.inflowReceiptCode = inflowReceiptCode;
    }

}
