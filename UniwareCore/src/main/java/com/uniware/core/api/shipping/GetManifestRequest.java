/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jun 21, 2015
 *  @author akshay
 */
package com.uniware.core.api.shipping;

import javax.validation.constraints.NotNull;

import com.unifier.core.api.base.ServiceRequest;

public class GetManifestRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -2982495869932696452L;
    @NotNull
    private String            shippingManifestCode;

    /**
     * 
     */
    public GetManifestRequest() {
    }

    /**
     * @param shippingManifestId
     */
    public GetManifestRequest(String shippingManifestCode) {
        this.shippingManifestCode = shippingManifestCode;
    }

    /**
     * @return the shippingManifestCode
     */
    public String getShippingManifestCode() {
        return shippingManifestCode;
    }

    /**
     * @param shippingManifestCode the shippingManifestCode to set
     */
    public void setShippingManifestCode(String shippingManifestCode) {
        this.shippingManifestCode = shippingManifestCode;
    }
    
}
