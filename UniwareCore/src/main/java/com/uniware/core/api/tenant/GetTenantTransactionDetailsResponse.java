/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 07-May-2014
 *  @author akshay
 */
package com.uniware.core.api.tenant;

import com.unifier.core.api.base.ServiceResponse;

public class GetTenantTransactionDetailsResponse extends ServiceResponse{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    public TenantTransactionDetailDTO tenantTransactionDetailDTO;
    
    public TenantTransactionDetailDTO getTenantTransactionDetailDTO() {
        return tenantTransactionDetailDTO;
    }

    public void setTenantTransactionDetailDTO(TenantTransactionDetailDTO clientTransactionDetailDTO) {
        this.tenantTransactionDetailDTO = clientTransactionDetailDTO;
    }

}
