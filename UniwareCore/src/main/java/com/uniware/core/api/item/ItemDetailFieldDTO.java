/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 15-May-2012
 *  @author vibhu
 */
package com.uniware.core.api.item;

import com.uniware.core.entity.ItemDetailField;

import java.io.Serializable;

/**
 * @author vibhu
 */
public class ItemDetailFieldDTO implements Serializable {

    private int    id;
    private String name;
    private String displayName;
    private String validatorPattern;

    public ItemDetailFieldDTO() {

    }

    public ItemDetailFieldDTO(ItemDetailField itemDetailField) {
        this.id = itemDetailField.getId();
        this.name = itemDetailField.getName();
        this.displayName = itemDetailField.getDisplayName();
        this.validatorPattern = itemDetailField.getValidatorPattern();
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the displayName
     */
    public String getDisplayName() {
        return displayName;
    }

    /**
     * @param displayName the displayName to set
     */
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getValidatorPattern() {
        return validatorPattern;
    }

    public void setValidatorPattern(String validatorPattern) {
        this.validatorPattern = validatorPattern;
    }

}