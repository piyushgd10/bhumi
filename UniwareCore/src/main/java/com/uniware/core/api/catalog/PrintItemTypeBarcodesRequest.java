/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 26-Mar-2014
 *  @author amit
 */
package com.uniware.core.api.catalog;

import java.util.HashMap;
import java.util.Map;

import com.unifier.core.api.base.ServiceRequest;

public class PrintItemTypeBarcodesRequest extends ServiceRequest {

    private static final long    serialVersionUID = -6777217405368853480L;

    private Map<String, Integer> skuToQtyMap      = new HashMap<String, Integer>();

    public PrintItemTypeBarcodesRequest() {
        super();
    }

    public Map<String, Integer> getSkuToQtyMap() {
        return skuToQtyMap;
    }

    public void setSkuToQtyMap(Map<String, Integer> skuToQtyMap) {
        this.skuToQtyMap = skuToQtyMap;
    }
}
