/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 25-Mar-2014
 *  @author karunsingla
 */
package com.uniware.core.api.dual.company;

import com.unifier.core.api.base.ServiceResponse;

public class ReceiveReversePickupFromRetailResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -6269443988534707920L;

}
