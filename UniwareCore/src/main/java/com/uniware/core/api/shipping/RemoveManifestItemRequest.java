/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 17-Jun-2013
 *  @author pankaj
 */
package com.uniware.core.api.shipping;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

public class RemoveManifestItemRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 1044511041185967721L;

    @NotBlank
    private String            manifestCode;
    @NotBlank
    private String            shippingPackageCode;

    public RemoveManifestItemRequest() {
        super();
    }

    public RemoveManifestItemRequest(String manifestCode, String shippingPackageCode) {
        super();
        this.manifestCode = manifestCode;
        this.shippingPackageCode = shippingPackageCode;
    }

    /**
     * @return the manifestCode
     */
    public String getManifestCode() {
        return manifestCode;
    }

    /**
     * @param manifestCode the manifestCode to set
     */
    public void setManifestCode(String manifestCode) {
        this.manifestCode = manifestCode;
    }

    /**
     * @return the shippingPackageCode
     */
    public String getShippingPackageCode() {
        return shippingPackageCode;
    }

    /**
     * @param shippingPackageCode the shippingPackageCode to set
     */
    public void setShippingPackageCode(String shippingPackageCode) {
        this.shippingPackageCode = shippingPackageCode;
    }

}
