/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Apr 10, 2012
 *  @author praveeng
 */
package com.uniware.core.api.purchase;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.uniware.core.api.party.dto.VendorAgreementDTO;

/**
 * @author praveeng
 */
public class PurchaseCartDTO {
    private Integer                            id;
    private Map<Integer, PurchaseCartItemsDTO> vendorToPurchaseCartItems = new HashMap<Integer, PurchaseCartItemsDTO>();

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the vendorToPurchaseCartItems
     */
    public Map<Integer, PurchaseCartItemsDTO> getVendorToPurchaseCartItems() {
        return vendorToPurchaseCartItems;
    }

    /**
     * @param vendorToPurchaseCartItems the vendorToPurchaseCartItems to set
     */
    public void setVendorToPurchaseCartItems(Map<Integer, PurchaseCartItemsDTO> vendorToPurchaseCartItems) {
        this.vendorToPurchaseCartItems = vendorToPurchaseCartItems;
    }

    public static class PurchaseCartItemsDTO {
        private Integer                   vendorId;
        private String                    vendorCode;
        private String                    vendorName;
        private List<VendorAgreementDTO>  vendorAgreements  = new ArrayList<VendorAgreementDTO>();
        private List<PurchaseCartItemDTO> purchaseCartItems = new ArrayList<PurchaseCartItemDTO>();

        /**
         * @return the vendorId
         */
        public Integer getVendorId() {
            return vendorId;
        }

        /**
         * @param vendorId the vendorId to set
         */
        public void setVendorId(Integer vendorId) {
            this.vendorId = vendorId;
        }

        /**
         * @return the vendorName
         */
        public String getVendorName() {
            return vendorName;
        }

        /**
         * @param vendorName the vendorName to set
         */
        public void setVendorName(String vendorName) {
            this.vendorName = vendorName;
        }

        /**
         * @return the vendorAgreements
         */
        public List<VendorAgreementDTO> getVendorAgreements() {
            return vendorAgreements;
        }

        /**
         * @param vendorAgreements the vendorAgreements to set
         */
        public void setVendorAgreements(List<VendorAgreementDTO> vendorAgreements) {
            this.vendorAgreements = vendorAgreements;
        }

        /**
         * @return the purchaseCartItems
         */
        public List<PurchaseCartItemDTO> getPurchaseCartItems() {
            return purchaseCartItems;
        }

        /**
         * @param purchaseCartItems the purchaseCartItems to set
         */
        public void setPurchaseCartItems(List<PurchaseCartItemDTO> purchaseCartItems) {
            this.purchaseCartItems = purchaseCartItems;
        }

        /**
         * @return the vendorCode
         */
        public String getVendorCode() {
            return vendorCode;
        }

        /**
         * @param vendorCode the vendorCode to set
         */
        public void setVendorCode(String vendorCode) {
            this.vendorCode = vendorCode;
        }

    }

    public static class PurchaseCartItemDTO {
        private Integer    id;
        private String     itemTypeName;
        private String     itemSku;
        private String     brand;
        private int        quantity;
        private BigDecimal unitPrice;

        /**
         * @return the id
         */
        public Integer getId() {
            return id;
        }

        /**
         * @param id the id to set
         */
        public void setId(Integer id) {
            this.id = id;
        }

        /**
         * @return the itemTypeName
         */
        public String getItemTypeName() {
            return itemTypeName;
        }

        /**
         * @param itemTypeName the itemTypeName to set
         */
        public void setItemTypeName(String itemTypeName) {
            this.itemTypeName = itemTypeName;
        }

        /**
         * @return the itemSku
         */
        public String getItemSku() {
            return itemSku;
        }

        /**
         * @param itemSku the itemSku to set
         */
        public void setItemSku(String itemSku) {
            this.itemSku = itemSku;
        }

        /**
         * @return the brand
         */
        public String getBrand() {
            return brand;
        }

        /**
         * @param brand the brand to set
         */
        public void setBrand(String brand) {
            this.brand = brand;
        }

        /**
         * @return the quantity
         */
        public int getQuantity() {
            return quantity;
        }

        /**
         * @param quantity the quantity to set
         */
        public void setQuantity(int quantity) {
            this.quantity = quantity;
        }

        /**
         * @return the unitPrice
         */
        public BigDecimal getUnitPrice() {
            return unitPrice;
        }

        /**
         * @param unitPrice the unitPrice to set
         */
        public void setUnitPrice(BigDecimal unitPrice) {
            this.unitPrice = unitPrice;
        }

    }

}
