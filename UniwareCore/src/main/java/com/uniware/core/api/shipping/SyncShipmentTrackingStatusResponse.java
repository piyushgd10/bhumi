/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 13-Feb-2015
 *  @author parijat
 */
package com.uniware.core.api.shipping;

import java.util.ArrayList;
import java.util.List;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author parijat
 *
 */
public class SyncShipmentTrackingStatusResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -6843087799151739516L;

    List<ShippingProviderShipmentTrackingResponseDTO> resultItems      = new ArrayList<>();

    /**
     * @return the resultItems
     */
    public List<ShippingProviderShipmentTrackingResponseDTO> getResultItems() {
        return resultItems;
    }

    /**
     * @param resultItems the resultItems to set
     */
    public void setResultItems(List<ShippingProviderShipmentTrackingResponseDTO> resultItems) {
        this.resultItems = resultItems;
    }

    public static class ShippingProviderShipmentTrackingResponseDTO {
        private String shippingProviderCode;
        private String message;

        public ShippingProviderShipmentTrackingResponseDTO(String shippingProviderCode, String message) {
            this.shippingProviderCode = shippingProviderCode;
            this.message = message;
        }

        /**
         * @return the shippingProviderCode
         */
        public String getShippingProviderCode() {
            return shippingProviderCode;
        }

        /**
         * @param shippingProviderCode the shippingProviderCode to set
         */
        public void setShippingProviderCode(String shippingProviderCode) {
            this.shippingProviderCode = shippingProviderCode;
        }

        /**
         * @return the message
         */
        public String getMessage() {
            return message;
        }

        /**
         * @param message the message to set
         */
        public void setMessage(String message) {
            this.message = message;
        }

    }

}
