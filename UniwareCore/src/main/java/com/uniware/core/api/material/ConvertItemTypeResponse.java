/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 05-Sep-2012
 *  @author praveeng
 */
package com.uniware.core.api.material;

import java.util.ArrayList;
import java.util.List;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author praveeng
 */
public class ConvertItemTypeResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 7007461682730811291L;
    List<String>              newItemCodes     = new ArrayList<String>();

    /**
     * @return the newItemCodes
     */
    public List<String> getNewItemCodes() {
        return newItemCodes;
    }

    /**
     * @param newItemCodes the newItemCodes to set
     */
    public void setNewItemCodes(List<String> newItemCodes) {
        this.newItemCodes = newItemCodes;
    }

}
