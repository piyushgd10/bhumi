/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 15-Feb-2012
 *  @author vibhu
 */
package com.uniware.core.api.party;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author vibhu
 */
public class EditPartyContactResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 3316194879996114482L;
    private PartyContactDTO   partyContactDTO;

    /**
     * @return the partyContactDTO
     */
    public PartyContactDTO getPartyContactDTO() {
        return partyContactDTO;
    }

    /**
     * @param partyContactDTO the partyContactDTO to set
     */
    public void setPartyContactDTO(PartyContactDTO partyContactDTO) {
        this.partyContactDTO = partyContactDTO;
    }

}
