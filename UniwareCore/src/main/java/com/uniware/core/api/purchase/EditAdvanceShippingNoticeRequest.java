package com.uniware.core.api.purchase;

import com.unifier.core.api.base.ServiceRequest;
import com.unifier.core.api.customfields.WsCustomFieldValue;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by vibhu on 01/06/14.
 */
public class EditAdvanceShippingNoticeRequest extends ServiceRequest {

    /**
     *
     */
    private static final long        serialVersionUID = 1L;

    @NotEmpty
    private String                   advanceShippingNoticeCode;

    @Valid
    private List<WsCustomFieldValue> customFieldValues;

    public String getAdvanceShippingNoticeCode() {
        return advanceShippingNoticeCode;
    }

    public void setAdvanceShippingNoticeCode(String advanceShippingNoticeCode) {
        this.advanceShippingNoticeCode = advanceShippingNoticeCode;
    }

    /**
     * @return the customFieldValues
     */
    public List<WsCustomFieldValue> getCustomFieldValues() {
        return customFieldValues;
    }

    /**
     * @param customFieldValues the customFieldValues to set
     */
    public void setCustomFieldValues(List<WsCustomFieldValue> customFieldValues) {

        this.customFieldValues = customFieldValues;
    }

}
