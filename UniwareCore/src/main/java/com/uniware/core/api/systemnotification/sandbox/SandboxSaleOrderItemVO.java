/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 25-Dec-2013
 *  @author sunny
 */
package com.uniware.core.api.systemnotification.sandbox;

import java.math.BigDecimal;

import com.uniware.core.entity.SaleOrderItem;

public class SandboxSaleOrderItemVO {

    private String               statusCode;
    private String               code;
    private String               combinationIdentifier;
    private String               channelSaleOrderItemCode;
    private String               itemDetails;
    private SandboxInvoiceItemVO invoiceItem;
    private BigDecimal           sellingPrice;

    public SandboxSaleOrderItemVO() {
    }

    public SandboxSaleOrderItemVO(SaleOrderItem soi) {
        statusCode = soi.getStatusCode();
        code = soi.getCode();
        combinationIdentifier = soi.getCombinationIdentifier();
        channelSaleOrderItemCode = soi.getChannelSaleOrderItemCode();
        itemDetails = soi.getItemDetails();
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCombinationIdentifier() {
        return combinationIdentifier;
    }

    public void setCombinationIdentifier(String combinationIdentifier) {
        this.combinationIdentifier = combinationIdentifier;
    }

    public String getChannelSaleOrderItemCode() {
        return channelSaleOrderItemCode;
    }

    public void setChannelSaleOrderItemCode(String channelSaleOrderItemCode) {
        this.channelSaleOrderItemCode = channelSaleOrderItemCode;
    }

    public String getItemDetails() {
        return itemDetails;
    }

    public void setItemDetails(String itemDetails) {
        this.itemDetails = itemDetails;
    }

    public SandboxInvoiceItemVO getInvoiceItem() {
        return invoiceItem;
    }

    public void setInvoiceItem(SandboxInvoiceItemVO invoiceItem) {
        this.invoiceItem = invoiceItem;
    }

    public BigDecimal getSellingPrice() {
        return sellingPrice;
    }

    public void setSellingPrice(BigDecimal sellingPrice) {
        this.sellingPrice = sellingPrice;
    }

}
