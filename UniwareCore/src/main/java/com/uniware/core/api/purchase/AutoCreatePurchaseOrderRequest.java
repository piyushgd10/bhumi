/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 27-Mar-2012
 *  @author vibhu
 */
package com.uniware.core.api.purchase;

import com.unifier.core.api.base.ServiceRequest;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author vibhu
 */
public class AutoCreatePurchaseOrderRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long                 serialVersionUID = -8602029012770397959L;

    @NotNull
    private String                            vendorCode;

    @NotNull
    private Integer                           vendorAgreementId;

    @NotEmpty
    private String                            name;

    @NotEmpty
    private List<AutoCreatePurchaseOrderItem> poItems          = new ArrayList<AutoCreatePurchaseOrderRequest.AutoCreatePurchaseOrderItem>();

    /**
     * @return the vendorCode
     */
    public String getVendorCode() {
        return vendorCode;
    }

    /**
     * @param vendorCode the vendorCode to set
     */
    public void setVendorCode(String vendorCode) {
        this.vendorCode = vendorCode;
    }

    /**
     * @return the vendorAgreementId
     */
    public Integer getVendorAgreementId() {
        return vendorAgreementId;
    }

    /**
     * @param vendorAgreementId the vendorAgreementId to set
     */
    public void setVendorAgreementId(Integer vendorAgreementId) {
        this.vendorAgreementId = vendorAgreementId;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    public static class AutoCreatePurchaseOrderItem {

        @NotNull
        private String     itemSKU;

        @NotNull
        @Min(value = 1)
        private Integer    quantity;

        @NotNull
        private BigDecimal unitPrice;

        /**
         * @return the itemSKU
         */
        public String getItemSKU() {
            return itemSKU;
        }

        /**
         * @return the quantity
         */
        public Integer getQuantity() {
            return quantity;
        }

        /**
         * @param itemSKU the itemSKU to set
         */
        public void setItemSKU(String itemSKU) {
            this.itemSKU = itemSKU;
        }

        /**
         * @param quantity the quantity to set
         */
        public void setQuantity(Integer quantity) {
            this.quantity = quantity;
        }

        /**
         * @return the unitPrice
         */
        public BigDecimal getUnitPrice() {
            return unitPrice;
        }

        /**
         * @param unitPrice the unitPrice to set
         */
        public void setUnitPrice(BigDecimal unitPrice) {
            this.unitPrice = unitPrice;
        }

    }

    /**
     * @return the poItems
     */
    public List<AutoCreatePurchaseOrderItem> getPoItems() {
        return poItems;
    }

    /**
     * @param poItems the poItems to set
     */
    public void setPoItems(List<AutoCreatePurchaseOrderItem> poItems) {
        this.poItems = poItems;
    }
}
