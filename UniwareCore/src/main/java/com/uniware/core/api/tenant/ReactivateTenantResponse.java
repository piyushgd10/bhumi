/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jun 5, 2015
 *  @author harsh
 */
package com.uniware.core.api.tenant;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author harsh
 */
public class ReactivateTenantResponse extends ServiceResponse {

    private static final long serialVersionUID = -1487263372893159208L;

}
