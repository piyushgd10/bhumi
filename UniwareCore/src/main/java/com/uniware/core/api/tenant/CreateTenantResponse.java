/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jun 20, 2012
 *  @author ankit
 */
package com.uniware.core.api.tenant;

import com.unifier.core.api.base.ServiceResponse;
import com.uniware.core.api.warehouse.FacilityDTO;

/**
 * @author Sunny
 */
public class CreateTenantResponse extends ServiceResponse {

    private static final long serialVersionUID = 1502610690451527378L;

    private Integer           tenantId;

    private String            accessUrl;

    private String            successRedirectUrl;

    private FacilityDTO       facilityDTO;

    public Integer getTenantId() {
        return tenantId;
    }

    public void setTenantId(Integer tenantId) {
        this.tenantId = tenantId;
    }

    public String getAccessUrl() {
        return accessUrl;
    }

    public void setAccessUrl(String accessUrl) {
        this.accessUrl = accessUrl;
    }

    public String getSuccessRedirectUrl() {
        return successRedirectUrl;
    }

    public void setSuccessRedirectUrl(String successRedirectUrl) {
        this.successRedirectUrl = successRedirectUrl;
    }

    public FacilityDTO getFacilityDTO() {
        return facilityDTO;
    }

    public void setFacilityDTO(FacilityDTO facilityDTO) {
        this.facilityDTO = facilityDTO;
    }

    @Override
    public String toString() {
        return "CreateTenantResponse [tenantId=" + tenantId + ", accessUrl=" + accessUrl + ", isSuccessful()=" + isSuccessful() + ", getMessage()=" + getMessage()
                + ", getErrors()=" + getErrors() + ", getWarnings()=" + getWarnings() + "]";
    }

}
