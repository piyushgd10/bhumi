/*
 * Copyright 2017 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 8/8/17 1:06 PM
 * @author digvijaysharma
 */

package com.uniware.core.api.picker;

import java.util.List;
import java.util.Map;

import com.unifier.core.api.base.ServiceResponse;

/**
 * Created by bhuvneshwarkumar on 13/09/16.
 */
public class GetPickBucketDetailResponse extends ServiceResponse {

    private String                               picklistCode;

    private Map<String, List<PickbucketItemDTO>> bucketItems;

    public String getPicklistCode() {
        return picklistCode;
    }

    public void setPicklistCode(String picklistCode) {
        this.picklistCode = picklistCode;
    }

    public Map<String, List<PickbucketItemDTO>> getBucketItems() {
        return bucketItems;
    }

    public void setBucketItems(Map<String, List<PickbucketItemDTO>> bucketItems) {
        this.bucketItems = bucketItems;
    }

    public static class PickbucketItemDTO {

        private String  itemCode;
        private Integer quantity;
        private String  shelfCode;
        private boolean putbackComplete;

        public PickbucketItemDTO(String itemCode, Integer quantity, String shelfCode, boolean putbackComplete) {
            this.itemCode = itemCode;
            this.quantity = quantity;
            this.shelfCode = shelfCode;
            this.putbackComplete = putbackComplete;
        }

        public String getItemCode() {
            return itemCode;
        }

        public void setItemCode(String itemCode) {
            this.itemCode = itemCode;
        }

        public Integer getQuantity() {
            return quantity;
        }

        public void setQuantity(Integer quantity) {
            this.quantity = quantity;
        }

        public String getShelfCode() {
            return shelfCode;
        }

        public void setShelfCode(String shelfCode) {
            this.shelfCode = shelfCode;
        }

        public boolean isPutbackComplete() {
            return putbackComplete;
        }

        public void setPutbackComplete(boolean putbackComplete) {
            this.putbackComplete = putbackComplete;
        }
    }
}
