package com.uniware.core.api.saleorder;

/**
 * Created by akshayag on 12/9/15.
 */
public class PageRequest {
    int    page;
    int    size;
    String property;
    String sort;

    public PageRequest() {
    }

    public PageRequest(int page, int size) {
        this(page, size, "updated", "ASC");
    }

    public PageRequest(int page, int size, String property, String sort) {
        this.page = page;
        this.size = size;
        this.property = property;
        this.sort = sort;
    }

    public PageRequest(int page, int size, String property) {
        this(page, size, property, "ASC");
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getProperty() {
        return this.property;
    }

    public void setOffset(String property) {
        this.property = property;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    @Override
    public String toString() {
        return "PageRequest{" + "page=" + page + ", size=" + size + ", property=" + property + ", sort='" + sort + '\'' + '}';
    }
}
