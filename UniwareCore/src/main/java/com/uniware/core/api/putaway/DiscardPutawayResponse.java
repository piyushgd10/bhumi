/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jun 17, 2012
 *  @author singla
 */
package com.uniware.core.api.putaway;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author singla
 */
public class DiscardPutawayResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 2425825050187818485L;

}
