/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jun 20, 2012
 *  @author singla
 */
package com.uniware.core.api.channel;

import java.util.List;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author Sunny
 */
public class ForceSyncSaleOrderStatusResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -8331014310944611193L;
    private List<String>      updatedSaleOrderCodes;

    public List<String> getUpdatedSaleOrderCodes() {
        return updatedSaleOrderCodes;
    }

    public void setUpdatedSaleOrderCodes(List<String> updatedSaleOrderCodes) {
        this.updatedSaleOrderCodes = updatedSaleOrderCodes;
    }

}
