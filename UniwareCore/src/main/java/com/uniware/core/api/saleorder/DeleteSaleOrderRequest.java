/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 17/04/14
 *  @author amit
 */

package com.uniware.core.api.saleorder;

import javax.validation.constraints.NotNull;

import com.unifier.core.api.base.ServiceRequest;

public class DeleteSaleOrderRequest extends ServiceRequest {

    private static final long serialVersionUID = 9061542940073670004L;

    @NotNull
    private String            saleOrderCode;

    public DeleteSaleOrderRequest() {
        super();
    }

    public String getSaleOrderCode() {
        return saleOrderCode;
    }

    public void setSaleOrderCode(String saleOrderCode) {
        this.saleOrderCode = saleOrderCode;
    }

    @Override
    public String toString() {
        return "DeleteSaleOrderRequest{" + "saleOrderCode='" + saleOrderCode + '\'' + '}';
    }
}