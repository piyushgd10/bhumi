/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 03-Sep-2012
 *  @author praveeng
 */
package com.uniware.core.api.material;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author praveeng
 */
public class DiscardGatePassRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -6766416730693837701L;

    @NotBlank
    private String            gatePassCode;

    @NotNull
    private Integer           userId;

    /**
     * @return the gatePassCode
     */
    public String getGatePassCode() {
        return gatePassCode;
    }

    /**
     * @param gatePassCode the gatePassCode to set
     */
    public void setGatePassCode(String gatePassCode) {
        this.gatePassCode = gatePassCode;
    }

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

}
