/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 12-Jun-2012
 *  @author vibhu
 */
package com.uniware.core.api.inflow;

import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author vibhu
 */
public class CreateItemLabelsRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -4477116971616919404L;

    @NotBlank
    private String            itemSkuCode;

    @NotNull
    private Integer           quantity;

    private String            vendorSkuCode;

    private Integer           vendorId;

    @Past
    private Date              manufacturingDate;

    public CreateItemLabelsRequest(String itemSkuCode, Integer quantity) {
        super();
        setItemSkuCode(itemSkuCode);
        setQuantity(quantity);
    }

    public CreateItemLabelsRequest() {
        super();
    }

    /**
     * @return the vendorSkuCode
     */
    public String getVendorSkuCode() {
        return vendorSkuCode;
    }

    /**
     * @param vendorSkuCode the vendorSkuCode to set
     */
    public void setVendorSkuCode(String vendorSkuCode) {
        this.vendorSkuCode = vendorSkuCode;
    }

    public String getItemSkuCode() {
        return itemSkuCode;
    }

    public void setItemSkuCode(String itemSkuCode) {
        this.itemSkuCode = itemSkuCode;
    }

    /**
     * @return the quantity
     */
    public Integer getQuantity() {
        return quantity;
    }

    /**
     * @param quantity the quantity to set
     */
    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getVendorId() {
        return vendorId;
    }

    public void setVendorId(Integer vendorId) {
        this.vendorId = vendorId;
    }

    public Date getManufacturingDate() {
        return manufacturingDate;
    }

    public void setManufacturingDate(Date manufacturingDate) {
        this.manufacturingDate = manufacturingDate;
    }

    @Override
    public String toString() {
        return "CreateItemLabelsRequest{" + "itemSkuCode=" + itemSkuCode + ", quantity=" + quantity + ", vendorSkuCode='" + vendorSkuCode + '\'' + ", vendorId=" + vendorId + '}';
    }
}