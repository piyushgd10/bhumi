/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 03-Jun-2012
 *  @author vibhu
 */
package com.uniware.core.api.channel;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;
import com.uniware.core.api.model.WsChannelProductIdentifiers;

/**
 * @author Sunny
 */
public class SyncChannelCatalogRequest extends ServiceRequest {

    private static final long                 serialVersionUID          = 1963404117545473225L;
    @NotBlank
    private String                            channelCode;

    private List<WsChannelProductIdentifiers> channelProductIdentifiers = new ArrayList<>();

    private boolean                           forceRun;

    public SyncChannelCatalogRequest() {
        super();
    }

    public SyncChannelCatalogRequest(String channelCode) {
        super();
        this.channelCode = channelCode;
    }

    public SyncChannelCatalogRequest(String channelCode, List<WsChannelProductIdentifiers> channelProductIdentifiers, boolean forceRun) {
        super();
        this.channelCode = channelCode;
        this.channelProductIdentifiers = channelProductIdentifiers;
        this.forceRun = forceRun;
    }

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    public List<WsChannelProductIdentifiers> getChannelProductIdentifiers() {
        return channelProductIdentifiers;
    }

    public void setChannelProductIdentifiers(List<WsChannelProductIdentifiers> channelProductIdentifiers) {
        this.channelProductIdentifiers = channelProductIdentifiers;
    }

    public boolean isForceRun() {
        return forceRun;
    }

    public void setForceRun(boolean forceRun) {
        this.forceRun = forceRun;
    }

    @Override
    public String toString() {
        return "SyncChannelCatalogRequest{" + "channelCode='" + channelCode + '\'' + '}';
    }
}
