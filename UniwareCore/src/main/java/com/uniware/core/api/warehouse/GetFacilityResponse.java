/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 24-Jan-2014
 *  @author parijat
 */
package com.uniware.core.api.warehouse;

import com.unifier.core.api.base.ServiceResponse;

public class GetFacilityResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -2835593757489652641L;

    private FacilityDTO       facilityDTO;

    public FacilityDTO getFacilityDTO() {
        return facilityDTO;
    }

    public void setFacilityDTO(FacilityDTO facilityDTO) {
        this.facilityDTO = facilityDTO;
    }

}
