/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 30/04/14
 *  @author amit
 */

package com.uniware.core.api.cms;

import com.unifier.core.api.base.ServiceResponse;

public class GetContentForKeyResponse extends ServiceResponse {

    private static final long serialVersionUID = 6678249783062489499L;

    private String            content;

    public GetContentForKeyResponse() {
        super();
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "GetContentForKeyResponse{" + "content='" + content + '\'' + '}';
    }
}
