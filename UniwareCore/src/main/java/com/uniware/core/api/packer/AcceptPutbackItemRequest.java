/*
 * Copyright 2017 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 8/8/17 4:02 PM
 * @author digvijaysharma
 */
package com.uniware.core.api.packer;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author singla
 */
public class AcceptPutbackItemRequest extends ServiceRequest {
    /**
     * 
     */
    private static final long serialVersionUID = -421573802557526449L;

    @NotBlank
    private String            picklistCode;

    @NotBlank
    private String            saleOrderCode;

    @NotBlank
    private String            saleOrderItemCode;

    private String            itemCode;

    /**
     * @return the picklistCode
     */
    public String getPicklistCode() {
        return picklistCode;
    }

    /**
     * @param picklistCode the picklistCode to set
     */
    public void setPicklistCode(String picklistCode) {
        this.picklistCode = picklistCode;
    }

    public String getSaleOrderCode() {
        return saleOrderCode;
    }

    public void setSaleOrderCode(String saleOrderCode) {
        this.saleOrderCode = saleOrderCode;
    }

    public String getSaleOrderItemCode() {
        return saleOrderItemCode;
    }

    public void setSaleOrderItemCode(String saleOrderItemCode) {
        this.saleOrderItemCode = saleOrderItemCode;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }
}
