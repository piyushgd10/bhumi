/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 27-Apr-2013
 *  @author unicom
 */
package com.uniware.core.api.shipping;

import java.util.Date;
import java.util.List;

import org.springframework.data.annotation.Id;

import com.unifier.core.utils.DateUtils;
import com.uniware.core.api.async.PollingResult;

/**
 * @author Sunny
 */
public class ShippingManifestStatusDTO extends PollingResult {

    @Id
    private String                         id;
    private String                         shippingManifestCode;
    private String                         shippingManifestLink;
    private List<FailedShippingPackageDTO> failedShippingPackages;
    private String                         failedShipmentsBatchCode;
    private Date                           created;
    private Date                           updated;

    public ShippingManifestStatusDTO() {
    }

    public ShippingManifestStatusDTO(String shippingManifestCode) {
        this.shippingManifestCode = shippingManifestCode;
        this.created = DateUtils.getCurrentTime();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getShippingManifestCode() {
        return shippingManifestCode;
    }

    public void setShippingManifestCode(String shippingManifestCode) {
        this.shippingManifestCode = shippingManifestCode;
    }

    public String getShippingManifestLink() {
        return shippingManifestLink;
    }

    public void setShippingManifestLink(String shippingManifestLink) {
        this.shippingManifestLink = shippingManifestLink;
    }

    public List<FailedShippingPackageDTO> getFailedShippingPackages() {
        return failedShippingPackages;
    }

    public void setFailedShippingPackages(List<FailedShippingPackageDTO> failedShippingPackages) {
        this.failedShippingPackages = failedShippingPackages;
    }

    public String getFailedShipmentsBatchCode() {
        return failedShipmentsBatchCode;
    }

    public void setFailedShipmentsBatchCode(String failedShipmentsBatchCode) {
        this.failedShipmentsBatchCode = failedShipmentsBatchCode;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    //    public static void main(String[] args) {
    //        FailedShippingPackageDTO f1 = new FailedShippingPackageDTO("KP302", "failed on channel", false);
    //        FailedShippingPackageDTO f2 = new FailedShippingPackageDTO("KP303", "cancelled on channel", true);
    //        List<FailedShippingPackageDTO> failed = Arrays.asList(f1, f2);
    //        ShippingManifestStatusDTO sm = new ShippingManifestStatusDTO("SM202");
    //        sm.setCompleted(true);
    //        sm.setSuccessful(true);
    //        sm.setCurrentMileStone(100);
    //        sm.setMileStoneCount(100);
    //        sm.setFailedShipmentsBatchCode("BT323");
    //        sm.setShippingManifestLink("http://dsads.com/dsadas");
    //        sm.setFailedShippingPackages(failed);
    //        System.out.println(new Gson().toJson(sm));
    //    }

    public static class FailedShippingPackageDTO {
        private String  code;
        private String  saleOrderCode;
        private String  failureReason;
        private boolean cancelled;

        public FailedShippingPackageDTO() {
        }
        
        public FailedShippingPackageDTO(String code, String failureReason, boolean cancelled) {
            super();
            this.code = code;
            this.failureReason = failureReason;
            this.cancelled = cancelled;
        }

        public FailedShippingPackageDTO(String code, String saleOrderCode, String failureReason, boolean cancelled) {
            super();
            this.code = code;
            this.saleOrderCode = saleOrderCode;
            this.failureReason = failureReason;
            this.cancelled = cancelled;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getSaleOrderCode() {
            return saleOrderCode;
        }

        public void setSaleOrderCode(String saleOrderCode) {
            this.saleOrderCode = saleOrderCode;
        }

        public String getFailureReason() {
            return failureReason;
        }

        public void setFailureReason(String failureReason) {
            this.failureReason = failureReason;
        }

        public boolean isCancelled() {
            return cancelled;
        }

        public void setCancelled(boolean cancelled) {
            this.cancelled = cancelled;
        }
    }
}
