package com.uniware.core.api.picker;

import com.unifier.core.api.base.ServiceRequest;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Created by admin on 01/12/17.
 */
public class MarkInventoryNotFoundWhileReceivingRequest extends ServiceRequest {

    private static final long serialVersionUID = 12L;

    @NotBlank
    private String            picklistCode;

    @NotBlank
    private String            itemSku;

    @NotBlank
    private String            shelfCode;

    @NotNull
    @Min(value = 1)
    private Integer           quantityNotFound;

    public String getPicklistCode() {
        return picklistCode;
    }

    public void setPicklistCode(String picklistCode) {
        this.picklistCode = picklistCode;
    }

    public String getItemSku() {
        return itemSku;
    }

    public void setItemSku(String itemSku) {
        this.itemSku = itemSku;
    }

    public String getShelfCode() {
        return shelfCode;
    }

    public void setShelfCode(String shelfCode) {
        this.shelfCode = shelfCode;
    }

    public Integer getQuantityNotFound() {
        return quantityNotFound;
    }

    public void setQuantityNotFound(Integer quantityNotFound) {
        this.quantityNotFound = quantityNotFound;
    }
}
