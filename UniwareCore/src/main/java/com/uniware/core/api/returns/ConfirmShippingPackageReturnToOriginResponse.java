/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 17, 2012
 *  @author singla
 */
package com.uniware.core.api.returns;

import com.unifier.core.api.base.ServiceResponse;

import com.uniware.core.api.packer.ShippingPackageFullDTO;

/**
 * @author Sunny
 */
public class ConfirmShippingPackageReturnToOriginResponse extends ServiceResponse {

    /**
     *
     */
    private static final long serialVersionUID = 6656719055897096116L;

}
