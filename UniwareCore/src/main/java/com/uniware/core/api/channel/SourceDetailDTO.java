/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 18-Apr-2013
 *  @author unicom
 */
package com.uniware.core.api.channel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.uniware.core.entity.Source;
import com.uniware.core.entity.SourceConnector;

/**
 * @author Sunny Agarwal
 */
public class SourceDetailDTO implements Serializable {

    private Integer                               id;
    private String                                code;
    private String                                name;
    private String                                type;
    private String                                localization;
    private boolean                               enabled;
    private boolean                               active;
    private boolean                               orderSyncAvailable;
    private boolean                               inventorySyncAvailable;
    private boolean                               catalogSyncAvailable;
    private boolean                               orderReconciliationAvailable;
    private boolean                               orderStatusSyncAvailable;
    private boolean                               priceStatusSyncAvailable;
    private boolean                               useChannelSKU;
    private int                                   priority;
    private boolean                               thirdPartyConfigurationRequired;
    private boolean                               thirdPartyShippingAvailable;
    private boolean                               sellerTatRequired;
    private boolean                               pendencyConfigurationEnabled;
    private boolean                               commitPendencyOnChannel;
    private boolean                               sourceNotificationsEnabled;
    private boolean                               allowMultipleChannel;
    private boolean                               allowAnyShippingMethod;
    private boolean                               useSellerSkuForInventoryUpdate;
    private boolean                               packageTypeConfigured;
    private boolean                               productDelistingConfigured;
    private boolean                               shippingLabelAggregationConfigured;
    private boolean                               tatAvailableFromChannel;
    private boolean                               facilityAssociationRequired;
    private String                                landingPageScriptName;
    private Boolean                               thirdPartyShippingDefault;
    private String                                defaultInventoryUpdateFormula;
    private Source.ConfigurationStatus            productRelistingConfigured;

    private List<SourceConfigurationParameterDTO> sourceConfigurationParameters = new ArrayList<SourceConfigurationParameterDTO>();
    private List<SourceConnectorDTO>              sourceConnectors              = new ArrayList<SourceConnectorDTO>();

    public SourceDetailDTO() {
    }

    public SourceDetailDTO(Source s) {
        code = s.getCode();
        name = s.getName();
        enabled = s.isEnabled();
        active = s.isActive();
        type = s.getType().name();
        localization = s.getLocalization().name();
        orderSyncAvailable = s.isOrderSyncConfigured();
        catalogSyncAvailable = s.isCatalogSyncConfigured();
        inventorySyncAvailable = s.isInventorySyncConfigured();
        orderReconciliationAvailable = s.isOrderReconciliationEnabled();
        orderStatusSyncAvailable = s.isOrderStatusSyncAvailable();
        priceStatusSyncAvailable = s.isPricingSyncConfigured();
        priority = s.getPriority();
        thirdPartyConfigurationRequired = s.isThirdPartyConfigurationRequired();
        thirdPartyShippingAvailable = s.isThirdPartyShippingAvailable();
        pendencyConfigurationEnabled = s.isPendencyConfigurationEnabled();
        setCommitPendencyOnChannel(s.isCommitPendencyOnChannel());
        allowMultipleChannel = s.isAllowMultipleChannel();
        packageTypeConfigured = s.isPackageTypeConfigured();
        allowAnyShippingMethod = s.isAllowAnyShippingMethod();
        useSellerSkuForInventoryUpdate = s.isUseSellerSkuForInventoryUpdate();
        productDelistingConfigured = s.isProductDelistingConfigured();
        shippingLabelAggregationConfigured = s.isShippingLabelAggregationConfigured();
        landingPageScriptName = s.getLandingPageScriptName();
        thirdPartyShippingDefault = s.getThirdPartyShippingDefault();
        productRelistingConfigured = s.getProductRelistingConfigured();
        tatAvailableFromChannel = s.isTatAvailableFromChannel();
        facilityAssociationRequired = s.isFacilityAssociationRequired();
        defaultInventoryUpdateFormula = s.getDefaultInventoryUpdateFormula();
        for (SourceConnector sc : s.getSourceConnectors()) {
            sourceConnectors.add(new SourceConnectorDTO(sc));
        }
        Collections.sort(sourceConnectors, new Comparator<SourceConnectorDTO>() {
            @Override
            public int compare(SourceConnectorDTO o1, SourceConnectorDTO o2) {
                return o1.getPriority() - o2.getPriority();
            }
        });
    }

    public String getCode() {
        return code;
    }

    public void setCode(String sourceCode) {
        this.code = sourceCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String sourceName) {
        this.name = sourceName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLocalization() {
        return localization;
    }

    public void setLocalization(String localization) {
        this.localization = localization;
    }

    public List<SourceConnectorDTO> getSourceConnectors() {
        return sourceConnectors;
    }

    public void setSourceConnectors(List<SourceConnectorDTO> sourceConnectors) {
        this.sourceConnectors = sourceConnectors;
    }

    public List<SourceConfigurationParameterDTO> getSourceConfigurationParameters() {
        return sourceConfigurationParameters;
    }

    public void setSourceConfigurationParameters(List<SourceConfigurationParameterDTO> sourceConfigurationParameters) {
        this.sourceConfigurationParameters = sourceConfigurationParameters;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isOrderSyncAvailable() {
        return orderSyncAvailable;
    }

    public void setOrderSyncAvailable(boolean orderSyncAvailable) {
        this.orderSyncAvailable = orderSyncAvailable;
    }

    public boolean isCatalogSyncAvailable() {
        return catalogSyncAvailable;
    }

    public void setCatalogSyncAvailable(boolean catalogSyncAvailable) {
        this.catalogSyncAvailable = catalogSyncAvailable;
    }

    public boolean isPriceStatusSyncAvailable() {
        return priceStatusSyncAvailable;
    }

    public void setPriceStatusSyncAvailable(boolean priceStatusSyncAvailable) {
        this.priceStatusSyncAvailable = priceStatusSyncAvailable;
    }

    public boolean isInventorySyncAvailable() {
        return inventorySyncAvailable;
    }

    public void setInventorySyncAvailable(boolean inventorySyncAvailable) {
        this.inventorySyncAvailable = inventorySyncAvailable;
    }

    public boolean isOrderReconciliationAvailable() {
        return orderReconciliationAvailable;
    }

    public void setOrderReconciliationAvailable(boolean orderReconciliationAvailable) {
        this.orderReconciliationAvailable = orderReconciliationAvailable;
    }

    public boolean isOrderStatusSyncConfigured() {
        return orderStatusSyncAvailable;
    }

    public void setOrderStatusSyncConfigured(boolean orderStatusSyncConfigured) {
        this.orderStatusSyncAvailable = orderStatusSyncConfigured;
    }

    public boolean isUseChannelSKU() {
        return useChannelSKU;
    }

    public void setUseChannelSKU(boolean useChannelSKU) {
        this.useChannelSKU = useChannelSKU;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public boolean isThirdPartyConfigurationRequired() {
        return thirdPartyConfigurationRequired;
    }

    public boolean isThirdPartyShippingAvailable() {
        return thirdPartyShippingAvailable;
    }

    public void setThirdPartyShippingAvailable(boolean thirdPartyShippingAvailable) {
        this.thirdPartyShippingAvailable = thirdPartyShippingAvailable;
    }

    public void setThirdPartyConfigurationRequired(boolean thirdPartyConfigurationRequired) {
        this.thirdPartyConfigurationRequired = thirdPartyConfigurationRequired;
    }

    public boolean isAllowMultipleChannel() {
        return allowMultipleChannel;
    }

    public void setAllowMultipleChannel(boolean allowMultipleChannel) {
        this.allowMultipleChannel = allowMultipleChannel;
    }

    public boolean isSellerTatRequired() {
        return sellerTatRequired;
    }

    public void setSellerTatRequired(boolean sellerTatRequired) {
        this.sellerTatRequired = sellerTatRequired;
    }

    public boolean isSourceNotificationsEnabled() {
        return sourceNotificationsEnabled;
    }

    public void setSourceNotificationsEnabled(boolean sourceNotificationsEnabled) {
        this.sourceNotificationsEnabled = sourceNotificationsEnabled;
    }

    public boolean isPendencyConfigurationEnabled() {
        return pendencyConfigurationEnabled;
    }

    public void setPendencyConfigurationEnabled(boolean pendencyConfigurationEnabled) {
        this.pendencyConfigurationEnabled = pendencyConfigurationEnabled;
    }

    public boolean isCommitPendencyOnChannel() {
        return commitPendencyOnChannel;
    }

    public void setCommitPendencyOnChannel(boolean commitPendencyOnChannel) {
        this.commitPendencyOnChannel = commitPendencyOnChannel;
    }

    public boolean isPackageTypeConfigured() {
        return packageTypeConfigured;
    }

    public void setPackageTypeConfigured(boolean packageTypeConfigured) {
        this.packageTypeConfigured = packageTypeConfigured;
    }

    public boolean isUseSellerSkuForInventoryUpdate() {
        return useSellerSkuForInventoryUpdate;
    }

    public void setUseSellerSkuForInventoryUpdate(boolean useSellerSkuForInventoryUpdate) {
        this.useSellerSkuForInventoryUpdate = useSellerSkuForInventoryUpdate;
    }

    public boolean isAllowAnyShippingMethod() {
        return allowAnyShippingMethod;
    }

    public void setAllowAnyShippingMethod(boolean allowAnyShippingMethod) {
        this.allowAnyShippingMethod = allowAnyShippingMethod;
    }

    public boolean isOrderStatusSyncAvailable() {
        return orderStatusSyncAvailable;
    }

    public void setOrderStatusSyncAvailable(boolean orderStatusSyncAvailable) {
        this.orderStatusSyncAvailable = orderStatusSyncAvailable;
    }

    public String getLandingPageScriptName() {
        return landingPageScriptName;
    }

    public void setLandingPageScriptName(String landingPageScriptName) {
        this.landingPageScriptName = landingPageScriptName;
    }

    public boolean isProductDelistingConfigured() {
        return productDelistingConfigured;
    }

    public boolean isShippingLabelAggregationConfigured() {
        return shippingLabelAggregationConfigured;
    }

    public void setShippingLabelAggregationConfigured(boolean shippingLabelAggregationConfigured) {
        this.shippingLabelAggregationConfigured = shippingLabelAggregationConfigured;
    }

    public void setProductDelistingConfigured(boolean productDelistingConfigured) {
        this.productDelistingConfigured = productDelistingConfigured;
    }

    public Boolean getThirdPartyShippingDefault() {
        return thirdPartyShippingDefault;
    }

    public void setThirdPartyShippingDefault(Boolean thirdPartyShippingDefault) {
        this.thirdPartyShippingDefault = thirdPartyShippingDefault;
    }

    public Source.ConfigurationStatus getProductRelistingConfigured() {
        return productRelistingConfigured;
    }

    public void setProductRelistingConfigured(Source.ConfigurationStatus productRelistingConfigured) {
        this.productRelistingConfigured = productRelistingConfigured;
    }

    public boolean isTatAvailableFromChannel() {
        return tatAvailableFromChannel;
    }

    public void setTatAvailableFromChannel(boolean tatAvailableFromChannel) {
        this.tatAvailableFromChannel = tatAvailableFromChannel;
    }

    public boolean isFacilityAssociationRequired() {
        return facilityAssociationRequired;
    }

    public void setFacilityAssociationRequired(boolean facilityAssociationRequired) {
        this.facilityAssociationRequired = facilityAssociationRequired;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((code == null) ? 0 : code.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        SourceDetailDTO other = (SourceDetailDTO) obj;
        if (code == null) {
            if (other.code != null) {
                return false;
            }
        } else if (!code.equals(other.code)) {
            return false;
        }
        if (name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!name.equals(other.name)) {
            return false;
        }
        return true;
    }

    public String getDefaultInventoryUpdateFormula() {
        return defaultInventoryUpdateFormula;
    }

    public void setDefaultInventoryUpdateFormula(String defaultInventoryUpdateFormula) {
        this.defaultInventoryUpdateFormula = defaultInventoryUpdateFormula;
    }
}
