/*
 *  Copyright 2011 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 25, 2011
 *  @author ankitp
 */
package com.uniware.core.api.warehouse;

import com.unifier.core.api.base.ServiceResponse;

import java.util.ArrayList;
import java.util.List;

import com.uniware.core.entity.Shelf;

/**
 * @author ankitp
 */
public class SearchShelfResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long    serialVersionUID = 5522481945383263011L;
    private List<String>         shelfCodes       = new ArrayList<String>();
    private final List<ShelfDTO> shelfDTOs        = new ArrayList<ShelfDTO>();

    /**
     * @return the shelfDTOs
     */
    public List<ShelfDTO> getShelfDTOs() {
        return shelfDTOs;
    }

    /**
     * @return the shelfCodes
     */
    public List<String> getShelfCodes() {
        return shelfCodes;
    }

    /**
     * @param shelfCodes the shelfCodes to set
     */
    public void setShelfCodes(List<String> shelfCodes) {
        this.shelfCodes = shelfCodes;
    }

    /**
     * @param shelf
     */
    public void addShelf(Shelf shelf) {
        shelfCodes.add(shelf.getCode());

        ShelfDTO shelfDTO = new ShelfDTO();
        shelfDTO.setShelfCode(shelf.getCode());
        shelfDTO.setSectionCode(shelf.getSection().getCode());
        shelfDTO.setTotalVolume(shelf.getTotalVolume());
        shelfDTO.setFilledVolume(shelf.getFilledVolume());
        if (shelf.getTotalVolume() != 0) {
            shelfDTO.setFilledPercentage((int) (shelf.getFilledVolume() * 100 / shelf.getTotalVolume()));
        }
        shelfDTO.setItemCount(shelf.getItemCount());

        shelfDTOs.add(shelfDTO);
    }

    public static class ShelfDTO {
        private String shelfCode;
        private String sectionCode;
        private long   totalVolume;
        private long   filledVolume;
        private int    filledPercentage;
        private int    itemCount;

        /**
         * @return the shelfCode
         */
        public String getShelfCode() {
            return shelfCode;
        }

        /**
         * @param shelfCode the shelfCode to set
         */
        public void setShelfCode(String shelfCode) {
            this.shelfCode = shelfCode;
        }

        /**
         * @return the sectionCode
         */
        public String getSectionCode() {
            return sectionCode;
        }

        /**
         * @param sectionCode the sectionCode to set
         */
        public void setSectionCode(String sectionCode) {
            this.sectionCode = sectionCode;
        }

        /**
         * @return the totalVolume
         */
        public long getTotalVolume() {
            return totalVolume;
        }

        /**
         * @param totalVolume the totalVolume to set
         */
        public void setTotalVolume(long totalVolume) {
            this.totalVolume = totalVolume;
        }

        /**
         * @return the filledVolume
         */
        public long getFilledVolume() {
            return filledVolume;
        }

        /**
         * @param filledVolume the filledVolume to set
         */
        public void setFilledVolume(long filledVolume) {
            this.filledVolume = filledVolume;
        }

        /**
         * @return the filledPercentage
         */
        public int getFilledPercentage() {
            return filledPercentage;
        }

        /**
         * @param filledPercentage the filledPercentage to set
         */
        public void setFilledPercentage(int filledPercentage) {
            this.filledPercentage = filledPercentage;
        }

        /**
         * @return the itemCount
         */
        public int getItemCount() {
            return itemCount;
        }

        /**
         * @param itemCount the itemCount to set
         */
        public void setItemCount(int itemCount) {
            this.itemCount = itemCount;
        }

    }
}
