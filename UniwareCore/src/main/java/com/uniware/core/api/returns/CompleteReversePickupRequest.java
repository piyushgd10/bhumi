/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 9, 2012
 *  @author singla
 */
package com.uniware.core.api.returns;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;
import com.uniware.core.api.returns.MarkSaleOrderReturnedRequest.WsSaleOrderItem;

/**
 * @author Sunny
 */
public class CompleteReversePickupRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long     serialVersionUID = 4795720002305203341L;

    @NotBlank
    private String                reversePickupCode;

    @Valid
    private List<WsSaleOrderItem> saleOrderItems;

    @NotNull
    private Integer               userId;

    public String getReversePickupCode() {
        return reversePickupCode;
    }

    public void setReversePickupCode(String reversePickupCode) {
        this.reversePickupCode = reversePickupCode;
    }

    public List<WsSaleOrderItem> getSaleOrderItems() {
        return saleOrderItems;
    }

    public void setSaleOrderItems(List<WsSaleOrderItem> saleOrderItems) {
        this.saleOrderItems = saleOrderItems;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

}
