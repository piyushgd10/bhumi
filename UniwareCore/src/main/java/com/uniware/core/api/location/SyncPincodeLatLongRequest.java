/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 11-May-2015
 *  @author parijat
 */
package com.uniware.core.api.location;

import java.util.List;

import com.unifier.core.api.base.ServiceRequest;
import com.uniware.core.entity.Location;

/**
 * @author parijat
 */
public class SyncPincodeLatLongRequest extends ServiceRequest {

    private static final long serialVersionUID = 457876545678987678L;

    private List<Location>    locations;

    public List<Location> getLocations() {
        return locations;
    }

    public void setLocations(List<Location> locations) {
        this.locations = locations;
    }

}
