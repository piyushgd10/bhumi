package com.uniware.core.api.saleorder;

import com.unifier.core.api.base.ServiceResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bhuvneshwarkumar on 25/08/15.
 */
public class AcceptSaleOrderAlternateResponse extends ServiceResponse {
    private List<String>                                                       successfulSaleOrderItemCodes   = new ArrayList<>();

    public List<String> getSuccessfulSaleOrderItemCodes() {
        return successfulSaleOrderItemCodes;
    }

    public void setSuccessfulSaleOrderItemCodes(List<String> successfulSaleOrderItemCodes) {
        this.successfulSaleOrderItemCodes = successfulSaleOrderItemCodes;
    }

}
