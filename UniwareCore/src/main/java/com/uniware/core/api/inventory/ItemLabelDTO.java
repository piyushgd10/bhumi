/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 11/06/14
 *  @author amit
 */

package com.uniware.core.api.inventory;

import com.uniware.core.entity.ItemType;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class ItemLabelDTO implements Serializable {

    private static final long serialVersionUID = 993972274844739906L;

    private String            skuCode;

    private String            name;

    private String            description;

    private int               length;

    private int               width;

    private int               height;

    private int               weight;

    private String            color;

    private String            size;

    private String            brand;

    private BigDecimal        price;

    private String            categoryName;

    private String            categoryCode;

    private List<String>      itemCodeList     = new ArrayList<>();

    public ItemLabelDTO() {
        super();
    }

    public ItemLabelDTO(ItemType itemType) {
        super();
        setSkuCode(itemType.getSkuCode());
        setName(itemType.getName());
        setDescription(itemType.getDescription());
        setLength(itemType.getLength());
        setWidth(itemType.getWidth());
        setHeight(itemType.getHeight());
        setWeight(itemType.getWeight());
        setColor(itemType.getColor());
        setSize(itemType.getSize());
        setBrand(itemType.getBrand());
        setPrice(itemType.getMaxRetailPrice());
        setCategoryName(itemType.getCategory().getName());
        setCategoryCode(itemType.getCategory().getCode());
    }

    public String getSkuCode() {
        return skuCode;
    }

    public void setSkuCode(String skuCode) {
        this.skuCode = skuCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCategoryCode() {
        return categoryCode;
    }

    public void setCategoryCode(String categoryCode) {
        this.categoryCode = categoryCode;
    }

    public List<String> getItemCodeList() {
        return itemCodeList;
    }

    public void setItemCodeList(List<String> itemCodeList) {
        this.itemCodeList = itemCodeList;
    }

    @Override
    public String toString() {
        return "ItemLabelDTO{" + "skuCode='" + skuCode + '\'' + ", name='" + name + '\'' + ", description='" + description + '\'' + ", length=" + length + ", width=" + width
                + ", height=" + height + ", weight=" + weight + ", color='" + color + '\'' + ", size='" + size + '\'' + ", brand='" + brand + '\'' + ", price=" + price
                + ", categoryName='" + categoryName + '\'' + ", categoryCode='" + categoryCode + '\'' + ", itemCodeList=" + itemCodeList + '}';
    }
}