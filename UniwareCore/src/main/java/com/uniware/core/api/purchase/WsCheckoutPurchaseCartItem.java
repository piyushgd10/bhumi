/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 27-Jun-2013
 *  @author karunsingla
 */
package com.uniware.core.api.purchase;

import java.util.List;

import javax.validation.Valid;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

public class WsCheckoutPurchaseCartItem {

    @NotBlank
    private String                                 vendorCode;

    private String                                 vendorAgreementName;

    private boolean                                sendForApproval = true;

    @NotEmpty
    @Valid
    private List<WsCheckoutPurchaseCartVendorItem> checkoutPurchaseCartVendorItems;

    public String getVendorCode() {
        return vendorCode;
    }

    public void setVendorCode(String vendorCode) {
        this.vendorCode = vendorCode;
    }

    public List<WsCheckoutPurchaseCartVendorItem> getCheckoutPurchaseCartVendorItems() {
        return checkoutPurchaseCartVendorItems;
    }

    public void setCheckoutPurchaseCartVendorItems(List<WsCheckoutPurchaseCartVendorItem> checkoutPurchaseCartVendorItems) {
        this.checkoutPurchaseCartVendorItems = checkoutPurchaseCartVendorItems;
    }

    public String getVendorAgreementName() {
        return vendorAgreementName;
    }

    public void setVendorAgreementName(String vendorAgreementName) {
        this.vendorAgreementName = vendorAgreementName;
    }

    public boolean isSendForApproval() {
        return sendForApproval;
    }

    public void setSendForApproval(boolean sendForApproval) {
        this.sendForApproval = sendForApproval;
    }

}
