/**
 * Copyright 2017 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version 1.0, 19/09/17
 * @author aditya
 */
package com.uniware.core.api.ledger;

import com.uniware.core.entity.InventoryLedger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.common.base.Objects;
import com.unifier.core.api.base.ServiceResponse;

public class FetchInventoryLedgerSummaryResponse extends ServiceResponse {

    private Map<String, LedgerEntryGroupDTO> facilityToLedgerEntries = new HashMap<>();

    public Map<String, LedgerEntryGroupDTO> getFacilityToLedgerEntries() {
        return facilityToLedgerEntries;
    }

    public void setFacilityToLedgerEntries(Map<String, LedgerEntryGroupDTO> facilityToLedgerEntries) {
        this.facilityToLedgerEntries = facilityToLedgerEntries;
    }

    public static class LedgerEntryGroup {
        private int                     openingBalance;

        private Map<LedgerKey, Integer> countMap = new HashMap<>();

        public int getOpeningBalance() {
            return openingBalance;
        }

        public void setOpeningBalance(int openingBalance) {
            this.openingBalance = openingBalance;
        }

        public Map<LedgerKey, Integer> getCountMap() {
            return countMap;
        }

        public void setCountMap(Map<LedgerKey, Integer> countMap) {
            this.countMap = countMap;
        }
    }

    public static class LedgerEntryGroupDTO {
        private int                  openingBalance;

        private List<LedgerEntryDTO> ledgerEntryDTOs = new ArrayList<>();

        public int getOpeningBalance() {
            return openingBalance;
        }

        public void setOpeningBalance(int openingBalance) {
            this.openingBalance = openingBalance;
        }

        public List<LedgerEntryDTO> getLedgerEntryDTOs() {
            return ledgerEntryDTOs;
        }

        public void setLedgerEntryDTOs(List<LedgerEntryDTO> ledgerEntryDTOs) {
            this.ledgerEntryDTOs = ledgerEntryDTOs;
        }
    }

    /**
     * Groups on the basis of {@link InventoryLedger#transactionType} and {@link InventoryLedger#additionalInfo}
     */
    public static class LedgerKey {

        private String transactionType;

        private String additionalInfo;

        public LedgerKey(String transactionType, String additionalInfo) {
            this.transactionType = transactionType;
            this.additionalInfo = additionalInfo;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o)
                return true;
            if (o == null || getClass() != o.getClass())
                return false;
            LedgerKey ledgerKey = (LedgerKey) o;
            return Objects.equal(transactionType, ledgerKey.transactionType) && Objects.equal(additionalInfo, ledgerKey.additionalInfo);
        }

        @Override
        public int hashCode() {
            return Objects.hashCode(transactionType, additionalInfo);
        }

        public String getTransactionType() {
            return transactionType;
        }

        public void setTransactionType(String transactionType) {
            this.transactionType = transactionType;
        }

        public String getAdditionalInfo() {
            return additionalInfo;
        }

        public void setAdditionalInfo(String additionalInfo) {
            this.additionalInfo = additionalInfo;
        }
    }

    public static class LedgerEntryDTO {

        private String  transactionType;

        private String  additionalInfo;

        private Integer count;

        public String getTransactionType() {
            return transactionType;
        }

        public void setTransactionType(String transactionType) {
            this.transactionType = transactionType;
        }

        public String getAdditionalInfo() {
            return additionalInfo;
        }

        public void setAdditionalInfo(String additionalInfo) {
            this.additionalInfo = additionalInfo;
        }

        public Integer getCount() {
            return count;
        }

        public void setCount(Integer count) {
            this.count = count;
        }
    }
}
