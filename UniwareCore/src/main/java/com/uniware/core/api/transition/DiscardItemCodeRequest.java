/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 02-Jun-2012
 *  @author praveeng
 */
package com.uniware.core.api.transition;

import com.unifier.core.api.base.ServiceRequest;

import org.hibernate.validator.constraints.NotBlank;

/**
 * @author praveeng
 */
public class DiscardItemCodeRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -614577149542885230L;

    @NotBlank
    private String            itemCode;

    @NotBlank
    private String            itemSKU;

    /**
     * @return the itemCode
     */
    public String getItemCode() {
        return itemCode;
    }

    /**
     * @param itemCode the itemCode to set
     */
    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    /**
     * @return the itemSKU
     */
    public String getItemSKU() {
        return itemSKU;
    }

    /**
     * @param itemSKU the itemSKU to set
     */
    public void setItemSKU(String itemSKU) {
        this.itemSKU = itemSKU;
    }

}
