/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 27-Mar-2014
 *  @author parijat
 */
package com.uniware.core.api.recommendation;

import com.unifier.core.api.base.ServiceRequest;
import com.uniware.core.api.channel.ChannelItemTypeDTO;

/**
 * @author Sunny
 */
public class GetSimilarProductsRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long  serialVersionUID = 3461242331844154660L;

    private ChannelItemTypeDTO channelItemType;

    public GetSimilarProductsRequest() {
        super();
    }

    public GetSimilarProductsRequest(ChannelItemTypeDTO channelItemType) {
        super();
        this.channelItemType = channelItemType;
    }

    public ChannelItemTypeDTO getChannelItemType() {
        return channelItemType;
    }

    public void setChannelItemType(ChannelItemTypeDTO channelItemType) {
        this.channelItemType = channelItemType;
    }

}
