/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 15-Jan-2014
 *  @author akshay
 */
package com.uniware.core.api.catalog;

import com.unifier.core.api.base.ServiceResponse;
import com.uniware.core.entity.ShippingProviderLocation;

public class GetShippingProviderLocationDetailResponse extends ServiceResponse{


    /**
     * 
     */
    private static final long serialVersionUID = -4073458192014823898L;

    private ShippingProviderLocation shippingProviderLocation;

    public ShippingProviderLocation getShippingProviderLocation() {
        return shippingProviderLocation;
    }

    public void setShippingProviderLocation(ShippingProviderLocation shippingProviderLocation) {
        this.shippingProviderLocation = shippingProviderLocation;
    }
    

}
