/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jan 1, 2013
 *  @author praveeng
 */
package com.uniware.core.api.shipping;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author Sunny Agarwal
 */
public class GetAvailableRegulatoryFormsRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -4611912682299953808L;

    @NotBlank
    private String            shippingPackageCode;

    public GetAvailableRegulatoryFormsRequest() {
        super();
    }

    public GetAvailableRegulatoryFormsRequest(String shippingPackageCode) {
        super();
        this.shippingPackageCode = shippingPackageCode;
    }

    public String getShippingPackageCode() {
        return shippingPackageCode;
    }

    public void setShippingPackageCode(String shippingPackageCode) {
        this.shippingPackageCode = shippingPackageCode;
    }

}
