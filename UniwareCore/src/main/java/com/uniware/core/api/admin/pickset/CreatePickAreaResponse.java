/*
 * Copyright 2016 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 9/3/16 11:47 AM
 * @author amdalal
 */

package com.uniware.core.api.admin.pickset;

import com.unifier.core.api.base.ServiceResponse;

public class CreatePickAreaResponse extends ServiceResponse {

    private static final long serialVersionUID = -7106940215594916075L;

    private String            pickAreaName;

    public String getPickAreaName() {
        return pickAreaName;
    }

    public void setPickAreaName(String pickAreaName) {
        this.pickAreaName = pickAreaName;
    }
}
