/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jun 30, 2012
 *  @author singla
 */
package com.uniware.core.api.material;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

public class CreateGatePassRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -2281658182254903242L;

    @Valid
    @NotNull
    private WsGatePass        wsGatePass;

    private Integer           userId;

    @NotNull
    private String            type;

    @NotBlank
    private String            partyCode;

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return the partyCode
     */
    public String getPartyCode() {
        return partyCode;
    }

    /**
     * @param partyCode the partyCode to set
     */
    public void setPartyCode(String partyCode) {
        this.partyCode = partyCode;
    }

    /**
     * @return the wsGatePass
     */
    public WsGatePass getWsGatePass() {
        return wsGatePass;
    }

    /**
     * @param wsGatePass the wsGatePass to set
     */
    public void setWsGatePass(WsGatePass wsGatePass) {
        this.wsGatePass = wsGatePass;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

}
