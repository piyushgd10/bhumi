/*
 * Copyright 2017 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 18/8/17 11:51 AM
 * @author digvijaysharma
 */

package com.uniware.core.api.packer;

import org.hibernate.validator.constraints.NotBlank;

/**
 * Created by harshpal on 07/09/16.
 */
public class AllocateItemInPackageRequest extends AbstractAllocateItemRequest {

    private static final long serialVersionUID = -3971691439134064018L;

    @NotBlank
    private String            shippingPackageCode;

    @NotBlank
    private String            saleOrderCode;

    public String getShippingPackageCode() {
        return shippingPackageCode;
    }

    public void setShippingPackageCode(String shippingPackageCode) {
        this.shippingPackageCode = shippingPackageCode;
    }

    public String getSaleOrderCode() {
        return saleOrderCode;
    }

    public void setSaleOrderCode(String saleOrderCode) {
        this.saleOrderCode = saleOrderCode;
    }

}
