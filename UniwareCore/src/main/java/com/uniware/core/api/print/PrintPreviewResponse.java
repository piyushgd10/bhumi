/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 24-May-2012
 *  @author praveeng
 */
package com.uniware.core.api.print;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author praveeng
 */
public class PrintPreviewResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 8354216396797624020L;

}
