/*
 * Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 24/10/15 3:58 AM
 * @author unicom
 */

package com.uniware.core.api.saleorder;

import com.unifier.core.api.base.ServiceResponse;
import com.uniware.core.entity.SaleOrderItem;

import java.util.List;

/**
 * Created by unicom on 24/10/15.
 */
public class GetCancelledSaleOrdersResponse extends ServiceResponse {

    private List<SaleOrderDTO> saleOrders;

    public List<SaleOrderDTO> getSaleOrders() {
        return saleOrders;
    }

    public void setSaleOrders(List<SaleOrderDTO> cancelledSaleOrderDTO) {
        this.saleOrders = cancelledSaleOrderDTO;
    }

    public static class SaleOrderDTO {
        private String                 saleOrderCode;
        private String                 displaySaleOrderCode;
        private List<SaleOrderItemDTO> saleOrderItems;

        public SaleOrderDTO(String saleOrderCode, String displaySaleOrderCode, List<SaleOrderItemDTO> saleOrderItems) {
            this.saleOrderCode = saleOrderCode;
            this.displaySaleOrderCode = displaySaleOrderCode;
            this.saleOrderItems = saleOrderItems;
        }

        public SaleOrderDTO() {
        }

        public String getSaleOrderCode() {
            return saleOrderCode;
        }

        public void setSaleOrderCode(String saleOrderCode) {
            this.saleOrderCode = saleOrderCode;
        }

        public String getDisplaySaleOrderCode() {
            return displaySaleOrderCode;
        }

        public void setDisplaySaleOrderCode(String displaySaleOrderCode) {
            this.displaySaleOrderCode = displaySaleOrderCode;
        }

        public List<SaleOrderItemDTO> getSaleOrderItems() {
            return saleOrderItems;
        }

        public void setSaleOrderItems(List<SaleOrderItemDTO> saleOrderItems) {
            this.saleOrderItems = saleOrderItems;
        }
    }

    public static class SaleOrderItemDTO {
        private String saleOrderItemCode;
        private String channelProductId;
        private String skuCode;
        private String productDescription;

        public SaleOrderItemDTO() {
        }

        public SaleOrderItemDTO(SaleOrderItem saleOrderItem) {
            this.saleOrderItemCode = saleOrderItem.getCode();
            this.skuCode = saleOrderItem.getSellerSkuCode();
            this.productDescription = saleOrderItem.getItemType().getDescription();
            this.channelProductId = saleOrderItem.getChannelProductId();
        }

        public String getChannelProductId() {
            return channelProductId;
        }

        public void setChannelProductId(String channelProductId) {
            this.channelProductId = channelProductId;
        }

        public String getSaleOrderItemCode() {
            return saleOrderItemCode;
        }

        public void setSaleOrderItemCode(String saleOrderItemCode) {
            this.saleOrderItemCode = saleOrderItemCode;
        }

        public String getSkuCode() {
            return skuCode;
        }

        public void setSkuCode(String skuCode) {
            this.skuCode = skuCode;
        }

        public String getProductDescription() {
            return productDescription;
        }

        public void setProductDescription(String productDescription) {
            this.productDescription = productDescription;
        }
    }
}
