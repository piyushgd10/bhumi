/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 07-May-2012
 *  @author praveeng
 */
package com.uniware.core.api.shipping;

import java.util.Date;

import com.unifier.core.api.base.ServiceRequest;
import com.unifier.core.pagination.SearchOptions;

/**
 * @author praveeng
 */
public class SearchManifestRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 4625584457341419219L;
    private String            manifestCode;
    private String            generatedBy;
    private String            statusCode;
    private Integer           shippingProvider;
    private Date              fromDate;
    private Date              toDate;
    private SearchOptions     searchOptions;

    /**
     * @return the manifestCode
     */
    public String getManifestCode() {
        return manifestCode;
    }

    /**
     * @param manifestCode the manifestCode to set
     */
    public void setManifestCode(String manifestCode) {
        this.manifestCode = manifestCode;
    }

    /**
     * @return the generatedBy
     */
    public String getGeneratedBy() {
        return generatedBy;
    }

    /**
     * @param generatedBy the generatedBy to set
     */
    public void setGeneratedBy(String generatedBy) {
        this.generatedBy = generatedBy;
    }

    /**
     * @return the statusCode
     */
    public String getStatusCode() {
        return statusCode;
    }

    /**
     * @param statusCode the statusCode to set
     */
    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    /**
     * @return the fromDate
     */
    public Date getFromDate() {
        return fromDate;
    }

    /**
     * @param fromDate the fromDate to set
     */
    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    /**
     * @return the toDate
     */
    public Date getToDate() {
        return toDate;
    }

    /**
     * @param toDate the toDate to set
     */
    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    /**
     * @return the searchOptions
     */
    public SearchOptions getSearchOptions() {
        return searchOptions;
    }

    /**
     * @param searchOptions the searchOptions to set
     */
    public void setSearchOptions(SearchOptions searchOptions) {
        this.searchOptions = searchOptions;
    }

    /**
     * @return the shippingProvider
     */
    public Integer getShippingProvider() {
        return shippingProvider;
    }

    /**
     * @param shippingProvider the shippingProvider to set
     */
    public void setShippingProvider(Integer shippingProvider) {
        this.shippingProvider = shippingProvider;
    }

}
