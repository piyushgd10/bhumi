/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 1-July-2015
 *  @author akshay
 */
package com.uniware.core.api.shipping;

import com.unifier.core.api.base.ServiceResponse;

public class GetShippingPackageShipmentDetailsResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private String            statusCode;
    private String            shippingProviderCode;
    private String            trackingNumber;
    private String            shippingLabelLink;
    private String            label;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getShippingProviderCode() {
        return shippingProviderCode;
    }

    public void setShippingProviderCode(String shippingProviderCode) {
        this.shippingProviderCode = shippingProviderCode;
    }

    public String getTrackingNumber() {
        return trackingNumber;
    }

    public void setTrackingNumber(String trackingNumber) {
        this.trackingNumber = trackingNumber;
    }

    public String getShippingLabelLink() {
        return shippingLabelLink;
    }

    public void setShippingLabelLink(String shippingLabelLink) {
        this.shippingLabelLink = shippingLabelLink;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

}
