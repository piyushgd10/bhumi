/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 25-Dec-2013
 *  @author sunny
 */
package com.uniware.core.api.systemnotification.sandbox;

import java.util.ArrayList;
import java.util.List;

import com.uniware.core.entity.ShippingManifest;
import com.uniware.core.entity.ShippingManifestItem;

public class SandboxShippingManifestVO {

    private String                              code;
    private String                              statusCode;
    private String                              shippingProviderCode;
    private SandboxShippingMethodVO             shippingMethod;
    private String                              manifestLink;
    private List<SandboxShippingManifestItemVO> shippingManifestItems;

    public SandboxShippingManifestVO() {
        super();
    }

    public SandboxShippingManifestVO(ShippingManifest sm) {
        this.code = sm.getCode();
        this.statusCode = sm.getStatusCode();
        manifestLink = sm.getManifestLink();
        shippingProviderCode = sm.getShippingProviderCode() != null ? sm.getShippingProviderCode() : null;
        shippingMethod = sm.getShippingMethod() != null ? new SandboxShippingMethodVO(sm.getShippingMethod()) : null;
        shippingManifestItems = new ArrayList<SandboxShippingManifestItemVO>(sm.getShippingManifestItems().size());
        for (ShippingManifestItem smi : sm.getShippingManifestItems()) {
            shippingManifestItems.add(new SandboxShippingManifestItemVO(smi));
        }
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getShippingProviderCode() {
        return shippingProviderCode;
    }

    public void setShippingProviderCode(String shippingProviderCode) {
        this.shippingProviderCode = shippingProviderCode;
    }

    public SandboxShippingMethodVO getShippingMethod() {
        return shippingMethod;
    }

    public void setShippingMethod(SandboxShippingMethodVO shippingMethod) {
        this.shippingMethod = shippingMethod;
    }

    public String getManifestLink() {
        return manifestLink;
    }

    public void setManifestLink(String manifestLink) {
        this.manifestLink = manifestLink;
    }

    public List<SandboxShippingManifestItemVO> getShippingManifestItems() {
        return shippingManifestItems;
    }

    public void setShippingManifestItems(List<SandboxShippingManifestItemVO> shippingManifestItems) {
        this.shippingManifestItems = shippingManifestItems;
    }

}
