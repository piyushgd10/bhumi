/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 04-Apr-2014
 *  @author karunsingla
 */
package com.uniware.core.api.shipping;

import com.unifier.core.api.base.ServiceRequest;

public class GetPendingShipmentBatchRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 7963724015812016080L;

}
