/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 16, 2012
 *  @author praveeng
 */
package com.uniware.core.api.inventory;

import java.math.BigDecimal;

import com.uniware.core.entity.ItemType;
import com.uniware.core.entity.ItemTypeInventory;

/**
 * @author praveeng
 */
public class ItemTypeInventoryDTO {
    private String     shelf;
    private String     itemTypeName;
    private String     imageUrl;
    private String     productPageUrl;
    private String     itemSKU;
    private int        quantity;
    private BigDecimal maxRetailPrice;
    private int        quantityInPicking;
    private String     type;

    /**
     * @param inventory
     */
    public ItemTypeInventoryDTO(ItemTypeInventory inventory) {
        setShelf(inventory.getShelf().getCode());
        ItemType itemType = inventory.getItemType();
        setMaxRetailPrice(itemType.getMaxRetailPrice());
        setItemTypeName(itemType.getName());
        setItemSKU(itemType.getSkuCode());
        setImageUrl(itemType.getImageUrl());
        setProductPageUrl(itemType.getProductPageUrl());
        setQuantity(inventory.getQuantity());
        setType(inventory.getType().name());
    }

    /**
     * @return the shelf
     */
    public String getShelf() {
        return shelf;
    }

    /**
     * @param shelf the shelf to set
     */
    public void setShelf(String shelf) {
        this.shelf = shelf;
    }

    /**
     * @return the itemTypeName
     */
    public String getItemTypeName() {
        return itemTypeName;
    }

    /**
     * @param itemTypeName the itemTypeName to set
     */
    public void setItemTypeName(String itemTypeName) {
        this.itemTypeName = itemTypeName;
    }

    /**
     * @return the imageUrl
     */
    public String getImageUrl() {
        return imageUrl;
    }

    /**
     * @param imageUrl the imageUrl to set
     */
    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    /**
     * @return the productPageUrl
     */
    public String getProductPageUrl() {
        return productPageUrl;
    }

    /**
     * @param productPageUrl the productPageUrl to set
     */
    public void setProductPageUrl(String productPageUrl) {
        this.productPageUrl = productPageUrl;
    }

    /**
     * @return the itemSKU
     */
    public String getItemSKU() {
        return itemSKU;
    }

    /**
     * @param itemSKU the itemSKU to set
     */
    public void setItemSKU(String itemSKU) {
        this.itemSKU = itemSKU;
    }

    /**
     * @return the quantity
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     * @param quantity the quantity to set
     */
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    /**
     * @return the quantityInPicking
     */
    public int getQuantityInPicking() {
        return quantityInPicking;
    }

    /**
     * @param quantityInPicking the quantityInPicking to set
     */
    public void setQuantityInPicking(int quantityInPicking) {
        this.quantityInPicking = quantityInPicking;
    }

    /**
     * @return the maxRetailPrice
     */
    public BigDecimal getMaxRetailPrice() {
        return maxRetailPrice;
    }

    /**
     * @param maxRetailPrice the maxRetailPrice to set
     */
    public void setMaxRetailPrice(BigDecimal maxRetailPrice) {
        this.maxRetailPrice = maxRetailPrice;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    
}
