/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Apr 7, 2012
 *  @author singla
 */
package com.uniware.core.api.purchase;

import com.unifier.core.api.base.ServiceRequest;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author singla
 */
public class RemovePurchaseCartItemsRequest extends ServiceRequest {
    /**
     * 
     */
    private static final long              serialVersionUID = 8152461720350662439L;

    @NotNull
    private Integer                        userId;

    @NotEmpty
    @Valid
    private List<WsRemovePurchaseCartItem> removePurchaseCartItems;

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public List<WsRemovePurchaseCartItem> getRemovePurchaseCartItems() {
        return removePurchaseCartItems;
    }

    public void setRemovePurchaseCartItems(List<WsRemovePurchaseCartItem> removePurchaseCartItems) {
        this.removePurchaseCartItems = removePurchaseCartItems;
    }
}
