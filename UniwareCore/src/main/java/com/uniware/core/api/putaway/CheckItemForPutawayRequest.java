/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jun 2, 2012
 *  @author singla
 */
package com.uniware.core.api.putaway;

import com.unifier.core.api.base.ServiceRequest;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author singla
 */
public class CheckItemForPutawayRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -6999822264858899169L;

    @NotEmpty
    private String            itemCode;

    @NotBlank
    private String            putawayCode;

    /**
     * @return the itemCodes
     */
    public String getItemCode() {
        return itemCode;
    }

    /**
     * @param itemCodes the itemCodes to set
     */
    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    /**
     * @return the putawayId
     */
    public String getPutawayCode() {
        return putawayCode;
    }

    /**
     * @param putawayId the putawayId to set
     */
    public void setPutawayCode(String putawayCode) {
        this.putawayCode = putawayCode;
    }

}
