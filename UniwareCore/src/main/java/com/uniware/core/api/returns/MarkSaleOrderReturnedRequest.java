/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 9, 2012
 *  @author singla
 */
package com.uniware.core.api.returns;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import com.google.gson.Gson;
import com.unifier.core.api.base.ServiceRequest;
import com.uniware.core.entity.ItemTypeInventory;

/**
 * @author singla
 */
public class MarkSaleOrderReturnedRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long     serialVersionUID = 4795720002305203341L;

    @NotBlank
    private String                saleOrderCode;

    @NotEmpty
    @Valid
    private List<WsSaleOrderItem> saleOrderItems;

    @NotBlank
    private String                returnReason;

    private String                returnCode;

    @NotNull
    private Integer               userId;

    public String getSaleOrderCode() {
        return saleOrderCode;
    }

    public void setSaleOrderCode(String saleOrderCode) {
        this.saleOrderCode = saleOrderCode;
    }

    public List<WsSaleOrderItem> getSaleOrderItems() {
        return saleOrderItems;
    }

    public void setSaleOrderItems(List<WsSaleOrderItem> saleOrderItems) {
        this.saleOrderItems = saleOrderItems;
    }

    public String getReturnCode() {
        return returnCode;
    }

    public void setReturnCode(String returnCode) {
        this.returnCode = returnCode;
    }

    public String getReturnReason() {
        return returnReason;
    }

    public void setReturnReason(String returnReason) {
        this.returnReason = returnReason;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public static class WsSaleOrderItem {

        @NotBlank
        private String code;

        private String status = ItemTypeInventory.Type.BAD_INVENTORY.name();

        private String shelfCode;

        private String reason;

        public WsSaleOrderItem(String code, String status) {
            super();
            this.code = code;
            this.status = status;
        }

        public WsSaleOrderItem() {
            super();
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getShelfCode() { return shelfCode; }

        public void setShelfCode(String shelfCode) { this.shelfCode = shelfCode; }

        public String getReason() { return reason; }

        public void setReason(String reason) { this.reason = reason; }

        @Override
        public String toString() {
            return "WsSaleOrderItem [code=" + code + ", status=" + status + "]";
        }

    }

    public static WsSaleOrderItem getSaleOrderItemByCodeFromList(
            List<WsSaleOrderItem> saleOrderItems, String code)
    {
        if (saleOrderItems!=null && code != null && !code.isEmpty()) {
            for (WsSaleOrderItem saleOrderItem : saleOrderItems) {
                if (code.equals(saleOrderItem.getCode())) {
                    return saleOrderItem;
                }
            }
        }
        return null;
    }

    public static void main(String[] args) {
        MarkSaleOrderReturnedRequest request = new MarkSaleOrderReturnedRequest();
        request.setSaleOrderCode("dasddasdas");
        request.setReturnReason("dasd dasd");
        List<WsSaleOrderItem> saleOrderItems = new ArrayList<MarkSaleOrderReturnedRequest.WsSaleOrderItem>();
        WsSaleOrderItem orderItem = new WsSaleOrderItem();
        orderItem.setCode("dsadasd");
        orderItem.setStatus("BAD_INVENTORY");
        saleOrderItems.add(orderItem);
        request.setSaleOrderItems(saleOrderItems);
        System.out.println(new Gson().toJson(request));
    }
}
