/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 17-Mar-2012
 *  @author vibhu
 */
package com.uniware.core.api.returns;

import com.uniware.core.api.saleorder.AddressDTO;

/**
 * @author vibhu
 */
public class ReshipmentDTO {

    private int        id;
    private String     code;
    private String     saleOrderCode;
    private String     reshipmentSaleOrderCode;
    private String     reshipmentSaleOrderStatus;
    private String     actionCode;
    private String     actionDescription;
    private int        shippingPackageId;
    private String     shippingPackageCode;
    private AddressDTO shippingAddress;
    private String     reshipmentStatus;

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @return the shippingPackageId
     */
    public int getShippingPackageId() {
        return shippingPackageId;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @param shippingPackageId the shippingPackageId to set
     */
    public void setShippingPackageId(int shippingPackageId) {
        this.shippingPackageId = shippingPackageId;
    }

    /**
     * @return the shippingAddress
     */
    public AddressDTO getShippingAddress() {
        return shippingAddress;
    }

    /**
     * @param shippingAddress the shippingAddress to set
     */
    public void setShippingAddress(AddressDTO shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    /**
     * @return the reshipmentStatus
     */
    public String getReshipmentStatus() {
        return reshipmentStatus;
    }

    /**
     * @param reshipmentStatus the reshipmentStatus to set
     */
    public void setReshipmentStatus(String reshipmentStatus) {
        this.reshipmentStatus = reshipmentStatus;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the saleOrderCode
     */
    public String getSaleOrderCode() {
        return saleOrderCode;
    }

    /**
     * @param saleOrderCode the saleOrderCode to set
     */
    public void setSaleOrderCode(String saleOrderCode) {
        this.saleOrderCode = saleOrderCode;
    }

    /**
     * @return the reshipmentSaleOrderCode
     */
    public String getReshipmentSaleOrderCode() {
        return reshipmentSaleOrderCode;
    }

    /**
     * @param reshipmentSaleOrderCode the reshipmentSaleOrderCode to set
     */
    public void setReshipmentSaleOrderCode(String reshipmentSaleOrderCode) {
        this.reshipmentSaleOrderCode = reshipmentSaleOrderCode;
    }

    /**
     * @return the actionCode
     */
    public String getActionCode() {
        return actionCode;
    }

    /**
     * @param actionCode the actionCode to set
     */
    public void setActionCode(String actionCode) {
        this.actionCode = actionCode;
    }

    /**
     * @return the actionDescription
     */
    public String getActionDescription() {
        return actionDescription;
    }

    /**
     * @param actionDescription the actionDescription to set
     */
    public void setActionDescription(String actionDescription) {
        this.actionDescription = actionDescription;
    }

    /**
     * @return the reshipmentSaleOrderStatus
     */
    public String getReshipmentSaleOrderStatus() {
        return reshipmentSaleOrderStatus;
    }

    /**
     * @param reshipmentSaleOrderStatus the reshipmentSaleOrderStatus to set
     */
    public void setReshipmentSaleOrderStatus(String reshipmentSaleOrderStatus) {
        this.reshipmentSaleOrderStatus = reshipmentSaleOrderStatus;
    }

    /**
     * @return the shippingPackageCode
     */
    public String getShippingPackageCode() {
        return shippingPackageCode;
    }

    /**
     * @param shippingPackageCode the shippingPackageCode to set
     */
    public void setShippingPackageCode(String shippingPackageCode) {
        this.shippingPackageCode = shippingPackageCode;
    }

}
