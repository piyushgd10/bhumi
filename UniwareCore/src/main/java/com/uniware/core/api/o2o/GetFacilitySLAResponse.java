/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 *  @version     1.0, Sep 18, 2015
 *  @author akshay
 */
package com.uniware.core.api.o2o;

import com.unifier.core.api.base.ServiceResponse;

public class GetFacilitySLAResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -541498416267292329L;
    
    private Integer dispatchSLA;
    
    private Integer deliverySLA;

    public Integer getDispatchSLA() {
        return dispatchSLA;
    }

    public void setDispatchSLA(Integer dispatchSLA) {
        this.dispatchSLA = dispatchSLA;
    }

    public Integer getDeliverySLA() {
        return deliverySLA;
    }

    public void setDeliverySLA(Integer deliverySLA) {
        this.deliverySLA = deliverySLA;
    }

    
}
