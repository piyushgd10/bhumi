package com.uniware.core.api.cyclecount;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

/**
 * Created by harshpal on 21/04/16.
 */
public class RecountShelfRequest extends ServiceRequest {

    @NotBlank
    private String shelfCode;

    @NotBlank
    private String subCycleCountCode;

    public String getShelfCode() {
        return shelfCode;
    }

    public void setShelfCode(String shelfCode) {
        this.shelfCode = shelfCode;
    }

    public String getSubCycleCountCode() {
        return subCycleCountCode;
    }

    public void setSubCycleCountCode(String subCycleCountCode) {
        this.subCycleCountCode = subCycleCountCode;
    }
}
