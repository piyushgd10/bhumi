/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Sep 7, 2012
 *  @author singla
 */
package com.uniware.core.api.invoice;

import com.unifier.core.api.base.ServiceResponse;

public class UpdateInvoiceResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -688152510477113612L;

    private Integer           invoiceId;

    private String            shippingPackageCode;

    /**
     * @return the invoiceId
     */
    public Integer getInvoiceId() {
        return invoiceId;
    }

    /**
     * @param invoiceId the invoiceId to set
     */
    public void setInvoiceId(Integer invoiceId) {
        this.invoiceId = invoiceId;
    }

    /**
     * @return the shippingPackageCode
     */
    public String getShippingPackageCode() {
        return shippingPackageCode;
    }

    /**
     * @param shippingPackageCode the shippingPackageCode to set
     */
    public void setShippingPackageCode(String shippingPackageCode) {
        this.shippingPackageCode = shippingPackageCode;
    }
}
