package com.uniware.core.api.packer;

import java.util.List;

import org.hibernate.validator.constraints.NotEmpty;

import com.unifier.core.api.base.ServiceRequest;
import com.uniware.core.entity.Picklist;

/**
 * Created by harshpal on 17/11/16.
 */
public class AcceptPackagePutbackRequest extends ServiceRequest {

    private static final long   serialVersionUID = -4579732005362295955L;
    @NotEmpty
    public List<String>         shippingPackageCodes;

    public Picklist.Destination destination      = Picklist.Destination.STAGING;

    public List<String> getShippingPackageCodes() {
        return shippingPackageCodes;
    }

    public void setShippingPackageCodes(List<String> shippingPackageCodes) {
        this.shippingPackageCodes = shippingPackageCodes;
    }

    public Picklist.Destination getDestination() {
        return destination;
    }

    public void setDestination(Picklist.Destination destination) {
        this.destination = destination;
    }
}
