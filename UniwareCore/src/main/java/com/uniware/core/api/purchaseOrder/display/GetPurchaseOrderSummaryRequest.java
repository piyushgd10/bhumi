/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, May 8, 2015
 *  @author harsh
 */
package com.uniware.core.api.purchaseOrder.display;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author harsh
 */
public class GetPurchaseOrderSummaryRequest extends ServiceRequest {

    private static final long serialVersionUID = 3762822159923408503L;

    @NotBlank
    private String            purchaseOrderCode;

    public String getPurchaseOrderCode() {
        return purchaseOrderCode;
    }

    public void setPurchaseOrderCode(String purchaseOrderCode) {
        this.purchaseOrderCode = purchaseOrderCode;
    }

}
