/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 24-Dec-2013
 *  @author parijat
 */
package com.uniware.core.api.tax;

import java.util.List;

import com.unifier.core.api.base.ServiceResponse;
import com.uniware.core.api.catalog.TaxTypeDTO;

/**
 * @author parijat
 *
 */
public class GetAllTaxTypesResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 2381390152765229827L;

    private List<TaxTypeDTO>  allTaxTypes;

    /**
     * @return the allTaxTypes
     */
    public List<TaxTypeDTO> getAllTaxTypes() {
        return allTaxTypes;
    }

    /**
     * @param allTaxTypes the allTaxTypes to set
     */
    public void setAllTaxTypes(List<TaxTypeDTO> allTaxTypes) {
        this.allTaxTypes = allTaxTypes;
    }

}
