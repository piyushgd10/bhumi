package com.uniware.core.api.invoice;

import java.math.BigDecimal;

/**
 * Created by akshayag on 9/1/15.
 */
public class InvoiceItemDTO {
    private String     itemSku;
    private String     channelSku;
    private int        quantity;
    private BigDecimal unitPrice;
    private BigDecimal sellingPrice;
    private BigDecimal shippingCharges             = BigDecimal.ZERO;
    private BigDecimal shippingMethodCharges       = BigDecimal.ZERO;
    private BigDecimal cashOnDeliveryCharges       = BigDecimal.ZERO;
    private BigDecimal giftWrapCharges             = BigDecimal.ZERO;
    private BigDecimal prepaidAmount               = BigDecimal.ZERO;
    private BigDecimal voucherValue;
    private BigDecimal storeCredit;
    private BigDecimal subtotal;
    private BigDecimal total;
    private BigDecimal discount;
    private BigDecimal vat;
    private BigDecimal cst;
    private BigDecimal integratedGst               = BigDecimal.ZERO;
    private BigDecimal unionTerritoryGst           = BigDecimal.ZERO;
    private BigDecimal stateGst                    = BigDecimal.ZERO;
    private BigDecimal centralGst                  = BigDecimal.ZERO;
    private BigDecimal compensationCess            = BigDecimal.ZERO;
    private BigDecimal additionalTax;
    private BigDecimal totalTax;
    private BigDecimal taxPercentage;
    private BigDecimal vatPercentage;
    private BigDecimal cstPercentage;
    private BigDecimal integratedGstPercentage     = BigDecimal.ZERO;
    private BigDecimal unionTerritoryGstPercentage = BigDecimal.ZERO;
    private BigDecimal stateGstPercentage          = BigDecimal.ZERO;
    private BigDecimal centralGstPercentage        = BigDecimal.ZERO;
    private BigDecimal compensationCessPercentage  = BigDecimal.ZERO;
    private BigDecimal additionalTaxPercentage;

    public String getItemSku() {
        return itemSku;
    }

    public void setItemSku(String itemSku) {
        this.itemSku = itemSku;
    }

    public String getChannelSku() {
        return channelSku;
    }

    public void setChannelSku(String channelSku) {
        this.channelSku = channelSku;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }

    public BigDecimal getSellingPrice() {
        return sellingPrice;
    }

    public void setSellingPrice(BigDecimal sellingPrice) {
        this.sellingPrice = sellingPrice;
    }

    public BigDecimal getShippingCharges() {
        return shippingCharges;
    }

    public void setShippingCharges(BigDecimal shippingCharges) {
        this.shippingCharges = shippingCharges;
    }

    public BigDecimal getShippingMethodCharges() {
        return shippingMethodCharges;
    }

    public void setShippingMethodCharges(BigDecimal shippingMethodCharges) {
        this.shippingMethodCharges = shippingMethodCharges;
    }

    public BigDecimal getCashOnDeliveryCharges() {
        return cashOnDeliveryCharges;
    }

    public void setCashOnDeliveryCharges(BigDecimal cashOnDeliveryCharges) {
        this.cashOnDeliveryCharges = cashOnDeliveryCharges;
    }

    public BigDecimal getGiftWrapCharges() {
        return giftWrapCharges;
    }

    public void setGiftWrapCharges(BigDecimal giftWrapCharges) {
        this.giftWrapCharges = giftWrapCharges;
    }

    public BigDecimal getPrepaidAmount() {
        return prepaidAmount;
    }

    public void setPrepaidAmount(BigDecimal prepaidAmount) {
        this.prepaidAmount = prepaidAmount;
    }

    public BigDecimal getVoucherValue() {
        return voucherValue;
    }

    public void setVoucherValue(BigDecimal voucherValue) {
        this.voucherValue = voucherValue;
    }

    public BigDecimal getStoreCredit() {
        return storeCredit;
    }

    public void setStoreCredit(BigDecimal storeCredit) {
        this.storeCredit = storeCredit;
    }

    public BigDecimal getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(BigDecimal subtotal) {
        this.subtotal = subtotal;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    public BigDecimal getVat() {
        return vat;
    }

    public void setVat(BigDecimal vat) {
        this.vat = vat;
    }

    public BigDecimal getCst() {
        return cst;
    }

    public void setCst(BigDecimal cst) {
        this.cst = cst;
    }

    public BigDecimal getIntegratedGst() {
        return integratedGst;
    }

    public void setIntegratedGst(BigDecimal integratedGst) {
        this.integratedGst = integratedGst;
    }

    public BigDecimal getUnionTerritoryGst() {
        return unionTerritoryGst;
    }

    public void setUnionTerritoryGst(BigDecimal unionTerritoryGst) {
        this.unionTerritoryGst = unionTerritoryGst;
    }

    public BigDecimal getStateGst() {
        return stateGst;
    }

    public void setStateGst(BigDecimal stateGst) {
        this.stateGst = stateGst;
    }

    public BigDecimal getCentralGst() {
        return centralGst;
    }

    public void setCentralGst(BigDecimal centralGst) {
        this.centralGst = centralGst;
    }

    public BigDecimal getCompensationCess() {
        return compensationCess;
    }

    public void setCompensationCess(BigDecimal compensationCess) {
        this.compensationCess = compensationCess;
    }

    public BigDecimal getAdditionalTax() {
        return additionalTax;
    }

    public void setAdditionalTax(BigDecimal additionalTax) {
        this.additionalTax = additionalTax;
    }

    public BigDecimal getTotalTax() {
        return totalTax;
    }

    public void setTotalTax(BigDecimal totalTax) {
        this.totalTax = totalTax;
    }

    public BigDecimal getTaxPercentage() {
        return taxPercentage;
    }

    public void setTaxPercentage(BigDecimal taxPercentage) {
        this.taxPercentage = taxPercentage;
    }

    public BigDecimal getAdditionalTaxPercentage() {
        return additionalTaxPercentage;
    }

    public void setAdditionalTaxPercentage(BigDecimal additionalTaxPercentage) {
        this.additionalTaxPercentage = additionalTaxPercentage;
    }

    public BigDecimal getVatPercentage() {
        return vatPercentage;
    }

    public void setVatPercentage(BigDecimal vatPercentage) {
        this.vatPercentage = vatPercentage;
    }

    public BigDecimal getCstPercentage() {
        return cstPercentage;
    }

    public void setCstPercentage(BigDecimal cstPercentage) {
        this.cstPercentage = cstPercentage;
    }

    public BigDecimal getIntegratedGstPercentage() {
        return integratedGstPercentage;
    }

    public void setIntegratedGstPercentage(BigDecimal integratedGstPercentage) {
        this.integratedGstPercentage = integratedGstPercentage;
    }

    public BigDecimal getUnionTerritoryGstPercentage() {
        return unionTerritoryGstPercentage;
    }

    public void setUnionTerritoryGstPercentage(BigDecimal unionTerritoryGstPercentage) {
        this.unionTerritoryGstPercentage = unionTerritoryGstPercentage;
    }

    public BigDecimal getStateGstPercentage() {
        return stateGstPercentage;
    }

    public void setStateGstPercentage(BigDecimal stateGstPercentage) {
        this.stateGstPercentage = stateGstPercentage;
    }

    public BigDecimal getCentralGstPercentage() {
        return centralGstPercentage;
    }

    public void setCentralGstPercentage(BigDecimal centralGstPercentage) {
        this.centralGstPercentage = centralGstPercentage;
    }

    public BigDecimal getCompensationCessPercentage() {
        return compensationCessPercentage;
    }

    public void setCompensationCessPercentage(BigDecimal compensationCessPercentage) {
        this.compensationCessPercentage = compensationCessPercentage;
    }
}