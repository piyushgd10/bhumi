/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 22-Mar-2014
 *  @author karunsingla
 */
package com.uniware.core.api.inflow;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.unifier.core.api.base.ServiceRequest;

public class CreateVendorDebitInvoiceRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long    serialVersionUID = 7024325297239043978L;

    @NotNull
    @Valid
    private WsVendorDebitInvoice vendorInvoice;

    @NotNull
    private Integer              userId;

    public WsVendorDebitInvoice getVendorInvoice() {
        return vendorInvoice;
    }

    public void setVendorInvoice(WsVendorDebitInvoice vendorInvoice) {
        this.vendorInvoice = vendorInvoice;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }
}
