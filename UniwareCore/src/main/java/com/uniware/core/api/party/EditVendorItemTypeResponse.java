/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 23, 2012
 *  @author praveeng
 */
package com.uniware.core.api.party;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author praveeng
 */
public class EditVendorItemTypeResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -5794216590664362166L;

}
