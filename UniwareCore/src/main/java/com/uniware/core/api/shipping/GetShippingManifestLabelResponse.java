package com.uniware.core.api.shipping;

import com.unifier.core.api.base.ServiceResponse;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Created by akshayag on 9/7/15.
 */
public class GetShippingManifestLabelResponse extends ServiceResponse {

    private static final long serialVersionUID = -8669512589911806312L;

    @NotBlank
    private String            shippingManifest;

    public String getShippingManifest() {
        return shippingManifest;
    }

    public void setShippingManifest(String shippingManifest) {
        this.shippingManifest = shippingManifest;
    }
}
