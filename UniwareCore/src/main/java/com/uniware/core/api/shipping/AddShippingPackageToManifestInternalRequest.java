/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 29-Jan-2015
 *  @author akshaykochhar
 */
package com.uniware.core.api.shipping;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

public class AddShippingPackageToManifestInternalRequest extends ServiceRequest{

    /**
     * 
     */
    private static final long serialVersionUID = -6342311903183525018L;
    
    @NotBlank
    private String            shippingManifestCode;

    @NotNull
    private String            awbOrShipmentCode;

    private String            shippingPackageTypeCode;

    public AddShippingPackageToManifestInternalRequest() {
        super();
    }

    public AddShippingPackageToManifestInternalRequest(String shippingManifestCode, String awbOrShipmentCode) {
        super();
        this.shippingManifestCode = shippingManifestCode;
        this.awbOrShipmentCode = awbOrShipmentCode;
    }
    
    public AddShippingPackageToManifestInternalRequest(String shippingManifestCode, String awbOrShipmentCode, String shippingPackageTypeCode) {
        super();
        this.shippingManifestCode = shippingManifestCode;
        this.awbOrShipmentCode = awbOrShipmentCode;
        this.shippingPackageTypeCode = shippingPackageTypeCode;
    }

    /**
     * @return the shippingManifestCode
     */
    public String getShippingManifestCode() {
        return shippingManifestCode;
    }

    /**
     * @param shippingManifestCode the shippingManifestCode to set
     */
    public void setShippingManifestCode(String shippingManifestCode) {
        this.shippingManifestCode = shippingManifestCode;
    }

    /**
     * @return the awbOrShipmentCode
     */
    public String getAwbOrShipmentCode() {
        return awbOrShipmentCode;
    }

    /**
     * @param awbOrShipmentCode the awbOrShipmentCode to set
     */
    public void setAwbOrShipmentCode(String awbOrShipmentCode) {
        this.awbOrShipmentCode = awbOrShipmentCode;
    }

    /**
     * @return the shippingPackageTypeCode
     */
    public String getShippingPackageTypeCode() {
        return shippingPackageTypeCode;
    }

    /**
     * @param shippingPackageTypeCode the shippingPackageTypeCode to set
     */
    public void setShippingPackageTypeCode(String shippingPackageTypeCode) {
        this.shippingPackageTypeCode = shippingPackageTypeCode;
    }

}
