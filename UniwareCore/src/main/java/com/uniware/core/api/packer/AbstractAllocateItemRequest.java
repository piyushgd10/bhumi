/*
 * Copyright 2017 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 18/8/17 11:51 AM
 * @author digvijaysharma
 */
package com.uniware.core.api.packer;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author singla
 */
public class AbstractAllocateItemRequest extends ServiceRequest {

    /**
     *
     */
    private static final long serialVersionUID = -6205232394715646136L;

    @NotNull
    private String            itemCode;

    @NotBlank
    private String            picklistCode;

    private String            pickBucketCode;

    private Integer           userId;

    /**
     * @return the itemCode
     */
    public String getItemCode() {
        return itemCode;
    }

    /**
     * @param itemCode the itemCode to set
     */
    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getPicklistCode() {
        return picklistCode;
    }

    public void setPicklistCode(String picklistCode) {
        this.picklistCode = picklistCode;
    }

    public String getPickBucketCode() {
        return pickBucketCode;
    }

    public void setPickBucketCode(String pickBucketCode) {
        this.pickBucketCode = pickBucketCode;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }
}
