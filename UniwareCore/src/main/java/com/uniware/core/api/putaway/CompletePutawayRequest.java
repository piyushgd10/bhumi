/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 3, 2012
 *  @author singla
 */
package com.uniware.core.api.putaway;

import com.unifier.core.api.base.ServiceRequest;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author singla
 */
public class CompletePutawayRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 5397757953829722119L;
    @NotEmpty
    private String            putawayCode;

    /**
     * @return the putawayCode
     */
    public String getPutawayCode() {
        return putawayCode;
    }

    /**
     * @param putawayCode the putawayCode to set
     */
    public void setPutawayCode(String putawayCode) {
        this.putawayCode = putawayCode;
    }
}
