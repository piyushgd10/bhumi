/*
 * Copyright 2016 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 2/9/16 5:19 PM
 * @author bhuvneshwarkumar
 */

package com.uniware.core.api.picker;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

/**
 * Created by bhuvneshwarkumar on 02/09/16.
 */
public class GetPicklistDetailRequest extends ServiceRequest {

    private static final long serialVersionUID = 2857602697609532088L;

    @NotBlank
    private String            picklistCode;

    public String getPicklistCode() {
        return picklistCode;
    }

    public void setPicklistCode(String picklistCode) {
        this.picklistCode = picklistCode;
    }
}
