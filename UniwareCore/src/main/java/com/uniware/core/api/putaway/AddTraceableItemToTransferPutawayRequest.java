/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 10-May-2013
 *  @author karunsingla
 */
package com.uniware.core.api.putaway;

import com.unifier.core.api.base.ServiceRequest;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

/**
 * <ul>
 * Parameters:
 * <li>{@code itemCode}: item code</li>
 * <li>{@code userId}: id of user, set by context</li>
 * <li>{@code putawayCode}: code of putaway in which gatepass items are to be added.</li>
 * <li>{@code shelfCdoe}: shelf code on which item is to be transferred.</li>
 * </ul>
 */
public class AddTraceableItemToTransferPutawayRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -6692331925784794209L;

    @NotBlank
    private String            itemCode;

    @NotNull
    private Integer           userId;

    @NotBlank
    private String            putawayCode;

    private String            shelfCode;

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return the itemCode
     */
    public String getItemCode() {
        return itemCode;
    }

    /**
     * @param itemCode the itemCode to set
     */
    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    /**
     * @return the putawayCode
     */
    public String getPutawayCode() {
        return putawayCode;
    }

    /**
     * @param putawayCode the putawayCode to set
     */
    public void setPutawayCode(String putawayCode) {
        this.putawayCode = putawayCode;
    }

    public String getShelfCode() {
        return shelfCode;
    }

    public void setShelfCode(String shelfCode) {
        this.shelfCode = shelfCode;
    }
}
