package com.uniware.core.api.saleorder;

import com.unifier.core.api.base.ServiceResponse;
import com.unifier.core.ui.SelectItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bhuvneshwarkumar on 07/08/15.
 */
public class GetCancellationReasonsResponse extends ServiceResponse {

    private List<SelectItem> cancellationReasons;

    public List<SelectItem> getCancellationReasons() {
        return cancellationReasons;
    }

    public void setCancellationReasons(List<SelectItem> cancellationReasons) {
        this.cancellationReasons = cancellationReasons;
    }
}
