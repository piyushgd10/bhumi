/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 02-Apr-2015
 *  @author unicommerce
 */
package com.uniware.core.api.putaway;

import java.util.List;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.unifier.core.api.base.ServiceRequest;

public class SplitPutawayItemRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long        serialVersionUID = 1L;

    @NotNull
    private Integer                  putawayItemId;

    @NotEmpty
    private List<PutawayItemToSplit> putawayItems;

    public static class PutawayItemToSplit {

        private String  shelfCode;

        private Integer quantity;

        public String getShelfCode() {
            return shelfCode;
        }

        public void setShelfCode(String shelfCode) {
            this.shelfCode = shelfCode;
        }

        public Integer getQuantity() {
            return quantity;
        }

        public void setQuantity(Integer quantity) {
            this.quantity = quantity;
        }
    }

    public List<PutawayItemToSplit> getPutawayItems() {
        return putawayItems;
    }

    public void setPutawayItems(List<PutawayItemToSplit> putawayItems) {
        this.putawayItems = putawayItems;
    }

    public Integer getPutawayItemId() {
        return putawayItemId;
    }

    public void setPutawayItemId(Integer putawayItemId) {
        this.putawayItemId = putawayItemId;
    }

}
