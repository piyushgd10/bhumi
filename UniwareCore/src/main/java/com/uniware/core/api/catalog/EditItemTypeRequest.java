/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 9, 2012
 *  @author praveeng
 */
package com.uniware.core.api.catalog;

import com.unifier.core.api.base.ServiceRequest;

import javax.validation.Valid;

/**
 * @author praveeng
 */
public class EditItemTypeRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -8762524954443175974L;

    @Valid
    private WsItemType        itemType;

    /**
     * @return the itemType
     */
    public WsItemType getItemType() {
        return itemType;
    }

    /**
     * @param itemType the itemType to set
     */
    public void setItemType(WsItemType itemType) {
        this.itemType = itemType;
    }
}
