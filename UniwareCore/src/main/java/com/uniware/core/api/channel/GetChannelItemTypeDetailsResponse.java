/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 9, 2012
 *  @author praveeng
 */
package com.uniware.core.api.channel;

import java.util.List;

import com.unifier.core.api.base.ServiceResponse;
import com.uniware.core.api.catalog.dto.SimilarItemTypeDTO;

/**
 * @author Sunny Agarwal
 */
public class GetChannelItemTypeDetailsResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long        serialVersionUID = -5466574933334174621L;

    private ChannelItemTypeDTO       channelItemTypeDTO;

    private List<SimilarItemTypeDTO> similarProducts;

    public ChannelItemTypeDTO getChannelItemTypeDTO() {
        return channelItemTypeDTO;
    }

    public void setChannelItemTypeDTO(ChannelItemTypeDTO channelItemTypeDTO) {
        this.channelItemTypeDTO = channelItemTypeDTO;
    }

    public List<SimilarItemTypeDTO> getSimilarProducts() {
        return similarProducts;
    }

    public void setSimilarProducts(List<SimilarItemTypeDTO> similarProducts) {
        this.similarProducts = similarProducts;
    }

}
