/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jun 20, 2012
 *  @author ankit
 */
package com.uniware.core.api.tax;

import com.unifier.core.api.base.ServiceResponse;

import com.uniware.core.api.catalog.TaxTypeConfigurationDTO;

/**
 * @author ankit
 */
public class CreateTaxTypeConfigurationResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long       serialVersionUID = -4747152853048677706L;
    private TaxTypeConfigurationDTO taxTypeConfigurationDTO;

    /**
     * @return the taxTypeConfigurationDTO
     */
    public TaxTypeConfigurationDTO getTaxTypeConfigurationDTO() {
        return taxTypeConfigurationDTO;
    }

    /**
     * @param taxTypeConfigurationDTO the taxTypeConfigurationDTO to set
     */
    public void setTaxTypeConfigurationDTO(TaxTypeConfigurationDTO taxTypeConfigurationDTO) {
        this.taxTypeConfigurationDTO = taxTypeConfigurationDTO;
    }

}
