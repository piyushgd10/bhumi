/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Oct 23, 2015
 *  @author akshay
 */
package com.uniware.core.api.prices;

import java.math.BigDecimal;

import javax.validation.constraints.Digits;

import org.hibernate.validator.constraints.NotBlank;

public class CheckChannelItemTypePriceRequest extends AbstractChannelItemTypeRequest {

    private static final long serialVersionUID = -5694495120279883380L;
   
    @Digits(integer = 10, fraction = 2 , message = "max value cannot have more than 10 digits and 2 decimal places")
    private BigDecimal        msp;
    @Digits(integer = 10, fraction = 2, message = "max value cannot have more than 10 digits and 2 decimal places")
    private BigDecimal        mrp;
    @Digits(integer = 10, fraction = 2 ,message = "max value cannot have more than 10 digits and 2 decimal places")
    private BigDecimal        sellingPrice;
    
    @NotBlank
    private String currencyCode;
    
    public BigDecimal getSellingPrice() {
        return sellingPrice;
    }

    public void setSellingPrice(BigDecimal sellingPrice) {
        this.sellingPrice = sellingPrice;
    }

    public BigDecimal getMsp() {
        return msp;
    }

    public void setMsp(BigDecimal msp) {
        this.msp = msp;
    }

    public BigDecimal getMrp() {
        return mrp;
    }

    public void setMrp(BigDecimal mrp) {
        this.mrp = mrp;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }
}
