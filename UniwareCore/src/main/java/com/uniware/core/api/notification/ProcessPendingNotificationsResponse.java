/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 23, 2012
 *  @author singla
 */
package com.uniware.core.api.notification;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author Sunny
 */
public class ProcessPendingNotificationsResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -8572692049693720637L;

}
