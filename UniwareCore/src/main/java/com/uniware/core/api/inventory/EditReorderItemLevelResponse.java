/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 4, 2013
 *  @author praveeng
 */
package com.uniware.core.api.inventory;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author praveeng
 */
public class EditReorderItemLevelResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 1029063751462014552L;

}
