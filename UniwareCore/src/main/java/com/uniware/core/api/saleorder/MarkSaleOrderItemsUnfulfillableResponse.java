/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 06-May-2015
 *  @author akshay
 */
package com.uniware.core.api.saleorder;

import java.util.ArrayList;
import java.util.List;

import com.unifier.core.api.base.ServiceResponse;

public class MarkSaleOrderItemsUnfulfillableResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 3593943856474559108L;
    
    private List<String>      successfulSaleOrderItemCodes = new ArrayList<String>();

    /**
     * @return the successfulSaleOrderItemCodes
     */
    public List<String> getSuccessfulSaleOrderItemCodes() {
        return successfulSaleOrderItemCodes;
    }

    public void setSuccessfulSaleOrderItemCodes(List<String> successfulSaleOrderItemCodes) {
        this.successfulSaleOrderItemCodes = successfulSaleOrderItemCodes;
    }

}
