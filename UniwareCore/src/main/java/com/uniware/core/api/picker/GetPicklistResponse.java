/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jan 10, 2012
 *  @author singla
 */
package com.uniware.core.api.picker;

import com.unifier.core.api.base.ServiceResponse;

public class GetPicklistResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 6284414547113409023L;

    public GetPicklistResponse() {

    }

    public GetPicklistResponse(boolean successful, String message) {
        super(successful, message);
    }

    private PicklistDTO picklist;

    public void setPicklist(PicklistDTO picklist) {
        this.picklist = picklist;
    }

    public PicklistDTO getPicklist() {
        return picklist;
    }
}
