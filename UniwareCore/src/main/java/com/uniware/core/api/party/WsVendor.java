/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 02-Jun-2014
 *  @author unicom
 */
package com.uniware.core.api.party;

import java.util.List;

import javax.validation.Valid;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.customfields.WsCustomFieldValue;

/**
 * @author singla
 */
/**
 * @author unicom
 */
public class WsVendor {
    
    @NotBlank
    @Length(max = 45)
    private String                   code;
    @NotBlank
    @Length(max = 100)
    private String                   name;
    @Length(max = 45)
    private String                   pan;
    @Length(max = 45)
    private String                   tin;
    @Length(max = 45)
    private String                   cstNumber;
    @Length(max = 45)
    private String                   stNumber;
    @Length(max = 45)
    private String                   gstNumber;
    @Length(max = 256)
    private String                   website;
    private Integer                  purchaseExpiryPeriod;
    private boolean                  acceptsCForm;
    private boolean                  taxExempted;
    private Boolean                  enabled;
    private boolean                  registeredDealer;

    @Valid
    private List<WsCustomFieldValue> customFieldValues;

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the pan
     */
    public String getPan() {
        return pan;
    }

    /**
     * @param pan the pan to set
     */
    public void setPan(String pan) {
        this.pan = pan;
    }

    /**
     * @return the tin
     */
    public String getTin() {
        return tin;
    }

    /**
     * @param tin the tin to set
     */
    public void setTin(String tin) {
        this.tin = tin;
    }

    /**
     * @return the cstNumber
     */
    public String getCstNumber() {
        return cstNumber;
    }

    /**
     * @param cstNumber the cstNumber to set
     */
    public void setCstNumber(String cstNumber) {
        this.cstNumber = cstNumber;
    }

    /**
     * @return the stNumber
     */
    public String getStNumber() {
        return stNumber;
    }

    /**
     * @param stNumber the stNumber to set
     */
    public void setStNumber(String stNumber) {
        this.stNumber = stNumber;
    }

    /**
     * @return the website
     */
    public String getWebsite() {
        return website;
    }

    /**
     * @param website the website to set
     */
    public void setWebsite(String website) {
        this.website = website;
    }

    /**
     * @return the purchaseExpiryPeriod
     */
    public Integer getPurchaseExpiryPeriod() {
        return purchaseExpiryPeriod;
    }

    /**
     * @param purchaseExpiryPeriod the purchaseExpiryPeriod to set
     */
    public void setPurchaseExpiryPeriod(Integer purchaseExpiryPeriod) {
        this.purchaseExpiryPeriod = purchaseExpiryPeriod;
    }

    /**
     * @return the acceptsCForm
     */
    public boolean isAcceptsCForm() {
        return acceptsCForm;
    }

    /**
     * @param acceptsCForm the acceptsCForm to set
     */
    public void setAcceptsCForm(boolean acceptsCForm) {
        this.acceptsCForm = acceptsCForm;
    }

    

    /**
     * @return the customFieldValues
     */
    public List<WsCustomFieldValue> getCustomFieldValues() {
        return customFieldValues;
    }

    /**
     * @param customFieldValues the customFieldValues to set
     */
    public void setCustomFieldValues(List<WsCustomFieldValue> customFieldValues) {
        this.customFieldValues = customFieldValues;
    }

    /**
     * @return the taxExempted
     */
    public boolean isTaxExempted() {
        return taxExempted;
    }

    /**
     * @param taxExempted the taxExempted to set
     */
    public void setTaxExempted(boolean taxExempted) {
        this.taxExempted = taxExempted;
    }

    /**
     * @return the registeredDealer
     */
    public boolean isRegisteredDealer() {
        return registeredDealer;
    }

    /**
     * @param registeredDealer the registeredDealer to set
     */
    public void setRegisteredDealer(boolean registeredDealer) {
        this.registeredDealer = registeredDealer;
    }
    
    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public String getGstNumber() {
        return gstNumber;
    }

    public void setGstNumber(String gstNumber) {
        this.gstNumber = gstNumber;
    }
}

