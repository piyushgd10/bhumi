/*
 * Copyright 2016 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 1/9/16 3:51 PM
 * @author bhuvneshwarkumar
 */

package com.uniware.core.api.picker;

import com.unifier.core.api.base.ServiceResponse;

/**
 * Created by bhuvneshwarkumar on 01/09/16.
 */
public class SubmitPickBucketResponse extends ServiceResponse {

    private String picklistCode;

    public String getPicklistCode() {
        return picklistCode;
    }

    public void setPicklistCode(String picklistCode) {
        this.picklistCode = picklistCode;
    }
}
