/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jan 20, 2012
 *  @author singla
 */
package com.uniware.core.api.invoice;

import java.util.List;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author piyush
 */
public class CreateInvoiceBySaleOrderCodeRequest extends ServiceRequest {

    /**
     *
     */
    private static final long serialVersionUID = 9134119935682779672L;

    @NotBlank
    private String            saleOrderCode;

    @NotEmpty
    private List<String>      saleOrderItemCodes;

    private boolean           commitBlockedInventory;

    @NotNull
    private Integer           userId;

    private WsTaxInformation  taxInformation;

    /**
     * @return the saleOrderCode
     */
    public String getSaleOrderCode() {
        return saleOrderCode;
    }

    /**
     * @param saleOrderCode the shippingPackageCode to set
     */
    public void setSaleOrderCode(String saleOrderCode) {
        this.saleOrderCode = saleOrderCode;
    }

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public boolean isCommitBlockedInventory() {
        return commitBlockedInventory;
    }

    public void setCommitBlockedInventory(boolean commitBlockedInventory) {
        this.commitBlockedInventory = commitBlockedInventory;
    }

    public List<String> getSaleOrderItemCodes() {
        return saleOrderItemCodes;
    }

    public void setSaleOrderItemCodes(List<String> saleOrderItemCodes) {
        this.saleOrderItemCodes = saleOrderItemCodes;
    }

    public WsTaxInformation getTaxInformation() {
        return taxInformation;
    }

    public void setTaxInformation(WsTaxInformation taxInformation) {
        this.taxInformation = taxInformation;
    }
}
