/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 01-Jul-2012
 *  @author vibhu
 */
package com.uniware.core.api.material;

import com.unifier.core.api.base.ServiceResponse;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author vibhu
 */
public class SearchGatePassResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long       serialVersionUID = -7385731828055499434L;
    private Long                    totalRecords;
    private List<SearchGatePassDTO> elements         = new ArrayList<SearchGatePassDTO>();

    /**
     * @return the totalRecords
     */
    public Long getTotalRecords() {
        return totalRecords;
    }

    /**
     * @param totalRecords the totalRecords to set
     */
    public void setTotalRecords(Long totalRecords) {
        this.totalRecords = totalRecords;
    }

    /**
     * @return the elements
     */
    public List<SearchGatePassDTO> getElements() {
        return elements;
    }

    /**
     * @param elements the elements to set
     */
    public void setElements(List<SearchGatePassDTO> elements) {
        this.elements = elements;
    }

    public static class SearchGatePassDTO {

        private String username;
        private String code;
        private String type;
        private String statusCode;
        private Date   created;
        private String toParty;
        private String reference;

        /**
         * @return the username
         */
        public String getUsername() {
            return username;
        }

        /**
         * @param username the username to set
         */
        public void setUsername(String username) {
            this.username = username;
        }

        /**
         * @return the code
         */
        public String getCode() {
            return code;
        }

        /**
         * @param code the code to set
         */
        public void setCode(String code) {
            this.code = code;
        }

        /**
         * @return the type
         */
        public String getType() {
            return type;
        }

        /**
         * @param type the type to set
         */
        public void setType(String type) {
            this.type = type;
        }

        /**
         * @return the statusCode
         */
        public String getStatusCode() {
            return statusCode;
        }

        /**
         * @param statusCode the statusCode to set
         */
        public void setStatusCode(String statusCode) {
            this.statusCode = statusCode;
        }

        /**
         * @return the created
         */
        public Date getCreated() {
            return created;
        }

        /**
         * @param created the created to set
         */
        public void setCreated(Date created) {
            this.created = created;
        }

        /**
         * @return the toParty
         */
        public String getToParty() {
            return toParty;
        }

        /**
         * @param toParty the toParty to set
         */
        public void setToParty(String toParty) {
            this.toParty = toParty;
        }

        /**
         * @return the reference
         */
        public String getReference() {
            return reference;
        }

        /**
         * @param reference the reference to set
         */
        public void setReference(String reference) {
            this.reference = reference;
        }

    }
}