package com.uniware.core.api.shipping;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author ankurpratik on 26/04/17.
 */
public class AddSignatureToShippingManifestResponse extends ServiceResponse {

    private static final long serialVersionUID = 7491573315512365898L;
}
