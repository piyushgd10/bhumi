package com.uniware.core.api.saleorder;

import com.unifier.core.api.base.ServiceRequest;

/**
 * Created by akshayag on 9/16/15.
 */
public class GetShippingPackagesByStatusCodeRequest extends ServiceRequest {

    private String      statusCode;

    private PageRequest pageRequest;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public PageRequest getPageRequest() {
        return pageRequest;
    }

    public void setPageRequest(PageRequest pageRequest) {
        this.pageRequest = pageRequest;
    }
}
