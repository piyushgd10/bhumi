/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, May 20, 2015
 *  @author harsh
 */
package com.uniware.core.api.purchase;

import java.util.List;

import com.unifier.core.api.base.ServiceResponse;
import com.uniware.core.api.saleorder.display.GetSaleOrderActivitiesResponse.ActivityDTO;

/**
 * @author harsh
 */
public class GetPurchaseOrderActivitiesResponse extends ServiceResponse {

    private static final long serialVersionUID = -5051442671847490822L;

    private List<ActivityDTO> activities;

    public List<ActivityDTO> getActivities() {
        return activities;
    }

    public void setActivities(List<ActivityDTO> activities) {
        this.activities = activities;
    }

}
