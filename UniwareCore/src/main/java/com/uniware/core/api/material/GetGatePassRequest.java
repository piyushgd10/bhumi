/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 02-Jul-2012
 *  @author vibhu
 */
package com.uniware.core.api.material;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author vibhu
 */
public class GetGatePassRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -6614890937570223988L;

    private String            code;

    private boolean fetchInflowReceiptDetail = true;

    public boolean isFetchInflowReceiptDetail() {
        return fetchInflowReceiptDetail;
    }

    public void setFetchInflowReceiptDetail(boolean fetchInflowReceiptDetail) {
        this.fetchInflowReceiptDetail = fetchInflowReceiptDetail;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

}