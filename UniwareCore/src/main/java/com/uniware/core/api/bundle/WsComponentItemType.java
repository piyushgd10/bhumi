/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 26-Jul-2013
 *  @author sunny
 */
package com.uniware.core.api.bundle;

import java.math.BigDecimal;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

public class WsComponentItemType {

    @NotBlank
    private String     itemSku;

    @NotNull
    @Min(value = 1)
    private Integer    quantity;

    @NotNull
    @Min(value = 0)
    private BigDecimal price;

    public WsComponentItemType() {
        super();
    }

    public WsComponentItemType(String itemSku, Integer quantity, BigDecimal price) {
        super();
        this.itemSku = itemSku;
        this.quantity = quantity;
        this.price = price;
    }

    public String getItemSku() {
        return itemSku;
    }

    public void setItemSku(String itemSku) {
        this.itemSku = itemSku;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

}
