/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 09-Oct-2014
 *  @author parijat
 */
package com.uniware.core.api.channel;

import java.math.BigDecimal;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

/**
 * @author parijat
 */
public class WsChannelReconciliationInvoiceItem {

    @NotBlank
    private String     reconciliationIdentifier;

    private String     channelSaleOrderCode;
    private String     orderDate;
    private String     invoiceItemOrderStatus;

    private String     additionalInfo;
    private String     channelProductName;
    @NotNull
    private double     orderItemValue;
    @NotNull
    private double     settledValue;
    private boolean    isProcessed;
    private int        quantity;

    private BigDecimal totalRewards;
    private BigDecimal totalDiscounts;
    private BigDecimal totalOtherIncentive;
    @NotNull
    private BigDecimal totalSellerEarning;

    @NotNull
    private BigDecimal totalCommission;
    private BigDecimal totalFixedFee;
    private BigDecimal totalShippingCharge;
    @NotNull
    private BigDecimal totalShippingFee;
    private BigDecimal totalReverseShippingFee;
    private BigDecimal totalAdditionalFee;
    @NotNull
    private BigDecimal totalTax;
    @NotNull
    private double     totalChannelRecovery;
    private String     additionalFeeDescription;

    /**
     * @return the reconciliationIdentifier
     */
    public String getReconciliationIdentifier() {
        return reconciliationIdentifier;
    }

    /**
     * @param reconciliationIdentifier the reconciliationIdentifier to set
     */
    public void setReconciliationIdentifier(String reconciliationIdentifier) {
        this.reconciliationIdentifier = reconciliationIdentifier;
    }

    /**
     * @return the orderId
     */
    public String getChannelSaleOrderCode() {
        return channelSaleOrderCode;
    }

    /**
     * @param channelSaleOrderCode the orderId to set
     */
    public void setChannelSaleOrderCode(String channelSaleOrderCode) {
        this.channelSaleOrderCode = channelSaleOrderCode;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    public String getChannelProductName() {
        return channelProductName;
    }

    public void setChannelProductName(String channelProductName) {
        this.channelProductName = channelProductName;
    }

    /**
     * @return the orderDate
     */
    public String getOrderDate() {
        return orderDate;
    }

    /**
     * @param orderDate the orderDate to set
     */
    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    /**
     * @return the invoiceItemOrderStatus
     */
    public String getInvoiceItemOrderStatus() {
        return invoiceItemOrderStatus;
    }

    /**
     * @param invoiceItemOrderStatus the invoiceItemOrderStatus to set
     */
    public void setInvoiceItemOrderStatus(String invoiceItemOrderStatus) {
        this.invoiceItemOrderStatus = invoiceItemOrderStatus;
    }

    /**
     * @return the orderItemValue
     */
    public double getOrderItemValue() {
        return orderItemValue;
    }

    public void setOrderItemValue(double orderItemValue) {
        this.orderItemValue = orderItemValue;
    }

    public double getSettledValue() {
        return settledValue;
    }

    public void setSettledValue(double settledValue) {
        this.settledValue = settledValue;
    }

    public void setIsProcessed(boolean isProcessed) {
        this.isProcessed = isProcessed;
    }

    /**
     * @return the isProcessed
     */
    public boolean isProcessed() {
        return isProcessed;
    }

    /**
     * @param isProcessed the isProcessed to set
     */
    public void setProcessed(boolean isProcessed) {
        this.isProcessed = isProcessed;
    }

    /**
     * @return the quantity
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     * @param quantity the quantity to set
     */
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    /**
     * @return the totalRewards
     */
    public BigDecimal getTotalRewards() {
        return totalRewards;
    }

    /**
     * @param totalRewards the totalRewards to set
     */
    public void setTotalRewards(BigDecimal totalRewards) {
        this.totalRewards = totalRewards;
    }

    /**
     * @return the totalDiscounts
     */
    public BigDecimal getTotalDiscounts() {
        return totalDiscounts;
    }

    /**
     * @param totalDiscounts the totalDiscounts to set
     */
    public void setTotalDiscounts(BigDecimal totalDiscounts) {
        this.totalDiscounts = totalDiscounts;
    }

    /**
     * @return the totalOtherIncentive
     */
    public BigDecimal getTotalOtherIncentive() {
        return totalOtherIncentive;
    }

    /**
     * @param totalOtherIncentive the totalOtherIncentive to set
     */
    public void setTotalOtherIncentive(BigDecimal totalOtherIncentive) {
        this.totalOtherIncentive = totalOtherIncentive;
    }

    /**
     * @return the totalSellerEarning
     */
    public BigDecimal getTotalSellerEarning() {
        return totalSellerEarning;
    }

    /**
     * @param totalSellerEarning the totalSellerEarning to set
     */
    public void setTotalSellerEarning(BigDecimal totalSellerEarning) {
        this.totalSellerEarning = totalSellerEarning;
    }

    /**
     * @return the totalCommission
     */
    public BigDecimal getTotalCommission() {
        return totalCommission;
    }

    /**
     * @param totalCommission the totalCommission to set
     */
    public void setTotalCommission(BigDecimal totalCommission) {
        this.totalCommission = totalCommission;
    }

    /**
     * @return the totalFixedFee
     */
    public BigDecimal getTotalFixedFee() {
        return totalFixedFee;
    }

    /**
     * @param totalFixedFee the totalFixedFee to set
     */
    public void setTotalFixedFee(BigDecimal totalFixedFee) {
        this.totalFixedFee = totalFixedFee;
    }

    /**
     * @return the totalShippingCharge
     */
    public BigDecimal getTotalShippingCharge() {
        return totalShippingCharge;
    }

    /**
     * @param totalShippingCharge the totalShippingCharge to set
     */
    public void setTotalShippingCharge(BigDecimal totalShippingCharge) {
        this.totalShippingCharge = totalShippingCharge;
    }

    /**
     * @return the totalShippingFee
     */
    public BigDecimal getTotalShippingFee() {
        return totalShippingFee;
    }

    /**
     * @param totalShippingFee the totalShippingFee to set
     */
    public void setTotalShippingFee(BigDecimal totalShippingFee) {
        this.totalShippingFee = totalShippingFee;
    }

    /**
     * @return the totalReverseShippingFee
     */
    public BigDecimal getTotalReverseShippingFee() {
        return totalReverseShippingFee;
    }

    /**
     * @param totalReverseShippingFee the totalReverseShippingFee to set
     */
    public void setTotalReverseShippingFee(BigDecimal totalReverseShippingFee) {
        this.totalReverseShippingFee = totalReverseShippingFee;
    }

    /**
     * @return the totalAdditionalFee
     */
    public BigDecimal getTotalAdditionalFee() {
        return totalAdditionalFee;
    }

    /**
     * @param totalAdditionalFee the totalAdditionalFee to set
     */
    public void setTotalAdditionalFee(BigDecimal totalAdditionalFee) {
        this.totalAdditionalFee = totalAdditionalFee;
    }

    public BigDecimal getTotalTax() {
        return totalTax;
    }

    public void setTotalTax(BigDecimal totalTax) {
        this.totalTax = totalTax;
    }

    /**
     * @return the totalChannelRecovery
     */
    public double getTotalChannelRecovery() {
        return totalChannelRecovery;
    }

    public void setTotalChannelRecovery(double totalChannelRecovery) {
        this.totalChannelRecovery = totalChannelRecovery;
    }

    /**
     * @return the additionalFeeDescription
     */
    public String getAdditionalFeeDescription() {
        return additionalFeeDescription;
    }

    /**
     * @param additionalFeeDescription the additionalFeeDescription to set
     */
    public void setAdditionalFeeDescription(String additionalFeeDescription) {
        this.additionalFeeDescription = additionalFeeDescription;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */

    @Override
    public String toString() {
        return "WsChannelReconciliationInvoiceItem{" + "reconciliationIdentifier='" + reconciliationIdentifier + '\'' + ", orderId='" + channelSaleOrderCode + '\''
                + ", orderDate='" + orderDate + '\'' + ", invoiceItemOrderStatus='" + invoiceItemOrderStatus + '\'' + ", additionalInfo='" + additionalInfo + '\''
                + ", orderItemValue=" + orderItemValue + ", settledValue=" + settledValue + ", isProcessed=" + isProcessed + ", quantity=" + quantity + ", totalRewards="
                + totalRewards + ", totalDiscounts=" + totalDiscounts + ", totalOtherIncentive=" + totalOtherIncentive + ", totalSellerEarning=" + totalSellerEarning
                + ", totalCommission=" + totalCommission + ", totalFixedFee=" + totalFixedFee + ", totalShippingCharge=" + totalShippingCharge + ", totalShippingFee="
                + totalShippingFee + ", totalReverseShippingFee=" + totalReverseShippingFee + ", totalAdditionalFee=" + totalAdditionalFee + ", totalTax=" + totalTax
                + ", totalChannelRecovery=" + totalChannelRecovery + ", additionalFeeDescription='" + additionalFeeDescription + '\'' + '}';
    }
}
