/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Nov 25, 2015
 *  @author akshay
 */
package com.uniware.core.api.recommendation.dto;

import java.util.Date;

import com.uniware.core.vo.RecommendationSubscriptionVO;

public class RecommendationSubscriptionDTO {
    
    private String recommendationType;
    private String recommendationProvider;
    private Date   created;

    public RecommendationSubscriptionDTO() {
        super();
    }
    
    public RecommendationSubscriptionDTO(RecommendationSubscriptionVO rsVO) {
        super();
        this.recommendationType = rsVO.getRecommendationType();
        this.recommendationProvider = rsVO.getRecommendationProvider();
        this.created = rsVO.getCreated();
    }

    public String getRecommendationType() {
        return recommendationType;
    }

    public void setRecommendationType(String recommendationType) {
        this.recommendationType = recommendationType;
    }

    public String getRecommendationProvider() {
        return recommendationProvider;
    }

    public void setRecommendationProvider(String recommendationProvider) {
        this.recommendationProvider = recommendationProvider;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }


}
