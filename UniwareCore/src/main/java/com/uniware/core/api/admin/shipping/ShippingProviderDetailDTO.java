/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 *  @version     1.0, 22-Jan-2012
 *  @author vibhu
 */
package com.uniware.core.api.admin.shipping;

import java.util.ArrayList;
import java.util.List;

import com.uniware.core.entity.ShippingProvider;
import com.uniware.core.entity.ShippingProviderSource;

/**
 * @author vibhu
 */
public class ShippingProviderDetailDTO extends ShippingProviderDTO {

    /**
     * @return the methods
     */
    public List<ShippingProviderMethodDTO> getMethods() {
        return methods;
    }

    private final List<ShippingProviderMethodDTO> methods = new ArrayList<>();

    //    private Map<String, APIParamDTO>                     parameters = new HashMap<String, APIParamDTO>();

    /**
     * @param shippingProvider
     */
    public ShippingProviderDetailDTO(ShippingProvider shippingProvider, ShippingProviderSource providerSource) {
        super(shippingProvider, providerSource);
    }

    public void addShippingMethod(ShippingProviderMethodDTO method) {
        methods.add(method);
    }

    /* *//**
     * @param paramDTO
     */
    /*
    public void addAPIParamDTO(APIParamDTO paramDTO) {
     parameters.put(paramDTO.getName(), paramDTO);
    }

    *//**
     * @return the parameters
     */
    /*
    public Map<String, APIParamDTO> getParameters() {
     return parameters;
    }

    *//**
     * @param parameters the parameters to set
     */
    /*
    public void setParameters(Map<String, APIParamDTO> parameters) {
     this.parameters = parameters;
    }*/

}