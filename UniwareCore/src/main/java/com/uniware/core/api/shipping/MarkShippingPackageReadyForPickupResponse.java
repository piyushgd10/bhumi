/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Apr 18, 2012
 *  @author singla
 */
package com.uniware.core.api.shipping;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author singla
 */
public class MarkShippingPackageReadyForPickupResponse extends ServiceResponse {


}
