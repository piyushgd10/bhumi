/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jul 6, 2012
 *  @author singla
 */
package com.uniware.core.api.jabong;

import com.unifier.core.api.base.ServiceResponse;

public class JabongQualityRejectItemResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 7609858163132074676L;

}
