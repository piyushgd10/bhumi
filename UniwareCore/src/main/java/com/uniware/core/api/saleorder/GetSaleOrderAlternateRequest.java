package com.uniware.core.api.saleorder;

import com.unifier.core.api.base.ServiceRequest;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import java.util.List;

/**
 * Created by bhuvneshwarkumar on 25/08/15.
 */
public class GetSaleOrderAlternateRequest extends ServiceRequest {
    @NotEmpty
    private List<Integer> saleOrderItemAlternateList;

    public List<Integer> getSaleOrderItemAlternateList() {
        return saleOrderItemAlternateList;
    }

    public void setSaleOrderItemAlternateList(List<Integer> saleOrderItemAlternateList) {
        this.saleOrderItemAlternateList = saleOrderItemAlternateList;
    }
}
