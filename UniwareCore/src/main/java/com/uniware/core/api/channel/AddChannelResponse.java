/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jun 20, 2012
 *  @author singla
 */
package com.uniware.core.api.channel;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author Sunny
 */
public class AddChannelResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 8034049854364994904L;

    private ChannelDetailDTO  channelDetailDto;

    /**
     * @return the channelDetailDto
     */
    public ChannelDetailDTO getChannelDetailDto() {
        return channelDetailDto;
    }

    /**
     * @param channelDetailDto the channelDetailDto to set
     */
    public void setChannelDetailDto(ChannelDetailDTO channelDetailDto) {
        this.channelDetailDto = channelDetailDto;
    }
    

}
