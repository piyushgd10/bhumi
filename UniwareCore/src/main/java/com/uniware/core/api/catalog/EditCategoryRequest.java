/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 10, 2012
 *  @author praveeng
 */
package com.uniware.core.api.catalog;

import com.unifier.core.api.base.ServiceRequest;

import javax.validation.Valid;

/**
 * @author praveeng
 */
public class EditCategoryRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 1131986133069784739L;

    @Valid
    private WsCategory        category;

    /**
     * @return the category
     */
    public WsCategory getCategory() {
        return category;
    }

    /**
     * @param category the category to set
     */
    public void setCategory(WsCategory category) {
        this.category = category;
    }

}
