/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 15-May-2012
 *  @author vibhu
 */
package com.uniware.core.api.item;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author vibhu
 */
public class GetItemDetailRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 7697442599513738329L;

    private String            itemCode;

    /**
     * @return the itemCode
     */
    public String getItemCode() {
        return itemCode;
    }

    /**
     * @param itemCode the itemCode to set
     */
    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

}
