package com.uniware.core.api.metadata;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author ankurpratik on 05/06/17.
 */
public class AppVersionResponse extends ServiceResponse {

    private static final long serialVersionUID = -4204568277266534463L;

    private AppUpdateDTO      appUpdateDTO     = new AppUpdateDTO();

    public AppVersionResponse(int majorVersion, double minorVersion) {
        appUpdateDTO = new AppUpdateDTO(majorVersion, minorVersion);
        this.setSuccessful(true);
    }

    public AppUpdateDTO getAppUpdateDTO() {
        return appUpdateDTO;
    }

    public void setAppUpdateDTO(AppUpdateDTO appUpdateDTO) {
        this.appUpdateDTO = appUpdateDTO;
    }

    public static class AppUpdateDTO {
        private double minorVersion;
        private int    majorVersion;

        public AppUpdateDTO() {
        }

        public AppUpdateDTO(int majorVersion, double minorVersion) {
            this.majorVersion = majorVersion;
            this.minorVersion = minorVersion;
        }

        public double getMinorVersion() {
            return minorVersion;
        }

        public void setMinorVersion(double minorVersion) {
            this.minorVersion = minorVersion;
        }

        public int getMajorVersion() {
            return majorVersion;
        }

        public void setMajorVersion(int majorVersion) {
            this.majorVersion = majorVersion;
        }
    }
}
