package com.uniware.core.api.invoice;

import java.util.ArrayList;
import java.util.List;

import com.unifier.core.api.base.ServiceResponse;

/**
 * Created by Sagar Sahni on 12/06/17.
 */
public class GetAllInvoicesDetailsResponse extends ServiceResponse {

    private List<InvoiceDTO> invoices = new ArrayList<>();

    public List<InvoiceDTO> getInvoices() {
        return invoices;
    }

    public void setInvoices(List<InvoiceDTO> invoices) {
        this.invoices = invoices;
    }
}
