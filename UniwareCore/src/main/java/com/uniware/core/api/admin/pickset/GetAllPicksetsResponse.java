/**
 * Copyright 2017 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version 1.0, 26/09/17
 * @author aditya
 */
package com.uniware.core.api.admin.pickset;

import java.util.ArrayList;
import java.util.List;

import com.unifier.core.api.base.ServiceResponse;
import com.uniware.core.entity.PickSet;

public class GetAllPicksetsResponse extends ServiceResponse {

    private List<ZoneDTO> zones = new ArrayList<>();

    public List<ZoneDTO> getZones() {
        return zones;
    }

    public void setZones(List<ZoneDTO> zones) {
        this.zones = zones;
    }

    public ZoneDTO addZone(PickSet pickset) {
        ZoneDTO zone = new ZoneDTO(pickset.getId(), pickset.getName());
        this.zones.add(zone);
        return zone;
    }

    public class ZoneDTO {

        private Integer id;
        private String  name;

        public ZoneDTO(Integer id, String name) {
            this.id = id;
            this.name = name;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
