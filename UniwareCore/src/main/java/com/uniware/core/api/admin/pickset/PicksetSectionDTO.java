/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 23-Feb-2012
 *  @author vibhu
 */
package com.uniware.core.api.admin.pickset;

import com.uniware.core.entity.Section;

/**
 * @author vibhu
 */
public class PicksetSectionDTO {

    private int sectionId;
    private int picksetId;

    public PicksetSectionDTO() {

    }

    /**
     * @param section
     */
    public PicksetSectionDTO(Section section) {
        this.sectionId = section.getId();
        this.picksetId = section.getPickSet().getId();
    }

    /**
     * @return the sectionId
     */
    public int getSectionId() {
        return sectionId;
    }

    /**
     * @param sectionId the sectionId to set
     */
    public void setSectionId(int sectionId) {
        this.sectionId = sectionId;
    }

    /**
     * @return the picksetId
     */
    public int getPicksetId() {
        return picksetId;
    }

    /**
     * @param picksetId the picksetId to set
     */
    public void setPicksetId(int picksetId) {
        this.picksetId = picksetId;
    }

}
