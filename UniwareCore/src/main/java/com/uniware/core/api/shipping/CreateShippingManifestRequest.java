/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 7, 2012
 *  @author singla
 */
package com.uniware.core.api.shipping;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;
import com.unifier.core.api.customfields.WsCustomFieldValue;

/**
 * @author singla
 */
public class CreateShippingManifestRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long        serialVersionUID   = 5090362284101726468L;

    @NotBlank
    private String                   channel;

    private String                   shippingProviderCode;

    private String                   shippingProviderName;

    private String                   shippingMethodCode;

    @NotNull
    private Integer                  userId;

    private String                   comments;

    @NotNull
    private Boolean                  thirdPartyShipping = false;

    @Valid
    private List<WsCustomFieldValue> customFieldValues;

    public String getShippingProviderCode() {
        return shippingProviderCode;
    }

    public void setShippingProviderCode(String shippingProviderCode) {
        this.shippingProviderCode = shippingProviderCode;
    }

    public String getShippingProviderName() {
        return shippingProviderName;
    }

    public void setShippingProviderName(String shippingProviderName) {
        this.shippingProviderName = shippingProviderName;
    }

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return the comments
     */
    public String getComments() {
        return comments;
    }

    /**
     * @param comments the comments to set
     */
    public void setComments(String comments) {
        this.comments = comments;
    }

    /**
     * @return the shippingMethodId
     */
    public String getShippingMethodCode() {
        return shippingMethodCode;
    }

    /**
     * @param shippingMethodId the shippingMethodId to set
     */
    public void setShippingMethodCode(String shippingMethodCode) {
        this.shippingMethodCode = shippingMethodCode;
    }

    /**
     * @return the customFieldValues
     */
    public List<WsCustomFieldValue> getCustomFieldValues() {
        return customFieldValues;
    }

    /**
     * @param customFieldValues the customFieldValues to set
     */
    public void setCustomFieldValues(List<WsCustomFieldValue> customFieldValues) {
        this.customFieldValues = customFieldValues;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public Boolean getThirdPartyShipping() {
        return thirdPartyShipping;
    }

    public void setThirdPartyShipping(Boolean thirdPartyShipping) {
        this.thirdPartyShipping = thirdPartyShipping;
    }

}
