/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 03-Dec-2013
 *  @author akshay
 */
package com.uniware.core.api.inflow;

import com.unifier.core.api.base.ServiceRequest;
import com.unifier.core.utils.DateUtils.DateRange;

public class GetInflowReceiptsRequest extends ServiceRequest{

    /**
     * 
     */
    private static final long serialVersionUID = -2250651437201121016L;
    private String            purchaseOrderCode;
    private DateRange         createdBetween;

    public String getPurchaseOrderCode() {
        return purchaseOrderCode;
    }

    public void setPurchaseOrderCode(String purchaseOrderCode) {
        this.purchaseOrderCode = purchaseOrderCode;
    }

    public DateRange getCreatedBetween() {
        return createdBetween;
    }

    public void setCreatedBetween(DateRange createdBetween) {
        this.createdBetween = createdBetween;
    }

}
