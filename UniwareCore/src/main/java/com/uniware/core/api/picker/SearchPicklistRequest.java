/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Apr 4, 2012
 *  @author praveeng
 */
package com.uniware.core.api.picker;

import java.util.Date;
import java.util.List;

import com.unifier.core.api.base.ServiceRequest;
import com.unifier.core.pagination.SearchOptions;

/**
 * @author praveeng
 */
public class SearchPicklistRequest extends ServiceRequest {
    /**
     * 
     */
    private static final long serialVersionUID = 3702070336597542452L;
    private List<String>      statusCodes;
    private Date              fromDate;
    private Date              toDate;
    private SearchOptions     searchOptions;

    /**
     * @return the fromDate
     */
    public Date getFromDate() {
        return fromDate;
    }

    /**
     * @param fromDate the fromDate to set
     */
    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    /**
     * @return the toDate
     */
    public Date getToDate() {
        return toDate;
    }

    /**
     * @param toDate the toDate to set
     */
    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    /**
     * @return the searchOptions
     */
    public SearchOptions getSearchOptions() {
        return searchOptions;
    }

    /**
     * @param searchOptions the searchOptions to set
     */
    public void setSearchOptions(SearchOptions searchOptions) {
        this.searchOptions = searchOptions;
    }

    /**
     * @return the statusCodes
     */
    public List<String> getStatusCodes() {
        return statusCodes;
    }

    /**
     * @param statusCodes the statusCodes to set
     */
    public void setStatusCodes(List<String> statusCodes) {
        this.statusCodes = statusCodes;
    }

}
