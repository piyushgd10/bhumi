/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 27-Jan-2015
 *  @author akshaykochhar
 */
package com.uniware.core.api.tenant;

import com.unifier.core.api.base.ServiceResponse;

public class RefreshTenantSetupStateResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 6734820228159956606L;

    private String            setupStatus;

    public String getSetupStatus() {
        return setupStatus;
    }

    public void setSetupStatus(String setupStatus) {
        this.setupStatus = setupStatus;
    }

}
