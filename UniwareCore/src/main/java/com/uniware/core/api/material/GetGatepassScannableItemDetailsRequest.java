/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 08-Apr-2015
 *  @author akshaykochhar
 */
package com.uniware.core.api.material;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

public class GetGatepassScannableItemDetailsRequest extends ServiceRequest{

    /**
     * 
     */
    private static final long serialVersionUID = -4456224816897582977L;
    
    @NotBlank
    private String gatePassCode;
    
    @NotBlank
    private String itemCode;
    
    public String getGatePassCode() {
        return gatePassCode;
    }

    public void setGatePassCode(String gatePassCode) {
        this.gatePassCode = gatePassCode;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }
    
}
