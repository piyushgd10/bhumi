/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 *  @version     1.0, Jun 20, 2012
 *  @author ankit
 */
package com.uniware.core.api.tax;

import java.math.BigDecimal;
import java.util.List;

import javax.validation.Valid;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author akshay
 */
public class EditTaxTypesConfigurationRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 1053545764943197444L;

    @NotBlank
    private String            taxTypeCode;

    private List<String>                 stateCodes;

    private BigDecimal                   vat;

    private BigDecimal                   cst;

    private BigDecimal                   cstWithCForm;

    private BigDecimal                   additionalTax;

    @Valid
    private List<WsTaxTypeConfiguration> taxTypeConfigurations;

    /**
     * @return the taxTypeCode
     */
    public String getTaxTypeCode() {
        return taxTypeCode;
    }

    /**
     * @param taxTypeCode the taxTypeCode to set
     */
    public void setTaxTypeCode(String taxTypeCode) {
        this.taxTypeCode = taxTypeCode;
    }

    public List<WsTaxTypeConfiguration> getTaxTypeConfigurations() {
        return taxTypeConfigurations;
    }

    public void setTaxTypeConfigurations(List<WsTaxTypeConfiguration> taxTypeConfigurations) {
        this.taxTypeConfigurations = taxTypeConfigurations;
    }

    public List<String> getStateCodes() {
        return stateCodes;
    }

    public void setStateCodes(List<String> stateCodes) {
        this.stateCodes = stateCodes;
    }

    public BigDecimal getVat() {
        return vat;
    }

    public void setVat(BigDecimal vat) {
        this.vat = vat;
    }

    public BigDecimal getCst() {
        return cst;
    }

    public void setCst(BigDecimal cst) {
        this.cst = cst;
    }

    public BigDecimal getCstWithCForm() {
        return cstWithCForm;
    }

    public void setCstWithCForm(BigDecimal cstWithCForm) {
        this.cstWithCForm = cstWithCForm;
    }

    public BigDecimal getAdditionalTax() {
        return additionalTax;
    }

    public void setAdditionalTax(BigDecimal additionalTax) {
        this.additionalTax = additionalTax;
    }

}
