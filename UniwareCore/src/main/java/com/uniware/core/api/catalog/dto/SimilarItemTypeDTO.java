/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 9, 2012
 *  @author praveeng
 */
package com.uniware.core.api.catalog.dto;

import java.math.BigDecimal;
import java.util.List;

import com.uniware.core.entity.ItemType;

/**
 * @author Sunny
 */
public class SimilarItemTypeDTO {
    private Integer      id;
    private String       skuCode;
    private String       name;
    private String       description;
    private BigDecimal   price;
    private String       color;
    private String       size;
    private String       brand;
    private String       productPageUrl;
    private String       imageUrl;
    private List<String> tags;

    public SimilarItemTypeDTO(String skuCode, String name, String imageUrl) {
        this.skuCode = skuCode;
        this.name = name;
        this.imageUrl = imageUrl;
    }

    public SimilarItemTypeDTO(ItemType itemType) {
        this.id = itemType.getId();
        this.skuCode = itemType.getSkuCode();
        this.name = itemType.getName();
        this.description = itemType.getDescription();
        this.price = itemType.getMaxRetailPrice();
        this.tags = itemType.getTagList();
        this.color = itemType.getColor();
        this.size = itemType.getSize();
        this.brand = itemType.getBrand();
        this.productPageUrl = itemType.getProductPageUrl();
        this.imageUrl = itemType.getImageUrl();
        this.tags = itemType.getTagList();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSkuCode() {
        return skuCode;
    }

    public void setSkuCode(String skuCode) {
        this.skuCode = skuCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getProductPageUrl() {
        return productPageUrl;
    }

    public void setProductPageUrl(String productPageUrl) {
        this.productPageUrl = productPageUrl;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    @Override
    public String toString() {
        return "SimilarItemTypeDTO [skuCode=" + skuCode + ", name=" + name + ", color=" + color + ", size=" + size + ", brand=" + brand + ", imageUrl=" + imageUrl + "]";
    }

}
