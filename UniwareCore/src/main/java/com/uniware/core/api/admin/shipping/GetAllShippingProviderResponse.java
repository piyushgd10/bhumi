/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 13-Feb-2015
 *  @author parijat
 */
package com.uniware.core.api.admin.shipping;

import java.util.List;

import com.unifier.core.api.base.ServiceResponse;

public class GetAllShippingProviderResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -7778751297974827189L;

    private List<ShippingProviderDetailDTO> shippingProviders;

    /**
     * @return the shippingProviders
     */
    public List<ShippingProviderDetailDTO> getShippingProviders() {
        return shippingProviders;
    }

    /**
     * @param shippingProviders the shippingProviders to set
     */
    public void setShippingProviders(List<ShippingProviderDetailDTO> shippingProviders) {
        this.shippingProviders = shippingProviders;
    }

}
