/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Oct 1, 2012
 *  @author singla
 */
package com.uniware.core.api.saleorder;

import com.unifier.core.api.base.ServiceRequest;

import java.util.List;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

public class SwitchSaleOrderItemFacilityRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -8298355690654375421L;

    @NotEmpty
    private List<String>      saleOrderItemCodes;

    @NotBlank
    private String            facilityCode;

    @NotBlank
    private String            saleOrderCode;

    /**
     * @return the facilityCode
     */
    public String getFacilityCode() {
        return facilityCode;
    }

    /**
     * @param facilityCode the facilityCode to set
     */
    public void setFacilityCode(String facilityCode) {
        this.facilityCode = facilityCode;
    }

    public List<String> getSaleOrderItemCodes() {
        return saleOrderItemCodes;
    }

    public void setSaleOrderItemCodes(List<String> saleOrderItemCodes) {
        this.saleOrderItemCodes = saleOrderItemCodes;
    }

    public String getSaleOrderCode() {
        return saleOrderCode;
    }

    public void setSaleOrderCode(String saleOrderCode) {
        this.saleOrderCode = saleOrderCode;
    }

}
