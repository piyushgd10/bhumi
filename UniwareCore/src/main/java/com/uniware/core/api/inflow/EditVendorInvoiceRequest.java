/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 11-Jun-2014
 *  @author akshay
 */
package com.uniware.core.api.inflow;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;
import com.unifier.core.api.customfields.WsCustomFieldValue;

public class EditVendorInvoiceRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    @NotBlank
    private String vendorCode;
    
    @NotBlank
    private String vendorInvoiceCode;
    
    @NotBlank
    private String vendorInvoiceNumber;
    
    @NotNull
    private Date   vendorInvoiceDate;
    
    @Valid
    private List<WsCustomFieldValue> customFieldValues;
    
    public List<WsCustomFieldValue> getCustomFieldValues() {
        return customFieldValues;
    }

    public void setCustomFieldValues(List<WsCustomFieldValue> customFieldValues) {
        this.customFieldValues = customFieldValues;
    }
    
    public String getVendorCode() {
        return vendorCode;
    }

    public void setVendorCode(String vendorCode) {
        this.vendorCode = vendorCode;
    }

    public String getVendorInvoiceNumber() {
        return vendorInvoiceNumber;
    }

    public void setVendorInvoiceNumber(String vendorInvoiceNumber) {
        this.vendorInvoiceNumber = vendorInvoiceNumber;
    }
    
    public Date getVendorInvoiceDate() {
        return vendorInvoiceDate;
    }

    public void setVendorInvoiceDate(Date vendorInvoiceDate) {
        this.vendorInvoiceDate = vendorInvoiceDate;
    }
    
    public String getVendorInvoiceCode() {
        return vendorInvoiceCode;
    }

    public void setVendorInvoiceCode(String vendorInvoiceCode) {
        this.vendorInvoiceCode = vendorInvoiceCode;
    }
    
}
