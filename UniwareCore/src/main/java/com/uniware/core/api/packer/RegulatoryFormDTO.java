/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 28-Jun-2013
 *  @author sunny
 */
package com.uniware.core.api.packer;

import com.unifier.core.expressions.Expression;

import java.io.Serializable;

public class RegulatoryFormDTO {

    private String     countryCode;
    private String     state;
    private String     code;
    private String     name;
    private String     goodsMovementType;
    private Expression conditionExpression;
    private boolean    enabled;

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGoodsMovementType() {
        return goodsMovementType;
    }

    public void setGoodsMovementType(String goodsMovementType) {
        this.goodsMovementType = goodsMovementType;
    }

    public Expression getConditionExpression() {
        return conditionExpression;
    }

    public void setConditionExpression(Expression conditionExpression) {
        this.conditionExpression = conditionExpression;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}
