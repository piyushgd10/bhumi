package com.uniware.core.api.tenant;

import com.unifier.core.annotation.constraints.Password;
import com.unifier.core.api.base.ServiceRequest;
import com.uniware.core.api.party.WsFacility;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created by bhuvneshwarkumar on 01/10/15.
 */

@Document(collection = "tenantSetupLog")
public class TenantSetupLog {

    private CreateTenantRequest  request;
    private CreateTenantResponse response;

    public CreateTenantRequest getRequest() {
        return request;
    }

    public void setRequest(CreateTenantRequest request) {
        this.request = request;
    }

    public CreateTenantResponse getResponse() {
        return response;
    }

    public void setResponse(CreateTenantResponse response) {
        this.response = response;
    }

}
