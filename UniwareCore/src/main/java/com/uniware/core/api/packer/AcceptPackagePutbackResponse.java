package com.uniware.core.api.packer;

import com.unifier.core.api.base.ServiceResponse;

/**
 * Created by harshpal on 17/11/16.
 */
public class AcceptPackagePutbackResponse extends ServiceResponse {
    private static final long serialVersionUID = -5281880148320697204L;
}
