/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 03-Mar-2012
 *  @author vibhu
 */
package com.uniware.core.api.admin.shipping;


/**
 * @author vibhu
 */
public class ShippingProviderMethodDTO {

    private int     id;
    private String  name;
    private String  code;
    private boolean COD;
    private boolean enabled;
    private String  trackingNumberGeneration;
    private int     availableAWBs;

    /**
     * @return the trackingNumberGeneration
     */
    public String getTrackingNumberGeneration() {
        return trackingNumberGeneration;
    }

    /**
     * @param trackingNumberGeneration the trackingNumberGeneration to set
     */
    public void setTrackingNumberGeneration(String trackingNumberGeneration) {
        this.trackingNumberGeneration = trackingNumberGeneration;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the cOD
     */
    public boolean isCOD() {
        return COD;
    }

    /**
     * @param cOD the cOD to set
     */
    public void setCOD(boolean cOD) {
        COD = cOD;
    }

    /**
     * @return the enabled
     */
    public boolean isEnabled() {
        return enabled;
    }

    /**
     * @param enabled the enabled to set
     */
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    /**
     * @return the availableAWBs
     */
    public int getAvailableAWBs() {
        return availableAWBs;
    }

    /**
     * @param availableAWBs the availableAWBs to set
     */
    public void setAvailableAWBs(int availableAWBs) {
        this.availableAWBs = availableAWBs;
    }

}