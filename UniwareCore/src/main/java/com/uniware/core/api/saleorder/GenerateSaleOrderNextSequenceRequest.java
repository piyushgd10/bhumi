/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 13-Aug-2014
 *  @author unicom
 */
package com.uniware.core.api.saleorder;

import com.unifier.core.api.base.ServiceRequest;

public class GenerateSaleOrderNextSequenceRequest extends ServiceRequest{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
}
