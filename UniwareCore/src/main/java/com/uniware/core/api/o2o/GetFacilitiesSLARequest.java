/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 *  @version     1.0, Aug 20, 2015
 *  @author akshay
 */
package com.uniware.core.api.o2o;

import java.util.List;

import javax.validation.Valid;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import com.unifier.core.api.base.ServiceRequest;

public class GetFacilitiesSLARequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @Valid
    @NotEmpty
    private List<WsProduct>   products;

    public List<WsProduct> getProducts() {
        return products;
    }

    public void setProducts(List<WsProduct> products) {
        this.products = products;
    }

    public static class WsProduct {
        @NotBlank
        private String       sellerSkuCode;

        @NotEmpty
        private List<String> facilityCodes;

        public String getSellerSkuCode() {
            return sellerSkuCode;
        }

        public void setSellerSkuCode(String sellerSkuCode) {
            this.sellerSkuCode = sellerSkuCode;
        }

        public List<String> getFacilityCodes() {
            return facilityCodes;
        }

        public void setFacilityCodes(List<String> facilityCodes) {
            this.facilityCodes = facilityCodes;
        }

    }

}
