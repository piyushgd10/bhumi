/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 25-Jun-2014
 *  @author akshay
 */
package com.uniware.core.api.tax;

import java.util.ArrayList;
import java.util.List;

import com.unifier.core.api.base.ServiceResponse;
import com.uniware.core.api.catalog.TaxTypeConfigurationDTO;

public class GetTaxTypeConfigurationResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    List<TaxTypeConfigurationDTO> taxTypeConfigurations = new ArrayList<>();
    
    public List<TaxTypeConfigurationDTO> getTaxTypeConfigurations() {
        return taxTypeConfigurations;
    }

    public void setTaxTypeConfigurations(List<TaxTypeConfigurationDTO> taxTypeConfigurations) {
        this.taxTypeConfigurations = taxTypeConfigurations;
    }

}
