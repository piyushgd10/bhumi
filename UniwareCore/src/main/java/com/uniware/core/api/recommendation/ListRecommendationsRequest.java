/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Nov 25, 2015
 *  @author akshay
 */
package com.uniware.core.api.recommendation;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;
import com.unifier.core.utils.DateUtils.DateRange;

public class ListRecommendationsRequest extends ServiceRequest {

    private static final long serialVersionUID = -6850263486764230067L;

    @NotBlank
    private String            recommendationStatus;

    private DateRange         createdBetween;

    private int               start            = 0;

    private int               noOfResults      = 10;

    public String getRecommendationStatus() {
        return recommendationStatus;
    }

    public void setRecommendationStatus(String recommendationStatus) {
        this.recommendationStatus = recommendationStatus;
    }

    public DateRange getCreatedBetween() {
        return createdBetween;
    }

    public void setCreatedBetween(DateRange createdBetween) {
        this.createdBetween = createdBetween;
    }

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getNoOfResults() {
        return noOfResults;
    }

    public void setNoOfResults(int noOfResults) {
        this.noOfResults = noOfResults;
    }

}
