/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, May 3, 2012
 *  @author singla
 */
package com.uniware.core.api.inflow;

import com.unifier.core.api.base.ServiceRequest;

import javax.validation.constraints.NotNull;

/**
 * @author singla
 */
public class GetRejectionReportRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -2166145417873901207L;

    @NotNull
    private Integer           inflowReceiptId;

    /**
     * @return the inflowReceiptId
     */
    public Integer getInflowReceiptId() {
        return inflowReceiptId;
    }

    /**
     * @param inflowReceiptId the inflowReceiptId to set
     */
    public void setInflowReceiptId(Integer inflowReceiptId) {
        this.inflowReceiptId = inflowReceiptId;
    }
}
