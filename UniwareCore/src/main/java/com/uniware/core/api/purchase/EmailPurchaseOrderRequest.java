/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Oct 6, 2012
 *  @author Pankaj
 */
package com.uniware.core.api.purchase;

import javax.validation.constraints.NotNull;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author Pankaj
 */
public class EmailPurchaseOrderRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -2470361070657292547L;

    @NotNull
    private String            purchaseOrderCode;

    /**
     * @return the purchaseOrderId
     */
    public String getPurchaseOrderCode() {
        return purchaseOrderCode;
    }

    /**
     * @param purchaseOrderId the purchaseOrderId to set
     */
    public void setPurchaseOrderCode(String purchaseOrderCode) {
        this.purchaseOrderCode = purchaseOrderCode;
    }

}
