/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 21, 2012
 *  @author praveeng
 */
package com.uniware.core.api.party;

import java.util.Date;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import com.uniware.core.entity.VendorAgreement;

/**
 * @author praveeng
 */
public class WsVendorAgreement {

    @NotBlank
    private String vendorCode;
    private String vendorAgreementStatus = VendorAgreement.StatusCode.ACTIVE.name();
    @NotEmpty
    private String name;
    @NotNull
    private Date   startTime;
    @NotNull
    private Date   endTime;
    @NotEmpty
    private String agreementText;

    /**
     * @return the vendorCode
     */
    public String getVendorCode() {
        return vendorCode;
    }

    /**
     * @param vendorCode the vendorCode to set
     */
    public void setVendorCode(String vendorCode) {
        this.vendorCode = vendorCode;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the vendorAgreementStatus
     */
    public String getVendorAgreementStatus() {
        return vendorAgreementStatus;
    }

    /**
     * @param vendorAgreementStatus the vendorAgreementStatus to set
     */
    public void setVendorAgreementStatus(String vendorAgreementStatus) {
        this.vendorAgreementStatus = vendorAgreementStatus;
    }

    /**
     * @return the agreementText
     */
    public String getAgreementText() {
        return agreementText;
    }

    /**
     * @param agreementText the agreementText to set
     */
    public void setAgreementText(String agreementText) {
        this.agreementText = agreementText;
    }

    /**
     * @return the startTime
     */
    public Date getStartTime() {
        return startTime;
    }

    /**
     * @return the endTime
     */
    public Date getEndTime() {
        return endTime;
    }

    /**
     * @param startTime the startTime to set
     */
    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    /**
     * @param endTime the endTime to set
     */
    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

}
