/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 12-Jul-2012
 *  @author praveeng
 */
package com.uniware.core.api.inflow;

import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author praveeng
 */
public class AddItemToInflowReceiptWapRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -3223732250895403778L;
    @NotNull
    private String            inflowReceiptCode;

    @NotBlank
    @Pattern(regexp = "^[a-zA-Z0-9-_]+$", message = "Item Code can only contain alphanumeric, _ and -")
    private String            itemCode;

    @NotBlank
    private String            itemSKU;

    @Past
    private Date              manufacturingDate;

    /**
     * @return the itemCode
     */
    public String getItemCode() {
        return itemCode;
    }

    /**
     * @param itemCode the itemCode to set
     */
    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    /**
     * @return the itemSKU
     */
    public String getItemSKU() {
        return itemSKU;
    }

    /**
     * @param itemSKU the itemSKU to set
     */
    public void setItemSKU(String itemSKU) {
        this.itemSKU = itemSKU;
    }

    /**
     * @return the inflowReceiptCode
     */
    public String getInflowReceiptCode() {
        return inflowReceiptCode;
    }

    /**
     * @param inflowReceiptCode the inflowReceiptCode to set
     */
    public void setInflowReceiptCode(String inflowReceiptCode) {
        this.inflowReceiptCode = inflowReceiptCode;
    }

    public Date getManufacturingDate() {
        return manufacturingDate;
    }

    public void setManufacturingDate(Date manufacturingDate) {
        this.manufacturingDate = manufacturingDate;
    }
}
