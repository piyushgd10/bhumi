/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Sep 18, 2013
 *  @author sunny
 */
package com.uniware.core.api.search;

public enum SearchType {
    SALE_ORDER("Sale Order", "/order/orderitems?orderCode=#{#saleOrder.code}", "Enter Sale/Display Order Code", true),
    FAILED_SALE_ORDER("Sale Order", "/orders?orderCode=#{#saleOrder.code}#viewName=Failed+Orders&filters[0][id]=orderCodeFilter&filters[0][text]=#{#saleOrder.code}&filters[1][id]=statusFilter&filters[1][selectedValues][]=FAILED", "Enter Sale/Display Order Code", true),
    SHIPPING_PACKAGE("Shipping Package", "/order/shipments?orderCode=#{#shippingPackage.saleOrder.code}&shipmentCode=#{#shippingPackage.code}", "Enter Shipping Package Code",false),
    PURCHASE_ORDER("Purchase Order", "/procure/poItems?legacy=1&poCode=#{#purchaseOrder.code}", "Enter Purchase Order Code", false),
    ITEM_TYPE("Item Type", "/products/edit?sku=#{#itemType.skuCode}", "Enter SKU Code", true),
    MANIFEST("Shipping Manifest", "/manifests/edit?code=#{#shippingManifest.manifestCode}", "Enter Shipping Manifest Number", true),
    INVOICE("Invoice", "/order/invoices?orderCode=#{#shippingPackage.saleOrder.code}&invoiceCode=#{#invoice.code}", "Enter Invoice Code", false),
    VENDOR("Vendor", "/vendors/edit?code=#{#vendor.code}", "Enter Vendor Name", true);

    private String  name;
    private String  landingPageUrl;
    private String  placeholderText;
    private boolean allFacilityType;

    private SearchType(String name, String landingPageUrl, String placeholderText, boolean allFacilityType) {
        this.name = name;
        this.landingPageUrl = landingPageUrl;
        this.placeholderText = placeholderText;
        this.allFacilityType = allFacilityType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLandingPageUrl() {
        return landingPageUrl;
    }

    public void setLandingPageUrl(String landingPageUrl) {
        this.landingPageUrl = landingPageUrl;
    }

    public String getPlaceholderText() {
        return placeholderText;
    }

    public void setPlaceholderText(String placeholderText) {
        this.placeholderText = placeholderText;
    }

    public boolean isAllFacilityType() {
        return allFacilityType;
    }

    public void setAllFacilityType(boolean allFacilityType) {
        this.allFacilityType = allFacilityType;
    }

}
