package com.uniware.core.api.tax;

import com.unifier.core.api.base.ServiceRequest;
import com.uniware.core.api.model.WsInvoice;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import java.util.Map;

/**
 * Created by Sagar Sahni on 29/05/17.
 */
public class GetTaxPercentagesRequest extends ServiceRequest {

    private Map<String, WsTaxClass> taxClassMap;

    @NotNull
    private WsInvoice.Source        source;

    @NotBlank
    private String                  sourceState;

    @NotBlank
    private String                  sourceCountry;

    @NotBlank
    private String                  destinationState;

    @NotBlank
    private String                  destinationCountry;

    private Boolean                 providesCForm;

    public Map<String, WsTaxClass> getTaxClassMap() {
        return taxClassMap;
    }

    public void setTaxClassMap(Map<String, WsTaxClass> taxClassMap) {
        this.taxClassMap = taxClassMap;
    }

    public WsInvoice.Source getSource() {
        return source;
    }

    public void setSource(WsInvoice.Source source) {
        this.source = source;
    }

    public String getSourceState() {
        return sourceState;
    }

    public void setSourceState(String sourceState) {
        this.sourceState = sourceState;
    }

    public String getDestinationState() {
        return destinationState;
    }

    public void setDestinationState(String destinationState) {
        this.destinationState = destinationState;
    }

    public Boolean getProvidesCForm() {
        return providesCForm;
    }

    public void setProvidesCForm(Boolean providesCForm) {
        this.providesCForm = providesCForm;
    }

    public String getSourceCountry() {
        return sourceCountry;
    }

    public void setSourceCountry(String sourceCountry) {
        this.sourceCountry = sourceCountry;
    }

    public String getDestinationCountry() {
        return destinationCountry;
    }

    public void setDestinationCountry(String destinationCountry) {
        this.destinationCountry = destinationCountry;
    }

    @Override
    public String toString() {
        return "GetTaxPercentagesRequest{" +
                "taxClassMap=" + taxClassMap +
                ", source=" + source +
                ", sourceState='" + sourceState + '\'' +
                ", sourceCountry='" + sourceCountry + '\'' +
                ", destinationState='" + destinationState + '\'' +
                ", destinationCountry='" + destinationCountry + '\'' +
                ", providesCForm=" + providesCForm +
                '}';
    }
}
