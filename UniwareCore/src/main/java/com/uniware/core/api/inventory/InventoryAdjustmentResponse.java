/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 27, 2012
 *  @author singla
 */
package com.uniware.core.api.inventory;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author singla
 */
public class InventoryAdjustmentResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 8384662598963896982L;

}
