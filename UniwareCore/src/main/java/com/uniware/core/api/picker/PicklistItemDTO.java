/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jan 10, 2012
 *  @author singla
 */
package com.uniware.core.api.picker;

import java.math.BigDecimal;
import java.util.Map;

/**
 * @author singla
 */
public class PicklistItemDTO {

    private int                 saleOrderItemId;
    private int                 slot;
    private String              saleOrderItemCode;
    private String              itemSku;
    private String              itemName;
    private String              brand;
    private String              color;
    private String              size;
    private BigDecimal          maxRetailPrice;
    private String              shelfCode;
    private boolean             splittable;
    private String              saleOrderCode;
    private String              displayOrderCode;
    private String              statusCode;
    private int                 numberOfItems;
    private String              channelName;
    private String              shippingPackageStatusCode;
    private String              itemCode;
    private String              shippingPackageCode;
    private Map<String, Object> itemTypeCustomFields;

    /**
     * @return the itemTypeCustomFields
     */
    public Map<String, Object> getItemTypeCustomFields() {
        return itemTypeCustomFields;
    }

    /**
     * @param itemTypeCustomFields the itemTypeCustomFields to set
     */
    public void setItemTypeCustomFields(Map<String, Object> itemTypeCustomFields) {
        this.itemTypeCustomFields = itemTypeCustomFields;
    }

    /**
     * @return the shippingPackageStatusCode
     */
    public String getShippingPackageStatusCode() {
        return shippingPackageStatusCode;
    }

    /**
     * @param shippingPackageStatusCode the shippingPackageStatusCode to set
     */
    public void setShippingPackageStatusCode(String shippingPackageStatusCode) {
        this.shippingPackageStatusCode = shippingPackageStatusCode;
    }

    /**
     * @return the slot
     */
    public int getSlot() {
        return slot;
    }

    /**
     * @param slot the slot to set
     */
    public void setSlot(int slot) {
        this.slot = slot;
    }

    /**
     * @return the brand
     */
    public String getBrand() {
        return brand;
    }

    /**
     * @param brand the brand to set
     */
    public void setBrand(String brand) {
        this.brand = brand;
    }

    /**
     * @return the color
     */
    public String getColor() {
        return color;
    }

    /**
     * @param color the color to set
     */
    public void setColor(String color) {
        this.color = color;
    }

    /**
     * @return the size
     */
    public String getSize() {
        return size;
    }

    /**
     * @param size the size to set
     */
    public void setSize(String size) {
        this.size = size;
    }

    /**
     * @return the itemSku
     */
    public String getItemSku() {
        return itemSku;
    }

    /**
     * @param itemSku the itemSku to set
     */
    public void setItemSku(String itemSku) {
        this.itemSku = itemSku;
    }

    /**
     * @return the itemName
     */
    public String getItemName() {
        return itemName;
    }

    /**
     * @param itemName the itemName to set
     */
    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    /**
     * @return the shelfCode
     */
    public String getShelfCode() {
        return shelfCode;
    }

    /**
     * @param shelfCode the shelfCode to set
     */
    public void setShelfCode(String shelfCode) {
        this.shelfCode = shelfCode;
    }

    /**
     * @return the splittable
     */
    public boolean isSplittable() {
        return splittable;
    }

    /**
     * @param splittable the splittable to set
     */
    public void setSplittable(boolean splittable) {
        this.splittable = splittable;
    }

    /**
     * @return the statusCode
     */
    public String getStatusCode() {
        return statusCode;
    }

    /**
     * @param statusCode the statusCode to set
     */
    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    /**
     * @return the saleOrderItemId
     */
    public int getSaleOrderItemId() {
        return saleOrderItemId;
    }

    /**
     * @param saleOrderItemId the saleOrderItemId to set
     */
    public void setSaleOrderItemId(int saleOrderItemId) {
        this.saleOrderItemId = saleOrderItemId;
    }

    /**
     * @return the saleOrderCode
     */
    public String getSaleOrderCode() {
        return saleOrderCode;
    }

    /**
     * @param saleOrderCode the saleOrderCode to set
     */
    public void setSaleOrderCode(String saleOrderCode) {
        this.saleOrderCode = saleOrderCode;
    }

    /**
     * @return the numberOfItems
     */
    public int getNumberOfItems() {
        return numberOfItems;
    }

    /**
     * @param numberOfItems the numberOfItems to set
     */
    public void setNumberOfItems(int numberOfItems) {
        this.numberOfItems = numberOfItems;
    }

    /**
     * @return the maxRetailPrice
     */
    public BigDecimal getMaxRetailPrice() {
        return maxRetailPrice;
    }

    /**
     * @param maxRetailPrice the maxRetailPrice to set
     */
    public void setMaxRetailPrice(BigDecimal maxRetailPrice) {
        this.maxRetailPrice = maxRetailPrice;
    }

    /**
     * @return the itemCode
     */
    public String getItemCode() {
        return itemCode;
    }

    /**
     * @param itemCode the itemCode to set
     */
    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    /**
     * @return the channelName
     */
    public String getChannelName() {
        return channelName;
    }

    /**
     * @param channelName the channelName to set
     */
    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    /**
     * @return the displayOrderCode
     */
    public String getDisplayOrderCode() {
        return displayOrderCode;
    }

    /**
     * @param displayOrderCode the displayOrderCode to set
     */
    public void setDisplayOrderCode(String displayOrderCode) {
        this.displayOrderCode = displayOrderCode;
    }
    
    public String getShippingPackageCode() {
        return shippingPackageCode;
    }

    public void setShippingPackageCode(String shippingPackageCode) {
        this.shippingPackageCode = shippingPackageCode;
    }
    
    public String getSaleOrderItemCode() {
        return saleOrderItemCode;
    }

    public void setSaleOrderItemCode(String saleOrderItemCode) {
        this.saleOrderItemCode = saleOrderItemCode;
    }

}
