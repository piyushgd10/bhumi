/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 2, 2012
 *  @author praveeng
 */
package com.uniware.core.api.purchase;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author praveeng
 */
public class GetPurchaseOrderRequest extends ServiceRequest {
    /**
     * 
     */
    private static final long serialVersionUID             = -3828893208167364186L;

    @NotBlank
    private String            purchaseOrderCode;

    private boolean           fetchPurchaseOrderItemDetail = true;

    /**
     * @return the purchaseOrderCode
     */
    public String getPurchaseOrderCode() {
        return purchaseOrderCode;
    }

    /**
     * @param purchaseOrderCode the purchaseOrderCode to set
     */
    public void setPurchaseOrderCode(String purchaseOrderCode) {
        this.purchaseOrderCode = purchaseOrderCode;
    }

    public boolean isFetchPurchaseOrderItemDetail() {
        return fetchPurchaseOrderItemDetail;
    }

    public void setFetchPurchaseOrderItemDetail(boolean fetchPurchaseOrderItemDetail) {
        this.fetchPurchaseOrderItemDetail = fetchPurchaseOrderItemDetail;
    }
}
