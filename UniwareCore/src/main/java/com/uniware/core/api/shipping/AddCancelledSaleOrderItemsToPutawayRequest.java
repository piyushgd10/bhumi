/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 14-May-2012
 *  @author praveeng
 */
package com.uniware.core.api.shipping;

import java.util.List;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

/**
 *<ol> Contains:
 * <li>{@code putawayCode} putaway code in which cancelled sale order items are to be added.</li>
 * <li>{@code shipmentCode} shipping package code</li>
 * <li>{@code cancelledSaleOrderItems} List of {@link WsSaleOrderItem}</li>
 *</ol>
 */
public class AddCancelledSaleOrderItemsToPutawayRequest extends ServiceRequest {

    private static final long     serialVersionUID = -6376748302415662367L;

    @NotNull
    private Integer               userId;

    @NotBlank
    private String                putawayCode;

    @NotBlank
    private String                shipmentCode;

    private List<WsSaleOrderItem> cancelledSaleOrderItems;

    /**
     * @return the putawayId
     */
    public String getPutawayCode() {
        return putawayCode;
    }

    /**
     * @param putawayCode the putawayId to set
     */
    public void setPutawayCode(String putawayCode) {
        this.putawayCode = putawayCode;
    }

    /**
     * @return the shipmentCode
     */
    public String getShipmentCode() {
        return shipmentCode;
    }

    /**
     * @param shipmentCode the shipmentCode to set
     */
    public void setShipmentCode(String shipmentCode) {
        this.shipmentCode = shipmentCode;
    }

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public List<WsSaleOrderItem> getCancelledSaleOrderItems() {
        return cancelledSaleOrderItems;
    }

    public void setCancelledSaleOrderItems(List<WsSaleOrderItem> cancelledSaleOrderItems) {
        this.cancelledSaleOrderItems = cancelledSaleOrderItems;
    }

    /**
     * <ol>
     *     <li>{@code code} code of sale order item</li>
     *     <li>{@code status} Contains one of the values in {@link StatusCode}</li>
     * </ol>
     */
    public static class WsSaleOrderItem {

        /**
         * Contains {@code GOOD_INVENTORY, BAD_INVENTORY}
         */
        public enum StatusCode {
            GOOD_INVENTORY,
            BAD_INVENTORY
        }

        @NotBlank
        private String     code;

        @NotNull
        private StatusCode status;

        public StatusCode getStatus() {
            return status;
        }

        public void setStatus(StatusCode status) {
            this.status = status;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }
    }
}
