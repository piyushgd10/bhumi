/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 20, 2012
 *  @author praveeng
 */
package com.uniware.core.api.party.dto;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.unifier.core.api.customfields.CustomFieldMetadataDTO;
import com.unifier.core.transport.http.HttpSender;
import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.EncryptionUtils;
import com.uniware.core.entity.Vendor;
import com.uniware.core.entity.VendorAgreement;

/**
 * @author praveeng
 */
public class VendorFullDTO extends GenericPartyDTO {

    private int                          id;
    private boolean                      configured;
    private Integer                      purchaseExpiryPeriod;
    private boolean                      acceptsCForm;
    private String                       signupLink;
    private String                       signedUsername;
    private List<CustomFieldMetadataDTO> customFieldValues;
    private List<VendorAgreementDTO>     vendorAgreements = new ArrayList<VendorAgreementDTO>();
    
    /**
     * @param vendor
     */
    public VendorFullDTO(Vendor vendor) {
        super(vendor);
        this.id = vendor.getId();
        this.configured = vendor.isConfigured();
        this.purchaseExpiryPeriod = vendor.getPurchaseExpiryPeriod();
        this.acceptsCForm = vendor.isAcceptsCForm();
        if (vendor.getUser() == null) {
            Map<String, String> urlParams = new HashMap<String, String>();
            urlParams.put("vendorId", String.valueOf(vendor.getId()));
            String timestamp = String.valueOf(DateUtils.getCurrentTime().getTime());
            urlParams.put("timestamp", timestamp);
            urlParams.put("checksum", EncryptionUtils.md5Encode(String.valueOf(vendor.getId()) + timestamp, "unicom"));
            this.signupLink = HttpSender.createURL("/signup/vendor", urlParams);
        } else {
            this.signedUsername = vendor.getUser().getUsername();
        }
        for (VendorAgreement vendorAgreement : vendor.getVendorAgreements()) {
            this.vendorAgreements.add(new VendorAgreementDTO(vendorAgreement));
        }
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the vendorAgreements
     */
    public List<VendorAgreementDTO> getVendorAgreements() {
        return vendorAgreements;
    }

    /**
     * @param vendorAgreements the vendorAgreements to set
     */
    public void setVendorAgreements(List<VendorAgreementDTO> vendorAgreements) {
        this.vendorAgreements = vendorAgreements;
    }

    public void addVendorAgreements(VendorAgreementDTO agreement) {
        vendorAgreements.add(agreement);
    }

    /**
     * @return the configured
     */
    public boolean isConfigured() {
        return configured;
    }

    /**
     * @param configured the configured to set
     */
    public void setConfigured(boolean configured) {
        this.configured = configured;
    }


    /**
     * @return the purchaseExpiryPeriod
     */
    public Integer getPurchaseExpiryPeriod() {
        return purchaseExpiryPeriod;
    }

    /**
     * @param purchaseExpiryPeriod the purchaseExpiryPeriod to set
     */
    public void setPurchaseExpiryPeriod(Integer purchaseExpiryPeriod) {
        this.purchaseExpiryPeriod = purchaseExpiryPeriod;
    }

    /**
     * @return the acceptsCForm
     */
    public boolean isAcceptsCForm() {
        return acceptsCForm;
    }

    /**
     * @param acceptsCForm the acceptsCForm to set
     */
    public void setAcceptsCForm(boolean acceptsCForm) {
        this.acceptsCForm = acceptsCForm;
    }

    /**
     * @return the customFieldValues
     */
    public List<CustomFieldMetadataDTO> getCustomFieldValues() {
        return customFieldValues;
    }

    /**
     * @param customFieldValues the customFieldValues to set
     */
    public void setCustomFieldValues(List<CustomFieldMetadataDTO> customFieldValues) {
        this.customFieldValues = customFieldValues;
    }

    public String getSignupLink() {
        return signupLink;
    }

    public void setSignupLink(String signupLink) {
        this.signupLink = signupLink;
    }

    /**
     * @return the signedUsername
     */
    public String getSignedUsername() {
        return signedUsername;
    }

    /**
     * @param signedUsername the signedUsername to set
     */
    public void setSignedUsername(String signedUsername) {
        this.signedUsername = signedUsername;
    }

}
