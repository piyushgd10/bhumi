/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 28-Jan-2015
 *  @author akshaykochhar
 */
package com.uniware.core.api.shipping;

import com.unifier.core.api.base.ServiceResponse;

public class AddShippingPackageToManifestInternalResponse extends ServiceResponse{

    /**
     * 
     */
    private static final long serialVersionUID = -5277438690597512272L;
    
    private ManifestItemDTO manifestItem;

    public ManifestItemDTO getManifestItem() {
        return manifestItem;
    }

    public void setManifestItem(ManifestItemDTO manifestItem) {
        this.manifestItem = manifestItem;
    }

}
