/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 24-Dec-2011
 *  @author vibhu
 */
package com.uniware.core.api.admin.shipping;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author Sunny
 */
public class AddShippingProviderMethodsRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -8441687950733318686L;
    @Valid
    private List<MethodDTO>   shippingMethods  = new ArrayList<MethodDTO>();

    public AddShippingProviderMethodsRequest() {
    }

    public AddShippingProviderMethodsRequest(List<MethodDTO> shippingMethods) {
        this.shippingMethods = shippingMethods;
    }

    /**
     * @return the shippingMethods
     */
    public List<MethodDTO> getShippingMethods() {
        return shippingMethods;
    }

    /**
     * @param shippingMethods the shippingMethods to set
     */
    public void setShippingMethods(List<MethodDTO> shippingMethods) {
        this.shippingMethods = shippingMethods;
    }

}