/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 12-Feb-2015
 *  @author parijat
 */
package com.uniware.core.api.shipping;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.unifier.core.api.base.ServiceRequest;
import com.uniware.core.api.model.WsShippingProviderDetail;

/**
 * @author parijat
 *
 */
public class AddShippingProviderRequest extends ServiceRequest {

    /**
     *
     */
    private static final long serialVersionUID = 5704285220992920486L;

    @NotNull
    @Valid
    private WsShippingProviderDetail wsShippingProvider;

    /**
     * @return the wsShippingProvider
     */
    public WsShippingProviderDetail getWsShippingProvider() {
        return wsShippingProvider;
    }

    /**
     * @param wsShippingProvider the wsShippingProvider to set
     */
    public void setWsShippingProvider(WsShippingProviderDetail wsShippingProvider) {
        this.wsShippingProvider = wsShippingProvider;
    }

}
