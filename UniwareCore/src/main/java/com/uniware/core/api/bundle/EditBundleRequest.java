/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 14, 2012
 *  @author singla
 */
package com.uniware.core.api.bundle;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author Sunny
 */
public class EditBundleRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 1861135562369859701L;

    @NotNull
    @Valid
    private WsBundle          wsBundle;

    public EditBundleRequest() {
    }

    public EditBundleRequest(WsBundle wsBundle) {
        this.wsBundle = wsBundle;
    }

    public WsBundle getWsBundle() {
        return wsBundle;
    }

    public void setWsBundle(WsBundle wsBundle) {
        this.wsBundle = wsBundle;
    }

}
