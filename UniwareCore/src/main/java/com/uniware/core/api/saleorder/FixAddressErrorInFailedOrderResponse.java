/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 10-Jun-2014
 *  @author unicom
 */
package com.uniware.core.api.saleorder;

import com.unifier.core.api.base.ServiceResponse;

public class FixAddressErrorInFailedOrderResponse extends ServiceResponse {

    private static final long serialVersionUID = -4163268626552791111L;

}
