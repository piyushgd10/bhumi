/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 3, 2012
 *  @author praveeng
 */
package com.uniware.core.api.party;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author praveeng
 */
public class ActivateVendorAgreementResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 6194038992870628859L;

}
