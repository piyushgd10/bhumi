/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 11-Feb-2015
 *  @author parijat
 */
package com.uniware.core.api.shipping;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author parijat
 *
 */
public class AddShippingProviderConnectorResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -9029459864137369712L;

}
