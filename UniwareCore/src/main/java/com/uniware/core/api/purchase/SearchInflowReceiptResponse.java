/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 21-May-2012
 *  @author praveeng
 */
package com.uniware.core.api.purchase;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author praveeng
 */
public class SearchInflowReceiptResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long     serialVersionUID = 8817457926777302861L;
    private List<WsInflowReceipt> elements         = new ArrayList<WsInflowReceipt>();
    private Long                  totalRecords;

    /**
     * @return the elements
     */
    public List<WsInflowReceipt> getElements() {
        return elements;
    }

    /**
     * @param elements the elements to set
     */
    public void setElements(List<WsInflowReceipt> elements) {
        this.elements = elements;
    }

    /**
     * @return the totalRecords
     */
    public Long getTotalRecords() {
        return totalRecords;
    }

    /**
     * @param totalRecords the totalRecords to set
     */
    public void setTotalRecords(Long totalRecords) {
        this.totalRecords = totalRecords;
    }

    public static class WsInflowReceipt {
        private String code;
        private String statusCode;
        private Date   created;
        private String vendorInvoiceNumber;
        private Date   vendorInvoiceDate;
        private String vendorName;
        private String purchaseOrderCode;

        /**
         * @return the code
         */
        public String getCode() {
            return code;
        }

        /**
         * @param code the code to set
         */
        public void setCode(String code) {
            this.code = code;
        }

        /**
         * @return the statusCode
         */
        public String getStatusCode() {
            return statusCode;
        }

        /**
         * @param statusCode the statusCode to set
         */
        public void setStatusCode(String statusCode) {
            this.statusCode = statusCode;
        }

        /**
         * @return the created
         */
        public Date getCreated() {
            return created;
        }

        /**
         * @param created the created to set
         */
        public void setCreated(Date created) {
            this.created = created;
        }

        /**
         * @return the vendorInvoiceNumber
         */
        public String getVendorInvoiceNumber() {
            return vendorInvoiceNumber;
        }

        /**
         * @param vendorInvoiceNumber the vendorInvoiceNumber to set
         */
        public void setVendorInvoiceNumber(String vendorInvoiceNumber) {
            this.vendorInvoiceNumber = vendorInvoiceNumber;
        }

        /**
         * @return the vendorInvoiceDate
         */
        public Date getVendorInvoiceDate() {
            return vendorInvoiceDate;
        }

        /**
         * @param vendorInvoiceDate the vendorInvoiceDate to set
         */
        public void setVendorInvoiceDate(Date vendorInvoiceDate) {
            this.vendorInvoiceDate = vendorInvoiceDate;
        }

        /**
         * @return the vendorName
         */
        public String getVendorName() {
            return vendorName;
        }

        /**
         * @param vendorName the vendorName to set
         */
        public void setVendorName(String vendorName) {
            this.vendorName = vendorName;
        }

        /**
         * @return the purchaseOrderCode
         */
        public String getPurchaseOrderCode() {
            return purchaseOrderCode;
        }

        /**
         * @param purchaseOrderCode the purchaseOrderCode to set
         */
        public void setPurchaseOrderCode(String purchaseOrderCode) {
            this.purchaseOrderCode = purchaseOrderCode;
        }

    }
}
