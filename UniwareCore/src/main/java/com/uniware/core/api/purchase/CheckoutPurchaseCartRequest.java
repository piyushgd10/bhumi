/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Apr 7, 2012
 *  @author singla
 */
package com.uniware.core.api.purchase;

import com.unifier.core.api.base.ServiceRequest;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author singla
 */
public class CheckoutPurchaseCartRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long                serialVersionUID = 2622957883004592137L;

    @NotNull
    private Integer                          userId;

    @NotEmpty
    @Valid
    private List<WsCheckoutPurchaseCartItem> checkoutPurchaseCartItems;

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public List<WsCheckoutPurchaseCartItem> getCheckoutPurchaseCartItems() {
        return checkoutPurchaseCartItems;
    }

    public void setCheckoutPurchaseCartItems(List<WsCheckoutPurchaseCartItem> checkoutPurchaseCartItems) {
        this.checkoutPurchaseCartItems = checkoutPurchaseCartItems;
    }

}
