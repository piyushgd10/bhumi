package com.uniware.core.api.dual.company;

import java.util.List;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

public class WsInflowItemType {
    @NotBlank
    private String       itemSkuCode;

    @NotEmpty
    private List<String> itemCodes;

    /**
     * @return the itemSkuCode
     */
    public String getItemSkuCode() {
        return itemSkuCode;
    }

    /**
     * @param itemSkuCode the itemSkuCode to set
     */
    public void setItemSkuCode(String itemSkuCode) {
        this.itemSkuCode = itemSkuCode;
    }

    /**
     * @return the itemCodes
     */
    public List<String> getItemCodes() {
        return itemCodes;
    }

    /**
     * @param itemCodes the itemCodes to set
     */
    public void setItemCodes(List<String> itemCodes) {
        this.itemCodes = itemCodes;
    }
}