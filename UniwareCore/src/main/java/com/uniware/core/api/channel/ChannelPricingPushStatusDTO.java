/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Oct 19, 2015
 *  @author akshay
 */
package com.uniware.core.api.channel;

import java.io.Serializable;
import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;

import com.uniware.core.entity.Channel;

@Document(collection = "channelPricingPushStatus")
@CompoundIndexes({ @CompoundIndex(name = "tenant_channel", def = "{'tenantCode' :  1, 'channelCode' :  1}", unique = true) })
public class ChannelPricingPushStatusDTO implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -4807880477697994796L;

    @Id
    private String                      id;

    private String                      channelCode;
    private String                      tenantCode;

    private boolean                     running;
    private String                      message;
    private long                        totalMileStones;
    private long                        currentMileStone;
    private int                         lastSuccessfullPushCount;
    private long                        todaysSuccessfullPushCount;
    private int                         lastFailedPushCount;
    private long                        failedCount;
    private Channel.SyncExecutionStatus syncExecutionStatus = Channel.SyncExecutionStatus.IDLE;
    private boolean                     lastPushSuccessfull;
    private Date                        lastPushTime;
    private Date                        lastPushFailedTime;
    private Date                        lastSuccessfullPriceSyncTime;

    public ChannelPricingPushStatusDTO(String channelCode) {
        this.channelCode = channelCode;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    public String getTenantCode() {
        return tenantCode;
    }

    public void setTenantCode(String tenantCode) {
        this.tenantCode = tenantCode;
    }

    public boolean isRunning() {
        return running;
    }

    public void setRunning(boolean running) {
        this.running = running;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public long getTotalMileStones() {
        return totalMileStones;
    }

    public void setTotalMileStones(long totalMileStones) {
        this.totalMileStones = totalMileStones;
    }

    public long getCurrentMileStone() {
        return currentMileStone;
    }

    public void setCurrentMileStone(long currentMileStone) {
        this.currentMileStone = currentMileStone;
    }

    public int getLastSuccessfullPushCount() {
        return lastSuccessfullPushCount;
    }

    public void setLastSuccessfullPushCount(int lastSuccessfullPushCount) {
        this.lastSuccessfullPushCount = lastSuccessfullPushCount;
    }

    public long getTodaysSuccessfullPushCount() {
        return todaysSuccessfullPushCount;
    }

    public void setTodaysSuccessfullPushCount(long todaysSuccessfullPushCount) {
        this.todaysSuccessfullPushCount = todaysSuccessfullPushCount;
    }

    public int getLastFailedPushCount() {
        return lastFailedPushCount;
    }

    public void setLastFailedPushCount(int lastFailedPushCount) {
        this.lastFailedPushCount = lastFailedPushCount;
    }

    public long getFailedCount() {
        return failedCount;
    }

    public void setFailedCount(long failedCount) {
        this.failedCount = failedCount;
    }

    public boolean isLastPushSuccessful() {
        return lastPushSuccessfull;
    }

    public void setLastPushSuccessful(boolean lastPushSuccessfull) {
        this.lastPushSuccessfull = lastPushSuccessfull;
    }

    public Date getLastPushTime() {
        return lastPushTime;
    }

    public void setLastPushTime(Date lastPushTime) {
        this.lastPushTime = lastPushTime;
    }

    public Date getLastPushFailedTime() {
        return lastPushFailedTime;
    }

    public void setLastPushFailedTime(Date lastPushFailedTime) {
        this.lastPushFailedTime = lastPushFailedTime;
    }

    public Date getLastSuccessfullPriceSyncTime() {
        return lastSuccessfullPriceSyncTime;
    }

    public void setLastSuccessfullPriceSyncTime(Date lastSuccessfullPriceSyncTime) {
        this.lastSuccessfullPriceSyncTime = lastSuccessfullPriceSyncTime;
    }

    public void incrementSuccessfullPushCount(int size) {
        lastSuccessfullPushCount += size;
    }

    public void incrementFailedPushCount(int size) {
        lastFailedPushCount += size;
    }

    public Channel.SyncExecutionStatus getSyncExecutionStatus() {
        return syncExecutionStatus;
    }

    public void setSyncExecutionStatus(Channel.SyncExecutionStatus syncExecutionStatus) {
        this.syncExecutionStatus = syncExecutionStatus;
    }

    public void reset() {
        running = false;
        totalMileStones = 0;
        currentMileStone = 0;
        lastSuccessfullPushCount = 0;
        lastFailedPushCount = 0;
        lastPushSuccessfull = false;
    }

    public float getPercentageComplete() {
        if (totalMileStones == 0) {
            return 0;
        }
        return ((float) (currentMileStone * 10000 / totalMileStones)) / 100;
    }

    public void setMileStone(String message, int completedMilestones) {
        this.message = message;
        currentMileStone += completedMilestones;
    }

}
