/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 14-May-2012
 *  @author praveeng
 */
package com.uniware.core.api.returns;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author praveeng
 */
public class AddReturnsSaleOrderItemsToPutawayRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long      serialVersionUID = 4749175056926468594L;

    @NotEmpty
    private String                 shipmentCode;

    @NotNull
    private Integer                userId;

    @NotBlank
    private String                 putawayCode;

    @Valid
    private List<SaleOrderItemDTO> saleOrderItems   = new ArrayList<SaleOrderItemDTO>();;

    /**
     * @return the shipmentCode
     */
    public String getShipmentCode() {
        return shipmentCode;
    }

    /**
     * @param shipmentCode the shipmentCode to set
     */
    public void setShipmentCode(String shipmentCode) {
        this.shipmentCode = shipmentCode;
    }

    /**
     * @return the saleOrderItems
     */
    public List<SaleOrderItemDTO> getSaleOrderItems() {
        return saleOrderItems;
    }

    /**
     * @param saleOrderItems the saleOrderItems to set
     */
    public void setSaleOrderItems(List<SaleOrderItemDTO> saleOrderItems) {
        this.saleOrderItems = saleOrderItems;
    }

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return the putawayCode
     */
    public String getPutawayCode() {
        return putawayCode;
    }

    /**
     * @param putawayCode the putawayCode to set
     */
    public void setPutawayCode(String putawayCode) {
        this.putawayCode = putawayCode;
    }

    public static class SaleOrderItemDTO {

        @NotBlank
        private String  code;

        @NotEmpty
        private String  status;

        private boolean expiredReturn;

        public SaleOrderItemDTO() {
        }

        public SaleOrderItemDTO(String code, String status) {
            this.code = code;
            this.status = status;
        }

        /**
         * @return the status
         */
        public String getStatus() {
            return status;
        }

        /**
         * @param status the status to set
         */
        public void setStatus(String status) {
            this.status = status;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public boolean isExpiredReturn() {
            return expiredReturn;
        }

        public void setExpiredReturn(boolean expiredReturn) {
            this.expiredReturn = expiredReturn;
        }
    }

}
