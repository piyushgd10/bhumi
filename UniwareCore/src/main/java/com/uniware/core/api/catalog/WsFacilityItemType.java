/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Nov 16, 2012
 *  @author singla
 */
package com.uniware.core.api.catalog;

import java.math.BigDecimal;
import java.util.List;

import javax.validation.Valid;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.customfields.WsCustomFieldValue;

/**
 * @author singla
 */
public class WsFacilityItemType {
    @NotBlank
    private String                   facilityCode;
    @NotBlank
    private String                   itemTypeSkuCode;

    private String                   facilitySkuCode;

    private Integer                  inventory;

    private Integer                  priority;
    private BigDecimal               transferPrice;
    private BigDecimal               commission;
    private BigDecimal               shippingCharges;
    private Boolean                  enabled;

    @Valid
    private List<WsCustomFieldValue> customFieldValues;

    /**
     * @return the commission
     */
    public BigDecimal getCommission() {
        return commission;
    }

    /**
     * @param commission the commission to set
     */
    public void setCommission(BigDecimal commission) {
        this.commission = commission;
    }

    /**
     * @return the shippingCharges
     */
    public BigDecimal getShippingCharges() {
        return shippingCharges;
    }

    /**
     * @param shippingCharges the shippingCharges to set
     */
    public void setShippingCharges(BigDecimal shippingCharges) {
        this.shippingCharges = shippingCharges;
    }

    /**
     * @return the facilityCode
     */
    public String getFacilityCode() {
        return facilityCode;
    }

    /**
     * @param facilityCode the facilityCode to set
     */
    public void setFacilityCode(String facilityCode) {
        this.facilityCode = facilityCode;
    }

    /**
     * @return the itemTypeSkuCode
     */
    public String getItemTypeSkuCode() {
        return itemTypeSkuCode;
    }

    /**
     * @param itemTypeSkuCode the itemTypeSkuCode to set
     */
    public void setItemTypeSkuCode(String itemTypeSkuCode) {
        this.itemTypeSkuCode = itemTypeSkuCode;
    }

    /**
     * @return the facilitySkuCode
     */
    public String getFacilitySkuCode() {
        return facilitySkuCode;
    }

    /**
     * @param facilitySkuCode the facilitySkuCode to set
     */
    public void setFacilitySkuCode(String facilitySkuCode) {
        this.facilitySkuCode = facilitySkuCode;
    }

    /**
     * @return the inventory
     */
    public Integer getInventory() {
        return inventory;
    }

    /**
     * @param inventory the inventory to set
     */
    public void setInventory(Integer inventory) {
        this.inventory = inventory;
    }

    /**
     * @return the priority
     */
    public Integer getPriority() {
        return priority;
    }

    /**
     * @param priority the priority to set
     */
    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    /**
     * @return the enabled
     */
    public Boolean getEnabled() {
        return enabled;
    }

    /**
     * @param enabled the enabled to set
     */
    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public BigDecimal getTransferPrice() {
        return transferPrice;
    }

    public void setTransferPrice(BigDecimal transferPrice) {
        this.transferPrice = transferPrice;
    }

    public List<WsCustomFieldValue> getCustomFieldValues() {
        return customFieldValues;
    }

    public void setCustomFieldValues(List<WsCustomFieldValue> customFieldValues) {
        this.customFieldValues = customFieldValues;
    }

}
