/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 25-Jan-2012
 *  @author vibhu
 */
package com.uniware.core.api.party;

import java.util.HashMap;
import java.util.Map;

import com.uniware.core.entity.Party;

/**
 * @author vibhu
 */
public class PartyDTO {

    private int                          id;
    private String                       code;
    private String                       alternateCode;
    private String                       name;
    private PartyDetailDTO               detail;
    private Map<String, PartyAddressDTO> partyAddresses = new HashMap<String, PartyAddressDTO>();
    private Map<String, PartyContactDTO> partyContacts  = new HashMap<String, PartyContactDTO>();

    public PartyDTO() {
    }

    /**
     * 
     */
    public PartyDTO(Party party) {
        this.id = party.getId();
        this.code = party.getCode();
        this.alternateCode = party.getAlternateCode();
        this.name = party.getName();
        this.detail = new PartyDetailDTO(party);
    }

    public void addPartyAddress(PartyAddressDTO partyAddress) {
        this.partyAddresses.put(partyAddress.getAddressType(), partyAddress);
    }

    public void addPartyContact(PartyContactDTO partyContact) {
        this.partyContacts.put(partyContact.getContactType(), partyContact);
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the detail
     */
    public PartyDetailDTO getDetail() {
        return detail;
    }

    /**
     * @param detail the detail to set
     */
    public void setDetail(PartyDetailDTO detail) {
        this.detail = detail;
    }

    /**
     * @return the partyAddresses
     */
    public Map<String, PartyAddressDTO> getPartyAddresses() {
        return partyAddresses;
    }

    /**
     * @param partyAddresses the partyAddresses to set
     */
    public void setPartyAddresses(Map<String, PartyAddressDTO> partyAddresses) {
        this.partyAddresses = partyAddresses;
    }

    /**
     * @return the partyContacts
     */
    public Map<String, PartyContactDTO> getPartyContacts() {
        return partyContacts;
    }

    /**
     * @param partyContacts the partyContacts to set
     */
    public void setPartyContacts(Map<String, PartyContactDTO> partyContacts) {
        this.partyContacts = partyContacts;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    public String getAlternateCode() {
        return alternateCode;
    }

    public void setAlternateCode(String alternateCode) {
        this.alternateCode = alternateCode;
    }
}