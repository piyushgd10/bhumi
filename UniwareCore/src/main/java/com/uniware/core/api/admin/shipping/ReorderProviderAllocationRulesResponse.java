/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 11-Aug-2012
 *  @author praveeng
 */
package com.uniware.core.api.admin.shipping;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author praveeng
 */
public class ReorderProviderAllocationRulesResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 3371821868504930602L;

}
