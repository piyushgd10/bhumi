/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, May 9, 2012
 *  @author singla
 */
package com.uniware.core.api.putaway;

import java.util.ArrayList;
import java.util.List;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author singla
 */
public class CreatePutawayListResponse extends ServiceResponse {

    /**
     * 
     */

    private List<String> inactiveShelfCodes = new ArrayList<>();

    public List<String> getInactiveShelfCodes() {
        return inactiveShelfCodes;
    }

    public void setInactiveShelfCodes(List<String> inactiveShelfCodes) {
        this.inactiveShelfCodes = inactiveShelfCodes;
    }

    private static final long serialVersionUID = 6821693074218801204L;

}
