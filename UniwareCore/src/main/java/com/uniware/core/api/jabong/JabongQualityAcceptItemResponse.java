/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jul 6, 2012
 *  @author singla
 */
package com.uniware.core.api.jabong;

import com.unifier.core.api.base.ServiceResponse;

public class JabongQualityAcceptItemResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 7042155971843791907L;

    private int               numberOfItems    = 0;
    private String            saleOrderCode;
    private String            saleOrderItemCode;

    public String getSaleOrderCode() {
        return saleOrderCode;
    }

    public void setSaleOrderCode(String saleOrderCode) {
        this.saleOrderCode = saleOrderCode;
    }

    public String getSaleOrderItemCode() {
        return saleOrderItemCode;
    }

    public void setSaleOrderItemCode(String saleOrderItemCode) {
        this.saleOrderItemCode = saleOrderItemCode;
    }

    public void setNumberOfItems(int numberOfItems) {
        this.numberOfItems = numberOfItems;
    }

    public int getNumberOfItems() {
        return numberOfItems;
    }

}
