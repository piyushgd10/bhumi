/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 9, 2012
 *  @author singla
 */
package com.uniware.core.api.returns;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author singla
 */
public class MarkSaleOrderReturnedResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 5956968031728252517L;

}