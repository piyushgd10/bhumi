/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 29, 2012
 *  @author singla
 */
package com.uniware.core.api.purchase;

import java.math.BigDecimal;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author singla
 */
public class AddOrEditPurchaseOrderItemsRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long         serialVersionUID = 1383325854258959712L;

    private String                    purchaseOrderCode;

    private String                    vendorCode;

    private String                    logisticChargesDivisionMethod;

    private String                    currencyCode;

    private BigDecimal                logisticCharges  = BigDecimal.ZERO;

    @NotNull
    @Valid
    private List<WsPurchaseOrderItem> purchaseOrderItems;

    /**
     * @return the purchaseOrderCode
     */
    public String getPurchaseOrderCode() {
        return purchaseOrderCode;
    }

    /**
     * @param purchaseOrderCode the purchaseOrderCode to set
     */
    public void setPurchaseOrderCode(String purchaseOrderCode) {
        this.purchaseOrderCode = purchaseOrderCode;
    }

    /**
     * @return the vendorCode
     */
    public String getVendorCode() {
        return vendorCode;
    }

    /**
     * @param vendorCode the vendorCode to set
     */
    public void setVendorCode(String vendorCode) {
        this.vendorCode = vendorCode;
    }

    /**
     * @return the logisticChargesDivisionMethod
     */
    public String getLogisticChargesDivisionMethod() {
        return logisticChargesDivisionMethod;
    }

    /**
     * @param logisticChargesDivisionMethod the logisticChargesDivisionMethod to set
     */
    public void setLogisticChargesDivisionMethod(String logisticChargesDivisionMethod) {
        this.logisticChargesDivisionMethod = logisticChargesDivisionMethod;
    }

    /**
     * @return the logisticCharges
     */
    public BigDecimal getLogisticCharges() {
        return logisticCharges;
    }

    /**
     * @param logisticCharges the logisticCharges to set
     */
    public void setLogisticCharges(BigDecimal logisticCharges) {
        this.logisticCharges = logisticCharges;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    /**
     * @return the purchaseOrderItems
     */
    public List<WsPurchaseOrderItem> getPurchaseOrderItems() {
        return purchaseOrderItems;
    }

    /**
     * @param purchaseOrderItems the purchaseOrderItems to set
     */
    public void setPurchaseOrderItems(List<WsPurchaseOrderItem> purchaseOrderItems) {
        this.purchaseOrderItems = purchaseOrderItems;
    }

}
