/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Nov 25, 2015
 *  @author akshay
 */
package com.uniware.core.api.prices;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.validator.constraints.NotEmpty;

import com.unifier.core.api.base.ServiceRequest;

public class GetChannelItemTypePricesRequest extends ServiceRequest {

    private static final long serialVersionUID = -4325863162412247305L;
    
    @NotEmpty
    private List<String> sellerSkuCodes = new ArrayList<>();

    public List<String> getSellerSkuCodes() {
        return sellerSkuCodes;
    }

    public void setSellerSkuCodes(List<String> sellerSkuCodes) {
        this.sellerSkuCodes = sellerSkuCodes;
    }
    
}
