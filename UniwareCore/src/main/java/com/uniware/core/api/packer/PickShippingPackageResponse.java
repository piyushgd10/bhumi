/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 05-Feb-2014
 *  @author karunsingla
 */
package com.uniware.core.api.packer;

import com.unifier.core.api.base.ServiceResponse;

public class PickShippingPackageResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -4620127775566588398L;

}
