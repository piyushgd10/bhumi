/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 03-Jun-2012
 *  @author vibhu
 */
package com.uniware.core.api.usernotification;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;
import com.uniware.core.vo.UserNotificationTypeVO.NotificationType;

/**
 * @author Sunny
 */
public class AddUserNotificationRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -767335784096230275L;
    @NotNull
    private NotificationType  notificationType;

    @NotBlank
    private String            identifier;

    @NotBlank
    private String            message;

    private String            actionUrl;

    private String            title;

    public AddUserNotificationRequest(String identifier, NotificationType notificationType, String message, String actionUrl, String title) {
        super();
        this.identifier = identifier;
        this.notificationType = notificationType;
        this.message = message;
        this.actionUrl = actionUrl;
        this.title = title;
    }

    public AddUserNotificationRequest() {
        super();
    }

    public NotificationType getNotificationType() {
        return notificationType;
    }

    public void setNotificationType(NotificationType notificationType) {
        this.notificationType = notificationType;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getActionUrl() {
        return actionUrl;
    }

    public void setActionUrl(String actionUrl) {
        this.actionUrl = actionUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}
