/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 9, 2012
 *  @author praveeng
 */
package com.uniware.core.api.catalog;

/**
 * @author praveeng
 */
public class CreateItemTypeResponse extends ItemTypeResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -5300675916228168637L;

}
