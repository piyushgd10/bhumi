package com.uniware.core.api.picker;

import java.util.ArrayList;
import java.util.List;

import com.unifier.core.api.base.ServiceResponse;

/**
 * Created by bhuvneshwarkumar on 29/08/16.
 */
public class CreateBulkPickBucketResponse extends ServiceResponse {

    private static final long serialVersionUID = -6578381817207207892L;

    private List<String>      pickBucketCodes  = new ArrayList<>();

    public List<String> getPickBucketCodes() {
        return pickBucketCodes;
    }

    public void setPickBucketCodes(List<String> pickBucketCodes) {
        this.pickBucketCodes = pickBucketCodes;
    }
}
