/*
*  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
*  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
*  
*  @version     1.0, 02/09/14
*  @author sunny
*/

package com.uniware.core.api.catalog;

import java.util.List;

import com.unifier.core.api.base.ServiceResponse;
import com.uniware.core.api.catalog.dto.ItemTypeLookupDTO;

public class LookupSaleOrderLineItemsResponse extends ServiceResponse {
    private static final long serialVersionUID = -5490785385407630594L;

    private List<LineItem>    lineItems;

    public List<LineItem> getLineItems() {
        return lineItems;
    }

    public void setLineItems(List<LineItem> lineItems) {
        this.lineItems = lineItems;
    }

    public static class LineItem {
        private ItemTypeLookupDTO itemType;
        private Integer           quantity;
        private Double            sellingPrice;

        public ItemTypeLookupDTO getItemType() {
            return itemType;
        }

        public void setItemType(ItemTypeLookupDTO itemType) {
            this.itemType = itemType;
        }

        public Integer getQuantity() {
            return quantity;
        }

        public void setQuantity(Integer quantity) {
            this.quantity = quantity;
        }

        public Double getSellingPrice() {
            return sellingPrice;
        }

        public void setSellingPrice(Double sellingPrice) {
            this.sellingPrice = sellingPrice;
        }

    }
}