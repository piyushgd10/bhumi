/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 24-Dec-2011
 *  @author vibhu
 */
package com.uniware.core.api.warehouse;

import com.unifier.core.api.base.ServiceRequest;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author vibhu
 */
public class CreateSectionRequest extends ServiceRequest {

    /**
     *
     */
    private static final long serialVersionUID = -8441687950733318686L;
    @NotEmpty
    private String code;
    @NotEmpty
    private String name;
    private int    picksetId;

    private boolean useAsdefault;

    public CreateSectionRequest() {
        super();
    }

    public CreateSectionRequest(String code, String name, int picksetId, boolean useAsdefault) {
        super();
        this.code = code;
        this.name = name;
        this.picksetId = picksetId;
        this.useAsdefault = useAsdefault;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the picksetId
     */
    public int getPicksetId() {
        return picksetId;
    }

    /**
     * @param picksetId the picksetId to set
     */
    public void setPicksetId(int picksetId) {
        this.picksetId = picksetId;
    }

    public boolean isUseAsdefault() {
        return useAsdefault;
    }

    public void setUseAsdefault(boolean useAsdefault) {
        this.useAsdefault = useAsdefault;
    }

}