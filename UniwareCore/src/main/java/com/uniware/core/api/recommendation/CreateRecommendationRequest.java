/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 4, 2015
 *  @author akshay
 */
package com.uniware.core.api.recommendation;

import java.util.Date;

import javax.validation.constraints.Future;
import javax.validation.constraints.NotNull;

import org.codehaus.jackson.annotate.JsonSubTypes;
import org.codehaus.jackson.annotate.JsonSubTypes.Type;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.annotate.JsonTypeInfo.As;
import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

public class CreateRecommendationRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long         serialVersionUID = 4267556831788208518L;

    private String                    reason;

    @NotBlank
    private String                    source;

    @NotNull
    @Future(message = "must be a future interval")
    private Date                      expiryTime;

    @NotBlank
    private String                    createdBy;

    @NotNull
    @JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = As.PROPERTY, property = "recommendationType")
    @JsonSubTypes({@Type(name = "PRICE", value = PriceRecommendationBody.class) })
    private RecommendationBody recommendationBody;

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getExpiryTime() {
        return expiryTime;
    }

    public void setExpiryTime(Date expiryTime) {
        this.expiryTime = expiryTime;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public RecommendationBody getRecommendationBody() {
        return recommendationBody;
    }

    public void setRecommendationBody(RecommendationBody recommendationBody) {
        this.recommendationBody = recommendationBody;
    }

}
