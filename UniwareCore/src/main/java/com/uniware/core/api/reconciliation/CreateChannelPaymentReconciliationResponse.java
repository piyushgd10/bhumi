/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 07-Dec-2013
 *  @author parijat
 */
package com.uniware.core.api.reconciliation;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author parijat
 *
 */
public class CreateChannelPaymentReconciliationResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -2032759561348691482L;



}
