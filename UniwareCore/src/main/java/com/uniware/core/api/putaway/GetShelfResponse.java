/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 18-Jun-2012
 *  @author vibhu
 */
package com.uniware.core.api.putaway;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author vibhu
 */
public class GetShelfResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 1122769846858019786L;

    private String            shelfCode;

    /**
     * @return the shelfCode
     */
    public String getShelfCode() {
        return shelfCode;
    }

    /**
     * @param shelfCode the shelfCode to set
     */
    public void setShelfCode(String shelfCode) {
        this.shelfCode = shelfCode;
    }

}