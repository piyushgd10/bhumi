package com.uniware.core.api.o2o;

import com.unifier.core.api.base.ServiceResponse;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by sunny on 09/04/15.
 */
public class GetAvailableFacilitiesResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long        serialVersionUID = -1933715378263660511L;

    private List<ProductFacilityDTO> productFacilities;

    public List<ProductFacilityDTO> getProductFacilities() {
        return productFacilities;
    }

    public void setProductFacilities(List<ProductFacilityDTO> productFacilities) {
        this.productFacilities = productFacilities;
    }

    public static class ProductFacilityDTO {
        private String                  sellerSkuCode;
        private Integer                 quantity;
        private List<ShippingMethodDTO> shippingMethods;
        private Integer                 dispatchSLA;
        private Integer                 deliverySLA;
        private List<FacilityDTO>       facilities;

        public String getSellerSkuCode() {
            return sellerSkuCode;
        }

        public void setSellerSkuCode(String sellerSkuCode) {
            this.sellerSkuCode = sellerSkuCode;
        }

        public Integer getQuantity() {
            return quantity;
        }

        public void setQuantity(Integer quantity) {
            this.quantity = quantity;
        }

        public List<ShippingMethodDTO> getShippingMethods() {
            return shippingMethods;
        }

        public void setShippingMethods(List<ShippingMethodDTO> shippingMethods) {
            this.shippingMethods = shippingMethods;
        }

        public Integer getDispatchSLA() {
            return dispatchSLA;
        }

        public void setDispatchSLA(Integer dispatchSLA) {
            this.dispatchSLA = dispatchSLA;
        }

        public Integer getDeliverySLA() {
            return deliverySLA;
        }

        public void setDeliverySLA(Integer deliverySLA) {
            this.deliverySLA = deliverySLA;
        }

        public List<FacilityDTO> getFacilities() {
            return facilities;
        }

        public void setFacilities(List<FacilityDTO> facilities) {
            this.facilities = facilities;
        }
    }

    public static class ShippingMethodDTO {
        private String shippingMethodCode;
        private String paymentMethodCode;
        private String shippingMethodName;

        public ShippingMethodDTO() {
        }

        public ShippingMethodDTO(String shippingMethodCode, String paymentMethodCode, String shippingMethodName) {
            this.shippingMethodCode = shippingMethodCode;
            this.paymentMethodCode = paymentMethodCode;
            this.shippingMethodName = shippingMethodName;
        }

        public String getShippingMethodCode() {
            return shippingMethodCode;
        }

        public void setShippingMethodCode(String shippingMethodCode) {
            this.shippingMethodCode = shippingMethodCode;
        }

        public String getPaymentMethodCode() {
            return paymentMethodCode;
        }

        public void setPaymentMethodCode(String paymentMethodCode) {
            this.paymentMethodCode = paymentMethodCode;
        }

        public String getShippingMethodName() {
            return shippingMethodName;
        }

        public void setShippingMethodName(String shippingMethodName) {
            this.shippingMethodName = shippingMethodName;
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + ((paymentMethodCode == null) ? 0 : paymentMethodCode.hashCode());
            result = prime * result + ((shippingMethodCode == null) ? 0 : shippingMethodCode.hashCode());
            result = prime * result + ((shippingMethodName == null) ? 0 : shippingMethodName.hashCode());
            return result;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            ShippingMethodDTO other = (ShippingMethodDTO) obj;
            if (paymentMethodCode == null) {
                if (other.paymentMethodCode != null)
                    return false;
            } else if (!paymentMethodCode.equals(other.paymentMethodCode))
                return false;
            if (shippingMethodCode == null) {
                if (other.shippingMethodCode != null)
                    return false;
            } else if (!shippingMethodCode.equals(other.shippingMethodCode))
                return false;
            if (shippingMethodName == null) {
                if (other.shippingMethodName != null)
                    return false;
            } else if (!shippingMethodName.equals(other.shippingMethodName))
                return false;
            return true;
        }
    }

    public static class FacilityDTO {
        private String  code;
        private String  title;
        private String  shortAddress;
        private Integer inventory;
        private List<ShippingMethodDTO> shippingMethods = new ArrayList<>();
        private BigDecimal                             distance;
        private GetAvailableFacilitiesRequest.Location location;
        private Date                                   lastUpdated;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getShortAddress() {
            return shortAddress;
        }

        public void setShortAddress(String shortAddress) {
            this.shortAddress = shortAddress;
        }

        public Integer getInventory() {
            return inventory;
        }

        public void setInventory(Integer inventory) {
            this.inventory = inventory;
        }

        public List<ShippingMethodDTO> getShippingMethods() {
            return shippingMethods;
        }

        public void setShippingMethods(List<ShippingMethodDTO> shippingMethods) {
            this.shippingMethods = shippingMethods;
        }

        public BigDecimal getDistance() {
            return distance;
        }

        public void setDistance(BigDecimal distance) {
            this.distance = distance;
        }

        public GetAvailableFacilitiesRequest.Location getLocation() {
            return location;
        }

        public void setLocation(GetAvailableFacilitiesRequest.Location location) {
            this.location = location;
        }

        public Date getLastUpdated() {
            return lastUpdated;
        }

        public void setLastUpdated(Date lastUpdated) {
            this.lastUpdated = lastUpdated;
        }
    }
}
