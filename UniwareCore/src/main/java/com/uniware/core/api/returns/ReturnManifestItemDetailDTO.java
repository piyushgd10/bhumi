/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 02-Feb-2015
 *  @author akshaykochhar
 */
package com.uniware.core.api.returns;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.uniware.core.api.saleorder.AddressDTO;

public class ReturnManifestItemDetailDTO {

    private String     shippingPackageCode;
    private String     shippingProviderCode;
    private String     trackingNumber;
    private String     shippingMethodCode;
    private AddressDTO shippingAddress;
    private BigDecimal actualWeight;
    private String     channel;
    private BigDecimal totalPrice;
    private Integer    noOfItems;
    private Integer    noOfBoxes;
    private String     statusCode;
    private Date       updated;
    private String     returnReason;
    private boolean    cashOnDelivery;
    private List<ReturnManifestLineItem> returnManifestLineItems = new ArrayList<ReturnManifestLineItem>();

    public String getShippingPackageCode() {
        return shippingPackageCode;
    }

    public void setShippingPackageCode(String shippingPackageCode) {
        this.shippingPackageCode = shippingPackageCode;
    }

    public String getShippingProviderCode() {
        return shippingProviderCode;
    }

    public void setShippingProviderCode(String shippingProviderCode) {
        this.shippingProviderCode = shippingProviderCode;
    }

    public String getTrackingNumber() {
        return trackingNumber;
    }

    public void setTrackingNumber(String trackingNumber) {
        this.trackingNumber = trackingNumber;
    }

    public String getShippingMethodCode() {
        return shippingMethodCode;
    }

    public void setShippingMethodCode(String shippingMethodCode) {
        this.shippingMethodCode = shippingMethodCode;
    }

    public AddressDTO getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(AddressDTO shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public BigDecimal getActualWeight() {
        return actualWeight;
    }

    public void setActualWeight(BigDecimal actualWeight) {
        this.actualWeight = actualWeight;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Integer getNoOfItems() {
        return noOfItems;
    }

    public void setNoOfItems(Integer noOfItems) {
        this.noOfItems = noOfItems;
    }

    public Integer getNoOfBoxes() {
        return noOfBoxes;
    }

    public void setNoOfBoxes(Integer noOfBoxes) {
        this.noOfBoxes = noOfBoxes;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public String getReturnReason() {
        return returnReason;
    }

    public void setReturnReason(String returnReason) {
        this.returnReason = returnReason;
    }

    public boolean isCashOnDelivery() {
        return cashOnDelivery;
    }

    public void setCashOnDelivery(boolean cashOnDelivery) {
        this.cashOnDelivery = cashOnDelivery;
    }

    public List<ReturnManifestLineItem> getReturnManifestLineItems() {
        return returnManifestLineItems;
    }

    public void setReturnManifestLineItems(List<ReturnManifestLineItem> returnManifestLineItems) {
        this.returnManifestLineItems = returnManifestLineItems;
    }

    public static class ReturnManifestLineItem {
        private String lineItemIdentifier;
        private String itemName;
        private String sellerSkuCode;
        private int    quantity;

        public String getLineItemIdentifier() {
            return lineItemIdentifier;
        }

        public void setLineItemIdentifier(String lineItemIdentifier) {
            this.lineItemIdentifier = lineItemIdentifier;
        }

        public String getItemName() {
            return itemName;
        }

        public void setItemName(String itemName) {
            this.itemName = itemName;
        }

        public int getQuantity() {
            return quantity;
        }

        public void setQuantity(int quantity) {
            this.quantity = quantity;
        }

        public String getSellerSkuCode() {
            return sellerSkuCode;
        }

        public void setSellerSkuCode(String sellerSkuCode) {
            this.sellerSkuCode = sellerSkuCode;
        }
    }

}

