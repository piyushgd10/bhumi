package com.uniware.core.api.invoice;

import com.unifier.core.api.base.ServiceResponse;

/**
 * Created by Sagar Sahni on 01/06/17.
 */
public class CreateShippingPackageReverseInvoiceResponse extends ServiceResponse {

    private String invoiceCode;
    private String invoiceDisplayCode;
    private String shippingPackageCode;

    public String getInvoiceDisplayCode() {
        return invoiceDisplayCode;
    }

    public void setInvoiceDisplayCode(String invoiceDisplayCode) {
        this.invoiceDisplayCode = invoiceDisplayCode;
    }

    public String getInvoiceCode() {
        return invoiceCode;
    }

    public void setInvoiceCode(String invoiceCode) {
        this.invoiceCode = invoiceCode;
    }

    public String getShippingPackageCode() {
        return shippingPackageCode;
    }

    public void setShippingPackageCode(String shippingPackageCode) {
        this.shippingPackageCode = shippingPackageCode;
    }
}
