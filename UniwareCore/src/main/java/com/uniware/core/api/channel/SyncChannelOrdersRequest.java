/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 03-Jun-2012
 *  @author vibhu
 */
package com.uniware.core.api.channel;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.hibernate.validator.constraints.NotEmpty;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author Sunny
 */
public class SyncChannelOrdersRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 1963404117545473225L;

    @NotEmpty
    private List<String>      channelCodes;

    private boolean           syncNewOrders = false;

    public SyncChannelOrdersRequest() {
        super();
    }

    public SyncChannelOrdersRequest(String channelCode) {
        List<String> codes = new ArrayList<String>();
        codes.add(channelCode);
        this.channelCodes = codes;
    }

    public SyncChannelOrdersRequest(List<String> channelCodes) {
        super();
        this.channelCodes = channelCodes;
    }

    public SyncChannelOrdersRequest(List<String> channelCodes, boolean syncNewOrders) {
        this.channelCodes = channelCodes;
        this.syncNewOrders = syncNewOrders;
    }

    public List<String> getChannelCodes() {
        return channelCodes;
    }

    public void setChannelCodes(List<String> channelCodes) {
        this.channelCodes = channelCodes;
    }

    public boolean isSyncNewOrders() {
        return syncNewOrders;
    }

    public void setSyncNewOrders(boolean syncNewOrders) {
        this.syncNewOrders = syncNewOrders;
    }

}
