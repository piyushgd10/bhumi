/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 22-Mar-2014
 *  @author karunsingla
 */
package com.uniware.core.api.inflow;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.unifier.core.api.base.ServiceResponse;
import com.unifier.core.api.customfields.CustomFieldMetadataDTO;

public class GetVendorInvoiceResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 7024325297239043978L;

    private VendorInvoiceDTO  vendorInvoice;

    public VendorInvoiceDTO getVendorInvoice() {
        return vendorInvoice;
    }

    public void setVendorInvoice(VendorInvoiceDTO vendorInvoice) {
        this.vendorInvoice = vendorInvoice;
    }

    public static class VendorInvoiceDTO {

        private String                           code;
        private String                           type;
        private String                           createdBy;
        private String                           purchaseOrderCode;
        private String                           vendorCode;
        private String                           vendorName;
        private String                           statusCode;
        private Date                             created;
        private String                           vendorInvoiceNumber;
        private Date                             vendorInvoiceDate;
        private String                           creditInvoiceCode;
        private final List<VendorInvoiceItemDTO> vendorInvoiceItems = new ArrayList<VendorInvoiceItemDTO>();
        private List<CustomFieldMetadataDTO>     customFieldValues;
        private List<String>                     inflowReceipts = new ArrayList<String>();
        private List<String>                     debitInvoiceCodes = new ArrayList<String>();

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getCreatedBy() {
            return createdBy;
        }

        public void setCreatedBy(String createdBy) {
            this.createdBy = createdBy;
        }

        public String getPurchaseOrderCode() {
            return purchaseOrderCode;
        }

        public void setPurchaseOrderCode(String purchaseOrderCode) {
            this.purchaseOrderCode = purchaseOrderCode;
        }

        public String getVendorCode() {
            return vendorCode;
        }

        public void setVendorCode(String vendorCode) {
            this.vendorCode = vendorCode;
        }

        public String getVendorName() {
            return vendorName;
        }

        public void setVendorName(String vendorName) {
            this.vendorName = vendorName;
        }

        public Date getCreated() {
            return created;
        }

        public void setCreated(Date created) {
            this.created = created;
        }

        public String getVendorInvoiceNumber() {
            return vendorInvoiceNumber;
        }

        public void setVendorInvoiceNumber(String vendorInvoiceNumber) {
            this.vendorInvoiceNumber = vendorInvoiceNumber;
        }

        public Date getVendorInvoiceDate() {
            return vendorInvoiceDate;
        }

        public void setVendorInvoiceDate(Date vendorInvoiceDate) {
            this.vendorInvoiceDate = vendorInvoiceDate;
        }

        public String getStatusCode() {
            return statusCode;
        }

        public void setStatusCode(String statusCode) {
            this.statusCode = statusCode;
        }

        public List<VendorInvoiceItemDTO> getVendorInvoiceItems() {
            return vendorInvoiceItems;
        }

        public void addVendorInvoiceItemDTO(VendorInvoiceItemDTO vendorInvoiceItemDTO) {
            this.vendorInvoiceItems.add(vendorInvoiceItemDTO);
        }
        
        public List<CustomFieldMetadataDTO> getCustomFieldValues() {
            return customFieldValues;
        }

        public void setCustomFieldValues(List<CustomFieldMetadataDTO> customFieldValues) {
            this.customFieldValues = customFieldValues;
        }

        public List<String> getInflowReceipts() {
            return inflowReceipts;
        }

        public void setInflowReceipts(List<String> inflowReceipts) {
            this.inflowReceipts = inflowReceipts;
        }

        public List<String> getDebitInvoiceCodes() {
            return debitInvoiceCodes;
        }

        public void setDebitInvoiceCodes(List<String> debitInvoiceCodes) {
            this.debitInvoiceCodes = debitInvoiceCodes;
        }

        public String getCreditInvoiceCode() {
            return creditInvoiceCode;
        }

        public void setCreditInvoiceCode(String creditInvoiceCode) {
            this.creditInvoiceCode = creditInvoiceCode;
        }
    }

    public static class VendorInvoiceItemDTO {

        private String     itemTypeSkuCode;
        private String     itemTypeName;
        private String     description;
        private BigDecimal unitPrice;
        private int        quantity;
        private BigDecimal subtotal;
        private BigDecimal vat;
        private BigDecimal cst;
        private BigDecimal integratedGst;
        private BigDecimal integratedGstPercentage;
        private BigDecimal unionTerritoryGst;
        private BigDecimal unionTerritoryGstPercentage;
        private BigDecimal stateGst;
        private BigDecimal stateGstPercentage;
        private BigDecimal centralGst;
        private BigDecimal centralGstPercentage;
        private BigDecimal compensationCess;
        private BigDecimal compensationCessPercentage;
        private BigDecimal total;
        private BigDecimal taxPercentage;
        private BigDecimal discount;

        public String getItemTypeSkuCode() {
            return itemTypeSkuCode;
        }

        public void setItemTypeSkuCode(String itemTypeSkuCode) {
            this.itemTypeSkuCode = itemTypeSkuCode;
        }

        public String getItemTypeName() {
            return itemTypeName;
        }

        public void setItemTypeName(String itemTypeName) {
            this.itemTypeName = itemTypeName;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public BigDecimal getUnitPrice() {
            return unitPrice;
        }

        public void setUnitPrice(BigDecimal unitPrice) {
            this.unitPrice = unitPrice;
        }

        public int getQuantity() {
            return quantity;
        }

        public void setQuantity(int quantity) {
            this.quantity = quantity;
        }

        public BigDecimal getSubtotal() {
            return subtotal;
        }

        public void setSubtotal(BigDecimal subtotal) {
            this.subtotal = subtotal;
        }

        public BigDecimal getVat() {
            return vat;
        }

        public void setVat(BigDecimal vat) {
            this.vat = vat;
        }

        public BigDecimal getCst() {
            return cst;
        }

        public void setCst(BigDecimal cst) {
            this.cst = cst;
        }

        public BigDecimal getTotal() {
            return total;
        }

        public void setTotal(BigDecimal total) {
            this.total = total;
        }

        public BigDecimal getTaxPercentage() {
            return taxPercentage;
        }

        public void setTaxPercentage(BigDecimal taxPercentage) {
            this.taxPercentage = taxPercentage;
        }

        public BigDecimal getDiscount() {
            return discount;
        }

        public void setDiscount(BigDecimal discount) {
            this.discount = discount;
        }

        public BigDecimal getIntegratedGst() {
            return integratedGst;
        }

        public void setIntegratedGst(BigDecimal integratedGst) {
            this.integratedGst = integratedGst;
        }

        public BigDecimal getIntegratedGstPercentage() {
            return integratedGstPercentage;
        }

        public void setIntegratedGstPercentage(BigDecimal integratedGstPercentage) {
            this.integratedGstPercentage = integratedGstPercentage;
        }

        public BigDecimal getUnionTerritoryGst() {
            return unionTerritoryGst;
        }

        public void setUnionTerritoryGst(BigDecimal unionTerritoryGst) {
            this.unionTerritoryGst = unionTerritoryGst;
        }

        public BigDecimal getUnionTerritoryGstPercentage() {
            return unionTerritoryGstPercentage;
        }

        public void setUnionTerritoryGstPercentage(BigDecimal unionTerritoryGstPercentage) {
            this.unionTerritoryGstPercentage = unionTerritoryGstPercentage;
        }

        public BigDecimal getStateGst() {
            return stateGst;
        }

        public void setStateGst(BigDecimal stateGst) {
            this.stateGst = stateGst;
        }

        public BigDecimal getStateGstPercentage() {
            return stateGstPercentage;
        }

        public void setStateGstPercentage(BigDecimal stateGstPercentage) {
            this.stateGstPercentage = stateGstPercentage;
        }

        public BigDecimal getCentralGst() {
            return centralGst;
        }

        public void setCentralGst(BigDecimal centralGst) {
            this.centralGst = centralGst;
        }

        public BigDecimal getCentralGstPercentage() {
            return centralGstPercentage;
        }

        public void setCentralGstPercentage(BigDecimal centralGstPercentage) {
            this.centralGstPercentage = centralGstPercentage;
        }

        public BigDecimal getCompensationCess() {
            return compensationCess;
        }

        public void setCompensationCess(BigDecimal compensationCess) {
            this.compensationCess = compensationCess;
        }

        public BigDecimal getCompensationCessPercentage() {
            return compensationCessPercentage;
        }

        public void setCompensationCessPercentage(BigDecimal compensationCessPercentage) {
            this.compensationCessPercentage = compensationCessPercentage;
        }
    }
}
