/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 01-Jun-2014
 *  @author vibhu
 */
package com.uniware.core.api.reversepickup;

import com.unifier.core.api.base.ServiceRequest;
import com.unifier.core.api.customfields.WsCustomFieldValue;

import java.util.List;

import javax.validation.Valid;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author vibhu
 */
public class EditReversePickupMetadataRequest extends ServiceRequest {

    /**
     *
     */
    private static final long        serialVersionUID = 1L;

    @NotEmpty
    private String                   reversePickupCode;

    @Valid
    private List<WsCustomFieldValue> customFieldValues;


    public String getReversePickupCode() {
        return reversePickupCode;
    }

    public void setReversePickupCode(String reversePickupCode) {
        this.reversePickupCode = reversePickupCode;
    }

    /**
     * @return the customFieldValues
     */
    public List<WsCustomFieldValue> getCustomFieldValues() {
        return customFieldValues;
    }

    /**
     * @param customFieldValues the customFieldValues to set
     */
    public void setCustomFieldValues(List<WsCustomFieldValue> customFieldValues) {

        this.customFieldValues = customFieldValues;
    }

}
