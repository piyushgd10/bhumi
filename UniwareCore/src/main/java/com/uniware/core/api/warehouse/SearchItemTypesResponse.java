/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 9, 2012
 *  @author praveeng
 */
package com.uniware.core.api.warehouse;

import com.unifier.core.api.base.ServiceResponse;

import java.util.ArrayList;
import java.util.List;

import com.uniware.core.api.catalog.dto.ItemTypeDTO;

/**
 * @author praveeng
 */
public class SearchItemTypesResponse extends ServiceResponse {

    /**
     * 
     */

    private static final long serialVersionUID = -4765673113302309584L;
    private Long              totalRecords;
    private List<ItemTypeDTO> elements         = new ArrayList<ItemTypeDTO>();

    /**
     * @return the totalRecords
     */
    public Long getTotalRecords() {
        return totalRecords;
    }

    /**
     * @param totalRecords the totalRecords to set
     */
    public void setTotalRecords(Long totalRecords) {
        this.totalRecords = totalRecords;
    }

    /**
     * @return the elements
     */
    public List<ItemTypeDTO> getElements() {
        return elements;
    }

    /**
     * @param elements the elements to set
     */
    public void setElements(List<ItemTypeDTO> elements) {
        this.elements = elements;
    }

}
