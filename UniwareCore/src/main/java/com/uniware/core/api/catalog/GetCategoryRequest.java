/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 10, 2012
 *  @author praveeng
 */
package com.uniware.core.api.catalog;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author praveeng
 */
public class GetCategoryRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 711227543578093324L;
    private String            sectionCode;

    /**
     * @return the sectionCode
     */
    public String getSectionCode() {
        return sectionCode;
    }

    /**
     * @param sectionCode the sectionCode to set
     */
    public void setSectionCode(String sectionCode) {
        this.sectionCode = sectionCode;
    }

}
