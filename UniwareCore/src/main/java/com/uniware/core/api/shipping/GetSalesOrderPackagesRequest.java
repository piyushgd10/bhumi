/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jan 1, 2013
 *  @author praveeng
 */
package com.uniware.core.api.shipping;

import com.unifier.core.api.base.ServiceRequest;

import org.hibernate.validator.constraints.NotBlank;

/**
 * @author praveeng
 */
public class GetSalesOrderPackagesRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -4611912682299953808L;

    @NotBlank
    private String            saleOrderCode;

    /**
     * @return the saleOrderCode
     */
    public String getSaleOrderCode() {
        return saleOrderCode;
    }

    /**
     * @param saleOrderCode the saleOrderCode to set
     */
    public void setSaleOrderCode(String saleOrderCode) {
        this.saleOrderCode = saleOrderCode;
    }

}
