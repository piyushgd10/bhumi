/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 29, 2012
 *  @author singla
 */
package com.uniware.core.api.purchase;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.unifier.core.api.base.ServiceRequest;
import com.unifier.core.api.customfields.WsCustomFieldValue;
import com.uniware.core.entity.PurchaseOrder;

/**
 * @author singla
 */
public class CreatePurchaseOrderRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long         serialVersionUID = -8602029012770397959L;

    private String                    purchaseOrderCode;

    private String                    type             = PurchaseOrder.Type.MANUAL.name();

    @NotNull
    private String                    vendorCode;

    private String                    vendorAgreementName;

    private String                    currencyCode;

    @NotNull
    private Integer                   userId;

    private Date                      expiryDate;

    private Date                      deliveryDate;

    private String                    logisticChargesDivisionMethod;

    private BigDecimal                logisticCharges  = BigDecimal.ZERO;

    @Valid
    private List<WsPurchaseOrderItem> purchaseOrderItems;

    @Valid
    private List<WsCustomFieldValue>  customFieldValues;

    /**
     * @return the purchaseOrderCode
     */
    public String getPurchaseOrderCode() {
        return purchaseOrderCode;
    }

    /**
     * @param purchaseOrderCode the purchaseOrderCode to set
     */
    public void setPurchaseOrderCode(String purchaseOrderCode) {
        this.purchaseOrderCode = purchaseOrderCode;
    }

    /**
     * @return the vendorCode
     */
    public String getVendorCode() {
        return vendorCode;
    }

    /**
     * @param vendorCode the vendorCode to set
     */
    public void setVendorCode(String vendorCode) {
        this.vendorCode = vendorCode;
    }

    /**
     * @return the vendorAgreementId
     */
    public String getVendorAgreementName() {
        return vendorAgreementName;
    }

    /**
     * @param vendorAgreementId the vendorAgreementId to set
     */
    public void setVendorAgreementName(String vendorAgreementName) {
        this.vendorAgreementName = vendorAgreementName;
    }

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    public String getLogisticChargesDivisionMethod() {
        return logisticChargesDivisionMethod;
    }

    public void setLogisticChargesDivisionMethod(String logisticChargesDivisionMethod) {
        this.logisticChargesDivisionMethod = logisticChargesDivisionMethod;
    }

    public BigDecimal getLogisticCharges() {
        return logisticCharges;
    }

    public void setLogisticCharges(BigDecimal logisticCharges) {
        this.logisticCharges = logisticCharges;
    }

    public List<WsPurchaseOrderItem> getPurchaseOrderItems() {
        return purchaseOrderItems;
    }

    public void setPurchaseOrderItems(List<WsPurchaseOrderItem> purchaseOrderItems) {
        this.purchaseOrderItems = purchaseOrderItems;
    }

    /**
     * @return the customFields
     */
    public List<WsCustomFieldValue> getCustomFieldValues() {
        return customFieldValues;
    }

    /**
     * @param customFields the customFields to set
     */
    public void setCustomFieldValues(List<WsCustomFieldValue> customFieldValues) {
        this.customFieldValues = customFieldValues;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    public Date getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(Date deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

}
