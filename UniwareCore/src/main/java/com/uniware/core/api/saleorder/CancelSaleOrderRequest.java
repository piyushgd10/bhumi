/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 14, 2012
 *  @author singla
 */
package com.uniware.core.api.saleorder;

import java.util.List;

import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author singla
 */
public class CancelSaleOrderRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -3060206168358655120L;

    @NotBlank
    private String            saleOrderCode;

    private List<String>      saleOrderItemCodes;
    private boolean           cancelPartially;

    private boolean           cancelOnChannel  = true;

    @Size(max = 100)
    private String            cancellationReason;

    /**
     * @return the saleOrderCode
     */
    public String getSaleOrderCode() {
        return saleOrderCode;
    }

    /**
     * @param saleOrderCode the saleOrderCode to set
     */
    public void setSaleOrderCode(String saleOrderCode) {
        this.saleOrderCode = saleOrderCode;
    }

    /**
     * @return the saleOrderItemCodes
     */
    public List<String> getSaleOrderItemCodes() {
        return saleOrderItemCodes;
    }

    /**
     * @param saleOrderItemCodes the saleOrderItemCodes to set
     */
    public void setSaleOrderItemCodes(List<String> saleOrderItemCodes) {
        this.saleOrderItemCodes = saleOrderItemCodes;
    }

    /**
     * @return the cancelPartially
     */
    public boolean isCancelPartially() {
        return cancelPartially;
    }

    /**
     * @param cancelPartially the cancelPartially to set
     */
    public void setCancelPartially(boolean cancelPartially) {
        this.cancelPartially = cancelPartially;
    }

    /**
     * @return the cancellationReason
     */
    public String getCancellationReason() {
        return cancellationReason;
    }

    /**
     * @param cancellationReason the cancellationReason to set
     */
    public void setCancellationReason(String cancellationReason) {
        this.cancellationReason = cancellationReason;
    }

    public boolean isCancelOnChannel() {
        return cancelOnChannel;
    }

    public void setCancelOnChannel(boolean cancelOnChannel) {
        this.cancelOnChannel = cancelOnChannel;
    }

}
