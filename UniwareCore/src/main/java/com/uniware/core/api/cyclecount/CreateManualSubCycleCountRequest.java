package com.uniware.core.api.cyclecount;

import java.util.Date;
import java.util.Set;

import javax.validation.constraints.Future;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import com.unifier.core.api.base.ServiceRequest;

/**
 * Created by harshpal on 2/26/16.
 */
public class CreateManualSubCycleCountRequest extends ServiceRequest {

    @NotBlank
    private String cycleCountCode;

    @NotEmpty
    Set<String>    shelfCodes;

    @NotNull
    @Future
    private Date   targetCompletionDate;

    public String getCycleCountCode() {
        return cycleCountCode;
    }

    public void setCycleCountCode(String cycleCountCode) {
        this.cycleCountCode = cycleCountCode;
    }

    public Set<String> getShelfCodes() {
        return shelfCodes;
    }

    public void setShelfCodes(Set<String> shelfCodes) {
        this.shelfCodes = shelfCodes;
    }

    public Date getTargetCompletionDate() {
        return targetCompletionDate;
    }

    public void setTargetCompletionDate(Date targetCompletionDate) {
        this.targetCompletionDate = targetCompletionDate;
    }

}
