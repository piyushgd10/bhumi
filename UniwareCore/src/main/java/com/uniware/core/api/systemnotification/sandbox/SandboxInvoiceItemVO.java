/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 25-Dec-2013
 *  @author sunny
 */
package com.uniware.core.api.systemnotification.sandbox;

import java.math.BigDecimal;

import com.uniware.core.entity.InvoiceItem;

public class SandboxInvoiceItemVO {

    private BigDecimal vat = BigDecimal.ZERO;
    private BigDecimal cst = BigDecimal.ZERO;
    private BigDecimal integratedGst     = BigDecimal.ZERO;
    private BigDecimal unionTerritoryGst = BigDecimal.ZERO;
    private BigDecimal stateGst          = BigDecimal.ZERO;
    private BigDecimal centralGst        = BigDecimal.ZERO;
    private BigDecimal compensationCess  = BigDecimal.ZERO;
    private BigDecimal additionalTax     = BigDecimal.ZERO;

    public SandboxInvoiceItemVO() {
    }

    public SandboxInvoiceItemVO(InvoiceItem invoiceItem) {
//        this.vat = invoiceItem.getVat();
//        this.cst = invoiceItem.getCst();
//        this.additionalTax = invoiceItem.getAdditionalTax();
//        this.integratedGst = invoiceItem.getIntegratedGst();
//        this.centralGst = invoiceItem.getCentralGst();
//        this.stateGst = invoiceItem.getStateGst();
//        this.unionTerritoryGst = invoiceItem.getUnionTerritoryGst();
//        this.compensationCess = invoiceItem.getCompensationCess();

    }

    public BigDecimal getVat() {
        return vat;
    }

    public void setVat(BigDecimal vat) {
        this.vat = vat;
    }

    public BigDecimal getCst() {
        return cst;
    }

    public void setCst(BigDecimal cst) {
        this.cst = cst;
    }

}
