/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 10-Oct-2013
 *  @author parijat
 */
package com.uniware.core.api.purchase;

import com.unifier.core.api.base.ServiceResponse;

public class DeleteReorderConfigResponse extends ServiceResponse{

    /**
     * 
     */
    private static final long serialVersionUID = -437289379287389217L;

}
