/**
 * Copyright 2017 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version 1.0, 06/09/17
 * @author aditya
 */
package com.uniware.core.api.warehouse;

import com.unifier.core.api.base.ServiceRequest;
import javax.validation.constraints.NotNull;

public class UpdateShelfRequest extends ServiceRequest{
    @NotNull
    private String  shelfCode;

    @NotNull
    private String  shelfTypeCode;

    private Boolean enabled;

    public String getShelfCode() {
        return shelfCode;
    }

    public void setShelfCode(String shelfCode) {
        this.shelfCode = shelfCode;
    }

    public String getShelfTypeCode() {
        return shelfTypeCode;
    }

    public void setShelfTypeCode(String shelfTypeCode) {
        this.shelfTypeCode = shelfTypeCode;
    }

    public Boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}
