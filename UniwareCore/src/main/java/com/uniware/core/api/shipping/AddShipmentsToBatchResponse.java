/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 04-Apr-2014
 *  @author karunsingla
 */
package com.uniware.core.api.shipping;

import com.unifier.core.api.base.ServiceResponse;

public class AddShipmentsToBatchResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -6425828723241635015L;

    private String            shipmentBatchCode;

    public String getShipmentBatchCode() {
        return shipmentBatchCode;
    }

    public void setShipmentBatchCode(String shipmentBatchCode) {
        this.shipmentBatchCode = shipmentBatchCode;
    }

}
