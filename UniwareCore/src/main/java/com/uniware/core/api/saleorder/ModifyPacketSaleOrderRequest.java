/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 14, 2012
 *  @author singla
 */
package com.uniware.core.api.saleorder;

import com.unifier.core.api.base.ServiceRequest;

import java.util.List;

/**
 * @author singla
 */
public class ModifyPacketSaleOrderRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -3060206168358655120L;

    private String            saleOrderCode;
    private List<String>      saleOrderItemCodes;

    /**
     * @return the saleOrderCode
     */
    public String getSaleOrderCode() {
        return saleOrderCode;
    }

    /**
     * @param saleOrderCode the saleOrderCode to set
     */
    public void setSaleOrderCode(String saleOrderCode) {
        this.saleOrderCode = saleOrderCode;
    }

    /**
     * @return the saleOrderItemCodes
     */
    public List<String> getSaleOrderItemCodes() {
        return saleOrderItemCodes;
    }

    /**
     * @param saleOrderItemCodes the saleOrderItemCodes to set
     */
    public void setSaleOrderItemCodes(List<String> saleOrderItemCodes) {
        this.saleOrderItemCodes = saleOrderItemCodes;
    }

}