/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jun 20, 2012
 *  @author ankit
 */
package com.uniware.core.api.facility;

import com.unifier.core.api.base.ServiceResponse;
import com.uniware.core.api.warehouse.FacilityDTO;

/**
 * @author Sunny
 */
public class CreateFacilityResponse extends ServiceResponse {

    private static final long serialVersionUID = -5342490352978264760L;

    private FacilityDTO       facilityDTO;

    public FacilityDTO getFacilityDTO() {
        return facilityDTO;
    }

    public void setFacilityDTO(FacilityDTO facilityDTO) {
        this.facilityDTO = facilityDTO;
    }

}
