/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 9, 2012
 *  @author singla
 */
package com.uniware.core.api.shipping;

import com.unifier.core.api.base.ServiceRequest;

import javax.validation.constraints.NotNull;

/**
 * @author singla
 */
public class GetShippingManifestRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 7319049617938044859L;

    @NotNull
    private String            shippingManifestCode;

    /**
     * 
     */
    public GetShippingManifestRequest() {
    }

    /**
     * @param shippingManifestId
     */
    public GetShippingManifestRequest(String shippingManifestCode) {
        this.shippingManifestCode = shippingManifestCode;
    }

    /**
     * @return the shippingManifestCode
     */
    public String getShippingManifestCode() {
        return shippingManifestCode;
    }

    /**
     * @param shippingManifestCode the shippingManifestCode to set
     */
    public void setShippingManifestCode(String shippingManifestCode) {
        this.shippingManifestCode = shippingManifestCode;
    }

}
