/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Nov 25, 2015
 *  @author akshay
 */
package com.uniware.core.api.recommendation;

import com.unifier.core.api.base.ServiceRequest;

public class GetSubscribedRecommendationProvidersRequest extends ServiceRequest {

    private static final long serialVersionUID = 8512923830665199710L;
    
    private String recommendationProvider;
    
    public GetSubscribedRecommendationProvidersRequest() {}
    
    public GetSubscribedRecommendationProvidersRequest(String recommendationProvider) {
        this.recommendationProvider = recommendationProvider;
    }

    public String getRecommendationProvider() {
        return recommendationProvider;
    }

    public void setRecommendationProvider(String recommendationProvider) {
        this.recommendationProvider = recommendationProvider;
    }
}
