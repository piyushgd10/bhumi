/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 30-Jun-2013
 *  @author unicom
 */
package com.uniware.core.api.packer;

import java.util.List;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import com.unifier.core.api.base.ServiceRequest;

public class BulkAllocateItemRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 234242424L;

    @NotEmpty
    private List<String>      itemCodes;

    @NotBlank
    private String            shippingPackageCode;

    /**
     * @return the itemCodes
     */
    public List<String> getItemCodes() {
        return itemCodes;
    }

    /**
     * @param itemCodes the itemCodes to set
     */
    public void setItemCodes(List<String> itemCodes) {
        this.itemCodes = itemCodes;
    }

    /**
     * @return the shippingPackageCode
     */
    public String getShippingPackageCode() {
        return shippingPackageCode;
    }

    /**
     * @param shippingPackageCode the shippingPackageCode to set
     */
    public void setShippingPackageCode(String shippingPackageCode) {
        this.shippingPackageCode = shippingPackageCode;
    }

}
