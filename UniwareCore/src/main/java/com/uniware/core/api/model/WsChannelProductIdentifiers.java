/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 02-Dec-2014
 *  @author akshaykochhar
 */
package com.uniware.core.api.model;

public class WsChannelProductIdentifiers {
    
    private String channelSkuCode;
    
    private String channelProductId;
    
    public WsChannelProductIdentifiers() {
    }
    
    public WsChannelProductIdentifiers(String channelSkuCode, String channelProductId) {
        this.channelSkuCode = channelSkuCode;
        this.channelProductId = channelProductId;
    }
    

    public String getChannelSkuCode() {
        return channelSkuCode;
    }

    public void setChannelSkuCode(String channelSkuCode) {
        this.channelSkuCode = channelSkuCode;
    }

    public String getChannelProductId() {
        return channelProductId;
    }

    public void setChannelProductId(String channelProductId) {
        this.channelProductId = channelProductId;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((channelProductId == null) ? 0 : channelProductId.hashCode());
        result = prime * result + ((channelSkuCode == null) ? 0 : channelSkuCode.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        WsChannelProductIdentifiers other = (WsChannelProductIdentifiers) obj;
        if (channelProductId == null) {
            if (other.channelProductId != null)
                return false;
        } else if (!channelProductId.equals(other.channelProductId))
            return false;
        if (channelSkuCode == null) {
            if (other.channelSkuCode != null)
                return false;
        } else if (!channelSkuCode.equals(other.channelSkuCode))
            return false;
        return true;
    }

}
