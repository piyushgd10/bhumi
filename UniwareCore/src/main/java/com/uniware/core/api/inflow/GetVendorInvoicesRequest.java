/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 *  @version     1.0, Feb 29, 2012
 *  @author singla
 */
package com.uniware.core.api.inflow;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

public class GetVendorInvoicesRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -3771389465564119281L;
    
    @NotBlank
    private String purchaseOrderCode;

    public String getPurchaseOrderCode() {
        return purchaseOrderCode;
    }

    public void setPurchaseOrderCode(String purchaseOrderCode) {
        this.purchaseOrderCode = purchaseOrderCode;
    }
    
}
