/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 24, 2011
 *  @author ankitp
 */
package com.uniware.core.api.warehouse;

import java.math.BigDecimal;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author ankitp
 */
public class CreateShippingPackageTypeRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -1467452867569855702L;

    @NotEmpty
    private String            code;
    @NotNull
    private Integer           boxLength;
    @NotNull
    private Integer           boxWidth;
    @NotNull
    private Integer           boxHeight;
    @NotNull
    private Integer           boxWeight;
    @NotNull
    private BigDecimal        packingCost;

    public CreateShippingPackageTypeRequest() {
    }

    public CreateShippingPackageTypeRequest(String code, Integer boxLength, Integer boxWidth, Integer boxHeight, Integer boxWeight, BigDecimal packingCost) {
        this.code = code;
        this.boxLength = boxLength;
        this.boxWidth = boxWidth;
        this.boxHeight = boxHeight;
        this.boxWeight = boxWeight;
        this.packingCost = packingCost;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the boxLength
     */
    public Integer getBoxLength() {
        return boxLength;
    }

    /**
     * @param boxLength the boxLength to set
     */
    public void setBoxLength(Integer boxLength) {
        this.boxLength = boxLength;
    }

    /**
     * @return the boxWidth
     */
    public Integer getBoxWidth() {
        return boxWidth;
    }

    /**
     * @param boxWidth the boxWidth to set
     */
    public void setBoxWidth(Integer boxWidth) {
        this.boxWidth = boxWidth;
    }

    /**
     * @return the boxHeight
     */
    public Integer getBoxHeight() {
        return boxHeight;
    }

    /**
     * @param boxHeight the boxHeight to set
     */
    public void setBoxHeight(Integer boxHeight) {
        this.boxHeight = boxHeight;
    }

    /**
     * @return the boxWeight
     */
    public Integer getBoxWeight() {
        return boxWeight;
    }

    /**
     * @param boxWeight the boxWeight to set
     */
    public void setBoxWeight(Integer boxWeight) {
        this.boxWeight = boxWeight;
    }

    /**
     * @return the packingCost
     */
    public BigDecimal getPackingCost() {
        return packingCost;
    }

    /**
     * @param packingCost the packingCost to set
     */
    public void setPackingCost(BigDecimal packingCost) {
        this.packingCost = packingCost;
    }

}