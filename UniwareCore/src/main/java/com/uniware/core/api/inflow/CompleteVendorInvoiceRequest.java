/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 25-Mar-2014
 *  @author karunsingla
 */
package com.uniware.core.api.inflow;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

public class CompleteVendorInvoiceRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -4898253422826517879L;

    @NotBlank
    private String            vendorInvoiceCode;

    public String getVendorInvoiceCode() {
        return vendorInvoiceCode;
    }

    public void setVendorInvoiceCode(String vendorInvoiceCode) {
        this.vendorInvoiceCode = vendorInvoiceCode;
    }
}
