/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jan 10, 2012
 *  @author singla
 */
package com.uniware.core.api.saleorder;

import java.util.List;

import javax.validation.constraints.NotNull;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author singla
 */
public class GetSaleOrderRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 7646307772810377253L;

    @NotNull
    private String            code;

    private List<String>      facilityCodes;

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the facilityCodes
     */
    public List<String> getFacilityCodes() {
        return facilityCodes;
    }

    /**
     * @param facilityCodes the facilityCodes to set
     */
    public void setFacilityCodes(List<String> facilityCodes) {
        this.facilityCodes = facilityCodes;
    }

}
