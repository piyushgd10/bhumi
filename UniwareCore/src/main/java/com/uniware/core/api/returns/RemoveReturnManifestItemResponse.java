/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 03-Feb-2015
 *  @author akshaykochhar
 */
package com.uniware.core.api.returns;

import com.unifier.core.api.base.ServiceResponse;

public class RemoveReturnManifestItemResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -6095261375075926881L;

}
