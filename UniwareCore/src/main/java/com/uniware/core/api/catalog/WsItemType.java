/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 24-Dec-2011
 *  @author vibhu
 */
package com.uniware.core.api.catalog;

import java.math.BigDecimal;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.annotation.constraints.OptionalBlank;
import com.unifier.core.api.customfields.WsCustomFieldValue;
import com.unifier.core.utils.StringUtils;
import com.uniware.core.api.bundle.WsComponentItemType;
import com.uniware.core.entity.ItemType;
import com.uniware.core.entity.ItemType.Type;

/**
 * @author praveeng
 */
public class WsItemType {

    @OptionalBlank
    private String                    categoryCode;

    @NotBlank
    @Pattern(regexp = "^[a-zA-Z0-9-_\\.]+$", message = "Sku Code can only contain alphanumeric, _ and -.")
    @Length(min = 3)
    private String                    skuCode;

    @OptionalBlank
    @Length(max = 200)
    private String                    name;

    private Type                      type = Type.SIMPLE;

    private String                    description;
    private String                    scanIdentifier;
    private Integer                   length;
    private Integer                   width;
    private Integer                   height;

    private Integer                   weight;

    private Integer                   minOrderSize;

    private String                    color;
    private String                    size;
    private String                    brand;

    private String                    ean;
    private String                    upc;
    private String                    isbn;

    private BigDecimal                maxRetailPrice;
    private BigDecimal                basePrice;
    private BigDecimal                costPrice;
    private String                    taxTypeCode;
    private String                    gstTaxTypeCode;
    private String                    hsnCode;

    @Length(max = 255)
    private String                    imageUrl;

    @Length(max = 255)
    private String                    productPageUrl;

    private String                    features;

    private Integer                   tat;

    private List<String>              tags;

    private String                    itemDetailFieldsText;

    private Boolean                   requiresCustomization;

    private Integer                  shelfLife;

    private Boolean                  expirable;

    private Boolean                   enabled;

    @Valid
    private List<WsComponentItemType> componentItemTypes;

    @Valid
    private List<WsCustomFieldValue>  customFieldValues;

    public WsItemType(ItemType itemType) {
        this.categoryCode = itemType.getCategory().getCode();
        this.skuCode = itemType.getSkuCode();
        this.name = itemType.getName();
        this.type = itemType.getType();
        this.description = itemType.getDescription();
        this.length = itemType.getLength();
        this.width = itemType.getWidth();
        this.height = itemType.getHeight();
        this.weight = itemType.getWeight();
        this.taxTypeCode = itemType.getTaxType() != null ? itemType.getTaxType().getCode() : null;
        this.ean = itemType.getEan();
        this.upc = itemType.getUpc();
        this.isbn = itemType.getIsbn();
        this.maxRetailPrice = itemType.getMaxRetailPrice();
        this.imageUrl = itemType.getImageUrl();
        this.productPageUrl = itemType.getProductPageUrl();
        this.features = itemType.getFeatures();
        this.tags = itemType.getTagList();
        this.color = itemType.getColor();
        this.size = itemType.getSize();
        this.brand = itemType.getBrand();
        this.requiresCustomization = itemType.isRequiresCustomization();
        this.enabled = itemType.isEnabled();
        this.scanIdentifier = itemType.getScanIdentifier();
        this.costPrice = itemType.getCostPrice();
        this.tat = itemType.getTat();
    }

    public WsItemType() {
    }

    /**
     * @return the categoryCode
     */
    public String getCategoryCode() {
        return categoryCode;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return StringUtils.trimString(description);
    }

    /**
     * @return the height
     */
    public Integer getHeight() {
        return height;
    }

    /**
     * @return the length
     */
    public Integer getLength() {
        return length;
    }

    /**
     * @return the name
     */
    public String getName() {
        return StringUtils.trimString(name);
    }

    /**
     * @return the skuCode
     */
    public String getSkuCode() {
        return skuCode;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    /**
     * @return the weight
     */
    public Integer getWeight() {
        return weight;
    }

    /**
     * @return the width
     */
    public Integer getWidth() {
        return width;
    }

    /**
     * @param categoryCode the categoryCode to set
     */
    public void setCategoryCode(String categoryCode) {
        this.categoryCode = categoryCode;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the scanIdentifier
     */
    public String getScanIdentifier() {
        return StringUtils.trimString(scanIdentifier);
    }

    /**
     * @param scanIdentifier the scanIdentifier to set
     */
    public void setScanIdentifier(String scanIdentifier) {
        this.scanIdentifier = scanIdentifier;
    }

    /**
     * @param height the height to set
     */
    public void setHeight(Integer height) {
        this.height = height;
    }

    /**
     * @param length the length to set
     */
    public void setLength(Integer length) {
        this.length = length;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @param skuCode the skuCode to set
     */
    public void setSkuCode(String skuCode) {
        this.skuCode = skuCode;
    }

    /**
     * @param weight the weight to set
     */
    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    /**
     * @param width the width to set
     */
    public void setWidth(Integer width) {
        this.width = width;
    }

    /**
     * @return the tags
     */
    public List<String> getTags() {
        return tags;
    }

    /**
     * @param tags the tags to set
     */
    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    /**
     * @return the imageUrl
     */
    public String getImageUrl() {
        return StringUtils.trimString(imageUrl);
    }

    /**
     * @param imageUrl the imageUrl to set
     */
    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    /**
     * @return the productPageUrl
     */
    public String getProductPageUrl() {
        return StringUtils.trimString(productPageUrl);
    }

    /**
     * @param productPageUrl the productPageUrl to set
     */
    public void setProductPageUrl(String productPageUrl) {
        this.productPageUrl = productPageUrl;
    }

    /**
     * @return the customFieldsText
     */
    public String getItemDetailFieldsText() {
        return StringUtils.trimString(itemDetailFieldsText);
    }

    /**
     * @param itemDetailFieldsText the customFieldsText to set
     */
    public void setItemDetailFieldsText(String itemDetailFieldsText) {
        this.itemDetailFieldsText = itemDetailFieldsText;
    }

    /**
     * @return the features
     */
    public String getFeatures() {
        return StringUtils.trimString(features);
    }

    /**
     * @param features the features to set
     */
    public void setFeatures(String features) {
        this.features = features;
    }

    /**
     * @return ean
     */
    public String getEan() {
        return ean;
    }

    public void setEan(String ean) {
        this.ean = ean;
    }

    /**
     * @return upc
     */
    public String getUpc() {
        return upc;
    }

    public void setUpc(String upc) {
        this.upc = upc;
    }

    /**
     * @return isbn
     */
    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    /* (non-Javadoc)
         * @see java.lang.Object#toString()
         */
    @Override
    public String toString() {
        return "WsItemType [categoryCode=" + categoryCode + ", skuCode=" + skuCode + ", name=" + name + ", description=" + description + ", length=" + length + ", width=" + width
                + ", height=" + height + ", weight=" + weight + ", imageUrl=" + imageUrl + ", productPageUrl=" + productPageUrl + ", features=" + features + ", tags=" + tags
                + ", customFieldsText=" + itemDetailFieldsText + ", ean=" + ean + ", upc=" + upc + ", isbn=" + isbn + "]";
    }

    /**
     * @return the maxRetailPrice
     */
    public BigDecimal getMaxRetailPrice() {
        return maxRetailPrice;
    }

    /**
     * @param maxRetailPrice the maxRetailPrice to set
     */
    public void setMaxRetailPrice(BigDecimal maxRetailPrice) {
        this.maxRetailPrice = maxRetailPrice;
    }

    public BigDecimal getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(BigDecimal basePrice) {
        this.basePrice = basePrice;
    }

    public BigDecimal getCostPrice() {
        return costPrice;
    }

    public void setCostPrice(BigDecimal costPrice) {
        this.costPrice = costPrice;
    }

    /**
     * @return the taxTypeCode
     */
    public String getTaxTypeCode() {
        return taxTypeCode;
    }

    /**
     * @param taxTypeCode the taxTypeCode to set
     */
    public void setTaxTypeCode(String taxTypeCode) {
        this.taxTypeCode = taxTypeCode;
    }

    public String getGstTaxTypeCode() {
        return gstTaxTypeCode;
    }

    public void setGstTaxTypeCode(String gstTaxTypeCode) {
        this.gstTaxTypeCode = gstTaxTypeCode;
    }

    public String getHsnCode() {
        return hsnCode;
    }

    public void setHsnCode(String hsnCode) {
        this.hsnCode = hsnCode;
    }

    /**
     * @return the customFieldValues
     */
    public List<WsCustomFieldValue> getCustomFieldValues() {
        return customFieldValues;
    }

    /**
     * @param customFieldValues the customFieldValues to set
     */
    public void setCustomFieldValues(List<WsCustomFieldValue> customFieldValues) {
        this.customFieldValues = customFieldValues;
    }

    /**
     * @return the minOrderSize
     */
    public Integer getMinOrderSize() {
        return minOrderSize;
    }

    /**
     * @param minOrderSize the minOrderSize to set
     */
    public void setMinOrderSize(Integer minOrderSize) {
        this.minOrderSize = minOrderSize;
    }

    /**
     * @return the color
     */
    public String getColor() {
        return StringUtils.trimString(color);
    }

    /**
     * @param color the color to set
     */
    public void setColor(String color) {
        this.color = color;
    }

    /**
     * @return the size
     */
    public String getSize() {
        return StringUtils.trimString(size);
    }

    /**
     * @param size the size to set
     */
    public void setSize(String size) {
        this.size = size;
    }

    /**
     * @return the brand
     */
    public String getBrand() {
        return StringUtils.trimString(brand);
    }

    /**
     * @param brand the brand to set
     */
    public void setBrand(String brand) {
        this.brand = brand;
    }

    /**
     * @return the requiresCustomization
     */
    public Boolean getRequiresCustomization() {
        return requiresCustomization;
    }

    /**
     * @param requiresCustomization the requiresCustomization to set
     */
    public void setRequiresCustomization(Boolean requiresCustomization) {
        this.requiresCustomization = requiresCustomization;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public List<WsComponentItemType> getComponentItemTypes() {
        return componentItemTypes;
    }

    public void setComponentItemTypes(List<WsComponentItemType> componentItemTypes) {
        this.componentItemTypes = componentItemTypes;
    }

    public Integer getTat() {
        return tat;
    }

    public void setTat(Integer tat) {
        this.tat = tat;
    }

    public Integer getShelfLife() {
        return shelfLife;
    }

    public void setShelfLife(Integer shelfLife) {
        this.shelfLife = shelfLife;
    }

    public Boolean getExpirable() {
        return expirable;
    }

    public void setExpirable(Boolean expirable) {
        this.expirable = expirable;
    }
}
