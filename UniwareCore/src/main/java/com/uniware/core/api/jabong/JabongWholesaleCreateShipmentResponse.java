/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 10-Dec-2013
 *  @author karunsingla
 */
package com.uniware.core.api.jabong;

import com.unifier.core.api.base.ServiceResponse;

public class JabongWholesaleCreateShipmentResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 6505564568626682562L;

    private String            shipmentCode;

    public String getShipmentCode() {
        return shipmentCode;
    }

    public void setShipmentCode(String shipmentCode) {
        this.shipmentCode = shipmentCode;
    }

}
