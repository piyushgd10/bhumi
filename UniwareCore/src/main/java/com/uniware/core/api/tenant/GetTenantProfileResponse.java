/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 23-Jun-2015
 *  @author parijat
 */
package com.uniware.core.api.tenant;

import com.unifier.core.api.base.ServiceResponse;
import com.unifier.services.vo.TenantProfileVO.Status;
import com.unifier.services.vo.TenantProfileVO.Type;

import java.util.List;

public class GetTenantProfileResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 6077391198681711583L;

    private TenantProfileDTO  profile;

    private List<FeatureDTO> features;

    private String tenantStatus;

    public TenantProfileDTO getProfile() {
        return profile;
    }

    public void setProfile(TenantProfileDTO profile) {
        this.profile = profile;
    }

    public List<FeatureDTO> getFeatures() {
        return features;
    }

    public void setFeatures(List<FeatureDTO> features) {
        this.features = features;
    }

    public String getTenantStatus() {
        return tenantStatus;
    }

    public void setTenantStatus(String tenantStatus) {
        this.tenantStatus = tenantStatus;
    }

    public class TenantProfileDTO {

        private String tenantCode;

        private String accessUrl;

        private Type tenantType;

        private String mobile;

        private Status statusCode;

        private String currentVersion;

        private String accountCode;

        public TenantProfileDTO(String tenantCode, String accessUrl, Type tenantType, String mobile, Status statusCode, String currentVersion, String accountCode) {
            this.tenantCode = tenantCode;
            this.accessUrl = accessUrl;
            this.tenantType = tenantType;
            this.mobile = mobile;
            this.statusCode = statusCode;
            this.currentVersion = currentVersion;
            this.accountCode = accountCode;
        }

        public String getTenantCode() {
            return tenantCode;
        }

        public String getAccessUrl() {
            return accessUrl;
        }

        public Type getTenantType() {
            return tenantType;
        }

        public String getMobile() {
            return mobile;
        }

        public Status getStatusCode() {
            return statusCode;
        }

        public String getCurrentVersion() {
            return currentVersion;
        }

        public String getAccountCode() {
            return accountCode;
        }

        public void setAccountCode(String accountCode) {
            this.accountCode = accountCode;
        }
    }

    public class FeatureDTO {

        public FeatureDTO() {
        }

        public FeatureDTO(String code, String name) {
            this.code = code;
            this.name = name;
        }

        private String code;
        private String name;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

    }

}
