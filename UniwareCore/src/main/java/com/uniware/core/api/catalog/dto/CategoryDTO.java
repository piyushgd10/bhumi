/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 10, 2012
 *  @author praveeng
 */
package com.uniware.core.api.catalog.dto;

import com.unifier.core.configuration.ConfigurationManager;
import com.uniware.core.configuration.TaxConfiguration;
import com.uniware.core.entity.Category;

/**
 * @author praveeng
 */
public class CategoryDTO {
    private int     id;
    private String  code;
    private String  name;
    private String  taxTypeCode;
    private String  gstTaxTypeCode;
    private int     grnExpiryTolerance;
    private int     dispatchExpiryTolerance;
    private int     returnExpiryTolerance;
    private int     shelfLife;
    private boolean expirable;

    public CategoryDTO() {

    }

    public CategoryDTO(Category category)
    {
        this.id = category.getId();
        this.code = category.getCode();
        this.name = category.getName();
        if (category.getTaxType() != null) {
            this.taxTypeCode = ConfigurationManager.getInstance().getConfiguration(TaxConfiguration.class).getTaxTypeById(category.getTaxType().getId()).getCode();
        }
        this.gstTaxTypeCode = ConfigurationManager.getInstance().getConfiguration(TaxConfiguration.class).getTaxTypeById(category.getGstTaxType().getId()).getCode();
        this.grnExpiryTolerance = category.getGrnExpiryTolerance();
        this.dispatchExpiryTolerance = category.getDispatchExpiryTolerance();
        this.shelfLife = category.getShelfLife();
        this.returnExpiryTolerance = category.getReturnExpiryTolerance();

    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the taxTypeCode
     */
    public String getTaxTypeCode() {
        return taxTypeCode;
    }

    /**
     * @param taxTypeCode the taxTypeCode to set
     */
    public void setTaxTypeCode(String taxTypeCode) {
        this.taxTypeCode = taxTypeCode;
    }

    public String getGstTaxTypeCode() {
        return gstTaxTypeCode;
    }

    public void setGstTaxTypeCode(String gstTaxTypeCode) {
        this.gstTaxTypeCode = gstTaxTypeCode;
    }

    public int getGrnExpiryTolerance() {
        return grnExpiryTolerance;
    }

    public void setGrnExpiryTolerance(int grnExpiryTolerance) {
        this.grnExpiryTolerance = grnExpiryTolerance;
    }

    public int getDispatchExpiryTolerance() {
        return dispatchExpiryTolerance;
    }

    public void setDispatchExpiryTolerance(int dispatchExpiryTolerance) {
        this.dispatchExpiryTolerance = dispatchExpiryTolerance;
    }

    public int getReturnExpiryTolerance() {
        return returnExpiryTolerance;
    }

    public void setReturnExpiryTolerance(int returnExpiryTolerance) {
        this.returnExpiryTolerance = returnExpiryTolerance;
    }

    public int getShelfLife() {
        return shelfLife;
    }

    public void setShelfLife(int shelfLife) {
        this.shelfLife = shelfLife;
    }

    public boolean isExpirable() {
        return expirable;
    }

    public void setExpirable(boolean expirable) {
        this.expirable = expirable;
    }
}
