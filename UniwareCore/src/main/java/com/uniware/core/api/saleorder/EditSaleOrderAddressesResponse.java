/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 24-Jul-2012
 *  @author vibhu
 */
package com.uniware.core.api.saleorder;

import com.unifier.core.api.base.ServiceResponse;

import java.util.ArrayList;
import java.util.List;

import com.uniware.core.api.model.WsAddressDetail;

/**
 * @author vibhu
 */
public class EditSaleOrderAddressesResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long     serialVersionUID = 1L;
    private List<WsAddressDetail> addresses        = new ArrayList<WsAddressDetail>();

    /**
     * @return the addresses
     */
    public List<WsAddressDetail> getAddresses() {
        return addresses;
    }

    /**
     * @param addresses the addresses to set
     */
    public void setAddresses(List<WsAddressDetail> addresses) {
        this.addresses = addresses;
    }

}
