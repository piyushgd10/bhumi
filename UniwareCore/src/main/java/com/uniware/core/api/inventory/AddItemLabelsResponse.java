/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jun 19, 2012
 *  @author singla
 */
package com.uniware.core.api.inventory;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author singla
 */
public class AddItemLabelsResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 5980874368549112250L;

}
