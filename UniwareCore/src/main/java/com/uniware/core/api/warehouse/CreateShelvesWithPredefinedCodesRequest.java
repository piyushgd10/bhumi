/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 16-Aug-2012
 *  @author praveeng
 */
package com.uniware.core.api.warehouse;

import com.unifier.core.api.base.ServiceRequest;
import com.unifier.core.utils.StringUtils;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author praveeng
 */
public class CreateShelvesWithPredefinedCodesRequest extends ServiceRequest {

    public CreateShelvesWithPredefinedCodesRequest() {

    }

    public CreateShelvesWithPredefinedCodesRequest(String shelfTypeCode, String sectionCode, String shelfCodes) {
        this.shelfTypeCode = shelfTypeCode;
        this.sectionCode = sectionCode;
        this.shelfCodes = StringUtils.split(shelfCodes);
    }

    /**
     * 
     */
    private static final long serialVersionUID = -7809317054269909598L;

    @NotEmpty
    private String            shelfTypeCode;

    @NotEmpty
    private String            sectionCode;

    @Size(min = 1)
    List<String>              shelfCodes       = new ArrayList<String>();

    /**
     * @return the shelfTypeCode
     */
    public String getShelfTypeCode() {
        return shelfTypeCode;
    }

    /**
     * @param shelfTypeCode the shelfTypeCode to set
     */
    public void setShelfTypeCode(String shelfTypeCode) {
        this.shelfTypeCode = shelfTypeCode;
    }

    /**
     * @return the sectionCode
     */
    public String getSectionCode() {
        return sectionCode;
    }

    /**
     * @param sectionCode the sectionCode to set
     */
    public void setSectionCode(String sectionCode) {
        this.sectionCode = sectionCode;
    }

    /**
     * @return the shelfCodes
     */
    public List<String> getShelfCodes() {
        return shelfCodes;
    }

    /**
     * @param shelfCodes the shelfCodes to set
     */
    public void setShelfCodes(List<String> shelfCodes) {
        this.shelfCodes = shelfCodes;
    }

}
