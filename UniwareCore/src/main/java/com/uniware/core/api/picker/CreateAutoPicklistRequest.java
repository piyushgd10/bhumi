package com.uniware.core.api.picker;

import javax.validation.constraints.Min;

public class CreateAutoPicklistRequest extends AbstractCreatePicklistRequest {

    private static final long serialVersionUID = 3452546872113674537L;

    @Min(value = 1)
    private Integer           noOfItems;

    public Integer getNoOfItems() {
        return noOfItems;
    }

    public void setNoOfItems(Integer noOfItems) {
        this.noOfItems = noOfItems;
    }
}
