package com.uniware.core.api.packer;

import java.util.List;

import com.unifier.core.api.base.ServiceResponse;

/**
 * Created by karunsingla on 28/06/14.
 */
public class ReceivePicklistResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long    serialVersionUID = -576597338719493222L;
    private List<PutbackItemDTO> putbackItems;

    public List<PutbackItemDTO> getPutbackItems() {
        return putbackItems;
    }

    public void setPutbackItems(List<PutbackItemDTO> putbackItems) {
        this.putbackItems = putbackItems;
    }
}
