/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 23-Mar-2014
 *  @author karunsingla
 */
package com.uniware.core.api.returns;

import com.unifier.core.api.base.ServiceResponse;

public class ReshipShippingPackageResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 2595882246787815993L;

}
