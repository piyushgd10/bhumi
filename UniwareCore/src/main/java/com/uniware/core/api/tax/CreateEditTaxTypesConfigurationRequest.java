/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 *  @version     1.0, Jun 20, 2012
 *  @author ankit
 */
package com.uniware.core.api.tax;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author akshay
 */
public class CreateEditTaxTypesConfigurationRequest extends ServiceRequest {

    /**
     *
     */
    private static final long serialVersionUID = 1053545764943197444L;

    @NotBlank
    private String taxTypeCode;

    private String taxTypeName;

    private boolean isEdit;

    private boolean gst;

    @Valid
    private List<WsTaxTypeConfiguration> taxTypeConfigurations;

    /**
     * @return the taxTypeCode
     */
    public String getTaxTypeCode() {
        return taxTypeCode;
    }

    /**
     * @param taxTypeCode the taxTypeCode to set
     */
    public void setTaxTypeCode(String taxTypeCode) {
        this.taxTypeCode = taxTypeCode;
    }

    public String getTaxTypeName() {
        return taxTypeName;
    }

    public void setTaxTypeName(String taxTypeName) {
        this.taxTypeName = taxTypeName;
    }

    public boolean isEdit() {
        return isEdit;
    }

    public boolean isGst() {
        return gst;
    }

    public void setGst(boolean gst) {
        this.gst = gst;
    }

    public void setEdit(boolean isEdit) {
        this.isEdit = isEdit;
    }

    public List<WsTaxTypeConfiguration> getTaxTypeConfigurations() {
        return taxTypeConfigurations;
    }

    public void setTaxTypeConfigurations(List<WsTaxTypeConfiguration> taxTypeConfigurations) {
        this.taxTypeConfigurations = taxTypeConfigurations;
    }

    public void addTaxTypeConfiguration(WsTaxTypeConfiguration wsTaxTypeConfiguration) {
        if (taxTypeConfigurations == null) {
            taxTypeConfigurations = new ArrayList<>();
        }
        taxTypeConfigurations.add(wsTaxTypeConfiguration);
    }
}
