/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 24-Jun-2014
 *  @author unicom
 */
package com.uniware.core.api.inventory;

import java.util.ArrayList;
import java.util.List;

import com.unifier.core.api.base.ServiceResponse;

public class GetItemTypeInventoryResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    private List<InventoryDTO> inventoryDTOs = new ArrayList<InventoryDTO>();
    
    public List<InventoryDTO> getInventoryDTOs() {
        return inventoryDTOs;
    }

    public void setInventoryDTOs(List<InventoryDTO> inventoryDTOs) {
        this.inventoryDTOs = inventoryDTOs;
    }

}
