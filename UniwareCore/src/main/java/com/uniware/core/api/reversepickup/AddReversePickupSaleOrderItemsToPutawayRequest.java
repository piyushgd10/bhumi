/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 14-May-2012
 *  @author praveeng
 */
package com.uniware.core.api.reversepickup;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author praveeng
 */
public class AddReversePickupSaleOrderItemsToPutawayRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long     serialVersionUID = 4749175056926468594L;

    @NotEmpty
    private String                reversePickupCode;

    @NotNull
    private Integer               userId;

    @NotBlank
    private String                putawayCode;

    @Valid
    @NotEmpty
    private List<WsSaleOrderItem> saleOrderItems   = new ArrayList<WsSaleOrderItem>();

    /**
     * @return the reversePickupCode
     */
    public String getReversePickupCode() {
        return reversePickupCode;
    }

    /**
     * @param reversePickupCode the reversePickupCode to set
     */
    public void setReversePickupCode(String reversePickupCode) {
        this.reversePickupCode = reversePickupCode;
    }

    /**
     * @return the saleOrderItems
     */
    public List<WsSaleOrderItem> getSaleOrderItems() {
        return saleOrderItems;
    }

    /**
     * @param saleOrderItems the saleOrderItems to set
     */
    public void setSaleOrderItems(List<WsSaleOrderItem> saleOrderItems) {
        this.saleOrderItems = saleOrderItems;
    }

    /**
     * <ol>
     *     <li>{@code code} sale order item code</li>
     *     <li>{@code status} Can be one of {@link StatusCode}</li>
     * </ol>
     */
    public static class WsSaleOrderItem {

        /**
         * Contains {@code GOOD_INVENTORY, BAD_INVENTORY, RETURN_TO_CUSTOMER, LIQUIDATED}
         */
        public enum StatusCode {
            GOOD_INVENTORY,
            BAD_INVENTORY,
            RETURN_TO_CUSTOMER,
            LIQUIDATED,
            DEAD_INVENTORY
        }

        @NotBlank
        private String code;

        @NotBlank
        private String status;

        private boolean expiredReturn;

        private String shelfCode;

        private String returnReason;

        public boolean isExpiredReturn() {
            return expiredReturn;
        }

        public void setExpiredReturn(boolean expiredReturn) {
            this.expiredReturn = expiredReturn;
        }

        public WsSaleOrderItem(String code, String status) {
            super();
            this.code = code;
            this.status = status;
        }

        public WsSaleOrderItem() {
            super();
        }

        /**
         * @return the status
         */
        public String getStatus() {
            return status;
        }

        /**
         * @param status the status to set
         */
        public void setStatus(String status) {
            this.status = status;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getShelfCode() { return shelfCode; }

        public void setShelfCode(String shelfCode) { this.shelfCode = shelfCode; }

        public String getReturnReason() { return returnReason; }

        public void setReturnReason(String returnReason) { this.returnReason = returnReason; }
    }

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return the putawayCode
     */
    public String getPutawayCode() {
        return putawayCode;
    }

    /**
     * @param putawayCode the putawayCode to set
     */
    public void setPutawayCode(String putawayCode) {
        this.putawayCode = putawayCode;
    }

}
