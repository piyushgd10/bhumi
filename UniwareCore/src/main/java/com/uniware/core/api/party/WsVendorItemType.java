/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jul 28, 2012
 *  @author singla
 */
package com.uniware.core.api.party;

import java.math.BigDecimal;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Min;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.customfields.WsCustomFieldValue;

/**
 * @author singla
 */
public class WsVendorItemType {

    @NotBlank
    private String                   vendorCode;

    @NotBlank
    private String                   itemTypeSkuCode;

    @Length(max = 100)
    private String                   vendorSkuCode;

    @Min(value = 0)
    private Integer                  inventory;

    private BigDecimal               unitPrice;
    private int                      priority;
    private Boolean                  enabled;

    @Valid
    private List<WsCustomFieldValue> customFieldValues;

    /**
     * @return the vendorSkuCode
     */
    public String getVendorSkuCode() {
        return vendorSkuCode;
    }

    /**
     * @param vendorSkuCode the vendorSkuCode to set
     */
    public void setVendorSkuCode(String vendorSkuCode) {
        this.vendorSkuCode = vendorSkuCode;
    }

    /**
     * @return the unitPrice
     */
    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    /**
     * @param unitPrice the unitPrice to set
     */
    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }

    public String getVendorCode() {
        return vendorCode;
    }

    public void setVendorCode(String vendorCode) {
        this.vendorCode = vendorCode;
    }

    /**
     * @return the itemTypeSkuCode
     */
    public String getItemTypeSkuCode() {
        return itemTypeSkuCode;
    }

    /**
     * @param itemTypeSkuCode the itemTypeSkuCode to set
     */
    public void setItemTypeSkuCode(String itemTypeSkuCode) {
        this.itemTypeSkuCode = itemTypeSkuCode;
    }

    /**
     * @return the priority
     */
    public int getPriority() {
        return priority;
    }

    /**
     * @param priority the priority to set
     */
    public void setPriority(int priority) {
        this.priority = priority;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    /**
     * @return the customFieldValues
     */
    public List<WsCustomFieldValue> getCustomFieldValues() {
        return customFieldValues;
    }

    /**
     * @param customFieldValues the customFieldValues to set
     */
    public void setCustomFieldValues(List<WsCustomFieldValue> customFieldValues) {
        this.customFieldValues = customFieldValues;
    }

    public Integer getInventory() {
        return inventory;
    }

    public void setInventory(Integer inventory) {
        this.inventory = inventory;
    }

}
