/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 11-Feb-2015
 *  @author parijat
 */
package com.uniware.core.api.model;

import java.util.List;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

public class WSShippingProviderConnector {

    @NotBlank
    private String shippingCode;

    @NotBlank
    private String name;

    @NotEmpty
    private List<WsShippingProviderConnectorParameter> shippingProviderConnectorParameters;

    /**
     * @return the shippingCode
     */
    public String getShippingCode() {
        return shippingCode;
    }

    /**
     * @param shippingCode the shippingCode to set
     */
    public void setShippingCode(String shippingCode) {
        this.shippingCode = shippingCode;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the shippingProviderConnectorParameters
     */
    public List<WsShippingProviderConnectorParameter> getShippingProviderConnectorParameters() {
        return shippingProviderConnectorParameters;
    }

    /**
     * @param shippingProviderConnectorParameters the shippingProviderConnectorParameters to set
     */
    public void setShippingProviderConnectorParameters(List<WsShippingProviderConnectorParameter> shippingProviderConnectorParameters) {
        this.shippingProviderConnectorParameters = shippingProviderConnectorParameters;
    }

}
