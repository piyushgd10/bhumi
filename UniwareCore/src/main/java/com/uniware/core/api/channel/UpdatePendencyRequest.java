/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 03-Jun-2012
 *  @author vibhu
 */
package com.uniware.core.api.channel;

import java.util.List;

import javax.validation.Valid;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author Sunny
 */
public class UpdatePendencyRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long         serialVersionUID = -8161481212900201589L;

    @NotBlank
    private String                    channelCode;

    @Valid
    private List<WsUpdatePendency> pendencyDTOs;

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    public List<WsUpdatePendency> getPendencyDTOs() {
        return pendencyDTOs;
    }

    public void setPendencyDTOs(List<WsUpdatePendency> pendencyDTOs) {
        this.pendencyDTOs = pendencyDTOs;
    }

}
