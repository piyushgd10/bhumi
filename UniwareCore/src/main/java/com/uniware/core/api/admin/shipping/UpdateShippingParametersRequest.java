/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 07-Mar-2013
 *  @author Pankaj
 */
package com.uniware.core.api.admin.shipping;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author Pankaj
 */
public class UpdateShippingParametersRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @NotBlank
    private String            code;
    private String            name;
    private String            scraperScript;
    private String            trackingNumberAllocationScript;
    private String            postManifestScript;
    private String            trackingLink;
    private boolean           globalServiceability;
    private boolean           enabled;
    private boolean           trackingEnabled;

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the scraperScript
     */
    public String getScraperScript() {
        return scraperScript;
    }

    /**
     * @param scraperScript the scraperScript to set
     */
    public void setScraperScript(String scraperScript) {
        this.scraperScript = scraperScript;
    }

    /**
     * @return the trackingNumberAllocationScript
     */
    public String getTrackingNumberAllocationScript() {
        return trackingNumberAllocationScript;
    }

    /**
     * @param trackingNumberAllocationScript the trackingNumberAllocationScript to set
     */
    public void setTrackingNumberAllocationScript(String trackingNumberAllocationScript) {
        this.trackingNumberAllocationScript = trackingNumberAllocationScript;
    }

    /**
     * @return the postManifestScript
     */
    public String getPostManifestScript() {
        return postManifestScript;
    }

    /**
     * @param postManifestScript the postManifestScript to set
     */
    public void setPostManifestScript(String postManifestScript) {
        this.postManifestScript = postManifestScript;
    }

    /**
     * @return the trackingLink
     */
    public String getTrackingLink() {
        return trackingLink;
    }

    /**
     * @param trackingLink the trackingLink to set
     */
    public void setTrackingLink(String trackingLink) {
        this.trackingLink = trackingLink;
    }

    /**
     * @return the globalServiceability
     */
    public boolean isGlobalServiceability() {
        return globalServiceability;
    }

    /**
     * @param globalServiceability the globalServiceability to set
     */
    public void setGlobalServiceability(boolean globalServiceability) {
        this.globalServiceability = globalServiceability;
    }

    /**
     * @return the enabled
     */
    public boolean isEnabled() {
        return enabled;
    }

    /**
     * @param enabled the enabled to set
     */
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    /**
     * @return the trackingEnabled
     */
    public boolean isTrackingEnabled() {
        return trackingEnabled;
    }

    /**
     * @param trackingEnabled the trackingEnabled to set
     */
    public void setTrackingEnabled(boolean trackingEnabled) {
        this.trackingEnabled = trackingEnabled;
    }

}
