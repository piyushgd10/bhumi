/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 27-Jan-2015
 *  @author akshaykochhar
 */
package com.uniware.core.api.tenant;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

public class RefreshTenantSetupStateRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    public RefreshTenantSetupStateRequest() {
        
    }

    public RefreshTenantSetupStateRequest(String code) {
        this.code = code;
    }
    
    @NotBlank
    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

}
