/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jun 20, 2012
 *  @author singla
 */
package com.uniware.core.api.dual.company;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author singla
 */
public class AutoCompletePutawayRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long      serialVersionUID = 2232865815634253026L;

    @NotBlank
    private String                 purchaseOrderCode;

    private String                 inflowReceiptCode;

    @Valid
    private List<WsInflowItemType> inflowItemTypes;

    private String                 vendorInvoiceNumber;

    private Date                   vendorInvoiceDate;

    public boolean                 onlyPrepareGRN;

    /**
     * @return the purchaseOrderCode
     */
    public String getPurchaseOrderCode() {
        return purchaseOrderCode;
    }

    /**
     * @param purchaseOrderCode the purchaseOrderCode to set
     */
    public void setPurchaseOrderCode(String purchaseOrderCode) {
        this.purchaseOrderCode = purchaseOrderCode;
    }

    /**
     * @return the inflowItemTypes
     */
    public List<WsInflowItemType> getInflowItemTypes() {
        return inflowItemTypes;
    }

    /**
     * @param inflowItemTypes the inflowItemTypes to set
     */
    public void setInflowItemTypes(List<WsInflowItemType> inflowItemTypes) {
        this.inflowItemTypes = inflowItemTypes;
    }

    /**
     * @return the vendorInvoiceNumber
     */
    public String getVendorInvoiceNumber() {
        return vendorInvoiceNumber;
    }

    /**
     * @param vendorInvoiceNumber the vendorInvoiceNumber to set
     */
    public void setVendorInvoiceNumber(String vendorInvoiceNumber) {
        this.vendorInvoiceNumber = vendorInvoiceNumber;
    }

    /**
     * @return the vendorInvoiceDate
     */
    public Date getVendorInvoiceDate() {
        return vendorInvoiceDate;
    }

    /**
     * @param vendorInvoiceDate the vendorInvoiceDate to set
     */
    public void setVendorInvoiceDate(Date vendorInvoiceDate) {
        this.vendorInvoiceDate = vendorInvoiceDate;
    }

    public boolean isOnlyPrepareGRN() {
        return onlyPrepareGRN;
    }

    public void setOnlyPrepareGRN(boolean onlyPrepareGRN) {
        this.onlyPrepareGRN = onlyPrepareGRN;
    }

    public String getInflowReceiptCode() {
        return inflowReceiptCode;
    }

    public void setInflowReceiptCode(String inflowReceiptCode) {
        this.inflowReceiptCode = inflowReceiptCode;
    }

}
