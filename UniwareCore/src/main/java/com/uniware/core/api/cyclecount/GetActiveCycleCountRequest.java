package com.uniware.core.api.cyclecount;

import java.util.Set;

import javax.validation.constraints.Min;

import com.unifier.core.api.base.ServiceRequest;

/**
 * Created by harshpal on 3/8/16.
 */
public class GetActiveCycleCountRequest extends ServiceRequest {

    @Min(1)
    private Integer     pageSize;
    @Min(1)
    private Integer     pageStartIndex;
    private Set<String> statusCodes;

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Integer getPageStartIndex() {
        return pageStartIndex;
    }

    public void setPageStartIndex(Integer pageStartIndex) {
        this.pageStartIndex = pageStartIndex;
    }

    public Set<String> getStatusCodes() {
        return statusCodes;
    }

    public void setStatusCodes(Set<String> statusCodes) {
        this.statusCodes = statusCodes;
    }
}
