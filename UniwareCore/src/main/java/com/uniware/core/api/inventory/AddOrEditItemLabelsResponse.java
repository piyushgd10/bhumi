/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jun 20, 2012
 *  @author singla
 */
package com.uniware.core.api.inventory;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author singla
 */
public class AddOrEditItemLabelsResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 6168062365258295894L;

}
