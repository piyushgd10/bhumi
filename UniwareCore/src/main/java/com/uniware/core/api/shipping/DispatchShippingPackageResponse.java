/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 10-Dec-2013
 *  @author parijat
 */
package com.uniware.core.api.shipping;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author Sunny
 */
public class DispatchShippingPackageResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -3976289163745417591L;

    private String            shippingPackageCode;
    private boolean           cancelledOnChannel;

    public boolean isCancelledOnChannel() {
        return cancelledOnChannel;
    }

    public void setCancelledOnChannel(boolean cancelledOnChannel) {
        this.cancelledOnChannel = cancelledOnChannel;
    }

    public String getShippingPackageCode() {
        return shippingPackageCode;
    }

    public void setShippingPackageCode(String shippingPackageCode) {
        this.shippingPackageCode = shippingPackageCode;
    }

}
