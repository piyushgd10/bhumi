/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 15-May-2014
 *  @author unicom
 */
package com.uniware.core.api.saleorder;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

public class RemoveMappingErrorRetrySaleOrderRequest extends ServiceRequest {

    private static final long serialVersionUID = -1625715386633175087L;

    @NotBlank
    private String            channelProductId;

    public String getChannelProductId() {
        return channelProductId;
    }

    public void setChannelProductId(String channelProductId) {
        this.channelProductId = channelProductId;
    }

}
