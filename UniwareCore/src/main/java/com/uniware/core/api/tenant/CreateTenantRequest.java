/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 14, 2012
 *  @author singla
 */
package com.uniware.core.api.tenant;

import com.unifier.core.annotation.constraints.Password;
import com.unifier.core.api.base.ServiceRequest;
import com.uniware.core.api.party.WsFacility;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * @author Sunny
 */
public class CreateTenantRequest extends ServiceRequest {
	
	private static final long	serialVersionUID	= -3749078213185359215L;
	
	@NotBlank
	private String				accessUrl;
	
	@NotBlank
	private String				username;
	
	@NotBlank
	private String				code;
	
	@NotBlank
	private String				name;

	@NotBlank
	private String				companyName;
	
	@NotNull
	private String				tenantType;
	
	@NotBlank
	private String				mode;
	
	@NotBlank
	private String				password;
	
	@NotBlank
	private String				confirmPassword;
	
	@NotBlank
	private String				apiUsername;
	
	@NotBlank
	@Password
	private String				apiPassword;
	
	@Email
	private String				email;
	
	private String				mobile;
	
	private String				referrerCode;
	
	private String				inviteCode;
	
	@Valid
	private WsFacility			facility;
	
	private Integer				currentUserId;

	public String getAccessUrl() {
		return accessUrl;
	}
	
	public void setAccessUrl(String accessUrl) {
		this.accessUrl = accessUrl;
	}
	
	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getCode() {
		return code;
	}
	
	public void setCode(String code) {
		this.code = code;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getTenantType() {
		return tenantType;
	}
	
	public void setTenantType(String tenantType) {
		this.tenantType = tenantType;
	}
	
	public String getMode() {
		return mode;
	}
	
	public void setMode(String mode) {
		this.mode = mode;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getConfirmPassword() {
		return confirmPassword;
	}
	
	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getApiUsername() {
		return apiUsername;
	}
	
	public void setApiUsername(String apiUsername) {
		this.apiUsername = apiUsername;
	}
	
	public String getApiPassword() {
		return apiPassword;
	}
	
	public void setApiPassword(String apiPassword) {
		this.apiPassword = apiPassword;
	}
	
	public WsFacility getFacility() {
		return facility;
	}
	
	public void setFacility(WsFacility facility) {
		this.facility = facility;
	}
	
	public Integer getCurrentUserId() {
		return currentUserId;
	}
	
	public void setCurrentUserId(Integer currentUserId) {
		this.currentUserId = currentUserId;
	}
	
	public String getReferrerCode() {
		return referrerCode;
	}
	
	public void setReferrerCode(String referrerCode) {
		this.referrerCode = referrerCode;
	}
	
	@Override
	public String toString() {
		return "CreateTenantRequest [accessUrl=" + accessUrl + ", username=" + username + ", name=" + name + ", password=" + password + ", confirmPassword=" + confirmPassword + ", email=" + email + "]";
	}
	
	public String getInviteCode() {
		return inviteCode;
	}
	
	public void setInviteCode(String inviteCode) {
		this.inviteCode = inviteCode;
	}
	
	public String getMobile() {
		return mobile;
	}
	
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}


	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

}
