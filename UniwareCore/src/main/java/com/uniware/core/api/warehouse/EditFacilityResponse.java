/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 14-Feb-2012
 *  @author vibhu
 */
package com.uniware.core.api.warehouse;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author vibhu
 */
public class EditFacilityResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -857938331985955268L;
    private FacilityDTO       facilityDTO;

    /**
     * @return the warehouseDTO
     */
    public FacilityDTO getFacilityDTO() {
        return facilityDTO;
    }

    /**
     * @param warehouseDTO the warehouseDTO to set
     */
    public void setFacilityDTO(FacilityDTO facilityDTO) {
        this.facilityDTO = facilityDTO;
    }

}
