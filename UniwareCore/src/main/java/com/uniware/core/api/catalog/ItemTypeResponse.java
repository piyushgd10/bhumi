/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 24-Dec-2011
 *  @author vibhu
 */
package com.uniware.core.api.catalog;

import com.unifier.core.api.base.ServiceResponse;

import com.uniware.core.api.catalog.dto.ItemTypeFullDTO;

/**
 * @author vibhu
 */
public class ItemTypeResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -5300675916228168637L;
    private ItemTypeFullDTO   itemType;

    /**
     * @return the itemType
     */
    public ItemTypeFullDTO getItemType() {
        return itemType;
    }

    /**
     * @param itemType the itemType to set
     */
    public void setItemType(ItemTypeFullDTO itemType) {
        this.itemType = itemType;
    }

}
