/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 11-Nov-2013
 *  @author unicom
 */
package com.uniware.core.api.inflow;

import com.unifier.core.api.base.ServiceResponse;

public class AddSaleOrderItemDetailResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

}
