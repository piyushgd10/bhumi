package com.uniware.core.api.shipping;

import com.unifier.core.api.base.ServiceRequest;
import org.hibernate.validator.constraints.NotBlank;

/**
 * @author ankurpratik on 28/03/17.
 */
public class FetchShippingManifestPdfRequest extends ServiceRequest {

    private static final long serialVersionUID = 3671090228471078534L;

    @NotBlank
    private String shippingManifestCode;

    public String getShippingManifestCode() {
        return shippingManifestCode;
    }

    public void setShippingManifestCode(String shippingManifestCode) {
        this.shippingManifestCode = shippingManifestCode;
    }
}
