/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 03-Jun-2012
 *  @author vibhu
 */
package com.uniware.core.api.channel;

import java.math.BigDecimal;
import java.util.List;

import javax.validation.Valid;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author Sunny
 */
public class UpdateChannelItemTypeTaxRequest extends ServiceRequest {

    /**
     *
     */
    private static final long          serialVersionUID = 1544077290732461048L;

    private BigDecimal                 globalTaxPercentage;

    @NotEmpty
    @Valid
    private List<WsChannelItemTypeTax> channelItemTypeTaxes;

    public BigDecimal getGlobalTaxPercentage() {
        return globalTaxPercentage;
    }

    public void setGlobalTaxPercentage(BigDecimal globalTaxPercentage) {
        this.globalTaxPercentage = globalTaxPercentage;
    }

    public List<WsChannelItemTypeTax> getChannelItemTypeTaxes() {
        return channelItemTypeTaxes;
    }

    public void setChannelItemTypeTaxes(List<WsChannelItemTypeTax> channelItemTypeTaxes) {
        this.channelItemTypeTaxes = channelItemTypeTaxes;
    }

    public static class WsChannelItemTypeTax {

        @NotBlank
        private String     channelCode;

        @NotBlank
        private String     channelProductId;

        @NotBlank
        private String     hsnCode;
        private BigDecimal vatPercentage;
        private BigDecimal cstPercentage;
        private BigDecimal centralGstPercentage;
        private BigDecimal stateGstPercentage;
        private BigDecimal unionTerritoryGstPercentage;
        private BigDecimal integratedGstPercentage;
        private BigDecimal compensationCessPercentage;

        public String getChannelCode() {
            return channelCode;
        }

        public void setChannelCode(String channelCode) {
            this.channelCode = channelCode;
        }

        public String getChannelProductId() {
            return channelProductId;
        }

        public void setChannelProductId(String channelProductId) {
            this.channelProductId = channelProductId;
        }

        public String getHsnCode() {
            return hsnCode;
        }

        public void setHsnCode(String hsnCode) {
            this.hsnCode = hsnCode;
        }

        public BigDecimal getVatPercentage() {
            return vatPercentage;
        }

        public void setVatPercentage(BigDecimal vatPercentage) {
            this.vatPercentage = vatPercentage;
        }

        public BigDecimal getCstPercentage() {
            return cstPercentage;
        }

        public void setCstPercentage(BigDecimal cstPercentage) {
            this.cstPercentage = cstPercentage;
        }

        public BigDecimal getCentralGstPercentage() {
            return centralGstPercentage;
        }

        public void setCentralGstPercentage(BigDecimal centralGstPercentage) {
            this.centralGstPercentage = centralGstPercentage;
        }

        public BigDecimal getStateGstPercentage() {
            return stateGstPercentage;
        }

        public void setStateGstPercentage(BigDecimal stateGstPercentage) {
            this.stateGstPercentage = stateGstPercentage;
        }

        public BigDecimal getUnionTerritoryGstPercentage() {
            return unionTerritoryGstPercentage;
        }

        public void setUnionTerritoryGstPercentage(BigDecimal unionTerritoryGstPercentage) {
            this.unionTerritoryGstPercentage = unionTerritoryGstPercentage;
        }

        public BigDecimal getIntegratedGstPercentage() {
            return integratedGstPercentage;
        }

        public void setIntegratedGstPercentage(BigDecimal integratedGstPercentage) {
            this.integratedGstPercentage = integratedGstPercentage;
        }

        public BigDecimal getCompensationCessPercentage() {
            return compensationCessPercentage;
        }

        public void setCompensationCessPercentage(BigDecimal compensationCessPercentage) {
            this.compensationCessPercentage = compensationCessPercentage;
        }
    }

}
