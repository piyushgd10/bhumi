/*
 * Copyright 2017 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 11/12/17
 * @author piyush
 */
package com.uniware.core.api.channel;

import java.util.List;

import javax.validation.constraints.NotNull;

import com.unifier.core.api.base.ServiceRequest;
import org.hibernate.validator.constraints.NotEmpty;

public class EditChannelAssociatedFacilityRequest extends ServiceRequest {

    public enum Action {
        ADD,
        REMOVE
    }

    @NotNull
    private String       channelCode;

    @NotEmpty
    private List<String> facilityCodes;

    private Action       action;

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    public List<String> getFacilityCodes() {
        return facilityCodes;
    }

    public void setFacilityCodes(List<String> facilityCodes) {
        this.facilityCodes = facilityCodes;
    }

    public Action getAction() {
        return action;
    }

    public void setAction(Action action) {
        this.action = action;
    }
}
