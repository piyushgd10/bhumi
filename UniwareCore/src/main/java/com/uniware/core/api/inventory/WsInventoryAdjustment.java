/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 30-Jan-2014
 *  @author karunsingla
 */
package com.uniware.core.api.inventory;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import com.uniware.core.entity.InventoryAdjustment.AdjustmentType;
import com.uniware.core.entity.ItemTypeInventory.Type;

public class WsInventoryAdjustment {

    @NotBlank
    private String         itemSKU;

    @NotNull
    @Min(0)
    private Integer        quantity;

    @NotBlank
    private String         shelfCode;

    private Type           inventoryType;

    private String         transferToShelfCode;
    
    private Integer        sla;

    @NotNull
    private AdjustmentType adjustmentType;

    @Length(max = 255)
    private String         remarks;

    public String getItemSKU() {
        return itemSKU;
    }

    public void setItemSKU(String itemSKU) {
        this.itemSKU = itemSKU;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getShelfCode() {
        return shelfCode;
    }

    public void setShelfCode(String shelfCode) {
        this.shelfCode = shelfCode;
    }

    public Type getInventoryType() {
        return inventoryType;
    }

    public void setInventoryType(Type inventoryType) {
        this.inventoryType = inventoryType;
    }

    public String getTransferToShelfCode() {
        return transferToShelfCode;
    }

    public void setTransferToShelfCode(String transferToShelfCode) {
        this.transferToShelfCode = transferToShelfCode;
    }
    
    public Integer getSla() {
        return sla;
    }

    public void setSla(Integer sla) {
        this.sla = sla;
    }

    public AdjustmentType getAdjustmentType() {
        return adjustmentType;
    }

    public void setAdjustmentType(AdjustmentType adjustmentType) {
        this.adjustmentType = adjustmentType;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }
}
