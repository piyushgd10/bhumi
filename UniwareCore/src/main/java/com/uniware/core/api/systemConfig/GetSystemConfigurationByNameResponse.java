/*
 * Copyright 2016 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 15/9/16 3:17 PM
 * @author bhuvneshwarkumar
 */

package com.uniware.core.api.systemConfig;

import java.util.HashMap;
import java.util.Map;

import com.unifier.core.api.base.ServiceResponse;

/**
 * Created by bhuvneshwarkumar on 15/09/16.
 */
public class GetSystemConfigurationByNameResponse extends ServiceResponse {

    private Map<String, String> configurations = new HashMap<>();

    public Map<String, String> getConfigurations() {
        return configurations;
    }

    public void setConfigurations(Map<String, String> configurations) {
        this.configurations = configurations;
    }
}
