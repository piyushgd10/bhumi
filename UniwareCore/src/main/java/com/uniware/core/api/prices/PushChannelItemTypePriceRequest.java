/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Oct 13, 2015
 *  @author akshay
 */
package com.uniware.core.api.prices;


public class PushChannelItemTypePriceRequest extends AbstractChannelItemTypeRequest {

    private static final long serialVersionUID = 3341528689438847422L;
    
    private boolean forcePush = false;

    public boolean isForcePush() {
        return forcePush;
    }

    public void setForcePush(boolean forcePush) {
        this.forcePush = forcePush;
    }
}
