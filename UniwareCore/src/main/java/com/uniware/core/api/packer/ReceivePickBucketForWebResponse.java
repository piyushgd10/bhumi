/*
 * Copyright 2016 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 10/9/16 8:10 PM
 * @author bhuvneshwarkumar
 */

package com.uniware.core.api.packer;

import com.uniware.core.entity.PickBatch;

/**
 * Created by bhuvneshwarkumar on 10/09/16.
 */
public class ReceivePickBucketForWebResponse extends ReceivePicklistResponse {

    private String               picklistCode;

    private PickBatch.StatusCode pickBatchStatus;

    public String getPicklistCode() {
        return picklistCode;
    }

    public void setPicklistCode(String picklistCode) {
        this.picklistCode = picklistCode;
    }

    public PickBatch.StatusCode getPickBatchStatus() {
        return pickBatchStatus;
    }

    public void setPickBatchStatus(PickBatch.StatusCode pickBatchStatus) {
        this.pickBatchStatus = pickBatchStatus;
    }
}
