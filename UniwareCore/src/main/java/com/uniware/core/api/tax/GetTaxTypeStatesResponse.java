/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 23-Dec-2014
 *  @author parijat
 */
package com.uniware.core.api.tax;

import java.util.HashMap;
import java.util.Map;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author parijat
 *
 */
public class GetTaxTypeStatesResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -3952379251310674446L;

    private Map<String, String> facilityStates   = new HashMap<String, String>();

    private Map<String, String> vendorStates     = new HashMap<String, String>();

    private Map<String, String> restOfIndiaStates = new HashMap<>();

    public Map<String, String> getFacilityStates() {
        return facilityStates;
    }

    public void setFacilityStates(Map<String, String> facilityStates) {
        this.facilityStates = facilityStates;
    }

    public Map<String, String> getVendorStates() {
        return vendorStates;
    }

    public void setVendorStates(Map<String, String> vendorStates) {
        this.vendorStates = vendorStates;
    }

    public Map<String, String> getRestOfIndiaStates() {
        return restOfIndiaStates;
    }

    public void setRestOfIndiaStates(Map<String, String> restOfIndiaStates) {
        this.restOfIndiaStates = restOfIndiaStates;
    }

}

