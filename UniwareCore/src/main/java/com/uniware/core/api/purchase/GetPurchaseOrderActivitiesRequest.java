/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, May 20, 2015
 *  @author harsh
 */
package com.uniware.core.api.purchase;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author harsh
 */
public class GetPurchaseOrderActivitiesRequest extends ServiceRequest {

    private static final long serialVersionUID = -2369776197588089088L;

    private String            purchaseOrderCode;

    public String getPurchaseOrderCode() {
        return purchaseOrderCode;
    }

    public void setPurchaseOrderCode(String purchaseOrderCode) {
        this.purchaseOrderCode = purchaseOrderCode;
    }

}
