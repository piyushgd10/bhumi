/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 *  @version     1.0, 20-Jul-2012
 *  @author praveeng
 */
package com.uniware.core.api.catalog;

import java.util.Date;

import com.uniware.core.entity.TaxType;

/**
 * @author praveeng
 */
public class TaxTypeDTO {
    private Integer id;
    private String code;
    private String name;
    private Date   created;
    private Date   updated;

    /**
     * @param taxType
     */
    public TaxTypeDTO(TaxType taxType) {
        setId(taxType.getId());
        setCode(taxType.getCode());
        setName(taxType.getName());
        setCreated(taxType.getCreated());
        setUpdated(taxType.getUpdated());
    }



    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }



    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }



    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the created
     */
    public Date getCreated() {
        return created;
    }

    /**
     * @param created the created to set
     */
    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

}
