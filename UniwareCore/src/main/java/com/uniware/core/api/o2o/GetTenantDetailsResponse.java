package com.uniware.core.api.o2o;

import com.unifier.core.api.base.ServiceResponse;
import com.unifier.services.vo.TenantProfileVO;

/**
 * Created by piyush on 1/12/16.
 */
public class GetTenantDetailsResponse extends ServiceResponse {
    /**
     *
     */
    private static final long serialVersionUID = 7741948787105703348L;

    private TenantDTO         tenant;

    public TenantDTO getTenant() {
        return tenant;
    }

    public void setTenant(TenantDTO tenant) {
        this.tenant = tenant;
    }

    public static class TenantDTO {
        private String                         tenantCode;
        private String                         companyName;
        private TenantProfileVO.SellerType     sellerType;
        private TenantProfileVO.ShippingMethod shippingMethod;
        private int                            cutoffTime;

        public TenantDTO() {
        }

        public TenantDTO(TenantProfileVO tenantProfileVO) {
            this.tenantCode = tenantProfileVO.getTenantCode();
            this.companyName = tenantProfileVO.getCompanyName();
            this.sellerType = tenantProfileVO.getSellerType();
            this.shippingMethod = tenantProfileVO.getShippingMethod();
            this.cutoffTime = tenantProfileVO.getCutoffTime();
        }

        public String getTenantCode() {
            return tenantCode;
        }

        public void setTenantCode(String tenantCode) {
            this.tenantCode = tenantCode;
        }

        public String getCompanyName() {
            return companyName;
        }

        public void setCompanyName(String companyName) {
            this.companyName = companyName;
        }

        public TenantProfileVO.SellerType getSellerType() {
            return sellerType;
        }

        public void setSellerType(TenantProfileVO.SellerType sellerType) {
            this.sellerType = sellerType;
        }

        public TenantProfileVO.ShippingMethod getShippingMethod() {
            return shippingMethod;
        }

        public void setShippingMethod(TenantProfileVO.ShippingMethod shippingMethod) {
            this.shippingMethod = shippingMethod;
        }

        public int getCutoffTime() {
            return cutoffTime;
        }

        public void setCutoffTime(int cutoffTime) {
            this.cutoffTime = cutoffTime;
        }
    }
}
