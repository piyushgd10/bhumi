/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jan 10, 2012
 *  @author singla
 */
package com.uniware.core.api.saleorder.display;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Set;

import com.unifier.core.api.base.ServiceResponse;
import com.unifier.core.api.customfields.CustomFieldMetadataDTO;
import com.uniware.core.api.model.WsAddressDetail;

public class GetSaleOrderSummaryResponse extends ServiceResponse {

    /**
     *
     */
    private static final long serialVersionUID = 6284414547113409023L;

    public GetSaleOrderSummaryResponse() {
    }

    private SaleOrderSummaryDTO saleOrderSummary;
    private boolean             invalidFacility;
    private Set<String>         validFacilities;

    public SaleOrderSummaryDTO getSaleOrderSummary() {
        return saleOrderSummary;
    }

    public void setSaleOrderSummary(SaleOrderSummaryDTO saleOrderSummary) {
        this.saleOrderSummary = saleOrderSummary;
    }

    public boolean isInvalidFacility() {
        return invalidFacility;
    }

    public void setInvalidFacility(boolean invalidFacility) {
        this.invalidFacility = invalidFacility;
    }

    public Set<String> getValidFacilities() {
        return validFacilities;
    }

    public void setValidFacilities(Set<String> validFacilities) {
        this.validFacilities = validFacilities;
    }

    public static class SaleOrderSummaryDTO {
        private String                       code;
        private String                       displayOrderCode;
        private int                          priority;
        private String                       status;
        private String                       customerCode;
        private String                       customerName;
        private String                       channel;
        private boolean                      thirdPartyShipping;
        private String                       paymentMethod;
        private String                       paymentInstrument;
        private String                       currencyCode;
        private BigDecimal                   totalPrice = BigDecimal.ZERO;
        private Date                         displayOrderDateTime;
        private Date                         fulfillmentTat;
        private Date                         created;
        private Date                         updated;
        private boolean                      cFormProvided;
        private boolean                      taxExempted;
        private WsAddressDetail              billingAddress;
        private WsAddressDetail              shippingAddress;
        private boolean                      addressEditable;
        private boolean                      podRequired;
        private List<CustomFieldMetadataDTO> customFieldValues;
        private String                       notificationEmail;
        private String                       notificationMobile;
        private ItemStatusDTO                itemStatus;
        private int                          saleOrderItemCount;
        private int                          shipmentCount;
        private int                          invoiceCount;
        private int                          returnsCount;
        private int                          activityCount;

        public SaleOrderSummaryDTO() {
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getDisplayOrderCode() {
            return displayOrderCode;
        }

        public void setDisplayOrderCode(String displayOrderCode) {
            this.displayOrderCode = displayOrderCode;
        }

        public int getPriority() {
            return priority;
        }

        public void setPriority(int priority) {
            this.priority = priority;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getCustomerCode() {
            return customerCode;
        }

        public void setCustomerCode(String customerCode) {
            this.customerCode = customerCode;
        }

        public String getCustomerName() {
            return customerName;
        }

        public void setCustomerName(String customerName) {
            this.customerName = customerName;
        }

        public String getChannel() {
            return channel;
        }

        public void setChannel(String channel) {
            this.channel = channel;
        }

        public boolean isThirdPartyShipping() {
            return thirdPartyShipping;
        }

        public void setThirdPartyShipping(boolean thirdPartyShipping) {
            this.thirdPartyShipping = thirdPartyShipping;
        }

        public String getPaymentMethod() {
            return paymentMethod;
        }

        public void setPaymentMethod(String paymentMethod) {
            this.paymentMethod = paymentMethod;
        }

        public String getPaymentInstrument() {
            return paymentInstrument;
        }

        public void setPaymentInstrument(String paymentInstrument) {
            this.paymentInstrument = paymentInstrument;
        }

        public String getCurrencyCode() {
            return currencyCode;
        }

        public void setCurrencyCode(String currencyCode) {
            this.currencyCode = currencyCode;
        }

        public BigDecimal getTotalPrice() {
            return totalPrice;
        }

        public void setTotalPrice(BigDecimal totalPrice) {
            this.totalPrice = totalPrice;
        }

        public Date getDisplayOrderDateTime() {
            return displayOrderDateTime;
        }

        public void setDisplayOrderDateTime(Date displayOrderDateTime) {
            this.displayOrderDateTime = displayOrderDateTime;
        }

        public Date getFulfillmentTat() {
            return fulfillmentTat;
        }

        public void setFulfillmentTat(Date fulfillmentTat) {
            this.fulfillmentTat = fulfillmentTat;
        }

        public Date getCreated() {
            return created;
        }

        public void setCreated(Date created) {
            this.created = created;
        }

        public Date getUpdated() {
            return updated;
        }

        public void setUpdated(Date updated) {
            this.updated = updated;
        }

        public boolean iscFormProvided() {
            return cFormProvided;
        }

        public void setcFormProvided(boolean cFormProvided) {
            this.cFormProvided = cFormProvided;
        }

        public boolean isTaxExempted() {
            return taxExempted;
        }

        public void setTaxExempted(boolean taxExempted) {
            this.taxExempted = taxExempted;
        }

        public WsAddressDetail getBillingAddress() {
            return billingAddress;
        }

        public void setBillingAddress(WsAddressDetail billingAddress) {
            this.billingAddress = billingAddress;
        }

        public WsAddressDetail getShippingAddress() {
            return shippingAddress;
        }

        public void setShippingAddress(WsAddressDetail shippingAddress) {
            this.shippingAddress = shippingAddress;
        }

        public boolean isAddressEditable() {
            return addressEditable;
        }

        public void setAddressEditable(boolean addressEditable) {
            this.addressEditable = addressEditable;
        }

        public ItemStatusDTO getItemStatus() {
            return itemStatus;
        }

        public void setItemStatus(ItemStatusDTO itemStatus) {
            this.itemStatus = itemStatus;
        }

        public List<CustomFieldMetadataDTO> getCustomFieldValues() {
            return customFieldValues;
        }

        public void setCustomFieldValues(List<CustomFieldMetadataDTO> customFieldValues) {
            this.customFieldValues = customFieldValues;
        }

        public String getNotificationEmail() {
            return notificationEmail;
        }

        public void setNotificationEmail(String notificationEmail) {
            this.notificationEmail = notificationEmail;
        }

        public String getNotificationMobile() {
            return notificationMobile;
        }

        public void setNotificationMobile(String notificationMobile) {
            this.notificationMobile = notificationMobile;
        }

        public int getSaleOrderItemCount() {
            return saleOrderItemCount;
        }

        public int getShipmentCount() {
            return shipmentCount;
        }

        public int getInvoiceCount() {
            return invoiceCount;
        }

        public int getReturnsCount() {
            return returnsCount;
        }

        public int getActivityCount() {
            return activityCount;
        }

        public void setSaleOrderItemCount(int saleOrderItemCount) {
            this.saleOrderItemCount = saleOrderItemCount;
        }

        public void setShipmentCount(int shipmentCount) {
            this.shipmentCount = shipmentCount;
        }

        public void setInvoiceCount(int invoiceCount) {
            this.invoiceCount = invoiceCount;
        }

        public void setReturnsCount(int returnsCount) {
            this.returnsCount = returnsCount;
        }

        public void setActivityCount(int activityCount) {
            this.activityCount = activityCount;
        }

        public boolean isPodRequired() {
            return podRequired;
        }

        public void setPodRequired(boolean podRequired) {
            this.podRequired = podRequired;
        }
    }

    public static class ItemStatusDTO {
        private int totalItems;
        private int inProcess;
        private int cancellable;
        private int unfulfillable;
        private int dispatched;
        private int cancelled;
        private int returned;
        private int replaced;
        private int reshipped;

        public int getTotalItems() {
            return totalItems;
        }

        public void setTotalItems(int totalItems) {
            this.totalItems = totalItems;
        }

        public int getInProcess() {
            return inProcess;
        }

        public void setInProcess(int inProgress) {
            this.inProcess = inProgress;
        }

        public int getCancellable() {
            return cancellable;
        }

        public void setCancellable(int cancellable) {
            this.cancellable = cancellable;
        }

        public int getUnfulfillable() {
            return unfulfillable;
        }

        public void setUnfulfillable(int unfulfillable) {
            this.unfulfillable = unfulfillable;
        }

        public int getDispatched() {
            return dispatched;
        }

        public void setDispatched(int dispatched) {
            this.dispatched = dispatched;
        }

        public int getCancelled() {
            return cancelled;
        }

        public void setCancelled(int cancelled) {
            this.cancelled = cancelled;
        }

        public int getReturned() {
            return returned;
        }

        public void setReturned(int returned) {
            this.returned = returned;
        }

        public int getReplaced() {
            return replaced;
        }

        public void setReplaced(int replaced) {
            this.replaced = replaced;
        }

        public int getReshipped() {
            return reshipped;
        }

        public void setReshipped(int reshipped) {
            this.reshipped = reshipped;
        }

    }
}
