/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 17-May-2013
 *  @author praveeng
 */
package com.uniware.core.api.material;

import com.unifier.core.api.base.ServiceRequest;

import org.hibernate.validator.constraints.NotBlank;

/**
 * @author praveeng
 */
public class GetInboundGatePassRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -5732681444672076935L;

    @NotBlank
    public String             code;

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

}
