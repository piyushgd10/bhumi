/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, May 1, 2012
 *  @author singla
 */
package com.uniware.core.api.inflow;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author singla
 */
public class CreateReceiptItemCodesResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 6659260161275928548L;

}
