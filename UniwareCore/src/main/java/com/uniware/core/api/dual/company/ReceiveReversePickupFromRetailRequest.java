/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 25-Mar-2014
 *  @author karunsingla
 */
package com.uniware.core.api.dual.company;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import com.unifier.core.api.base.ServiceRequest;
import com.unifier.core.api.customfields.WsCustomFieldValue;

public class ReceiveReversePickupFromRetailRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long               serialVersionUID = 1408848297149690141L;

    @NotEmpty
    @Valid
    private List<WsRetailReversePickupItem> reversePickupItems;

    @NotNull
    private Integer                         userId;
    
    @Valid
    private List<WsCustomFieldValue> customFieldValues;

    public static class WsRetailReversePickupItem {
        @NotBlank
        public String        saleOrderCode;

        @NotEmpty
        private List<String> itemCodes;
        
        public String getSaleOrderCode() {
            return saleOrderCode;
        }

        public void setSaleOrderCode(String saleOrderCode) {
            this.saleOrderCode = saleOrderCode;
        }

        public List<String> getItemCodes() {
            return itemCodes;
        }

        public void setItemCodes(List<String> itemCodes) {
            this.itemCodes = itemCodes;
        }
        
    }

    public List<WsRetailReversePickupItem> getReversePickupItems() {
        return reversePickupItems;
    }

    public void setReversePickupItems(List<WsRetailReversePickupItem> reversePickupItems) {
        this.reversePickupItems = reversePickupItems;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }
    
    public List<WsCustomFieldValue> getCustomFieldValues() {
        return customFieldValues;
    }

    public void setCustomFieldValues(List<WsCustomFieldValue> customFieldValues) {
        this.customFieldValues = customFieldValues;
    }
}
