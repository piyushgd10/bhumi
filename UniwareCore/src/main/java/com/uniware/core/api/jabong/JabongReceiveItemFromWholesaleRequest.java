/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jul 4, 2012
 *  @author singla
 */
package com.uniware.core.api.jabong;

import com.unifier.core.api.base.ServiceRequest;

import java.util.List;

import org.hibernate.validator.constraints.NotBlank;

public class JabongReceiveItemFromWholesaleRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long                    serialVersionUID = -8098562252436822947L;

    private List<WsJabongWholesaleSaleOrderItem> saleOrderItems;

    public static class WsJabongWholesaleSaleOrderItem {
        @NotBlank
        private String saleOrderItemCode;

        @NotBlank
        private String itemCode;

        /**
         * @return the saleOrderItemCode
         */
        public String getSaleOrderItemCode() {
            return saleOrderItemCode;
        }

        /**
         * @param saleOrderItemCode the saleOrderItemCode to set
         */
        public void setSaleOrderItemCode(String saleOrderItemCode) {
            this.saleOrderItemCode = saleOrderItemCode;
        }

        /**
         * @return the itemCode
         */
        public String getItemCode() {
            return itemCode;
        }

        /**
         * @param itemCode the itemCode to set
         */
        public void setItemCode(String itemCode) {
            this.itemCode = itemCode;
        }
    }

    public List<WsJabongWholesaleSaleOrderItem> getSaleOrderItems() {
        return saleOrderItems;
    }

    public void setSaleOrderItems(List<WsJabongWholesaleSaleOrderItem> saleOrderItems) {
        this.saleOrderItems = saleOrderItems;
    }
}
