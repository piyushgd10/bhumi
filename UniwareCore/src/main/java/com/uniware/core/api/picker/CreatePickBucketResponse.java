/*
 * Copyright 2016 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 19/9/16 6:02 PM
 * @author bhuvneshwarkumar
 */

package com.uniware.core.api.picker;

import com.unifier.core.api.base.ServiceResponse;

/**
 * Created by bhuvneshwarkumar on 19/09/16.
 */
public class CreatePickBucketResponse extends ServiceResponse {
    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
