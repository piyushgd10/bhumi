/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jan 13, 2012
 *  @author singla
 */
package com.uniware.core.api.packer;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author singla
 */
public class ShippingPackageItemDTO {
    private String                   saleOrderItemCode;
    private String                   itemSku;
    private String                   itemName;
    private String                   shelfCode;
    private String                   itemCode;
    private String                   softAllocatedItemCode;
    private String                   statusCode;
    private String                   itemTypeImageUrl;
    private String                   itemTypePageUrl;
    private String                   color;
    private String                   size;
    private String                   brand;
    private String                   scanIdentifier;

    /**
     * @return the itemSku
     */
    public String getItemSku() {
        return itemSku;
    }

    /**
     * @param itemSku the itemSku to set
     */
    public void setItemSku(String itemSku) {
        this.itemSku = itemSku;
    }

    /**
     * @return the itemName
     */
    public String getItemName() {
        return itemName;
    }

    /**
     * @param itemName the itemName to set
     */
    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    /**
     * @return the shelfCode
     */
    public String getShelfCode() {
        return shelfCode;
    }

    /**
     * @param shelfCode the shelfCode to set
     */
    public void setShelfCode(String shelfCode) {
        this.shelfCode = shelfCode;
    }

    /**
     * @return the itemCode
     */
    public String getItemCode() {
        return itemCode;
    }

    /**
     * @param itemCode the itemCode to set
     */
    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getSoftAllocatedItemCode() {
        return softAllocatedItemCode;
    }

    public void setSoftAllocatedItemCode(String softAllocatedItemCode) {
        this.softAllocatedItemCode = softAllocatedItemCode;
    }

    /**
     * @return the itemTypeImageUrl
     */
    public String getItemTypeImageUrl() {
        return itemTypeImageUrl;
    }

    /**
     * @param itemTypeImageUrl the itemTypeImageUrl to set
     */
    public void setItemTypeImageUrl(String itemTypeImageUrl) {
        this.itemTypeImageUrl = itemTypeImageUrl;
    }

    /**
     * @return the itemTypePageUrl
     */
    public String getItemTypePageUrl() {
        return itemTypePageUrl;
    }

    /**
     * @param itemTypePageUrl the itemTypePageUrl to set
     */
    public void setItemTypePageUrl(String itemTypePageUrl) {
        this.itemTypePageUrl = itemTypePageUrl;
    }

    /**
     * @return the color
     */
    public String getColor() {
        return color;
    }

    /**
     * @param color the color to set
     */
    public void setColor(String color) {
        this.color = color;
    }

    /**
     * @return the size
     */
    public String getSize() {
        return size;
    }

    /**
     * @param size the size to set
     */
    public void setSize(String size) {
        this.size = size;
    }

    /**
     * @return the brand
     */
    public String getBrand() {
        return brand;
    }

    /**
     * @param brand the brand to set
     */
    public void setBrand(String brand) {
        this.brand = brand;
    }

    /**
     * @return the scanIdentifier
     */
    public String getScanIdentifier() {
        return scanIdentifier;
    }

    /**
     * @param scanIdentifier the scanIdentifier to set
     */
    public void setScanIdentifier(String scanIdentifier) {
        this.scanIdentifier = scanIdentifier;
    }

    public String getSaleOrderItemCode() {
        return saleOrderItemCode;
    }

    public void setSaleOrderItemCode(String saleOrderItemCode) {
        this.saleOrderItemCode = saleOrderItemCode;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }
}
