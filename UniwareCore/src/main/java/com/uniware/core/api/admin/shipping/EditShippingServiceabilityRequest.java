/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 17-Feb-2015
 *  @author parijat
 */
package com.uniware.core.api.admin.shipping;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author parijat
 *
 */
public class EditShippingServiceabilityRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 4718031740286500438L;

    @NotBlank
    private String            shippingProviderCode;

    @NotBlank
    private String            serviceabilityCode;

    /**
     * @return the shippingProviderCode
     */
    public String getShippingProviderCode() {
        return shippingProviderCode;
    }

    /**
     * @param shippingProviderCode the shippingProviderCode to set
     */
    public void setShippingProviderCode(String shippingProviderCode) {
        this.shippingProviderCode = shippingProviderCode;
    }

    /**
     * @return the serviceabilityCode
     */
    public String getServiceabilityCode() {
        return serviceabilityCode;
    }

    /**
     * @param serviceabilityCode the serviceabilityCode to set
     */
    public void setServiceabilityCode(String serviceabilityCode) {
        this.serviceabilityCode = serviceabilityCode;
    }

}
