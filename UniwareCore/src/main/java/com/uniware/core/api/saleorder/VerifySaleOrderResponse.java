/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 15, 2012
 *  @author praveeng
 */
package com.uniware.core.api.saleorder;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author Sunny Agarwal
 */
public class VerifySaleOrderResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 3731332708262428826L;

}
