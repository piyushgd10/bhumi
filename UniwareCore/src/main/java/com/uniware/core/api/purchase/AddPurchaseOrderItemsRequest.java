/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 29, 2012
 *  @author singla
 */
package com.uniware.core.api.purchase;

import com.unifier.core.api.base.ServiceRequest;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * @author singla
 */
public class AddPurchaseOrderItemsRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long         serialVersionUID = 1383325854258959712L;

    @NotNull
    private String                    purchaseOrderCode;

    @NotNull
    @Valid
    private List<WsPurchaseOrderItem> purchaseOrderItems;

    /**
     * @return the purchaseOrderCode
     */
    public String getPurchaseOrderCode() {
        return purchaseOrderCode;
    }

    /**
     * @param purchaseOrderCode the purchaseOrderCode to set
     */
    public void setPurchaseOrderCode(String purchaseOrderCode) {
        this.purchaseOrderCode = purchaseOrderCode;
    }

    /**
     * @return the purchaseOrderItems
     */
    public List<WsPurchaseOrderItem> getPurchaseOrderItems() {
        return purchaseOrderItems;
    }

    /**
     * @param purchaseOrderItems the purchaseOrderItems to set
     */
    public void setPurchaseOrderItems(List<WsPurchaseOrderItem> purchaseOrderItems) {
        this.purchaseOrderItems = purchaseOrderItems;
    }

}
