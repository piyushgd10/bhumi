/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 09-Feb-2013
 *  @author praveeng
 */
package com.uniware.core.api.shipping;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author praveeng
 */
public class MarkPackagesReconciledResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 2535804469471240826L;

}
