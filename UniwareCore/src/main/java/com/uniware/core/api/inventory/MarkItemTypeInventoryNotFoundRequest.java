/*
 * Copyright 2017 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 07/11/17
 * @author piyush
 */
package com.uniware.core.api.inventory;

import com.unifier.core.api.base.ServiceRequest;
import org.hibernate.validator.constraints.NotBlank;

public class MarkItemTypeInventoryNotFoundRequest extends ServiceRequest {

    @NotBlank
    private String            itemSku;

    @NotBlank
    private String            shelfCode;

    public MarkItemTypeInventoryNotFoundRequest(String skuCode, String shelfCode) {
        this.itemSku = skuCode;
        this.shelfCode = shelfCode;
    }

    public String getItemSku() {
        return itemSku;
    }

    public void setItemSku(String itemSku) {
        this.itemSku = itemSku;
    }

    public String getShelfCode() {
        return shelfCode;
    }

    public void setShelfCode(String shelfCode) {
        this.shelfCode = shelfCode;
    }
}
