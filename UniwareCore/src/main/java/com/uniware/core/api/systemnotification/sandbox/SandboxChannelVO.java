/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 25-Dec-2013
 *  @author sunny
 */
package com.uniware.core.api.systemnotification.sandbox;

import com.unifier.core.configuration.ConfigurationManager;
import com.uniware.core.entity.Channel;
import com.uniware.core.entity.Source;

public class SandboxChannelVO {

    private String            code;
    private String            name;
    private boolean           enabled;
    private String            orderSyncStatus;
    private String            inventorySyncStatus;
    private boolean           thirdPartyShipping;
    private Integer           tat;
    private boolean           notificationsEnabled;
    private String            inventoryUpdateFormula;
    private boolean           allowCombinedManifest;
    private boolean           autoVerifyOrders;
    private String            packageType;
    private Integer           inventoryAllocationPriority;
    private String            httpProxyHost;
    private String            httpProxyPort;
    private SandboxCustomerVO customer;

    public SandboxChannelVO() {
        super();
    }

    public SandboxChannelVO(Channel channel) {
        code = channel.getCode();
        name = channel.getName();
        enabled = channel.isEnabled();
        orderSyncStatus = channel.getOrderSyncStatus().name();
        inventorySyncStatus = channel.getInventorySyncStatus().name();
        thirdPartyShipping = channel.isThirdPartyShipping();
        tat = channel.getTat();
        notificationsEnabled = channel.isNotificationsEnabled();
        inventoryUpdateFormula = channel.getInventoryUpdateFormula();
        allowCombinedManifest = channel.isAllowCombinedManifest();
        autoVerifyOrders = channel.isAutoVerifyOrders();
        packageType = channel.getPackageType();
        inventoryAllocationPriority = channel.getInventoryAllocationPriority();
        httpProxyHost = channel.getHttpProxyHost();
        httpProxyPort = channel.getHttpProxyPort();
        if (channel.getCustomer() != null) {
            customer = new SandboxCustomerVO(channel.getCustomer());
        }
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getOrderSyncStatus() {
        return orderSyncStatus;
    }

    public void setOrderSyncStatus(String orderSyncStatus) {
        this.orderSyncStatus = orderSyncStatus;
    }

    public String getInventorySyncStatus() {
        return inventorySyncStatus;
    }

    public void setInventorySyncStatus(String inventorySyncStatus) {
        this.inventorySyncStatus = inventorySyncStatus;
    }

    public boolean isThirdPartyShipping() {
        return thirdPartyShipping;
    }

    public void setThirdPartyShipping(boolean thirdPartyShipping) {
        this.thirdPartyShipping = thirdPartyShipping;
    }

    public Integer getTat() {
        return tat;
    }

    public void setTat(Integer tat) {
        this.tat = tat;
    }

    public boolean isNotificationsEnabled() {
        return notificationsEnabled;
    }

    public void setNotificationsEnabled(boolean notificationsEnabled) {
        this.notificationsEnabled = notificationsEnabled;
    }

    public String getInventoryUpdateFormula() {
        return inventoryUpdateFormula;
    }

    public void setInventoryUpdateFormula(String inventoryUpdateFormula) {
        this.inventoryUpdateFormula = inventoryUpdateFormula;
    }

    public boolean isAllowCombinedManifest() {
        return allowCombinedManifest;
    }

    public void setAllowCombinedManifest(boolean allowCombinedManifest) {
        this.allowCombinedManifest = allowCombinedManifest;
    }

    public boolean isAutoVerifyOrders() {
        return autoVerifyOrders;
    }

    public void setAutoVerifyOrders(boolean autoVerifyOrders) {
        this.autoVerifyOrders = autoVerifyOrders;
    }

    public String getPackageType() {
        return packageType;
    }

    public void setPackageType(String packageType) {
        this.packageType = packageType;
    }

    public Integer getInventoryAllocationPriority() {
        return inventoryAllocationPriority;
    }

    public void setInventoryAllocationPriority(Integer inventoryAllocationPriority) {
        this.inventoryAllocationPriority = inventoryAllocationPriority;
    }

    public String getHttpProxyHost() {
        return httpProxyHost;
    }

    public void setHttpProxyHost(String httpProxyHost) {
        this.httpProxyHost = httpProxyHost;
    }

    public String getHttpProxyPort() {
        return httpProxyPort;
    }

    public void setHttpProxyPort(String httpProxyPort) {
        this.httpProxyPort = httpProxyPort;
    }

    public SandboxCustomerVO getCustomer() {
        return customer;
    }

    public void setCustomer(SandboxCustomerVO customer) {
        this.customer = customer;
    }

}
