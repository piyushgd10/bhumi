/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 24-Dec-2011
 *  @author vibhu
 */
package com.uniware.core.api.admin.pickset;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author vibhu
 */
public class EditPicksetResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 2620227928985499987L;
    private PicksetDTO        picksetDTO;

    /**
     * @return the picksetDTO
     */
    public PicksetDTO getPicksetDTO() {
        return picksetDTO;
    }

    /**
     * @param picksetDTO the picksetDTO to set
     */
    public void setPicksetDTO(PicksetDTO picksetDTO) {
        this.picksetDTO = picksetDTO;
    }

}