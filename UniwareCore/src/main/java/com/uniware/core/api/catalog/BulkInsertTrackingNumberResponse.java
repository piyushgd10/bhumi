/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 27, 2012
 *  @author praveeng
 */
package com.uniware.core.api.catalog;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author praveeng
 */
public class BulkInsertTrackingNumberResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -3675624153635821273L;

}
