/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 3, 2012
 *  @author praveeng
 */
package com.uniware.core.api.inflow;

import com.unifier.core.api.base.ServiceRequest;

import javax.validation.constraints.NotNull;

/**
 * @author praveeng
 */
public class GetInflowReceiptRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -327683044329999162L;

    @NotNull
    private String            inflowReceiptCode;

    public GetInflowReceiptRequest() {

    }

    public GetInflowReceiptRequest(String inflowReceiptCode) {
        this.inflowReceiptCode = inflowReceiptCode;
    }

    /**
     * @return the inflowReceiptCode
     */
    public String getInflowReceiptCode() {
        return inflowReceiptCode;
    }

    /**
     * @param inflowReceiptCode the inflowReceiptCode to set
     */
    public void setInflowReceiptCode(String inflowReceiptCode) {
        this.inflowReceiptCode = inflowReceiptCode;
    }

}