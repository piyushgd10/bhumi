/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jun 30, 2012
 *  @author singla
 */
package com.uniware.core.api.material;

import com.unifier.core.api.base.ServiceResponse;

public class EditInboundGatePassResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long  serialVersionUID = 1238808324906830823L;

    private InboundGatePassDTO inboundGatePassDTO;

    /**
     * @return the inboundGatePassDTO
     */
    public InboundGatePassDTO getInboundGatePassDTO() {
        return inboundGatePassDTO;
    }

    /**
     * @param inboundGatePassDTO the inboundGatePassDTO to set
     */
    public void setInboundGatePassDTO(InboundGatePassDTO inboundGatePassDTO) {
        this.inboundGatePassDTO = inboundGatePassDTO;
    }

}
