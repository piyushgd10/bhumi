/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 25-Sep-2012
 *  @author praveeng
 */
package com.uniware.core.api.saleorder;

import com.unifier.core.api.base.ServiceRequest;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import com.uniware.core.api.model.WsAddressDetail;
import com.uniware.core.api.model.WsAddressRef;

/**
 * @author praveeng
 */
public class EditSaleOrderAddressRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long  serialVersionUID = -224822924494717873L;

    @NotNull
    @Valid
    private WsSaleOrderAddress saleOrderAddress;

    /**
     * @return the saleOrderAddress
     */
    public WsSaleOrderAddress getSaleOrderAddress() {
        return saleOrderAddress;
    }

    /**
     * @param saleOrderAddress the saleOrderAddress to set
     */
    public void setSaleOrderAddress(WsSaleOrderAddress saleOrderAddress) {
        this.saleOrderAddress = saleOrderAddress;
    }

    public static class WsSaleOrderAddress {

        @NotBlank
        private String                       saleOrderCode;

        @NotEmpty
        @Valid
        private List<WsAddressDetail>        addresses;

        @Valid
        private WsAddressRef                 billingAddress;

        @NotNull
        @Valid
        private WsAddressRef                 shippingAddress;

        @Valid
        private List<WsSaleOrderAddressItem> saleOrderAddressItems;

        /**
         * @return the saleOrderCode
         */
        public String getSaleOrderCode() {
            return saleOrderCode;
        }

        /**
         * @param saleOrderCode the saleOrderCode to set
         */
        public void setSaleOrderCode(String saleOrderCode) {
            this.saleOrderCode = saleOrderCode;
        }

        /**
         * @return the addresses
         */
        public List<WsAddressDetail> getAddresses() {
            return addresses;
        }

        /**
         * @param addresses the addresses to set
         */
        public void setAddresses(List<WsAddressDetail> addresses) {
            this.addresses = addresses;
        }

        /**
         * @return the billingAddress
         */
        public WsAddressRef getBillingAddress() {
            return billingAddress;
        }

        /**
         * @param billingAddress the billingAddress to set
         */
        public void setBillingAddress(WsAddressRef billingAddress) {
            this.billingAddress = billingAddress;
        }

        /**
         * @return the shippingAddress
         */
        public WsAddressRef getShippingAddress() {
            return shippingAddress;
        }

        /**
         * @param shippingAddress the shippingAddress to set
         */
        public void setShippingAddress(WsAddressRef shippingAddress) {
            this.shippingAddress = shippingAddress;
        }

        /**
         * @return the saleOrderAddressItems
         */
        public List<WsSaleOrderAddressItem> getSaleOrderAddressItems() {
            return saleOrderAddressItems;
        }

        /**
         * @param saleOrderAddressItems the saleOrderAddressItems to set
         */
        public void setSaleOrderAddressItems(List<WsSaleOrderAddressItem> saleOrderAddressItems) {
            this.saleOrderAddressItems = saleOrderAddressItems;
        }

    }

    public static class WsSaleOrderAddressItem {
        @NotBlank
        private String       saleOrderItemCode;

        @Valid
        @NotNull
        private WsAddressRef shippingAddress;

        /**
         * @return the saleOrderItemCode
         */
        public String getSaleOrderItemCode() {
            return saleOrderItemCode;
        }

        /**
         * @param saleOrderItemCode the saleOrderItemCode to set
         */
        public void setSaleOrderItemCode(String saleOrderItemCode) {
            this.saleOrderItemCode = saleOrderItemCode;
        }

        /**
         * @return the shippingAddress
         */
        public WsAddressRef getShippingAddress() {
            return shippingAddress;
        }

        /**
         * @param shippingAddress the shippingAddress to set
         */
        public void setShippingAddress(WsAddressRef shippingAddress) {
            this.shippingAddress = shippingAddress;
        }

    }

}
