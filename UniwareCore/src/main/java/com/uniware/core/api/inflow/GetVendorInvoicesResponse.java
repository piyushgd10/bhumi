/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 *  @version     1.0, May 25, 2015
 *  @author akshay
 */
package com.uniware.core.api.inflow;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.unifier.core.api.base.ServiceResponse;

public class GetVendorInvoicesResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long            serialVersionUID = 5570046405801860433L;

    private List<VendorInvoiceDetailDTO> vandorInvoices   = new ArrayList<>();

    public List<VendorInvoiceDetailDTO> getVandorInvoices() {
        return vandorInvoices;
    }

    public void setVandorInvoices(List<VendorInvoiceDetailDTO> vandorInvoices) {
        this.vandorInvoices = vandorInvoices;
    }

    public static class VendorInvoiceDetailDTO {

        public VendorInvoiceDetailDTO() {
        }

        private String     code;
        private String     status;
        private String     createdBy;
        private String     type;
        private String     invoiceAmount;
        private int        noOfItems;
        private BigDecimal amount;
        private String     grnAdded;
        private Date       createdOn;
        private Date       invoiceDate;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getCreatedBy() {
            return createdBy;
        }

        public void setCreatedBy(String createdBy) {
            this.createdBy = createdBy;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getInvoiceAmount() {
            return invoiceAmount;
        }

        public void setInvoiceAmount(String invoiceAmount) {
            this.invoiceAmount = invoiceAmount;
        }

        public int getNoOfItems() {
            return noOfItems;
        }

        public void setNoOfItems(int noOfItems) {
            this.noOfItems = noOfItems;
        }

        public String getGrnAdded() {
            return grnAdded;
        }

        public void setGrnAdded(String grnAdded) {
            this.grnAdded = grnAdded;
        }

        public Date getCreatedOn() {
            return createdOn;
        }

        public void setCreatedOn(Date createdOn) {
            this.createdOn = createdOn;
        }

        public Date getInvoiceDate() {
            return invoiceDate;
        }

        public void setInvoiceDate(Date invoiceDate) {
            this.invoiceDate = invoiceDate;
        }

        public BigDecimal getAmount() {
            return amount;
        }

        public void setAmount(BigDecimal amount) {
            this.amount = amount;
        }
    }

}
