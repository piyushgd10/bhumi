/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 02-Nov-2014
 *  @author parijat
 */
package com.uniware.core.api.channel;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author parijat
 *
 */
public class GetChannelReconciliationSyncStatusResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long                        serialVersionUID = 344278789785758769L;

    private ChannelReconciliationInvoiceSyncStatusVO statusVO;

    /**
     * @return the statusVO
     */
    public ChannelReconciliationInvoiceSyncStatusVO getStatusVO() {
        return statusVO;
    }

    /**
     * @param statusVO the statusVO to set
     */
    public void setStatusVO(ChannelReconciliationInvoiceSyncStatusVO statusVO) {
        this.statusVO = statusVO;
    }

}
