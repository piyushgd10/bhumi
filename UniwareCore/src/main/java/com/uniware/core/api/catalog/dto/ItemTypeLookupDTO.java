/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 02-May-2012
 *  @author praveeng
 */
package com.uniware.core.api.catalog.dto;

import com.uniware.core.api.item.ItemDetailFieldDTO;
import java.math.BigDecimal;

import java.util.List;
import org.apache.commons.lang.builder.HashCodeBuilder;

public class ItemTypeLookupDTO {
    private Integer    itemTypeId;
    private String     itemSKU;
    private String     name;
    private String     scanIdentifier;
    private BigDecimal maxRetailPrice;
    private BigDecimal basePrice;
    private BigDecimal costPrice;
    private String     imageUrl;
    private List<ItemDetailFieldDTO> itemDetailFieldDTOList;

    public ItemTypeLookupDTO() {
        super();
    }

    @Override
    public boolean equals(Object obj){
        if(obj == null || !(obj instanceof ItemTypeLookupDTO)){
            return false;
        }
        ItemTypeLookupDTO object = (ItemTypeLookupDTO)obj;
        return object.itemSKU.equals(this.itemSKU) && object.scanIdentifier.equals(this.scanIdentifier);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(itemSKU).append(scanIdentifier).hashCode();
    }

    public Integer getItemTypeId() {
        return itemTypeId;
    }

    public void setItemTypeId(Integer itemTypeId) {
        this.itemTypeId = itemTypeId;
    }

    public String getItemSKU() {
        return itemSKU;
    }

    public void setItemSKU(String itemSKU) {
        this.itemSKU = itemSKU;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getScanIdentifier() {
        return scanIdentifier;
    }

    public void setScanIdentifier(String scanIdentifier) {
        this.scanIdentifier = scanIdentifier;
    }

    public BigDecimal getMaxRetailPrice() {
        return maxRetailPrice;
    }

    public void setMaxRetailPrice(BigDecimal maxRetailPrice) {
        this.maxRetailPrice = maxRetailPrice;
    }

    public BigDecimal getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(BigDecimal basePrice) {
        this.basePrice = basePrice;
    }
    public BigDecimal getCostPrice() {
        return costPrice;
    }

    public void setCostPrice(BigDecimal costPrice) {
        this.costPrice = costPrice;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public List<ItemDetailFieldDTO> getItemDetailFieldDTOList() {
        return itemDetailFieldDTOList;
    }

    public void setItemDetailFieldDTOList(List<ItemDetailFieldDTO> itemDetailFieldDTOList) {
        this.itemDetailFieldDTOList = itemDetailFieldDTOList;
    }

    @Override
    public String toString() {
        return "ItemTypeLookupDTO [itemSKU=" + itemSKU + ", name=" + name + ", scanIdentifier=" + scanIdentifier + ", maxRetailPrice=" + maxRetailPrice + ", basePrice="
                + basePrice + ", imageUrl=" + imageUrl + "]";
    }

}
