/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 05-Sep-2013
 *  @author parijat
 */
package com.uniware.core.api.channel;

import org.hibernate.validator.constraints.NotBlank;

/**
 * @author parijat
 * Use this object to change status of any entity 
 *
 */
public class WsChangeStatus {
    
    @NotBlank
    private String entityIdentifier;

    private boolean enabled;
    
    

    public WsChangeStatus(String entityIdentifier, boolean enabled) {
        this.entityIdentifier = entityIdentifier;
        this.enabled = enabled;
    }

    public String getEntityIdentifier() {
        return entityIdentifier;
    }

    public void setEntityIdentifier(String entityIdentifier) {
        this.entityIdentifier = entityIdentifier;
    }

    /**
     * @return the enabled
     */
    public boolean isEnabled() {
        return enabled;
    }

    /**
     * @param enabled the enabled to set
     */
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

}
