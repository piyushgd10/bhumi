/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 24-Dec-2011
 *  @author vibhu
 */
package com.uniware.core.api.admin.pickset;

import static com.uniware.core.entity.PickArea.DEFAULT_PICK_AREA;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.unifier.core.api.base.ServiceRequest;
import com.uniware.core.entity.PickSet;

/**
 * @author vibhu
 */
public class CreatePicksetRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -8441687950733318686L;
    @NotEmpty
    private String            name;
    @NotNull
    private Integer           capacity;

    private PickSet.Type      pickSetType      = PickSet.Type.STOCKING;

    private String            pickAreaName     = DEFAULT_PICK_AREA;

    private boolean           useAsDefault;

    public CreatePicksetRequest() {
        super();
    }

    public CreatePicksetRequest(String name, Integer capacity, Boolean useAsDefault) {
        super();
        this.name = name;
        this.capacity = capacity;
        this.useAsDefault = useAsDefault;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the capacity
     */
    public Integer getCapacity() {
        return capacity;
    }

    /**
     * @param capacity the capacity to set
     */
    public void setCapacity(Integer capacity) {
        this.capacity = capacity;
    }

    public boolean isUseAsDefault() {
        return useAsDefault;
    }

    public void setUseAsDefault(boolean useAsDefault) {
        this.useAsDefault = useAsDefault;
    }

    public PickSet.Type getPickSetType() {
        return pickSetType;
    }

    public void setPickSetType(PickSet.Type pickSetType) {
        this.pickSetType = pickSetType;
    }

    public String getPickAreaName() {
        return pickAreaName;
    }

    public void setPickAreaName(String pickAreaName) {
        this.pickAreaName = pickAreaName;
    }
}