/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jul 1, 2012
 *  @author singla
 */
package com.uniware.core.api.material;

import com.unifier.core.api.base.ServiceResponse;

public class CompleteGatePassResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -1507850777349304286L;

}
