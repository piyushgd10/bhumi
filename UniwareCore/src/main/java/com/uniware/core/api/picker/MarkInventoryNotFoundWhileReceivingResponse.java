package com.uniware.core.api.picker;

import com.unifier.core.api.base.ServiceResponse;

/**
 * Created by admin on 04/12/17.
 */
public class MarkInventoryNotFoundWhileReceivingResponse extends ServiceResponse {

    private static final long serialVersionUID = -2098104976733L;
    private PicklistDTO       picklist;

    public void setPicklist(PicklistDTO picklist) {
        this.picklist = picklist;
    }

    public PicklistDTO getPicklist() {
        return picklist;
    }
}
