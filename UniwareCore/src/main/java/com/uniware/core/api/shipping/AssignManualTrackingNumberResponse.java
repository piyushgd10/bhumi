/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, May 24, 2012
 *  @author singla
 */
package com.uniware.core.api.shipping;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author singla
 */
public class AssignManualTrackingNumberResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -1309404691036021722L;

    private String            shippingPackageCode;
    private String            shippingProviderCode;
    private String            trackingNumber;

    public String getShippingPackageCode() {
        return shippingPackageCode;
    }

    public void setShippingPackageCode(String shippingPackageCode) {
        this.shippingPackageCode = shippingPackageCode;
    }

    public String getShippingProviderCode() {
        return shippingProviderCode;
    }

    public void setShippingProviderCode(String shippingProviderCode) {
        this.shippingProviderCode = shippingProviderCode;
    }

    public String getTrackingNumber() {
        return trackingNumber;
    }

    public void setTrackingNumber(String trackingNumber) {
        this.trackingNumber = trackingNumber;
    }

}
