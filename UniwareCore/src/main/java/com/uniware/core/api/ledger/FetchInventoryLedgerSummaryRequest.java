/**
 * Copyright 2017 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version 1.0, 19/09/17
 * @author aditya
 */
package com.uniware.core.api.ledger;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

/**
 *
 */
public class FetchInventoryLedgerSummaryRequest extends ServiceRequest {

    @NotBlank
    private String       skuCode;

    @Valid
    private Date         from;

    @Valid
    private Date         to;

    private List<String> facilityCodes;

    public String getSkuCode() {
        return skuCode;
    }

    public void setSkuCode(String skuCode) {
        this.skuCode = skuCode;
    }

    public Date getFrom() {
        return from;
    }

    public void setFrom(Date from) {
        this.from = from;
    }

    public Date getTo() {
        return to;
    }

    public void setTo(Date to) {
        this.to = to;
    }

    public List<String> getFacilityCodes() {
        return facilityCodes;
    }

    public void setFacilityCodes(List<String> facilityCodes) {
        this.facilityCodes = facilityCodes;
    }

    @Override public String toString() {
        return "FetchInventoryLedgerSummaryRequest{" + "skuCode='" + skuCode + '\'' + ", from=" + from + ", to=" + to
                + ", facilityCodes=" + facilityCodes + '}';
    }
}
