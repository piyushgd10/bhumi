/*
 * Copyright 2016 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 15/9/16 3:16 PM
 * @author bhuvneshwarkumar
 */

package com.uniware.core.api.systemConfig;

import java.util.List;

import com.unifier.core.api.base.ServiceRequest;

/**
 * Created by bhuvneshwarkumar on 15/09/16.
 */
public class GetSystemConfigurationByNameRequest extends ServiceRequest {

    private List<String> configNames;

    public List<String> getConfigNames() {
        return configNames;
    }

    public void setConfigNames(List<String> configNames) {
        this.configNames = configNames;
    }
}
