/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 30-Sep-2014
 *  @author parijat
 */
package com.uniware.core.api.channel;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Id;

import org.springframework.data.mongodb.core.mapping.Document;

import com.uniware.core.entity.Channel;

/**
 * @author parijat
 */
@Document(collection = "channelReconciliationSyncStatus")
//@CompoundIndexes({ @CompoundIndex(name = "tenant_channel", def = "{'tenantCode':1,'channelCode':1}", unique = true) })
public class ChannelReconciliationInvoiceSyncStatusVO implements Serializable {

    @Id
    private String                      id;

    private String                      tenantCode;
    private String                      channelCode;
    private Channel.SyncExecutionStatus syncExecutionStatus = Channel.SyncExecutionStatus.IDLE;
    private boolean                     interrupted;
    private boolean                     successful;
    private long                        totalMileStones     = 100;
    private long                        currentMileStone;
    private String                      message;
    private int                         successfulImports;
    private int                         failedImports;
    private int                         successfulProcessed;
    private int                         failedProcessed;
    private int                         ignoredProcessed;
    private String                      failedLog;
    private Date                        lastSyncTime;
    private Date                        lastSyncFailedNotificationTime;

    public ChannelReconciliationInvoiceSyncStatusVO(String channelCode) {
        this.channelCode = channelCode;
    }

    public float getPercentageComplete() {
        if (totalMileStones == 0) {
            return 0;
        }
        return ((float) (currentMileStone * 10000 / totalMileStones)) / 100;
    }

    public void reset() {
        totalMileStones = 100;
        currentMileStone = 0;
        interrupted = false;
        successful = false;
        message = null;
        successfulImports = 0;
        failedImports = 0;
    }

    public void setMileStone(String message) {
        currentMileStone++;
        this.message = message;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the tenantCode
     */
    public String getTenantCode() {
        return tenantCode;
    }

    /**
     * @param tenantCode the tenantCode to set
     */
    public void setTenantCode(String tenantCode) {
        this.tenantCode = tenantCode;
    }

    /**
     * @return the channelCode
     */
    public String getChannelCode() {
        return channelCode;
    }

    /**
     * @param channelCode the channelCode to set
     */
    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    /**
     * @return the interrupted
     */
    public boolean isInterrupted() {
        return interrupted;
    }

    /**
     * @param interrupted the interrupted to set
     */
    public void setInterrupted(boolean interrupted) {
        this.interrupted = interrupted;
    }

    /**
     * @return the successful
     */
    public boolean isSuccessful() {
        return successful;
    }

    /**
     * @param successful the successful to set
     */
    public void setSuccessful(boolean successful) {
        this.successful = successful;
    }

    /**
     * @return the totalMileStones
     */
    public long getTotalMileStones() {
        return totalMileStones;
    }

    /**
     * @param totalMileStones the totalMileStones to set
     */
    public void setTotalMileStones(long totalMileStones) {
        this.totalMileStones = totalMileStones;
    }

    /**
     * @return the currentMileStone
     */
    public long getCurrentMileStone() {
        return currentMileStone;
    }

    /**
     * @param currentMileStone the currentMileStone to set
     */
    public void setCurrentMileStone(long currentMileStone) {
        this.currentMileStone = currentMileStone;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return the successfulImports
     */
    public int getSuccessfulImports() {
        return successfulImports;
    }

    /**
     * @param successfulImports the successfulImports to set
     */
    public void setSuccessfulImports(int successfulImports) {
        this.successfulImports = successfulImports;
    }

    /**
     * @return the failedImports
     */
    public int getFailedImports() {
        return failedImports;
    }

    /**
     * @param failedImports the failedImports to set
     */
    public void setFailedImports(int failedImports) {
        this.failedImports = failedImports;
    }

    /**
     * @return the successfulProcessed
     */
    public int getSuccessfulProcessed() {
        return successfulProcessed;
    }

    /**
     * @param successfulProcessed the successfulProcessed to set
     */
    public void setSuccessfulProcessed(int successfulProcessed) {
        this.successfulProcessed = successfulProcessed;
    }

    /**
     * @return the failedProcessed
     */
    public int getFailedProcessed() {
        return failedProcessed;
    }

    /**
     * @param failedProcessed the failedProcessed to set
     */
    public void setFailedProcessed(int failedProcessed) {
        this.failedProcessed = failedProcessed;
    }

    /**
     * @return the ignoredProcessed
     */
    public int getIgnoredProcessed() {
        return ignoredProcessed;
    }

    /**
     * @param ignoredProcessed the ignoredProcessed to set
     */
    public void setIgnoredProcessed(int ignoredProcessed) {
        this.ignoredProcessed = ignoredProcessed;
    }

    /**
     * @return the failedLog
     */
    public String getFailedLog() {
        return failedLog;
    }

    /**
     * @param failedLog the failedLog to set
     */
    public void setFailedLog(String failedLog) {
        this.failedLog = failedLog;
    }

    public Channel.SyncExecutionStatus getSyncExecutionStatus() {
        return syncExecutionStatus;
    }

    public void setSyncExecutionStatus(Channel.SyncExecutionStatus syncExecutionStatus) {
        this.syncExecutionStatus = syncExecutionStatus;
    }

    /**
     * @return the lastSyncTime
     */
    public Date getLastSyncTime() {
        return lastSyncTime;
    }

    /**
     * @param lastSyncTime the lastSyncTime to set
     */
    public void setLastSyncTime(Date lastSyncTime) {
        this.lastSyncTime = lastSyncTime;
    }

    /**
     * @return the lastSyncFailedNotificationTime
     */
    public Date getLastSyncFailedNotificationTime() {
        return lastSyncFailedNotificationTime;
    }

    /**
     * @param lastSyncFailedNotificationTime the lastSyncFailedNotificationTime to set
     */
    public void setLastSyncFailedNotificationTime(Date lastSyncFailedNotificationTime) {
        this.lastSyncFailedNotificationTime = lastSyncFailedNotificationTime;
    }

}
