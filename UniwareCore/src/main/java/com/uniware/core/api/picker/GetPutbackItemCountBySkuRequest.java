/*
 * Copyright 2017 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 03/11/17
 * @author piyush
 */
package com.uniware.core.api.picker;

import com.unifier.core.api.base.ServiceRequest;
import org.hibernate.validator.constraints.NotBlank;

public class GetPutbackItemCountBySkuRequest extends ServiceRequest {

    @NotBlank
    private String skuCode;

    public String getSkuCode() {
        return skuCode;
    }

    public void setSkuCode(String skuCode) {
        this.skuCode = skuCode;
    }
}
