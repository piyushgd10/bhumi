/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jun 20, 2012
 *  @author ankit
 */
package com.uniware.core.api.tenant;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author Sunny
 */
public class CleanupTenantDataResponse extends ServiceResponse {

    private static final long serialVersionUID = 1502610690451527378L;

}
