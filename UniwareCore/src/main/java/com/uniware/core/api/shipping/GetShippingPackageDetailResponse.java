/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 17-Dec-2013
 *  @author akshay
 */
package com.uniware.core.api.shipping;

import com.unifier.core.api.base.ServiceResponse;

public class GetShippingPackageDetailResponse extends ServiceResponse{

    /**
     * 
     */
    private static final long serialVersionUID = 820261635221513613L;

    private ShippingPackageDetailDTO shippingPackageDetailDTO;

    public ShippingPackageDetailDTO getShippingPackageDetailDTO() {
        return shippingPackageDetailDTO;
    }

    public void setShippingPackageDetailDTO(ShippingPackageDetailDTO shippingPackageDetailDTO) {
        this.shippingPackageDetailDTO = shippingPackageDetailDTO;
    }

}
