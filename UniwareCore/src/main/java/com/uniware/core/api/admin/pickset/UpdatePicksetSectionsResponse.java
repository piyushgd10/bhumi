/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 23-Feb-2012
 *  @author vibhu
 */
package com.uniware.core.api.admin.pickset;

import com.unifier.core.api.base.ServiceResponse;
import com.uniware.core.entity.Section;

import java.util.ArrayList;
import java.util.List;

/**
 * @author vibhu
 */
public class UpdatePicksetSectionsResponse extends ServiceResponse {

    /**
     *
     */
    private static final long                    serialVersionUID   = -3892276490428943163L;
    private              List<PicksetSectionDTO> picksetSectionDTOs = new ArrayList<PicksetSectionDTO>();

    /**
     * @return the picksetSectionDTOs
     */
    public List<PicksetSectionDTO> getPicksetSectionDTOs() {
        return picksetSectionDTOs;
    }

    /**
     * @param picksetSectionDTOs the picksetSectionDTOs to set
     */
    public void setPicksetSectionDTOs(List<PicksetSectionDTO> picksetSectionDTOs) {
        this.picksetSectionDTOs = picksetSectionDTOs;
    }

    public void addPicksetSection(Section section) {
        picksetSectionDTOs.add(new PicksetSectionDTO(section));
    }

}