/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 18-Apr-2013
 *  @author unicom
 */
package com.uniware.core.api.channel;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author Sunny Agarwal
 */
public class WsChannelConnector {

    @NotBlank
    private String channelCode;

    @NotBlank
    private String name;

    @NotEmpty
    private List<WsChannelConnectorParameter> channelConnectorParameters;

    private List<VerifyAndSyncParamsResponse.WsAuthenticationChallenge> challengeReplies;

    public WsChannelConnector() {
    }

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<WsChannelConnectorParameter> getChannelConnectorParameters() {
        return channelConnectorParameters;
    }

    public void setChannelConnectorParameters(List<WsChannelConnectorParameter> channelConnectorParameters) {
        this.channelConnectorParameters = channelConnectorParameters;
    }

    public void addChannelConnectorParameter(WsChannelConnectorParameter channelConnectorParameter) {
        if (this.channelConnectorParameters == null) {
            this.channelConnectorParameters = new ArrayList<WsChannelConnectorParameter>();
        }
        this.channelConnectorParameters.add(channelConnectorParameter);
    }

    public List<VerifyAndSyncParamsResponse.WsAuthenticationChallenge> getChallengeReplies() {
        return challengeReplies;
    }

    public void setChallengeReplies(List<VerifyAndSyncParamsResponse.WsAuthenticationChallenge> challengeReplies) {
        this.challengeReplies = challengeReplies;
    }
}
