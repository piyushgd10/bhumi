/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 7, 2012
 *  @author singla
 */
package com.uniware.core.api.shipping;

import java.util.List;

import com.unifier.core.api.base.ServiceResponse;
import com.uniware.core.entity.Channel;
import com.uniware.core.entity.Source;

/**
 * @author Sunny
 */
public class GetChannelsForManifestResponse extends ServiceResponse {

    /**
     *
     */
    private static final long serialVersionUID = -5193898301331038238L;

    private List<ChannelDTO>  channels;

    public List<ChannelDTO> getChannels() {
        return channels;
    }

    public void setChannels(List<ChannelDTO> channels) {
        this.channels = channels;
    }

    public static class ChannelDTO {
        private String  code;
        private String  name;
        private String  sourceCode;
        private String  sourceName;
        private boolean allowCombinedManifest;
        private boolean allowVendorSelf;
        private boolean allowAnyShippingMethod;

        private boolean manifestAllReadyToShipOrdersTogether;

        public ChannelDTO() {
            super();
        }

        public ChannelDTO(Channel c, Source s) {
            this.code = c.getCode();
            this.name = c.getName();
            this.sourceCode = s.getCode();
            this.sourceName = s.getName();
            this.allowCombinedManifest = c.isAllowCombinedManifest();
            this.allowAnyShippingMethod = s.isAllowAnyShippingMethod();
            this.allowVendorSelf = s.isAllowVendorSelf();
            this.manifestAllReadyToShipOrdersTogether = s.isManifestAllReadyToShipOrdersTogether();
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getSourceCode() {
            return sourceCode;
        }

        public void setSourceCode(String sourceCode) {
            this.sourceCode = sourceCode;
        }

        public String getSourceName() {
            return sourceName;
        }

        public void setSourceName(String sourceName) {
            this.sourceName = sourceName;
        }

        public boolean isAllowCombinedManifest() {
            return allowCombinedManifest;
        }

        public void setAllowCombinedManifest(boolean allowCombinedManifest) {
            this.allowCombinedManifest = allowCombinedManifest;
        }

        public boolean isAllowVendorSelf() {
            return allowVendorSelf;
        }

        public void setAllowVendorSelf(boolean allowVendorSelf) {
            this.allowVendorSelf = allowVendorSelf;
        }

        public boolean isAllowAnyShippingMethod() {
            return allowAnyShippingMethod;
        }

        public void setAllowAnyShippingMethod(boolean allowAnyShippingMethod) {
            this.allowAnyShippingMethod = allowAnyShippingMethod;
        }

        public boolean isManifestAllReadyToShipOrdersTogether() {
            return manifestAllReadyToShipOrdersTogether;
        }

        public void setManifestAllReadyToShipOrdersTogether(boolean manifestAllReadyToShipOrdersTogether) {
            this.manifestAllReadyToShipOrdersTogether = manifestAllReadyToShipOrdersTogether;
        }
    }

}
