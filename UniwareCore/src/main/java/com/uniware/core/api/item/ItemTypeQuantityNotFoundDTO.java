/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 03-May-2013
 *  @author unicom
 */
package com.uniware.core.api.item;

/**
 * @author unicom
 */
public class ItemTypeQuantityNotFoundDTO {

    private String  itemSku;
    private String  shelfCode;
    private Integer notFoundQuantity;
    private String  type;

    /**
     * @return the itemSku
     */
    public String getItemSku() {
        return itemSku;
    }

    /**
     * @param itemSku the itemSku to set
     */
    public void setItemSku(String itemSku) {
        this.itemSku = itemSku;
    }

    /**
     * @return the shelfCode
     */
    public String getShelfCode() {
        return shelfCode;
    }

    /**
     * @param shelfCode the shelfCode to set
     */
    public void setShelfCode(String shelfCode) {
        this.shelfCode = shelfCode;
    }

    /**
     * @return the notFoundQuantity
     */
    public Integer getNotFoundQuantity() {
        return notFoundQuantity;
    }

    /**
     * @param notFoundQuantity the notFoundQuantity to set
     */
    public void setNotFoundQuantity(Integer notFoundQuantity) {
        this.notFoundQuantity = notFoundQuantity;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    
}
