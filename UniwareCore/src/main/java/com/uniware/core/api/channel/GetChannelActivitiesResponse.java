/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 10-Apr-2015
 *  @author unicom
 */
package com.uniware.core.api.channel;

import java.util.List;

import com.unifier.core.api.base.ServiceResponse;
import com.uniware.core.api.saleorder.display.GetSaleOrderActivitiesResponse.ActivityDTO;

public class GetChannelActivitiesResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private List<ActivityDTO> activities;

    public GetChannelActivitiesResponse() {
    }

    public List<ActivityDTO> getActivities() {
        return activities;
    }

    public void setActivities(List<ActivityDTO> activities) {
        this.activities = activities;
    }

}
