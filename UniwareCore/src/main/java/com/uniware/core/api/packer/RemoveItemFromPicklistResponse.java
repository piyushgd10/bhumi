/*
 * Copyright 2016 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 1/9/16 7:05 PM
 * @author bhuvneshwarkumar
 */

package com.uniware.core.api.packer;

import com.unifier.core.api.base.ServiceResponse;

/**
 * Created by bhuvneshwarkumar on 01/09/16.
 */
public class RemoveItemFromPicklistResponse extends ServiceResponse {
    private String saleOrderItemCode;
    private String saleOrderCode;
    private String shippingPackageCode;

    public String getSaleOrderItemCode() {
        return saleOrderItemCode;
    }

    public void setSaleOrderItemCode(String saleOrderItemCode) {
        this.saleOrderItemCode = saleOrderItemCode;
    }

    public String getSaleOrderCode() {
        return saleOrderCode;
    }

    public void setSaleOrderCode(String saleOrderCode) {
        this.saleOrderCode = saleOrderCode;
    }

    public String getShippingPackageCode() {
        return shippingPackageCode;
    }

    public void setShippingPackageCode(String shippingPackageCode) {
        this.shippingPackageCode = shippingPackageCode;
    }
}
