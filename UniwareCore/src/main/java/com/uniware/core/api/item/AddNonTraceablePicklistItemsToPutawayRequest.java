/*
 * Copyright 2017 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 27/09/17
 * @author piyush
 */
package com.uniware.core.api.item;

import java.util.List;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;
import com.uniware.core.entity.ItemTypeInventory;

public class AddNonTraceablePicklistItemsToPutawayRequest extends ServiceRequest {

    @NotBlank
    private String              putawayCode;
    @NotNull
    private Integer             userId;

    private List<WsPutbackItem> putbackItems;

    public String getPutawayCode() {
        return putawayCode;
    }

    public void setPutawayCode(String putawayCode) {
        this.putawayCode = putawayCode;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public List<WsPutbackItem> getPutbackItems() {
        return putbackItems;
    }

    public void setPutbackItems(List<WsPutbackItem> putbackItems) {
        this.putbackItems = putbackItems;
    }

    public static class WsPutbackItem {
        @NotBlank
        private String                 skuCode;

        @NotNull
        @Min(value = 1)
        private Integer                quantity;

        private ItemTypeInventory.Type inventoryType;

        public String getSkuCode() {
            return skuCode;
        }

        public void setSkuCode(String skuCode) {
            this.skuCode = skuCode;
        }

        public ItemTypeInventory.Type getInventoryType() {
            return inventoryType;
        }

        public void setInventoryType(ItemTypeInventory.Type inventoryType) {
            this.inventoryType = inventoryType;
        }

        public Integer getQuantity() {
            return quantity;
        }

        public void setQuantity(Integer quantity) {
            this.quantity = quantity;
        }
    }
}
