/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 16, 2012
 *  @author praveeng
 */
package com.uniware.core.api.inventory;

import com.unifier.core.api.base.ServiceResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * @author praveeng
 */
public class SearchInventoryResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long          serialVersionUID = -8551905747311063894L;
    private Long                       totalRecords;
    private List<ItemTypeInventoryDTO> elements         = new ArrayList<ItemTypeInventoryDTO>();

    /**
     * @return the elements
     */
    public List<ItemTypeInventoryDTO> getElements() {
        return elements;
    }

    /**
     * @param elements the elements to set
     */
    public void setElements(List<ItemTypeInventoryDTO> elements) {
        this.elements = elements;
    }

    /**
     * @return the totalRecords
     */
    public Long getTotalRecords() {
        return totalRecords;
    }

    /**
     * @param totalRecords the totalRecords to set
     */
    public void setTotalRecords(Long totalRecords) {
        this.totalRecords = totalRecords;
    }

}
