/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 10-May-2013
 *  @author karunsingla
 */
package com.uniware.core.api.putaway;

import com.unifier.core.api.base.ServiceResponse;

public class AddTraceableItemToTransferPutawayResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 605362725320490268L;

}
