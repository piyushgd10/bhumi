/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jun 20, 2012
 *  @author singla
 */
package com.uniware.core.api.channel;

import com.unifier.core.api.base.ServiceResponse;

import java.util.List;

/**
 * @author Sunny
 */
public class AddChannelConnectorResponse extends ServiceResponse {

    /**
     *
     */
    private static final long serialVersionUID = 1036731140637386851L;

    private List<VerifyAndSyncParamsResponse.WsAuthenticationChallenge> challenges;

    public List<VerifyAndSyncParamsResponse.WsAuthenticationChallenge> getChallenges() {
        return challenges;
    }

    public void setChallenges(List<VerifyAndSyncParamsResponse.WsAuthenticationChallenge> challenges) {
        this.challenges = challenges;
    }
}
