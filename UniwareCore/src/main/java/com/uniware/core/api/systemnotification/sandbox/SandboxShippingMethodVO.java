/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 25-Dec-2013
 *  @author sunny
 */
package com.uniware.core.api.systemnotification.sandbox;

import com.uniware.core.entity.ShippingMethod;

public class SandboxShippingMethodVO {

    private String code;
    private String shippingMethodCode;

    public SandboxShippingMethodVO() {
        super();
    }

    public SandboxShippingMethodVO(ShippingMethod shippingMethod) {
        this.code = shippingMethod.getCode();
        this.shippingMethodCode = shippingMethod.getShippingMethodCode();
    }

    public String getShippingMethodCode() {
        return shippingMethodCode;
    }

    public void setShippingMethodCode(String shippingMethodCode) {
        this.shippingMethodCode = shippingMethodCode;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

}
