/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, May 6, 2015
 *  @author harsh
 */
package com.uniware.core.api.purchase;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author harsh
 */
public class GetNextPurchaseOrderCodeRequest extends ServiceRequest {

    private static final long serialVersionUID = -4628699523738384655L;

}
