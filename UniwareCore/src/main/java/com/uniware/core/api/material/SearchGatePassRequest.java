/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 01-Jul-2012
 *  @author vibhu
 */
package com.uniware.core.api.material;

import com.unifier.core.api.base.ServiceRequest;
import com.unifier.core.pagination.SearchOptions;

import java.util.Date;

/**
 * @author vibhu
 */
public class SearchGatePassRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 3888897753997145506L;
    private String            gatePassCode;
    private String            statusCode;
    private String            usernameContains;
    private Integer           vendorId;
    private String            toParty;
    private Date              fromDate;
    private Date              toDate;
    private SearchOptions     searchOptions;

    /**
     * @return the gatePassCode
     */
    public String getGatePassCode() {
        return gatePassCode;
    }

    /**
     * @param gatePassCode the gatePassCode to set
     */
    public void setGatePassCode(String gatePassCode) {
        this.gatePassCode = gatePassCode;
    }

    /**
     * @return the statusCode
     */
    public String getStatusCode() {
        return statusCode;
    }

    /**
     * @param statusCode the statusCode to set
     */
    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    /**
     * @return the fromDate
     */
    public Date getFromDate() {
        return fromDate;
    }

    /**
     * @param fromDate the fromDate to set
     */
    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    /**
     * @return the toDate
     */
    public Date getToDate() {
        return toDate;
    }

    /**
     * @param toDate the toDate to set
     */
    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    /**
     * @return the searchOptions
     */
    public SearchOptions getSearchOptions() {
        return searchOptions;
    }

    /**
     * @param searchOptions the searchOptions to set
     */
    public void setSearchOptions(SearchOptions searchOptions) {
        this.searchOptions = searchOptions;
    }

    /**
     * @return the usernameContains
     */
    public String getUsernameContains() {
        return usernameContains;
    }

    /**
     * @param usernameContains the usernameContains to set
     */
    public void setUsernameContains(String usernameContains) {
        this.usernameContains = usernameContains;
    }

    /**
     * @return the toParty
     */
    public String getToParty() {
        return toParty;
    }

    /**
     * @param toParty the toParty to set
     */
    public void setToParty(String toParty) {
        this.toParty = toParty;
    }

    /**
     * @return the vendorId
     */
    public Integer getVendorId() {
        return vendorId;
    }

    /**
     * @param vendorId the vendorId to set
     */
    public void setVendorId(Integer vendorId) {
        this.vendorId = vendorId;
    }
    
    
}