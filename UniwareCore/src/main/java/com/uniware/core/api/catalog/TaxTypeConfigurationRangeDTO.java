/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 21-Nov-2014
 *  @author parijat
 */
package com.uniware.core.api.catalog;

import java.math.BigDecimal;

public class TaxTypeConfigurationRangeDTO implements Comparable<TaxTypeConfigurationRangeDTO> {

    private BigDecimal vat;
    private BigDecimal cst;
    private BigDecimal integratedGst;
    private BigDecimal unionTerritoryGst;
    private BigDecimal stateGst;
    private BigDecimal centralGst;
    private BigDecimal compensationCess;
    private BigDecimal cstFormc;
    private BigDecimal additionalTax;
    private BigDecimal startPrice;
    private BigDecimal endPrice;

    public BigDecimal getStartPrice() {
        return startPrice;
    }

    public void setStartPrice(BigDecimal startPrice) {
        this.startPrice = startPrice;
    }

    public BigDecimal getEndPrice() {
        return endPrice;
    }

    public void setEndPrice(BigDecimal endPrice) {
        this.endPrice = endPrice;
    }

    public BigDecimal getAdditionalTax() {
        return additionalTax;
    }

    public void setAdditionalTax(BigDecimal additionalTax) {
        this.additionalTax = additionalTax;
    }

    public BigDecimal getVat() {
        return vat;
    }

    public void setVat(BigDecimal vat) {
        this.vat = vat;
    }

    public BigDecimal getCst() {
        return cst;
    }

    public void setCst(BigDecimal cst) {
        this.cst = cst;
    }

    public BigDecimal getIntegratedGst() {
        return integratedGst;
    }

    public void setIntegratedGst(BigDecimal integratedGst) {
        this.integratedGst = integratedGst;
    }

    public BigDecimal getUnionTerritoryGst() {
        return unionTerritoryGst;
    }

    public void setUnionTerritoryGst(BigDecimal unionTerritoryGst) {
        this.unionTerritoryGst = unionTerritoryGst;
    }

    public BigDecimal getStateGst() {
        return stateGst;
    }

    public void setStateGst(BigDecimal stateGst) {
        this.stateGst = stateGst;
    }

    public BigDecimal getCentralGst() {
        return centralGst;
    }

    public void setCentralGst(BigDecimal centralGst) {
        this.centralGst = centralGst;
    }

    public BigDecimal getCompensationCess() {
        return compensationCess;
    }

    public void setCompensationCess(BigDecimal compensationCess) {
        this.compensationCess = compensationCess;
    }

    public BigDecimal getCstFormc() {
        return cstFormc;
    }

    public void setCstFormc(BigDecimal cstFormc) {
        this.cstFormc = cstFormc;
    }

    @Override
    public int compareTo(TaxTypeConfigurationRangeDTO o) {
        if (o == null) {
            return 0;
        }
        return this.startPrice.compareTo(o.getStartPrice());
    }

}