/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 02-Feb-2015
 *  @author akshaykochhar
 */
package com.uniware.core.api.shipping;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import com.uniware.core.api.packer.RegulatoryFormDTO;
import com.uniware.core.api.saleorder.AddressDTO;
import com.uniware.core.api.warehouse.ShippingPackageTypeDTO;

public class ManifestItemDetailDTO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private String                  shippingPackageCode;
    private String                  invoiceCode;
    private String                  invoiceDisplayCode;
    private Integer                 shippingPackageId;
    private String                  trackingNumber;
    private Integer                 quantity;
    private Integer                 noOfBoxes;
    private Integer                 weight;
    private BigDecimal              totalAmount;
    private BigDecimal              sellingAmount;
    private BigDecimal              taxAmount;
    private BigDecimal              shippingCharges;
    private BigDecimal              collectableAmount;
    private String                  shippingMethod;
    private AddressDTO              shippingAddress;
    private ShippingPackageTypeDTO  shippingPackageType;
    private String                  orderCode;
    private String                  displayOrderCode;
    private boolean                 cashOnDelivery;
    private String                  productSKUs;
    private String                  productDescription;
    private Map<String, Object>     shippingPackageCustomFieldValues;
    private Map<String, Object>     saleOrderCustomFieldValues;
    private List<RegulatoryFormDTO> stateRegulatoryFormDTOs;

    public String getInvoiceDisplayCode() {
        return invoiceDisplayCode;
    }

    public void setInvoiceDisplayCode(String invoiceDisplayCode) {
        this.invoiceDisplayCode = invoiceDisplayCode;
    }

    /**
     * @return the invoiceCode
     */
    public String getInvoiceCode() {
        return invoiceCode;
    }

    /**
     * @param invoiceCode the invoiceCode to set
     */
    public void setInvoiceCode(String invoiceCode) {
        this.invoiceCode = invoiceCode;
    }

    public String getOrderCode() {
        return orderCode;
    }

    public void setOrderCode(String orderCode) {
        this.orderCode = orderCode;
    }

    public BigDecimal getTaxAmount() {
        return taxAmount;
    }

    public void setTaxAmount(BigDecimal taxAmount) {
        this.taxAmount = taxAmount;
    }

    /**
     * @return the sellingAmount
     */
    public BigDecimal getSellingAmount() {
        return sellingAmount;
    }

    /**
     * @param sellingAmount the sellingAmount to set
     */
    public void setSellingAmount(BigDecimal sellingAmount) {
        this.sellingAmount = sellingAmount;
    }

    /**
     * @return the shippingCharges
     */
    public BigDecimal getShippingCharges() {
        return shippingCharges;
    }

    /**
     * @param shippingCharges the shippingCharges to set
     */
    public void setShippingCharges(BigDecimal shippingCharges) {
        this.shippingCharges = shippingCharges;
    }

    /**
     * @return the shippingPackageId
     */
    public Integer getShippingPackageId() {
        return shippingPackageId;
    }

    /**
     * @param shippingPackageId the shippingPackageId to set
     */
    public void setShippingPackageId(Integer shippingPackageId) {
        this.shippingPackageId = shippingPackageId;
    }

    /**
     * @return the trackingNumber
     */
    public String getTrackingNumber() {
        return trackingNumber;
    }

    /**
     * @param trackingNumber the trackingNumber to set
     */
    public void setTrackingNumber(String trackingNumber) {
        this.trackingNumber = trackingNumber;
    }

    /**
     * @return the quantity
     */
    public Integer getQuantity() {
        return quantity;
    }

    /**
     * @param quantity the quantity to set
     */
    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    /**
     * @return the noOfBoxes
     */
    public Integer getNoOfBoxes() {
        return noOfBoxes;
    }

    /**
     * @param noOfBoxes the noOfBoxes to set
     */
    public void setNoOfBoxes(Integer noOfBoxes) {
        this.noOfBoxes = noOfBoxes;
    }

    /**
     * @return the weight
     */
    public Integer getWeight() {
        return weight;
    }

    /**
     * @param weight the weight to set
     */
    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    /**
     * @return the totalAmount
     */
    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    /**
     * @param totalAmount the totalAmount to set
     */
    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    /**
     * @return the shippingMethod
     */
    public String getShippingMethod() {
        return shippingMethod;
    }

    /**
     * @param shippingMethod the shippingMethod to set
     */
    public void setShippingMethod(String shippingMethod) {
        this.shippingMethod = shippingMethod;
    }

    /**
     * @return the shippingAddress
     */
    public AddressDTO getShippingAddress() {
        return shippingAddress;
    }

    /**
     * @param shippingAddress the shippingAddress to set
     */
    public void setShippingAddress(AddressDTO shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    /**
     * @return the cashOnDelivery
     */
    public boolean isCashOnDelivery() {
        return cashOnDelivery;
    }

    /**
     * @param cashOnDelivery the cashOnDelivery to set
     */
    public void setCashOnDelivery(boolean cashOnDelivery) {
        this.cashOnDelivery = cashOnDelivery;
    }

    /**
     * @return the productDescription
     */
    public String getProductDescription() {
        return productDescription;
    }

    /**
     * @param productDescription the productDescription to set
     */
    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    /**
     * @return the shippingPackageCode
     */
    public String getShippingPackageCode() {
        return shippingPackageCode;
    }

    /**
     * @param shippingPackageCode the shippingPackageCode to set
     */
    public void setShippingPackageCode(String shippingPackageCode) {
        this.shippingPackageCode = shippingPackageCode;
    }

    /**
     * @return the displayOrderCode
     */
    public String getDisplayOrderCode() {
        return displayOrderCode;
    }

    /**
     * @param displayOrderCode the displayOrderCode to set
     */
    public void setDisplayOrderCode(String displayOrderCode) {
        this.displayOrderCode = displayOrderCode;
    }

    /**
     * @return the shippingPackageType
     */
    public ShippingPackageTypeDTO getShippingPackageType() {
        return shippingPackageType;
    }

    /**
     * @param shippingPackageType the shippingPackageType to set
     */
    public void setShippingPackageType(ShippingPackageTypeDTO shippingPackageType) {
        this.shippingPackageType = shippingPackageType;
    }

    /**
     * @return the productSKUs
     */
    public String getProductSKUs() {
        return productSKUs;
    }

    /**
     * @param productSKUs the productSKUs to set
     */
    public void setProductSKUs(String productSKUs) {
        this.productSKUs = productSKUs;
    }

    /**
     * @return the shippingPackageCustomFieldValues
     */
    public Map<String, Object> getShippingPackageCustomFieldValues() {
        return shippingPackageCustomFieldValues;
    }

    /**
     * @param shippingPackageCustomFieldValues the shippingPackageCustomFieldValues to set
     */
    public void setShippingPackageCustomFieldValues(Map<String, Object> shippingPackageCustomFieldValues) {
        this.shippingPackageCustomFieldValues = shippingPackageCustomFieldValues;
    }

    /**
     * @return the saleOrderCustomFieldValues
     */
    public Map<String, Object> getSaleOrderCustomFieldValues() {
        return saleOrderCustomFieldValues;
    }

    /**
     * @param saleOrderCustomFieldValues the saleOrderCustomFieldValues to set
     */
    public void setSaleOrderCustomFieldValues(Map<String, Object> saleOrderCustomFieldValues) {
        this.saleOrderCustomFieldValues = saleOrderCustomFieldValues;
    }

    /**
     * @return the collectableAmount
     */
    public BigDecimal getCollectableAmount() {
        return collectableAmount;
    }

    /**
     * @param collectableAmount the collectableAmount to set
     */
    public void setCollectableAmount(BigDecimal collectableAmount) {
        this.collectableAmount = collectableAmount;
    }

    public List<RegulatoryFormDTO> getStateRegulatoryFormDTOs() {
        return stateRegulatoryFormDTOs;
    }

    public void setStateRegulatoryFormDTOs(List<RegulatoryFormDTO> stateRegulatoryFormDTOs) {
        this.stateRegulatoryFormDTOs = stateRegulatoryFormDTOs;
    }

}
