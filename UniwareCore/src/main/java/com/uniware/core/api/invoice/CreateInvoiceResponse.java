/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jan 20, 2012
 *  @author singla
 */
package com.uniware.core.api.invoice;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author singla
 */
public class CreateInvoiceResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 3773117733355945263L;

    private String            invoiceCode;

    private String            shippingPackageCode;

    public CreateInvoiceResponse(CreateShippingPackageInvoiceResponse shippingPackageInvoice) {
        this.invoiceCode = shippingPackageInvoice.getInvoiceCode();
        this.shippingPackageCode = shippingPackageInvoice.getShippingPackageCode();
    }
    public CreateInvoiceResponse(){}
    /**
     * @return the invoiceCode
     */
    public String getInvoiceCode() {
        return invoiceCode;
    }

    /**
     * @param invoiceCode the invoiceCode to set
     */
    public void setInvoiceCode(String invoiceCode) {
        this.invoiceCode = invoiceCode;
    }

    /**
     * @return the shippingPackageCode
     */
    public String getShippingPackageCode() {
        return shippingPackageCode;
    }

    /**
     * @param shippingPackageCode the shippingPackageCode to set
     */
    public void setShippingPackageCode(String shippingPackageCode) {
        this.shippingPackageCode = shippingPackageCode;
    }
}
