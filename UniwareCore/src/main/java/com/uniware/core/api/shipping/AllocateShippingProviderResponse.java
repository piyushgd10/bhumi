/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jan 23, 2012
 *  @author singla
 */
package com.uniware.core.api.shipping;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author singla
 */
public class AllocateShippingProviderResponse extends ServiceResponse {

    /**
     *
     */
    private static final long serialVersionUID = -5104527497740336170L;


    private String            shippingPackageCode;
    private String            shippingProviderCode;
    private String            shippingLabelLink;
    private String            trackingNumber;
    private String            shipmentLabelFormat;
    private String            shippingManagedBy;
    private String            statusCode;

    /**
     * @return the shippingPackageCode
     */
    public String getShippingPackageCode() {
        return shippingPackageCode;
    }

    /**
     * @param shippingPackageCode the shippingPackageCode to set
     */
    public void setShippingPackageCode(String shippingPackageCode) {
        this.shippingPackageCode = shippingPackageCode;
    }

    /**
     * @return the shippingProviderCode
     */
    public String getShippingProviderCode() {
        return shippingProviderCode;
    }

    /**
     * @param shippingProviderCode the shippingProviderCode to set
     */
    public void setShippingProviderCode(String shippingProviderCode) {
        this.shippingProviderCode = shippingProviderCode;
    }

    /**
     * @return the trackingNumber
     */
    public String getTrackingNumber() {
        return trackingNumber;
    }

    /**
     * @param trackingNumber the trackingNumber to set
     */
    public void setTrackingNumber(String trackingNumber) {
        this.trackingNumber = trackingNumber;
    }

    /**
     * @return the shippingLabelLink
     */
    public String getShippingLabelLink() {
        return shippingLabelLink;
    }

    /**
     * @param shippingLabelLink the shippingLabelLink to set
     */
    public void setShippingLabelLink(String shippingLabelLink) {
        this.shippingLabelLink = shippingLabelLink;
    }

    public String getShipmentLabelFormat() {
        return shipmentLabelFormat;
    }

    /**
     * @param shipmentLabelFormat Shipment label format as per current value of thirdPartyShipping of shipping package
     *            in scope.
     */
    public void setShipmentLabelFormat(String shipmentLabelFormat) {
        this.shipmentLabelFormat = shipmentLabelFormat;
    }

    public String getShippingManagedBy() {
        return shippingManagedBy;
    }

    public void setShippingManagedBy(String shippingManagedBy) {
        this.shippingManagedBy = shippingManagedBy;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

}
