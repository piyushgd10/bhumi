/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 24-Dec-2011
 *  @author vibhu
 */
package com.uniware.core.api.catalog;

import com.unifier.core.api.base.ServiceResponse;

import com.uniware.core.api.catalog.dto.CategoryDTO;

/**
 * @author vibhu
 */
public class CategoryResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 6697651353632817219L;
    private CategoryDTO       category;

    /**
     * @return the category
     */
    public CategoryDTO getCategory() {
        return category;
    }

    /**
     * @param category the category to set
     */
    public void setCategory(CategoryDTO category) {
        this.category = category;
    }

}
