/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 08-Apr-2013
 *  @author unicom
 */
package com.uniware.core.api.inflow;



/**
 * @author unicom
 *
 */
public class PrintLabelsRequest {
    
 String itemCodes;

/**
 * @return the itemCodes
 */
public String getItemCodes() {
    return itemCodes;
}

/**
 * @param itemCodes the itemCodes to set
 */
public void setItemCodes(String itemCodes) {
    this.itemCodes = itemCodes;
}

 
}
