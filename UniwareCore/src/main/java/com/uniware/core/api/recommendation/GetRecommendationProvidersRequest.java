/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Nov 25, 2015
 *  @author akshay
 */
package com.uniware.core.api.recommendation;

import com.unifier.core.api.base.ServiceRequest;

public class GetRecommendationProvidersRequest extends ServiceRequest {

    private static final long serialVersionUID = 4158093265277116436L;
    
    private String recommendationType;
    private String provider;
    
    public GetRecommendationProvidersRequest() {}
    
    public GetRecommendationProvidersRequest(String recommendationType, String provider) {
        super();
        this.recommendationType = recommendationType;
        this.provider = provider;
    }

    public String getRecommendationType() {
        return recommendationType;
    }

    public void setRecommendationType(String recommendationType) {
        this.recommendationType = recommendationType;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }
}
