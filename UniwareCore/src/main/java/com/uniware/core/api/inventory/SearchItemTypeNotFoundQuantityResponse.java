/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 03-May-2013
 *  @author unicom
 */
package com.uniware.core.api.inventory;

import java.util.ArrayList;
import java.util.List;

import com.unifier.core.api.base.ServiceResponse;
import com.uniware.core.api.item.ItemTypeQuantityNotFoundDTO;

/**
 * @author unicom
 */
public class SearchItemTypeNotFoundQuantityResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long                       serialVersionUID        = 243327795620583774L;

    private final List<ItemTypeQuantityNotFoundDTO> itemQuantityNotFoundDTO = new ArrayList<ItemTypeQuantityNotFoundDTO>();

    /**
     * @return the itemQuantityNotFoundDTO
     */
    public List<ItemTypeQuantityNotFoundDTO> getItemQuantityNotFoundDTO() {
        return itemQuantityNotFoundDTO;
    }

}
