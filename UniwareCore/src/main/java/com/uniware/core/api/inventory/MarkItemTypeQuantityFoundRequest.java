/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 03-May-2013
 *  @author unicom
 */
package com.uniware.core.api.inventory;

import java.util.Date;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import javax.validation.constraints.Past;
import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author unicom
 */
public class MarkItemTypeQuantityFoundRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 22133421L;

    @NotBlank
    private String            itemSku;

    @NotBlank
    private String            shelfCode;

    @NotNull
    @Min(value = 1)
    private Integer           quantityFound;

    @Past
    private Date ageingStartDate;

    /**
     * @return the itemSku
     */
    public String getItemSku() {
        return itemSku;
    }

    /**
     * @param itemSku the itemSku to set
     */
    public void setItemSku(String itemSku) {
        this.itemSku = itemSku;
    }

    /**
     * @return the quantityFound
     */
    public Integer getQuantityFound() {
        return quantityFound;
    }

    /**
     * @param quantityFound the quantityFound to set
     */
    public void setQuantityFound(Integer quantityFound) {
        this.quantityFound = quantityFound;
    }

    /**
     * @return the shelfCode
     */
    public String getShelfCode() {
        return shelfCode;
    }

    /**
     * @param shelfCode the shelfCode to set
     */
    public void setShelfCode(String shelfCode) {
        this.shelfCode = shelfCode;
    }

    public Date getAgeingStartDate() {
        return ageingStartDate;
    }

    public void setAgeingStartDate(Date ageingStartDate) {
        this.ageingStartDate = ageingStartDate;
    }

}
