/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Sep 7, 2012
 *  @author singla
 */
package com.uniware.core.api.invoice;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

public class UpdateInvoiceRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long       serialVersionUID      = 2790539142697529960L;

    @NotBlank
    private String                  shippingPackageCode;

    private Map<String, WsTaxInformation.ProductTax> channelProductIdToTax = new HashMap<>();

    /**
     * @return the shippingPackageCode
     */
    public String getShippingPackageCode() {
        return shippingPackageCode;
    }

    /**
     * @param shippingPackageCode the shippingPackageCode to set
     */
    public void setShippingPackageCode(String shippingPackageCode) {
        this.shippingPackageCode = shippingPackageCode;
    }

    public Map<String, WsTaxInformation.ProductTax> getChannelProductIdToTax() {
        return channelProductIdToTax;
    }

    public void setChannelProductIdToTax(Map<String, WsTaxInformation.ProductTax> channelProductIdToTax) {
        this.channelProductIdToTax = channelProductIdToTax;
    }

}
