/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jan 10, 2012
 *  @author singla
 */
package com.uniware.core.api.saleorder.display;

import java.math.BigDecimal;

import com.unifier.core.api.base.ServiceResponse;

public class GetSaleOrderLineItemPriceSummaryResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 6284414547113409023L;

    public GetSaleOrderLineItemPriceSummaryResponse() {
    }

    private LineItemPriceSummaryDTO lineItemPriceSummary;

    public LineItemPriceSummaryDTO getLineItemPriceSummary() {
        return lineItemPriceSummary;
    }

    public void setLineItemPriceSummary(LineItemPriceSummaryDTO lineItemPriceSummary) {
        this.lineItemPriceSummary = lineItemPriceSummary;
    }

    public static class LineItemPriceSummaryDTO {

        private BigDecimal totalPrice            = BigDecimal.ZERO;
        private BigDecimal sellingPrice          = BigDecimal.ZERO;
        private BigDecimal shippingCharges       = BigDecimal.ZERO;
        private BigDecimal cashOnDeliveryCharges = BigDecimal.ZERO;
        private BigDecimal giftWrapCharges       = BigDecimal.ZERO;
        private BigDecimal discount              = BigDecimal.ZERO;
        private BigDecimal prepaidAmount         = BigDecimal.ZERO;
        private BigDecimal shippingMethodCharges = BigDecimal.ZERO;
        private BigDecimal tax                   = BigDecimal.ZERO;
        private BigDecimal additionalTax         = BigDecimal.ZERO;

        public LineItemPriceSummaryDTO() {
        }

        public BigDecimal getTotalPrice() {
            return totalPrice;
        }

        public void setTotalPrice(BigDecimal totalPrice) {
            this.totalPrice = totalPrice;
        }

        public BigDecimal getSellingPrice() {
            return sellingPrice;
        }

        public void setSellingPrice(BigDecimal sellingPrice) {
            this.sellingPrice = sellingPrice;
        }

        public BigDecimal getShippingCharges() {
            return shippingCharges;
        }

        public void setShippingCharges(BigDecimal shippingCharges) {
            this.shippingCharges = shippingCharges;
        }

        public BigDecimal getCashOnDeliveryCharges() {
            return cashOnDeliveryCharges;
        }

        public void setCashOnDeliveryCharges(BigDecimal cashOnDeliveryCharges) {
            this.cashOnDeliveryCharges = cashOnDeliveryCharges;
        }

        public BigDecimal getGiftWrapCharges() {
            return giftWrapCharges;
        }

        public void setGiftWrapCharges(BigDecimal giftWrapCharges) {
            this.giftWrapCharges = giftWrapCharges;
        }

        public BigDecimal getDiscount() {
            return discount;
        }

        public void setDiscount(BigDecimal discount) {
            this.discount = discount;
        }

        public BigDecimal getPrepaidAmount() {
            return prepaidAmount;
        }

        public void setPrepaidAmount(BigDecimal prepaidAmount) {
            this.prepaidAmount = prepaidAmount;
        }

        public BigDecimal getShippingMethodCharges() {
            return shippingMethodCharges;
        }

        public void setShippingMethodCharges(BigDecimal shippingMethodCharges) {
            this.shippingMethodCharges = shippingMethodCharges;
        }

        public BigDecimal getTax() {
            return tax;
        }

        public void setTax(BigDecimal tax) {
            this.tax = tax;
        }

        public BigDecimal getAdditionalTax() {
            return additionalTax;
        }

        public void setAdditionalTax(BigDecimal additionalTax) {
            this.additionalTax = additionalTax;
        }

    }

}
