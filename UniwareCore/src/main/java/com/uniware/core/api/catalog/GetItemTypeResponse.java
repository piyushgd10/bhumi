/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 24-Jan-2014
 *  @author parijat
 */
package com.uniware.core.api.catalog;

import com.unifier.core.api.base.ServiceResponse;
import com.uniware.core.api.catalog.dto.ItemTypeFullDTO;

public class GetItemTypeResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 2419310412782191893L;

    private ItemTypeFullDTO   itemTypeDTO;

    public ItemTypeFullDTO getItemTypeDTO() {
        return itemTypeDTO;
    }

    public void setItemTypeDTO(ItemTypeFullDTO itemTypeDTO) {
        this.itemTypeDTO = itemTypeDTO;
    }

}
