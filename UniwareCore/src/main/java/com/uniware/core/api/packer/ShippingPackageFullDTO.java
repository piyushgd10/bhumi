/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 *  @version     1.0, Jan 13, 2012
 *  @author singla
 */
package com.uniware.core.api.packer;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.unifier.core.api.customfields.CustomFieldMetadataDTO;
import com.unifier.core.utils.StringUtils;
import com.uniware.core.api.saleorder.AddressDTO;
import com.uniware.core.entity.Invoice;
import com.uniware.core.entity.Reshipment;
import com.uniware.core.entity.ShippingMethod;
import com.uniware.core.entity.ShippingPackage;
import com.uniware.core.entity.ShippingPackageType;

/**
 * @author praveen
 */
public class ShippingPackageFullDTO {

    private int                          id;
    private String                       code;
    private String                       shippingProviderCode;
    private boolean                      providerEditable;
    private String                       shippingMethodCode;
    private String                       shippingPackageType;
    private AddressDTO                   shippingAddress;
    private String                       trackingStatus;
    private String                       courierStatus;
    private String                       zone;
    private BigDecimal                   estimatedWeight;
    private BigDecimal                   actualWeight;
    private Date                         created;
    private String                       orderNumber;
    private String                       displayOrderNumber;
    private String                       orderStatus;
    private String                       reshipmentCode;
    private String                       parentPackageCode;
    private Integer                      noOfItems;
    private Integer                      noOfBoxes;

    private String                       invoiceCode;
    private String                       invoiceDisplayCode;
    private String                       returnInvoiceCode;
    private String                       returnInvoiceDisplayCode;
    private String                       picklistNumber;
    private Integer                      length;
    private Integer                      width;
    private Integer                      height;
    private String                       trackingNumber;
    private String                       shippingLabelLink;
    private String                       statusCode;
    private BigDecimal                   totalPrice;
    private BigDecimal                   collectableAmount;
    private Date                         dispatchTime;
    private Date                         deliveryTime;
    private boolean                      repackageable;
    private boolean                      requiresCustomization;
    private boolean                      splittable;
    private List<SaleOrderItemDTO>       shippingPackageItems    = new ArrayList<SaleOrderItemDTO>();
    private List<ShippingPackageFullDTO> shippingPackageChildren = new ArrayList<ShippingPackageFullDTO>();
    private List<CustomFieldMetadataDTO> customFieldValues;
    private List<CustomFieldMetadataDTO> saleOrderCustomFieldValues;
    private List<RegulatoryFormDTO>      stateRegulatoryForms;
    private String                       reshipmentSaleOrderCode;
    private String                       reshipmentSaleOrderStatus;
    private String                       shippingManifestCode;
    private String                       returnManifestCode;
    private String                       channelName;
    private boolean                      thirdPartyShipping;
    private Date                         invoiceDate;
    private Date                         invoiceChannelCreated;
    private String                       invoicedByUser;

    public ShippingPackageFullDTO() {
    }

    /**
     * @param shippingPackage
     */
    public ShippingPackageFullDTO(ShippingPackage shippingPackage) {
        this.id = shippingPackage.getId();
        this.code = shippingPackage.getCode();
        this.trackingNumber = shippingPackage.getTrackingNumber();
        this.shippingLabelLink = shippingPackage.getShippingLabelLink();
        this.statusCode = shippingPackage.getStatusCode();
        this.noOfItems = shippingPackage.getNoOfItems();
        this.noOfBoxes = shippingPackage.getNoOfBoxes();
        this.totalPrice = shippingPackage.getTotalPrice();
        this.requiresCustomization = shippingPackage.isRequiresCustomization();
        this.collectableAmount = shippingPackage.getCollectableAmount();
        this.shippingProviderCode = shippingPackage.getShippingProviderCode();
        ShippingMethod method = shippingPackage.getShippingMethod();
        if (method != null) {
            this.shippingMethodCode = method.getShippingMethodCode();
        }
        ShippingPackageType packageType = shippingPackage.getShippingPackageType();
        this.shippingPackageType = packageType.getCode();
        this.length = shippingPackage.getBoxLength();
        this.width = shippingPackage.getBoxWidth();
        this.height = shippingPackage.getBoxHeight();
        this.dispatchTime = shippingPackage.getDispatchTime();
        this.shippingAddress = new AddressDTO(shippingPackage.getShippingAddress());
        this.thirdPartyShipping = shippingPackage.isThirdPartyShipping();
        this.trackingStatus = shippingPackage.getShipmentTrackingStatusCode();
        this.courierStatus = shippingPackage.getProviderStatusCode();
        this.deliveryTime = shippingPackage.getDeliveryTime();

        this.estimatedWeight = new BigDecimal(shippingPackage.getEstimatedWeight()).divide(new BigDecimal(1000.0), 3, RoundingMode.HALF_EVEN);
        this.actualWeight = new BigDecimal(shippingPackage.getActualWeight()).divide(new BigDecimal(1000.0), 3, RoundingMode.HALF_EVEN);
        this.created = shippingPackage.getCreated();

        Reshipment reshipment = shippingPackage.getReshipment();
        if (reshipment != null) {
            this.setReshipmentCode(reshipment.getCode());
        }

        ShippingPackage parentPackage = shippingPackage.getParentShippingPackage();
        if (parentPackage != null) {
            this.setParentPackageCode(parentPackage.getCode());
        }

        Invoice invoice = shippingPackage.getInvoice();
        if (invoice != null) {
            this.setInvoiceCode(invoice.getCode());
            this.setInvoiceDisplayCode(invoice.getDisplayCode());
            this.setInvoiceDate(invoice.getCreated());
            this.setInvoiceChannelCreated(invoice.getChannelCreated());
            this.setInvoicedByUser(invoice.getUser().getName());
        }
        Invoice returnInvoice = shippingPackage.getInvoice();
        if(returnInvoice != null) {
            this.returnInvoiceCode = returnInvoice.getCode();
            this.returnInvoiceDisplayCode = returnInvoice.getDisplayCode();
        }
        if (shippingPackage.getSaleOrderItems().size() > 1 && this.repackageable == false
                && StringUtils.equalsAny(shippingPackage.getStatusCode(), ShippingPackage.StatusCode.READY_TO_SHIP.name(), ShippingPackage.StatusCode.PACKED.name())) {
            this.splittable = true;
        }
        if (shippingPackage.getReshipment() != null && shippingPackage.getReshipment().getReshipmentSaleOrder() != null) {
            setReshipmentSaleOrderCode(shippingPackage.getReshipment().getReshipmentSaleOrder().getCode());
            setReshipmentSaleOrderStatus(shippingPackage.getReshipment().getReshipmentSaleOrder().getStatusCode());
        }
    }

    public String getInvoiceDisplayCode() {
        return invoiceDisplayCode;
    }

    public void setInvoiceDisplayCode(String invoiceDisplayCode) {
        this.invoiceDisplayCode = invoiceDisplayCode;
    }

    public String getReturnInvoiceDisplayCode() {
        return returnInvoiceDisplayCode;
    }

    public void setReturnInvoiceDisplayCode(String returnInvoiceDisplayCode) {
        this.returnInvoiceDisplayCode = returnInvoiceDisplayCode;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the shippingPackageItems
     */
    public List<SaleOrderItemDTO> getShippingPackageItems() {
        return shippingPackageItems;
    }

    /**
     * @param shippingPackageItems the shippingPackageItems to set
     */
    public void setShippingPackageItems(List<SaleOrderItemDTO> shippingPackageItems) {
        this.shippingPackageItems = shippingPackageItems;
    }

    /**
     * @return the noOfItems
     */
    public Integer getNoOfItems() {
        return noOfItems;
    }

    /**
     * @param noOfItems the noOfItems to set
     */
    public void setNoOfItems(Integer noOfItems) {
        this.noOfItems = noOfItems;
    }

    /**
     * @return the noOfBoxes
     */
    public Integer getNoOfBoxes() {
        return noOfBoxes;
    }

    /**
     * @param noOfBoxes the noOfBoxes to set
     */
    public void setNoOfBoxes(Integer noOfBoxes) {
        this.noOfBoxes = noOfBoxes;
    }

    /**
     * @return the invoiceCode
     */
    public String getInvoiceCode() {
        return invoiceCode;
    }

    /**
     * @param invoiceCode the invoiceCode to set
     */
    public void setInvoiceCode(String invoiceCode) {
        this.invoiceCode = invoiceCode;
    }

    /**
     * @return the trackingNumber
     */
    public String getTrackingNumber() {
        return trackingNumber;
    }

    /**
     * @param trackingNumber the trackingNumber to set
     */
    public void setTrackingNumber(String trackingNumber) {
        this.trackingNumber = trackingNumber;
    }

    /**
     * @return the statusCode
     */
    public String getStatusCode() {
        return statusCode;
    }

    /**
     * @param statusCode the statusCode to set
     */
    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    /**
     * @return the repackageable
     */
    public boolean isRepackageable() {
        return repackageable;
    }

    /**
     * @param repackageable the repackageable to set
     */
    public void setRepackageable(boolean repackageable) {
        this.repackageable = repackageable;
    }

    /**
     * @return the splittable
     */
    public boolean isSplittable() {
        return splittable;
    }

    /**
     * @param splittable the splittable to set
     */
    public void setSplittable(boolean splittable) {
        this.splittable = splittable;
    }

    /**
     * @return the shippingPackageChildren
     */
    public List<ShippingPackageFullDTO> getShippingPackageChildren() {
        return shippingPackageChildren;
    }

    /**
     * @param shippingPackageChildren the shippingPackageChildren to set
     */
    public void setShippingPackageChildren(List<ShippingPackageFullDTO> shippingPackageChildren) {
        this.shippingPackageChildren = shippingPackageChildren;
    }

    /**
     * @return the shippingPackageType
     */
    public String getShippingPackageType() {
        return shippingPackageType;
    }

    /**
     * @param shippingPackageType the shippingPackageType to set
     */
    public void setShippingPackageType(String shippingPackageType) {
        this.shippingPackageType = shippingPackageType;
    }

    /**
     * @return the shippingAddress
     */
    public AddressDTO getShippingAddress() {
        return shippingAddress;
    }

    /**
     * @param shippingAddress the shippingAddress to set
     */
    public void setShippingAddress(AddressDTO shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    public boolean isThirdPartyShipping() {
        return thirdPartyShipping;
    }

    public void setThirdPartyShipping(boolean thirdPartyShipping) {
        this.thirdPartyShipping = thirdPartyShipping;
    }

    /**
     * @return the trackingStatus
     */
    public String getTrackingStatus() {
        return trackingStatus;
    }

    /**
     * @param trackingStatus the trackingStatus to set
     */
    public void setTrackingStatus(String trackingStatus) {
        this.trackingStatus = trackingStatus;
    }

    /**
     * @return the zone
     */
    public String getZone() {
        return zone;
    }

    /**
     * @param zone the zone to set
     */
    public void setZone(String zone) {
        this.zone = zone;
    }

    /**
     * @return the estimatedWeight
     */
    public BigDecimal getEstimatedWeight() {
        return estimatedWeight;
    }

    /**
     * @param estimatedWeight the estimatedWeight to set
     */
    public void setEstimatedWeight(BigDecimal estimatedWeight) {
        this.estimatedWeight = estimatedWeight;
    }

    /**
     * @return the actualWeight
     */
    public BigDecimal getActualWeight() {
        return actualWeight;
    }

    /**
     * @param actualWeight the actualWeight to set
     */
    public void setActualWeight(BigDecimal actualWeight) {
        this.actualWeight = actualWeight;
    }

    /**
     * @return the created
     */
    public Date getCreated() {
        return created;
    }

    /**
     * @param created the created to set
     */
    public void setCreated(Date created) {
        this.created = created;
    }

    /**
     * @return the orderNumber
     */
    public String getOrderNumber() {
        return orderNumber;
    }

    /**
     * @param orderNumber the orderNumber to set
     */
    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getDisplayOrderNumber() {
        return displayOrderNumber;
    }

    public void setDisplayOrderNumber(String displayOrderNumber) {
        this.displayOrderNumber = displayOrderNumber;
    }

    /**
     * @return the orderStatus
     */
    public String getOrderStatus() {
        return orderStatus;
    }

    /**
     * @param orderStatus the orderStatus to set
     */
    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    /**
     * @return the reshipmentCode
     */
    public String getReshipmentCode() {
        return reshipmentCode;
    }

    /**
     * @param reshipmentCode the reshipmentCode to set
     */
    public void setReshipmentCode(String reshipmentCode) {
        this.reshipmentCode = reshipmentCode;
    }

    /**
     * @return the parentPackageCode
     */
    public String getParentPackageCode() {
        return parentPackageCode;
    }

    /**
     * @param parentPackageCode the parentPackageCode to set
     */
    public void setParentPackageCode(String parentPackageCode) {
        this.parentPackageCode = parentPackageCode;
    }

    /**
     * @return the shippingProviderCode
     */
    public String getShippingProviderCode() {
        return shippingProviderCode;
    }

    /**
     * @param shippingProviderCode the shippingProviderCode to set
     */
    public void setShippingProviderCode(String shippingProviderCode) {
        this.shippingProviderCode = shippingProviderCode;
    }

    /**
     * @return the shippingMethodCode
     */
    public String getShippingMethodCode() {
        return shippingMethodCode;
    }

    /**
     * @param shippingMethodCode the shippingMethodCode to set
     */
    public void setShippingMethodCode(String shippingMethodCode) {
        this.shippingMethodCode = shippingMethodCode;
    }

    /**
     * @param saleOrderItemDTO
     */
    public void addSaleOrderItemDTO(SaleOrderItemDTO saleOrderItemDTO) {
        shippingPackageItems.add(saleOrderItemDTO);
    }

    /**
     * @return the courierStatus
     */
    public String getCourierStatus() {
        return courierStatus;
    }

    /**
     * @param courierStatus the courierStatus to set
     */
    public void setCourierStatus(String courierStatus) {
        this.courierStatus = courierStatus;
    }

    /**
     * @return the reshipmentSaleOrderCode
     */
    public String getReshipmentSaleOrderCode() {
        return reshipmentSaleOrderCode;
    }

    /**
     * @param reshipmentSaleOrderCode the reshipmentSaleOrderCode to set
     */
    public void setReshipmentSaleOrderCode(String reshipmentSaleOrderCode) {
        this.reshipmentSaleOrderCode = reshipmentSaleOrderCode;
    }

    /**
     * @return the reshipmentSaleOrderStatus
     */
    public String getReshipmentSaleOrderStatus() {
        return reshipmentSaleOrderStatus;
    }

    /**
     * @param reshipmentSaleOrderStatus the reshipmentSaleOrderStatus to set
     */
    public void setReshipmentSaleOrderStatus(String reshipmentSaleOrderStatus) {
        this.reshipmentSaleOrderStatus = reshipmentSaleOrderStatus;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the shippingManifestCode
     */
    public String getShippingManifestCode() {
        return shippingManifestCode;
    }

    /**
     * @param shippingManifestCode the shippingManifestCode to set
     */
    public void setShippingManifestCode(String shippingManifestCode) {
        this.shippingManifestCode = shippingManifestCode;
    }

    /**
     * @return the returnManifestCode
     */
    public String getReturnManifestCode() {
        return returnManifestCode;
    }

    /**
     * @param returnManifestCode the returnManifestCode to set
     */
    public void setReturnManifestCode(String returnManifestCode) {
        this.returnManifestCode = returnManifestCode;
    }

    /**
     * @return the providerEditable
     */
    public boolean isProviderEditable() {
        return providerEditable;
    }

    /**
     * @param providerEditable the providerEditable to set
     */
    public void setProviderEditable(boolean providerEditable) {
        this.providerEditable = providerEditable;
    }

    /**
     * @return the length
     */
    public Integer getLength() {
        return length;
    }

    /**
     * @param length the length to set
     */
    public void setLength(Integer length) {
        this.length = length;
    }

    /**
     * @return the width
     */
    public Integer getWidth() {
        return width;
    }

    /**
     * @param width the width to set
     */
    public void setWidth(Integer width) {
        this.width = width;
    }

    /**
     * @return the height
     */
    public Integer getHeight() {
        return height;
    }

    /**
     * @param height the height to set
     */
    public void setHeight(Integer height) {
        this.height = height;
    }

    /**
     * @return the dispatchTime
     */
    public Date getDispatchTime() {
        return dispatchTime;
    }

    /**
     * @param dispatchTime the dispatchTime to set
     */
    public void setDispatchTime(Date dispatchTime) {
        this.dispatchTime = dispatchTime;
    }

    /**
     * @return the deliveryTime
     */
    public Date getDeliveryTime() {
        return deliveryTime;
    }

    /**
     * @param deliveryTime the deliveryTime to set
     */
    public void setDeliveryTime(Date deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    /**
     * @return the customFieldValues
     */
    public List<CustomFieldMetadataDTO> getCustomFieldValues() {
        return customFieldValues;
    }

    /**
     * @param customFieldValues the customFieldValues to set
     */
    public void setCustomFieldValues(List<CustomFieldMetadataDTO> customFieldValues) {
        this.customFieldValues = customFieldValues;
    }

    /**
     * @return the picklistNumber
     */
    public String getPicklistNumber() {
        return picklistNumber;
    }

    /**
     * @param picklistNumber the picklistNumber to set
     */
    public void setPicklistNumber(String picklistNumber) {
        this.picklistNumber = picklistNumber;
    }

    /**
     * @return the shippingLabelLink
     */
    public String getShippingLabelLink() {
        return shippingLabelLink;
    }

    /**
     * @param shippingLabelLink the shippingLabelLink to set
     */
    public void setShippingLabelLink(String shippingLabelLink) {
        this.shippingLabelLink = shippingLabelLink;
    }

    public List<RegulatoryFormDTO> getStateRegulatoryForms() {
        return stateRegulatoryForms;
    }

    public void setStateRegulatoryForms(List<RegulatoryFormDTO> stateRegulatoryForms) {
        this.stateRegulatoryForms = stateRegulatoryForms;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    public BigDecimal getCollectableAmount() {
        return collectableAmount;
    }

    public void setCollectableAmount(BigDecimal collectableAmount) {
        this.collectableAmount = collectableAmount;
    }

    public List<CustomFieldMetadataDTO> getSaleOrderCustomFieldValues() {
        return saleOrderCustomFieldValues;
    }

    public void setSaleOrderCustomFieldValues(List<CustomFieldMetadataDTO> saleOrderCustomFieldValues) {
        this.saleOrderCustomFieldValues = saleOrderCustomFieldValues;
    }

    public boolean isRequiresCustomization() {
        return requiresCustomization;
    }

    public void setRequiresCustomization(boolean requiresCustomization) {
        this.requiresCustomization = requiresCustomization;
    }

    public String getReturnInvoiceCode() {
        return returnInvoiceCode;
    }

    public void setReturnInvoiceCode(String returnInvoiceCode) {
        this.returnInvoiceCode = returnInvoiceCode;
    }

    public Date getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(Date invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public String getInvoicedByUser() {
        return invoicedByUser;
    }

    public void setInvoicedByUser(String invoicedByUser) {
        this.invoicedByUser = invoicedByUser;
    }

    public Date getInvoiceChannelCreated() {
        return invoiceChannelCreated;
    }

    public void setInvoiceChannelCreated(Date invoiceChannelCreated) {
        this.invoiceChannelCreated = invoiceChannelCreated;
    }
}