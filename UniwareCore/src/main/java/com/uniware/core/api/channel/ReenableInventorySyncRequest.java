/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 06-Mar-2014
 *  @author karunsingla
 */
package com.uniware.core.api.channel;

import java.util.List;

import javax.validation.Valid;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import com.unifier.core.api.base.ServiceRequest;

public class ReenableInventorySyncRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long               serialVersionUID = -1830795841570995999L;

    @NotEmpty
    @Valid
    private List<WsReenableChannelItemType> channelItemTypes;

    public List<WsReenableChannelItemType> getChannelItemTypes() {
        return channelItemTypes;
    }

    public void setChannelItemTypes(List<WsReenableChannelItemType> channelItemTypes) {
        this.channelItemTypes = channelItemTypes;
    }

    public static class WsReenableChannelItemType {

        @NotBlank
        private String channelCode;

        @NotBlank
        private String channelProductId;

        public WsReenableChannelItemType() {
        }

        public WsReenableChannelItemType(String channelCode, String channelProductId) {
            this.channelCode = channelCode;
            this.channelProductId = channelProductId;
        }

        public String getChannelCode() {
            return channelCode;
        }

        public void setChannelCode(String channelCode) {
            this.channelCode = channelCode;
        }

        public String getChannelProductId() {
            return channelProductId;
        }

        public void setChannelProductId(String channelProductId) {
            this.channelProductId = channelProductId;
        }
    }

}
