/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 20-Jan-2012
 *  @author vibhu
 */
package com.uniware.core.api.saleorder;

import com.unifier.core.cache.CacheManager;
import com.uniware.core.api.item.ItemDetailFieldDTO;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.unifier.core.api.customfields.CustomFieldMetadataDTO;
import com.unifier.core.utils.StringUtils;
import com.uniware.core.entity.SaleOrderItem;
import com.uniware.core.entity.ShippingPackage;

/**
 * @author vibhu
 */
public class SaleOrderItemDTO {

    private int                          id;
    private String                       shippingPackageCode;
    private String                       shippingPackageStatus;
    private String                       facilityCode;
    private String                       facilityName;
    private String                       alternateFacilityCode;
    private String                       reversePickupCode;
    private int                          shippingAddressId;
    private int                          packetNumber;
    private String                       combinationIdentifier;
    private String                       combinationDescription;

    private String                       item;
    private String                       shippingMethodCode;
    private String                       itemName;
    private String                       itemSku;
    private String                       channelProductId;
    private String                       imageUrl;
    private String                       statusCode;
    private String                       code;
    private String                       shelfCode;
    private BigDecimal                   totalPrice;
    private BigDecimal                   sellingPrice;
    private BigDecimal                   shippingCharges;
    private BigDecimal                   shippingMethodCharges;
    private BigDecimal                   cashOnDeliveryCharges;
    private BigDecimal                   prepaidAmount;
    private String                       voucherCode;
    private BigDecimal                   voucherValue;
    private BigDecimal                   storeCredit;
    private BigDecimal                   discount;
    private Boolean                      giftWrap;
    private BigDecimal                   giftWrapCharges;
    private BigDecimal                   taxPercentage;
    private String                       giftMessage;

    private boolean                      cancellable;
    private boolean                      editAddress;
    private boolean                      reversePickable;
    private boolean                      packetConfigurable;
    private Date                         created;
    private Date                         updated;
    private boolean                      onHold;
    private Integer                      saleOrderItemAlternateId;
    private String                       cancellationReason;
    private String                       pageUrl;
    private String                       color;
    private String                       brand;
    private String                       size;
    private String                       replacementSaleOrderCode;
    private List<CustomFieldMetadataDTO> customFieldValues;
    List<ItemDetailFieldDTO> itemDetailFieldDTOList = new ArrayList<>();

    public List<ItemDetailFieldDTO> getItemDetailFieldDTOList() {
        return itemDetailFieldDTOList;
    }

    public void setItemDetailFieldDTOList(List<ItemDetailFieldDTO> itemDetailFieldDTOList) {
        this.itemDetailFieldDTOList = itemDetailFieldDTOList;
    }
    /**
     * @return the cancellationReason
     */
    public String getCancellationReason() {
        return cancellationReason;
    }

    /**
     * @param cancellationReason the cancellationReason to set
     */
    public void setCancellationReason(String cancellationReason) {
        this.cancellationReason = cancellationReason;
    }

    /**
     * @return the created
     */
    public Date getCreated() {
        return created;
    }

    /**
     * @param created the created to set
     */
    public void setCreated(Date created) {
        this.created = created;
    }

    /**
     * @return the updated
     */
    public Date getUpdated() {
        return updated;
    }

    /**
     * @param updated the updated to set
     */
    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    /**
     * @param saleOrderItem
     */
    public SaleOrderItemDTO() {
    }

    /**
     * @param saleOrderItem
     */
    public SaleOrderItemDTO(SaleOrderItem saleOrderItem) {
        this.id = saleOrderItem.getId();
        this.shippingAddressId = saleOrderItem.getShippingAddress().getId();
        ShippingPackage shippingPackage = saleOrderItem.getShippingPackage();
        if (shippingPackage != null) {
            this.shippingPackageCode = shippingPackage.getCode();
            this.shippingPackageStatus = shippingPackage.getStatusCode();
        }
        this.packetNumber = saleOrderItem.getPacketNumber();
        this.combinationIdentifier = saleOrderItem.getCombinationIdentifier();
        this.combinationDescription = saleOrderItem.getCombinationDescription();
        this.channelProductId = saleOrderItem.getChannelProductId();

        this.totalPrice = saleOrderItem.getTotalPrice();
        this.sellingPrice = saleOrderItem.getSellingPrice();
        this.prepaidAmount = saleOrderItem.getPrepaidAmount();
        this.shippingCharges = !saleOrderItem.getShippingCharges().equals(BigDecimal.ZERO) ? saleOrderItem.getShippingCharges() : null;
        this.shippingMethodCode = saleOrderItem.getShippingMethod().getCode();
        this.voucherCode = saleOrderItem.getVoucherCode();
        this.voucherValue = saleOrderItem.getVoucherValue();
        this.shippingMethodCharges = !saleOrderItem.getShippingMethodCharges().equals(BigDecimal.ZERO) ? saleOrderItem.getShippingMethodCharges() : null;
        this.cashOnDeliveryCharges = !saleOrderItem.getCashOnDeliveryCharges().equals(BigDecimal.ZERO) ? saleOrderItem.getCashOnDeliveryCharges() : null;
        this.storeCredit = !saleOrderItem.getStoreCredit().equals(BigDecimal.ZERO) ? saleOrderItem.getStoreCredit() : null;
        this.giftWrap = saleOrderItem.isGiftWrap() ? true : null;
        this.giftWrapCharges = !saleOrderItem.getGiftWrapCharges().equals(BigDecimal.ZERO) ? saleOrderItem.getGiftWrapCharges() : null;
        this.setDiscount(!saleOrderItem.getDiscount().equals(BigDecimal.ZERO) ? saleOrderItem.getDiscount() : null);
        this.giftMessage = saleOrderItem.getGiftMessage();
        this.setCancellationReason(saleOrderItem.getCancellationReason());

        this.code = saleOrderItem.getCode();
        this.onHold = saleOrderItem.isOnHold();
        this.cancellable = saleOrderItem.isCancellable();
        this.reversePickable = saleOrderItem.isReversePickable();
        this.packetConfigurable = saleOrderItem.isPacketConfigurable();
        this.statusCode = saleOrderItem.getStatusCode();
        this.created = saleOrderItem.getCreated();
        this.updated = saleOrderItem.getUpdated();
    }

    /**
     * @return the shippingAddressId
     */
    public int getShippingAddressId() {
        return shippingAddressId;
    }

    /**
     * @param shippingAddressId the shippingAddressId to set
     */
    public void setShippingAddressId(int shippingAddressId) {
        this.shippingAddressId = shippingAddressId;
    }

    /**
     * @return the packetNumber
     */
    public int getPacketNumber() {
        return packetNumber;
    }

    /**
     * @param packetNumber the packetNumber to set
     */
    public void setPacketNumber(int packetNumber) {
        this.packetNumber = packetNumber;
    }

    /**
     * @return the item
     */
    public String getItem() {
        return item;
    }

    /**
     * @param item the item to set
     */
    public void setItem(String item) {
        this.item = item;
    }

    /**
     * @return the shippingMethod
     */
    public String getShippingMethodCode() {
        return shippingMethodCode;
    }

    /**
     * @param shippingMethod the shippingMethod to set
     */
    public void setShippingMethodCode(String shippingMethodCode) {
        this.shippingMethodCode = shippingMethodCode;
    }

    /**
     * @return the statusCode
     */
    public String getStatusCode() {
        return statusCode;
    }

    /**
     * @param statusCode the statusCode to set
     */
    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    public String getShelfCode() {
        return shelfCode;
    }

    public void setShelfCode(String shelfCode) {
        this.shelfCode = shelfCode;
    }

    /**
     * @return the totalPrice
     */
    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    /**
     * @param totalPrice the totalPrice to set
     */
    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    /**
     * @return the sellingPrice
     */
    public BigDecimal getSellingPrice() {
        return sellingPrice;
    }

    /**
     * @param sellingPrice the sellingPrice to set
     */
    public void setSellingPrice(BigDecimal sellingPrice) {
        this.sellingPrice = sellingPrice;
    }

    /**
     * @return the shippingCharges
     */
    public BigDecimal getShippingCharges() {
        return shippingCharges;
    }

    /**
     * @param shippingCharges the shippingCharges to set
     */
    public void setShippingCharges(BigDecimal shippingCharges) {
        this.shippingCharges = shippingCharges;
    }

    /**
     * @return the shippingMethodCharges
     */
    public BigDecimal getShippingMethodCharges() {
        return shippingMethodCharges;
    }

    /**
     * @param shippingMethodCharges the shippingMethodCharges to set
     */
    public void setShippingMethodCharges(BigDecimal shippingMethodCharges) {
        this.shippingMethodCharges = shippingMethodCharges;
    }

    /**
     * @return the cashOnDeliveryCharges
     */
    public BigDecimal getCashOnDeliveryCharges() {
        return cashOnDeliveryCharges;
    }

    /**
     * @param cashOnDeliveryCharges the cashOnDeliveryCharges to set
     */
    public void setCashOnDeliveryCharges(BigDecimal cashOnDeliveryCharges) {
        this.cashOnDeliveryCharges = cashOnDeliveryCharges;
    }

    /**
     * @return the itemName
     */
    public String getItemName() {
        return itemName;
    }

    /**
     * @param itemName the itemName to set
     */
    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    /**
     * @return the itemSku
     */
    public String getItemSku() {
        return itemSku;
    }

    /**
     * @param itemSku the itemSku to set
     */
    public void setItemSku(String itemSku) {
        this.itemSku = itemSku;
    }

    /**
     * @return the cancellable
     */
    public boolean isCancellable() {
        return cancellable;
    }

    /**
     * @param cancellable the cancellable to set
     */
    public void setCancellable(boolean cancellable) {
        this.cancellable = cancellable;
    }

    /**
     * @return the reversePickable
     */
    public boolean isReversePickable() {
        return reversePickable;
    }

    /**
     * @param reversePickable the reversePickable to set
     */
    public void setReversePickable(boolean reversePickable) {
        this.reversePickable = reversePickable;
    }

    /**
     * @return the reversePickupCode
     */
    public String getReversePickupCode() {
        return reversePickupCode;
    }

    /**
     * @param reversePickupCode the reversePickupCode to set
     */
    public void setReversePickupCode(String reversePickupCode) {
        this.reversePickupCode = reversePickupCode;
    }

    /**
     * @return the imageUrl
     */
    public String getImageUrl() {
        return imageUrl;
    }

    /**
     * @param imageUrl the imageUrl to set
     */
    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    /**
     * @return the packetConfigurable
     */
    public boolean isPacketConfigurable() {
        return packetConfigurable;
    }

    /**
     * @param packetConfigurable the packetConfigurable to set
     */
    public void setPacketConfigurable(boolean packetConfigurable) {
        this.packetConfigurable = packetConfigurable;
    }

    /**
     * @return the giftWrapCharges
     */
    public BigDecimal getGiftWrapCharges() {
        return giftWrapCharges;
    }

    /**
     * @param giftWrapCharges the giftWrapCharges to set
     */
    public void setGiftWrapCharges(BigDecimal giftWrapCharges) {
        this.giftWrapCharges = giftWrapCharges;
    }

    /**
     * @return the giftWrap
     */
    public Boolean getGiftWrap() {
        return giftWrap;
    }

    /**
     * @param giftWrap the giftWrap to set
     */
    public void setGiftWrap(Boolean giftWrap) {
        this.giftWrap = giftWrap;
    }

    /**
     * @return the discount
     */
    public BigDecimal getDiscount() {
        return discount;
    }

    /**
     * @param discount the discount to set
     */
    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    /**
     * @return the giftMessage
     */
    public String getGiftMessage() {
        return giftMessage;
    }

    /**
     * @param giftMessage the giftMessage to set
     */
    public void setGiftMessage(String giftMessage) {
        this.giftMessage = giftMessage;
    }

    /**
     * @return the editAddress
     */
    public boolean isEditAddress() {
        return editAddress;
    }

    /**
     * @param editAddress the editAddress to set
     */
    public void setEditAddress(boolean editAddress) {
        this.editAddress = editAddress;
    }

    /**
     * @return the onHold
     */
    public boolean isOnHold() {
        return onHold;
    }

    /**
     * @param onHold the onHold to set
     */
    public void setOnHold(boolean onHold) {
        this.onHold = onHold;
    }

    /**
     * @return the saleOrderItemAlternateId
     */
    public Integer getSaleOrderItemAlternateId() {
        return saleOrderItemAlternateId;
    }

    /**
     * @param saleOrderItemAlternateId the saleOrderItemAlternateId to set
     */
    public void setSaleOrderItemAlternateId(Integer saleOrderItemAlternateId) {
        this.saleOrderItemAlternateId = saleOrderItemAlternateId;
    }

    /**
     * @return the voucherCode
     */
    public String getVoucherCode() {
        return voucherCode;
    }

    /**
     * @param voucherCode the voucherCode to set
     */
    public void setVoucherCode(String voucherCode) {
        this.voucherCode = voucherCode;
    }

    /**
     * @return the voucherValue
     */
    public BigDecimal getVoucherValue() {
        return voucherValue;
    }

    /**
     * @param voucherValue the voucherValue to set
     */
    public void setVoucherValue(BigDecimal voucherValue) {
        this.voucherValue = voucherValue;
    }

    /**
     * @return the shippingPackageCode
     */
    public String getShippingPackageCode() {
        return shippingPackageCode;
    }

    /**
     * @param shippingPackageCode the shippingPackageCode to set
     */
    public void setShippingPackageCode(String shippingPackageCode) {
        this.shippingPackageCode = shippingPackageCode;
    }

    /**
     * @return the facilityCode
     */
    public String getFacilityCode() {
        return facilityCode;
    }

    /**
     * @param facilityCode the facilityCode to set
     */
    public void setFacilityCode(String facilityCode) {
        this.facilityCode = facilityCode;
    }

    /**
     * @return the facilityName
     */
    public String getFacilityName() {
        return facilityName;
    }

    /**
     * @param facilityName the facilityName to set
     */
    public void setFacilityName(String facilityName) {
        this.facilityName = facilityName;
    }

    /**
     * @return the shippingPackageStatus
     */
    public String getShippingPackageStatus() {
        return shippingPackageStatus;
    }

    /**
     * @param shippingPackageStatus the shippingPackageStatus to set
     */
    public void setShippingPackageStatus(String shippingPackageStatus) {
        this.shippingPackageStatus = shippingPackageStatus;
    }

    /**
     * @return the prepaidAmount
     */
    public BigDecimal getPrepaidAmount() {
        return prepaidAmount;
    }

    /**
     * @param prepaidAmount the prepaidAmount to set
     */
    public void setPrepaidAmount(BigDecimal prepaidAmount) {
        this.prepaidAmount = prepaidAmount;
    }

    /**
     * @return the storeCredit
     */
    public BigDecimal getStoreCredit() {
        return storeCredit;
    }

    /**
     * @param storeCredit the storeCredit to set
     */
    public void setStoreCredit(BigDecimal storeCredit) {
        this.storeCredit = storeCredit;
    }

    /**
     * @return the combinationIdentifier
     */
    public String getCombinationIdentifier() {
        return combinationIdentifier;
    }

    /**
     * @param combinationIdentifier the combinationIdentifier to set
     */
    public void setCombinationIdentifier(String combinationIdentifier) {
        this.combinationIdentifier = combinationIdentifier;
    }

    /**
     * @return the combinationDescription
     */
    public String getCombinationDescription() {
        return combinationDescription;
    }

    /**
     * @param combinationDescription the combinationDescription to set
     */
    public void setCombinationDescription(String combinationDescription) {
        this.combinationDescription = combinationDescription;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    public String getPageUrl() {
        return pageUrl;
    }

    public void setPageUrl(String pageUrl) {
        this.pageUrl = pageUrl;
    }

    public List<CustomFieldMetadataDTO> getCustomFieldValues() {
        return customFieldValues;
    }

    public void setCustomFieldValues(List<CustomFieldMetadataDTO> customFieldValues) {
        this.customFieldValues = customFieldValues;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getChannelProductId() {
        return channelProductId;
    }

    public void setChannelProductId(String channelProductId) {
        this.channelProductId = channelProductId;
    }

    public BigDecimal getTaxPercentage() {
        return taxPercentage;
    }

    public void setTaxPercentage(BigDecimal taxPercentage) {
        this.taxPercentage = taxPercentage;
    }

    public String getReplacementSaleOrderCode() {
        return replacementSaleOrderCode;
    }

    public void setReplacementSaleOrderCode(String replacementSaleOrderCode) {
        this.replacementSaleOrderCode = replacementSaleOrderCode;
    }

    public String getAlternateFacilityCode() {
        return alternateFacilityCode;
    }

    public void setAlternateFacilityCode(String alternateFacilityCode) {
        this.alternateFacilityCode = alternateFacilityCode;
    }
}
