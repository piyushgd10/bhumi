/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *  @version     1.0, Nov 27, 2015
 *  @author      shobhit
 */

package com.uniware.core.api.prices;

import org.hibernate.validator.constraints.NotBlank;

public class GetUIPricePropertiesByChannelRequest {

    private static final long serialVersionUID = 3341528689438847422L;

    @NotBlank
    private String channelCode;

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }
}