/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 24, 2012
 *  @author praveeng
 */
package com.uniware.core.api.party;

import com.unifier.core.api.base.ServiceRequest;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * @author praveeng
 */
public class CreateVendorItemTypeRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -6740506409183994151L;

    @NotNull
    @Valid
    private WsVendorItemType  vendorItemType;

    /**
     * @return the vendorItemType
     */
    public WsVendorItemType getVendorItemType() {
        return vendorItemType;
    }

    /**
     * @param vendorItemType the vendorItemType to set
     */
    public void setVendorItemType(WsVendorItemType vendorItemType) {
        this.vendorItemType = vendorItemType;
    }
}
