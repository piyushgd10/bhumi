/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 26-Apr-2013
 *  @author pankaj
 */
package com.uniware.core.api.shipping;

import com.unifier.core.api.base.ServiceResponse;
import com.uniware.core.api.packer.ShippingPackageFullDTO;

/**
 * @author pankaj
 */
public class EditShippingPackageDetailResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long      serialVersionUID = -6799232124380953889L;

    private ShippingPackageFullDTO shippingPackageFullDTO;

    /**
     * @return the shippingPackageFullDTO
     */
    public ShippingPackageFullDTO getShippingPackageFullDTO() {
        return shippingPackageFullDTO;
    }

    /**
     * @param shippingPackageFullDTO the shippingPackageFullDTO to set
     */
    public void setShippingPackageFullDTO(ShippingPackageFullDTO shippingPackageFullDTO) {
        this.shippingPackageFullDTO = shippingPackageFullDTO;
    }

}
