/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 11/06/14
 *  @author amit
 */

package com.uniware.core.api.inventory;

import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;

public class WsItemLabel {

    @NotBlank
    private String  itemSkuCode;

    @NotNull
    private Integer quantity;

    private String  vendorSkuCode;

    public WsItemLabel() {
        super();
    }

    public String getItemSkuCode() {
        return itemSkuCode;
    }

    public void setItemSkuCode(String itemSkuCode) {
        this.itemSkuCode = itemSkuCode;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getVendorSkuCode() {
        return vendorSkuCode;
    }

    public void setVendorSkuCode(String vendorSkuCode) {
        this.vendorSkuCode = vendorSkuCode;
    }

    @Override
    public String toString() {
        return "WsItemLabel{" + "itemSkuCode='" + itemSkuCode + '\'' + ", quantity=" + quantity + ", vendorSkuCode='" + vendorSkuCode + '\'' + '}';
    }
}