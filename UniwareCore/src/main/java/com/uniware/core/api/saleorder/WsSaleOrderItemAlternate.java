/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jul 29, 2012
 *  @author singla
 */
package com.uniware.core.api.saleorder;

import java.math.BigDecimal;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

/**
 * @author singla
 */
public class WsSaleOrderItemAlternate {

    @NotBlank
    private String     itemSku;

    @NotNull
    private BigDecimal amountDifference;

    /**
     * @return the itemSku
     */
    public String getItemSku() {
        return itemSku;
    }

    /**
     * @param itemSku the itemSku to set
     */
    public void setItemSku(String itemSku) {
        this.itemSku = itemSku;
    }

    /**
     * @return the amountDifference
     */
    public BigDecimal getAmountDifference() {
        return amountDifference;
    }

    /**
     * @param amountDifference the amountDifference to set
     */
    public void setAmountDifference(BigDecimal amountDifference) {
        this.amountDifference = amountDifference;
    }
}
