/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jan 10, 2012
 *  @author singla
 */
package com.uniware.core.api.saleorder.display;

import java.util.Date;
import java.util.List;

import com.unifier.core.api.base.ServiceResponse;

public class GetSaleOrderActivitiesResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 6284414547113409023L;

    private List<ActivityDTO> activities;

    public GetSaleOrderActivitiesResponse() {
    }

    public List<ActivityDTO> getActivities() {
        return activities;
    }

    public void setActivities(List<ActivityDTO> activities) {
        this.activities = activities;
    }

    public static class ActivityDTO implements Comparable<ActivityDTO> {
        private String activityType;
        private String username;
        private String log;
        private Date   created;

        public ActivityDTO() {
        }

        public String getActivityType() {
            return activityType;
        }

        public void setActivityType(String activityType) {
            this.activityType = activityType;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getLog() {
            return log;
        }

        public void setLog(String log) {
            this.log = log;
        }

        public Date getCreated() {
            return created;
        }

        public void setCreated(Date created) {
            this.created = created;
        }

        @Override
        public int compareTo(ActivityDTO o) {
            return o.getCreated().compareTo(this.created);
        }

    }
}
