/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 26-Aug-2015
 *  @author akshay
 */
package com.uniware.core.api.shipping;

import java.util.List;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import com.unifier.core.api.base.ServiceRequest;

public class VerifySaleOrderItemsPODCodeRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -5024475370620721112L;

    @NotBlank
    private String            saleOrderCode;

    @NotEmpty
    private List<String>      saleOrderItemCodes;

    @NotBlank
    private String            podCode;

    public String getSaleOrderCode() {
        return saleOrderCode;
    }

    public void setSaleOrderCode(String saleOrderCode) {
        this.saleOrderCode = saleOrderCode;
    }

    public List<String> getSaleOrderItemCodes() {
        return saleOrderItemCodes;
    }

    public void setSaleOrderItemCodes(List<String> saleOrderItemCodes) {
        this.saleOrderItemCodes = saleOrderItemCodes;
    }

    public String getPodCode() {
        return podCode;
    }

    public void setPodCode(String podCode) {
        this.podCode = podCode;
    }

}
