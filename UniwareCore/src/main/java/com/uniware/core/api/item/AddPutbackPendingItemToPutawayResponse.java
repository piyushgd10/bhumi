/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 */
package com.uniware.core.api.item;

import com.unifier.core.api.base.ServiceResponse;

public class AddPutbackPendingItemToPutawayResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 3075230775396924616L;

}
