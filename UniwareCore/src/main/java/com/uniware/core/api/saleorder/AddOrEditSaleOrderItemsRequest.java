package com.uniware.core.api.saleorder;

import java.math.BigDecimal;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.unifier.core.api.base.ServiceRequest;
import com.uniware.core.api.model.WSSaleOrderItemLight;

public class AddOrEditSaleOrderItemsRequest extends ServiceRequest {

	/**
	 * 
	 */
	private static final long serialVersionUID = 239748923789374987L;
	
	private String					saleOrderCode;
	
	private String                    logisticChargesDivisionMethod;

    private BigDecimal                logisticCharges  = BigDecimal.ZERO;
	
	@NotNull
	@Valid
	private List<WSSaleOrderItemLight>	saleOrderItems;
	

	/**
	 * @return the saleOrderCode
	 */
	public String getSaleOrderCode() {
		return saleOrderCode;
	}

	/**
	 * @param saleOrderCode the saleOrderCode to set
	 */
	public void setSaleOrderCode(String saleOrderCode) {
		this.saleOrderCode = saleOrderCode;
	}

	/**
	 * @return the saleOrderItems
	 */
	public List<WSSaleOrderItemLight> getSaleOrderItems() {
		return saleOrderItems;
	}

	/**
	 * @param saleOrderItems the saleOrderItems to set
	 */
	public void setSaleOrderItems(List<WSSaleOrderItemLight> saleOrderItems) {
		this.saleOrderItems = saleOrderItems;
	}

    /**
     * @return the logisticChargesDivisionMethod
     */
    public String getLogisticChargesDivisionMethod() {
        return logisticChargesDivisionMethod;
    }

    /**
     * @param logisticChargesDivisionMethod the logisticChargesDivisionMethod to set
     */
    public void setLogisticChargesDivisionMethod(String logisticChargesDivisionMethod) {
        this.logisticChargesDivisionMethod = logisticChargesDivisionMethod;
    }

    /**
     * @return the logisticCharges
     */
    public BigDecimal getLogisticCharges() {
        return logisticCharges;
    }

    /**
     * @param logisticCharges the logisticCharges to set
     */
    public void setLogisticCharges(BigDecimal logisticCharges) {
        this.logisticCharges = logisticCharges;
    }
	
}
