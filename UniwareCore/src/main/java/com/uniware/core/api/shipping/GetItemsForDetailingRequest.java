/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 07-Apr-2014
 *  @author karunsingla
 */
package com.uniware.core.api.shipping;

import java.util.List;

import org.hibernate.validator.constraints.NotEmpty;

import com.unifier.core.api.base.ServiceRequest;

public class GetItemsForDetailingRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -3973310063416352301L;

    @NotEmpty
    private List<String>      shippingPackageCodes;

    public List<String> getShippingPackageCodes() {
        return shippingPackageCodes;
    }

    public void setShippingPackageCodes(List<String> shippingPackageCodes) {
        this.shippingPackageCodes = shippingPackageCodes;
    }
}
