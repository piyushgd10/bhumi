package com.uniware.core.api.o2o;

import java.util.List;

import org.hibernate.validator.constraints.NotEmpty;

import com.unifier.core.api.base.ServiceRequest;

/**
 * Created by sunny on 09/04/15.
 */
public class GetFacilityDetailsRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -8904268179068048944L;
    
    @NotEmpty
    private List<String> facilityCodes;

    public List<String> getFacilityCodes() {
        return facilityCodes;
    }

    public void setFacilityCodes(List<String> facilityCodes) {
        this.facilityCodes = facilityCodes;
    }
}
