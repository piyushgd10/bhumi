/*
 * Copyright 2017 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 19/07/17
 * @author piyush
 */
package com.uniware.core.api.invoice;

public class WsShippingProviderInfo {

    private Boolean thirdPartyShippingNotAvailable;

    private String  additionalInfo;

    private Boolean manualTrackingNumberAllocation;

    private String  trackingNumber;

    private String  shippingProvider;

    private String  shippingLabelLink;

    private String  shipmentLabelFormat;

    public Boolean getThirdPartyShippingNotAvailable() {
        return thirdPartyShippingNotAvailable;
    }

    public void setThirdPartyShippingNotAvailable(Boolean thirdPartyShippingNotAvailable) {
        this.thirdPartyShippingNotAvailable = thirdPartyShippingNotAvailable;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    public Boolean getManualTrackingNumberAllocation() {
        return manualTrackingNumberAllocation;
    }

    public void setManualTrackingNumberAllocation(Boolean manualTrackingNumberAllocation) {
        this.manualTrackingNumberAllocation = manualTrackingNumberAllocation;
    }

    public String getTrackingNumber() {
        return trackingNumber;
    }

    public void setTrackingNumber(String trackingNumber) {
        this.trackingNumber = trackingNumber;
    }

    public String getShippingProvider() {
        return shippingProvider;
    }

    public void setShippingProvider(String shippingProvider) {
        this.shippingProvider = shippingProvider;
    }

    public String getShippingLabelLink() {
        return shippingLabelLink;
    }

    public void setShippingLabelLink(String shippingLabelLink) {
        this.shippingLabelLink = shippingLabelLink;
    }

    public String getShipmentLabelFormat() {
        return shipmentLabelFormat;
    }

    public void setShipmentLabelFormat(String shipmentLabelFormat) {
        this.shipmentLabelFormat = shipmentLabelFormat;
    }
}
