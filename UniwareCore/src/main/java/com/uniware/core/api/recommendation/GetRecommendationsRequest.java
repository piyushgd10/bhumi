/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Nov 25, 2015
 *  @author akshay
 */
package com.uniware.core.api.recommendation;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.validator.constraints.NotEmpty;

import com.unifier.core.api.base.ServiceRequest;

public class GetRecommendationsRequest extends ServiceRequest {

    private static final long serialVersionUID     = 3524728368646610132L;

    @NotEmpty
    private List<String>      referenceIdentifiers = new ArrayList<>();

    public List<String> getReferenceIdentifiers() {
        return referenceIdentifiers;
    }

    public void setReferenceIdentifiers(List<String> referenceIdentifiers) {
        this.referenceIdentifiers = referenceIdentifiers;
    }

}
