/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 8, 2012
 *  @author praveeng
 */
package com.uniware.core.api.catalog;

import javax.validation.constraints.Size;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author praveeng
 */
public class CreateTagRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 5154871822618804224L;

    private Integer           id;

    @Size(max = 45, min = 1)
    private String            name;

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

}
