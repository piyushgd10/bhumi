/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 17-May-2013
 *  @author praveeng
 */
package com.uniware.core.api.material;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author praveeng
 */
public class GetInboundGatePassResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -2717530015832895818L;
    private InboundGatePassDTO inboundGatePassDTO;
    /**
     * @return the inboundGatePassDTO
     */
    public InboundGatePassDTO getInboundGatePassDTO() {
        return inboundGatePassDTO;
    }
    /**
     * @param inboundGatePassDTO the inboundGatePassDTO to set
     */
    public void setInboundGatePassDTO(InboundGatePassDTO inboundGatePassDTO) {
        this.inboundGatePassDTO = inboundGatePassDTO;
    }
    

}
