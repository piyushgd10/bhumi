/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, May 5, 2015
 *  @author akshay
 */
package com.uniware.core.api.facility;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

public class GetSellerDetailsFromTinRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -5282136482627615208L;

    @NotBlank
    private String            tin;

    public String getTin() {
        return tin;
    }

    public void setTin(String tin) {
        this.tin = tin;
    }

}
