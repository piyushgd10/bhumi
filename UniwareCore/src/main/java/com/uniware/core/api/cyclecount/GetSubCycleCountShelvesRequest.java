package com.uniware.core.api.cyclecount;

import java.util.Set;

import javax.validation.constraints.Min;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

/**
 * Created by harshpal on 20/04/16.
 */
public class GetSubCycleCountShelvesRequest extends ServiceRequest {

    @NotBlank
    public String        subCycleCountCode;

    @Min(1)
    private Integer      pageSize;
    @Min(1)
    private Integer      pageStartIndex;
    private Set<String>  statusCodes;
    private Set<Integer> pickSetIds;
    private boolean      currentUserShelvesOnly = true;

    private Integer      userId;

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Integer getPageStartIndex() {
        return pageStartIndex;
    }

    public void setPageStartIndex(Integer pageStartIndex) {
        this.pageStartIndex = pageStartIndex;
    }

    public Set<String> getStatusCodes() {
        return statusCodes;
    }

    public void setStatusCodes(Set<String> statusCodes) {
        this.statusCodes = statusCodes;
    }

    public Set<Integer> getPickSetIds() {
        return pickSetIds;
    }

    public void setPickSetIds(Set<Integer> pickSetIds) {
        this.pickSetIds = pickSetIds;
    }

    public boolean isCurrentUserShelvesOnly() {
        return currentUserShelvesOnly;
    }

    public void setCurrentUserShelvesOnly(boolean currentUserShelvesOnly) {
        this.currentUserShelvesOnly = currentUserShelvesOnly;
    }

    public String getSubCycleCountCode() {
        return subCycleCountCode;
    }

    public void setSubCycleCountCode(String subCycleCountCode) {
        this.subCycleCountCode = subCycleCountCode;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }
}
