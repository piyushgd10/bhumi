/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 15-Feb-2012
 *  @author vibhu
 */
package com.uniware.core.api.party;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author vibhu
 */
public class EditPartyAddressRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -8451532158158205378L;

    @NotNull
    @Valid
    private WsPartyAddress    partyAddress;

    public EditPartyAddressRequest() {
    }

    public EditPartyAddressRequest(WsPartyAddress partyAddress) {
        this.partyAddress = partyAddress;
    }

    /**
     * @return the partyAddress
     */
    public WsPartyAddress getPartyAddress() {
        return partyAddress;
    }

    /**
     * @param partyAddress the partyAddress to set
     */
    public void setPartyAddress(WsPartyAddress partyAddress) {
        this.partyAddress = partyAddress;
    }
}
