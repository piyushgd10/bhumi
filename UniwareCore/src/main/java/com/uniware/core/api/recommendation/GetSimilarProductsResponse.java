/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 27-Mar-2014
 *  @author parijat
 */
package com.uniware.core.api.recommendation;

import java.util.ArrayList;
import java.util.List;

import com.unifier.core.api.base.ServiceResponse;
import com.uniware.core.api.catalog.dto.SimilarItemTypeDTO;

/**
 * @author Sunny
 */
public class GetSimilarProductsResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long        serialVersionUID = 6433308221759991999L;

    private List<SimilarItemTypeDTO> similarItemTypes = new ArrayList<SimilarItemTypeDTO>();

    public List<SimilarItemTypeDTO> getSimilarItemTypes() {
        return similarItemTypes;
    }

    public void setSimilarItemTypes(List<SimilarItemTypeDTO> similarItemTypes) {
        this.similarItemTypes = similarItemTypes;
    }

}
