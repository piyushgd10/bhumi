/*
 * Copyright 2017 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 30/6/17 8:12 PM
 * @author digvijaysharma
 */

package com.uniware.core.api.invoice;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.unifier.core.api.base.ServiceRequest;

/**
 * Created by digvijaysharma on 30/06/17.
 */
public class GetInvoiceTaxDetailsFromChannelRequest extends ServiceRequest {

    /**
     *
     */
    private static final long      serialVersionUID = 9134119935562379672L;

    private String                 invoiceCode;

    private Date                   channelCreatedTime;

    private Boolean                thirdPartyInvoicingNotAvailable;

    private List<String>           cancelledSaleOrderItemCodes;

    @NotNull
    @Valid
    private WsTaxInformation       taxInformation;

    @Valid
    private WsShippingProviderInfo shippingProviderInfo;

    public String getInvoiceCode() {
        return invoiceCode;
    }

    public void setInvoiceCode(String invoiceCode) {
        this.invoiceCode = invoiceCode;
    }

    public List<String> getCancelledSaleOrderItemCodes() {
        return cancelledSaleOrderItemCodes;
    }

    public void setCancelledSaleOrderItemCodes(List<String> cancelledSaleOrderItemCodes) {
        this.cancelledSaleOrderItemCodes = cancelledSaleOrderItemCodes;
    }

    public WsTaxInformation getTaxInformation() {
        return taxInformation;
    }

    public void setTaxInformation(WsTaxInformation taxInformation) {
        this.taxInformation = taxInformation;
    }

    public Date getChannelCreatedTime() {
        return channelCreatedTime;
    }

    public void setChannelCreatedTime(Date channelCreatedTime) {
        this.channelCreatedTime = channelCreatedTime;
    }

    public Boolean getThirdPartyInvoicingNotAvailable() {
        return thirdPartyInvoicingNotAvailable;
    }

    public void setThirdPartyInvoicingNotAvailable(Boolean thirdPartyInvoicingNotAvailable) {
        this.thirdPartyInvoicingNotAvailable = thirdPartyInvoicingNotAvailable;
    }

    public WsShippingProviderInfo getShippingProviderInfo() {
        return shippingProviderInfo;
    }

    public void setShippingProviderInfo(WsShippingProviderInfo shippingProviderInfo) {
        this.shippingProviderInfo = shippingProviderInfo;
    }
}
