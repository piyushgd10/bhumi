/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 27-Aug-2013
 *  @author karunsingla
 */
package com.uniware.core.api.returns;

import com.unifier.core.api.base.ServiceResponse;

public class AddReturnedSaleOrderItemsToPutawayResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -2373882395713595935L;

}
