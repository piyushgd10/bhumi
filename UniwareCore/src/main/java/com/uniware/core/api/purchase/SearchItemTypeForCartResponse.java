/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Apr 9, 2012
 *  @author praveeng
 */
package com.uniware.core.api.purchase;

import com.unifier.core.api.base.ServiceResponse;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @author praveeng
 */
public class SearchItemTypeForCartResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long     serialVersionUID = 7923361682283502979L;
    private List<ItemTypeCartDTO> itemTypes        = new ArrayList<ItemTypeCartDTO>();
    private Long                  totalRecords;

    public List<ItemTypeCartDTO> getItemTypes() {
        return itemTypes;
    }

    public void setItemTypes(List<ItemTypeCartDTO> itemTypes) {
        this.itemTypes = itemTypes;
    }

    /**
     * @return the totalRecords
     */
    public Long getTotalRecords() {
        return totalRecords;
    }

    /**
     * @param totalRecords the totalRecords to set
     */
    public void setTotalRecords(Long totalRecords) {
        this.totalRecords = totalRecords;
    }

    public static class ItemTypeCartDTO {
        private String                  skuCode;
        private String                  name;
        private String                  itemTypeImageUrl;
        private String                  itemTypePageUrl;
        private List<VendorItemTypeDTO> vendorItemTypes = new ArrayList<VendorItemTypeDTO>();

        /**
         * @return the skuCode
         */
        public String getSkuCode() {
            return skuCode;
        }

        /**
         * @param skuCode the skuCode to set
         */
        public void setSkuCode(String skuCode) {
            this.skuCode = skuCode;
        }

        /**
         * @return the name
         */
        public String getName() {
            return name;
        }

        /**
         * @param name the name to set
         */
        public void setName(String name) {
            this.name = name;
        }

        /**
         * @return the vendorItemTypes
         */
        public List<VendorItemTypeDTO> getVendorItemTypes() {
            return vendorItemTypes;
        }

        /**
         * @param vendorItemTypes the vendorItemTypes to set
         */
        public void setVendorItemTypes(List<VendorItemTypeDTO> vendorItemTypes) {
            this.vendorItemTypes = vendorItemTypes;
        }

        /**
         * @return the itemTypeImageUrl
         */
        public String getItemTypeImageUrl() {
            return itemTypeImageUrl;
        }

        /**
         * @param itemTypeImageUrl the itemTypeImageUrl to set
         */
        public void setItemTypeImageUrl(String itemTypeImageUrl) {
            this.itemTypeImageUrl = itemTypeImageUrl;
        }

        /**
         * @return the itemTypePageUrl
         */
        public String getItemTypePageUrl() {
            return itemTypePageUrl;
        }

        /**
         * @param itemTypePageUrl the itemTypePageUrl to set
         */
        public void setItemTypePageUrl(String itemTypePageUrl) {
            this.itemTypePageUrl = itemTypePageUrl;
        }

    }

    public static class VendorItemTypeDTO {

        private Integer    vendorId;
        private String     vendorCode;
        private String     vendorName;
        private BigDecimal unitPrice;
        private String     vendorSkuCode;

        /**
         * @return the vendorId
         */
        public Integer getVendorId() {
            return vendorId;
        }

        /**
         * @param vendorId the vendorId to set
         */
        public void setVendorId(Integer vendorId) {
            this.vendorId = vendorId;
        }

        /**
         * @return the vendorName
         */
        public String getVendorName() {
            return vendorName;
        }

        /**
         * @param vendorName the vendorName to set
         */
        public void setVendorName(String vendorName) {
            this.vendorName = vendorName;
        }

        /**
         * @return the unitPrice
         */
        public BigDecimal getUnitPrice() {
            return unitPrice;
        }

        /**
         * @param unitPrice the unitPrice to set
         */
        public void setUnitPrice(BigDecimal unitPrice) {
            this.unitPrice = unitPrice;
        }

        /**
         * @return the vendorSkuCode
         */
        public String getVendorSkuCode() {
            return vendorSkuCode;
        }

        /**
         * @param vendorSkuCode the vendorSkuCode to set
         */
        public void setVendorSkuCode(String vendorSkuCode) {
            this.vendorSkuCode = vendorSkuCode;
        }

        /**
         * @return the vendorCode
         */
        public String getVendorCode() {
            return vendorCode;
        }

        /**
         * @param vendorCode the vendorCode to set
         */
        public void setVendorCode(String vendorCode) {
            this.vendorCode = vendorCode;
        }

    }

}
