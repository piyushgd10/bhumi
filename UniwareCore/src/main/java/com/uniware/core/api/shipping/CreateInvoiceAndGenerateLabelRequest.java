/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 18-Dec-2013
 *  @author akshay
 */
package com.uniware.core.api.shipping;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

public class CreateInvoiceAndGenerateLabelRequest extends ServiceRequest{

    /**
     * 
     */
    private static final long serialVersionUID = -5391353511685611568L;

    @NotBlank
    private String            shippingPackageCode;

    @NotNull
    private Integer           userId;

    public String getShippingPackageCode() {
        return shippingPackageCode;
    }

    public void setShippingPackageCode(String shippingPackageCode) {
        this.shippingPackageCode = shippingPackageCode;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

}
