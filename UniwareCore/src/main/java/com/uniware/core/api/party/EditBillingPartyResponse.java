/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 20, 2012
 *  @author praveeng
 */
package com.uniware.core.api.party;

import com.unifier.core.api.base.ServiceResponse;
import com.uniware.core.api.party.dto.BillingPartyDTO;

/**
 * @author Sunny
 */
public class EditBillingPartyResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -2426856553133649676L;

    private BillingPartyDTO   billingParty;

    public BillingPartyDTO getBillingParty() {
        return billingParty;
    }

    public void setBillingParty(BillingPartyDTO billingParty) {
        this.billingParty = billingParty;
    }

}
