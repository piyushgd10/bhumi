/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 26-May-2015
 *  @author akshay
 */
package com.uniware.core.api.shipping;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

import javax.validation.constraints.NotNull;

public class AutoDispatchShippingPackageRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -2073601998418841861L;

    @NotBlank
    private String shippingPackageCode;

    @NotNull
    private Integer                  userId;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getShippingPackageCode() {
        return shippingPackageCode;
    }

    public void setShippingPackageCode(String shippingPackageCode) {
        this.shippingPackageCode = shippingPackageCode;
    }
    
}
