/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 13-Aug-2013
 *  @author parijat
 */
package com.uniware.core.api.inventory;

import com.unifier.core.api.base.ServiceResponse;
import com.uniware.core.vo.ChannelInventoryUpdateSnapshotVO;

/**
 * @author Sunny
 */
public class CalculateChannelItemTypeInventoryResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long                serialVersionUID = 8279106511789864171L;

    private long                             calculatedInventory;
    private ChannelInventoryUpdateSnapshotVO channelInventoryUpdateSnapshot;

    public long getCalculatedInventory() {
        return calculatedInventory;
    }

    public void setCalculatedInventory(long calculatedInventory) {
        this.calculatedInventory = calculatedInventory;
    }

    public ChannelInventoryUpdateSnapshotVO getChannelInventoryUpdateSnapshot() {
        return channelInventoryUpdateSnapshot;
    }

    public void setChannelInventoryUpdateSnapshot(ChannelInventoryUpdateSnapshotVO channelInventoryUpdateSnapshot) {
        this.channelInventoryUpdateSnapshot = channelInventoryUpdateSnapshot;
    }

}
