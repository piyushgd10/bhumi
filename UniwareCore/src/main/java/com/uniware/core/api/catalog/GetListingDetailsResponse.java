/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Nov 25, 2015
 *  @author akshay
 */
package com.uniware.core.api.catalog;

import java.util.ArrayList;
import java.util.List;

import com.unifier.core.api.base.ServiceResponse;

public class GetListingDetailsResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -6131515417557429986L;
    
    private List<ListingDetailDTO> listings = new ArrayList<>();

    public List<ListingDetailDTO> getListings() {
        return listings;
    }

    public void setListings(List<ListingDetailDTO> listings) {
        this.listings = listings;
    }

}
