/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Apr 13, 2012
 *  @author praveeng
 */
package com.uniware.core.api.purchase;

import java.util.ArrayList;
import java.util.List;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author Sunny
 */
public class GetVendorPurchaseOrdersResponse extends ServiceResponse {

    private static final long serialVersionUID   = -16646522218046951L;
    private List<String>      purchaseOrderCodes = new ArrayList<String>();

    public List<String> getPurchaseOrderCodes() {
        return purchaseOrderCodes;
    }

    public void setPurchaseOrderCodes(List<String> purchaseOrderCodes) {
        this.purchaseOrderCodes = purchaseOrderCodes;
    }

}
