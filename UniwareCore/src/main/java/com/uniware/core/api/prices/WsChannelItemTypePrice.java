/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Oct 19, 2015
 *  @author bhupi
 */

package com.uniware.core.api.prices;

import java.math.BigDecimal;

import javax.validation.Valid;

import org.hibernate.validator.constraints.NotBlank;

import com.uniware.core.api.prices.dto.PriceDTO;

public class WsChannelItemTypePrice {

    @NotBlank
    private String channelCode;

    @NotBlank
    private String channelProductId;
    
    public WsChannelItemTypePrice() {
        this.prices = new PriceDTO();
    }
    
    // TODO do not delegate to dto methods
    @Valid
    private PriceDTO prices;
    
    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    public String getChannelProductId() {
        return channelProductId;
    }

    public void setChannelProductId(String channelProductId) {
        this.channelProductId = channelProductId;
    }

    public PriceDTO getPrices() {
        return prices;
    }

    public void setPrices(PriceDTO prices) {
        this.prices = prices;
    }
    
    public BigDecimal getMsp() {
        return prices.getMsp();
    }

    public void setMsp(BigDecimal msp) {
        this.prices.setMsp(msp);
    }

    public BigDecimal getMrp() {
        return prices.getMrp();
    }

    public void setMrp(BigDecimal mrp) {
        this.setMrp(mrp);
    }

    public BigDecimal getSellingPrice() {
        return prices.getSellingPrice();
    }

    public void setSellingPrice(BigDecimal sellingPrice) {
        this.prices.setSellingPrice(sellingPrice);
    }

    public BigDecimal getTransferPrice() {
        return prices.getTransferPrice();
    }

    public void setTransferPrice(BigDecimal transferPrice) {
        this.setTransferPrice(transferPrice);
    }

    public BigDecimal getCompetitivePrice() {
        return prices.getCompetitivePrice();
    }

    public void setCompetitivePrice(BigDecimal competitivePrice) {
        this.prices.setCompetitivePrice(competitivePrice);
    }
    
    public String getCurrencyCode() {
        return prices.getCurrencyCode();
    }
    
    public void setCurrencyCode(String currencyCode) {
        this.prices.setCurrencyCode(currencyCode);
    }
}