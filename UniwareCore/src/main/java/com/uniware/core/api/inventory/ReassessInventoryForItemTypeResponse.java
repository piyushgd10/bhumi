/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 10-Jun-2014
 *  @author akshay
 */
package com.uniware.core.api.inventory;

import com.unifier.core.api.base.ServiceResponse;

public class ReassessInventoryForItemTypeResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

}
