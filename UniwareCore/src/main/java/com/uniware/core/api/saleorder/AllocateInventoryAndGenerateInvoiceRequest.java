/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 27-Aug-2015
 *  @author parijat
 */
package com.uniware.core.api.saleorder;

import com.unifier.core.api.base.ServiceRequest;
import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.NotBlank;

public class AllocateInventoryAndGenerateInvoiceRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 876545678765567L;

    @NotBlank
    private String            saleOrderCode;

    @NotNull
    private Integer           userId;

    @Valid
    private List<SaleOrderItemForDetail> saleOrderItemForDetailList;

    public AllocateInventoryAndGenerateInvoiceRequest() {
    }

    public AllocateInventoryAndGenerateInvoiceRequest(String saleOrderCode, Integer userId, List<SaleOrderItemForDetail> saleOrderItemForDetailList) {
        this.saleOrderCode = saleOrderCode;
        this.userId = userId;
        this.saleOrderItemForDetailList = saleOrderItemForDetailList;
    }

    public List<SaleOrderItemForDetail> getSaleOrderItemForDetailList() {
        return saleOrderItemForDetailList;
    }

    public void setSaleOrderItemForDetailList(List<SaleOrderItemForDetail> saleOrderItemForDetailList) {
        this.saleOrderItemForDetailList = saleOrderItemForDetailList;
    }

    public String getSaleOrderCode() {
        return saleOrderCode;
    }

    public void setSaleOrderCode(String saleOrderCode) {
        this.saleOrderCode = saleOrderCode;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }
}
