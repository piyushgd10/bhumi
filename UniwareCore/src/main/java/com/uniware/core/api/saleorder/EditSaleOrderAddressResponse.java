/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 25-Sep-2012
 *  @author praveeng
 */
package com.uniware.core.api.saleorder;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author praveeng
 */
public class EditSaleOrderAddressResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 6610937437159238943L;

}
