/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jun 20, 2012
 *  @author ankit
 */
package com.uniware.core.api.async;

/**
 * @author Sunny
 */
public class PollingResult {

    private long    mileStoneCount = 100;
    private long    currentMileStone;
    private String  currentStatus;
    private boolean completed;
    private boolean successful;
    private float   percentageComplete;

    public float getPercentageComplete() {
        return percentageComplete;
    }

    public void setMileStone(String currentStatus) {
        this.currentStatus = currentStatus;
        currentMileStone++;
        percentageComplete = ((float) (currentMileStone * 10000 / mileStoneCount)) / 100;
    }

    public long getMileStoneCount() {
        return mileStoneCount;
    }

    public void setMileStoneCount(long mileStoneCount) {
        this.mileStoneCount = mileStoneCount;
    }

    public long getCurrentMileStone() {
        return currentMileStone;
    }

    public void setCurrentMileStone(long currentMileStone) {
        this.currentMileStone = currentMileStone;
    }

    public String getCurrentStatus() {
        return currentStatus;
    }

    public void setCurrentStatus(String currentStatus) {
        this.currentStatus = currentStatus;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    public boolean isSuccessful() {
        return successful;
    }

    public void setSuccessful(boolean successful) {
        this.successful = successful;
    }
}
