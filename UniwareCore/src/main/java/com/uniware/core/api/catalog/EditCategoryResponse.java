/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 10, 2012
 *  @author praveeng
 */
package com.uniware.core.api.catalog;

/**
 * @author praveeng
 */
public class EditCategoryResponse extends CategoryResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 3793511917105307580L;

}
