/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *  @version     1.0, Nov 27, 2015
 *  @author      shobhit
 */

package com.uniware.core.api.prices;

import com.unifier.core.api.base.ServiceResponse;

import java.util.List;

public class GetUIPricePropertiesByChannelResponse extends ServiceResponse {

    private static final long serialVersionUID = 4232588337843254365L;

    private List<String> availableFields;

    private List<String> editableFields;

    private boolean priceComputationSupported;

    public List<String> getAvailableFields() {
        return availableFields;
    }

    public void setAvailableFields(List<String> availableFields) {
        this.availableFields = availableFields;
    }

    public List<String> getEditableFields() {
        return editableFields;
    }

    public void setEditableFields(List<String> editableFields) {
        this.editableFields = editableFields;
    }

    public boolean isPriceComputationSupported() {
        return priceComputationSupported;
    }

    public void setPriceComputationSupported(boolean priceComputationSupported) {
        this.priceComputationSupported = priceComputationSupported;
    }
}
