/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 26-Jul-2013
 *  @author sunny
 */
package com.uniware.core.api.bundle;

import java.math.BigDecimal;

import com.uniware.core.api.bundle.BundleDTO.ItemTypeDTO;
import com.uniware.core.entity.BundleItemType;

public class ComponentItemTypeDTO {

    private ItemTypeDTO itemTypeDTO;
    private Integer     quantity;
    private BigDecimal  price;
    private BigDecimal  priceRatio;

    public ComponentItemTypeDTO(BundleItemType bundleItemType) {
        itemTypeDTO = new ItemTypeDTO(bundleItemType.getItemType());
        quantity = bundleItemType.getQuantity();
        price = bundleItemType.getPrice();
        priceRatio = bundleItemType.getPriceRatio();
    }

    public ItemTypeDTO getItemTypeDTO() {
        return itemTypeDTO;
    }

    public void setItemTypeDTO(ItemTypeDTO itemTypeDTO) {
        this.itemTypeDTO = itemTypeDTO;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getPriceRatio() {
        return priceRatio;
    }

    public void setPriceRatio(BigDecimal priceRatio) {
        this.priceRatio = priceRatio;
    }

    @Override
    public String toString() {
        return "ComponentItemTypeDTO [itemTypeDTO=" + itemTypeDTO.getSkuCode() + ", quantity=" + quantity + ", price=" + price + ", priceRatio=" + priceRatio + "]";
    }

}
