/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 03-Jun-2012
 *  @author vibhu
 */
package com.uniware.core.api.channel;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author Sunny
 */
public class PreConfigureChannelConnectorRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -8298110712457622895L;

    @NotBlank
    private String            channelCode;

    @NotBlank
    private String            connectorName;
    
    @Valid
    private List<WsChannelConnectorParameter> channelConnectorParameters = new ArrayList<>();

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    public String getConnectorName() {
        return connectorName;
    }

    public void setConnectorName(String connectorName) {
        this.connectorName = connectorName;
    }

    public List<WsChannelConnectorParameter> getChannelConnectorParameters() {
        return channelConnectorParameters;
    }

    public void setChannelConnectorParameters(List<WsChannelConnectorParameter> channelConnectorParameters) {
        this.channelConnectorParameters = channelConnectorParameters;
    }

}
