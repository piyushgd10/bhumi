/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 18-Apr-2013
 *  @author unicom
 */
package com.uniware.core.api.channel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.persistence.Transient;

import com.unifier.core.api.customfields.CustomFieldMetadataDTO;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.utils.CollectionUtils;
import com.unifier.core.utils.StringUtils;
import com.uniware.core.cache.FacilityCache;
import com.uniware.core.entity.Channel;
import com.uniware.core.entity.Facility;
import com.uniware.core.entity.Source;

/**
 * @author Sunny Agarwal
 */
public class ChannelDetailDTO implements Serializable {

    private int                                           id;
    private String                                        code;
    private String                                        name;
    private SourceDetailDTO                               source;
    private boolean                                       enabled;
    private String                                        colorCode;
    private String                                        shortName;
    private String                                        orderSyncStatus;
    private String                                        inventorySyncStatus;
    private String                                        channelWarehouseInventorySyncStatus;
    private String                                        orderReconciliationSyncStatus;
    private String                                        pricingSyncStatus;
    private boolean                                       thirdPartyShipping;
    private String                                        customerCode;
    private String                                        billingPartyCode;
    private String                                        associatedFacilityCode;
    private String                                        ledgerName;
    private List<Integer>                                 associatedFacilityIds;
    private List<ChannelAssociatedFacilityDTO>            associatedFacilities;
    private List<CustomFieldMetadataDTO>                  customFieldValues;
    private List<ChannelConnectorDTO>                     channelConnectors              = new ArrayList<ChannelConnectorDTO>();
    private List<ChannelConfigurationParameterDTO>        channelConfigurationParameters = new ArrayList<ChannelConfigurationParameterDTO>();
    private Map<String, ChannelConfigurationParameterDTO> parameterNameToParameter       = new ConcurrentHashMap<String, ChannelConfigurationParameterDTO>();

    public ChannelDetailDTO() {
    }

    public ChannelDetailDTO(Channel channel, Source source) {
        id = channel.getId();
        code = channel.getCode();
        name = channel.getName();
        enabled = channel.isEnabled();
        colorCode = channel.getColorCode();
        shortName = channel.getShortName();
        orderSyncStatus = channel.getOrderSyncStatus().name();
        inventorySyncStatus = channel.getInventorySyncStatus().name();
        thirdPartyShipping = channel.isThirdPartyShipping();
        customerCode = channel.getCustomer() != null ? channel.getCustomer().getCode() : null;
        billingPartyCode = channel.getBillingParty() != null ? channel.getBillingParty().getCode() : null;
        ledgerName = channel.getLedgerName();
        this.source = new SourceDetailDTO(source);
        orderReconciliationSyncStatus = channel.getReconciliationSyncStatus().name();
        pricingSyncStatus = channel.getPricingSyncStatus().name();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getChannelWarehouseInventorySyncStatus() {
        return channelWarehouseInventorySyncStatus;
    }

    public void setChannelWarehouseInventorySyncStatus(String channelWarehouseInventorySyncStatus) {
        this.channelWarehouseInventorySyncStatus = channelWarehouseInventorySyncStatus;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public SourceDetailDTO getSource() {
        return source;
    }

    public void setSource(SourceDetailDTO source) {
        this.source = source;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getOrderSyncStatus() {
        return orderSyncStatus;
    }

    public void setOrderSyncStatus(String orderSyncStatus) {
        this.orderSyncStatus = orderSyncStatus;
    }

    public String getInventorySyncStatus() {
        return inventorySyncStatus;
    }

    public void setInventorySyncStatus(String inventorySyncStatus) {
        this.inventorySyncStatus = inventorySyncStatus;
    }

    public String getPricingSyncStatus() {
        return pricingSyncStatus;
    }

    public void setPricingSyncStatus(String pricingSyncStatus) {
        this.pricingSyncStatus = pricingSyncStatus;
    }

    public boolean isThirdPartyShipping() {
        return thirdPartyShipping;
    }

    public void setThirdPartyShipping(boolean thirdPartyShipping) {
        this.thirdPartyShipping = thirdPartyShipping;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    public String getBillingPartyCode() {
        return billingPartyCode;
    }

    public void setBillingPartyCode(String billingPartyCode) {
        this.billingPartyCode = billingPartyCode;
    }

    public List<CustomFieldMetadataDTO> getCustomFieldValues() {
        return customFieldValues;
    }

    public void setCustomFieldValues(List<CustomFieldMetadataDTO> customFieldValues) {
        this.customFieldValues = customFieldValues;
    }

    public List<ChannelConnectorDTO> getChannelConnectors() {
        return channelConnectors;
    }

    public void setChannelConnectors(List<ChannelConnectorDTO> channelConnectors) {
        this.channelConnectors = channelConnectors;
    }

    public List<ChannelConfigurationParameterDTO> getChannelConfigurationParameters() {
        return channelConfigurationParameters;
    }

    public void setChannelConfigurationParameters(List<ChannelConfigurationParameterDTO> channelConfigurationParameters) {
        this.channelConfigurationParameters = channelConfigurationParameters;
    }

    public String getOrderReconciliationSyncStatus() {
        return orderReconciliationSyncStatus;
    }

    public void setOrderReconciliationSyncStatus(String orderReconciliationSyncStatus) {
        this.orderReconciliationSyncStatus = orderReconciliationSyncStatus;
    }

    public String getLedgerName() {
        return ledgerName;
    }

    public void setLedgerName(String ledgerName) {
        this.ledgerName = ledgerName;
    }

    public String getColorCode() {
        return colorCode;
    }

    public void setColorCode(String colorCode) {
        this.colorCode = colorCode;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getInventoryUpdateFormula() {
        Object paramVal = getParameterValue(Channel.INVENTORY_UPDATE_FORMULA);
        return paramVal != null ? String.valueOf(paramVal) : null;
    }

    public String getPackageType() {
        Object paramVal = getParameterValue(Channel.PACKAGE_TYPE);
        return paramVal != null ? String.valueOf(paramVal) : null;
    }

    public String getAssociatedFacility() {
        return associatedFacilityCode;
    }

    public void setAssociatedFacilityCode(String associatedFacilityCode) {
        this.associatedFacilityCode = associatedFacilityCode;
    }

    public List<Integer> getAssociatedFacilityIds() {
        return associatedFacilityIds;
    }

    public void setAssociatedFacilityIds(List<Integer> associatedFacilityIds) {
        this.associatedFacilityIds = associatedFacilityIds;
    }

    public List<ChannelAssociatedFacilityDTO> getAssociatedFacilities() {
        return associatedFacilities;
    }

    public void setAssociatedFacilities(List<ChannelAssociatedFacilityDTO> associatedFacilities) {
        this.associatedFacilities = associatedFacilities;
    }

    public boolean isProductRelistingEnabled() {
        Object paramVal = getParameterValue(Channel.PRODUCT_RELISTING_ENABLED);
        String paramValStr = paramVal != null ? String.valueOf(paramVal) : null;
        return Boolean.parseBoolean(paramValStr);
    }

    public Object getParameterValue(String parameterName) {
        if (parameterNameToParameter.get(parameterName) == null) {
            for (ChannelConfigurationParameterDTO parameter : channelConfigurationParameters) {
                parameterNameToParameter.put(parameter.getName(), parameter);
            }
        }
        if (parameterNameToParameter.get(parameterName) != null) {
            return parameterNameToParameter.get(parameterName).getValue();
        } else {
            return null;
        }
    }

    @Transient
    public boolean isOn() {
        return Channel.SyncStatus.ON.name().equals(this.orderSyncStatus);
    }

    @Transient
    public List<Integer> getFacilitiesForChannelInventorySync() {
        List<Integer> facilityIds = null;
        if (getSource().isFacilityAssociationRequired() && StringUtils.isNotBlank(getAssociatedFacility())) {
            Facility associatedFacility = CacheManager.getInstance().getCache(FacilityCache.class).getFacilityByCode(getAssociatedFacility());
            if (associatedFacility != null) {
                facilityIds = Collections.singletonList(associatedFacility.getId());
            }
        }
        if (CollectionUtils.isEmpty(facilityIds)) {
            facilityIds = CollectionUtils.isEmpty(getAssociatedFacilityIds()) ? CacheManager.getInstance().getCache(FacilityCache.class).getFacilitiesForChannelInventorySync()
                    : getAssociatedFacilityIds();
        }
        return facilityIds;
    }
}
