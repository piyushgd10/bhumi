/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 15-Jan-2014
 *  @author akshay
 */
package com.uniware.core.api.catalog;

import java.util.ArrayList;
import java.util.List;

import com.unifier.core.api.base.ServiceResponse;

public class GetServiceabilityResponse extends ServiceResponse{


    /**
     * 
     */
    private static final long serialVersionUID = -4073458192014823898L;

    private List<String>      facilityCodes    = new ArrayList<String>();

    public List<String> getFacilityCodes() {
        return facilityCodes;
    }

    public void setFacilityCodes(List<String> facilityCodes) {
        this.facilityCodes = facilityCodes;
    }

}
