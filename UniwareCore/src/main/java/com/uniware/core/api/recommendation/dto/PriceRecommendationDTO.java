/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Nov 25, 2015
 *  @author akshay
 */
package com.uniware.core.api.recommendation.dto;

import com.uniware.core.vo.PriceRecommendationVO;

public class PriceRecommendationDTO extends RecommendationBody {

    private String              channelCode;
    private String              channelProductId;
    private String              sellerSkuCode;
    private String              recommendationType = "PRICE";
    private RecommendedPriceDTO recommendedPrice;

    public PriceRecommendationDTO() {
        super();
    }

    public PriceRecommendationDTO(PriceRecommendationVO rec) {
        this.channelCode = rec.getChannelCode();
        this.channelProductId = rec.getChannelProductId();
        this.sellerSkuCode = rec.getSellerSkuCode();
        this.recommendedPrice = new RecommendedPriceDTO(rec.getMsp(), rec.getMrp(), rec.getSellingPrice(), rec.getCurrencyCode());
    }

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    public String getChannelProductId() {
        return channelProductId;
    }

    public void setChannelProductId(String channelProductId) {
        this.channelProductId = channelProductId;
    }

    public String getSellerSkuCode() {
        return sellerSkuCode;
    }

    public void setSellerSkuCode(String sellerSkuCode) {
        this.sellerSkuCode = sellerSkuCode;
    }

    public String getRecommendationType() {
        return recommendationType;
    }

    public void setRecommendationType(String recommendationType) {
        this.recommendationType = recommendationType;
    }

    public RecommendedPriceDTO getRecommendedPrice() {
        return recommendedPrice;
    }

    public void setRecommendedPrice(RecommendedPriceDTO recommendedPrice) {
        this.recommendedPrice = recommendedPrice;
    }
}
