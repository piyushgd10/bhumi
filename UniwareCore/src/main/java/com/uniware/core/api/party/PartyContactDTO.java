/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 20, 2012
 *  @author praveeng
 */
package com.uniware.core.api.party;

import com.uniware.core.entity.PartyContact;

/**
 * @author praveeng
 */
public class PartyContactDTO {
    private String contactType;
    private String contactTypeName;
    private String name;
    private String email;
    private String phone;
    private String fax;

    /**
     * @param partyContact
     */
    public PartyContactDTO(PartyContact partyContact) {
        this.contactType = partyContact.getPartyContactType().getCode();
        this.contactTypeName = partyContact.getPartyContactType().getName();
        this.name = partyContact.getName();
        this.email = partyContact.getEmail();
        this.phone = partyContact.getPhone();
        this.fax = partyContact.getFax();
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone the phone to set
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * @return the fax
     */
    public String getFax() {
        return fax;
    }

    /**
     * @param fax the fax to set
     */
    public void setFax(String fax) {
        this.fax = fax;
    }

    /**
     * @return the contactType
     */
    public String getContactType() {
        return contactType;
    }

    /**
     * @param contactType the contactType to set
     */
    public void setContactType(String contactType) {
        this.contactType = contactType;
    }

    /**
     * @return the contactTypeName
     */
    public String getContactTypeName() {
        return contactTypeName;
    }

    /**
     * @param contactTypeName the contactTypeName to set
     */
    public void setContactTypeName(String contactTypeName) {
        this.contactTypeName = contactTypeName;
    }

}
