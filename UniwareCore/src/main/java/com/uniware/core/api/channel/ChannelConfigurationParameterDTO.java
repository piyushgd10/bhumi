/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jan 13, 2015
 *  @author harshpal
 */
package com.uniware.core.api.channel;

import com.uniware.core.entity.ChannelConfigurationParameter;

import java.io.Serializable;

public class ChannelConfigurationParameterDTO implements Serializable {
    private String name;
    private Object value;

    public ChannelConfigurationParameterDTO(ChannelConfigurationParameter cp) {
        this.name = cp.getSourceConfigurationParameterName();
        this.value = cp.getValue();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

}
