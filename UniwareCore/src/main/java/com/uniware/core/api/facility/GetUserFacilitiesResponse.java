/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jun 20, 2012
 *  @author ankit
 */
package com.uniware.core.api.facility;

import java.util.List;

import com.unifier.core.api.base.ServiceResponse;
import com.uniware.core.api.warehouse.FacilityDTO;

/**
 * @author Sunny
 */
public class GetUserFacilitiesResponse extends ServiceResponse {

    private static final long serialVersionUID = -5342490352978264760L;

    private List<FacilityDTO> facilities;

    private boolean           accessibleAllFacilities;

    private String            currentFacilityCode;

    public List<FacilityDTO> getFacilities() {
        return facilities;
    }

    public void setFacilities(List<FacilityDTO> facilities) {
        this.facilities = facilities;
    }

    public boolean isAccessibleAllFacilities() {
        return accessibleAllFacilities;
    }

    public void setAccessibleAllFacilities(boolean accessibleAllFacilities) {
        this.accessibleAllFacilities = accessibleAllFacilities;
    }

    public String getCurrentFacilityCode() {
        return currentFacilityCode;
    }

    public void setCurrentFacilityCode(String currentFacilityCode) {
        this.currentFacilityCode = currentFacilityCode;
    }

}
