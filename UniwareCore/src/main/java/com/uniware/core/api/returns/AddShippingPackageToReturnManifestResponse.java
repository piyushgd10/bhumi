/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, May 26, 2012
 *  @author singla
 */
package com.uniware.core.api.returns;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author singla
 */
public class AddShippingPackageToReturnManifestResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long           serialVersionUID = -2997117357489910558L;
    private ReturnManifestItemDetailDTO manifestItem;
    private Integer                     totalManifestItemCount;

    public ReturnManifestItemDetailDTO getManifestItem() {
        return manifestItem;
    }

    public void setManifestItem(ReturnManifestItemDetailDTO manifestItem) {
        this.manifestItem = manifestItem;
    }

    public Integer getTotalManifestItemCount() {
        return totalManifestItemCount;
    }

    public void setTotalManifestItemCount(Integer totalManifestItemCount) {
        this.totalManifestItemCount = totalManifestItemCount;
    }

}
