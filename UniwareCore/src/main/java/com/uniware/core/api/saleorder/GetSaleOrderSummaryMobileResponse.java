package com.uniware.core.api.saleorder;

import com.uniware.core.api.saleorder.display.GetSaleOrderSummaryResponse;

/**
 * Created by akshayag on 9/7/15.
 */
public class GetSaleOrderSummaryMobileResponse extends GetSaleOrderSummaryResponse{
    private static final long serialVersionUID = 346789876543456789L;

    private String sourceCode;
    private String colorCode;
    private String shortName;

    public GetSaleOrderSummaryMobileResponse() {

    }

    public GetSaleOrderSummaryMobileResponse(GetSaleOrderSummaryResponse response) {
        this.setSaleOrderSummary(response.getSaleOrderSummary());
        this.setErrors(response.getErrors());
        this.setMessage(response.getMessage());
        this.setSuccessful(response.isSuccessful());
    }

    public String getSourceCode() {
        return sourceCode;
    }

    public void setSourceCode(String sourceCode) {
        this.sourceCode = sourceCode;
    }

    public String getColorCode() {
        return colorCode;
    }

    public void setColorCode(String colorCode) {
        this.colorCode = colorCode;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

}
