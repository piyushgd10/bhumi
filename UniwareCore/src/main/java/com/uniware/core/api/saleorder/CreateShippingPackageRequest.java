/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 02-Jun-2014
 *  @author unicom
 */
package com.uniware.core.api.saleorder;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import com.google.gson.Gson;
import com.unifier.core.api.base.ServiceRequest;

public class CreateShippingPackageRequest extends ServiceRequest {

    private static final long serialVersionUID = 7672781000385690945L;

    @NotBlank
    private String            saleOrderCode;

    @NotEmpty
    private List<String>      saleOrderItemCodes;

    public String getSaleOrderCode() {
        return saleOrderCode;
    }

    public void setSaleOrderCode(String saleOrderCode) {
        this.saleOrderCode = saleOrderCode;
    }

    public List<String> getSaleOrderItemCodes() {
        return saleOrderItemCodes;
    }

    public void setSaleOrderItemCodes(List<String> saleOrderItemCodes) {
        this.saleOrderItemCodes = saleOrderItemCodes;
    }

    public static void main(String[] args) {
        CreateShippingPackageRequest request = new CreateShippingPackageRequest();
        request.setSaleOrderCode("123231");
        List<String> saleOrderItemCodes = new ArrayList<String>();
        saleOrderItemCodes.add("dsadasd");
        request.setSaleOrderItemCodes(saleOrderItemCodes);
        System.out.println(new Gson().toJson(request));
    }
}
