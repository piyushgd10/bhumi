/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 13-Aug-2013
 *  @author parijat
 */
package com.uniware.core.api.inventory;

import java.util.ArrayList;
import java.util.List;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author parijat
 */
public class UpdateInventoryOnChannelResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 8279106511789864171L;
    
    List<ChannelInventorySyncResponseDTO> resultItems      = new ArrayList<>();

    public static class ChannelInventorySyncResponseDTO {
        private String channelCode;
        private String message;

        public ChannelInventorySyncResponseDTO() {

        }

        public ChannelInventorySyncResponseDTO(String channelCode, String message) {
            this.channelCode = channelCode;
            this.message = message;
        }

        public String getChannelCode() {
            return channelCode;
        }

        public void setChannelCode(String channelCode) {
            this.channelCode = channelCode;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

    }

    public List<ChannelInventorySyncResponseDTO> getResultItems() {
        return resultItems;
    }

    public void setResultItems(List<ChannelInventorySyncResponseDTO> resultItems) {
        this.resultItems = resultItems;
    }

}
