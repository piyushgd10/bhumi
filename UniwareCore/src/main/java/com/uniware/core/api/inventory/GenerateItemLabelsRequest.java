/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 11/06/14
 *  @author amit
 */

package com.uniware.core.api.inventory;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.validation.constraints.Past;

import com.unifier.core.api.base.ServiceRequest;

public class GenerateItemLabelsRequest extends ServiceRequest {

    private static final long serialVersionUID = -7463435897013674004L;

    private List<WsItemLabel> itemLabelList    = new ArrayList<>();

    private int               vendorId;

    @Past
    private Date              manufacturingDate;

    public GenerateItemLabelsRequest() {
        super();
    }

    public List<WsItemLabel> getItemLabelList() {
        return itemLabelList;
    }

    public void setItemLabelList(List<WsItemLabel> itemLabelList) {
        this.itemLabelList = itemLabelList;
    }

    public int getVendorId() {
        return vendorId;
    }

    public void setVendorId(int vendorId) {
        this.vendorId = vendorId;
    }

    public Date getManufacturingDate() {
        return manufacturingDate;
    }

    public void setManufacturingDate(Date manufacturingDate) {
        this.manufacturingDate = manufacturingDate;
    }

    @Override
    public String toString() {
        return "GenerateItemLabelsRequest{" + "itemLabelList=" + itemLabelList + ", vendorId=" + vendorId + ", manufacturingDate=" + manufacturingDate + "}";
    }
}
