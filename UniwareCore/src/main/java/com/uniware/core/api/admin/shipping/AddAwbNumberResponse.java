/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 03-Jun-2012
 *  @author vibhu
 */
package com.uniware.core.api.admin.shipping;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author vibhu
 */
public class AddAwbNumberResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 3388551590054156545L;

    private int               addedNumber;

    private int               duplicateNumbers;

    /**
     * @return the addedNumber
     */
    public int getAddedNumber() {
        return addedNumber;
    }

    /**
     * @param addedNumber the addedNumber to set
     */
    public void setAddedNumber(int addedNumber) {
        this.addedNumber = addedNumber;
    }

    public int getDuplicateNumbers() {
        return duplicateNumbers;
    }

    public void setDuplicateNumbers(int duplicateNumbers) {
        this.duplicateNumbers = duplicateNumbers;
    }

    public void addNumberAdded() {
        this.addedNumber++;
    }

    public void addDuplicateNumberAdded() {
        this.duplicateNumbers++;
    }
}
