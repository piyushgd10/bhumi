package com.uniware.core.api.channelWarehouse;

import com.unifier.core.api.base.ServiceResponse;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by digvijaysharma on 20/01/17.
 */
public class SyncChannelWarehouseInventoryResponse extends ServiceResponse {

    /**
     *
     */
    private static final long         serialVersionUID = 8034049854364994904L;

    List<SyncChannelWarehouseInventoryResponse.ChannelWarehouseInventorySyncResponseDTO> resultItems      = new ArrayList<>();

    public static class ChannelWarehouseInventorySyncResponseDTO {
        private String channelCode;
        private String message;

        public ChannelWarehouseInventorySyncResponseDTO() {

        }

        public ChannelWarehouseInventorySyncResponseDTO(String channelCode, String message) {
            this.channelCode = channelCode;
            this.message = message;
        }

        public String getChannelCode() {
            return channelCode;
        }

        public void setChannelCode(String channelCode) {
            this.channelCode = channelCode;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

    }

    public List<SyncChannelWarehouseInventoryResponse.ChannelWarehouseInventorySyncResponseDTO> getResultItems() {
        return resultItems;
    }

    public void setResultItems(List<SyncChannelWarehouseInventoryResponse.ChannelWarehouseInventorySyncResponseDTO> resultItems) {
        this.resultItems = resultItems;
    }

}
