/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 26-Aug-2015
 *  @author akshay
 */
package com.uniware.core.api.shipping;

import com.unifier.core.api.base.ServiceResponse;

public class VerifySaleOrderItemsPODCodeResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 5370150557456170633L;

}
