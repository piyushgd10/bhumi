/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 20-Jun-2013
 *  @author karunsingla
 */
package com.uniware.core.api.returns;

import com.unifier.core.api.base.ServiceResponse;

public class RedispatchReversePickupResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -7737024779907260786L;

}
