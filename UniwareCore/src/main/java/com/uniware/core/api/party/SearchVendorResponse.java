/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 20, 2012
 *  @author praveeng
 */
package com.uniware.core.api.party;

import com.unifier.core.api.base.ServiceResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * @author praveeng
 */
public class SearchVendorResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 3063459246624174168L;
    List<VendorSearchDTO>     vendors          = new ArrayList<VendorSearchDTO>();

    /**
     * @return the vendors
     */
    public List<VendorSearchDTO> getVendors() {
        return vendors;
    }

    /**
     * @param vendors the vendors to set
     */
    public void setVendors(List<VendorSearchDTO> vendors) {
        this.vendors = vendors;
    }

    public static class VendorSearchDTO {
        private Integer id;
        private String  code;
        private String  name;
        private String  website;
        private String  contactName;
        private String  contactEmail;
        private String  city;
        private String  phone;

        /**
         * @return the id
         */
        public Integer getId() {
            return id;
        }

        /**
         * @param id the id to set
         */
        public void setId(Integer id) {
            this.id = id;
        }

        /**
         * @return the code
         */
        public String getCode() {
            return code;
        }

        /**
         * @param code the code to set
         */
        public void setCode(String code) {
            this.code = code;
        }

        /**
         * @return the name
         */
        public String getName() {
            return name;
        }

        /**
         * @param name the name to set
         */
        public void setName(String name) {
            this.name = name;
        }

        /**
         * @return the website
         */
        public String getWebsite() {
            return website;
        }

        /**
         * @param website the website to set
         */
        public void setWebsite(String website) {
            this.website = website;
        }

        /**
         * @return the contactName
         */
        public String getContactName() {
            return contactName;
        }

        /**
         * @param contactName the contactName to set
         */
        public void setContactName(String contactName) {
            this.contactName = contactName;
        }

        /**
         * @return the contactEmail
         */
        public String getContactEmail() {
            return contactEmail;
        }

        /**
         * @param contactEmail the contactEmail to set
         */
        public void setContactEmail(String contactEmail) {
            this.contactEmail = contactEmail;
        }

        /**
         * @return the city
         */
        public String getCity() {
            return city;
        }

        /**
         * @param city the city to set
         */
        public void setCity(String city) {
            this.city = city;
        }

        /**
         * @return the phone
         */
        public String getPhone() {
            return phone;
        }

        /**
         * @param phone the phone to set
         */
        public void setPhone(String phone) {
            this.phone = phone;
        }

    }

}
