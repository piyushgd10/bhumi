/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 17-Feb-2015
 *  @author parijat
 */
package com.uniware.core.api.admin.shipping;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author parijat
 *
 */
public class EditShippingServiceabilityResposne extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 8169360198313228863L;

    private int               locationDeleteCount;

    /**
     * @return the locationDeleteCount
     */
    public int getLocationDeleteCount() {
        return locationDeleteCount;
    }

    /**
     * @param locationDeleteCount the locationDeleteCount to set
     */
    public void setLocationDeleteCount(int locationDeleteCount) {
        this.locationDeleteCount = locationDeleteCount;
    }

}
