package com.uniware.core.api.o2o;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import com.unifier.core.api.base.ServiceRequest;

/**
 * Created by sunny on 09/04/15.
 */
public class GetAvailableFacilitiesRequest extends ServiceRequest {

    /**
     *
     */
    private static final long serialVersionUID = -8031335870840949000L;

    @Valid
    @NotEmpty
    private List<WsProduct> products;

    private String pincode;

    private Location location;

    @NotNull
    private List<String> shippingMethods = new ArrayList<>();

    @NotNull
    private Integer proximityInKms = 10;

    @NotNull
    private Integer numberOfResults = 10;

    public List<WsProduct> getProducts() {
        return products;
    }

    public void setProducts(List<WsProduct> products) {
        this.products = products;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public List<String> getShippingMethods() {
        return shippingMethods;
    }

    public void setShippingMethods(List<String> shippingMethods) {
        this.shippingMethods = (shippingMethods != null ? shippingMethods : new ArrayList<String>());
    }

    public Integer getProximityInKms() {
        return proximityInKms;
    }

    public void setProximityInKms(Integer proximityInKms) {
        this.proximityInKms = (proximityInKms != null ? proximityInKms : 10);
    }

    public Integer getNumberOfResults() {
        return numberOfResults;
    }

    public void setNumberOfResults(Integer numberOfResults) {
        this.numberOfResults = (numberOfResults != null ? numberOfResults : 10);
    }

    public static class WsProduct {
        @NotBlank
        private String sellerSkuCode;

        @NotNull
        private Integer quantity;

        public String getSellerSkuCode() {
            return sellerSkuCode;
        }

        public void setSellerSkuCode(String sellerSkuCode) {
            this.sellerSkuCode = sellerSkuCode;
        }

        public Integer getQuantity() {
            return quantity;
        }

        public void setQuantity(Integer quantity) {
            this.quantity = quantity;
        }
    }

    public static class Location {
        @NotNull
        private Double longitude;
        @NotNull
        private Double latitude;

        public Location() {
        }

        public Location(Double longitude, Double latitude) {
            this.longitude = longitude;
            this.latitude = latitude;
        }

        public double getLongitude() {
            return longitude;
        }

        public void setLongitude(double longitude) {
            this.longitude = longitude;
        }

        public double getLatitude() {
            return latitude;
        }

        public void setLatitude(double latitude) {
            this.latitude = latitude;
        }
    }

}
