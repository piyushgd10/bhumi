package com.uniware.core.api.cyclecount;

import java.util.List;

import com.unifier.core.api.base.ServiceResponse;

/**
 * Created by harshpal on 3/7/16.
 */
public class CompleteCountForShelfResponse extends ServiceResponse {

    private List<ErrorItemDTO> errorItems;

    private List<String>       invalidItemCodes;

    public List<ErrorItemDTO> getErrorItems() {
        return errorItems;
    }

    public void setErrorItems(List<ErrorItemDTO> errorItems) {
        this.errorItems = errorItems;
    }

    public List<String> getInvalidItemCodes() {
        return invalidItemCodes;
    }

    public void setInvalidItemCodes(List<String> invalidItemCodes) {
        this.invalidItemCodes = invalidItemCodes;
    }
}
