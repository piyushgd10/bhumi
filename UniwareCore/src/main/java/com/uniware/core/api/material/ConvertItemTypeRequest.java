/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 05-Sep-2012
 *  @author praveeng
 */
package com.uniware.core.api.material;

import java.util.HashSet;
import java.util.Set;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author praveeng
 */
public class ConvertItemTypeRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 6394699042567239973L;

    @NotBlank
    private String            fromItemType;

    @NotBlank
    private String            toItemType;

    @NotNull
    private Integer           fromRatio;

    @NotNull
    private Integer           toRatio;

    @NotBlank
    private String            shelfCode;

    private Set<String>       itemCodes        = new HashSet<String>();

    /**
     * @return the fromItemType
     */
    public String getFromItemType() {
        return fromItemType;
    }

    /**
     * @param fromItemType the fromItemType to set
     */
    public void setFromItemType(String fromItemType) {
        this.fromItemType = fromItemType;
    }

    /**
     * @return the toItemType
     */
    public String getToItemType() {
        return toItemType;
    }

    /**
     * @param toItemType the toItemType to set
     */
    public void setToItemType(String toItemType) {
        this.toItemType = toItemType;
    }

    /**
     * @return the fromRatio
     */
    public Integer getFromRatio() {
        return fromRatio;
    }

    /**
     * @param fromRatio the fromRatio to set
     */
    public void setFromRatio(Integer fromRatio) {
        this.fromRatio = fromRatio;
    }

    /**
     * @return the toRatio
     */
    public Integer getToRatio() {
        return toRatio;
    }

    /**
     * @param toRatio the toRatio to set
     */
    public void setToRatio(Integer toRatio) {
        this.toRatio = toRatio;
    }

    /**
     * @return the shelfCode
     */
    public String getShelfCode() {
        return shelfCode;
    }

    /**
     * @param shelfCode the shelfCode to set
     */
    public void setShelfCode(String shelfCode) {
        this.shelfCode = shelfCode;
    }

    /**
     * @return the itemCodes
     */
    public Set<String> getItemCodes() {
        return itemCodes;
    }

    /**
     * @param itemCodes the itemCodes to set
     */
    public void setItemCodes(Set<String> itemCodes) {
        this.itemCodes = itemCodes;
    }

}
