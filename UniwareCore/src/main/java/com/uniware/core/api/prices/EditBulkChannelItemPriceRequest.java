/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *  @version     1.0, Dec 4, 2015
 *  @author shipra
 */
package com.uniware.core.api.prices;

import com.unifier.core.api.base.ServiceRequest;
import org.hibernate.validator.constraints.NotBlank;

import java.util.List;

public class EditBulkChannelItemPriceRequest extends ServiceRequest {
    private List<EditOneChannelItemTypePriceRequest> channelItemTypePriceRequests;

    @NotBlank
    private String channelCode;

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    public List<EditOneChannelItemTypePriceRequest> getChannelItemTypePriceRequests() {
        return channelItemTypePriceRequests;
    }

    public void setChannelItemTypePriceRequests(List<EditOneChannelItemTypePriceRequest> channelItemTypePriceRequests) {
        this.channelItemTypePriceRequests = channelItemTypePriceRequests;
    }
}
