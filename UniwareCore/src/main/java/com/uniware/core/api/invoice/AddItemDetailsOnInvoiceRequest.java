/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 28-Jun-2013
 *  @author karunsingla
 */
package com.uniware.core.api.invoice;

import com.unifier.core.api.base.ServiceRequest;

import java.util.List;

import javax.validation.Valid;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import com.uniware.core.api.inflow.ItemDetail;

public class AddItemDetailsOnInvoiceRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -2811231208649482670L;

    @NotBlank
    private String            saleOrderCode;

    @NotBlank
    private String            saleOrderItemCode;

    @NotEmpty
    @Valid
    private List<ItemDetail>  itemDetails;

    public String getSaleOrderCode() {
        return saleOrderCode;
    }

    public void setSaleOrderCode(String saleOrderCode) {
        this.saleOrderCode = saleOrderCode;
    }

    public String getSaleOrderItemCode() {
        return saleOrderItemCode;
    }

    public void setSaleOrderItemCode(String saleOrderItemCode) {
        this.saleOrderItemCode = saleOrderItemCode;
    }

    public List<ItemDetail> getItemDetails() {
        return itemDetails;
    }

    public void setItemDetails(List<ItemDetail> itemDetails) {
        this.itemDetails = itemDetails;
    }
}
