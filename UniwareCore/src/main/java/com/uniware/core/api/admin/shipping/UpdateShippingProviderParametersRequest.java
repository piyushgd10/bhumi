/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 18-Aug-2012
 *  @author vibhu
 */
package com.uniware.core.api.admin.shipping;

import com.unifier.core.api.base.ServiceRequest;

import java.util.ArrayList;
import java.util.List;

/**
 * @author vibhu
 */
public class UpdateShippingProviderParametersRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long                         serialVersionUID = 2991501145402572863L;
    private List<ShippingProviderParameterRequestDTO> parameters       = new ArrayList<UpdateShippingProviderParametersRequest.ShippingProviderParameterRequestDTO>();

    /**
     * @return the parameters
     */
    public List<ShippingProviderParameterRequestDTO> getParameters() {
        return parameters;
    }

    /**
     * @param parameters the parameters to set
     */
    public void setParameters(List<ShippingProviderParameterRequestDTO> parameters) {
        this.parameters = parameters;
    }

    public static class ShippingProviderParameterRequestDTO {
        private Integer id;
        private String  value;

        /**
         * @return the id
         */
        public Integer getId() {
            return id;
        }

        /**
         * @param id the id to set
         */
        public void setId(Integer id) {
            this.id = id;
        }

        /**
         * @return the value
         */
        public String getValue() {
            return value;
        }

        /**
         * @param value the value to set
         */
        public void setValue(String value) {
            this.value = value;
        }
    }
}
