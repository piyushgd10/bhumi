/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 03-Dec-2013
 *  @author akshay
 */
package com.uniware.core.api.inflow;

import java.util.ArrayList;
import java.util.List;

import com.unifier.core.api.base.ServiceResponse;

public class GetInflowReceiptsResponse extends ServiceResponse{

    /**
     * 
     */
    private static final long serialVersionUID = 16710331148552995L;
    private List<String>      inflowReceiptCodes = new ArrayList<String>();

    public List<String> getInflowReceiptCodes() {
        return inflowReceiptCodes;
    }

    public void setInflowReceiptCodes(List<String> inflowReceiptCodes) {
        this.inflowReceiptCodes = inflowReceiptCodes;
    }

}
