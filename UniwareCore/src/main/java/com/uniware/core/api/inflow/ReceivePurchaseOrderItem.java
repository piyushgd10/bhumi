/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 1, 2012
 *  @author singla
 */
package com.uniware.core.api.inflow;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * @author singla
 */
public class ReceivePurchaseOrderItem {

    @NotNull
    private Integer purchaseOrderItemId;

    @NotNull
    @Min(value = 1)
    private Integer quantity;

    /**
     * @return the purchaseOrderItemId
     */
    public Integer getPurchaseOrderItemId() {
        return purchaseOrderItemId;
    }

    /**
     * @param purchaseOrderItemId the purchaseOrderItemId to set
     */
    public void setPurchaseOrderItemId(Integer purchaseOrderItemId) {
        this.purchaseOrderItemId = purchaseOrderItemId;
    }

    /**
     * @return the quantity
     */
    public Integer getQuantity() {
        return quantity;
    }

    /**
     * @param quantity the quantity to set
     */
    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }
}
