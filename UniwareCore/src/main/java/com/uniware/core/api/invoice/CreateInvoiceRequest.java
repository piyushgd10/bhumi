/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jan 20, 2012
 *  @author singla
 */
package com.uniware.core.api.invoice;

import com.unifier.core.api.base.ServiceRequest;
import java.util.HashMap;
import java.util.Map;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.NotBlank;

/**
 * @author singla
 */
public class CreateInvoiceRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long       serialVersionUID      = 9134119935682779672L;

    @NotBlank
    private String                  shippingPackageCode;

    private boolean                 commitBlockedInventory;

    @NotNull
    private Integer                 userId;

    private Map<String, WsTaxInformation.ProductTax> channelProductIdToTax = new HashMap<>();

    private boolean                 skipDetailing         = false;

    public String getShippingPackageCode() {
        return shippingPackageCode;
    }

    public void setShippingPackageCode(String shippingPackageCode) {
        this.shippingPackageCode = shippingPackageCode;
    }

    public boolean isCommitBlockedInventory() {
        return commitBlockedInventory;
    }

    public void setCommitBlockedInventory(boolean commitBlockedInventory) {
        this.commitBlockedInventory = commitBlockedInventory;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Map<String, WsTaxInformation.ProductTax> getChannelProductIdToTax() {
        return channelProductIdToTax;
    }

    public void setChannelProductIdToTax(Map<String, WsTaxInformation.ProductTax> channelProductIdToTax)
    {
        this.channelProductIdToTax = channelProductIdToTax;
    }

    public boolean isSkipDetailing() {
        return skipDetailing;
    }

    public void setSkipDetailing(boolean skipDetailing) {
        this.skipDetailing = skipDetailing;
    }
}
