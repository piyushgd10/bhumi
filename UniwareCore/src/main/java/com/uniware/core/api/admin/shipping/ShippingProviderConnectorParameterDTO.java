/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 11-Feb-2015
 *  @author parijat
 */
package com.uniware.core.api.admin.shipping;

import com.unifier.core.utils.StringUtils;
import com.uniware.core.entity.ShippingProviderConnectorParameter;
import com.uniware.core.entity.ShippingSourceConnectorParameter;

public class ShippingProviderConnectorParameterDTO {

    private String name;
    private String displayName;
    private String value;
    private String type;
    private int    priority;

    public ShippingProviderConnectorParameterDTO() {

    }

    public ShippingProviderConnectorParameterDTO(ShippingProviderConnectorParameter spcp, ShippingSourceConnectorParameter sscp) {
        this.name = sscp.getName();
        this.displayName = sscp.getDisplayName();
        this.type = sscp.getType().name();
        this.priority = sscp.getPriority();
        // TODO : remove null check
        if (spcp != null) {
            this.value = spcp.getValue();
        } else {
            this.value = StringUtils.EMPTY_STRING;
        }
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the displayName
     */
    public String getDisplayName() {
        return displayName;
    }

    /**
     * @param displayName the displayName to set
     */
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    /**
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the priority
     */
    public int getPriority() {
        return priority;
    }

    /**
     * @param priority the priority to set
     */
    public void setPriority(int priority) {
        this.priority = priority;
    }

}
