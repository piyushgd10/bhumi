package com.uniware.core.api.customizations;

import java.util.List;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;
import com.unifier.core.api.customfields.WsCustomFieldValue;

public class MarkItemDamagedOutboundQCRequest extends ServiceRequest {

    /**
     *
     */
    private static final long        serialVersionUID = -2356656502985154054L;

    @NotBlank
    private String                   saleOrderCode;

    @NotBlank
    private String                   shippingPackageCode;

    @NotBlank
    private String                   itemCode;
    
    @Length(max = 255)
    private String                   rejectionReason;

    private List<WsCustomFieldValue> shippingPackageCustomFieldValues;

    private List<WsCustomFieldValue> saleOrderCustomFields;

    boolean fixedPacket = false;

    public String getSaleOrderCode() {
        return saleOrderCode;
    }

    public void setSaleOrderCode(String saleOrderCode) {
        this.saleOrderCode = saleOrderCode;
    }

    public String getShippingPackageCode() {
        return shippingPackageCode;
    }

    public void setShippingPackageCode(String shippingPackageCode) {
        this.shippingPackageCode = shippingPackageCode;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getRejectionReason() {
        return rejectionReason;
    }

    public void setRejectionReason(String rejectionReason) {
        this.rejectionReason = rejectionReason;
    }

    public List<WsCustomFieldValue> getSaleOrderCustomFields() {
        return saleOrderCustomFields;
    }

    public void setSaleOrderCustomFields(List<WsCustomFieldValue> saleOrderCustomFields) {
        this.saleOrderCustomFields = saleOrderCustomFields;
    }

    public List<WsCustomFieldValue> getShippingPackageCustomFieldValues() {
        return shippingPackageCustomFieldValues;
    }

    public void setShippingPackageCustomFieldValues(List<WsCustomFieldValue> shippingPackageCustomFieldValues) {
        this.shippingPackageCustomFieldValues = shippingPackageCustomFieldValues;
    }

    public boolean isFixedPacket() {
        return fixedPacket;
    }

    public void setFixedPacket(boolean fixedPacket) {
        this.fixedPacket = fixedPacket;
    }
}