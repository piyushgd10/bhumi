package com.uniware.core.api.shipping;

import com.unifier.core.api.base.ServiceRequest;

/**
 * Created by akshayag on 9/7/15.
 */
public class GetShippingManifestLabelRequest extends ServiceRequest {

    private String shippingManifestCode;

    public String getShippingManifestCode() {
        return shippingManifestCode;
    }

    public void setShippingManifestCode(String shippingManifestCode) {
        this.shippingManifestCode = shippingManifestCode;
    }
}
