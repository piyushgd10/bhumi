/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 27-Mar-2014
 *  @author parijat
 */
package com.uniware.core.api.reconciliation;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author parijat
 *
 */
public class GetOrderReconciliationDetailRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 5917137156374780194L;

    @NotBlank
    private String            orderCode;

    public String getOrderCode() {
        return orderCode;
    }

    public void setOrderCode(String orderCode) {
        this.orderCode = orderCode;
    }

}
