/*
 * Copyright 2017 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 16/08/17
 * @author piyush
 */
package com.uniware.core.api.picker;

import com.unifier.core.api.base.ServiceRequest;
import com.uniware.core.entity.PicklistItem;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

public class CompletePickingRequest extends ServiceRequest {

    @NotBlank
    private String                        picklistCode;

    @NotNull
    private Integer                       userId;

    private List<PicklistItem.StatusCode> statusCodes = new ArrayList<>();

    public String getPicklistCode() {
        return picklistCode;
    }

    public void setPicklistCode(String picklistCode) {
        this.picklistCode = picklistCode;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public List<PicklistItem.StatusCode> getStatusCodes() {
        return statusCodes;
    }

    public void setStatusCodes(List<PicklistItem.StatusCode> statusCodes) {
        this.statusCodes = statusCodes;
    }
}
