/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 10-Jun-2014
 *  @author akshay
 */
package com.uniware.core.api.inventory;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

public class ReassessInventoryForItemTypeRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @NotBlank
    private String            itemSku;

    public String getItemSku() {
        return itemSku;
    }

    public void setItemSku(String itemSku) {
        this.itemSku = itemSku;
    }

}
