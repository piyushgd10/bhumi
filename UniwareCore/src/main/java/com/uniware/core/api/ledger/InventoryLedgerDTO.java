/**
 * Copyright 2017 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version 1.0, 19/09/17
 * @author aditya
 */
package com.uniware.core.api.ledger;

import com.uniware.core.entity.InventoryLedger;
import java.util.Date;

public class InventoryLedgerDTO {
    private InventoryLedger.TransactionType transactionType;

    private String                          transactionIdentifier;

    private int                             oldBalance;

    private int                             newBalance;

    private int                             changedQuantity;

    private String                          facility;

    private Date                            transactionTimestamp;

    private String                          additionalInfo;

    public InventoryLedgerDTO(InventoryLedger ledger) {
        setNewBalance(ledger.getNewBalance());
        setOldBalance(ledger.getOldBalance());
        setTransactionIdentifier(ledger.getTransactionIdentifier());
        setTransactionType(ledger.getTransactionType());
        setChangedQuantity(ledger.getNewBalance() - ledger.getOldBalance());
        setFacility(ledger.getFacility().getCode());
        setTransactionTimestamp(ledger.getCreated());
        setAdditionalInfo(ledger.getAdditionalInfo());
    }

    public InventoryLedger.TransactionType getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(InventoryLedger.TransactionType transactionType) {
        this.transactionType = transactionType;
    }

    public String getTransactionIdentifier() {
        return transactionIdentifier;
    }

    public void setTransactionIdentifier(String transactionIdentifier) {
        this.transactionIdentifier = transactionIdentifier;
    }

    public int getOldBalance() {
        return oldBalance;
    }

    public void setOldBalance(int oldBalance) {
        this.oldBalance = oldBalance;
    }

    public int getNewBalance() {
        return newBalance;
    }

    public void setNewBalance(int newBalance) {
        this.newBalance = newBalance;
    }

    public int getChangedQuantity() {
        return changedQuantity;
    }

    public void setChangedQuantity(int changedQuantity) {
        this.changedQuantity = changedQuantity;
    }

    public String getFacility() {
        return facility;
    }

    public void setFacility(String facility) {
        this.facility = facility;
    }

    public Date getTransactionTimestamp() {
        return transactionTimestamp;
    }

    public void setTransactionTimestamp(Date transactionTimestamp) {
        this.transactionTimestamp = transactionTimestamp;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }
}
