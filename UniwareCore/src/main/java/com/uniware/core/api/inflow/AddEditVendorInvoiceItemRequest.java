/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 25-Mar-2014
 *  @author karunsingla
 */
package com.uniware.core.api.inflow;

import java.util.List;

import javax.validation.Valid;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import com.unifier.core.api.base.ServiceRequest;

public class AddEditVendorInvoiceItemRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long         serialVersionUID = -7727256628923746700L;

    @NotBlank
    private String                    vendorInvoiceCode;

    @Valid
    @NotEmpty
    private List<WsVendorInvoiceItem> vendorInvoiceItems;

    public List<WsVendorInvoiceItem> getVendorInvoiceItems() {
        return vendorInvoiceItems;
    }

    public void setVendorInvoiceItems(List<WsVendorInvoiceItem> vendorInvoiceItems) {
        this.vendorInvoiceItems = vendorInvoiceItems;
    }

    public String getVendorInvoiceCode() {
        return vendorInvoiceCode;
    }

    public void setVendorInvoiceCode(String vendorInvoiceCode) {
        this.vendorInvoiceCode = vendorInvoiceCode;
    }

}
