/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 06-Mar-2014
 *  @author karunsingla
 */
package com.uniware.core.api.channel;

import com.unifier.core.api.base.ServiceResponse;

public class ReenableInventorySyncResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 4972314962356210398L;

}
