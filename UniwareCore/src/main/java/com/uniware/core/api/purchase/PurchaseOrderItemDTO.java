/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 29, 2012
 *  @author singla
 */
package com.uniware.core.api.purchase;

import java.math.BigDecimal;
import java.util.Map;

/**
 * @author singla
 */
public class PurchaseOrderItemDTO {

    private int                 id;
    private String              itemSKU;
    private Map<String, Object> itemTypeCustomFields;
    private Integer             itemTypeId;
    private String              itemTypeImageUrl;
    private String              itemTypePageUrl;
    private String              itemTypeName;
    private String              vendorSkuCode;
    private String              color;
    private String              brand;
    private String              size;
    private int                 quantity;
    private int                 rejectedQuantity;
    private int                 pendingQuantity;
    private BigDecimal          unitPrice;
    private BigDecimal          maxRetailPrice;
    private BigDecimal          discount;
    private BigDecimal          discountPercentage;
    private BigDecimal          subtotal;
    private BigDecimal          total;
    private BigDecimal          taxPercentage;
    private BigDecimal          tax;
    private String              taxType;
    private BigDecimal          logisticCharges;
    private boolean             expirable;
    private int                 shelfLife;
    private int                 grnExpiryTolerance;

    public Integer getItemTypeId() {
        return itemTypeId;
    }

    public void setItemTypeId(Integer itemTypeId) {
        this.itemTypeId = itemTypeId;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the itemSKU
     */
    public String getItemSKU() {
        return itemSKU;
    }

    /**
     * @param itemSKU the itemSKU to set
     */
    public void setItemSKU(String itemSKU) {
        this.itemSKU = itemSKU;
    }

    /**
     * @return the itemTypeName
     */
    public String getItemTypeName() {
        return itemTypeName;
    }

    /**
     * @param itemTypeName the itemTypeName to set
     */
    public void setItemTypeName(String itemTypeName) {
        this.itemTypeName = itemTypeName;
    }

    /**
     * @return the vendorSkuCode
     */
    public String getVendorSkuCode() {
        return vendorSkuCode;
    }

    /**
     * @param vendorSkuCode the vendorSkuCode to set
     */
    public void setVendorSkuCode(String vendorSkuCode) {
        this.vendorSkuCode = vendorSkuCode;
    }

    /**
     * @return the quantity
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     * @param quantity the quantity to set
     */
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    /**
     * @return the unitPrice
     */
    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    /**
     * @param unitPrice the unitPrice to set
     */
    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }

    /**
     * @return the itemTypePageUrl
     */
    public String getItemTypePageUrl() {
        return itemTypePageUrl;
    }

    /**
     * @param itemTypePageUrl the itemTypePageUrl to set
     */
    public void setItemTypePageUrl(String itemTypePageUrl) {
        this.itemTypePageUrl = itemTypePageUrl;
    }

    /**
     * @return the itemTypeImageUrl
     */
    public String getItemTypeImageUrl() {
        return itemTypeImageUrl;
    }

    /**
     * @param itemTypeImageUrl the itemTypeImageUrl to set
     */
    public void setItemTypeImageUrl(String itemTypeImageUrl) {
        this.itemTypeImageUrl = itemTypeImageUrl;
    }

    /**
     * @return the pendingQuantity
     */
    public int getPendingQuantity() {
        return pendingQuantity;
    }

    /**
     * @param pendingQuantity the pendingQuantity to set
     */
    public void setPendingQuantity(int pendingQuantity) {
        this.pendingQuantity = pendingQuantity;
    }

    /**
     * @return the rejectedQuantity
     */
    public int getRejectedQuantity() {
        return rejectedQuantity;
    }

    /**
     * @param rejectedQuantity the rejectedQuantity to set
     */
    public void setRejectedQuantity(int rejectedQuantity) {
        this.rejectedQuantity = rejectedQuantity;
    }

    /**
     * @return the subtotal
     */
    public BigDecimal getSubtotal() {
        return subtotal;
    }

    /**
     * @param subtotal the subtotal to set
     */
    public void setSubtotal(BigDecimal subtotal) {
        this.subtotal = subtotal;
    }

    /**
     * @return the total
     */
    public BigDecimal getTotal() {
        return total;
    }

    /**
     * @param total the total to set
     */
    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    /**
     * @return the itemTypeCustomFields
     */
    public Map<String, Object> getItemTypeCustomFields() {
        return itemTypeCustomFields;
    }

    /**
     * @param itemTypeCustomFields the itemTypeCustomFields to set
     */
    public void setItemTypeCustomFields(Map<String, Object> itemTypeCustomFields) {
        this.itemTypeCustomFields = itemTypeCustomFields;
    }

    /**
     * @return the maxRetailPrice
     */
    public BigDecimal getMaxRetailPrice() {
        return maxRetailPrice;
    }

    /**
     * @param maxRetailPrice the maxRetailPrice to set
     */
    public void setMaxRetailPrice(BigDecimal maxRetailPrice) {
        this.maxRetailPrice = maxRetailPrice;
    }

    /**
     * @return the discount
     */
    public BigDecimal getDiscount() {
        return discount;
    }

    /**
     * @param discount the discount to set
     */
    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    /**
     * @return the discountPercentage
     */
    public BigDecimal getDiscountPercentage() {
        return discountPercentage;
    }

    /**
     * @param discountPercentage the discountPercentage to set
     */
    public void setDiscountPercentage(BigDecimal discountPercentage) {
        this.discountPercentage = discountPercentage;
    }

    /**
     * @return the color
     */
    public String getColor() {
        return color;
    }

    /**
     * @param color the color to set
     */
    public void setColor(String color) {
        this.color = color;
    }

    /**
     * @return the brand
     */
    public String getBrand() {
        return brand;
    }

    /**
     * @param brand the brand to set
     */
    public void setBrand(String brand) {
        this.brand = brand;
    }

    /**
     * @return the size
     */
    public String getSize() {
        return size;
    }

    /**
     * @param size the size to set
     */
    public void setSize(String size) {
        this.size = size;
    }

    /**
     * @return the taxPercentage
     */
    public BigDecimal getTaxPercentage() {
        return taxPercentage;
    }

    /**
     * @param taxPercentage the taxPercentage to set
     */
    public void setTaxPercentage(BigDecimal taxPercentage) {
        this.taxPercentage = taxPercentage;
    }

    /**
     * @return the tax
     */
    public BigDecimal getTax() {
        return tax;
    }

    /**
     * @param tax the tax to set
     */
    public void setTax(BigDecimal tax) {
        this.tax = tax;
    }

    /**
     * @return the logisticCharges
     */
    public BigDecimal getLogisticCharges() {
        return logisticCharges;
    }

    /**
     * @param logisticCharges the logisticCharges to set
     */
    public void setLogisticCharges(BigDecimal logisticCharges) {
        this.logisticCharges = logisticCharges;
    }

    /**
     * @return the taxType
     */
    public String getTaxType() {
        return taxType;
    }

    /**
     * @param taxType the taxType to set
     */
    public void setTaxType(String taxType) {
        this.taxType = taxType;
    }

    public boolean isExpirable() {
        return expirable;
    }

    public void setExpirable(boolean expirable) {
        this.expirable = expirable;
    }

    public int getShelfLife() {
        return shelfLife;
    }

    public void setShelfLife(int shelfLife) {
        this.shelfLife = shelfLife;
    }

    public int getGrnExpiryTolerance() {
        return grnExpiryTolerance;
    }

    public void setGrnExpiryTolerance(int grnExpiryTolerance) {
        this.grnExpiryTolerance = grnExpiryTolerance;
    }
}