package com.uniware.core.api.picker;

import com.uniware.core.entity.Picklist;
import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

import javax.validation.constraints.NotNull;

public class GetPickSetWiseItemCountsRequest extends ServiceRequest {

    private static final long serialVersionUID = 7984201431521746161L;

    @NotNull
    private Picklist.Destination destination;

    public Picklist.Destination getDestination() {
        return destination;
    }

    public void setDestination(Picklist.Destination destination) {
        this.destination = destination;
    }
}
