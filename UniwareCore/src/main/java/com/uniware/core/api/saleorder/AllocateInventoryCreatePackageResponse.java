/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 27-Aug-2015
 *  @author parijat
 */
package com.uniware.core.api.saleorder;

import java.util.List;

import com.unifier.core.api.base.ServiceResponse;

public class AllocateInventoryCreatePackageResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private List<String>      shippingPackageCodes;

    public List<String> getShippingPackageCodes() {
        return shippingPackageCodes;
    }

    public void setShippingPackageCodes(List<String> shippingPackageCodes) {
        this.shippingPackageCodes = shippingPackageCodes;
    }
}
