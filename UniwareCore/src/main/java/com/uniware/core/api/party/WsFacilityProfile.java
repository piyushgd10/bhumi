/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 14-May-2015
 *  @author parijat
 */
package com.uniware.core.api.party;

import org.hibernate.validator.constraints.NotBlank;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

public class WsFacilityProfile {

    @NotBlank
    private String               facilityCode;

    @NotBlank
    private String               sellerName;

    @NotBlank
    private String               contactEmail;

    @NotBlank
    private String               mobile;

    private String               alternatePhone;

    @NotNull
    private List<Double>         position;
    @NotNull
    private Boolean              autoSetupServiceability;
    private Double               deliveryRadius;
    @NotNull
    private Boolean      autoSetupPickupServiceability;
    private Double       pickupRadius;
    @NotNull
    private Boolean      storePickupEnabled;
    @NotNull
    private List<String> pickupShippingMethods;
    @NotNull
    private Boolean      storeDeliveryEnabled;
    @NotNull
    private List<String> selfShippingMethods;

    private int dispatchSLA;

    private int deliverySLA;

    @Valid
    private List<WsStoreTimings> storeTimings = new ArrayList<>();

    @Valid
    private List<WsStoreHoliday> storeHolidays = new ArrayList<>();

    public String getFacilityCode() {
        return facilityCode;
    }

    public void setFacilityCode(String facilityCode) {
        this.facilityCode = facilityCode;
    }

    public String getSellerName() {
        return sellerName;
    }

    public void setSellerName(String sellerName) {
        this.sellerName = sellerName;
    }

    public String getContactEmail() {
        return contactEmail;
    }

    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getAlternatePhone() {
        return alternatePhone;
    }

    public void setAlternatePhone(String alternatePhone) {
        this.alternatePhone = alternatePhone;
    }

    public List<Double> getPosition() {
        return position;
    }

    public void setPosition(List<Double> position) {
        this.position = position;
    }

    public Boolean getAutoSetupServiceability() {
        return autoSetupServiceability;
    }

    public void setAutoSetupServiceability(Boolean autoSetupServiceability) {
        this.autoSetupServiceability = autoSetupServiceability;
    }

    public Double getDeliveryRadius() {
        return deliveryRadius;
    }

    public void setDeliveryRadius(Double deliveryRadius) {
        this.deliveryRadius = deliveryRadius;
    }

    public Boolean getStorePickupEnabled() {
        return storePickupEnabled;
    }

    public void setStorePickupEnabled(Boolean storePickupEnabled) {
        this.storePickupEnabled = storePickupEnabled;
    }

    public Boolean getStoreDeliveryEnabled() {
        return storeDeliveryEnabled;
    }

    public void setStoreDeliveryEnabled(Boolean storeDeliveryEnabled) {
        this.storeDeliveryEnabled = storeDeliveryEnabled;
    }

    public List<WsStoreTimings> getStoreTimings() {
        return storeTimings;
    }

    public void setStoreTimings(List<WsStoreTimings> storeTimings) {
        this.storeTimings = storeTimings;
    }

    public List<WsStoreHoliday> getStoreHolidays() {
        return storeHolidays;
    }

    public void setStoreHolidays(List<WsStoreHoliday> storeHolidays) {
        this.storeHolidays = storeHolidays;
    }

    public List<String> getPickupShippingMethods() {
        return pickupShippingMethods;
    }

    public void setPickupShippingMethods(List<String> pickupShippingMethods) {
        this.pickupShippingMethods = pickupShippingMethods;
    }

    public List<String> getSelfShippingMethods() {
        return selfShippingMethods;
    }

    public void setSelfShippingMethods(List<String> selfShippingMethods) {
        this.selfShippingMethods = selfShippingMethods;
    }

    public int getDispatchSLA() {
        return dispatchSLA;
    }

    public void setDispatchSLA(int dispatchSLA) {
        this.dispatchSLA = dispatchSLA;
    }

    public int getDeliverySLA() {
        return deliverySLA;
    }

    public void setDeliverySLA(int deliverySLA) {
        this.deliverySLA = deliverySLA;
    }

    public Boolean getAutoSetupPickupServiceability() {
        return autoSetupPickupServiceability;
    }

    public void setAutoSetupPickupServiceability(Boolean autoSetupPickupServiceability) {
        this.autoSetupPickupServiceability = autoSetupPickupServiceability;
    }

    public Double getPickupRadius() {
        return pickupRadius;
    }

    public void setPickupRadius(Double pickupRadius) {
        this.pickupRadius = pickupRadius;
    }
}
