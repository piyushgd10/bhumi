/*
 * Copyright 2017 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 09/06/17
 * @author piyush
 */
package com.uniware.core.api.warehouse;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;
import com.uniware.core.api.sequence.WsSequence;

public class EditPartySequenceRequest extends ServiceRequest {

    @NotBlank
    private String     code;

    private boolean    billingParty = false;

    @Valid
    @NotNull
    private WsSequence sequence;

    public String getCode() {
        return code;
    }

    public void setCode(String facilityCode) {
        this.code = facilityCode;
    }

    public boolean isBillingParty() {
        return billingParty;
    }

    public void setBillingParty(boolean billingParty) {
        this.billingParty = billingParty;
    }

    public WsSequence getSequence() {
        return sequence;
    }

    public void setSequence(WsSequence sequence) {
        this.sequence = sequence;
    }
}
