/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 24-May-2012
 *  @author praveeng
 */
package com.uniware.core.api.print;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author praveeng
 */
public class PrintPreviewRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 8773434484937041006L;
    private String            objectCode;
    private String            name;
    private String            template;
    private int               numberOfCopies;
    private String            printDialog;
    private String            pageSize;
    private String            pageMargins;
    private boolean           landscape;

    /**
     * @return the objectCode
     */
    public String getObjectCode() {
        return objectCode;
    }

    /**
     * @param objectCode the objectCode to set
     */
    public void setObjectCode(String objectCode) {
        this.objectCode = objectCode;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the template
     */
    public String getTemplate() {
        return template;
    }

    /**
     * @param template the template to set
     */
    public void setTemplate(String template) {
        this.template = template;
    }

    /**
     * @return the numberOfCopies
     */
    public int getNumberOfCopies() {
        return numberOfCopies;
    }

    /**
     * @param numberOfCopies the numberOfCopies to set
     */
    public void setNumberOfCopies(int numberOfCopies) {
        this.numberOfCopies = numberOfCopies;
    }

    /**
     * @return the printDialog
     */
    public String getPrintDialog() {
        return printDialog;
    }

    /**
     * @param printDialog the printDialog to set
     */
    public void setPrintDialog(String printDialog) {
        this.printDialog = printDialog;
    }

    /**
     * @return the pageSize
     */
    public String getPageSize() {
        return pageSize;
    }

    /**
     * @param pageSize the pageSize to set
     */
    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }

    /**
     * @return the pageMargins
     */
    public String getPageMargins() {
        return pageMargins;
    }

    /**
     * @param pageMargins the pageMargins to set
     */
    public void setPageMargins(String pageMargins) {
        this.pageMargins = pageMargins;
    }

    /**
     * @return the landscape
     */
    public boolean isLandscape() {
        return landscape;
    }

    /**
     * @param landscape the landscape to set
     */
    public void setLandscape(boolean landscape) {
        this.landscape = landscape;
    }

}
