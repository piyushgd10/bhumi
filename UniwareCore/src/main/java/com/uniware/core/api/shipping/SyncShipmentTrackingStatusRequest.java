/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 13-Feb-2015
 *  @author parijat
 */
package com.uniware.core.api.shipping;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author parijat
 *
 */
public class SyncShipmentTrackingStatusRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 856356402757648191L;

    @NotBlank
    private String            shippingProviderCode;

    /**
     * @return the shippingProviderCode
     */
    public String getShippingProviderCode() {
        return shippingProviderCode;
    }

    /**
     * @param shippingProviderCode the shippingProviderCode to set
     */
    public void setShippingProviderCode(String shippingProviderCode) {
        this.shippingProviderCode = shippingProviderCode;
    }

}
