/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 28-Nov-2013
 *  @author akshay
 */
package com.uniware.core.api.purchase;

import com.unifier.core.api.base.ServiceRequest;
import com.unifier.core.utils.DateUtils.DateRange;

public class GetPurchaseOrdersRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 3257635189063326432L;
    private String            vendorName;
    private DateRange         approvedBetween;
    private DateRange         createdBetween;

    public DateRange getApprovedBetween() {
        return approvedBetween;
    }

    public void setApprovedBetween(DateRange approvedBetween) {
        this.approvedBetween = approvedBetween;
    }

    public DateRange getCreatedBetween() {
        return createdBetween;
    }

    public void setCreatedBetween(DateRange createdBetween) {
        this.createdBetween = createdBetween;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

}
