/*
 * Copyright 2017 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 03/11/17
 * @author piyush
 */
package com.uniware.core.api.picker;

import java.util.List;

import com.unifier.core.api.base.ServiceResponse;
import com.uniware.core.entity.ItemTypeInventory;

public class GetPutbackItemCountBySkuResponse extends ServiceResponse {

    private String                    skuCode;

    private List<PutbackItemCountDTO> putbackItems;

    public String getSkuCode() {
        return skuCode;
    }

    public void setSkuCode(String skuCode) {
        this.skuCode = skuCode;
    }

    public List<PutbackItemCountDTO> getPutbackItems() {
        return putbackItems;
    }

    public void setPutbackItems(List<PutbackItemCountDTO> putbackItems) {
        this.putbackItems = putbackItems;
    }

    public static class PutbackItemCountDTO {

        private ItemTypeInventory.Type type;

        private Integer                quantity;

        public PutbackItemCountDTO(ItemTypeInventory.Type type, Integer quantity) {
            this.type = type;
            this.quantity = quantity;
        }

        public ItemTypeInventory.Type getType() {
            return type;
        }

        public void setType(ItemTypeInventory.Type type) {
            this.type = type;
        }

        public Integer getQuantity() {
            return quantity;
        }

        public void setQuantity(Integer quantity) {
            this.quantity = quantity;
        }
    }
}
