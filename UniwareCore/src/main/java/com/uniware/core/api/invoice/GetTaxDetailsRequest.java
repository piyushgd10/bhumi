/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jan 20, 2012
 *  @author sunny
 */
package com.uniware.core.api.invoice;

import java.math.BigDecimal;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;
import com.uniware.core.api.model.WsInvoice;

/**
 * @author sunny
 */
public class GetTaxDetailsRequest extends ServiceRequest {

    /**
     *
     */
    private static final long       serialVersionUID = 9134119935682779672L;

    @Valid
    private List<WsItemType>        itemTypes;

    @Valid
    private List<WsChannelItemType> channelItemTypes;

    @NotBlank
    private String                  channelCode;

    private String                  customerCode;

    private String                  shippingState;

    private String                  shippingCountry;

    private WsInvoice.Source        source           = WsInvoice.Source.SHIPPING_PACKAGE;

    public List<WsItemType> getItemTypes() {
        return itemTypes;
    }

    public void setItemTypes(List<WsItemType> itemTypes) {
        this.itemTypes = itemTypes;
    }

    public List<WsChannelItemType> getChannelItemTypes() {
        return channelItemTypes;
    }

    public void setChannelItemTypes(List<WsChannelItemType> channelItemTypes) {
        this.channelItemTypes = channelItemTypes;
    }

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    public String getShippingState() {
        return shippingState;
    }

    public void setShippingState(String shippingState) {
        this.shippingState = shippingState;
    }

    public String getShippingCountry() {
        return shippingCountry;
    }

    public void setShippingCountry(String shippingCountry) {
        this.shippingCountry = shippingCountry;
    }

    public WsInvoice.Source getSource() {
        return source;
    }

    public void setSource(WsInvoice.Source source) {
        this.source = source;
    }

    public static class WsItemType {
        @NotBlank
        private String     itemSku;

        @NotNull
        private BigDecimal sellingPrice;

        private int        quantity = 0;

        public String getItemSku() {
            return itemSku;
        }

        public void setItemSku(String itemSku) {
            this.itemSku = itemSku;
        }

        public BigDecimal getSellingPrice() {
            return sellingPrice;
        }

        public void setSellingPrice(BigDecimal sellingPrice) {
            this.sellingPrice = sellingPrice;
        }

        public int getQuantity() {
            return quantity;
        }

        public void setQuantity(int quantity) {
            this.quantity = quantity;
        }
    }

    public static class WsChannelItemType {
        @NotBlank
        private String     channelProductId;

        @NotNull
        private BigDecimal sellingPrice;

        private int        quantity = 0;

        @NotNull
        private BigDecimal taxPercentage;

        public String getChannelProductId() {
            return channelProductId;
        }

        public void setChannelProductId(String channelProductId) {
            this.channelProductId = channelProductId;
        }

        public BigDecimal getSellingPrice() {
            return sellingPrice;
        }

        public void setSellingPrice(BigDecimal sellingPrice) {
            this.sellingPrice = sellingPrice;
        }

        public int getQuantity() {
            return quantity;
        }

        public void setQuantity(int quantity) {
            this.quantity = quantity;
        }

        public BigDecimal getTaxPercentage() {
            return taxPercentage;
        }

        public void setTaxPercentage(BigDecimal taxPercentage) {
            this.taxPercentage = taxPercentage;
        }

    }

}
