/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, May 8, 2012
 *  @author singla
 */
package com.uniware.core.api.putaway;

import com.unifier.core.api.base.ServiceRequest;

import java.util.List;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * <ul>
 * Parameters:
 * <li>{@code inflowReceiptCode}: code of Inflow Receipt of which inflow receipt items are to be added in putaway.</li>
 * <li>{@code putawayCode}: code of putaway in which gatepass items are to be added.</li>
 * <li>{@code userId}: id of user, set by context</li>
 * <li>{@code inflowReceiptItemIds}: List of ids of inflow receipt items in inflow receipt of which putaway is to be done.</li>
 * </ul>
 */
public class AddInflowReceiptItemsToPutawayRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 8120252039097314738L;

    @NotNull
    private Integer           userId;

    @NotBlank
    private String            putawayCode;

    @NotNull
    private String            inflowReceiptCode;

    @NotEmpty
    private List<Integer>     inflowReceiptItemIds;

    /**
     * @return the inflowReceiptItemIds
     */
    public List<Integer> getInflowReceiptItemIds() {
        return inflowReceiptItemIds;
    }

    /**
     * @param inflowReceiptItemIds the inflowReceiptItemIds to set
     */
    public void setInflowReceiptItemIds(List<Integer> inflowReceiptItemIds) {
        this.inflowReceiptItemIds = inflowReceiptItemIds;
    }

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return the inflowReceiptCode
     */
    public String getInflowReceiptCode() {
        return inflowReceiptCode;
    }

    /**
     * @param inflowReceiptCode the inflowReceiptCode to set
     */
    public void setInflowReceiptCode(String inflowReceiptCode) {
        this.inflowReceiptCode = inflowReceiptCode;
    }

    /**
     * @return the putawayCode
     */
    public String getPutawayCode() {
        return putawayCode;
    }

    /**
     * @param putawayCode the putawayCode to set
     */
    public void setPutawayCode(String putawayCode) {
        this.putawayCode = putawayCode;
    }
}
