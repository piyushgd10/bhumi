/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 26, 2015
 *  @author harshpal
 */
package com.uniware.core.api.channel;

import com.unifier.core.api.base.ServiceResponse;

public class GetChannelOrderSyncStatusResponse extends ServiceResponse {

    private static final long         serialVersionUID = -4093588016148389855L;

    private ChannelOrderSyncStatusDTO orderSyncStatus;

    public ChannelOrderSyncStatusDTO getOrderSyncStatus() {
        return orderSyncStatus;
    }

    public void setOrderSyncStatus(ChannelOrderSyncStatusDTO orderSyncStatus) {
        this.orderSyncStatus = orderSyncStatus;
    }

}
