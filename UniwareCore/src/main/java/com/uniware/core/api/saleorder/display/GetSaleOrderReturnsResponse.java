/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jan 10, 2012
 *  @author singla
 */
package com.uniware.core.api.saleorder.display;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.unifier.core.api.base.ServiceResponse;
import com.unifier.core.api.customfields.CustomFieldMetadataDTO;

public class GetSaleOrderReturnsResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 6284414547113409023L;

    public GetSaleOrderReturnsResponse() {
    }

    private List<SaleOrderReturnDTO> saleOrderReturns;

    public List<SaleOrderReturnDTO> getSaleOrderReturns() {
        return saleOrderReturns;
    }

    public void setSaleOrderReturns(List<SaleOrderReturnDTO> saleOrderReturns) {
        this.saleOrderReturns = saleOrderReturns;
    }

    public static class SaleOrderReturnDTO {

        public enum Type {
            CUSTOMER_RETURNED("Customer Returned"),
            COURIER_RETURNED("Courier Returned");

            private String name;

            Type(String name) {
                this.name = name;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }
        }

        public static class SaleOrderItemDTO {
            private String code;
            private String productName;
            private String itemSku;
            private String shelfCode;
            private String statusCode;

            public String getCode() {
                return code;
            }

            public void setCode(String code) {
                this.code = code;
            }

            public String getProductName() {
                return productName;
            }

            public void setProductName(String productName) {
                this.productName = productName;
            }

            public String getItemSku() {
                return itemSku;
            }

            public void setItemSku(String itemSku) {
                this.itemSku = itemSku;
            }

            public String getShelfCode() {
                return shelfCode;
            }

            public void setShelfCode(String shelfCode) {
                this.shelfCode = shelfCode;
            }

            public String getStatusCode() {
                return statusCode;
            }

            public void setStatusCode(String statusCode) {
                this.statusCode = statusCode;
            }

        }
        
        public static class ReplacedSaleOrderItemDTO {
            private int    saleOrderItemId;
            private String itemSku;
            private String itemName;
            private String saleOrderItemCode;
            
            public int getSaleOrderItemId() {
                return saleOrderItemId;
            }

            public void setSaleOrderItemId(int saleOrderItemId) {
                this.saleOrderItemId = saleOrderItemId;
            }

            public String getItemSku() {
                return itemSku;
            }

            public void setItemSku(String itemSku) {
                this.itemSku = itemSku;
            }

            public String getItemName() {
                return itemName;
            }

            public void setItemName(String itemName) {
                this.itemName = itemName;
            }

            public String getSaleOrderItemCode() {
                return saleOrderItemCode;
            }

            public void setSaleOrderItemCode(String saleOrderItemCode) {
                this.saleOrderItemCode = saleOrderItemCode;
            }
        }


        private String                       code;
        private String                       type;
        private Date                         created;
        private String                       statusCode;
        private String                       actionCode;
        private String                       returnReason;
        private String                       replacementSaleOrderCode;
        private String                       reshipmentShippingPackageCode;
        private String                       reshipmentSaleOrderCode;
        private String                       trackingNumber;
        private String                       shippingProvider;
        private String                       redispatchShippingProvider;
        private String                       redispatchTrackingNumber;
        private String                       redispatchReason;
        private String                       redispatchManifestCode;
        private List<CustomFieldMetadataDTO> reversePickupCustomFieldValues = new ArrayList<CustomFieldMetadataDTO>();;
        private List<SaleOrderItemDTO>       saleOrderItems = new ArrayList<SaleOrderItemDTO>();
        private List<ReplacedSaleOrderItemDTO> replacedSaleOrderItems = new ArrayList<ReplacedSaleOrderItemDTO>();

        public SaleOrderReturnDTO() {
            super();
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public Date getCreated() {
            return created;
        }

        public void setCreated(Date created) {
            this.created = created;
        }

        public String getStatusCode() {
            return statusCode;
        }

        public void setStatusCode(String statusCode) {
            this.statusCode = statusCode;
        }

        public String getActionCode() {
            return actionCode;
        }

        public void setActionCode(String actionCode) {
            this.actionCode = actionCode;
        }

        public String getReturnReason() {
            return returnReason;
        }

        public void setReturnReason(String returnReason) {
            this.returnReason = returnReason;
        }

        public String getReplacementSaleOrderCode() {
            return replacementSaleOrderCode;
        }

        public void setReplacementSaleOrderCode(String replacementSaleOrderCode) {
            this.replacementSaleOrderCode = replacementSaleOrderCode;
        }

        public String getReshipmentSaleOrderCode() {
            return reshipmentSaleOrderCode;
        }

        public void setReshipmentSaleOrderCode(String reshipmentSaleOrderCode) {
            this.reshipmentSaleOrderCode = reshipmentSaleOrderCode;
        }

        public String getReshipmentShippingPackageCode() {
            return reshipmentShippingPackageCode;
        }

        public void setReshipmentShippingPackageCode(String reshipmentShippingPackageCode) {
            this.reshipmentShippingPackageCode = reshipmentShippingPackageCode;
        }

        public List<SaleOrderItemDTO> getSaleOrderItems() {
            return saleOrderItems;
        }

        public void setSaleOrderItems(List<SaleOrderItemDTO> saleOrderItems) {
            this.saleOrderItems = saleOrderItems;
        }

        public String getTrackingNumber() {
            return trackingNumber;
        }

        public void setTrackingNumber(String trackingNumber) {
            this.trackingNumber = trackingNumber;
        }

        public String getShippingProvider() {
            return shippingProvider;
        }

        public void setShippingProvider(String shippingProvider) {
            this.shippingProvider = shippingProvider;
        }

        public String getRedispatchShippingProvider() {
            return redispatchShippingProvider;
        }

        public void setRedispatchShippingProvider(String redispatchShippingProvider) {
            this.redispatchShippingProvider = redispatchShippingProvider;
        }

        public String getRedispatchTrackingNumber() {
            return redispatchTrackingNumber;
        }

        public void setRedispatchTrackingNumber(String redispatchTrackingNumber) {
            this.redispatchTrackingNumber = redispatchTrackingNumber;
        }

        public String getRedispatchReason() {
            return redispatchReason;
        }

        public void setRedispatchReason(String redispatchReason) {
            this.redispatchReason = redispatchReason;
        }

        public String getRedispatchManifestCode() {
            return redispatchManifestCode;
        }

        public void setRedispatchManifestCode(String redispatchManifestCode) {
            this.redispatchManifestCode = redispatchManifestCode;
        }

        public List<CustomFieldMetadataDTO> getReversePickupCustomFieldValues() {
            return reversePickupCustomFieldValues;
        }

        public void setReversePickupCustomFieldValues(List<CustomFieldMetadataDTO> reversePickupCustomFieldValues) {
            this.reversePickupCustomFieldValues = reversePickupCustomFieldValues;
        }

        public List<ReplacedSaleOrderItemDTO> getReplacedSaleOrderItems() {
            return replacedSaleOrderItems;
        }

        public void setReplacedSaleOrderItems(List<ReplacedSaleOrderItemDTO> replacedSaleOrderItems) {
            this.replacedSaleOrderItems = replacedSaleOrderItems;
        }
        
    }

}
