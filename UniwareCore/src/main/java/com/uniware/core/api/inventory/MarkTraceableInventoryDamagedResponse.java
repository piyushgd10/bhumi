/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Oct 16, 2012
 *  @author singla
 */
package com.uniware.core.api.inventory;

import com.unifier.core.api.base.ServiceResponse;

public class MarkTraceableInventoryDamagedResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -2067205746649864798L;

}
