/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Nov 25, 2015
 *  @author akshay
 */
package com.uniware.core.api.recommendation;

import java.util.ArrayList;
import java.util.List;

import com.unifier.core.api.base.ServiceResponse;

public class GetRecommendationTypesResponse extends ServiceResponse {

    private static final long serialVersionUID = -4562926091415252839L;
    
    private List<String> recommendationTypes = new ArrayList<>();

    public List<String> getRecommendationTypes() {
        return recommendationTypes;
    }

    public void setRecommendationTypes(final List<String> recommendationTypes) {
        this.recommendationTypes = recommendationTypes;
    }
}
