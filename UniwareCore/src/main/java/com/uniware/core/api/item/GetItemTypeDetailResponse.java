/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 06-Jun-2012
 *  @author vibhu
 */
package com.uniware.core.api.item;

import java.util.ArrayList;
import java.util.List;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author vibhu
 */
public class GetItemTypeDetailResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -4204568277266534463L;

    private ItemTypeDTO       itemTypeDTO      = new ItemTypeDTO();

    /**
     * @return the itemTypeDTO
     */
    public ItemTypeDTO getItemTypeDTO() {
        return itemTypeDTO;
    }

    /**
     * @param itemTypeDTO the itemTypeDTO to set
     */
    public void setItemTypeDTO(ItemTypeDTO itemTypeDTO) {
        this.itemTypeDTO = itemTypeDTO;
    }

    public static class ItemTypeDTO {

        private String                  skuCode;
        private String                  name;
        private String                  categoryName;
        private String                  itemTypeImageUrl;
        private String                  itemTypePageUrl;

        private int                     openSale;
        private int                     openPurchase;
        private int                     inventory;
        private int                     putawayPending;
        private int                     inPicking;
        private int                     pendingGRN;
        private boolean                 enabled;

        private int                     quantityNotFound;
        private int                     quantityDamaged;

        private List<VendorItemTypeDTO> vendorItemTypes = new ArrayList<VendorItemTypeDTO>();

        public String getSkuCode() {
            return skuCode;
        }

        public void setSkuCode(String skuCode) {
            this.skuCode = skuCode;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getCategoryName() {
            return categoryName;
        }

        public void setCategoryName(String categoryName) {
            this.categoryName = categoryName;
        }

        public String getItemTypeImageUrl() {
            return itemTypeImageUrl;
        }

        public void setItemTypeImageUrl(String itemTypeImageUrl) {
            this.itemTypeImageUrl = itemTypeImageUrl;
        }

        public String getItemTypePageUrl() {
            return itemTypePageUrl;
        }

        public void setItemTypePageUrl(String itemTypePageUrl) {
            this.itemTypePageUrl = itemTypePageUrl;
        }

        public int getOpenSale() {
            return openSale;
        }

        public void setOpenSale(int openSale) {
            this.openSale = openSale;
        }

        public int getOpenPurchase() {
            return openPurchase;
        }

        public void setOpenPurchase(int openPurchase) {
            this.openPurchase = openPurchase;
        }

        public int getInventory() {
            return inventory;
        }

        public void setInventory(int inventory) {
            this.inventory = inventory;
        }

        public int getPutawayPending() {
            return putawayPending;
        }

        public void setPutawayPending(int putawayPending) {
            this.putawayPending = putawayPending;
        }

        public int getInPicking() {
            return inPicking;
        }

        public void setInPicking(int inPicking) {
            this.inPicking = inPicking;
        }

        public int getPendingGRN() {
            return pendingGRN;
        }

        public void setPendingGRN(int pendingGRN) {
            this.pendingGRN = pendingGRN;
        }

        public int getQuantityNotFound() {
            return quantityNotFound;
        }

        public void setQuantityNotFound(int quantityNotFound) {
            this.quantityNotFound = quantityNotFound;
        }

        public int getQuantityDamaged() {
            return quantityDamaged;
        }

        public void setQuantityDamaged(int quantityDamaged) {
            this.quantityDamaged = quantityDamaged;
        }

        public boolean isEnabled() {
            return enabled;
        }

        public void setEnabled(boolean enabled) {
            this.enabled = enabled;
        }

        public List<VendorItemTypeDTO> getVendorItemTypes() {
            return vendorItemTypes;
        }

        public void setVendorItemTypes(List<VendorItemTypeDTO> vendorItemTypes) {
            this.vendorItemTypes = vendorItemTypes;
        }

    }
}
