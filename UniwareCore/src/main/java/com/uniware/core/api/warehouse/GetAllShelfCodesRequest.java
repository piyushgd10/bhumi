/*
 * Copyright 2017 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 18/04/17
 * @author piyush
 */
package com.uniware.core.api.warehouse;

import com.unifier.core.api.base.ServiceRequest;

public class GetAllShelfCodesRequest extends ServiceRequest {
}
