package com.uniware.core.api.saleorder;

import java.util.List;

import com.unifier.core.api.base.ServiceRequest;

/**
 * Created by akshayag on 9/30/15.
 */
public class AddSaleOrderToManifestRequest extends ServiceRequest {

    private String       shippingManifestCode;
    private String       saleOrderCode;
    private List<String> saleOrderItemCodes;

    public String getShippingManifestCode() {
        return shippingManifestCode;
    }

    public void setShippingManifestCode(String shippingManifestCode) {
        this.shippingManifestCode = shippingManifestCode;
    }

    public String getSaleOrderCode() {
        return saleOrderCode;
    }

    public void setSaleOrderCode(String saleOrderCode) {
        this.saleOrderCode = saleOrderCode;
    }

    public List<String> getSaleOrderItemCodes() {
        return saleOrderItemCodes;
    }

    public void setSaleOrderItemCodes(List<String> saleOrderItemCodes) {
        this.saleOrderItemCodes = saleOrderItemCodes;
    }
}
