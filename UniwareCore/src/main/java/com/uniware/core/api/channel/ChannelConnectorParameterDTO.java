/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 18-Apr-2013
 *  @author unicom
 */
package com.uniware.core.api.channel;

import com.uniware.core.entity.ChannelConnectorParameter;
import com.uniware.core.entity.SourceConnectorParameter;

import java.io.Serializable;

/**
 * @author Sunny Agarwal
 */
public class ChannelConnectorParameterDTO implements Serializable {

    private String name;
    private String displayName;
    private String value;
    private String type;
    private int    priority;

    public ChannelConnectorParameterDTO() {
    }

    public ChannelConnectorParameterDTO(ChannelConnectorParameter ccp, SourceConnectorParameter scp) {
        name = scp.getName();
        displayName = scp.getDisplayName();
        type = scp.getType().name();
        priority = scp.getPriority();
        value = ccp.getValue();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

}
