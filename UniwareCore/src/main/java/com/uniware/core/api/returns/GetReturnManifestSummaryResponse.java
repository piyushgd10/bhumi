/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 02-Feb-2015
 *  @author akshaykochhar
 */
package com.uniware.core.api.returns;

import java.util.Date;
import java.util.List;

import com.unifier.core.api.base.ServiceResponse;
import com.unifier.core.api.customfields.CustomFieldMetadataDTO;

public class GetReturnManifestSummaryResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long        serialVersionUID = -9103893716917848791L;

    private ReturnManifestSummaryDTO summary;

    public ReturnManifestSummaryDTO getSummary() {
        return summary;
    }

    public void setSummary(ReturnManifestSummaryDTO summary) {
        this.summary = summary;
    }

    public static class ReturnManifestSummaryDTO {

        private String                       code;
        private String                       username;
        private String                       statusCode;
        private String                       shippingProvider;
        private String                       shippingProviderCode;
        private Date                         created;
        private Date                         closed;
        private List<CustomFieldMetadataDTO> customFieldValues;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getStatusCode() {
            return statusCode;
        }

        public void setStatusCode(String statusCode) {
            this.statusCode = statusCode;
        }

        public String getShippingProvider() {
            return shippingProvider;
        }

        public void setShippingProvider(String shippingProvider) {
            this.shippingProvider = shippingProvider;
        }

        public String getShippingProviderCode() {
            return shippingProviderCode;
        }

        public void setShippingProviderCode(String shippingProviderCode) {
            this.shippingProviderCode = shippingProviderCode;
        }

        public Date getCreated() {
            return created;
        }

        public void setCreated(Date created) {
            this.created = created;
        }

        public Date getClosed() {
            return closed;
        }

        public void setClosed(Date closed) {
            this.closed = closed;
        }

        public List<CustomFieldMetadataDTO> getCustomFieldValues() {
            return customFieldValues;
        }

        public void setCustomFieldValues(List<CustomFieldMetadataDTO> customFieldValues) {
            this.customFieldValues = customFieldValues;
        }

    }

}
