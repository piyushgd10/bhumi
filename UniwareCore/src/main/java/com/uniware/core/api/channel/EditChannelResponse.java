/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jun 20, 2012
 *  @author singla
 */
package com.uniware.core.api.channel;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author Sunny
 */
public class EditChannelResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 1369085297474436784L;
    private ChannelDetailDTO  channelDetailDTO;

    public ChannelDetailDTO getChannelDetailDTO() {
        return channelDetailDTO;
    }

    public void setChannelDetailDTO(ChannelDetailDTO channelDetailDTO) {
        this.channelDetailDTO = channelDetailDTO;
    }

}
