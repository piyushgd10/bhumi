/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 12-Feb-2014
 *  @author harsh
 */
package com.uniware.core.api.metadata;

public class InformationSchemaDTO {

    String table;
    String column;
    String datatype;

    public InformationSchemaDTO() {

    }

    public InformationSchemaDTO(String table, String column, String datatype) {
        this.table = table;
        this.column = column;
        this.datatype = datatype;
    }

    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }

    public String getColumn() {
        return column;
    }

    public void setColumn(String column) {
        this.column = column;
    }

    public String getDatatype() {
        return datatype;
    }

    public void setDatatype(String datatype) {
        this.datatype = datatype;
    }

}
