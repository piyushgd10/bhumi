/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 02-Aug-2013
 *  @author pankaj
 */
package com.uniware.core.api.bundle;

import com.unifier.core.api.base.ServiceResponse;

public class GetBundleByCodeResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 6260835205752294869L;

    private BundleDTO         bundleDTO;

    public BundleDTO getBundleDTO() {
        return bundleDTO;
    }

    public void setBundleDTO(BundleDTO bundleDTO) {
        this.bundleDTO = bundleDTO;
    }

}
