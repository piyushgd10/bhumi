/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jun 20, 2012
 *  @author singla
 */
package com.uniware.core.api.channel;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author Sunny
 */
public class GetChannelOrderSummaryResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -8331014310944611193L;
    private long              todaysSuccessfulOrderCount;
    private long              todaysFailedOrderCount;
    private long              todaysRevenue;

    public long getTodaysSuccessfulOrderCount() {
        return todaysSuccessfulOrderCount;
    }

    public void setTodaysSuccessfulOrderCount(long todaysSuccessfulOrderCount) {
        this.todaysSuccessfulOrderCount = todaysSuccessfulOrderCount;
    }

    public long getTodaysFailedOrderCount() {
        return todaysFailedOrderCount;
    }

    public void setTodaysFailedOrderCount(long todaysFailedOrderCount) {
        this.todaysFailedOrderCount = todaysFailedOrderCount;
    }

    public long getTodaysRevenue() {
        return todaysRevenue;
    }

    public void setTodaysRevenue(long todaysRevenue) {
        this.todaysRevenue = todaysRevenue;
    }

}
