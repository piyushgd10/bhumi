/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 20, 2012
 *  @author praveeng
 */
package com.uniware.core.api.party.dto;

import com.uniware.core.api.party.PartySearchDTO;

/**
 * @author Sunny
 */
public class CustomerSearchDTO extends PartySearchDTO {

}
