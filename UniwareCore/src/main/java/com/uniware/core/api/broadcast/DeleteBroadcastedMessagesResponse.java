/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 13-Feb-2014
 *  @author akshay
 */
package com.uniware.core.api.broadcast;

import com.unifier.core.api.base.ServiceResponse;

public class DeleteBroadcastedMessagesResponse extends ServiceResponse{

    /**
     * 
     */
    private static final long serialVersionUID = -7240487527751761943L;

}
