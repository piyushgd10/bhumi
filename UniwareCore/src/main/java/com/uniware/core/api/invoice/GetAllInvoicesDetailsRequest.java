package com.uniware.core.api.invoice;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

/**
 * Created by Sagar Sahni on 12/06/17.
 */
public class GetAllInvoicesDetailsRequest extends ServiceRequest {

    @NotBlank
    private String shippingPackageCode;

    public String getShippingPackageCode() {
        return shippingPackageCode;
    }

    public void setShippingPackageCode(String shippingPackageCode) {
        this.shippingPackageCode = shippingPackageCode;
    }
}
