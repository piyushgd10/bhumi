/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 20-Jan-2014
 *  @author akshay
 */
package com.uniware.core.api.broadcast;

import com.unifier.core.api.base.ServiceRequest;

public class RefreshBroadcastMessageRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 20506720469167347L;

}
