/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 9, 2012
 *  @author praveeng
 */
package com.uniware.core.api.channel;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author Sunny Agarwal
 */
public class SyncChannelItemTypeInventoryResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -5466574933334174621L;

    private String            lastInventorySyncStatus;
    private Integer           lastInventoryUpdate;
    private String            message;

    public String getLastInventorySyncStatus() {
        return lastInventorySyncStatus;
    }

    public void setLastInventorySyncStatus(String lastInventorySyncStatus) {
        this.lastInventorySyncStatus = lastInventorySyncStatus;
    }

    public Integer getLastInventoryUpdate() {
        return lastInventoryUpdate;
    }

    public void setLastInventoryUpdate(Integer lastInventoryUpdate) {
        this.lastInventoryUpdate = lastInventoryUpdate;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    
}
