/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 28-Jan-2015
 *  @author akshaykochhar
 */
package com.uniware.core.api.shipping;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.unifier.core.api.base.ServiceResponse;
import com.uniware.core.api.saleorder.AddressDTO;

public class GetShippingManifestItemDetailsResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long             serialVersionUID = -5178090170704320339L;

    private ShippingManifestItemDetailDTO shippingManifestItemDetail;

    public ShippingManifestItemDetailDTO getShippingManifestItemDetail() {
        return shippingManifestItemDetail;
    }

    public void setShippingManifestItemDetail(ShippingManifestItemDetailDTO shippingManifestItemDetail) {
        this.shippingManifestItemDetail = shippingManifestItemDetail;
    }

    public static class ShippingManifestItemDetailDTO {

        private String                        code;
        private String                        shippingProviderCode;
        private String                        shippingMethodCode;
        private String                        shippingPackageType;
        private AddressDTO                    shippingAddress;
        private BigDecimal                    actualWeight;
        private Integer                       noOfItems;
        private Integer                       noOfBoxes;
        private String                        trackingNumber;
        private String                        statusCode;
        private boolean                       cashOnDelivery;
        private BigDecimal                    totalAmount;
        private List<ShippingPackageLineItem> shippingPackageLineItems = new ArrayList<ShippingPackageLineItem>();
        private Date                          updated;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getShippingProviderCode() {
            return shippingProviderCode;
        }

        public void setShippingProviderCode(String shippingProviderCode) {
            this.shippingProviderCode = shippingProviderCode;
        }

        public String getShippingMethodCode() {
            return shippingMethodCode;
        }

        public void setShippingMethodCode(String shippingMethodCode) {
            this.shippingMethodCode = shippingMethodCode;
        }

        public String getShippingPackageType() {
            return shippingPackageType;
        }

        public void setShippingPackageType(String shippingPackageType) {
            this.shippingPackageType = shippingPackageType;
        }

        public AddressDTO getShippingAddress() {
            return shippingAddress;
        }

        public void setShippingAddress(AddressDTO shippingAddress) {
            this.shippingAddress = shippingAddress;
        }

        public BigDecimal getActualWeight() {
            return actualWeight;
        }

        public void setActualWeight(BigDecimal actualWeight) {
            this.actualWeight = actualWeight;
        }

        public Integer getNoOfItems() {
            return noOfItems;
        }

        public void setNoOfItems(Integer noOfItems) {
            this.noOfItems = noOfItems;
        }

        public Integer getNoOfBoxes() {
            return noOfBoxes;
        }

        public void setNoOfBoxes(Integer noOfBoxes) {
            this.noOfBoxes = noOfBoxes;
        }

        public String getTrackingNumber() {
            return trackingNumber;
        }

        public void setTrackingNumber(String trackingNumber) {
            this.trackingNumber = trackingNumber;
        }

        public String getStatusCode() {
            return statusCode;
        }

        public void setStatusCode(String statusCode) {
            this.statusCode = statusCode;
        }

        public List<ShippingPackageLineItem> getShippingPackageLineItems() {
            return shippingPackageLineItems;
        }

        public void setShippingPackageLineItems(List<ShippingPackageLineItem> shippingPackageLineItems) {
            this.shippingPackageLineItems = shippingPackageLineItems;
        }

        public Date getUpdated() {
            return updated;
        }

        public void setUpdated(Date updated) {
            this.updated = updated;
        }
        
        public boolean isCashOnDelivery() {
            return cashOnDelivery;
        }

        public void setCashOnDelivery(boolean cashOnDelivery) {
            this.cashOnDelivery = cashOnDelivery;
        }
        
        public BigDecimal getTotalAmount() {
            return totalAmount;
        }

        public void setTotalAmount(BigDecimal totalAmount) {
            this.totalAmount = totalAmount;
        }

        public static class ShippingPackageLineItem {
            private String lineItemIdentifier;
            private String itemName;
            private String sellerSkuCode;
            private int    quantity;

            public String getLineItemIdentifier() {
                return lineItemIdentifier;
            }

            public void setLineItemIdentifier(String lineItemIdentifier) {
                this.lineItemIdentifier = lineItemIdentifier;
            }

            public String getItemName() {
                return itemName;
            }

            public void setItemName(String itemName) {
                this.itemName = itemName;
            }

            public int getQuantity() {
                return quantity;
            }

            public void setQuantity(int quantity) {
                this.quantity = quantity;
            }

            public String getSellerSkuCode() {
                return sellerSkuCode;
            }

            public void setSellerSkuCode(String sellerSkuCode) {
                this.sellerSkuCode = sellerSkuCode;
            }
        }
    }
}
