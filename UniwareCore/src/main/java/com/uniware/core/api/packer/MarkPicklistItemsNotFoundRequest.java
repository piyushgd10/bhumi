/*
 * Copyright 2017 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 16/08/17
 * @author piyush
 */
package com.uniware.core.api.packer;

import com.unifier.core.api.base.ServiceRequest;
import com.uniware.core.api.picker.EditPicklistRequest;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

public class MarkPicklistItemsNotFoundRequest extends ServiceRequest {

    @NotBlank
    private String                    picklistCode;

    @NotNull
    private Integer                   userId;

    @NotEmpty
    @Valid
    private List<EditPicklistRequest.WsEditPicklistItem> missingPicklistItems;

    public String getPicklistCode() {
        return picklistCode;
    }

    public void setPicklistCode(String picklistCode) {
        this.picklistCode = picklistCode;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public List<EditPicklistRequest.WsEditPicklistItem> getMissingPicklistItems() {
        return missingPicklistItems;
    }

    public void setMissingPicklistItems(List<EditPicklistRequest.WsEditPicklistItem> missingPicklistItems) {
        this.missingPicklistItems = missingPicklistItems;
    }

    public static class MissingPicklistItem {

        @NotBlank
        private String saleOrderItemCode;

        @NotBlank
        private String saleOrderCode;

        private String itemCode;

        public String getSaleOrderItemCode() {
            return saleOrderItemCode;
        }

        public void setSaleOrderItemCode(String saleOrderItemCode) {
            this.saleOrderItemCode = saleOrderItemCode;
        }

        public String getSaleOrderCode() {
            return saleOrderCode;
        }

        public void setSaleOrderCode(String saleOrderCode) {
            this.saleOrderCode = saleOrderCode;
        }

        public String getItemCode() {
            return itemCode;
        }

        public void setItemCode(String itemCode) {
            this.itemCode = itemCode;
        }
    }
}
