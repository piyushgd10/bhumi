/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jan 17, 2013
 *  @author Pankaj
 */
package com.uniware.core.api.inventory;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.unifier.core.api.base.ServiceResponse;
import com.uniware.core.entity.ItemType;
import com.uniware.core.entity.ItemTypeInventorySnapshot;

import javax.persistence.Transient;

/**
 * @author Pankaj
 */
public class SearchInventorySnapshotResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long          serialVersionUID = -8141339624898631224L;
    private List<InventorySnapshotDTO> elements         = new ArrayList<InventorySnapshotDTO>();
    private Long                       totalRecords;

    /**
     * @return the totalRecords
     */
    public Long getTotalRecords() {
        return totalRecords;
    }

    /**
     * @param totalRecords the totalRecords to set
     */
    public void setTotalRecords(Long totalRecords) {
        this.totalRecords = totalRecords;
    }

    /**
     * @return the elements
     */
    public List<InventorySnapshotDTO> getElements() {
        return elements;
    }

    /**
     * @param elements the elements to set
     */
    public void setElements(List<InventorySnapshotDTO> elements) {
        this.elements = elements;
    }

    static public class InventorySnapshotDTO {
        private Integer itemTypeId;
        private String  itemName;
        private long    pendingInventoryAssessment;
        private long    openSale;
        private long    inventory;
        private long    openPurchase;
        private long    putawayPending;
        private long    pendingStockTransfer;
        private long    vendorInventory;
        private long    inventoryBlocked;
        private long    virtualInventory;
        private String  itemSku;
        private String  category;
        private Date    created;
        private Date    updated;

        public InventorySnapshotDTO(Integer itemTypeId, String itemSku, long pendingInventoryAssessment, long openSale, long inventory, long openPurchase, long putawayPending,
                long pendingStockTransfer, long vendorInventory, long inventoryBlocked) {
            this.itemTypeId = itemTypeId;
            this.pendingInventoryAssessment = pendingInventoryAssessment;
            this.openSale = openSale;
            this.inventory = inventory;
            this.openPurchase = openPurchase;
            this.putawayPending = putawayPending;
            this.pendingStockTransfer = pendingStockTransfer;
            this.vendorInventory = vendorInventory;
            this.itemSku = itemSku;
            this.inventoryBlocked = inventoryBlocked;
        }
        
        public InventorySnapshotDTO(Integer itemTypeId, String itemSku, long pendingInventoryAssessment, long openSale, long inventory, long openPurchase, long putawayPending,
                long pendingStockTransfer, long vendorInventory, long inventoryBlocked, long virtualInventory) {
            this.itemTypeId = itemTypeId;
            this.pendingInventoryAssessment = pendingInventoryAssessment;
            this.openSale = openSale;
            this.inventory = inventory;
            this.openPurchase = openPurchase;
            this.putawayPending = putawayPending;
            this.pendingStockTransfer = pendingStockTransfer;
            this.vendorInventory = vendorInventory;
            this.itemSku = itemSku;
            this.inventoryBlocked = inventoryBlocked;
            this.virtualInventory = virtualInventory;
        }

        public InventorySnapshotDTO(ItemType itemType, ItemTypeInventorySnapshot itemTypeInventorySnapshot) {
            this.itemTypeId = itemType.getId();
            this.pendingInventoryAssessment = itemTypeInventorySnapshot.getPendingInventoryAssessment();
            this.openSale = itemTypeInventorySnapshot.getOpenSale();
            this.inventory = itemTypeInventorySnapshot.getInventory();
            this.openPurchase = itemTypeInventorySnapshot.getOpenPurchase();
            this.putawayPending = itemTypeInventorySnapshot.getPutawayPending();
            this.pendingStockTransfer = itemTypeInventorySnapshot.getPendingStockTransfer();
            this.vendorInventory = itemTypeInventorySnapshot.getVendorInventory();
            this.inventoryBlocked = itemTypeInventorySnapshot.getInventoryBlocked();
            this.virtualInventory = itemTypeInventorySnapshot.getVirtualInventory();
            this.itemSku = itemType.getSkuCode();
            this.itemName = itemType.getName();
        }

        /**
         * 
         */
        public InventorySnapshotDTO() {
        }

        /**
         * @return the itemName
         */
        public String getItemName() {
            return itemName;
        }

        /**
         * @param itemName the itemName to set
         */
        public void setItemName(String itemName) {
            this.itemName = itemName;
        }

        /**
         * @return the openSale
         */
        public long getOpenSale() {
            return openSale;
        }

        /**
         * @param openSale the openSale to set
         */
        public void setOpenSale(long openSale) {
            this.openSale = openSale;
        }

        /**
         * @return the inventory
         */
        public long getInventory() {
            return inventory;
        }

        /**
         * @param inventory the inventory to set
         */
        public void setInventory(long inventory) {
            this.inventory = inventory;
        }

        /**
         * @return the openPurchase
         */
        public long getOpenPurchase() {
            return openPurchase;
        }

        /**
         * @param openPurchase the openPurchase to set
         */
        public void setOpenPurchase(long openPurchase) {
            this.openPurchase = openPurchase;
        }

        /**
         * @return the putawayPending
         */
        public long getPutawayPending() {
            return putawayPending;
        }

        /**
         * @param putawayPending the putawayPending to set
         */
        public void setPutawayPending(Integer putawayPending) {
            this.putawayPending = putawayPending;
        }
        
        public long getVirtualInventory() {
            return virtualInventory;
        }

        public void setVirtualInventory(long virtualInventory) {
            this.virtualInventory = virtualInventory;
        }

        /**
         * @return the itemSku
         */
        public String getItemSku() {
            return itemSku;
        }

        /**
         * @param itemSku the itemSku to set
         */
        public void setItemSku(String itemSku) {
            this.itemSku = itemSku;
        }

        /**
         * @return the category
         */
        public String getCategory() {
            return category;
        }

        /**
         * @param category the category to set
         */
        public void setCategory(String category) {
            this.category = category;
        }

        /**
         * @return the created
         */
        public Date getCreated() {
            return created;
        }

        /**
         * @param created the created to set
         */
        public void setCreated(Date created) {
            this.created = created;
        }

        /**
         * @return the updated
         */
        public Date getUpdated() {
            return updated;
        }

        /**
         * @param updated the updated to set
         */
        public void setUpdated(Date updated) {
            this.updated = updated;
        }

        /**
         * @return the itemTypeId
         */
        public Integer getItemTypeId() {
            return itemTypeId;
        }

        /**
         * @param itemTypeId the itemTypeId to set
         */
        public void setItemTypeId(Integer itemTypeId) {
            this.itemTypeId = itemTypeId;
        }

        /**
         * @return the pendingStockTrasfer
         */
        public long getPendingStockTransfer() {
            return pendingStockTransfer;
        }

        /**
         * @param pendingStockTrasfer the pendingStockTrasfer to set
         */
        public void setPendingStockTransfer(long pendingStockTransfer) {
            this.pendingStockTransfer = pendingStockTransfer;
        }

        public long getVendorInventory() {
            return vendorInventory;
        }

        public void setVendorInventory(long vendorInventory) {
            this.vendorInventory = vendorInventory;
        }

        public long getInventoryBlocked() {
            return inventoryBlocked;
        }

        public void setInventoryBlocked(long inventoryBlocked) {
            this.inventoryBlocked = inventoryBlocked;
        }

        public long getPendingInventoryAssessment() {
            return pendingInventoryAssessment;
        }

        @Transient
        public InventorySnapshotDTO add(ItemTypeInventorySnapshot snapshot) {
            this.pendingInventoryAssessment = this.pendingInventoryAssessment + snapshot.getPendingInventoryAssessment();
            this.openSale = this.openSale + snapshot.getOpenSale();
            this.inventory = this.inventory + snapshot.getInventory();
            this.openPurchase = this.openPurchase + snapshot.getOpenPurchase();
            this.putawayPending = this.putawayPending + snapshot.getPutawayPending();
            this.pendingStockTransfer = this.pendingStockTransfer + snapshot.getPendingStockTransfer();
            this.vendorInventory = this.vendorInventory + snapshot.getVendorInventory();
            this.inventoryBlocked = this.inventoryBlocked + snapshot.getInventoryBlocked();
            this.virtualInventory = this.virtualInventory + snapshot.getVirtualInventory();
            return this;
        }
    }

}
