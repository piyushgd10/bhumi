/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 28-Jan-2015
 *  @author akshaykochhar
 */
package com.uniware.core.api.shipping;

import com.unifier.core.api.base.ServiceResponse;

public class GetManifestSummaryResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 1721297961924991984L;

    private ManifestSummaryDTO   summary;
    
    public ManifestSummaryDTO getSummary() {
        return summary;
    }

    public void setSummary(ManifestSummaryDTO summary) {
        this.summary = summary;
    }
}
