/*
*  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
*  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
*  
*  @version     1.0, 02/09/14
*  @author sunny
*/

package com.uniware.core.api.catalog;

import java.io.InputStream;

import javax.validation.constraints.NotNull;

import com.unifier.core.api.base.ServiceRequest;

public class LookupSaleOrderLineItemsRequest extends ServiceRequest {

    private static final long serialVersionUID = -6407438147922948722L;

    @NotNull
    private InputStream       inputStream;

    public InputStream getInputStream() {
        return inputStream;
    }

    public void setInputStream(InputStream inputStream) {
        this.inputStream = inputStream;
    }
}
