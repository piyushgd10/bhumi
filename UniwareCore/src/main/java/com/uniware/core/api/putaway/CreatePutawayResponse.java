/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, May 8, 2012
 *  @author singla
 */
package com.uniware.core.api.putaway;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author singla
 */
public class CreatePutawayResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -2997816780794654029L;

    private PutawayDTO        putawayDTO;

    /**
     * @return the putawayDTO
     */
    public PutawayDTO getPutawayDTO() {
        return putawayDTO;
    }

    /**
     * @param putawayDTO the putawayDTO to set
     */
    public void setPutawayDTO(PutawayDTO putawayDTO) {
        this.putawayDTO = putawayDTO;
    }

}