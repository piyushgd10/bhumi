/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 07-Apr-2014
 *  @author harsh
 */
package com.uniware.core.api.channel;

import com.unifier.core.api.base.ServiceRequest;

public class GetChannelInventoryUpdateSnapshotRequest extends ServiceRequest {

    private static final long serialVersionUID = 1662520150487021264L;

    String                    snapshotId;

    public String getSnapshotId() {
        return snapshotId;
    }

    public void setSnapshotId(String snapshotId) {
        this.snapshotId = snapshotId;
    }

}
