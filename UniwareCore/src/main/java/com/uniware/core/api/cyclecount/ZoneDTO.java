package com.uniware.core.api.cyclecount;

/**
 * Created by harshpal on 08/04/16.
 */
public class ZoneDTO {
    private String name;
    private long   totalShelfCount;
    private long   pendingShelfCount;

    public ZoneDTO() {
    }

    public ZoneDTO(String name, long totalShelfCount, long pendingShelfCount) {
        this.name = name;
        this.totalShelfCount = totalShelfCount;
        this.pendingShelfCount = pendingShelfCount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getTotalShelfCount() {
        return totalShelfCount;
    }

    public void setTotalShelfCount(int totalShelfCount) {
        this.totalShelfCount = totalShelfCount;
    }

    public long getPendingShelfCount() {
        return pendingShelfCount;
    }

    public void setPendingShelfCount(int pendingShelfCount) {
        this.pendingShelfCount = pendingShelfCount;
    }
}
