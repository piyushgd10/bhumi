/*
 * Copyright 2017 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 20/04/17
 * @author piyush
 */
package com.uniware.core.api.putaway;

import com.unifier.core.api.base.ServiceRequest;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import java.util.List;

public class CompletePutawayItemsRequest extends ServiceRequest {

    @NotBlank
    private String             putawayCode;

    @NotEmpty
    private List<WsShelfItems> shelfItemsList;

    public String getPutawayCode() {
        return putawayCode;
    }

    public void setPutawayCode(String putawayCode) {
        this.putawayCode = putawayCode;
    }

    public List<WsShelfItems> getShelfItemsList() {
        return shelfItemsList;
    }

    public void setShelfItemsList(List<WsShelfItems> shelfItemsList) {
        this.shelfItemsList = shelfItemsList;
    }

    public static class WsShelfItems {

        @NotBlank
        private String              shelfCode;

        private List<String>        itemCodes;

        private List<WsPutawayItem> putawayItems;

        public String getShelfCode() {
            return shelfCode;
        }

        public void setShelfCode(String shelfCode) {
            this.shelfCode = shelfCode;
        }

        public List<String> getItemCodes() {
            return itemCodes;
        }

        public void setItemCodes(List<String> itemCodes) {
            this.itemCodes = itemCodes;
        }

        public List<WsPutawayItem> getPutawayItems() {
            return putawayItems;
        }

        public void setPutawayItems(List<WsPutawayItem> putawayItems) {
            this.putawayItems = putawayItems;
        }
    }

    public static class WsPutawayItem {

        private String skuCode;

        private int    quantity;

        private String type;

        public String getSkuCode() {
            return skuCode;
        }

        public void setSkuCode(String skuCode) {
            this.skuCode = skuCode;
        }

        public int getQuantity() {
            return quantity;
        }

        public void setQuantity(int quantity) {
            this.quantity = quantity;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }
    }
}
