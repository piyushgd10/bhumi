/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 2, 2012
 *  @author praveeng
 */
package com.uniware.core.api.purchase;

import java.util.List;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author Sunny Agarwal
 */
public class GetVendorPurchaseOrderDetailRequest extends ServiceRequest {
    /**
     * 
     */
    private static final long serialVersionUID = -3828893208167364186L;

    @NotEmpty
    private List<Integer>     vendorIds;

    @NotBlank
    private String            purchaseOrderCode;

    /**
     * @return the purchaseOrderCode
     */
    public String getPurchaseOrderCode() {
        return purchaseOrderCode;
    }

    /**
     * @param purchaseOrderCode the purchaseOrderCode to set
     */
    public void setPurchaseOrderCode(String purchaseOrderCode) {
        this.purchaseOrderCode = purchaseOrderCode;
    }

    public List<Integer> getVendorIds() {
        return vendorIds;
    }

    public void setVendorIds(List<Integer> vendorIds) {
        this.vendorIds = vendorIds;
    }

}
