/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 23-Mar-2014
 *  @author karunsingla
 */
package com.uniware.core.api.material;

import com.unifier.core.api.base.ServiceResponse;

public class RemoveItemFromGatePassResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 4517002771651247148L;

}
