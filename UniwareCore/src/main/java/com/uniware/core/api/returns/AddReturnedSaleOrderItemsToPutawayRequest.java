/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 27-Aug-2013
 *  @author karunsingla
 */
package com.uniware.core.api.returns;

import com.unifier.core.api.base.ServiceRequest;
import com.unifier.core.api.customfields.WsCustomFieldValue;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import com.uniware.core.api.reversepickup.AddReversePickupSaleOrderItemsToPutawayRequest.WsSaleOrderItem;

public class AddReturnedSaleOrderItemsToPutawayRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long     serialVersionUID = -7666020251134718820L;

    @NotBlank
    private String                putawayCode;

    @NotNull
    private Integer               userId;

    @NotBlank
    private String                saleOrderCode;

    private List<WsSaleOrderItem> returnedSaleOrderItems;
    
    @Valid
    private List<WsCustomFieldValue>  customFieldValues;

    @NotBlank
    @Length(max = 500, min = 1)
    private String                reason;

    public String getSaleOrderCode() {
        return saleOrderCode;
    }

    public void setSaleOrderCode(String saleOrderCode) {
        this.saleOrderCode = saleOrderCode;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getPutawayCode() {
        return putawayCode;
    }

    public void setPutawayCode(String putawayCode) {
        this.putawayCode = putawayCode;
    }

    public List<WsSaleOrderItem> getReturnedSaleOrderItems() {
        return returnedSaleOrderItems;
    }

    public void setReturnedSaleOrderItems(List<WsSaleOrderItem> returnedSaleOrderItems) {
        this.returnedSaleOrderItems = returnedSaleOrderItems;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public List<WsCustomFieldValue> getCustomFieldValues() {
        return customFieldValues;
    }

    public void setCustomFieldValues(List<WsCustomFieldValue> customFieldValues) {
        this.customFieldValues = customFieldValues;
    }
    
    
}
