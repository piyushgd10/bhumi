/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 10-May-2012
 *  @author praveeng
 */
package com.uniware.core.api.purchase;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author praveeng
 */
public class CreateASNResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 4534802540463054538L;
    private String            asnCode;

    public String getAsnCode() {
        return asnCode;
    }

    public void setAsnCode(String asnCode) {
        this.asnCode = asnCode;
    }

}