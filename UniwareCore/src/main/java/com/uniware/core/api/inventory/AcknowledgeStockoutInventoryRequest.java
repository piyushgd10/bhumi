package com.uniware.core.api.inventory;

import com.unifier.core.api.base.ServiceRequest;
import com.uniware.core.entity.InventoryAvailabilityChangeLedger;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Created by piyush on 1/6/16.
 */
public class AcknowledgeStockoutInventoryRequest extends ServiceRequest {

    @NotNull
    private List<InventoryAvailabilityChangeLedger> pendingInventoryLedgers;

    @NotNull
    private String facilityCode;

    public AcknowledgeStockoutInventoryRequest(List<InventoryAvailabilityChangeLedger> inventoryLedgers, String facilityCode) {
        this.pendingInventoryLedgers = inventoryLedgers;
        this.facilityCode = facilityCode;
    }

    public List<InventoryAvailabilityChangeLedger> getPendingInventoryLedgers() {
        return pendingInventoryLedgers;
    }

    public void setPendingInventoryLedgers(List<InventoryAvailabilityChangeLedger> pendingInventoryLedgers) {
        this.pendingInventoryLedgers = pendingInventoryLedgers;
    }

    public String getFacilityCode() {
        return facilityCode;
    }

    public void setFacilityCode(String facilityCode) {
        this.facilityCode = facilityCode;
    }
}
