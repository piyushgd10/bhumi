package com.uniware.core.api.prices;

import com.unifier.core.api.base.ServiceResponse;
import com.uniware.core.api.channel.ChannelPricingPushStatusDTO;

/**
 * Created by shobhit on 10/11/15.
 */
public class GetChannelPricingSyncStatusResponse extends ServiceResponse{

    private static final long serialVersionUID = -8331014310944611193L;
    private ChannelPricingPushStatusDTO priceSyncStatus;

    public ChannelPricingPushStatusDTO getPriceSyncStatus() {
        return priceSyncStatus;
    }

    public void setPriceSyncStatus(ChannelPricingPushStatusDTO priceSyncStatus) {
        this.priceSyncStatus = priceSyncStatus;
    }

}
