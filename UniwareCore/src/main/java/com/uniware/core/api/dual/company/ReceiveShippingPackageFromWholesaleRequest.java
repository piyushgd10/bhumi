/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Oct 19, 2012
 *  @author singla
 */
package com.uniware.core.api.dual.company;

import com.unifier.core.api.base.ServiceRequest;

import java.math.BigDecimal;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

public class ReceiveShippingPackageFromWholesaleRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long              serialVersionUID = -3654208960654364558L;

    @NotBlank
    private String                         saleOrderCode;

    @NotEmpty
    @Valid
    private List<WsWholesaleSaleOrderItem> saleOrderItems;

    @NotBlank
    private String                         shippingPackageCode;

    /**
     * @return the saleOrderCode
     */
    public String getSaleOrderCode() {
        return saleOrderCode;
    }

    /**
     * @param saleOrderCode the saleOrderCode to set
     */
    public void setSaleOrderCode(String saleOrderCode) {
        this.saleOrderCode = saleOrderCode;
    }

    /**
     * @return the shippingPackageCode
     */
    public String getShippingPackageCode() {
        return shippingPackageCode;
    }

    /**
     * @param shippingPackageCode the shippingPackageCode to set
     */
    public void setShippingPackageCode(String shippingPackageCode) {
        this.shippingPackageCode = shippingPackageCode;
    }

    /**
     * @return the saleOrderItems
     */
    public List<WsWholesaleSaleOrderItem> getSaleOrderItems() {
        return saleOrderItems;
    }

    /**
     * @param saleOrderItems the saleOrderItems to set
     */
    public void setSaleOrderItems(List<WsWholesaleSaleOrderItem> saleOrderItems) {
        this.saleOrderItems = saleOrderItems;
    }

    public static class WsWholesaleSaleOrderItem {

        @NotBlank
        private String     saleOrderItemCode;

        @NotBlank
        private String     itemCode;

        @NotNull
        private BigDecimal sellingPrice;

        private String     itemCustomFieldValues;

        /**
         * @return the saleOrderItemCode
         */
        public String getSaleOrderItemCode() {
            return saleOrderItemCode;
        }

        /**
         * @param saleOrderItemCode the saleOrderItemCode to set
         */
        public void setSaleOrderItemCode(String saleOrderItemCode) {
            this.saleOrderItemCode = saleOrderItemCode;
        }

        /**
         * @return the itemCode
         */
        public String getItemCode() {
            return itemCode;
        }

        /**
         * @param itemCode the itemCode to set
         */
        public void setItemCode(String itemCode) {
            this.itemCode = itemCode;
        }

        /**
         * @return the sellingPrice
         */
        public BigDecimal getSellingPrice() {
            return sellingPrice;
        }

        /**
         * @param sellingPrice the sellingPrice to set
         */
        public void setSellingPrice(BigDecimal sellingPrice) {
            this.sellingPrice = sellingPrice;
        }

        public String getItemCustomFieldValues() {
            return itemCustomFieldValues;
        }

        public void setItemCustomFieldValues(String itemCustomFieldValues) {
            this.itemCustomFieldValues = itemCustomFieldValues;
        }

    }

}
