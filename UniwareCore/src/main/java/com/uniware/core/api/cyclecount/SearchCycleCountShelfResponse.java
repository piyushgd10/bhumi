package com.uniware.core.api.cyclecount;

import com.unifier.core.api.base.ServiceResponse;

/**
 * Created by harshpal on 20/04/16.
 */
public class SearchCycleCountShelfResponse extends ServiceResponse {

    SubCycleCountDTO.ShelfDTO shelf;

    public SubCycleCountDTO.ShelfDTO getShelf() {
        return shelf;
    }

    public void setShelf(SubCycleCountDTO.ShelfDTO shelf) {
        this.shelf = shelf;
    }
}
