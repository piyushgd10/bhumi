/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 11-Nov-2013
 *  @author unicom
 */
package com.uniware.core.api.inflow;

import java.util.List;

import javax.validation.Valid;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import com.unifier.core.api.base.ServiceRequest;

public class AddSaleOrderItemDetailRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @NotBlank
    private String            saleOrderCode;

    @NotBlank
    private String            saleOrderItemCode;

    @NotEmpty
    @Valid
    private List<ItemDetail>  itemDetails;

    public String getSaleOrderCode() {
        return saleOrderCode;
    }

    public void setSaleOrderCode(String saleOrderCode) {
        this.saleOrderCode = saleOrderCode;
    }

    public String getSaleOrderItemCode() {
        return saleOrderItemCode;
    }

    public void setSaleOrderItemCode(String saleOrderItemCode) {
        this.saleOrderItemCode = saleOrderItemCode;
    }

    public List<ItemDetail> getItemDetails() {
        return itemDetails;
    }

    public void setItemDetails(List<ItemDetail> itemDetails) {
        this.itemDetails = itemDetails;
    }

}
