/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 14-May-2012
 *  @author praveeng
 */
package com.uniware.core.api.reversepickup;

import com.unifier.core.api.base.ServiceResponse;

import java.util.ArrayList;
import java.util.List;

import com.uniware.core.api.putaway.PutawayItemDTO;

/**
 * @author praveeng
 */
public class AddReversePickupSaleOrderItemsToPutawayResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long    serialVersionUID = 515395403749889928L;
    private List<PutawayItemDTO> putawayItems     = new ArrayList<PutawayItemDTO>();

    /**
     * @return the putawayItems
     */
    public List<PutawayItemDTO> getPutawayItems() {
        return putawayItems;
    }

    /**
     * @param putawayItems the putawayItems to set
     */
    public void setPutawayItems(List<PutawayItemDTO> putawayItems) {
        this.putawayItems = putawayItems;
    }

}
