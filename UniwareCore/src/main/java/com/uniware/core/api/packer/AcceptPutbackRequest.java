/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 23, 2012
 *  @author singla
 */
package com.uniware.core.api.packer;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author singla
 */
public class AcceptPutbackRequest extends ServiceRequest {
    /**
     * 
     */
    private static final long serialVersionUID = -421573802557576449L;

    @NotBlank
    private String            picklistCode;

    /**
     * @return the picklistCode
     */
    public String getPicklistCode() {
        return picklistCode;
    }

    /**
     * @param picklistCode the picklistCode to set
     */
    public void setPicklistCode(String picklistCode) {
        this.picklistCode = picklistCode;
    }

}
