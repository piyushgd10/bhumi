/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, May 7, 2012
 *  @author singla
 */
package com.uniware.core.api.inflow;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author singla
 */
public class DiscardTraceableInflowReceiptItemResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long    serialVersionUID = -6624313682487708423L;

    private InflowReceiptItemDTO inflowReceiptItem;

    /**
     * @return the inflowReceiptItemDTO
     */
    public InflowReceiptItemDTO getInflowReceiptItem() {
        return inflowReceiptItem;
    }

    /**
     * @param inflowReceiptItemDTO the inflowReceiptItemDTO to set
     */
    public void setInflowReceiptItemDTO(InflowReceiptItemDTO inflowReceiptItem) {
        this.inflowReceiptItem = inflowReceiptItem;
    }

}
