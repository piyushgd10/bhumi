/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Nov 25, 2015
 *  @author akshay
 */
package com.uniware.core.api.recommendation;

import java.util.ArrayList;
import java.util.List;

import com.unifier.core.api.base.ServiceResponse;
import com.uniware.core.api.recommendation.dto.RecommendationSubscriptionDTO;

public class GetSubscribedRecommendationProvidersResponse extends ServiceResponse {

    private static final long serialVersionUID = 3646784109574037816L;
    
    private List<RecommendationSubscriptionDTO> subscriptions = new ArrayList<>();

    public List<RecommendationSubscriptionDTO> getSubscriptions() {
        return subscriptions;
    }

    public void setSubscriptions(List<RecommendationSubscriptionDTO> subscriptions) {
        this.subscriptions = subscriptions;
    }

}
