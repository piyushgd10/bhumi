/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 2, 2012
 *  @author singla
 */
package com.uniware.core.api.inflow;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author singla
 */
public class RejectNonTraceableItemResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long    serialVersionUID = -4150194382129746651L;

    private InflowReceiptItemDTO inflowReceiptItemDTO;

    /**
     * @return the inflowReceiptItemDTO
     */
    public InflowReceiptItemDTO getInflowReceiptItemDTO() {
        return inflowReceiptItemDTO;
    }

    /**
     * @param inflowReceiptItemDTO the inflowReceiptItemDTO to set
     */
    public void setInflowReceiptItemDTO(InflowReceiptItemDTO inflowReceiptItemDTO) {
        this.inflowReceiptItemDTO = inflowReceiptItemDTO;
    }
}
