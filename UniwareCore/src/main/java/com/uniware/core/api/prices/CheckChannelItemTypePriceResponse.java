/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Oct 23, 2015
 *  @author akshay
 */
package com.uniware.core.api.prices;

import com.unifier.core.api.base.ServiceResponse;
import com.uniware.core.api.prices.dto.PriceDTO;

public class CheckChannelItemTypePriceResponse extends ServiceResponse {

    private static final long serialVersionUID = 7275310957912395279L;
    
    private PriceDTO channelItemTypePrice;

    public PriceDTO getChannelItemTypePrice() {
        return channelItemTypePrice;
    }

    public void setChannelItemTypePrice(PriceDTO channelItemTypePrice) {
        this.channelItemTypePrice = channelItemTypePrice;
    }
    
}
