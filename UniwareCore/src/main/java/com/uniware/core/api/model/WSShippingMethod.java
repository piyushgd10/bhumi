/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 12-Feb-2015
 *  @author parijat
 */
package com.uniware.core.api.model;

import org.hibernate.validator.constraints.NotEmpty;

public class WSShippingMethod {

    @NotEmpty
    private String  code;

    private boolean enabled;

    @NotEmpty
    private String  awbGeneration;

    private String  facilityCode;

    public WSShippingMethod() {
    }

    public WSShippingMethod(String code, boolean enabled, String awbGeneration) {
        this.code = code;
        this.enabled = enabled;
        this.awbGeneration = awbGeneration;
    }

    public WSShippingMethod(String code, boolean enabled, String awbGeneration, String facilityCode) {
        this.code = code;
        this.enabled = enabled;
        this.awbGeneration = awbGeneration;
        this.facilityCode = facilityCode;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the enabled
     */
    public boolean isEnabled() {
        return enabled;
    }

    /**
     * @param enabled the enabled to set
     */
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    /**
     * @return the awbGeneration
     */
    public String getAwbGeneration() {
        return awbGeneration;
    }

    /**
     * @param awbGeneration the awbGeneration to set
     */
    public void setAwbGeneration(String awbGeneration) {
        this.awbGeneration = awbGeneration;
    }

    public String getFacilityCode() {
        return facilityCode;
    }

    public void setFacilityCode(String facilityCode) {
        this.facilityCode = facilityCode;
    }

}
