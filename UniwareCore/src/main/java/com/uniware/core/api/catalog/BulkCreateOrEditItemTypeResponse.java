/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, May 27, 2012
 *  @author singla
 */
package com.uniware.core.api.catalog;

import com.unifier.core.api.base.ServiceResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * @author singla
 */
public class BulkCreateOrEditItemTypeResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -2444048709025857647L;

    private List<String>      successfulSkuCodes;
    private List<String>      failedSkuCodes;

    /**
     * @return the failedSkuCodes
     */
    public List<String> getFailedSkuCodes() {
        return failedSkuCodes;
    }

    /**
     * @param failedSkuCodes the failedSkuCodes to set
     */
    public void setFailedSkuCodes(List<String> failedSkuCodes) {
        this.failedSkuCodes = failedSkuCodes;
    }

    /**
     * @return the successfulSkuCodes
     */
    public List<String> getSuccessfulSkuCodes() {
        return successfulSkuCodes;
    }

    /**
     * @param successfulSkuCodes the successfulSkuCodes to set
     */
    public void setSuccessfulSkuCodes(List<String> successfulSkuCodes) {
        this.successfulSkuCodes = successfulSkuCodes;
    }

    public void addSuccessfulSkuCode(String skuCode) {
        if (this.successfulSkuCodes == null) {
            this.successfulSkuCodes = new ArrayList<String>();
        }
        this.successfulSkuCodes.add(skuCode);
    }

    public void addFailedSkuCode(String skuCode) {
        if (this.failedSkuCodes == null) {
            this.failedSkuCodes = new ArrayList<String>();
        }
        this.failedSkuCodes.add(skuCode);
    }
}
