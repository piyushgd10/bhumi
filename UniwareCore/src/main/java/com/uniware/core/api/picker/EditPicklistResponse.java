/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jan 11, 2012
 *  @author singla
 */
package com.uniware.core.api.picker;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author singla
 */
public class EditPicklistResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -209810497673301835L;

    private PicklistDTO       picklist;

    public void setPicklist(PicklistDTO picklist) {
        this.picklist = picklist;
    }

    public PicklistDTO getPicklist() {
        return picklist;
    }
}
