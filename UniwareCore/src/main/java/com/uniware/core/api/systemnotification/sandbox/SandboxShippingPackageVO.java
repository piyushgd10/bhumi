/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 25-Dec-2013
 *  @author sunny
 */
package com.uniware.core.api.systemnotification.sandbox;

import java.util.HashSet;
import java.util.Set;

import com.uniware.core.entity.SaleOrderItem;
import com.uniware.core.entity.ShippingPackage;

public class SandboxShippingPackageVO {

    private String                      code;
    private SandboxSaleOrderVO          saleOrder;
    private Set<SandboxSaleOrderItemVO> saleOrderItems;
    private String                      statusCode;
    private String                      trackingNumber;
    private SandboxShippingProviderVO   shippingProvider;
    private SandboxInvoiceVO            invoice;
    private String                      shippingLabelLink;

    public SandboxShippingPackageVO() {
        super();
    }

    public SandboxShippingPackageVO(ShippingPackage sp) {
        this.code = sp.getCode();
        this.saleOrder = new SandboxSaleOrderVO(sp.getSaleOrder());
        this.statusCode = sp.getStatusCode();
        this.trackingNumber = sp.getTrackingNumber();
        this.shippingLabelLink = sp.getShippingLabelLink();
        this.shippingProvider = sp.getShippingProviderCode() != null ? new SandboxShippingProviderVO(sp.getShippingProviderCode(), sp.getShippingProviderName()) : null;
        this.saleOrderItems = new HashSet<SandboxSaleOrderItemVO>(sp.getSaleOrderItems().size());
        this.invoice = new SandboxInvoiceVO(sp.getInvoice());
        for (SaleOrderItem soi : sp.getSaleOrderItems()) {
            saleOrderItems.add(new SandboxSaleOrderItemVO(soi));
        }
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public SandboxSaleOrderVO getSaleOrder() {
        return saleOrder;
    }

    public void setSaleOrder(SandboxSaleOrderVO saleOrder) {
        this.saleOrder = saleOrder;
    }

    public Set<SandboxSaleOrderItemVO> getSaleOrderItems() {
        return saleOrderItems;
    }

    public void setSaleOrderItems(Set<SandboxSaleOrderItemVO> saleOrderItems) {
        this.saleOrderItems = saleOrderItems;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getTrackingNumber() {
        return trackingNumber;
    }

    public void setTrackingNumber(String trackingNumber) {
        this.trackingNumber = trackingNumber;
    }

    public SandboxShippingProviderVO getShippingProvider() {
        return shippingProvider;
    }

    public void setShippingProvider(SandboxShippingProviderVO shippingProvider) {
        this.shippingProvider = shippingProvider;
    }

    public String getShippingLabelLink() {
        return shippingLabelLink;
    }

    public void setShippingLabelLink(String shippingLabelLink) {
        this.shippingLabelLink = shippingLabelLink;
    }

    public SandboxInvoiceVO getInvoice() {
        return invoice;
    }

    public void setInvoice(SandboxInvoiceVO invoice) {
        this.invoice = invoice;
    }

}
