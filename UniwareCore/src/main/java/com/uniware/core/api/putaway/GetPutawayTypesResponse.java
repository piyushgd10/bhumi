/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 30-Mar-2015
 *  @author unicommerce
 */
package com.uniware.core.api.putaway;

import java.util.List;

import com.unifier.core.api.base.ServiceResponse;

public class GetPutawayTypesResponse extends ServiceResponse{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    private List<String> types;

    public List<String> getTypes() {
        return types;
    }

    public void setTypes(List<String> types) {
        this.types = types;
    }
    
}
