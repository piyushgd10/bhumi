package com.uniware.core.api.prices;

/**
 * Created by shobhit on 10/11/15.
 */
public class GetChannelPricingSyncStatusRequest {

    private static final long serialVersionUID = 1544077290732461048L;

    private String            channelCode;

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }
}
