/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 20, 2012
 *  @author praveeng
 */
package com.uniware.core.api.party;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author praveeng
 */
public class CreatePartyContactResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -6987391793911965878L;
    private PartyContactDTO   partyContactDTO;
    /**
     * @return the partyContactDTO
     */
    public PartyContactDTO getPartyContactDTO() {
        return partyContactDTO;
    }
    /**
     * @param partyContactDTO the partyContactDTO to set
     */
    public void setPartyContactDTO(PartyContactDTO partyContactDTO) {
        this.partyContactDTO = partyContactDTO;
    }
}
