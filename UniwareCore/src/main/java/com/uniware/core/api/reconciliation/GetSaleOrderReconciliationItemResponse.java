package com.uniware.core.api.reconciliation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.unifier.core.api.base.ServiceResponse;

/**
 * Created by akshayag on 10/4/16.
 */

public class GetSaleOrderReconciliationItemResponse extends ServiceResponse {

    private List<SaleOrderReconciliationItemDTO> saleOrderReconciliationItems = new ArrayList<>();

    public static class SaleOrderReconciliationItemDTO {
        private String     saleOrderItemCode;
        private BigDecimal voucherValue            = BigDecimal.ZERO;
        private BigDecimal storeCredit             = BigDecimal.ZERO;
        private BigDecimal dropshipShippingCharges = BigDecimal.ZERO;
        private BigDecimal dropshipCommission      = BigDecimal.ZERO;
        private BigDecimal giftWrapCharges         = BigDecimal.ZERO;
        private BigDecimal totalPrice;
        private BigDecimal sellingPrice;
        private BigDecimal discount                = BigDecimal.ZERO;
        private BigDecimal prepaidAmount           = BigDecimal.ZERO;
        private BigDecimal shippingCharges         = BigDecimal.ZERO;
        private BigDecimal shippingMethodCharges   = BigDecimal.ZERO;
        private BigDecimal cashOnDeliveryCharges   = BigDecimal.ZERO;
        private String     statusCode;
        private String     shippingMethod;

        public String getSaleOrderItemCode() {
            return saleOrderItemCode;
        }

        public void setSaleOrderItemCode(String saleOrderItemCode) {
            this.saleOrderItemCode = saleOrderItemCode;
        }

        public BigDecimal getVoucherValue() {
            return voucherValue;
        }

        public void setVoucherValue(BigDecimal voucherValue) {
            this.voucherValue = voucherValue;
        }

        public BigDecimal getStoreCredit() {
            return storeCredit;
        }

        public void setStoreCredit(BigDecimal storeCredit) {
            this.storeCredit = storeCredit;
        }

        public BigDecimal getDropshipShippingCharges() {
            return dropshipShippingCharges;
        }

        public void setDropshipShippingCharges(BigDecimal dropshipShippingCharges) {
            this.dropshipShippingCharges = dropshipShippingCharges;
        }

        public BigDecimal getDropshipCommission() {
            return dropshipCommission;
        }

        public void setDropshipCommission(BigDecimal dropshipCommission) {
            this.dropshipCommission = dropshipCommission;
        }

        public BigDecimal getGiftWrapCharges() {
            return giftWrapCharges;
        }

        public void setGiftWrapCharges(BigDecimal giftWrapCharges) {
            this.giftWrapCharges = giftWrapCharges;
        }

        public BigDecimal getTotalPrice() {
            return totalPrice;
        }

        public void setTotalPrice(BigDecimal totalPrice) {
            this.totalPrice = totalPrice;
        }

        public BigDecimal getSellingPrice() {
            return sellingPrice;
        }

        public void setSellingPrice(BigDecimal sellingPrice) {
            this.sellingPrice = sellingPrice;
        }

        public BigDecimal getDiscount() {
            return discount;
        }

        public void setDiscount(BigDecimal discount) {
            this.discount = discount;
        }

        public BigDecimal getPrepaidAmount() {
            return prepaidAmount;
        }

        public void setPrepaidAmount(BigDecimal prepaidAmount) {
            this.prepaidAmount = prepaidAmount;
        }

        public BigDecimal getShippingCharges() {
            return shippingCharges;
        }

        public void setShippingCharges(BigDecimal shippingCharges) {
            this.shippingCharges = shippingCharges;
        }

        public BigDecimal getShippingMethodCharges() {
            return shippingMethodCharges;
        }

        public void setShippingMethodCharges(BigDecimal shippingMethodCharges) {
            this.shippingMethodCharges = shippingMethodCharges;
        }

        public BigDecimal getCashOnDeliveryCharges() {
            return cashOnDeliveryCharges;
        }

        public void setCashOnDeliveryCharges(BigDecimal cashOnDeliveryCharges) {
            this.cashOnDeliveryCharges = cashOnDeliveryCharges;
        }

        public String getStatusCode() {
            return statusCode;
        }

        public void setStatusCode(String statusCode) {
            this.statusCode = statusCode;
        }

        public String getShippingMethod() {
            return shippingMethod;
        }

        public void setShippingMethod(String shippingMethod) {
            this.shippingMethod = shippingMethod;
        }
    }

    public List<SaleOrderReconciliationItemDTO> getSaleOrderReconciliationItems() {
        return saleOrderReconciliationItems;
    }

    public void setSaleOrderReconciliationItems(List<SaleOrderReconciliationItemDTO> saleOrderReconciliationItems) {
        this.saleOrderReconciliationItems = saleOrderReconciliationItems;
    }
}
