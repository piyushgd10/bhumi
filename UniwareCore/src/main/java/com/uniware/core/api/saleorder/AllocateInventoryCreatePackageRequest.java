/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 27-Aug-2015
 *  @author parijat
 */
package com.uniware.core.api.saleorder;

import com.unifier.core.api.base.ServiceRequest;

import javax.validation.constraints.NotNull;

public class AllocateInventoryCreatePackageRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 876545678765567L;

    @NotNull
    private Integer           saleOrderId;

    /**
     * @return the saleOrderId
     */
    public Integer getSaleOrderId() {
        return saleOrderId;
    }

    /**
     * @param saleOrderId the saleOrderId to set
     */
    public void setSaleOrderId(Integer saleOrderId) {
        this.saleOrderId = saleOrderId;
    }

}
