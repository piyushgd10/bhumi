package com.uniware.core.api.saleorder;

import com.uniware.core.api.inflow.ItemDetail;
import java.util.List;
import javax.validation.Valid;

/**
 * Created by admin on 02/03/17.
 */
public class SaleOrderItemForDetail {

    private String            saleOrderItemCode;

    @Valid
    private List<ItemDetail> itemDetails;

    public SaleOrderItemForDetail() {
    }

    public SaleOrderItemForDetail(String saleOrderItemCode, List<ItemDetail> itemDetails) {
        this.saleOrderItemCode = saleOrderItemCode;
        this.itemDetails = itemDetails;
    }

    public String getSaleOrderItemCode() {
        return saleOrderItemCode;
    }

    public void setSaleOrderItemCode(String saleOrderItemCode) {
        this.saleOrderItemCode = saleOrderItemCode;
    }

    public List<ItemDetail> getItemDetails() {
        return itemDetails;
    }

    public void setItemDetails(List<ItemDetail> itemDetails) {
        this.itemDetails = itemDetails;
    }

}
