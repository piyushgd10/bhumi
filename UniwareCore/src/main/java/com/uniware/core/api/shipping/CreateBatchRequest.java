/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 29-Sep-2014
 *  @author akshay
 */
package com.uniware.core.api.shipping;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

public class CreateBatchRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    @NotBlank
    private String shipmentBatchCode;

    public String getShipmentBatchCode() {
        return shipmentBatchCode;
    }

    public void setShipmentBatchCode(String shipmentBatchCode) {
        this.shipmentBatchCode = shipmentBatchCode;
    }
    
    

}
