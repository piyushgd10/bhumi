/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 24-Dec-2011
 *  @author vibhu
 */
package com.uniware.core.api.channel;

import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import org.hibernate.validator.constraints.NotBlank;

import com.uniware.core.entity.ChannelItemType;
import com.uniware.core.entity.ChannelItemType.ListingStatus;
import com.uniware.core.vo.ChannelItemTypeVO;

/**
 * @author Sunny Agarwal
 */
public class ChannelItemTypeDTO {

    @NotBlank
    private String                            channelCode;

    @NotBlank
    private String                            channelProductId;

    private String                            sellerSkuCode;

    private String                            skuCode;

    private ChannelItemType.Status            statusCode;

    private String                            productName;

    private String                            productUrl;

    private String                            size;

    private String                            color;

    private String                            brand;

    private Set<String>                       imageUrls;

    private String                            productDescription;

    private ListingStatus                     listingStatus;

    private boolean                           verified;

    @Valid
    private List<ChannelItemTypeVO.Attribute> attributes;

    public ChannelItemTypeDTO() {
    }

    public ChannelItemTypeDTO(ChannelItemType cit, ChannelItemTypeVO citVO) {
        this.channelCode = cit.getChannel().getCode();
        this.channelProductId = cit.getChannelProductId();
        this.sellerSkuCode = cit.getSellerSkuCode();
        this.skuCode = cit.getItemType() != null ? cit.getItemType().getSkuCode() : null;
        this.statusCode = cit.getStatusCode();
        this.productName = cit.getProductName();
        this.productUrl = cit.getProductUrl();
        this.size = cit.getSize();
        this.color = cit.getColor();
        this.listingStatus = cit.getListingStatus();
        this.verified = cit.isVerified();
        if (citVO != null) {
            this.brand = citVO.getBrand();
            this.imageUrls = citVO.getImageUrls();
            this.productDescription = citVO.getProductDescription();
            this.attributes = citVO.getAttributes();
        }
    }

    public String getChannelProductId() {
        return channelProductId;
    }

    public void setChannelProductId(String channelProductId) {
        this.channelProductId = channelProductId;
    }

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    public String getSellerSkuCode() {
        return sellerSkuCode;
    }

    public void setSellerSkuCode(String sellerSkuCode) {
        this.sellerSkuCode = sellerSkuCode;
    }

    public String getSkuCode() {
        return skuCode;
    }

    public void setSkuCode(String skuCode) {
        this.skuCode = skuCode;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductUrl() {
        return productUrl;
    }

    public void setProductUrl(String productUrl) {
        this.productUrl = productUrl;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public Set<String> getImageUrls() {
        return imageUrls;
    }

    public void setImageUrls(Set<String> imageUrls) {
        this.imageUrls = imageUrls;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public ChannelItemType.Status getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(ChannelItemType.Status statusCode) {
        this.statusCode = statusCode;
    }

    public ListingStatus getListingStatus() {
        return listingStatus;
    }

    public void setListingStatus(ListingStatus listingStatus) {
        this.listingStatus = listingStatus;
    }

    public boolean isVerified() {
        return verified;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }

    public List<ChannelItemTypeVO.Attribute> getAttributes() {
        return attributes;
    }

    public void setAttributes(List<ChannelItemTypeVO.Attribute> attributes) {
        this.attributes = attributes;
    }

}
