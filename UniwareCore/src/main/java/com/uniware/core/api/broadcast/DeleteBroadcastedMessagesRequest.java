/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 13-Feb-2014
 *  @author akshay
 */
package com.uniware.core.api.broadcast;

import java.util.List;

import org.hibernate.validator.constraints.NotEmpty;

import com.unifier.core.api.base.ServiceRequest;

public class DeleteBroadcastedMessagesRequest extends ServiceRequest{

    /**
     * 
     */
    private static final long serialVersionUID = 605027573008224001L;

    @NotEmpty
    private List<String>      messages;

    public List<String> getMessages() {
        return messages;
    }

    public void setMessages(List<String> messages) {
        this.messages = messages;
    }

}
