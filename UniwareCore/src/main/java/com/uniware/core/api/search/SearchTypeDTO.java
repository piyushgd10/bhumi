/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 18-Dec-2014
 *  @author unicommerce
 */
package com.uniware.core.api.search;

import com.unifier.core.entity.SearchTypes;

import java.io.Serializable;

public class SearchTypeDTO implements Serializable {

    private String  name;
    private String  code;
    private String  accessResourceName;
    private String  landingPageUrl;
    private String  placeholderText;
    private boolean allFacilityType;
    private boolean hidden;
    
    public SearchTypeDTO(){   
    }
    
    public SearchTypeDTO(SearchTypes searchType){
        this.setName(searchType.getName());
        this.setCode(searchType.getType());
        this.setPlaceholderText(searchType.getPlaceHolderText());
        this.setAccessResourceName(searchType.getAccessResource().getName());
        this.setLandingPageUrl(searchType.getLandingPageUrl());
        this.setAllFacilityType(searchType.isAllFacilityType());
        this.setHidden(searchType.isHidden());
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getAccessResourceName() {
        return accessResourceName;
    }

    public void setAccessResourceName(String accessResourceName) {
        this.accessResourceName = accessResourceName;
    }

    public String getLandingPageUrl() {
        return landingPageUrl;
    }

    public void setLandingPageUrl(String landingPageUrl) {
        this.landingPageUrl = landingPageUrl;
    }

    public String getPlaceholderText() {
        return placeholderText;
    }

    public void setPlaceholderText(String placeholderText) {
        this.placeholderText = placeholderText;
    }

    public boolean isAllFacilityType() {
        return allFacilityType;
    }

    public void setAllFacilityType(boolean allFacilityType) {
        this.allFacilityType = allFacilityType;
    }

    public boolean isHidden() {
        return hidden;
    }

    public void setHidden(boolean hidden) {
        this.hidden = hidden;
    }
}
