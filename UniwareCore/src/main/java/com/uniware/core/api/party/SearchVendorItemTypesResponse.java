/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 23, 2012
 *  @author praveeng
 */
package com.uniware.core.api.party;

import com.unifier.core.api.base.ServiceResponse;

import java.util.ArrayList;
import java.util.List;

import com.uniware.core.api.party.dto.VendorItemTypeDTO;

/**
 * @author praveeng
 */
public class SearchVendorItemTypesResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long       serialVersionUID = -1805589637713989105L;
    private List<VendorItemTypeDTO> elements         = new ArrayList<VendorItemTypeDTO>();
    private Long                    totalRecords;

    /**
     * @return the totalRecords
     */
    public Long getTotalRecords() {
        return totalRecords;
    }

    /**
     * @param totalRecords the totalRecords to set
     */
    public void setTotalRecords(Long totalRecords) {
        this.totalRecords = totalRecords;
    }

    /**
     * @return the elements
     */
    public List<VendorItemTypeDTO> getElements() {
        return elements;
    }

    /**
     * @param elements the elements to set
     */
    public void setElements(List<VendorItemTypeDTO> elements) {
        this.elements = elements;
    }

}
