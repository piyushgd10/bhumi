package com.uniware.core.api.cyclecount;

import com.unifier.core.api.base.ServiceResponse;

/**
 * Created by harshpal on 14/04/16.
 */
public class AddShelfToSubCycleCountResponse extends ServiceResponse {

    private String zoneName;
    private int    pendingPutawaysCount;

    public String getZoneName() {
        return zoneName;
    }

    public void setZoneName(String zoneName) {
        this.zoneName = zoneName;
    }

    public int getPendingPutawaysCount() {
        return pendingPutawaysCount;
    }

    public void setPendingPutawaysCount(int pendingPutawaysCount) {
        this.pendingPutawaysCount = pendingPutawaysCount;
    }
    private int    pickupPendingCount;

    public int getPickupPendingCount() {
        return pickupPendingCount;
    }

    public void setPickupPendingCount(int pickupPendingCount) {
        this.pickupPendingCount = pickupPendingCount;
    }

}
