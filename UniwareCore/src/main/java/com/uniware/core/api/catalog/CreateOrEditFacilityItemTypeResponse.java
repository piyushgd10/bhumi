/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Nov 16, 2012
 *  @author singla
 */
package com.uniware.core.api.catalog;

import com.unifier.core.api.base.ServiceResponse;

public class CreateOrEditFacilityItemTypeResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 2667021468256228069L;

}
