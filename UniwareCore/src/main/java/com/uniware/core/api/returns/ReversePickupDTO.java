/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 18, 2012
 *  @author praveeng
 */
package com.uniware.core.api.returns;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.unifier.core.api.customfields.CustomFieldMetadataDTO;
import com.uniware.core.api.packer.SaleOrderItemDTO;

/**
 * @author praveeng
 */
public class ReversePickupDTO {

    private Integer                        id;
    private String                         code;
    private String                         statusCode;
    private String                         replacementSaleOrderCode;
    private String                         replacementSaleOrderStatus;
    private String                         shippingProvider;
    private String                         trackingNumber;
    private String                         saleOrderCode;
    private String                         saleOrderStatus;
    private String                         actionCode;
    private Date                           created;
    private String                         trackingStatus;
    private String                         providerStatus;
    private String                         redispatchShippingProvider;
    private String                         redispatchTrackingNumber;
    private String                         redispatchReason;
    private String                         redispatchManifestCode;
    private String                         reason;
    private List<SaleOrderItemDTO>         saleOrderItems         = new ArrayList<SaleOrderItemDTO>();
    private List<ReplacedSaleOrderItemDTO> replacedSaleOrderItems = new ArrayList<ReplacedSaleOrderItemDTO>();
    private List<CustomFieldMetadataDTO> customFieldValues;

    public static class ReplacedSaleOrderItemDTO {
        private int    saleOrderItemId;
        private String itemSku;
        private String itemName;
        private String saleOrderItemCode;

        /**
         * @return the saleOrderItemId
         */

        public int getSaleOrderItemId() {
            return saleOrderItemId;
        }

        /**
         * @param saleOrderItemId the saleOrderItemId to set
         */
        public void setSaleOrderItemId(int saleOrderItemId) {
            this.saleOrderItemId = saleOrderItemId;
        }

        /**
         * @return the itemSku
         */
        public String getItemSku() {
            return itemSku;
        }

        /**
         * @param itemSku the itemSku to set
         */
        public void setItemSku(String itemSku) {
            this.itemSku = itemSku;
        }

        /**
         * @return the itemName
         */
        public String getItemName() {
            return itemName;
        }

        /**
         * @param itemName the itemName to set
         */
        public void setItemName(String itemName) {
            this.itemName = itemName;
        }

        /**
         * @return the saleOrderItemCode
         */
        public String getSaleOrderItemCode() {
            return saleOrderItemCode;
        }

        /**
         * @param saleOrderItemCode the saleOrderItemCode to set
         */
        public void setSaleOrderItemCode(String saleOrderItemCode) {
            this.saleOrderItemCode = saleOrderItemCode;
        }
    }

    /**
     * @return the statusCode
     */
    public String getStatusCode() {
        return statusCode;
    }

    /**
     * @param statusCode the statusCode to set
     */
    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    /**
     * @return the replacementSaleOrderCode
     */
    public String getReplacementSaleOrderCode() {
        return replacementSaleOrderCode;
    }

    /**
     * @param replacementSaleOrderCode the replacementSaleOrderCode to set
     */
    public void setReplacementSaleOrderCode(String replacementSaleOrderCode) {
        this.replacementSaleOrderCode = replacementSaleOrderCode;
    }

    /**
     * @return the saleOrderCode
     */
    public String getSaleOrderCode() {
        return saleOrderCode;
    }

    /**
     * @param saleOrderCode the saleOrderCode to set
     */
    public void setSaleOrderCode(String saleOrderCode) {
        this.saleOrderCode = saleOrderCode;
    }

    /**
     * @return the actionCode
     */
    public String getActionCode() {
        return actionCode;
    }

    /**
     * @param actionCode the actionCode to set
     */
    public void setActionCode(String actionCode) {
        this.actionCode = actionCode;
    }

    /**
     * @return the created
     */
    public Date getCreated() {
        return created;
    }

    /**
     * @param created the created to set
     */
    public void setCreated(Date created) {
        this.created = created;
    }

    /**
     * @return the trackingStatus
     */
    public String getTrackingStatus() {
        return trackingStatus;
    }

    /**
     * @param trackingStatus the trackingStatus to set
     */
    public void setTrackingStatus(String trackingStatus) {
        this.trackingStatus = trackingStatus;
    }

    /**
     * @return the providerStatus
     */
    public String getProviderStatus() {
        return providerStatus;
    }

    /**
     * @param providerStatus the providerStatus to set
     */
    public void setProviderStatus(String providerStatus) {
        this.providerStatus = providerStatus;
    }

    /**
     * @return the saleOrderItems
     */
    public List<SaleOrderItemDTO> getSaleOrderItems() {
        return saleOrderItems;
    }

    /**
     * @param saleOrderItems the saleOrderItems to set
     */
    public void setSaleOrderItems(List<SaleOrderItemDTO> saleOrderItems) {
        this.saleOrderItems = saleOrderItems;
    }

    /**
     * @return the saleOrderStatus
     */
    public String getSaleOrderStatus() {
        return saleOrderStatus;
    }

    /**
     * @param saleOrderStatus the saleOrderStatus to set
     */
    public void setSaleOrderStatus(String saleOrderStatus) {
        this.saleOrderStatus = saleOrderStatus;
    }

    /**
     * @return the replacementSaleOrderStatus
     */
    public String getReplacementSaleOrderStatus() {
        return replacementSaleOrderStatus;
    }

    /**
     * @param replacementSaleOrderStatus the replacementSaleOrderStatus to set
     */
    public void setReplacementSaleOrderStatus(String replacementSaleOrderStatus) {
        this.replacementSaleOrderStatus = replacementSaleOrderStatus;
    }

    /**
     * @return the shippingProvider
     */
    public String getShippingProvider() {
        return shippingProvider;
    }

    /**
     * @param shippingProvider the shippingProvider to set
     */
    public void setShippingProvider(String shippingProvider) {
        this.shippingProvider = shippingProvider;
    }

    /**
     * @return the trackingNumber
     */
    public String getTrackingNumber() {
        return trackingNumber;
    }

    /**
     * @param trackingNumber the trackingNumber to set
     */
    public void setTrackingNumber(String trackingNumber) {
        this.trackingNumber = trackingNumber;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the redispatchShippingProvider
     */
    public String getRedispatchShippingProvider() {
        return redispatchShippingProvider;
    }

    /**
     * @param redispatchShippingProvider the redispatchShippingProvider to set
     */
    public void setRedispatchShippingProvider(String redispatchShippingProvider) {
        this.redispatchShippingProvider = redispatchShippingProvider;
    }

    /**
     * @return the redispatchTrackingNumber
     */
    public String getRedispatchTrackingNumber() {
        return redispatchTrackingNumber;
    }

    /**
     * @param redispatchTrackingNumber the redispatchTrackingNumber to set
     */
    public void setRedispatchTrackingNumber(String redispatchTrackingNumber) {
        this.redispatchTrackingNumber = redispatchTrackingNumber;
    }

    /**
     * @return the redispatchReason
     */
    public String getRedispatchReason() {
        return redispatchReason;
    }

    /**
     * @param redispatchReason the redispatchReason to set
     */
    public void setRedispatchReason(String redispatchReason) {
        this.redispatchReason = redispatchReason;
    }

    /**
     * @return the redispatchManifestCode
     */
    public String getRedispatchManifestCode() {
        return redispatchManifestCode;
    }

    /**
     * @param redispatchManifestCode the redispatchManifestCode to set
     */
    public void setRedispatchManifestCode(String redispatchManifestCode) {
        this.redispatchManifestCode = redispatchManifestCode;
    }

    /**
     * @return the reason
     */
    public String getReason() {
        return reason;
    }

    /**
     * @param reason the reason to set
     */
    public void setReason(String reason) {
        this.reason = reason;
    }

    /**
     * @return the replacedSaleOrderItems
     */
    public List<ReplacedSaleOrderItemDTO> getReplacedSaleOrderItems() {
        return replacedSaleOrderItems;
    }

    /**
     * @param replacedSaleOrderItems the replacedSaleOrderItems to set
     */
    public void setReplacedSaleOrderItems(List<ReplacedSaleOrderItemDTO> replacedSaleOrderItems) {
        this.replacedSaleOrderItems = replacedSaleOrderItems;
    }

    public List<CustomFieldMetadataDTO> getCustomFieldValues() {
        return customFieldValues;
    }

    public void setCustomFieldValues(List<CustomFieldMetadataDTO> customFieldValues) {
        this.customFieldValues = customFieldValues;
    }
}
