/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jun 20, 2012
 *  @author singla
 */
package com.uniware.core.api.channel;

import java.util.ArrayList;
import java.util.List;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author singla
 */
public class SyncChannelOrdersResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long         serialVersionUID = 8034049854364994904L;

    List<ChannelOrderSyncResponseDTO> resultItems      = new ArrayList<>();

    public static class ChannelOrderSyncResponseDTO {
        private String channelCode;
        private String message;

        public ChannelOrderSyncResponseDTO() {

        }

        public ChannelOrderSyncResponseDTO(String channelCode, String message) {
            this.channelCode = channelCode;
            this.message = message;
        }

        public String getChannelCode() {
            return channelCode;
        }

        public void setChannelCode(String channelCode) {
            this.channelCode = channelCode;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

    }

    public List<ChannelOrderSyncResponseDTO> getResultItems() {
        return resultItems;
    }

    public void setResultItems(List<ChannelOrderSyncResponseDTO> resultItems) {
        this.resultItems = resultItems;
    }

}
