/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jun 20, 2012
 *  @author ankit
 */
package com.uniware.core.api.bundle;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author Sunny
 */
public class CreateBundleResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 8176212261586557873L;

    private BundleDTO         bundleDTO;

    public BundleDTO getBundleDTO() {
        return bundleDTO;
    }

    public void setBundleDTO(BundleDTO bundleDTO) {
        this.bundleDTO = bundleDTO;
    }

    @Override
    public String toString() {
        return "CreateBundleResponse [bundleDTO=" + bundleDTO + "]";
    }

}
