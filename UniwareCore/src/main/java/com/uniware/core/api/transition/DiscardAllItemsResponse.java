/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 02-Jun-2012
 *  @author praveeng
 */
package com.uniware.core.api.transition;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author praveeng
 */
public class DiscardAllItemsResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -8738505572908381084L;

}
