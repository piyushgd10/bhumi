/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 23, 2012
 *  @author praveeng
 */
package com.uniware.core.api.party;

import java.util.ArrayList;
import java.util.List;

import com.unifier.core.api.base.ServiceResponse;
import com.uniware.core.api.party.dto.VendorItemTypeDTO;

/**
 * @author Sunny
 */
public class GetVendorItemTypesResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long       serialVersionUID = -5794216590664362166L;

    private List<VendorItemTypeDTO> vendorItemTypes  = new ArrayList<VendorItemTypeDTO>();
    private int                     pageNumber;
    private int                     pageSize;
    private int                     totalRecords;

    public List<VendorItemTypeDTO> getVendorItemTypes() {
        return vendorItemTypes;
    }

    public void setVendorItemTypes(List<VendorItemTypeDTO> vendorItemTypes) {
        this.vendorItemTypes = vendorItemTypes;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getTotalRecords() {
        return totalRecords;
    }

    public void setTotalRecords(int totalRecords) {
        this.totalRecords = totalRecords;
    }

}
