/*
 * Copyright 2017 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 31/10/17
 * @author piyush
 */
package com.uniware.core.api.picker;

public class ReceiveNonTraceablePicklistItemResponse extends ReceivePickBucketItemResponse {
}
