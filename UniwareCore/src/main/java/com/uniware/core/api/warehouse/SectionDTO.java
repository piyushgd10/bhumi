/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 22-Jan-2012
 *  @author vibhu
 */
package com.uniware.core.api.warehouse;

import com.uniware.core.entity.Section;

/**
 * @author vibhu
 */
public class SectionDTO {
    private int     id;
    private String  code;
    private String  name;
    private boolean enabled;
    private boolean editable;
    private int     picksetId;

    public SectionDTO() {
    }

    public SectionDTO(Section section) {
        this.setId(section.getId());
        this.code = section.getCode();
        this.name = section.getName();
        this.enabled = section.isEnabled();
        this.editable = section.isEditable();
        this.picksetId = section.getPickSet().getId();
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the enabled
     */
    public boolean isEnabled() {
        return enabled;
    }

    /**
     * @param enabled the enabled to set
     */
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the isEditable
     */
    public boolean isEditable() {
        return editable;
    }

    /**
     * @param isEditable the isEditable to set
     */
    public void setEditable(boolean isEditable) {
        this.editable = isEditable;
    }

    /**
     * @return the picksetId
     */
    public int getPicksetId() {
        return picksetId;
    }

    /**
     * @param picksetId the picksetId to set
     */
    public void setPicksetId(int picksetId) {
        this.picksetId = picksetId;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

}