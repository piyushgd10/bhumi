/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Oct 19, 2015
 *  @author bhupi
 */
package com.uniware.core.api.prices;

import com.unifier.core.api.base.ServiceRequest;

public class PullAllPricesFromChannelRequest extends ServiceRequest {

    public enum Filter {
        ALL,
        DIRTY
    }

    private static final long serialVersionUID = -7591725873200786611L;

    private String filter;

    private String channel;

    public String getFilter() {
        return filter;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }

    public String getChannelCode() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }
}
