/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 25-Mar-2014
 *  @author amit
 */
package com.uniware.core.api.print;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotNull;

import com.google.gson.Gson;
import com.unifier.core.api.base.ServiceRequest;

/**
 * @author amit
 */
public class GetPrintableTemplateRequest extends ServiceRequest {

    private static final long serialVersionUID = -631420436168218426L;

    @NotNull
    private List<String>      typeList         = new ArrayList<String>();

    private String            licenseKey;

    public GetPrintableTemplateRequest() {
        super();
    }

    public GetPrintableTemplateRequest(List<String> typeList) {
        this.typeList = typeList;
    }

    public List<String> getTypeList() {
        return typeList;
    }

    public void setTypeList(List<String> typeList) {
        this.typeList = typeList;
    }

    public String getLicenseKey() {
        return licenseKey;
    }

    public void setLicenseKey(String licenseKey) {
        this.licenseKey = licenseKey;
    }

    public static void main(String[] args) {
        GetPrintableTemplateRequest r = new GetPrintableTemplateRequest();
        r.setLicenseKey("cd11702495e894bc7c163247db4ad440");
        List<String> typeList = new ArrayList<String>();
        typeList.add("ITEM_TYPE_CSV");
        r.setTypeList(typeList);
        System.out.println(new Gson().toJson(r));
    }
}
