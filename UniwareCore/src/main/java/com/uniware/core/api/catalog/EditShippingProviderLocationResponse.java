/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 27, 2012
 *  @author praveeng
 */
package com.uniware.core.api.catalog;

import com.unifier.core.api.base.ServiceResponse;
import com.uniware.core.api.catalog.SearchShippingProviderLocationResponse.ShippingProviderLocationDTO;

/**
 * @author praveeng
 */
public class EditShippingProviderLocationResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 1810374999343453152L;
    
    private ShippingProviderLocationDTO shippingProviderLocation;

    public ShippingProviderLocationDTO getShippingProviderLocation() {
        return shippingProviderLocation;
    }

    public void setShippingProviderLocation(ShippingProviderLocationDTO shippingProviderLocation) {
        this.shippingProviderLocation = shippingProviderLocation;
    }
    
}
