/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 18, 2013
 *  @author praveeng
 */
package com.uniware.core.api.material;

import org.hibernate.validator.constraints.NotBlank;

/**
 * @author praveeng
 */
public class WsInboundGatePass {

    private String  code;
    private String  partyCode;

    @NotBlank
    private String  materialDescription;

    private Integer noOfParcel;
    private String  transporter;
    private String  trackingNumber;
    private String  referenceNumber;
    private String  vehicleNumber;
    private String  remarks;

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the partyCode
     */
    public String getPartyCode() {
        return partyCode;
    }

    /**
     * @param partyCode the partyCode to set
     */
    public void setPartyCode(String partyCode) {
        this.partyCode = partyCode;
    }

    /**
     * @return the materialDescription
     */
    public String getMaterialDescription() {
        return materialDescription;
    }

    /**
     * @param materialDescription the materialDescription to set
     */
    public void setMaterialDescription(String materialDescription) {
        this.materialDescription = materialDescription;
    }

    /**
     * @return the noOfParcel
     */
    public Integer getNoOfParcel() {
        return noOfParcel;
    }

    /**
     * @param noOfParcel the noOfParcel to set
     */
    public void setNoOfParcel(Integer noOfParcel) {
        this.noOfParcel = noOfParcel;
    }

    /**
     * @return the transporter
     */
    public String getTransporter() {
        return transporter;
    }

    /**
     * @param transporter the transporter to set
     */
    public void setTransporter(String transporter) {
        this.transporter = transporter;
    }

    /**
     * @return the trackingNumber
     */
    public String getTrackingNumber() {
        return trackingNumber;
    }

    /**
     * @param trackingNumber the trackingNumber to set
     */
    public void setTrackingNumber(String trackingNumber) {
        this.trackingNumber = trackingNumber;
    }

    /**
     * @return the referenceNumber
     */
    public String getReferenceNumber() {
        return referenceNumber;
    }

    /**
     * @param referenceNumber the referenceNumber to set
     */
    public void setReferenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    /**
     * @return the vehicleNumber
     */
    public String getVehicleNumber() {
        return vehicleNumber;
    }

    /**
     * @param vehicleNumber the vehicleNumber to set
     */
    public void setVehicleNumber(String vehicleNumber) {
        this.vehicleNumber = vehicleNumber;
    }

    /**
     * @return the remarks
     */
    public String getRemarks() {
        return remarks;
    }

    /**
     * @param remarks the remarks to set
     */
    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

}
