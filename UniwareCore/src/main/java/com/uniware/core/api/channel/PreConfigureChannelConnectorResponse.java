/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jun 20, 2012
 *  @author singla
 */
package com.uniware.core.api.channel;

import com.unifier.core.api.base.ServiceResponse;
import com.unifier.scraper.redirection.Form;

/**
 * @author Sunny
 */
public class PreConfigureChannelConnectorResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -9018924286025819646L;

    private Form              redirectForm;

    public Form getRedirectForm() {
        return redirectForm;
    }

    public void setRedirectForm(Form redirectForm) {
        this.redirectForm = redirectForm;
    }

}
