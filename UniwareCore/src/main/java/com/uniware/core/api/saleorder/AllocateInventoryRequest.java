package com.uniware.core.api.saleorder;

import com.unifier.core.api.base.ServiceRequest;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Created by akshaykochhar on 30/08/16.
 */
public class AllocateInventoryRequest extends ServiceRequest {

    /**
     *
     */
    private static final long serialVersionUID = 87654567876556722L;

    @NotBlank
    private String           saleOrderCode;

    public String getSaleOrderCode() {
        return saleOrderCode;
    }

    public void setSaleOrderCode(String saleOrderCode) {
        this.saleOrderCode = saleOrderCode;
    }
}
