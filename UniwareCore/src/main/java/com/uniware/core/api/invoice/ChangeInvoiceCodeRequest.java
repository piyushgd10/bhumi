package com.uniware.core.api.invoice;

import com.unifier.core.api.base.ServiceRequest;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import java.util.List;

/**
 * Created by piyush on 9/17/15.
 */
public class ChangeInvoiceCodeRequest extends ServiceRequest{

    @NotBlank
    private String saleOrderCode;

    @NotBlank
    private String invoiceCode;

    @NotEmpty
    private List<String> saleOrderItemCodes;

    public String getSaleOrderCode() {
        return saleOrderCode;
    }

    public void setSaleOrderCode(String saleOrderCode) {
        this.saleOrderCode = saleOrderCode;
    }

    public String getInvoiceCode() {
        return invoiceCode;
    }

    public void setInvoiceCode(String invoiceCode) {
        this.invoiceCode = invoiceCode;
    }

    public List<String> getSaleOrderItemCodes() {
        return saleOrderItemCodes;
    }

    public void setSaleOrderItemCodes(List<String> saleOrderItemCodes) {
        this.saleOrderItemCodes = saleOrderItemCodes;
    }
}
