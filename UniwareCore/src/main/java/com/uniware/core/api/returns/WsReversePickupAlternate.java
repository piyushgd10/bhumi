/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Oct 12, 2012
 *  @author singla
 */
package com.uniware.core.api.returns;

import java.math.BigDecimal;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author singla
 */
public class WsReversePickupAlternate {
    @NotEmpty
    private String     itemSku;

    @NotNull
    private BigDecimal totalPrice;

    @NotNull
    private BigDecimal sellingPrice;
    private BigDecimal discount        = BigDecimal.ZERO;
    private BigDecimal shippingCharges = BigDecimal.ZERO;
    private BigDecimal prepaidAmount   = BigDecimal.ZERO;

    /**
     * @return the itemSku
     */
    public String getItemSku() {
        return itemSku;
    }

    /**
     * @param itemSku the itemSku to set
     */
    public void setItemSku(String itemSku) {
        this.itemSku = itemSku;
    }

    /**
     * @return the totalPrice
     */
    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    /**
     * @param totalPrice the totalPrice to set
     */
    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    /**
     * @return the sellingPrice
     */
    public BigDecimal getSellingPrice() {
        return sellingPrice;
    }

    /**
     * @param sellingPrice the sellingPrice to set
     */
    public void setSellingPrice(BigDecimal sellingPrice) {
        this.sellingPrice = sellingPrice;
    }

    /**
     * @return the discount
     */
    public BigDecimal getDiscount() {
        return discount;
    }

    /**
     * @param discount the discount to set
     */
    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    /**
     * @return the shippingCharges
     */
    public BigDecimal getShippingCharges() {
        return shippingCharges;
    }

    /**
     * @param shippingCharges the shippingCharges to set
     */
    public void setShippingCharges(BigDecimal shippingCharges) {
        this.shippingCharges = shippingCharges;
    }

    /**
     * @return the prepaidAmount
     */
    public BigDecimal getPrepaidAmount() {
        return prepaidAmount;
    }

    /**
     * @param prepaidAmount the prepaidAmount to set
     */
    public void setPrepaidAmount(BigDecimal prepaidAmount) {
        this.prepaidAmount = prepaidAmount;
    }
}
