/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 02-Jul-2012
 *  @author vibhu
 */
package com.uniware.core.api.material;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author vibhu
 */
public class GetGatePassResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 3567896610659328036L;

    private GatePassDTO       gatePassDTO;

    /**
     * @return the gatePassDTO
     */
    public GatePassDTO getGatePassDTO() {
        return gatePassDTO;
    }

    /**
     * @param gatePassDTO the gatePassDTO to set
     */
    public void setGatePassDTO(GatePassDTO gatePassDTO) {
        this.gatePassDTO = gatePassDTO;
    }

}