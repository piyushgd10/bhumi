/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 03-Sep-2012
 *  @author praveeng
 */
package com.uniware.core.api.material;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author praveeng
 */
public class DiscardGatePassResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 8645758604485555931L;

}
