/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 22-Jan-2012
 *  @author vibhu
 */
package com.uniware.core.api.warehouse;

import com.uniware.core.entity.ShelfType;

/**
 * @author vibhu
 */
public class ShelfTypeDTO {
    private String  code;
    private int     length;
    private int     width;
    private int     height;
    private boolean enabled;
    private boolean editable;

    public ShelfTypeDTO() {
    }

    public ShelfTypeDTO(ShelfType shelfType) {
        this.code = shelfType.getCode();
        this.length = shelfType.getLength();
        this.width = shelfType.getWidth();
        this.height = shelfType.getHeight();
        this.enabled = shelfType.isEnabled();
        this.editable = shelfType.isEditable();
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the enabled
     */
    public boolean isEnabled() {
        return enabled;
    }

    /**
     * @param enabled the enabled to set
     */
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    /**
     * @return the length
     */
    public int getLength() {
        return length;
    }

    /**
     * @param length the length to set
     */
    public void setLength(int length) {
        this.length = length;
    }

    /**
     * @return the width
     */
    public int getWidth() {
        return width;
    }

    /**
     * @param width the width to set
     */
    public void setWidth(int width) {
        this.width = width;
    }

    /**
     * @return the height
     */
    public int getHeight() {
        return height;
    }

    /**
     * @param height the height to set
     */
    public void setHeight(int height) {
        this.height = height;
    }

    /**
     * @return the editable
     */
    public boolean isEditable() {
        return editable;
    }

    /**
     * @param editable the editable to set
     */
    public void setEditable(boolean editable) {
        this.editable = editable;
    }
}