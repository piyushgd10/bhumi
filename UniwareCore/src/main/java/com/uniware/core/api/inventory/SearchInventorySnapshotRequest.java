/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jan 17, 2013
 *  @author Pankaj
 */
package com.uniware.core.api.inventory;

import com.unifier.core.api.base.ServiceRequest;
import com.unifier.core.pagination.SearchOptions;

/**
 * @author Pankaj
 */
public class SearchInventorySnapshotRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 6232300199073681097L;

    private String            itemTypeKeyword;
    private String            category;
    private SearchOptions     searchOptions;
    private boolean           enabled;

    /**
     * @return the enabled
     */
    public boolean isEnabled() {
        return enabled;
    }

    /**
     * @param enabled the enabled to set
     */
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    /**
     * @return the searchOptions
     */
    public SearchOptions getSearchOptions() {
        return searchOptions;
    }

    /**
     * @param searchOptions the searchOptions to set
     */
    public void setSearchOptions(SearchOptions searchOptions) {
        this.searchOptions = searchOptions;
    }

    /**
     * @return the itemTypeKeyword
     */
    public String getItemTypeKeyword() {
        return itemTypeKeyword;
    }

    /**
     * @param itemTypeKeyword the itemTypeKeyword to set
     */
    public void setItemTypeKeyword(String itemTypeKeyword) {
        this.itemTypeKeyword = itemTypeKeyword;
    }

    /**
     * @return the category
     */
    public String getCategory() {
        return category;
    }

    /**
     * @param category the category to set
     */
    public void setCategory(String category) {
        this.category = category;
    }

}
