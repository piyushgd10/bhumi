/*
 * Copyright 2017 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 8/8/17 4:40 PM
 * @author digvijaysharma
 */

package com.uniware.core.api.picker;

import java.util.List;
import java.util.Set;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.unifier.core.api.base.ServiceRequest;

/**
 * Created by bhuvneshwarkumar on 01/09/16.
 */
public class SubmitPickBucketRequest extends ServiceRequest {

    private static final long serialVersionUID = 1437241677667424403L;

    @NotEmpty
    private String            pickBucketCode;

    private Set<String>       itemCodes;

    private List<PickedItem>  pickedItems;

    @NotNull
    private Integer           userId;

    public String getPickBucketCode() {
        return pickBucketCode;
    }

    public void setPickBucketCode(String pickBucketCode) {
        this.pickBucketCode = pickBucketCode;
    }

    public Set<String> getItemCodes() {
        return itemCodes;
    }

    public void setItemCodes(Set<String> itemCodes) {
        this.itemCodes = itemCodes;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public List<PickedItem> getPickedItems() {
        return pickedItems;
    }

    public void setPickedItems(List<PickedItem> pickedItems) {
        this.pickedItems = pickedItems;
    }

    public static class PickedItem {

        private String  skuCode;

        private Integer quantity;

        public String getSkuCode() {
            return skuCode;
        }

        public void setSkuCode(String skuCode) {
            this.skuCode = skuCode;
        }

        public Integer getQuantity() {
            return quantity;
        }

        public void setQuantity(Integer quantity) {
            this.quantity = quantity;
        }
    }
}
