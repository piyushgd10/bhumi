/*
 * Copyright 2017 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 31/05/17
 * @author piyush
 */
package com.uniware.core.api.invoice;

import com.unifier.core.api.base.ServiceRequest;
import com.uniware.core.api.model.WsInvoice;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public class GenerateOrUpdateInvoiceRequest extends ServiceRequest {

    /**
     *
     */
    private static final long       serialVersionUID      = 9134119935562379672L;

    @NotNull
    @Valid
    private WsInvoice invoice;

    public WsInvoice getInvoice() {
        return invoice;
    }

    public void setInvoice(WsInvoice invoice) {
        this.invoice = invoice;
    }
}
