/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 25-Dec-2013
 *  @author sunny
 */
package com.uniware.core.api.systemnotification.sandbox;

import java.util.HashSet;
import java.util.Set;

import com.uniware.core.entity.Invoice;
import com.uniware.core.entity.InvoiceItem;

public class SandboxInvoiceVO {

    private String                    code;
    private Set<SandboxInvoiceItemVO> invoiceItems = new HashSet<SandboxInvoiceItemVO>();

    public SandboxInvoiceVO() {
    }

    public SandboxInvoiceVO(Invoice invoice) {
        code = invoice.getCode();
        invoiceItems = new HashSet<>(invoice.getInvoiceItems().size());
        for (InvoiceItem invi : invoice.getInvoiceItems()) {
            invoiceItems.add(new SandboxInvoiceItemVO(invi));
        }
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Set<SandboxInvoiceItemVO> getInvoiceItems() {
        return invoiceItems;
    }

    public void setInvoiceItems(Set<SandboxInvoiceItemVO> invoiceItems) {
        this.invoiceItems = invoiceItems;
    }

}
