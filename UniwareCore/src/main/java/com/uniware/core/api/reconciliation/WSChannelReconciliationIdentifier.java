/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 03-Nov-2014
 *  @author parijat
 */
package com.uniware.core.api.reconciliation;

import org.hibernate.validator.constraints.NotBlank;

public class WSChannelReconciliationIdentifier {

    @NotBlank
    private String channelCode;

    @NotBlank
    private String reconciliationIdentifier;

    private String comments;

    /**
     * @return the channelCode
     */
    public String getChannelCode() {
        return channelCode;
    }

    /**
     * @param channelCode the channelCode to set
     */
    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    /**
     * @return the reconciliationIdentifier
     */
    public String getReconciliationIdentifier() {
        return reconciliationIdentifier;
    }

    /**
     * @param reconciliationIdentifier the reconciliationIdentifier to set
     */
    public void setReconciliationIdentifier(String reconciliationIdentifier) {
        this.reconciliationIdentifier = reconciliationIdentifier;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }
}
