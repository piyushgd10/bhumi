/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 20-Aug-2014
 *  @author Amit
 */
package com.uniware.core.api.packer;

import org.hibernate.validator.constraints.NotEmpty;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author Amit
 */
public class PrintPicklistItemRequest extends ServiceRequest {

    private static final long     serialVersionUID = 9035884051671914779L;

    @NotEmpty
    private String saleOrderCode;

    @NotEmpty
    private String saleOrderItemCode;

    public PrintPicklistItemRequest() {
        super();
    }

    public String getSaleOrderCode() {
        return saleOrderCode;
    }

    public void setSaleOrderCode(String saleOrderCode) {
        this.saleOrderCode = saleOrderCode;
    }

    public String getSaleOrderItemCode() {
        return saleOrderItemCode;
    }

    public void setSaleOrderItemCode(String saleOrderItemCode) {
        this.saleOrderItemCode = saleOrderItemCode;
    }

    @Override
    public String toString() {
        return "PrintPicklistItemRequest{" + "saleOrderCode='" + saleOrderCode + '\'' + ", saleOrderItemCode='" + saleOrderItemCode + '\'' + '}';
    }
}
