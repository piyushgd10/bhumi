/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 22-Mar-2014
 *  @author karunsingla
 */
package com.uniware.core.api.inflow;

import com.unifier.core.api.base.ServiceResponse;

public class AddInflowReceiptToVendorInvoiceResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -6056414591507056254L;

}
