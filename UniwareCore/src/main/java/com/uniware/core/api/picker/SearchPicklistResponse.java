/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Apr 4, 2012
 *  @author praveeng
 */
package com.uniware.core.api.picker;

import com.unifier.core.api.base.ServiceResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * @author praveeng
 */
public class SearchPicklistResponse extends ServiceResponse {
    /**
     * 
     */
    private static final long serialVersionUID = 3928235967429015413L;
    private List<PicklistDTO> elements         = new ArrayList<PicklistDTO>();
    private Long              totalRecords;

    /**
     * @return the elements
     */
    public List<PicklistDTO> getElements() {
        return elements;
    }

    /**
     * @param elements the elements to set
     */
    public void setElements(List<PicklistDTO> elements) {
        this.elements = elements;
    }

    /**
     * @return the totalRecords
     */
    public Long getTotalRecords() {
        return totalRecords;
    }

    /**
     * @param totalRecords the totalRecords to set
     */
    public void setTotalRecords(Long totalRecords) {
        this.totalRecords = totalRecords;
    }

}
