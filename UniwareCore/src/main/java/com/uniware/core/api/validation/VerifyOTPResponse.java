package com.uniware.core.api.validation;

import com.unifier.core.api.base.ServiceResponse;

/**
 * Created by admin on 10/6/15.
 */
public class VerifyOTPResponse extends ServiceResponse {

    private static final long serialVersionUID = 7151039368033818972L;

    private boolean verified;

    public boolean isVerified() {
        return verified;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }
}
