/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 26-Jul-2013
 *  @author sunny
 */
package com.uniware.core.api.bundle;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

public class WsBundle {

    @NotNull
    private String                    itemSku;

    @Valid
    @NotEmpty
    private List<WsComponentItemType> wsComponentItemTypes;

    public WsBundle() {
    }

    public WsBundle(String itemSku) {
        this.itemSku = itemSku;
    }

    public WsBundle(String itemSku, List<WsComponentItemType> wsComponentItemTypes) {
        super();
        this.itemSku = itemSku;
        this.wsComponentItemTypes = wsComponentItemTypes;
    }

    public String getItemSku() {
        return itemSku;
    }

    public void setItemSku(String itemSku) {
        this.itemSku = itemSku;
    }

    public List<WsComponentItemType> getWsComponentItemTypes() {
        return wsComponentItemTypes;
    }

    public void setWsComponentItemTypes(List<WsComponentItemType> wsComponentItemTypes) {
        this.wsComponentItemTypes = wsComponentItemTypes;
    }

}
