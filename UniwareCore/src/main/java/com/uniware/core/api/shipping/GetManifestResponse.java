/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jun 21, 2015
 *  @author akshay
 */
package com.uniware.core.api.shipping;

import com.unifier.core.api.base.ServiceResponse;

public class GetManifestResponse extends ServiceResponse {
    /**
     * 
     */
    private static final long serialVersionUID = -5587112979406280725L;

    private ManifestDTO       shippingManifest;

    /**
     * @return the shippingManifest
     */
    public ManifestDTO getShippingManifest() {
        return shippingManifest;
    }

    /**
     * @param shippingManifest the shippingManifest to set
     */
    public void setShippingManifest(ManifestDTO shippingManifest) {
        this.shippingManifest = shippingManifest;
    }
    
}
