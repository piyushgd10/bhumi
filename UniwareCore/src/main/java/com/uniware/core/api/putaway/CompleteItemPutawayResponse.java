/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jun 2, 2012
 *  @author singla
 */
package com.uniware.core.api.putaway;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author singla
 */
public class CompleteItemPutawayResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -1764019521539759969L;

}
