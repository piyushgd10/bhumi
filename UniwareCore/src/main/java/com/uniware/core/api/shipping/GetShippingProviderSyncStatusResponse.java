/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 25-Feb-2015
 *  @author parijat
 */
package com.uniware.core.api.shipping;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author parijat
 *
 */
public class GetShippingProviderSyncStatusResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 6586714058617986556L;

    private ShipmentTrackingSyncStatusDTO statusDTO;

    /**
     * @return the statusDTO
     */
    public ShipmentTrackingSyncStatusDTO getStatusDTO() {
        return statusDTO;
    }

    /**
     * @param statusDTO the statusDTO to set
     */
    public void setStatusDTO(ShipmentTrackingSyncStatusDTO statusDTO) {
        this.statusDTO = statusDTO;
    }

}
