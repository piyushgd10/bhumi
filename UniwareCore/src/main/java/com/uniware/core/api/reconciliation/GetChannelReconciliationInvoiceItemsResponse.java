/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 27-Mar-2014
 *  @author parijat
 */
package com.uniware.core.api.reconciliation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.unifier.core.api.base.ServiceResponse;
import com.uniware.core.entity.SaleOrderItem;
import com.uniware.core.vo.ChannelReconciliationInvoiceItem;

/**
 * @author parijat
 */
public class GetChannelReconciliationInvoiceItemsResponse extends ServiceResponse {

    private static final long                         serialVersionUID = 6173770281702816939L;

    private List<ChannelReconciliationInvoiceItemDTO> invoiceItems     = new ArrayList<>();

    public List<ChannelReconciliationInvoiceItemDTO> getInvoiceItems() {
        return invoiceItems;
    }

    public void setInvoiceItems(List<ChannelReconciliationInvoiceItemDTO> invoiceItems) {
        this.invoiceItems = invoiceItems;
    }

    public static class ChannelReconciliationInvoiceItemDTO {
        private String     invoiceCode;
        private String     reconciliationIdentifier;
        private String     channelCode;
        private String     tenantCode;
        private String     channelSaleOrderCode;
        private Date       orderDate;
        private String     orderItemStatus;
        private String     paymentComments;
        private String     channelProductName;
        private String     additionalInfo;
        private double     orderItemValue          = 0;
        private double     settledValue            = 0;
        private Date       depositDate;
        private String     statusCode              = ChannelReconciliationInvoiceItem.InvoiceItemProcessedStatus.UNPROCESSED.name();
        private int        quantity;
        private BigDecimal totalRewards            = BigDecimal.ZERO;
        private BigDecimal totalDiscounts          = BigDecimal.ZERO;
        private BigDecimal totalOtherIncentive     = BigDecimal.ZERO;
        private BigDecimal totalSellerEarning      = BigDecimal.ZERO;
        private BigDecimal totalCommission         = BigDecimal.ZERO;
        private BigDecimal totalFixedFee           = BigDecimal.ZERO;
        private BigDecimal totalShippingCharge     = BigDecimal.ZERO;
        private BigDecimal totalShippingFee        = BigDecimal.ZERO;
        private BigDecimal totalReverseShippingFee = BigDecimal.ZERO;
        private BigDecimal totalAdditionalFee      = BigDecimal.ZERO;
        private BigDecimal totalChannelPenalty     = BigDecimal.ZERO;
        private double     totalChannelRecovery    = 0;
        private BigDecimal totalTax                = BigDecimal.ZERO;
        private String     additionalFeeDescription;
        private Date       created;

        public ChannelReconciliationInvoiceItemDTO() {
        }

        public ChannelReconciliationInvoiceItemDTO(ChannelReconciliationInvoiceItem channelReconciliationInvoiceItem, SaleOrderItem saleOrderItem) {
            this.invoiceCode = channelReconciliationInvoiceItem.getInvoiceCode();
            this.reconciliationIdentifier = channelReconciliationInvoiceItem.getReconciliationIdentifier();
            this.channelCode = channelReconciliationInvoiceItem.getChannelCode();
            this.tenantCode = channelReconciliationInvoiceItem.getTenantCode();
            this.channelSaleOrderCode = channelReconciliationInvoiceItem.getChannelSaleOrderCode();
            this.orderDate = channelReconciliationInvoiceItem.getOrderDate();
            this.orderItemStatus = saleOrderItem != null ? saleOrderItem.getStatusCode() : null;
            this.paymentComments = channelReconciliationInvoiceItem.getInvoiceItemOrderStatus();
            this.channelProductName = channelReconciliationInvoiceItem.getChannelProductName();
            this.additionalInfo = channelReconciliationInvoiceItem.getAdditionalInfo();
            this.orderItemValue = channelReconciliationInvoiceItem.getOrderItemValue();
            this.settledValue = channelReconciliationInvoiceItem.getSettledValue();
            this.depositDate = channelReconciliationInvoiceItem.getDepositDate();
            this.statusCode = channelReconciliationInvoiceItem.getStatusCode();
            this.quantity = channelReconciliationInvoiceItem.getQuantity();
            this.totalRewards = channelReconciliationInvoiceItem.getTotalRewards();
            this.totalDiscounts = channelReconciliationInvoiceItem.getTotalDiscounts();
            this.totalOtherIncentive = channelReconciliationInvoiceItem.getTotalOtherIncentive();
            this.totalSellerEarning = channelReconciliationInvoiceItem.getTotalSellerEarning();
            this.totalCommission = channelReconciliationInvoiceItem.getTotalCommission();
            this.totalFixedFee = channelReconciliationInvoiceItem.getTotalFixedFee();
            this.totalShippingCharge = channelReconciliationInvoiceItem.getTotalShippingCharge();
            this.totalShippingFee = channelReconciliationInvoiceItem.getTotalShippingFee();
            this.totalReverseShippingFee = channelReconciliationInvoiceItem.getTotalReverseShippingFee();
            this.totalAdditionalFee = channelReconciliationInvoiceItem.getTotalAdditionalFee();
            this.totalChannelPenalty = channelReconciliationInvoiceItem.getTotalChannelPenalty();
            this.totalChannelRecovery = channelReconciliationInvoiceItem.getTotalChannelRecovery();
            this.totalTax = channelReconciliationInvoiceItem.getTotalTax();
            this.additionalFeeDescription = channelReconciliationInvoiceItem.getAdditionalFeeDescription();
            this.created = channelReconciliationInvoiceItem.getCreated();
        }

        public String getInvoiceCode() {
            return invoiceCode;
        }

        public void setInvoiceCode(String invoiceCode) {
            this.invoiceCode = invoiceCode;
        }

        public String getReconciliationIdentifier() {
            return reconciliationIdentifier;
        }

        public void setReconciliationIdentifier(String reconciliationIdentifier) {
            this.reconciliationIdentifier = reconciliationIdentifier;
        }

        public String getChannelCode() {
            return channelCode;
        }

        public void setChannelCode(String channelCode) {
            this.channelCode = channelCode;
        }

        public String getTenantCode() {
            return tenantCode;
        }

        public void setTenantCode(String tenantCode) {
            this.tenantCode = tenantCode;
        }

        public String getChannelSaleOrderCode() {
            return channelSaleOrderCode;
        }

        public void setChannelSaleOrderCode(String channelSaleOrderCode) {
            this.channelSaleOrderCode = channelSaleOrderCode;
        }

        public Date getOrderDate() {
            return orderDate;
        }

        public void setOrderDate(Date orderDate) {
            this.orderDate = orderDate;
        }

        public String getOrderItemStatus() {
            return orderItemStatus;
        }

        public void setOrderItemStatus(String orderItemStatus) {
            this.orderItemStatus = orderItemStatus;
        }

        public String getPaymentComments() {
            return paymentComments;
        }

        public void setPaymentComments(String paymentComments) {
            this.paymentComments = paymentComments;
        }

        public String getChannelProductName() {
            return channelProductName;
        }

        public void setChannelProductName(String channelProductName) {
            this.channelProductName = channelProductName;
        }

        public String getAdditionalInfo() {
            return additionalInfo;
        }

        public void setAdditionalInfo(String additionalInfo) {
            this.additionalInfo = additionalInfo;
        }

        public double getOrderItemValue() {
            return orderItemValue;
        }

        public void setOrderItemValue(double orderItemValue) {
            this.orderItemValue = orderItemValue;
        }

        public double getSettledValue() {
            return settledValue;
        }

        public void setSettledValue(double settledValue) {
            this.settledValue = settledValue;
        }

        public Date getDepositDate() {
            return depositDate;
        }

        public void setDepositDate(Date depositDate) {
            this.depositDate = depositDate;
        }

        public String getStatusCode() {
            return statusCode;
        }

        public void setStatusCode(String statusCode) {
            this.statusCode = statusCode;
        }

        public int getQuantity() {
            return quantity;
        }

        public void setQuantity(int quantity) {
            this.quantity = quantity;
        }

        public BigDecimal getTotalRewards() {
            return totalRewards;
        }

        public void setTotalRewards(BigDecimal totalRewards) {
            this.totalRewards = totalRewards;
        }

        public BigDecimal getTotalDiscounts() {
            return totalDiscounts;
        }

        public void setTotalDiscounts(BigDecimal totalDiscounts) {
            this.totalDiscounts = totalDiscounts;
        }

        public BigDecimal getTotalOtherIncentive() {
            return totalOtherIncentive;
        }

        public void setTotalOtherIncentive(BigDecimal totalOtherIncentive) {
            this.totalOtherIncentive = totalOtherIncentive;
        }

        public BigDecimal getTotalSellerEarning() {
            return totalSellerEarning;
        }

        public void setTotalSellerEarning(BigDecimal totalSellerEarning) {
            this.totalSellerEarning = totalSellerEarning;
        }

        public BigDecimal getTotalCommission() {
            return totalCommission;
        }

        public void setTotalCommission(BigDecimal totalCommission) {
            this.totalCommission = totalCommission;
        }

        public BigDecimal getTotalFixedFee() {
            return totalFixedFee;
        }

        public void setTotalFixedFee(BigDecimal totalFixedFee) {
            this.totalFixedFee = totalFixedFee;
        }

        public BigDecimal getTotalShippingCharge() {
            return totalShippingCharge;
        }

        public void setTotalShippingCharge(BigDecimal totalShippingCharge) {
            this.totalShippingCharge = totalShippingCharge;
        }

        public BigDecimal getTotalShippingFee() {
            return totalShippingFee;
        }

        public void setTotalShippingFee(BigDecimal totalShippingFee) {
            this.totalShippingFee = totalShippingFee;
        }

        public BigDecimal getTotalReverseShippingFee() {
            return totalReverseShippingFee;
        }

        public void setTotalReverseShippingFee(BigDecimal totalReverseShippingFee) {
            this.totalReverseShippingFee = totalReverseShippingFee;
        }

        public BigDecimal getTotalAdditionalFee() {
            return totalAdditionalFee;
        }

        public void setTotalAdditionalFee(BigDecimal totalAdditionalFee) {
            this.totalAdditionalFee = totalAdditionalFee;
        }

        public BigDecimal getTotalChannelPenalty() {
            return totalChannelPenalty;
        }

        public void setTotalChannelPenalty(BigDecimal totalChannelPenalty) {
            this.totalChannelPenalty = totalChannelPenalty;
        }

        public double getTotalChannelRecovery() {
            return totalChannelRecovery;
        }

        public void setTotalChannelRecovery(double totalChannelRecovery) {
            this.totalChannelRecovery = totalChannelRecovery;
        }

        public BigDecimal getTotalTax() {
            return totalTax;
        }

        public void setTotalTax(BigDecimal totalTax) {
            this.totalTax = totalTax;
        }

        public String getAdditionalFeeDescription() {
            return additionalFeeDescription;
        }

        public void setAdditionalFeeDescription(String additionalFeeDescription) {
            this.additionalFeeDescription = additionalFeeDescription;
        }

        public Date getCreated() {
            return created;
        }

        public void setCreated(Date created) {
            this.created = created;
        }
    }
}
