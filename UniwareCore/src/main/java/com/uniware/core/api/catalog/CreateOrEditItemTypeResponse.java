/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 18-Apr-2012
 *  @author vibhu
 */
package com.uniware.core.api.catalog;

/**
 * @author vibhu
 */
public class CreateOrEditItemTypeResponse extends ItemTypeResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 7256189248564358967L;

}