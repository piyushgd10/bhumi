/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, May 8, 2012
 *  @author singla
 */
package com.uniware.core.api.putaway;

import com.unifier.core.api.base.ServiceRequest;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

/**
 * @author singla
 */
public class CreatePutawayRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 3723108920932257270L;

    @NotNull
    private Integer           userId;

    @NotBlank
    private String            type;

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

}
