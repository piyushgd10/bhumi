/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 18-Apr-2013
 *  @author unicom
 */
package com.uniware.core.api.channel;

/**
 * @author Sunny Agarwal
 */
public class ChannelParameterDTO {

    private String name;
    private String description;
    private String value;
    private String type;
    private int    priority;

    /**
     * 
     */
    public ChannelParameterDTO() {
    }

    public ChannelParameterDTO(String name, String value) {
        super();
        this.name = name;
        this.value = value;
    }

    public ChannelParameterDTO(String name, String value, String type, int priority) {
        this.name = name;
        this.value = value;
        this.type = type;
        this.priority = priority;
    }

    public ChannelParameterDTO(String name, String description, String value, String type, int priority) {
        this.name = name;
        this.description = description;
        this.value = value;
        this.type = type;
        this.priority = priority;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}
