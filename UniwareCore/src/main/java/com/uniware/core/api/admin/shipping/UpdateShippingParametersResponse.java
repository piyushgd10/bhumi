/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 07-Mar-2013
 *  @author Pankaj
 */
package com.uniware.core.api.admin.shipping;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author Pankaj
 */
public class UpdateShippingParametersResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 529571293966963598L;

}
