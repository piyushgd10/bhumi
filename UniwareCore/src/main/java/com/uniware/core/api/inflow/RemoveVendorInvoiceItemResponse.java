/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 28-Mar-2014
 *  @author karunsingla
 */
package com.uniware.core.api.inflow;

import com.unifier.core.api.base.ServiceResponse;

public class RemoveVendorInvoiceItemResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -3980287209624802463L;

}
