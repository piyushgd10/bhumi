/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Nov 25, 2015
 *  @author akshay
 */
package com.uniware.core.api.recommendation;

import com.unifier.core.api.base.ServiceResponse;

public class SubscriptionRecommendationsResponse extends ServiceResponse {

    private static final long serialVersionUID = -7378680398581853203L;

}
