/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 9, 2012
 *  @author singla
 */
package com.uniware.core.api.returns;

import com.unifier.core.api.base.ServiceResponse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author singla
 */
public class CreateReversePickupResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long   serialVersionUID                     = 5956968031728252517L;
    private String              reversePickupCode;
    private List<String>        saleOrderItemCodes                   = new ArrayList<String>();

    /**
     * @return the reversePickupCode
     */
    public String getReversePickupCode() {
        return reversePickupCode;
    }

    /**
     * @param reversePickupCode the reversePickupCode to set
     */
    public void setReversePickupCode(String reversePickupCode) {
        this.reversePickupCode = reversePickupCode;
    }

    /**
     * @return the saleOrderItemCodes
     */
    public List<String> getSaleOrderItemCodes() {
        return saleOrderItemCodes;
    }

    /**
     * @param saleOrderItemCodes the saleOrderItemCodes to set
     */
    public void setSaleOrderItemCodes(List<String> saleOrderItemCodes) {
        this.saleOrderItemCodes = saleOrderItemCodes;
    }

    public void addSaleOrderItemCode(String code) {
        saleOrderItemCodes.add(code);
    }

}