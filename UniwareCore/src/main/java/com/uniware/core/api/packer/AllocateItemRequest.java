/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jan 16, 2012
 *  @author singla
 */
package com.uniware.core.api.packer;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author singla
 */
public class AllocateItemRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -6205232394715646136L;

    @NotNull
    private String            itemCode;

    @NotBlank
    private String            shippingPackageCode;

    /**
     * @return the itemCode
     */
    public String getItemCode() {
        return itemCode;
    }

    /**
     * @param itemCode the itemCode to set
     */
    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    /**
     * @return the shippingPackageCode
     */
    public String getShippingPackageCode() {
        return shippingPackageCode;
    }

    /**
     * @param shippingPackageCode the shippingPackageCode to set
     */
    public void setShippingPackageCode(String shippingPackageCode) {
        this.shippingPackageCode = shippingPackageCode;
    }

}
