/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jun 20, 2012
 *  @author singla
 */
package com.uniware.core.api.channel;

import java.util.List;

import com.unifier.core.api.base.ServiceResponse;
import com.unifier.scraper.redirection.Form;

/**
 * @author Sunny
 */
public class GoToChannelLandingPageResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -9018924286025819646L;

    private List<Form>        redirectForms;

    public List<Form> getRedirectForms() {
        return redirectForms;
    }

    public void setRedirectForms(List<Form> redirectForms) {
        this.redirectForms = redirectForms;
    }

}
