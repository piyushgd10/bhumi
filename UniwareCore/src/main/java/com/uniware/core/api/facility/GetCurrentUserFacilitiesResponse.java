/*
 * Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 3/5/15 4:46 PM
 * @author amdalal
 */

package com.uniware.core.api.facility;

import java.util.ArrayList;
import java.util.List;

import com.unifier.core.api.base.ServiceResponse;
import com.uniware.core.entity.Facility;

public class GetCurrentUserFacilitiesResponse extends ServiceResponse {

    private static final long serialVersionUID = 8015608140599533156L;

    private List<FacilityDTO> facilityDTOList  = new ArrayList<>();

    private String            currentFacilityCode;

    private boolean           accessibleAllFacilities;

    public List<FacilityDTO> getFacilityDTOList() {
        return facilityDTOList;
    }

    public void setFacilityDTOList(List<FacilityDTO> facilityDTOList) {
        this.facilityDTOList = facilityDTOList;
    }

    public String getCurrentFacilityCode() {
        return currentFacilityCode;
    }

    public void setCurrentFacilityCode(String currentFacilityCode) {
        this.currentFacilityCode = currentFacilityCode;
    }

    public boolean isAccessibleAllFacilities() {
        return accessibleAllFacilities;
    }

    public void setAccessibleAllFacilities(boolean accessibleAllFacilities) {
        this.accessibleAllFacilities = accessibleAllFacilities;
    }

    public static class FacilityDTO {

        private String code;

        private String name;

        private String displayName;

        private String type;

        public FacilityDTO() {
            super();
        }

        public FacilityDTO(Facility facility) {
            super();
            setCode(facility.getCode());
            setName(facility.getName());
            setDisplayName(facility.getDisplayName());
            setType(facility.getType());
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDisplayName() {
            return displayName;
        }

        public void setDisplayName(String displayName) {
            this.displayName = displayName;
        }
    }
}
