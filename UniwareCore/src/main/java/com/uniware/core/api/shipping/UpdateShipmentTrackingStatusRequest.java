/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 26-Apr-2013
 *  @author unicom
 */
package com.uniware.core.api.shipping;

import java.util.Date;

import javax.validation.constraints.NotNull;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author unicom
 */
public class UpdateShipmentTrackingStatusRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 243242342L;

    @NotNull
    private String            shippingPackageCode;

    @NotNull
    private String            shipmentTrackingStatus;

    @NotNull
    private Date              statusDate;

    private boolean           manualStatusUpdate;

    /**
     * @return the shippingPackageCode
     */
    public String getShippingPackageCode() {
        return shippingPackageCode;
    }

    /**
     * @param shippingPackageCode the shippingPackageCode to set
     */
    public void setShippingPackageCode(String shippingPackageCode) {
        this.shippingPackageCode = shippingPackageCode;
    }

    /**
     * @return the shipmentTrackingStatus
     */
    public String getShipmentTrackingStatus() {
        return shipmentTrackingStatus;
    }

    /**
     * @param shipmentTrackingStatus the shipmentTrackingStatus to set
     */
    public void setShipmentTrackingStatus(String shipmentTrackingStatus) {
        this.shipmentTrackingStatus = shipmentTrackingStatus;
    }

    /**
     * @return the statusDate
     */
    public Date getStatusDate() {
        return statusDate;
    }

    /**
     * @param statusDate the statusDate to set
     */
    public void setStatusDate(Date statusDate) {
        this.statusDate = statusDate;
    }

    /**
     * @return the manualStatusUpdate
     */
    public boolean isManualStatusUpdate() {
        return manualStatusUpdate;
    }

    /**
     * @param manualStatusUpdate the manualStatusUpdate to set
     */
    public void setManualStatusUpdate(boolean manualStatusUpdate) {
        this.manualStatusUpdate = manualStatusUpdate;
    }

}
