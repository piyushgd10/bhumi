/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 10-May-2013
 *  @author karunsingla
 */
package com.uniware.core.api.putaway;

import com.unifier.core.api.base.ServiceResponse;

public class AddNonTraceableItemToTransferPutawayResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 2549297537656051529L;

}
