package com.uniware.core.api.channelWarehouse;

import com.unifier.core.api.base.ServiceRequest;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * Created by digvijaysharma on 20/01/17.
 */
public class SyncChannelWarehouseInventoryRequest extends ServiceRequest {

    /**
     *
     */
    private static final long serialVersionUID = 1963404117545473225L;

    @NotEmpty
    private String      channelCode;

    public SyncChannelWarehouseInventoryRequest() {
        super();
    }

    public SyncChannelWarehouseInventoryRequest(String channelCode) {
        this.channelCode = channelCode;
    }

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }
}

