/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 14-May-2012
 *  @author praveeng
 */
package com.uniware.core.api.item;

import com.unifier.core.api.base.ServiceRequest;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * <ul>
 * Parameters:
 * <li>{@code itemCode}: code of item which is to be added in putaway.</li>
 * <li>{@code putawayCode}: code of putaway in which gatepass items are to be added.</li>
 * <li>{@code userId}: id of user, set by context</li>
 * </ul>
 * 
 * @author praveeng
 */
public class AddInspectedNotBadItemToPutawayRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 876013105113367675L;

    @NotEmpty
    private String            itemCode;

    @NotNull
    private Integer           userId;

    @NotBlank
    private String            putawayCode;

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return the itemCode
     */
    public String getItemCode() {
        return itemCode;
    }

    /**
     * @param itemCode the itemCode to set
     */
    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    /**
     * @return the putawayCode
     */
    public String getPutawayCode() {
        return putawayCode;
    }

    /**
     * @param putawayCode the putawayCode to set
     */
    public void setPutawayCode(String putawayCode) {
        this.putawayCode = putawayCode;
    }

}
