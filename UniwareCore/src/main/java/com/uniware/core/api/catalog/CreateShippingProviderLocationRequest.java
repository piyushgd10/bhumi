/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 26, 2012
 *  @author praveeng
 */
package com.uniware.core.api.catalog;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;
import com.unifier.core.api.customfields.WsCustomFieldValue;

/**
 * @author praveeng
 */
public class CreateShippingProviderLocationRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long        serialVersionUID = 2100269454729337684L;

    @NotBlank
    private String                   shippingProviderCode;

    @NotBlank
    private String                   shippingMethodName;

    private String                   name;

    @NotBlank
    private String                   pincode;

    private String                   routingCode;

    private String                   facilityCode;

    @NotNull
    private Integer                  priority;

    private boolean                  enabled          = true;

    private String                   filterExpression;

    @Valid
    private List<WsCustomFieldValue> customFieldValues;

    public CreateShippingProviderLocationRequest() {
    }

    public CreateShippingProviderLocationRequest(String shippingProviderCode, String shippingMethodName, String pincode, Integer priority) {
        this.shippingProviderCode = shippingProviderCode;
        this.shippingMethodName = shippingMethodName;
        this.pincode = pincode;
        this.priority = priority;
    }

    public CreateShippingProviderLocationRequest(String shippingProviderCode, String shippingMethodName, String pincode, Integer priority, String facilityCode) {
        this.shippingProviderCode = shippingProviderCode;
        this.shippingMethodName = shippingMethodName;
        this.pincode = pincode;
        this.priority = priority;
        this.facilityCode = facilityCode;
    }

    public CreateShippingProviderLocationRequest(AddOrEditShippingProviderLocationRequest request) {
        this.shippingProviderCode = request.getShippingProviderCode();
        this.shippingMethodName = request.getShippingMethodName();
        this.name = request.getName();
        this.pincode = request.getPincode();
        this.routingCode = request.getRoutingCode();
        this.priority = request.getPriority();
        this.enabled = request.isEnabled();
        this.filterExpression = request.getFilterExpression();
        this.customFieldValues = request.getCustomFieldValues();
    }

    /**
     * @return the shippingProviderCode
     */
    public String getShippingProviderCode() {
        return shippingProviderCode;
    }

    /**
     * @param shippingProviderCode the shippingProviderCode to set
     */
    public void setShippingProviderCode(String shippingProviderCode) {
        this.shippingProviderCode = shippingProviderCode;
    }

    /**
     * @return the shippingMethodName
     */
    public String getShippingMethodName() {
        return shippingMethodName;
    }

    /**
     * @param shippingMethodName the shippingMethodName to set
     */
    public void setShippingMethodName(String shippingMethodName) {
        this.shippingMethodName = shippingMethodName;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the pincode
     */
    public String getPincode() {
        return pincode;
    }

    /**
     * @param pincode the pincode to set
     */
    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    /**
     * @return the routingCode
     */
    public String getRoutingCode() {
        return routingCode;
    }

    /**
     * @param routingCode the routingCode to set
     */
    public void setRoutingCode(String routingCode) {
        this.routingCode = routingCode;
    }

    /**
     * @return the priority
     */
    public Integer getPriority() {
        return priority;
    }

    /**
     * @param priority the priority to set
     */
    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public String getFilterExpression() {
        return filterExpression;
    }

    public void setFilterExpression(String filterExpression) {
        this.filterExpression = filterExpression;
    }

    public List<WsCustomFieldValue> getCustomFieldValues() {
        return customFieldValues;
    }

    public void setCustomFieldValues(List<WsCustomFieldValue> customFieldValues) {
        this.customFieldValues = customFieldValues;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getFacilityCode() {
        return facilityCode;
    }

    public void setFacilityCode(String facilityCode) {
        this.facilityCode = facilityCode;
    }

}
