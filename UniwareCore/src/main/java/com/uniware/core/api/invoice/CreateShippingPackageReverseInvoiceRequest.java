package com.uniware.core.api.invoice;

import java.util.Set;

import javax.validation.constraints.NotNull;

import com.unifier.core.api.base.ServiceRequest;

/**
 * Created by Sagar Sahni on 01/06/17.
 */
public class CreateShippingPackageReverseInvoiceRequest extends ServiceRequest {

    private String      shippingPackageCode;

    private String      invoiceCode;

    @NotNull
    private Integer     userId;

    private String      saleOrderCode;

    private Set<String> saleOrderItemCodes;

    public String getShippingPackageCode() {
        return shippingPackageCode;
    }

    public void setShippingPackageCode(String shippingPackageCode) {
        this.shippingPackageCode = shippingPackageCode;
    }

    public String getInvoiceCode() {
        return invoiceCode;
    }

    public void setInvoiceCode(String invoiceCode) {
        this.invoiceCode = invoiceCode;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getSaleOrderCode() {
        return saleOrderCode;
    }

    public void setSaleOrderCode(String saleOrderCode) {
        this.saleOrderCode = saleOrderCode;
    }

    public Set<String> getSaleOrderItemCodes() {
        return saleOrderItemCodes;
    }

    public void setSaleOrderItemCodes(Set<String> saleOrderItemCodes) {
        this.saleOrderItemCodes = saleOrderItemCodes;
    }
}
