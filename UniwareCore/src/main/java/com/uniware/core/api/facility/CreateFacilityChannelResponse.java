/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 03-Sep-2015
 *  @author parijat
 */
package com.uniware.core.api.facility;

import com.unifier.core.api.base.ServiceResponse;

public class CreateFacilityChannelResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 876543456789L;

    private CreateFacilityResponse facilityResponse;

    /**
     * @return the facilityResponse
     */
    public CreateFacilityResponse getFacilityResponse() {
        return facilityResponse;
    }

    /**
     * @param facilityResponse the facilityResponse to set
     */
    public void setFacilityResponse(CreateFacilityResponse facilityResponse) {
        this.facilityResponse = facilityResponse;
    }

}
