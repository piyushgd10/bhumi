/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 15, 2011
 *  @author singla
 */
package com.uniware.core.api.saleorder;

import com.unifier.core.api.base.ServiceRequest;
import com.unifier.core.pagination.SearchOptions;

import java.util.Date;

/**
 * @author singla
 */
public class SearchSaleOrderItemRequest extends ServiceRequest {
    /**
     * 
     */
    private static final long serialVersionUID = 3148768119801793617L;
    private String            saleOrderCode;
    private String            saleOrderItemCode;
    private String            status;
    private String            itemNameContains;
    private String            itemSkuContains;
    private Boolean           cashOnDelivery;
    private SearchOptions     searchOptions;
    private Date              fromDate;
    private Date              toDate;
    private boolean           onHold;

    /**
     * @return the saleOrderCode
     */
    public String getSaleOrderCode() {
        return saleOrderCode;
    }

    /**
     * @param saleOrderCode the saleOrderCode to set
     */
    public void setSaleOrderCode(String saleOrderCode) {
        this.saleOrderCode = saleOrderCode;
    }

    /**
     * @return the saleOrderItemCode
     */
    public String getSaleOrderItemCode() {
        return saleOrderItemCode;
    }

    /**
     * @param saleOrderItemCode the saleOrderItemCode to set
     */
    public void setSaleOrderItemCode(String saleOrderItemCode) {
        this.saleOrderItemCode = saleOrderItemCode;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the searchOptions
     */
    public SearchOptions getSearchOptions() {
        return searchOptions;
    }

    /**
     * @param searchOptions the searchOptions to set
     */
    public void setSearchOptions(SearchOptions searchOptions) {
        this.searchOptions = searchOptions;
    }

    /**
     * @return the itemNameContains
     */
    public String getItemNameContains() {
        return itemNameContains;
    }

    /**
     * @return the itemSkuContains
     */
    public String getItemSkuContains() {
        return itemSkuContains;
    }

    /**
     * @param itemNameContains the itemNameContains to set
     */
    public void setItemNameContains(String itemNameContains) {
        this.itemNameContains = itemNameContains;
    }

    /**
     * @param itemSkuContains the itemSkuContains to set
     */
    public void setItemSkuContains(String itemSkuContains) {
        this.itemSkuContains = itemSkuContains;
    }

    /**
     * @return the fromDate
     */
    public Date getFromDate() {
        return fromDate;
    }

    /**
     * @param fromDate the fromDate to set
     */
    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    /**
     * @return the toDate
     */
    public Date getToDate() {
        return toDate;
    }

    /**
     * @param toDate the toDate to set
     */
    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    /**
     * @return the cashOnDelivery
     */
    public Boolean getCashOnDelivery() {
        return cashOnDelivery;
    }

    /**
     * @param cashOnDelivery the cashOnDelivery to set
     */
    public void setCashOnDelivery(Boolean cashOnDelivery) {
        this.cashOnDelivery = cashOnDelivery;
    }

    /**
     * @return the onHold
     */
    public boolean isOnHold() {
        return onHold;
    }

    /**
     * @param onHold the onHold to set
     */
    public void setOnHold(boolean onHold) {
        this.onHold = onHold;
    }

}