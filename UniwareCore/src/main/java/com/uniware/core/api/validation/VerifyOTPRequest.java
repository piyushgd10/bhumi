package com.uniware.core.api.validation;

import com.unifier.core.api.base.ServiceRequest;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Created by admin on 10/6/15.
 */
public class VerifyOTPRequest extends ServiceRequest{

    private static final long serialVersionUID = 7151039368033818972L;

    @NotBlank
    private String email;

    @NotBlank
    private String otp;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

}
