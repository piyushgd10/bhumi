/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 29-Mar-2012
 *  @author vibhu
 */
package com.uniware.core.api.returns;

import com.unifier.core.api.base.ServiceResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * @author vibhu
 */
public class SearchAwaitingShippingPackageResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long                    serialVersionUID = -1174289477269305541L;
    private Long                                 totalRecords;
    private final List<PackageAwaitingActionDTO> elements         = new ArrayList<PackageAwaitingActionDTO>();

    /**
     * @return the totalRecords
     */
    public Long getTotalRecords() {
        return totalRecords;
    }

    /**
     * @param totalRecords the totalRecords to set
     */
    public void setTotalRecords(Long totalRecords) {
        this.totalRecords = totalRecords;
    }

    /**
     * @return the elements
     */
    public List<PackageAwaitingActionDTO> getElements() {
        return elements;
    }

}
