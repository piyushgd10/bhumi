/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Apr 1, 2012
 *  @author praveeng
 */
package com.uniware.core.api.catalog.dto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.unifier.core.api.customfields.CustomFieldMetadataDTO;
import com.unifier.core.configuration.ConfigurationManager;
import com.uniware.core.api.bundle.ComponentItemTypeDTO;
import com.uniware.core.configuration.TaxConfiguration;
import com.uniware.core.entity.ItemType;
import com.uniware.core.entity.ItemType.Type;

/**
 * @author praveeng
 */
public class ItemTypeFullDTO {

    private Integer                      tat;
    private Integer                      id;
    private String                       skuCode;
    private String                       categoryCode;
    private String                       name;
    private String                       description;
    private String                       scanIdentifier;
    private Integer                      length;
    private Integer                      width;
    private Integer                      height;
    private Integer                      weight;
    private String                       color;
    private String                       size;
    private String                       brand;
    private String                       ean;
    private String                       upc;
    private String                       isbn;
    private BigDecimal                   maxRetailPrice;
    private BigDecimal                   basePrice;
    private BigDecimal                   costPrice;
    private String                       taxTypeCode;
    private String                       gstTaxTypeCode;
    private String                       hsnCode;
    private String                       imageUrl;
    private String                       productPageUrl;
    private Type                         type;
    private boolean                      requiresCustomization;
    private String                       itemDetailFieldsText;
    private boolean                      enabled;
    private List<String>                 tags;
    private Integer                      shelfLife;
    private Boolean                      expirable;
    private List<CustomFieldMetadataDTO> customFieldValues;
    private List<ComponentItemTypeDTO>   componentItemTypes = new ArrayList<ComponentItemTypeDTO>(0);

    public ItemTypeFullDTO() {
    }

    /**
     * @param itemType
     */
    public ItemTypeFullDTO(ItemType itemType) {
        this.id = itemType.getId();
        this.skuCode = itemType.getSkuCode();
        this.categoryCode = itemType.getCategory().getCode();
        this.name = itemType.getName();
        this.description = itemType.getDescription();
        this.scanIdentifier = itemType.getScanIdentifier();
        this.length = itemType.getLength();
        this.width = itemType.getWidth();
        this.height = itemType.getHeight();
        this.weight = itemType.getWeight();
        this.ean = itemType.getEan();
        this.upc = itemType.getUpc();
        this.isbn = itemType.getIsbn();
        this.maxRetailPrice = itemType.getMaxRetailPrice();
        this.basePrice = itemType.getBasePrice();
        this.costPrice = itemType.getCostPrice();
        this.tags = itemType.getTagList();
        type = itemType.getType();
        if (itemType.getTaxType() != null) {
            this.taxTypeCode = ConfigurationManager.getInstance().getConfiguration(TaxConfiguration.class).getTaxTypeById(itemType.getTaxType().getId()).getCode();
        }
        if (itemType.getGstTaxType() != null) {
            this.gstTaxTypeCode = ConfigurationManager.getInstance().getConfiguration(TaxConfiguration.class).getTaxTypeById(itemType.getGstTaxType().getId()).getCode();
        }
        this.hsnCode = itemType.getHsnCode();
        this.requiresCustomization = itemType.isRequiresCustomization();
        this.imageUrl = itemType.getImageUrl();
        this.setProductPageUrl(itemType.getProductPageUrl());
        this.color = itemType.getColor();
        this.size = itemType.getSize();
        this.brand = itemType.getBrand();
        this.itemDetailFieldsText = itemType.getItemDetailFieldsText();
        this.enabled = itemType.isEnabled();
        this.tat = itemType.getTat();
        this.shelfLife = itemType.getShelfLife();
        this.expirable = itemType.getExpirable();
    }

    /**
     * @return the customFieldValues
     */
    public List<CustomFieldMetadataDTO> getCustomFieldValues() {
        return customFieldValues;
    }

    /**
     * @param customFieldValues the customFieldValues to set
     */
    public void setCustomFieldValues(List<CustomFieldMetadataDTO> customFieldValues) {
        this.customFieldValues = customFieldValues;
    }

    public Integer getShelfLife() {
        return shelfLife;
    }

    public void setShelfLife(Integer shelfLife) {
        this.shelfLife = shelfLife;
    }

    public Boolean getExpirable() {
        return expirable;
    }

    public void setExpirable(Boolean expirable) {
        this.expirable = expirable;
    }

    public Integer getTat() {
        return tat;
    }

    public void setTat(Integer tat) {
        this.tat = tat;
    }

    public String getEan() {
        return ean;
    }

    public void setEan(String ean) {
        this.ean = ean;
    }

    public String getUpc() {
        return upc;
    }

    public void setUpc(String upc) {
        this.upc = upc;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    /**
     * @return the maxRetailPRice
     */
    public BigDecimal getMaxRetailPrice() {
        return maxRetailPrice;
    }

    /**
     * @param maxRetailPrice the maxRetailPRice to set
     */
    public void setMaxRetailPrice(BigDecimal maxRetailPrice) {
        this.maxRetailPrice = maxRetailPrice;
    }

    public BigDecimal getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(BigDecimal basePrice) {
        this.basePrice = basePrice;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the skuCode
     */
    public String getSkuCode() {
        return skuCode;
    }

    /**
     * @param skuCode the skuCode to set
     */
    public void setSkuCode(String skuCode) {
        this.skuCode = skuCode;
    }

    /**
     * @return the categoryCode
     */
    public String getCategoryCode() {
        return categoryCode;
    }

    /**
     * @param categoryCode the categoryCode to set
     */
    public void setCategoryCode(String categoryCode) {
        this.categoryCode = categoryCode;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the length
     */
    public Integer getLength() {
        return length;
    }

    /**
     * @param length the length to set
     */
    public void setLength(Integer length) {
        this.length = length;
    }

    /**
     * @return the width
     */
    public Integer getWidth() {
        return width;
    }

    /**
     * @param width the width to set
     */
    public void setWidth(Integer width) {
        this.width = width;
    }

    /**
     * @return the height
     */
    public Integer getHeight() {
        return height;
    }

    /**
     * @param height the height to set
     */
    public void setHeight(Integer height) {
        this.height = height;
    }

    /**
     * @return the weight
     */
    public Integer getWeight() {
        return weight;
    }

    /**
     * @param weight the weight to set
     */
    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    /**
     * @return the imageUrl
     */
    public String getImageUrl() {
        return imageUrl;
    }

    /**
     * @param imageUrl the imageUrl to set
     */
    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    /**
     * @return the tags
     */
    public List<String> getTags() {
        return tags;
    }

    /**
     * @param tags the tags to set
     */
    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    /**
     * @return the productPageUrl
     */
    public String getProductPageUrl() {
        return productPageUrl;
    }

    /**
     * @param productPageUrl the productPageUrl to set
     */
    public void setProductPageUrl(String productPageUrl) {
        this.productPageUrl = productPageUrl;
    }

    /**
     * @return the taxTypeCode
     */
    public String getTaxTypeCode() {
        return taxTypeCode;
    }

    /**
     * @param taxTypeCode the taxTypeCode to set
     */
    public void setTaxTypeCode(String taxTypeCode) {
        this.taxTypeCode = taxTypeCode;
    }

    public String getGstTaxTypeCode() {
        return gstTaxTypeCode;
    }

    public void setGstTaxTypeCode(String gstTaxTypeCode) {
        this.gstTaxTypeCode = gstTaxTypeCode;
    }

    public String getHsnCode() {
        return hsnCode;
    }

    public void setHsnCode(String hsnCode) {
        this.hsnCode = hsnCode;
    }

    /**
     * @return the color
     */
    public String getColor() {
        return color;
    }

    /**
     * @return the size
     */
    public String getSize() {
        return size;
    }

    /**
     * @return the brand
     */
    public String getBrand() {
        return brand;
    }

    public String getItemDetailFieldsText() {
        return itemDetailFieldsText;
    }

    public void setItemDetailFieldsText(String itemDetailFieldsText) {
        this.itemDetailFieldsText = itemDetailFieldsText;
    }

    /**
     * @return the scanIdentifier
     */
    public String getScanIdentifier() {
        return scanIdentifier;
    }

    /**
     * @param scanIdentifier the scanIdentifier to set
     */
    public void setScanIdentifier(String scanIdentifier) {
        this.scanIdentifier = scanIdentifier;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public List<ComponentItemTypeDTO> getComponentItemTypes() {
        return componentItemTypes;
    }

    public void setComponentItemTypes(List<ComponentItemTypeDTO> componentItemTypes) {
        this.componentItemTypes = componentItemTypes;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public boolean isRequiresCustomization() {
        return requiresCustomization;
    }

    public void setRequiresCustomization(boolean requiresCustomization) {
        this.requiresCustomization = requiresCustomization;
    }

    public BigDecimal getCostPrice() {
        return costPrice;
    }

    public void setCostPrice(BigDecimal costPrice) {
        this.costPrice = costPrice;
    }
}
