/*
 * Copyright 2016 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 13/9/16 7:54 PM
 * @author bhuvneshwarkumar
 */

package com.uniware.core.api.admin.pickset;

import com.unifier.core.api.base.ServiceRequest;
import com.uniware.core.entity.PickSet;

/**
 * Created by bhuvneshwarkumar on 13/09/16.
 */
public class GetPickSetsByTypeRequest extends ServiceRequest {

    private PickSet.Type type;

    public PickSet.Type getType() {
        return type;
    }

    public void setType(PickSet.Type type) {
        this.type = type;
    }
}
