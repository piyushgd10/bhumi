/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 08-Apr-2015
 *  @author akshaykochhar
 */
package com.uniware.core.api.material;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.unifier.core.api.base.ServiceResponse;
import com.unifier.core.api.customfields.CustomFieldMetadataDTO;

public class GetGatepassSummaryResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -4796572641428158122L;

    public static class GatepassSummaryDTO {

        private String                       code;
        private String                       statusCode;
        private String                       type;
        private String                       toPartyName;
        private String                       reference;
        private String                       purpose;
        private String                       username;
        private String                       invoiceCode;
        private String                       invoiceDisplayCode;
        private String                       returnInvoiceCode;
        private String                       returnInvoiceDisplayCode;
        private BigDecimal                   total = new BigDecimal(0);
        private Integer                      totalSku;
        private Integer                      totalItems;
        private Date                         created;
        private List<CustomFieldMetadataDTO> customFieldValues;

        public String getInvoiceDisplayCode() {
            return invoiceDisplayCode;
        }

        public void setInvoiceDisplayCode(String invoiceDisplayCode) {
            this.invoiceDisplayCode = invoiceDisplayCode;
        }

        public String getReturnInvoiceDisplayCode() {
            return returnInvoiceDisplayCode;
        }

        public void setReturnInvoiceDisplayCode(String returnInvoiceDisplayCode) {
            this.returnInvoiceDisplayCode = returnInvoiceDisplayCode;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getStatusCode() {
            return statusCode;
        }

        public void setStatusCode(String statusCode) {
            this.statusCode = statusCode;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getToPartyName() {
            return toPartyName;
        }

        public void setToPartyName(String toPartyName) {
            this.toPartyName = toPartyName;
        }

        public String getReference() {
            return reference;
        }

        public void setReference(String reference) {
            this.reference = reference;
        }

        public String getPurpose() {
            return purpose;
        }

        public void setPurpose(String purpose) {
            this.purpose = purpose;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getInvoiceCode() {
            return invoiceCode;
        }

        public void setInvoiceCode(String invoiceCode) {
            this.invoiceCode = invoiceCode;
        }

        public String getReturnInvoiceCode() {
            return returnInvoiceCode;
        }

        public void setReturnInvoiceCode(String returnInvoiceCode) {
            this.returnInvoiceCode = returnInvoiceCode;
        }

        public BigDecimal getTotal() {
            return total;
        }

        public void setTotal(BigDecimal total) {
            this.total = total;
        }
        
        public Integer getTotalItems() {
            return totalItems;
        }

        public void setTotalItems(Integer totalItems) {
            this.totalItems = totalItems;
        }

        public Integer getTotalSku() {
            return totalSku;
        }

        public void setTotalSku(Integer totalSku) {
            this.totalSku = totalSku;
        }

        public Date getCreated() {
            return created;
        }

        public void setCreated(Date created) {
            this.created = created;
        }

        public List<CustomFieldMetadataDTO> getCustomFieldValues() {
            return customFieldValues;
        }

        public void setCustomFieldValues(List<CustomFieldMetadataDTO> customFieldValues) {
            this.customFieldValues = customFieldValues;
        }

    }

    private GatepassSummaryDTO summary;

    public GatepassSummaryDTO getSummary() {
        return summary;
    }

    public void setSummary(GatepassSummaryDTO summary) {
        this.summary = summary;
    }

}
