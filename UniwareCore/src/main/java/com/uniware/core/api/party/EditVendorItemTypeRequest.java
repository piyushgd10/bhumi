/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 23, 2012
 *  @author praveeng
 */
package com.uniware.core.api.party;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author praveeng
 */
public class EditVendorItemTypeRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 152893380075956746L;

    @NotNull
    Integer                   userId;

    @NotNull
    private String            actionType;

    @NotNull
    @Valid
    private WsVendorItemType  vendorItemType;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getActionType() {
        return actionType;
    }

    public void setActionType(String actionType) {
        this.actionType = actionType;
    }

    /**
     * @return the vendorItemType
     */
    public WsVendorItemType getVendorItemType() {
        return vendorItemType;
    }

    /**
     * @param vendorItemType the vendorItemType to set
     */
    public void setVendorItemType(WsVendorItemType vendorItemType) {
        this.vendorItemType = vendorItemType;
    }
}
