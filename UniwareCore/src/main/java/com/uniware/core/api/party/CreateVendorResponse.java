/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 20, 2012
 *  @author praveeng
 */
package com.uniware.core.api.party;

/**
 * @author praveeng
 */
public class CreateVendorResponse extends VendorResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -2426856553133649676L;

}
