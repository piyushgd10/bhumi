/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 25-Jun-2014
 *  @author akshay
 */
package com.uniware.core.api.tax;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

public class GetTaxTypeConfigurationRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @NotBlank
    private String            taxTypeCode;

    public String getTaxTypeCode() {
        return taxTypeCode;
    }

    public void setTaxTypeCode(String taxTypeCode) {
        this.taxTypeCode = taxTypeCode;
    }
    
}
