/*
 * Copyright 2016 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 19/9/16 5:06 PM
 * @author bhuvneshwarkumar
 */

package com.uniware.core.api.picker;

import java.util.List;

import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

import com.unifier.core.api.base.ServiceRequest;

/**
 * Created by bhuvneshwarkumar on 19/09/16.
 */
public class DisablePickBucketRequest extends ServiceRequest {

    @Size(min = 1)
    @NotEmpty
    private List<String> pickBucketCodes;

    public List<String> getPickBucketCodes() {
        return pickBucketCodes;
    }

    public void setPickBucketCodes(List<String> pickBucketCodes) {
        this.pickBucketCodes = pickBucketCodes;
    }
}
