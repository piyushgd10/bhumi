/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 20, 2013
 *  @author praveen
 */
package com.uniware.core.api.purchase;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author praveen
 */
public class RemoveVendorItemsFromCartResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -621136643810142992L;

}
