package com.uniware.core.api.cyclecount;

import java.util.Date;

/**
 * Created by harshpal on 3/14/16.
 */
public class CycleCountItemDTO {
    private String shelfCode;
    private String skuCode;
    private int    expectedInventory;
    private int    actualInventory;
    private int    reconciledCount;
    private Date   created;

    public String getShelfCode() {
        return shelfCode;
    }

    public void setShelfCode(String shelfCode) {
        this.shelfCode = shelfCode;
    }

    public String getSkuCode() {
        return skuCode;
    }

    public void setSkuCode(String skuCode) {
        this.skuCode = skuCode;
    }

    public int getExpectedInventory() {
        return expectedInventory;
    }

    public void setExpectedInventory(int expectedInventory) {
        this.expectedInventory = expectedInventory;
    }

    public int getActualInventory() {
        return actualInventory;
    }

    public void setActualInventory(int actualInventory) {
        this.actualInventory = actualInventory;
    }

    public int getReconciledCount() {
        return reconciledCount;
    }

    public void setReconciledCount(int reconciledCount) {
        this.reconciledCount = reconciledCount;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }
}
