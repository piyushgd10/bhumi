/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jun 20, 2012
 *  @author singla
 */
package com.uniware.core.api.currency;

import java.math.BigDecimal;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author Sunny
 */
public class GetCurrencyConversionRateResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -8331014310944611193L;
    private BigDecimal        conversionRate;

    public BigDecimal getConversionRate() {
        return conversionRate;
    }

    public void setConversionRate(BigDecimal conversionRate) {
        this.conversionRate = conversionRate;
    }

}
