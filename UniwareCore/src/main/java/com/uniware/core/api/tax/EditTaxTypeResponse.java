/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 *  @version     1.0, 20-Jul-2012
 *  @author praveeng
 */
package com.uniware.core.api.tax;

import com.unifier.core.api.base.ServiceResponse;

import com.uniware.core.api.catalog.TaxTypeDTO;

/**
 * @author praveeng
 */
public class EditTaxTypeResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -373759278523577611L;
    private TaxTypeDTO        taxTypeDTO;

    /**
     * @return the taxTypeDTO
     */
    public TaxTypeDTO getTaxTypeDTO() {
        return taxTypeDTO;
    }

    /**
     * @param taxTypeDTO the taxTypeDTO to set
     */
    public void setTaxTypeDTO(TaxTypeDTO taxTypeDTO) {
        this.taxTypeDTO = taxTypeDTO;
    }

}
