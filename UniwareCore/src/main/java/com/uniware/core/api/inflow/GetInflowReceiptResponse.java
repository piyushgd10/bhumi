/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 3, 2012
 *  @author praveeng
 */
package com.uniware.core.api.inflow;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author praveeng
 */
public class GetInflowReceiptResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 7735177452806895075L;
    private InflowReceiptDTO  inflowReceipt;
    private String            vendorCode;

    public String getVendorCode() {
        return vendorCode;
    }

    public void setVendorCode(String vendorCode) {
        this.vendorCode = vendorCode;
    }

    /**
     * @return the inflowReceipt
     */
    public InflowReceiptDTO getInflowReceipt() {
        return inflowReceipt;
    }

    /**
     * @param inflowReceipt the inflowReceipt to set
     */
    public void setInflowReceipt(InflowReceiptDTO inflowReceipt) {
        this.inflowReceipt = inflowReceipt;
    }

}