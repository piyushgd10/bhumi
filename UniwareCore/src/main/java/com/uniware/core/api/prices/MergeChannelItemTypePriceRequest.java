package com.uniware.core.api.prices;

import java.math.BigDecimal;

/**
 * Request to merge prices from channel into uniware.
 * Contains transfer price and competitive prices as determined by the channel, in addition to
 * selling price, msp and mrp.
 * 
 * @author shobhit
 */
public class MergeChannelItemTypePriceRequest extends EditChannelItemTypePriceRequest {

    private static final long serialVersionUID = -8139979867842827991L;

    private BigDecimal transferPrice;
    private BigDecimal competitivePrice;

    public BigDecimal getCompetitivePrice() {
        return competitivePrice;
    }

    public void setCompetitivePrice(BigDecimal competitivePrice) {
        this.competitivePrice = competitivePrice;
    }

    public BigDecimal getTransferPrice() {
        return transferPrice;
    }

    public void setTransferPrice(BigDecimal transferPrice) {
        this.transferPrice = transferPrice;
    }
}
