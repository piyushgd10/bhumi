/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Oct 22, 2012
 *  @author Pankaj
 */
package com.uniware.core.api.facility;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author Pankaj
 */
public class DeleteFacilityAllocationRuleRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 6697651353632817219L;

    @NotBlank
    private String            name;

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

}
