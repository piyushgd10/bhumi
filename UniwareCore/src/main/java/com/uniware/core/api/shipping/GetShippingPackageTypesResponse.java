package com.uniware.core.api.shipping;

import java.util.List;

import com.unifier.core.api.base.ServiceResponse;
import com.uniware.core.api.warehouse.ShippingPackageTypeDTO;

/**
 * Created by karunsingla on 26/05/14.
 */
public class GetShippingPackageTypesResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long            serialVersionUID = 8232004934621245315L;
    private List<ShippingPackageTypeDTO> shippingPackageTypes;

    public List<ShippingPackageTypeDTO> getShippingPackageTypes() {
        return shippingPackageTypes;
    }

    public void setShippingPackageTypes(List<ShippingPackageTypeDTO> shippingPackageTypes) {
        this.shippingPackageTypes = shippingPackageTypes;
    }
}
