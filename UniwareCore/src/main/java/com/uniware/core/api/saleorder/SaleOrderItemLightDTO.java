/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 13-Sep-2013
 *  @author parijat
 */
package com.uniware.core.api.saleorder;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author parijat
 */
public class SaleOrderItemLightDTO {

    private int        id;
    private String     itemName;
    private String     itemSku;
    private String     statusCode;
    private String     code;
    private BigDecimal basePrice;
    private BigDecimal totalPrice;
    private BigDecimal sellingPrice;
    private Date       created;
    private Date       updated;
    private BigDecimal quantity;
    private BigDecimal shippingCharges;
    private BigDecimal discount;
    private BigDecimal discountPercentage;
    private BigDecimal maxRetailPrice;

    /**
     * @param itemTypeName
     * @param itemSKU
     * @param partiallyCreated
     * @param code
     * @param basePrice
     * @param totalPrice
     * @param sellingPrice
     * @param quantity
     */
    public SaleOrderItemLightDTO(String itemName, String itemSku, BigDecimal basePrice, BigDecimal totalPrice, BigDecimal sellingPrice, BigDecimal quantity, BigDecimal discount,
            BigDecimal discountPercentage, BigDecimal maxRetailPrice) {
        this.itemName = itemName;
        this.itemSku = itemSku;
        this.basePrice = basePrice;
        this.totalPrice = totalPrice;
        this.sellingPrice = sellingPrice;
        this.quantity = quantity;
        this.discount = discount;
        this.discountPercentage = discountPercentage;
        this.maxRetailPrice = maxRetailPrice;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemSku() {
        return itemSku;
    }

    public void setItemSku(String itemSku) {
        this.itemSku = itemSku;
    }

    /**
     * @return the statusCode
     */
    public String getStatusCode() {
        return statusCode;
    }

    /**
     * @param statusCode the statusCode to set
     */
    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the totalPrice
     */
    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    /**
     * @param totalPrice the totalPrice to set
     */
    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    /**
     * @return the sellingPrice
     */
    public BigDecimal getSellingPrice() {
        return sellingPrice;
    }

    /**
     * @param sellingPrice the sellingPrice to set
     */
    public void setSellingPrice(BigDecimal sellingPrice) {
        this.sellingPrice = sellingPrice;
    }

    /**
     * @return the created
     */
    public Date getCreated() {
        return created;
    }

    /**
     * @param created the created to set
     */
    public void setCreated(Date created) {
        this.created = created;
    }

    /**
     * @return the updated
     */
    public Date getUpdated() {
        return updated;
    }

    /**
     * @param updated the updated to set
     */
    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    /**
     * @return the quantity
     */
    public BigDecimal getQuantity() {
        return quantity;
    }

    /**
     * @param quantity the quantity to set
     */
    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    /**
     * @return the basePrice
     */
    public BigDecimal getBasePrice() {
        return basePrice;
    }

    /**
     * @param basePrice the basePrice to set
     */
    public void setBasePrice(BigDecimal basePrice) {
        this.basePrice = basePrice;
    }

    /**
     * @return the shippingCharges
     */
    public BigDecimal getShippingCharges() {
        return shippingCharges;
    }

    /**
     * @param shippingCharges the shippingCharges to set
     */
    public void setShippingCharges(BigDecimal shippingCharges) {
        this.shippingCharges = shippingCharges;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    public BigDecimal getDiscountPercentage() {
        return discountPercentage;
    }

    public void setDiscountPercentage(BigDecimal discountPercentage) {
        this.discountPercentage = discountPercentage;
    }

    public BigDecimal getMaxRetailPrice() {
        return maxRetailPrice;
    }

    public void setMaxRetailPrice(BigDecimal maxRetailPrice) {
        this.maxRetailPrice = maxRetailPrice;
    }

}
