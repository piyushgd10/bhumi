/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Nov 27, 2012
 *  @author singla
 */
package com.uniware.core.api.inventory;

/**
 * @author singla
 */
public class WsItemTypeInventorySnapshot {

    private String  itemTypeSKU;
    private Integer inventory;
    private Integer openSale;
    private Integer openPurchase;
    private Integer putawayPending;
    private Integer inventoryBlocked;
    private Integer pendingStockTransfer;
    private Integer vendorInventory;
    private Integer virtualInventory;

    /**
     * @return the itemTypeSKU
     */
    public String getItemTypeSKU() {
        return itemTypeSKU;
    }

    /**
     * @param itemTypeSKU the itemTypeSKU to set
     */
    public void setItemTypeSKU(String itemTypeSKU) {
        this.itemTypeSKU = itemTypeSKU;
    }

    /**
     * @return the inventory
     */
    public Integer getInventory() {
        return inventory;
    }

    /**
     * @param inventory the inventory to set
     */
    public void setInventory(Integer inventory) {
        this.inventory = inventory;
    }

    /**
     * @return the openSale
     */
    public Integer getOpenSale() {
        return openSale;
    }

    /**
     * @param openSale the openSale to set
     */
    public void setOpenSale(Integer openSale) {
        this.openSale = openSale;
    }

    /**
     * @return the openPurchase
     */
    public Integer getOpenPurchase() {
        return openPurchase;
    }

    /**
     * @param openPurchase the openPurchase to set
     */
    public void setOpenPurchase(Integer openPurchase) {
        this.openPurchase = openPurchase;
    }

    /**
     * @return the putawayPending
     */
    public Integer getPutawayPending() {
        return putawayPending;
    }

    /**
     * @param putawayPending the putawayPending to set
     */
    public void setPutawayPending(Integer putawayPending) {
        this.putawayPending = putawayPending;
    }

    public Integer getInventoryBlocked() {
        return inventoryBlocked;
    }

    public void setInventoryBlocked(Integer inventoryBlocked) {
        this.inventoryBlocked = inventoryBlocked;
    }

    public Integer getPendingStockTransfer() {
        return pendingStockTransfer;
    }

    public void setPendingStockTransfer(Integer pendingStockTransfer) {
        this.pendingStockTransfer = pendingStockTransfer;
    }

    public Integer getVendorInventory() {
        return vendorInventory;
    }

    public void setVendorInventory(Integer vendorInventory) {
        this.vendorInventory = vendorInventory;
    }

    public Integer getVirtualInventory() {
        return virtualInventory;
    }

    public void setVirtualInventory(Integer virtualInventory) {
        this.virtualInventory = virtualInventory;
    }
    
}
