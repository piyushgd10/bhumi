/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 9, 2012
 *  @author singla
 */
package com.uniware.core.api.shipping;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author singla
 */
public class GetShippingManifestResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -2000166784711006769L;

    private ManifestDTO       shippingManifest;

    /**
     * @return the shippingManifest
     */
    public ManifestDTO getShippingManifest() {
        return shippingManifest;
    }

    /**
     * @param shippingManifest the shippingManifest to set
     */
    public void setShippingManifest(ManifestDTO shippingManifest) {
        this.shippingManifest = shippingManifest;
    }
}
