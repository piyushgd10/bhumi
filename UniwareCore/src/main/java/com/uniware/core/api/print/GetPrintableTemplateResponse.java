/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 25-Mar-2014
 *  @author amit
 */
package com.uniware.core.api.print;

import java.util.HashMap;
import java.util.Map;

import com.unifier.core.api.base.ServiceResponse;
import com.unifier.core.api.print.PrintTemplateDTO;

/**
 * @author amit
 */
public class GetPrintableTemplateResponse extends ServiceResponse {

    private static final long             serialVersionUID  = -1447466126729345979L;

    private Map<String, PrintTemplateDTO> typeToTemplateMap = new HashMap<String, PrintTemplateDTO>();

    public GetPrintableTemplateResponse() {
        super();
    }

    public Map<String, PrintTemplateDTO> getTypeToTemplateMap() {
        return typeToTemplateMap;
    }

    public void setTypeToTemplateMap(Map<String, PrintTemplateDTO> typeToTemplateMap) {
        this.typeToTemplateMap = typeToTemplateMap;
    }
}
