/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 21, 2012
 *  @author praveeng
 */
package com.uniware.core.api.party;

import com.unifier.core.api.base.ServiceRequest;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * @author praveeng
 */
public class AddOrEditVendorAgreementRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -8398038190712170939L;

    @NotNull
    @Valid
    private WsVendorAgreement vendorAgreement;

    /**
     * @return the vendorAgreement
     */
    public WsVendorAgreement getVendorAgreement() {
        return vendorAgreement;
    }

    /**
     * @param vendorAgreement the vendorAgreement to set
     */
    public void setVendorAgreement(WsVendorAgreement vendorAgreement) {
        this.vendorAgreement = vendorAgreement;
    }

}
