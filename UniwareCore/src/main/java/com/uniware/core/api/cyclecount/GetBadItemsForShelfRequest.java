package com.uniware.core.api.cyclecount;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

/**
 * Created by harshpal on 22/04/16.
 */
public class GetBadItemsForShelfRequest extends ServiceRequest {

    @NotBlank
    private String cycleCountCode;

    @NotBlank
    private String shelfCode;

    public String getCycleCountCode() {
        return cycleCountCode;
    }

    public void setCycleCountCode(String cycleCountCode) {
        this.cycleCountCode = cycleCountCode;
    }

    public String getShelfCode() {
        return shelfCode;
    }

    public void setShelfCode(String shelfCode) {
        this.shelfCode = shelfCode;
    }
}
