package com.uniware.core.api.cyclecount;

import java.util.Date;
import java.util.List;

import com.uniware.core.entity.SubCycleCount;

/**
 * Created by harshpal on 3/14/16.
 */
public class SubCycleCountDTO {

    private String         subCycleCountCode;
    private String         statusCode;
    private Date           targetCompletion;
    private Date           completed;
    private long           completedShelfCount;
    private long           pendingShelfCount;
    private long           blockedShelfCount;
    private long           readyShelfCount;
    private long           inProgressShelfCount;
    private Date           created;
    private Date           updated;
    private List<ShelfDTO> shelves;

    public SubCycleCountDTO(SubCycleCount subCycleCount) {
        this.subCycleCountCode = subCycleCount.getCode();
        this.statusCode = subCycleCount.getStatusCode();
        this.targetCompletion = subCycleCount.getTargetCompletion();
        this.completed = subCycleCount.getCompleted();
        this.created = subCycleCount.getCreated();
        this.updated = subCycleCount.getUpdated();
    }

    /*
    required inside a query
    see CycleCountDaoImpl.getSubCycleCountSummary
     */
    public SubCycleCountDTO(long blockedShelfCount, long pendingShelfCount, long readyShelfCount, long inProgressShelfCount, long completedShelfCount) {
        this.blockedShelfCount = blockedShelfCount;
        this.pendingShelfCount = pendingShelfCount;
        this.readyShelfCount = readyShelfCount;
        this.inProgressShelfCount = inProgressShelfCount;
        this.completedShelfCount = completedShelfCount;
    }

    public void prepare(SubCycleCount subCycleCount) {
        this.subCycleCountCode = subCycleCount.getCode();
        this.statusCode = subCycleCount.getStatusCode();
        this.targetCompletion = subCycleCount.getTargetCompletion();
        this.completed = subCycleCount.getCompleted();
        this.created = subCycleCount.getCreated();
        this.updated = subCycleCount.getUpdated();
    }

    public String getSubCycleCountCode() {
        return subCycleCountCode;
    }

    public void setSubCycleCountCode(String subCycleCountCode) {
        this.subCycleCountCode = subCycleCountCode;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public Date getTargetCompletion() {
        return targetCompletion;
    }

    public void setTargetCompletion(Date targetCompletion) {
        this.targetCompletion = targetCompletion;
    }

    public Date getCompleted() {
        return completed;
    }

    public void setCompleted(Date completed) {
        this.completed = completed;
    }

    public long getCompletedShelfCount() {
        return completedShelfCount;
    }

    public void setCompletedShelfCount(long completedShelfCount) {
        this.completedShelfCount = completedShelfCount;
    }

    public long getPendingShelfCount() {
        return pendingShelfCount;
    }

    public void setPendingShelfCount(long pendingShelfCount) {
        this.pendingShelfCount = pendingShelfCount;
    }

    public long getBlockedShelfCount() {
        return blockedShelfCount;
    }

    public void setBlockedShelfCount(long blockedShelfCount) {
        this.blockedShelfCount = blockedShelfCount;
    }

    public long getReadyShelfCount() {
        return readyShelfCount;
    }

    public void setReadyShelfCount(long readyShelfCount) {
        this.readyShelfCount = readyShelfCount;
    }

    public long getInProgressShelfCount() {
        return inProgressShelfCount;
    }

    public void setInProgressShelfCount(long inProgressShelfCount) {
        this.inProgressShelfCount = inProgressShelfCount;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public List<ShelfDTO> getShelves() {
        return shelves;
    }

    public void setShelves(List<ShelfDTO> shelves) {
        this.shelves = shelves;
    }

    public static class ShelfDTO {
        private String  shelfCode;
        private String  subCycleCountCode;
        private String  username;
        private String  zoneName;
        private long    quantity;
        private long    putawayPendingCount;
        private long    picklistPendingCount;
        private long    foundQuantity;
        private long    extraQuantity;
        private long    missingQuantity;
        private long    nonBarcodedCount;
        private String  statusCode;
        private boolean discarded;
        private Date    updated;

        public ShelfDTO() {
        }

        public ShelfDTO(String subCycleCountCode, String shelfCode, String username, String zoneName, Date updated) {
            this.subCycleCountCode = subCycleCountCode;
            this.shelfCode = shelfCode;
            this.username = username;
            this.zoneName = zoneName;
            this.updated = updated;
        }

        public String getShelfCode() {
            return shelfCode;
        }

        public void setShelfCode(String shelfCode) {
            this.shelfCode = shelfCode;
        }

        public String getSubCycleCountCode() {
            return subCycleCountCode;
        }

        public void setSubCycleCountCode(String subCycleCountCode) {
            this.subCycleCountCode = subCycleCountCode;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getZoneName() {
            return zoneName;
        }

        public void setZoneName(String zoneName) {
            this.zoneName = zoneName;
        }

        public long getQuantity() {
            return quantity;
        }

        public void setQuantity(long quantity) {
            this.quantity = quantity;
        }

        public long getPutawayPendingCount() {
            return putawayPendingCount;
        }

        public void setPutawayPendingCount(long putawayPendingCount) {
            this.putawayPendingCount = putawayPendingCount;
        }

        public long getPicklistPendingCount() {
            return picklistPendingCount;
        }

        public void setPicklistPendingCount(long packagePengingCount) {
            this.picklistPendingCount = packagePengingCount;
        }

        public long getFoundQuantity() {
            return foundQuantity;
        }

        public void setFoundQuantity(long foundQuantity) {
            this.foundQuantity = foundQuantity;
        }

        public long getExtraQuantity() {
            return extraQuantity;
        }

        public void setExtraQuantity(long extraQuantity) {
            this.extraQuantity = extraQuantity;
        }

        public long getMissingQuantity() {
            return missingQuantity;
        }

        public void setMissingQuantity(long missingQuantity) {
            this.missingQuantity = missingQuantity;
        }

        public long getNonBarcodedCount() {
            return nonBarcodedCount;
        }

        public void setNonBarcodedCount(long nonBarcodedCount) {
            this.nonBarcodedCount = nonBarcodedCount;
        }

        public String getStatusCode() {
            return statusCode;
        }

        public void setStatusCode(String statusCode) {
            this.statusCode = statusCode;
        }

        public boolean isDiscarded() {
            return discarded;
        }

        public void setDiscarded(boolean discarded) {
            this.discarded = discarded;
        }

        public Date getUpdated() {
            return updated;
        }

        public void setUpdated(Date updated) {
            this.updated = updated;
        }
    }
}
