/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jan 10, 2012
 *  @author singla
 */
package com.uniware.core.api.saleorder;

import com.unifier.core.api.base.ServiceResponse;

public class GetSaleOrderResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 6284414547113409023L;

    public GetSaleOrderResponse() {

    }

    public GetSaleOrderResponse(boolean successful, String message) {
        super(successful, message);
    }

    private SaleOrderDetailDTO saleOrderDTO;
    
    private boolean            isFailedOrderFetch;
    
    private boolean            isRefreshEnabled;  

    /**
     * @return the saleOrderDTO
     */
    public SaleOrderDetailDTO getSaleOrderDTO() {
        return saleOrderDTO;
    }

    /**
     * @param saleOrderDTO the saleOrderDTO to set
     */
    public void setSaleOrderDTO(SaleOrderDetailDTO saleOrderDTO) {
        this.saleOrderDTO = saleOrderDTO;
    }

    /**
     * @return the isFailedOrderFetch
     */
    public boolean isFailedOrderFetch() {
        return isFailedOrderFetch;
    }

    /**
     * @param isFailedOrderFetch the isFailedOrderFetch to set
     */
    public void setFailedOrderFetch(boolean isFailedOrderFetch) {
        this.isFailedOrderFetch = isFailedOrderFetch;
    }

    public boolean isRefreshEnabled() {
        return isRefreshEnabled;
    }

    public void setRefreshEnabled(boolean isRefreshEnabled) {
        this.isRefreshEnabled = isRefreshEnabled;
    }
   
}
