/*
 * Copyright 2016 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 1/9/16 7:04 PM
 * @author bhuvneshwarkumar
 */

package com.uniware.core.api.packer;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;
import com.uniware.core.entity.Item;

/**
 * Created by bhuvneshwarkumar on 01/09/16.
 */
public class RemoveItemFromPicklistRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 4782844397262826294L;

    @NotBlank
    private String  picklistCode;

    @NotBlank
    private String  itemCode;

    @NotNull
    private Integer userId;
    
    private String  rejectionReason;

    public String getPicklistCode() {
        return picklistCode;
    }

    public void setPicklistCode(String picklistCode) {
        this.picklistCode = picklistCode;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getRejectionReason() {
        return rejectionReason;
    }

    public void setRejectionReason(String rejectionReason) {
        this.rejectionReason = rejectionReason;
    }
}
