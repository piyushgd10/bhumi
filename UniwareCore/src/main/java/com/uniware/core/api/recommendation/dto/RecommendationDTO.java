/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Nov 25, 2015
 *  @author akshay
 */
package com.uniware.core.api.recommendation.dto;

import java.util.Date;

import com.uniware.core.vo.PriceRecommendationVO;

public class RecommendationDTO {

    private String             id;
    private String             recommendationStatus;
    private String             recommendationReason;
    private String             recommendationProvider;
    private String             closedBy;
    private String             createdBy;
    private String             closureReason;
    private Date               created;
    private Date               expiryTime;
    private Date               closureTime;
    private RecommendationBody recommendationBody;

    public RecommendationDTO() {
    }

    public RecommendationDTO(PriceRecommendationVO rec, RecommendationBody recommendationBody) {
        this.id = rec.getId();
        this.recommendationStatus = rec.getRecommendationStatus();
        this.recommendationReason = rec.getRecommendationReason();
        this.closedBy = rec.getClosedBy();
        this.closureReason = rec.getClosureReason();
        this.createdBy = rec.getCreatedBy();
        this.created = rec.getCreated();
        this.expiryTime = rec.getExpiryTime();
        this.closureTime = rec.getUpdated();
        this.recommendationProvider = rec.getRecommendationProvider();
        this.recommendationBody = recommendationBody;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRecommendationStatus() {
        return recommendationStatus;
    }

    public void setRecommendationStatus(String recommendationStatus) {
        this.recommendationStatus = recommendationStatus;
    }

    public String getRecommendationReason() {
        return recommendationReason;
    }

    public void setRecommendationReason(String recommendationReason) {
        this.recommendationReason = recommendationReason;
    }

    public String getRecommendationProvider() {
        return recommendationProvider;
    }

    public void setRecommendationProvider(String recommendationProvider) {
        this.recommendationProvider = recommendationProvider;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getExpiryTime() {
        return expiryTime;
    }

    public void setExpiryTime(Date expiryTime) {
        this.expiryTime = expiryTime;
    }

    public String getClosedBy() {
        return closedBy;
    }

    public void setClosedBy(String closedBy) {
        this.closedBy = closedBy;
    }

    public String getClosureReason() {
        return closureReason;
    }

    public void setClosureReason(String closureReason) {
        this.closureReason = closureReason;
    }

    public Date getClosureTime() {
        return closureTime;
    }

    public void setClosureTime(Date closureTime) {
        this.closureTime = closureTime;
    }

    public RecommendationBody getRecommendationBody() {
        return recommendationBody;
    }

    public void setRecommendationBody(RecommendationBody recommendationBody) {
        this.recommendationBody = recommendationBody;
    }
    
}
