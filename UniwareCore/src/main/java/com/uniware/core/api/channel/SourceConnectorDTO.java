/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 18-Apr-2013
 *  @author unicom
 */
package com.uniware.core.api.channel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.uniware.core.entity.SourceConnectorParameter;
import com.uniware.core.entity.SourceConnector;

/**
 * @author Sunny Agarwal
 */
public class SourceConnectorDTO implements Serializable {

    private String                            name;
    private String                            displayName;
    private String                            helpText;
    private int                               priority;
    private boolean                           thirdParty;
    private boolean                           requiredInOrderSync;
    private boolean                           requiredInInventorySync;
    private boolean                           requiredInReconciliationSync;
    private List<SourceConnectorParameterDTO> sourceConnectorParameters = new ArrayList<SourceConnectorParameterDTO>();

    public SourceConnectorDTO() {
    }

    public SourceConnectorDTO(SourceConnector sc) {
        name = sc.getName();
        displayName = sc.getDisplayName();
        helpText = sc.getHelpText();
        priority = sc.getPriority();
        thirdParty = sc.isThirdParty();
        requiredInOrderSync = sc.isRequiredInOrderSync();
        requiredInInventorySync = sc.isRequiredInInventorySync();
        requiredInReconciliationSync = sc.isRequiredInReconciliationSync();
        for (SourceConnectorParameter scp : sc.getSourceConnectorParameters()) {
            sourceConnectorParameters.add(new SourceConnectorParameterDTO(scp));
        }
        Collections.sort(sourceConnectorParameters, new Comparator<SourceConnectorParameterDTO>() {
            @Override
            public int compare(SourceConnectorParameterDTO o1, SourceConnectorParameterDTO o2) {
                return o1.getPriority() - o2.getPriority();
            }
        });
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHelpText() {
        return helpText;
    }

    public void setHelpText(String helpText) {
        this.helpText = helpText;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public boolean isThirdParty() {
        return thirdParty;
    }

    public void setThirdParty(boolean thirdParty) {
        this.thirdParty = thirdParty;
    }

    public boolean isRequiredInOrderSync() {
        return requiredInOrderSync;
    }

    public void setRequiredInOrderSync(boolean requiredInOrderSync) {
        this.requiredInOrderSync = requiredInOrderSync;
    }

    public boolean isRequiredInInventorySync() {
        return requiredInInventorySync;
    }

    public void setRequiredInInventorySync(boolean requiredInInventorySync) {
        this.requiredInInventorySync = requiredInInventorySync;
    }

    public boolean isRequiredInReconciliationSync() {
        return requiredInReconciliationSync;
    }

    public void setRequiredInReconciliationSync(boolean requiredInReconciliationSync) {
        this.requiredInReconciliationSync = requiredInReconciliationSync;
    }

    public List<SourceConnectorParameterDTO> getSourceConnectorParameters() {
        return sourceConnectorParameters;
    }

    public void setSourceConnectorParameters(List<SourceConnectorParameterDTO> sourceConnectorParameters) {
        this.sourceConnectorParameters = sourceConnectorParameters;
    }

}
