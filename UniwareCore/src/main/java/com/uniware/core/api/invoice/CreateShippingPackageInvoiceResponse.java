package com.uniware.core.api.invoice;

import com.uniware.core.api.packer.PutbackItemDTO;

import java.util.List;

/**
 * Created by Sagar Sahni on 31/05/17.
 */
public class CreateShippingPackageInvoiceResponse extends CreateInvoiceResponse {
    private List<PutbackItemDTO> putbackItems;

    public List<PutbackItemDTO> getPutbackItems() {
        return putbackItems;
    }

    public void setPutbackItems(List<PutbackItemDTO> putbackItems) {
        this.putbackItems = putbackItems;
    }
}
