/*
 * Copyright 2016 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 2/9/16 2:20 PM
 * @author bhuvneshwarkumar
 */

package com.uniware.core.api.picker;

import com.unifier.core.api.base.ServiceResponse;

/**
 * Created by bhuvneshwarkumar on 02/09/16.
 */
public class AssignPicklistToUserResponse extends ServiceResponse {

    private static final long serialVersionUID = 6223079502994618690L;
}
