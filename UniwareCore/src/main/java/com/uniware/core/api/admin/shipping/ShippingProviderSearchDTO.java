/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 22-Jan-2012
 *  @author vibhu
 */
package com.uniware.core.api.admin.shipping;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Sunny
 */
public class ShippingProviderSearchDTO {

    public enum PacketType {
        VENDOR_SELF,
        CHANNEL
    }

    private String code;
    private String name;
    private Set<PacketType>        packetTypes     = new HashSet<>(1);
    private Set<ShippingMethodDTO> shippingMethods = new HashSet<ShippingMethodDTO>();

    public ShippingProviderSearchDTO() {
    }

    public ShippingProviderSearchDTO(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<PacketType> getPacketTypes() {
        return packetTypes;
    }

    public void setPacketTypes(Set<PacketType> packetTypes) {
        this.packetTypes = packetTypes;
    }

    public Set<ShippingMethodDTO> getShippingMethods() {
        return shippingMethods;
    }

    public void setShippingMethods(Set<ShippingMethodDTO> shippingMethods) {
        this.shippingMethods = shippingMethods;
    }
}