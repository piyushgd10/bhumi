/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 02-Apr-2015
 *  @author unicommerce
 */
package com.uniware.core.api.putaway;

import com.unifier.core.api.base.ServiceResponse;

import java.util.ArrayList;
import java.util.List;

public class SplitPutawayItemResponse extends ServiceResponse{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private List<PutawayItemDTO> putawayItemDTOs = new ArrayList<>();

    public List<PutawayItemDTO> getPutawayItemDTOs() {
        return putawayItemDTOs;
    }

    public void setPutawayItemDTOs(List<PutawayItemDTO> putawayItemDTOs) {
        this.putawayItemDTOs = putawayItemDTOs;
    }
}
