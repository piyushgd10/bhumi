/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 11-Apr-2012
 *  @author vibhu
 */
package com.uniware.core.api.purchase;

import com.unifier.core.api.base.ServiceRequest;
import com.unifier.core.pagination.SearchOptions;

/**
 * @author vibhu
 */
public class GetBackOrderItemsRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private Integer           vendorId;
    private String            itemTypeName;
    private String            categoryCode;
    private Integer           userId;
    private Boolean           noVendors;

    private SearchOptions     searchOptions;

    /**
     * @return the vendorId
     */
    public Integer getVendorId() {
        return vendorId;
    }

    /**
     * @param vendorId the vendorId to set
     */
    public void setVendorId(Integer vendorId) {
        this.vendorId = vendorId;
    }

    public Boolean getNoVendors() {
        return noVendors;
    }

    public void setNoVendors(Boolean noVendors) {
        this.noVendors = noVendors;
    }

    /**
     * @return the itemTypeName
     */
    public String getItemTypeName() {
        return itemTypeName;
    }

    /**
     * @param itemTypeName the itemTypeName to set
     */
    public void setItemTypeName(String itemTypeName) {
        this.itemTypeName = itemTypeName;
    }

    /**
     * @return the categoryCode
     */
    public String getCategoryCode() {
        return categoryCode;
    }

    /**
     * @param categoryCode the categoryCode to set
     */
    public void setCategoryCode(String categoryCode) {
        this.categoryCode = categoryCode;
    }

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return the searchOptions
     */
    public SearchOptions getSearchOptions() {
        return searchOptions;
    }

    /**
     * @param searchOptions the searchOptions to set
     */
    public void setSearchOptions(SearchOptions searchOptions) {
        this.searchOptions = searchOptions;
    }

}
