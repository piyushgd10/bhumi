/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 02-Jun-2014
 *  @author unicom
 */
package com.uniware.core.api.saleorder;

import java.util.ArrayList;
import java.util.List;

import com.unifier.core.api.validation.WsError;
import com.uniware.core.api.model.WsSaleOrder;

public class AddressDetailErrorDTO {
    private WsSaleOrder   saleOrder;
    private List<WsError> errors = new ArrayList<>();

    public WsSaleOrder getSaleOrder() {
        return saleOrder;
    }

    public void setSaleOrder(WsSaleOrder saleOrder) {
        this.saleOrder = saleOrder;
    }

    public List<WsError> getErrors() {
        return errors;
    }

    public void setErrors(List<WsError> errors) {
        this.errors = errors;
    }

}
