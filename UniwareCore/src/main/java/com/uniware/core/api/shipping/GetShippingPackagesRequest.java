/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 17-Dec-2013
 *  @author akshay
 */
package com.uniware.core.api.shipping;

import com.unifier.core.api.base.ServiceRequest;

public class GetShippingPackagesRequest extends ServiceRequest{

    /**
     * 
     */
    private static final long serialVersionUID = -4611912682299953808L;

    private String            statusCode;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

}
