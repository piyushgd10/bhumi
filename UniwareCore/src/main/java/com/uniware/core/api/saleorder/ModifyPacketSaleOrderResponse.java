/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 14, 2012
 *  @author singla
 */
package com.uniware.core.api.saleorder;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author singla
 */
public class ModifyPacketSaleOrderResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -7203333525075904410L;

}
