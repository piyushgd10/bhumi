/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 10-Aug-2012
 *  @author vibhu
 */
package com.uniware.core.api.saleorder;

import com.unifier.core.api.base.ServiceResponse;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @author vibhu
 */
public class GetSaleOrderItemAlternateResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long         serialVersionUID = 5101637499639311401L;

    private SaleOrderItemAlternateDTO saleOrderItemAlternateDTO;

    /**
     * @return the saleOrderItemAlternateDTO
     */
    public SaleOrderItemAlternateDTO getSaleOrderItemAlternateDTO() {
        return saleOrderItemAlternateDTO;
    }

    /**
     * @param saleOrderItemAlternateDTO the saleOrderItemAlternateDTO to set
     */
    public void setSaleOrderItemAlternateDTO(SaleOrderItemAlternateDTO saleOrderItemAlternateDTO) {
        this.saleOrderItemAlternateDTO = saleOrderItemAlternateDTO;
    }

    public static class SaleOrderItemAlternateDTO {
        private String                                    statusCode;
        private List<SaleOrderItemAlternateSuggestionDTO> suggestions = new ArrayList<GetSaleOrderItemAlternateResponse.SaleOrderItemAlternateSuggestionDTO>();

        /**
         * @return the statusCode
         */
        public String getStatusCode() {
            return statusCode;
        }

        /**
         * @param statusCode the statusCode to set
         */
        public void setStatusCode(String statusCode) {
            this.statusCode = statusCode;
        }

        /**
         * @return the suggestions
         */
        public List<SaleOrderItemAlternateSuggestionDTO> getSuggestions() {
            return suggestions;
        }

        /**
         * @param suggestions the suggestions to set
         */
        public void setSuggestions(List<SaleOrderItemAlternateSuggestionDTO> suggestions) {
            this.suggestions = suggestions;
        }

        /**
         * @param suggestionDTO
         */
        public void addSaleOrderItemAlternateSuggestionDTO(SaleOrderItemAlternateSuggestionDTO suggestionDTO) {
            this.suggestions.add(suggestionDTO);
        }

    }

    public static class SaleOrderItemAlternateSuggestionDTO {
        private String     itemTypeSKU;
        private String     itemTypeName;
        private int        itemTypeId;
        private BigDecimal amountDifference;

        /**
         * @return the itemTypeSKU
         */
        public String getItemTypeSKU() {
            return itemTypeSKU;
        }

        /**
         * @param itemTypeSKU the itemTypeSKU to set
         */
        public void setItemTypeSKU(String itemTypeSKU) {
            this.itemTypeSKU = itemTypeSKU;
        }

        /**
         * @return the itemTypeName
         */
        public String getItemTypeName() {
            return itemTypeName;
        }

        /**
         * @param itemTypeName the itemTypeName to set
         */
        public void setItemTypeName(String itemTypeName) {
            this.itemTypeName = itemTypeName;
        }

        /**
         * @return the itemTypeId
         */
        public int getItemTypeId() {
            return itemTypeId;
        }

        /**
         * @param itemTypeId the itemTypeId to set
         */
        public void setItemTypeId(int itemTypeId) {
            this.itemTypeId = itemTypeId;
        }

        /**
         * @return the amountDifference
         */
        public BigDecimal getAmountDifference() {
            return amountDifference;
        }

        /**
         * @param amountDifference the amountDifference to set
         */
        public void setAmountDifference(BigDecimal amountDifference) {
            this.amountDifference = amountDifference;
        }

    }
}
