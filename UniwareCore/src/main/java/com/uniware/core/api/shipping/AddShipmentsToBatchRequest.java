/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 04-Apr-2014
 *  @author karunsingla
 */
package com.uniware.core.api.shipping;

import java.util.List;

import org.hibernate.validator.constraints.NotEmpty;

import com.unifier.core.api.base.ServiceRequest;

public class AddShipmentsToBatchRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -4351991265553683504L;

    private String            shipmentBatchCode;

    @NotEmpty
    private List<String>      shippingPackageCodes;

    public AddShipmentsToBatchRequest() {
        super();
    }

    public AddShipmentsToBatchRequest(String shipmentBatchCode, List<String> shippingPackageCodes) {
        super();
        this.shipmentBatchCode = shipmentBatchCode;
        this.shippingPackageCodes = shippingPackageCodes;
    }

    public String getShipmentBatchCode() {
        return shipmentBatchCode;
    }

    public void setShipmentBatchCode(String shipmentBatchCode) {
        this.shipmentBatchCode = shipmentBatchCode;
    }

    public List<String> getShippingPackageCodes() {
        return shippingPackageCodes;
    }

    public void setShippingPackageCodes(List<String> shippingPackageCodes) {
        this.shippingPackageCodes = shippingPackageCodes;
    }

}
