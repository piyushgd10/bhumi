package com.uniware.core.api.validation;

import com.unifier.core.api.base.ServiceRequest;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Created by admin on 9/29/15.
 */
public class SendOTPRequest extends ServiceRequest {

    private static final long serialVersionUID = 7151039368033818972L;

    @NotBlank
    private String mobile;

    @NotBlank
    private String email;

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
