/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jan 20, 2012
 *  @author sunny
 */
package com.uniware.core.api.invoice;

import java.math.BigDecimal;
import java.util.List;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author sunny
 */
public class GetTaxDetailsResponse extends ServiceResponse {

    /**
     *
     */
    private static final long serialVersionUID = 3773117733355945263L;

    private List<TaxDTO>      taxDetails;

    public List<TaxDTO> getTaxDetails() {
        return taxDetails;
    }

    public void setTaxDetails(List<TaxDTO> taxDetails) {
        this.taxDetails = taxDetails;
    }

    public static class TaxDTO {
        private String     itemSku;
        private String     taxTypeCode;
        private BigDecimal tax           = BigDecimal.ZERO;
        private BigDecimal additionalTax = BigDecimal.ZERO;
        private BigDecimal totalTax      = BigDecimal.ZERO;

        public TaxDTO() {
        }

        public TaxDTO(String itemSku, String taxTypeCode, BigDecimal tax, BigDecimal additionalTax) {
            this.itemSku = itemSku;
            this.taxTypeCode = taxTypeCode;
            this.tax = tax;
            this.additionalTax = additionalTax;
            this.totalTax = this.tax.add(this.additionalTax);
        }

        public String getItemSku() {
            return itemSku;
        }

        public void setItemSku(String itemSku) {
            this.itemSku = itemSku;
        }

        public String getTaxTypeCode() {
            return taxTypeCode;
        }

        public void setTaxTypeCode(String taxTypeCode) {
            this.taxTypeCode = taxTypeCode;
        }

        public BigDecimal getTax() {
            return tax;
        }

        public void setTax(BigDecimal tax) {
            this.tax = tax;
        }

        public BigDecimal getAdditionalTax() {
            return additionalTax;
        }

        public void setAdditionalTax(BigDecimal additionalTax) {
            this.additionalTax = additionalTax;
        }

        public BigDecimal getTotalTax() {
            return totalTax;
        }

        public void setTotalTax(BigDecimal totalTax) {
            this.totalTax = totalTax;
        }
    }

}
