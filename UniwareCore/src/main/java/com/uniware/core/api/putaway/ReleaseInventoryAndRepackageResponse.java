/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 05-Jun-2014
 *  @author unicom
 */
package com.uniware.core.api.putaway;

import java.util.HashMap;
import java.util.Map;

import com.unifier.core.api.base.ServiceResponse;

public class ReleaseInventoryAndRepackageResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long                     serialVersionUID       = 1L;

    private Map<String, FailedShippingPackageDTO> failedShippingPackages = new HashMap<>();
    private Integer                               failedPackagesCount;

    public static class FailedShippingPackageDTO {

        private String code;
        private String failureReason;
        
        public FailedShippingPackageDTO() {
            
        }
        
        public FailedShippingPackageDTO(String code, String failureReason) {
            super();
            this.code = code;
            this.failureReason = failureReason;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getFailureReason() {
            return failureReason;
        }

        public void setFailureReason(String failureReason) {
            this.failureReason = failureReason;
        }

    }

    public Map<String, FailedShippingPackageDTO> getFailedShippingPackages() {
        return failedShippingPackages;
    }

    public void setFailedShippingPackages(Map<String, FailedShippingPackageDTO> failedShippingPackages) {
        this.failedShippingPackages = failedShippingPackages;
    }

    /**
     * @return the failedPackagesCount
     */
    public Integer getFailedPackagesCount() {
        return failedPackagesCount;
    }

    /**
     * @param failedPackagesCount the failedPackagesCount to set
     */
    public void setFailedPackagesCount(Integer failedPackagesCount) {
        failedPackagesCount = failedShippingPackages.size();
    }

}
