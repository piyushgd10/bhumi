/*
 * Copyright 2017 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 11/12/17
 * @author piyush
 */
package com.uniware.core.api.channel;

import com.unifier.core.api.base.ServiceResponse;

import java.util.List;
import java.util.Set;

public class EditChannelAssociatedFacilityResponse extends ServiceResponse {

    private List<ChannelAssociatedFacilityDTO> facilities;

    public List<ChannelAssociatedFacilityDTO> getFacilities() {
        return facilities;
    }

    public void setFacilities(List<ChannelAssociatedFacilityDTO> facilities) {
        this.facilities = facilities;
    }

}
