/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 28-Jun-2013
 *  @author karunsingla
 */
package com.uniware.core.api.invoice;

import com.unifier.core.api.base.ServiceResponse;

public class AddItemDetailsOnInvoiceResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 4321764399161719074L;

}
