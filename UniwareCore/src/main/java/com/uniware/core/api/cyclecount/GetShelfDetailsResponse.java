package com.uniware.core.api.cyclecount;

import com.unifier.core.api.base.ServiceResponse;

/**
 * Created by harshpal on 3/21/16.
 */
public class GetShelfDetailsResponse extends ServiceResponse {

    private SubCycleCountDTO.ShelfDTO shelf;

    public SubCycleCountDTO.ShelfDTO getShelf() {
        return shelf;
    }

    public void setShelf(SubCycleCountDTO.ShelfDTO shelf) {
        this.shelf = shelf;
    }
}
