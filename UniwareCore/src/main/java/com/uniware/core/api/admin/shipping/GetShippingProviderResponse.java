/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 13-Feb-2015
 *  @author parijat
 */
package com.uniware.core.api.admin.shipping;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author parijat
 *
 */
public class GetShippingProviderResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 7151039368033818972L;

    private ShippingProviderDetailDTO detailDTO;

    /**
     * @return the detailDTO
     */
    public ShippingProviderDetailDTO getDetailDTO() {
        return detailDTO;
    }

    /**
     * @param detailDTO the detailDTO to set
     */
    public void setDetailDTO(ShippingProviderDetailDTO detailDTO) {
        this.detailDTO = detailDTO;
    }

}
