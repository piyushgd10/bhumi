/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 27, 2012
 *  @author singla
 */
package com.uniware.core.api.model;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

/**
 * @author singla
 */
public class WsSaleOrderItemCombination {

    public WsSaleOrderItemCombination() {
    }

    public WsSaleOrderItemCombination(String combinationIdentifier, String combinationDescription) {
        this.combinationIdentifier = combinationIdentifier;
        this.combinationDescription = combinationDescription;
    }

    @NotBlank
    private String combinationIdentifier;

    @NotBlank
    @Length(max = 200)
    private String combinationDescription;

    /**
     * @return the combinationIdentifier
     */
    public String getCombinationIdentifier() {
        return combinationIdentifier;
    }

    /**
     * @param combinationIdentifier the combinationIdentifier to set
     */
    public void setCombinationIdentifier(String combinationIdentifier) {
        this.combinationIdentifier = combinationIdentifier;
    }

    /**
     * @return the combinationDescription
     */
    public String getCombinationDescription() {
        return combinationDescription;
    }

    /**
     * @param combinationDescription the combinationDescription to set
     */
    public void setCombinationDescription(String combinationDescription) {
        this.combinationDescription = combinationDescription;
    }

}
