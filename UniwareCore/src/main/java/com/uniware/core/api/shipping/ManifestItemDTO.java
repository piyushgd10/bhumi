/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 8, 2012
 *  @author singla
 */
package com.uniware.core.api.shipping;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.uniware.core.api.saleorder.AddressDTO;
import com.uniware.core.api.warehouse.ShippingPackageTypeDTO;

/**
 * @author singla
 */
public class ManifestItemDTO implements Serializable {

    /**
     * 
     */
    private static final long      serialVersionUID  = -4301592970921555897L;
    private String                 shippingPackageCode;
    private String                 shippingPackageStatusCode;
    private String                 invoiceCode;
    private String                 invoiceDisplayCode;
    private String                 trackingNumber;
    private Integer                quantity;
    private Integer                noOfBoxes;
    private Integer                weight;
    private BigDecimal             totalAmount;
    private BigDecimal             shippingCharges;
    private BigDecimal             collectableAmount;
    private String                 shippingMethod;
    private AddressDTO             shippingAddress;
    private ShippingPackageTypeDTO shippingPackageType;
    private String                 displayOrderCode;
    private boolean                cashOnDelivery;
    private String                 shippingProviderCode;
    private String                 shippingProviderName;
    private List<ManifestLineItem> manifestLineItems = new ArrayList<ManifestLineItem>();

    /**
     * @return the invoiceCode
     */
    public String getInvoiceCode() {
        return invoiceCode;
    }

    /**
     * @param invoiceCode the invoiceCode to set
     */
    public void setInvoiceCode(String invoiceCode) {
        this.invoiceCode = invoiceCode;
    }

    public String getInvoiceDisplayCode() {
        return invoiceDisplayCode;
    }

    public void setInvoiceDisplayCode(String invoiceDisplayCode) {
        this.invoiceDisplayCode = invoiceDisplayCode;
    }

    /**
     * @return the shippingCharges
     */
    public BigDecimal getShippingCharges() {
        return shippingCharges;
    }

    /**
     * @param shippingCharges the shippingCharges to set
     */
    public void setShippingCharges(BigDecimal shippingCharges) {
        this.shippingCharges = shippingCharges;
    }

    /**
     * @return the trackingNumber
     */
    public String getTrackingNumber() {
        return trackingNumber;
    }

    /**
     * @param trackingNumber the trackingNumber to set
     */
    public void setTrackingNumber(String trackingNumber) {
        this.trackingNumber = trackingNumber;
    }

    /**
     * @return the quantity
     */
    public Integer getQuantity() {
        return quantity;
    }

    /**
     * @param quantity the quantity to set
     */
    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    /**
     * @return the noOfBoxes
     */
    public Integer getNoOfBoxes() {
        return noOfBoxes;
    }

    /**
     * @param noOfBoxes the noOfBoxes to set
     */
    public void setNoOfBoxes(Integer noOfBoxes) {
        this.noOfBoxes = noOfBoxes;
    }

    /**
     * @return the weight
     */
    public Integer getWeight() {
        return weight;
    }

    /**
     * @param weight the weight to set
     */
    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    /**
     * @return the totalAmount
     */
    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    /**
     * @param totalAmount the totalAmount to set
     */
    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    /**
     * @return the shippingMethod
     */
    public String getShippingMethod() {
        return shippingMethod;
    }

    /**
     * @param shippingMethod the shippingMethod to set
     */
    public void setShippingMethod(String shippingMethod) {
        this.shippingMethod = shippingMethod;
    }

    /**
     * @return the shippingAddress
     */
    public AddressDTO getShippingAddress() {
        return shippingAddress;
    }

    /**
     * @param shippingAddress the shippingAddress to set
     */
    public void setShippingAddress(AddressDTO shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    /**
     * @return the cashOnDelivery
     */
    public boolean isCashOnDelivery() {
        return cashOnDelivery;
    }

    /**
     * @param cashOnDelivery the cashOnDelivery to set
     */
    public void setCashOnDelivery(boolean cashOnDelivery) {
        this.cashOnDelivery = cashOnDelivery;
    }
    
    public String getShippingProviderCode() {
        return shippingProviderCode;
    }

    public void setShippingProviderCode(String shippingProviderCode) {
        this.shippingProviderCode = shippingProviderCode;
    }

    public String getShippingProviderName() {
        return shippingProviderName;
    }

    public void setShippingProviderName(String shippingProviderName) {
        this.shippingProviderName = shippingProviderName;
    }

    /**
     * @return the shippingPackageCode
     */
    public String getShippingPackageCode() {
        return shippingPackageCode;
    }

    /**
     * @param shippingPackageCode the shippingPackageCode to set
     */
    public void setShippingPackageCode(String shippingPackageCode) {
        this.shippingPackageCode = shippingPackageCode;
    }

    
    public String getShippingPackageStatusCode() {
        return shippingPackageStatusCode;
    }

    public void setShippingPackageStatusCode(String shippingPackageStatusCode) {
        this.shippingPackageStatusCode = shippingPackageStatusCode;
    }

    /**
     * @return the displayOrderCode
     */
    public String getDisplayOrderCode() {
        return displayOrderCode;
    }

    /**
     * @param displayOrderCode the displayOrderCode to set
     */
    public void setDisplayOrderCode(String displayOrderCode) {
        this.displayOrderCode = displayOrderCode;
    }

    /**
     * @return the shippingPackageType
     */
    public ShippingPackageTypeDTO getShippingPackageType() {
        return shippingPackageType;
    }

    /**
     * @param shippingPackageType the shippingPackageType to set
     */
    public void setShippingPackageType(ShippingPackageTypeDTO shippingPackageType) {
        this.shippingPackageType = shippingPackageType;
    }

    /**
     * @return the collectableAmount
     */
    public BigDecimal getCollectableAmount() {
        return collectableAmount;
    }

    /**
     * @param collectableAmount the collectableAmount to set
     */
    public void setCollectableAmount(BigDecimal collectableAmount) {
        this.collectableAmount = collectableAmount;
    }

    public List<ManifestLineItem> getManifestLineItems() {
        return manifestLineItems;
    }

    public void setManifestLineItems(List<ManifestLineItem> manifestLineItems) {
        this.manifestLineItems = manifestLineItems;
    }
    
    public static class ManifestLineItem {
        private String                lineItemIdentifier;
        private String                itemName;
        private String                sellerSkuCode;
        private int                   quantity;

        public String getLineItemIdentifier() {
            return lineItemIdentifier;
        }

        public void setLineItemIdentifier(String lineItemIdentifier) {
            this.lineItemIdentifier = lineItemIdentifier;
        }

        public String getItemName() {
            return itemName;
        }

        public void setItemName(String itemName) {
            this.itemName = itemName;
        }

        public int getQuantity() {
            return quantity;
        }

        public void setQuantity(int quantity) {
            this.quantity = quantity;
        }

        public String getSellerSkuCode() {
            return sellerSkuCode;
        }

        public void setSellerSkuCode(String sellerSkuCode) {
            this.sellerSkuCode = sellerSkuCode;
        }
    }

}
