/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 14-Aug-2013
 *  @author parijat
 */
package com.uniware.core.api.channel;

import com.uniware.core.entity.Channel;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.Date;

/**
 * @author parijat
 */
@Document(collection = "channelInventorySyncStatus")
//@CompoundIndexes({ @CompoundIndex(name = "tenant_channel", def = "{'tenantCode' :  1, 'channelCode' :  1}", unique = true) })
public class ChannelInventorySyncStatusDTO implements Serializable {

    @Id
    private String                      id;

    private String                      channelCode;
    private String                      tenantCode;
    private Channel.SyncExecutionStatus syncExecutionStatus = Channel.SyncExecutionStatus.IDLE;
    private String                      message;
    private long                        totalMileStones;
    private long                        currentMileStone;
    private int                         lastSuccessfulImportCount;
    private long                        todaysSuccessfulImportCount;
    private int                         lastFailedImportCount;
    private long                        failedCount;
    private boolean                     lastSyncSuccessful;
    private Date                        lastSyncTime;
    private Date                        lastSyncFailedNotificationTime;
    private Date                        lastSuccessfulInventorySyncTime;

    public ChannelInventorySyncStatusDTO(String channelCode) {
        this.channelCode = channelCode;
    }

    public float getPercentageComplete() {
        if (totalMileStones == 0) {
            return 0;
        }
        return ((float) (currentMileStone * 10000 / totalMileStones)) / 100;
    }

    public Channel.SyncExecutionStatus getSyncExecutionStatus() {
        return syncExecutionStatus;
    }

    public void setSyncExecutionStatus(Channel.SyncExecutionStatus syncExecutionStatus) {
        this.syncExecutionStatus = syncExecutionStatus;
    }

    public Date getLastSyncTime() {
        return lastSyncTime;
    }

    public void setLastSyncTime(Date lastSyncTime) {
        this.lastSyncTime = lastSyncTime;
    }

    public long getTotalMileStones() {
        return totalMileStones;
    }

    public void setTotalMileStones(long totalMileStones) {
        this.totalMileStones = totalMileStones;
    }

    public long getCurrentMileStone() {
        return currentMileStone;
    }

    public void setCurrentMileStone(long currentMileStone) {
        this.currentMileStone = currentMileStone;
    }

    public void reset() {
        syncExecutionStatus = Channel.SyncExecutionStatus.IDLE;
        totalMileStones = 0;
        currentMileStone = 0;
        lastSuccessfulImportCount = 0;
        lastFailedImportCount = 0;
        lastSyncSuccessful = false;
    }

    public void setMileStone(String message, int completedMilestones) {
        this.message = message;
        currentMileStone += completedMilestones;
    }

    public int getLastSuccessfulImportCount() {
        return lastSuccessfulImportCount;
    }

    public void setLastSuccessfulImportCount(int lastSuccessfulImportCount) {
        this.lastSuccessfulImportCount = lastSuccessfulImportCount;
    }

    public long getTodaysSuccessfulImportCount() {
        return todaysSuccessfulImportCount;
    }

    public void setTodaysSuccessfulImportCount(long todaysSuccessfulImportCount) {
        this.todaysSuccessfulImportCount = todaysSuccessfulImportCount;
    }

    public int getLastFailedImportCount() {
        return lastFailedImportCount;
    }

    public void setLastFailedImportCount(int lastFailedImportCount) {
        this.lastFailedImportCount = lastFailedImportCount;
    }

    public long getFailedCount() {
        return failedCount;
    }

    public Date getLastSyncFailedNotificationTime() {
        return lastSyncFailedNotificationTime;
    }

    public void setLastSyncFailedNotificationTime(Date lastSyncFailedNotificationTime) {
        this.lastSyncFailedNotificationTime = lastSyncFailedNotificationTime;
    }

    public void setFailedCount(long failedCount) {
        this.failedCount = failedCount;
    }

    public void incrementSuccessFulSyncCount(int size) {
        lastSuccessfulImportCount += size;
    }

    public void incrementFailedSyncCount(int size) {
        lastFailedImportCount += size;
    }

    public Date getLastSuccessfulInventorySyncTime() {
        return lastSuccessfulInventorySyncTime;
    }

    public void setLastSuccessfulInventorySyncTime(Date lastSuccessfulInventorySyncTime) {
        this.lastSuccessfulInventorySyncTime = lastSuccessfulInventorySyncTime;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    public String getTenantCode() {
        return tenantCode;
    }

    public void setTenantCode(String tenantCode) {
        this.tenantCode = tenantCode;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isLastSyncSuccessful() {
        return lastSyncSuccessful;
    }

    public void setLastSyncSuccessful(boolean lastSyncSuccessful) {
        this.lastSyncSuccessful = lastSyncSuccessful;
    }

    @Override
    public String toString() {
        return "ChannelInventorySyncStatusDTO{" + "id='" + id + '\'' + ", channelCode='" + channelCode + '\'' + ", tenantCode='" + tenantCode + '\'' + ", syncExecutionStatus="
                + syncExecutionStatus + ", message='" + message + '\'' + ", totalMileStones=" + totalMileStones + ", currentMileStone=" + currentMileStone
                + ", lastSuccessfulImportCount=" + lastSuccessfulImportCount + ", todaysSuccessfulImportCount=" + todaysSuccessfulImportCount + ", lastFailedImportCount="
                + lastFailedImportCount + ", failedCount=" + failedCount + ", lastSyncSuccessful=" + lastSyncSuccessful + ", lastSyncTime=" + lastSyncTime
                + ", lastSyncFailedNotificationTime=" + lastSyncFailedNotificationTime + ", lastSuccessfulInventorySyncTime=" + lastSuccessfulInventorySyncTime + '}';
    }
}
