/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 13-Aug-2013
 *  @author parijat
 */
package com.uniware.core.api.inventory;

import java.util.List;

import org.hibernate.validator.constraints.NotEmpty;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author parijat
 */
public class UpdateInventoryOnChannelRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long          serialVersionUID = -8047079192436300927L;
    @NotEmpty
    private List<String>                     channelCodes;

    public UpdateInventoryOnChannelRequest() {
        super();
    }

    /**
     * @param channelCode
     */
    public UpdateInventoryOnChannelRequest(List<String> channelCodes) {
        super();
        this.channelCodes = channelCodes;
    }

    public List<String> getChannelCodes() {
        return channelCodes;
    }

    public void setChannelCodes(List<String> channelCodes) {
        this.channelCodes = channelCodes;
    }

    /**
     * @return the channelCode
     */

}
