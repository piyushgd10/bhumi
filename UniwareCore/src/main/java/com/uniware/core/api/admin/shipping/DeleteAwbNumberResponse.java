/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 03-Jun-2012
 *  @author vibhu
 */
package com.uniware.core.api.admin.shipping;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author Pankaj
 */
public class DeleteAwbNumberResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 3388551590054156545L;

    private int               deletedNumber;

    /**
     * @return the addedNumber
     */
    public int getDeletedNumbers() {
        return deletedNumber;
    }

    /**
     * @param addedNumber the addedNumber to set
     */
    public void setDeletedNumbers(Integer deletedNumber) {
        this.deletedNumber = deletedNumber;
    }
}
