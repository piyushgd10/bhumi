/*
 * Copyright 2017 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 04/05/17
 * @author piyush
 */
package com.uniware.core.api.model;

import java.math.BigDecimal;

import com.uniware.core.entity.InvoiceItem;
import com.uniware.core.entity.InvoiceItemTax;
import javax.validation.constraints.Min;

/**
 * Used when generating return invoice where we want to apply same taxes which were calculated in forward invoice. Also
 * used while fetching completed orders from channel. Eg. THIRD_PARTY_WAREHOUSE like FBA,FA,SD+.
 */
public class WsTaxPercentageDetail {

    private String     taxTypeCode;
    @Min(value = 0)
    private BigDecimal vat               = BigDecimal.ZERO;
    @Min(value = 0)
    private BigDecimal cst               = BigDecimal.ZERO;
    @Min(value = 0)
    private BigDecimal cstFormc          = BigDecimal.ZERO;
    @Min(value = 0)
    private BigDecimal taxPercentage     = BigDecimal.ZERO;
    @Min(value = 0)
    private BigDecimal serviceTax        = BigDecimal.ZERO;
    @Min(value = 0)
    private BigDecimal additionalTax     = BigDecimal.ZERO;
    @Min(value = 0)
    private BigDecimal centralGst        = BigDecimal.ZERO;
    @Min(value = 0)
    private BigDecimal stateGst          = BigDecimal.ZERO;
    @Min(value = 0)
    private BigDecimal unionTerritoryGst = BigDecimal.ZERO;
    @Min(value = 0)
    private BigDecimal integratedGst     = BigDecimal.ZERO;
    @Min(value = 0)
    private BigDecimal compensationCess  = BigDecimal.ZERO;

    public String getTaxTypeCode() {
        return taxTypeCode;
    }

    public void setTaxTypeCode(String taxTypeCode) {
        this.taxTypeCode = taxTypeCode;
    }

    public BigDecimal getVat() {
        return vat;
    }

    public void setVat(BigDecimal vat) {
        this.vat = vat;
    }

    public BigDecimal getCst() {
        return cst;
    }

    public void setCst(BigDecimal cst) {
        this.cst = cst;
    }

    public BigDecimal getCstFormc() {
        return cstFormc;
    }

    public void setCstFormc(BigDecimal cstFormc) {
        this.cstFormc = cstFormc;
    }

    public BigDecimal getTaxPercentage() {
        return taxPercentage;
    }

    public void setTaxPercentage(BigDecimal taxPercentage) {
        this.taxPercentage = taxPercentage;
    }

    public BigDecimal getServiceTax() {
        return serviceTax;
    }

    public void setServiceTax(BigDecimal serviceTax) {
        this.serviceTax = serviceTax;
    }

    public BigDecimal getAdditionalTax() {
        return additionalTax;
    }

    public void setAdditionalTax(BigDecimal additionalTax) {
        this.additionalTax = additionalTax;
    }

    public BigDecimal getCentralGst() {
        return centralGst;
    }

    public void setCentralGst(BigDecimal centralGst) {
        this.centralGst = centralGst;
    }

    public BigDecimal getStateGst() {
        return stateGst;
    }

    public void setStateGst(BigDecimal stateGst) {
        this.stateGst = stateGst;
    }

    public BigDecimal getUnionTerritoryGst() {
        return unionTerritoryGst;
    }

    public void setUnionTerritoryGst(BigDecimal unionTerritoryGst) {
        this.unionTerritoryGst = unionTerritoryGst;
    }

    public BigDecimal getIntegratedGst() {
        return integratedGst;
    }

    public void setIntegratedGst(BigDecimal integratedGst) {
        this.integratedGst = integratedGst;
    }

    public BigDecimal getCompensationCess() {
        return compensationCess;
    }

    public void setCompensationCess(BigDecimal compensationCess) {
        this.compensationCess = compensationCess;
    }

    public static WsTaxPercentageDetail prepareWsTaxPercentageDetail(InvoiceItem invoiceItem) {
        WsTaxPercentageDetail taxPercentageDetail = new WsTaxPercentageDetail();
        taxPercentageDetail.setTaxTypeCode(invoiceItem.getTaxTypeCode());
        InvoiceItemTax spInvoiceItemTax = invoiceItem.getSellingPriceInvoiceItemTax();
        taxPercentageDetail.setVat(spInvoiceItemTax.getTaxPercentage());
        taxPercentageDetail.setCst(spInvoiceItemTax.getTaxPercentage());
        taxPercentageDetail.setAdditionalTax(spInvoiceItemTax.getAdditionalTaxPercentage());
        taxPercentageDetail.setTaxPercentage(spInvoiceItemTax.getTaxPercentage());
        taxPercentageDetail.setCentralGst(spInvoiceItemTax.getCentralGstPercentage());
        taxPercentageDetail.setStateGst(spInvoiceItemTax.getStateGstPercentage());
        taxPercentageDetail.setUnionTerritoryGst(spInvoiceItemTax.getUnionTerritoryGstPercentage());
        taxPercentageDetail.setIntegratedGst(spInvoiceItemTax.getIntegratedGstPercentage());
        taxPercentageDetail.setCompensationCess(spInvoiceItemTax.getCompensationCessPercentage());
        return taxPercentageDetail;
    }

    @Override public String toString() {
        return "WsTaxPercentageDetail{" + "taxTypeCode='" + taxTypeCode + '\'' + ", vat=" + vat + ", cst=" + cst
                + ", cstFormc=" + cstFormc + ", taxPercentage=" + taxPercentage + ", serviceTax=" + serviceTax
                + ", additionalTax=" + additionalTax + ", centralGst=" + centralGst + ", stateGst=" + stateGst
                + ", unionTerritoryGst=" + unionTerritoryGst + ", integratedGst=" + integratedGst
                + ", compensationCess=" + compensationCess + '}';
    }
}
