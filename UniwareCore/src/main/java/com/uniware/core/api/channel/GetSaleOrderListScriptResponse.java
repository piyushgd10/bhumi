/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 23-Apr-2013
 *  @author unicom
 */
package com.uniware.core.api.channel;

import java.util.List;
import java.util.Map;

import com.unifier.core.api.base.ServiceResponse;
import com.unifier.core.utils.XMLParser.Element;

/**
 * @author Sunny Agarwal
 */
public class GetSaleOrderListScriptResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long   serialVersionUID = 1066791106292526555L;
    private List<Element>       saleOrderElements;
    private Map<String, Object> saleOrderCodeToOrderElement;
    private boolean             hasMoreResults;
    private Integer             totalPages;

    public void setSaleOrderElements(List<Element> saleOrderElements) {
        this.saleOrderElements = saleOrderElements;
    }

    public void setSaleOrderCodeToOrderElement(Map<String, Object> saleOrderCodeToOrderElement) {
        this.saleOrderCodeToOrderElement = saleOrderCodeToOrderElement;
    }

    public List<Element> getSaleOrderElements() {
        return saleOrderElements;
    }

    public Map<String, Object> getSaleOrderCodeToOrderElement() {
        return saleOrderCodeToOrderElement;
    }

    /**
     * @return the hasMoreResults
     */
    public boolean isHasMoreResults() {
        return hasMoreResults;
    }

    /**
     * @param hasMoreResults the hasMoreResults to set
     */
    public void setHasMoreResults(boolean hasMoreResults) {
        this.hasMoreResults = hasMoreResults;
    }

    /**
     * @return the totalPages
     */
    public Integer getTotalPages() {
        return totalPages;
    }

    /**
     * @param totalPages the totalPages to set
     */
    public void setTotalPages(Integer totalPages) {
        this.totalPages = totalPages;
    }

}