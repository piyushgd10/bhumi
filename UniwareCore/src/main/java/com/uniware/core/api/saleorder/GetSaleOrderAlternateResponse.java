package com.uniware.core.api.saleorder;

import com.unifier.core.api.base.ServiceResponse;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bhuvneshwarkumar on 25/08/15.
 */
public class GetSaleOrderAlternateResponse extends ServiceResponse {
    @NotEmpty
    private List<WsSOIAlternate> saleOrderItemAlternateDTOList = new ArrayList<>();

    public static class WsSOIAlternate {
        @NotBlank
        private Integer                                                     saleOrderItemAlternateId;
        @NotBlank
        private GetSaleOrderItemAlternateResponse.SaleOrderItemAlternateDTO saleOrderItemAlternateDTO;

        public WsSOIAlternate(Integer saleOrderItemAlternateId, GetSaleOrderItemAlternateResponse.SaleOrderItemAlternateDTO saleOrderItemAlternateDTO) {
            this.saleOrderItemAlternateId = saleOrderItemAlternateId;
            this.saleOrderItemAlternateDTO = saleOrderItemAlternateDTO;
        }

        public Integer getSaleOrderItemAlternateId() {
            return saleOrderItemAlternateId;
        }

        public void setSaleOrderItemAlternateId(Integer saleOrderItemAlternateId) {
            this.saleOrderItemAlternateId = saleOrderItemAlternateId;
        }

        public GetSaleOrderItemAlternateResponse.SaleOrderItemAlternateDTO getSaleOrderItemAlternateDTO() {
            return saleOrderItemAlternateDTO;
        }

        public void setSaleOrderItemAlternateDTO(GetSaleOrderItemAlternateResponse.SaleOrderItemAlternateDTO saleOrderItemAlternateDTO) {
            this.saleOrderItemAlternateDTO = saleOrderItemAlternateDTO;
        }
    }

    public List<WsSOIAlternate> getSaleOrderItemAlternateDTOList() {
        return saleOrderItemAlternateDTOList;
    }

    public void setSaleOrderItemAlternateDTOList(List<WsSOIAlternate> saleOrderItemAlternateDTOList) {
        this.saleOrderItemAlternateDTOList = saleOrderItemAlternateDTOList;
    }
}
