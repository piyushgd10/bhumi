/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 12-Jun-2014
 *  @author unicom
 */
package com.uniware.core.api.saleorder;

import com.uniware.core.entity.PaymentMethod;

public class PaymentMethodDTO {

    private String  code;
    private String  name;
    private boolean enabled;

    public PaymentMethodDTO() {

    }

    public PaymentMethodDTO(PaymentMethod pm) {
        code = pm.getCode();
        name = pm.getName();
        enabled = pm.isEnabled();
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

}
