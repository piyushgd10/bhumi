/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 16-Aug-2012
 *  @author praveeng
 */
package com.uniware.core.api.warehouse;

import com.unifier.core.api.base.ServiceResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * @author praveeng
 */
public class CreateShelvesWithPredefinedCodesResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 2960171985827335097L;
    List<String>              shelfCodes       = new ArrayList<String>();

    /**
     * @return the shelfCodes
     */
    public List<String> getShelfCodes() {
        return shelfCodes;
    }

    /**
     * @param shelfCodes the shelfCodes to set
     */
    public void setShelfCodes(List<String> shelfCodes) {
        this.shelfCodes = shelfCodes;
    }

}
