package com.uniware.core.api.cyclecount;

import java.util.Set;

import com.unifier.core.api.base.ServiceResponse;

/**
 * Created by harshpal on 2/26/16.
 */
public class CreateManualSubCycleCountResponse extends ServiceResponse {

    private String      subCycleCountCode;

    private Set<String> invalidShelfCodes;

    public String getSubCycleCountCode() {
        return subCycleCountCode;
    }

    public void setSubCycleCountCode(String subCycleCountCode) {
        this.subCycleCountCode = subCycleCountCode;
    }

    public Set<String> getInvalidShelfCodes() {
        return invalidShelfCodes;
    }

    public void setInvalidShelfCodes(Set<String> invalidShelfCodes) {
        this.invalidShelfCodes = invalidShelfCodes;
    }
}
