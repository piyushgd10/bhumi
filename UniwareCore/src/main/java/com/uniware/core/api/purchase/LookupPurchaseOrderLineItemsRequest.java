/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, May 6, 2015
 *  @author harsh
 */
package com.uniware.core.api.purchase;

import java.io.InputStream;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author harsh
 */
public class LookupPurchaseOrderLineItemsRequest extends ServiceRequest {

    private static final long serialVersionUID = 37656785081740876L;

    @NotBlank
    private String            vendorCode;

    @NotNull
    private InputStream       inputStream;

    public String getVendorCode() {
        return vendorCode;
    }

    public void setVendorCode(String vendorCode) {
        this.vendorCode = vendorCode;
    }

    public InputStream getInputStream() {
        return inputStream;
    }

    public void setInputStream(InputStream inputStream) {
        this.inputStream = inputStream;
    }

}
