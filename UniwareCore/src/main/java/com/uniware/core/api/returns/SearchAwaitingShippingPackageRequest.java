/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 29-Mar-2012
 *  @author vibhu
 */
package com.uniware.core.api.returns;

import com.unifier.core.api.base.ServiceRequest;
import com.unifier.core.pagination.SearchOptions;

/**
 * @author vibhu
 */
public class SearchAwaitingShippingPackageRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 6134594963166598899L;
    private String            shippingPackageCode;
    private String            shippingPackageStatus;
    private String            shippingProvider;
    private String            trackingNumber;
    private String            saleOrderCode;
    private SearchOptions     searchOptions;

    /**
     * @return the shippingPackageCode
     */
    public String getShippingPackageCode() {
        return shippingPackageCode;
    }

    /**
     * @param shippingPackageCode the shippingPackageCode to set
     */
    public void setShippingPackageCode(String shippingPackageCode) {
        this.shippingPackageCode = shippingPackageCode;
    }

    /**
     * @return the shippingProvider
     */
    public String getShippingProvider() {
        return shippingProvider;
    }

    /**
     * @param shippingProvider the shippingProvider to set
     */
    public void setShippingProvider(String shippingProvider) {
        this.shippingProvider = shippingProvider;
    }

    /**
     * @return the trackingNumber
     */
    public String getTrackingNumber() {
        return trackingNumber;
    }

    /**
     * @param trackingNumber the trackingNumber to set
     */
    public void setTrackingNumber(String trackingNumber) {
        this.trackingNumber = trackingNumber;
    }

    /**
     * @return the saleOrderCode
     */
    public String getSaleOrderCode() {
        return saleOrderCode;
    }

    /**
     * @param saleOrderCode the saleOrderCode to set
     */
    public void setSaleOrderCode(String saleOrderCode) {
        this.saleOrderCode = saleOrderCode;
    }

    /**
     * @return the searchOptions
     */
    public SearchOptions getSearchOptions() {
        return searchOptions;
    }

    /**
     * @param searchOptions the searchOptions to set
     */
    public void setSearchOptions(SearchOptions searchOptions) {
        this.searchOptions = searchOptions;
    }

    /**
     * @return the shippingPackageStatus
     */
    public String getShippingPackageStatus() {
        return shippingPackageStatus;
    }

    /**
     * @param shippingPackageStatus the shippingPackageStatus to set
     */
    public void setShippingPackageStatus(String shippingPackageStatus) {
        this.shippingPackageStatus = shippingPackageStatus;
    }

}