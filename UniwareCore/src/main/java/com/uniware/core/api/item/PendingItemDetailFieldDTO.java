/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 17-Dec-2015
 *  @author akshay
 */
package com.uniware.core.api.item;

import java.util.ArrayList;
import java.util.List;

public class PendingItemDetailFieldDTO {

    private String                   name;
    private String                   skuCode;
    private String                   itemCode;
    private List<ItemDetailFieldDTO> pendingItemDetailFields = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSkuCode() {
        return skuCode;
    }

    public void setSkuCode(String skuCode) {
        this.skuCode = skuCode;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public List<ItemDetailFieldDTO> getPendingItemDetailFields() {
        return pendingItemDetailFields;
    }

    public void setPendingItemDetailFields(List<ItemDetailFieldDTO> pendingItemDetailFields) {
        this.pendingItemDetailFields = pendingItemDetailFields;
    }

}
