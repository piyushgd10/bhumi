/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 *  @version     1.0, 20-Jul-2012
 *  @author praveeng
 */
package com.uniware.core.api.catalog;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author praveeng
 */
public class CreateTaxTypeResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -7818890047954200886L;
    private TaxTypeDTO taxTypeDTO;
    /**
     * @return the taxTypeDTO
     */
    public TaxTypeDTO getTaxTypeDTO() {
        return taxTypeDTO;
    }
    /**
     * @param taxTypeDTO the taxTypeDTO to set
     */
    public void setTaxTypeDTO(TaxTypeDTO taxTypeDTO) {
        this.taxTypeDTO = taxTypeDTO;
    }



}
