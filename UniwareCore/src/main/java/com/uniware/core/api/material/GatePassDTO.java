/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 02-Jul-2012
 *  @author vibhu
 */
package com.uniware.core.api.material;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.unifier.core.api.customfields.CustomFieldMetadataDTO;

/**
 * @author vibhu
 */
public class GatePassDTO {

    private int                          id;
    private String                       code;
    private String                       statusCode;
    private String                       type;
    private String                       invoiceCode;
    private String                       invoiceDisplayCode;
    private String                       returnInvoiceCode;
    private String                       returnInvoiceDisplayCode;
    private String                       toPartyName;
    private String                       reference;
    private String                       purpose;
    private String                       username;
    private Date                         created;
    private List<GatePassItemDTO>        gatePassItemDTOs = new ArrayList<GatePassItemDTO>();
    private List<CustomFieldMetadataDTO> customFieldValues;

    public String getInvoiceDisplayCode() {
        return invoiceDisplayCode;
    }

    public void setInvoiceDisplayCode(String invoiceDisplayCode) {
        this.invoiceDisplayCode = invoiceDisplayCode;
    }

    public String getReturnInvoiceDisplayCode() {
        return returnInvoiceDisplayCode;
    }

    public void setReturnInvoiceDisplayCode(String returnInvoiceDisplayCode) {
        this.returnInvoiceDisplayCode = returnInvoiceDisplayCode;
    }

    /**
     * @return the customFieldValues
     */
    public List<CustomFieldMetadataDTO> getCustomFieldValues() {
        return customFieldValues;
    }

    /**
     * @param customFieldValues the customFieldValues to set
     */
    public void setCustomFieldValues(List<CustomFieldMetadataDTO> customFieldValues) {
        this.customFieldValues = customFieldValues;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the toPartyName
     */
    public String getToPartyName() {
        return toPartyName;
    }

    /**
     * @param toPartyName the toPartyName to set
     */
    public void setToPartyName(String toPartyName) {
        this.toPartyName = toPartyName;
    }

    /**
     * @return the reference
     */
    public String getReference() {
        return reference;
    }

    /**
     * @param reference the reference to set
     */
    public void setReference(String reference) {
        this.reference = reference;
    }

    /**
     * @return the purpose
     */
    public String getPurpose() {
        return purpose;
    }

    /**
     * @param purpose the purpose to set
     */
    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the statusCode
     */
    public String getStatusCode() {
        return statusCode;
    }

    /**
     * @param statusCode the statusCode to set
     */
    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getInvoiceCode() {
        return invoiceCode;
    }

    public void setInvoiceCode(String invoiceCode) {
        this.invoiceCode = invoiceCode;
    }

    public String getReturnInvoiceCode() {
        return returnInvoiceCode;
    }

    public void setReturnInvoiceCode(String returnInvoiceCode) {
        this.returnInvoiceCode = returnInvoiceCode;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the created
     */
    public Date getCreated() {
        return created;
    }

    /**
     * @param created the created to set
     */
    public void setCreated(Date created) {
        this.created = created;
    }

    /**
     * @return the gatePassItemDTOs
     */
    public List<GatePassItemDTO> getGatePassItemDTOs() {
        return gatePassItemDTOs;
    }

    /**
     * @param gatePassItemDTOs the gatePassItemDTOs to set
     */
    public void setGatePassItemDTOs(List<GatePassItemDTO> gatePassItemDTOs) {
        this.gatePassItemDTOs = gatePassItemDTOs;
    }

    public static class GatePassItemDTO {
        private Integer    gatePassItemId;
        private String     gatePassItemStatus;
        private String     itemCode;
        private String     itemStatus;
        private String     inventoryType;
        private String     itemTypeName;
        private String     itemTypeSKU;
        private String     itemTypeImageUrl;
        private String     itemTypePageUrl;
        private String     inflowReceiptCode;
        private String     itemCondition;
        private String     reason;
        private BigDecimal total         = BigDecimal.ZERO;
        private BigDecimal unitPrice     = BigDecimal.ZERO;
        private BigDecimal taxPercentage = BigDecimal.ZERO;
        private BigDecimal integratedGstPercentage     = BigDecimal.ZERO;
        private BigDecimal unionTerritoryGstPercentage = BigDecimal.ZERO;
        private BigDecimal stateGstPercentage          = BigDecimal.ZERO;
        private BigDecimal centralGstPercentage        = BigDecimal.ZERO;
        private BigDecimal compensationCessPercentage  = BigDecimal.ZERO;
        private int        quantity;
        private int        pendingQuantity;

        /**
         * @return the gatePassItemId
         */
        public Integer getGatePassItemId() {
            return gatePassItemId;
        }

        /**
         * @param gatePassItemId the gatePassItemId to set
         */
        public void setGatePassItemId(Integer gatePassItemId) {
            this.gatePassItemId = gatePassItemId;
        }

        /**
         * @return the gatePassItemStatus
         */
        public String getGatePassItemStatus() {
            return gatePassItemStatus;
        }

        /**
         * @param gatePassItemStatus the gatePassItemStatus to set
         */
        public void setGatePassItemStatus(String gatePassItemStatus) {
            this.gatePassItemStatus = gatePassItemStatus;
        }

        /**
         * @return the itemCode
         */
        public String getItemCode() {
            return itemCode;
        }

        /**
         * @param itemCode the itemCode to set
         */
        public void setItemCode(String itemCode) {
            this.itemCode = itemCode;
        }

        /**
         * @return the itemStatus
         */
        public String getItemStatus() {
            return itemStatus;
        }

        /**
         * @param itemStatus the itemStatus to set
         */
        public void setItemStatus(String itemStatus) {
            this.itemStatus = itemStatus;
        }

        /**
         * @return the itemTypeName
         */
        public String getItemTypeName() {
            return itemTypeName;
        }

        /**
         * @param itemTypeName the itemTypeName to set
         */
        public void setItemTypeName(String itemTypeName) {
            this.itemTypeName = itemTypeName;
        }

        /**
         * @return the itemTypeSKU
         */
        public String getItemTypeSKU() {
            return itemTypeSKU;
        }

        /**
         * @param itemTypeSKU the itemTypeSKU to set
         */
        public void setItemTypeSKU(String itemTypeSKU) {
            this.itemTypeSKU = itemTypeSKU;
        }

        /**
         * @return the inflowReceiptCode
         */
        public String getInflowReceiptCode() {
            return inflowReceiptCode;
        }

        /**
         * @param inflowReceiptCode the inflowReceiptCode to set
         */
        public void setInflowReceiptCode(String inflowReceiptCode) {
            this.inflowReceiptCode = inflowReceiptCode;
        }

        /**
         * @return the itemCondition
         */
        public String getItemCondition() {
            return itemCondition;
        }

        /**
         * @param itemCondition the itemCondition to set
         */
        public void setItemCondition(String itemCondition) {
            this.itemCondition = itemCondition;
        }

        /**
         * @return the reason
         */
        public String getReason() {
            return reason;
        }

        /**
         * @param reason the reason to set
         */
        public void setReason(String reason) {
            this.reason = reason;
        }

        /**
         * @return the total
         */
        public BigDecimal getTotal() {
            return total;
        }

        /**
         * @param total the total to set
         */
        public void setTotal(BigDecimal total) {
            this.total = total;
        }

        /**
         * @return the quantity
         */
        public int getQuantity() {
            return quantity;
        }

        /**
         * @param quantity the quantity to set
         */
        public void setQuantity(int quantity) {
            this.quantity = quantity;
        }

        /**
         * @return the pendingQuantity
         */
        public int getPendingQuantity() {
            return pendingQuantity;
        }

        /**
         * @param pendingQuantity the pendingQuantity to set
         */
        public void setPendingQuantity(int pendingQuantity) {
            this.pendingQuantity = pendingQuantity;
        }

        public String getInventoryType() {
            return inventoryType;
        }

        public void setInventoryType(String inventoryType) {
            this.inventoryType = inventoryType;
        }

        public BigDecimal getUnitPrice() {
            return unitPrice;
        }

        public void setUnitPrice(BigDecimal unitPrice) {
            this.unitPrice = unitPrice;
        }

        public BigDecimal getTaxPercentage() {
            return taxPercentage;
        }

        public void setTaxPercentage(BigDecimal taxPercentage) {
            this.taxPercentage = taxPercentage;
        }

        public String getItemTypeImageUrl() {
            return itemTypeImageUrl;
        }

        public void setItemTypeImageUrl(String itemTypeImageUrl) {
            this.itemTypeImageUrl = itemTypeImageUrl;
        }

        public String getItemTypePageUrl() {
            return itemTypePageUrl;
        }

        public void setItemTypePageUrl(String itemTypePageUrl) {
            this.itemTypePageUrl = itemTypePageUrl;
        }

        public BigDecimal getIntegratedGstPercentage() {
            return integratedGstPercentage;
        }

        public void setIntegratedGstPercentage(BigDecimal integratedGstPercentage) {
            this.integratedGstPercentage = integratedGstPercentage;
        }

        public BigDecimal getUnionTerritoryGstPercentage() {
            return unionTerritoryGstPercentage;
        }

        public void setUnionTerritoryGstPercentage(BigDecimal unionTerritoryGstPercentage) {
            this.unionTerritoryGstPercentage = unionTerritoryGstPercentage;
        }

        public BigDecimal getStateGstPercentage() {
            return stateGstPercentage;
        }

        public void setStateGstPercentage(BigDecimal stateGstPercentage) {
            this.stateGstPercentage = stateGstPercentage;
        }

        public BigDecimal getCentralGstPercentage() {
            return centralGstPercentage;
        }

        public void setCentralGstPercentage(BigDecimal centralGstPercentage) {
            this.centralGstPercentage = centralGstPercentage;
        }

        public BigDecimal getCompensationCessPercentage() {
            return compensationCessPercentage;
        }

        public void setCompensationCessPercentage(BigDecimal compensationCessPercentage) {
            this.compensationCessPercentage = compensationCessPercentage;
        }
    }
}
