/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 05-Jun-2013
 *  @author unicom
 */
package com.uniware.core.api.party;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.unifier.core.api.base.ServiceRequest;

public class CreateVendorDetailsRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long    serialVersionUID = -4155284320739097166L;

    @NotNull
    @Valid
    private WsVendor             wsVendor;

    @NotNull
    @Valid
    private WsPartyAddress       shippingAddress;

    @NotNull
    @Valid
    private WsPartyAddress       billingAddress;

    @NotNull
    @Valid
    private List<WsPartyContact> contacts;

    @NotNull
    @Valid
    private WsVendorAgreement    agreement;

    public WsVendor getWsVendor() {
        return wsVendor;
    }

    public void setWsVendor(WsVendor wsVendor) {
        this.wsVendor = wsVendor;
    }

    /**
     * @return the shippingAddress
     */
    public WsPartyAddress getShippingAddress() {
        return shippingAddress;
    }

    /**
     * @param shippingAddress the shippingAddress to set
     */
    public void setShippingAddress(WsPartyAddress shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    /**
     * @return the billingAddress
     */
    public WsPartyAddress getBillingAddress() {
        return billingAddress;
    }

    /**
     * @param billingAddress the billingAddress to set
     */
    public void setBillingAddress(WsPartyAddress billingAddress) {
        this.billingAddress = billingAddress;
    }

    /**
     * @return the contacts
     */
    public List<WsPartyContact> getContacts() {
        return contacts;
    }

    /**
     * @param contacts the contacts to set
     */
    public void setContacts(List<WsPartyContact> contacts) {
        this.contacts = contacts;
    }

    /**
     * @return the agreement
     */
    public WsVendorAgreement getAgreement() {
        return agreement;
    }

    /**
     * @param agreement the agreement to set
     */
    public void setAgreement(WsVendorAgreement agreement) {
        this.agreement = agreement;
    }

}
