/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 24-May-2013
 *  @author unicom
 */
package com.uniware.core.api.notification;

import java.util.List;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author unicom
 */
public class UpdateNotificationRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -355494836543601299L;

    private List<Integer>     notificationIds;
    private String            status;

    /**
     * @return the notificationIds
     */
    public List<Integer> getNotificationIds() {
        return notificationIds;
    }

    /**
     * @param notificationIds the notificationIds to set
     */
    public void setNotificationIds(List<Integer> notificationIds) {
        this.notificationIds = notificationIds;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

}
