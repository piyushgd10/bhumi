/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 10-Oct-2013
 *  @author parijat
 */
package com.uniware.core.api.purchase;

import javax.validation.constraints.NotNull;

import com.unifier.core.api.base.ServiceRequest;

public class DeleteReorderConfigRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -732728978978877L;
    
    @NotNull
    private String        itemSkuCode;

    /**
     * @return the itemSkuCode
     */
    public String getItemSkuCode() {
        return itemSkuCode;
    }

    /**
     * @param itemSkuCode the itemSkuCode to set
     */
    public void setItemSkuCode(String itemSkuCode) {
        this.itemSkuCode = itemSkuCode;
    }

}
