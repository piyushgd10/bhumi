/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 30-Sep-2014
 *  @author parijat
 */
package com.uniware.core.api.channel;

import java.util.List;
import java.util.Map;

import com.unifier.core.api.base.ServiceResponse;
import com.unifier.core.utils.XMLParser.Element;

/**
 * @author parijat
 *
 */
public class GetChannelReconciliationListResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 1750887166686375365L;

    private List<Element>       channelReconciliationElements;
    private Map<String, Object> channelCodeToReconciliationElement;
    private boolean             hasMoreResults;
    private Integer             totalPages;

    /**
     * @return the channelReconciliationElements
     */
    public List<Element> getChannelReconciliationElements() {
        return channelReconciliationElements;
    }

    /**
     * @param channelReconciliationElements the channelReconciliationElements to set
     */
    public void setChannelReconciliationElements(List<Element> channelReconciliationElements) {
        this.channelReconciliationElements = channelReconciliationElements;
    }

    /**
     * @return the channelCodeToReconciliationElement
     */
    public Map<String, Object> getChannelCodeToReconciliationElement() {
        return channelCodeToReconciliationElement;
    }

    /**
     * @param channelCodeToReconciliationElement the channelCodeToReconciliationElement to set
     */
    public void setChannelCodeToReconciliationElement(Map<String, Object> channelCodeToReconciliationElement) {
        this.channelCodeToReconciliationElement = channelCodeToReconciliationElement;
    }

    /**
     * @return the hasMoreResults
     */
    public boolean isHasMoreResults() {
        return hasMoreResults;
    }

    /**
     * @param hasMoreResults the hasMoreResults to set
     */
    public void setHasMoreResults(boolean hasMoreResults) {
        this.hasMoreResults = hasMoreResults;
    }

    /**
     * @return the totalPages
     */
    public Integer getTotalPages() {
        return totalPages;
    }

    /**
     * @param totalPages the totalPages to set
     */
    public void setTotalPages(Integer totalPages) {
        this.totalPages = totalPages;
    }

}
