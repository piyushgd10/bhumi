package com.uniware.core.api.cyclecount;

import java.util.List;

import com.unifier.core.api.base.ServiceResponse;

/**
 * Created by harshpal on 22/04/16.
 */
public class GetBadItemsForShelfResponse extends ServiceResponse {

    List<ErrorItemDTO> items;

    public List<ErrorItemDTO> getItems() {
        return items;
    }

    public void setItems(List<ErrorItemDTO> items) {
        this.items = items;
    }
}
