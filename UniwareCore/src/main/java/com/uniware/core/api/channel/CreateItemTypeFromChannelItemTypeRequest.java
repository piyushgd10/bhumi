/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 9, 2012
 *  @author praveeng
 */
package com.uniware.core.api.channel;

import java.util.List;

import javax.validation.Valid;

import org.hibernate.validator.constraints.NotEmpty;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author Sunny Agarwal
 */
public class CreateItemTypeFromChannelItemTypeRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long               serialVersionUID = -5149174516181942588L;

    @Valid
    @NotEmpty
    private List<WsUnlinkedChannelItemType> channelItemTypes;

    public CreateItemTypeFromChannelItemTypeRequest() {
        super();
    }

    public List<WsUnlinkedChannelItemType> getChannelItemTypes() {
        return channelItemTypes;
    }

    public void setChannelItemTypes(List<WsUnlinkedChannelItemType> channelItemTypes) {
        this.channelItemTypes = channelItemTypes;
    }

}
