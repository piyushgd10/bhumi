/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 20, 2012
 *  @author praveeng
 */
package com.uniware.core.api.party;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author praveeng
 */
public class GetBillingPartyRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 5120973552098733732L;
    private String            code;

    public GetBillingPartyRequest() {
    }

    public GetBillingPartyRequest(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

}
