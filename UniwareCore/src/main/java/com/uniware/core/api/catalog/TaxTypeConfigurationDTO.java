/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 *  @version     1.0, 03-Aug-2012
 *  @author akshay
 */
package com.uniware.core.api.catalog;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author akshay
 */
public class TaxTypeConfigurationDTO {

    private String                             stateCode;
    private String                             taxTypeCode;
    private String                             taxTypeName;
    private Date                               updated;

    private List<TaxTypeConfigurationRangeDTO> ranges = new ArrayList<>();

    /**
     * @return the stateCode
     */
    public String getStateCode() {
        return stateCode;
    }

    /**
     * @param stateCode the stateCode to set
     */
    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }

    /**
     * @return the taxTypeCode
     */
    public String getTaxTypeCode() {
        return taxTypeCode;
    }

    /**
     * @param taxTypeCode the taxTypeCode to set
     */
    public void setTaxTypeCode(String taxTypeCode) {
        this.taxTypeCode = taxTypeCode;
    }

    public String getTaxTypeName() {
        return taxTypeName;
    }

    public void setTaxTypeName(String taxTypeName) {
        this.taxTypeName = taxTypeName;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public List<TaxTypeConfigurationRangeDTO> getRanges() {
        return ranges;
    }

    public void setRanges(List<TaxTypeConfigurationRangeDTO> ranges) {
        this.ranges = ranges;
    }
}
