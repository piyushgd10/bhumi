/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 20, 2012
 *  @author praveeng
 */
package com.uniware.core.api.customer;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author Sunny
 */
public class EditCustomerRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -8699411146004803918L;

    @NotNull
    @Valid
    private WsCustomer        customer;

    public WsCustomer getCustomer() {
        return customer;
    }

    public void setCustomer(WsCustomer customer) {
        this.customer = customer;
    }

}
