package com.uniware.core.api.cyclecount;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

/**
 * Created by harshpal on 20/04/16.
 */
public class SearchCycleCountShelfRequest extends ServiceRequest {

    @NotBlank
    private String shelfCode;

    public String getShelfCode() {
        return shelfCode;
    }

    public void setShelfCode(String shelfCode) {
        this.shelfCode = shelfCode;
    }
}
