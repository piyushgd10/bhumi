/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 18-Apr-2013
 *  @author unicom
 */
package com.uniware.core.api.channel;

import com.uniware.core.entity.SourceConnectorParameter;

import java.io.Serializable;

/**
 * @author Sunny Agarwal
 */
public class SourceConnectorParameterDTO implements Serializable {

    private String name;
    private String displayName;
    private String displayPlaceHolder;
    private String type;
    private int    priority;

    public SourceConnectorParameterDTO() {
    }

    public SourceConnectorParameterDTO(SourceConnectorParameter scp) {
        name = scp.getName();
        displayName = scp.getDisplayName();
        displayPlaceHolder = scp.getDisplayPlaceHolder();
        type = scp.getType().name();
        priority = scp.getPriority();
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDisplayPlaceHolder() {
        return displayPlaceHolder;
    }

    public void setDisplayPlaceHolder(String displayPlaceHolder) {
        this.displayPlaceHolder = displayPlaceHolder;
    }

}
