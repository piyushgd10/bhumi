package com.uniware.core.api.saleorder;

import com.unifier.core.api.base.ServiceRequest;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Created by bhuvneshwarkumar on 07/08/15.
 */
public class GetCancellationReasonsRequest extends ServiceRequest{

    @NotBlank
    private String channelName;

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }
}
