package com.uniware.core.api.invoice;

import com.unifier.core.api.base.ServiceResponse;

/**
 * Created by piyush on 8/26/15.
 */
public class CreateInvoiceWithDetailsResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 1640621554174312045L;

    private String            invoiceCode;

    private String            shippingPackageCode;

    public String getInvoiceCode() {
        return invoiceCode;
    }

    public void setInvoiceCode(String invoiceCode) {
        this.invoiceCode = invoiceCode;
    }

    public String getShippingPackageCode() {
        return shippingPackageCode;
    }

    public void setShippingPackageCode(String shippingPackageCode) {
        this.shippingPackageCode = shippingPackageCode;
    }
}
