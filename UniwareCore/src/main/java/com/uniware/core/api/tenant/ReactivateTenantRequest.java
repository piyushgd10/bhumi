/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jun 5, 2015
 *  @author harsh
 */
package com.uniware.core.api.tenant;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author harsh
 */
public class ReactivateTenantRequest extends ServiceRequest {

    private static final long serialVersionUID = 62848179370644941L;

    @NotBlank
    private String            tenantCode;

    private boolean           cleanUpData;

    private boolean           clearAWBs;

    public String getTenantCode() {
        return tenantCode;
    }

    public void setTenantCode(String tenantCode) {
        this.tenantCode = tenantCode;
    }

    public boolean isCleanUpData() {
        return cleanUpData;
    }

    public void setCleanUpData(boolean cleanUpData) {
        this.cleanUpData = cleanUpData;
    }

    public boolean isClearAWBs() {
        return clearAWBs;
    }

    public void setClearAWBs(boolean clearAWBs) {
        this.clearAWBs = clearAWBs;
    }
}
