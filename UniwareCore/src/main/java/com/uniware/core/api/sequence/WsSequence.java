/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 16-Feb-2014
 *  @author karunsingla
 */
package com.uniware.core.api.sequence;

import javax.validation.constraints.Min;

import org.hibernate.validator.constraints.Length;

import com.unifier.core.annotation.constraints.OptionalBlank;

public class WsSequence {

    private String  name;

    @OptionalBlank
    @Length(max = 20)
    private String prefix;

    @Min(value = 1)
    private int    currentValue;

    private String nextYearPrefix;

    private boolean resetCounterNextYear;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public int getCurrentValue() {
        return currentValue;
    }

    public void setCurrentValue(int currentValue) {
        this.currentValue = currentValue;
    }

    public String getNextYearPrefix() {
        return nextYearPrefix;
    }

    public void setNextYearPrefix(String nextYearPrefix) {
        this.nextYearPrefix = nextYearPrefix;
    }

    public boolean isResetCounterNextYear() {
        return resetCounterNextYear;
    }

    public void setResetCounterNextYear(boolean resetCounterNextYear) {
        this.resetCounterNextYear = resetCounterNextYear;
    }
}
