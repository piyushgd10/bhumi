/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jan 6, 2012
 *  @author singla
 */
package com.uniware.core.api.shipping;

import com.unifier.core.api.base.ServiceResponse;

import java.util.ArrayList;
import java.util.List;

import com.uniware.core.api.packer.ShippingPackageFullDTO;

/**
 * @author singla
 */
public class SplitShippingPackageResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long            serialVersionUID = 8182997643203148537L;

    private List<ShippingPackageFullDTO> shippingPackages = new ArrayList<ShippingPackageFullDTO>();

    private ShippingPackageFullDTO       parentShippingPackage;

    /**
     * @return the shippingPackages
     */
    public List<ShippingPackageFullDTO> getShippingPackages() {
        return shippingPackages;
    }

    /**
     * @param shippingPackages the shippingPackages to set
     */
    public void setShippingPackages(List<ShippingPackageFullDTO> shippingPackages) {
        this.shippingPackages = shippingPackages;
    }

    /**
     * @return the parentShippingPackage
     */
    public ShippingPackageFullDTO getParentShippingPackage() {
        return parentShippingPackage;
    }

    /**
     * @param parentShippingPackage the parentShippingPackage to set
     */
    public void setParentShippingPackage(ShippingPackageFullDTO parentShippingPackage) {
        this.parentShippingPackage = parentShippingPackage;
    }

}
