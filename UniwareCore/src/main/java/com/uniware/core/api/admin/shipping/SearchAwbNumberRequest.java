/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 03-Jun-2012
 *  @author vibhu
 */
package com.uniware.core.api.admin.shipping;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author vibhu
 */
public class SearchAwbNumberRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -871667958228391685L;

    private String            providerCode;

    /**
     * @return the providerCode
     */
    public String getProviderCode() {
        return providerCode;
    }

    /**
     * @param providerCode the providerCode to set
     */
    public void setProviderCode(String providerCode) {
        this.providerCode = providerCode;
    }

}
