/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 28-Jan-2015
 *  @author akshaykochhar
 */
package com.uniware.core.api.shipping;

import java.util.Date;
import java.util.List;

import com.unifier.core.api.customfields.CustomFieldMetadataDTO;

public class ManifestSummaryDTO {

    private String                       code;
    private String                       username;
    private String                       statusCode;
    private String                       shippingProvider;
    private String                       shippingProviderCode;
    private String                       shippingMethod;
    private String                       shippingArrangedBy;
    private String                       channel;
    private String                       shippingManfestLink;
    private boolean                      cashOnDelivery;
    private String                       comments;
    private Date                         created;
    private Date                         closed;
    private boolean                      fetchCurrentChannelManifestEnabled;
    private List<CustomFieldMetadataDTO> customFieldValues;

    public ManifestSummaryDTO() {
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getShippingProvider() {
        return shippingProvider;
    }

    public void setShippingProvider(String shippingProvider) {
        this.shippingProvider = shippingProvider;
    }

    public String getShippingProviderCode() {
        return shippingProviderCode;
    }

    public void setShippingProviderCode(String shippingProviderCode) {
        this.shippingProviderCode = shippingProviderCode;
    }

    public String getShippingMethod() {
        return shippingMethod;
    }

    public void setShippingMethod(String shippingMethod) {
        this.shippingMethod = shippingMethod;
    }

    public String getShippingArrangedBy() {
        return shippingArrangedBy;
    }

    public void setShippingArrangedBy(String shippingArrangedBy) {
        this.shippingArrangedBy = shippingArrangedBy;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getShippingManfestLink() {
        return shippingManfestLink;
    }

    public void setShippingManfestLink(String shippingManfestLink) {
        this.shippingManfestLink = shippingManfestLink;
    }

    public boolean isCashOnDelivery() {
        return cashOnDelivery;
    }

    public void setCashOnDelivery(boolean cashOnDelivery) {
        this.cashOnDelivery = cashOnDelivery;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getClosed() {
        return closed;
    }

    public void setClosed(Date closed) {
        this.closed = closed;
    }

    public boolean isFetchCurrentChannelManifestEnabled() {
        return fetchCurrentChannelManifestEnabled;
    }

    public void setFetchCurrentChannelManifestEnabled(boolean fetchCurrentChannelManifestEnabled) {
        this.fetchCurrentChannelManifestEnabled = fetchCurrentChannelManifestEnabled;
    }

    public List<CustomFieldMetadataDTO> getCustomFieldValues() {
        return customFieldValues;
    }

    public void setCustomFieldValues(List<CustomFieldMetadataDTO> customFieldValues) {
        this.customFieldValues = customFieldValues;
    }

}
