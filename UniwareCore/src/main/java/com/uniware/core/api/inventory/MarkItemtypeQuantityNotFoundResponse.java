/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 10-Jun-2014
 *  @author akshay
 */
package com.uniware.core.api.inventory;

import java.util.ArrayList;
import java.util.List;

import com.unifier.core.api.base.ServiceResponse;

public class MarkItemtypeQuantityNotFoundResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    public List<ItemTypeInventoryDTO> inventoryDTOs = new ArrayList<ItemTypeInventoryDTO>();

    public List<ItemTypeInventoryDTO> getInventoryDTOs() {
        return inventoryDTOs;
    }

    public void setInventoryDTOs(List<ItemTypeInventoryDTO> inventoryDTOs) {
        this.inventoryDTOs = inventoryDTOs;
    }

}
