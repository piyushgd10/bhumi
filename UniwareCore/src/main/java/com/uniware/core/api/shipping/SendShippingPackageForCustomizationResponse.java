/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 14, 2013
 *  @author karunsingla
 */
package com.uniware.core.api.shipping;

import com.unifier.core.api.base.ServiceResponse;

public class SendShippingPackageForCustomizationResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -6404501934909132493L;
    
    private String shippingPackageCode;

    /**
     * @return the shippingPackageCode
     */
    public String getShippingPackageCode() {
        return shippingPackageCode;
    }

    /**
     * @param shippingPackageCode the shippingPackageCode to set
     */
    public void setShippingPackageCode(String shippingPackageCode) {
        this.shippingPackageCode = shippingPackageCode;
    }
    

}
