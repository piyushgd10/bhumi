/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 14-May-2012
 *  @author praveeng
 */
package com.uniware.core.api.putaway;

import com.uniware.core.entity.PutawayItem;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author praveeng
 */
public class PutawayItemDTO {

    private Integer      id;
    private String       shelfCode;
    private String       itemTypeName;
    private String       itemTypeSkuCode;
    private String       inventoryType;
    private int          quantity;
    private int          putawayQuantity;
    private String       statusCode;
    private Set<String>  pendingItemCodes;
    private List<String> disallowedShelfCodes;

    public PutawayItemDTO(){
    }

    public PutawayItemDTO(PutawayItem putawayItem) {
        this.itemTypeSkuCode = putawayItem.getItemType().getSkuCode();
        this.id = putawayItem.getId();
        this.shelfCode = putawayItem.getShelf().getCode();
        this.itemTypeName = putawayItem.getItemType().getName();
        this.inventoryType = putawayItem.getType().name();
        this.quantity = putawayItem.getQuantity();
        this.putawayQuantity = putawayItem.getPutawayQuantity();
        this.statusCode = putawayItem.getStatusCode();
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the shelfCode
     */
    public String getShelfCode() {
        return shelfCode;
    }

    /**
     * @param shelfCode the shelfCode to set
     */
    public void setShelfCode(String shelfCode) {
        this.shelfCode = shelfCode;
    }

    /**
     * @return the itemTypeName
     */
    public String getItemTypeName() {
        return itemTypeName;
    }

    /**
     * @param itemTypeName the itemTypeName to set
     */
    public void setItemTypeName(String itemTypeName) {
        this.itemTypeName = itemTypeName;
    }

    /**
     * @return the itemTypeSkuCode
     */
    public String getItemTypeSkuCode() {
        return itemTypeSkuCode;
    }

    /**
     * @param itemTypeSkuCode the itemTypeSkuCode to set
     */
    public void setItemTypeSkuCode(String itemTypeSkuCode) {
        this.itemTypeSkuCode = itemTypeSkuCode;
    }

    /**
     * @return the quantity
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     * @param quantity the quantity to set
     */
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    /**
     * @return the putawayQuantity
     */
    public int getPutawayQuantity() {
        return putawayQuantity;
    }

    /**
     * @param putawayQuantity the putawayQuantity to set
     */
    public void setPutawayQuantity(int putawayQuantity) {
        this.putawayQuantity = putawayQuantity;
    }

    /**
     * @return the statusCode
     */
    public String getStatusCode() {
        return statusCode;
    }

    /**
     * @param statusCode the statusCode to set
     */
    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getInventoryType() {
        return inventoryType;
    }

    public void setInventoryType(String inventoryType) {
        this.inventoryType = inventoryType;
    }

    public Set<String> getPendingItemCodes() {
        return pendingItemCodes;
    }

    public void setPendingItemCodes(Set<String> pendingItemCodes) {
        this.pendingItemCodes = pendingItemCodes;
    }

    public List<String> getDisallowedShelfCodes() {
        return disallowedShelfCodes;
    }

    public void setDisallowedShelfCodes(List<String> disallowedShelfCodes) {
        this.disallowedShelfCodes = disallowedShelfCodes;
    }
}
