/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 29-Sep-2014
 *  @author akshay
 */
package com.uniware.core.api.shipping;

import com.unifier.core.api.base.ServiceResponse;
import com.uniware.core.entity.Source;

import java.io.File;
import java.util.List;

public class GeneratePDFShippingLabelResponse extends ServiceResponse {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private File shippingLabel;

    public File getShippingLabel() {
        return shippingLabel;
    }

    public void setShippingLabel(File shippingLabel) {
        this.shippingLabel = shippingLabel;
    }
}
