/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 23-Apr-2013
 *  @author unicom
 */
package com.uniware.core.api.channel;

import java.util.Map;
import com.unifier.core.api.base.ServiceResponse;

/**
 * @author Piyush
 */
public class GetCatalogSyncPreprocessorResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long   serialVersionUID = 4001946297072204571L;

    private Map<String, Object> resultItems;

    public Map<String, Object> getResultItems() {
        return resultItems;
    }

    public void setResultItems(Map<String, Object> resultItems) {
        this.resultItems = resultItems;
    }

}