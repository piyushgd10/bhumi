/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 27-Mar-2014
 *  @author parijat
 */
package com.uniware.core.api.reconciliation;

import java.util.List;

import com.unifier.core.api.base.ServiceResponse;
import com.uniware.core.entity.ChannelPaymentReconciliationDetailDTO;

/**
 * @author parijat
 *
 */
public class GetOrderReconciliationDetailResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 4219837522587647976L;
    
    private List<ChannelPaymentReconciliationDetailDTO> orderReconciliationDetails;

    private boolean                               isReconciliationEnabled;

    public List<ChannelPaymentReconciliationDetailDTO> getOrderReconciliationDetails() {
        return orderReconciliationDetails;
    }

    public void setOrderReconciliationDetails(List<ChannelPaymentReconciliationDetailDTO> orderReconciliationDetails) {
        this.orderReconciliationDetails = orderReconciliationDetails;
    }

    /**
     * @return the isReconciliationEnabled
     */
    public boolean isReconciliationEnabled() {
        return isReconciliationEnabled;
    }

    /**
     * @param isReconciliationEnabled the isReconciliationEnabled to set
     */
    public void setReconciliationEnabled(boolean isReconciliationEnabled) {
        this.isReconciliationEnabled = isReconciliationEnabled;
    }
       

}
