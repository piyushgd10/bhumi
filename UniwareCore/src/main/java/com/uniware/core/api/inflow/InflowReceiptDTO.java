/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 3, 2012
 *  @author praveeng
 */
package com.uniware.core.api.inflow;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.unifier.core.api.customfields.CustomFieldMetadataDTO;
import com.uniware.core.api.purchase.PurchaseOrderDTO;

/**
 * @author praveeng
 */
public class InflowReceiptDTO {

    private String                       code;
    private String                       statusCode;
    private Date                         created;
    private String                       createdBy;
    private String                       vendorInvoiceNumber;
    private Date                         vendorInvoiceDate;
    private PurchaseOrderDTO             purchaseOrder;
    private BigDecimal                   totalReceivedAmount = new BigDecimal(0);
    private BigDecimal                   totalRejectedAmount = new BigDecimal(0);
    private int                          totalQuantity;
    private int                          totalRejectedQuantity;
    private List<InflowReceiptItemDTO>   inflowReceiptItems  = new ArrayList<InflowReceiptItemDTO>();
    private List<CustomFieldMetadataDTO> customFieldValues;

    /**
     * @return the statusCode
     */
    public String getStatusCode() {
        return statusCode;
    }

    /**
     * @param statusCode the statusCode to set
     */
    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    /**
     * @return the created
     */
    public Date getCreated() {
        return created;
    }

    /**
     * @param created the created to set
     */
    public void setCreated(Date created) {
        this.created = created;
    }

    /**
     * @return the createdBy
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     * @param createdBy the createdBy to set
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    /**
     * @return the purchaseOrder
     */
    public PurchaseOrderDTO getPurchaseOrder() {
        return purchaseOrder;
    }

    /**
     * @param purchaseOrder the purchaseOrder to set
     */
    public void setPurchaseOrder(PurchaseOrderDTO purchaseOrder) {
        this.purchaseOrder = purchaseOrder;
    }

    /**
     * @return the totalReceivedAmount
     */
    public BigDecimal getTotalReceivedAmount() {
        return totalReceivedAmount;
    }

    /**
     * @param totalReceivedAmount the totalReceivedAmount to set
     */
    public void setTotalReceivedAmount(BigDecimal totalReceivedAmount) {
        this.totalReceivedAmount = totalReceivedAmount;
    }

    /**
     * @return the totalRejectedAmount
     */
    public BigDecimal getTotalRejectedAmount() {
        return totalRejectedAmount;
    }

    /**
     * @param totalRejectedAmount the totalRejectedAmount to set
     */
    public void setTotalRejectedAmount(BigDecimal totalRejectedAmount) {
        this.totalRejectedAmount = totalRejectedAmount;
    }

    /**
     * @return the inflowReceiptItems
     */
    public List<InflowReceiptItemDTO> getInflowReceiptItems() {
        return inflowReceiptItems;
    }

    /**
     * @param inflowReceiptItems the inflowReceiptItems to set
     */
    public void setInflowReceiptItems(List<InflowReceiptItemDTO> inflowReceiptItems) {
        this.inflowReceiptItems = inflowReceiptItems;
    }

    /**
     * @return the vendorInvoiceNumber
     */
    public String getVendorInvoiceNumber() {
        return vendorInvoiceNumber;
    }

    /**
     * @param vendorInvoiceNumber the vendorInvoiceNumber to set
     */
    public void setVendorInvoiceNumber(String vendorInvoiceNumber) {
        this.vendorInvoiceNumber = vendorInvoiceNumber;
    }

    /**
     * @return the vendorInvoiceDate
     */
    public Date getVendorInvoiceDate() {
        return vendorInvoiceDate;
    }

    /**
     * @param vendorInvoiceDate the vendorInvoiceDate to set
     */
    public void setVendorInvoiceDate(Date vendorInvoiceDate) {
        this.vendorInvoiceDate = vendorInvoiceDate;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the totalQuantity
     */
    public int getTotalQuantity() {
        return totalQuantity;
    }

    /**
     * @param totalQuantity the totalQuantity to set
     */
    public void setTotalQuantity(int totalQuantity) {
        this.totalQuantity = totalQuantity;
    }

    /**
     * @return the totalRejectedQuantity
     */
    public int getTotalRejectedQuantity() {
        return totalRejectedQuantity;
    }

    /**
     * @param totalRejectedQuantity the totalRejectedQuantity to set
     */
    public void setTotalRejectedQuantity(int totalRejectedQuantity) {
        this.totalRejectedQuantity = totalRejectedQuantity;
    }

    /**
     * @return the customFieldValues
     */
    public List<CustomFieldMetadataDTO> getCustomFieldValues() {
        return customFieldValues;
    }

    /**
     * @param customFieldValues the customFieldValues to set
     */
    public void setCustomFieldValues(List<CustomFieldMetadataDTO> customFieldValues) {
        this.customFieldValues = customFieldValues;
    }

}