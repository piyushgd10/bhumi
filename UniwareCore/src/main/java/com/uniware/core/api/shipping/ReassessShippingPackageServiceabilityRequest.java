/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 27-Aug-2015
 *  @author parijat
 */
package com.uniware.core.api.shipping;

import javax.validation.constraints.NotNull;

import com.unifier.core.api.base.ServiceRequest;

public class ReassessShippingPackageServiceabilityRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 356789876543245678L;

    @NotNull
    private Integer           shippingPackageId;

    /**
     * @return the shippingPackageId
     */
    public Integer getShippingPackageId() {
        return shippingPackageId;
    }

    /**
     * @param shippingPackageId the shippingPackageId to set
     */
    public void setShippingPackageId(Integer shippingPackageId) {
        this.shippingPackageId = shippingPackageId;
    }

}
