package com.uniware.core.api.cyclecount;

import java.util.List;

/**
 * Created by harshpal on 3/8/16.
 */
public class CycleCountDTO {
    private String                 cycleCountCode;
    private List<SubCycleCountDTO> subCycleCounts;

    public String getCycleCountCode() {
        return cycleCountCode;
    }

    public void setCycleCountCode(String cycleCountCode) {
        this.cycleCountCode = cycleCountCode;
    }

    public List<SubCycleCountDTO> getSubCycleCounts() {
        return subCycleCounts;
    }

    public void setSubCycleCounts(List<SubCycleCountDTO> subCycleCounts) {
        this.subCycleCounts = subCycleCounts;
    }
}