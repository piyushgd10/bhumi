/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 10-Dec-2013
 *  @author akshay
 */
package com.uniware.core.api.item;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.unifier.core.api.base.ServiceResponse;
import com.unifier.core.api.customfields.CustomFieldMetadataDTO;

public class GetAllFacilityItemDetailResponse extends ServiceResponse{

    /**
     * 
     */
    private static final long serialVersionUID = -3218784276308501590L;

    private ItemDTO           itemDTO;

    public ItemDTO getItemDTO() {
        return itemDTO;
    }

    public void setItemDTO(ItemDTO itemDTO) {
        this.itemDTO = itemDTO;
    }

    public static class GatePassDTO {

        private int    id;
        private String code;
        private String statusCode;
        private String type;
        private String toPartyName;
        private String reference;
        private String purpose;
        private Date   created;
        private String facilityName;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getStatusCode() {
            return statusCode;
        }

        public void setStatusCode(String statusCode) {
            this.statusCode = statusCode;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getToPartyName() {
            return toPartyName;
        }

        public void setToPartyName(String toPartyName) {
            this.toPartyName = toPartyName;
        }

        public String getReference() {
            return reference;
        }

        public void setReference(String reference) {
            this.reference = reference;
        }

        public String getPurpose() {
            return purpose;
        }

        public void setPurpose(String purpose) {
            this.purpose = purpose;
        }

        public Date getCreated() {
            return created;
        }

        public void setCreated(Date created) {
            this.created = created;
        }

        public String getFacilityName() {
            return facilityName;
        }

        public void setFacilityName(String facilityName) {
            this.facilityName = facilityName;
        }


    };

    public static class SaleOrderItemDTO {

        private String picklistNumber;
        private Date   created;
        private String saleOrderCode;
        private String saleOrderItemCode;
        private String saleOrderItemStatus;
        private String shippingPackageCode;
        private String shippingPackageStatus;
        private String itemSku;
        private String itemName;
        private String facilityName;
        private int    id;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public Date getCreated() {
            return created;
        }

        public void setCreated(Date created) {
            this.created = created;
        }

        public String getItemSku() {
            return itemSku;
        }

        public void setItemSku(String itemSku) {
            this.itemSku = itemSku;
        }

        public String getItemName() {
            return itemName;
        }

        public void setItemName(String itemName) {
            this.itemName = itemName;
        }

        /**
         * @return the saleOrderCode
         */
        public String getSaleOrderCode() {
            return saleOrderCode;
        }

        /**
         * @param saleOrderCode the saleOrderCode to set
         */
        public void setSaleOrderCode(String saleOrderCode) {
            this.saleOrderCode = saleOrderCode;
        }

        /**
         * @return the saleOrderItemCode
         */
        public String getSaleOrderItemCode() {
            return saleOrderItemCode;
        }

        /**
         * @param saleOrderItemCode the saleOrderItemCode to set
         */
        public void setSaleOrderItemCode(String saleOrderItemCode) {
            this.saleOrderItemCode = saleOrderItemCode;
        }

        /**
         * @return the saleOrderItemStatus
         */
        public String getSaleOrderItemStatus() {
            return saleOrderItemStatus;
        }

        /**
         * @param saleOrderItemStatus the saleOrderItemStatus to set
         */
        public void setSaleOrderItemStatus(String saleOrderItemStatus) {
            this.saleOrderItemStatus = saleOrderItemStatus;
        }

        /**
         * @return the shippingPackageCode
         */
        public String getShippingPackageCode() {
            return shippingPackageCode;
        }

        /**
         * @param shippingPackageCode the shippingPackageCode to set
         */
        public void setShippingPackageCode(String shippingPackageCode) {
            this.shippingPackageCode = shippingPackageCode;
        }

        /**
         * @return the shippingPackageStatus
         */
        public String getShippingPackageStatus() {
            return shippingPackageStatus;
        }

        /**
         * @param shippingPackageStatus the shippingPackageStatus to set
         */
        public void setShippingPackageStatus(String shippingPackageStatus) {
            this.shippingPackageStatus = shippingPackageStatus;
        }

        /**
         * @return the picklistNumber
         */
        public String getPicklistNumber() {
            return picklistNumber;
        }

        /**
         * @param picklistNumber the picklistNumber to set
         */
        public void setPicklistNumber(String picklistNumber) {
            this.picklistNumber = picklistNumber;
        }

        public String getFacilityName() {
            return facilityName;
        }

        public void setFacilityName(String facilityName) {
            this.facilityName = facilityName;
        }


    }

    public static class ItemDTO {

        private String                       code;
        private String                       status;
        private String                       inflowReceiptCode;
        private String                       lastPutawayCode;

        private String                       itemSKU;
        private String                       itemTypeName;
        private String                       itemTypeImageUrl;
        private String                       itemTypePageUrl;
        private String                       vendor;
        private BigDecimal                   unitPrice;
        private BigDecimal                   maxRetailPrice;
        private String                       vendorSkuCode;
        private String                       color;
        private String                       size;
        private String                       brand;

        private Date                         created;
        private List<SaleOrderItemDTO>       saleOrderItemDTOs = new ArrayList<SaleOrderItemDTO>();
        private List<GatePassDTO>            gatePassDTOs      = new ArrayList<GatePassDTO>();
        private List<CustomFieldMetadataDTO> itemTypeCustomFieldValues;
        private List<CustomFieldMetadataDTO> saleOrderCustomFieldValues;
        private Map<String, Object>          itemCustomFields;

        /**
         * @return the saleOrderCustomFieldValues
         */
        public List<CustomFieldMetadataDTO> getSaleOrderCustomFieldValues() {
            return saleOrderCustomFieldValues;
        }

        /**
         * @param saleOrderCustomFieldValues the saleOrderCustomFieldValues to set
         */
        public void setSaleOrderCustomFieldValues(List<CustomFieldMetadataDTO> saleOrderCustomFieldValues) {
            this.saleOrderCustomFieldValues = saleOrderCustomFieldValues;
        }

        /**
         * @return the itemCustomFields
         */
        public Map<String, Object> getItemCustomFields() {
            return itemCustomFields;
        }

        /**
         * @param itemCustomFields the itemCustomFields to set
         */
        public void setItemCustomFields(Map<String, Object> itemCustomFields) {
            this.itemCustomFields = itemCustomFields;
        }

        /**
         * @return the code
         */
        public String getCode() {
            return code;
        }

        /**
         * @param code the code to set
         */
        public void setCode(String code) {
            this.code = code;
        }

        /**
         * @return the status
         */
        public String getStatus() {
            return status;
        }

        /**
         * @param status the status to set
         */
        public void setStatus(String status) {
            this.status = status;
        }

        /**
         * @return the inflowReceiptCode
         */
        public String getInflowReceiptCode() {
            return inflowReceiptCode;
        }

        /**
         * @param inflowReceiptCode the inflowReceiptCode to set
         */
        public void setInflowReceiptCode(String inflowReceiptCode) {
            this.inflowReceiptCode = inflowReceiptCode;
        }

        /**
         * @return the itemSKU
         */
        public String getItemSKU() {
            return itemSKU;
        }

        /**
         * @param itemSKU the itemSKU to set
         */
        public void setItemSKU(String itemSKU) {
            this.itemSKU = itemSKU;
        }

        /**
         * @return the itemTypeName
         */
        public String getItemTypeName() {
            return itemTypeName;
        }

        /**
         * @param itemTypeName the itemTypeName to set
         */
        public void setItemTypeName(String itemTypeName) {
            this.itemTypeName = itemTypeName;
        }

        /**
         * @return the itemTypeImageUrl
         */
        public String getItemTypeImageUrl() {
            return itemTypeImageUrl;
        }

        /**
         * @param itemTypeImageUrl the itemTypeImageUrl to set
         */
        public void setItemTypeImageUrl(String itemTypeImageUrl) {
            this.itemTypeImageUrl = itemTypeImageUrl;
        }

        /**
         * @return the itemTypePageUrl
         */
        public String getItemTypePageUrl() {
            return itemTypePageUrl;
        }

        /**
         * @param itemTypePageUrl the itemTypePageUrl to set
         */
        public void setItemTypePageUrl(String itemTypePageUrl) {
            this.itemTypePageUrl = itemTypePageUrl;
        }

        /**
         * @return the vendorSkuCode
         */
        public String getVendorSkuCode() {
            return vendorSkuCode;
        }

        /**
         * @param vendorSkuCode the vendorSkuCode to set
         */
        public void setVendorSkuCode(String vendorSkuCode) {
            this.vendorSkuCode = vendorSkuCode;
        }

        /**
         * @return the lastPutawayCode
         */
        public String getLastPutawayCode() {
            return lastPutawayCode;
        }

        /**
         * @param lastPutawayCode the lastPutawayCode to set
         */
        public void setLastPutawayCode(String lastPutawayCode) {
            this.lastPutawayCode = lastPutawayCode;
        }

        /**
         * @return the created
         */
        public Date getCreated() {
            return created;
        }

        /**
         * @param created the created to set
         */
        public void setCreated(Date created) {
            this.created = created;
        }

        /**
         * @return the saleOrderItemDTOs
         */
        public List<SaleOrderItemDTO> getSaleOrderItemDTOs() {
            return saleOrderItemDTOs;
        }

        /**
         * @param saleOrderItemDTOs the saleOrderItemDTOs to set
         */
        public void setSaleOrderItemDTOs(List<SaleOrderItemDTO> saleOrderItemDTOs) {
            this.saleOrderItemDTOs = saleOrderItemDTOs;
        }

        public void addSaleOrderItemDTO(SaleOrderItemDTO saleOrderItemDTO) {
            this.saleOrderItemDTOs.add(saleOrderItemDTO);
        }

        /**
         * @return the vendor
         */
        public String getVendor() {
            return vendor;
        }

        /**
         * @param vendor the vendor to set
         */
        public void setVendor(String vendor) {
            this.vendor = vendor;
        }

        /**
         * @return the unitPrice
         */
        public BigDecimal getUnitPrice() {
            return unitPrice;
        }

        /**
         * @param unitPrice the unitPrice to set
         */
        public void setUnitPrice(BigDecimal unitPrice) {
            this.unitPrice = unitPrice;
        }

        /**
         * @return the itemTypeCustomFieldValues
         */
        public List<CustomFieldMetadataDTO> getItemTypeCustomFieldValues() {
            return itemTypeCustomFieldValues;
        }

        /**
         * @param itemTypeCustomFieldValues the itemTypeCustomFieldValues to set
         */
        public void setItemTypeCustomFieldValues(List<CustomFieldMetadataDTO> itemTypeCustomFieldValues) {
            this.itemTypeCustomFieldValues = itemTypeCustomFieldValues;
        }

        /**
         * @return the maxRetailPrice
         */
        public BigDecimal getMaxRetailPrice() {
            return maxRetailPrice;
        }

        /**
         * @param maxRetailPrice the maxRetailPrice to set
         */
        public void setMaxRetailPrice(BigDecimal maxRetailPrice) {
            this.maxRetailPrice = maxRetailPrice;
        }

        /**
         * @return the color
         */
        public String getColor() {
            return color;
        }

        /**
         * @param color the color to set
         */
        public void setColor(String color) {
            this.color = color;
        }

        /**
         * @return the size
         */
        public String getSize() {
            return size;
        }

        /**
         * @param size the size to set
         */
        public void setSize(String size) {
            this.size = size;
        }

        /**
         * @return the brand
         */
        public String getBrand() {
            return brand;
        }

        /**
         * @param brand the brand to set
         */
        public void setBrand(String brand) {
            this.brand = brand;
        }

        public List<GatePassDTO> getGatePassDTOs() {
            return gatePassDTOs;
        }

        public void setGatePassDTOs(List<GatePassDTO> gatePassDTOs) {
            this.gatePassDTOs = gatePassDTOs;
        }

        public void addGatePassDTO(GatePassDTO gatePassDTO) {
            this.gatePassDTOs.add(gatePassDTO);
        }

    }

}
