/*
 * Copyright 2017 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 18/8/17 11:51 AM
 * @author digvijaysharma
 */
package com.uniware.core.api.packer;

/**
 * @author singla
 */
public class AllocateItemInPackageResponse extends AbstractAllocateItemResponse {

    private static final long serialVersionUID = -8266371053142184563L;

    private String            itemCode;

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }
}
