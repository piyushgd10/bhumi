/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 8, 2012
 *  @author singla
 */
package com.uniware.core.api.shipping;

import java.util.List;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author Sunny
 */
public class AddShippingPackagesToManifestResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long     serialVersionUID = 4535400467372845984L;
    private List<ManifestItemDTO> manifestItems;

    public List<ManifestItemDTO> getManifestItems() {
        return manifestItems;
    }

    public void setManifestItems(List<ManifestItemDTO> manifestItems) {
        this.manifestItems = manifestItems;
    }

}
