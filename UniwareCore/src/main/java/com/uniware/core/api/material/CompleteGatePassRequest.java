/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jul 1, 2012
 *  @author singla
 */
package com.uniware.core.api.material;

import com.unifier.core.api.base.ServiceRequest;

import org.hibernate.validator.constraints.NotEmpty;

public class CompleteGatePassRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -5453213333327788701L;

    @NotEmpty
    private String            gatePassCode;

    private Integer           userId;

    /**
     * @return the gassPassCode
     */
    public String getGatePassCode() {
        return gatePassCode;
    }

    /**
     * @param gassPassCode the gassPassCode to set
     */
    public void setGatePassCode(String gatePassCode) {
        this.gatePassCode = gatePassCode;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }
}
