package com.uniware.core.api.reconciliation;

import com.unifier.core.api.base.ServiceRequest;

/**
 * Created by akshayag on 10/4/16.
 */
public class GetSaleOrderReconciliationItemRequest extends ServiceRequest {

    private String channelCode;

    private String reconciliationIdentifier;

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    public String getReconciliationIdentifier() {
        return reconciliationIdentifier;
    }

    public void setReconciliationIdentifier(String reconciliationIdentifier) {
        this.reconciliationIdentifier = reconciliationIdentifier;
    }
}
