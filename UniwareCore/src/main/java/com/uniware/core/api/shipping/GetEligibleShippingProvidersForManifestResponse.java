/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 7, 2012
 *  @author singla
 */
package com.uniware.core.api.shipping;

import java.util.List;

import com.unifier.core.api.base.ServiceResponse;
import com.uniware.core.api.admin.shipping.ShippingProviderSearchDTO;

/**
 * @author Sunny
 */
public class GetEligibleShippingProvidersForManifestResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long               serialVersionUID = -5193898301331038238L;

    private List<ShippingProviderSearchDTO> shippingProviders;

    public List<ShippingProviderSearchDTO> getShippingProviders() {
        return shippingProviders;
    }

    public void setShippingProviders(List<ShippingProviderSearchDTO> shippingProviders) {
        this.shippingProviders = shippingProviders;
    }

}
