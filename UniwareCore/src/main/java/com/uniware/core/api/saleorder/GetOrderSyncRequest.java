/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 06-Jan-2015
 *  @author akshaykochhar
 */
package com.uniware.core.api.saleorder;

import com.unifier.core.api.base.ServiceRequest;

public class GetOrderSyncRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 3421483226093598886L;

}
