/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Apr 11, 2012
 *  @author singla
 */
package com.uniware.core.api.model;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

/**
 * @author singla
 */
public class WsShippingProvider {

    @NotNull
    @Min(value = 1)
    private Integer packetNumber;

    @NotBlank
    private String  code;

    @Length(max = 45)
    private String  trackingNumber;

    /**
     * @return the packetNumber
     */
    public Integer getPacketNumber() {
        return packetNumber;
    }

    /**
     * @param packetNumber the packetNumber to set
     */
    public void setPacketNumber(Integer packetNumber) {
        this.packetNumber = packetNumber;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the trackingNumber
     */
    public String getTrackingNumber() {
        return trackingNumber;
    }

    /**
     * @param trackingNumber the trackingNumber to set
     */
    public void setTrackingNumber(String trackingNumber) {
        this.trackingNumber = trackingNumber;
    }
}
