/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 14-May-2012
 *  @author praveeng
 */
package com.uniware.core.api.returns;

import com.unifier.core.api.base.ServiceResponse;

import java.util.ArrayList;
import java.util.List;

import com.uniware.core.api.packer.SaleOrderItemDTO;

/**
 * @author praveeng
 */
public class ScanReturnShipmentResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long      serialVersionUID = -2661518975756482562L;
    private List<SaleOrderItemDTO> saleOrderItems   = new ArrayList<SaleOrderItemDTO>();

    /**
     * @return the saleOrderItems
     */
    public List<SaleOrderItemDTO> getSaleOrderItems() {
        return saleOrderItems;
    }

    /**
     * @param saleOrderItems the saleOrderItems to set
     */
    public void setSaleOrderItems(List<SaleOrderItemDTO> saleOrderItems) {
        this.saleOrderItems = saleOrderItems;
    }

}
