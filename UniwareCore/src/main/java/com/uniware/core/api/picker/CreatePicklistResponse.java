/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 14, 2011
 *  @author singla
 */
package com.uniware.core.api.picker;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author singla
 */
public class CreatePicklistResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -647513971935143907L;

    public CreatePicklistResponse() {

    }

    public CreatePicklistResponse(boolean successful, String message) {
        super(successful, message);
    }

    private PicklistDTO picklist;

    public void setPicklist(PicklistDTO picklist) {
        this.picklist = picklist;
    }

    public PicklistDTO getPicklist() {
        return picklist;
    }

}
