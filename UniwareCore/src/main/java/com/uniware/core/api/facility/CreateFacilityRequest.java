/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 14, 2012
 *  @author singla
 */
package com.uniware.core.api.facility;

import com.unifier.core.api.application.SystemConfigurationDTO;
import com.unifier.core.api.base.ServiceRequest;
import com.uniware.core.api.party.WsFacility;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import java.util.List;

/**
 * @author Sunny
 */
public class CreateFacilityRequest extends ServiceRequest {

    private static final long serialVersionUID = -5518831278150233763L;

    @NotEmpty
    private String username;

    @Valid
    private WsFacility facility;

    private List<SystemConfigurationDTO> additionalSystemConfigs;

    public WsFacility getFacility() {
        return facility;
    }

    public void setFacility(WsFacility facility) {
        this.facility = facility;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public List<SystemConfigurationDTO> getAdditionalSystemConfigs() {
        return additionalSystemConfigs;
    }

    public void setAdditionalSystemConfigs(List<SystemConfigurationDTO> additionalSystemConfigs) {
        this.additionalSystemConfigs = additionalSystemConfigs;
    }
}
