package com.uniware.core.api.saleorder;

import java.util.Date;
import java.util.List;

import com.unifier.core.api.base.ServiceResponse;
import com.uniware.core.vo.SaleOrderVO;

/**
 * Created by akshayag on 9/16/15.
 */
public class GetFailedSaleOrdersResponse extends ServiceResponse {

    private List<SaleOrderDTO> saleOrders;

    public List<SaleOrderDTO> getSaleOrders() {
        return saleOrders;
    }

    public void setSaleOrders(List<SaleOrderDTO> failedSaleOrderDTO1) {
        this.saleOrders = failedSaleOrderDTO1;
    }

    public static class SaleOrderDTO {
        private String saleOrderCode;
        private String displaySaleOrderCode;
        private Date   fulfillmentTat;
        private String reason;

        public SaleOrderDTO(SaleOrderVO saleOrderVO) {
            this.saleOrderCode = saleOrderVO.getRequest().getSaleOrder().getCode();
            this.displaySaleOrderCode = saleOrderVO.getRequest().getSaleOrder().getDisplayOrderCode();
            this.fulfillmentTat = saleOrderVO.getRequest().getSaleOrder().getFulfillmentTat();
            this.reason = saleOrderVO.getResponse().getErrors().toString();
        }

        public String getReason() {
            return reason;
        }

        public void setReason(String reason) {
            this.reason = reason;
        }

        public String getSaleOrderCode() {
            return saleOrderCode;
        }

        public void setSaleOrderCode(String saleOrderCode) {
            this.saleOrderCode = saleOrderCode;
        }

        public String getDisplaySaleOrderCode() {
            return displaySaleOrderCode;
        }

        public void setDisplaySaleOrderCode(String displaySaleOrderCode) {
            this.displaySaleOrderCode = displaySaleOrderCode;
        }

        public Date getFulfillmentTat() {
            return fulfillmentTat;
        }

        public void setFulfillmentTat(Date fulfillmentTat) {
            this.fulfillmentTat = fulfillmentTat;
        }

    }

}
