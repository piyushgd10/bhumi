package com.uniware.core.api.cyclecount;

import com.uniware.core.entity.Item;

/**
 * Created by harshpal on 14/04/16.
 */
public class ErrorItemDTO {
    private String itemCode;
    private String statusCode;
    private String skuCode;
    private String itemTypeName;
    private String reasonCode;
    private String comment;

    public ErrorItemDTO(Item item, String reasonCode, String comment) {
        this.itemCode = item.getCode();
        this.statusCode = item.getStatusCode();
        this.skuCode = item.getItemType().getSkuCode();
        this.itemTypeName = item.getItemType().getName();
        this.reasonCode = reasonCode;
        this.comment = comment;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getSkuCode() {
        return skuCode;
    }

    public void setSkuCode(String skuCode) {
        this.skuCode = skuCode;
    }

    public String getReasonCode() {
        return reasonCode;
    }

    public void setReasonCode(String reasonCode) {
        this.reasonCode = reasonCode;
    }

    public String getItemTypeName() {
        return itemTypeName;
    }

    public void setItemTypeName(String itemTypeName) {
        this.itemTypeName = itemTypeName;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
