package com.uniware.core.api.cyclecount;

import com.unifier.core.api.base.ServiceResponse;

/**
 * Created by harshpal on 2/18/16.
 */
public class CreateSubCycleCountResponse extends ServiceResponse {

    private String subCycleCountCode;

    public String getSubCycleCountCode() {
        return subCycleCountCode;
    }

    public void setSubCycleCountCode(String subCycleCountCode) {
        this.subCycleCountCode = subCycleCountCode;
    }
}
