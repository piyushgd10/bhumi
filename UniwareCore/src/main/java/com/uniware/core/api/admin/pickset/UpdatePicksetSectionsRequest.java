/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 23-Feb-2012
 *  @author vibhu
 */
package com.uniware.core.api.admin.pickset;

import com.unifier.core.api.base.ServiceRequest;

import java.util.ArrayList;
import java.util.List;

/**
 * @author vibhu
 */
public class UpdatePicksetSectionsRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long       serialVersionUID   = -4811915200480686220L;
    private List<PicksetSectionDTO> picksetSectionDTOs = new ArrayList<PicksetSectionDTO>();

    /**
     * @return the picksetSectionDTOs
     */
    public List<PicksetSectionDTO> getPicksetSectionDTOs() {
        return picksetSectionDTOs;
    }

    /**
     * @param picksetSectionDTOs the picksetSectionDTOs to set
     */
    public void setPicksetSectionDTOs(List<PicksetSectionDTO> picksetSectionDTOs) {
        this.picksetSectionDTOs = picksetSectionDTOs;
    }

}