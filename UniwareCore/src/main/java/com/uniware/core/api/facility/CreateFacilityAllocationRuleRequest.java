/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Oct 22, 2012
 *  @author Pankaj
 */
package com.uniware.core.api.facility;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author Pankaj
 */
public class CreateFacilityAllocationRuleRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 6697651353632817219L;

    @NotBlank
    private String            name;

    @NotBlank
    private String            conditionExpressionText;

    @NotBlank
    private String            facilityCode;

    private boolean           enabled;

    @NotNull
    private Integer           preference;

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the conditionExpressionText
     */
    public String getConditionExpressionText() {
        return conditionExpressionText;
    }

    /**
     * @param conditionExpressionText the conditionExpressionText to set
     */
    public void setConditionExpressionText(String conditionExpressionText) {
        this.conditionExpressionText = conditionExpressionText;
    }

    /**
     * @return the facilityCode
     */
    public String getFacilityCode() {
        return facilityCode;
    }

    /**
     * @param facilityCode the facilityCode to set
     */
    public void setFacilityCode(String facilityCode) {
        this.facilityCode = facilityCode;
    }

    /**
     * @return the enabled
     */
    public boolean isEnabled() {
        return enabled;
    }

    /**
     * @param enabled the enabled to set
     */
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    /**
     * @return the preference
     */
    public Integer getPreference() {
        return preference;
    }

    /**
     * @param preference the preference to set
     */
    public void setPreference(Integer preference) {
        this.preference = preference;
    }

}
