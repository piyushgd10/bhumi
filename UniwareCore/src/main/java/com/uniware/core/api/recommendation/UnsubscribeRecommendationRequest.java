/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Nov 25, 2015
 *  @author akshay
 */
package com.uniware.core.api.recommendation;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

public class UnsubscribeRecommendationRequest extends ServiceRequest {

    private static final long serialVersionUID = -4562482373139749958L;
    
    @NotBlank
    private String recommendationType;
    @NotBlank
    private String recommendationProvider;

    public String getRecommendationType() {
        return recommendationType;
    }

    public void setRecommendationType(String recommendationType) {
        this.recommendationType = recommendationType;
    }

    public String getRecommendationProvider() {
        return recommendationProvider;
    }

    public void setRecommendationProvider(String recommendationProvider) {
        this.recommendationProvider = recommendationProvider;
    }

}
