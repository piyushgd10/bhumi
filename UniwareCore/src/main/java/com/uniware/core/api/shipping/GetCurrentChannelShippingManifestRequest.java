/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 9, 2012
 *  @author singla
 */
package com.uniware.core.api.shipping;

import javax.validation.constraints.NotNull;

import com.unifier.core.api.base.ServiceRequest;
import org.hibernate.validator.constraints.NotBlank;

/**
 * @author singla
 */
public class GetCurrentChannelShippingManifestRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 7319049617938044859L;

    @NotNull
    private String            channelCode;

    @NotBlank
    private String            shippingManifestCode;

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    public String getShippingManifestCode() {
        return shippingManifestCode;
    }

    public void setShippingManifestCode(String shippingManifestCode) {
        this.shippingManifestCode = shippingManifestCode;
    }
}
