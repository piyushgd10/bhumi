/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 11-Aug-2012
 *  @author praveeng
 */
package com.uniware.core.api.admin.shipping;

import java.util.HashMap;
import java.util.Map;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author praveeng
 */
public class ReorderProviderAllocationRulesRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long    serialVersionUID                  = -3624265116388653677L;
    private Map<String, Integer> allocationRuleNameToPreferenceMap = new HashMap<String, Integer>();

    /**
     * @return the allocationRuleNameToPreferenceMap
     */
    public Map<String, Integer> getAllocationRuleNameToPreferenceMap() {
        return allocationRuleNameToPreferenceMap;
    }

    /**
     * @param allocationRuleNameToPreferenceMap the allocationRuleNameToPreferenceMap to set
     */
    public void setAllocationRuleNameToPreferenceMap(Map<String, Integer> allocationRuleNameToPreferenceMap) {
        this.allocationRuleNameToPreferenceMap = allocationRuleNameToPreferenceMap;
    }

}
