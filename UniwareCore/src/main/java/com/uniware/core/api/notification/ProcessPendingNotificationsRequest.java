/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 23, 2012
 *  @author singla
 */
package com.uniware.core.api.notification;

import java.util.List;
import java.util.Set;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.unifier.core.api.base.ServiceRequest;
import com.uniware.core.entity.Notification;

/**
 * @author Sunny
 */
public class ProcessPendingNotificationsRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long  serialVersionUID = -1183756618265880005L;

    @NotNull
    private List<Notification> pendingNotifications;

    @NotEmpty
    private Set<String>        channelsToProcess;

    public ProcessPendingNotificationsRequest() {
    }

    public ProcessPendingNotificationsRequest(List<Notification> pendingNotifications, Set<String> channelsToProcess) {
        this.pendingNotifications = pendingNotifications;
        this.channelsToProcess = channelsToProcess;
    }

    public List<Notification> getPendingNotifications() {
        return pendingNotifications;
    }

    public void setPendingNotifications(List<Notification> pendingNotifications) {
        this.pendingNotifications = pendingNotifications;
    }

    public Set<String> getChannelsToProcess() {
        return channelsToProcess;
    }

    public void setChannelsToProcess(Set<String> channelsToProcess) {
        this.channelsToProcess = channelsToProcess;
    }

}
