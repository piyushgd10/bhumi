/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 30-Oct-2014
 *  @author sunny
 */
package com.uniware.core.api.reconciliation;

import com.unifier.core.entity.SaleOrderReconciliation;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class WsSaleOrderPaymentReconciliation {
    private List<WsPaymentReconciliationEntry> paymentReconciliationEntries;

    public List<WsPaymentReconciliationEntry> getPaymentReconciliationEntries() {
        return paymentReconciliationEntries;
    }

    public void setPaymentReconciliationEntries(List<WsPaymentReconciliationEntry> paymentReconciliationEntries) {
        this.paymentReconciliationEntries = paymentReconciliationEntries;
    }

    public static class WsPaymentReconciliationEntry {

        private String     channelCode;
        private String     saleOrderCode;
        private BigDecimal sellingPrice    = BigDecimal.ZERO;
        private BigDecimal shippingCharges = BigDecimal.ZERO;
        private String     statusCode      = SaleOrderReconciliation.ReconciliationStatus.UNRECONCILED.name();
        private String     reconciliationIdentifier;
        private String     channelSaleOrderCode;
        private String     currencyCode;
        private Date       created;
        private Date       updated;
        private Date       lastSettledDate;
        private Date       orderTransactionDate;
        private Date       reconcileCreated;

        public String getReconciliationIdentifier() {
            return reconciliationIdentifier;
        }

        public void setReconciliationIdentifier(String reconciliationIdentifier) {
            this.reconciliationIdentifier = reconciliationIdentifier;
        }

        public Date getLastSettledDate() {
            return lastSettledDate;
        }

        public void setLastSettledDate(Date lastSettledDate) {
            this.lastSettledDate = lastSettledDate;
        }

        public Date getOrderTransactionDate() {
            return orderTransactionDate;
        }

        public void setOrderTransactionDate(Date orderTransactionDate) {
            this.orderTransactionDate = orderTransactionDate;
        }

        public Date getReconcileCreated() {
            return reconcileCreated;
        }

        public void setReconcileCreated(Date reconcileCreated) {
            this.reconcileCreated = reconcileCreated;
        }

        public String getChannelCode() {
            return channelCode;
        }

        public void setChannelCode(String channelCode) {
            this.channelCode = channelCode;
        }

        public String getSaleOrderCode() {
            return saleOrderCode;
        }

        public void setSaleOrderCode(String saleOrderCode) {
            this.saleOrderCode = saleOrderCode;
        }

        public BigDecimal getSellingPrice() {
            return sellingPrice;
        }

        public void setSellingPrice(BigDecimal sellingPrice) {
            this.sellingPrice = sellingPrice;
        }

        public String getChannelSaleOrderCode() {
            return channelSaleOrderCode;
        }

        public void setChannelSaleOrderCode(String channelSaleOrderCode) {
            this.channelSaleOrderCode = channelSaleOrderCode;
        }

        public BigDecimal getShippingCharges() {
            return shippingCharges;
        }

        public void setShippingCharges(BigDecimal shippingCharges) {
            this.shippingCharges = shippingCharges;
        }

        public String getStatusCode() {
            return statusCode;
        }

        public void setStatusCode(String statusCode) {
            this.statusCode = statusCode;
        }

        public String getCurrencyCode() {
            return currencyCode;
        }

        public void setCurrencyCode(String currencyCode) {
            this.currencyCode = currencyCode;
        }

        public Date getCreated() {
            return created;
        }

        public void setCreated(Date created) {
            this.created = created;
        }

        public Date getUpdated() {
            return updated;
        }

        public void setUpdated(Date updated) {
            this.updated = updated;
        }
    }
}