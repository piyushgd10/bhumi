/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 05-Sep-2013
 *  @author parijat
 */
package com.uniware.core.api.channel;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author Sunny
 */
public class ChangeChannelInventorySyncStatusRequest extends ServiceRequest {
    private static final long serialVersionUID = -3013467246317900363L;

    private String            channelCode;

    private String            statusCode;

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

}
