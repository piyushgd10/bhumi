package com.uniware.core.api.picker;

import com.unifier.core.api.base.ServiceResponse;

/**
 * Created by bhuvneshwarkumar on 29/08/16.
 */
public class CreatePickBatchResponse extends ServiceResponse {

    private static final long serialVersionUID = -7120853430769456995L;
}
