/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 29, 2012
 *  @author singla
 */
package com.uniware.core.api.purchase;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author singla
 */
public class EditPurchaseOrderItemResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long    serialVersionUID = -6945971815712181504L;

    private PurchaseOrderItemDTO purchaseOrderItem;

    /**
     * @return the purchaseOrderItem
     */
    public PurchaseOrderItemDTO getPurchaseOrderItem() {
        return purchaseOrderItem;
    }

    /**
     * @param purchaseOrderItem the purchaseOrderItem to set
     */
    public void setPurchaseOrderItem(PurchaseOrderItemDTO purchaseOrderItem) {
        this.purchaseOrderItem = purchaseOrderItem;
    }
}
