/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, May 1, 2012
 *  @author singla
 */
package com.uniware.core.api.inflow;

import com.unifier.core.api.base.ServiceRequest;

import javax.validation.constraints.NotNull;

/**
 * @author singla
 */
public class CreateReceiptItemCodesRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 5658786822293316296L;

    @NotNull
    private Integer           inflowReceiptItemId;

    /**
     * @return the inflowReceiptItemId
     */
    public Integer getInflowReceiptItemId() {
        return inflowReceiptItemId;
    }

    /**
     * @param inflowReceiptItemId the inflowReceiptItemId to set
     */
    public void setInflowReceiptItemId(Integer inflowReceiptItemId) {
        this.inflowReceiptItemId = inflowReceiptItemId;
    }
}
