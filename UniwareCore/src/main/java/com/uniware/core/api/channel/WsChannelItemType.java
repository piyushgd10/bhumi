/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 24-Dec-2011
 *  @author vibhu
 */
package com.uniware.core.api.channel;

import java.math.BigDecimal;
import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import org.hibernate.validator.constraints.NotBlank;

/**
 * @author Sunny Agarwal
 */
public class WsChannelItemType {

    @NotBlank
    private String                           channelCode;

    @NotBlank
    private String                           channelProductId;

    private String                           sellerSkuCode;

    private String                           skuCode;

    private String                           productName;

    private String                           productUrl;

    private String                           size;

    private String                           color;

    private String                           brand;

    private Set<String>                      imageUrls;

    private String                           productDescription;

    private String                           vertical;

    private Integer                          blockedInventory;

    private int                              pendency;

    private boolean                          live = true;

    private boolean                          verified;

    private Boolean                          disabled;

    private BigDecimal                       commissionPercentage;

    private BigDecimal                       paymentGatewayCharge;

    private BigDecimal                       logisticsCost;

    private Integer                          currentInventoryOnChannel;

    private BigDecimal                       transferPrice;

    @Valid
    private List<WsChannelItemTypeAttribute> attributes;

    public WsChannelItemType() {
    }

    public String getChannelProductId() {
        return channelProductId;
    }

    public void setChannelProductId(String channelProductId) {
        this.channelProductId = channelProductId;
    }

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    public String getSellerSkuCode() {
        return sellerSkuCode;
    }

    public void setSellerSkuCode(String sellerSkuCode) {
        this.sellerSkuCode = sellerSkuCode;
    }

    public String getSkuCode() {
        return skuCode;
    }

    public void setSkuCode(String skuCode) {
        this.skuCode = skuCode;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductUrl() {
        return productUrl;
    }

    public void setProductUrl(String productUrl) {
        this.productUrl = productUrl;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public Set<String> getImageUrls() {
        return imageUrls;
    }

    public void setImageUrls(Set<String> imageUrls) {
        this.imageUrls = imageUrls;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public String getVertical() {
        return vertical;
    }

    public void setVertical(String vertical) {
        this.vertical = vertical;
    }

    public Integer getBlockedInventory() {
        return blockedInventory;
    }

    public void setBlockedInventory(Integer blockedInventory) {
        this.blockedInventory = blockedInventory;
    }

    public int getPendency() {
        return pendency;
    }

    public void setPendency(int pendency) {
        this.pendency = pendency;
    }

    public boolean isLive() {
        return live;
    }

    public void setLive(boolean live) {
        this.live = live;
    }

    public boolean isVerified() {
        return verified;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }

    public Boolean getDisabled() {
        return disabled;
    }

    public void setDisabled(Boolean disabled) {
        this.disabled = disabled;
    }

    public List<WsChannelItemTypeAttribute> getAttributes() {
        return attributes;
    }

    public void setAttributes(List<WsChannelItemTypeAttribute> attributes) {
        this.attributes = attributes;
    }

    public BigDecimal getCommissionPercentage() {
        return commissionPercentage;
    }

    public void setCommissionPercentage(BigDecimal commissionPercentage) {
        this.commissionPercentage = commissionPercentage;
    }

    public BigDecimal getPaymentGatewayCharge() {
        return paymentGatewayCharge;
    }

    public void setPaymentGatewayCharge(BigDecimal paymentGatewayCharge) {
        this.paymentGatewayCharge = paymentGatewayCharge;
    }

    public BigDecimal getLogisticsCost() {
        return logisticsCost;
    }

    public void setLogisticsCost(BigDecimal logisticsCost) {
        this.logisticsCost = logisticsCost;
    }

    public Integer getCurrentInventoryOnChannel() {
        return currentInventoryOnChannel;
    }

    public void setCurrentInventoryOnChannel(Integer currentInventoryOnChannel) {
        this.currentInventoryOnChannel = currentInventoryOnChannel;
    }

    public BigDecimal getTransferPrice() {
        return transferPrice;
    }

    public void setTransferPrice(BigDecimal transferPrice) {
        this.transferPrice = transferPrice;
    }

    @Override
    public String toString() {
        return "WsChannelItemType{" + "channelCode='" + channelCode + '\'' + ", channelProductId='" + channelProductId + '\'' + ", sellerSkuCode='" + sellerSkuCode + '\''
                + ", skuCode='" + skuCode + '\'' + ", productName='" + productName + '\'' + ", productUrl='" + productUrl + '\'' + ", size='" + size + '\'' + ", color='" + color
                + '\'' + ", brand='" + brand + '\'' + ", imageUrls=" + imageUrls + ", productDescription='" + productDescription + '\'' + ", vertical='" + vertical + '\''
                + ", blockedInventory=" + blockedInventory + ", pendency=" + pendency + ", live=" + live + ", verified=" + verified + ", disabled=" + disabled
                + ", commissionPercentage=" + commissionPercentage + ", paymentGatewayCharge=" + paymentGatewayCharge + ", logisticsCost=" + logisticsCost
                + ", currentInventoryOnChannel=" + currentInventoryOnChannel + ", transferPrice=" + transferPrice + ", attributes=" + attributes + '}';
    }
}
