/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 20, 2012
 *  @author praveeng
 */
package com.uniware.core.api.party;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author praveeng
 */
public class EditPartyContactRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -4017975225606166158L;
    /**
     * 
     */
    @NotNull
    @Valid
    private WsPartyContact    partyContact;

    public EditPartyContactRequest() {
    }

    public EditPartyContactRequest(WsPartyContact partyContact) {
        this.partyContact = partyContact;
    }

    /**
     * @return the partyContact
     */
    public WsPartyContact getPartyContact() {
        return partyContact;
    }

    /**
     * @param partyContact the partyContact to set
     */
    public void setPartyContact(WsPartyContact partyContact) {
        this.partyContact = partyContact;
    }
}
