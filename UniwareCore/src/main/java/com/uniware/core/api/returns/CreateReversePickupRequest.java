/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 9, 2012
 *  @author singla
 */
package com.uniware.core.api.returns;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.unifier.core.api.base.ServiceRequest;
import com.unifier.core.api.customfields.WsCustomFieldValue;
import com.uniware.core.api.model.WsAddressDetail;

/**
 * @author singla
 */
public class CreateReversePickupRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long         serialVersionUID = 4795720002305203341L;

    @NotEmpty
    private String                    saleOrderCode;

    @NotEmpty
    private List<WsReversePickupItem> reversePickItems;

    @Valid
    private WsAddressDetail           shippingAddress;

    @NotNull
    private String                    actionCode;

    @Valid
    private List<WsCustomFieldValue>  customFieldValues;

    private String                    reversePickupCode;

    /**
     * @return the saleOrderCode
     */
    public String getSaleOrderCode() {
        return saleOrderCode;
    }

    /**
     * @param saleOrderCode the saleOrderCode to set
     */
    public void setSaleOrderCode(String saleOrderCode) {
        this.saleOrderCode = saleOrderCode;
    }

    /**
     * @return the actionCode
     */
    public String getActionCode() {
        return actionCode;
    }

    /**
     * @param actionCode the actionCode to set
     */
    public void setActionCode(String actionCode) {
        this.actionCode = actionCode;
    }

    /**
     * @return the shippingAddress
     */
    public WsAddressDetail getShippingAddress() {
        return shippingAddress;
    }

    /**
     * @param shippingAddress the shippingAddress to set
     */
    public void setShippingAddress(WsAddressDetail shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    /**
     * @return the reversePickItems
     */
    public List<WsReversePickupItem> getReversePickItems() {
        return reversePickItems;
    }

    /**
     * @param reversePickItems the reversePickItems to set
     */
    public void setReversePickItems(List<WsReversePickupItem> reversePickItems) {
        this.reversePickItems = reversePickItems;
    }

    /**
     * @return the customFieldValues
     */
    public List<WsCustomFieldValue> getCustomFieldValues() {
        return customFieldValues;
    }

    /**
     * @param customFieldValues the customFieldValues to set
     */
    public void setCustomFieldValues(List<WsCustomFieldValue> customFieldValues) {
        this.customFieldValues = customFieldValues;
    }

    public String getReversePickupCode() {
        return reversePickupCode;
    }

    public void setReversePickupCode(String reversePickupCode) {
        this.reversePickupCode = reversePickupCode;
    }

}
