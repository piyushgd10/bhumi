/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 10-Jan-2014
 *  @author parijat
 */
package com.uniware.core.api.reconciliation;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.unifier.core.api.base.ServiceRequest;
import com.unifier.core.entity.SaleOrderReconciliation;

public class MarkChannelPaymentsDisputedRequest extends ServiceRequest {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     *
     */

    @NotNull
    @Valid
    private List<WSChannelReconciliationIdentifier> channelRecoIdentifiers;

    private SaleOrderReconciliation.ReconciliationStatus statusCode;

    /**
     * @return the channelRecoIdentifiers
     */
    public List<WSChannelReconciliationIdentifier> getChannelRecoIdentifiers() {
        return channelRecoIdentifiers;
    }

    /**
     * @param channelRecoIdentifiers the channelRecoIdentifiers to set
     */
    public void setChannelRecoIdentifiers(List<WSChannelReconciliationIdentifier> channelRecoIdentifiers) {
        this.channelRecoIdentifiers = channelRecoIdentifiers;
    }

    public SaleOrderReconciliation.ReconciliationStatus getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(SaleOrderReconciliation.ReconciliationStatus statusCode) {
        this.statusCode = statusCode;
    }
}
