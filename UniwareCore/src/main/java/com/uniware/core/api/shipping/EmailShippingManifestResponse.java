/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 7, 2012
 *  @author singla
 */
package com.uniware.core.api.shipping;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author singla
 */
public class EmailShippingManifestResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 7201260112069288570L;

    private Integer           shippingManifestId;
    private String            shippingManifestCode;

    /**
     * @return the shippingManifestId
     */
    public Integer getShippingManifestId() {
        return shippingManifestId;
    }

    /**
     * @param shippingManifestId the shippingManifestId to set
     */
    public void setShippingManifestId(Integer shippingManifestId) {
        this.shippingManifestId = shippingManifestId;
    }

    /**
     * @return the shippingManifestCode
     */
    public String getShippingManifestCode() {
        return shippingManifestCode;
    }

    /**
     * @param shippingManifestCode the shippingManifestCode to set
     */
    public void setShippingManifestCode(String shippingManifestCode) {
        this.shippingManifestCode = shippingManifestCode;
    }

}
