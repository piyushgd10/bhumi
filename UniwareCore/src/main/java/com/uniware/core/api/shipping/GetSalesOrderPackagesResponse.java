/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jan 1, 2013
 *  @author praveeng
 */
package com.uniware.core.api.shipping;

import com.unifier.core.api.base.ServiceResponse;

import java.util.ArrayList;
import java.util.List;

import com.uniware.core.api.saleorder.ShippingPackageDTO;

/**
 * @author praveeng
 */
public class GetSalesOrderPackagesResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long        serialVersionUID = 6450122808444320902L;

    private List<ShippingPackageDTO> elements         = new ArrayList<ShippingPackageDTO>();

    /**
     * @return the elements
     */
    public List<ShippingPackageDTO> getElements() {
        return elements;
    }

    /**
     * @param elements the elements to set
     */
    public void setElements(List<ShippingPackageDTO> elements) {
        this.elements = elements;
    }

}
