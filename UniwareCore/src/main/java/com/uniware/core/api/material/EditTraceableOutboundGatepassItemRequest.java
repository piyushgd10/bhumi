/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Apr 30, 2015
 *  @author akshay
 */
package com.uniware.core.api.material;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

public class EditTraceableOutboundGatepassItemRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 7182991257141728538L;
    
    @NotBlank
    private String            gatepassCode;

    @NotBlank
    private String            itemCode;
    
    private String            reason;
    
    public String getGatepassCode() {
        return gatepassCode;
    }

    public void setGatepassCode(String gatepassCode) {
        this.gatepassCode = gatepassCode;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

}
