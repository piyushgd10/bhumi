/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 16, 2012
 *  @author praveeng
 */
package com.uniware.core.api.inventory;

import com.unifier.core.api.base.ServiceRequest;
import com.unifier.core.pagination.SearchOptions;

/**
 * @author praveeng
 */
public class SearchInventoryRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -2636835681971242046L;
    private String            itemType;
    private String            shelf;
    private SearchOptions     searchOptions;

    /**
     * @return the itemType
     */
    public String getItemType() {
        return itemType;
    }

    /**
     * @param itemType the itemType to set
     */
    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    /**
     * @return the shelf
     */
    public String getShelf() {
        return shelf;
    }

    /**
     * @param shelf the shelf to set
     */
    public void setShelf(String shelf) {
        this.shelf = shelf;
    }

    /**
     * @return the searchOptions
     */
    public SearchOptions getSearchOptions() {
        return searchOptions;
    }

    /**
     * @param searchOptions the searchOptions to set
     */
    public void setSearchOptions(SearchOptions searchOptions) {
        this.searchOptions = searchOptions;
    }

}
