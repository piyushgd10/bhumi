/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jun 20, 2012
 *  @author singla
 */
package com.uniware.core.api.usernotification;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author Sunny
 */
public class AddUserNotificationRespone extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 5136222167598910731L;

}
