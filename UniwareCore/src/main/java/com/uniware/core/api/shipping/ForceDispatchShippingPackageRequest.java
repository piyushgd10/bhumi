/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 17-Jun-2013
 *  @author pankaj
 */
package com.uniware.core.api.shipping;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

import javax.validation.constraints.NotNull;

public class ForceDispatchShippingPackageRequest extends ServiceRequest {

    /**
     *
     */
    private static final long serialVersionUID = 1044511041185967721L;

    @NotBlank
    private String            shippingPackageCode;

    private String            shippingProviderCode;

    private String            trackingNumber;

    private boolean           skipDetailing    = true;

    @NotNull
    private Integer           userId;

    public ForceDispatchShippingPackageRequest() {
        super();
    }

    public String getShippingPackageCode() {
        return shippingPackageCode;
    }

    public void setShippingPackageCode(String shippingPackageCode) {
        this.shippingPackageCode = shippingPackageCode;
    }

    public String getShippingProviderCode() {
        return shippingProviderCode;
    }

    public void setShippingProviderCode(String shippingProviderCode) {
        this.shippingProviderCode = shippingProviderCode;
    }

    public String getTrackingNumber() {
        return trackingNumber;
    }

    public void setTrackingNumber(String trackingNumber) {
        this.trackingNumber = trackingNumber;
    }

    public boolean isSkipDetailing() {
        return skipDetailing;
    }

    public void setSkipDetailing(boolean skipDetailing) {
        this.skipDetailing = skipDetailing;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }
}
