/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 25-Jan-2012
 *  @author vibhu
 */
package com.uniware.core.api.party.dto;

import java.util.HashMap;
import java.util.Map;

import com.uniware.core.api.party.PartyAddressDTO;
import com.uniware.core.api.party.PartyContactDTO;
import com.uniware.core.entity.Party;
import com.uniware.core.entity.PartyAddressType;
import com.uniware.core.entity.PartyContact;
import com.uniware.core.utils.Constants;

/**
 * @author Sunny
 */
public class GenericPartyDTO {
    private String                       name;
    private String                       code;
    private String                       alternateCode;
    private String                       pan;
    private String                       tin;
    private String                       cstNumber;
    private String                       cinNumber;
    private String                       gstNumber;
    private String                       stNumber;
    private boolean                      enabled;
    private boolean                      taxExempted;
    private String                       website;
    private boolean                      registeredDealer;
    private String                       logoUrl;
    private String                       signatureUrl;
    private String                       uniwareAccessUrl;
    private String                       uniwareApiUser;
    private String                       uniwareApiPassword;
    private PartyAddressDTO              billingAddress;
    private PartyAddressDTO              shippingAddress;
    private Map<String, PartyContactDTO> partyContacts = new HashMap<>(1);

    public GenericPartyDTO() {
    }

    public GenericPartyDTO(Party party) {
        name = party.getName();
        code = party.getCode();
        alternateCode = party.getAlternateCode();
        pan = party.getPan();
        tin = party.getTin();
        cstNumber = party.getCstNumber();
        cinNumber = party.getCinNumber();
        gstNumber = party.getGstNumber();
        stNumber = party.getStNumber();
        enabled = party.isEnabled();
        taxExempted = party.isTaxExempted();
        website = party.getWebsite();
        registeredDealer = party.isRegisteredDealer();
        logoUrl = party.getBinaryObject() != null ? (party.getBinaryObject().getId() > 0 ? Constants.OBJECT_STORE_VIEW_PREPEND + party.getBinaryObject().getId() : "") : "";
        signatureUrl = party.getSignatureBinaryObject() != null ? (party.getSignatureBinaryObject().getId() > 0 ? Constants.OBJECT_STORE_VIEW_PREPEND
                + party.getSignatureBinaryObject().getId() : "") : "";
        uniwareAccessUrl = party.getUniwareAccessUrl();
        uniwareApiUser = party.getUniwareApiUser();
        uniwareApiPassword = party.getUniwareApiPassword();
        billingAddress = new PartyAddressDTO(party.getPartyAddressByType(PartyAddressType.Code.BILLING.name()));
        shippingAddress = new PartyAddressDTO(party.getPartyAddressByType(PartyAddressType.Code.SHIPPING.name()));
        for (PartyContact partyContact : party.getPartyContacts()) {
            partyContacts.put(partyContact.getPartyContactType().getCode(), new PartyContactDTO(partyContact));
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getPan() {
        return pan;
    }

    public void setPan(String pan) {
        this.pan = pan;
    }

    public String getTin() {
        return tin;
    }

    public void setTin(String tin) {
        this.tin = tin;
    }

    public String getCstNumber() {
        return cstNumber;
    }

    public void setCstNumber(String cstNumber) {
        this.cstNumber = cstNumber;
    }

    public String getCinNumber() {
        return cinNumber;
    }

    public void setCinNumber(String cinNumber) {
        this.cinNumber = cinNumber;
    }

    public String getStNumber() {
        return stNumber;
    }

    public void setStNumber(String stNumber) {
        this.stNumber = stNumber;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public boolean isTaxExempted() {
        return taxExempted;
    }

    public void setTaxExempted(boolean taxExempted) {
        this.taxExempted = taxExempted;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public boolean isRegisteredDealer() {
        return registeredDealer;
    }

    public void setRegisteredDealer(boolean registeredDealer) {
        this.registeredDealer = registeredDealer;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    public String getSignatureUrl() {
        return signatureUrl;
    }

    public void setSignatureUrl(String signatureUrl) {
        this.signatureUrl = signatureUrl;
    }

    public String getUniwareAccessUrl() {
        return uniwareAccessUrl;
    }

    public void setUniwareAccessUrl(String uniwareAccessUrl) {
        this.uniwareAccessUrl = uniwareAccessUrl;
    }

    public String getUniwareApiUser() {
        return uniwareApiUser;
    }

    public void setUniwareApiUser(String uniwareApiUser) {
        this.uniwareApiUser = uniwareApiUser;
    }

    public String getUniwareApiPassword() {
        return uniwareApiPassword;
    }

    public void setUniwareApiPassword(String uniwareApiPassword) {
        this.uniwareApiPassword = uniwareApiPassword;
    }

    public PartyAddressDTO getBillingAddress() {
        return billingAddress;
    }

    public void setBillingAddress(PartyAddressDTO billingAddress) {
        this.billingAddress = billingAddress;
    }

    public PartyAddressDTO getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(PartyAddressDTO shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    public Map<String, PartyContactDTO> getPartyContacts() {
        return partyContacts;
    }

    public void setPartyContacts(Map<String, PartyContactDTO> partyContacts) {
        this.partyContacts = partyContacts;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((code == null) ? 0 : code.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        GenericPartyDTO other = (GenericPartyDTO) obj;
        if (code == null) {
            if (other.code != null)
                return false;
        } else if (!code.equals(other.code))
            return false;
        return true;
    }

    public String getAlternateCode() {
        return alternateCode;
    }

    public void setAlternateCode(String alternateCode) {
        this.alternateCode = alternateCode;
    }

    public String getGstNumber() {
        return gstNumber;
    }

    public void setGstNumber(String gstNumber) {
        this.gstNumber = gstNumber;
    }
}