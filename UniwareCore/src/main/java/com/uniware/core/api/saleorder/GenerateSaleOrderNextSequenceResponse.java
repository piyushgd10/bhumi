/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 13-Aug-2014
 *  @author akshay
 */
package com.uniware.core.api.saleorder;

import com.unifier.core.api.base.ServiceResponse;

public class GenerateSaleOrderNextSequenceResponse extends ServiceResponse{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    private String nextSequence;
    
    public String getNextSequence() {
        return nextSequence;
    }

    public void setNextSequence(String nextSequence) {
        this.nextSequence = nextSequence;
    }

}
