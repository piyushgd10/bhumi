/*
 * Copyright 2016 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 9/3/16 11:46 AM
 * @author amdalal
 */

package com.uniware.core.api.admin.pickset;

import org.hibernate.validator.constraints.NotEmpty;

import com.unifier.core.api.base.ServiceRequest;

public class CreatePickAreaRequest extends ServiceRequest {

    private static final long serialVersionUID = -2600783183996511983L;

    @NotEmpty
    private String            name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
