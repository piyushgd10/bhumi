/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 03-Jun-2012
 *  @author vibhu
 */
package com.uniware.core.api.channel;

import javax.validation.Valid;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author Sunny
 */
public class AddChannelRequest extends ServiceRequest {

    private static final long serialVersionUID = 265792286152502109L;
    
    @Valid
    private WsChannel         wsChannel;

    public WsChannel getWsChannel() {
        return wsChannel;
    }

    public void setWsChannel(WsChannel wsChannel) {
        this.wsChannel = wsChannel;
    }

}
