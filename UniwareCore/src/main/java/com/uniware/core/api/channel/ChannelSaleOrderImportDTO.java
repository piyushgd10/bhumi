/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 18-Apr-2013
 *  @author unicom
 */
package com.uniware.core.api.channel;

import com.uniware.core.entity.Channel;

/**
 * @author Sunny Agarwal
 */
public class ChannelSaleOrderImportDTO {

    private Integer id;
    private String  channelCode;
    private String  sourceCode;
    private String  channelName;
    private String  instructions;

    public ChannelSaleOrderImportDTO() {

    }

    public ChannelSaleOrderImportDTO(Channel channel) {
        id = channel.getId();
        channelCode = channel.getCode();
        channelName = channel.getName();
        sourceCode = channel.getSourceCode();
    }

    public ChannelSaleOrderImportDTO(ChannelDetailDTO channel) {
        id = channel.getId();
        channelCode = channel.getCode();
        channelName = channel.getName();
        sourceCode = channel.getSource().getCode();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    public String getSourceCode() {
        return sourceCode;
    }

    public void setSourceCode(String sourceCode) {
        this.sourceCode = sourceCode;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public String getInstructions() {
        return instructions;
    }

    public void setInstructions(String instructions) {
        this.instructions = instructions;
    }

}
