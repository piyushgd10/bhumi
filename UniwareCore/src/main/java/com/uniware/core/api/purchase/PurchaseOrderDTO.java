/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 11, 2012
 *  @author praveeng
 */
package com.uniware.core.api.purchase;

import java.util.Date;
import java.util.List;

import com.unifier.core.api.customfields.CustomFieldMetadataDTO;
import com.uniware.core.entity.PurchaseOrder;

/**
 * @author praveeng
 */
public class PurchaseOrderDTO {

    private String                       code;
    private String                       fromParty;
    private String                       vendorCode;
    private String                       vendorName;
    private String                       vendorAgreement;
    private String                       statusCode;
    private Date                         created;
    private Date                         approved;
    private Date                         deliveryDate;
    private Date                         expiryDate;
    private List<PurchaseOrderItemDTO>   purchaseOrderItems;
    private List<CustomFieldMetadataDTO> customFieldValues;

    /**
     * 
     */
    public PurchaseOrderDTO() {
    }

    public PurchaseOrderDTO(PurchaseOrder purchaseOrder) {
        setFromParty(purchaseOrder.getFromParty().getName());
        setVendorCode(purchaseOrder.getVendor().getCode());
        setVendorName(purchaseOrder.getVendor().getName());
        if(!(purchaseOrder.getVendorAgreement() == null)){
            setVendorAgreement(purchaseOrder.getVendorAgreement().getName());
        }
        setStatusCode(purchaseOrder.getStatusCode());
        setCreated(purchaseOrder.getCreated());
        setApproved(purchaseOrder.getApproveTime());
        setExpiryDate(purchaseOrder.getExpiryDate());
        setDeliveryDate(purchaseOrder.getDeliveryDate());
        this.code = purchaseOrder.getCode();
    }

    /**
     * @return the fromParty
     */
    public String getFromParty() {
        return fromParty;
    }

    /**
     * @param fromParty the fromParty to set
     */
    public void setFromParty(String fromParty) {
        this.fromParty = fromParty;
    }

    /**
     * @return the vendorAgreement
     */
    public String getVendorAgreement() {
        return vendorAgreement;
    }

    /**
     * @param vendorAgreement the vendorAgreement to set
     */
    public void setVendorAgreement(String vendorAgreement) {
        this.vendorAgreement = vendorAgreement;
    }

    /**
     * @return the statusCode
     */
    public String getStatusCode() {
        return statusCode;
    }

    /**
     * @param statusCode the statusCode to set
     */
    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    /**
     * @return the vendorName
     */
    public String getVendorName() {
        return vendorName;
    }

    /**
     * @param vendorName the vendorName to set
     */
    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    /**
     * @return the created
     */
    public Date getCreated() {
        return created;
    }

    /**
     * @param created the created to set
     */
    public void setCreated(Date created) {
        this.created = created;
    }

    /**
     * @return the vendorCode
     */
    public String getVendorCode() {
        return vendorCode;
    }

    /**
     * @param vendorCode the vendorCode to set
     */
    public void setVendorCode(String vendorCode) {
        this.vendorCode = vendorCode;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the approved
     */
    public Date getApproved() {
        return approved;
    }

    /**
     * @param approved the approved to set
     */
    public void setApproved(Date approved) {
        this.approved = approved;
    }

    /**
     * @return the deliveryDate
     */
    public Date getDeliveryDate() {
        return deliveryDate;
    }

    /**
     * @param deliveryDate the deliveryDate to set
     */
    public void setDeliveryDate(Date deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    /**
     * @return the expiryDate
     */
    public Date getExpiryDate() {
        return expiryDate;
    }

    /**
     * @param expiryDate the expiryDate to set
     */
    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    public List<PurchaseOrderItemDTO> getPurchaseOrderItems() {
        return purchaseOrderItems;
    }

    public void setPurchaseOrderItems(List<PurchaseOrderItemDTO> purchaseOrderItems) {
        this.purchaseOrderItems = purchaseOrderItems;
    }

    /**
     * @return the customFieldValues
     */
    public List<CustomFieldMetadataDTO> getCustomFieldValues() {
        return customFieldValues;
    }

    /**
     * @param customFieldValues the customFieldValues to set
     */
    public void setCustomFieldValues(List<CustomFieldMetadataDTO> customFieldValues) {
        this.customFieldValues = customFieldValues;
    }

}
