/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 03-Jun-2012
 *  @author vibhu
 */
package com.uniware.core.api.usernotification;

import java.util.List;

import com.unifier.core.api.base.ServiceRequest;
import com.uniware.core.vo.UserNotificationTypeVO.NotificationType;

/**
 * @author Sunny
 */
public class GetUserNotificationsRequest extends ServiceRequest {

    private static final long      serialVersionUID = -767335784096230275L;

    private List<NotificationType> notificationTypes;
    private int                    start            = 0;
    private int                    pageSize         = 10;

    public List<NotificationType> getNotificationTypes() {
        return notificationTypes;
    }

    public void setNotificationTypes(List<NotificationType> notificationTypes) {
        this.notificationTypes = notificationTypes;
    }

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

}
