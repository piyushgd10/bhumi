/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 26-May-2015
 *  @author akshay
 */
package com.uniware.core.api.shipping;

import com.unifier.core.api.base.ServiceResponse;

public class AutoDispatchShippingPackageResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -7047240199565782341L;
    
    
}
