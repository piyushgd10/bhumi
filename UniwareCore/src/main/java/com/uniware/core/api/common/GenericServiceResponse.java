/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 29-Jan-2014
 *  @author karunsingla
 */
package com.uniware.core.api.common;

import com.unifier.core.api.base.ServiceResponse;

public class GenericServiceResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -5963718474933688617L;

    private Object            payload;

    public Object getPayload() {
        return payload;
    }

    public void setPayload(Object payload) {
        this.payload = payload;
    }
}
