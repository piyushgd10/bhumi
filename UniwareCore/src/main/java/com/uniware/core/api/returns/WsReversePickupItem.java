/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Oct 12, 2012
 *  @author singla
 */
package com.uniware.core.api.returns;

import javax.validation.Valid;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

/**
 * @author singla
 */
public class WsReversePickupItem {
    @NotBlank
    private String                   saleOrderItemCode;

    @NotBlank
    @Length(max = 500, min = 1)
    private String                   reason;

    @Valid
    private WsReversePickupAlternate reversePickupAlternate;

    public WsReversePickupItem() {
        super();
    }

    public WsReversePickupItem(String saleOrderItemCode, String reason) {
        super();
        this.saleOrderItemCode = saleOrderItemCode;
        this.reason = reason;
    }

    /**
     * @return the saleOrderItemCode
     */
    public String getSaleOrderItemCode() {
        return saleOrderItemCode;
    }

    /**
     * @param saleOrderItemCode the saleOrderItemCode to set
     */
    public void setSaleOrderItemCode(String saleOrderItemCode) {
        this.saleOrderItemCode = saleOrderItemCode;
    }

    /**
     * @return the reversePickupAlternate
     */
    public WsReversePickupAlternate getReversePickupAlternate() {
        return reversePickupAlternate;
    }

    /**
     * @param reversePickupAlternate the reversePickupAlternate to set
     */
    public void setReversePickupAlternate(WsReversePickupAlternate reversePickupAlternate) {
        this.reversePickupAlternate = reversePickupAlternate;
    }

    /**
     * @return the reason
     */
    public String getReason() {
        return reason;
    }

    /**
     * @param reason the reason to set
     */
    public void setReason(String reason) {
        this.reason = reason;
    }

}
