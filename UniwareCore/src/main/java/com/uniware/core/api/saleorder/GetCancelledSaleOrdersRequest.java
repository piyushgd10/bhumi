/*
 * Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 24/10/15 3:58 AM
 * @author unicom
 */

package com.uniware.core.api.saleorder;

import com.unifier.core.api.base.ServiceRequest;

/**
 * Created by unicom on 24/10/15.
 */
public class GetCancelledSaleOrdersRequest extends ServiceRequest {

    Integer cancelledSinceDays;

    public Integer getCancelledSinceDays() {
        return cancelledSinceDays;
    }

    public void setCancelledSinceDays(Integer cancelledSinceDays) {
        this.cancelledSinceDays = cancelledSinceDays;
    }
}
