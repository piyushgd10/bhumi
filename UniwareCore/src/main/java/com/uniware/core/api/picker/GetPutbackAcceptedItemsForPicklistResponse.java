/*
 * Copyright 2017 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 27/09/17
 * @author piyush
 */
package com.uniware.core.api.picker;

import com.unifier.core.api.base.ServiceResponse;
import com.uniware.core.api.packer.PutbackItemDTO;

import java.util.List;

public class GetPutbackAcceptedItemsForPicklistResponse extends ServiceResponse {

    private String                     picklistCode;
    private String                     statusCode;
    private String                     destination;
    private List<PutbackItemDetailDTO> putbackItems;

    public String getPicklistCode() {
        return picklistCode;
    }

    public void setPicklistCode(String picklistCode) {
        this.picklistCode = picklistCode;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public List<PutbackItemDetailDTO> getPutbackItems() {
        return putbackItems;
    }

    public void setPutbackItems(List<PutbackItemDetailDTO> putbackItems) {
        this.putbackItems = putbackItems;
    }
}
