/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 11/06/14
 *  @author amit
 */

package com.uniware.core.api.inventory;

import com.unifier.core.api.base.ServiceResponse;

import java.util.ArrayList;
import java.util.List;

public class GenerateItemLabelsResponse extends ServiceResponse {

    private static final long  serialVersionUID = -3746752832959471871L;

    private List<ItemLabelDTO> itemLabelDTOList = new ArrayList<>();

    public GenerateItemLabelsResponse() {
        super();
    }

    public List<ItemLabelDTO> getItemLabelDTOList() {
        return itemLabelDTOList;
    }

    public void setItemLabelDTOList(List<ItemLabelDTO> itemLabelDTOList) {
        this.itemLabelDTOList = itemLabelDTOList;
    }

    @Override
    public String toString() {
        return "GenerateItemLabelsResponse{" + "itemLabelDTOList=" + itemLabelDTOList + '}';
    }
}