/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 06-Jan-2015
 *  @author akshaykochhar
 */
package com.uniware.core.api.inventory;

import com.unifier.core.api.base.ServiceResponse;

public class UpdateInventoryOnAllChannelsResponse extends ServiceResponse{

    /**
     * 
     */
    private static final long serialVersionUID = -1206416209051811320L;

}
