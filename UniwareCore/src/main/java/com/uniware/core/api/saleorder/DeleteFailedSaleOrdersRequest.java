/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 26-Nov-2013
 *  @author Deepshikha
 */
package com.uniware.core.api.saleorder;

import com.unifier.core.api.base.ServiceRequest;

public class DeleteFailedSaleOrdersRequest extends ServiceRequest{

    /**
     * 
     */
    private static final long serialVersionUID = 4395479983994236519L;
    

}
