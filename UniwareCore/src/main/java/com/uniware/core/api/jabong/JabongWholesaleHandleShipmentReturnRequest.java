/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jul 9, 2012
 *  @author singla
 */
package com.uniware.core.api.jabong;

import com.unifier.core.api.base.ServiceRequest;

import org.hibernate.validator.constraints.NotBlank;

public class JabongWholesaleHandleShipmentReturnRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -9218416931301051426L;

    @NotBlank
    private String            saleOrderItemCode;

    /**
     * @return the saleOrderItemCode
     */
    public String getSaleOrderItemCode() {
        return saleOrderItemCode;
    }

    /**
     * @param saleOrderItemCode the saleOrderItemCode to set
     */
    public void setSaleOrderItemCode(String saleOrderItemCode) {
        this.saleOrderItemCode = saleOrderItemCode;
    }

}
