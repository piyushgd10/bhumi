/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Oct 22, 2012
 *  @author Pankaj
 */
package com.uniware.core.api.facility;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author Pankaj
 */
public class DeleteFacilityAllocationRuleResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 6697651353632817219L;

}
