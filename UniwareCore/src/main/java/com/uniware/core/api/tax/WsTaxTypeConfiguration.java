/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 10-Dec-2013
 *  @author karunsingla
 */
package com.uniware.core.api.tax;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

public class WsTaxTypeConfiguration {

    private String stateCode;

    private String taxTypeCode;

    @Valid
    private List<WSTaxTypeConfigurationRange> ranges;

    public WsTaxTypeConfiguration() {
    }

    public WsTaxTypeConfiguration(String stateCode, String taxTypeCode) {
        this.stateCode = stateCode;
        this.taxTypeCode = taxTypeCode;
    }

    public String getStateCode() {
        return stateCode;
    }

    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }

    public String getTaxTypeCode() {
        return taxTypeCode;
    }

    public void setTaxTypeCode(String taxTypeCode) {
        this.taxTypeCode = taxTypeCode;
    }

    public List<WSTaxTypeConfigurationRange> getRanges() {
        return ranges;
    }

    public void setRanges(List<WSTaxTypeConfigurationRange> ranges) {
        this.ranges = ranges;
    }

    public void addTaxTypeConfigurationRange(WSTaxTypeConfigurationRange wsTaxTypeConfigurationRange) {
        if (ranges == null) {
            ranges = new ArrayList<>();
        }
        ranges.add(wsTaxTypeConfigurationRange);
    }

    public static class WSTaxTypeConfigurationRange {

        private BigDecimal vat;

        private BigDecimal cst;

        private BigDecimal cstWithCForm;

        private BigDecimal centralGst;

        private BigDecimal stateGst;

        private BigDecimal unionTerritoryGst;

        private BigDecimal integratedGst;

        private BigDecimal compensationCess;

        private BigDecimal additionalTax;

        private BigDecimal startPrice;

        private BigDecimal endPrice;

        public WSTaxTypeConfigurationRange() {
        }

        public WSTaxTypeConfigurationRange(BigDecimal vat, BigDecimal cst, BigDecimal cstWithCForm, BigDecimal additionalTax, BigDecimal startPrice) {
            this.vat = vat;
            this.cst = cst;
            this.cstWithCForm = cstWithCForm;
            this.additionalTax = additionalTax;
            this.startPrice = startPrice;
        }

        public WSTaxTypeConfigurationRange(BigDecimal vat, BigDecimal cst, BigDecimal cstWithCForm,
                BigDecimal centralGst, BigDecimal stateGst, BigDecimal unionTerritoryGst, BigDecimal integratedGst,
                BigDecimal compensationCess, BigDecimal additionalTax, BigDecimal startPrice, BigDecimal endPrice)
        {
            this.vat = vat;
            this.cst = cst;
            this.cstWithCForm = cstWithCForm;
            this.centralGst = centralGst;
            this.stateGst = stateGst;
            this.unionTerritoryGst = unionTerritoryGst;
            this.integratedGst = integratedGst;
            this.compensationCess = compensationCess;
            this.additionalTax = additionalTax;
            this.startPrice = startPrice;
            this.endPrice = endPrice;
        }

        public BigDecimal getVat() {
            return vat;
        }

        public void setVat(BigDecimal vat) {
            this.vat = vat;
        }

        public BigDecimal getCst() {
            return cst;
        }

        public void setCst(BigDecimal cst) {
            this.cst = cst;
        }

        public BigDecimal getCstWithCForm() {
            return cstWithCForm;
        }

        public void setCstWithCForm(BigDecimal cstWithCForm) {
            this.cstWithCForm = cstWithCForm;
        }

        public BigDecimal getAdditionalTax() {
            return additionalTax;
        }

        public void setAdditionalTax(BigDecimal additionalTax) {
            this.additionalTax = additionalTax;
        }

        public BigDecimal getStartPrice() {
            return startPrice;
        }

        public void setStartPrice(BigDecimal startPrice) {
            this.startPrice = startPrice;
        }

        public BigDecimal getEndPrice() {
            return endPrice;
        }

        public void setEndPrice(BigDecimal endPrice) {
            this.endPrice = endPrice;
        }

        public BigDecimal getCentralGst() {
            return centralGst;
        }

        public void setCentralGst(BigDecimal centralGst) {
            this.centralGst = centralGst;
        }

        public BigDecimal getStateGst() {
            return stateGst;
        }

        public void setStateGst(BigDecimal stateGst) {
            this.stateGst = stateGst;
        }

        public BigDecimal getUnionTerritoryGst() {
            return unionTerritoryGst;
        }

        public void setUnionTerritoryGst(BigDecimal unionTerritoryGst) {
            this.unionTerritoryGst = unionTerritoryGst;
        }

        public BigDecimal getIntegratedGst() {
            return integratedGst;
        }

        public void setIntegratedGst(BigDecimal integratedGst) {
            this.integratedGst = integratedGst;
        }

        public BigDecimal getCompensationCess() {
            return compensationCess;
        }

        public void setCompensationCess(BigDecimal compensationCess) {
            this.compensationCess = compensationCess;
        }
    }
}
