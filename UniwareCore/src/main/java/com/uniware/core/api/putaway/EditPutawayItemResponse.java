/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 7, 2012
 *  @author praveeng
 */
package com.uniware.core.api.putaway;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author praveeng
 */
public class EditPutawayItemResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -4380674203852355446L;

}
