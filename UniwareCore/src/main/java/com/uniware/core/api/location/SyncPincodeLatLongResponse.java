/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 11-May-2015
 *  @author parijat
 */
package com.uniware.core.api.location;

import java.util.Map;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author parijat
 */
public class SyncPincodeLatLongResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 457876555678987678L;

    private int               totalCount;
    private int               successCount;
    private int               failCount;
    private Map<String, String> errorMessages;

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public int getSuccessCount() {
        return successCount;
    }

    public void setSuccessCount(int successCount) {
        this.successCount = successCount;
    }

    public int getFailCount() {
        return failCount;
    }

    public void setFailCount(int failCount) {
        this.failCount = failCount;
    }

    public Map<String, String> getErrorMessages() {
        return errorMessages;
    }

    public void setErrorMessages(Map<String, String> errorMessages) {
        this.errorMessages = errorMessages;
    }

}
