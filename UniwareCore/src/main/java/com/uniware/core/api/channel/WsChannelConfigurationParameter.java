/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jan 9, 2015
 *  @author harshpal
 */
package com.uniware.core.api.channel;

import org.hibernate.validator.constraints.NotBlank;

public class WsChannelConfigurationParameter {

    @NotBlank
    private String name;

    private String value;

    public WsChannelConfigurationParameter() {

    }

    public WsChannelConfigurationParameter(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
