/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 10-Jun-2014
 *  @author unicom
 */
package com.uniware.core.api.inventory;

import com.unifier.core.api.base.ServiceRequest;

public class SearchItemTypeInventoryRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private String            skuCode;

    public String getSkuCode() {
        return skuCode;
    }

    public void setSkuCode(String skuCode) {
        this.skuCode = skuCode;
    }

}
