/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, May 27, 2012
 *  @author singla
 */
package com.uniware.core.api.catalog;

import com.unifier.core.api.base.ServiceRequest;

import java.util.List;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author singla
 */
public class BulkCreateOrEditItemTypeRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -1982047932244663276L;

    @NotEmpty
    private List<WsItemType>  itemTypes;

    /**
     * @return the itemTypes
     */
    public List<WsItemType> getItemTypes() {
        return itemTypes;
    }

    /**
     * @param itemTypes the itemTypes to set
     */
    public void setItemTypes(List<WsItemType> itemTypes) {
        this.itemTypes = itemTypes;
    }
}
