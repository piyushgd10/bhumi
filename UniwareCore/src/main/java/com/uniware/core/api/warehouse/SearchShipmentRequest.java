/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 14, 2012
 *  @author praveeng
 */
package com.uniware.core.api.warehouse;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author praveeng
 */
public class SearchShipmentRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 8546231706729843243L;
    private String            shipmentCode;

    /**
     * @return the shipmentCode
     */
    public String getShipmentCode() {
        return shipmentCode;
    }

    /**
     * @param shipmentCode the shipmentCode to set
     */
    public void setShipmentCode(String shipmentCode) {
        this.shipmentCode = shipmentCode;
    }

}
