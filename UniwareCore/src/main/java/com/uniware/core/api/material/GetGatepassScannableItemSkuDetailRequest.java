/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 08-Apr-2015
 *  @author akshaykochhar
 */
package com.uniware.core.api.material;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

public class GetGatepassScannableItemSkuDetailRequest extends ServiceRequest{

    /**
     * 
     */
    private static final long serialVersionUID = -3901641393336775414L;
    
    @NotBlank
    private String itemSKU;
    
    @NotBlank
    private String gatePassCode;

    public String getItemSKU() {
        return itemSKU;
    }

    public void setItemSKU(String itemSKU) {
        this.itemSKU = itemSKU;
    }

    public String getGatePassCode() {
        return gatePassCode;
    }

    public void setGatePassCode(String gatePassCode) {
        this.gatePassCode = gatePassCode;
    }
    
}
