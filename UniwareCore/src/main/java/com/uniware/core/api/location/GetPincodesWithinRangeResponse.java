/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 12-May-2015
 *  @author parijat
 */
package com.uniware.core.api.location;

import java.util.List;

import com.unifier.core.api.base.ServiceResponse;
import com.uniware.core.entity.Location;

/**
 * @author parijat
 *
 */
public class GetPincodesWithinRangeResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -2315581654045386913L;

    private List<Location>    locations;

    public List<Location> getLocations() {
        return locations;
    }

    public void setLocations(List<Location> locations) {
        this.locations = locations;
    }

}
