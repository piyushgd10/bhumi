package com.uniware.core.api.saleorder;

import com.unifier.core.api.base.ServiceResponse;
import com.uniware.core.entity.SaleOrderItem;

import java.util.Date;
import java.util.List;

/**
 * Created by akshayag on 9/16/15.
 */
public class GetUnverifiedSaleOrdersResponse extends ServiceResponse {

    private List<SaleOrderDTO> saleOrders;

    public List<SaleOrderDTO> getSaleOrders() {
        return saleOrders;
    }

    public void setSaleOrders(List<SaleOrderDTO> saleOrders) {
        this.saleOrders = saleOrders;
    }

    public static class SaleOrderDTO {
        private String                 saleOrderCode;
        private String                 displaySaleOrderCode;
        private Date                   fulfillmentTat;
        private Date                   displayOrderDateTime;
        private List<SaleOrderItemDTO> saleOrderItems;

        public SaleOrderDTO() {
        }

        public SaleOrderDTO(String saleOrderCode, String displaySaleOrderCode, List<SaleOrderItemDTO> saleOrderItems, Date fulfillmentTat, Date displayOrderDateTime) {
            this.saleOrderCode = saleOrderCode;
            this.displaySaleOrderCode = displaySaleOrderCode;
            this.saleOrderItems = saleOrderItems;
            this.fulfillmentTat = fulfillmentTat;
            this.displayOrderDateTime = displayOrderDateTime;
        }

        public String getSaleOrderCode() {
            return saleOrderCode;
        }

        public void setSaleOrderCode(String saleOrderCode) {
            this.saleOrderCode = saleOrderCode;
        }

        public String getDisplaySaleOrderCode() {
            return displaySaleOrderCode;
        }

        public void setDisplaySaleOrderCode(String displaySaleOrderCode) {
            this.displaySaleOrderCode = displaySaleOrderCode;
        }

        public Date getFulfillmentTat() {
            return fulfillmentTat;
        }

        public void setFulfillmentTat(Date fulfillmentTat) {
            this.fulfillmentTat = fulfillmentTat;
        }

        public Date getDisplayOrderDateTime() {
            return displayOrderDateTime;
        }

        public void setDisplayOrderDateTime(Date displayOrderDateTime) {
            this.displayOrderDateTime = displayOrderDateTime;
        }

        public List<SaleOrderItemDTO> getSaleOrderItems() {
            return saleOrderItems;
        }

        public void setSaleOrderItems(List<SaleOrderItemDTO> saleOrderItems) {
            this.saleOrderItems = saleOrderItems;
        }
    }

    public static class SaleOrderItemDTO {
        private String saleOrderItemCode;
        private String skuCode;
        private String productDescription;
        private String imageUrl;
        private String channelProductId;

        public SaleOrderItemDTO() {
        }

        public SaleOrderItemDTO(SaleOrderItem saleOrderItem, String imageUrl) {
            this.saleOrderItemCode = saleOrderItem.getCode();
            this.skuCode = saleOrderItem.getSellerSkuCode();
            this.productDescription = saleOrderItem.getChannelProductName();
            this.imageUrl = imageUrl;
            this.channelProductId = saleOrderItem.getChannelProductId();
        }

        public String getSaleOrderItemCode() {
            return saleOrderItemCode;
        }

        public void setSaleOrderItemCode(String saleOrderItemCode) {
            this.saleOrderItemCode = saleOrderItemCode;
        }

        public String getSkuCode() {
            return skuCode;
        }

        public void setSkuCode(String skuCode) {
            this.skuCode = skuCode;
        }

        public String getProductDescription() {
            return productDescription;
        }

        public void setProductDescription(String productDescription) {
            this.productDescription = productDescription;
        }

        public String getImageUrl() {
            return imageUrl;
        }

        public void setImageUrl(String imageUrl) {
            this.imageUrl = imageUrl;
        }

        public String getChannelProductId() {
            return channelProductId;
        }

        public void setChannelProductId(String channelProductId) {
            this.channelProductId = channelProductId;
        }
    }

}
