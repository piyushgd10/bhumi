/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 26-Apr-2013
 *  @author pankaj
 */
package com.uniware.core.api.shipping;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author Sunny
 */
public class DispatchShippingPackageRequest extends ServiceRequest {

    private static final long serialVersionUID = 5483329287181658392L;

    @NotBlank
    private String            shippingPackageCode;

    public String getShippingPackageCode() {
        return shippingPackageCode;
    }

    public void setShippingPackageCode(String shippingPackageCode) {
        this.shippingPackageCode = shippingPackageCode;
    }

}
