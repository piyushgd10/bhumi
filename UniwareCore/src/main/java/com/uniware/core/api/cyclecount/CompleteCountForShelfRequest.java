package com.uniware.core.api.cyclecount;

import java.util.Set;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

/**
 * Created by harshpal on 3/7/16.
 */
public class CompleteCountForShelfRequest extends ServiceRequest {

    @NotBlank
    private String      shelfCode;

    @NotBlank
    private String      subCycleCountCode;

    @NotNull
    private Set<String> itemCodes;

    private int         nonBarcodedItemCount;

    public String getShelfCode() {
        return shelfCode;
    }

    public void setShelfCode(String shelfCode) {
        this.shelfCode = shelfCode;
    }

    public String getSubCycleCountCode() {
        return subCycleCountCode;
    }

    public void setSubCycleCountCode(String subCycleCountCode) {
        this.subCycleCountCode = subCycleCountCode;
    }

    public Set<String> getItemCodes() {
        return itemCodes;
    }

    public void setItemCodes(Set<String> itemCodes) {
        this.itemCodes = itemCodes;
    }

    public int getNonBarcodedItemCount() {
        return nonBarcodedItemCount;
    }

    public void setNonBarcodedItemCount(int nonBarcodedItemCount) {
        this.nonBarcodedItemCount = nonBarcodedItemCount;
    }
}
