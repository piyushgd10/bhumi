/*
*  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
*  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
*  
*  @version     1.0, 05/09/14
*  @author sunny
*/

package com.uniware.core.api.invoice;

import java.math.BigDecimal;

public class TaxDetailDTO {

    private String     itemSku;
    private String     channelProductId;
    private String     taxTypeCode;
    private BigDecimal vat                     = BigDecimal.ZERO;
    private BigDecimal cst                     = BigDecimal.ZERO;
    private BigDecimal additionalTax           = BigDecimal.ZERO;
    private BigDecimal taxPercentage           = BigDecimal.ZERO;
    private BigDecimal additionalTaxPercentage = BigDecimal.ZERO;
    private BigDecimal centralGst              = BigDecimal.ZERO;
    private BigDecimal stateGst                = BigDecimal.ZERO;
    private BigDecimal unionTerritoryGst       = BigDecimal.ZERO;
    private BigDecimal integratedGst           = BigDecimal.ZERO;
    private BigDecimal compensationCess        = BigDecimal.ZERO;

    public String getItemSku() {
        return itemSku;
    }

    public void setItemSku(String itemSku) {
        this.itemSku = itemSku;
    }

    public String getChannelProductId() {
        return channelProductId;
    }

    public void setChannelProductId(String channelProductId) {
        this.channelProductId = channelProductId;
    }

    public String getTaxTypeCode() {
        return taxTypeCode;
    }

    public void setTaxTypeCode(String taxTypeCode) {
        this.taxTypeCode = taxTypeCode;
    }

    public BigDecimal getVat() {
        return vat;
    }

    public void setVat(BigDecimal vat) {
        this.vat = vat;
    }

    public BigDecimal getCst() {
        return cst;
    }

    public void setCst(BigDecimal cst) {
        this.cst = cst;
    }

    public BigDecimal getAdditionalTax() {
        return additionalTax;
    }

    public void setAdditionalTax(BigDecimal additionalTax) {
        this.additionalTax = additionalTax;
    }

    public BigDecimal getTaxPercentage() {
        return taxPercentage;
    }

    public void setTaxPercentage(BigDecimal taxPercentage) {
        this.taxPercentage = taxPercentage;
    }

    public BigDecimal getAdditionalTaxPercentage() {
        return additionalTaxPercentage;
    }

    public void setAdditionalTaxPercentage(BigDecimal additionalTaxPercentage) {
        this.additionalTaxPercentage = additionalTaxPercentage;
    }

    public BigDecimal getCentralGst() {
        return centralGst;
    }

    public void setCentralGst(BigDecimal centralGst) {
        this.centralGst = centralGst;
    }

    public BigDecimal getStateGst() {
        return stateGst;
    }

    public void setStateGst(BigDecimal stateGst) {
        this.stateGst = stateGst;
    }

    public BigDecimal getUnionTerritoryGst() {
        return unionTerritoryGst;
    }

    public void setUnionTerritoryGst(BigDecimal unionTerritoryGst) {
        this.unionTerritoryGst = unionTerritoryGst;
    }

    public BigDecimal getIntegratedGst() {
        return integratedGst;
    }

    public void setIntegratedGst(BigDecimal integratedGst) {
        this.integratedGst = integratedGst;
    }

    public BigDecimal getCompensationCess() {
        return compensationCess;
    }

    public void setCompensationCess(BigDecimal compensationCess) {
        this.compensationCess = compensationCess;
    }

    @Override public String toString() {
        return "TaxDetailDTO{" +
                "itemSku='" + itemSku + '\'' +
                ", channelProductId='" + channelProductId + '\'' +
                ", taxTypeCode='" + taxTypeCode + '\'' +
                ", vat=" + vat +
                ", cst=" + cst +
                ", additionalTax=" + additionalTax +
                ", taxPercentage=" + taxPercentage +
                ", additionalTaxPercentage=" + additionalTaxPercentage +
                ", centralGst=" + centralGst +
                ", stateGst=" + stateGst +
                ", unionTerritoryGst=" + unionTerritoryGst +
                ", integratedGst=" + integratedGst +
                ", compensationCess=" + compensationCess +
                '}';
    }
}
