package com.uniware.core.api.cyclecount;

import com.unifier.core.api.base.ServiceRequest;

/**
 * Created by harshpal on 3/21/16.
 */
public class GetShelfDetailsRequest extends ServiceRequest {

    private String shelfCode;

    public String getShelfCode() {
        return shelfCode;
    }

    public void setShelfCode(String shelfCode) {
        this.shelfCode = shelfCode;
    }
}
