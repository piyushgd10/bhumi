/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 10-Oct-2013
 *  @author parijat
 */
package com.uniware.core.api.purchase;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

public class AddUpdateReorderConfigRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 3827298139729789L;

    @NotBlank
    private String            itemSkuCode;

    @NotNull
    @Min(value = 0)
    private Integer           reorderQuantity;

    @NotNull
    @Min(value = 0)
    private Integer           reorderThreshold;

    /**
     * @return the itemSkuCode
     */
    public String getItemSkuCode() {
        return itemSkuCode;
    }

    /**
     * @param itemSkuCode the itemSkuCode to set
     */
    public void setItemSkuCode(String itemSkuCode) {
        this.itemSkuCode = itemSkuCode;
    }

    /**
     * @return the reorderQuantity
     */
    public Integer getReorderQuantity() {
        return reorderQuantity;
    }

    /**
     * @param reorderQuantity the reorderQuantity to set
     */
    public void setReorderQuantity(Integer reorderQuantity) {
        this.reorderQuantity = reorderQuantity;
    }

    /**
     * @return the reorderThreshold
     */
    public Integer getReorderThreshold() {
        return reorderThreshold;
    }

    /**
     * @param reorderThreshold the reorderThreshold to set
     */
    public void setReorderThreshold(Integer reorderThreshold) {
        this.reorderThreshold = reorderThreshold;
    }

}
