/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 20, 2013
 *  @author karunsingla
 */
package com.uniware.core.api.shipping;

import com.unifier.core.api.base.ServiceResponse;

public class UpdateTrackingStatusResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 2233335618436437190L;

    private int               totalPolled;
    private int               failures;
    private int               totalChanged;

    public int getTotalPolled() {
        return totalPolled;
    }

    public void setTotalPolled(int totalPolled) {
        this.totalPolled = totalPolled;
    }

    public int getFailures() {
        return failures;
    }

    public void setFailures(int failures) {
        this.failures = failures;
    }

    public int getTotalChanged() {
        return totalChanged;
    }

    public void setTotalChanged(int totalChanged) {
        this.totalChanged = totalChanged;
    }

}
