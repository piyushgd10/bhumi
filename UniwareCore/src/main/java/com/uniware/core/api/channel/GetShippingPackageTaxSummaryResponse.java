/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jun 20, 2012
 *  @author singla
 */
package com.uniware.core.api.channel;

import java.math.BigDecimal;
import java.util.List;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author Sunny
 */
public class GetShippingPackageTaxSummaryResponse extends ServiceResponse {

    /**
     *
     */
    private static final long           serialVersionUID = -8331014310944611193L;

    private BigDecimal                  globalTaxPercentage;

    private List<ShippingPackageTaxDTO> shippingPackageTaxes;

    public BigDecimal getGlobalTaxPercentage() {
        return globalTaxPercentage;
    }

    public void setGlobalTaxPercentage(BigDecimal globalTaxPercentage) {
        this.globalTaxPercentage = globalTaxPercentage;
    }

    public List<ShippingPackageTaxDTO> getShippingPackageTaxes() {
        return shippingPackageTaxes;
    }

    public void setShippingPackageTaxes(List<ShippingPackageTaxDTO> shippingPackageTaxes) {
        this.shippingPackageTaxes = shippingPackageTaxes;
    }

    public static class ShippingPackageTaxDTO {
        private String                        code;
        private String                        channelCode;
        private List<ShippingPackageLineItem> lineItems;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getChannelCode() {
            return channelCode;
        }

        public void setChannelCode(String channelCode) {
            this.channelCode = channelCode;
        }

        public List<ShippingPackageLineItem> getLineItems() {
            return lineItems;
        }

        public void setLineItems(List<ShippingPackageLineItem> lineItems) {
            this.lineItems = lineItems;
        }

        public static class ShippingPackageLineItem {
            private String     lineItemIdentifier;
            private String     sellerSkuCode;
            private String     itemName;
            private int        quantity;
            private String     hsnCode;
            private BigDecimal vatPercentage;
            private BigDecimal cstPercentage;
            private BigDecimal centralGstPercentage;
            private BigDecimal stateGstPercentage;
            private BigDecimal unionTerritoryGstPercentage;
            private BigDecimal integratedGstPercentage;
            private BigDecimal compensationCessPercentage;
            private String     taxType;
            private BigDecimal taxPercentage;

            public String getHsnCode() {
                return hsnCode;
            }

            public void setHsnCode(String hsnCode) {
                this.hsnCode = hsnCode;
            }

            public String getLineItemIdentifier() {
                return lineItemIdentifier;
            }

            public void setLineItemIdentifier(String lineItemIdentifier) {
                this.lineItemIdentifier = lineItemIdentifier;
            }

            public String getSellerSkuCode() {
                return sellerSkuCode;
            }

            public void setSellerSkuCode(String sellerSkuCode) {
                this.sellerSkuCode = sellerSkuCode;
            }

            public String getItemName() {
                return itemName;
            }

            public void setItemName(String itemName) {
                this.itemName = itemName;
            }

            public int getQuantity() {
                return quantity;
            }

            public void setQuantity(int quantity) {
                this.quantity = quantity;
            }

            public BigDecimal getCentralGstPercentage() {
                return centralGstPercentage;
            }

            public void setCentralGstPercentage(BigDecimal centralGstPercentage) {
                this.centralGstPercentage = centralGstPercentage;
            }

            public BigDecimal getStateGstPercentage() {
                return stateGstPercentage;
            }

            public void setStateGstPercentage(BigDecimal stateGstPercentage) {
                this.stateGstPercentage = stateGstPercentage;
            }

            public BigDecimal getUnionTerritoryGstPercentage() {
                return unionTerritoryGstPercentage;
            }

            public void setUnionTerritoryGstPercentage(BigDecimal unionTerritoryGstPercentage) {
                this.unionTerritoryGstPercentage = unionTerritoryGstPercentage;
            }

            public BigDecimal getIntegratedGstPercentage() {
                return integratedGstPercentage;
            }

            public void setIntegratedGstPercentage(BigDecimal integratedGstPercentage) {
                this.integratedGstPercentage = integratedGstPercentage;
            }

            public BigDecimal getCompensationCessPercentage() {
                return compensationCessPercentage;
            }

            public void setCompensationCessPercentage(BigDecimal compensationCessPercentage) {
                this.compensationCessPercentage = compensationCessPercentage;
            }

            public BigDecimal getVatPercentage() {
                return vatPercentage;
            }

            public void setVatPercentage(BigDecimal vatPercentage) {
                this.vatPercentage = vatPercentage;
            }

            public BigDecimal getCstPercentage() {
                return cstPercentage;
            }

            public void setCstPercentage(BigDecimal cstPercentage) {
                this.cstPercentage = cstPercentage;
            }

            //            public String getTaxType() {
            //                return taxType;
            //            }
            //
            //            public void setTaxType(String taxType) {
            //                this.taxType = taxType;
            //            }
            //
            //            public BigDecimal getTaxPercentage() {
            //                return taxPercentage;
            //            }
            //
            //            public void setTaxPercentage(BigDecimal taxPercentage) {
            //                this.taxPercentage = taxPercentage;
            //            }
        }
    }

}
