/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Oct 14, 2015
 *  @author akshay
 */
package com.uniware.core.api.channel;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

/**
 * Request that updates whether or not price changes should be pushed from uniware
 * to the channel. 
 */
public class ChangeChannelPricingSyncStatusRequest extends ServiceRequest {

    private static final long serialVersionUID = 46688415051112393L;

    @NotBlank
    private String channelCode;

    @NotBlank
    private String statusCode;

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

}
