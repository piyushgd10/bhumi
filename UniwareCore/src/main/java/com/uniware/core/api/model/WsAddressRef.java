/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 22, 2011
 *  @author singla
 */
package com.uniware.core.api.model;

import org.hibernate.validator.constraints.NotBlank;

/**
 * @author singla
 */
public class WsAddressRef {

    @NotBlank
    private String referenceId;

    /**
     * 
     */
    public WsAddressRef() {
    }

    public WsAddressRef(String referenceId) {
        this.referenceId = referenceId;
    }

    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }

    public String getReferenceId() {
        return referenceId;
    }
}
