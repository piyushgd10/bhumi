/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, March 18, 2013
 *  @author praveeng
 */
package com.uniware.core.api.material;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.unifier.core.api.base.ServiceRequest;

public class CreateInboundGatePassRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -2281658182254903242L;

    @Valid
    @NotNull
    private WsInboundGatePass wsInboundGatePass;

    private Integer           userId;

    /**
     * @return the wsInboundGatePass
     */
    public WsInboundGatePass getWsInboundGatePass() {
        return wsInboundGatePass;
    }

    /**
     * @param wsInboundGatePass the wsInboundGatePass to set
     */
    public void setWsInboundGatePass(WsInboundGatePass wsInboundGatePass) {
        this.wsInboundGatePass = wsInboundGatePass;
    }

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

}
