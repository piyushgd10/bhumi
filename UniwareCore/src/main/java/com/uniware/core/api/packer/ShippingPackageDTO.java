/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jan 13, 2012
 *  @author singla
 */
package com.uniware.core.api.packer;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @author singla
 */
public class ShippingPackageDTO {
    private String                       code;
    private String                       saleOrderCode;
    private String                       shippingProviderCode;
    private String                       trackingNumber;
    private String                       invoiceCode;
    private String                       invoiceDisplayCode;
    private String                       returnInvoiceCode;
    private String                       returnInvoiceDisplayCode;
    private String                       shippingLabelLink;
    private boolean                      requiresCustomization;
    private String                       statusCode;
    private String                       channel;
    private BigDecimal                   totalAmount;
    private String                       packageType;

    private List<ShippingPackageItemDTO> shippingPackageItems = new ArrayList<ShippingPackageItemDTO>();

    public String getInvoiceDisplayCode() {
        return invoiceDisplayCode;
    }

    public void setInvoiceDisplayCode(String invoiceDisplayCode) {
        this.invoiceDisplayCode = invoiceDisplayCode;
    }

    public String getReturnInvoiceDisplayCode() {
        return returnInvoiceDisplayCode;
    }

    public void setReturnInvoiceDisplayCode(String returnInvoiceDisplayCode) {
        this.returnInvoiceDisplayCode = returnInvoiceDisplayCode;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the shippingPackageItems
     */
    public List<ShippingPackageItemDTO> getShippingPackageItems() {
        return shippingPackageItems;
    }

    /**
     * @param shippingPackageItems the shippingPackageItems to set
     */
    public void setShippingPackageItems(List<ShippingPackageItemDTO> shippingPackageItems) {
        this.shippingPackageItems = shippingPackageItems;
    }

    /**
     * @return the shippingProviderCode
     */
    public String getShippingProviderCode() {
        return shippingProviderCode;
    }

    /**
     * @param shippingProviderCode the shippingProviderCode to set
     */
    public void setShippingProviderCode(String shippingProviderCode) {
        this.shippingProviderCode = shippingProviderCode;
    }

    /**
     * @return the invoiceCode
     */
    public String getInvoiceCode() {
        return invoiceCode;
    }

    /**
     * @param invoiceCode the invoiceCode to set
     */
    public void setInvoiceCode(String invoiceCode) {
        this.invoiceCode = invoiceCode;
    }

    /**
     * @return the trackingNumber
     */
    public String getTrackingNumber() {
        return trackingNumber;
    }

    /**
     * @param trackingNumber the trackingNumber to set
     */
    public void setTrackingNumber(String trackingNumber) {
        this.trackingNumber = trackingNumber;
    }

    /**
     * @return the saleOrderCode
     */
    public String getSaleOrderCode() {
        return saleOrderCode;
    }

    /**
     * @param saleOrderCode the saleOrderCode to set
     */
    public void setSaleOrderCode(String saleOrderCode) {
        this.saleOrderCode = saleOrderCode;
    }

    /**
     * @return the shippingLabelLink
     */
    public String getShippingLabelLink() {
        return shippingLabelLink;
    }

    /**
     * @param shippingLabelLink the shippingLabelLink to set
     */
    public void setShippingLabelLink(String shippingLabelLink) {
        this.shippingLabelLink = shippingLabelLink;
    }

    /**
     * @return the requiresCustomization
     */
    public boolean isRequiresCustomization() {
        return requiresCustomization;
    }

    /**
     * @param requiresCustomization the requiresCustomization to set
     */
    public void setRequiresCustomization(boolean requiresCustomization) {
        this.requiresCustomization = requiresCustomization;
    }

    /**
     * @return the statusCode
     */
    public String getStatusCode() {
        return statusCode;
    }

    /**
     * @param statusCode the statusCode to set
     */
    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getPackageType() {
        return packageType;
    }

    public void setPackageType(String packageType) {
        this.packageType = packageType;
    }

    public String getReturnInvoiceCode() {
        return returnInvoiceCode;
    }

    public void setReturnInvoiceCode(String returnInvoiceCode) {
        this.returnInvoiceCode = returnInvoiceCode;
    }
}
