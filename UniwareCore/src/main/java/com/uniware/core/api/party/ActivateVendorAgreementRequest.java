/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 3, 2012
 *  @author praveeng
 */
package com.uniware.core.api.party;

import com.unifier.core.api.base.ServiceRequest;

import javax.validation.constraints.NotNull;

/**
 * @author praveeng
 */
public class ActivateVendorAgreementRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -8716314941082472102L;
    @NotNull
    private Integer           vendorAgreementId;
    /**
     * @return the vendorAgreementId
     */
    public Integer getVendorAgreementId() {
        return vendorAgreementId;
    }
    /**
     * @param vendorAgreementId the vendorAgreementId to set
     */
    public void setVendorAgreementId(Integer vendorAgreementId) {
        this.vendorAgreementId = vendorAgreementId;
    }
}
