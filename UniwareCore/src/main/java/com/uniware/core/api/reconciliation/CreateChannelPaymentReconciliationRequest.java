/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 07-Dec-2013
 *  @author parijat
 */
package com.uniware.core.api.reconciliation;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;
import com.uniware.core.vo.ChannelReconciliationInvoice;

/**
 * @author parijat
 */
public class CreateChannelPaymentReconciliationRequest extends ServiceRequest {

    /**
     *
     */
    private static final long            serialVersionUID = 3410605121840494377L;

    @NotBlank
    private String                       reconciliationIdentifier;

    @NotNull
    private ChannelReconciliationInvoice invoiceVO;

    @NotNull
    private double                       totalSettlementValue;

    public CreateChannelPaymentReconciliationRequest() {

    }

    public CreateChannelPaymentReconciliationRequest(String reconciliationIdentifier, ChannelReconciliationInvoice invoiceVO, double totalSettlementValue) {
        this.reconciliationIdentifier = reconciliationIdentifier;
        this.invoiceVO = invoiceVO;
        this.totalSettlementValue = totalSettlementValue;
    }

    /**
     * @return the reconciliationIdentifier
     */
    public String getReconciliationIdentifier() {
        return reconciliationIdentifier;
    }

    /**
     * @param reconciliationIdentifier the reconciliationIdentifier to set
     */
    public void setReconciliationIdentifier(String reconciliationIdentifier) {
        this.reconciliationIdentifier = reconciliationIdentifier;
    }

    /**
     * @return the invoiceVO
     */
    public ChannelReconciliationInvoice getInvoiceVO() {
        return invoiceVO;
    }

    /**
     * @param invoiceVO the invoiceVO to set
     */
    public void setInvoiceVO(ChannelReconciliationInvoice invoiceVO) {
        this.invoiceVO = invoiceVO;
    }

    /**
     * @return the totalSettlementValue
     */
    public double getTotalSettlementValue() {
        return totalSettlementValue;
    }

    public void setTotalSettlementValue(double totalSettlementValue) {
        this.totalSettlementValue = totalSettlementValue;
    }
    /*public static class TransactionRow {

        @NotBlank
        private String     orderStatus;
        @NotBlank
        private String     status;

        @NotNull
        @Min(value = 0)
        private BigDecimal reconciliationAmount;

        private String     currencyCode;
        private String     currencyConversionRate;

        public TransactionRow() {

        }

        public TransactionRow(String status, String orderStatus, BigDecimal reconciliationAmount) {
            this.orderStatus = orderStatus;
            this.status = status;
            this.reconciliationAmount = reconciliationAmount;
        }

        public TransactionRow(String status, String orderStatus, BigDecimal reconciliationAmount, String currencyCode, String currencyConversionRate) {
            this.orderStatus = orderStatus;
            this.status = status;
            this.reconciliationAmount = reconciliationAmount;
            this.currencyCode = currencyCode;
            this.currencyConversionRate = currencyConversionRate;
        }

        public String getOrderStatus() {
            return orderStatus;
        }

        public void setOrderStatus(String orderStatus) {
            this.orderStatus = orderStatus;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public BigDecimal getReconciliationAmount() {
            return reconciliationAmount;
        }

        public void setReconciliationAmount(BigDecimal reconciliationAmount) {
            this.reconciliationAmount = reconciliationAmount;
        }

        public String getCurrencyCode() {
            return currencyCode;
        }

        public void setCurrencyCode(String currencyCode) {
            this.currencyCode = currencyCode;
        }

        public String getCurrencyConversionRate() {
            return currencyConversionRate;
        }

        public void setCurrencyConversionRate(String currencyConversionRate) {
            this.currencyConversionRate = currencyConversionRate;
        }

        @Override
        public String toString() {
            return "TransactionRow [orderStatus=" + orderStatus + ", status=" + status + ", reconciliationAmount=" + reconciliationAmount + ", currencyCode=" + currencyCode
                    + ", currencyConversionRate=" + currencyConversionRate + "]";
        }

    }*/

}
