/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 3, 2012
 *  @author singla
 */
package com.uniware.core.api.inflow;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author singla
 */
public class CreateInflowPutawayResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -3456652535297144264L;
    private InflowReceiptDTO  inflowReceipt;

    /**
     * @return the inflowReceipt
     */
    public InflowReceiptDTO getInflowReceipt() {
        return inflowReceipt;
    }

    /**
     * @param inflowReceipt the inflowReceipt to set
     */
    public void setInflowReceipt(InflowReceiptDTO inflowReceipt) {
        this.inflowReceipt = inflowReceipt;
    }

}
