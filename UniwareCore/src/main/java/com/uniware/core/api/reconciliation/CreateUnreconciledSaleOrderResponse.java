/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 10-Jan-2014
 *  @author parijat
 */
package com.uniware.core.api.reconciliation;

import com.unifier.core.api.base.ServiceResponse;

public class CreateUnreconciledSaleOrderResponse extends ServiceResponse {

    private static final long serialVersionUID = -8206066774217490459L;

}
