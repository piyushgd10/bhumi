/**
 * Copyright 2017 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version 1.0, 19/09/17
 * @author aditya
 */
package com.uniware.core.api.ledger;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Min;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

/**
 * Request for fetching ledger details of a particular sku. Contains the following:
 * <ul>
 * <li>{@link #skuCode}: SkuCode of the ledger entries.</li>
 * <li>{@link #from}: The date after which the ledger entries were made. Together with {@link #to}, this determines the
 * date range.</li>
 * <li>{@link #to}: The date before the ledger entries were made. Together with {@link #from}, this determines the date
 * range.</li>
 * <li>{@link #facilityCodes}: List of facility codes in which ledger entries are to be found. If empty, all
 * facilities for the tenant are considered.</li>
 * <li>{@link #start}: Used for offset. Along with {@link #pageSize}, handles the pagination.</li>
 * <li>{@link #pageSize}: Used for offset. Along with {@link #start}, handles the pagination.</li>
 * </ul>
 */
public class FetchInventoryLedgerRequest extends ServiceRequest {
    @NotBlank
    private String       skuCode;

    @Valid
    private Date         from;

    @Valid
    private Date         to;

    private List<String> facilityCodes;

    @Min(value = 0)
    private int          start;

    @Min(value = 1)
    private int          pageSize;

    public Date getFrom() {
        return from;
    }

    public void setFrom(Date from) {
        this.from = from;
    }

    public Date getTo() {
        return to;
    }

    public void setTo(Date to) {
        this.to = to;
    }

    public List<String> getFacilityCodes() {
        return facilityCodes;
    }

    public void setFacilityCodes(List<String> facilityCodes) {
        this.facilityCodes = facilityCodes;
    }

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public String getSkuCode() {
        return skuCode;
    }

    public void setSkuCode(String skuCode) {
        this.skuCode = skuCode;
    }
}
