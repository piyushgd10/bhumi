/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 14, 2011
 *  @author singla
 */
package com.uniware.core.api.picker;

import javax.validation.constraints.NotNull;

import com.unifier.core.api.base.ServiceRequest;
import com.uniware.core.entity.Picklist;

/**
 * @author singla
 */
public abstract class AbstractCreatePicklistRequest extends ServiceRequest {

    private static final long    serialVersionUID = 8372928376769473299L;

    @NotNull
    private Picklist.Destination destination;

    @NotNull
    private Integer              pickSetId;

    @NotNull
    private Integer              userId;

    public Picklist.Destination getDestination() {
        return destination;
    }

    public void setDestination(Picklist.Destination destination) {
        this.destination = destination;
    }

    public Integer getPickSetId() {
        return pickSetId;
    }

    public void setPickSetId(Integer pickSetId) {
        this.pickSetId = pickSetId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }
}
