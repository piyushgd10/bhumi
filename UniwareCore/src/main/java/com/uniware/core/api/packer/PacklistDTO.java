/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jan 12, 2012
 *  @author singla
 */
package com.uniware.core.api.packer;

import com.uniware.core.entity.PicklistItem;
import com.uniware.core.entity.SaleOrderItem;
import com.uniware.core.entity.ShippingPackage;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author singla
 */
public class PacklistDTO {

    private String                       code;
    private String                       statusCode;
    private String                       type;
    private String                       destination;
    private String                       username;
    private Date                         created;
    private Date                         updated;

    private List<ShippingPackageDTO>     packlistItems = new ArrayList<>();
    private List<PacklistRemovedItemDTO> putbackItems  = new ArrayList<>();

    private List<PacklistDamagedItemDTO>  damagedItems  = new ArrayList<>();
    private List<PacklistNotFoundItemDTO> notFoundItems = new ArrayList<>();

    private Map<String, Integer> pickBucketScannedQuantity;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public List<ShippingPackageDTO> getPacklistItems() {
        return packlistItems;
    }

    public void setPacklistItems(List<ShippingPackageDTO> packlistItems) {
        this.packlistItems = packlistItems;
    }

    public List<PacklistRemovedItemDTO> getPutbackItems() {
        return putbackItems;
    }

    public void setPutbackItems(List<PacklistRemovedItemDTO> putbackItems) {
        this.putbackItems = putbackItems;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public List<PacklistDamagedItemDTO> getDamagedItems() {
        return damagedItems;
    }

    public void setDamagedItems(List<PacklistDamagedItemDTO> damagedItems) {
        this.damagedItems = damagedItems;
    }

    public List<PacklistNotFoundItemDTO> getNotFoundItems() {
        return notFoundItems;
    }

    public void setNotFoundItems(List<PacklistNotFoundItemDTO> notFoundItems) {
        this.notFoundItems = notFoundItems;
    }

    public Map<String, Integer> getPickBucketScannedQuantity() {
        return pickBucketScannedQuantity;
    }

    public void setPickBucketScannedQuantity(Map<String, Integer> pickBucketScannedQuantity) {
        this.pickBucketScannedQuantity = pickBucketScannedQuantity;
    }

    public static class PacklistDamagedItemDTO extends PacklistItemDTO {
        private String itemCode;

        public PacklistDamagedItemDTO(ShippingPackage shippingPackage, SaleOrderItem saleOrderItem, PicklistItem picklistItem) {
            super(shippingPackage, saleOrderItem);
            this.itemCode = picklistItem.getItemCode();
        }

        public String getItemCode() {
            return itemCode;
        }

        public void setItemCode(String itemCode) {
            this.itemCode = itemCode;
        }
    }

    public static class PacklistNotFoundItemDTO extends PacklistItemDTO {
        private String itemCode;

        public PacklistNotFoundItemDTO(ShippingPackage shippingPackage, SaleOrderItem saleOrderItem, PicklistItem picklistItem) {
            super(shippingPackage, saleOrderItem);
            this.itemCode = picklistItem.getItemCode();
        }

        public String getItemCode() {
            return itemCode;
        }

        public void setItemCode(String itemCode) {
            this.itemCode = itemCode;
        }
    }

    public static class PacklistRemovedItemDTO extends PacklistItemDTO {
        private String putbackStatus;

        public PacklistRemovedItemDTO(ShippingPackage shippingPackage, SaleOrderItem saleOrderItem, PicklistItem picklistItem) {
            super(shippingPackage, saleOrderItem);
            this.putbackStatus = picklistItem.getStatusCode().name();
        }

        public String getPutbackStatus() {
            return putbackStatus;
        }

        public void setPutbackStatus(String putbackStatus) {
            this.putbackStatus = putbackStatus;
        }
    }

    public static class PacklistItemDTO {
        private String shippingPackageCode;
        private String saleOrderCode;
        private String saleOrderItemCode;
        private String itemSku;
        private String itemName;
        private String reason;

        public PacklistItemDTO(ShippingPackage shippingPackage, SaleOrderItem saleOrderItem) {
            this.shippingPackageCode = shippingPackage.getCode();
            this.saleOrderCode = shippingPackage.getSaleOrder().getCode();
            this.saleOrderItemCode = saleOrderItem.getCode();
            this.itemName = saleOrderItem.getItemType().getName();
            this.itemSku = saleOrderItem.getItemType().getSkuCode();
        }

        public String getShippingPackageCode() {
            return shippingPackageCode;
        }

        public void setShippingPackageCode(String shippingPackageCode) {
            this.shippingPackageCode = shippingPackageCode;
        }

        public String getSaleOrderCode() {
            return saleOrderCode;
        }

        public void setSaleOrderCode(String saleOrderCode) {
            this.saleOrderCode = saleOrderCode;
        }

        public String getSaleOrderItemCode() {
            return saleOrderItemCode;
        }

        public void setSaleOrderItemCode(String saleOrderItemCode) {
            this.saleOrderItemCode = saleOrderItemCode;
        }

        public String getItemSku() {
            return itemSku;
        }

        public void setItemSku(String itemSku) {
            this.itemSku = itemSku;
        }

        public String getItemName() {
            return itemName;
        }

        public void setItemName(String itemName) {
            this.itemName = itemName;
        }

        public String getReason() {
            return reason;
        }

        public void setReason(String reason) {
            this.reason = reason;
        }

    }

}