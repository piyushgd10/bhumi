/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 07-May-2013
 *  @author unicom
 */
package com.uniware.core.api.admin.shipping;

import org.hibernate.validator.constraints.NotBlank;

/**
 * @author Sunny
 */
public class MethodDTO {

    @NotBlank
    private String  providerCode;
    @NotBlank
    private String  methodCode;
    private boolean cod;
    private boolean enabled;
    @NotBlank
    private String  trackingNumberGeneration;
    private String  name;

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the trackingNumberGeneration
     */
    public String getTrackingNumberGeneration() {
        return trackingNumberGeneration;
    }

    /**
     * @param trackingNumberGeneration the trackingNumberGeneration to set
     */
    public void setTrackingNumberGeneration(String trackingNumberGeneration) {
        this.trackingNumberGeneration = trackingNumberGeneration;
    }

    /**
     * @return the providerCode
     */
    public String getProviderCode() {
        return providerCode;
    }

    /**
     * @param providerCode the providerCode to set
     */
    public void setProviderCode(String providerCode) {
        this.providerCode = providerCode;
    }

    /**
     * @return the enabled
     */
    public boolean isEnabled() {
        return enabled;
    }

    /**
     * @param enabled the enabled to set
     */
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    /**
     * @return the methodCode
     */
    public String getMethodCode() {
        return methodCode;
    }

    /**
     * @param methodCode the methodCode to set
     */
    public void setMethodCode(String methodCode) {
        this.methodCode = methodCode;
    }

    /**
     * @return the cod
     */
    public boolean isCod() {
        return cod;
    }

    /**
     * @param cod the cod to set
     */
    public void setCod(boolean cod) {
        this.cod = cod;
    }
}