/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jul 29, 2012
 *  @author singla
 */
package com.uniware.core.api.purchase;

import java.util.Date;
import java.util.List;

import com.unifier.core.api.customfields.WsCustomFieldValue;

/**
 * @author singla
 */
public class WsPurchaseOrderMetadata {

    private String                   type;

    private Date                     expiryDate;

    private Date                     deliveryDate;

    private List<WsCustomFieldValue> customFieldValues;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the expiryDate
     */
    public Date getExpiryDate() {
        return expiryDate;
    }

    /**
     * @param expiryDate the expiryDate to set
     */
    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    /**
     * @return the deliveryDate
     */
    public Date getDeliveryDate() {
        return deliveryDate;
    }

    /**
     * @param deliveryDate the deliveryDate to set
     */
    public void setDeliveryDate(Date deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    /**
     * @return the customFields
     */
    public List<WsCustomFieldValue> getCustomFieldValues() {
        return customFieldValues;
    }

    /**
     * @param customFields the customFields to set
     */
    public void setCustomFieldValues(List<WsCustomFieldValue> customFieldValues) {
        this.customFieldValues = customFieldValues;
    }

}
