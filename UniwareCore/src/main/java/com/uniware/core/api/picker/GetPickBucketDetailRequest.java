/*
 * Copyright 2017 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 8/8/17 1:06 PM
 * @author digvijaysharma
 */

package com.uniware.core.api.picker;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

/**
 * Created by bhuvneshwarkumar on 13/09/16.
 */
public class GetPickBucketDetailRequest extends ServiceRequest {

    @NotBlank
    private String pickBucketCode;

    public String getPickBucketCode() {
        return pickBucketCode;
    }

    public void setPickBucketCode(String pickBucketCode) {
        this.pickBucketCode = pickBucketCode;
    }
}
