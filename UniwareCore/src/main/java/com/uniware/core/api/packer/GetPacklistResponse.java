/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jan 12, 2012
 *  @author singla
 */
package com.uniware.core.api.packer;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author singla
 */
public class GetPacklistResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -5007937678874543614L;
    private PacklistDTO       packlist;

    /**
     * @return the packlist
     */
    public PacklistDTO getPacklist() {
        return packlist;
    }

    /**
     * @param packlist the packlist to set
     */
    public void setPacklist(PacklistDTO packlist) {
        this.packlist = packlist;
    }
}
