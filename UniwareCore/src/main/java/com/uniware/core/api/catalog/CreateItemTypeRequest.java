/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 9, 2012
 *  @author praveeng
 */
package com.uniware.core.api.catalog;

import com.unifier.core.api.base.ServiceRequest;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * @author praveeng
 */
public class CreateItemTypeRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 7256184220055321136L;

    @Valid
    @NotNull
    private WsItemType        itemType;

    /**
     * @return the itemType
     */
    public WsItemType getItemType() {
        return itemType;
    }

    /**
     * @param itemType the itemType to set
     */
    public void setItemType(WsItemType itemType) {
        this.itemType = itemType;
    }

}
