/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 9, 2012
 *  @author singla
 */
package com.uniware.core.api.shipping;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author singla
 */
public class GetCurrentChannelShippingManifestResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -2000166784711006769L;

    private String            channelManifestLink;

    public String getChannelManifestLink() {
        return channelManifestLink;
    }

    public void setChannelManifestLink(String channelManifestLink) {
        this.channelManifestLink = channelManifestLink;
    }

}
