/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 29-Sep-2014
 *  @author parijat
 */
package com.uniware.core.api.channel;

import java.util.ArrayList;
import java.util.List;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author parijat
 */
public class SyncChannelReconciliationInvoiceResponse extends ServiceResponse {

    private static final long                     serialVersionUID = 1183851513484647877L;

    private List<ChannelReconciliationInvoiceDTO> resultItems      = new ArrayList<>();

    public static class ChannelReconciliationInvoiceDTO {
        private String channelCode;
        private String message;

        public ChannelReconciliationInvoiceDTO() {
        }

        public ChannelReconciliationInvoiceDTO(String channelCode, String message) {
            this.channelCode = channelCode;
            this.message = message;
        }

        public String getChannelCode() {
            return channelCode;
        }

        public void setChannelCode(String channelCode) {
            this.channelCode = channelCode;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public List<ChannelReconciliationInvoiceDTO> getResultItems() {
        return resultItems;
    }

    public void setResultItems(List<ChannelReconciliationInvoiceDTO> resultItems) {
        this.resultItems = resultItems;
    }
}
