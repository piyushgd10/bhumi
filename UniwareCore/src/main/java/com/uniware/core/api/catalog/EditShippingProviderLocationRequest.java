/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 27, 2012
 *  @author praveeng
 */
package com.uniware.core.api.catalog;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;
import com.unifier.core.api.customfields.WsCustomFieldValue;

/**
 * @author praveeng
 */
public class EditShippingProviderLocationRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long        serialVersionUID = 4598628119697022022L;

    @NotBlank
    private String                   shippingProviderCode;

    @NotBlank
    private String                   shippingMethodName;

    private String                   name;

    @NotBlank
    private String                   pincode;

    private String                   routingCode;

    @NotNull
    private Integer                  priority;

    private boolean                  enabled          = true;

    private String                   filterExpression;

    @Valid
    private List<WsCustomFieldValue> customFieldValues;

    public EditShippingProviderLocationRequest() {
    }

    public EditShippingProviderLocationRequest(AddOrEditShippingProviderLocationRequest request) {
        this.shippingProviderCode = request.getShippingProviderCode();
        this.shippingMethodName = request.getShippingMethodName();
        this.name = request.getName();
        this.pincode = request.getPincode();
        this.routingCode = request.getRoutingCode();
        this.priority = request.getPriority();
        this.enabled = request.isEnabled();
        this.filterExpression = request.getFilterExpression();
        this.customFieldValues = request.getCustomFieldValues();
    }
    /**
     * @return the enabled
     */
    public boolean isEnabled() {
        return enabled;
    }

    /**
     * @param enabled the enabled to set
     */
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getShippingProviderCode() {
        return shippingProviderCode;
    }

    public void setShippingProviderCode(String shippingProviderCode) {
        this.shippingProviderCode = shippingProviderCode;
    }

    /**
     * @return the shippingMethodName
     */
    public String getShippingMethodName() {
        return shippingMethodName;
    }

    /**
     * @param shippingMethodName the shippingMethodName to set
     */
    public void setShippingMethodName(String shippingMethodName) {
        this.shippingMethodName = shippingMethodName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getRoutingCode() {
        return routingCode;
    }

    public void setRoutingCode(String routingCode) {
        this.routingCode = routingCode;
    }

    /**
     * @return the priority
     */
    public int getPriority() {
        return priority;
    }

    /**
     * @param priority the priority to set
     */
    public void setPriority(int priority) {
        this.priority = priority;
    }

    public String getFilterExpression() {
        return filterExpression;
    }

    public void setFilterExpression(String filterExpression) {
        this.filterExpression = filterExpression;
    }

    public List<WsCustomFieldValue> getCustomFieldValues() {
        return customFieldValues;
    }

    public void setCustomFieldValues(List<WsCustomFieldValue> customFieldValues) {
        this.customFieldValues = customFieldValues;
    }

}
