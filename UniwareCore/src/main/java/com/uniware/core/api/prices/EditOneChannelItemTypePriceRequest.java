/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *  @version     1.0, Dec 4, 2015
 *  @author shipra
 */
package com.uniware.core.api.prices;

import com.uniware.core.api.prices.AbstractChannelItemTypeRequest;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Digits;
import java.math.BigDecimal;

public class EditOneChannelItemTypePriceRequest {

    private static final long serialVersionUID = -8139979867842827991L;

    @Digits(integer = 10, fraction = 2, message = "max value cannot have more than 10 digits and 2 decimal places")
    private BigDecimal msp;
    @Digits(integer = 10, fraction = 2, message = "max value cannot have more than 10 digits and 2 decimal places")
    private BigDecimal mrp;
    @Digits(integer = 10, fraction = 2, message = "max value cannot have more than 10 digits and 2 decimal places")
    private BigDecimal sellingPrice;
    @NotBlank
    private String     currencyCode;
    @NotBlank
    private String channelProductId;
    private String sellerSkuCode;



    public BigDecimal getSellingPrice() {
        return sellingPrice;
    }

    public void setSellingPrice(BigDecimal sellingPrice) {
        this.sellingPrice = sellingPrice;
    }

    public BigDecimal getMsp() {
        return msp;
    }

    public void setMsp(BigDecimal msp) {
        this.msp = msp;
    }

    public BigDecimal getMrp() {
        return mrp;
    }

    public void setMrp(BigDecimal mrp) {
        this.mrp = mrp;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getChannelProductId() {
        return channelProductId;
    }

    public void setChannelProductId(String channelProductId) {
        this.channelProductId = channelProductId;
    }

    public String getSellerSkuCode() {
        return sellerSkuCode;
    }

    public void setSellerSkuCode(String sellerSkuCode) {
        this.sellerSkuCode = sellerSkuCode;
    }
}
