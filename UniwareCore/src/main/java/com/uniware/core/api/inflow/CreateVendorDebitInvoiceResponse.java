/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 22-Mar-2014
 *  @author karunsingla
 */
package com.uniware.core.api.inflow;

import com.unifier.core.api.base.ServiceResponse;

public class CreateVendorDebitInvoiceResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 3431538559820305302L;

    private String            vendorInvoiceCode;

    public String getVendorInvoiceCode() {
        return vendorInvoiceCode;
    }

    public void setVendorInvoiceCode(String vendorInvoiceCode) {
        this.vendorInvoiceCode = vendorInvoiceCode;
    }
}
