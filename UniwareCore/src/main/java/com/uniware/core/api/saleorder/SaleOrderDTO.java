/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jan 11, 2012
 *  @author singla
 */
package com.uniware.core.api.saleorder;

import java.util.Date;

import com.uniware.core.entity.SaleOrder;

/**
 * @author singla
 */
public class SaleOrderDTO {

    private String code;
    private String displayOrderCode;
    private String channel;
    private Date   displayOrderDateTime;
    private String status;
    private Date   created;
    private Date   updated;
    private String notificationEmail;
    private String notificationMobile;

    public SaleOrderDTO() {
    }

    public SaleOrderDTO(SaleOrder saleOrder) {
        this.code = saleOrder.getCode();
        this.displayOrderCode = saleOrder.getDisplayOrderCode();
        if (saleOrder.getChannel() != null) {
            this.channel = saleOrder.getChannel().getCode();
        }
        this.displayOrderDateTime = saleOrder.getDisplayOrderDateTime();
        this.status = saleOrder.getStatusCode();
        this.updated = saleOrder.getUpdated();
        this.created = saleOrder.getCreated();
        this.notificationMobile = saleOrder.getNotificationMobile() == null ? "" : saleOrder.getNotificationMobile();
        this.notificationEmail = saleOrder.getNotificationEmail() == null ? "" : saleOrder.getNotificationEmail();
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the created
     */
    public Date getCreated() {
        return created;
    }

    /**
     * @param created the created to set
     */
    public void setCreated(Date created) {
        this.created = created;
    }

    /**
     * @return the updated
     */
    public Date getUpdated() {
        return updated;
    }

    /**
     * @param updated the updated to set
     */
    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    /**
     * @return the notificationEmail
     */
    public String getNotificationEmail() {
        return notificationEmail;
    }

    /**
     * @param notificationEmail the notificationEmail to set
     */
    public void setNotificationEmail(String notificationEmail) {
        this.notificationEmail = notificationEmail;
    }

    /**
     * @return the notificationMobile
     */
    public String getNotificationMobile() {
        return notificationMobile;
    }

    /**
     * @param notificationMobile the notificationMobile to set
     */
    public void setNotificationMobile(String notificationMobile) {
        this.notificationMobile = notificationMobile;
    }

    /**
     * @return the displayOrderCode
     */
    public String getDisplayOrderCode() {
        return displayOrderCode;
    }

    /**
     * @param displayOrderCode the displayOrderCode to set
     */
    public void setDisplayOrderCode(String displayOrderCode) {
        this.displayOrderCode = displayOrderCode;
    }

    /**
     * @return the displayOrderDateTime
     */
    public Date getDisplayOrderDateTime() {
        return displayOrderDateTime;
    }

    /**
     * @param displayOrderDateTime the displayOrderDateTime to set
     */
    public void setDisplayOrderDateTime(Date displayOrderDateTime) {
        this.displayOrderDateTime = displayOrderDateTime;
    }

    /**
     * @return the source
     */
    public String getChannel() {
        return channel;
    }

    /**
     * @param channel the source to set
     */
    public void setChannel(String channel) {
        this.channel = channel;
    }

}
