/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 08-Oct-2013
 *  @author akshay
 */
package com.uniware.core.api.channel;

import com.uniware.core.entity.Source;

import java.io.Serializable;

public class SourceDTO implements Serializable {

    private String  code;
    private String  name;
    private boolean pendencyConfigurationEnabled;
    private String  landingPageScriptName;
    private boolean enabled;
    private int     priority;

    public SourceDTO() {
        super();
    }

    public SourceDTO(Source source) {
        setCode(source.getCode());
        setName(source.getName());
        setEnabled(source.isEnabled());
        setPriority(source.getPriority());
        setPendencyConfigurationEnabled(source.isPendencyConfigurationEnabled());
        setLandingPageScriptName(source.getLandingPageScriptName());
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLandingPageScriptName() {
        return landingPageScriptName;
    }

    public void setLandingPageScriptName(String landingPageScriptName) {
        this.landingPageScriptName = landingPageScriptName;
    }

    public boolean isPendencyConfigurationEnabled() {
        return pendencyConfigurationEnabled;
    }

    public void setPendencyConfigurationEnabled(boolean pendencyConfigurationEnabled) {
        this.pendencyConfigurationEnabled = pendencyConfigurationEnabled;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

}
