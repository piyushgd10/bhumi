/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Oct 20, 2015
 *  @author akshay
 */
package com.uniware.core.api.prices.dto;

public class ListingPricesDTO {

    private String   sellerSkuCode;
    private String   channelProductId;
    private String   channelCode;
    private String   imageUrl;
    private String   productName;
    private PriceDTO prices;

    public String getSellerSkuCode() {
        return sellerSkuCode;
    }

    public void setSellerSkuCode(String sellerSkuCode) {
        this.sellerSkuCode = sellerSkuCode;
    }

    public String getChannelProductId() {
        return channelProductId;
    }

    public void setChannelProductId(String channelProductId) {
        this.channelProductId = channelProductId;
    }

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public PriceDTO getPrices() {
        return prices;
    }

    public void setPrices(PriceDTO prices) {
        this.prices = prices;
    }

}
