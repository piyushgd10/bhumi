/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 19-Sep-2012
 *  @author praveeng
 */
package com.uniware.core.api.catalog;

import com.unifier.core.api.base.ServiceRequest;
import com.unifier.core.pagination.SearchOptions;

/**
 * @author praveeng
 */
public class SearchShippingProviderLocationRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -4685564987397851435L;
    private String            state;
    private String            shippingProvider;
    private String            shippingMethod;
    private String            shippingProviderRegion;
    private String            pincode;
    private String            routingCode;
    private boolean           enabled;
    private SearchOptions     searchOptions;

    /**
     * @return the state
     */
    public String getState() {
        return state;
    }

    /**
     * @param state the state to set
     */
    public void setState(String state) {
        this.state = state;
    }

    /**
     * @return the shippingProvider
     */
    public String getShippingProvider() {
        return shippingProvider;
    }

    /**
     * @param shippingProvider the shippingProvider to set
     */
    public void setShippingProvider(String shippingProvider) {
        this.shippingProvider = shippingProvider;
    }

    /**
     * @return the shippingMethod
     */
    public String getShippingMethod() {
        return shippingMethod;
    }

    /**
     * @param shippingMethod the shippingMethod to set
     */
    public void setShippingMethod(String shippingMethod) {
        this.shippingMethod = shippingMethod;
    }

    /**
     * @return the shippingProviderRegion
     */
    public String getShippingProviderRegion() {
        return shippingProviderRegion;
    }

    /**
     * @param shippingProviderRegion the shippingProviderRegion to set
     */
    public void setShippingProviderRegion(String shippingProviderRegion) {
        this.shippingProviderRegion = shippingProviderRegion;
    }

    /**
     * @return the pincode
     */
    public String getPincode() {
        return pincode;
    }

    /**
     * @param pincode the pincode to set
     */
    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    /**
     * @return the routingCode
     */
    public String getRoutingCode() {
        return routingCode;
    }

    /**
     * @param routingCode the routingCode to set
     */
    public void setRoutingCode(String routingCode) {
        this.routingCode = routingCode;
    }

    /**
     * @return the enabled
     */
    public boolean isEnabled() {
        return enabled;
    }

    /**
     * @param enabled the enabled to set
     */
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    /**
     * @return the searchOptions
     */
    public SearchOptions getSearchOptions() {
        return searchOptions;
    }

    /**
     * @param searchOptions the searchOptions to set
     */
    public void setSearchOptions(SearchOptions searchOptions) {
        this.searchOptions = searchOptions;
    }

}
