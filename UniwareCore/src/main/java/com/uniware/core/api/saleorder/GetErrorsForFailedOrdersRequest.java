/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 02-Jun-2014
 *  @author unicom
 */
package com.uniware.core.api.saleorder;

import java.util.List;

import javax.validation.constraints.Size;

import com.unifier.core.api.base.ServiceRequest;

public class GetErrorsForFailedOrdersRequest extends ServiceRequest {

    private static final long serialVersionUID = 7672781000385690945L;

    @Size(min = 1, max = 50)
    List<String>              saleOrderCodes;

    public List<String> getSaleOrderCodes() {
        return saleOrderCodes;
    }

    public void setSaleOrderCodes(List<String> saleOrderCodes) {
        this.saleOrderCodes = saleOrderCodes;
    }

}
