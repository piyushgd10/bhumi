/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 03-Jun-2012
 *  @author vibhu
 */
package com.uniware.core.api.channel;

import java.util.Map;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author Sunny
 */
public class PostConfigureChannelConnectorRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long   serialVersionUID = 1417801739977690313L;

    private Map<String, String> requestParams;

    public Map<String, String> getRequestParams() {
        return requestParams;
    }

    public void setRequestParams(Map<String, String> requestParams) {
        this.requestParams = requestParams;
    }

}
