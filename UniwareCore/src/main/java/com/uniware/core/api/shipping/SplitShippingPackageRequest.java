/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jan 6, 2012
 *  @author singla
 */
package com.uniware.core.api.shipping;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author singla
 */
public class SplitShippingPackageRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -3813993542150296831L;

    @NotBlank
    private String            shippingPackageCode;

    @Valid
    @NotEmpty
    private List<SplitItem>   splitItems;

    @NotNull
    private Integer           userId;

    public static class SplitItem {

        @NotBlank
        private String  saleOrderItemCode;

        @NotNull
        private Integer splitNumber;

        /**
         * @return the saleOrderItemCode
         */
        public String getSaleOrderItemCode() {
            return saleOrderItemCode;
        }

        /**
         * @param saleOrderItemCode the saleOrderItemCode to set
         */
        public void setSaleOrderItemCode(String saleOrderItemCode) {
            this.saleOrderItemCode = saleOrderItemCode;
        }

        /**
         * @return the splitNumber
         */
        public Integer getSplitNumber() {
            return splitNumber;
        }

        /**
         * @param splitNumber the splitNumber to set
         */
        public void setSplitNumber(Integer splitNumber) {
            this.splitNumber = splitNumber;
        }
    }

    /**
     * @return the shippingPackageCode
     */
    public String getShippingPackageCode() {
        return shippingPackageCode;
    }

    /**
     * @param shippingPackageCode the shippingPackageCode to set
     */
    public void setShippingPackageCode(String shippingPackageCode) {
        this.shippingPackageCode = shippingPackageCode;
    }

    /**
     * @return the splitItems
     */
    public List<SplitItem> getSplitItems() {
        return splitItems;
    }

    /**
     * @param splitItems the splitItems to set
     */
    public void setSplitItems(List<SplitItem> splitItems) {
        this.splitItems = splitItems;
    }

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }
}
