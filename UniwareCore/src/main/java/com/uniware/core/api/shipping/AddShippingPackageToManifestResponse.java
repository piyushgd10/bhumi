/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 8, 2012
 *  @author singla
 */
package com.uniware.core.api.shipping;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author singla
 */
public class AddShippingPackageToManifestResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -2882824685335670839L;

    private ManifestItemDTO   manifestItem;

    private Integer totalManifestItemCount;

    public ManifestItemDTO getManifestItem() {
        return manifestItem;
    }

    public void setManifestItem(ManifestItemDTO manifestItem) {
        this.manifestItem = manifestItem;
    }

    public Integer getTotalManifestItemCount() {
        return totalManifestItemCount;
    }

    public void setTotalManifestItemCount(Integer totalManifestItemCount) {
        this.totalManifestItemCount = totalManifestItemCount;
    }

    
}
