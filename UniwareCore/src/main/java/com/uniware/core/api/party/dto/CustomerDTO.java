/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 06-Sep-2013
 *  @author parijat
 */
package com.uniware.core.api.party.dto;

import com.uniware.core.entity.Customer;

/**
 * @author parijat
 */
public class CustomerDTO extends GenericPartyDTO {

    private boolean providesCform;
    private boolean dualCompanyRetail;

    /**
     * @param providesCform
     * @param dualCompanyRetail
     * @param taxExempted
     */
    public CustomerDTO(Customer customer) {
        super(customer);
        this.providesCform = customer.isProvidesCform();
        this.dualCompanyRetail = customer.isDualCompanyRetail();
    }

    /**
     * @return the providesCform
     */
    public boolean isProvidesCform() {
        return providesCform;
    }

    /**
     * @param providesCform the providesCform to set
     */
    public void setProvidesCform(boolean providesCform) {
        this.providesCform = providesCform;
    }

    /**
     * @return the dualCompanyRetail
     */
    public boolean isDualCompanyRetail() {
        return dualCompanyRetail;
    }

    /**
     * @param dualCompanyRetail the dualCompanyRetail to set
     */
    public void setDualCompanyRetail(boolean dualCompanyRetail) {
        this.dualCompanyRetail = dualCompanyRetail;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getCode() == null) ? 0 : getCode().hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof CustomerDTO))
            return false;
        CustomerDTO other = (CustomerDTO) obj;
        if (getCode() == null) {
            if (other.getCode() != null)
                return false;
        } else if (!getCode().equals(other.getCode()))
            return false;
        return true;
    }

}
