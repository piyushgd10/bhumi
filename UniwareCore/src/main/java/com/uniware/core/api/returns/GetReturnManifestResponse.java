/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 26-May-2012
 *  @author praveeng
 */
package com.uniware.core.api.returns;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author praveeng
 */
public class GetReturnManifestResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -4543817473185458517L;

    private ReturnManifestDTO returnManifest;

    /**
     * @return the returnManifest
     */
    public ReturnManifestDTO getReturnManifest() {
        return returnManifest;
    }

    /**
     * @param returnManifest the returnManifest to set
     */
    public void setReturnManifest(ReturnManifestDTO returnManifest) {
        this.returnManifest = returnManifest;
    }

}
