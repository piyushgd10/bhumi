/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 18-Apr-2013
 *  @author unicom
 */
package com.uniware.core.api.channel;

import com.unifier.core.utils.StringUtils;
import com.uniware.core.entity.ChannelConnector;
import com.uniware.core.entity.ChannelConnectorParameter;
import com.uniware.core.entity.SourceConnectorParameter;
import com.uniware.core.entity.SourceConnector;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Sunny Agarwal
 */
public class ChannelConnectorDTO implements Serializable {

    private String                             name;
    private String                             displayName;
    private String                             helpText;
    private int                                priority;
    private String                             statusCode;
    private String                             errorMessage;
    private boolean                            thirdParty;
    private boolean                            requiredInOrderSync;
    private boolean                            requiredInInventorySync;
    private boolean                            requirdInReconciliationSync;
    private List<ChannelConnectorParameterDTO> channelConnectorParameters = new ArrayList<ChannelConnectorParameterDTO>();

    public ChannelConnectorDTO() {
    }

    public ChannelConnectorDTO(ChannelConnector channelConnector, SourceConnector sourceConnector) {
        name = channelConnector.getSourceConnectorName();
        displayName = sourceConnector.getDisplayName();
        helpText = sourceConnector.getHelpText();
        priority = sourceConnector.getPriority();
        statusCode = channelConnector.getStatusCode().name();
        errorMessage = (!ChannelConnector.Status.ACTIVE.name().equalsIgnoreCase(statusCode) && StringUtils.isNotBlank(channelConnector.getErrorMessage())) ? channelConnector.getErrorMessage()
                : "";
        thirdParty = sourceConnector.isThirdParty();
        requiredInInventorySync = sourceConnector.isRequiredInInventorySync();
        requiredInOrderSync = sourceConnector.isRequiredInOrderSync();
        requirdInReconciliationSync = sourceConnector.isRequiredInReconciliationSync();
        Map<String, ChannelConnectorParameter> nameToChannelConnectorParameter = new HashMap<String, ChannelConnectorParameter>();
        for (ChannelConnectorParameter ccp : channelConnector.getChannelConnectorParameters()) {
            nameToChannelConnectorParameter.put(ccp.getName(), ccp);
        }
        for (SourceConnectorParameter scp : sourceConnector.getSourceConnectorParameters()) {
            if (scp.getType() != SourceConnectorParameter.Type.HIDDEN) {
                ChannelConnectorParameter connectorParam = nameToChannelConnectorParameter.get(scp.getName());
                if (connectorParam != null) {
                    channelConnectorParameters.add(new ChannelConnectorParameterDTO(connectorParam, scp));
                }
            }
        }
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHelpText() {
        return helpText;
    }

    public void setHelpText(String helpText) {
        this.helpText = helpText;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public boolean isThirdParty() {
        return thirdParty;
    }

    public void setThirdParty(boolean thirdParty) {
        this.thirdParty = thirdParty;
    }

    public boolean isRequiredInOrderSync() {
        return requiredInOrderSync;
    }

    public void setRequiredInOrderSync(boolean requiredInOrderSync) {
        this.requiredInOrderSync = requiredInOrderSync;
    }

    public boolean isRequiredInInventorySync() {
        return requiredInInventorySync;
    }

    public void setRequiredInInventorySync(boolean requiredInInventorySync) {
        this.requiredInInventorySync = requiredInInventorySync;
    }

    public boolean isRequirdInReconciliationSync() {
        return requirdInReconciliationSync;
    }

    public void setRequirdInReconciliationSync(boolean requirdInReconciliationSync) {
        this.requirdInReconciliationSync = requirdInReconciliationSync;
    }

    public List<ChannelConnectorParameterDTO> getChannelConnectorParameters() {
        return channelConnectorParameters;
    }

    public void setChannelConnectorParameters(List<ChannelConnectorParameterDTO> channelConnectorParameters) {
        this.channelConnectorParameters = channelConnectorParameters;
    }

}
