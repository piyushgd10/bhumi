/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 10-Dec-2013
 *  @author unicom
 */
package com.uniware.core.api.item;

import com.unifier.core.api.base.ServiceRequest;

public class GetAllFacilityItemDetailRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private String            itemCode;

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

}
