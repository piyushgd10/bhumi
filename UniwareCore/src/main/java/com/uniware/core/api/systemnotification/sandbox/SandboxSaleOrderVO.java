/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 25-Dec-2013
 *  @author sunny
 */
package com.uniware.core.api.systemnotification.sandbox;

import java.util.Date;

import com.uniware.core.entity.SaleOrder;

public class SandboxSaleOrderVO {

    private String statusCode;
    private String code;
    private String displayOrderCode;
    private String additionalInfo;
    private Date   displayOrderDateTime;

    public SandboxSaleOrderVO() {
    }

    public SandboxSaleOrderVO(SaleOrder so) {
        statusCode = so.getStatusCode();
        code = so.getCode();
        additionalInfo = so.getAdditionalInfo();
        displayOrderCode = so.getDisplayOrderCode();
        displayOrderDateTime = so.getDisplayOrderDateTime();
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDisplayOrderCode() {
        return displayOrderCode;
    }

    public void setDisplayOrderCode(String displayOrderCode) {
        this.displayOrderCode = displayOrderCode;
    }

    public Date getDisplayOrderDateTime() {
        return displayOrderDateTime;
    }

    public void setDisplayOrderDateTime(Date displayOrderDateTime) {
        this.displayOrderDateTime = displayOrderDateTime;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

}
