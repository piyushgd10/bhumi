/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 14-May-2012
 *  @author praveeng
 */
package com.uniware.core.api.putaway;

import java.util.ArrayList;
import java.util.List;

import com.unifier.core.api.base.ServiceResponse;

public class SearchDirectReturnsResponse extends ServiceResponse {
    /**
     * 
     */
    private static final long                  serialVersionUID = 4236786481704462098L;
    private final List<DirectSaleOrderItemDTO> saleOrderItems   = new ArrayList<DirectSaleOrderItemDTO>();

    public List<DirectSaleOrderItemDTO> getSaleOrderItems() {
        return saleOrderItems;
    }

    public static class DirectSaleOrderItemDTO {
        private int    id;
        private String code;
        private String saleOrderCode;
        private String itemSku;
        private String itemName;
        private String color;
        private String brand;
        private String size;
        private String saleOrderItemStatus;
        private String trackingNumber;
        private String itemCode;
        private String imageUrl;
        private String pageUrl;

        public String getSaleOrderItemStatus() {
            return saleOrderItemStatus;
        }

        public void setSaleOrderItemStatus(String saleOrderItemStatus) {
            this.saleOrderItemStatus = saleOrderItemStatus;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getSaleOrderCode() {
            return saleOrderCode;
        }

        public void setSaleOrderCode(String saleOrderCode) {
            this.saleOrderCode = saleOrderCode;
        }

        public String getItemSku() {
            return itemSku;
        }

        public void setItemSku(String itemSku) {
            this.itemSku = itemSku;
        }

        public String getItemName() {
            return itemName;
        }

        public void setItemName(String itemName) {
            this.itemName = itemName;
        }

        public String getTrackingNumber() {
            return trackingNumber;
        }

        public void setTrackingNumber(String trackingNumber) {
            this.trackingNumber = trackingNumber;
        }

        public String getItemCode() {
            return itemCode;
        }

        public void setItemCode(String itemCode) {
            this.itemCode = itemCode;
        }

        public String getImageUrl() {
            return imageUrl;
        }

        public void setImageUrl(String imageUrl) {
            this.imageUrl = imageUrl;
        }

        public String getPageUrl() {
            return pageUrl;
        }

        public void setPageUrl(String pageUrl) {
            this.pageUrl = pageUrl;
        }

        public String getColor() {
            return color;
        }

        public void setColor(String color) {
            this.color = color;
        }

        public String getBrand() {
            return brand;
        }

        public void setBrand(String brand) {
            this.brand = brand;
        }

        public String getSize() {
            return size;
        }

        public void setSize(String size) {
            this.size = size;
        }
    }

    public void addDirectSaleOrderItemDTO(DirectSaleOrderItemDTO directSaleOrderItemDTO) {
        saleOrderItems.add(directSaleOrderItemDTO);
    }
}