/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Sep 18, 2013
 *  @author sunny
 */
package com.uniware.core.api.search;

import java.util.ArrayList;
import java.util.List;

public class SearchDTO {

    public static class Entry {
        private String value;
        private String url;

        public Entry() {
        }

        public Entry(String value, String url) {
            super();
            this.value = value;
            this.url = url;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        @Override
        public String toString() {
            return "Entry [value=" + value + ", url=" + url + "]";
        }

    }

    public String      type;
    public List<Entry> searchResults = new ArrayList<Entry>();

    public SearchDTO() {
    }

    public SearchDTO(String type) {
        super();
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<Entry> getSearchResults() {
        return searchResults;
    }

    public void setSearchResults(List<Entry> searchResults) {
        this.searchResults = searchResults;
    }

    @Override
    public String toString() {
        return "SearchDTO [type=" + type + ", searchResults=" + searchResults + "]";
    }

}