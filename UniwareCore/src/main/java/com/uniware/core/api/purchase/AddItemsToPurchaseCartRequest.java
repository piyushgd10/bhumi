/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Apr 7, 2012
 *  @author singla
 */
package com.uniware.core.api.purchase;

import com.unifier.core.api.base.ServiceRequest;

import java.math.BigDecimal;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author singla
 */
public class AddItemsToPurchaseCartRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long        serialVersionUID = 5637425828228663053L;

    @NotNull
    private Integer                  userId;

    @Valid
    @NotEmpty
    private List<WsPurchaseCartItem> purchaseCartItems;

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return the purchaseCartItems
     */
    public List<WsPurchaseCartItem> getPurchaseCartItems() {
        return purchaseCartItems;
    }

    /**
     * @param purchaseCartItems the purchaseCartItems to set
     */
    public void setPurchaseCartItems(List<WsPurchaseCartItem> purchaseCartItems) {
        this.purchaseCartItems = purchaseCartItems;
    }

    public static class WsPurchaseCartItem {
        @NotBlank
        private String     vendorCode;
        @NotEmpty
        private String     itemSku;

        @NotNull
        @Min(value = 1)
        private Integer    quantity;

        private BigDecimal unitPrice;

        /**
         * @return the itemSku
         */
        public String getItemSku() {
            return itemSku;
        }

        /**
         * @param itemSku the itemSku to set
         */
        public void setItemSku(String itemSku) {
            this.itemSku = itemSku;
        }

        /**
         * @return the quantity
         */
        public Integer getQuantity() {
            return quantity;
        }

        /**
         * @param quantity the quantity to set
         */
        public void setQuantity(Integer quantity) {
            this.quantity = quantity;
        }

        /**
         * @return the unitPrice
         */
        public BigDecimal getUnitPrice() {
            return unitPrice;
        }

        /**
         * @param unitPrice the unitPrice to set
         */
        public void setUnitPrice(BigDecimal unitPrice) {
            this.unitPrice = unitPrice;
        }

        /**
         * @return the vendorCode
         */
        public String getVendorCode() {
            return vendorCode;
        }

        /**
         * @param vendorCode the vendorCode to set
         */
        public void setVendorCode(String vendorCode) {
            this.vendorCode = vendorCode;
        }
    }
}
