package com.uniware.core.api.cyclecount;

import com.unifier.core.api.base.ServiceResponse;
import com.uniware.core.entity.Item;

/**
 * Created by harshpal on 21/04/16.
 */
public class GetItemResponse extends ServiceResponse {

    private ItemDTO item;

    public ItemDTO getItem() {
        return item;
    }

    public void setItem(ItemDTO item) {
        this.item = item;
    }

    public static class ItemDTO {
        private String code;
        private String itemTypeName;
        private String skuCode;
        private String statusCode;
        private String rejectionReason;

        public ItemDTO(Item item) {
            this.code = item.getCode();
            this.itemTypeName = item.getItemType().getName();
            this.skuCode = item.getItemType().getSkuCode();
            this.statusCode = item.getStatusCode();
            this.rejectionReason = item.getRejectionReason();
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getItemTypeName() {
            return itemTypeName;
        }

        public void setItemTypeName(String itemTypeName) {
            this.itemTypeName = itemTypeName;
        }

        public String getSkuCode() {
            return skuCode;
        }

        public void setSkuCode(String skuCode) {
            this.skuCode = skuCode;
        }

        public String getStatusCode() {
            return statusCode;
        }

        public void setStatusCode(String statusCode) {
            this.statusCode = statusCode;
        }

        public String getRejectionReason() {
            return rejectionReason;
        }

        public void setRejectionReason(String rejectionReason) {
            this.rejectionReason = rejectionReason;
        }
    }
}
