/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 12-Feb-2015
 *  @author parijat
 */
package com.uniware.core.api.shipping;

import com.unifier.core.api.base.ServiceResponse;
import com.uniware.core.api.admin.shipping.ShippingProviderDetailDTO;

/**
 * @author parijat
 *
 */
public class EditShippingProviderResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -8029668320935328232L;

    private ShippingProviderDetailDTO shippingProviderDetailDTO;

    /**
     * @return the shippingProviderDetailDTO
     */
    public ShippingProviderDetailDTO getShippingProviderDetailDTO() {
        return shippingProviderDetailDTO;
    }

    /**
     * @param shippingProviderDetailDTO the shippingProviderDetailDTO to set
     */
    public void setShippingProviderDetailDTO(ShippingProviderDetailDTO shippingProviderDetailDTO) {
        this.shippingProviderDetailDTO = shippingProviderDetailDTO;
    }

}
