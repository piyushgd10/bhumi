/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 22-Mar-2014
 *  @author karunsingla
 */
package com.uniware.core.api.inflow;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

public class AddInflowReceiptToVendorInvoiceRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -5438478583752816808L;

    @NotBlank
    private String            vendorInvoiceCode;

    @NotBlank
    private String            purchaseOrderCode;

    @NotBlank
    private String            inflowReceiptCode;

    public String getVendorInvoiceCode() {
        return vendorInvoiceCode;
    }

    public void setVendorInvoiceCode(String vendorInvoiceCode) {
        this.vendorInvoiceCode = vendorInvoiceCode;
    }

    public String getInflowReceiptCode() {
        return inflowReceiptCode;
    }

    public void setInflowReceiptCode(String inflowReceiptCode) {
        this.inflowReceiptCode = inflowReceiptCode;
    }

    public String getPurchaseOrderCode() {
        return purchaseOrderCode;
    }

    public void setPurchaseOrderCode(String purchaseOrderCode) {
        this.purchaseOrderCode = purchaseOrderCode;
    }

}
