/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 20, 2012
 *  @author praveeng
 */
package com.uniware.core.api.party.dto;

import com.uniware.core.api.sequence.SequenceDTO;
import com.uniware.core.entity.BillingParty;
import com.uniware.core.entity.Sequence;

/**
 * @author Sunny
 */
public class BillingPartyDTO extends GenericPartyDTO {

    private SequenceDTO retailInvoiceSequence;

    private SequenceDTO taxInvoiceSequence;

    private SequenceDTO saleInvoiceSequence;

    private SequenceDTO saleReturnInvoiceSequence;

    public BillingPartyDTO() {

    }

    public BillingPartyDTO(BillingParty billingParty, Sequence retailInvoiceSequence, Sequence taxInvoiceSequence) {
        super(billingParty);
        this.retailInvoiceSequence = new SequenceDTO(retailInvoiceSequence);
        this.taxInvoiceSequence = new SequenceDTO(taxInvoiceSequence);
    }

    public BillingPartyDTO(BillingParty billingParty, Sequence retailInvoiceSequence, Sequence taxInvoiceSequence, Sequence saleInvoiceSequence, Sequence saleReturnInvoiceSequence) {
        super(billingParty);
        this.retailInvoiceSequence = new SequenceDTO(retailInvoiceSequence);
        this.taxInvoiceSequence = new SequenceDTO(taxInvoiceSequence);
        this.saleInvoiceSequence = new SequenceDTO(saleInvoiceSequence);
        this.saleReturnInvoiceSequence = new SequenceDTO(saleReturnInvoiceSequence);
    }

    public SequenceDTO getRetailInvoiceSequence() {
        return retailInvoiceSequence;
    }

    public void setRetailInvoiceSequence(SequenceDTO retailInvoiceSequence) {
        this.retailInvoiceSequence = retailInvoiceSequence;
    }

    public SequenceDTO getTaxInvoiceSequence() {
        return taxInvoiceSequence;
    }

    public void setTaxInvoiceSequence(SequenceDTO taxInvoiceSequence) {
        this.taxInvoiceSequence = taxInvoiceSequence;
    }

    public SequenceDTO getSaleInvoiceSequence() {
        return saleInvoiceSequence;
    }

    public void setSaleInvoiceSequence(SequenceDTO saleInvoiceSequence) {
        this.saleInvoiceSequence = saleInvoiceSequence;
    }

    public SequenceDTO getSaleReturnInvoiceSequence() {
        return saleReturnInvoiceSequence;
    }

    public void setSaleReturnInvoiceSequence(SequenceDTO saleReturnInvoiceSequence) {
        this.saleReturnInvoiceSequence = saleReturnInvoiceSequence;
    }
}
