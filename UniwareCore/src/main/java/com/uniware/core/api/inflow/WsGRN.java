/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 *  @version     1.0, 30-Aug-2012
 *  @author akshay
 */
package com.uniware.core.api.inflow;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;

import org.hibernate.validator.constraints.NotEmpty;

import com.unifier.core.api.customfields.WsCustomFieldValue;

/**
 * @author akshay
 */
public class WsGRN {

    @NotEmpty
    private String                   vendorInvoiceNumber;

    @NotNull
    @Past
    private Date                     vendorInvoiceDate;

    @Valid
    private List<WsCustomFieldValue> customFieldValues;

    private String                   currencyCode;

    @NotNull
    private Integer                  userId;

    /**
     * @return the vendorInvoiceNumber
     */
    public String getVendorInvoiceNumber() {
        return vendorInvoiceNumber;
    }

    /**
     * @param vendorInvoiceNumber the vendorInvoiceNumber to set
     */
    public void setVendorInvoiceNumber(String vendorInvoiceNumber) {
        this.vendorInvoiceNumber = vendorInvoiceNumber;
    }

    /**
     * @return the vendorInvoiceDate
     */
    public Date getVendorInvoiceDate() {
        return vendorInvoiceDate;
    }

    /**
     * @param vendorInvoiceDate the vendorInvoiceDate to set
     */
    public void setVendorInvoiceDate(Date vendorInvoiceDate) {
        this.vendorInvoiceDate = vendorInvoiceDate;
    }

    /**
     * @return the customFieldValues
     */
    public List<WsCustomFieldValue> getCustomFieldValues() {
        return customFieldValues;
    }

    /**
     * @param customFieldValues the customFieldValues to set
     */
    public void setCustomFieldValues(List<WsCustomFieldValue> customFieldValues) {
        this.customFieldValues = customFieldValues;
    }

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

}