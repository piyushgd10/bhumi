/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jan 10, 2012
 *  @author singla
 */
package com.uniware.core.api.picker;

import javax.validation.constraints.NotNull;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author singla
 */
public class GetPicklistRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 7646307772810377253L;

    @NotNull
    private String            picklistCode;

    /**
     * @return the picklistCode
     */
    public String getPicklistCode() {
        return picklistCode;
    }

    /**
     * @param picklistCode the picklistCode to set
     */
    public void setPicklistCode(String picklistCode) {
        this.picklistCode = picklistCode;
    }

}
