/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jul 29, 2012
 *  @author singla
 */
package com.uniware.core.api.purchase;

import com.unifier.core.api.base.ServiceResponse;

public class ClonePurchaseOrderResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -8871740852730606127L;

    private String            purchaseOrderCode;

    /**
     * @return the purchaseOrderCode
     */
    public String getPurchaseOrderCode() {
        return purchaseOrderCode;
    }

    /**
     * @param purchaseOrderCode the purchaseOrderCode to set
     */
    public void setPurchaseOrderCode(String purchaseOrderCode) {
        this.purchaseOrderCode = purchaseOrderCode;
    }

}
