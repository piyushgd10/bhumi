/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jul 29, 2012
 *  @author singla
 */
package com.uniware.core.api.purchase;

import java.math.BigDecimal;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import com.unifier.core.api.base.ServiceRequest;

public class ClonePurchaseOrderRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long         serialVersionUID = -8061655953662729738L;

    @NotBlank
    private String                    purchaseOrderCode;

    @NotEmpty
    @Valid
    private List<WsPurchaseOrderItem> purchaseOrderItems;

    @NotNull
    private Integer                   userId;

    @NotNull
    @Valid
    private WsPurchaseOrderMetadata   purchaseOrderMetadata;

    private String                    logisticChargesDivisionMethod;

    private BigDecimal                logisticCharges  = BigDecimal.ZERO;

    @NotNull
    private String                    vendorCode;

    /**
     * @return the purchaseOrderCode
     */
    public String getPurchaseOrderCode() {
        return purchaseOrderCode;
    }

    /**
     * @param purchaseOrderCode the purchaseOrderCode to set
     */
    public void setPurchaseOrderCode(String purchaseOrderCode) {
        this.purchaseOrderCode = purchaseOrderCode;
    }

    /**
     * @return the purchaseOrderItems
     */
    public List<WsPurchaseOrderItem> getPurchaseOrderItems() {
        return purchaseOrderItems;
    }

    /**
     * @param purchaseOrderItems the purchaseOrderItems to set
     */
    public void setPurchaseOrderItems(List<WsPurchaseOrderItem> purchaseOrderItems) {
        this.purchaseOrderItems = purchaseOrderItems;
    }

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return the purchaseOrderMetadata
     */
    public WsPurchaseOrderMetadata getPurchaseOrderMetadata() {
        return purchaseOrderMetadata;
    }

    /**
     * @param purchaseOrderMetadata the purchaseOrderMetadata to set
     */
    public void setPurchaseOrderMetadata(WsPurchaseOrderMetadata purchaseOrderMetadata) {
        this.purchaseOrderMetadata = purchaseOrderMetadata;
    }

    /**
     * @return the logisticChargesDivisionMethod
     */
    public String getLogisticChargesDivisionMethod() {
        return logisticChargesDivisionMethod;
    }

    /**
     * @param logisticChargesDivisionMethod the logisticChargesDivisionMethod to set
     */
    public void setLogisticChargesDivisionMethod(String logisticChargesDivisionMethod) {
        this.logisticChargesDivisionMethod = logisticChargesDivisionMethod;
    }

    /**
     * @return the logisticCharges
     */
    public BigDecimal getLogisticCharges() {
        return logisticCharges;
    }

    /**
     * @param logisticCharges the logisticCharges to set
     */
    public void setLogisticCharges(BigDecimal logisticCharges) {
        this.logisticCharges = logisticCharges;
    }

    public String getVendorCode() {
        return vendorCode;
    }

    public void setVendorCode(String vendorCode) {
        this.vendorCode = vendorCode;
    }

}
