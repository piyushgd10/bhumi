/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 9, 2012
 *  @author praveeng
 */
package com.uniware.core.api.channel;

import com.unifier.core.api.base.ServiceRequest;
import com.uniware.core.api.prices.WsChannelItemTypePrice;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * @author Sunny Agarwal
 */
public class CreateChannelItemTypeRequest extends ServiceRequest {

    private static final long      serialVersionUID = -5149174516181942588L;

    @Valid
    @NotNull
    private WsChannelItemType      channelItemType;

    private String                 syncId;

    @Valid
    private WsChannelItemTypePrice channelItemTypePrice;

    public CreateChannelItemTypeRequest() {
        super();
    }

    public CreateChannelItemTypeRequest(WsChannelItemType channelItemType) {
        super();
        this.channelItemType = channelItemType;
    }

    public CreateChannelItemTypeRequest(WsChannelItemType channelItemType, String syncId) {
        super();
        this.channelItemType = channelItemType;
        this.syncId = syncId;
    }

    public String getSyncId() {
        return syncId;
    }

    public void setSyncId(String syncId) {
        this.syncId = syncId;
    }

    public WsChannelItemType getChannelItemType() {
        return channelItemType;
    }

    public void setChannelItemType(WsChannelItemType channelItemType) {
        this.channelItemType = channelItemType;
    }

    public WsChannelItemTypePrice getChannelItemTypePrice() {
        return channelItemTypePrice;
    }

    public void setChannelItemTypePrice(WsChannelItemTypePrice channelItemTypePrice) {
        this.channelItemTypePrice = channelItemTypePrice;
    }

    @Override
    public String toString() {
        return "CreateChannelItemTypeRequest{" + "channelItemType=" + channelItemType + ", syncId='" + syncId + '\'' + ", channelItemTypePrice=" + channelItemTypePrice + '}';
    }
}
