/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 06-Jan-2015
 *  @author akshaykochhar
 */
package com.uniware.core.api.channel;

import java.io.Serializable;
import java.util.List;

public class ChannelInventoryStatusDTO implements Serializable {


    private int                          channelId;
    private String                        code;
    private String                        name;
    private boolean                       enabled;
    private String                        colorCode;
    private String                        shortName;
    private String                        inventorySyncStatus;
    private SourceDTO                     sourceDTO;
    private List<ChannelConnectorDTO>     channelConnectors;
    private ChannelInventorySyncStatusDTO lastInventorySyncResult;

    public ChannelInventoryStatusDTO() {
    }

    public ChannelInventoryStatusDTO(ChannelSyncDTO channelSyncDTO) {
        setChannelId(channelSyncDTO.getChannelId());
        this.code = channelSyncDTO.getCode();
        this.name = channelSyncDTO.getName();
        this.enabled = channelSyncDTO.isEnabled();
        this.colorCode = channelSyncDTO.getColorCode();
        this.shortName = channelSyncDTO.getShortName();
        this.inventorySyncStatus = channelSyncDTO.getInventorySyncStatus();
        this.lastInventorySyncResult = channelSyncDTO.getLastInventorySyncResult();
        this.channelConnectors = channelSyncDTO.getChannelConnectors();
        this.sourceDTO = channelSyncDTO.getSourceDTO();
    }

    public ChannelInventoryStatusDTO(String code, String name, boolean enabled, String colorCode, String shortName, String status, ChannelInventorySyncStatusDTO lastSyncResult) {
        this.code = code;
        this.name = name;
        this.enabled = enabled;
        this.colorCode = colorCode;
        this.shortName = shortName;
        this.inventorySyncStatus = status;
        this.lastInventorySyncResult = lastSyncResult;
    }

    public int getChannelId() {
        return channelId;
    }

    public void setChannelId(int channelId) {
        this.channelId = channelId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getColorCode() {
        return colorCode;
    }

    public void setColorCode(String colorCode) {
        this.colorCode = colorCode;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getInventorySyncStatus() {
        return inventorySyncStatus;
    }

    public void setInventorySyncStatus(String inventorySyncStatus) {
        this.inventorySyncStatus = inventorySyncStatus;
    }

    public ChannelInventorySyncStatusDTO getLastInventorySyncResult() {
        return lastInventorySyncResult;
    }

    public void setLastInventorySyncResult(ChannelInventorySyncStatusDTO lastInventorySyncResult) {
        this.lastInventorySyncResult = lastInventorySyncResult;
    }

    public SourceDTO getSourceDTO() {
        return sourceDTO;
    }

    public void setSourceDTO(SourceDTO sourceDTO) {
        this.sourceDTO = sourceDTO;
    }

    public List<ChannelConnectorDTO> getChannelConnectors() {
        return channelConnectors;
    }

    public void setChannelConnectors(List<ChannelConnectorDTO> channelConnectors) {
        this.channelConnectors = channelConnectors;
    }
    
}
