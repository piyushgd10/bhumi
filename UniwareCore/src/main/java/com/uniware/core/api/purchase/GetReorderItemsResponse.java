/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 2, 2013
 *  @author praveeng
 */
package com.uniware.core.api.purchase;

import com.unifier.core.api.base.ServiceResponse;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.uniware.core.entity.ItemType;

/**
 * @author praveeng
 */
public class GetReorderItemsResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long    serialVersionUID = -1085028315666368631L;
    private List<ReorderItemDTO> itemTypes        = new ArrayList<ReorderItemDTO>();

    /**
     * @return the itemTypes
     */
    public List<ReorderItemDTO> getItemTypes() {
        return itemTypes;
    }

    /**
     * @param itemTypes the itemTypes to set
     */
    public void setItemTypes(List<ReorderItemDTO> itemTypes) {
        this.itemTypes = itemTypes;
    }

    public static class ReorderItemDTO {
        private String                         skuCode;
        private String                         name;
        private String                         itemTypeImageUrl;
        private String                         itemTypePageUrl;
        private int                            waitingQuantity;
        private int                            minOrderSize;
        private int                            reorderQuantity;
        private List<ReorderVendorItemTypeDTO> vendorItemTypes = new ArrayList<GetReorderItemsResponse.ReorderVendorItemTypeDTO>();

        public ReorderItemDTO() {
        }

        /**
         * @param itemType
         */
        public ReorderItemDTO(ItemType itemType, int quantity) {
            this.itemTypeImageUrl = itemType.getImageUrl();
            this.itemTypePageUrl = itemType.getProductPageUrl();
            this.name = itemType.getName();
            this.skuCode = itemType.getSkuCode();
            this.waitingQuantity = quantity;
            this.minOrderSize = itemType.getMinOrderSize();
        }
        
        

        /**
         * @param skuCode
         * @param name
         * @param itemTypeImageUrl
         * @param itemTypePageUrl
         * @param waitingQuantity
         * @param minOrderSize
         * @param reorderQuantity
         * @param vendorItemTypes
         */
        public ReorderItemDTO(String itemName, String itemSku, int minOrderSize, int quantity, int reorderQuantity) {
            this.name = itemName;
            this.skuCode = itemSku;
            this.waitingQuantity = quantity;
            this.minOrderSize = minOrderSize;
            this.reorderQuantity = reorderQuantity;
        }

        /**
         * @return the skuCode
         */
        public String getSkuCode() {
            return skuCode;
        }

        /**
         * @param skuCode the skuCode to set
         */
        public void setSkuCode(String skuCode) {
            this.skuCode = skuCode;
        }

        /**
         * @return the name
         */
        public String getName() {
            return name;
        }

        /**
         * @param name the name to set
         */
        public void setName(String name) {
            this.name = name;
        }

        /**
         * @return the itemTypeImageUrl
         */
        public String getItemTypeImageUrl() {
            return itemTypeImageUrl;
        }

        /**
         * @param itemTypeImageUrl the itemTypeImageUrl to set
         */
        public void setItemTypeImageUrl(String itemTypeImageUrl) {
            this.itemTypeImageUrl = itemTypeImageUrl;
        }

        /**
         * @return the itemTypePageUrl
         */
        public String getItemTypePageUrl() {
            return itemTypePageUrl;
        }

        /**
         * @param itemTypePageUrl the itemTypePageUrl to set
         */
        public void setItemTypePageUrl(String itemTypePageUrl) {
            this.itemTypePageUrl = itemTypePageUrl;
        }

        /**
         * @return the waitingQuantity
         */
        public int getWaitingQuantity() {
            return waitingQuantity;
        }

        /**
         * @param waitingQuantity the waitingQuantity to set
         */
        public void setWaitingQuantity(int waitingQuantity) {
            this.waitingQuantity = waitingQuantity;
        }

        /**
         * @return the minOrderSize
         */
        public int getMinOrderSize() {
            return minOrderSize;
        }

        /**
         * @param minOrderSize the minOrderSize to set
         */
        public void setMinOrderSize(int minOrderSize) {
            this.minOrderSize = minOrderSize;
        }

        /**
         * @return the vendorItemTypes
         */
        public List<ReorderVendorItemTypeDTO> getVendorItemTypes() {
            return vendorItemTypes;
        }

        /**
         * @param vendorItemTypes the vendorItemTypes to set
         */
        public void setVendorItemTypes(List<ReorderVendorItemTypeDTO> vendorItemTypes) {
            this.vendorItemTypes = vendorItemTypes;
        }

        /**
         * @return the reorderQuantity
         */
        public int getReorderQuantity() {
            return reorderQuantity;
        }

        /**
         * @param reorderQuantity the reorderQuantity to set
         */
        public void setReorderQuantity(int reorderQuantity) {
            this.reorderQuantity = reorderQuantity;
        }
        
    }

    public static class ReorderVendorItemTypeDTO {

        private Integer    vendorId;
        private String     vendorCode;
        private String     vendorName;
        private BigDecimal unitPrice;

        /**
         * @return the vendorId
         */
        public Integer getVendorId() {
            return vendorId;
        }

        /**
         * @param vendorId the vendorId to set
         */
        public void setVendorId(Integer vendorId) {
            this.vendorId = vendorId;
        }

        /**
         * @return the vendorCode
         */
        public String getVendorCode() {
            return vendorCode;
        }

        /**
         * @param vendorCode the vendorCode to set
         */
        public void setVendorCode(String vendorCode) {
            this.vendorCode = vendorCode;
        }

        /**
         * @return the vendorName
         */
        public String getVendorName() {
            return vendorName;
        }

        /**
         * @param vendorName the vendorName to set
         */
        public void setVendorName(String vendorName) {
            this.vendorName = vendorName;
        }

        /**
         * @return the unitPrice
         */
        public BigDecimal getUnitPrice() {
            return unitPrice;
        }

        /**
         * @param unitPrice the unitPrice to set
         */
        public void setUnitPrice(BigDecimal unitPrice) {
            this.unitPrice = unitPrice;
        }

    }

}
