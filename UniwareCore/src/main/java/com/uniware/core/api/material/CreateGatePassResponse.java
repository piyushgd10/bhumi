/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jun 30, 2012
 *  @author singla
 */
package com.uniware.core.api.material;

import com.unifier.core.api.base.ServiceResponse;

public class CreateGatePassResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 1238808324906830823L;

    private String            gatePassCode;

    /**
     * @return the gatePassCode
     */
    public String getGatePassCode() {
        return gatePassCode;
    }

    /**
     * @param gatePassCode the gatePassCode to set
     */
    public void setGatePassCode(String gatePassCode) {
        this.gatePassCode = gatePassCode;
    }
}
