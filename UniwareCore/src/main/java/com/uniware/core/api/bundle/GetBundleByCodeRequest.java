/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 02-Aug-2013
 *  @author pankaj
 */
package com.uniware.core.api.bundle;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

public class GetBundleByCodeRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 7355980441390627880L;

    @NotBlank
    private String            code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

}
