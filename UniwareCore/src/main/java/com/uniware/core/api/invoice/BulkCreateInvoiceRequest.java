/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 5, 2013
 *  @author karunsingla
 */
package com.uniware.core.api.invoice;

import com.unifier.core.api.base.ServiceRequest;

import java.util.List;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

public class BulkCreateInvoiceRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -7872723106807427973L;

    @NotEmpty
    private List<String>      shippingPackageCodes;

    @NotNull
    private Integer           userId;

    public List<String> getShippingPackageCodes() {
        return shippingPackageCodes;
    }

    public void setShippingPackageCodes(List<String> shippingPackageCodes) {
        this.shippingPackageCodes = shippingPackageCodes;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }
}
