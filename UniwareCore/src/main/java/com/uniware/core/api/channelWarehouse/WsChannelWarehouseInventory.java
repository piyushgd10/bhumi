package com.uniware.core.api.channelWarehouse;

import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * Created by digvijaysharma on 22/01/17.
 */
public class WsChannelWarehouseInventory {

    @NotEmpty
    private String sellerSkuCode;

    @NotNull
    private int               totalQuantity;

    @NotNull
    private int               reservedQuantity;

    @NotNull
    private int               inboundQuantity;

    @NotNull
    private int               fulfillableQuantity;

    @NotNull
    private int               unfulfillableQuantity;

    public String getSellerSkuCode() {
        return sellerSkuCode;
    }

    public void setSellerSkuCode(String sellerSkuCode) {
        this.sellerSkuCode = sellerSkuCode;
    }

    public int getTotalQuantity() {
        return totalQuantity;
    }

    public void setTotalQuantity(int totalQuantity) {
        this.totalQuantity = totalQuantity;
    }

    public int getReservedQuantity() {
        return reservedQuantity;
    }

    public void setReservedQuantity(int reservedQuantity) {
        this.reservedQuantity = reservedQuantity;
    }

    public int getInboundQuantity() {
        return inboundQuantity;
    }

    public void setInboundQuantity(int inboundQuantity) {
        this.inboundQuantity = inboundQuantity;
    }

    public int getFulfillableQuantity() {
        return fulfillableQuantity;
    }

    public void setFulfillableQuantity(int fulfillableQuantity) {
        this.fulfillableQuantity = fulfillableQuantity;
    }

    public int getUnfulfillableQuantity() {
        return unfulfillableQuantity;
    }

    public void setUnfulfillableQuantity(int unfulfillableQuantity) {
        this.unfulfillableQuantity = unfulfillableQuantity;
    }

    @Override
    public String toString() {
        return "ChannelWarehouseInventory : \n" +
                "sellerSkuCode = " + sellerSkuCode +
                "unfulfillableQuantity = " + unfulfillableQuantity +
                "fulfillableQuantity = " + fulfillableQuantity +
                "inboundQuantity = " + inboundQuantity +
                "reservedQuantity = " + reservedQuantity +
                "totalQuantity = " + totalQuantity;
    }
}
