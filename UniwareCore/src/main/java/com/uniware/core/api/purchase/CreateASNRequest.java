/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 10-May-2012
 *  @author praveeng
 */
package com.uniware.core.api.purchase;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import com.unifier.core.api.base.ServiceRequest;
import com.unifier.core.api.customfields.WsCustomFieldValue;

/**
 * @author praveeng
 */
public class CreateASNRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long        serialVersionUID = 3814235292331517377L;

    @NotNull
    private Date                     expectedDeliveryDate;

    @NotBlank
    private String                   purchaseOrderCode;

    @NotBlank
    private String                   vendorCode;

    @Valid
    @NotEmpty
    private List<WsASNItem>          asnItems         = new ArrayList<WsASNItem>();

    @Valid
    private List<WsCustomFieldValue> customFieldValues;

    /**
     * @return the expectedDeliveryDate
     */
    public Date getExpectedDeliveryDate() {
        return expectedDeliveryDate;
    }

    /**
     * @param expectedDeliveryDate the expectedDeliveryDate to set
     */
    public void setExpectedDeliveryDate(Date expectedDeliveryDate) {
        this.expectedDeliveryDate = expectedDeliveryDate;
    }

    /**
     * @return the purchaseOrderCode
     */
    public String getPurchaseOrderCode() {
        return purchaseOrderCode;
    }

    /**
     * @param purchaseOrderCode the purchaseOrderCode to set
     */
    public void setPurchaseOrderCode(String purchaseOrderCode) {
        this.purchaseOrderCode = purchaseOrderCode;
    }

    /**
     * @return the asnItems
     */
    public List<WsASNItem> getAsnItems() {
        return asnItems;
    }

    /**
     * @param asnItems the asnItems to set
     */
    public void setAsnItems(List<WsASNItem> asnItems) {
        this.asnItems = asnItems;
    }

    /**
     * @return the vendorCode
     */
    public String getVendorCode() {
        return vendorCode;
    }

    /**
     * @param vendorCode the vendorCode to set
     */
    public void setVendorCode(String vendorCode) {
        this.vendorCode = vendorCode;
    }

    public List<WsCustomFieldValue> getCustomFieldValues() {
        return customFieldValues;
    }

    public void setCustomFieldValues(List<WsCustomFieldValue> customFieldValues) {
        this.customFieldValues = customFieldValues;
    }

    public static class WsASNItem {

        @NotBlank
        private String  skuCode;

        @NotNull
        @Min(value = 1)
        private Integer quantity;

        public String getSkuCode() {
            return skuCode;
        }

        public void setSkuCode(String skuCode) {
            this.skuCode = skuCode;
        }

        /**
         * @return the quantity
         */
        public Integer getQuantity() {
            return quantity;
        }

        /**
         * @param quantity the quantity to set
         */
        public void setQuantity(Integer quantity) {
            this.quantity = quantity;
        }

    }

}
