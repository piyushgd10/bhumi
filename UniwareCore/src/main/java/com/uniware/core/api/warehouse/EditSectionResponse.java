/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 24-Dec-2011
 *  @author vibhu
 */
package com.uniware.core.api.warehouse;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author vibhu
 */
public class EditSectionResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 2620227928985499987L;
    private SectionDTO        sectionDTO;

    /**
     * @return the section
     */
    public SectionDTO getSectionDTO() {
        return sectionDTO;
    }

    /**
     * @param section the section to set
     */
    public void setSectionDTO(SectionDTO sectionDTO) {
        this.sectionDTO = sectionDTO;
    }

}