/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jun 6, 2012
 *  @author singla
 */
package com.uniware.core.api.purchase;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author singla
 */
public class RejectPurchaseOrderResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 3617572259272700677L;

}
