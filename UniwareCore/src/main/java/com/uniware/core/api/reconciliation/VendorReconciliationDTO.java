/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 11-Mar-2013
 *  @author pankaj
 */
package com.uniware.core.api.reconciliation;

import java.util.HashMap;
import java.util.Map;

/**
 * @author pankaj
 */
public class VendorReconciliationDTO {

    private Map<Integer, VendorReconciliationDataDTO> periodToDataMap = new HashMap<Integer, VendorReconciliationDataDTO>();

    /**
     * @return the periodToDataMap
     */
    public Map<Integer, VendorReconciliationDataDTO> getPeriodToDataMap() {
        return periodToDataMap;
    }

    /**
     * @param periodToDataMap the periodToDataMap to set
     */
    public void setPeriodToDataMap(Map<Integer, VendorReconciliationDataDTO> periodToDataMap) {
        this.periodToDataMap = periodToDataMap;
    }

    public static class VendorReconciliationDataDTO {
        private Integer itemsCount;
        private Double  payment;
        private Double  sellingIn;
        private Double  sellingOut;
        private Double  shippingIn;
        private Double  shippingOut;

        /**
         * @return the itemsCount
         */
        public Integer getItemsCount() {
            return itemsCount;
        }

        /**
         * @param itemsCount the itemsCount to set
         */
        public void setItemsCount(Integer itemsCount) {
            this.itemsCount = itemsCount;
        }

        /**
         * @return the payment
         */
        public Double getPayment() {
            return payment;
        }

        /**
         * @param payment the payment to set
         */
        public void setPayment(Double payment) {
            this.payment = payment;
        }

        /**
         * @return the sellingIn
         */
        public Double getSellingIn() {
            return sellingIn;
        }

        /**
         * @param sellingIn the sellingIn to set
         */
        public void setSellingIn(Double sellingIn) {
            this.sellingIn = sellingIn;
        }

        /**
         * @return the sellingOut
         */
        public Double getSellingOut() {
            return sellingOut;
        }

        /**
         * @param sellingOut the sellingOut to set
         */
        public void setSellingOut(Double sellingOut) {
            this.sellingOut = sellingOut;
        }

        /**
         * @return the shippingIn
         */
        public Double getShippingIn() {
            return shippingIn;
        }

        /**
         * @param shippingIn the shippingIn to set
         */
        public void setShippingIn(Double shippingIn) {
            this.shippingIn = shippingIn;
        }

        /**
         * @return the shippingOut
         */
        public Double getShippingOut() {
            return shippingOut;
        }

        /**
         * @param shippingOut the shippingOut to set
         */
        public void setShippingOut(Double shippingOut) {
            this.shippingOut = shippingOut;
        }

    }

}
