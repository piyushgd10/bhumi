/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 19, 2012
 *  @author praveeng
 */
package com.uniware.core.api.returns;

import com.uniware.core.api.saleorder.AddressDTO;

/**
 * @author praveeng
 */
public class PackageAwaitingActionDTO {
    private Integer    shippingPackageId;
    private String     shippingPackageCode;
    private String     saleOrderCode;
    private AddressDTO shippingAddress;
    private String     shippingPackageStatus;
    private String     trackingStatus;
    private String     providerStatus;
    private String     shippingMethod;
    private String     shippingProvider;
    private String     trackingNumber;

    /**
     * @return the shippingPackageId
     */
    public Integer getShippingPackageId() {
        return shippingPackageId;
    }

    /**
     * @param shippingPackageId the shippingPackageId to set
     */
    public void setShippingPackageId(Integer shippingPackageId) {
        this.shippingPackageId = shippingPackageId;
    }

    /**
     * @return the shippingPackageCode
     */
    public String getShippingPackageCode() {
        return shippingPackageCode;
    }

    /**
     * @param shippingPackageCode the shippingPackageCode to set
     */
    public void setShippingPackageCode(String shippingPackageCode) {
        this.shippingPackageCode = shippingPackageCode;
    }

    /**
     * @return the shippingAddress
     */
    public AddressDTO getShippingAddress() {
        return shippingAddress;
    }

    /**
     * @param shippingAddress the shippingAddress to set
     */
    public void setShippingAddress(AddressDTO shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    /**
     * @return the shippingPackageStatus
     */
    public String getShippingPackageStatus() {
        return shippingPackageStatus;
    }

    /**
     * @param shippingPackageStatus the shippingPackageStatus to set
     */
    public void setShippingPackageStatus(String shippingPackageStatus) {
        this.shippingPackageStatus = shippingPackageStatus;
    }

    /**
     * @return the trackingStatus
     */
    public String getTrackingStatus() {
        return trackingStatus;
    }

    /**
     * @param trackingStatus the trackingStatus to set
     */
    public void setTrackingStatus(String trackingStatus) {
        this.trackingStatus = trackingStatus;
    }

    /**
     * @return the providerStatus
     */
    public String getProviderStatus() {
        return providerStatus;
    }

    /**
     * @param providerStatus the providerStatus to set
     */
    public void setProviderStatus(String providerStatus) {
        this.providerStatus = providerStatus;
    }

    /**
     * @return the shippingMethod
     */
    public String getShippingMethod() {
        return shippingMethod;
    }

    /**
     * @param shippingMethod the shippingMethod to set
     */
    public void setShippingMethod(String shippingMethod) {
        this.shippingMethod = shippingMethod;
    }

    /**
     * @return the trackingNumber
     */
    public String getTrackingNumber() {
        return trackingNumber;
    }

    /**
     * @param trackingNumber the trackingNumber to set
     */
    public void setTrackingNumber(String trackingNumber) {
        this.trackingNumber = trackingNumber;
    }

    /**
     * @return the shippingProvider
     */
    public String getShippingProvider() {
        return shippingProvider;
    }

    /**
     * @param shippingProvider the shippingProvider to set
     */
    public void setShippingProvider(String shippingProvider) {
        this.shippingProvider = shippingProvider;
    }

    /**
     * @return the saleOrderCode
     */
    public String getSaleOrderCode() {
        return saleOrderCode;
    }

    /**
     * @param saleOrderCode the saleOrderCode to set
     */
    public void setSaleOrderCode(String saleOrderCode) {
        this.saleOrderCode = saleOrderCode;
    }

}
