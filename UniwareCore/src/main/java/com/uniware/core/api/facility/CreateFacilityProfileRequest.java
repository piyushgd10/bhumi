/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 14-May-2015
 *  @author parijat
 */
package com.uniware.core.api.facility;

import javax.validation.Valid;

import com.unifier.core.api.base.ServiceRequest;
import com.uniware.core.api.party.WsFacilityProfile;

public class CreateFacilityProfileRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 45789876789878L;

    @Valid
    private WsFacilityProfile facilityProfile;

    public WsFacilityProfile getFacilityProfile() {
        return facilityProfile;
    }

    public void setFacilityProfile(WsFacilityProfile facilityProfile) {
        this.facilityProfile = facilityProfile;
    }

}
