/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jul 29, 2012
 *  @author singla
 */
package com.uniware.core.api.saleorder;

import com.unifier.core.api.base.ServiceResponse;

import java.util.ArrayList;
import java.util.List;

public class CreateSaleOrderItemAlternateResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long  serialVersionUID             = -7050113297713345338L;

    private final List<String> successfulSaleOrderItemCodes = new ArrayList<String>();

    /**
     * @return the successfulSaleOrderItemCodes
     */
    public List<String> getSuccessfulSaleOrderItemCodes() {
        return successfulSaleOrderItemCodes;
    }
}
