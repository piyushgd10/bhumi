/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jul 9, 2012
 *  @author singla
 */
package com.uniware.core.api.jabong;

import com.unifier.core.api.base.ServiceResponse;

public class JabongWholesaleHandleShipmentReturnResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -7383829179000331242L;

}
