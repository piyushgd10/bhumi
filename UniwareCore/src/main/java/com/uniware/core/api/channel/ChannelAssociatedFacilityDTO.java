/*
 * Copyright 2017 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 19/12/17
 * @author piyush
 */
package com.uniware.core.api.channel;

import com.uniware.core.entity.Facility;

import java.io.Serializable;

public class ChannelAssociatedFacilityDTO implements Serializable {

    private Integer id;
    private String  code;
    private String  name;

    public ChannelAssociatedFacilityDTO() {
    }

    public ChannelAssociatedFacilityDTO(Facility facility) {
        this.id = facility.getId();
        this.code = facility.getCode();
        this.name = facility.getDisplayName();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
