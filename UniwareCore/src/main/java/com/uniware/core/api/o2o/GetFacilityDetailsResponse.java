package com.uniware.core.api.o2o;

import java.util.List;

import com.unifier.core.api.base.ServiceResponse;
import com.uniware.core.entity.Facility;
import com.uniware.core.entity.PartyAddress;

/**
 * Created by sunny on 09/04/15.
 */
public class GetFacilityDetailsResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long       serialVersionUID = 7741948787105703348L;

    private List<FacilityDetailDTO> facilities;

    public List<FacilityDetailDTO> getFacilities() {
        return facilities;
    }

    public void setFacilities(List<FacilityDetailDTO> facilities) {
        this.facilities = facilities;
    }

    public static class StoreTiming {

        private List<DayTiming> dayTimings;

        public List<DayTiming> getDayTimings() {
            return dayTimings;
        }

        public void setDayTimings(List<DayTiming> dayTimings) {
            this.dayTimings = dayTimings;
        }
    }

    public static class DayTiming {
        public enum DayOfWeek {
            MONDAY,
            TUESDAY,
            WEDNESDAY,
            THURSDAY,
            FRIDAY,
            SATURDAY,
            SUNDAY
        }

        private DayOfWeek dayOfWeek;
        private boolean   closed;
        private String    openingTime;
        private String    closingTime;

        public DayTiming() {
        }

        public DayTiming(DayOfWeek dayOfWeek, boolean closed, String openingTime, String closingTime) {
            this.dayOfWeek = dayOfWeek;
            this.closed = closed;
            this.openingTime = openingTime;
            this.closingTime = closingTime;
        }

        public DayOfWeek getDayOfWeek() {
            return dayOfWeek;
        }

        public void setDayOfWeek(DayOfWeek dayOfWeek) {
            this.dayOfWeek = dayOfWeek;
        }

        public boolean isClosed() {
            return closed;
        }

        public void setClosed(boolean closed) {
            this.closed = closed;
        }

        public String getOpeningTime() {
            return openingTime;
        }

        public void setOpeningTime(String openingTime) {
            this.openingTime = openingTime;
        }

        public String getClosingTime() {
            return closingTime;
        }

        public void setClosingTime(String closingTime) {
            this.closingTime = closingTime;
        }
    }

    public static class Location {

        private Double longitude;

        private Double latitude;

        public Location() {
        }

        public Location(Double longitude, Double latitude) {
            this.longitude = longitude;
            this.latitude = latitude;
        }

        public double getLongitude() {
            return longitude;
        }

        public void setLongitude(double longitude) {
            this.longitude = longitude;
        }

        public double getLatitude() {
            return latitude;
        }

        public void setLatitude(double latitude) {
            this.latitude = latitude;
        }
    }

    public static class AddressDTO {

        private String name;
        private String addressLine1;
        private String addressLine2;
        private String city;
        private String state;
        private String country;
        private String pincode;
        private String phone;

        public AddressDTO() {
        }

        public AddressDTO(PartyAddress partyAddress) {
            if (partyAddress != null) {
                this.name = partyAddress.getParty().getName();
                this.state = partyAddress.getStateCode();
                this.country = partyAddress.getCountryCode();
                this.addressLine1 = partyAddress.getAddressLine1();
                this.addressLine2 = partyAddress.getAddressLine2();
                this.city = partyAddress.getCity();
                this.pincode = partyAddress.getPincode();
                this.phone = partyAddress.getPhone();
            }
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getAddressLine1() {
            return addressLine1;
        }

        public void setAddressLine1(String addressLine1) {
            this.addressLine1 = addressLine1;
        }

        public String getAddressLine2() {
            return addressLine2;
        }

        public void setAddressLine2(String addressLine2) {
            this.addressLine2 = addressLine2;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getPincode() {
            return pincode;
        }

        public void setPincode(String pincode) {
            this.pincode = pincode;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

    }

    public static class FacilityDetailDTO {
        private String        code;
        private Facility.Type type;
        private String        title;
        private String        shortAddress;
        private AddressDTO    address;
        private String        email;
        private StoreTiming   storeTiming;
        private Location      location;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public Facility.Type getType() {
            return type;
        }

        public void setType(Facility.Type type) {
            this.type = type;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getShortAddress() {
            return shortAddress;
        }

        public void setShortAddress(String shortAddress) {
            this.shortAddress = shortAddress;
        }

        public AddressDTO getAddress() {
            return address;
        }

        public void setAddress(AddressDTO address) {
            this.address = address;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public StoreTiming getStoreTiming() {
            return storeTiming;
        }

        public void setStoreTiming(StoreTiming storeTiming) {
            this.storeTiming = storeTiming;
        }

        public Location getLocation() {
            return location;
        }

        public void setLocation(Location location) {
            this.location = location;
        }

    }

}