/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 15, 2011
 *  @author singla
 */
package com.uniware.core.api.saleorder;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author singla
 */
public class CreateSaleOrderResponse extends ServiceResponse {
    /**
     * 
     */
    private static final long serialVersionUID = 4135666044788133318L;

    private SaleOrderDetailDTO saleOrderDetailDTO;

    public SaleOrderDetailDTO getSaleOrderDetailDTO() {
        return saleOrderDetailDTO;
    }

    public void setSaleOrderDetailDTO(SaleOrderDetailDTO saleOrderDetailDTO) {
        this.saleOrderDetailDTO = saleOrderDetailDTO;
    }

    @Override
    public String toString() {
        return "CreateSaleOrderResponse [isSuccessful()=" + isSuccessful() + ", getMessage()=" + getMessage() + ", getErrors()=" + getErrors() + ", getWarnings()=" + getWarnings()
                + "]";
    }

    public static class ChannelItemTypeDTO {
        public enum Status {
            MISSING,
            UNLINKED,
            UNVERIFIED,
            IGNORED
        }

        private String channelProductId;
        private String sellerSkuCode;
        private String productName;
        private Status statusCode;

        public ChannelItemTypeDTO() {
            super();
        }

        public ChannelItemTypeDTO(String channelProductId, String sellerSkuCode, String productName) {
            super();
            this.channelProductId = channelProductId;
            this.sellerSkuCode = sellerSkuCode;
            this.productName = productName;
        }

        public String getChannelProductId() {
            return channelProductId;
        }

        public void setChannelProductId(String channelProductId) {
            this.channelProductId = channelProductId;
        }

        public String getSellerSkuCode() {
            return sellerSkuCode;
        }

        public void setSellerSkuCode(String sellerSkuCode) {
            this.sellerSkuCode = sellerSkuCode;
        }

        public String getProductName() {
            return productName;
        }

        public void setProductName(String productName) {
            this.productName = productName;
        }

        public Status getStatusCode() {
            return statusCode;
        }

        public void setStatusCode(Status status) {
            this.statusCode = status;
        }

    }
}
