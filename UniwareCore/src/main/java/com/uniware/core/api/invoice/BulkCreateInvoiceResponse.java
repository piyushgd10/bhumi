/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 5, 2013
 *  @author karunsingla
 */
package com.uniware.core.api.invoice;

import com.unifier.core.api.base.ServiceResponse;
import com.unifier.core.api.validation.WsError;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class BulkCreateInvoiceResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long          serialVersionUID   = 2022171938309459838L;

    private Map<String, List<WsError>> failures           = new HashMap<String, List<WsError>>();

    private Set<String>                successfulPackages = new HashSet<String>();

    public Set<String> getSuccessfulPackages() {
        return successfulPackages;
    }

    public Map<String, List<WsError>> getFailures() {
        return failures;
    }

}
