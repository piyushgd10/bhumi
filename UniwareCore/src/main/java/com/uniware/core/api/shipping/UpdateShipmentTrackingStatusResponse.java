/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 15-Jul-2013
 *  @author unicom
 */
package com.uniware.core.api.shipping;

import com.unifier.core.api.base.ServiceResponse;

public class UpdateShipmentTrackingStatusResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 9189889317662747874L;

}
