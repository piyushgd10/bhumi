/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 15, 2011
 *  @author singla
 */
package com.uniware.core.api.saleorder;

import java.util.Date;
import java.util.List;

import com.unifier.core.api.base.ServiceRequest;
import com.unifier.core.pagination.SearchOptions;

/**
 * @author singla
 */
public class SearchSaleOrderRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 3148768119801793617L;
    private String            displayOrderCode;
    private String            status;
    private String            channel;
    private String            customerEmailOrMobile;
    private String            customerName;
    private Boolean           cashOnDelivery;
    private Date              fromDate;
    private Date              toDate;
    private List<String>      facilityCodes;
    private SearchOptions     searchOptions;
    private Integer           updatedSinceInMinutes;
    private Boolean           onHold;

    /**
     * @return the customerEmailOrMobile
     */
    public String getCustomerEmailOrMobile() {
        return customerEmailOrMobile;
    }

    /**
     * @param customerEmailOrMobile the customerEmailOrMobile to set
     */
    public void setCustomerEmailOrMobile(String customerEmailOrMobile) {
        this.customerEmailOrMobile = customerEmailOrMobile;
    }

    /**
     * @return the cashOnDelivery
     */
    public Boolean getCashOnDelivery() {
        return cashOnDelivery;
    }

    /**
     * @param cashOnDelivery the cashOnDelivery to set
     */
    public void setCashOnDelivery(Boolean cashOnDelivery) {
        this.cashOnDelivery = cashOnDelivery;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the fromDate
     */
    public Date getFromDate() {
        return fromDate;
    }

    /**
     * @param fromDate the fromDate to set
     */
    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    /**
     * @return the toDate
     */
    public Date getToDate() {
        return toDate;
    }

    /**
     * @param toDate the toDate to set
     */
    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    /**
     * @return the searchOptions
     */
    public SearchOptions getSearchOptions() {
        return searchOptions;
    }

    /**
     * @param searchOptions the searchOptions to set
     */
    public void setSearchOptions(SearchOptions searchOptions) {
        this.searchOptions = searchOptions;
    }

    /**
     * @return the saleOrderNumber
     */
    public String getDisplayOrderCode() {
        return displayOrderCode;
    }

    /**
     * @param saleOrderNumber the saleOrderNumber to set
     */
    public void setDisplayOrderCode(String saleOrderNumber) {
        this.displayOrderCode = saleOrderNumber;
    }

    /**
     * @return the customerName
     */
    public String getCustomerName() {
        return customerName;
    }

    /**
     * @param customerName the customerName to set
     */
    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    /**
     * @return the facilityCodes
     */
    public List<String> getFacilityCodes() {
        return facilityCodes;
    }

    /**
     * @param facilityCodes the facilityCodes to set
     */
    public void setFacilityCodes(List<String> facilityCodes) {
        this.facilityCodes = facilityCodes;
    }

    /**
     * @return the channel
     */
    public String getChannel() {
        return channel;
    }

    /**
     * @param channel the channel to set
     */
    public void setChannel(String channel) {
        this.channel = channel;
    }

    public Integer getUpdatedSinceInMinutes() {
        return updatedSinceInMinutes;
    }

    public void setUpdatedSinceInMinutes(Integer updatedSinceInMinutes) {
        this.updatedSinceInMinutes = updatedSinceInMinutes;
    }
    
    public Boolean getOnHold() {
        return onHold;
    }

    public void setOnHold(Boolean onHold) {
        this.onHold = onHold;
    }
    
}
