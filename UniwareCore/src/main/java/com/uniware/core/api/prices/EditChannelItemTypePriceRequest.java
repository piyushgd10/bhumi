/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Oct 20, 2015
 *  @author akshay
 */
package com.uniware.core.api.prices;

import java.math.BigDecimal;

import javax.validation.constraints.Digits;

import org.hibernate.validator.constraints.NotBlank;

public class EditChannelItemTypePriceRequest extends AbstractChannelItemTypeRequest {

    private static final long serialVersionUID = -8139979867842827991L;

    @Digits(integer = 10, fraction = 2 , message = "max value cannot have more than 10 digits and 2 decimal places")
    private BigDecimal        msp;
    @Digits(integer = 10, fraction = 2, message = "max value cannot have more than 10 digits and 2 decimal places")
    private BigDecimal        mrp;
    @Digits(integer = 10, fraction = 2 ,message = "max value cannot have more than 10 digits and 2 decimal places")
    private BigDecimal        sellingPrice;
    @NotBlank
    private String            currencyCode;
    
    private boolean           isForceEdit;
    
    private boolean           forceMarkDirty;

    /**
     * True if we should ignore pending price edits.
     */
    public boolean isForceEdit() {
        return isForceEdit;
    }

    public void setIsForceEdit(boolean isForceEdit) {
        this.isForceEdit = isForceEdit;
    }

    /**
     * True of we should force mark the price as dirty even when there are no
     * price changes as part of the request.
     * <p>
     * This flag does not imply {@code isForceEdit}. i.e. in case there are pending
     * price updates and this flag is true but {@code isForceEdit} is false, no
     * changes will be made.
     */
    public boolean isForceMarkDirty() {
        return forceMarkDirty;
    }

    public void setForceMarkDirty(boolean forceMarkDirty) {
        this.forceMarkDirty = forceMarkDirty;
    }

    public BigDecimal getSellingPrice() {
        return sellingPrice;
    }

    public void setSellingPrice(BigDecimal sellingPrice) {
        this.sellingPrice = sellingPrice;
    }

    public BigDecimal getMsp() {
        return msp;
    }

    public void setMsp(BigDecimal msp) {
        this.msp = msp;
    }

    public BigDecimal getMrp() {
        return mrp;
    }

    public void setMrp(BigDecimal mrp) {
        this.mrp = mrp;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }
}
