package com.uniware.core.api.channelWarehouse;

import com.unifier.core.api.base.ServiceRequest;
import java.util.List;
import javax.validation.Valid;

/**
 * Created by digvijaysharma on 23/01/17.
 */
public class CreateChannelWarehouseInventoryRequest extends ServiceRequest {

    /**
     *
     */
    private static final long serialVersionUID = 1963404117545473225L;

    @Valid private List<WsChannelWarehouseInventory> channelWarehouseInventories;

    private int channelId;

    private Integer totalPages;

    public List<WsChannelWarehouseInventory> getChannelWarehouseInventories() {
        return channelWarehouseInventories;
    }

    public void setChannelWarehouseInventories(List<WsChannelWarehouseInventory> channelWarehouseInventories) {
        this.channelWarehouseInventories = channelWarehouseInventories;
    }

    public Integer getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(Integer totalPages) {
        this.totalPages = totalPages;
    }

    public int getChannelId() {
        return channelId;
    }

    public void setChannelId(int channelId) {
        this.channelId = channelId;
    }
}
