/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jan 10, 2012
 *  @author singla
 */
package com.uniware.core.api.saleorder.display;

import java.util.List;

import com.unifier.core.api.base.ServiceResponse;
import com.uniware.core.api.invoice.InvoiceDTO;

public class GetSaleOrderInvoicesResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 6284414547113409023L;

    public GetSaleOrderInvoicesResponse() {
    }

    private List<InvoiceDTO> invoices;

    public List<InvoiceDTO> getInvoices() {
        return invoices;
    }

    public void setInvoices(List<InvoiceDTO> invoices) {
        this.invoices = invoices;
    }

}
