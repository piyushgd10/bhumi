package com.uniware.core.api.returns;

import com.unifier.core.api.base.ServiceResponse;
import com.uniware.core.api.invoice.InvoiceDTO;

/**
 * Created by Sagar Sahni on 21/06/17.
 */
public class GetReversePickupInvoiceResponse extends ServiceResponse {

    private InvoiceDTO invoice;

    public InvoiceDTO getInvoice() {
        return invoice;
    }

    public void setInvoice(InvoiceDTO invoice) {
        this.invoice = invoice;
    }
}
