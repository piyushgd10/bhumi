/*
 * Copyright 2017 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 23/5/17 4:24 PM
 * @author digvijaysharma
 */

package com.uniware.core.api.tenant;

import com.unifier.core.api.base.ServiceResponse;

/**
 * Created by digvijaysharma on 23/05/17.
 */
public class StartTraceLogSessionResponse extends ServiceResponse {

    private static final long serialVersionUID = 6734820228159956606L;

    private String            loggingServerUrl;

    public String getLoggingServerUrl() {
        return loggingServerUrl;
    }

    public void setLoggingServerUrl(String loggingServerUrl) {
        this.loggingServerUrl = loggingServerUrl;
    }
}
