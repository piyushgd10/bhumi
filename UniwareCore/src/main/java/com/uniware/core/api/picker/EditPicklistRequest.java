/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jan 11, 2012
 *  @author singla
 */
package com.uniware.core.api.picker;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author singla
 */
public class EditPicklistRequest extends ServiceRequest {

    public enum Action {
        INVENTORY_NOT_FOUND,
        INVENTORY_DAMAGED,
        EXPIRED_ITEM,
        ABOUT_TO_EXPIRE
    }

    /**
     *
     */
    private static final long        serialVersionUID = 8363317680192759977L;

    @NotBlank
    private String                   picklistCode;

    @NotEmpty
    @Valid
    private List<WsEditPicklistItem> picklistItems;

    /**
     * @return the picklistCode
     */
    public String getPicklistCode() {
        return picklistCode;
    }

    /**
     * @param picklistCode the picklistCode to set
     */
    public void setPicklistCode(String picklistCode) {
        this.picklistCode = picklistCode;
    }

    public List<WsEditPicklistItem> getPicklistItems() {
        return picklistItems;
    }

    public void setPicklistItems(List<WsEditPicklistItem> picklistItems) {
        this.picklistItems = picklistItems;
    }

    public static class WsEditPicklistItem {

        @NotBlank
        private String saleOrderItemCode;

        @NotBlank
        private String saleOrderCode;

        @NotNull
        private Action action = Action.INVENTORY_NOT_FOUND;

        private String itemCode;

        private String rejectionReason;

        /**
         * @return the action
         */
        public Action getAction() {
            return action;
        }

        /**
         * @param action the action to set
         */
        public void setAction(Action action) {
            this.action = action;
        }

        /**
         * @return the itemCode
         */
        public String getItemCode() {
            return itemCode;
        }

        /**
         * @param itemCode the itemCode to set
         */
        public void setItemCode(String itemCode) {
            this.itemCode = itemCode;
        }

        public String getRejectionReason() {
            return rejectionReason;
        }

        public void setRejectionReason(String rejectionReason) {
            this.rejectionReason = rejectionReason;
        }

        public String getSaleOrderCode() {
            return saleOrderCode;
        }

        public void setSaleOrderCode(String saleOrderCode) {
            this.saleOrderCode = saleOrderCode;
        }

        public String getSaleOrderItemCode() {
            return saleOrderItemCode;
        }

        public void setSaleOrderItemCode(String saleOrderItemCode) {
            this.saleOrderItemCode = saleOrderItemCode;
        }
    }
}
