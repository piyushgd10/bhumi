/*
 * Copyright 2017 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 27/09/17
 * @author piyush
 */
package com.uniware.core.api.item;

import com.unifier.core.api.base.ServiceResponse;
import com.uniware.core.api.putaway.PutawayItemDTO;

import java.util.ArrayList;
import java.util.List;

public class AddNonTraceablePicklistItemsToPutawayResponse extends ServiceResponse{

    private List<PutawayItemDTO> putawayItems = new ArrayList<PutawayItemDTO>();

    public List<PutawayItemDTO> getPutawayItems() {
        return putawayItems;
    }

    public void setPutawayItems(List<PutawayItemDTO> putawayItems) {
        this.putawayItems = putawayItems;
    }
}
