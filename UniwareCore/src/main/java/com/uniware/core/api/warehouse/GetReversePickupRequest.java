/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 20, 2012
 *  @author praveeng
 */
package com.uniware.core.api.warehouse;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author praveeng
 */
public class GetReversePickupRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 3953990476172383363L;

    private String            trackingNumber;
    private String            reversePickupCode;

    /**
     * @return the trackingNumber
     */
    public String getTrackingNumber() {
        return trackingNumber;
    }

    /**
     * @param trackingNumber the trackingNumber to set
     */
    public void setTrackingNumber(String trackingNumber) {
        this.trackingNumber = trackingNumber;
    }

    /**
     * @return the reversePickupCode
     */
    public String getReversePickupCode() {
        return reversePickupCode;
    }

    /**
     * @param reversePickupCode the reversePickupCode to set
     */
    public void setReversePickupCode(String reversePickupCode) {
        this.reversePickupCode = reversePickupCode;
    }

}
