/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 03-Jun-2012
 *  @author vibhu
 */
package com.uniware.core.api.channel;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author Sunny
 */
public class EditChannelRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 8834052525577513129L;
    @NotNull
    @Valid
    private WsChannel         wsChannel;

    public WsChannel getWsChannel() {
        return wsChannel;
    }

    public void setWsChannel(WsChannel wsChannel) {
        this.wsChannel = wsChannel;
    }

}
