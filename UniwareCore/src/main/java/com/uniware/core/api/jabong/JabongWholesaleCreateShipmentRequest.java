/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 10-Dec-2013
 *  @author karunsingla
 */
package com.uniware.core.api.jabong;

import com.unifier.core.api.base.ServiceRequest;

import java.util.List;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

public class JabongWholesaleCreateShipmentRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -1948161630980609487L;

    @NotBlank
    private String            saleOrderCode;

    @NotEmpty
    private List<String>      saleOrderItemCodes;

    private boolean           doNotCreateInvoice;

    public String getSaleOrderCode() {
        return saleOrderCode;
    }

    public void setSaleOrderCode(String saleOrderCode) {
        this.saleOrderCode = saleOrderCode;
    }

    public List<String> getSaleOrderItemCodes() {
        return saleOrderItemCodes;
    }

    public void setSaleOrderItemCodes(List<String> saleOrderItemCodes) {
        this.saleOrderItemCodes = saleOrderItemCodes;
    }

    public boolean isDoNotCreateInvoice() {
        return doNotCreateInvoice;
    }

    public void setDoNotCreateInvoice(boolean doNotCreateInvoice) {
        this.doNotCreateInvoice = doNotCreateInvoice;
    }
}
