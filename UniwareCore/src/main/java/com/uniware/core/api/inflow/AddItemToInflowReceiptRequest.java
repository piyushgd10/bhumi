/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jun 11, 2012
 *  @author singla
 */
package com.uniware.core.api.inflow;

import java.util.Date;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author singla
 */
public class AddItemToInflowReceiptRequest extends ServiceRequest {
    /**
     * 
     */
    private static final long serialVersionUID = 2723452510230323967L;

    @NotNull
    private String            inflowReceiptCode;

    @NotBlank
    @Pattern(regexp = "^[a-zA-Z0-9-_]+$", message = "Item Code can only contain alphanumeric, _ and -")
    private String            itemCode;

    @Past
    private Date              manufacturingDate;

    /**
     * @return the itemCode
     */
    public String getItemCode() {
        return itemCode;
    }

    /**
     * @param itemCode the itemCode to set
     */
    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    /**
     * @return the inflowReceiptCode
     */
    public String getInflowReceiptCode() {
        return inflowReceiptCode;
    }

    /**
     * @param inflowReceiptCode the inflowReceiptCode to set
     */
    public void setInflowReceiptCode(String inflowReceiptCode) {
        this.inflowReceiptCode = inflowReceiptCode;
    }

    public Date getManufacturingDate() {
        return this.manufacturingDate;
    }

    public void setManufacturingDate(Date manufacturingDate) {
        this.manufacturingDate = manufacturingDate;
    }
}
