package com.uniware.core.api.shipping;

import com.unifier.core.annotation.constraints.Mobile;
import com.unifier.core.api.base.ServiceRequest;
import org.hibernate.validator.constraints.NotBlank;

/**
 * @author ankurpratik on 26/04/17.
 */
public class AddSignatureToShippingManifestRequest extends ServiceRequest {
    private static final long serialVersionUID = -7555153935489644246L;

    @NotBlank
    private String            shippingManifestCode;

    @NotBlank
    private String            signatureImage;

    private String            signeeName;

    @Mobile
    private String            signeeMobile;

    public String getShippingManifestCode() {
        return shippingManifestCode;
    }

    public void setShippingManifestCode(String shippingManifestCode) {
        this.shippingManifestCode = shippingManifestCode;
    }

    public String getSignatureImage() {
        return signatureImage;
    }

    public void setSignatureImage(String signatureImage) {
        this.signatureImage = signatureImage;
    }

    public String getSigneeName() {
        return signeeName;
    }

    public void setSigneeName(String signeeName) {
        this.signeeName = signeeName;
    }

    public String getSigneeMobile() {
        return signeeMobile;
    }

    public void setSigneeMobile(String signeeMobile) {
        this.signeeMobile = signeeMobile;
    }
}
