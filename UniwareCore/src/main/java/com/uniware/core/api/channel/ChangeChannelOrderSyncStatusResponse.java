/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 05-Sep-2013
 *  @author parijat
 */
package com.uniware.core.api.channel;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author Sunny
 */
public class ChangeChannelOrderSyncStatusResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -2387463132093011513L;

}
