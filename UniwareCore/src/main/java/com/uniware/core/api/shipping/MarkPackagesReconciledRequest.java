/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 09-Feb-2013
 *  @author praveeng
 */
package com.uniware.core.api.shipping;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.Size;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author praveeng
 */
public class MarkPackagesReconciledRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID     = 6366082956083060464L;

    @Size(min = 1)
    private List<String>      shippingPackageCodes = new ArrayList<String>();

    /**
     * @return the shippingPackageCodes
     */
    public List<String> getShippingPackageCodes() {
        return shippingPackageCodes;
    }

    /**
     * @param shippingPackageCodes the shippingPackageCodes to set
     */
    public void setShippingPackageCodes(List<String> shippingPackageCodes) {
        this.shippingPackageCodes = shippingPackageCodes;
    }

}
