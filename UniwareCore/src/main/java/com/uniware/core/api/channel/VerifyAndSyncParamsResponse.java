package com.uniware.core.api.channel;

import com.unifier.core.api.base.ServiceResponse;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class VerifyAndSyncParamsResponse extends ServiceResponse {
    private static final long                serialVersionUID = 6346878604896511519L;
    private              Map<String, String> persistentParams = new HashMap<>();
    private              Map<String, String> transientParams  = new HashMap<>();
    private List<WsAuthenticationChallenge> challenges;

    public Map<String, String> getPersistentParams() {
        return persistentParams;
    }

    public void setPersistentParams(Map<String, String> persistentParams) {
        this.persistentParams = persistentParams;
    }

    public Map<String, String> getTransientParams() {
        return transientParams;
    }

    public void setTransientParams(Map<String, String> transientParams) {
        this.transientParams = transientParams;
    }

    public List<WsAuthenticationChallenge> getChallenges() {
        return challenges;
    }

    public void setChallenges(List<WsAuthenticationChallenge> challenges) {
        this.challenges = challenges;
    }

    public enum ChallengeType {
        CAPTCHA,
        QUESTION
    }

    public static class WsAuthenticationChallenge {
        private String        code;
        private String        displayText;
        private ChallengeType challengeType;
        private String        callbackParams;
        private String        reply;

        public WsAuthenticationChallenge() {
        }

        public WsAuthenticationChallenge(String code, String displayText, ChallengeType challengeType, String callbackParams) {
            this.code = code;
            this.displayText = displayText;
            this.challengeType = challengeType;
            this.callbackParams = callbackParams;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getDisplayText() {
            return displayText;
        }

        public void setDisplayText(String displayText) {
            this.displayText = displayText;
        }

        public ChallengeType getChallengeType() {
            return challengeType;
        }

        public void setChallengeType(ChallengeType challengeType) {
            this.challengeType = challengeType;
        }

        public String getCallbackParams() {
            return callbackParams;
        }

        public void setCallbackParams(String callbackParams) {
            this.callbackParams = callbackParams;
        }

        public String getReply() {
            return reply;
        }

        public void setReply(String reply) {
            this.reply = reply;
        }
    }
}