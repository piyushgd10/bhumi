package com.uniware.core.api.saleorder;

/**
 * Created by akshayag on 9/21/15.
 */

public class GetSaleOrderMobileResponse extends GetSaleOrderResponse {
    /**
     *
     */
    private static final long serialVersionUID = 346789876543456789L;

    private String            sourceCode;
    private String            colorCode;
    private String            shortName;

    public GetSaleOrderMobileResponse() {

    }

    public GetSaleOrderMobileResponse(GetSaleOrderResponse response) {
        this.setSaleOrderDTO(response.getSaleOrderDTO());
        this.setFailedOrderFetch(response.isFailedOrderFetch());
        this.setRefreshEnabled(response.isRefreshEnabled());
        this.setErrors(response.getErrors());
        this.setMessage(response.getMessage());
        this.setSuccessful(response.isSuccessful());
    }

    public String getSourceCode() {
        return sourceCode;
    }

    public void setSourceCode(String sourceCode) {
        this.sourceCode = sourceCode;
    }

    public String getColorCode() {
        return colorCode;
    }

    public void setColorCode(String colorCode) {
        this.colorCode = colorCode;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }
}
