/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Apr 12, 2015
 *  @author akshay
 */
package com.uniware.core.api.material;

import com.unifier.core.api.base.ServiceResponse;

public class AddNonTraceableGatePassItemResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -5969562978512613710L;

}
