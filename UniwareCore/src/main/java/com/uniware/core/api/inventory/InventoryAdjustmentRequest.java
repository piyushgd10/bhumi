/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 27, 2012
 *  @author singla
 */
package com.uniware.core.api.inventory;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.unifier.core.api.base.ServiceRequest;
import com.uniware.core.entity.InventoryAdjustment;

/**
 * @author singla
 */
public class InventoryAdjustmentRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long     serialVersionUID = 9035884051345914779L;

    @Valid
    @NotNull
    private WsInventoryAdjustment inventoryAdjustment;

    @NotNull
    private Integer               userId;

    public InventoryAdjustmentRequest (){

    }

    public InventoryAdjustmentRequest(WsInventoryAdjustment inventoryAdjustment, Integer userId){
        this.inventoryAdjustment = inventoryAdjustment;
        this.userId = userId;
    }

    public WsInventoryAdjustment getInventoryAdjustment() {
        return inventoryAdjustment;
    }

    public void setInventoryAdjustment(WsInventoryAdjustment inventoryAdjustment) {
        this.inventoryAdjustment = inventoryAdjustment;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }
}