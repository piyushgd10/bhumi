/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 9, 2012
 *  @author praveeng
 */
package com.uniware.core.api.catalog.dto;

import java.math.BigDecimal;
import java.util.List;

import com.sun.org.apache.xpath.internal.operations.Bool;
import com.unifier.core.api.customfields.CustomFieldMetadataDTO;
import com.unifier.core.configuration.ConfigurationManager;
import com.uniware.core.configuration.TaxConfiguration;
import com.uniware.core.entity.ItemType;

/**
 * @author praveeng
 */
public class ItemTypeDTO {
    private Integer                      id;
    private String                       skuCode;
    private String                       name;
    private String                       description;
    private Integer                      length;
    private Integer                      width;
    private Integer                      height;
    private Integer                      weight;
    private BigDecimal                   price;
    private String                       color;
    private String                       size;
    private String                       brand;
    private String                       taxTypeCode;
    private String                       gstTaxTypeCode;
    private String                       hsnCode;
    private String                       categoryName;
    private String                       categoryCode;
    private String                       productPageUrl;
    private String                       imageUrl;
    private List<String>                 tags;
    private List<CustomFieldMetadataDTO> customFieldValues;
    private List<InventorySnapshotDTO>   inventorySnapshots;
    private Boolean                      expirable;
    private Integer                      shelfLife;

    public ItemTypeDTO(String skuCode, String name, String imageUrl) {
        this.skuCode = skuCode;
        this.name = name;
        this.imageUrl = imageUrl;
    }

    public ItemTypeDTO(ItemType itemType) {
        this.id = itemType.getId();
        this.skuCode = itemType.getSkuCode();
        this.name = itemType.getName();
        this.description = itemType.getDescription();
        this.length = itemType.getLength();
        this.width = itemType.getWidth();
        this.height = itemType.getHeight();
        this.weight = itemType.getWeight();
        this.price = itemType.getMaxRetailPrice();
        this.setCategoryCode(itemType.getCategory().getCode());
        this.setCategoryName(itemType.getCategory().getName());

        if (itemType.getTaxType() != null) {
            this.setTaxTypeCode(ConfigurationManager.getInstance().getConfiguration(TaxConfiguration.class).getTaxTypeById(itemType.getTaxType().getId()).getCode());
        }
        if (itemType.getGstTaxType() != null) {
            this.setGstTaxTypeCode(ConfigurationManager.getInstance().getConfiguration(TaxConfiguration.class).getTaxTypeById(itemType.getGstTaxType().getId()).getCode());
        }
        this.hsnCode = itemType.getHsnCode();
        this.tags = itemType.getTagList();
        this.color = itemType.getColor();
        this.size = itemType.getSize();
        this.brand = itemType.getBrand();
        this.productPageUrl = itemType.getProductPageUrl();
        this.imageUrl = itemType.getImageUrl();
        this.expirable = itemType.getExpirable();
        this.shelfLife = itemType.getShelfLife();
    }

    /**
     * @return the productPageUrl
     */
    public String getProductPageUrl() {
        return productPageUrl;
    }

    /**
     * @param productPageUrl the productPageUrl to set
     */
    public void setProductPageUrl(String productPageUrl) {
        this.productPageUrl = productPageUrl;
    }

    /**
     * @return the imageUrl
     */
    public String getImageUrl() {
        return imageUrl;
    }

    /**
     * @param imageUrl the imageUrl to set
     */
    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    /**
     * @return the customFieldValues
     */
    public List<CustomFieldMetadataDTO> getCustomFieldValues() {
        return customFieldValues;
    }

    /**
     * @param customFieldValues the customFieldValues to set
     */
    public void setCustomFieldValues(List<CustomFieldMetadataDTO> customFieldValues) {
        this.customFieldValues = customFieldValues;
    }

    /**
     * @param itemType
     */
    /**
     * 
     */
    public ItemTypeDTO() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the categoryName
     */
    public String getCategoryName() {
        return categoryName;
    }

    /**
     * @param categoryName the categoryName to set
     */
    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    /**
     * @return the categoryCode
     */
    public String getCategoryCode() {
        return categoryCode;
    }

    /**
     * @param categoryCode the categoryCode to set
     */
    public void setCategoryCode(String categoryCode) {
        this.categoryCode = categoryCode;
    }

    public String getSkuCode() {
        return skuCode;
    }

    public void setSkuCode(String skuCode) {
        this.skuCode = skuCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getLength() {
        return length;
    }

    public void setLength(Integer length) {
        this.length = length;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    /**
     * @return the taxTypeCode
     */
    public String getTaxTypeCode() {
        return taxTypeCode;
    }

    public String getGstTaxTypeCode() {
        return gstTaxTypeCode;
    }

    public void setGstTaxTypeCode(String gstTaxTypeCode) {
        this.gstTaxTypeCode = gstTaxTypeCode;
    }

    public String getHsnCode() {
        return hsnCode;
    }

    public void setHsnCode(String hsnCode) {
        this.hsnCode = hsnCode;
    }

    /**
     * @param taxTypeCode the taxTypeCode to set
     */
    public void setTaxTypeCode(String taxTypeCode) {
        this.taxTypeCode = taxTypeCode;
    }

    /**
     * @return the price
     */
    public BigDecimal getPrice() {
        return price;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    /**
     * @return the inventorySnapshots
     */
    public List<InventorySnapshotDTO> getInventorySnapshots() {
        return inventorySnapshots;
    }

    /**
     * @param inventorySnapshots the inventorySnapshots to set
     */
    public void setInventorySnapshots(List<InventorySnapshotDTO> inventorySnapshots) {
        this.inventorySnapshots = inventorySnapshots;
    }

    /**
     * @return the color
     */
    public String getColor() {
        return color;
    }

    /**
     * @param color the color to set
     */
    public void setColor(String color) {
        this.color = color;
    }

    /**
     * @return the size
     */
    public String getSize() {
        return size;
    }

    /**
     * @param size the size to set
     */
    public void setSize(String size) {
        this.size = size;
    }

    /**
     * @return the brand
     */
    public String getBrand() {
        return brand;
    }

    /**
     * @param brand the brand to set
     */
    public void setBrand(String brand) {
        this.brand = brand;
    }

    public Boolean isExpirable() {
        return expirable;
    }

    public void setExpirable(Boolean expirable) {
        this.expirable = expirable;
    }

    public Integer getShelfLife() {
        return shelfLife;
    }

    public void setShelfLife(Integer shelfLife) {
        this.shelfLife = shelfLife;
    }

    public static class InventorySnapshotDTO {
        private String facility;
        private long   pendingInventoryAssessment;
        private long   openSale;
        private long   inventory;
        private long   openPurchase;
        private long   putawayPending;
        private long   pendingStockTransfer;
        private long   vendorInventory;
        private long   virtualInventory;

        public InventorySnapshotDTO(String facility, long pendingInventoryAssessment, long openSale, long inventory, long openPurchase, long putawayPending,
                long pendingStockTransfer, long vendorInventory) {
            this.facility = facility;
            this.pendingInventoryAssessment = pendingInventoryAssessment;
            this.openSale = openSale;
            this.inventory = inventory;
            this.openPurchase = openPurchase;
            this.putawayPending = putawayPending;
            this.pendingStockTransfer = pendingStockTransfer;
            this.vendorInventory = vendorInventory;
            this.virtualInventory = virtualInventory;
        }

        /**
         * 
         */
        public InventorySnapshotDTO() {
        }

        /**
         * @return the facility
         */
        public String getFacility() {
            return facility;
        }

        /**
         * @param facility the facility to set
         */
        public void setFacility(String facility) {
            this.facility = facility;
        }

        /**
         * @return the openSale
         */
        public long getOpenSale() {
            return openSale;
        }

        /**
         * @param openSale the openSale to set
         */
        public void setOpenSale(long openSale) {
            this.openSale = openSale;
        }

        /**
         * @return the inventory
         */
        public long getInventory() {
            return inventory;
        }

        /**
         * @param inventory the inventory to set
         */
        public void setInventory(long inventory) {
            this.inventory = inventory;
        }

        /**
         * @return the openPurchase
         */
        public long getOpenPurchase() {
            return openPurchase;
        }

        /**
         * @param openPurchase the openPurchase to set
         */
        public void setOpenPurchase(long openPurchase) {
            this.openPurchase = openPurchase;
        }

        /**
         * @return the putawayPending
         */
        public long getPutawayPending() {
            return putawayPending;
        }

        /**
         * @param putawayPending the putawayPending to set
         */
        public void setPutawayPending(Integer putawayPending) {
            this.putawayPending = putawayPending;
        }

        /**
         * @return the pendingStockTrasfer
         */
        public long getPendingStockTransfer() {
            return pendingStockTransfer;
        }

        /**
         * @param pendingStockTransfer the pendingStockTrasfer to set
         */
        public void setPendingStockTransfer(long pendingStockTransfer) {
            this.pendingStockTransfer = pendingStockTransfer;
        }

        public long getPendingInventoryAssessment() {
            return pendingInventoryAssessment;
        }

        public void setPendingInventoryAssessment(long pendingInventoryAssessment) {
            this.pendingInventoryAssessment = pendingInventoryAssessment;
        }

        public long getVendorInventory() {
            return vendorInventory;
        }

        public void setVendorInventory(long vendorInventory) {
            this.vendorInventory = vendorInventory;
        }

        public void setPutawayPending(long putawayPending) {
            this.putawayPending = putawayPending;
        }

        public long getVirtualInventory() {
            return virtualInventory;
        }

        public void setVirtualInventory(long virtualInventory) {
            this.virtualInventory = virtualInventory;
        }

    }

}
