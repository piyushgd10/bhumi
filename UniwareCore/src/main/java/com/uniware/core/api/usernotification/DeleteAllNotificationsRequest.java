/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 05-Mar-2014
 *  @author parijat
 */
package com.uniware.core.api.usernotification;

import javax.validation.constraints.NotNull;

import com.unifier.core.api.base.ServiceRequest;

public class DeleteAllNotificationsRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 823117811956204012L;

    @NotNull
    private String            notificationIdentifier;

    public String getNotificationIdentifier() {
        return notificationIdentifier;
    }

    public void setNotificationIdentifier(String notificationIdentifier) {
        this.notificationIdentifier = notificationIdentifier;
    }

}
