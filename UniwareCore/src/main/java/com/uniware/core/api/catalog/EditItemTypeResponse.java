/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 9, 2012
 *  @author praveeng
 */
package com.uniware.core.api.catalog;

import com.uniware.core.api.catalog.dto.ItemTypeFullDTO;

/**
 * @author praveeng
 */
public class EditItemTypeResponse extends ItemTypeResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -4979572951223023183L;

    private ItemTypeFullDTO   itemTypeDTO;

    /**
     * @return the itemType
     */
    public ItemTypeFullDTO getItemTypeDTO() {
        return itemTypeDTO;
    }

    /**
     * @param itemType the itemType to set
     */
    public void setItemType(ItemTypeFullDTO itemTypeDTO) {
        this.itemTypeDTO = itemTypeDTO;
    }

}
