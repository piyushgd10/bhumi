/*
 * Copyright 2017 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 25/09/17
 * @author piyush
 */
package com.uniware.core.api.picker;

import com.unifier.core.api.base.ServiceRequest;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import java.util.List;

public class ScanShipmentAtStagingRequest extends ServiceRequest {

    @NotBlank
    private String picklistCode;

    @NotBlank
    private String shippingPackageCode;

    public String getPicklistCode() {
        return picklistCode;
    }

    public void setPicklistCode(String picklistCode) {
        this.picklistCode = picklistCode;
    }

    public String getShippingPackageCode() {
        return shippingPackageCode;
    }

    public void setShippingPackageCode(String shippingPackageCode) {
        this.shippingPackageCode = shippingPackageCode;
    }

}
