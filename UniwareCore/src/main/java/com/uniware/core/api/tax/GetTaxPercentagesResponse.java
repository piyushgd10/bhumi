package com.uniware.core.api.tax;

import com.unifier.core.api.base.ServiceResponse;
import java.util.Map;

/**
 * Created by Sagar Sahni on 29/05/17.
 */
public class GetTaxPercentagesResponse extends ServiceResponse {

    private Map<String, TaxPercentageDTO> identifierToTaxPercentages;

    public Map<String, TaxPercentageDTO> getIdentifierToTaxPercentages() {
        return identifierToTaxPercentages;
    }

    public void setIdentifierToTaxPercentages(Map<String, TaxPercentageDTO> identifierToTaxPercentages) {
        this.identifierToTaxPercentages = identifierToTaxPercentages;
    }
}
