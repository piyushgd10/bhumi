/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 24-Jul-2012
 *  @author vibhu
 */
package com.uniware.core.api.saleorder;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.unifier.core.api.base.ServiceRequest;
import com.uniware.core.cache.LocationCache;

/**
 * @author vibhu
 */
public class EditSaleOrderAddressesRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long         serialVersionUID = 1L;

    private List<SaleOrderAddressDTO> addresses        = new ArrayList<SaleOrderAddressDTO>();

    /**
     * @return the addresses
     */
    public List<SaleOrderAddressDTO> getAddresses() {
        return addresses;
    }

    /**
     * @param addresses the addresses to set
     */
    public void setAddresses(List<SaleOrderAddressDTO> addresses) {
        this.addresses = addresses;
    }

    public static class SaleOrderAddressDTO {

        private String  name;
        @NotNull
        private Integer addressId;
        @NotEmpty
        private String  addressLine1;
        private String  addressLine2;
        @NotEmpty
        private String  city;
        @NotEmpty
        private String  stateCode;
        @NotEmpty
        private String  pincode;
        @NotEmpty
        private String  phone;

        private String  countryCode = LocationCache.COUNTRY_CODE_INDIA;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        /**
         * @return the addressId
         */
        public Integer getAddressId() {
            return addressId;
        }

        /**
         * @param addressId the addressId to set
         */
        public void setAddressId(Integer addressId) {
            this.addressId = addressId;
        }

        /**
         * @return the addressLine1
         */
        public String getAddressLine1() {
            return addressLine1;
        }

        /**
         * @param addressLine1 the addressLine1 to set
         */
        public void setAddressLine1(String addressLine1) {
            this.addressLine1 = addressLine1;
        }

        /**
         * @return the addressLine2
         */
        public String getAddressLine2() {
            return addressLine2;
        }

        /**
         * @param addressLine2 the addressLine2 to set
         */
        public void setAddressLine2(String addressLine2) {
            this.addressLine2 = addressLine2;
        }

        /**
         * @return the city
         */
        public String getCity() {
            return city;
        }

        /**
         * @param city the city to set
         */
        public void setCity(String city) {
            this.city = city;
        }

        /**
         * @return the stateCode
         */
        public String getStateCode() {
            return stateCode;
        }

        /**
         * @param stateCode the stateCode to set
         */
        public void setStateCode(String stateCode) {
            this.stateCode = stateCode;
        }

        /**
         * @return the pincode
         */
        public String getPincode() {
            return pincode;
        }

        /**
         * @param pincode the pincode to set
         */
        public void setPincode(String pincode) {
            this.pincode = pincode;
        }

        /**
         * @return the phone
         */
        public String getPhone() {
            return phone;
        }

        /**
         * @param phone the phone to set
         */
        public void setPhone(String phone) {
            this.phone = phone;
        }

        /**
         * @return the countryCode
         */
        public String getCountryCode() {
            return countryCode;
        }

        /**
         * @param countryCode the countryCode to set
         */
        public void setCountryCode(String countryCode) {
            this.countryCode = countryCode;
        }

    }
}
