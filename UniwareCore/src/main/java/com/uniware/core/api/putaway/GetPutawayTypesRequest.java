/*
 * Copyright 2017 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 19/04/17
 * @author piyush
 */
package com.uniware.core.api.putaway;

import com.unifier.core.api.base.ServiceRequest;

public class GetPutawayTypesRequest extends ServiceRequest {

    private boolean webOnly;

    public boolean isWebOnly() {
        return webOnly;
    }

    public void setWebOnly(boolean webOnly) {
        this.webOnly = webOnly;
    }
}
