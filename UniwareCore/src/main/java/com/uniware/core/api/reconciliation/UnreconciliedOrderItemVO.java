/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 08-Jan-2014
 *  @author parijat
 */
package com.uniware.core.api.reconciliation;

import java.math.BigDecimal;
import java.util.Date;

public class UnreconciliedOrderItemVO {

    private String reconciliationIdentifier;
    private String     currencyCode;
    private BigDecimal currencyConversionRate;
    private BigDecimal transferPrice;
    private BigDecimal sellingPrice;
    private BigDecimal shippingCharges;
    private BigDecimal channelCommission;
    private BigDecimal channelPenalty;
    private String channelCode;
    private String     description;
    private Date   orderTransactionDate;

    public String getReconciliationIdentifier() {
        return reconciliationIdentifier;
    }

    public void setReconciliationIdentifier(String reconciliationIdentifier) {
        this.reconciliationIdentifier = reconciliationIdentifier;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public BigDecimal getCurrencyConversionRate() {
        return currencyConversionRate;
    }

    public void setCurrencyConversionRate(BigDecimal currencyConversionRate) {
        this.currencyConversionRate = currencyConversionRate;
    }

    public BigDecimal getTransferPrice() {
        return transferPrice;
    }

    public void setTransferPrice(BigDecimal transferPrice) {
        this.transferPrice = transferPrice;
    }

    public BigDecimal getSellingPrice() {
        return sellingPrice;
    }

    public void setSellingPrice(BigDecimal sellingPrice) {
        this.sellingPrice = sellingPrice;
    }

    public BigDecimal getShippingCharges() {
        return shippingCharges;
    }

    public void setShippingCharges(BigDecimal shippingCharges) {
        this.shippingCharges = shippingCharges;
    }

    public BigDecimal getChannelCommission() {
        return channelCommission;
    }

    public void setChannelCommission(BigDecimal channelCommission) {
        this.channelCommission = channelCommission;
    }

    public BigDecimal getChannelPenalty() {
        return channelPenalty;
    }

    public void setChannelPenalty(BigDecimal channelPenalty) {
        this.channelPenalty = channelPenalty;
    }

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    public Date getOrderTransactionDate() {
        return orderTransactionDate;
    }

    public void setOrderTransactionDate(Date orderTransactionDate) {
        this.orderTransactionDate = orderTransactionDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
