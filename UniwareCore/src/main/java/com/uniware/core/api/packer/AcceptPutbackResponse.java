/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 23, 2012
 *  @author singla
 */
package com.uniware.core.api.packer;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author singla
 */
public class AcceptPutbackResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 5432195144660891234L;

}
