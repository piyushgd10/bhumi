package com.uniware.core.api.o2o;

import com.unifier.core.api.base.ServiceRequest;

/**
 * Created by piyush on 1/12/16.
 */
public class GetTenantDetailsRequest extends ServiceRequest {

    private String facilityCode;

    public String getFacilityCode() {
        return facilityCode;
    }

    public void setFacilityCode(String facilityCode) {
        this.facilityCode = facilityCode;
    }
}
