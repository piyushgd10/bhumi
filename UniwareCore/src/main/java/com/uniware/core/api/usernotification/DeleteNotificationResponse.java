/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 05-Mar-2014
 *  @author parijat
 */
package com.uniware.core.api.usernotification;

import com.unifier.core.api.base.ServiceResponse;

public class DeleteNotificationResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -455793785541969266L;

}
