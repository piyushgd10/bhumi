/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 2, 2012
 *  @author praveeng
 */
package com.uniware.core.api.purchase;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author Sunny Agarwal
 */
public class GetVendorPurchaseOrderDetailResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long      serialVersionUID = -901233313257440377L;

    private PurchaseOrderDetailDTO purchaseOrderDetailDTO;

    public PurchaseOrderDetailDTO getPurchaseOrderDetailDTO() {
        return purchaseOrderDetailDTO;
    }

    public void setPurchaseOrderDetailDTO(PurchaseOrderDetailDTO purchaseOrderDetailDTO) {
        this.purchaseOrderDetailDTO = purchaseOrderDetailDTO;
    }

}
