/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 10, 2012
 *  @author praveeng
 */
package com.uniware.core.api.catalog;

import com.unifier.core.api.base.ServiceResponse;

import java.util.ArrayList;
import java.util.List;

import com.uniware.core.api.catalog.dto.CategoryDTO;

/**
 * @author praveeng
 */
public class GetCategoryResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -6773897774236717265L;
    private List<CategoryDTO> categories       = new ArrayList<CategoryDTO>();

    /**
     * @return the categories
     */
    public List<CategoryDTO> getCategories() {
        return categories;
    }

    /**
     * @param categories the categories to set
     */
    public void setCategories(List<CategoryDTO> categories) {
        this.categories = categories;
    }

}
