/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, May 1, 2012
 *  @author singla
 */
package com.uniware.core.api.inflow;

import com.unifier.core.api.base.ServiceRequest;

import java.util.Date;

import javax.validation.constraints.NotNull;

/**
 * @author singla
 */
public class UpdateTraceableInflowReceiptItemRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -4152956417315308288L;

    @NotNull
    private Integer           inflowReceiptItemId;

    private String            comments;

    private Date              expiry;

    /**
     * @return the comments
     */
    public String getComments() {
        return comments;
    }

    /**
     * @param comments the comments to set
     */
    public void setComments(String comments) {
        this.comments = comments;
    }

    /**
     * @return the expiry
     */
    public Date getExpiry() {
        return expiry;
    }

    /**
     * @param expiry the expiry to set
     */
    public void setExpiry(Date expiry) {
        this.expiry = expiry;
    }

    /**
     * @return the inflowReceiptItemId
     */
    public Integer getInflowReceiptItemId() {
        return inflowReceiptItemId;
    }

    /**
     * @param inflowReceiptItemId the inflowReceiptItemId to set
     */
    public void setInflowReceiptItemId(Integer inflowReceiptItemId) {
        this.inflowReceiptItemId = inflowReceiptItemId;
    }

}