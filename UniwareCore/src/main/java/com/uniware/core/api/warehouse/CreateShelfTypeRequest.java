/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 24, 2011
 *  @author ankitp
 */
package com.uniware.core.api.warehouse;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author ankitp
 */
public class CreateShelfTypeRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -1467452867569855702L;

    @NotEmpty
    private String            code;
    @NotNull
    private Integer           length;
    @NotNull
    private Integer           width;
    @NotNull
    private Integer           height;

    public CreateShelfTypeRequest() {
        super();
    }

    public CreateShelfTypeRequest(String code, Integer length, Integer width, Integer height) {
        super();
        this.code = code;
        this.length = length;
        this.width = width;
        this.height = height;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the length
     */
    public Integer getLength() {
        return length;
    }

    /**
     * @param length the length to set
     */
    public void setLength(Integer length) {
        this.length = length;
    }

    /**
     * @return the width
     */
    public Integer getWidth() {
        return width;
    }

    /**
     * @param width the width to set
     */
    public void setWidth(Integer width) {
        this.width = width;
    }

    /**
     * @return the height
     */
    public Integer getHeight() {
        return height;
    }

    /**
     * @param height the height to set
     */
    public void setHeight(Integer height) {
        this.height = height;
    }

}