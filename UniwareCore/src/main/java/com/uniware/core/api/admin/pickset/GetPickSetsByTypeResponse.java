/*
 * Copyright 2016 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *    
 * @version     1.0, 13/9/16 7:54 PM
 * @author bhuvneshwarkumar
 */

package com.uniware.core.api.admin.pickset;

import java.util.List;

import com.unifier.core.api.base.ServiceResponse;

/**
 * Created by bhuvneshwarkumar on 13/09/16.
 */
public class GetPickSetsByTypeResponse extends ServiceResponse {

    List<PicksetDTO> picksets;

    public List<PicksetDTO> getPicksets() {
        return picksets;
    }

    public void setPicksets(List<PicksetDTO> picksets) {
        this.picksets = picksets;
    }
}
