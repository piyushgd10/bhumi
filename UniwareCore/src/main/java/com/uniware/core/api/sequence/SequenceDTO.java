/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 16-Feb-2014
 *  @author karunsingla
 */
package com.uniware.core.api.sequence;

import com.uniware.core.entity.Sequence;

public class SequenceDTO {

    private String prefix;
    private String nextYearPrefix;
    private boolean resetCounterNextYear;
    private int    currentValue;

    public SequenceDTO() {

    }

    public SequenceDTO(Sequence sequence) {
        this.prefix = sequence.getPrefix();
        this.nextYearPrefix = sequence.getNextYearPrefix();
        this.resetCounterNextYear = sequence.isResetCounterNextYear();
        this.currentValue = sequence.getCurrentValue() + 1;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public int getCurrentValue() {
        return currentValue;
    }

    public void setCurrentValue(int currentValue) {
        this.currentValue = currentValue;
    }

    public String getNextYearPrefix() {
        return nextYearPrefix;
    }

    public void setNextYearPrefix(String nextYearPrefix) {
        this.nextYearPrefix = nextYearPrefix;
    }

    public boolean isResetCounterNextYear() {
        return resetCounterNextYear;
    }

    public void setResetCounterNextYear(boolean resetCounterNextYear) {
        this.resetCounterNextYear = resetCounterNextYear;
    }
}
