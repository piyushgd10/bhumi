/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, May 26, 2012
 *  @author singla
 */
package com.uniware.core.api.returns;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author singla
 */
public class CreateReturnManifestResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 71662175591364486L;

    private Integer           returnManifestId;
    private String            returnManifestCode;

    /**
     * @return the returnManifestId
     */
    public Integer getReturnManifestId() {
        return returnManifestId;
    }

    /**
     * @param returnManifestId the returnManifestId to set
     */
    public void setReturnManifestId(Integer returnManifestId) {
        this.returnManifestId = returnManifestId;
    }

    /**
     * @return the returnManifestCode
     */
    public String getReturnManifestCode() {
        return returnManifestCode;
    }

    /**
     * @param returnManifestCode the returnManifestCode to set
     */
    public void setReturnManifestCode(String returnManifestCode) {
        this.returnManifestCode = returnManifestCode;
    }
}
