/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 *  @version     1.0, Aug 20, 2015
 *  @author akshay
 */
package com.uniware.core.api.o2o;

import java.util.List;

import com.unifier.core.api.base.ServiceResponse;

public class GetFacilitiesSLAResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long              serialVersionUID        = -4976274726518309659L;

    private List<ProductFacilitySLA> productFacilitySLAs;
    
    public List<ProductFacilitySLA> getProductFacilitySLAs() {
        return productFacilitySLAs;
    }

    public void setProductFacilitySLAs(List<ProductFacilitySLA> productFacilitySLAs) {
        this.productFacilitySLAs = productFacilitySLAs;
    }

    public static class ProductFacilitySLA {
        
        private String sellerSkuCode;
        private List<FacilitySLA> facilitySLAs;
        
        public String getSellerSkuCode() {
            return sellerSkuCode;
        }

        public void setSellerSkuCode(String sellerSkuCode) {
            this.sellerSkuCode = sellerSkuCode;
        }

        public List<FacilitySLA> getFacilitySLAs() {
            return facilitySLAs;
        }

        public void setFacilitySLAs(List<FacilitySLA> facilitySLAs) {
            this.facilitySLAs = facilitySLAs;
        }

        public static class FacilitySLA {
            private String code;
            private int dispatchSLA;
            private int deliverySLA;
            public String getCode() {
                return code;
            }
            public void setCode(String code) {
                this.code = code;
            }
            public int getDispatchSLA() {
                return dispatchSLA;
            }
            public void setDispatchSLA(int dispatchSLA) {
                this.dispatchSLA = dispatchSLA;
            }
            public int getDeliverySLA() {
                return deliverySLA;
            }
            public void setDeliverySLA(int deliverySLA) {
                this.deliverySLA = deliverySLA;
            }
        }
    }
}
