/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 24-Mar-2014
 *  @author parijat
 */
package com.uniware.core.api.apiStatistics;

import java.util.List;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author parijat
 */
public class GetApiUserAccessStatisticsResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long          serialVersionUID = -3227645995720353559L;

    private List<ApiAccessStatisticsDTO> apiUserStatisticsDTO;

    public List<ApiAccessStatisticsDTO> getApiUserStatisticsDTO() {
        return apiUserStatisticsDTO;
    }

    public void setApiUserStatisticsDTO(List<ApiAccessStatisticsDTO> apiUserStatisticsDTO) {
        this.apiUserStatisticsDTO = apiUserStatisticsDTO;
    }

}
