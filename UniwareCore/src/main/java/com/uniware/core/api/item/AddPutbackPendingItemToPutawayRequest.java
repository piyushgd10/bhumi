/*
 * Copyright 2017 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 11/8/17 12:56 PM
 * @author digvijaysharma
 */
package com.uniware.core.api.item;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

public class AddPutbackPendingItemToPutawayRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 876013105112367675L;

    @NotBlank
    private String            itemCode;

    @NotNull
    private Integer           userId;

    @NotBlank
    private String            putawayCode;

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return the itemCode
     */
    public String getItemCode() {
        return itemCode;
    }

    /**
     * @param itemCode the itemCode to set
     */
    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    /**
     * @return the putawayCode
     */
    public String getPutawayCode() {
        return putawayCode;
    }

    /**
     * @param putawayCode the putawayCode to set
     */
    public void setPutawayCode(String putawayCode) {
        this.putawayCode = putawayCode;
    }

}
