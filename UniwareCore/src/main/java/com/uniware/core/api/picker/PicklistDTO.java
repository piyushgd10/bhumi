/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jan 11, 2012
 *  @author singla
 */
package com.uniware.core.api.picker;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.uniware.core.entity.Picklist;

/**
 * @author singla
 */
public class PicklistDTO {
    private int                   id;
    private String                code;
    private String                username;
    private Date                  created;
    private String                status;
    private List<PicklistItemDTO> picklistItems = new ArrayList<PicklistItemDTO>();

    public PicklistDTO() {
    }

    public PicklistDTO(Picklist picklist) {
        this.id = picklist.getId();
        this.code = picklist.getCode();
        this.username = picklist.getUser().getUsername();
        this.created = picklist.getCreated();
        this.status = picklist.getStatusCode();
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the created
     */
    public Date getCreated() {
        return created;
    }

    /**
     * @param created the created to set
     */
    public void setCreated(Date created) {
        this.created = created;
    }

    /**
     * @return the picklistItems
     */
    public List<PicklistItemDTO> getPicklistItems() {
        return picklistItems;
    }

    /**
     * @param picklistItems the picklistItems to set
     */
    public void setPicklistItems(List<PicklistItemDTO> picklistItems) {
        this.picklistItems = picklistItems;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }
}
