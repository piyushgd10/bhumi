/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 25-Dec-2013
 *  @author sunny
 */
package com.uniware.core.api.systemnotification.sandbox;

import com.uniware.core.entity.Source;

public class SandboxSourceVO {

    private String  code;
    private String  name;
    private String  type;
    private String  localization;
    private boolean enabled;
    private boolean orderSyncConfigured;
    private boolean inventorySyncConfigured;
    private boolean thirdPartyShipping;
    private boolean useChannelSKU;
    private int     priority;
    private boolean thirdPartyConfigurationRequired;
    private boolean useChannelSkuForInventoryUpdate;
    private Integer tat;
    private boolean notificationsEnabled;
    private boolean pendencyConfigurationEnabled;
    private boolean sourceNotificationsEnabled;
    private boolean allowMultipleChannel;
    private boolean allowCombinedManifest;
    private boolean autoVerifyOrders;
    private boolean allowAnyShippingMethod;
    private boolean useSellerSkuForInventoryUpdate;
    private boolean packageTypeConfigured;

    public SandboxSourceVO() {
        super();
    }

    public SandboxSourceVO(Source s) {
        code = s.getCode();
        name = s.getName();
        enabled = s.isEnabled();
        type = s.getType().name();
        localization = s.getLocalization().name();
        orderSyncConfigured = s.isOrderSyncConfigured();
        inventorySyncConfigured = s.isInventorySyncConfigured();
        priority = s.getPriority();
        thirdPartyConfigurationRequired = s.isThirdPartyConfigurationRequired();
        pendencyConfigurationEnabled = s.isPendencyConfigurationEnabled();
        allowMultipleChannel = s.isAllowMultipleChannel();
        notificationsEnabled = s.isNotificationsEnabled();
        allowCombinedManifest = s.isAllowCombinedManifest();
        autoVerifyOrders = s.isAutoVerifyOrders();
        packageTypeConfigured = s.isPackageTypeConfigured();
        allowAnyShippingMethod = s.isAllowAnyShippingMethod();
        useSellerSkuForInventoryUpdate = s.isUseSellerSkuForInventoryUpdate();
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLocalization() {
        return localization;
    }

    public void setLocalization(String localization) {
        this.localization = localization;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public boolean isOrderSyncConfigured() {
        return orderSyncConfigured;
    }

    public void setOrderSyncConfigured(boolean orderSyncConfigured) {
        this.orderSyncConfigured = orderSyncConfigured;
    }

    public boolean isInventorySyncConfigured() {
        return inventorySyncConfigured;
    }

    public void setInventorySyncConfigured(boolean inventorySyncConfigured) {
        this.inventorySyncConfigured = inventorySyncConfigured;
    }

    public boolean isThirdPartyShipping() {
        return thirdPartyShipping;
    }

    public void setThirdPartyShipping(boolean thirdPartyShipping) {
        this.thirdPartyShipping = thirdPartyShipping;
    }

    public boolean isUseChannelSKU() {
        return useChannelSKU;
    }

    public void setUseChannelSKU(boolean useChannelSKU) {
        this.useChannelSKU = useChannelSKU;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public boolean isThirdPartyConfigurationRequired() {
        return thirdPartyConfigurationRequired;
    }

    public void setThirdPartyConfigurationRequired(boolean thirdPartyConfigurationRequired) {
        this.thirdPartyConfigurationRequired = thirdPartyConfigurationRequired;
    }

    public boolean isUseChannelSkuForInventoryUpdate() {
        return useChannelSkuForInventoryUpdate;
    }

    public void setUseChannelSkuForInventoryUpdate(boolean useChannelSkuForInventoryUpdate) {
        this.useChannelSkuForInventoryUpdate = useChannelSkuForInventoryUpdate;
    }

    public Integer getTat() {
        return tat;
    }

    public void setTat(Integer tat) {
        this.tat = tat;
    }

    public boolean isNotificationsEnabled() {
        return notificationsEnabled;
    }

    public void setNotificationsEnabled(boolean notificationsEnabled) {
        this.notificationsEnabled = notificationsEnabled;
    }

    public boolean isPendencyConfigurationEnabled() {
        return pendencyConfigurationEnabled;
    }

    public void setPendencyConfigurationEnabled(boolean pendencyConfigurationEnabled) {
        this.pendencyConfigurationEnabled = pendencyConfigurationEnabled;
    }

    public boolean isSourceNotificationsEnabled() {
        return sourceNotificationsEnabled;
    }

    public void setSourceNotificationsEnabled(boolean sourceNotificationsEnabled) {
        this.sourceNotificationsEnabled = sourceNotificationsEnabled;
    }

    public boolean isAllowMultipleChannel() {
        return allowMultipleChannel;
    }

    public void setAllowMultipleChannel(boolean allowMultipleChannel) {
        this.allowMultipleChannel = allowMultipleChannel;
    }

    public boolean isAllowCombinedManifest() {
        return allowCombinedManifest;
    }

    public void setAllowCombinedManifest(boolean allowCombinedManifest) {
        this.allowCombinedManifest = allowCombinedManifest;
    }

    public boolean isAutoVerifyOrders() {
        return autoVerifyOrders;
    }

    public void setAutoVerifyOrders(boolean autoVerifyOrders) {
        this.autoVerifyOrders = autoVerifyOrders;
    }

    public boolean isAllowAnyShippingMethod() {
        return allowAnyShippingMethod;
    }

    public void setAllowAnyShippingMethod(boolean allowAnyShippingMethod) {
        this.allowAnyShippingMethod = allowAnyShippingMethod;
    }

    public boolean isUseSellerSkuForInventoryUpdate() {
        return useSellerSkuForInventoryUpdate;
    }

    public void setUseSellerSkuForInventoryUpdate(boolean useSellerSkuForInventoryUpdate) {
        this.useSellerSkuForInventoryUpdate = useSellerSkuForInventoryUpdate;
    }

    public boolean isPackageTypeConfigured() {
        return packageTypeConfigured;
    }

    public void setPackageTypeConfigured(boolean packageTypeConfigured) {
        this.packageTypeConfigured = packageTypeConfigured;
    }

}
