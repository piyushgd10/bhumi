package com.uniware.core.api.saleorder;

import com.unifier.core.api.base.ServiceRequest;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Created by bhuvneshwarkumar on 24/08/15.
 */
public class CreateSaleOrderAlternateRequest extends ServiceRequest {

    @Valid
    @NotEmpty
    private String saleOrderCode;

    @NotNull
    private Integer userId;

    @Valid
    @NotEmpty
    private List<WsSOItemAlternate> saleOrderItemAlternateList;

    public static class WsSOItemAlternate {
        @NotBlank
        private String                         saleOrderItemCode;
        @NotEmpty
        private List<WsSaleOrderItemAlternate> saleOrderItemAlternates;

        public String getSaleOrderItemCode() {
            return saleOrderItemCode;
        }

        public void setSaleOrderItemCode(String saleOrderItemCode) {
            this.saleOrderItemCode = saleOrderItemCode;
        }

        public List<WsSaleOrderItemAlternate> getSaleOrderItemAlternates() {
            return saleOrderItemAlternates;
        }

        public void setSaleOrderItemAlternates(List<WsSaleOrderItemAlternate> saleOrderItemAlternates) {
            this.saleOrderItemAlternates = saleOrderItemAlternates;
        }
    }

    public String getSaleOrderCode() {
        return saleOrderCode;
    }

    public void setSaleOrderCode(String saleOrderCode) {
        this.saleOrderCode = saleOrderCode;
    }

    public List<WsSOItemAlternate> getSaleOrderItemAlternateList() {
        return saleOrderItemAlternateList;
    }

    public void setSaleOrderItemAlternateList(List<WsSOItemAlternate> saleOrderItemAlternateList) {
        this.saleOrderItemAlternateList = saleOrderItemAlternateList;
    }

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

}
