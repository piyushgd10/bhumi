/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 18-Jun-2013
 *  @author unicom
 */
package com.uniware.core.api.channel;

public class FetchPendencyDTO {

    private Integer itemTypeId;

    private String  productName;

    private String  sellerSkuCode;

    private String  channelProductId;

    private String  channelProductIdentifier;

    private Integer requiredInventory;

    private Integer availableInventory;

    private Integer promisedQuantity;

    private Integer outOfStockQuantity;
    
    private String itemSku;

   

    public Integer getRequiredInventory() {
        return requiredInventory;
    }

    public void setRequiredInventory(Integer requiredInventory) {
        this.requiredInventory = requiredInventory;
    }

    public Integer getAvailableInventory() {
        return availableInventory;
    }

    public void setAvailableInventory(Integer availableInventory) {
        this.availableInventory = availableInventory;
    }

    public Integer getItemTypeId() {
        return itemTypeId;
    }

    public void setItemTypeId(Integer itemTypeId) {
        this.itemTypeId = itemTypeId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getSellerSkuCode() {
        return sellerSkuCode;
    }

    public void setSellerSkuCode(String sellerSkuCode) {
        this.sellerSkuCode = sellerSkuCode;
    }

    public String getChannelProductIdentifier() {
        return channelProductIdentifier;
    }

    public void setChannelProductIdentifier(String channelProductIdentifier) {
        this.channelProductIdentifier = channelProductIdentifier;
    }

    public String getChannelProductId() {
        return channelProductId;
    }

    public void setChannelProductId(String channelProductId) {
        this.channelProductId = channelProductId;
    }

    public Integer getPromisedQuantity() {
        return promisedQuantity;
    }

    public void setPromisedQuantity(Integer promisedQuantity) {
        this.promisedQuantity = promisedQuantity;
    }

    public Integer getOutOfStockQuantity() {
        return outOfStockQuantity;
    }

    public void setOutOfStockQuantity(Integer outOfStockQuantity) {
        this.outOfStockQuantity = outOfStockQuantity;
    }

    public String getItemSku() {
        return itemSku;
    }

    public void setItemSku(String itemSku) {
        this.itemSku = itemSku;
    }
}
