/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Sep 13, 2012
 *  @author pankaj
 */
package com.uniware.core.api.material;

import java.math.BigDecimal;
import java.util.List;

import javax.validation.Valid;

import org.hibernate.validator.constraints.Length;

import com.unifier.core.api.customfields.WsCustomFieldValue;

/**
 * @author pankaj
 */
public class WsGatePass {


    private String                   code;

    @Length(max = 500)
    private String                   purpose;

    private BigDecimal               transferAmount;

    @Length(max = 45)
    private String                   referenceNumber;

    @Valid
    private List<WsCustomFieldValue> customFieldValues;

    /**
     * @return the customFieldValues
     */
    public List<WsCustomFieldValue> getCustomFieldValues() {
        return customFieldValues;
    }

    /**
     * @param customFieldValues the customFieldValues to set
     */
    public void setCustomFieldValues(List<WsCustomFieldValue> customFieldValues) {
        this.customFieldValues = customFieldValues;
    }

    /**
     * @return the purpose
     */
    public String getPurpose() {
        return purpose;
    }

    /**
     * @param purpose the purpose to set
     */
    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    /**
     * @return the transferAmount
     */
    public BigDecimal getTransferAmount() {
        return transferAmount;
    }

    /**
     * @param transferAmount the transferAmount to set
     */
    public void setTransferAmount(BigDecimal transferAmount) {
        this.transferAmount = transferAmount;
    }

    /**
     * @return the referenceNumber
     */
    public String getReferenceNumber() {
        return referenceNumber;
    }

    /**
     * @param referenceNumber the referenceNumber to set
     */
    public void setReferenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
