package com.uniware.core.api.returns;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

/**
 * Created by Sagar Sahni on 21/06/17.
 */
public class GetReversePickupInvoiceRequest extends ServiceRequest {

    @NotBlank
    private String reversePickupCode;

    public String getReversePickupCode() {
        return reversePickupCode;
    }

    public void setReversePickupCode(String reversePickupCode) {
        this.reversePickupCode = reversePickupCode;
    }
}
