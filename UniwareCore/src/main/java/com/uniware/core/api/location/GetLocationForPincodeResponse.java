/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 14-Jul-2015
 *  @author parijat
 */
package com.uniware.core.api.location;

import com.unifier.core.api.base.ServiceResponse;
import com.uniware.core.entity.Location;

public class GetLocationForPincodeResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private Location          location;

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

}
