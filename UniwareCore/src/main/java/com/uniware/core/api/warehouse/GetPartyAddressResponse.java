/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 03-Feb-2014
 *  @author parijat
 */
package com.uniware.core.api.warehouse;

import com.unifier.core.api.base.ServiceResponse;
import com.uniware.core.api.party.PartyAddressDTO;

public class GetPartyAddressResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -3321395884721552399L;

    private PartyAddressDTO   partyAddressDTO;

    public PartyAddressDTO getPartyAddressDTO() {
        return partyAddressDTO;
    }

    public void setPartyAddressDTO(PartyAddressDTO partyAddressDTO) {
        this.partyAddressDTO = partyAddressDTO;
    }
}
