/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 25-Jan-2012
 *  @author vibhu
 */
package com.uniware.core.api.warehouse;

import java.util.List;

import com.unifier.core.api.customfields.CustomFieldMetadataDTO;
import com.uniware.core.api.party.dto.GenericPartyDTO;
import com.uniware.core.api.sequence.SequenceDTO;
import com.uniware.core.entity.Facility;
import com.uniware.core.entity.FacilityProfile;
import com.uniware.core.entity.Sequence;

/**
 * @author vibhu
 */
public class FacilityDTO extends GenericPartyDTO {

    Integer id;

    private String type;

    private String displayName;

    private SequenceDTO retailInvoiceSequence;

    private SequenceDTO taxInvoiceSequence;

    private List<CustomFieldMetadataDTO> customFieldValues;

    private boolean dummy;
    
    private boolean salesTaxOnShippingCharges;

    private FacilityProfile facilityProfile;

    public FacilityDTO() {

    }

    public FacilityDTO(Facility facility, Sequence retailInvoiceSequence, Sequence taxInvoiceSequence, FacilityProfile facilityProfile) {
        super(facility);
        this.id = facility.getId();
        this.type = facility.getType();
        this.displayName = facility.getDisplayName();
        this.retailInvoiceSequence = new SequenceDTO(retailInvoiceSequence);
        this.taxInvoiceSequence = new SequenceDTO(taxInvoiceSequence);
        this.facilityProfile = facilityProfile;
        this.salesTaxOnShippingCharges=facility.isSalesTaxOnShippingCharges();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public SequenceDTO getRetailInvoiceSequence() {
        return retailInvoiceSequence;
    }

    public void setRetailInvoiceSequence(SequenceDTO retailInvoiceSequence) {
        this.retailInvoiceSequence = retailInvoiceSequence;
    }

    public SequenceDTO getTaxInvoiceSequence() {
        return taxInvoiceSequence;
    }

    public void setTaxInvoiceSequence(SequenceDTO taxInvoiceSequence) {
        this.taxInvoiceSequence = taxInvoiceSequence;
    }

    public List<CustomFieldMetadataDTO> getCustomFieldValues() {
        return customFieldValues;
    }

    public void setCustomFieldValues(List<CustomFieldMetadataDTO> customFieldValues) {
        this.customFieldValues = customFieldValues;
    }

    public boolean isDummy() {
        return dummy;
    }

    public void setDummy(boolean dummy) {
        this.dummy = dummy;
    }
    
    public boolean isSalesTaxOnShippingCharges() {
		return salesTaxOnShippingCharges;
	}

	public void setSalesTaxOnShippingCharges(boolean salesTaxOnShippingCharges) {
		this.salesTaxOnShippingCharges = salesTaxOnShippingCharges;
	}

    public FacilityProfile getFacilityProfile() {
        return facilityProfile;
    }

    public void setFacilityProfile(FacilityProfile facilityProfile) {
        this.facilityProfile = facilityProfile;
    }

}