/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 18-Aug-2012
 *  @author vibhu
 */
package com.uniware.core.api.admin.shipping;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author vibhu
 */
public class UpdateShippingProviderParametersResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 4092241311369648630L;

}
