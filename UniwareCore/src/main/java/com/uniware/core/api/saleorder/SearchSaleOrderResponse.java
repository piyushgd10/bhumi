/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 15, 2012
 *  @author praveeng
 */
package com.uniware.core.api.saleorder;

import com.unifier.core.api.base.ServiceResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * @author praveeng
 */
public class SearchSaleOrderResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long  serialVersionUID = 3731332708262428826L;
    private Long               totalRecords;
    private List<SaleOrderDTO> elements         = new ArrayList<SaleOrderDTO>();

    /**
     * @return the totalRecords
     */
    public Long getTotalRecords() {
        return totalRecords;
    }

    /**
     * @param totalRecords the totalRecords to set
     */
    public void setTotalRecords(Long totalRecords) {
        this.totalRecords = totalRecords;
    }

    /**
     * @return the elements
     */
    public List<SaleOrderDTO> getElements() {
        return elements;
    }

    /**
     * @param elements the elements to set
     */
    public void setElements(List<SaleOrderDTO> elements) {
        this.elements = elements;
    }
}
