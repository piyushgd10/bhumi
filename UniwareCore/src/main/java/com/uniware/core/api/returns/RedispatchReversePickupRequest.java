/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 20-Jun-2013
 *  @author karunsingla
 */
package com.uniware.core.api.returns;

import com.unifier.core.api.base.ServiceRequest;

import org.hibernate.validator.constraints.NotBlank;

public class RedispatchReversePickupRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 7893025794698208701L;

    @NotBlank
    private String            reversePickupCode;

    @NotBlank
    private String            shippingProviderCode;

    private String            trackingNumber;

    private String            redispatchReason;

    private String            redispatchManifestCode;

    public String getReversePickupCode() {
        return reversePickupCode;
    }

    public void setReversePickupCode(String reversePickupCode) {
        this.reversePickupCode = reversePickupCode;
    }

    public String getShippingProviderCode() {
        return shippingProviderCode;
    }

    public void setShippingProviderCode(String shippingProviderCode) {
        this.shippingProviderCode = shippingProviderCode;
    }

    public String getRedispatchReason() {
        return redispatchReason;
    }

    public void setRedispatchReason(String redispatchReason) {
        this.redispatchReason = redispatchReason;
    }

    public String getTrackingNumber() {
        return trackingNumber;
    }

    public void setTrackingNumber(String trackingNumber) {
        this.trackingNumber = trackingNumber;
    }

    public String getRedispatchManifestCode() {
        return redispatchManifestCode;
    }

    public void setRedispatchManifestCode(String redispatchManifestCode) {
        this.redispatchManifestCode = redispatchManifestCode;
    }
}
