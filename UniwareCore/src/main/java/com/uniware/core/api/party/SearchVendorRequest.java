/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 20, 2012
 *  @author praveeng
 */
package com.uniware.core.api.party;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author praveeng
 */
public class SearchVendorRequest extends ServiceRequest {

    private static final long serialVersionUID = -537850274530655219L;

    private String            keyword;

    public SearchVendorRequest(String keyword) {
        super();
        this.keyword = keyword;
    }

    public SearchVendorRequest() {
        super();
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

}
