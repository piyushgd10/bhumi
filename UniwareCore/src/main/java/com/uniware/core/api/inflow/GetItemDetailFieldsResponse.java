/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 28-May-2012
 *  @author vibhu
 */
package com.uniware.core.api.inflow;

import com.unifier.core.api.base.ServiceResponse;

import java.util.ArrayList;
import java.util.List;

import com.uniware.core.api.item.ItemDetailFieldDTO;

/**
 * @author vibhu
 */
public class GetItemDetailFieldsResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long        serialVersionUID    = -1397577435642779908L;

    private List<ItemDetailFieldDTO> itemDetailFieldDTOs = new ArrayList<ItemDetailFieldDTO>();

    /**
     * @return the customFieldDTOs
     */
    public List<ItemDetailFieldDTO> getItemDetailFieldDTOs() {
        return itemDetailFieldDTOs;
    }

    /**
     * @param customFieldDTOs the customFieldDTOs to set
     */
    public void setItemDetailFieldDTOs(List<ItemDetailFieldDTO> itemDetailFieldDTOs) {
        this.itemDetailFieldDTOs = itemDetailFieldDTOs;
    }

}