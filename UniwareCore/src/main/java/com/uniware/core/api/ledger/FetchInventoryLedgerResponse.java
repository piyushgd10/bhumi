/**
 * Copyright 2017 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version 1.0, 19/09/17
 * @author aditya
 */
package com.uniware.core.api.ledger;

import java.util.List;

import com.unifier.core.api.base.ServiceResponse;

/**
 * Response for fetching ledger entries for a particular sku.
 * <ul>
 * Contains:
 * <li>{@link #totalCount}: Total count of ledger entries for a particular sku and date range, as specified in
 * {@link FetchInventoryLedgerRequest}. This is <b>different from size of {@link #inventoryLedgerDTOs}</b>, due to
 * pagination.</li>
 * <li>{@link #openingBalance}: Opening balance for the sku in the specified date range. <br/>
 * Example: if ledger entries were queried between '2017-11-20' and '2017-11-30', then opening balance would be the
 * {@code oldBalance} of the first entry created after '2017-11-20'.</li>
 * <li>{@link #inventoryLedgerDTOs}: List containing {@link InventoryLedgerDTO}s</li>
 * </ul>
 */
public class FetchInventoryLedgerResponse extends ServiceResponse {
    private static final long        serialVersionUID = 6932885319374808192L;

    private Long                     totalCount;

    private Integer                  openingBalance;

    private List<InventoryLedgerDTO> inventoryLedgerDTOs;

    public Long getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Long totalCount) {
        this.totalCount = totalCount;
    }

    public Integer getOpeningBalance() {
        return openingBalance;
    }

    public void setOpeningBalance(Integer openingBalance) {
        this.openingBalance = openingBalance;
    }

    public List<InventoryLedgerDTO> getInventoryLedgerDTOs() {
        return inventoryLedgerDTOs;
    }

    public void setInventoryLedgerDTOs(List<InventoryLedgerDTO> inventoryLedgerDTOs) {
        this.inventoryLedgerDTOs = inventoryLedgerDTOs;
    }
}
