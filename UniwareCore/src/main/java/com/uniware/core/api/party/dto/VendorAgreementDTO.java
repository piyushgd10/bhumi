/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 21, 2012
 *  @author praveeng
 */
package com.uniware.core.api.party.dto;

import java.util.Date;

import com.uniware.core.entity.VendorAgreement;

/**
 * @author praveeng
 */
public class VendorAgreementDTO {
    private int    id;
    private String vendorAgreementStatus;
    private String name;
    private Date   startTime;
    private Date   endTime;
    private String agreementText;

    /**
     * @param agreement
     */
    public VendorAgreementDTO(VendorAgreement agreement) {
        this.id = agreement.getId();
        this.vendorAgreementStatus = agreement.getStatusCode();
        this.name = agreement.getName();
        this.startTime = agreement.getStartTime();
        this.endTime = agreement.getEndTime();
        this.agreementText = agreement.getAgreementText();
    }

    /**
     * 
     */
    public VendorAgreementDTO() {
    }

    /**
     * @return the vendorAgreementStatus
     */
    public String getVendorAgreementStatus() {
        return vendorAgreementStatus;
    }

    /**
     * @param vendorAgreementStatus the vendorAgreementStatus to set
     */
    public void setVendorAgreementStatus(String vendorAgreementStatus) {
        this.vendorAgreementStatus = vendorAgreementStatus;
    }

    /**
     * @return the agreementText
     */
    public String getAgreementText() {
        return agreementText;
    }

    /**
     * @param agreementText the agreementText to set
     */
    public void setAgreementText(String agreementText) {
        this.agreementText = agreementText;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the startTime
     */
    public Date getStartTime() {
        return startTime;
    }

    /**
     * @return the endTime
     */
    public Date getEndTime() {
        return endTime;
    }

    /**
     * @param startTime the startTime to set
     */
    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    /**
     * @param endTime the endTime to set
     */
    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

}
