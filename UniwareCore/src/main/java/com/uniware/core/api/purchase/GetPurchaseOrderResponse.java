/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 2, 2012
 *  @author praveeng
 */
package com.uniware.core.api.purchase;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.unifier.core.api.base.ServiceResponse;
import com.unifier.core.api.customfields.CustomFieldMetadataDTO;
import com.uniware.core.api.party.PartyAddressDTO;

/**
 * @author praveeng
 */
public class GetPurchaseOrderResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long            serialVersionUID   = -8596367948859920058L;
    private Integer                      id;
    private String                       amendedPurchaseOrderCode;
    private String                       amendmentPurchaseOrderCode;
    private String                       name;
    private String                       code;
    private String                       type;
    private String                       fromParty;
    private String                       statusCode;
    private String                       vendorCode;
    private Integer                      vendorId;
    private String                       vendorName;
    private Date                         created;
    private String                       createdBy;
    private Date                         expiryDate;
    private Date                         deliveryDate;
    private String                       vendorAgreementName;
    private Integer                      inflowReceiptsCount;
    private List<CustomFieldMetadataDTO> customFieldValues;
    private List<PurchaseOrderItemDTO>   purchaseOrderItems = new ArrayList<PurchaseOrderItemDTO>();
    private PartyAddressDTO              partyAddressDTO;
    private BigDecimal                   logisticCharges;
    private String                       logisticChargesDivisionMethod;
    private PurchaseOrderPriceSummary    purchaseOrderPriceSummary;

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    public Integer getVendorId() {
        return vendorId;
    }

    public void setVendorId(Integer vendorId) {
        this.vendorId = vendorId;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the fromParty
     */
    public String getFromParty() {
        return fromParty;
    }

    /**
     * @param fromParty the fromParty to set
     */
    public void setFromParty(String fromParty) {
        this.fromParty = fromParty;
    }

    /**
     * @return the statusCode
     */
    public String getStatusCode() {
        return statusCode;
    }

    /**
     * @param statusCode the statusCode to set
     */
    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    /**
     * @return the vendorCode
     */
    public String getVendorCode() {
        return vendorCode;
    }

    /**
     * @param vendorCode the vendorCode to set
     */
    public void setVendorCode(String vendorCode) {
        this.vendorCode = vendorCode;
    }

    /**
     * @return the vendorName
     */
    public String getVendorName() {
        return vendorName;
    }

    /**
     * @param vendorName the vendorName to set
     */
    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    /**
     * @return the created
     */
    public Date getCreated() {
        return created;
    }

    /**
     * @param created the created to set
     */
    public void setCreated(Date created) {
        this.created = created;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    /**
     * @return the vendorAgreementName
     */
    public String getVendorAgreementName() {
        return vendorAgreementName;
    }

    /**
     * @param vendorAgreementName the vendorAgreementName to set
     */
    public void setVendorAgreementName(String vendorAgreementName) {
        this.vendorAgreementName = vendorAgreementName;
    }

    /**
     * @return the inflowReceiptsCount
     */
    public Integer getInflowReceiptsCount() {
        return inflowReceiptsCount;
    }

    /**
     * @param inflowReceiptsCount the inflowReceiptsCount to set
     */
    public void setInflowReceiptsCount(Integer inflowReceiptsCount) {
        this.inflowReceiptsCount = inflowReceiptsCount;
    }

    /**
     * @return the purchaseOrderItems
     */
    public List<PurchaseOrderItemDTO> getPurchaseOrderItems() {
        return purchaseOrderItems;
    }

    /**
     * @param purchaseOrderItems the purchaseOrderItems to set
     */
    public void setPurchaseOrderItems(List<PurchaseOrderItemDTO> purchaseOrderItems) {
        this.purchaseOrderItems = purchaseOrderItems;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the expiryDate
     */
    public Date getExpiryDate() {
        return expiryDate;
    }

    /**
     * @param expiryDate the expiryDate to set
     */
    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    /**
     * @return the deliveryDate
     */
    public Date getDeliveryDate() {
        return deliveryDate;
    }

    /**
     * @param deliveryDate the deliveryDate to set
     */
    public void setDeliveryDate(Date deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    /**
     * @return the customFieldValues
     */
    public List<CustomFieldMetadataDTO> getCustomFieldValues() {
        return customFieldValues;
    }

    /**
     * @param customFieldValues the customFieldValues to set
     */
    public void setCustomFieldValues(List<CustomFieldMetadataDTO> customFieldValues) {
        this.customFieldValues = customFieldValues;
    }

    /**
     * @return the amendedPurchaseOrderCode
     */
    public String getAmendedPurchaseOrderCode() {
        return amendedPurchaseOrderCode;
    }

    /**
     * @param amendedPurchaseOrderCode the amendedPurchaseOrderCode to set
     */
    public void setAmendedPurchaseOrderCode(String amendedPurchaseOrderCode) {
        this.amendedPurchaseOrderCode = amendedPurchaseOrderCode;
    }

    /**
     * @return the amendmentPurchaseOrderCode
     */
    public String getAmendmentPurchaseOrderCode() {
        return amendmentPurchaseOrderCode;
    }

    /**
     * @param amendmentPurchaseOrderCode the amendmentPurchaseOrderCode to set
     */
    public void setAmendmentPurchaseOrderCode(String amendmentPurchaseOrderCode) {
        this.amendmentPurchaseOrderCode = amendmentPurchaseOrderCode;
    }

    public PartyAddressDTO getPartyAddressDTO() {
        return partyAddressDTO;
    }

    public void setPartyAddressDTO(PartyAddressDTO partyAddressDTO) {
        this.partyAddressDTO = partyAddressDTO;
    }

    /**
     * @return the logisticCharges
     */
    public BigDecimal getLogisticCharges() {
        return logisticCharges;
    }

    /**
     * @param logisticCharges the logisticCharges to set
     */
    public void setLogisticCharges(BigDecimal logisticCharges) {
        this.logisticCharges = logisticCharges;
    }

    /**
     * @return the logisticChargesDivisionMethod
     */
    public String getLogisticChargesDivisionMethod() {
        return logisticChargesDivisionMethod;
    }

    /**
     * @param logisticChargesDivisionMethod the logisticChargesDivisionMethod to set
     */
    public void setLogisticChargesDivisionMethod(String logisticChargesDivisionMethod) {
        this.logisticChargesDivisionMethod = logisticChargesDivisionMethod;
    }

    public PurchaseOrderPriceSummary getPurchaseOrderPriceSummary() {
        return purchaseOrderPriceSummary;
    }

    public void setPurchaseOrderPriceSummary(PurchaseOrderPriceSummary purchaseOrderPriceSummary) {
        this.purchaseOrderPriceSummary = purchaseOrderPriceSummary;
    }

    public static class PurchaseOrderPriceSummary {

        private BigDecimal discount                       = new BigDecimal(0);
        private BigDecimal taxOnSales                     = new BigDecimal(0);
        private BigDecimal subTotalBeforeTaxesAndDiscount = new BigDecimal(0);
        private BigDecimal totalAmount                    = new BigDecimal(0);
        private BigDecimal logisticCharges                = new BigDecimal(0);
        private int        totalItems                     = 0;

        public BigDecimal getDiscount() {
            return discount;
        }

        public void setDiscount(BigDecimal discount) {
            this.discount = discount;
        }

        public BigDecimal getTaxOnSales() {
            return taxOnSales;
        }

        public void setTaxOnSales(BigDecimal taxOnSales) {
            this.taxOnSales = taxOnSales;
        }

        public BigDecimal getLogisticCharges() {
            return logisticCharges;
        }

        public void setLogisticCharges(BigDecimal logisticCharges) {
            this.logisticCharges = logisticCharges;
        }

        public BigDecimal getSubTotalBeforeTaxesAndDiscount() {
            return subTotalBeforeTaxesAndDiscount;
        }

        public void setSubTotalBeforeTaxesAndDiscount(BigDecimal subTotalBeforeTaxesAndDiscount) {
            this.subTotalBeforeTaxesAndDiscount = subTotalBeforeTaxesAndDiscount;
        }

        public int getTotalItems() {
            return totalItems;
        }

        public void setTotalItems(int totalItems) {
            this.totalItems = totalItems;
        }

        public BigDecimal getTotalAmount() {
            return totalAmount;
        }

        public void setTotalAmount(BigDecimal totalAmount) {
            this.totalAmount = totalAmount;
        }
    }
}
