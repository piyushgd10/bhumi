/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 14, 2012
 *  @author singla
 */
package com.uniware.core.api.tenant;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author Piyush
 */
public class DeactivateTenantRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -7327015136686928284L;

}
