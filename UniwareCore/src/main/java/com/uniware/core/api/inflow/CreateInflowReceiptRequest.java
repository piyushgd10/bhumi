/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 *  @version     1.0, Mar 1, 2012
 *  @author singla
 */
package com.uniware.core.api.inflow;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author singla
 */
public class CreateInflowReceiptRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 6498416130936736814L;

    @Valid
    @NotNull
    private WsGRN             wsGRN;

    @NotBlank
    private String            purchaseOrderCode;

    private boolean           vendorInvoiceDateCheckDisable;

    /**
     * @return the purchaseOrderCode
     */
    public String getPurchaseOrderCode() {
        return purchaseOrderCode;
    }

    /**
     * @param purchaseOrderCode the purchaseOrderCode to set
     */
    public void setPurchaseOrderCode(String purchaseOrderCode) {
        this.purchaseOrderCode = purchaseOrderCode;
    }

    /**
     * @return the wsGRN
     */
    public WsGRN getWsGRN() {
        return wsGRN;
    }

    /**
     * @param wsGRN the wsGRN to set
     */
    public void setWsGRN(WsGRN wsGRN) {
        this.wsGRN = wsGRN;
    }

    public boolean isVendorInvoiceDateCheckDisable() {
        return vendorInvoiceDateCheckDisable;
    }

    public void setVendorInvoiceDateCheckDisable(boolean vendorInvoiceDateCheckDisable) {
        this.vendorInvoiceDateCheckDisable = vendorInvoiceDateCheckDisable;
    }
}