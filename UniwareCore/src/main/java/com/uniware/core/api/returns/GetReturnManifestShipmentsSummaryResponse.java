/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 03-Feb-2015
 *  @author akshaykochhar
 */
package com.uniware.core.api.returns;

import com.unifier.core.api.base.ServiceResponse;

public class GetReturnManifestShipmentsSummaryResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -297271248321912284L;
    
    private Integer totalShipments;

    public Integer getTotalShipments() {
        return totalShipments;
    }

    public void setTotalShipments(Integer totalShipments) {
        this.totalShipments = totalShipments;
    }
}
