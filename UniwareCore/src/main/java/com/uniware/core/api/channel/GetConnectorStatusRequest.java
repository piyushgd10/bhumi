/*
 * Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 6/11/15 2:01 PM
 * @author unicom
 */

package com.uniware.core.api.channel;

import com.unifier.core.api.base.ServiceRequest;

/**
 * Created by unicom on 06/11/15.
 */
public class GetConnectorStatusRequest extends ServiceRequest{
}
