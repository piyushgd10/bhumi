/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 15-Feb-2012
 *  @author vibhu
 */
package com.uniware.core.api.party;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author vibhu
 */
public class CreatePartyAddressResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 4660733825270676750L;
    private PartyAddressDTO   partyAddressDTO;

    /**
     * @return the partyAddressDTO
     */
    public PartyAddressDTO getPartyAddressDTO() {
        return partyAddressDTO;
    }

    /**
     * @param partyAddressDTO the partyAddressDTO to set
     */
    public void setPartyAddressDTO(PartyAddressDTO partyAddressDTO) {
        this.partyAddressDTO = partyAddressDTO;
    }

}
