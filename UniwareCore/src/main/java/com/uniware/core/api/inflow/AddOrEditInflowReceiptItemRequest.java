/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Apr 30, 2012
 *  @author singla
 */
package com.uniware.core.api.inflow;

import com.unifier.core.api.base.ServiceRequest;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * @author singla
 */
public class AddOrEditInflowReceiptItemRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long   serialVersionUID = 3677907372947226587L;

    @NotNull
    private String              inflowReceiptCode;

    @NotNull
    private Integer             purchaseOrderItemId;

    private Integer             inflowReceiptItemId;

    @Valid
    @NotNull
    private WsInflowReceiptItem inflowReceiptItem;

    /**
     * @return the purchaseOrderItemId
     */
    public Integer getPurchaseOrderItemId() {
        return purchaseOrderItemId;
    }

    /**
     * @param purchaseOrderItemId the purchaseOrderItemId to set
     */
    public void setPurchaseOrderItemId(Integer purchaseOrderItemId) {
        this.purchaseOrderItemId = purchaseOrderItemId;
    }

    /**
     * @return the inflowReceiptItem
     */
    public WsInflowReceiptItem getInflowReceiptItem() {
        return inflowReceiptItem;
    }

    /**
     * @param inflowReceiptItem the inflowReceiptItem to set
     */
    public void setInflowReceiptItem(WsInflowReceiptItem inflowReceiptItem) {
        this.inflowReceiptItem = inflowReceiptItem;
    }

    /**
     * @return the inflowReceiptCode
     */
    public String getInflowReceiptCode() {
        return inflowReceiptCode;
    }

    /**
     * @param inflowReceiptCode the inflowReceiptCode to set
     */
    public void setInflowReceiptCode(String inflowReceiptCode) {
        this.inflowReceiptCode = inflowReceiptCode;
    }

    /**
     * @return the inflowReceiptItemId
     */
    public Integer getInflowReceiptItemId() {
        return inflowReceiptItemId;
    }

    /**
     * @param inflowReceiptItemId the inflowReceiptItemId to set
     */
    public void setInflowReceiptItemId(Integer inflowReceiptItemId) {
        this.inflowReceiptItemId = inflowReceiptItemId;
    }

}
