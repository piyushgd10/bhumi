/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 *  @version     1.0, 30-Aug-2012
 *  @author akshay
 */
package com.uniware.core.api.inflow;

import com.unifier.core.api.base.ServiceRequest;

import javax.validation.Valid;

/**
 * @author akshay
 */
public class EditInflowReceiptRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -5055711571998721241L;

    @Valid
    private WsGRN             wsGRN;

    private String  inflowReceiptCode;


    /**
     * @return the inflowReceiptCode
     */
    public String getInflowReceiptCode() {
        return inflowReceiptCode;
    }

    /**
     * @param inflowReceiptCode the inflowReceiptCode to set
     */
    public void setInflowReceiptCode(String inflowReceiptCode) {
        this.inflowReceiptCode = inflowReceiptCode;
    }

    /**
     * @return the wsGRN
     */
    public WsGRN getWsGRN() {
        return wsGRN;
    }

    /**
     * @param wsGRN the wsGRN to set
     */
    public void setWsGRN(WsGRN wsGRN) {
        this.wsGRN = wsGRN;
    }

}
