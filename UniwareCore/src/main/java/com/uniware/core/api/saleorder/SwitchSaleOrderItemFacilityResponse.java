/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Oct 1, 2012
 *  @author singla
 */
package com.uniware.core.api.saleorder;

import com.unifier.core.api.base.ServiceResponse;

import java.util.ArrayList;
import java.util.List;

public class SwitchSaleOrderItemFacilityResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID             = 8448681562574163235L;

    private List<String>      successfulSaleOrderItemCodes = new ArrayList<String>();

    /**
     * @return the successfulSaleOrderItemCodes
     */
    public List<String> getSuccessfulSaleOrderItemCodes() {
        return successfulSaleOrderItemCodes;
    }

    public void setSuccessfulSaleOrderItemCodes(List<String> successfulSaleOrderItemCodes) {
        this.successfulSaleOrderItemCodes = successfulSaleOrderItemCodes;
    }
}
