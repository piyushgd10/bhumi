package com.uniware.core.api.cyclecount;

import java.util.Date;

import javax.validation.constraints.Future;
import javax.validation.constraints.NotNull;

import com.unifier.core.api.base.ServiceRequest;

/**
 * Created by harshpal on 2/16/16.
 */
public class CreateCycleCountRequest extends ServiceRequest {

    @NotNull
    @Future
    private Date targetCompletionDate;

    public Date getTargetCompletionDate() {
        return targetCompletionDate;
    }

    public void setTargetCompletionDate(Date targetCompletionDate) {
        this.targetCompletionDate = targetCompletionDate;
    }

}
