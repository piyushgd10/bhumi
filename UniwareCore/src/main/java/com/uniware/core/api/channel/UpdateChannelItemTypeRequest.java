/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 9, 2012
 *  @author praveeng
 */
package com.uniware.core.api.channel;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author Sunny Agarwal
 */
public class UpdateChannelItemTypeRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -5149174516181942588L;
    @Valid
    @NotNull
    private WsChannelItemType wsChannelItemType;

    public UpdateChannelItemTypeRequest() {
        super();
    }

    public UpdateChannelItemTypeRequest(WsChannelItemType wsChannelItemType) {
        super();
        this.wsChannelItemType = wsChannelItemType;
    }

    public WsChannelItemType getWsChannelItemType() {
        return wsChannelItemType;
    }

    public void setWsChannelItemType(WsChannelItemType wsChannelItemType) {
        this.wsChannelItemType = wsChannelItemType;
    }

}
