/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Nov 27, 2012
 *  @author singla
 */
package com.uniware.core.api.inventory;

import com.unifier.core.api.base.ServiceResponse;

import java.util.ArrayList;
import java.util.List;

public class GetInventorySnapshotResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long                 serialVersionUID = -2949978454668111350L;

    private List<WsItemTypeInventorySnapshot> inventorySnapshots;

    /**
     * @return the inventorySnapshots
     */
    public List<WsItemTypeInventorySnapshot> getInventorySnapshots() {
        return inventorySnapshots;
    }

    /**
     * @param inventorySnapshots the inventorySnapshots to set
     */
    public void addInventorySnapshots(WsItemTypeInventorySnapshot inventorySnapshot) {
        if (inventorySnapshots == null) {
            inventorySnapshots = new ArrayList<WsItemTypeInventorySnapshot>();
        }
        inventorySnapshots.add(inventorySnapshot);
    }
}
