/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 24-Dec-2011
 *  @author vibhu
 */
package com.uniware.core.api.warehouse;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author vibhu
 */
public class EditShelfTypeResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 2620227928985499987L;
    private ShelfTypeDTO      shelfTypeDTO;

    /**
     * @return the shelfTypeDTO
     */
    public ShelfTypeDTO getShelfTypeDTO() {
        return shelfTypeDTO;
    }

    /**
     * @param shelfTypeDTO the shelfTypeDTO to set
     */
    public void setShelfTypeDTO(ShelfTypeDTO shelfTypeDTO) {
        this.shelfTypeDTO = shelfTypeDTO;
    }
}