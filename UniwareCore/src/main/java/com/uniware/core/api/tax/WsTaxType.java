/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 20-Jul-2012
 *  @author praveeng
 */
package com.uniware.core.api.tax;

import javax.validation.constraints.Size;

/**
 * @author praveeng
 */
public class WsTaxType {
    @Size(max = 45, min = 1)
    private String  code;

    @Size(max = 100, min = 1)
    private String  name;

    private boolean gst;

    public WsTaxType() {
    }

    /**
     * @param code
     * @param name
     */
    public WsTaxType(String code, String name) {
        super();
        this.code = code;
        this.name = name;
    }

    public WsTaxType(String code, String name, boolean gst) {
        super();
        this.code = code;
        this.name = name;
        this.gst = gst;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    public boolean isGst() {
        return gst;
    }

    public void setGst(boolean gst) {
        this.gst = gst;
    }
}
