/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 19-Sep-2012
 *  @author praveeng
 */
package com.uniware.core.api.catalog;

import com.unifier.core.api.base.ServiceResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * @author praveeng
 */
public class SearchShippingProviderLocationResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long                       serialVersionUID = -2911962647110948800L;
    private Long                                    totalRecords;
    private final List<ShippingProviderLocationDTO> elements         = new ArrayList<ShippingProviderLocationDTO>();

    /**
     * @return the totalRecords
     */
    public Long getTotalRecords() {
        return totalRecords;
    }

    /**
     * @param totalRecords the totalRecords to set
     */
    public void setTotalRecords(Long totalRecords) {
        this.totalRecords = totalRecords;
    }

    /**
     * @return the elements
     */
    public List<ShippingProviderLocationDTO> getElements() {
        return elements;
    }

    public static class ShippingProviderLocationDTO {
        private Integer id;
        private String  shippingProvider;
        private String  shippingProviderCode;
        private String  shippingMethod;
        private String  shippingMethodCode;
        private String  shippingProviderRegion;
        private String  pincode;
        private String  routingCode;
        private boolean cod;
        private int     priority;
        private boolean enabled;

        /**
         * @return the id
         */
        public Integer getId() {
            return id;
        }

        /**
         * @param id the id to set
         */
        public void setId(Integer id) {
            this.id = id;
        }

        /**
         * @return the shippingProvider
         */
        public String getShippingProvider() {
            return shippingProvider;
        }

        /**
         * @param shippingProvider the shippingProvider to set
         */
        public void setShippingProvider(String shippingProvider) {
            this.shippingProvider = shippingProvider;
        }

        /**
         * @return the shippingMethod
         */
        public String getShippingMethod() {
            return shippingMethod;
        }

        /**
         * @param shippingMethod the shippingMethod to set
         */
        public void setShippingMethod(String shippingMethod) {
            this.shippingMethod = shippingMethod;
        }

        /**
         * @return the shippingProviderRegion
         */
        public String getShippingProviderRegion() {
            return shippingProviderRegion;
        }

        /**
         * @param shippingProviderRegion the shippingProviderRegion to set
         */
        public void setShippingProviderRegion(String shippingProviderRegion) {
            this.shippingProviderRegion = shippingProviderRegion;
        }

        /**
         * @return the pincode
         */
        public String getPincode() {
            return pincode;
        }

        /**
         * @param pincode the pincode to set
         */
        public void setPincode(String pincode) {
            this.pincode = pincode;
        }

        /**
         * @return the routingCode
         */
        public String getRoutingCode() {
            return routingCode;
        }

        /**
         * @param routingCode the routingCode to set
         */
        public void setRoutingCode(String routingCode) {
            this.routingCode = routingCode;
        }

        /**
         * @return the priority
         */
        public int getPriority() {
            return priority;
        }

        /**
         * @param priority the priority to set
         */
        public void setPriority(int priority) {
            this.priority = priority;
        }

        /**
         * @return the enabled
         */
        public boolean isEnabled() {
            return enabled;
        }

        /**
         * @param enabled the enabled to set
         */
        public void setEnabled(boolean enabled) {
            this.enabled = enabled;
        }

        /**
         * @return the shippingProviderCode
         */
        public String getShippingProviderCode() {
            return shippingProviderCode;
        }

        /**
         * @param shippingProviderCode the shippingProviderCode to set
         */
        public void setShippingProviderCode(String shippingProviderCode) {
            this.shippingProviderCode = shippingProviderCode;
        }

        /**
         * @return the shippingMethodCode
         */
        public String getShippingMethodCode() {
            return shippingMethodCode;
        }

        /**
         * @param shippingMethodCode the shippingMethodCode to set
         */
        public void setShippingMethodCode(String shippingMethodCode) {
            this.shippingMethodCode = shippingMethodCode;
        }

        /**
         * @return the cod
         */
        public boolean isCod() {
            return cod;
        }

        /**
         * @param cod the cod to set
         */
        public void setCod(boolean cod) {
            this.cod = cod;
        }

    }

}
