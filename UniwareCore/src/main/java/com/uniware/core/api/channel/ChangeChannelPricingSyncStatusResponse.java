/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Oct 14, 2015
 *  @author akshay
 */
package com.uniware.core.api.channel;

import com.unifier.core.api.base.ServiceResponse;

public class ChangeChannelPricingSyncStatusResponse extends ServiceResponse{

    private static final long serialVersionUID = 2109532536888727702L;

}
