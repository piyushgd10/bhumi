/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 14-May-2012
 *  @author praveeng
 */
package com.uniware.core.api.returns;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author praveeng
 */
public class ScanReturnShipmentRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 2683328972359385984L;
    private String            shipmentCode;

    /**
     * @return the shipmentCode
     */
    public String getShipmentCode() {
        return shipmentCode;
    }

    /**
     * @param shipmentCode the shipmentCode to set
     */
    public void setShipmentCode(String shipmentCode) {
        this.shipmentCode = shipmentCode;
    }

}
