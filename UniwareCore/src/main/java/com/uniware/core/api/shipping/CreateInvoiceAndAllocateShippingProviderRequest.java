/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Apr 18, 2012
 *  @author singla
 */
package com.uniware.core.api.shipping;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;
import com.uniware.core.api.invoice.WsTaxInformation;

/**
 * @author singla
 */
public class CreateInvoiceAndAllocateShippingProviderRequest extends ServiceRequest {

    /**
     *
     */
    private static final long serialVersionUID = -8669512589911806312L;

    @NotBlank
    private String            shippingPackageCode;

    @NotNull
    private Integer           userId;

    private WsTaxInformation  taxInformation;

    /**
     * @return the shippingPackageCode
     */
    public String getShippingPackageCode() {
        return shippingPackageCode;
    }

    /**
     * @param shippingPackageCode the shippingPackageCode to set
     */
    public void setShippingPackageCode(String shippingPackageCode) {
        this.shippingPackageCode = shippingPackageCode;
    }

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public WsTaxInformation getTaxInformation() {
        return taxInformation;
    }

    public void setTaxInformation(WsTaxInformation taxInformation) {
        this.taxInformation = taxInformation;
    }
}
