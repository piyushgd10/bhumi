/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 24-Dec-2011
 *  @author vibhu
 */
package com.uniware.core.api.catalog;

import javax.validation.constraints.Min;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author vibhu
 */
public class WsCategory {

    @NotEmpty
    @Length(max = 45, min = 1)
    private String code;

    @NotEmpty
    @Length(max = 200, min = 1)
    private String name;

    private String taxTypeCode;

    private String itemDetailFieldsText;

    private String hsnCode;

    private String gstTaxTypeCode;

    @Min(value = 0)
    private int     grnExpiryTolerance;

    @Min(value = 0)
    private int     dispatchExpiryTolerance;

    @Min(value = 0)
    private int     returnExpiryTolerance;

    private boolean expirable;

    @Min(value = 0)
    private int shelfLife;

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the customFieldsText
     */
    public String getItemDetailFieldsText() {
        return itemDetailFieldsText;
    }

    /**
     * @param customFieldsText the customFieldsText to set
     */
    public void setItemDetailFieldsText(String customFieldsText) {
        this.itemDetailFieldsText = customFieldsText;
    }

    /**
     * @return the taxTypeCode
     */
    public String getTaxTypeCode() {
        return taxTypeCode;
    }

    /**
     * @param taxTypeCode the taxTypeCode to set
     */
    public void setTaxTypeCode(String taxTypeCode) {
        this.taxTypeCode = taxTypeCode;
    }

    public String getHsnCode() {
        return hsnCode;
    }

    public void setHsnCode(String hsnCode) {
        this.hsnCode = hsnCode;
    }

    public String getGstTaxTypeCode() {
        return gstTaxTypeCode;
    }

    public void setGstTaxTypeCode(String gstTaxTypeCode) {
        this.gstTaxTypeCode = gstTaxTypeCode;
    }

    public int getGrnExpiryTolerance() {
        return grnExpiryTolerance;
    }

    public void setGrnExpiryTolerance(int grnExpiryTolerance) {
        this.grnExpiryTolerance = grnExpiryTolerance;
    }

    public int getDispatchExpiryTolerance() {
        return dispatchExpiryTolerance;
    }

    public void setDispatchExpiryTolerance(int dispatchExpiryTolerance) {
        this.dispatchExpiryTolerance = dispatchExpiryTolerance;
    }

    public int getReturnExpiryTolerance() {
        return returnExpiryTolerance;
    }

    public void setReturnExpiryTolerance(int returnExpiryTolerance) {
        this.returnExpiryTolerance = returnExpiryTolerance;
    }

    public boolean getExpirable() {
        return expirable;
    }

    public void setExpirable(boolean expirable) {
        this.expirable = expirable;
    }

    public int getShelfLife() {
        return shelfLife;
    }

    public void setShelfLife(int shelfLife) {
        this.shelfLife = shelfLife;
    }
}
