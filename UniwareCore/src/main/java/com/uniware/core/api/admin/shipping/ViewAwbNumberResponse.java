/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 03-Jun-2012
 *  @author vibhu
 */
package com.uniware.core.api.admin.shipping;

import com.unifier.core.api.base.ServiceResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Pankaj
 */
public class ViewAwbNumberResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 3388551590054156545L;

    private List<String>      awbNumbers       = new ArrayList<String>();

    /**
     * @return the awbNumbers
     */
    public List<String> getAwbNumbers() {
        return awbNumbers;
    }

    /**
     * @param awbNumber the awbNumber to set
     */
    public void setAwbNumbers(List<String> awbNumbers) {
        this.awbNumbers = awbNumbers;
    }
}
