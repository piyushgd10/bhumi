/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 15, 2011
 *  @author singla
 */
package com.uniware.core.api.saleorder;

import java.util.List;

import org.hibernate.validator.constraints.NotEmpty;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author Sunny Agarwal
 */
public class VerifySaleOrdersRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 8089279253752912436L;

    @NotEmpty
    private List<String>      saleOrderCodes;

    public List<String> getSaleOrderCodes() {
        return saleOrderCodes;
    }

    public void setSaleOrderCodes(List<String> saleOrderCodes) {
        this.saleOrderCodes = saleOrderCodes;
    }

}
