/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 29, 2012
 *  @author singla
 */
package com.uniware.core.api.purchase;

import com.unifier.core.api.base.ServiceResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * @author singla
 */
public class AddPurchaseOrderItemsResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long          serialVersionUID   = -6945971815712181504L;

    private List<PurchaseOrderItemDTO> purchaseOrderItems = new ArrayList<PurchaseOrderItemDTO>();

    /**
     * @return the purchaseOrderItems
     */
    public List<PurchaseOrderItemDTO> getPurchaseOrderItems() {
        return purchaseOrderItems;
    }

    /**
     * @param purchaseOrderItems the purchaseOrderItems to set
     */
    public void setPurchaseOrderItems(List<PurchaseOrderItemDTO> purchaseOrderItems) {
        this.purchaseOrderItems = purchaseOrderItems;
    }

}
