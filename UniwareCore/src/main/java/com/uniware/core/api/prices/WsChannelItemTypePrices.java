/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Oct 20, 2015
 *  @author akshay
 */
package com.uniware.core.api.prices;

import java.util.List;

public class WsChannelItemTypePrices {
    
    private List<WsChannelItemTypePrice> channelItemTypePrices;
    
    public List<WsChannelItemTypePrice> getChannelItemTypePrices() {
        return channelItemTypePrices;
    }

    public void setChannelItemTypePrices(List<WsChannelItemTypePrice> channelItemTypePrices) {
        this.channelItemTypePrices = channelItemTypePrices;
    }
}
