/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 12-Feb-2015
 *  @author parijat
 */
package com.uniware.core.api.model;

import java.util.List;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

public class WsShippingProviderDetail {

    @NotBlank
    private String shippingSourceCode;

    @NotBlank
    private String name;

    private String                 code;

    private boolean                hidden;
    private boolean                enabled;
    private String                 trackingSyncStatus;

    @NotBlank
    private String                 serviceability;

    @NotEmpty
    private List<WSShippingMethod> shippingMethods;

    public WsShippingProviderDetail() {
    }

    public WsShippingProviderDetail(String shippingSourceCode, String name, String code, boolean enabled, boolean hidden, String trackingSyncStatus, String serviceability,
            List<WSShippingMethod> shippingMethods) {
        this.shippingSourceCode = shippingSourceCode;
        this.name = name;
        this.code = code;
        this.enabled = enabled;
        this.hidden = hidden;
        this.trackingSyncStatus = trackingSyncStatus;
        this.serviceability = serviceability;
        this.shippingMethods = shippingMethods;
    }

    /**
     * @return the shippingSourceCode
     */
    public String getShippingSourceCode() {
        return shippingSourceCode;
    }

    /**
     * @param shippingSourceCode the shippingSourceCode to set
     */
    public void setShippingSourceCode(String shippingSourceCode) {
        this.shippingSourceCode = shippingSourceCode;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the enabled
     */
    public boolean isEnabled() {
        return enabled;
    }

    /**
     * @param enabled the enabled to set
     */
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public boolean isHidden() {
        return hidden;
    }

    public void setHidden(boolean hidden) {
        this.hidden = hidden;
    }

    /**
     * @return the trackingSyncStatus
     */
    public String getTrackingSyncStatus() {
        return trackingSyncStatus;
    }

    /**
     * @param trackingSyncStatus the trackingSyncStatus to set
     */
    public void setTrackingSyncStatus(String trackingSyncStatus) {
        this.trackingSyncStatus = trackingSyncStatus;
    }

    /**
     * @return the serviceability
     */
    public String getServiceability() {
        return serviceability;
    }

    /**
     * @param serviceability the serviceability to set
     */
    public void setServiceability(String serviceability) {
        this.serviceability = serviceability;
    }

    /**
     * @return the shippingMethods
     */
    public List<WSShippingMethod> getShippingMethods() {
        return shippingMethods;
    }

    /**
     * @param shippingMethods the shippingMethods to set
     */
    public void setShippingMethods(List<WSShippingMethod> shippingMethods) {
        this.shippingMethods = shippingMethods;
    }

}
