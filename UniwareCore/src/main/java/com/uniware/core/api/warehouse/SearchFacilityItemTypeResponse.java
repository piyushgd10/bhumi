/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 28, 2012
 *  @author Pankaj
 */
package com.uniware.core.api.warehouse;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author Pankaj
 */
public class SearchFacilityItemTypeResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long               serialVersionUID = 1467940006267995696L;

    private Long                            totalRecords;
    private final List<FacilityItemTypeDTO> elements         = new ArrayList<FacilityItemTypeDTO>();

    /**
     * @return the totalRecords
     */
    public Long getTotalRecords() {
        return totalRecords;
    }

    /**
     * @param totalRecords the totalRecords to set
     */
    public void setTotalRecords(Long totalRecords) {
        this.totalRecords = totalRecords;
    }

    /**
     * @return the elements
     */
    public List<FacilityItemTypeDTO> getElements() {
        return elements;
    }

    public static class FacilityItemTypeDTO {
        private String  itemSkuCode;
        private String  facilityCode;
        private String  facilitySku;
        private Integer inventory;
        private Integer priority;
        private boolean enabled;
        private String  facilityDisplayName;
        private Date    created;
        private Date    updated;

        /**
         * @return the itemSkuCode
         */
        public String getItemSkuCode() {
            return itemSkuCode;
        }

        /**
         * @param itemSkuCode the itemSkuCode to set
         */
        public void setItemSkuCode(String itemSkuCode) {
            this.itemSkuCode = itemSkuCode;
        }

        /**
         * @return the facilityCode
         */
        public String getFacilityCode() {
            return facilityCode;
        }

        /**
         * @param facilityCode the facilityCode to set
         */
        public void setFacilityCode(String facilityCode) {
            this.facilityCode = facilityCode;
        }

        /**
         * @return the inventory
         */
        public Integer getInventory() {
            return inventory;
        }

        /**
         * @param inventory the inventory to set
         */
        public void setInventory(Integer inventory) {
            this.inventory = inventory;
        }

        /**
         * @return the priority
         */
        public Integer getPriority() {
            return priority;
        }

        /**
         * @param priority the priority to set
         */
        public void setPriority(Integer priority) {
            this.priority = priority;
        }

        /**
         * @return the enabled
         */
        public boolean isEnabled() {
            return enabled;
        }

        /**
         * @param enabled the enabled to set
         */
        public void setEnabled(boolean enabled) {
            this.enabled = enabled;
        }

        /**
         * @return the facilityDisplayName
         */
        public String getFacilityDisplayName() {
            return facilityDisplayName;
        }

        /**
         * @param facilityDisplayName the facilityDisplayName to set
         */
        public void setFacilityDisplayName(String facilityDisplayName) {
            this.facilityDisplayName = facilityDisplayName;
        }

        /**
         * @return the created
         */
        public Date getCreated() {
            return created;
        }

        /**
         * @param created the created to set
         */
        public void setCreated(Date created) {
            this.created = created;
        }

        /**
         * @return the updated
         */
        public Date getUpdated() {
            return updated;
        }

        /**
         * @param updated the updated to set
         */
        public void setUpdated(Date updated) {
            this.updated = updated;
        }

    }
}
