/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 7, 2012
 *  @author singla
 */
package com.uniware.core.api.shipping;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author singla
 */
public class CloseShippingManifestResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long         serialVersionUID = 7201260112069288570L;

    private String                    shippingManifestCode;
    private ShippingManifestStatusDTO shippingManifestStatus;

    public ShippingManifestStatusDTO getShippingManifestStatus() {
        return shippingManifestStatus;
    }

    public void setShippingManifestStatus(ShippingManifestStatusDTO shippingManifestStatus) {
        this.shippingManifestStatus = shippingManifestStatus;
    }

    public String getShippingManifestCode() {
        return shippingManifestCode;
    }

    public void setShippingManifestCode(String shippingManifestCode) {
        this.shippingManifestCode = shippingManifestCode;
    }

}
