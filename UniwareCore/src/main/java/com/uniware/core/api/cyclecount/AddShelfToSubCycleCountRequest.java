package com.uniware.core.api.cyclecount;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

/**
 * Created by harshpal on 14/04/16.
 */
public class AddShelfToSubCycleCountRequest extends ServiceRequest {

    @NotBlank
    private String  subCycleCountCode;

    @NotBlank
    private String  shelfCode;

    private boolean blockOnAdd;

    public String getSubCycleCountCode() {
        return subCycleCountCode;
    }

    public void setSubCycleCountCode(String subCycleCountCode) {
        this.subCycleCountCode = subCycleCountCode;
    }

    public String getShelfCode() {
        return shelfCode;
    }

    public void setShelfCode(String shelfCode) {
        this.shelfCode = shelfCode;
    }

    public boolean isBlockOnAdd() {
        return blockOnAdd;
    }

    public void setBlockOnAdd(boolean blockOnAdd) {
        this.blockOnAdd = blockOnAdd;
    }
}
