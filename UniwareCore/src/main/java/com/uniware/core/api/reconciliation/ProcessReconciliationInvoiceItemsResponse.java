/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 15-Oct-2014
 *  @author parijat
 */
package com.uniware.core.api.reconciliation;

import java.util.ArrayList;
import java.util.List;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author parijat
 *
 */
public class ProcessReconciliationInvoiceItemsResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -7536293788346211133L;

    private Integer           successCount;
    private List<String>      failedLogs       = new ArrayList<>();
    private Integer           ignoredCount;

    /**
     * @return the successCount
     */
    public Integer getSuccessCount() {
        return successCount;
    }

    /**
     * @param successCount the successCount to set
     */
    public void setSuccessCount(Integer successCount) {
        this.successCount = successCount;
    }

    /**
     * @return the failedLogs
     */
    public List<String> getFailedLogs() {
        return failedLogs;
    }

    /**
     * @param failedLogs the failedLogs to set
     */
    public void setFailedLogs(List<String> failedLogs) {
        this.failedLogs = failedLogs;
    }

    /**
     * @return the ignoredCount
     */
    public Integer getIgnoredCount() {
        return ignoredCount;
    }

    /**
     * @param ignoredCount the ignoredCount to set
     */
    public void setIgnoredCount(Integer ignoredCount) {
        this.ignoredCount = ignoredCount;
    }

}
