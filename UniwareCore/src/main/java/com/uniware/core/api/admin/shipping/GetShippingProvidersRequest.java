/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 18-Mar-2015
 *  @author akshaykochhar
 */
package com.uniware.core.api.admin.shipping;

import com.unifier.core.api.base.ServiceRequest;

public class GetShippingProvidersRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -4125441018271606950L;

}
