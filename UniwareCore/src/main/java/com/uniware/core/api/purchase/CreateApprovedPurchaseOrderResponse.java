/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 21, 2012
 *  @author praveeng
 */
package com.uniware.core.api.purchase;

import com.unifier.core.api.base.ServiceResponse;

import org.hibernate.validator.constraints.NotBlank;

/**
 * @author praveeng
 */
public class CreateApprovedPurchaseOrderResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -5275881225723994593L;

    @NotBlank
    private String            purchaseOrderCode;

    /**
     * @return the purchaseOrderCode
     */
    public String getPurchaseOrderCode() {
        return purchaseOrderCode;
    }

    /**
     * @param purchaseOrderCode the purchaseOrderCode to set
     */
    public void setPurchaseOrderCode(String purchaseOrderCode) {
        this.purchaseOrderCode = purchaseOrderCode;
    }

}
