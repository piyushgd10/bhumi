/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 10-Jan-2014
 *  @author parijat
 */
package com.uniware.core.api.reconciliation;

import com.unifier.core.api.base.ServiceRequest;
import com.unifier.core.entity.SaleOrderReconciliation;
import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public class MarkChannelPaymentsReconciliedRequest extends ServiceRequest {

    /**
     *
     */
    private static final long serialVersionUID = -3723551333741312L;

    @NotNull
    @Valid
    private List<WSChannelReconciliationIdentifier> channelRecoIdentifiers;

    private SaleOrderReconciliation.ReconciliationStatus statusCode;

    private String totalReconcileAmount;

    /**
     * @return the channelRecoIdentifiers
     */
    public List<WSChannelReconciliationIdentifier> getChannelRecoIdentifiers() {
        return channelRecoIdentifiers;
    }

    /**
     * @param channelRecoIdentifiers the channelRecoIdentifiers to set
     */
    public void setChannelRecoIdentifiers(List<WSChannelReconciliationIdentifier> channelRecoIdentifiers) {
        this.channelRecoIdentifiers = channelRecoIdentifiers;
    }

    public SaleOrderReconciliation.ReconciliationStatus getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(SaleOrderReconciliation.ReconciliationStatus statusCode) {
        this.statusCode = statusCode;
    }

    public String getTotalReconcileAmount() {
        return totalReconcileAmount;
    }

    public void setTotalReconcileAmount(String totalReconcileAmount) {
        this.totalReconcileAmount = totalReconcileAmount;
    }
}
