/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Oct 23, 2015
 *  @author akshay
 */
package com.uniware.core.api.prices.dto;

import java.math.BigDecimal;

import com.uniware.core.entity.ChannelItemTypePrice;

public class PriceDTO {

    private BigDecimal msp;
    private BigDecimal mrp;
    private BigDecimal sellingPrice;
    private BigDecimal transferPrice;
    private BigDecimal competitivePrice;
    private String     currencyCode;

    public PriceDTO() {
        super();
    }

    public PriceDTO(BigDecimal msp, BigDecimal mrp, BigDecimal sellingPrice, BigDecimal transferPrice, BigDecimal competitivePrice, String currency) {
        super();
        this.msp = msp;
        this.mrp = mrp;
        this.sellingPrice = sellingPrice;
        this.transferPrice = transferPrice;
        this.competitivePrice = competitivePrice;
        this.currencyCode = currency;
    }

    public PriceDTO(ChannelItemTypePrice channelItemTypePrice) {
        if (channelItemTypePrice != null) {
            this.sellingPrice = channelItemTypePrice.getSellingPrice();
            this.msp = channelItemTypePrice.getMsp();
            this.mrp = channelItemTypePrice.getMrp();
            this.transferPrice = channelItemTypePrice.getTransferPrice();
            this.competitivePrice = channelItemTypePrice.getCompetitivePrice();
            this.currencyCode = channelItemTypePrice.getCurrencyCode();
        }
    }

    public BigDecimal getMsp() {
        return msp;
    }

    public void setMsp(BigDecimal msp) {
        this.msp = msp;
    }

    public BigDecimal getMrp() {
        return mrp;
    }

    public void setMrp(BigDecimal mrp) {
        this.mrp = mrp;
    }

    public BigDecimal getSellingPrice() {
        return sellingPrice;
    }

    public void setSellingPrice(BigDecimal sellingPrice) {
        this.sellingPrice = sellingPrice;
    }

    public BigDecimal getTransferPrice() {
        return transferPrice;
    }

    public void setTransferPrice(BigDecimal transferPrice) {
        this.transferPrice = transferPrice;
    }

    public BigDecimal getCompetitivePrice() {
        return competitivePrice;
    }

    public void setCompetitivePrice(BigDecimal competitivePrice) {
        this.competitivePrice = competitivePrice;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }
    
}
