/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 17, 2012
 *  @author singla
 */
package com.uniware.core.api.returns;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author singla
 */
public class CreateReshipmentResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 4540950192958130753L;

    private String            reshipmentCode;

    public String getReshipmentCode() {
        return reshipmentCode;
    }

    public void setReshipmentCode(String reshipmentCode) {
        this.reshipmentCode = reshipmentCode;
    }
}
