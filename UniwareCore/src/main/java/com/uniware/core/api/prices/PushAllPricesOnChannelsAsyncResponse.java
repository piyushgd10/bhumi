/*
    *  Copyright 2015 Unicommerce eSolutions (P) Limited All Rights Reserved.
    *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
    *  
    *  @version     1.0, Oct 19, 2015
    *  @author akshay
    */
package com.uniware.core.api.prices;

import com.unifier.core.api.base.ServiceResponse;

public class PushAllPricesOnChannelsAsyncResponse extends ServiceResponse {

    private static final long                      serialVersionUID = 5864186440119842271L;


}