package com.uniware.core.api.cyclecount;

import java.util.Set;

import org.hibernate.validator.constraints.NotEmpty;

import com.unifier.core.api.base.ServiceRequest;

/**
 * Created by harshpal on 3/21/16.
 */
public class BlockShelvesForCycleCountRequest extends ServiceRequest {

    @NotEmpty
    private Set<String> shelfCodes;

    public Set<String> getShelfCodes() {
        return shelfCodes;
    }

    public void setShelfCodes(Set<String> shelfCodes) {
        this.shelfCodes = shelfCodes;
    }
}
