package com.uniware.core.api.saleorder;

import com.unifier.core.api.base.ServiceRequest;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import java.util.List;

/**
 * Created by bhuvneshwarkumar on 25/08/15.
 */
public class AcceptSaleOrderAlternateRequest extends ServiceRequest {
    @NotBlank
    private String saleOrderCode;

    @NotEmpty
    private List<WsSaleOrderItemAlternatePair> saleOrderItemAlternatePairs;

    public static class WsSaleOrderItemAlternatePair {
        @NotBlank
        private String saleOrderItemCode;
        @NotBlank
        private String selectedAlternateItemSku;

        public String getSelectedAlternateItemSku() {
            return selectedAlternateItemSku;
        }

        public void setSelectedAlternateItemSku(String selectedAlternateItemSku) {
            this.selectedAlternateItemSku = selectedAlternateItemSku;
        }

        public String getSaleOrderItemCode() {
            return saleOrderItemCode;
        }

        public void setSaleOrderItemCode(String saleOrderItemCode) {
            this.saleOrderItemCode = saleOrderItemCode;
        }
    }

    public String getSaleOrderCode() {
        return saleOrderCode;
    }

    public void setSaleOrderCode(String saleOrderCode) {
        this.saleOrderCode = saleOrderCode;
    }

    public List<WsSaleOrderItemAlternatePair> getSaleOrderItemAlternatePairs() {
        return saleOrderItemAlternatePairs;
    }

    public void setSaleOrderItemAlternatePairs(
            List<WsSaleOrderItemAlternatePair> saleOrderItemAlternatePairs) {
        this.saleOrderItemAlternatePairs = saleOrderItemAlternatePairs;
    }
}
