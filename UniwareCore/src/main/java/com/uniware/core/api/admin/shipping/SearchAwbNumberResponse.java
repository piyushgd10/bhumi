/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 03-Jun-2012
 *  @author vibhu
 */
package com.uniware.core.api.admin.shipping;

import java.util.ArrayList;
import java.util.List;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author vibhu
 */
public class SearchAwbNumberResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long                  serialVersionUID = 5760087471094193028L;

    private List<ShippingProviderMethodAwbDTO> awbNumbers       = new ArrayList<ShippingProviderMethodAwbDTO>();

    /**
     * @return the awbNumbers
     */
    public List<ShippingProviderMethodAwbDTO> getAwbNumbers() {
        return awbNumbers;
    }

    /**
     * @param awbNumbers the awbNumbers to set
     */
    public void setAwbNumbers(List<ShippingProviderMethodAwbDTO> awbNumbers) {
        this.awbNumbers = awbNumbers;
    }

    /**
     * @param awbNumberDTO
     */
    public void addShippingProviderMethodAwbDTO(ShippingProviderMethodAwbDTO awbNumberDTO) {
        awbNumbers.add(awbNumberDTO);
    }

    public static class ShippingProviderMethodAwbDTO {

        private String  providerCode;
        private String  providerName;
        private String  methodName;
        private String  methodCode;
        private String  type;
        private boolean cashOnDelivery;
        private int     availableAwbs;

        /**
         * @return the providerCode
         */
        public String getProviderCode() {
            return providerCode;
        }

        /**
         * @param providerCode the providerCode to set
         */
        public void setProviderCode(String providerCode) {
            this.providerCode = providerCode;
        }

        /**
         * @return the providerName
         */
        public String getProviderName() {
            return providerName;
        }

        /**
         * @param providerName the providerName to set
         */
        public void setProviderName(String providerName) {
            this.providerName = providerName;
        }

        /**
         * @return the methodName
         */
        public String getMethodName() {
            return methodName;
        }

        /**
         * @param methodName the methodName to set
         */
        public void setMethodName(String methodName) {
            this.methodName = methodName;
        }

        /**
         * @return the methodCode
         */
        public String getMethodCode() {
            return methodCode;
        }

        /**
         * @param methodCode the methodCode to set
         */
        public void setMethodCode(String methodCode) {
            this.methodCode = methodCode;
        }

        /**
         * @return the cashOnDelivery
         */
        public boolean isCashOnDelivery() {
            return cashOnDelivery;
        }

        /**
         * @param cashOnDelivery the cashOnDelivery to set
         */
        public void setCashOnDelivery(boolean cashOnDelivery) {
            this.cashOnDelivery = cashOnDelivery;
        }

        /**
         * @return the availableAwbs
         */
        public int getAvailableAwbs() {
            return availableAwbs;
        }

        /**
         * @param availableAwbs the availableAwbs to set
         */
        public void setAvailableAwbs(int availableAwbs) {
            this.availableAwbs = availableAwbs;
        }

        /**
         * @return the type
         */
        public String getType() {
            return type;
        }

        /**
         * @param type the type to set
         */
        public void setType(String type) {
            this.type = type;
        }

    }

}
