/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 02-Jun-2012
 *  @author praveeng
 */
package com.uniware.core.api.transition;

import com.unifier.core.api.base.ServiceRequest;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author praveeng
 */
public class CreateInventoryRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -1848824087443145185L;

    @NotEmpty
    private String            itemTypeSkuCode;

    /**
     * @return the itemTypeSkuCode
     */
    public String getItemTypeSkuCode() {
        return itemTypeSkuCode;
    }

    /**
     * @param itemTypeSkuCode the itemTypeSkuCode to set
     */
    public void setItemTypeSkuCode(String itemTypeSkuCode) {
        this.itemTypeSkuCode = itemTypeSkuCode;
    }

}
