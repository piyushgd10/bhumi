/*
*  Copyright 2015 Unicommerce eSolutions (P) Limited . All Rights Reserved.
*  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
*  
*  @version     1.0, 06/10/15
*  @author sunny
*/

package com.uniware.core.api.invoice;

import java.math.BigDecimal;
import java.util.List;
import javax.validation.Valid;

import com.unifier.core.api.customfields.WsCustomFieldValue;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

public class WsTaxInformation {

    @NotEmpty
    @Valid
    private List<ProductTax> productTaxes;

    public List<ProductTax> getProductTaxes() {
        return productTaxes;
    }

    public void setProductTaxes(List<ProductTax> productTaxes) {
        this.productTaxes = productTaxes;
    }

    public static class ProductTax {
        @NotBlank
        private String     channelProductId;

        private String     additionalInfo;

        private BigDecimal taxPercentage     = BigDecimal.ZERO;

        private BigDecimal centralGst        = BigDecimal.ZERO;

        private BigDecimal stateGst          = BigDecimal.ZERO;

        private BigDecimal unionTerritoryGst = BigDecimal.ZERO;

        private BigDecimal integratedGst     = BigDecimal.ZERO;

        private BigDecimal compensationCess  = BigDecimal.ZERO;

        @Valid
        private List<WsCustomFieldValue> customFieldValues;

        public String getChannelProductId() {
            return channelProductId;
        }

        public void setChannelProductId(String channelProductId) {
            this.channelProductId = channelProductId;
        }

        public BigDecimal getTaxPercentage() {
            return taxPercentage;
        }

        public void setTaxPercentage(BigDecimal taxPercentage) {
            this.taxPercentage = taxPercentage;
        }

        public BigDecimal getCentralGst() {
            return centralGst;
        }

        public void setCentralGst(BigDecimal centralGst) {
            this.centralGst = centralGst;
        }

        public BigDecimal getStateGst() {
            return stateGst;
        }

        public void setStateGst(BigDecimal stateGst) {
            this.stateGst = stateGst;
        }

        public BigDecimal getUnionTerritoryGst() {
            return unionTerritoryGst;
        }

        public void setUnionTerritoryGst(BigDecimal unionTerritoryGst) {
            this.unionTerritoryGst = unionTerritoryGst;
        }

        public BigDecimal getIntegratedGst() {
            return integratedGst;
        }

        public void setIntegratedGst(BigDecimal integratedGst) {
            this.integratedGst = integratedGst;
        }

        public BigDecimal getCompensationCess() {
            return compensationCess;
        }

        public void setCompensationCess(BigDecimal compensationCess) {
            this.compensationCess = compensationCess;
        }

        public String getAdditionalInfo() {
            return additionalInfo;
        }

        public void setAdditionalInfo(String additionalInfo) {
            this.additionalInfo = additionalInfo;
        }

        public List<WsCustomFieldValue> getCustomFieldValues() {
            return customFieldValues;
        }

        public void setCustomFieldValues(List<WsCustomFieldValue> customFieldValues) {
            this.customFieldValues = customFieldValues;
        }
    }
}
