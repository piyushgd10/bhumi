/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Oct 19, 2012
 *  @author singla
 */
package com.uniware.core.api.dual.company;

import com.unifier.core.api.base.ServiceResponse;

public class ReceiveShippingPackageFromWholesaleResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -1000493016403058234L;

}
