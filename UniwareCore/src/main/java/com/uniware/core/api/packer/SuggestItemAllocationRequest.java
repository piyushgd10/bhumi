/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Apr 12, 2012
 *  @author singla
 */
package com.uniware.core.api.packer;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author singla
 */
public class SuggestItemAllocationRequest extends AbstractAllocateItemRequest{

    /**
     * 
     */
    private static final long serialVersionUID = 3305759687785811347L;
}
