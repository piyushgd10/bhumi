package com.uniware.core.api.reconciliation;

import com.unifier.core.api.base.ServiceRequest;

/**
 * Created by Sagar on 18/04/17.
 */
public class PopulateInvoiceCodesForSaleOrderReconciliationRequest extends ServiceRequest {

    private int numberOfDays;

    public PopulateInvoiceCodesForSaleOrderReconciliationRequest(int numberOfDays) {
        this.numberOfDays = numberOfDays;
    }

    public int getNumberOfDays() {
        return numberOfDays;
    }

    public void setNumberOfDays(int numberOfDays) {
        this.numberOfDays = numberOfDays;
    }
}
