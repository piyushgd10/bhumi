package com.uniware.core.api.cyclecount;

import java.util.List;

import com.unifier.core.api.base.ServiceResponse;

/**
 * Created by harshpal on 08/04/16.
 */
public class GetZonesForCycleCountResponse extends ServiceResponse {

    private List<ZoneDTO> zones;

    public List<ZoneDTO> getZones() {
        return zones;
    }

    public void setZones(List<ZoneDTO> zones) {
        this.zones = zones;
    }
}
