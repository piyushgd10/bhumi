/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 05-Jan-2015
 *  @author akshaykochhar
 */
package com.uniware.core.api.inventory;

import java.util.List;

import com.unifier.core.api.base.ServiceResponse;
import com.uniware.core.api.channel.ChannelInventoryStatusDTO;

public class GetInventorySyncResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long               serialVersionUID = -4796195014145224463L;

    private List<ChannelInventoryStatusDTO> lastInventorySyncResults;

    public List<ChannelInventoryStatusDTO> getLastInventorySyncResults() {
        return lastInventorySyncResults;
    }

    public void setLastInventorySyncResults(List<ChannelInventoryStatusDTO> lastInventorySyncResults) {
        this.lastInventorySyncResults = lastInventorySyncResults;
    }

}
