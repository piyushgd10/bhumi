/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 *  @version     1.0, Mar 9, 2012
 *  @author praveeng
 */
package com.uniware.core.api.shipping;

import java.math.BigDecimal;
import java.util.List;

import javax.validation.constraints.Min;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;
import com.unifier.core.api.customfields.WsCustomFieldValue;

/**
 * @author praveeng
 */
public class EditShippingPackageRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long        serialVersionUID = 369328216298447372L;

    @NotBlank
    private String                   shippingPackageCode;

    private String                   shippingProviderCode;

    private String                   trackingNumber;

    private String                   shippingPackageTypeCode;

    private BigDecimal               actualWeight;

    private WsShippingBox            shippingBox;

    @Min(value = 1, message = "no of boxes should be greater than 1")
    private Integer                  noOfBoxes;

    private List<WsCustomFieldValue> customFieldValues;

    /**
     * @return the shippingPackageCode
     */
    public String getShippingPackageCode() {
        return shippingPackageCode;
    }

    /**
     * @param shippingPackageCode the shippingPackageCode to set
     */
    public void setShippingPackageCode(String shippingPackageCode) {
        this.shippingPackageCode = shippingPackageCode;
    }

    /**
     * @return the shippingProviderCode
     */
    public String getShippingProviderCode() {
        return shippingProviderCode;
    }

    /**
     * @param shippingProviderCode the shippingProviderCode to set
     */
    public void setShippingProviderCode(String shippingProviderCode) {
        this.shippingProviderCode = shippingProviderCode;
    }

    /**
     * @return the shippingPackageTypeCode
     */
    public String getShippingPackageTypeCode() {
        return shippingPackageTypeCode;
    }

    /**
     * @param shippingPackageTypeCode the shippingPackageTypeCode to set
     */
    public void setShippingPackageTypeCode(String shippingPackageTypeCode) {
        this.shippingPackageTypeCode = shippingPackageTypeCode;
    }

    /**
     * @return the actualWeight
     */
    public BigDecimal getActualWeight() {
        return actualWeight;
    }

    /**
     * @param actualWeight the actualWeight to set
     */
    public void setActualWeight(BigDecimal actualWeight) {
        this.actualWeight = actualWeight;
    }

    /**
     * @return the shippingBox
     */
    public WsShippingBox getShippingBox() {
        return shippingBox;
    }

    /**
     * @return the noOfBoxes
     */
    public Integer getNoOfBoxes() {
        return noOfBoxes;
    }

    /**
     * @param noOfBoxes the noOfBoxes to set
     */
    public void setNoOfBoxes(Integer noOfBoxes) {
        this.noOfBoxes = noOfBoxes;
    }

    /**
     * @param shippingBox the shippingBox to set
     */
    public void setShippingBox(WsShippingBox shippingBox) {
        this.shippingBox = shippingBox;
    }

    /**
     * @return the customFieldValues
     */
    public List<WsCustomFieldValue> getCustomFieldValues() {
        return customFieldValues;
    }

    /**
     * @param customFieldValues the customFieldValues to set
     */
    public void setCustomFieldValues(List<WsCustomFieldValue> customFieldValues) {
        this.customFieldValues = customFieldValues;
    }

    public String getTrackingNumber() {
        return trackingNumber;
    }

    public void setTrackingNumber(String trackingNumber) {
        this.trackingNumber = trackingNumber;
    }

}
