/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, May 2, 2012
 *  @author singla
 */
package com.uniware.core.api.inflow;

import com.unifier.core.api.base.ServiceRequest;

import java.util.List;

import javax.validation.Valid;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author singla
 */
public class AddItemDetailsRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 5075533376970576413L;

    @NotBlank
    private String            itemCode;

    @NotEmpty
    @Valid
    private List<ItemDetail>  itemDetails;

    /**
     * @return the itemCode
     */
    public String getItemCode() {
        return itemCode;
    }

    /**
     * @param itemCode the itemCode to set
     */
    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    /**
     * @return the itemCustomValues
     */
    public List<ItemDetail> getItemDetails() {
        return itemDetails;
    }

    /**
     * @param itemCustomValues the itemCustomValues to set
     */
    public void setItemDetails(List<ItemDetail> itemDetails) {
        this.itemDetails = itemDetails;
    }
}
