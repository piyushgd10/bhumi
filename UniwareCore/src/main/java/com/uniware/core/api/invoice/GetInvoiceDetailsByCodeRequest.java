/*
 * Copyright 2016 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 18/4/16 2:23 PM
 * @author unicom
 */

package com.uniware.core.api.invoice;

import com.unifier.core.api.base.ServiceRequest;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Created by unicom on 18/04/16.
 */
public class GetInvoiceDetailsByCodeRequest extends ServiceRequest{

    public static final long serialVersionUID = 1L;

    @NotBlank
    private String invoiceCode;

    public String getInvoiceCode() {
        return invoiceCode;
    }

    public void setInvoiceCode(String invoiceCode) {
        this.invoiceCode = invoiceCode;
    }
}
