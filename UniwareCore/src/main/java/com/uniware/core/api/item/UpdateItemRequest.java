/**
 * Copyright 2017 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version 1.0, 04/09/17
 * @author aditya
 */
package com.uniware.core.api.item;

import java.util.Date;

import com.unifier.core.api.base.ServiceRequest;
import javax.validation.constraints.Past;
import org.hibernate.validator.constraints.NotBlank;

public class UpdateItemRequest extends ServiceRequest {
    @NotBlank
    private String itemCode;

    @NotBlank
    private String facilityCode;

    @Past
    private Date manufacturingDate;

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getFacilityCode() {
        return facilityCode;
    }

    public void setFacilityCode(String facilityCode) {
        this.facilityCode = facilityCode;
    }

    public Date getManufacturingDate() {
        return manufacturingDate;
    }

    public void setManufacturingDate(Date manufacturingDate) {
        this.manufacturingDate = manufacturingDate;
    }
}
