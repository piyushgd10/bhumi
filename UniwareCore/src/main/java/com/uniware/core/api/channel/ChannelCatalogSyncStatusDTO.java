/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 27-Apr-2013
 *  @author unicom
 */
package com.uniware.core.api.channel;

import java.io.Serializable;
import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.uniware.core.entity.Channel;

/**
 * @author Sunny
 */
@Document(collection = "channelCatalogSyncStatus")
//@CompoundIndexes({ @CompoundIndex(name = "tenant_channel", def = "{'tenantCode' :  1, 'channelCode' :  1}", unique = true) })
public class ChannelCatalogSyncStatusDTO implements Serializable {

    @Id
    private String                      id;
    private String                      channelCode;
    private String                      tenantCode;
    private Channel.SyncExecutionStatus syncExecutionStatus = Channel.SyncExecutionStatus.IDLE;
    private boolean                     interrupted;
    private boolean                     successful;
    private String                      syncId;
    private long                        totalMileStones     = 100;
    private long                        currentMileStone;
    private String                      message;
    private Long                        lastCountOnChannel;
    private int                         successfulImports;
    private int                         failedImports;
    private Date                        lastSyncTime;
    private Date                        lastSyncFailedNotificationTime;

    public ChannelCatalogSyncStatusDTO(String channelCode) {
        this.channelCode = channelCode;
    }

    public float getPercentageComplete() {
        if (totalMileStones == 0) {
            return 0;
        }
        return ((float) (currentMileStone * 10000 / totalMileStones)) / 100;
    }

    public boolean isInterrupted() {
        return interrupted;
    }

    public void setInterrupted(boolean interrupted) {
        this.interrupted = interrupted;
    }

    public Channel.SyncExecutionStatus getSyncExecutionStatus() {
        return syncExecutionStatus;
    }

    public void setSyncExecutionStatus(Channel.SyncExecutionStatus syncExecutionStatus) {
        this.syncExecutionStatus = syncExecutionStatus;
    }

    public Date getLastSyncTime() {
        return lastSyncTime;
    }

    public void setLastSyncTime(Date lastSyncTime) {
        this.lastSyncTime = lastSyncTime;
    }

    public boolean isSuccessful() {
        return successful;
    }

    public void setSuccessful(boolean successful) {
        this.successful = successful;
    }

    public String getSyncId() {
        return syncId;
    }

    public void setSyncId(String syncId) {
        this.syncId = syncId;
    }

    public long getTotalMileStones() {
        return totalMileStones;
    }

    public void setTotalMileStones(long totalMileStones) {
        this.totalMileStones = totalMileStones;
    }

    public long getCurrentMileStone() {
        return currentMileStone;
    }

    public void setCurrentMileStone(long currentMileStone) {
        this.currentMileStone = currentMileStone;
    }

    public Long getLastCountOnChannel() {
        return lastCountOnChannel;
    }

    public void setLastCountOnChannel(Long lastCountOnChannel) {
        this.lastCountOnChannel = lastCountOnChannel;
    }

    public int getSuccessfulImports() {
        return successfulImports;
    }

    public void setSuccessfulImports(int successfulImports) {
        this.successfulImports = successfulImports;
    }

    public void addSuccessfulImports(int successfulImports) {
        this.successfulImports += successfulImports;
    }

    public int getFailedImports() {
        return failedImports;
    }

    public void setFailedImports(int failedImports) {
        this.failedImports = failedImports;
    }

    public void addFailedImports(int failedImports) {
        this.failedImports += failedImports;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void reset() {
        syncExecutionStatus = Channel.SyncExecutionStatus.IDLE;
        totalMileStones = 100;
        currentMileStone = 0;
        interrupted = false;
        successful = false;
    }

    public void setMileStone(String message, int currentMileStone) {
        this.currentMileStone = currentMileStone;
        this.message = message + currentMileStone;
    }

    public Date getLastSyncFailedNotificationTime() {
        return lastSyncFailedNotificationTime;
    }

    public void setLastSyncFailedNotificationTime(Date lastSyncFailedNotificationTime) {
        this.lastSyncFailedNotificationTime = lastSyncFailedNotificationTime;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    public String getTenantCode() {
        return tenantCode;
    }

    public void setTenantCode(String tenantCode) {
        this.tenantCode = tenantCode;
    }
}