/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 20, 2012
 *  @author praveeng
 */
package com.uniware.core.api.customer;

import com.unifier.core.api.base.ServiceResponse;
import com.uniware.core.api.party.dto.CustomerDTO;

/**
 * @author Sunny
 */
public class EditCustomerResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 8628063066835167489L;
    
    private CustomerDTO customer;

    /**
     * @return the customer
     */
    public CustomerDTO getCustomer() {
        return customer;
    }

    /**
     * @param customer the customer to set
     */
    public void setCustomer(CustomerDTO customer) {
        this.customer = customer;
    }
    
}
