/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 10, 2012
 *  @author praveeng
 */
package com.uniware.core.api.catalog.dto;

import com.uniware.core.entity.Category;

/**
 * @author praveeng
 */
public class CategorySearchDTO {
    private String code;
    private String name;

    public CategorySearchDTO() {

    }

    /**
     * @param category
     */
    public CategorySearchDTO(Category category) {
        this.code = category.getCode();
        this.name = category.getName();
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }
}
