/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 30-Oct-2014
 *  @author parijat
 */
package com.uniware.core.api.reconciliation;

import com.unifier.core.api.base.ServiceResponse;
import com.uniware.core.vo.ChannelReconciliationInvoice;

/**
 * @author parijat
 *
 */
public class GetReconciliationInvoiceDetailResponse extends ServiceResponse {

    /**
     *
     */
    private static final long serialVersionUID = 4594678908765456789L;

    private ChannelReconciliationInvoice invoiceVO;

    /**
     * @return the invoiceVO
     */
    public ChannelReconciliationInvoice getInvoiceVO() {
        return invoiceVO;
    }

    /**
     * @param invoiceVO the invoiceVO to set
     */
    public void setInvoiceVO(ChannelReconciliationInvoice invoiceVO) {
        this.invoiceVO = invoiceVO;
    }

}
