/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jun 20, 2012
 *  @author ankit
 */
package com.uniware.core.api.async;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author Sunny
 */
public class AsyncServiceResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 8176212261586557873L;

    private String            requestId;

    public AsyncServiceResponse() {
        super();
    }

    public AsyncServiceResponse(String requestId) {
        super();
        this.requestId = requestId;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }
}
