package com.uniware.core.api.cyclecount;

import com.unifier.core.api.base.ServiceResponse;

/**
 * Created by harshpal on 2/17/16.
 */
public class CreateCycleCountResponse extends ServiceResponse {

    private String cycleCountCode;

    public String getCycleCountCode() {
        return cycleCountCode;
    }

    public void setCycleCountCode(String cycleCountCode) {
        this.cycleCountCode = cycleCountCode;
    }
}
