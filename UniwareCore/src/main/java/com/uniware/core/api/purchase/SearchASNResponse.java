/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 11-May-2012
 *  @author praveeng
 */
package com.uniware.core.api.purchase;

import com.unifier.core.api.base.ServiceResponse;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author praveeng
 */
public class SearchASNResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -2429507549732962899L;
    private List<WsASN>       asnList          = new ArrayList<WsASN>();

    public static class WsASN {
        private Integer id;
        private Date    expectedDeliveryDate;
        private Date    created;

        /**
         * @return the id
         */
        public Integer getId() {
            return id;
        }

        /**
         * @param id the id to set
         */
        public void setId(Integer id) {
            this.id = id;
        }

        /**
         * @return the expectedDeliveryDate
         */
        public Date getExpectedDeliveryDate() {
            return expectedDeliveryDate;
        }

        /**
         * @param expectedDeliveryDate the expectedDeliveryDate to set
         */
        public void setExpectedDeliveryDate(Date expectedDeliveryDate) {
            this.expectedDeliveryDate = expectedDeliveryDate;
        }

        /**
         * @return the created
         */
        public Date getCreated() {
            return created;
        }

        /**
         * @param created the created to set
         */
        public void setCreated(Date created) {
            this.created = created;
        }
    }

    /**
     * @return the asnList
     */
    public List<WsASN> getAsnList() {
        return asnList;
    }

    /**
     * @param asnList the asnList to set
     */
    public void setAsnList(List<WsASN> asnList) {
        this.asnList = asnList;
    }
}
