/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 18-Mar-2015
 *  @author akshaykochhar
 */
package com.uniware.core.api.admin.shipping;

import java.util.List;

import com.unifier.core.api.base.ServiceResponse;

public class GetShippingProvidersResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 8184251304756728075L;
    
    private List<ShippingProviderSearchDTO> shippingProviders;

    public List<ShippingProviderSearchDTO> getShippingProviders() {
        return shippingProviders;
    }

    public void setShippingProviders(List<ShippingProviderSearchDTO> shippingProviders) {
        this.shippingProviders = shippingProviders;
    }
    
}
