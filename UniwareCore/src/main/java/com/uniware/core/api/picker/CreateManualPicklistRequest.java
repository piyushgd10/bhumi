/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 14, 2011
 *  @author singla
 */
package com.uniware.core.api.picker;

import com.unifier.core.api.base.ServiceRequest;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author singla
 */
public class CreateManualPicklistRequest extends AbstractCreatePicklistRequest {

    private static final long serialVersionUID = -5549183275261977590L;

    @NotEmpty
    private List<String>      shippingPackageCodes;

    private Integer           noOfShipments;

    public List<String> getShippingPackageCodes() {
        return shippingPackageCodes;
    }

    public void setShippingPackageCodes(List<String> shippingPackageCodes) {
        this.shippingPackageCodes = shippingPackageCodes;
    }

    public Integer getNoOfShipments() {
        return noOfShipments;
    }

    public void setNoOfShipments(Integer noOfShipments) {
        this.noOfShipments = noOfShipments;
    }

    public CreateManualPicklistRequest() {

    }
}
