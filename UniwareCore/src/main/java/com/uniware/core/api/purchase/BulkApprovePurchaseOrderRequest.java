/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 12-Jun-2012
 *  @author vibhu
 */
package com.uniware.core.api.purchase;

import java.util.List;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author vibhu
 */
public class BulkApprovePurchaseOrderRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -2470361070657292547L;

    @NotNull
    private Integer           userId;

    @NotEmpty
    private List<String>      purchaseOrderCodes;

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return the purchaseOrderCodes
     */
    public List<String> getPurchaseOrderCodes() {
        return purchaseOrderCodes;
    }

    /**
     * @param purchaseOrderCodes the purchaseOrderCodes to set
     */
    public void setPurchaseOrderCodes(List<String> purchaseOrderCodes) {
        this.purchaseOrderCodes = purchaseOrderCodes;
    }

}
