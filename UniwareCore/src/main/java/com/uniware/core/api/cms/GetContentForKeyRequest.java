/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 30/04/14
 *  @author amit
 */

package com.uniware.core.api.cms;

import javax.validation.constraints.NotNull;

import com.unifier.core.api.base.ServiceRequest;
import com.uniware.core.vo.ContentVO;

public class GetContentForKeyRequest extends ServiceRequest {

    private static final long      serialVersionUID = -6152538473375615039L;

    /**
     * Key for which content is requested. Format: "ns:key"
     */
    @NotNull
    private String                 requestString;

    private ContentVO.LanguageCode languageCode;

    public GetContentForKeyRequest() {
        super();
    }

    public String getRequestString() {
        return requestString;
    }

    public void setRequestString(String requestString) {
        this.requestString = requestString;
    }

    public ContentVO.LanguageCode getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(ContentVO.LanguageCode languageCode) {
        this.languageCode = languageCode;
    }

    @Override
    public String toString() {
        return "GetContentForKeyRequest{" + "requestString='" + requestString + '\'' + ", languageCode=" + languageCode + '}';
    }

    public static void main(String[] args) {

    }
}
