/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jan 23, 2013
 *  @author singla
 */
package com.uniware.core.api.returns;

import com.unifier.core.api.base.ServiceRequest;

import org.hibernate.validator.constraints.NotBlank;

public class CancelReversePickupRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 8723581748141812963L;

    @NotBlank
    private String            reversePickupCode;

    /**
     * @return the reversePickupCode
     */
    public String getReversePickupCode() {
        return reversePickupCode;
    }

    /**
     * @param reversePickupCode the reversePickupCode to set
     */
    public void setReversePickupCode(String reversePickupCode) {
        this.reversePickupCode = reversePickupCode;
    }

}
