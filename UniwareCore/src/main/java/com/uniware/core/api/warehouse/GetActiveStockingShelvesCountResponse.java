/**
 * Copyright 2017 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version 1.0, 30/08/17
 * @author aditya
 */
package com.uniware.core.api.warehouse;

import com.unifier.core.api.base.ServiceResponse;

public class GetActiveStockingShelvesCountResponse extends ServiceResponse {

    private long count;

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }
}
