/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, May 8, 2012
 *  @author singla
 */
package com.uniware.core.api.putaway;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author singla
 */
public class PutawayDTO {

    private int                  id;
    private String               code;
    private String               type;
    private String               username;
    private Date                 created;
    private String               status;
    private int                  totalQuantity;
    private List<PutawayItemDTO> putawayItemDTOs = new ArrayList<PutawayItemDTO>();

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the created
     */
    public Date getCreated() {
        return created;
    }

    /**
     * @param created the created to set
     */
    public void setCreated(Date created) {
        this.created = created;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the putawayItemDTOs
     */
    public List<PutawayItemDTO> getPutawayItemDTOs() {
        return putawayItemDTOs;
    }

    /**
     * @param putawayItemDTOs the putawayItemDTOs to set
     */
    public void setPutawayItemDTOs(List<PutawayItemDTO> putawayItemDTOs) {
        this.putawayItemDTOs = putawayItemDTOs;
    }

    /**
     * @param preparePutawayItemDTO
     */
    public void addPutawayItemDTO(PutawayItemDTO putawayItemDTO) {
        this.totalQuantity += putawayItemDTO.getQuantity();
        this.putawayItemDTOs.add(putawayItemDTO);
    }

    /**
     * @return the totalQuantity
     */
    public int getTotalQuantity() {
        return totalQuantity;
    }

    /**
     * @param totalQuantity the totalQuantity to set
     */
    public void setTotalQuantity(int totalQuantity) {
        this.totalQuantity = totalQuantity;
    }

}
