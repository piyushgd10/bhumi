/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jun 20, 2012
 *  @author ankit
 */
package com.uniware.core.api.tax;

import com.unifier.core.api.base.ServiceRequest;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * @author ankit
 */
public class CreateTaxTypeConfigurationRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long      serialVersionUID = 1053545764943197444L;

    @NotNull
    @Valid
    private WsTaxTypeConfiguration taxTypeConfiguration;

    public WsTaxTypeConfiguration getTaxTypeConfiguration() {
        return taxTypeConfiguration;
    }

    public void setTaxTypeConfiguration(WsTaxTypeConfiguration taxTypeConfiguration) {
        this.taxTypeConfiguration = taxTypeConfiguration;
    }
}
