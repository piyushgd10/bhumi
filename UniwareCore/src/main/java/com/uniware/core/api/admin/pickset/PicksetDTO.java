/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 22-Jan-2012
 *  @author vibhu
 */
package com.uniware.core.api.admin.pickset;

import java.util.ArrayList;
import java.util.List;

import com.uniware.core.entity.PickSet;

/**
 * @author vibhu
 */
public class PicksetDTO {

    private int           id;
    private String        name;
    private String        type;
    private String        pickAreaName;
    private int           capacity;
    private boolean       enabled;
    private boolean       editable;
    private List<Integer> sectionIds;

    public PicksetDTO() {
    }

    public PicksetDTO(PickSet pickset) {
        this.id = pickset.getId();
        this.name = pickset.getName();
        this.type = pickset.getType().name();
        this.pickAreaName = pickset.getPickArea() != null ? pickset.getPickArea().getName() : null;
        this.capacity = pickset.getCapacity();
        this.enabled = pickset.isActive();
        this.editable = pickset.isEditable();
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the enabled
     */
    public boolean isEnabled() {
        return enabled;
    }

    /**
     * @param enabled the enabled to set
     */
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    /**
     * @return the capacity
     */
    public int getCapacity() {
        return capacity;
    }

    /**
     * @param capacity the capacity to set
     */
    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @param sectionDTO
     */
    public void addSectionId(int sectionId) {
        if (sectionIds == null) {
            sectionIds = new ArrayList<Integer>();
        }
        sectionIds.add(sectionId);
    }

    /**
     * @return the isEditable
     */
    public boolean isEditable() {
        return editable;
    }

    /**
     * @param isEditable the isEditable to set
     */
    public void setEditable(boolean isEditable) {
        this.editable = isEditable;
    }

    /**
     * @return the sectionIds
     */
    public List<Integer> getSectionIds() {
        return sectionIds;
    }

    /**
     * @param sectionIds the sectionIds to set
     */
    public void setSectionIds(List<Integer> sectionIds) {
        this.sectionIds = sectionIds;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPickAreaName() {
        return pickAreaName;
    }

    public void setPickAreaName(String pickAreaName) {
        this.pickAreaName = pickAreaName;
    }
}