/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jun 20, 2012
 *  @author singla
 */
package com.uniware.core.api.usernotification;

import java.util.List;

import com.unifier.core.api.base.ServiceResponse;
import com.uniware.core.vo.UserNotificationVO;

/**
 * @author Sunny
 */
public class GetUserNotificationsRespone extends ServiceResponse {

    /**
     * 
     */
    private static final long        serialVersionUID = 5136222167598910731L;
    private List<UserNotificationVO> userNotifications;

    public List<UserNotificationVO> getUserNotifications() {
        return userNotifications;
    }

    public void setUserNotifications(List<UserNotificationVO> userNotifications) {
        this.userNotifications = userNotifications;
    }

}
