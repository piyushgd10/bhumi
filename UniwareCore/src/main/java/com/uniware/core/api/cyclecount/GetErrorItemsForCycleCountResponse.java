package com.uniware.core.api.cyclecount;

import java.util.List;

import com.unifier.core.api.base.ServiceResponse;

/**
 * Created by harshpal on 22/04/16.
 */
public class GetErrorItemsForCycleCountResponse extends ServiceResponse {

    private List<ShelfErrorItemsDTO> shelves;

    public List<ShelfErrorItemsDTO> getShelves() {
        return shelves;
    }

    public void setShelves(List<ShelfErrorItemsDTO> shelves) {
        this.shelves = shelves;
    }

    public static class ShelfErrorItemsDTO {
        private String shelfCode;
        private Long   errorItemCount;

        public ShelfErrorItemsDTO() {
        }

        public ShelfErrorItemsDTO(String shelfCode, Long errorItemCount) {
            this.shelfCode = shelfCode;
            this.errorItemCount = errorItemCount;
        }

        public String getShelfCode() {
            return shelfCode;
        }

        public void setShelfCode(String shelfCode) {
            this.shelfCode = shelfCode;
        }

        public Long getErrorItemCount() {
            return errorItemCount;
        }

        public void setErrorItemCount(long errorItemCount) {
            this.errorItemCount = errorItemCount;
        }
    }
}
