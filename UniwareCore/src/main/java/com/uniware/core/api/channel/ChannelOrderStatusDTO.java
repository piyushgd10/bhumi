/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 27-Apr-2013
 *  @author unicom
 */
package com.uniware.core.api.channel;

import java.io.Serializable;
import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author Sunny
 */
@Document(collection = "channelShipmentSyncStatus")
//@CompoundIndexes({ @CompoundIndex(name = "tenant_channel", def = "{'tenantCode' :  1, 'channelCode' :  1}", unique = true) })
public class ChannelOrderStatusDTO implements Serializable {

    @Id
    private String  id;
    private String  channelCode;
    private String  tenantCode;
    private boolean running;
    private Date    lastSyncTime;
    private int     shipmentsScanned;
    private long    totalMileStones = 100;
    private long    currentMileStone;

    public ChannelOrderStatusDTO() {
    }

    public ChannelOrderStatusDTO(String channelCode) {
        this.channelCode = channelCode;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    public String getTenantCode() {
        return tenantCode;
    }

    public void setTenantCode(String tenantCode) {
        this.tenantCode = tenantCode;
    }

    public Date getLastSyncTime() {
        return lastSyncTime;
    }

    public void setLastSyncTime(Date lastSyncTime) {
        this.lastSyncTime = lastSyncTime;
    }

    public int getShipmentsScanned() {
        return shipmentsScanned;
    }

    public void setShipmentsScanned(int shipmentsScanned) {
        this.shipmentsScanned = shipmentsScanned;
    }

    public boolean isRunning() {
        return running;
    }

    public void setRunning(boolean running) {
        this.running = running;
    }

    public long getTotalMileStones() {
        return totalMileStones;
    }

    public void setTotalMileStones(long totalMileStones) {
        this.totalMileStones = totalMileStones;
    }

    public long getCurrentMileStone() {
        return currentMileStone;
    }

    public void setCurrentMileStone(long currentMileStone) {
        this.currentMileStone = currentMileStone;
    }

    public void incrementMileStone() {
        currentMileStone++;
    }

    public float getPercentageComplete() {
        if (totalMileStones == 0) {
            return 0;
        }
        return ((float) (currentMileStone * 10000 / totalMileStones)) / 100;
    }

    public void reset() {
        running = false;
        lastSyncTime = null;
        shipmentsScanned = 0;
        currentMileStone = 0;
        totalMileStones = 100;
    }
}