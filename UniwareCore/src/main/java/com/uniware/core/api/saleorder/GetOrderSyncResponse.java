/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 06-Jan-2015
 *  @author akshaykochhar
 */
package com.uniware.core.api.saleorder;

import java.util.List;

import com.unifier.core.api.base.ServiceResponse;
import com.uniware.core.api.channel.ChannelOrderSyncDTO;

public class GetOrderSyncResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -6595631876904069297L;

    List<ChannelOrderSyncDTO> lastOrderSyncResults;

    public List<ChannelOrderSyncDTO> getLastOrderSyncResults() {
        return lastOrderSyncResults;
    }

    public void setLastOrderSyncResults(List<ChannelOrderSyncDTO> lastOrderSyncResults) {
        this.lastOrderSyncResults = lastOrderSyncResults;
    }

}
