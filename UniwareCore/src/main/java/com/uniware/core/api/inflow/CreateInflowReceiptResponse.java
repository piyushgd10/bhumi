/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 1, 2012
 *  @author singla
 */
package com.uniware.core.api.inflow;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author singla
 */
public class CreateInflowReceiptResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -6239554958117500968L;
    private String            inflowReceiptCode;

    /**
     * @return the inflowReceiptCode
     */
    public String getInflowReceiptCode() {
        return inflowReceiptCode;
    }

    /**
     * @param inflowReceiptCode the inflowReceiptCode to set
     */
    public void setInflowReceiptCode(String inflowReceiptCode) {
        this.inflowReceiptCode = inflowReceiptCode;
    }

}
