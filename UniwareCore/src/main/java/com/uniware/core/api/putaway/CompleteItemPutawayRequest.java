/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jun 2, 2012
 *  @author singla
 */
package com.uniware.core.api.putaway;

import com.unifier.core.api.base.ServiceRequest;
import com.uniware.core.api.putaway.CompletePutawayItemsRequest.WsPutawayItem;

import java.util.List;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author singla
 */
public class CompleteItemPutawayRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long   serialVersionUID = -6999822264858899169L;

    @NotBlank
    private String              shelfCode;

    private List<String>        itemCodes;

    @NotBlank
    private String              putawayCode;

    private List<WsPutawayItem> putawayItems;

    /**
     * @return the itemCodes
     */
    public List<String> getItemCodes() {
        return itemCodes;
    }

    /**
     * @param itemCodes the itemCodes to set
     */
    public void setItemCodes(List<String> itemCodes) {
        this.itemCodes = itemCodes;
    }

    /**
     * @return the shelfCode
     */
    public String getShelfCode() {
        return shelfCode;
    }

    /**
     * @param shelfCode the shelfCode to set
     */
    public void setShelfCode(String shelfCode) {
        this.shelfCode = shelfCode;
    }

    /**
     * @return the putawayCode
     */
    public String getPutawayCode() {
        return putawayCode;
    }

    /**
     * @param putawayCode the putawayCode to set
     */
    public void setPutawayCode(String putawayCode) {
        this.putawayCode = putawayCode;
    }

    public List<WsPutawayItem> getPutawayItems() {
        return putawayItems;
    }

    public void setPutawayItems(List<WsPutawayItem> putawayItems) {
        this.putawayItems = putawayItems;
    }
}
