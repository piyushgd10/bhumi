/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 18-Apr-2012
 *  @author vibhu
 */
package com.uniware.core.api.catalog;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author vibhu
 */
public class CreateOrEditItemTypeRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -446429913412313165L;
    private WsItemType        itemType;

    /**
     * @return the itemType
     */
    public WsItemType getItemType() {
        return itemType;
    }

    /**
     * @param itemType the itemType to set
     */
    public void setItemType(WsItemType itemType) {
        this.itemType = itemType;
    }

}
