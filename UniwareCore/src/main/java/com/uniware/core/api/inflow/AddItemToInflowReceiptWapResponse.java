/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 12-Jul-2012
 *  @author praveeng
 */
package com.uniware.core.api.inflow;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author praveeng
 */
public class AddItemToInflowReceiptWapResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long    serialVersionUID = 3907031168796271373L;

    private InflowReceiptItemDTO inflowReceiptItemDTO;

    public InflowReceiptItemDTO getInflowReceiptItemDTO() {
        return inflowReceiptItemDTO;
    }

    public void setInflowReceiptItemDTO(InflowReceiptItemDTO inflowReceiptItemDTO) {
        this.inflowReceiptItemDTO = inflowReceiptItemDTO;
    }

}
