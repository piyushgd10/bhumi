/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jun 19, 2012
 *  @author singla
 */
package com.uniware.core.api.inventory;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.Past;

import org.hibernate.validator.constraints.NotEmpty;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author singla
 */
public class AddItemLabelsRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 9040409441228645995L;

    @NotEmpty
    private String            itemSkuCode;

    @NotEmpty
    private List<String>      itemCodes;

    @Past
    private Date              manufacturingDate;

    /**
     * @return the itemCodes
     */
    public List<String> getItemCodes() {
        return itemCodes;
    }

    /**
     * @param itemCodes the itemCodes to set
     */
    public void setItemCodes(List<String> itemCodes) {
        this.itemCodes = itemCodes;
    }

    /**
     * @return the itemSkuCode
     */
    public String getItemSkuCode() {
        return itemSkuCode;
    }

    /**
     * @param itemSkuCode the itemSkuCode to set
     */
    public void setItemSkuCode(String itemSkuCode) {
        this.itemSkuCode = itemSkuCode;
    }

    public Date getManufacturingDate() {
        return this.manufacturingDate;
    }

    public void setManufacturingDate(Date manufacturingDate) {
        this.manufacturingDate = manufacturingDate;
    }
}
