/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 14, 2012
 *  @author praveeng
 */
package com.uniware.core.api.warehouse;

import com.unifier.core.api.base.ServiceResponse;

import com.uniware.core.api.packer.ShippingPackageFullDTO;

/**
 * @author praveeng
 */
public class SearchShipmentResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long      serialVersionUID = 7860578329797263993L;
    private ShippingPackageFullDTO shippingPackageFullDTO;
    /**
     * @return the shippingPackageFullDTO
     */
    public ShippingPackageFullDTO getShippingPackageFullDTO() {
        return shippingPackageFullDTO;
    }
    /**
     * @param shippingPackageFullDTO the shippingPackageFullDTO to set
     */
    public void setShippingPackageFullDTO(ShippingPackageFullDTO shippingPackageFullDTO) {
        this.shippingPackageFullDTO = shippingPackageFullDTO;
    }

}
