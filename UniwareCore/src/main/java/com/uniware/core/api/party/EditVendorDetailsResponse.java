/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 30-Jan-2014
 *  @author akshay
 */
package com.uniware.core.api.party;

import com.unifier.core.api.base.ServiceResponse;

public class EditVendorDetailsResponse extends ServiceResponse{

    /**
     * 
     */
    private static final long serialVersionUID = -1499578281458315607L;

}
