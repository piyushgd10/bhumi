/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 08-Apr-2015
 *  @author akshaykochhar
 */
package com.uniware.core.api.material;

import com.unifier.core.api.base.ServiceResponse;

public class GetGatepassScannableItemDetailsResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 4695969715583627854L;

    public static class ScannedItemDetailDTO {

        private String            itemCode;
        private String            itemName;
        private String            itemSKU;
        private String            inventoryType;
        private String            shelfCode;

        public String getItemCode() {
            return itemCode;
        }

        public void setItemCode(String itemCode) {
            this.itemCode = itemCode;
        }

        public String getItemName() {
            return itemName;
        }

        public void setItemName(String itemName) {
            this.itemName = itemName;
        }

        public String getItemSKU() {
            return itemSKU;
        }

        public void setItemSKU(String itemSKU) {
            this.itemSKU = itemSKU;
        }

        public String getInventoryType() {
            return inventoryType;
        }

        public void setInventoryType(String inventoryType) {
            this.inventoryType = inventoryType;
        }

        public String getShelfCode() {
            return shelfCode;
        }

        public void setShelfCode(String shelfCode) {
            this.shelfCode = shelfCode;
        }
        
    }
    
    private ScannedItemDetailDTO scannedItem;

    public ScannedItemDetailDTO getScannedItem() {
        return scannedItem;
    }

    public void setScannedItem(ScannedItemDetailDTO scannedItem) {
        this.scannedItem = scannedItem;
    }
    
}
