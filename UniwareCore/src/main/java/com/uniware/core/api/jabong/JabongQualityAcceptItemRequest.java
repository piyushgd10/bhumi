/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jul 6, 2012
 *  @author singla
 */
package com.uniware.core.api.jabong;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

public class JabongQualityAcceptItemRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -7052058928562180473L;

    @NotBlank
    private String            itemCode;

    private String            acceptReason;

    /**
     * @return the itemCode
     */
    public String getItemCode() {
        return itemCode;
    }

    /**
     * @param itemCode the itemCode to set
     */
    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getAcceptReason() {
        return acceptReason;
    }

    public void setAcceptReason(String acceptReason) {
        this.acceptReason = acceptReason;
    }

    @Override
    public String toString() {
        return "JabongQualityAcceptItemRequest [itemCode=" + itemCode + "]";
    }

}
