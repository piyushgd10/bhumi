/**
 * Copyright 2017 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version 1.0, 09/02/17
 * @author aditya
 */
package com.uniware.core.api.cyclecount;

import java.util.Date;

import com.uniware.core.entity.ItemTypeInventory;

//Created for use in closing cycle count. Serves as a container for a query.
public class MissingInventoryDTO {
    private String                 skuCode;
    private ItemTypeInventory.Type inventoryType;
    private Long                   notFound;
    private int                    shelfId;
    private Date                   ageingStartDate;

    public MissingInventoryDTO(String skuCode, ItemTypeInventory.Type inventoryType, Long notFound, Date ageingStartDate, int shelfId) {
        this.skuCode = skuCode;
        this.inventoryType = inventoryType;
        this.notFound = notFound;
        this.shelfId = shelfId;
    }

    public MissingInventoryDTO() {
    }

    public String getSkuCode() {
        return skuCode;
    }

    public void setSkuCode(String skuCode) {
        this.skuCode = skuCode;
    }

    public ItemTypeInventory.Type getInventoryType() {
        return inventoryType;
    }

    public void setInventoryType(ItemTypeInventory.Type inventoryType) {
        this.inventoryType = inventoryType;
    }

    public Long getNotFound() {
        return notFound;
    }

    public void setNotFound(Long notFound) {
        this.notFound = notFound;
    }

    public int getShelfId() {
        return shelfId;
    }

    public void setShelfId(int shelfId) {
        this.shelfId = shelfId;
    }

    public Date getAgeingStartDate() {
        return ageingStartDate;
    }

    public void setAgeingStartDate(Date ageingStartDate) {
        this.ageingStartDate = ageingStartDate;
    }

    @Override
    public String toString() {
        return "[ Skucode:" + skuCode + ", notFound: " + notFound + ", shelfId: " + shelfId + ", ageingStartDate: " + ageingStartDate + " ]";
    }

}
