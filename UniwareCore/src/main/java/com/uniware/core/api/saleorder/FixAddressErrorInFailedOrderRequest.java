/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 10-Jun-2014
 *  @author unicom
 */
package com.uniware.core.api.saleorder;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;
import com.uniware.core.api.model.WsAddressDetail;

public class FixAddressErrorInFailedOrderRequest extends ServiceRequest {

    private static final long serialVersionUID = 8745920824846732972L;
    @NotBlank
    private String            saleOrderCode;
    @NotNull
    @Valid
    private WsAddressDetail   address;

    public String getSaleOrderCode() {
        return saleOrderCode;
    }

    public void setSaleOrderCode(String saleOrderCode) {
        this.saleOrderCode = saleOrderCode;
    }

    public WsAddressDetail getAddress() {
        return address;
    }

    public void setAddress(WsAddressDetail address) {
        this.address = address;
    }
}
