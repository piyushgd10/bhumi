/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Sep 13, 2012
 *  @author Pankaj
 */

package com.uniware.core.api.material;

/**
 * @author Pankaj
 */

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

public class EditGatePassRequest extends ServiceRequest {

    private static final long serialVersionUID = -2281658182254903242L;

    @NotBlank
    private String            gatePassCode;

    @Valid
    @NotNull
    private WsGatePass        wsGatePass;

    public WsGatePass getWsGatePass() {
        return wsGatePass;
    }

    public void setWsGatePass(WsGatePass wsGatePass) {
        this.wsGatePass = wsGatePass;
    }

    public String getGatePassCode() {
        return gatePassCode;
    }

    public void setGatePassCode(String gatePassCode) {
        this.gatePassCode = gatePassCode;
    }

}
