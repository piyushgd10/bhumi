/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jan 19, 2015
 *  @author harshpal
 */
package com.uniware.core.api.channel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.uniware.core.entity.SourceConfigurationParameter;

public class SourceConfigurationParameterDTO implements Serializable {

    private String                  name;
    private String                  displayNameKey;
    private String                  displayPlaceHolder;
    private String                  groupName;
    private String                  type;
    private Object                  defaultValue;
    private String                  javaType;
    private String                  tooltipKey;
    private List<PossibleValuesDTO> possibleValues = new ArrayList<>();
    private boolean                 required;

    public SourceConfigurationParameterDTO() {
    }

    @SuppressWarnings("unchecked")
    public SourceConfigurationParameterDTO(SourceConfigurationParameter sp) {
        name = sp.getName();
        displayNameKey = sp.getDisplayNameKey();
        displayPlaceHolder = sp.getDisplayPlaceHolder();
        setGroupName(sp.getGroupName().name());
        type = sp.getType().name();
        for (SourceConfigurationParameter.PossibleValue pv : sp.getPossibleValues()) {
            possibleValues.add(new PossibleValuesDTO(pv.getValue(), pv.getDisplayValue()));
        }
        required = sp.isRequired();
        setJavaType(sp.getJavaType());
        setTooltipKey(sp.getTooltipKey());
    }

    public String getDisplayNameKey() {
        return displayNameKey;
    }

    public void setDisplayNameKey(String displayNameKey) {
        this.displayNameKey = displayNameKey;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(Object defaultValue) {
        this.defaultValue = defaultValue;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getJavaType() {
        return javaType;
    }

    public void setJavaType(String javaType) {
        this.javaType = javaType;
    }

    public String getDisplayPlaceHolder() {
        return displayPlaceHolder;
    }

    public void setDisplayPlaceHolder(String displayPlaceHolder) {
        this.displayPlaceHolder = displayPlaceHolder;
    }

    public List<PossibleValuesDTO> getPossibleValues() {
        return possibleValues;
    }

    public void setPossibleValues(List<PossibleValuesDTO> possibleValues) {
        this.possibleValues = possibleValues;
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public String getTooltipKey() {
        return tooltipKey;
    }

    public void setTooltipKey(String tooltipKey) {
        this.tooltipKey = tooltipKey;
    }
}

class PossibleValuesDTO implements Serializable {
    String value;
    String displayValue;

    public PossibleValuesDTO() {
    }

    public PossibleValuesDTO(String value, String displayValue) {
        this.value = value;
        this.displayValue = displayValue;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getDisplayValue() {
        return displayValue;
    }

    public void setDisplayValue(String displayValue) {
        this.displayValue = displayValue;
    }

}
