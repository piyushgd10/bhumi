/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 15, 2012
 *  @author praveeng
 */
package com.uniware.core.api.saleorder;

import com.unifier.core.api.base.ServiceResponse;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author praveeng
 */
public class SearchSaleOrderItemResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long            serialVersionUID = 3731332708262428826L;
    private Long                         totalRecords;
    private List<SearchSaleOrderItemDTO> elements         = new ArrayList<SearchSaleOrderItemDTO>();

    /**
     * @return the totalRecords
     */
    public Long getTotalRecords() {
        return totalRecords;
    }

    /**
     * @param totalRecords the totalRecords to set
     */
    public void setTotalRecords(Long totalRecords) {
        this.totalRecords = totalRecords;
    }

    /**
     * @param saleOrderItem
     */
    public void add(SearchSaleOrderItemDTO searchSaleOrderItemDTO) {
        elements.add(searchSaleOrderItemDTO);
    }

    public static class SearchSaleOrderItemDTO {

        private Integer saleOrderItemId;
        private String  itemName;
        private String  itemSku;
        private String  imageUrl;
        private String  status;
        private Date    created;
        private String  saleOrderCode;

        /**
         * @return the saleOrderItemId
         */
        public Integer getSaleOrderItemId() {
            return saleOrderItemId;
        }

        /**
         * @param saleOrderItemId the saleOrderItemId to set
         */
        public void setSaleOrderItemId(Integer saleOrderItemId) {
            this.saleOrderItemId = saleOrderItemId;
        }

        /**
         * @return the itemName
         */
        public String getItemName() {
            return itemName;
        }

        /**
         * @return the itemSku
         */
        public String getItemSku() {
            return itemSku;
        }

        /**
         * @return the status
         */
        public String getStatus() {
            return status;
        }

        /**
         * @param itemName the itemName to set
         */
        public void setItemName(String itemName) {
            this.itemName = itemName;
        }

        /**
         * @param itemSku the itemSku to set
         */
        public void setItemSku(String itemSku) {
            this.itemSku = itemSku;
        }

        /**
         * @param status the status to set
         */
        public void setStatus(String status) {
            this.status = status;
        }

        /**
         * @return the saleOrderCode
         */
        public String getSaleOrderCode() {
            return saleOrderCode;
        }

        /**
         * @param saleOrderCode the saleOrderCode to set
         */
        public void setSaleOrderCode(String saleOrderCode) {
            this.saleOrderCode = saleOrderCode;
        }

        /**
         * @return the imageUrl
         */
        public String getImageUrl() {
            return imageUrl;
        }

        /**
         * @param imageUrl the imageUrl to set
         */
        public void setImageUrl(String imageUrl) {
            this.imageUrl = imageUrl;
        }

        /**
         * @return the created
         */
        public Date getCreated() {
            return created;
        }

        /**
         * @param created the created to set
         */
        public void setCreated(Date created) {
            this.created = created;
        }

    }

    /**
     * @return the elements
     */
    public List<SearchSaleOrderItemDTO> getElements() {
        return elements;
    }

    /**
     * @param elements the elements to set
     */
    public void setElements(List<SearchSaleOrderItemDTO> elements) {
        this.elements = elements;
    }
}