/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 7, 2012
 *  @author praveeng
 */
package com.uniware.core.api.putaway;

import com.unifier.core.api.base.ServiceRequest;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author praveeng
 */
public class EditPutawayItemRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 5065263501527961754L;
    @NotNull
    private Integer           putawayItemId;

    @NotEmpty
    private String            shelfCode;

    private Integer           quantity;

    /**
     * @return the shelfCode
     */
    public String getShelfCode() {
        return shelfCode;
    }

    /**
     * @param shelfCode the shelfCode to set
     */
    public void setShelfCode(String shelfCode) {
        this.shelfCode = shelfCode;
    }

    /**
     * @return the quantity
     */
    public Integer getQuantity() {
        return quantity;
    }

    /**
     * @param quantity the quantity to set
     */
    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    /**
     * @return the putawayItemId
     */
    public Integer getPutawayItemId() {
        return putawayItemId;
    }

    /**
     * @param putawayItemId the putawayItemId to set
     */
    public void setPutawayItemId(Integer putawayItemId) {
        this.putawayItemId = putawayItemId;
    }

}
