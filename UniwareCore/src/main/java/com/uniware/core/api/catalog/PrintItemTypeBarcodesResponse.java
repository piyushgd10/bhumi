/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 26-Mar-2014
 *  @author amit
 */
package com.uniware.core.api.catalog;

import com.unifier.core.api.base.ServiceResponse;

public class PrintItemTypeBarcodesResponse extends ServiceResponse {

    private static final long serialVersionUID = 866592788460569499L;

    private String            outputCsv;

    public PrintItemTypeBarcodesResponse() {
        super();
    }

    public PrintItemTypeBarcodesResponse(String outputCsv) {
        super();
        setOutputCsv(outputCsv);
    }

    public String getOutputCsv() {
        return outputCsv;
    }

    public void setOutputCsv(String outputCsv) {
        this.outputCsv = outputCsv;
    }
}
