/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 14, 2013
 *  @author karunsingla
 */
package com.uniware.core.api.shipping;

import com.unifier.core.api.base.ServiceRequest;

import org.hibernate.validator.constraints.NotBlank;

public class CompleteCustomizationForShippingPackageRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -6627613889322398304L;

    @NotBlank
    private String            shippingPackageCode;

    public String getShippingPackageCode() {
        return shippingPackageCode;
    }

    public void setShippingPackageCode(String shippingPackageCode) {
        this.shippingPackageCode = shippingPackageCode;
    }
}
