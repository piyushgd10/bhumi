/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 30-Jun-2014
 *  @author akshay
 */
package com.uniware.core.api.invoice;

import com.unifier.core.api.base.ServiceResponse;

public class GetInvoiceDetailsResponse extends ServiceResponse {

    private static final long serialVersionUID = 1L;

    private InvoiceDTO        invoice;

    public InvoiceDTO getInvoice() {
        return invoice;
    }

    public void setInvoice(InvoiceDTO invoice) {
        this.invoice = invoice;
    }

}
