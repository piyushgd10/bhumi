/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 14-Feb-2012
 *  @author vibhu
 */
package com.uniware.core.api.warehouse;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.unifier.core.annotation.constraints.OptionalNull;
import com.unifier.core.api.base.ServiceRequest;
import com.uniware.core.api.party.WsFacility;
import com.uniware.core.api.sequence.WsSequence;

/**
 * @author vibhu
 */
public class EditFacilityRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -369579850358428349L;
    @NotNull
    @Valid
    private WsFacility        facility;

    @OptionalNull
    @Valid
    private WsSequence        taxInvoiceSequence;

    @OptionalNull
    @Valid
    private WsSequence        retailInvoiceSequence;

    public WsFacility getFacility() {
        return facility;
    }

    public void setFacility(WsFacility facility) {
        this.facility = facility;
    }

    public WsSequence getTaxInvoiceSequence() {
        return taxInvoiceSequence;
    }

    public void setTaxInvoiceSequence(WsSequence taxInvoiceSequence) {
        this.taxInvoiceSequence = taxInvoiceSequence;
    }

    public WsSequence getRetailInvoiceSequence() {
        return retailInvoiceSequence;
    }

    public void setRetailInvoiceSequence(WsSequence retailInvoiceSequence) {
        this.retailInvoiceSequence = retailInvoiceSequence;
    }

}
