package com.uniware.core.api.shipping;

import com.unifier.core.api.base.ServiceRequest;

/**
 * Created by karunsingla on 26/05/14.
 */
public class GetShippingPackageTypesRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 4090815454056838086L;
}
