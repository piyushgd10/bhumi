/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 17-Jun-2013
 *  @author pankaj
 */
package com.uniware.core.api.shipping;

import com.unifier.core.api.base.ServiceResponse;

public class RemoveManifestItemResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 2676542996637807430L;

    private Integer           totalManifestItemCount;

    public Integer getTotalManifestItemCount() {
        return totalManifestItemCount;
    }

    public void setTotalManifestItemCount(Integer totalManifestItemCount) {
        this.totalManifestItemCount = totalManifestItemCount;
    }

}
