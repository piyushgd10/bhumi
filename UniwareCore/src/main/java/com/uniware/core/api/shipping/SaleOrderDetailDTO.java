/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 17-Dec-2013
 *  @author akshay
 */
package com.uniware.core.api.shipping;

import java.util.ArrayList;
import java.util.List;

import com.unifier.core.api.customfields.CustomFieldMetadataDTO;
import com.uniware.core.api.model.WsAddressDetail;
import com.uniware.core.api.saleorder.SaleOrderDTO;

public class SaleOrderDetailDTO extends SaleOrderDTO {

    private boolean                      cod;
    private int                          priority;
    private String                       currencyCode;
    private String                       customerCode;
    private WsAddressDetail              billingAddress;
    private List<WsAddressDetail>        addresses = new ArrayList<WsAddressDetail>();
    private List<CustomFieldMetadataDTO> customFieldValues;

    public boolean isCod() {
        return cod;
    }

    public void setCod(boolean cod) {
        this.cod = cod;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public WsAddressDetail getBillingAddress() {
        return billingAddress;
    }

    public void setBillingAddress(WsAddressDetail billingAddress) {
        this.billingAddress = billingAddress;
    }

    public List<WsAddressDetail> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<WsAddressDetail> addresses) {
        this.addresses = addresses;
    }

    public List<CustomFieldMetadataDTO> getCustomFieldValues() {
        return customFieldValues;
    }

    public void setCustomFieldValues(List<CustomFieldMetadataDTO> customFieldValues) {
        this.customFieldValues = customFieldValues;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

}
