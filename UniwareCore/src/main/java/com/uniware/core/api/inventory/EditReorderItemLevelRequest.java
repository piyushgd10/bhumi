/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 4, 2013
 *  @author praveeng
 */
package com.uniware.core.api.inventory;

import com.unifier.core.api.base.ServiceRequest;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

/**
 * @author praveeng
 */
public class EditReorderItemLevelRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -575354897779711914L;

    @NotBlank
    private String            skuCode;

    @NotNull
    private Integer           minBinSize;

    /**
     * @return the skuCode
     */
    public String getSkuCode() {
        return skuCode;
    }

    /**
     * @param skuCode the skuCode to set
     */
    public void setSkuCode(String skuCode) {
        this.skuCode = skuCode;
    }

    /**
     * @return the minBinSize
     */
    public Integer getMinBinSize() {
        return minBinSize;
    }

    /**
     * @param minBinSize the minBinSize to set
     */
    public void setMinBinSize(Integer minBinSize) {
        this.minBinSize = minBinSize;
    }

}
