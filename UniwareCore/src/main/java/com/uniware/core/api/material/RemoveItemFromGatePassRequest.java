/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 23-Mar-2014
 *  @author karunsingla
 */
package com.uniware.core.api.material;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

public class RemoveItemFromGatePassRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 4395460225591721252L;

    @NotBlank
    private String            gatePassCode;

    @NotBlank
    private String            itemCode;

    public String getGatePassCode() {
        return gatePassCode;
    }

    public void setGatePassCode(String gatePassCode) {
        this.gatePassCode = gatePassCode;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

}
