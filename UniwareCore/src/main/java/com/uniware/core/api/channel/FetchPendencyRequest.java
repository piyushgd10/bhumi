/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 03-Jun-2012
 *  @author vibhu
 */
package com.uniware.core.api.channel;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author Sunny
 */
public class FetchPendencyRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -767335784096230275L;
    @NotBlank
    private String            channelCode;

    private boolean forceRefresh;

    public FetchPendencyRequest() {
        super();
    }

    public FetchPendencyRequest(String channelCode, boolean forceRefresh) {
        super();
        this.channelCode = channelCode;
        this.forceRefresh = forceRefresh;
    }

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    public boolean isForceRefresh() {
        return forceRefresh;
    }

    public void setForceRefresh(boolean forceRefresh) {
        this.forceRefresh = forceRefresh;
    }
}
