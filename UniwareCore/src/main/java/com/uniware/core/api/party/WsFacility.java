/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jul 27, 2012
 *  @author singla
 */
package com.uniware.core.api.party;

import com.unifier.core.annotation.constraints.OptionalBlank;
import com.unifier.core.api.customfields.WsCustomFieldValue;

import javax.validation.Valid;
import java.util.List;

/**
 * @author Sunny Agarwal
 */
public class WsFacility extends WsGenericParty {

    @OptionalBlank
    private String                   type;

    @OptionalBlank
    private String                   displayName;

    private boolean                  dummy;
    
    private boolean                  salesTaxOnShippingCharges;

    private boolean                  inventorySynDisabled = false;

    @Valid
    private List<WsCustomFieldValue> customFieldValues;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public boolean isDummy() {
        return dummy;
    }

    public void setDummy(boolean dummy) {
        this.dummy = dummy;
    }

    public List<WsCustomFieldValue> getCustomFieldValues() {
        return customFieldValues;
    }

    public void setCustomFieldValues(List<WsCustomFieldValue> customFieldValues) {
        this.customFieldValues = customFieldValues;
    }

    public boolean isInventorySynDisabled() {
        return inventorySynDisabled;
    }

    public void setInventorySynDisabled(boolean inventorySynDisabled) {
        this.inventorySynDisabled = inventorySynDisabled;
    }

    public boolean isSalesTaxOnShippingCharges() {
		return salesTaxOnShippingCharges;
	}

	public void setSalesTaxOnShippingCharges(boolean salesTaxOnShippingCharges) {
		this.salesTaxOnShippingCharges = salesTaxOnShippingCharges;
	}
}
