/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 22-Jan-2012
 *  @author vibhu
 */
package com.uniware.core.api.admin.shipping;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.uniware.core.entity.ShippingProvider;
import com.uniware.core.entity.ShippingProviderConnector;
import com.uniware.core.entity.ShippingProviderSource;
import com.uniware.core.entity.ShippingSourceConnector;

/**
 * @author vibhu
 */
public class ShippingProviderDTO {

    private Integer                            id;
    private String                             code;
    private String                             sourceCode;
    private String                             name;
    private String                             trackingLink;
    private String                             shortName;
    private String                             colorCode;
    private boolean                            enabled;
    private boolean                            trackingEnabled;
    private boolean                            configured;
    private String                             scraperScript;
    private String                             trackingNumberAllocationScript;
    private String                             trackingSyncStatus;
    private String                             postManifestScript;
    private String                             serviceability;
    private List<ShippingProviderConnectorDTO> shippingProviderConnectors = new ArrayList<ShippingProviderConnectorDTO>();

    public ShippingProviderDTO() {
    }

    public ShippingProviderDTO(ShippingProvider shippingProvider, ShippingProviderSource providerSource) {
        this.id = shippingProvider.getId();
        this.code = shippingProvider.getCode();
        this.sourceCode = shippingProvider.getShippingProviderSourceCode();
        this.name = shippingProvider.getName();
        this.trackingLink = providerSource.getTrackingLink();
        this.shortName = shippingProvider.getShortName();
        this.colorCode = shippingProvider.getColorCode();
        this.enabled = shippingProvider.isEnabled();
        this.trackingEnabled = providerSource.isTrackingEnabled();
        this.configured = shippingProvider.isConfigured();
        this.scraperScript = providerSource.getScraperScript();
        this.trackingNumberAllocationScript = providerSource.getTrackingAllocationScript();
        this.trackingSyncStatus = shippingProvider.getTrackingSyncStatus().name();
        this.postManifestScript = providerSource.getPostManifestScript();
        this.serviceability = shippingProvider.getShippingServiceablity().name();
        Map<String, ShippingSourceConnector> sourceConnectors = new HashMap<>();
        for (ShippingSourceConnector sourceConnector : providerSource.getShippingSourceConnectors()) {
            sourceConnectors.put(sourceConnector.getName(), sourceConnector);
        }
        for (ShippingProviderConnector connector : shippingProvider.getShippingProviderConnectors()) {
            shippingProviderConnectors.add(new ShippingProviderConnectorDTO(connector, sourceConnectors.get(connector.getShippingSourceConnectorName())));
        }
        Collections.sort(shippingProviderConnectors, new Comparator<ShippingProviderConnectorDTO>() {
            @Override
            public int compare(ShippingProviderConnectorDTO o1, ShippingProviderConnectorDTO o2) {
                return o1.getPriority() - o2.getPriority();
            }
        });
    }

    /**
     * @return the shippingProviderConnectors
     */
    public List<ShippingProviderConnectorDTO> getShippingProviderConnectors() {
        return shippingProviderConnectors;
    }

    /**
     * @param shippingProviderConnectors the shippingProviderConnectors to set
     */
    public void setShippingProviderConnectors(List<ShippingProviderConnectorDTO> shippingProviderConnectors) {
        this.shippingProviderConnectors = shippingProviderConnectors;
    }

    /**
     * @return the scraperScript
     */
    public String getScraperScript() {
        return scraperScript;
    }

    /**
     * @param scraperScript the scraperScript to set
     */
    public void setScraperScript(String scraperScript) {
        this.scraperScript = scraperScript;
    }

    /**
     * @return the trackingNumberAllocationScript
     */
    public String getTrackingNumberAllocationScript() {
        return trackingNumberAllocationScript;
    }

    /**
     * @param trackingNumberAllocationScript the trackingNumberAllocationScript to set
     */
    public void setTrackingNumberAllocationScript(String trackingNumberAllocationScript) {
        this.trackingNumberAllocationScript = trackingNumberAllocationScript;
    }

    /**
     * @return the trackingSyncStatus
     */
    public String getTrackingSyncStatus() {
        return trackingSyncStatus;
    }

    /**
     * @param trackingSyncStatus the trackingSyncStatus to set
     */
    public void setTrackingSyncStatus(String trackingSyncStatus) {
        this.trackingSyncStatus = trackingSyncStatus;
    }

    /**
     * @return the postManifestScript
     */
    public String getPostManifestScript() {
        return postManifestScript;
    }

    /**
     * @param postManifestScript the postManifestScript to set
     */
    public void setPostManifestScript(String postManifestScript) {
        this.postManifestScript = postManifestScript;
    }

    /**
     * @return the serviceability
     */
    public String getServiceability() {
        return serviceability;
    }

    /**
     * @param serviceability the serviceability to set
     */
    public void setServiceability(String serviceability) {
        this.serviceability = serviceability;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the sourceCode
     */
    public String getSourceCode() {
        return sourceCode;
    }

    /**
     * @param sourceCode the sourceCode to set
     */
    public void setSourceCode(String sourceCode) {
        this.sourceCode = sourceCode;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the enabled
     */
    public boolean isEnabled() {
        return enabled;
    }

    /**
     * @param enabled the enabled to set
     */
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    /**
     * @return the trackingEnabled
     */
    public boolean isTrackingEnabled() {
        return trackingEnabled;
    }

    /**
     * @param trackingEnabled the trackingEnabled to set
     */
    public void setTrackingEnabled(boolean trackingEnabled) {
        this.trackingEnabled = trackingEnabled;
    }

    /**
     * @return the configured
     */
    public boolean isConfigured() {
        return configured;
    }

    /**
     * @param configured the configured to set
     */
    public void setConfigured(boolean configured) {
        this.configured = configured;
    }

    /**
     * @return the trackingLink
     */
    public String getTrackingLink() {
        return trackingLink;
    }

    /**
     * @param trackingLink the trackingLink to set
     */
    public void setTrackingLink(String trackingLink) {
        this.trackingLink = trackingLink;
    }

    /**
     * @return the shortName
     */
    public String getShortName() {
        return shortName;
    }

    /**
     * @param shortName the shortName to set
     */
    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    /**
     * @return the colorCode
     */
    public String getColorCode() {
        return colorCode;
    }

    /**
     * @param colorCode the colorCode to set
     */
    public void setColorCode(String colorCode) {
        this.colorCode = colorCode;
    }

}