/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 11-Feb-2015
 *  @author parijat
 */
package com.uniware.core.api.shipping;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.unifier.core.utils.StringUtils;
import com.uniware.core.entity.ShippingProviderSource;
import com.uniware.core.entity.ShippingSourceConnector;

public class ShippingSourceDetailDTO implements Serializable {

    private String                           id;
    private String                           code;
    private String                           name;
    private boolean                          enabled;
    private String                           handlerClass;
    private Integer                          maxTrackingNumberPerRequest;
    private String                           scraperScript;
    private String                           trackingAllocationScript;
    private String                           trackingLink;
    private boolean                          trackingEnabled;
    private String                           shippingPaymentMethods;
    private String                           availableServiceabilities;
    // shipping method
    // serviceability
    private List<ShippingSourceConnectorDTO> connectors = new ArrayList<>();

    public ShippingSourceDetailDTO() {

    }

    public ShippingSourceDetailDTO(ShippingProviderSource source, String shippingMethodNames) {
        this.id = source.getId();
        this.code = source.getCode();
        this.name = source.getName();
        this.enabled = source.isEnabled();
        this.handlerClass = source.getHandlerClassName();
        this.maxTrackingNumberPerRequest = source.getMaxTrackingNumberPerRequest();
        this.scraperScript = source.getScraperScript();
        this.trackingAllocationScript = source.getTrackingAllocationScript();
        this.trackingLink = source.getTrackingLink();
        this.trackingEnabled = source.isTrackingEnabled();
        this.shippingPaymentMethods = StringUtils.isEmpty(source.getShippingPaymentEnabledMethodsCSV()) ? shippingMethodNames : source.getShippingPaymentEnabledMethodsCSV();
        this.availableServiceabilities = source.getAvailableServiceabilitiesCSV();
        for (ShippingSourceConnector connector : source.getShippingSourceConnectors()) {
            connectors.add(new ShippingSourceConnectorDTO(connector));
        }
        Collections.sort(connectors, new Comparator<ShippingSourceConnectorDTO>() {

            @Override
            public int compare(ShippingSourceConnectorDTO o1, ShippingSourceConnectorDTO o2) {
                return o1.getPriority() - o2.getPriority();
            }
        });
    }


    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the enabled
     */
    public boolean isEnabled() {
        return enabled;
    }

    /**
     * @param enabled the enabled to set
     */
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    /**
     * @return the handlerClass
     */
    public String getHandlerClass() {
        return handlerClass;
    }

    /**
     * @param handlerClass the handlerClass to set
     */
    public void setHandlerClass(String handlerClass) {
        this.handlerClass = handlerClass;
    }

    /**
     * @return the maxTrackingNumberPerRequest
     */
    public Integer getMaxTrackingNumberPerRequest() {
        return maxTrackingNumberPerRequest;
    }

    /**
     * @param maxTrackingNumberPerRequest the maxTrackingNumberPerRequest to set
     */
    public void setMaxTrackingNumberPerRequest(Integer maxTrackingNumberPerRequest) {
        this.maxTrackingNumberPerRequest = maxTrackingNumberPerRequest;
    }

    /**
     * @return the scraperScript
     */
    public String getScraperScript() {
        return scraperScript;
    }

    /**
     * @param scraperScript the scraperScript to set
     */
    public void setScraperScript(String scraperScript) {
        this.scraperScript = scraperScript;
    }

    /**
     * @return the trackingAllocationScript
     */
    public String getTrackingAllocationScript() {
        return trackingAllocationScript;
    }

    /**
     * @param trackingAllocationScript the trackingAllocationScript to set
     */
    public void setTrackingAllocationScript(String trackingAllocationScript) {
        this.trackingAllocationScript = trackingAllocationScript;
    }

    /**
     * @return the trackingLink
     */
    public String getTrackingLink() {
        return trackingLink;
    }

    /**
     * @param trackingLink the trackingLink to set
     */
    public void setTrackingLink(String trackingLink) {
        this.trackingLink = trackingLink;
    }

    /**
     * @return the trackingEnabled
     */
    public boolean isTrackingEnabled() {
        return trackingEnabled;
    }

    /**
     * @param trackingEnabled the trackingEnabled to set
     */
    public void setTrackingEnabled(boolean trackingEnabled) {
        this.trackingEnabled = trackingEnabled;
    }

    /**
     * @return the shippingPaymentMethods
     */
    public String getShippingPaymentMethods() {
        return shippingPaymentMethods;
    }

    /**
     * @param shippingPaymentMethods the shippingPaymentMethods to set
     */
    public void setShippingPaymentMethods(String shippingPaymentMethods) {
        this.shippingPaymentMethods = shippingPaymentMethods;
    }

    /**
     * @return the availableServiceabilities
     */
    public String getAvailableServiceabilities() {
        return availableServiceabilities;
    }

    /**
     * @param availableServiceabilities the availableServiceabilities to set
     */
    public void setAvailableServiceabilities(String availableServiceabilities) {
        this.availableServiceabilities = availableServiceabilities;
    }

    /**
     * @return the connectors
     */
    public List<ShippingSourceConnectorDTO> getConnectors() {
        return connectors;
    }

    /**
     * @param connectors the connectors to set
     */
    public void setConnectors(List<ShippingSourceConnectorDTO> connectors) {
        this.connectors = connectors;
    }

}
