package com.uniware.core.api.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.hibernate.validator.constraints.Length;

import com.uniware.core.entity.Invoice;

/**
 * Created by piyush on 8/12/15.
 */
public class WsInvoice {

    @Length(max = 45)
    private String              code;

    @Length(max = 45)
    private String              displayCode;

    private Date                channelCreated;

    private String              channelCode;

    private String              fromPartyCode;

    private String              toPartyCode;

    private Source              source;

    private String              destinationStateCode;

    private String              destinationCountryCode;

    private Invoice.Type        type                         = Invoice.Type.SALE;

    private Integer             userId;

    private Boolean             productManagementSwitchedOff = false;

    private Boolean             taxExempted                  = false;

    private Boolean             cformProvided                = false;

    @Valid
    private List<WsInvoiceItem> invoiceItems                 = new ArrayList<>();

    public List<WsInvoiceItem> getInvoiceItems() {
        return invoiceItems;
    }

    public void setInvoiceItems(List<WsInvoiceItem> invoiceItems) {
        this.invoiceItems = invoiceItems;
    }

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    public Source getSource() {
        return source;
    }

    public void setSource(Source source) {
        this.source = source;
    }

    public String getCode() {
        return code;
    }

    public String getDisplayCode() {
        return displayCode;
    }

    public void setDisplayCode(String displayCode) {
        this.displayCode = displayCode;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getFromPartyCode() {
        return fromPartyCode;
    }

    public void setFromPartyCode(String fromPartyCode) {
        this.fromPartyCode = fromPartyCode;
    }

    public String getToPartyCode() {
        return toPartyCode;
    }

    public void setToPartyCode(String toPartyCode) {
        this.toPartyCode = toPartyCode;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Boolean getCformProvided() {
        return cformProvided;
    }

    public void setCformProvided(Boolean cformProvided) {
        this.cformProvided = cformProvided;
    }

    public String getDestinationStateCode() {
        return destinationStateCode;
    }

    public void setDestinationStateCode(String destinationStateCode) {
        this.destinationStateCode = destinationStateCode;
    }

    public String getDestinationCountryCode() {
        return destinationCountryCode;
    }

    public void setDestinationCountryCode(String destinationCountryCode) {
        this.destinationCountryCode = destinationCountryCode;
    }

    public Invoice.Type getType() {
        return type;
    }

    public void setType(Invoice.Type type) {
        this.type = type;
    }

    public Boolean getProductManagementSwitchedOff() {
        return productManagementSwitchedOff;
    }

    public void setProductManagementSwitchedOff(Boolean productManagementSwitchedOff) {
        this.productManagementSwitchedOff = productManagementSwitchedOff;
    }

    public Boolean getTaxExempted() {
        return taxExempted;
    }

    public void setTaxExempted(Boolean taxExempted) {
        this.taxExempted = taxExempted;
    }

    public Date getChannelCreated() {
        return channelCreated;
    }

    public void setChannelCreated(Date channelCreated) {
        this.channelCreated = channelCreated;
    }

    public enum Source {
        SHIPPING_PACKAGE,
        PURCHASE_ORDER,
        GATEPASS
    }

}
