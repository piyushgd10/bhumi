/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 29-Sep-2014
 *  @author akshay
 */
package com.uniware.core.api.shipping;

import com.unifier.core.api.base.ServiceResponse;
import com.uniware.core.entity.Source;

import java.io.File;
import java.util.List;

public class GenerateShippingLabelsResponse extends ServiceResponse {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private Source.ShipmentLabelFormat shipmentLabelFormat;
    private List<File>                 shippingLabels;
    private String                     downloadFileName;

    public List<File> getShippingLabels() {
        return shippingLabels;
    }

    public void setShippingLabels(List<File> shippingLabels) {
        this.shippingLabels = shippingLabels;
    }

    public Source.ShipmentLabelFormat getShipmentLabelFormat() {
        return shipmentLabelFormat;
    }

    public void setShipmentLabelFormat(Source.ShipmentLabelFormat shipmentLabelFormat) {
        this.shipmentLabelFormat = shipmentLabelFormat;
    }

    public String getDownloadFileName() {
        return downloadFileName;
    }

    public void setDownloadFileName(String downloadFileName) {
        this.downloadFileName = downloadFileName;
    }

    @Override public String toString() {
        return "GenerateShippingLabelsResponse{" + "shipmentLabelFormat=" + shipmentLabelFormat + ", shippingLabels=" + shippingLabels + ", downloadFileName='" + downloadFileName
                + '\'' + '}';
    }
}
