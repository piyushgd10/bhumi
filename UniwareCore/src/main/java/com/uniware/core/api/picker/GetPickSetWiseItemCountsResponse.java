package com.uniware.core.api.picker;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.unifier.core.api.base.ServiceResponse;

public class GetPickSetWiseItemCountsResponse extends ServiceResponse {

    private static final long     serialVersionUID = 7819872185111306246L;

    private List<PickSetCountDTO> pickSetCountDTOs = new ArrayList<>();

    public List<PickSetCountDTO> getPickSetCountDTOs() {
        return pickSetCountDTOs;
    }

    public void setPickSetCountDTOs(List<PickSetCountDTO> pickSetCountDTOs) {
        this.pickSetCountDTOs = pickSetCountDTOs;
    }

    public static class PickSetCountDTO {

        private Integer              picksetId;

        private String               pickSetName;

        private Integer              itemCount;

        public PickSetCountDTO(Integer picksetId, String name, Integer itemCount) {
            setPicksetId(picksetId);
            setPickSetName(name);
            setItemCount(itemCount);
        }

        public Integer getPicksetId() {
            return picksetId;
        }

        public void setPicksetId(Integer picksetId) {
            this.picksetId = picksetId;
        }

        public String getPickSetName() {
            return pickSetName;
        }

        public void setPickSetName(String pickSetName) {
            this.pickSetName = pickSetName;
        }

        public Integer getItemCount() {
            return itemCount;
        }

        public void setItemCount(Integer itemCount) {
            this.itemCount = itemCount;
        }
    }
}
