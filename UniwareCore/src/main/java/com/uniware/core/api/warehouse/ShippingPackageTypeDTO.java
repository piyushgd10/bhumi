/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 22-Jan-2012
 *  @author vibhu
 */
package com.uniware.core.api.warehouse;

import java.math.BigDecimal;

import com.uniware.core.entity.ShippingPackageType;

/**
 * @author vibhu
 */
public class ShippingPackageTypeDTO {

    private String     code;
    private int        boxLength;
    private int        boxWidth;
    private int        boxHeight;
    private int        boxWeight;
    private BigDecimal packingCost = BigDecimal.ZERO;
    private boolean    enabled;
    private boolean    editable;

    public ShippingPackageTypeDTO() {
    }

    public ShippingPackageTypeDTO(ShippingPackageType sPackageType) {
        this.code = sPackageType.getCode();
        this.boxLength = sPackageType.getBoxLength();
        this.boxWidth = sPackageType.getBoxWidth();
        this.boxHeight = sPackageType.getBoxHeight();
        this.boxWeight = sPackageType.getBoxWeight();
        this.packingCost = sPackageType.getPackingCost();
        this.enabled = sPackageType.isEnabled();
        this.editable = sPackageType.isEditable();
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the boxLength
     */
    public int getBoxLength() {
        return boxLength;
    }

    /**
     * @param boxLength the boxLength to set
     */
    public void setBoxLength(int boxLength) {
        this.boxLength = boxLength;
    }

    /**
     * @return the boxWidth
     */
    public int getBoxWidth() {
        return boxWidth;
    }

    /**
     * @param boxWidth the boxWidth to set
     */
    public void setBoxWidth(int boxWidth) {
        this.boxWidth = boxWidth;
    }

    /**
     * @return the boxHeight
     */
    public int getBoxHeight() {
        return boxHeight;
    }

    /**
     * @param boxHeight the boxHeight to set
     */
    public void setBoxHeight(int boxHeight) {
        this.boxHeight = boxHeight;
    }

    /**
     * @return the enabled
     */
    public boolean isEnabled() {
        return enabled;
    }

    /**
     * @param enabled the enabled to set
     */
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    /**
     * @return the boxWeight
     */
    public int getBoxWeight() {
        return boxWeight;
    }

    /**
     * @param boxWeight the boxWeight to set
     */
    public void setBoxWeight(int boxWeight) {
        this.boxWeight = boxWeight;
    }

    /**
     * @return the editable
     */
    public boolean isEditable() {
        return editable;
    }

    /**
     * @param editable the editable to set
     */
    public void setEditable(boolean editable) {
        this.editable = editable;
    }

    /**
     * @return the packingCost
     */
    public BigDecimal getPackingCost() {
        return packingCost;
    }

    /**
     * @param packingCost the packingCost to set
     */
    public void setPackingCost(BigDecimal packingCost) {
        this.packingCost = packingCost;
    }
}