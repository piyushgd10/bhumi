/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 20, 2012
 *  @author praveeng
 */
package com.uniware.core.api.party;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.google.gson.Gson;
import com.unifier.core.api.base.ServiceRequest;

/**
 * @author Sunny
 */
public class CreateBillingPartyRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -8607921700580971155L;

    @NotNull
    @Valid
    private WsBillingParty    billingParty;

    public WsBillingParty getBillingParty() {
        return billingParty;
    }

    public void setBillingParty(WsBillingParty billingParty) {
        this.billingParty = billingParty;
    }

    public static void main(String[] args) {
        CreateBillingPartyRequest request = new CreateBillingPartyRequest();
        WsBillingParty billingParty = new WsBillingParty();
        billingParty.setName("sunny");
        billingParty.setCode("sunny");
        billingParty.setPan("AOEPA2677E");
        request.setBillingParty(billingParty);
        WsPartyAddress address = new WsPartyAddress();
        address.setAddressLine1("dasd");
        address.setCity("delhi");
        address.setStateCode("delhi");
        address.setPincode("110020");
        address.setPhone("9999999999");
        address.setAddressType("BILLING");
        address.setPartyCode("sunny");
        billingParty.setBillingAddress(address);
        WsPartyAddress address1 = new WsPartyAddress();
        address1.setAddressLine1("dasd");
        address1.setCity("delhi");
        address1.setStateCode("delhi");
        address1.setPincode("110020");
        address1.setPhone("9999999999");
        address1.setAddressType("SHIPPING");
        address1.setPartyCode("sunny");
        billingParty.setShippingAddress(address1);
        System.out.println(new Gson().toJson(request));
    }
}
