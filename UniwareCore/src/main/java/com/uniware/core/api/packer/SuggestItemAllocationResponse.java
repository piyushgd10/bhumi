/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Apr 12, 2012
 *  @author singla
 */
package com.uniware.core.api.packer;

import java.util.List;

import com.unifier.core.api.base.ServiceResponse;
import com.uniware.core.api.item.ItemDetailFieldDTO;

/**
 * @author singla
 */
public class SuggestItemAllocationResponse extends AbstractAllocateItemResponse {

    /**
     * 
     */
    private static final long        serialVersionUID = 4292975982262894070L;

}