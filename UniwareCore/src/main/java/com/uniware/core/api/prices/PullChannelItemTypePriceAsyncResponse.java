/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Oct 13, 2015
 *  @author bhupi
 */
package com.uniware.core.api.prices;

import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import com.unifier.core.api.base.ServiceResponse;

public class PullChannelItemTypePriceAsyncResponse extends ServiceResponse {

    private static final long serialVersionUID = 8739966347835728863L;
    
    private String channelCode;
    private Future<PullChannelItemTypePriceResponse> asyncTaskResult;

    public void setAsyncResult(final Future<PullChannelItemTypePriceResponse> asyncTaskResult) {
        this.asyncTaskResult = asyncTaskResult;
    }
    
    /**
     * Waits if necessary for the push to complete and returns a {@code PullChannelItemTypePriceResponse}
     *
     * @throws CancellationException if the push was cancelled
     * @throws ExecutionException if the push threw an exception
     * @throws InterruptedException if the current thread was interrupted while waiting
     */
    public PullChannelItemTypePriceResponse sync() throws InterruptedException, ExecutionException {
        if (null != asyncTaskResult) {
            return asyncTaskResult.get();
        }
        PullChannelItemTypePriceResponse response = new PullChannelItemTypePriceResponse();
        response.setResponse(this);
        return response;
    }
    
    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }
}
