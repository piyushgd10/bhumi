/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, May 6, 2015
 *  @author harsh
 */
package com.uniware.core.api.purchase;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author harsh
 */
public class GetNextPurchaseOrderCodeResponse extends ServiceResponse {

    private static final long serialVersionUID = 2791264746434826219L;
    private String            nextPurchaseOrderCode;

    public String getNextPurchaseOrderCode() {
        return nextPurchaseOrderCode;
    }

    public void setNextPurchaseOrderCode(String nextPurchaseOrderCode) {
        this.nextPurchaseOrderCode = nextPurchaseOrderCode;
    }

}
