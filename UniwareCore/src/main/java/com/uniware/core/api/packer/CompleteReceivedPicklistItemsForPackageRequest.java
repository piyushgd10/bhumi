/*
 * Copyright 2017 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 05/09/17
 * @author piyush
 */
package com.uniware.core.api.packer;

import com.unifier.core.api.base.ServiceRequest;
import com.uniware.core.entity.Picklist;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;

public class CompleteReceivedPicklistItemsForPackageRequest extends ServiceRequest{

    @NotBlank
    private String               shippingPackageCode;

    @NotNull
    private Picklist.Destination destination;

    public String getShippingPackageCode() {
        return shippingPackageCode;
    }

    public void setShippingPackageCode(String shippingPackageCode) {
        this.shippingPackageCode = shippingPackageCode;
    }

    public Picklist.Destination getDestination() {
        return destination;
    }

    public void setDestination(Picklist.Destination destination) {
        this.destination = destination;
    }
}
