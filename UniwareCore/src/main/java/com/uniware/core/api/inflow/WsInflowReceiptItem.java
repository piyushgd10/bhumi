/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Aug 31, 2012
 *  @author singla
 */
package com.uniware.core.api.inflow;

import java.math.BigDecimal;

import java.util.Date;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;

/**
 * @author singla
 */
public class WsInflowReceiptItem {
    @NotNull
    @Min(value = 0)
    private Integer    quantity;

    private BigDecimal unitPrice;

    private BigDecimal maxRetailPrice;

    private BigDecimal discount;

    private BigDecimal additionalCost = BigDecimal.ZERO;

    @Past
    private Date       manufacturingDate;

    private String     batchCode;

    /**
     * @return the quantity
     */
    public Integer getQuantity() {
        return quantity;
    }

    /**
     * @param quantity the quantity to set
     */
    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    /**
     * @return the unitPrice
     */
    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    /**
     * @param unitPrice the unitPrice to set
     */
    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }

    /**
     * @return the maxRetailPrice
     */
    public BigDecimal getMaxRetailPrice() {
        return maxRetailPrice;
    }

    /**
     * @param maxRetailPrice the maxRetailPrice to set
     */
    public void setMaxRetailPrice(BigDecimal maxRetailPrice) {
        this.maxRetailPrice = maxRetailPrice;
    }

    /**
     * @return the discount
     */
    public BigDecimal getDiscount() {
        return discount;
    }

    /**
     * @param discount the discount to set
     */
    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    /**
     * @return the additionalCost
     */
    public BigDecimal getAdditionalCost() {
        return additionalCost;
    }

    /**
     * @param additionalCost the additionalCost to set
     */
    public void setAdditionalCost(BigDecimal additionalCost) {
        this.additionalCost = additionalCost;
    }

    /**
     * @return the batchCode
     */
    public String getBatchCode() {
        return batchCode;
    }

    /**
     * @param batchCode the batchCode to set
     */
    public void setBatchCode(String batchCode) {
        this.batchCode = batchCode;
    }

    public Date getManufacturingDate() {
        return manufacturingDate;
    }

    public void setManufacturingDate(Date manufacturingDate) {
        this.manufacturingDate = manufacturingDate;
    }
}
