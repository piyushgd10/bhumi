/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 08-Apr-2015
 *  @author akshaykochhar
 */
package com.uniware.core.api.material;

import com.unifier.core.api.base.ServiceRequest;

public class GetOutboundGatepassTypesRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 8124869187308032237L;

}
