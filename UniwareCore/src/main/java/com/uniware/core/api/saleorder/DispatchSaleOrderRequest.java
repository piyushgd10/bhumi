package com.uniware.core.api.saleorder;

import com.unifier.core.api.base.ServiceRequest;

import java.util.List;

/**
 * Created by akshayag on 9/30/15.
 */
public class DispatchSaleOrderRequest extends ServiceRequest {

    private List<String> saleOrderCodes;

    private String       channelCode;

    private String       shippingProviderCode;

    private String       shippingProviderName;

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    public List<String> getSaleOrderCodes() {
        return saleOrderCodes;
    }

    public void setSaleOrderCodes(List<String> saleOrderCodes) {
        this.saleOrderCodes = saleOrderCodes;
    }

    public String getShippingProviderCode() {
        return shippingProviderCode;
    }

    public void setShippingProviderCode(String shippingProviderCode) {
        this.shippingProviderCode = shippingProviderCode;
    }

    public String getShippingProviderName() {
        return shippingProviderName;
    }

    public void setShippingProviderName(String shippingProviderName) {
        this.shippingProviderName = shippingProviderName;
    }
}
