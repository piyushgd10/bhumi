/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 10-Jan-2014
 *  @author parijat
 */
package com.uniware.core.api.reconciliation;

import com.unifier.core.api.base.ServiceRequest;
import org.hibernate.validator.constraints.NotEmpty;

public class CreateUnreconciledSaleOrderRequest extends ServiceRequest {

    private static final long serialVersionUID = -9162548821635667509L;

    @NotEmpty
    private String            saleOrderCode;

    public CreateUnreconciledSaleOrderRequest(String saleOrderCode) {
        this.saleOrderCode = saleOrderCode;
    }

    public String getSaleOrderCode() {
        return saleOrderCode;
    }

    public void setSaleOrderCode(String saleOrderCode) {
        this.saleOrderCode = saleOrderCode;
    }
}
