package com.uniware.core.api.tax;

import java.math.BigDecimal;

/**
 * Created by Sagar Sahni on 29/05/17.
 */
public class WsTaxClass {
    private String     taxTypeCode;

    private String     gstTaxTypeCode;

    private BigDecimal sellingPrice;

    public WsTaxClass(String taxTypeCode, String gstTaxTypeCode, BigDecimal sellingPrice) {
        this.taxTypeCode = taxTypeCode;
        this.gstTaxTypeCode = gstTaxTypeCode;
        this.sellingPrice = sellingPrice;
    }

    public String getTaxTypeCode() {
        return taxTypeCode;
    }

    public void setTaxTypeCode(String taxTypeCode) {
        this.taxTypeCode = taxTypeCode;
    }

    public String getGstTaxTypeCode() {
        return gstTaxTypeCode;
    }

    public void setGstTaxTypeCode(String gstTaxTypeCode) {
        this.gstTaxTypeCode = gstTaxTypeCode;
    }

    public BigDecimal getSellingPrice() {
        return sellingPrice;
    }

    public void setSellingPrice(BigDecimal sellingPrice) {
        this.sellingPrice = sellingPrice;
    }
}
