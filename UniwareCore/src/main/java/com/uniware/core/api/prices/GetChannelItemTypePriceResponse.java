/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Oct 13, 2015
 *  @author bhupi
 */
package com.uniware.core.api.prices;

import java.util.Date;

import com.unifier.core.api.base.ServiceResponse;
import com.uniware.core.api.prices.dto.PriceDTO;

public class GetChannelItemTypePriceResponse extends ServiceResponse {

    private static final long       serialVersionUID = 8739966347835728863L;

    private PriceDTO channelItemTypePrice;
    
    private boolean isDirty;
    
    private Date version;

    public PriceDTO getChannelItemTypePrice() {
        return channelItemTypePrice;
    }

    public void setChannelItemTypePrice(PriceDTO channelItemTypePrice) {
        this.channelItemTypePrice = channelItemTypePrice;
    }

    public boolean isDirty() {
        return isDirty;
    }

    public void setDirty(boolean isDirty) {
        this.isDirty = isDirty;
    }

    public Date getVersion() {
        return version;
    }

    public void setVersion(Date version) {
        this.version = version;
    }
}
