/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 04-Aug-2012
 *  @author praveeng
 */
package com.uniware.core.api.admin.shipping;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author praveeng
 */
public class CreateProviderAllocationRuleResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long                 serialVersionUID = 7171915883507608486L;
    private ShippingProviderAllocationRuleDTO providerAllocationRuleDTO;

    /**
     * @return the providerAllocationRuleDTO
     */
    public ShippingProviderAllocationRuleDTO getProviderAllocationRuleDTO() {
        return providerAllocationRuleDTO;
    }

    /**
     * @param providerAllocationRuleDTO the providerAllocationRuleDTO to set
     */
    public void setProviderAllocationRuleDTO(ShippingProviderAllocationRuleDTO providerAllocationRuleDTO) {
        this.providerAllocationRuleDTO = providerAllocationRuleDTO;
    }

}
