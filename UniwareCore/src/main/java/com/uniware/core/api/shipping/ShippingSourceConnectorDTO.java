/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 11-Feb-2015
 *  @author parijat
 */
package com.uniware.core.api.shipping;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.uniware.core.entity.ShippingSourceConnector;
import com.uniware.core.entity.ShippingSourceConnectorParameter;

public class ShippingSourceConnectorDTO implements Serializable {

    private String                                    name;
    private String                                    displayName;
    private String                                    helpText;
    private int                                       priority;
    private boolean                                   requiredInAWBFetch;
    private boolean                                   requiredInTracking;
    private String                                    verificationScriptName;
    private List<ShippingSourceConnectorParameterDTO> params = new ArrayList<>();

    public ShippingSourceConnectorDTO() {
    }

    public ShippingSourceConnectorDTO(ShippingSourceConnector connector) {
        this.name = connector.getName();
        this.displayName = connector.getDisplayName();
        this.helpText = connector.getHelpText();
        this.priority = connector.getPriority();
        this.requiredInAWBFetch = connector.isRequiredInAwbFetch();
        this.requiredInTracking = connector.isRequiredInTracking();
        this.verificationScriptName = connector.getVerificationScriptName();
        for (ShippingSourceConnectorParameter paramter : connector.getShippingSourceConnectorParameters()) {
            params.add(new ShippingSourceConnectorParameterDTO(paramter));
        }
        Collections.sort(params, new Comparator<ShippingSourceConnectorParameterDTO>() {

            @Override
            public int compare(ShippingSourceConnectorParameterDTO o1, ShippingSourceConnectorParameterDTO o2) {
                return o1.getPriority() - o2.getPriority();
            }

        });
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the displayName
     */
    public String getDisplayName() {
        return displayName;
    }

    /**
     * @param displayName the displayName to set
     */
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    /**
     * @return the helpText
     */
    public String getHelpText() {
        return helpText;
    }

    /**
     * @param helpText the helpText to set
     */
    public void setHelpText(String helpText) {
        this.helpText = helpText;
    }

    /**
     * @return the priority
     */
    public int getPriority() {
        return priority;
    }

    /**
     * @param priority the priority to set
     */
    public void setPriority(int priority) {
        this.priority = priority;
    }

    /**
     * @return the requiredInAWBFetch
     */
    public boolean isRequiredInAWBFetch() {
        return requiredInAWBFetch;
    }

    /**
     * @param requiredInAWBFetch the requiredInAWBFetch to set
     */
    public void setRequiredInAWBFetch(boolean requiredInAWBFetch) {
        this.requiredInAWBFetch = requiredInAWBFetch;
    }

    /**
     * @return the requiredInTracking
     */
    public boolean isRequiredInTracking() {
        return requiredInTracking;
    }

    /**
     * @param requiredInTracking the requiredInTracking to set
     */
    public void setRequiredInTracking(boolean requiredInTracking) {
        this.requiredInTracking = requiredInTracking;
    }

    /**
     * @return the verificationScriptName
     */
    public String getVerificationScriptName() {
        return verificationScriptName;
    }

    /**
     * @param verificationScriptName the verificationScriptName to set
     */
    public void setVerificationScriptName(String verificationScriptName) {
        this.verificationScriptName = verificationScriptName;
    }

    /**
     * @return the params
     */
    public List<ShippingSourceConnectorParameterDTO> getParams() {
        return params;
    }

    /**
     * @param params the params to set
     */
    public void setParams(List<ShippingSourceConnectorParameterDTO> params) {
        this.params = params;
    }

}
