/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 03-May-2012
 *  @author praveeng
 */
package com.uniware.core.api.transition;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author praveeng
 */
public class ItemTypeDTO {

    private String                  category;
    private boolean                 expirable;
    private String                  itemSKU;
    private String                  itemTypeImageUrl;
    private String                  itemTypePageUrl;
    private String                  itemTypeName;
    private List<VendorItemTypeDTO> vendorItemTypes      = new ArrayList<VendorItemTypeDTO>();
    private Integer                 goodInventoryCount;
    private Integer                 inTransitionCount;
    private Map<String, Integer>    shelfToItemTypeCount = new HashMap<String, Integer>();

    /**
     * @return the vendorItemTypes
     */
    public List<VendorItemTypeDTO> getVendorItemTypes() {
        return vendorItemTypes;
    }

    /**
     * @param vendorItemTypes the vendorItemTypes to set
     */
    public void setVendorItemTypes(List<VendorItemTypeDTO> vendorItemTypes) {
        this.vendorItemTypes = vendorItemTypes;
    }

    /**
     * @return the goodInventoryCount
     */
    public Integer getGoodInventoryCount() {
        return goodInventoryCount;
    }

    /**
     * @param goodInventoryCount the goodInventoryCount to set
     */
    public void setGoodInventoryCount(Integer goodInventoryCount) {
        this.goodInventoryCount = goodInventoryCount;
    }

    /**
     * @return the inTransitionCount
     */
    public Integer getInTransitionCount() {
        return inTransitionCount;
    }

    /**
     * @param inTransitionCount the inTransitionCount to set
     */
    public void setInTransitionCount(Integer inTransitionCount) {
        this.inTransitionCount = inTransitionCount;
    }

    public static class VendorItemTypeDTO {

        private boolean vendorEnabled;
        private String  vendorCode;
        private String  vendorName;
        private String  vendorSkuCode;

        /**
         * @return the vendorEnabled
         */
        public boolean isVendorEnabled() {
            return vendorEnabled;
        }

        /**
         * @param vendorEnabled the vendorEnabled to set
         */
        public void setVendorEnabled(boolean vendorEnabled) {
            this.vendorEnabled = vendorEnabled;
        }

        /**
         * @return the vendorCode
         */
        public String getVendorCode() {
            return vendorCode;
        }

        /**
         * @param vendorCode the vendorCode to set
         */
        public void setVendorCode(String vendorCode) {
            this.vendorCode = vendorCode;
        }

        /**
         * @return the vendorName
         */
        public String getVendorName() {
            return vendorName;
        }

        /**
         * @param vendorName the vendorName to set
         */
        public void setVendorName(String vendorName) {
            this.vendorName = vendorName;
        }

        /**
         * @return the vendorSkuCode
         */
        public String getVendorSkuCode() {
            return vendorSkuCode;
        }

        /**
         * @param vendorSkuCode the vendorSkuCode to set
         */
        public void setVendorSkuCode(String vendorSkuCode) {
            this.vendorSkuCode = vendorSkuCode;
        }

    }

    /**
     * @return the category
     */
    public String getCategory() {
        return category;
    }

    /**
     * @param category the category to set
     */
    public void setCategory(String category) {
        this.category = category;
    }

    /**
     * @return the itemSKU
     */
    public String getItemSKU() {
        return itemSKU;
    }

    /**
     * @param itemSKU the itemSKU to set
     */
    public void setItemSKU(String itemSKU) {
        this.itemSKU = itemSKU;
    }

    /**
     * @return the itemTypeImageUrl
     */
    public String getItemTypeImageUrl() {
        return itemTypeImageUrl;
    }

    /**
     * @param itemTypeImageUrl the itemTypeImageUrl to set
     */
    public void setItemTypeImageUrl(String itemTypeImageUrl) {
        this.itemTypeImageUrl = itemTypeImageUrl;
    }

    /**
     * @return the itemTypePageUrl
     */
    public String getItemTypePageUrl() {
        return itemTypePageUrl;
    }

    /**
     * @param itemTypePageUrl the itemTypePageUrl to set
     */
    public void setItemTypePageUrl(String itemTypePageUrl) {
        this.itemTypePageUrl = itemTypePageUrl;
    }

    /**
     * @return the itemTypeName
     */
    public String getItemTypeName() {
        return itemTypeName;
    }

    /**
     * @param itemTypeName the itemTypeName to set
     */
    public void setItemTypeName(String itemTypeName) {
        this.itemTypeName = itemTypeName;
    }

    /**
     * @return the shelfToItemTypeCount
     */
    public Map<String, Integer> getShelfToItemTypeCount() {
        return shelfToItemTypeCount;
    }

    /**
     * @param shelfToItemTypeCount the shelfToItemTypeCount to set
     */
    public void setShelfToItemTypeCount(Map<String, Integer> shelfToItemTypeCount) {
        this.shelfToItemTypeCount = shelfToItemTypeCount;
    }

    public boolean isExpirable() {
        return expirable;
    }

    public void setExpirable(boolean expirable) {
        this.expirable = expirable;
    }
}
