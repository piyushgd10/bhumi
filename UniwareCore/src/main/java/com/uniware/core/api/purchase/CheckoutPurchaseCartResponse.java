/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Apr 7, 2012
 *  @author singla
 */
package com.uniware.core.api.purchase;

import java.util.ArrayList;
import java.util.List;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author singla
 */
public class CheckoutPurchaseCartResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long      serialVersionUID = 8779746088097179724L;

    private List<PurchaseOrderDTO> purchaseOrders   = new ArrayList<CheckoutPurchaseCartResponse.PurchaseOrderDTO>();

    public static class PurchaseOrderDTO {
        private String purchaseOrderCode;
        private String vendorName;

        public PurchaseOrderDTO() {

        }

        /**
         * @param purchaseOrderName
         * @param vendorName
         */
        public PurchaseOrderDTO(String purchaseOrderCode, String vendorName) {
            this.purchaseOrderCode = purchaseOrderCode;
            this.vendorName = vendorName;
        }

        /**
         * @return the vendorName
         */
        public String getVendorName() {
            return vendorName;
        }

        /**
         * @param vendorName the vendorName to set
         */
        public void setVendorName(String vendorName) {
            this.vendorName = vendorName;
        }

        /**
         * @return the purchaseOrderCode
         */
        public String getPurchaseOrderCode() {
            return purchaseOrderCode;
        }

        /**
         * @param purchaseOrderCode the purchaseOrderCode to set
         */
        public void setPurchaseOrderCode(String purchaseOrderCode) {
            this.purchaseOrderCode = purchaseOrderCode;
        }
    }

    /**
     * @return the purchaseOrders
     */
    public List<PurchaseOrderDTO> getPurchaseOrders() {
        return purchaseOrders;
    }

    /**
     * @param purchaseOrders the purchaseOrders to set
     */
    public void setPurchaseOrders(List<PurchaseOrderDTO> purchaseOrders) {
        this.purchaseOrders = purchaseOrders;
    }
}
