/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 9, 2012
 *  @author praveeng
 */
package com.uniware.core.api.channel;

import com.uniware.core.api.catalog.ItemTypeResponse;

/**
 * @author Sunny Agarwal
 */
public class EditChannelItemTypeResponse extends ItemTypeResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -5466574933334174621L;

}
