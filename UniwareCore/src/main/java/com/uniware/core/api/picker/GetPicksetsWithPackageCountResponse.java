/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Apr 4, 2012
 *  @author praveeng
 */
package com.uniware.core.api.picker;

import java.util.List;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author Sunny
 */
public class GetPicksetsWithPackageCountResponse extends ServiceResponse {
    /**
     * 
     */
    private static final long serialVersionUID = 3928235967429015413L;
    private List<PicksetDTO>  picksets;

    public List<PicksetDTO> getPicksets() {
        return picksets;
    }

    public void setPicksets(List<PicksetDTO> picksets) {
        this.picksets = picksets;
    }

    public static class PicksetDTO {
        private int    id;
        private String name;
        private int    shippingPackageCount;

        public PicksetDTO(int id, String name, int shippingPackageCount) {
            super();
            this.id = id;
            this.name = name;
            this.shippingPackageCount = shippingPackageCount;
        }

        public PicksetDTO() {
            super();
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getShippingPackageCount() {
            return shippingPackageCount;
        }

        public void setShippingPackageCount(int shippingPackageCount) {
            this.shippingPackageCount = shippingPackageCount;
        }

    }
}
