/*
 * Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 4/15/15 6:23 PM
 * @author amdalal
 */

package com.uniware.core.api.tenant;

import com.unifier.core.api.base.ServiceResponse;

public class AllowSourceResponse extends ServiceResponse {

    private static final long serialVersionUID = 1028568605989035576L;
}
