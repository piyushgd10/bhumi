package com.uniware.core.api.invoice;

/**
 * Created by Sagar Sahni on 31/05/17.
 */
public class CreateShippingPackageInvoiceRequest extends CreateInvoiceRequest {

    private boolean forceCreate = false;

    public CreateShippingPackageInvoiceRequest(CreateInvoiceRequest request) {
        this.setChannelProductIdToTax(request.getChannelProductIdToTax());
        this.setSkipDetailing(request.isSkipDetailing());
        this.setShippingPackageCode(request.getShippingPackageCode());
        this.setCommitBlockedInventory(request.isCommitBlockedInventory());
        this.setUserId(request.getUserId());
    }

    public CreateShippingPackageInvoiceRequest() {

    }

    public boolean isForceCreate() {
        return forceCreate;
    }

    public void setForceCreate(boolean forceDispatch) {
        this.forceCreate = forceDispatch;
    }
}
