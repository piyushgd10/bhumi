/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 20, 2012
 *  @author praveeng
 */
package com.uniware.core.api.party;

/**
 * @author praveeng
 */
public class EditVendorResponse extends VendorResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 8628063066835167489L;

}
