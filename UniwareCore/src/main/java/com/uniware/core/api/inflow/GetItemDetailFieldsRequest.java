/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 28-May-2012
 *  @author vibhu
 */
package com.uniware.core.api.inflow;

import com.unifier.core.api.base.ServiceRequest;

import org.hibernate.validator.constraints.NotBlank;

/**
 * @author vibhu
 */
public class GetItemDetailFieldsRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 976180299811089015L;

    @NotBlank
    private String            itemCode;

    private String            inflowReceiptCode;

    /**
     * @return the itemCode
     */
    public String getItemCode() {
        return itemCode;
    }

    /**
     * @param itemCode the itemCode to set
     */
    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    /**
     * @return the inflowReceiptCode
     */
    public String getInflowReceiptCode() {
        return inflowReceiptCode;
    }

    /**
     * @param inflowReceiptCode the inflowReceiptCode to set
     */
    public void setInflowReceiptCode(String inflowReceiptCode) {
        this.inflowReceiptCode = inflowReceiptCode;
    }

}