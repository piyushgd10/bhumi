/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 28-Nov-2013
 *  @author akshay
 */
package com.uniware.core.api.purchase;

import java.util.ArrayList;
import java.util.List;

import com.unifier.core.api.base.ServiceResponse;

public class GetPurchaseOrdersResponse extends ServiceResponse{

    /**
     * 
     */
    private static final long serialVersionUID = 8393094981846183324L;
    private List<String>      purchaseOrderCodes = new ArrayList<String>();

    public List<String> getPurchaseOrderCodes() {
        return purchaseOrderCodes;
    }

    public void setPurchaseOrderCodes(List<String> purchaseOrderCodes) {
        this.purchaseOrderCodes = purchaseOrderCodes;
    }

}
