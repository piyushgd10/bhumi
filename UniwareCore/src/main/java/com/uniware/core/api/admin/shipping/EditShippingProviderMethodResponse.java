/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 10-Mar-2012
 *  @author vibhu
 */
package com.uniware.core.api.admin.shipping;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author vibhu
 */
public class EditShippingProviderMethodResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long         serialVersionUID = -1719588324673505822L;
    private ShippingProviderMethodDTO methodDTO;

    /**
     * @return the methodDTO
     */
    public ShippingProviderMethodDTO getMethodDTO() {
        return methodDTO;
    }

    /**
     * @param methodDTO the methodDTO to set
     */
    public void setMethodDTO(ShippingProviderMethodDTO methodDTO) {
        this.methodDTO = methodDTO;
    }

}
