package com.uniware.core.api.shipping;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author ankurpratik on 28/03/17.
 */
public class FetchShippingManifestPdfResponse extends ServiceResponse {

    private static final long serialVersionUID = -2900571061020950403L;

    private String manifestUrl;

    public String getManifestUrl() {
        return manifestUrl;
    }

    public void setManifestUrl(String manifestUrl) {
        this.manifestUrl = manifestUrl;
    }
}
