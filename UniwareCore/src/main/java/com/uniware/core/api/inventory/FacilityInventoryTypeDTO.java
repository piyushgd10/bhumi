package com.uniware.core.api.inventory;

import java.util.Date;

import com.uniware.core.entity.ItemTypeInventory.Type;

/**
 * Created by piyush on 7/25/16.
 */
public class FacilityInventoryTypeDTO {

    private String  facilityCode;
    private Integer facilityId;
    private Date    ageingDate;
    private long    quantity;
    private long    notFound;
    private long    blocked;
    private long    damaged;
    private long    lost;
    private Type type;

    //    public FacilityInventoryTypeDTO(String facilityCode, Date ageingDate, long quantity, long notFound, long blocked, long damaged, long lost, Type type) {
    //        setFacilityCode(facilityCode);
    //        setAgeingDate(ageingDate);
    //        setQuantity(quantity);
    //        setNotFound(notFound);
    //        setBlocked(blocked);
    //        setDamaged(damaged);
    //        setLost(lost);
    //        setType(type);
    //    }

    public FacilityInventoryTypeDTO(Integer facilityId, Date ageingDate, long quantity, long notFound, long blocked, long damaged, long lost, Type type) {
        setFacilityId(facilityId);
        setAgeingDate(ageingDate);
        setQuantity(quantity);
        setNotFound(notFound);
        setBlocked(blocked);
        setDamaged(damaged);
        setLost(lost);
        setType(type);
    }

    //    public FacilityInventoryTypeDTO(String facilityCode, Date ageingDate, long quantity, Type type) {
    //        setFacilityCode(facilityCode);
    //        setAgeingDate(ageingDate);
    //        setQuantity(quantity);
    //        setType(type);
    //    }

    public FacilityInventoryTypeDTO(Integer facilityId, Date ageingDate, long quantity, Type type) {
        setFacilityId(facilityId);
        setAgeingDate(ageingDate);
        setQuantity(quantity);
        setType(type);
    }

    public FacilityInventoryTypeDTO(Integer facilityId, Date ageingDate, long quantity) {
        setFacilityId(facilityId);
        setAgeingDate(ageingDate);
        setQuantity(quantity);
    }

    //    public FacilityInventoryTypeDTO(String facilityCode, long quantity, long notFound, Type type) {
    //        setQuantity(quantity);
    //        setFacilityCode(facilityCode);
    //        setType(type);
    //        setNotFound(notFound);
    //    }
    //
    //    public FacilityInventoryTypeDTO(String facilityCode, long quantity) {
    //        setQuantity(quantity);
    //        setFacilityCode(facilityCode);
    //    }

    public FacilityInventoryTypeDTO(Integer facilityId, long quantity) {
        setQuantity(quantity);
        setFacilityId(facilityId);
    }

    public String getFacilityCode() {
        return facilityCode;
    }

    public void setFacilityCode(String facilityCode) {
        this.facilityCode = facilityCode;
    }

    public Integer getFacilityId() {
        return facilityId;
    }

    public void setFacilityId(Integer facilityId) {
        this.facilityId = facilityId;
    }

    public Date getAgeingDate() {
        return ageingDate;
    }

    public void setAgeingDate(Date ageingDate) {
        this.ageingDate = ageingDate;
    }

    public long getQuantity() {
        return quantity;
    }

    public void setQuantity(long quantity) {
        this.quantity = quantity;
    }

    public long getNotFound() {
        return notFound;
    }

    public void setNotFound(long notFound) {
        this.notFound = notFound;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public long getBlocked() {
        return blocked;
    }

    public void setBlocked(long blocked) {
        this.blocked = blocked;
    }

    public long getDamaged() {
        return damaged;
    }

    public void setDamaged(long damaged) {
        this.damaged = damaged;
    }

    public long getLost() {
        return lost;
    }

    public void setLost(long lost) {
        this.lost = lost;
    }

}
