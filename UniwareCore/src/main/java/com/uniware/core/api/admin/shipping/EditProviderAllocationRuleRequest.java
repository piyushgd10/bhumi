/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 04-Aug-2012
 *  @author praveeng
 */
package com.uniware.core.api.admin.shipping;

import com.unifier.core.api.base.ServiceRequest;

import javax.validation.Valid;

/**
 * @author praveeng
 */
public class EditProviderAllocationRuleRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long        serialVersionUID = -120943013372752264L;

    @Valid
    private WsProviderAllocationRule wsProviderAllocationRule;

    /**
     * @return the wsProviderAllocationRule
     */
    public WsProviderAllocationRule getWsProviderAllocationRule() {
        return wsProviderAllocationRule;
    }

    /**
     * @param wsProviderAllocationRule the wsProviderAllocationRule to set
     */
    public void setWsProviderAllocationRule(WsProviderAllocationRule wsProviderAllocationRule) {
        this.wsProviderAllocationRule = wsProviderAllocationRule;
    }

}
