/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jan 23, 2012
 *  @author singla
 */
package com.uniware.core.api.shipping;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author Sunny
 */
public class GetShippingProviderParametersRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -2681965325110136571L;

    @NotBlank
    private String            shippingProviderCode;

    public String getShippingProviderCode() {
        return shippingProviderCode;
    }

    public void setShippingProviderCode(String shippingProviderCode) {
        this.shippingProviderCode = shippingProviderCode;
    }

}
