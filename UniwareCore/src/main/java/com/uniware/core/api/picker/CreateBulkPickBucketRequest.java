package com.uniware.core.api.picker;

import javax.validation.constraints.Min;

import com.unifier.core.api.base.ServiceRequest;

/**
 * Created by bhuvneshwarkumar on 29/08/16.
 */
public class CreateBulkPickBucketRequest extends ServiceRequest {

    private static final long serialVersionUID = -8273266534893112700L;

    @Min(value = 1)
    private Integer           noOfBucket;
    private Integer           length;
    private Integer           width;
    private Integer           height;

    public Integer getNoOfBucket() {
        return noOfBucket;
    }

    public void setNoOfBucket(Integer noOfBucket) {
        this.noOfBucket = noOfBucket;
    }

    public Integer getLength() {
        return length;
    }

    public void setLength(Integer length) {
        this.length = length;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }
}
