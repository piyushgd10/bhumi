/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 07/05/14
 *  @author amit
 */

package com.uniware.core.api.templatecustomization;

import com.unifier.core.api.base.ServiceResponse;

public class GetCustomizedDummyTemplateResponse extends ServiceResponse {

    private static final long serialVersionUID = -294691940602356683L;

    private String            evaluatedTemplate;

    public GetCustomizedDummyTemplateResponse() {
        super();
    }

    public String getEvaluatedTemplate() {
        return evaluatedTemplate;
    }

    public void setEvaluatedTemplate(String evaluatedTemplate) {
        this.evaluatedTemplate = evaluatedTemplate;
    }

    @Override
    public String toString() {
        return "GetCustomizedDummyTemplateResponse{" + "evaluatedTemplate='" + evaluatedTemplate + '\'' + '}';
    }
}