/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 02-Jun-2012
 *  @author praveeng
 */
package com.uniware.core.api.transition;

import com.unifier.core.api.base.ServiceResponse;

public class CreateItemsResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 519067111173819436L;
    private String            itemCodes;

    /**
     * @return the itemCodes
     */
    public String getItemCodes() {
        return itemCodes;
    }

    /**
     * @param itemCodes the itemCodes to set
     */
    public void setItemCodes(String itemCodes) {
        this.itemCodes = itemCodes;
    }

}
