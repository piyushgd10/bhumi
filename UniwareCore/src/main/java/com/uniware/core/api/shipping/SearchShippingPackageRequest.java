/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 29-Mar-2012
 *  @author ankit
 */
package com.uniware.core.api.shipping;

import java.util.List;

import javax.validation.Valid;

import com.unifier.core.api.base.ServiceRequest;
import com.unifier.core.pagination.SearchOptions;
import com.unifier.core.utils.DateUtils;

/**
 * @author ankit
 */
public class SearchShippingPackageRequest extends ServiceRequest {

    /**
     *
     */
    private static final long   serialVersionUID = 6134594963166598899L;
    private String              shippingPackageCode;
    private String              saleOrderCode;
    private String              channelCode;
    private List<String>        statuses;
    private DateUtils.DateRange createTime;
    private DateUtils.DateRange dispatchTime;
    private boolean             containsCancelledItems;
    private boolean             onHold;
    private String              shippingProvider;
    private String              shippingMethod;
    private String              trackingNumber;
    private String              invoiceCode;
    private Boolean             cashOnDelivery;
    @Valid
    private SearchOptions       searchOptions;
    private Boolean             paymentReconciled;
    private String              itemTypeSkuCode;
    private Integer             updatedSinceInMinutes;

    /**
     * @return the shippingPackageCode
     */
    public String getShippingPackageCode() {
        return shippingPackageCode;
    }

    /**
     * @param shippingPackageCode the shippingPackageCode to set
     */
    public void setShippingPackageCode(String shippingPackageCode) {
        this.shippingPackageCode = shippingPackageCode;
    }

    /**
     * @return the statuses
     */
    public List<String> getStatuses() {
        return statuses;
    }

    /**
     * @param statuses the statuses to set
     */
    public void setStatuses(List<String> statuses) {
        this.statuses = statuses;
    }

    public DateUtils.DateRange getDispatchTime() {
        return dispatchTime;
    }

    public void setDispatchTime(DateUtils.DateRange dispatchTime) {
        this.dispatchTime = dispatchTime;
    }

    public DateUtils.DateRange getCreateTime() {
        return createTime;
    }

    public void setCreateTime(DateUtils.DateRange createTime) {
        this.createTime = createTime;
    }

    /**
     * @return the containsCancelledItems
     */
    public boolean isContainsCancelledItems() {
        return containsCancelledItems;
    }

    /**
     * @param containsCancelledItems the containsCancelledItems to set
     */
    public void setContainsCancelledItems(boolean containsCancelledItems) {
        this.containsCancelledItems = containsCancelledItems;
    }

    /**
     * @return the shippingProvider
     */
    public String getShippingProvider() {
        return shippingProvider;
    }

    /**
     * @param shippingProvider the shippingProvider to set
     */
    public void setShippingProvider(String shippingProvider) {
        this.shippingProvider = shippingProvider;
    }

    /**
     * @return the shippingMethod
     */
    public String getShippingMethod() {
        return shippingMethod;
    }

    /**
     * @param shippingMethod the shippingMethod to set
     */
    public void setShippingMethod(String shippingMethod) {
        this.shippingMethod = shippingMethod;
    }

    /**
     * @return the trackingNumber
     */
    public String getTrackingNumber() {
        return trackingNumber;
    }

    /**
     * @param trackingNumber the trackingNumber to set
     */
    public void setTrackingNumber(String trackingNumber) {
        this.trackingNumber = trackingNumber;
    }

    /**
     * @return the searchOptions
     */
    public SearchOptions getSearchOptions() {
        return searchOptions;
    }

    /**
     * @param searchOptions the searchOptions to set
     */
    public void setSearchOptions(SearchOptions searchOptions) {
        this.searchOptions = searchOptions;
    }

    /**
     * @return the cashOnDelivery
     */
    public Boolean getCashOnDelivery() {
        return cashOnDelivery;
    }

    /**
     * @param cashOnDelivery the cashOnDelivery to set
     */
    public void setCashOnDelivery(Boolean cashOnDelivery) {
        this.cashOnDelivery = cashOnDelivery;
    }

    /**
     * @return the invoiceCode
     */
    public String getInvoiceCode() {
        return invoiceCode;
    }

    /**
     * @param invoiceCode the invoiceCode to set
     */
    public void setInvoiceCode(String invoiceCode) {
        this.invoiceCode = invoiceCode;
    }

    /**
     * @return the onHold
     */
    public boolean isOnHold() {
        return onHold;
    }

    /**
     * @param onHold the onHold to set
     */
    public void setOnHold(boolean onHold) {
        this.onHold = onHold;
    }

    /**
     * @return the saleOrderCodeContains
     */
    public String getSaleOrderCode() {
        return saleOrderCode;
    }

    /**
     * @param saleOrderCode the saleOrderCode to set
     */
    public void setSaleOrderCode(String saleOrderCode) {
        this.saleOrderCode = saleOrderCode;
    }

    /**
     * @return the paymentReconciled
     */
    public Boolean getPaymentReconciled() {
        return paymentReconciled;
    }

    /**
     * @param paymentReconciled the paymentReconciled to set
     */
    public void setPaymentReconciled(Boolean paymentReconciled) {
        this.paymentReconciled = paymentReconciled;
    }

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    public String getItemTypeSkuCode() {
        return itemTypeSkuCode;
    }

    public void setItemTypeSkuCode(String itemTypeSkuCode) {
        this.itemTypeSkuCode = itemTypeSkuCode;
    }

    public Integer getUpdatedSinceInMinutes() {
        return updatedSinceInMinutes;
    }

    public void setUpdatedSinceInMinutes(Integer updatedSinceInMinutes) {
        this.updatedSinceInMinutes = updatedSinceInMinutes;
    }
}
