/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 14-Feb-2012
 *  @author vibhu
 */
package com.uniware.core.api.party;

import com.uniware.core.entity.Party;

/**
 * @author vibhu
 */
public class PartyDetailDTO {

    private String  code;
    private String  name;
    private String  pan;
    private String  tin;
    private String  cstNumber;
    private String  stNumber;
    private String  website;
    private Integer binaryObjectId;
    private Integer signatureBinaryObjectId;
    private boolean registeredDealer;

    /**
     * @param party
     */
    public PartyDetailDTO(Party party) {
        this.code = party.getCode();
        this.name = party.getName();
        this.pan = party.getPan();
        this.tin = party.getTin();
        this.cstNumber = party.getCstNumber();
        this.stNumber = party.getStNumber();
        this.website = party.getWebsite();
        this.registeredDealer = party.isRegisteredDealer();
        if (party.getBinaryObject() != null) {
            this.binaryObjectId = party.getBinaryObject().getId();
        }
        if (party.getSignatureBinaryObject() != null) {
            this.signatureBinaryObjectId = party.getSignatureBinaryObject().getId();
        }

    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the pan
     */
    public String getPan() {
        return pan;
    }

    /**
     * @param pan the pan to set
     */
    public void setPan(String pan) {
        this.pan = pan;
    }

    /**
     * @return the tin
     */
    public String getTin() {
        return tin;
    }

    /**
     * @param tin the tin to set
     */
    public void setTin(String tin) {
        this.tin = tin;
    }

    /**
     * @return the cstNumber
     */
    public String getCstNumber() {
        return cstNumber;
    }

    /**
     * @param cstNumber the cstNumber to set
     */
    public void setCstNumber(String cstNumber) {
        this.cstNumber = cstNumber;
    }

    /**
     * @return the stNumber
     */
    public String getStNumber() {
        return stNumber;
    }

    /**
     * @param stNumber the stNumber to set
     */
    public void setStNumber(String stNumber) {
        this.stNumber = stNumber;
    }

    /**
     * @return the website
     */
    public String getWebsite() {
        return website;
    }

    /**
     * @param website the website to set
     */
    public void setWebsite(String website) {
        this.website = website;
    }

    /**
     * @return the binaryObjectId
     */
    public Integer getBinaryObjectId() {
        return binaryObjectId;
    }

    /**
     * @param binaryObjectId the binaryObjectId to set
     */
    public void setBinaryObjectId(Integer binaryObjectId) {
        this.binaryObjectId = binaryObjectId;
    }

    public Integer getSignatureBinaryObjectId() {
        return signatureBinaryObjectId;
    }

    public void setSignatureBinaryObjectId(Integer signatureBinaryObjectId) {
        this.signatureBinaryObjectId = signatureBinaryObjectId;
    }

    public boolean isRegisteredDealer() {
        return registeredDealer;
    }

    public void setRegisteredDealer(boolean registeredDealer) {
        this.registeredDealer = registeredDealer;
    }

}
