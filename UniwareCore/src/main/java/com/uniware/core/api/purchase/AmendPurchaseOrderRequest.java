/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jun 17, 2012
 *  @author singla
 */
package com.uniware.core.api.purchase;

import com.unifier.core.api.base.ServiceRequest;

import java.math.BigDecimal;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author singla
 */
public class AmendPurchaseOrderRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long         serialVersionUID = 6576925592925244155L;

    @NotBlank
    private String                    purchaseOrderCode;

    @NotEmpty
    @Valid
    private List<WsPurchaseOrderItem> purchaseOrderItems;

    private String                    logisticChargesDivisionMethod;

    private BigDecimal                logisticCharges  = BigDecimal.ZERO;

    @NotNull
    private Integer                   userId;

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return the purchaseOrderItems
     */
    public List<WsPurchaseOrderItem> getPurchaseOrderItems() {
        return purchaseOrderItems;
    }

    /**
     * @param purchaseOrderItems the purchaseOrderItems to set
     */
    public void setPurchaseOrderItems(List<WsPurchaseOrderItem> purchaseOrderItems) {
        this.purchaseOrderItems = purchaseOrderItems;
    }

    /**
     * @return the purchaseOrderCode
     */
    public String getPurchaseOrderCode() {
        return purchaseOrderCode;
    }

    /**
     * @param purchaseOrderCode the purchaseOrderCode to set
     */
    public void setPurchaseOrderCode(String purchaseOrderCode) {
        this.purchaseOrderCode = purchaseOrderCode;
    }

    /**
     * @return the logisticChargesDivisionMethod
     */
    public String getLogisticChargesDivisionMethod() {
        return logisticChargesDivisionMethod;
    }

    /**
     * @param logisticChargesDivisionMethod the logisticChargesDivisionMethod to set
     */
    public void setLogisticChargesDivisionMethod(String logisticChargesDivisionMethod) {
        this.logisticChargesDivisionMethod = logisticChargesDivisionMethod;
    }

    /**
     * @return the logisticCharges
     */
    public BigDecimal getLogisticCharges() {
        return logisticCharges;
    }

    /**
     * @param logisticCharges the logisticCharges to set
     */
    public void setLogisticCharges(BigDecimal logisticCharges) {
        this.logisticCharges = logisticCharges;
    }

}
