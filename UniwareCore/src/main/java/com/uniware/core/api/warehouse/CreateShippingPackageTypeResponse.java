/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 24, 2011
 *  @author ankitp
 */
package com.uniware.core.api.warehouse;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author ankitp
 *
 */
public class CreateShippingPackageTypeResponse extends ServiceResponse{

    /**
     * 
     */
    private static final long serialVersionUID = 3656970563658098747L;

    private ShippingPackageTypeDTO shippingPackageTypeDTO;

    /**
     * @return the shippingPackageTypeDTO
     */
    public ShippingPackageTypeDTO getShippingPackageTypeDTO() {
        return shippingPackageTypeDTO;
    }

    /**
     * @param shippingPackageTypeDTO the shippingPackageTypeDTO to set
     */
    public void setShippingPackageTypeDTO(ShippingPackageTypeDTO shippingPackageTypeDTO) {
        this.shippingPackageTypeDTO = shippingPackageTypeDTO;
    }

}