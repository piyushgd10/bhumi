/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jun 17, 2012
 *  @author singla
 */
package com.uniware.core.api.returns;

import com.unifier.core.api.base.ServiceRequest;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

/**
 * @author singla
 */
public class DiscardReturnManifestRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -7056728248710821352L;

    @NotNull
    private Integer           userId;

    @NotBlank
    private String           returnManifestCode;

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return the returnManifestId
     */
    public String getReturnManifestCode() {
        return returnManifestCode;
    }

    /**
     * @param returnManifestId the returnManifestId to set
     */
    public void setReturnManifestCode(String returnManifestCode) {
        this.returnManifestCode = returnManifestCode;
    }
}
