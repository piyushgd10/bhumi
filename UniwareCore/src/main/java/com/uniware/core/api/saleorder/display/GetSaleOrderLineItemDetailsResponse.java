/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jan 10, 2012
 *  @author singla
 */
package com.uniware.core.api.saleorder.display;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Set;

import com.unifier.core.api.base.ServiceResponse;

public class GetSaleOrderLineItemDetailsResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long    serialVersionUID = 6284414547113409023L;

    private List<LineItemDetail> lineItemDetails;
    private boolean              invalidFacility;
    private Set<String>          validFacilities;

    public List<LineItemDetail> getLineItemDetails() {
        return lineItemDetails;
    }

    public void setLineItemDetails(List<LineItemDetail> lineItemDetails) {
        this.lineItemDetails = lineItemDetails;
    }

    public boolean isInvalidFacility() {
        return invalidFacility;
    }

    public void setInvalidFacility(boolean invalidFacility) {
        this.invalidFacility = invalidFacility;
    }

    public Set<String> getValidFacilities() {
        return validFacilities;
    }

    public void setValidFacilities(Set<String> validFacilities) {
        this.validFacilities = validFacilities;
    }

    public static class LineItemDetail {
        private String                 lineItemIdentifier;
        private List<SaleOrderItemDTO> saleOrderItems;

        public String getLineItemIdentifier() {
            return lineItemIdentifier;
        }

        public void setLineItemIdentifier(String lineItemIdentifier) {
            this.lineItemIdentifier = lineItemIdentifier;
        }

        public List<SaleOrderItemDTO> getSaleOrderItems() {
            return saleOrderItems;
        }

        public void setSaleOrderItems(List<SaleOrderItemDTO> saleOrderItems) {
            this.saleOrderItems = saleOrderItems;
        }

    }

    public static class SaleOrderItemDTO {
        private int        id;
        private String     code;
        private String     skuCode;
        private String     productName;
        private BigDecimal sellingPrice;
        private BigDecimal shippingCharges       = BigDecimal.ZERO;
        private BigDecimal shippingMethodCharges = BigDecimal.ZERO;
        private BigDecimal cashOnDeliveryCharges = BigDecimal.ZERO;
        private BigDecimal giftWrapCharges       = BigDecimal.ZERO;
        private String     giftMessage;
        private String     statusCode;
        private String     itemCode;
        private boolean    cancellable;
        private boolean    holdable;
        private boolean    unholdable;
        private boolean    inventoryReleasable;
        private boolean    reversePickable;
        private boolean    packageModifiable;
        private boolean    facilitySwitchable;
        private String     shippingPackageCode;
        private String     shippingPackageStatusCode;
        private boolean    shippingInfoAvailable;
        private String     shippingProviderName;
        private String     trackingNumber;
        private Date       dispatchTime;
        private Integer    saleOrderItemAlternateId;
        private BigDecimal totalPrice;
        private boolean    onHold;
        private boolean    bundled;
        private String     combinationIdentifier;
        private String     combinationDescription;
        private String     cancellationReason;
        private String     returnReason;
        private String     channelProductId;
        private String     sellerSkuCode;

        public SaleOrderItemDTO() {
            super();
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getChannelProductId() {
            return channelProductId;
        }

        public void setChannelProductId(String channelProductId) {
            this.channelProductId = channelProductId;
        }

        public String getSellerSkuCode() {
            return sellerSkuCode;
        }

        public void setSellerSkuCode(String sellerSkuCode) {
            this.sellerSkuCode = sellerSkuCode;
        }

        public String getProductName() {
            return productName;
        }

        public void setProductName(String productName) {
            this.productName = productName;
        }

        public BigDecimal getSellingPrice() {
            return sellingPrice;
        }

        public void setSellingPrice(BigDecimal sellingPrice) {
            this.sellingPrice = sellingPrice;
        }

        public BigDecimal getShippingCharges() {
            return shippingCharges;
        }

        public void setShippingCharges(BigDecimal shippingCharges) {
            this.shippingCharges = shippingCharges;
        }

        public BigDecimal getShippingMethodCharges() {
            return shippingMethodCharges;
        }

        public void setShippingMethodCharges(BigDecimal shippingMethodCharges) {
            this.shippingMethodCharges = shippingMethodCharges;
        }

        public BigDecimal getCashOnDeliveryCharges() {
            return cashOnDeliveryCharges;
        }

        public void setCashOnDeliveryCharges(BigDecimal cashOnDeliveryCharges) {
            this.cashOnDeliveryCharges = cashOnDeliveryCharges;
        }

        public BigDecimal getGiftWrapCharges() {
            return giftWrapCharges;
        }

        public void setGiftWrapCharges(BigDecimal giftWrapCharges) {
            this.giftWrapCharges = giftWrapCharges;
        }

        public String getGiftMessage() {
            return giftMessage;
        }

        public void setGiftMessage(String giftMessage) {
            this.giftMessage = giftMessage;
        }

        public String getStatusCode() {
            return statusCode;
        }

        public void setStatusCode(String statusCode) {
            this.statusCode = statusCode;
        }

        public String getItemCode() {
            return itemCode;
        }

        public void setItemCode(String itemCode) {
            this.itemCode = itemCode;
        }

        public boolean isCancellable() {
            return cancellable;
        }

        public void setCancellable(boolean cancellable) {
            this.cancellable = cancellable;
        }

        public boolean isHoldable() {
            return holdable;
        }

        public void setHoldable(boolean holdable) {
            this.holdable = holdable;
        }

        public boolean isUnholdable() {
            return unholdable;
        }

        public void setUnholdable(boolean unholdable) {
            this.unholdable = unholdable;
        }

        public boolean isInventoryReleasable() {
            return inventoryReleasable;
        }

        public void setInventoryReleasable(boolean inventoryReleasable) {
            this.inventoryReleasable = inventoryReleasable;
        }

        public boolean isReversePickable() {
            return reversePickable;
        }

        public void setReversePickable(boolean reversePickable) {
            this.reversePickable = reversePickable;
        }

        public boolean isPackageModifiable() {
            return packageModifiable;
        }

        public void setPackageModifiable(boolean packageModifiable) {
            this.packageModifiable = packageModifiable;
        }

        public boolean isFacilitySwitchable() {
            return facilitySwitchable;
        }

        public void setFacilitySwitchable(boolean facilitySwitchable) {
            this.facilitySwitchable = facilitySwitchable;
        }

        public String getShippingPackageCode() {
            return shippingPackageCode;
        }

        public void setShippingPackageCode(String shippingPackageCode) {
            this.shippingPackageCode = shippingPackageCode;
        }

        public String getShippingPackageStatusCode() {
            return shippingPackageStatusCode;
        }

        public void setShippingPackageStatusCode(String shippingPackageStatusCode) {
            this.shippingPackageStatusCode = shippingPackageStatusCode;
        }

        public boolean isShippingInfoAvailable() {
            return shippingInfoAvailable;
        }

        public void setShippingInfoAvailable(boolean shippingInfoAvailable) {
            this.shippingInfoAvailable = shippingInfoAvailable;
        }

        public String getShippingProviderName() {
            return shippingProviderName;
        }

        public void setShippingProviderName(String shippingProviderName) {
            this.shippingProviderName = shippingProviderName;
        }

        public String getTrackingNumber() {
            return trackingNumber;
        }

        public void setTrackingNumber(String trackingNumber) {
            this.trackingNumber = trackingNumber;
        }

        public Date getDispatchTime() {
            return dispatchTime;
        }

        public void setDispatchTime(Date dispatchTime) {
            this.dispatchTime = dispatchTime;
        }

        public BigDecimal getTotalPrice() {
            return totalPrice;
        }

        public void setTotalPrice(BigDecimal totalPrice) {
            this.totalPrice = totalPrice;
        }

        public Integer getSaleOrderItemAlternateId() {
            return saleOrderItemAlternateId;
        }

        public void setSaleOrderItemAlternateId(Integer saleOrderItemAlternateId) {
            this.saleOrderItemAlternateId = saleOrderItemAlternateId;
        }

        public boolean isOnHold() {
            return onHold;
        }

        public void setOnHold(boolean onHold) {
            this.onHold = onHold;
        }

        public boolean isBundled() {
            return bundled;
        }

        public void setBundled(boolean bundled) {
            this.bundled = bundled;
        }

        public String getCombinationIdentifier() {
            return combinationIdentifier;
        }

        public void setCombinationIdentifier(String combinationIdentifier) {
            this.combinationIdentifier = combinationIdentifier;
        }

        public String getSkuCode() {
            return skuCode;
        }

        public void setSkuCode(String skuCode) {
            this.skuCode = skuCode;
        }

        public String getCombinationDescription() {
            return combinationDescription;
        }

        public void setCombinationDescription(String combinationDescription) {
            this.combinationDescription = combinationDescription;
        }

        public String getCancellationReason() {
            return cancellationReason;
        }

        public void setCancellationReason(String cancellationReason) {
            this.cancellationReason = cancellationReason;
        }

        public String getReturnReason() {
            return returnReason;
        }

        public void setReturnReason(String returnReason) {
            this.returnReason = returnReason;
        }

    }
}
