/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 24-Dec-2011
 *  @author vibhu
 */
package com.uniware.core.api.channel;

import com.uniware.core.entity.ChannelItemType;

/**
 * @author Sunny Agarwal
 */
public class ChannelItemTypeLookupDTO {

    private String channelCode;

    private String channelProductId;

    private String sellerSkuCode;

    private String skuCode;

    public ChannelItemTypeLookupDTO() {
        super();
    }

    public ChannelItemTypeLookupDTO(ChannelItemType cit) {
        this.channelCode = cit.getChannel().getCode();
        this.channelProductId = cit.getChannelProductId();
        this.sellerSkuCode = cit.getSellerSkuCode();
        this.skuCode = cit.getItemType() != null ? cit.getItemType().getSkuCode() : null;
    }

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    public String getChannelProductId() {
        return channelProductId;
    }

    public void setChannelProductId(String channelProductId) {
        this.channelProductId = channelProductId;
    }

    public String getSellerSkuCode() {
        return sellerSkuCode;
    }

    public void setSellerSkuCode(String sellerSkuCode) {
        this.sellerSkuCode = sellerSkuCode;
    }

    public String getSkuCode() {
        return skuCode;
    }

    public void setSkuCode(String skuCode) {
        this.skuCode = skuCode;
    }

}
