package com.uniware.core.api.warehouse;

import com.unifier.core.api.base.ServiceRequest;
import com.unifier.core.pagination.SearchOptions;

/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 9, 2012
 *  @author praveeng
 */

public class SearchItemTypesRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 8412267883565481215L;

    private String            keyword;
    private String            productCode;
    private String            categoryCode;
    private Boolean           getInventorySnapshot;
    private SearchOptions     searchOptions;

    public SearchItemTypesRequest() {
        super();
    }

    public SearchItemTypesRequest(String keyword, String productCode, String categoryCode, Boolean getInventorySnapshot, SearchOptions searchOptions) {
        super();
        this.keyword = keyword;
        this.productCode = productCode;
        this.categoryCode = categoryCode;
        this.getInventorySnapshot = getInventorySnapshot;
        this.searchOptions = searchOptions;
    }

    public SearchItemTypesRequest(String keyword) {
        super();
        this.keyword = keyword;
    }

    /**
     * @return the productCode
     */
    public String getProductCode() {
        return productCode;
    }

    /**
     * @param productCode the productCode to set
     */
    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    /**
     * @return the categoryCode
     */
    public String getCategoryCode() {
        return categoryCode;
    }

    /**
     * @param categoryCode the categoryCode to set
     */
    public void setCategoryCode(String categoryCode) {
        this.categoryCode = categoryCode;
    }

    /**
     * @return the keyword
     */
    public String getKeyword() {
        return keyword;
    }

    /**
     * @param keyword the keyword to set
     */
    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    /**
     * @return the searchOptions
     */
    public SearchOptions getSearchOptions() {
        return searchOptions;
    }

    /**
     * @param searchOptions the searchOptions to set
     */
    public void setSearchOptions(SearchOptions searchOptions) {
        this.searchOptions = searchOptions;
    }

    /**
     * @return the getInventorySnapshot
     */
    public Boolean getGetInventorySnapshot() {
        return getInventorySnapshot;
    }

    /**
     * @param getInventorySnapshot the getInventorySnapshot to set
     */
    public void setGetInventorySnapshot(Boolean getInventorySnapshot) {
        this.getInventorySnapshot = getInventorySnapshot;
    }

}
