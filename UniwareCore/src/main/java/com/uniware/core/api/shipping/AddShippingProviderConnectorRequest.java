/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 11-Feb-2015
 *  @author parijat
 */
package com.uniware.core.api.shipping;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.unifier.core.api.base.ServiceRequest;
import com.uniware.core.api.model.WSShippingProviderConnector;

public class AddShippingProviderConnectorRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 8854783702923585020L;

    @NotNull
    @Valid
    private WSShippingProviderConnector wsShippingProviderConnector;

    /**
     * @return the wsShippingProviderConnector
     */
    public WSShippingProviderConnector getWsShippingProviderConnector() {
        return wsShippingProviderConnector;
    }

    /**
     * @param wsShippingProviderConnector the wsShippingProviderConnector to set
     */
    public void setWsShippingProviderConnector(WSShippingProviderConnector wsShippingProviderConnector) {
        this.wsShippingProviderConnector = wsShippingProviderConnector;
    }

}
