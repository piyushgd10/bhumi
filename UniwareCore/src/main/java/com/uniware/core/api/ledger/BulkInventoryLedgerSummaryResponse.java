/**
 * Copyright 2017 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version 1.0, 19/09/17
 * @author aditya
 */
package com.uniware.core.api.ledger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.unifier.core.api.base.ServiceResponse;

public class BulkInventoryLedgerSummaryResponse extends ServiceResponse {

    private List<LedgerSummary> ledgerSummary     = new ArrayList<>();

    private List<String>        invalidSkus       = new ArrayList<>();

    private List<String>        zeroInventorySkus = new ArrayList<>();

    public List<String> getZeroInventorySkus() {
        return zeroInventorySkus;
    }

    public void setZeroInventorySkus(List<String> zeroInventorySkus) {
        this.zeroInventorySkus = zeroInventorySkus;
    }

    public List<LedgerSummary> getLedgerSummary() {
        return ledgerSummary;
    }

    public void setLedgerSummary(List<LedgerSummary> ledgerSummary) {
        this.ledgerSummary = ledgerSummary;
    }

    public List<String> getInvalidSkus() {
        return invalidSkus;
    }

    public void setInvalidSkus(List<String> invalidSkus) {
        this.invalidSkus = invalidSkus;
    }

    public static class LedgerSummary {

        private String                                                               skuCode;

        private Map<String, FetchInventoryLedgerSummaryResponse.LedgerEntryGroupDTO> ledgerEntries = new HashMap<>();

        public Map<String, FetchInventoryLedgerSummaryResponse.LedgerEntryGroupDTO> getLedgerEntries() {
            return ledgerEntries;
        }

        public void setLedgerEntries(Map<String, FetchInventoryLedgerSummaryResponse.LedgerEntryGroupDTO> ledgerEntries) {
            this.ledgerEntries = ledgerEntries;
        }

        public String getSkuCode() {
            return skuCode;
        }

        public void setSkuCode(String skuCode) {
            this.skuCode = skuCode;
        }
    }
}
