package com.uniware.core.api.jabong;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author ankur
 */
public class ReleaseShelfResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 6761860160497930449L;

    private ShelfDTO          shelfDTO         = new ShelfDTO();

    /**
     * @return the shelfDTO
     */
    public ShelfDTO getShelfDTO() {
        return shelfDTO;
    }

    /**
     * @param shelfDTO the shelfDTO to set
     */
    public void setshelfDTO(ShelfDTO shelfDTO) {
        this.shelfDTO = shelfDTO;
    }

    public static class ShelfDTO {

        private String shelfCode;

        public String getShelfCode() {
            return shelfCode;
        }

        public void setShelfCode(String shelfCode) {
            this.shelfCode = shelfCode;
        }

    }
}
