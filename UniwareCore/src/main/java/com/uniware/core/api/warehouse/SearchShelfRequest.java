/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 24, 2011
 *  @author ankitp
 */
package com.uniware.core.api.warehouse;

import com.unifier.core.api.base.ServiceRequest;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author ankitp
 *
 */
public class SearchShelfRequest extends ServiceRequest{

    /**
     * 
     */
    private static final long serialVersionUID = -8383164138007062297L;
    
    @NotEmpty
    private String shelfTypeCode;
    @NotEmpty
    private String sectionCode;

    private String codeContains;
    /**
     * @return the shelfTypeCode
     */
    public String getShelfTypeCode() {
        return shelfTypeCode;
    }
    /**
     * @param shelfTypeCode the shelfTypeCode to set
     */
    public void setShelfTypeCode(String shelfTypeCode) {
        this.shelfTypeCode = shelfTypeCode;
    }
    /**
     * @return the sectionCode
     */
    public String getSectionCode() {
        return sectionCode;
    }
    /**
     * @param sectionCode the sectionCode to set
     */
    public void setSectionCode(String sectionCode) {
        this.sectionCode = sectionCode;
    }
    /**
     * @return the codeContains
     */
    public String getCodeContains() {
        return codeContains;
    }
    /**
     * @param codeContains the codeContains to set
     */
    public void setCodeContains(String codeContains) {
        this.codeContains = codeContains;
    }

}
