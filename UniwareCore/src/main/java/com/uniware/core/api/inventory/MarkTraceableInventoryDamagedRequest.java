/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Oct 16, 2012
 *  @author singla
 */
package com.uniware.core.api.inventory;

import com.unifier.core.api.base.ServiceRequest;

import org.hibernate.validator.constraints.NotEmpty;

public class MarkTraceableInventoryDamagedRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 5359336999738796537L;

    @NotEmpty
    private String            itemCode;
    
    private String            rejectionReason;

    /**
     * @return the itemCode
     */
    public String getItemCode() {
        return itemCode;
    }

    /**
     * @param itemCode the itemCode to set
     */
    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }
    
    public String getRejectionReason() {
        return rejectionReason;
    }

    public void setRejectionReason(String rejectionReason) {
        this.rejectionReason = rejectionReason;
    }

}
