/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 26-May-2012
 *  @author praveeng
 */
package com.uniware.core.api.returns;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author praveeng
 */
public class GetReturnManifestRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 6013823597397620914L;
    private String            returnManifestCode;

    /**
     * @return the returnManifestCode
     */
    public String getReturnManifestCode() {
        return returnManifestCode;
    }

    /**
     * @param returnManifestCode the returnManifestCode to set
     */
    public void setReturnManifestCode(String returnManifestCode) {
        this.returnManifestCode = returnManifestCode;
    }

}
