/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 26-Apr-2013
 *  @author unicom
 */
package com.uniware.core.api.shipping;

import java.util.Date;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author Sunny
 */
public class UpdateShippingPackageTrackingStatusRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 243242342L;

    @NotBlank
    private String            shippingPackageCode;

    @NotBlank
    private String            shippingProviderCode;

    @NotBlank
    private String            trackingNumber;

    @NotBlank
    private String            statusCode;

    private String            shipmentTrackingStatusCode;

    private String            providerStatusCode;

    @NotNull
    private Date              statusDate;

    private boolean           disableStatusUpdate;

    public String getShippingPackageCode() {
        return shippingPackageCode;
    }

    public void setShippingPackageCode(String shippingPackageCode) {
        this.shippingPackageCode = shippingPackageCode;
    }

    public String getShippingProviderCode() {
        return shippingProviderCode;
    }

    public void setShippingProviderCode(String shippingProviderCode) {
        this.shippingProviderCode = shippingProviderCode;
    }

    public String getTrackingNumber() {
        return trackingNumber;
    }

    public void setTrackingNumber(String trackingNumber) {
        this.trackingNumber = trackingNumber;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getProviderStatusCode() {
        return providerStatusCode;
    }

    public void setProviderStatusCode(String providerStatusCode) {
        this.providerStatusCode = providerStatusCode;
    }

    public String getShipmentTrackingStatusCode() {
        return shipmentTrackingStatusCode;
    }

    public void setShipmentTrackingStatusCode(String shipmentTrackingStatusCode) {
        this.shipmentTrackingStatusCode = shipmentTrackingStatusCode;
    }

    public Date getStatusDate() {
        return statusDate;
    }

    public void setStatusDate(Date statusDate) {
        this.statusDate = statusDate;
    }

    public boolean isDisableStatusUpdate() {
        return disableStatusUpdate;
    }

    public void setDisableStatusUpdate(boolean disableStatusUpdate) {
        this.disableStatusUpdate = disableStatusUpdate;
    }

}
