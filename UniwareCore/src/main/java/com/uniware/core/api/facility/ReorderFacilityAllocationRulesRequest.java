/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Oct 26, 2012
 *  @author Pankaj
 */
package com.uniware.core.api.facility;

import java.util.HashMap;
import java.util.Map;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author Pankaj
 */
public class ReorderFacilityAllocationRulesRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long    serialVersionUID                  = -3624265116388653677L;
    private Map<String, Integer> allocationRuleNameToPreferenceMap = new HashMap<String, Integer>();

    /**
     * @return the allocationRuleNameToPreferenceMap
     */
    public Map<String, Integer> getAllocationRuleNameToPreferenceMap() {
        return allocationRuleNameToPreferenceMap;
    }

    /**
     * @param allocationRuleNameToPreferenceMap the allocationRuleNameToPreferenceMap to set
     */
    public void setAllocationRuleNameToPreferenceMap(Map<String, Integer> allocationRuleNameToPreferenceMap) {
        this.allocationRuleNameToPreferenceMap = allocationRuleNameToPreferenceMap;
    }

}
