/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 27-Aug-2015
 *  @author parijat
 */
package com.uniware.core.api.shipping;

import javax.validation.constraints.NotNull;

import com.unifier.core.api.base.ServiceRequest;

public class ReassessShippingPackageRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 4589876568876587567L;

    @NotNull
    private String            shippingPackageCode;

    /**
     * @return the shippingPackageCode
     */
    public String getShippingPackageCode() {
        return shippingPackageCode;
    }

    /**
     * @param shippingPackageCode the shippingPackageCode to set
     */
    public void setShippingPackageCode(String shippingPackageCode) {
        this.shippingPackageCode = shippingPackageCode;
    }

}
