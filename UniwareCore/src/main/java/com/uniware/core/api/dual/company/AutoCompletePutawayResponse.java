/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jun 20, 2012
 *  @author singla
 */
package com.uniware.core.api.dual.company;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author singla
 */
public class AutoCompletePutawayResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 1141955267014622029L;

}
