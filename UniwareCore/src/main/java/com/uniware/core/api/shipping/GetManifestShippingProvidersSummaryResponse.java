/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 28-Jan-2015
 *  @author akshaykochhar
 */
package com.uniware.core.api.shipping;

import java.util.List;

import com.unifier.core.api.base.ServiceResponse;
import com.uniware.core.entity.ShippingPackage;

public class GetManifestShippingProvidersSummaryResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long          serialVersionUID = 7033950911667503874L;

    private ManifestProviderSummaryDTO manifestSummary;

    public ManifestProviderSummaryDTO getManifestSummary() {
        return manifestSummary;
    }

    public void setManifestSummary(ManifestProviderSummaryDTO manifestSummary) {
        this.manifestSummary = manifestSummary;
    }

    public static class ManifestProviderSummaryDTO {

        private Integer                   totalShipments;
        private List<ManifestProviderDTO> shipmentProviders;

        public ManifestProviderSummaryDTO() {
        }

        public ManifestProviderSummaryDTO(Integer totalShipments, List<ManifestProviderDTO> shipmentProviders) {
            this.totalShipments = totalShipments;
            this.shipmentProviders = shipmentProviders;
        }

        public Integer getTotalShipments() {
            return totalShipments;
        }

        public void setTotalShipments(Integer totalShipments) {
            this.totalShipments = totalShipments;
        }

        public List<ManifestProviderDTO> getShipmentProviders() {
            return shipmentProviders;
        }

        public void setShipmentProviders(List<ManifestProviderDTO> shipmentProviders) {
            this.shipmentProviders = shipmentProviders;
        }

        public static class ManifestProviderDTO {

            public ManifestProviderDTO() {
            }

            public ManifestProviderDTO(String shippingProviderCode, String shippingProviderName) {
                this.providerCode = shippingProviderCode;
                this.providerName = shippingProviderName;
            }

            private String  providerCode;
            private String  providerName;
            private int noOfShipments;
            private int noOfBoxes;
            private int noOfProducts;

            public Integer getNoOfShipments() {
                return noOfShipments;
            }

            public void setNoOfShipments(Integer noOfShipments) {
                this.noOfShipments = noOfShipments;
            }

            public Integer getNoOfBoxes() {
                return noOfBoxes;
            }

            public void setNoOfBoxes(Integer noOfBoxes) {
                this.noOfBoxes = noOfBoxes;
            }

            public Integer getNoOfProducts() {
                return noOfProducts;
            }

            public void setNoOfProducts(Integer noOfProducts) {
                this.noOfProducts = noOfProducts;
            }

            public String getProviderCode() {
                return providerCode;
            }

            public void setProviderCode(String providerCode) {
                this.providerCode = providerCode;
            }

            public String getProviderName() {
                return providerName;
            }

            public void setProviderName(String providerName) {
                this.providerName = providerName;
            }

            public void addShipment(ShippingPackage shippingPackage) {
                this.noOfBoxes += shippingPackage.getNoOfBoxes();
                this.noOfProducts += shippingPackage.getNoOfItems();
                this.noOfShipments++;
            }

        }
    }
}
