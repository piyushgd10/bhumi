/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 15, 2011
 *  @author singla
 */
package com.uniware.core.api.model;

import java.math.BigDecimal;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unifier.core.api.customfields.WsCustomFieldValue;
import com.unifier.core.utils.NumberUtils;
import com.uniware.core.entity.BundleItemType;

/**
 * @author singla
 */
public class WsSaleOrderItem {

    private static final Logger      LOG                   = LoggerFactory.getLogger(WsSaleOrderItem.class);

    @NotBlank
    private String                   itemSku;

    private String                   itemName;

    private String                   channelProductId;

    private String                   channelSaleOrderItemCode;

    @NotBlank
    private String                   shippingMethodCode;

    @NotBlank
    @Length(max = 45)
    private String                   code;

    private boolean                  giftWrap;

    @Length(max = 256)
    private String                   giftMessage;

    @NotNull
    @Min(value = 0)
    private BigDecimal               totalPrice;

    @NotNull
    @Min(value = 0)
    private BigDecimal               sellingPrice;

    @Min(value = 0)
    private BigDecimal               prepaidAmount         = BigDecimal.ZERO;

    @Min(value = 0)
    private BigDecimal               discount              = BigDecimal.ZERO;

    @Min(value = 0)
    private BigDecimal               shippingCharges       = BigDecimal.ZERO;

    @Min(value = 0)
    private BigDecimal               shippingMethodCharges = BigDecimal.ZERO;

    @Min(value = 0)
    private BigDecimal               cashOnDeliveryCharges = BigDecimal.ZERO;

    @Min(value = 0)
    private BigDecimal               giftWrapCharges       = BigDecimal.ZERO;

    @Length(max = 45)
    private String                   voucherCode;

    @Min(value = 0)
    private BigDecimal               voucherValue          = BigDecimal.ZERO;

    @Min(value = 0)
    private BigDecimal               storeCredit           = BigDecimal.ZERO;

    @Min(value = 0)
    private BigDecimal               channelTransferPrice  = BigDecimal.ZERO;

    private BigDecimal               transferPrice         = BigDecimal.ZERO;

    private int                      packetNumber;
    private boolean                  onHold;
    private String                   facilityCode;

    @Length(max = 45)
    private String                   combinationIdentifier;

    private Boolean                  requiresCustomization;

    @Valid
    private WsAddressRef             shippingAddress;

    @Valid
    private List<WsCustomFieldValue> customFieldValues;

    private List<String>             itemDetailFields;

    public WsSaleOrderItem() {
        super();
    }

    public String getItemSku() {
        return itemSku;
    }

    public void setItemSku(String itemSku) {
        this.itemSku = itemSku;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getChannelProductId() {
        return channelProductId;
    }

    public void setChannelProductId(String channelProductId) {
        this.channelProductId = channelProductId;
    }

    public String getChannelSaleOrderItemCode() {
        return channelSaleOrderItemCode;
    }

    public void setChannelSaleOrderItemCode(String channelSaleOrderItemCode) {
        this.channelSaleOrderItemCode = channelSaleOrderItemCode;
    }

    public String getShippingMethodCode() {
        return shippingMethodCode;
    }

    public void setShippingMethodCode(String shippingMethodCode) {
        this.shippingMethodCode = shippingMethodCode;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public boolean isGiftWrap() {
        return giftWrap;
    }

    public void setGiftWrap(boolean giftWrap) {
        this.giftWrap = giftWrap;
    }

    public String getGiftMessage() {
        return giftMessage;
    }

    public void setGiftMessage(String giftMessage) {
        this.giftMessage = giftMessage;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    public BigDecimal getSellingPrice() {
        return sellingPrice;
    }

    public void setSellingPrice(BigDecimal sellingPrice) {
        this.sellingPrice = sellingPrice;
    }

    public BigDecimal getPrepaidAmount() {
        return prepaidAmount;
    }

    public void setPrepaidAmount(BigDecimal prepaidAmount) {
        this.prepaidAmount = prepaidAmount;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    public BigDecimal getShippingCharges() {
        return shippingCharges;
    }

    public void setShippingCharges(BigDecimal shippingCharges) {
        this.shippingCharges = shippingCharges;
    }

    public BigDecimal getShippingMethodCharges() {
        return shippingMethodCharges;
    }

    public void setShippingMethodCharges(BigDecimal shippingMethodCharges) {
        this.shippingMethodCharges = shippingMethodCharges;
    }

    public BigDecimal getCashOnDeliveryCharges() {
        return cashOnDeliveryCharges;
    }

    public void setCashOnDeliveryCharges(BigDecimal cashOnDeliveryCharges) {
        this.cashOnDeliveryCharges = cashOnDeliveryCharges;
    }

    public BigDecimal getGiftWrapCharges() {
        return giftWrapCharges;
    }

    public void setGiftWrapCharges(BigDecimal giftWrapCharges) {
        this.giftWrapCharges = giftWrapCharges;
    }

    public String getVoucherCode() {
        return voucherCode;
    }

    public void setVoucherCode(String voucherCode) {
        this.voucherCode = voucherCode;
    }

    public BigDecimal getVoucherValue() {
        return voucherValue;
    }

    public void setVoucherValue(BigDecimal voucherValue) {
        this.voucherValue = voucherValue;
    }

    public BigDecimal getStoreCredit() {
        return storeCredit;
    }

    public void setStoreCredit(BigDecimal storeCredit) {
        this.storeCredit = storeCredit;
    }

    public BigDecimal getChannelTransferPrice() {
        return channelTransferPrice;
    }

    public void setChannelTransferPrice(BigDecimal channelTransferPrice) {
        this.channelTransferPrice = channelTransferPrice;
    }

    public BigDecimal getTransferPrice() {
        return transferPrice;
    }

    public void setTransferPrice(BigDecimal transferPrice) {
        this.transferPrice = transferPrice;
    }

    public int getPacketNumber() {
        return packetNumber;
    }

    public void setPacketNumber(int packetNumber) {
        this.packetNumber = packetNumber;
    }

    public boolean isOnHold() {
        return onHold;
    }

    public void setOnHold(boolean onHold) {
        this.onHold = onHold;
    }

    public String getFacilityCode() {
        return facilityCode;
    }

    public void setFacilityCode(String facilityCode) {
        this.facilityCode = facilityCode;
    }

    public String getCombinationIdentifier() {
        return combinationIdentifier;
    }

    public void setCombinationIdentifier(String combinationIdentifier) {
        this.combinationIdentifier = combinationIdentifier;
    }

    public Boolean getRequiresCustomization() {
        return requiresCustomization;
    }

    public void setRequiresCustomization(Boolean requiresCustomization) {
        this.requiresCustomization = requiresCustomization;
    }

    public WsAddressRef getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(WsAddressRef shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    public List<WsCustomFieldValue> getCustomFieldValues() {
        return customFieldValues;
    }

    public void setCustomFieldValues(List<WsCustomFieldValue> customFieldValues) {
        this.customFieldValues = customFieldValues;
    }

    public static class WsSaleOrderItemPricing {
        private BundleItemType        bundleItemType;
        private String                code;
        private BigDecimal            totalPrice;
        private BigDecimal            sellingPrice;
        private BigDecimal            prepaidAmount;
        private BigDecimal            discount;
        private BigDecimal            shippingCharges;
        private BigDecimal            shippingMethodCharges;
        private BigDecimal            cashOnDeliveryCharges;
        private BigDecimal            giftWrapCharges;
        private BigDecimal            voucherValue;
        private BigDecimal            storeCredit;
        private BigDecimal            channelTransferPrice;
        private BigDecimal            transferPrice;
        private Queue<BundleItemType> bundleItemTypes = new LinkedBlockingQueue<>();
        private BigDecimal            totalRatio      = BigDecimal.ZERO;
        private Integer               counter         = 0;

        public WsSaleOrderItemPricing(WsSaleOrderItem wsSaleOrderItem) {
            this(wsSaleOrderItem, null);
        }

        public WsSaleOrderItemPricing(WsSaleOrderItem wsSaleOrderItem, List<BundleItemType> bundleItemTypes) {
            code = wsSaleOrderItem.getCode();
            totalPrice = wsSaleOrderItem.getTotalPrice();
            sellingPrice = wsSaleOrderItem.getSellingPrice();
            prepaidAmount = wsSaleOrderItem.getPrepaidAmount();
            discount = wsSaleOrderItem.getDiscount();
            shippingCharges = wsSaleOrderItem.getShippingCharges();
            shippingMethodCharges = wsSaleOrderItem.getShippingMethodCharges();
            cashOnDeliveryCharges = wsSaleOrderItem.getCashOnDeliveryCharges();
            giftWrapCharges = wsSaleOrderItem.getGiftWrapCharges();
            voucherValue = wsSaleOrderItem.getVoucherValue();
            storeCredit = wsSaleOrderItem.getStoreCredit();
            channelTransferPrice = wsSaleOrderItem.getChannelTransferPrice();
            transferPrice = wsSaleOrderItem.getTransferPrice();
            if (bundleItemTypes != null) {
                this.bundleItemTypes.addAll(bundleItemTypes);
                for (BundleItemType bundleItemType : bundleItemTypes) {
                    totalRatio = totalRatio.add(bundleItemType.getPriceRatio());
                }
            }
        }

        public WsSaleOrderItemPricing(WsSaleOrderItemPricing pricing, BundleItemType bundleItemType, BigDecimal ratio, BigDecimal totalRatio, String code) {
            this.bundleItemType = bundleItemType;
            this.code = code;
            boolean totalRatioZero = BigDecimal.ZERO.compareTo(totalRatio) == 0;
            LOG.info("Total Ratio: {}, Total Ratio Zero: {}", totalRatio, totalRatioZero);
            sellingPrice = totalRatioZero ? BigDecimal.ZERO : NumberUtils.divide(pricing.getSellingPrice().multiply(ratio), totalRatio);
            prepaidAmount = totalRatioZero ? BigDecimal.ZERO : NumberUtils.divide(pricing.getPrepaidAmount().multiply(ratio), totalRatio);
            discount = totalRatioZero ? BigDecimal.ZERO : NumberUtils.divide(pricing.getDiscount().multiply(ratio), totalRatio);
            shippingCharges = totalRatioZero ? BigDecimal.ZERO : NumberUtils.divide(pricing.getShippingCharges().multiply(ratio), totalRatio);
            shippingMethodCharges = totalRatioZero ? BigDecimal.ZERO : NumberUtils.divide(pricing.getShippingMethodCharges().multiply(ratio), totalRatio);
            cashOnDeliveryCharges = totalRatioZero ? BigDecimal.ZERO : NumberUtils.divide(pricing.getCashOnDeliveryCharges().multiply(ratio), totalRatio);
            giftWrapCharges = totalRatioZero ? BigDecimal.ZERO : NumberUtils.divide(pricing.getGiftWrapCharges().multiply(ratio), totalRatio);
            voucherValue = totalRatioZero ? BigDecimal.ZERO : NumberUtils.divide(pricing.getVoucherValue().multiply(ratio), totalRatio);
            storeCredit = totalRatioZero ? BigDecimal.ZERO : NumberUtils.divide(pricing.getStoreCredit().multiply(ratio), totalRatio);
            channelTransferPrice = totalRatioZero ? BigDecimal.ZERO : NumberUtils.divide(pricing.getChannelTransferPrice().multiply(ratio), totalRatio);
            transferPrice = totalRatioZero ? BigDecimal.ZERO : pricing.getTransferPrice() != null ? NumberUtils.divide(pricing.getTransferPrice().multiply(ratio), totalRatio)
                    : null;
            totalPrice = totalRatioZero ? BigDecimal.ZERO : sellingPrice.add(shippingCharges).add(shippingMethodCharges).add(cashOnDeliveryCharges).add(giftWrapCharges);
        }

        public boolean hasNext() {
            return !bundleItemTypes.isEmpty();
        }

        public WsSaleOrderItemPricing next() {
            BundleItemType bundleItemType = bundleItemTypes.poll();
            WsSaleOrderItemPricing next;
            if (bundleItemTypes.isEmpty()) {
                next = new WsSaleOrderItemPricing(this, bundleItemType, BigDecimal.ONE, BigDecimal.ONE, code + '-' + counter++);
            } else {
                next = new WsSaleOrderItemPricing(this, bundleItemType, bundleItemType.getPriceRatio(), totalRatio, code + '-' + counter++);
                totalRatio = totalRatio.subtract(bundleItemType.getPriceRatio());
                totalPrice = totalPrice.subtract(next.getTotalPrice());
                sellingPrice = sellingPrice.subtract(next.getSellingPrice());
                prepaidAmount = prepaidAmount.subtract(next.getPrepaidAmount());
                discount = discount.subtract(next.getDiscount());
                shippingCharges = shippingCharges.subtract(next.getShippingCharges());
                shippingMethodCharges = shippingMethodCharges.subtract(next.getShippingMethodCharges());
                cashOnDeliveryCharges = cashOnDeliveryCharges.subtract(next.getCashOnDeliveryCharges());
                giftWrapCharges = giftWrapCharges.subtract(next.getGiftWrapCharges());
                voucherValue = voucherValue.subtract(next.getVoucherValue());
                storeCredit = storeCredit.subtract(next.getStoreCredit());
                channelTransferPrice = channelTransferPrice.subtract(next.getChannelTransferPrice());
                transferPrice = transferPrice != null ? transferPrice.subtract(next.getTransferPrice()) : null;
            }
            return next;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public BundleItemType getBundleItemType() {
            return bundleItemType;
        }

        public void setBundleItemType(BundleItemType bundleItemType) {
            this.bundleItemType = bundleItemType;
        }

        public BigDecimal getTotalPrice() {
            return totalPrice;
        }

        public void setTotalPrice(BigDecimal totalPrice) {
            this.totalPrice = totalPrice;
        }

        public BigDecimal getSellingPrice() {
            return sellingPrice;
        }

        public void setSellingPrice(BigDecimal sellingPrice) {
            this.sellingPrice = sellingPrice;
        }

        public BigDecimal getPrepaidAmount() {
            return prepaidAmount;
        }

        public void setPrepaidAmount(BigDecimal prepaidAmount) {
            this.prepaidAmount = prepaidAmount;
        }

        public BigDecimal getDiscount() {
            return discount;
        }

        public void setDiscount(BigDecimal discount) {
            this.discount = discount;
        }

        public BigDecimal getShippingCharges() {
            return shippingCharges;
        }

        public void setShippingCharges(BigDecimal shippingCharges) {
            this.shippingCharges = shippingCharges;
        }

        public BigDecimal getShippingMethodCharges() {
            return shippingMethodCharges;
        }

        public void setShippingMethodCharges(BigDecimal shippingMethodCharges) {
            this.shippingMethodCharges = shippingMethodCharges;
        }

        public BigDecimal getCashOnDeliveryCharges() {
            return cashOnDeliveryCharges;
        }

        public void setCashOnDeliveryCharges(BigDecimal cashOnDeliveryCharges) {
            this.cashOnDeliveryCharges = cashOnDeliveryCharges;
        }

        public BigDecimal getGiftWrapCharges() {
            return giftWrapCharges;
        }

        public void setGiftWrapCharges(BigDecimal giftWrapCharges) {
            this.giftWrapCharges = giftWrapCharges;
        }

        public BigDecimal getVoucherValue() {
            return voucherValue;
        }

        public void setVoucherValue(BigDecimal voucherValue) {
            this.voucherValue = voucherValue;
        }

        public BigDecimal getStoreCredit() {
            return storeCredit;
        }

        public void setStoreCredit(BigDecimal storeCredit) {
            this.storeCredit = storeCredit;
        }

        public BigDecimal getChannelTransferPrice() {
            return channelTransferPrice;
        }

        public void setChannelTransferPrice(BigDecimal channelTransferPrice) {
            this.channelTransferPrice = channelTransferPrice;
        }

        public BigDecimal getTransferPrice() {
            return transferPrice;
        }

        public void setTransferPrice(BigDecimal transferPrice) {
            this.transferPrice = transferPrice;
        }
    }

    public List<String> getItemDetailFields() {
        return itemDetailFields;
    }

    public void setItemDetailFields(List<String> itemDetailFields) {
        this.itemDetailFields = itemDetailFields;
    }

    public static void main(String[] args) {
        BigDecimal total = new BigDecimal(100);
        BigDecimal newTotal = BigDecimal.ZERO;
        int quantity = 3;
        while (quantity > 0) {
            BigDecimal val = NumberUtils.divide(total, new BigDecimal(quantity));
            newTotal = newTotal.add(val);
            System.out.println(val);
            total = total.subtract(val);
            quantity--;
        }
        System.out.println(newTotal);
    }
}
