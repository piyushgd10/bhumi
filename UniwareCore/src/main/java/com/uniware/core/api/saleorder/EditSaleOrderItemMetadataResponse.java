/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 30-Oct-2013
 *  @author akshay
 */
package com.uniware.core.api.saleorder;

import com.unifier.core.api.base.ServiceResponse;

public class EditSaleOrderItemMetadataResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

}
