/*
 * Copyright 2017 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 31/05/17
 * @author piyush
 */
package com.uniware.core.api.invoice;

import com.unifier.core.api.base.ServiceResponse;

public class GenerateOrUpdateInvoiceResponse extends ServiceResponse {

    /**
     *
     */
    private static final long serialVersionUID = 3773116698355945263L;

    private String            invoiceCode;

    private String            invoiceDisplayCode;

    public String getInvoiceDisplayCode() {
        return invoiceDisplayCode;
    }

    public void setInvoiceDisplayCode(String invoiceDisplayCode) {
        this.invoiceDisplayCode = invoiceDisplayCode;
    }

    /**
     * @return the invoiceCode
     */
    public String getInvoiceCode() {
        return invoiceCode;
    }

    /**
     * @param invoiceCode the invoiceCode to set
     */
    public void setInvoiceCode(String invoiceCode) {
        this.invoiceCode = invoiceCode;
    }
}
