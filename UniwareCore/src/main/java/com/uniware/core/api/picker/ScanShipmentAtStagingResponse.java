/*
 * Copyright 2017 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 25/09/17
 * @author piyush
 */
package com.uniware.core.api.picker;

import com.unifier.core.api.base.ServiceResponse;
import com.uniware.core.api.packer.PutbackItemDTO;

import java.util.List;

public class ScanShipmentAtStagingResponse extends ServiceResponse {

    private String  shelfCode;

    private List<PutbackItemDTO> putbackItems;

    public String getShelfCode() {
        return shelfCode;
    }

    public void setShelfCode(String shelfCode) {
        this.shelfCode = shelfCode;
    }

    public List<PutbackItemDTO> getPutbackItems() {
        return putbackItems;
    }

    public void setPutbackItems(List<PutbackItemDTO> putbackItems) {
        this.putbackItems = putbackItems;
    }
}
