/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 31-Oct-2014
 *  @author parijat
 */
package com.uniware.core.api.channel;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author parijat
 *
 */
public class ChangeChannelReconciliationSyncStatusResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 23456789876545678L;

}
