package com.uniware.core.api.party;

import com.unifier.core.api.base.ServiceResponse;
import com.uniware.core.api.party.dto.GenericPartyDTO;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sagar Sahni on 16/01/18.
 */
public class GetBillingPartyAndFacilityWithGSTINResponse extends ServiceResponse {

    private List<GenericPartyDTO> partyDTOList = new ArrayList<>();

    public GetBillingPartyAndFacilityWithGSTINResponse() {
    }

    public List<GenericPartyDTO> getPartyDTOList() {

        return partyDTOList;
    }

    public void setPartyDTOList(List<GenericPartyDTO> partyDTOList) {
        this.partyDTOList = partyDTOList;
    }
}
