/*
 * Copyright 2016 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 31/8/16 4:44 PM
 * @author bhuvneshwarkumar
 */

package com.uniware.core.api.catalog;

import com.unifier.core.api.base.ServiceResponse;
import com.uniware.core.api.catalog.dto.ItemTypeFullDTO;

/**
 * Created by bhuvneshwarkumar on 31/08/16.
 */
public class GetItemTypeByItemResponse extends ServiceResponse {

    private ItemTypeFullDTO itemTypeDTO;

    public ItemTypeFullDTO getItemTypeDTO() {
        return itemTypeDTO;
    }

    public void setItemTypeDTO(ItemTypeFullDTO itemTypeDTO) {
        this.itemTypeDTO = itemTypeDTO;
    }
}
