/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Apr 15, 2013
 *  @author praveeng
 */
package com.uniware.core.api.catalog;

import javax.validation.Valid;

import com.unifier.core.api.base.ServiceRequest;

public class EditFacilityItemTypeRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long  serialVersionUID = -5620911375650604009L;

    @Valid
    private WsFacilityItemType wsFacilityItemType;

    /**
     * @return the wsFacilityItemType
     */
    public WsFacilityItemType getWsFacilityItemType() {
        return wsFacilityItemType;
    }

    /**
     * @param wsFacilityItemType the wsFacilityItemType to set
     */
    public void setWsFacilityItemType(WsFacilityItemType wsFacilityItemType) {
        this.wsFacilityItemType = wsFacilityItemType;
    }

}
