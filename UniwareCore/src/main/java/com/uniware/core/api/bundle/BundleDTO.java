/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 26-Jul-2013
 *  @author sunny
 */
package com.uniware.core.api.bundle;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.uniware.core.entity.Bundle;
import com.uniware.core.entity.BundleItemType;
import com.uniware.core.entity.ItemType;

public class BundleDTO {

    private Integer                    id;
    private String                     code;
    private Date                       created;
    private Date                       updated;
    private ItemTypeDTO                itemTypeDTO;
    private List<ComponentItemTypeDTO> componentItemTypes = new ArrayList<ComponentItemTypeDTO>(2);

    public BundleDTO(Bundle bundle) {
        id = bundle.getId();
        code = bundle.getCode();
        created = bundle.getCreated();
        updated = bundle.getUpdated();
        itemTypeDTO = new ItemTypeDTO(bundle.getItemType());
        for (BundleItemType bundleItemType : bundle.getBundleItemTypes()) {
            componentItemTypes.add(new ComponentItemTypeDTO(bundleItemType));
        }
    }

    public static class ItemTypeDTO {
        private String skuCode;
        private String name;

        public ItemTypeDTO() {
        }

        public ItemTypeDTO(ItemType itemType) {
            skuCode = itemType.getSkuCode();
            name = itemType.getName();
        }

        public String getSkuCode() {
            return skuCode;
        }

        public void setSkuCode(String skuCode) {
            this.skuCode = skuCode;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public ItemTypeDTO getItemTypeDTO() {
        return itemTypeDTO;
    }

    public void setItemTypeDTO(ItemTypeDTO itemTypeDTO) {
        this.itemTypeDTO = itemTypeDTO;
    }

    public List<ComponentItemTypeDTO> getComponentItemTypes() {
        return componentItemTypes;
    }

    public void setComponentItemTypes(List<ComponentItemTypeDTO> componentItemTypes) {
        this.componentItemTypes = componentItemTypes;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    @Override
    public String toString() {
        return "BundleDTO [code=" + code + ", created=" + created + ", updated=" + updated + ", itemTypeDTO=" + itemTypeDTO.getSkuCode() + ", componentItemTypes="
                + componentItemTypes + "]";
    }

}
