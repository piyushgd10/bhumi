/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jul 27, 2012
 *  @author singla
 */
package com.uniware.core.api.party;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.annotation.constraints.OptionalBlank;
import com.unifier.core.annotation.constraints.OptionalNull;

/**
 * @author Sunny
 */
public class WsGenericParty {

    @OptionalBlank
    @Length(max = 100)
    private String               name;

    @NotBlank
    @Length(max = 100)
    @Pattern(regexp = "^[a-zA-Z0-9-_]+$", message = "Party Code can only contain alphanumeric, _ and -.")
    private String               code;

    @Length(max = 100)
    @Pattern(regexp = "^[a-zA-Z0-9-_.]+$", message = "Party Code can only contain alphanumeric, _ , . and -")
    private String               alternateCode;

    @Length(max = 45)
    private String               pan;

    @Length(max = 45)
    private String               tin;

    @Length(max = 45)
    private String               cinNumber;

    @Length(max = 45)
    private String               cstNumber;

    @Length(max = 45)
    private String               stNumber;

    @Length(max = 45)
    private String               gstNumber;

    private Boolean              enabled;

    private Boolean              taxExempted;

    @Length(max = 256)
    private String               website;

    private boolean              registeredDealer;

    private Integer              binaryObjectId;

    private Integer              signatureBinaryObjectId;

    private String               uniwareAccessUrl;

    private String               uniwareApiUser;

    private String               uniwareApiPassword;

    @OptionalNull
    @Valid
    private WsPartyAddress       billingAddress;

    @OptionalNull
    @Valid
    private WsPartyAddress       shippingAddress;

    @Valid
    private List<WsPartyContact> partyContacts = new ArrayList<WsPartyContact>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getPan() {
        return pan;
    }

    public void setPan(String pan) {
        this.pan = pan;
    }

    public String getTin() {
        return tin;
    }

    public void setTin(String tin) {
        this.tin = tin;
    }

    public String getCstNumber() {
        return cstNumber;
    }

    public void setCstNumber(String cstNumber) {
        this.cstNumber = cstNumber;
    }

    public String getStNumber() {
        return stNumber;
    }

    public void setStNumber(String stNumber) {
        this.stNumber = stNumber;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public Boolean getTaxExempted() {
        return taxExempted;
    }

    public void setTaxExempted(Boolean taxExempted) {
        this.taxExempted = taxExempted;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public boolean isRegisteredDealer() {
        return registeredDealer;
    }

    public void setRegisteredDealer(boolean registeredDealer) {
        this.registeredDealer = registeredDealer;
    }

    public Integer getBinaryObjectId() {
        return binaryObjectId;
    }

    public void setBinaryObjectId(Integer binaryObjectId) {
        this.binaryObjectId = binaryObjectId;
    }

    public Integer getSignatureBinaryObjectId() {
        return signatureBinaryObjectId;
    }

    public void setSignatureBinaryObjectId(Integer signatureBinaryObjectId) {
        this.signatureBinaryObjectId = signatureBinaryObjectId;
    }

    public String getUniwareAccessUrl() {
        return uniwareAccessUrl;
    }

    public void setUniwareAccessUrl(String uniwareAccessUrl) {
        this.uniwareAccessUrl = uniwareAccessUrl;
    }

    public String getUniwareApiUser() {
        return uniwareApiUser;
    }

    public void setUniwareApiUser(String uniwareApiUser) {
        this.uniwareApiUser = uniwareApiUser;
    }

    public String getUniwareApiPassword() {
        return uniwareApiPassword;
    }

    public void setUniwareApiPassword(String uniwareApiPassword) {
        this.uniwareApiPassword = uniwareApiPassword;
    }

    public WsPartyAddress getBillingAddress() {
        return billingAddress;
    }

    public void setBillingAddress(WsPartyAddress billingAddress) {
        this.billingAddress = billingAddress;
    }

    public WsPartyAddress getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(WsPartyAddress shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    public List<WsPartyContact> getPartyContacts() {
        return partyContacts;
    }

    public void setPartyContacts(List<WsPartyContact> partyContacts) {
        this.partyContacts = partyContacts;
    }

    public String getCinNumber() {
        return cinNumber;
    }

    public void setCinNumber(String cinNumber) {
        this.cinNumber = cinNumber;
    }

    public String getAlternateCode() {
        return alternateCode;
    }

    public void setAlternateCode(String alternateCode) {
        this.alternateCode = alternateCode;
    }

    public String getGstNumber() {
        return gstNumber;
    }

    public void setGstNumber(String gstNumber) {
        this.gstNumber = gstNumber;
    }
}
