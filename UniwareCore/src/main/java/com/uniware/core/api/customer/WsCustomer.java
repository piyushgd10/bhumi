/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jul 27, 2012
 *  @author singla
 */
package com.uniware.core.api.customer;

import com.uniware.core.api.party.WsGenericParty;

/**
 * @author Sunny
 */
public class WsCustomer extends WsGenericParty {

    private boolean providesCform;

    private boolean dualCompanyRetail;

    public boolean isProvidesCform() {
        return providesCform;
    }

    public void setProvidesCform(boolean providesCform) {
        this.providesCform = providesCform;
    }

    public boolean isDualCompanyRetail() {
        return dualCompanyRetail;
    }

    public void setDualCompanyRetail(boolean dualCompanyRetail) {
        this.dualCompanyRetail = dualCompanyRetail;
    }

}
