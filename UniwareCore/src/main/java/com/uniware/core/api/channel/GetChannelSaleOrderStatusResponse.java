/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jun 20, 2012
 *  @author singla
 */
package com.uniware.core.api.channel;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author Sunny
 */
public class GetChannelSaleOrderStatusResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long     serialVersionUID = -8331014310944611193L;
    private ChannelOrderStatusDTO channelOrderStatusDTO;

    public ChannelOrderStatusDTO getChannelOrderStatusDTO() {
        return channelOrderStatusDTO;
    }

    public void setChannelOrderStatusDTO(ChannelOrderStatusDTO channelOrderStatusDTO) {
        this.channelOrderStatusDTO = channelOrderStatusDTO;
    }

}
