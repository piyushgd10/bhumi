/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 07-May-2014
 *  @author akshay
 */
package com.uniware.core.api.tenant;

import java.math.BigInteger;

public class TenantTransactionDetailDTO {

    private String     transactionType;

    private BigInteger returns       = BigInteger.ZERO;

    private BigInteger dispatched    = BigInteger.ZERO;

    private BigInteger reversePickup = BigInteger.ZERO;

    private BigInteger warehouses    = BigInteger.ZERO;

    private Integer    channels;

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public BigInteger getReturns() {
        return returns;
    }

    public void setReturns(BigInteger returns) {
        this.returns = returns;
    }

    public BigInteger getDispatched() {
        return dispatched;
    }

    public void setDispatched(BigInteger dispatched) {
        this.dispatched = dispatched;
    }

    public BigInteger getReversePickup() {
        return reversePickup;
    }

    public void setReversePickup(BigInteger reversePickup) {
        this.reversePickup = reversePickup;
    }

    public BigInteger getWarehouses() {
        return warehouses;
    }

    public void setWarehouses(BigInteger warehouses) {
        this.warehouses = warehouses;
    }

    public Integer getChannels() {
        return channels;
    }

    public void setChannels(Integer channels) {
        this.channels = channels;
    }
}
