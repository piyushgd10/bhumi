/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Nov 25, 2015
 *  @author akshay
 */
package com.uniware.core.api.recommendation;

import java.util.ArrayList;
import java.util.List;

import com.unifier.core.api.base.ServiceResponse;
import com.uniware.core.api.recommendation.dto.RecommendationProviderDTO;

public class GetRecommendationProvidersResponse extends ServiceResponse {

    private static final long serialVersionUID = -5698791052594617814L;
    
    private List<RecommendationProviderDTO> recommendationProviders = new ArrayList<>();

    public List<RecommendationProviderDTO> getRecommendationProviders() {
        return recommendationProviders;
    }

    public void setRecommendationProviders(final List<RecommendationProviderDTO> recommendationProviders) {
        this.recommendationProviders = recommendationProviders;
    }
}
