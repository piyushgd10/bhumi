package com.uniware.core.api.jabong;

import org.hibernate.validator.constraints.NotEmpty;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author ankur
 */
public class ReleaseShelfRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -4541125656301902840L;
    @NotEmpty
    private String            shelfCode;

    public String getShelfCode() {
        return shelfCode;
    }

    public void setShelfCode(String shelfCode) {
        this.shelfCode = shelfCode;
    }

}
