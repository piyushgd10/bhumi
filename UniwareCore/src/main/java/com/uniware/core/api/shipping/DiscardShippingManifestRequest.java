/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jun 17, 2012
 *  @author singla
 */
package com.uniware.core.api.shipping;

import com.unifier.core.api.base.ServiceRequest;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

/**
 * @author singla
 */
public class DiscardShippingManifestRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 7166195799316394064L;
    @NotNull
    private Integer           userId;

    @NotBlank
    private String            shippingManifestCode;

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return the shippingManifestCode
     */
    public String getShippingManifestCode() {
        return shippingManifestCode;
    }

    /**
     * @param shippingManifestCode the shippingManifestCode to set
     */
    public void setShippingManifestCode(String shippingManifestCode) {
        this.shippingManifestCode = shippingManifestCode;
    }

}
