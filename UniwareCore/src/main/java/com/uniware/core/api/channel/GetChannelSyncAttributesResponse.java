package com.uniware.core.api.channel;

import com.unifier.core.api.base.ServiceResponse;

import java.util.List;

/**
 * Created by Samdeesh on 7/29/15.
 */
public class GetChannelSyncAttributesResponse extends ServiceResponse {

    private static final long serialVersionUID = -1593985340157028319L;
    private List<ChannelSyncAttributesDTO> channels;

    public List<ChannelSyncAttributesDTO> getChannels() {
        return channels;
    }

    public void setChannels(List<ChannelSyncAttributesDTO> channels) {
        this.channels = channels;
    }
}
