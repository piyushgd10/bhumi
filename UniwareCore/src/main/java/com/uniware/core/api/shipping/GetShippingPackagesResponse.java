/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 17-Dec-2013
 *  @author akshay
 */
package com.uniware.core.api.shipping;

import java.util.ArrayList;
import java.util.List;

import com.unifier.core.api.base.ServiceResponse;

public class GetShippingPackagesResponse extends ServiceResponse{

    /**
     * 
     */
    private static final long serialVersionUID = 6450122808444320902L;
    List<String>              shippingPackages = new ArrayList<String>();

    public List<String> getShippingPackages() {
        return shippingPackages;
    }

    public void setShippingPackages(List<String> shippingPackages) {
        this.shippingPackages = shippingPackages;
    }

    public void addShippingPackage(String shippingPackageCode) {
        shippingPackages.add(shippingPackageCode);
    }

}
