/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Oct 20, 2015
 *  @author akshay
 */
package com.uniware.core.api.prices;

import com.unifier.core.api.base.ServiceResponse;
import com.uniware.core.api.prices.dto.PriceDTO;

public class EditChannelItemTypePriceResponse extends ServiceResponse {

    private static final long serialVersionUID = 4232588337843254365L;
    
    private PriceDTO channelItemTypePrice;
    
    private boolean isNoChangeDetected;

    public PriceDTO getChannelItemTypePrice() {
        return channelItemTypePrice;
    }

    public void setChannelItemTypePrice(PriceDTO channelItemTypePrice) {
        this.channelItemTypePrice = channelItemTypePrice;
    }

    public boolean isNoChangeDetected() {
        return isNoChangeDetected;
    }

    public void setNoChangeDetected(boolean isNoChangeDetected) {
        this.isNoChangeDetected = isNoChangeDetected;
    }
}
