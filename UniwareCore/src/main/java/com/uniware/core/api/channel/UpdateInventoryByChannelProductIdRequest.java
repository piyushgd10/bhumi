/*
 * Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 12/18/14 12:55 PM
 * @author amdalal
 */

package com.uniware.core.api.channel;

import javax.validation.constraints.Min;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

public class UpdateInventoryByChannelProductIdRequest extends ServiceRequest {

    private static final long serialVersionUID = 7341799508691282808L;

    @NotBlank
    private String            channelCode;

    @NotBlank
    private String            channelProductId;

    @Min(value = 0)
    private int               inventory;

    public UpdateInventoryByChannelProductIdRequest() {
        super();
    }

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    public String getChannelProductId() {
        return channelProductId;
    }

    public void setChannelProductId(String channelProductId) {
        this.channelProductId = channelProductId;
    }

    public int getInventory() {
        return inventory;
    }

    public void setInventory(int inventory) {
        this.inventory = inventory;
    }

    @Override
    public String toString() {
        return "UpdateInventoryByChannelProductIdRequest{" + "channelCode='" + channelCode + '\'' + ", channelProductId='" + channelProductId + '\'' + ", inventory=" + inventory
                + '}';
    }
}
