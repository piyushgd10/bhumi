/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 04-Aug-2012
 *  @author praveeng
 */
package com.uniware.core.api.admin.shipping;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

/**
 * @author praveeng
 */
public class WsProviderAllocationRule {
    @NotNull
    private Integer shippingProviderId;

    @NotBlank
    private String  name;

    @NotBlank
    private String  conditionExpressionText;

    @NotBlank
    private String  allocationCriteria;

    @NotNull
    private Integer preference;

    private boolean enabled;

    /**
     * @return the shippingProviderId
     */
    public Integer getShippingProviderId() {
        return shippingProviderId;
    }

    /**
     * @param shippingProviderId the shippingProviderId to set
     */
    public void setShippingProviderId(Integer shippingProviderId) {
        this.shippingProviderId = shippingProviderId;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the conditionExpressionText
     */
    public String getConditionExpressionText() {
        return conditionExpressionText;
    }

    /**
     * @param conditionExpressionText the conditionExpressionText to set
     */
    public void setConditionExpressionText(String conditionExpressionText) {
        this.conditionExpressionText = conditionExpressionText;
    }

    /**
     * @return the allocationCriteria
     */
    public String getAllocationCriteria() {
        return allocationCriteria;
    }

    /**
     * @param allocationCriteria the allocationCriteria to set
     */
    public void setAllocationCriteria(String allocationCriteria) {
        this.allocationCriteria = allocationCriteria;
    }

    /**
     * @return the preference
     */
    public int getPreference() {
        return preference;
    }

    /**
     * @param preference the preference to set
     */
    public void setPreference(int preference) {
        this.preference = preference;
    }

    /**
     * @return the enabled
     */
    public boolean isEnabled() {
        return enabled;
    }

    /**
     * @param enabled the enabled to set
     */
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

}
