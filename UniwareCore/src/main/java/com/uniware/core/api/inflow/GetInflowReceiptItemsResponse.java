/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Apr 14, 2012
 *  @author praveeng
 */
package com.uniware.core.api.inflow;

import com.unifier.core.api.base.ServiceResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * @author praveeng
 */
public class GetInflowReceiptItemsResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long          serialVersionUID   = 8128503480302482376L;
    private List<InflowReceiptItemDTO> inflowReceiptItems = new ArrayList<InflowReceiptItemDTO>();

    /**
     * @return the inflowReceiptItems
     */
    public List<InflowReceiptItemDTO> getInflowReceiptItems() {
        return inflowReceiptItems;
    }

    /**
     * @param inflowReceiptItems the inflowReceiptItems to set
     */
    public void setInflowReceiptItems(List<InflowReceiptItemDTO> inflowReceiptItems) {
        this.inflowReceiptItems = inflowReceiptItems;
    }
}
