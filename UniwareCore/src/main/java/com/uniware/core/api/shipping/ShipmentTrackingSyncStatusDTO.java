/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 24-Feb-2015
 *  @author parijat
 */
package com.uniware.core.api.shipping;

import java.io.Serializable;
import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "shipmentTrackingSyncStatus")
@CompoundIndexes({ @CompoundIndex(name = "tenant_shipping_provider", def = "{'tenantCode' :  1, 'shippingProviderCode' :  1}", unique = true) })
public class ShipmentTrackingSyncStatusDTO implements Serializable {

    @Id
    private String  id;

    private String  shippingProviderCode;
    private String  tenantCode;
    private boolean running;
    private String  message;
    private long    totalMileStones = 100;
    private long    currentMileStone;
    private int     successfulProcessed;
    private long    failedProcessed;
    private boolean lastSyncSuccessful;
    private Date    lastSyncTime;
    private Date    lastSyncFailedNotificationTime;

    public ShipmentTrackingSyncStatusDTO(String shippingProviderCode) {
        this.shippingProviderCode = shippingProviderCode;
    }

    public float getPercentageComplete() {
        if (totalMileStones == 0) {
            return 0;
        }
        return ((float) (currentMileStone * 10000 / totalMileStones)) / 100;
    }

    public void reset() {
        running = false;
        totalMileStones = 100;
        currentMileStone = 0;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the shippingProviderCode
     */
    public String getShippingProviderCode() {
        return shippingProviderCode;
    }

    /**
     * @param shippingProviderCode the shippingProviderCode to set
     */
    public void setShippingProviderCode(String shippingProviderCode) {
        this.shippingProviderCode = shippingProviderCode;
    }

    /**
     * @return the tenantCode
     */
    public String getTenantCode() {
        return tenantCode;
    }

    /**
     * @param tenantCode the tenantCode to set
     */
    public void setTenantCode(String tenantCode) {
        this.tenantCode = tenantCode;
    }

    /**
     * @return the running
     */
    public boolean isRunning() {
        return running;
    }

    /**
     * @param running the running to set
     */
    public void setRunning(boolean running) {
        this.running = running;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return the totalMileStones
     */
    public long getTotalMileStones() {
        return totalMileStones;
    }

    /**
     * @param totalMileStones the totalMileStones to set
     */
    public void setTotalMileStones(long totalMileStones) {
        this.totalMileStones = totalMileStones;
    }

    /**
     * @return the currentMileStone
     */
    public long getCurrentMileStone() {
        return currentMileStone;
    }

    /**
     * @param currentMileStone the currentMileStone to set
     */
    public void setCurrentMileStone(long currentMileStone) {
        this.currentMileStone = currentMileStone;
    }

    /**
     * @return the successfulProcessed
     */
    public int getSuccessfulProcessed() {
        return successfulProcessed;
    }

    /**
     * @param successfulProcessed the successfulProcessed to set
     */
    public void setSuccessfulProcessed(int successfulProcessed) {
        this.successfulProcessed = successfulProcessed;
    }

    /**
     * @return the failedProcessed
     */
    public long getFailedProcessed() {
        return failedProcessed;
    }

    /**
     * @param failedProcessed the failedProcessed to set
     */
    public void setFailedProcessed(long failedProcessed) {
        this.failedProcessed = failedProcessed;
    }

    /**
     * @return the lastSyncSuccessful
     */
    public boolean isLastSyncSuccessful() {
        return lastSyncSuccessful;
    }

    /**
     * @param lastSyncSuccessful the lastSyncSuccessful to set
     */
    public void setLastSyncSuccessful(boolean lastSyncSuccessful) {
        this.lastSyncSuccessful = lastSyncSuccessful;
    }

    /**
     * @return the lastSyncTime
     */
    public Date getLastSyncTime() {
        return lastSyncTime;
    }

    /**
     * @param lastSyncTime the lastSyncTime to set
     */
    public void setLastSyncTime(Date lastSyncTime) {
        this.lastSyncTime = lastSyncTime;
    }

    /**
     * @return the lastSyncFailedNotificationTime
     */
    public Date getLastSyncFailedNotificationTime() {
        return lastSyncFailedNotificationTime;
    }

    /**
     * @param lastSyncFailedNotificationTime the lastSyncFailedNotificationTime to set
     */
    public void setLastSyncFailedNotificationTime(Date lastSyncFailedNotificationTime) {
        this.lastSyncFailedNotificationTime = lastSyncFailedNotificationTime;
    }

}
