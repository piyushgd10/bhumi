/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Apr 13, 2012
 *  @author praveeng
 */
package com.uniware.core.api.purchase;

import java.util.List;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.unifier.core.api.base.ServiceRequest;
import com.unifier.core.utils.DateUtils.DateRange;

/**
 * @author Sunny Agarwal
 */
public class GetVendorPurchaseOrdersRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 5849565547194915028L;
    @NotEmpty
    private List<Integer>     vendorIds;
    @NotNull
    private DateRange         approvedBetween;

    public List<Integer> getVendorIds() {
        return vendorIds;
    }

    public void setVendorIds(List<Integer> vendorIds) {
        this.vendorIds = vendorIds;
    }

    public DateRange getApprovedBetween() {
        return approvedBetween;
    }

    public void setApprovedBetween(DateRange approvedBetween) {
        this.approvedBetween = approvedBetween;
    }

}
