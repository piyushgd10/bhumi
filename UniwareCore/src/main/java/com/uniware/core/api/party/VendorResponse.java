/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 20, 2012
 *  @author praveeng
 */
package com.uniware.core.api.party;

import com.unifier.core.api.base.ServiceResponse;

import com.uniware.core.api.party.dto.VendorFullDTO;

/**
 * @author praveeng
 */
public class VendorResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -3848289031578180571L;
    private VendorFullDTO     vendor;

    /**
     * @return the vendor
     */
    public VendorFullDTO getVendor() {
        return vendor;
    }

    /**
     * @param vendor the vendor to set
     */
    public void setVendor(VendorFullDTO vendor) {
        this.vendor = vendor;
    }
}
