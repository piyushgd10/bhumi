/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 05-Mar-2015
 *  @author parijat
 */
package com.uniware.core.api.shipping;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author parijat
 *
 */
public class VerifyandSyncShippingConnectorParamsResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 4186088625787150190L;

    private boolean           isGoodToSync;

    /**
     * @return the isGoodToSync
     */
    public boolean isGoodToSync() {
        return isGoodToSync;
    }

    /**
     * @param isGoodToSync the isGoodToSync to set
     */
    public void setGoodToSync(boolean isGoodToSync) {
        this.isGoodToSync = isGoodToSync;
    }

}
