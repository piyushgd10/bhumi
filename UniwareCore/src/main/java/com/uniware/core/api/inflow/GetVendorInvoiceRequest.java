/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 22-Mar-2014
 *  @author karunsingla
 */
package com.uniware.core.api.inflow;

import org.hibernate.validator.constraints.NotEmpty;

import com.unifier.core.api.base.ServiceRequest;

public class GetVendorInvoiceRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 7024325297239043978L;

    @NotEmpty
    private String            vendorInvoiceCode;

    public String getVendorInvoiceCode() {
        return vendorInvoiceCode;
    }

    public void setVendorInvoiceCode(String vendorInvoiceCode) {
        this.vendorInvoiceCode = vendorInvoiceCode;
    }

}
