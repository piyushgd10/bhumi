/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 *  @version     1.0, 30-Aug-2012
 *  @author akshay
 */
package com.uniware.core.api.inflow;

/**
 * @author akshay
 *
 */
public class EditInflowReceiptResponse extends SearchReceiptResponse{

    /**
     * 
     */
    private static final long serialVersionUID = -5784979554981339083L;

    private Integer           inflowReceiptId;
    private String            inflowReceiptCode;
    /**
     * @return the inflowReceiptId
     */
    public Integer getInflowReceiptId() {
        return inflowReceiptId;
    }
    /**
     * @param inflowReceiptId the inflowReceiptId to set
     */
    public void setInflowReceiptId(Integer inflowReceiptId) {
        this.inflowReceiptId = inflowReceiptId;
    }
    /**
     * @return the inflowReceiptCode
     */
    public String getInflowReceiptCode() {
        return inflowReceiptCode;
    }
    /**
     * @param inflowReceiptCode the inflowReceiptCode to set
     */
    public void setInflowReceiptCode(String inflowReceiptCode) {
        this.inflowReceiptCode = inflowReceiptCode;
    }


}
