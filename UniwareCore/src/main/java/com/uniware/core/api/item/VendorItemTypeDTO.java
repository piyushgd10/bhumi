/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jan 10, 2013
 *  @author Pankaj
 */
package com.uniware.core.api.item;

/**
 * @author Pankaj
 */
public class VendorItemTypeDTO {

    private boolean vendorEnabled;
    private String  vendorCode;
    private String  vendorName;
    private String  vendorSkuCode;
    private int     vendorId;

    public int getVendorId() {
        return vendorId;
    }

    public void setVendorId(int vendorId) {
        this.vendorId = vendorId;
    }

    /**
     * @return the vendorEnabled
     */
    public boolean isVendorEnabled() {
        return vendorEnabled;
    }

    /**
     * @param vendorEnabled the vendorEnabled to set
     */
    public void setVendorEnabled(boolean vendorEnabled) {
        this.vendorEnabled = vendorEnabled;
    }

    /**
     * @return the vendorCode
     */
    public String getVendorCode() {
        return vendorCode;
    }

    /**
     * @param vendorCode the vendorCode to set
     */
    public void setVendorCode(String vendorCode) {
        this.vendorCode = vendorCode;
    }

    /**
     * @return the vendorName
     */
    public String getVendorName() {
        return vendorName;
    }

    /**
     * @param vendorName the vendorName to set
     */
    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    /**
     * @return the vendorSkuCode
     */
    public String getVendorSkuCode() {
        return vendorSkuCode;
    }

    /**
     * @param vendorSkuCode the vendorSkuCode to set
     */
    public void setVendorSkuCode(String vendorSkuCode) {
        this.vendorSkuCode = vendorSkuCode;
    }

}
