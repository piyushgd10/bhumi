/*
 * Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 12/18/14 12:59 PM
 * @author amdalal
 */

package com.uniware.core.api.channel;

import com.unifier.core.api.base.ServiceResponse;

public class UpdateInventoryByChannelProductIdResponse extends ServiceResponse {

    private static final long serialVersionUID = -3323139278201511531L;
}
