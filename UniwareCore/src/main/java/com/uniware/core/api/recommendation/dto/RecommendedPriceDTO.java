/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Nov 25, 2015
 *  @author akshay
 */
package com.uniware.core.api.recommendation.dto;

import java.math.BigDecimal;

public class RecommendedPriceDTO {

    private BigDecimal msp;
    private BigDecimal mrp;
    private BigDecimal sellingPrice;
    private String     currencyCode;

    public RecommendedPriceDTO() {
    }

    public RecommendedPriceDTO(BigDecimal msp, BigDecimal mrp, BigDecimal sellingPrice, String currencyCode) {
        this.msp = msp;
        this.mrp = mrp;
        this.sellingPrice = sellingPrice;
        this.currencyCode = currencyCode;
    }

    public BigDecimal getMsp() {
        return msp;
    }

    public void setMsp(BigDecimal msp) {
        this.msp = msp;
    }

    public BigDecimal getMrp() {
        return mrp;
    }

    public void setMrp(BigDecimal mrp) {
        this.mrp = mrp;
    }

    public BigDecimal getSellingPrice() {
        return sellingPrice;
    }

    public void setSellingPrice(BigDecimal sellingPrice) {
        this.sellingPrice = sellingPrice;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }
    
}
