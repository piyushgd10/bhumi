/*
 * Copyright 2017 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 8/8/17 1:14 PM
 * @author digvijaysharma
 */

package com.uniware.core.api.picker;

import org.hibernate.validator.constraints.NotBlank;

/**
 * Created by bhuvneshwarkumar on 09/09/16.
 */
public class CompletePickBatchRequest extends SearchPicklistRequest {

    @NotBlank
    private String pickBucketCode;

    public String getPickBucketCode() {
        return pickBucketCode;
    }

    public void setPickBucketCode(String pickBucketCode) {
        this.pickBucketCode = pickBucketCode;
    }
}
