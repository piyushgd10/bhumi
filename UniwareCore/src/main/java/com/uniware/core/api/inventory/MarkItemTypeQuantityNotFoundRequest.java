/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 10-Jun-2014
 *  @author akshay
 */
package com.uniware.core.api.inventory;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

public class MarkItemTypeQuantityNotFoundRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @NotBlank
    private String            itemSku;
    
    @NotBlank
    private String            shelfCode;
    
    @NotNull
    @Min(value = 1)
    private Integer           quantityNotFound;

    public MarkItemTypeQuantityNotFoundRequest() {
    }

    public MarkItemTypeQuantityNotFoundRequest(String itemSku, String shelfCode, Integer quantityNotFound) {
        this.itemSku = itemSku;
        this.shelfCode = shelfCode;
        this.quantityNotFound = quantityNotFound;
    }

    public String getItemSku() {
        return itemSku;
    }

    public void setItemSku(String itemSku) {
        this.itemSku = itemSku;
    }

    public String getShelfCode() {
        return shelfCode;
    }

    public void setShelfCode(String shelfCode) {
        this.shelfCode = shelfCode;
    }

    public Integer getQuantityNotFound() {
        return quantityNotFound;
    }

    public void setQuantityNotFound(Integer quantityNotFound) {
        this.quantityNotFound = quantityNotFound;
    }

}
