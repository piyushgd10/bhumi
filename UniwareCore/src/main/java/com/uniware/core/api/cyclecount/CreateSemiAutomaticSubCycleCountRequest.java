package com.uniware.core.api.cyclecount;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Future;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

/**
 * Created by harshpal on 08/04/16.
 */
public class CreateSemiAutomaticSubCycleCountRequest extends ServiceRequest {

    @NotBlank
    private String                 cycleCountCode;

    @NotNull
    @Future
    private Date                   targetCompletionDate;

    @NotNull
    @Valid
    private List<WsCycleCountZone> zones;

    public String getCycleCountCode() {
        return cycleCountCode;
    }

    public void setCycleCountCode(String cycleCountCode) {
        this.cycleCountCode = cycleCountCode;
    }

    public Date getTargetCompletionDate() {
        return targetCompletionDate;
    }

    public void setTargetCompletionDate(Date targetCompletionDate) {
        this.targetCompletionDate = targetCompletionDate;
    }

    public List<WsCycleCountZone> getZones() {
        return zones;
    }

    public void setZones(List<WsCycleCountZone> zones) {
        this.zones = zones;
    }

    public static class WsCycleCountZone {
        @NotBlank
        private String zoneName;
        @Min(1)
        private int    shelfCount;

        public String getZoneName() {
            return zoneName;
        }

        public void setZoneName(String zoneName) {
            this.zoneName = zoneName;
        }

        public int getShelfCount() {
            return shelfCount;
        }

        public void setShelfCount(int shelfCount) {
            this.shelfCount = shelfCount;
        }
    }
}
