/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 11-Feb-2015
 *  @author parijat
 */
package com.uniware.core.api.admin.shipping;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.uniware.core.entity.ShippingProviderConnector;
import com.uniware.core.entity.ShippingProviderConnectorParameter;
import com.uniware.core.entity.ShippingSourceConnector;
import com.uniware.core.entity.ShippingSourceConnectorParameter;



public class ShippingProviderConnectorDTO {

    private String  name;
    private String  displayName;
    private String  helpText;
    private int     priority;
    private boolean requiredInAWBFetch;
    private boolean requiredInTracking;
    private String  verificationScriptName;
    private String  statusCode;
    private String  errorMessage;
    private List<ShippingProviderConnectorParameterDTO> shippingProviderConnectorParameters = new ArrayList<ShippingProviderConnectorParameterDTO>();

    public ShippingProviderConnectorDTO() {

    }

    public ShippingProviderConnectorDTO(ShippingProviderConnector connector, ShippingSourceConnector shippingSourceConnector) {
        this.name = shippingSourceConnector.getName();
        this.displayName = shippingSourceConnector.getDisplayName();
        this.helpText = shippingSourceConnector.getHelpText();
        this.priority = shippingSourceConnector.getPriority();
        this.requiredInAWBFetch = shippingSourceConnector.isRequiredInAwbFetch();
        this.requiredInTracking = shippingSourceConnector.isRequiredInTracking();
        this.verificationScriptName = shippingSourceConnector.getVerificationScriptName();
        this.statusCode = connector.getStatusCode().name();
        this.errorMessage = connector.getErrorMessage();
        Map<String, ShippingProviderConnectorParameter> namedConnectorParams = new HashMap<>();
        for (ShippingProviderConnectorParameter spcp : connector.getShippingProviderConnectorParameters()) {
            namedConnectorParams.put(spcp.getName(), spcp);
        }
        for (ShippingSourceConnectorParameter sscp : shippingSourceConnector.getShippingSourceConnectorParameters()) {
            if (sscp.getType() != ShippingSourceConnectorParameter.Type.HIDDEN && namedConnectorParams.get(sscp.getName()) != null) {
                ShippingProviderConnectorParameter providerConnectorParamter = namedConnectorParams.get(sscp.getName());
                shippingProviderConnectorParameters.add(new ShippingProviderConnectorParameterDTO(providerConnectorParamter, sscp));
            }
        }

        Collections.sort(shippingProviderConnectorParameters, new Comparator<ShippingProviderConnectorParameterDTO>() {
            @Override
            public int compare(ShippingProviderConnectorParameterDTO o1, ShippingProviderConnectorParameterDTO o2) {
                return o1.getPriority() - o2.getPriority();
            }
        });
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the displayName
     */
    public String getDisplayName() {
        return displayName;
    }

    /**
     * @param displayName the displayName to set
     */
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    /**
     * @return the helpText
     */
    public String getHelpText() {
        return helpText;
    }

    /**
     * @param helpText the helpText to set
     */
    public void setHelpText(String helpText) {
        this.helpText = helpText;
    }

    /**
     * @return the priority
     */
    public int getPriority() {
        return priority;
    }

    /**
     * @param priority the priority to set
     */
    public void setPriority(int priority) {
        this.priority = priority;
    }

    /**
     * @return the requiredInAWBFetch
     */
    public boolean isRequiredInAWBFetch() {
        return requiredInAWBFetch;
    }

    /**
     * @param requiredInAWBFetch the requiredInAWBFetch to set
     */
    public void setRequiredInAWBFetch(boolean requiredInAWBFetch) {
        this.requiredInAWBFetch = requiredInAWBFetch;
    }

    /**
     * @return the requiredInTracking
     */
    public boolean isRequiredInTracking() {
        return requiredInTracking;
    }

    /**
     * @param requiredInTracking the requiredInTracking to set
     */
    public void setRequiredInTracking(boolean requiredInTracking) {
        this.requiredInTracking = requiredInTracking;
    }

    /**
     * @return the verificationScriptName
     */
    public String getVerificationScriptName() {
        return verificationScriptName;
    }

    /**
     * @param verificationScriptName the verificationScriptName to set
     */
    public void setVerificationScriptName(String verificationScriptName) {
        this.verificationScriptName = verificationScriptName;
    }

    /**
     * @return the statusCode
     */
    public String getStatusCode() {
        return statusCode;
    }

    /**
     * @param statusCode the statusCode to set
     */
    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    /**
     * @return the errorMessage
     */
    public String getErrorMessage() {
        return errorMessage;
    }

    /**
     * @param errorMessage the errorMessage to set
     */
    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    /**
     * @return the shippingProviderConnectorParameters
     */
    public List<ShippingProviderConnectorParameterDTO> getShippingProviderConnectorParameters() {
        return shippingProviderConnectorParameters;
    }

    /**
     * @param shippingProviderConnectorParameters the shippingProviderConnectorParameters to set
     */
    public void setShippingProviderConnectorParameters(List<ShippingProviderConnectorParameterDTO> shippingProviderConnectorParameters) {
        this.shippingProviderConnectorParameters = shippingProviderConnectorParameters;
    }

}
