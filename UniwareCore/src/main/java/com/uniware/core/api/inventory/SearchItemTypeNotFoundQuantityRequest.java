/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 03-May-2013
 *  @author unicom
 */
package com.uniware.core.api.inventory;

import com.unifier.core.api.base.ServiceRequest;
import org.hibernate.validator.constraints.NotBlank;

/**
 * @author unicom
 */
public class SearchItemTypeNotFoundQuantityRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 243327795620583774L;

    @NotBlank
    private String            itemSkuCode;

    public String getItemSkuCode() {
        return itemSkuCode;
    }

    public void setItemSkuCode(String itemSkuCode) {
        this.itemSkuCode = itemSkuCode;
    }
}
