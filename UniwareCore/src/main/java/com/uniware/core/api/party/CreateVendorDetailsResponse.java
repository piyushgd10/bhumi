/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 05-Jun-2013
 *  @author unicom
 */
package com.uniware.core.api.party;

import com.unifier.core.api.base.ServiceResponse;

public class CreateVendorDetailsResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -1444056902598176740L;

}
