/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 07-Apr-2014
 *  @author karunsingla
 */
package com.uniware.core.api.shipping;

import java.util.List;
import java.util.Map;

import com.unifier.core.api.base.ServiceResponse;
import com.uniware.core.api.item.ItemDetailFieldDTO;

public class GetItemsForDetailingResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long                 serialVersionUID = 9000415779611505859L;

    private List<SaleOrderItemForDetalingDTO> saleOrderItemsForDetailing;

    private List<ItemDetailFieldDTO>          itemDetailFields;

    public List<SaleOrderItemForDetalingDTO> getSaleOrderItemsForDetailing() {
        return saleOrderItemsForDetailing;
    }

    public void setSaleOrderItemsForDetailing(List<SaleOrderItemForDetalingDTO> saleOrderItemsForDetailing) {
        this.saleOrderItemsForDetailing = saleOrderItemsForDetailing;
    }

    public List<ItemDetailFieldDTO> getItemDetailFields() {
        return itemDetailFields;
    }

    public void setItemDetailFields(List<ItemDetailFieldDTO> itemDetailFields) {
        this.itemDetailFields = itemDetailFields;
    }

    public static class SaleOrderItemForDetalingDTO {
        private String              saleOrderCode;
        private String              saleOrderItemCode;
        private String              shippingPackageCode;
        private String              itemTypeSkuCode;
        private String              itemTypeName;
        private List<String>        itemDetailFieldNames;
        private Map<String, String> itemDetailFieldValues;

        public String getSaleOrderCode() {
            return saleOrderCode;
        }

        public void setSaleOrderCode(String saleOrderCode) {
            this.saleOrderCode = saleOrderCode;
        }

        public String getSaleOrderItemCode() {
            return saleOrderItemCode;
        }

        public void setSaleOrderItemCode(String saleOrderItemCode) {
            this.saleOrderItemCode = saleOrderItemCode;
        }

        public String getShippingPackageCode() {
            return shippingPackageCode;
        }

        public void setShippingPackageCode(String shippingPackageCode) {
            this.shippingPackageCode = shippingPackageCode;
        }

        public String getItemTypeSkuCode() {
            return itemTypeSkuCode;
        }

        public void setItemTypeSkuCode(String itemTypeSkuCode) {
            this.itemTypeSkuCode = itemTypeSkuCode;
        }

        public String getItemTypeName() {
            return itemTypeName;
        }

        public void setItemTypeName(String itemTypeName) {
            this.itemTypeName = itemTypeName;
        }

        public List<String> getItemDetailFieldNames() {
            return itemDetailFieldNames;
        }

        public void setItemDetailFieldNames(List<String> itemDetailFieldNames) {
            this.itemDetailFieldNames = itemDetailFieldNames;
        }

        public Map<String, String> getItemDetailFieldValues() {
            return itemDetailFieldValues;
        }

        public void setItemDetailFieldValues(Map<String, String> itemDetailFieldValues) {
            this.itemDetailFieldValues = itemDetailFieldValues;
        }
    }

}
