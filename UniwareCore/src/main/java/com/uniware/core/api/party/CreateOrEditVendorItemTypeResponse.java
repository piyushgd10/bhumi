/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 23-Jul-2013
 *  @author unicom
 */
package com.uniware.core.api.party;

import com.unifier.core.api.base.ServiceResponse;

public class CreateOrEditVendorItemTypeResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 8200357660589576035L;

}
