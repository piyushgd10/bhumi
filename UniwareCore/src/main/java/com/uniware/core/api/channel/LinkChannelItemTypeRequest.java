/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 9, 2012
 *  @author praveeng
 */
package com.uniware.core.api.channel;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author Sunny Agarwal
 */
public class LinkChannelItemTypeRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -5149174516181942588L;

    @NotBlank
    private String            channelCode;
    @NotBlank
    private String            channelProductId;
    @NotBlank
    private String            skuCode;

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    public String getChannelProductId() {
        return channelProductId;
    }

    public void setChannelProductId(String channelProductId) {
        this.channelProductId = channelProductId;
    }

    public String getSkuCode() {
        return skuCode;
    }

    public void setSkuCode(String skuCode) {
        this.skuCode = skuCode;
    }

}
