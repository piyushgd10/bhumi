/*
 * Copyright 2016 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 13/9/16 1:00 PM
 * @author bhuvneshwarkumar
 */

package com.uniware.core.api.picker;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

/**
 * Created by bhuvneshwarkumar on 13/09/16.
 */
public class ReceivePickBucketItemRequest extends ServiceRequest {

    @NotBlank
    private String pickBucketCode;

    @NotBlank
    private String itemCodeOrSkuCode;

    public String getItemCodeOrSkuCode() {
        return itemCodeOrSkuCode;
    }

    public void setItemCodeOrSkuCode(String itemCodeOrSkuCode) {
        this.itemCodeOrSkuCode = itemCodeOrSkuCode;
    }

    public String getPickBucketCode() {
        return pickBucketCode;
    }

    public void setPickBucketCode(String pickBucketCode) {
        this.pickBucketCode = pickBucketCode;
    }
}
