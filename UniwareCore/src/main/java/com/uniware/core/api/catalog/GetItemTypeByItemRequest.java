/*
 * Copyright 2016 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 31/8/16 4:44 PM
 * @author bhuvneshwarkumar
 */

package com.uniware.core.api.catalog;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

/**
 * Created by bhuvneshwarkumar on 31/08/16.
 */
public class GetItemTypeByItemRequest extends ServiceRequest {
    @NotBlank
    private String itemCode;

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }
}
