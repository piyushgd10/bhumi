/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jan 10, 2012
 *  @author singla
 */
package com.uniware.core.api.saleorder;

import com.unifier.core.api.base.ServiceResponse;

public class RefreshSaleOrderResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -5569840987186007537L;

}
