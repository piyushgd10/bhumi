/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 17/04/14
 *  @author amit
 */

package com.uniware.core.api.saleorder;

import com.unifier.core.api.base.ServiceResponse;

public class DeleteSaleOrderResponse extends ServiceResponse {

    private static final long serialVersionUID = 3459606688809221528L;

    public DeleteSaleOrderResponse() {
        super();
    }
}