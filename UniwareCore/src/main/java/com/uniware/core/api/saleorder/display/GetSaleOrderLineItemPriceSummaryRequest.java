/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jan 10, 2012
 *  @author singla
 */
package com.uniware.core.api.saleorder.display;

import java.util.List;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author singla
 */
public class GetSaleOrderLineItemPriceSummaryRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 7646307772810377253L;

    @NotBlank
    private String            code;

    private List<String>      facilityCodes;

    @NotBlank
    private String            lineItemIdentifier;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public List<String> getFacilityCodes() {
        return facilityCodes;
    }

    public void setFacilityCodes(List<String> facilityCodes) {
        this.facilityCodes = facilityCodes;
    }

    public String getLineItemIdentifier() {
        return lineItemIdentifier;
    }

    public void setLineItemIdentifier(String lineItemIdentifier) {
        this.lineItemIdentifier = lineItemIdentifier;
    }

}
