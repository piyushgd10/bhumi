/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 24, 2011
 *  @author ankitp
 */
package com.uniware.core.api.warehouse;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author ankitp
 *
 */
public class CreateShelfTypeResponse extends ServiceResponse{

    /**
     * 
     */
    private static final long serialVersionUID = 3656970563658098747L;

    private ShelfTypeDTO shelfTypeDTO;

    /**
     * @return the shelfType
     */
    public ShelfTypeDTO getShelfTypeDTO() {
        return shelfTypeDTO;
    }

    /**
     * @param shelfType the shelfType to set
     */
    public void setShelfTypeDTO(ShelfTypeDTO shelfTypeDTO) {
        this.shelfTypeDTO = shelfTypeDTO;
    }
}
