/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Sep 13, 2012
 *  @author Pankaj
 */
package com.uniware.core.api.material;

/**
 * @author Pankaj
 */
import com.unifier.core.api.base.ServiceResponse;

public class EditGatePassResponse extends ServiceResponse {

    private static final long serialVersionUID = -2281658182254903242L;

    private String            gatePassCode;

    /**
     * @return the code
     */
    public String getGatePassCode() {
        return gatePassCode;
    }

    /**
     * @param code the code to set
     */
    public void setGatePassCode(String gatePassCode) {
        this.gatePassCode = gatePassCode;
    }

}
