/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jan 1, 2013
 *  @author praveeng
 */
package com.uniware.core.api.shipping;

import java.util.ArrayList;
import java.util.List;

import com.unifier.core.api.base.ServiceResponse;
import com.uniware.core.api.packer.RegulatoryFormDTO;

/**
 * @author Sunny Agarwal
 */
public class GetAvailableRegulatoryFormsResponse extends ServiceResponse {

    /**
     *
     */
    private static final long serialVersionUID = 6450122808444320902L;

    private List<RegulatoryFormDTO> stateRegulatoryForms = new ArrayList<RegulatoryFormDTO>();

    public List<RegulatoryFormDTO> getStateRegulatoryForms() {
        return stateRegulatoryForms;
    }

    public void setStateRegulatoryForms(List<RegulatoryFormDTO> stateRegulatoryForms) {
        this.stateRegulatoryForms = stateRegulatoryForms;
    }

}
