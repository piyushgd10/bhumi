/*
 * Copyright 2017 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 18/8/17 11:51 AM
 * @author digvijaysharma
 */

package com.uniware.core.api.packer;

import java.util.List;

import com.unifier.core.api.base.ServiceResponse;
import com.uniware.core.api.item.ItemDetailFieldDTO;

/**
 * Created by harshpal on 07/09/16.
 */
public abstract class AbstractAllocateItemResponse extends ServiceResponse {

    private String                   saleOrderItemCode;
    private String                   saleOrderCode;
    private String                   shippingPackageCode;
    private String                   skuCode;
    private String                   productName;
    private List<ItemDetailFieldDTO> itemDetailFields;
    private String                   action;
    private boolean                  putbackItem;

    public String getSaleOrderItemCode() {
        return saleOrderItemCode;
    }

    public void setSaleOrderItemCode(String saleOrderItemCode) {
        this.saleOrderItemCode = saleOrderItemCode;
    }

    public String getSaleOrderCode() {
        return saleOrderCode;
    }

    public void setSaleOrderCode(String saleOrderCode) {
        this.saleOrderCode = saleOrderCode;
    }

    public String getShippingPackageCode() {
        return shippingPackageCode;
    }

    public void setShippingPackageCode(String shippingPackageCode) {
        this.shippingPackageCode = shippingPackageCode;
    }

    public String getSkuCode() {
        return skuCode;
    }

    public void setSkuCode(String skuCode) {
        this.skuCode = skuCode;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public List<ItemDetailFieldDTO> getItemDetailFields() {
        return itemDetailFields;
    }

    public void setItemDetailFields(List<ItemDetailFieldDTO> itemDetailFields) {
        this.itemDetailFields = itemDetailFields;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public boolean isPutbackItem() {
        return putbackItem;
    }

    public void setPutbackItem(boolean putbackItem) {
        this.putbackItem = putbackItem;
    }
}
