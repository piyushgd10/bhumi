/*
 * Copyright 2016 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 18/4/16 2:25 PM
 * @author unicom
 */

package com.uniware.core.api.invoice;

import com.unifier.core.api.base.ServiceResponse;

/**
 * Created by unicom on 18/04/16.
 */
public class GetInvoiceDetailsByCodeResponse extends ServiceResponse {

    private static final long serialVersionUID = 1L;

    private InvoiceDTO        invoice;

    public InvoiceDTO getInvoice() {
        return invoice;
    }

    public void setInvoice(InvoiceDTO invoice) {
        this.invoice = invoice;
    }

}
