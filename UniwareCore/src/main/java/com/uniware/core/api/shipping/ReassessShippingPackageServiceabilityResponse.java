/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 27-Aug-2015
 *  @author parijat
 */
package com.uniware.core.api.shipping;

import com.unifier.core.api.base.ServiceResponse;
import com.uniware.core.entity.ShippingPackage;

public class ReassessShippingPackageServiceabilityResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 98765432456789876L;

    private ShippingPackage   shippingPackage;

    /**
     * @return the shippingPackage
     */
    public ShippingPackage getShippingPackage() {
        return shippingPackage;
    }

    /**
     * @param shippingPackage the shippingPackage to set
     */
    public void setShippingPackage(ShippingPackage shippingPackage) {
        this.shippingPackage = shippingPackage;
    }

}
