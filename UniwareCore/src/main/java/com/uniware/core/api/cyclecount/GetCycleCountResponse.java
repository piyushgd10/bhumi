package com.uniware.core.api.cyclecount;

import com.unifier.core.api.base.ServiceResponse;

/**
 * Created by harshpal on 3/8/16.
 */
public class GetCycleCountResponse extends ServiceResponse {

    private Long          resultCount;
    private CycleCountDTO cycleCount;

    public CycleCountDTO getCycleCount() {
        return cycleCount;
    }

    public void setCycleCount(CycleCountDTO cycleCount) {
        this.cycleCount = cycleCount;
    }

    public Long getResultCount() {
        return resultCount;
    }

    public void setResultCount(Long resultCount) {
        this.resultCount = resultCount;
    }
}
