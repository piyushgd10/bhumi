/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 09-Jan-2014
 *  @author akshay
 */
package com.uniware.core.api.facility;

import java.util.ArrayList;
import java.util.List;

import com.unifier.core.api.base.ServiceResponse;
import com.uniware.core.api.warehouse.FacilityDTO;

public class GetAllFacilityDetailsResponse extends ServiceResponse{

    /**
     * 
     */
    private static final long serialVersionUID = -1561180325068987867L;

    private List<FacilityDTO> facilities       = new ArrayList<FacilityDTO>();

    public List<FacilityDTO> getFacilities() {
        return facilities;
    }

    public void setFacilities(List<FacilityDTO> facilities) {
        this.facilities = facilities;
    }

}
