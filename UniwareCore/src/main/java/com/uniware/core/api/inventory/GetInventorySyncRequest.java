/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 05-Jan-2015
 *  @author akshaykochhar
 */
package com.uniware.core.api.inventory;

import com.unifier.core.api.base.ServiceRequest;

public class GetInventorySyncRequest extends ServiceRequest{

    /**
     * 
     */
    private static final long serialVersionUID = -1241085402971579327L;

}
