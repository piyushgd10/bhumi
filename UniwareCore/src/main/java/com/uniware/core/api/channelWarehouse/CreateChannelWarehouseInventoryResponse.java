package com.uniware.core.api.channelWarehouse;

import com.unifier.core.api.base.ServiceResponse;

/**
 * Created by digvijaysharma on 24/01/17.
 */
public class CreateChannelWarehouseInventoryResponse extends ServiceResponse {

    /**
     *
     */
    private static final long serialVersionUID = 8034049854364994904L;

    private int successfulImports;

    private int failedImports;

    private boolean hasMoreResults;

    private Integer totalPages;

    public boolean isHasMoreResults() {
        return hasMoreResults;
    }

    public void setHasMoreResults(boolean hasMoreResults) {
        this.hasMoreResults = hasMoreResults;
    }

    public Integer getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(Integer totalPages) {
        this.totalPages = totalPages;
    }

    public int getSuccessfulImports() {
        return successfulImports;
    }

    public void setSuccessfulImports(int successfulImports) {
        this.successfulImports = successfulImports;
    }

    public int getFailedImports() {
        return failedImports;
    }

    public void setFailedImports(int failedImports) {
        this.failedImports = failedImports;
    }
}
