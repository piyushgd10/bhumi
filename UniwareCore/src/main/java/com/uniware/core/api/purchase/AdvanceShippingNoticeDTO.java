/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 11-May-2012
 *  @author praveeng
 */
package com.uniware.core.api.purchase;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.unifier.core.api.customfields.CustomFieldMetadataDTO;

/**
 * @author praveeng
 */
public class AdvanceShippingNoticeDTO {
    private Integer                      id;
    private Integer                      vendorId;
    private String                       code;
    private String                       purchaseOrderCode;
    private Date                         expectedDeliveryDate;
    private Date                         created;
    private List<ASNItemDTO>             asnItems = new ArrayList<ASNItemDTO>();
    private List<CustomFieldMetadataDTO> customFieldValues;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the purchaseOrderCode
     */
    public String getPurchaseOrderCode() {
        return purchaseOrderCode;
    }

    /**
     * @param purchaseOrderCode the purchaseOrderCode to set
     */
    public void setPurchaseOrderCode(String purchaseOrderCode) {
        this.purchaseOrderCode = purchaseOrderCode;
    }

    /**
     * @return the expectedDeliveryDate
     */
    public Date getExpectedDeliveryDate() {
        return expectedDeliveryDate;
    }

    /**
     * @param expectedDeliveryDate the expectedDeliveryDate to set
     */
    public void setExpectedDeliveryDate(Date expectedDeliveryDate) {
        this.expectedDeliveryDate = expectedDeliveryDate;
    }

    /**
     * @return the created
     */
    public Date getCreated() {
        return created;
    }

    /**
     * @param created the created to set
     */
    public void setCreated(Date created) {
        this.created = created;
    }

    /**
     * @return the asnItems
     */
    public List<ASNItemDTO> getAsnItems() {
        return asnItems;
    }

    /**
     * @param asnItems the asnItems to set
     */
    public void setAsnItems(List<ASNItemDTO> asnItems) {
        this.asnItems = asnItems;
    }

    public List<CustomFieldMetadataDTO> getCustomFieldValues() {
        return customFieldValues;
    }

    public void setCustomFieldValues(List<CustomFieldMetadataDTO> customFieldValues) {
        this.customFieldValues = customFieldValues;
    }

    public Integer getVendorId() {
        return vendorId;
    }

    public void setVendorId(Integer vendorId) {
        this.vendorId = vendorId;
    }
}
