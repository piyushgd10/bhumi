/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 05/05/14
 *  @author amit
 */

package com.uniware.core.api.cms;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;
import com.uniware.core.vo.ContentVO;

public class SaveContentRequest extends ServiceRequest {

    private static final long      serialVersionUID = 7360084060309366803L;

    @NotBlank
    private String                 nsIdentifier;

    @NotBlank
    private String                 key;

    @NotBlank
    private String                 content;

    private ContentVO.LanguageCode languageCode;

    public SaveContentRequest() {
        super();
    }

    public String getNsIdentifier() {
        return nsIdentifier;
    }

    public void setNsIdentifier(String nsIdentifier) {
        this.nsIdentifier = nsIdentifier;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public ContentVO.LanguageCode getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(ContentVO.LanguageCode languageCode) {
        this.languageCode = languageCode;
    }

    @Override
    public String toString() {
        return "SaveContentRequest{" + "nsIdentifier='" + nsIdentifier + '\'' + ", key='" + key + '\'' + ", content='" + content + '\'' + ", languageCode=" + languageCode + '}';
    }
}
