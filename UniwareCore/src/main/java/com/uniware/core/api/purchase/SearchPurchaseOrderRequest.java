/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Apr 13, 2012
 *  @author praveeng
 */
package com.uniware.core.api.purchase;

import java.util.Date;

import com.unifier.core.api.base.ServiceRequest;
import com.unifier.core.pagination.SearchOptions;

/**
 * @author praveeng
 */
public class SearchPurchaseOrderRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -7721282385982654917L;
    private String            purchaseOrderCode;
    private Integer           vendorId;
    private String            itemTypeName;
    private Integer           categoryId;
    private String            status;
    private Date              fromDate;
    private Date              toDate;
    private SearchOptions     searchOptions;
    private Date              approvedFromDate;
    private Date              approvedToDate;

    /**
     * @return the vendorId
     */
    public Integer getVendorId() {
        return vendorId;
    }

    /**
     * @param vendorId the vendorId to set
     */
    public void setVendorId(Integer vendorId) {
        this.vendorId = vendorId;
    }

    /**
     * @return the itemTypeName
     */
    public String getItemTypeName() {
        return itemTypeName;
    }

    /**
     * @return the categoryId
     */
    public Integer getCategoryId() {
        return categoryId;
    }

    /**
     * @param categoryId the categoryId to set
     */
    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    /**
     * @param itemTypeName the itemTypeName to set
     */
    public void setItemTypeName(String itemTypeName) {
        this.itemTypeName = itemTypeName;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the fromDate
     */
    public Date getFromDate() {
        return fromDate;
    }

    /**
     * @param fromDate the fromDate to set
     */
    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    /**
     * @return the toDate
     */
    public Date getToDate() {
        return toDate;
    }

    /**
     * @param toDate the toDate to set
     */
    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    /**
     * @return the purchaseOrderCode
     */
    public String getPurchaseOrderCode() {
        return purchaseOrderCode;
    }

    /**
     * @param purchaseOrderCode the purchaseOrderCode to set
     */
    public void setPurchaseOrderCode(String purchaseOrderCode) {
        this.purchaseOrderCode = purchaseOrderCode;
    }

    /**
     * @return the searchOptions
     */
    public SearchOptions getSearchOptions() {
        return searchOptions;
    }

    /**
     * @param searchOptions the searchOptions to set
     */
    public void setSearchOptions(SearchOptions searchOptions) {
        this.searchOptions = searchOptions;
    }

    /**
     * @return the approvedFromDate
     */
    public Date getApprovedFromDate() {
        return approvedFromDate;
    }

    /**
     * @param approvedFromDate the approvedFromDate to set
     */
    public void setApprovedFromDate(Date approvedFromDate) {
        this.approvedFromDate = approvedFromDate;
    }

    /**
     * @return the approvedToDate
     */
    public Date getApprovedToDate() {
        return approvedToDate;
    }

    /**
     * @param approvedToDate the approvedToDate to set
     */
    public void setApprovedToDate(Date approvedToDate) {
        this.approvedToDate = approvedToDate;
    }

}
