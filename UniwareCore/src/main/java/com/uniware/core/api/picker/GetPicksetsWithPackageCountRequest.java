/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Apr 4, 2012
 *  @author praveeng
 */
package com.uniware.core.api.picker;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author praveeng
 */
public class GetPicksetsWithPackageCountRequest extends ServiceRequest {
    /**
     * 
     */
    private static final long serialVersionUID = 3702070336597542452L;

}
