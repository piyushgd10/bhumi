/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 04-Aug-2012
 *  @author praveeng
 */
package com.uniware.core.api.admin.shipping;

import com.uniware.core.entity.ShippingProviderAllocationRule;

/**
 * @author praveeng
 */
public class ShippingProviderAllocationRuleDTO {
    private Integer id;
    private Integer shippingProviderId;
    private String  shippingProviderCode;
    private String  name;
    private String  conditionExpressionText;
    private String  allocationCriteria;
    private int     preference;
    private boolean enabled;

    public ShippingProviderAllocationRuleDTO() {

    }

    public ShippingProviderAllocationRuleDTO(ShippingProviderAllocationRule allocationRule) {
        this.id = allocationRule.getId();
        this.shippingProviderId = allocationRule.getShippingProvider().getId();
        this.shippingProviderCode = allocationRule.getShippingProvider().getCode();
        this.name = allocationRule.getName();
        this.conditionExpressionText = allocationRule.getConditionExpressionText();
        this.allocationCriteria = allocationRule.getAllocationCriteria();
        this.preference = allocationRule.getPreference();
        this.enabled = allocationRule.isEnabled();
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the shippingProviderId
     */
    public Integer getShippingProviderId() {
        return shippingProviderId;
    }

    /**
     * @param shippingProviderId the shippingProviderId to set
     */
    public void setShippingProviderId(Integer shippingProviderId) {
        this.shippingProviderId = shippingProviderId;
    }

    /**
     * @return the shippingProviderCode
     */
    public String getShippingProviderCode() {
        return shippingProviderCode;
    }

    /**
     * @param shippingProviderCode the shippingProviderCode to set
     */
    public void setShippingProviderCode(String shippingProviderCode) {
        this.shippingProviderCode = shippingProviderCode;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the conditionExpressionText
     */
    public String getConditionExpressionText() {
        return conditionExpressionText;
    }

    /**
     * @param conditionExpressionText the conditionExpressionText to set
     */
    public void setConditionExpressionText(String conditionExpressionText) {
        this.conditionExpressionText = conditionExpressionText;
    }

    /**
     * @return the allocationCriteria
     */
    public String getAllocationCriteria() {
        return allocationCriteria;
    }

    /**
     * @param allocationCriteria the allocationCriteria to set
     */
    public void setAllocationCriteria(String allocationCriteria) {
        this.allocationCriteria = allocationCriteria;
    }

    /**
     * @return the preference
     */
    public int getPreference() {
        return preference;
    }

    /**
     * @param preference the preference to set
     */
    public void setPreference(int preference) {
        this.preference = preference;
    }

    /**
     * @return the enabled
     */
    public boolean isEnabled() {
        return enabled;
    }

    /**
     * @param enabled the enabled to set
     */
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

}
