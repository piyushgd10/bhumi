/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 17-Jul-2013
 *  @author unicom
 */
package com.uniware.core.api.shipping;

import java.util.Date;

import javax.validation.constraints.NotNull;

import com.unifier.core.api.base.ServiceRequest;

public class UpdateTrackingStatusRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 5315185557016028541L;

    @NotNull
    private String            providerCode;

    @NotNull
    private String            trackingNumber;

    @NotNull
    private String            trackingStatus;

    private Date              statusDate;

    public String getProviderCode() {
        return providerCode;
    }

    public void setProviderCode(String providerCode) {
        this.providerCode = providerCode;
    }

    public String getTrackingNumber() {
        return trackingNumber;
    }

    public void setTrackingNumber(String trackingNumber) {
        this.trackingNumber = trackingNumber;
    }

    public String getTrackingStatus() {
        return trackingStatus;
    }

    public void setTrackingStatus(String trackingStatus) {
        this.trackingStatus = trackingStatus;
    }

    public Date getStatusDate() {
        return statusDate;
    }

    public void setStatusDate(Date statusDate) {
        this.statusDate = statusDate;
    }

}
