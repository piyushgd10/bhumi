/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 14-Jul-2014
 *  @author unicom
 */
package com.uniware.core.api.tenant;

import com.unifier.core.api.base.ServiceResponse;
import com.uniware.core.entity.TenantSetupState;

public class GetTenantSetupStateResponse extends ServiceResponse {

    private static final long    serialVersionUID = -4884255807525960862L;

    private TenantSetupState     tenantState;

    private CreateTenantResponse createTenantResponse;

    public CreateTenantResponse getCreateTenantResponse() {
        return createTenantResponse;
    }

    public void setCreateTenantResponse(CreateTenantResponse createTenantResponse) {
        this.createTenantResponse = createTenantResponse;
    }

    public TenantSetupState getTenantState() {
        return tenantState;
    }

    public void setTenantState(TenantSetupState tenantState) {
        this.tenantState = tenantState;
    }
}
