/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 07/05/14
 *  @author amit
 */

package com.uniware.core.api.templatecustomization;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotNull;

import com.unifier.core.api.base.ServiceRequest;

public class GetCustomizedDummyTemplateRequest extends ServiceRequest {

    private static final long            serialVersionUID = -8568501676783129088L;

    @NotNull
    private String                       templateString;

    private List<CustomizationParameter> parameterList    = new ArrayList<>();

    public GetCustomizedDummyTemplateRequest() {
        super();
    }

    public String getTemplateString() {
        return templateString;
    }

    public void setTemplateString(String template) {
        this.templateString = template;
    }

    public List<CustomizationParameter> getParameterList() {
        return parameterList;
    }

    public void setParameterList(List<CustomizationParameter> parameterList) {
        this.parameterList = parameterList;
    }

    public static class CustomizationParameter {

        private String key;

        @NotNull
        private String value;

        public CustomizationParameter() {
            super();
        }

        public CustomizationParameter(String key, String value) {
            super();
            setKey(key);
            setValue(value);
        }

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return "CustomizationParameter{" + "key='" + key + '\'' + ", value=" + value + '}';
        }
    }

    @Override
    public String toString() {
        return "GetCustomizedDummyTemplateRequest{" + "templateString='" + templateString + '\'' + ", parameterList=" + parameterList + '}';
    }
}
