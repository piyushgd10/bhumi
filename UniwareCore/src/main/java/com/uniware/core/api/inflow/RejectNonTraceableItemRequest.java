/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 2, 2012
 *  @author singla
 */
package com.uniware.core.api.inflow;

import com.unifier.core.api.base.ServiceRequest;

import java.util.Date;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * @author singla
 */
public class RejectNonTraceableItemRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 3677907372947226587L;

    @NotNull
    private Integer           inflowReceiptItemId;

    @NotNull
    @Min(value = 0)
    private Integer           rejectedQuantity;

    private String            comments;

    private Date              expiry;

    /**
     * @return the inflowReceiptItemId
     */
    public Integer getInflowReceiptItemId() {
        return inflowReceiptItemId;
    }

    /**
     * @param inflowReceiptItemId the inflowReceiptItemId to set
     */
    public void setInflowReceiptItemId(Integer inflowReceiptItemId) {
        this.inflowReceiptItemId = inflowReceiptItemId;
    }

    /**
     * @return the rejectedQuantity
     */
    public Integer getRejectedQuantity() {
        return rejectedQuantity;
    }

    /**
     * @param rejectedQuantity the rejectedQuantity to set
     */
    public void setRejectedQuantity(Integer rejectedQuantity) {
        this.rejectedQuantity = rejectedQuantity;
    }

    /**
     * @return the comments
     */
    public String getComments() {
        return comments;
    }

    /**
     * @param comments the comments to set
     */
    public void setComments(String comments) {
        this.comments = comments;
    }

    /**
     * @return the expiry
     */
    public Date getExpiry() {
        return expiry;
    }

    /**
     * @param expiry the expiry to set
     */
    public void setExpiry(Date expiry) {
        this.expiry = expiry;
    }

}