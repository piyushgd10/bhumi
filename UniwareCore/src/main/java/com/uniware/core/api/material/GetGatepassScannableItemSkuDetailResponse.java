/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 08-Apr-2015
 *  @author akshaykochhar
 */
package com.uniware.core.api.material;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.unifier.core.api.base.ServiceResponse;

public class GetGatepassScannableItemSkuDetailResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -388322051521465342L;
    
    
    private ScannedItemSkuDetailDTO scannedItem;

    public ScannedItemSkuDetailDTO getScannedItem() {
        return scannedItem;
    }

    public void setScannedItem(ScannedItemSkuDetailDTO scannedItem) {
        this.scannedItem = scannedItem;
    }
    
    

    public static class ScannedItemSkuDetailDTO {

        private String     itemName;
        private String     itemSKU;
        private BigDecimal unitPrice;
        private Integer    existingQuantity;
        private List<ShelfwiseInventoryDTO> availableInventory = new ArrayList<>();

        public String getItemName() {
            return itemName;
        }

        public void setItemName(String itemName) {
            this.itemName = itemName;
        }

        public String getItemSKU() {
            return itemSKU;
        }

        public void setItemSKU(String itemSKU) {
            this.itemSKU = itemSKU;
        }

        public BigDecimal getUnitPrice() {
            return unitPrice;
        }

        public void setUnitPrice(BigDecimal unitPrice) {
            this.unitPrice = unitPrice;
        }

        public Integer getExistingQuantity() {
            return existingQuantity;
        }

        public void setExistingQuantity(Integer existingQuantity) {
            this.existingQuantity = existingQuantity;
        }

        public List<ShelfwiseInventoryDTO> getAvailableInventory() {
            return availableInventory;
        }

        public void setAvailableInventory(List<ShelfwiseInventoryDTO> availableInventory) {
            this.availableInventory = availableInventory;
        }



        public static class ShelfwiseInventoryDTO {

            private String                 shelfCode;

            private List<ShelfInventoryTypeDetailDTO> inventory = new ArrayList<>();

            public String getShelfCode() {
                return shelfCode;
            }

            public void setShelfCode(String shelfCode) {
                this.shelfCode = shelfCode;
            }

            public List<ShelfInventoryTypeDetailDTO> getInventory() {
                return inventory;
            }

            public void setInventory(List<ShelfInventoryTypeDetailDTO> inventory) {
                this.inventory = inventory;
            }


            public static class ShelfInventoryTypeDetailDTO {
                private String  type;
                private Integer quantity;

                public String getType() {
                    return type;
                }

                public void setType(String type) {
                    this.type = type;
                }

                public Integer getQuantity() {
                    return quantity;
                }

                public void setQuantity(Integer quantity) {
                    this.quantity = quantity;
                }

            }

        }

    }

}
