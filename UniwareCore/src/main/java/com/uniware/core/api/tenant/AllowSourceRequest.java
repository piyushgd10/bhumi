/*
 * Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *    
 * @version     1.0, 4/15/15 6:22 PM
 * @author amdalal
 */

package com.uniware.core.api.tenant;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

public class AllowSourceRequest extends ServiceRequest {

    private static final long serialVersionUID = 442104670201554729L;

    @NotBlank
    private String      sourceCode;

    public AllowSourceRequest() {
    }

    public String getSourceCode() {
        return sourceCode;
    }

    public void setSourceCode(String sourceCode) {
        this.sourceCode = sourceCode;
    }
}
