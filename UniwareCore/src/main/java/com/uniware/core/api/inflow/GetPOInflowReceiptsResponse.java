/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 4, 2012
 *  @author praveeng
 */
package com.uniware.core.api.inflow;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.unifier.core.api.base.ServiceResponse;
import com.unifier.core.api.customfields.CustomFieldMetadataDTO;

/**
 * @author praveeng
 */
public class GetPOInflowReceiptsResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long     serialVersionUID = -7696087291395822813L;
    private List<POInflowReceipt> inflowReceipts   = new ArrayList<POInflowReceipt>();

    /**
     * @return the inflowReceipts
     */
    public List<POInflowReceipt> getInflowReceipts() {
        return inflowReceipts;
    }

    /**
     * @param inflowReceipts the inflowReceipts to set
     */
    public void setInflowReceipts(List<POInflowReceipt> inflowReceipts) {
        this.inflowReceipts = inflowReceipts;
    }

    public static class POInflowReceipt {

        private String                       code;
        private String                       statusCode;
        private String                       vendorInvoiceNumber;
        private Date                         vendorInvoiceDate;
        private Date                         created;
        private String                       createdBy;
        private int                          totalQuantity;
        private int                          totalRejectedQuantity;
        private BigDecimal                   totalReceivedAmount = new BigDecimal(0);
        private BigDecimal                   totalRejectedAmount = new BigDecimal(0);
        private List<CustomFieldMetadataDTO> customFieldValues;

        /**
         * @return the code
         */
        public String getCode() {
            return code;
        }

        /**
         * @param code the code to set
         */
        public void setCode(String code) {
            this.code = code;
        }

        /**
         * @return the statusCode
         */
        public String getStatusCode() {
            return statusCode;
        }

        /**
         * @param statusCode the statusCode to set
         */
        public void setStatusCode(String statusCode) {
            this.statusCode = statusCode;
        }

        /**
         * @return the created
         */
        public Date getCreated() {
            return created;
        }

        /**
         * @param created the created to set
         */
        public void setCreated(Date created) {
            this.created = created;
        }

        public String getCreatedBy() {
            return createdBy;
        }

        public void setCreatedBy(String createdBy) {
            this.createdBy = createdBy;
        }

        /**
         * @return the totalReceivedAmount
         */
        public BigDecimal getTotalReceivedAmount() {
            return totalReceivedAmount;
        }

        /**
         * @param totalReceivedAmount the totalReceivedAmount to set
         */
        public void setTotalReceivedAmount(BigDecimal totalReceivedAmount) {
            this.totalReceivedAmount = totalReceivedAmount;
        }

        /**
         * @return the totalRejectedAmount
         */
        public BigDecimal getTotalRejectedAmount() {
            return totalRejectedAmount;
        }

        /**
         * @param totalRejectedAmount the totalRejectedAmount to set
         */
        public void setTotalRejectedAmount(BigDecimal totalRejectedAmount) {
            this.totalRejectedAmount = totalRejectedAmount;
        }

        /**
         * @return the totalQuantity
         */
        public int getTotalQuantity() {
            return totalQuantity;
        }

        /**
         * @param totalQuantity the totalQuantity to set
         */
        public void setTotalQuantity(int totalQuantity) {
            this.totalQuantity = totalQuantity;
        }

        /**
         * @return the totalRejectQuantity
         */
        public int getTotalRejectedQuantity() {
            return totalRejectedQuantity;
        }

        /**
         * @param totalRejectQuantity the totalRejectQuantity to set
         */
        public void setTotalRejectedQuantity(int totalRejectQuantity) {
            this.totalRejectedQuantity = totalRejectQuantity;
        }

        public String getVendorInvoiceNumber() {
            return vendorInvoiceNumber;
        }

        public void setVendorInvoiceNumber(String vendorInvoiceNumber) {
            this.vendorInvoiceNumber = vendorInvoiceNumber;
        }

        public Date getVendorInvoiceDate() {
            return vendorInvoiceDate;
        }

        public void setVendorInvoiceDate(Date vendorInvoiceDate) {
            this.vendorInvoiceDate = vendorInvoiceDate;
        }

        /**
         * @return the customFieldValues
         */
        public List<CustomFieldMetadataDTO> getCustomFieldValues() {
            return customFieldValues;
        }

        /**
         * @param customFieldValues the customFieldValues to set
         */
        public void setCustomFieldValues(List<CustomFieldMetadataDTO> customFieldValues) {
            this.customFieldValues = customFieldValues;
        }
    }
}
