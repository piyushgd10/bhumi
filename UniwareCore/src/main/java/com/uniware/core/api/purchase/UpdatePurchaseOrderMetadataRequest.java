/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 25-Jun-2012
 *  @author vibhu
 */
package com.uniware.core.api.purchase;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author vibhu
 */
public class UpdatePurchaseOrderMetadataRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long       serialVersionUID = -613557818884614900L;

    @NotBlank
    private String                  purchaseOrderCode;

    @Valid
    @NotNull
    private WsPurchaseOrderMetadata purchaseOrderMetadata;

    /**
     * @return the purchaseOrderCode
     */
    public String getPurchaseOrderCode() {
        return purchaseOrderCode;
    }

    /**
     * @param purchaseOrderCode the purchaseOrderCode to set
     */
    public void setPurchaseOrderCode(String purchaseOrderCode) {
        this.purchaseOrderCode = purchaseOrderCode;
    }

    /**
     * @return the purchaseOrderMetadata
     */
    public WsPurchaseOrderMetadata getPurchaseOrderMetadata() {
        return purchaseOrderMetadata;
    }

    /**
     * @param purchaseOrderMetadata the purchaseOrderMetadata to set
     */
    public void setPurchaseOrderMetadata(WsPurchaseOrderMetadata purchaseOrderMetadata) {
        this.purchaseOrderMetadata = purchaseOrderMetadata;
    }

}
