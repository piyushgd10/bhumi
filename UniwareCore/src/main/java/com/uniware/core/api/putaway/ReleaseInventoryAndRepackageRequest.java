/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 05-Jun-2014
 *  @author akshay
 */
package com.uniware.core.api.putaway;

import java.util.List;

import javax.validation.constraints.NotNull;

import com.unifier.core.api.base.ServiceRequest;

public class ReleaseInventoryAndRepackageRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @NotNull
    private List<String>            shippingPackageCodes;

    @NotNull
    private Integer           userId;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public List<String> getShippingPackageCodes() {
        return shippingPackageCodes;
    }

    public void setShippingPackageCodes(List<String> shippingPackageCodes) {
        this.shippingPackageCodes = shippingPackageCodes;
    }

    

}
