/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jul 29, 2012
 *  @author singla
 */
package com.uniware.core.api.saleorder;

import com.unifier.core.api.base.ServiceRequest;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

public class CreateSaleOrderItemAlternateRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long              serialVersionUID = 655478307783390368L;

    @NotEmpty
    @Valid
    private List<WsSaleOrderItem>          saleOrderItems;

    @Valid
    @NotEmpty
    private List<WsSaleOrderItemAlternate> saleOrderItemAlternates;

    @NotNull
    private Integer                        userId;

    /**
     * @return the saleOrderItemAlternates
     */
    public List<WsSaleOrderItemAlternate> getSaleOrderItemAlternates() {
        return saleOrderItemAlternates;
    }

    /**
     * @param saleOrderItemAlternates the saleOrderItemAlternates to set
     */
    public void setSaleOrderItemAlternates(List<WsSaleOrderItemAlternate> saleOrderItemAlternates) {
        this.saleOrderItemAlternates = saleOrderItemAlternates;
    }

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return the saleOrderItems
     */
    public List<WsSaleOrderItem> getSaleOrderItems() {
        return saleOrderItems;
    }

    /**
     * @param saleOrderItems the saleOrderItems to set
     */
    public void setSaleOrderItems(List<WsSaleOrderItem> saleOrderItems) {
        this.saleOrderItems = saleOrderItems;
    }

    public static class WsSaleOrderItem {

        @NotBlank
        private String saleOrderCode;

        @NotBlank
        private String saleOrderItemCode;

        /**
         * @return the saleOrderCode
         */
        public String getSaleOrderCode() {
            return saleOrderCode;
        }

        /**
         * @param saleOrderCode the saleOrderCode to set
         */
        public void setSaleOrderCode(String saleOrderCode) {
            this.saleOrderCode = saleOrderCode;
        }

        /**
         * @return the saleOrderItemCode
         */
        public String getSaleOrderItemCode() {
            return saleOrderItemCode;
        }

        /**
         * @param saleOrderItemCode the saleOrderItemCode to set
         */
        public void setSaleOrderItemCode(String saleOrderItemCode) {
            this.saleOrderItemCode = saleOrderItemCode;
        }
    }
}
