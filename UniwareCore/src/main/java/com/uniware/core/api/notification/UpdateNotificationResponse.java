/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 24-May-2013
 *  @author unicom
 */
package com.uniware.core.api.notification;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author unicom
 */
public class UpdateNotificationResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 234232334242L;

}
