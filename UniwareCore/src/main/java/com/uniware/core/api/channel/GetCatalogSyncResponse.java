/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 23-Apr-2013
 *  @author unicom
 */
package com.uniware.core.api.channel;

import java.util.List;
import java.util.Map;

import com.unifier.core.api.base.ServiceResponse;
import com.unifier.core.utils.XMLParser.Element;

/**
 * @author Sunny Agarwal
 */
public class GetCatalogSyncResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long   serialVersionUID = 1066791106292526555L;
    private List<Element>       channelItemTypeElements;
    private boolean             hasMoreResults;
    private Integer             totalPages;
    private boolean             async;
    private Map<String, Object> channelSkuCodeToChannelItemTypeDetail;

    public List<Element> getChannelItemTypeElements() {
        return channelItemTypeElements;
    }

    public void setChannelItemTypeElements(List<Element> channelItemTypeElements) {
        this.channelItemTypeElements = channelItemTypeElements;
    }

    public boolean isHasMoreResults() {
        return hasMoreResults;
    }

    public void setHasMoreResults(boolean hasMoreResults) {
        this.hasMoreResults = hasMoreResults;
    }

    public Integer getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(Integer totalPages) {
        this.totalPages = totalPages;
    }

    public boolean isAsync() {
        return async;
    }

    public void setAsync(boolean async) {
        this.async = async;
    }

    public Map<String, Object> getChannelSkuCodeToChannelItemTypeDetail() {
        return channelSkuCodeToChannelItemTypeDetail;
    }

    public void setChannelSkuCodeToChannelItemTypeDetail(Map<String, Object> channelSkuCodeToChannelItemTypeDetail) {
        this.channelSkuCodeToChannelItemTypeDetail = channelSkuCodeToChannelItemTypeDetail;
    }

}