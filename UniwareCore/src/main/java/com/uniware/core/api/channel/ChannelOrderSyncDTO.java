/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 06-Jan-2015
 *  @author akshaykochhar
 */
package com.uniware.core.api.channel;

import java.io.Serializable;
import java.util.List;

public class ChannelOrderSyncDTO implements Serializable {

    private String                    code;
    private String                    name;
    private boolean                   enabled;
    private String                    colorCode;
    private String                    shortName;
    private String                    orderSyncStatus;
    private SourceDTO                 sourceDTO;
    private List<ChannelConnectorDTO> channelConnectors;
    private ChannelOrderSyncStatusDTO lastOrderSyncResult;

    public ChannelOrderSyncDTO() {
    }

    public ChannelOrderSyncDTO(ChannelSyncDTO channelSyncDTO) {
        this.code = channelSyncDTO.getCode();
        this.name = channelSyncDTO.getName();
        this.enabled = channelSyncDTO.isEnabled();
        this.colorCode = channelSyncDTO.getColorCode();
        this.shortName = channelSyncDTO.getShortName();
        this.orderSyncStatus = channelSyncDTO.getOrderSyncStatus();
        this.lastOrderSyncResult = channelSyncDTO.getLastOrderSyncResult();
        this.channelConnectors = channelSyncDTO.getChannelConnectors();
        this.sourceDTO = channelSyncDTO.getSourceDTO();
    }

    public ChannelOrderSyncDTO(String code, String name, boolean enabled, String colorCode, String shortName, String status, ChannelOrderSyncStatusDTO lastSyncResult) {
        this.code = code;
        this.name = name;
        this.enabled = enabled;
        this.colorCode = colorCode;
        this.shortName = shortName;
        this.orderSyncStatus = status;
        this.lastOrderSyncResult = lastSyncResult;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getColorCode() {
        return colorCode;
    }

    public void setColorCode(String colorCode) {
        this.colorCode = colorCode;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getOrderSyncStatus() {
        return orderSyncStatus;
    }

    public void setOrderSyncStatus(String orderSyncStatus) {
        this.orderSyncStatus = orderSyncStatus;
    }

    public ChannelOrderSyncStatusDTO getLastOrderSyncResult() {
        return lastOrderSyncResult;
    }

    public void setLastOrderSyncResult(ChannelOrderSyncStatusDTO lastOrderSyncResult) {
        this.lastOrderSyncResult = lastOrderSyncResult;
    }

    public SourceDTO getSourceDTO() {
        return sourceDTO;
    }

    public void setSourceDTO(SourceDTO sourceDTO) {
        this.sourceDTO = sourceDTO;
    }

    public List<ChannelConnectorDTO> getChannelConnectors() {
        return channelConnectors;
    }

    public void setChannelConnectors(List<ChannelConnectorDTO> channelConnectors) {
        this.channelConnectors = channelConnectors;
    }
    
}
