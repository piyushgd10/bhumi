/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 03-Sep-2015
 *  @author parijat
 */
package com.uniware.core.api.facility;

import com.unifier.core.api.base.ServiceRequest;
import com.uniware.core.api.channel.AddChannelConnectorRequest;
import com.uniware.core.api.channel.EditChannelRequest;
import com.uniware.core.api.warehouse.EditFacilityRequest;

import javax.validation.constraints.NotNull;
import java.util.List;

public class EditFacilityChannelRequest extends ServiceRequest {

    /**
     *
     */
    private static final long serialVersionUID = 8587656789L;

    @NotNull
    private EditChannelRequest channelRequest;

    private List<AddChannelConnectorRequest> channelConnectorRequests;

    @NotNull
    private EditFacilityRequest facilityRequest;

    @NotNull
    private CreateFacilityProfileRequest facilityProfileRequest;

    /**
     * @return the channelRequest
     */
    public EditChannelRequest getChannelRequest() {
        return channelRequest;
    }

    /**
     * @param channelRequest the channelRequest to set
     */
    public void setChannelRequest(EditChannelRequest channelRequest) {
        this.channelRequest = channelRequest;
    }

    /**
     * @return the channelConnectorRequests
     */
    public List<AddChannelConnectorRequest> getChannelConnectorRequests() {
        return channelConnectorRequests;
    }

    /**
     * @param channelConnectorRequests the channelConnectorRequests to set
     */
    public void setChannelConnectorRequests(List<AddChannelConnectorRequest> channelConnectorRequests) {
        this.channelConnectorRequests = channelConnectorRequests;
    }

    /**
     * @return the facilityRequest
     */
    public EditFacilityRequest getFacilityRequest() {
        return facilityRequest;
    }

    /**
     * @param facilityRequest the facilityRequest to set
     */
    public void setFacilityRequest(EditFacilityRequest facilityRequest) {
        this.facilityRequest = facilityRequest;
    }

    /**
     * @return the facilityProfileRequest
     */
    public CreateFacilityProfileRequest getFacilityProfileRequest() {
        return facilityProfileRequest;
    }

    /**
     * @param facilityProfileRequest the facilityProfileRequest to set
     */
    public void setFacilityProfileRequest(CreateFacilityProfileRequest facilityProfileRequest) {
        this.facilityProfileRequest = facilityProfileRequest;
    }

}
