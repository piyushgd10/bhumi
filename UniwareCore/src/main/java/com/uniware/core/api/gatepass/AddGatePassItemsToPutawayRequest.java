/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 14-May-2012
 *  @author praveeng
 */
package com.uniware.core.api.gatepass;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import com.unifier.core.api.base.ServiceRequest;
import com.uniware.core.entity.ItemTypeInventory.Type;

/**
 * <ul>
 * Parameters:
 * <li>{@code gatePassCode}: code of gatepass of which gatepass items are to be added in putaway.</li>
 * <li>{@code putawayCode}: code of putaway in which gatepass items are to be added.</li>
 * <li>{@code userId}: id of user, set by context</li>
 * <li>{@code gatePassItems}: {@link List} of {@link WsGatePassItem}</li>
 * </ul>
 */
public class AddGatePassItemsToPutawayRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long    serialVersionUID = 876013105113367675L;

    @NotEmpty
    private String               gatePassCode;

    @NotNull
    private Integer              userId;

    @NotBlank
    private String               putawayCode;

    @Valid
    private List<WsGatePassItem> gatePassItems    = new ArrayList<WsGatePassItem>();

    /**
     * @return the gatePassCode
     */
    public String getGatePassCode() {
        return gatePassCode;
    }

    /**
     * @param gatePassCode the gatePassCode to set
     */
    public void setGatePassCode(String gatePassCode) {
        this.gatePassCode = gatePassCode;
    }

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return the gatePassItems
     */
    public List<WsGatePassItem> getGatePassItems() {
        return gatePassItems;
    }

    /**
     * @param gatePassItems the gatePassItems to set
     */
    public void setGatePassItems(List<WsGatePassItem> gatePassItems) {
        this.gatePassItems = gatePassItems;
    }

    /**
     * @return the putawayCode
     */
    public String getPutawayCode() {
        return putawayCode;
    }

    /**
     * @param putawayCode the putawayCode to set
     */
    public void setPutawayCode(String putawayCode) {
        this.putawayCode = putawayCode;
    }

    /**
     * {@code id} id of {@link com.uniware.core.entity.OutboundGatePassItem} {@status } Enum of {@link Type}
     */
    public static class WsGatePassItem {

        @NotNull
        private Integer id;

        @NotNull
        private Type    status;

        @Min(value = 1)
        private Integer quantity;

        /**
         * @return the id
         */
        public Integer getId() {
            return id;
        }

        /**
         * @param id the id to set
         */
        public void setId(Integer id) {
            this.id = id;
        }

        /**
         * @return the status
         */
        public Type getStatus() {
            return status;
        }

        /**
         * @param status the status to set
         */
        public void setStatus(Type status) {
            this.status = status;
        }

        /**
         * @return the quantity
         */
        public Integer getQuantity() {
            return quantity;
        }

        /**
         * @param quantity the quantity to set
         */
        public void setQuantity(Integer quantity) {
            this.quantity = quantity;
        }

    }
}
