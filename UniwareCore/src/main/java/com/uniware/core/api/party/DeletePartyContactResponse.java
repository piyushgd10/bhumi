/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 04-Apr-2013
 *  @author pankaj
 */
package com.uniware.core.api.party;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author pankaj
 */
public class DeletePartyContactResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -3679019555091617089L;

}
