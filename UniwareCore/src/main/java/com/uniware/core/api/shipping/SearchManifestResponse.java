/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 07-May-2012
 *  @author praveeng
 */
package com.uniware.core.api.shipping;

import com.unifier.core.api.base.ServiceResponse;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author praveeng
 */
public class SearchManifestResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long         serialVersionUID = 5985187804527166641L;
    private List<ShippingManifestDTO> elements         = new ArrayList<ShippingManifestDTO>();
    private Long                      totalRecords;

    /**
     * @return the elements
     */
    public List<ShippingManifestDTO> getElements() {
        return elements;
    }

    /**
     * @param elements the elements to set
     */
    public void setElements(List<ShippingManifestDTO> elements) {
        this.elements = elements;
    }

    /**
     * @return the totalRecords
     */
    public Long getTotalRecords() {
        return totalRecords;
    }

    /**
     * @param totalRecords the totalRecords to set
     */
    public void setTotalRecords(Long totalRecords) {
        this.totalRecords = totalRecords;
    }

    public static class ShippingManifestDTO {
        private String manifestCode;
        private String generatedBy;
        private String username;
        private String statusCode;
        private Date   created;
        private String shippingProvider;
        private String shippingMethod;

        private String paymentMethodCode;

        /**
         * @return the manifestCode
         */
        public String getManifestCode() {
            return manifestCode;
        }

        /**
         * @param manifestCode the manifestCode to set
         */
        public void setManifestCode(String manifestCode) {
            this.manifestCode = manifestCode;
        }

        /**
         * @return the generatedBy
         */
        public String getGeneratedBy() {
            return generatedBy;
        }

        /**
         * @param generatedBy the generatedBy to set
         */
        public void setGeneratedBy(String generatedBy) {
            this.generatedBy = generatedBy;
        }

        /**
         * @return the username
         */
        public String getUsername() {
            return username;
        }

        /**
         * @param username the username to set
         */
        public void setUsername(String username) {
            this.username = username;
        }

        /**
         * @return the statusCode
         */
        public String getStatusCode() {
            return statusCode;
        }

        /**
         * @param statusCode the statusCode to set
         */
        public void setStatusCode(String statusCode) {
            this.statusCode = statusCode;
        }

        /**
         * @return the created
         */
        public Date getCreated() {
            return created;
        }

        /**
         * @param created the created to set
         */
        public void setCreated(Date created) {
            this.created = created;
        }

        /**
         * @return the shippingProvider
         */
        public String getShippingProvider() {
            return shippingProvider;
        }

        /**
         * @param shippingProvider the shippingProvider to set
         */
        public void setShippingProvider(String shippingProvider) {
            this.shippingProvider = shippingProvider;
        }

        /**
         * @return the shippingMethod
         */
        public String getShippingMethod() {
            return shippingMethod;
        }

        /**
         * @param shippingMethod the shippingMethod to set
         */
        public void setShippingMethod(String shippingMethod) {
            this.shippingMethod = shippingMethod;
        }

        public String getPaymentMethodCode() {
            return paymentMethodCode;
        }

        public void setPaymentMethodCode(String paymentMethodCode) {
            this.paymentMethodCode = paymentMethodCode;
        }
    }

}
