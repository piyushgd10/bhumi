/*
 * Copyright 2017 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 06/12/17
 * @author piyush
 */
package com.uniware.core.api.channel;

import com.unifier.core.api.base.ServiceResponse;

public class ReenableCITInventorySyncStatusResponse extends ServiceResponse {
}
