/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 03-Feb-2015
 *  @author akshaykochhar
 */
package com.uniware.core.api.returns;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

public class GetReturnManifestShipmentsSummaryRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -3086382739117710725L;
    
    @NotBlank
    private String returnManifestCode;

    public String getReturnManifestCode() {
        return returnManifestCode;
    }

    public void setReturnManifestCode(String returnManifestCode) {
        this.returnManifestCode = returnManifestCode;
    }
}
