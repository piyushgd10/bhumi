/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 27-Mar-2014
 *  @author parijat
 */
package com.uniware.core.api.reconciliation;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author parijat
 *
 */
public class GetChannelPaymentReconciliationMetadataResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 2492138789890566464L;


}
