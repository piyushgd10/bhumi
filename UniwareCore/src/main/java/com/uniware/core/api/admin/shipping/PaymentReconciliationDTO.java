/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 12, 2012
 *  @author praveeng
 */
package com.uniware.core.api.admin.shipping;

import java.math.BigDecimal;

/**
 * @author praveeng
 */
public class PaymentReconciliationDTO {

    private String     paymentMethodCode;
    private BigDecimal tolerance;
    private boolean    enabled;

    /**
     * @return the paymentMethodCode
     */
    public String getPaymentMethodCode() {
        return paymentMethodCode;
    }

    /**
     * @param paymentMethodCode the paymentMethodCode to set
     */
    public void setPaymentMethodCode(String paymentMethodCode) {
        this.paymentMethodCode = paymentMethodCode;
    }

    /**
     * @return the tolerance
     */
    public BigDecimal getTolerance() {
        return tolerance;
    }

    /**
     * @param tolerance the tolerance to set
     */
    public void setTolerance(BigDecimal tolerance) {
        this.tolerance = tolerance;
    }

    /**
     * @return the enabled
     */
    public boolean isEnabled() {
        return enabled;
    }

    /**
     * @param enabled the enabled to set
     */
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

}
