package com.uniware.core.api.tax;

import java.math.BigDecimal;

/**
 * Created by Sagar Sahni on 29/05/17.
 */
public class TaxPercentageDTO {

    private String     appliedTaxTypeCode;
    private BigDecimal vat               = BigDecimal.ZERO;
    private BigDecimal cst               = BigDecimal.ZERO;
    private BigDecimal cstFormc          = BigDecimal.ZERO;
    private BigDecimal serviceTax        = BigDecimal.ZERO;
    private BigDecimal taxPercentage     = BigDecimal.ZERO;
    private BigDecimal additionalTax     = BigDecimal.ZERO;
    private BigDecimal centralGst        = BigDecimal.ZERO;
    private BigDecimal stateGst          = BigDecimal.ZERO;
    private BigDecimal unionTerritoryGst = BigDecimal.ZERO;
    private BigDecimal integratedGst     = BigDecimal.ZERO;
    private BigDecimal compensationCess  = BigDecimal.ZERO;

    public String getAppliedTaxTypeCode() {
        return appliedTaxTypeCode;
    }

    public void setAppliedTaxTypeCode(String appliedTaxTypeCode) {
        this.appliedTaxTypeCode = appliedTaxTypeCode;
    }

    public BigDecimal getVat() {
        return vat;
    }

    public void setVat(BigDecimal vat) {
        this.vat = vat;
    }

    public BigDecimal getCst() {
        return cst;
    }

    public void setCst(BigDecimal cst) {
        this.cst = cst;
    }

    public BigDecimal getCstFormc() {
        return cstFormc;
    }

    public void setCstFormc(BigDecimal cstFormc) {
        this.cstFormc = cstFormc;
    }

    public BigDecimal getTaxPercentage() {
        return taxPercentage;
    }

    public void setTaxPercentage(BigDecimal taxPercentage) {
        this.taxPercentage = taxPercentage;
    }

    public BigDecimal getServiceTax() {
        return serviceTax;
    }

    public void setServiceTax(BigDecimal serviceTax) {
        this.serviceTax = serviceTax;
    }

    public BigDecimal getAdditionalTax() {
        return additionalTax;
    }

    public void setAdditionalTax(BigDecimal additionalTax) {
        this.additionalTax = additionalTax;
    }

    public BigDecimal getCentralGst() {
        return centralGst;
    }

    public void setCentralGst(BigDecimal centralGst) {
        this.centralGst = centralGst;
    }

    public BigDecimal getStateGst() {
        return stateGst;
    }

    public void setStateGst(BigDecimal stateGst) {
        this.stateGst = stateGst;
    }

    public BigDecimal getUnionTerritoryGst() {
        return unionTerritoryGst;
    }

    public void setUnionTerritoryGst(BigDecimal unionTerritoryGst) {
        this.unionTerritoryGst = unionTerritoryGst;
    }

    public BigDecimal getIntegratedGst() {
        return integratedGst;
    }

    public void setIntegratedGst(BigDecimal integratedGst) {
        this.integratedGst = integratedGst;
    }

    public BigDecimal getCompensationCess() {
        return compensationCess;
    }

    public void setCompensationCess(BigDecimal compensationCess) {
        this.compensationCess = compensationCess;
    }

    public BigDecimal getTotalGstTaxPercentage() {
        return this.centralGst.add(this.stateGst).add(this.unionTerritoryGst).add(this.integratedGst).add(this.compensationCess);
    }

    @Override public String toString() {
        return "TaxPercentageDTO{" +
                "appliedTaxTypeCode='" + appliedTaxTypeCode + '\'' +
                ", vat=" + vat +
                ", cst=" + cst +
                ", cstFormc=" + cstFormc +
                ", serviceTax=" + serviceTax +
                ", taxPercentage=" + taxPercentage +
                ", additionalTax=" + additionalTax +
                ", centralGst=" + centralGst +
                ", stateGst=" + stateGst +
                ", unionTerritoryGst=" + unionTerritoryGst +
                ", integratedGst=" + integratedGst +
                ", compensationCess=" + compensationCess +
                '}';
    }
}
