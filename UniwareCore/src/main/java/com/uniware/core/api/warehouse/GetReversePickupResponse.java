/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 20, 2012
 *  @author praveeng
 */
package com.uniware.core.api.warehouse;

import com.unifier.core.api.base.ServiceResponse;

import com.uniware.core.api.returns.ReversePickupDTO;

/**
 * @author praveeng
 */
public class GetReversePickupResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -3678888165091742703L;
    private ReversePickupDTO  reversePickup;

    /**
     * @return the reversePickup
     */
    public ReversePickupDTO getReversePickup() {
        return reversePickup;
    }

    /**
     * @param reversePickup the reversePickup to set
     */
    public void setReversePickup(ReversePickupDTO reversePickup) {
        this.reversePickup = reversePickup;
    }

}
