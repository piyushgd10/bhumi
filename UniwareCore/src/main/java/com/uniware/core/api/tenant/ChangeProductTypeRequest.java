/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 07-May-2014
 *  @author akshay
 */
package com.uniware.core.api.tenant;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

public class ChangeProductTypeRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 5633324302876760767L;

    @NotBlank
    private String            productType;

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

}
