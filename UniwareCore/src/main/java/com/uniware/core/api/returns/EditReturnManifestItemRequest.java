/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 02-Apr-2013
 *  @author pankaj
 */
package com.uniware.core.api.returns;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author pankaj
 */
public class EditReturnManifestItemRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 1103234085545117844L;

    @NotBlank
    private String            returnManifestCode;

    @Size(max = 100)
    private String            returnReason;

    @NotNull
    private String            packageCode;

    /**
     * @return the packageCode
     */
    public String getPackageCode() {
        return packageCode;
    }

    /**
     * @param packageCode the packageCode to set
     */
    public void setPackageCode(String packageCode) {
        this.packageCode = packageCode;
    }

    /**
     * @return the returnManifestCode
     */
    public String getReturnManifestCode() {
        return returnManifestCode;
    }

    /**
     * @param returnManifestCode the returnManifestCode to set
     */
    public void setReturnManifestCode(String returnManifestCode) {
        this.returnManifestCode = returnManifestCode;
    }

    /**
     * @return the returnReason
     */
    public String getReturnReason() {
        return returnReason;
    }

    /**
     * @param returnReason the returnReason to set
     */
    public void setReturnReason(String returnReason) {
        this.returnReason = returnReason;
    }

}
