/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jul 26, 2012
 *  @author singla
 */
package com.uniware.core.api.saleorder;

import com.unifier.core.api.base.ServiceResponse;

public class UnholdSaleOrderItemsResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -5871783380813119597L;

}
