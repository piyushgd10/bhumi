package com.uniware.core.api.channel;

import java.io.Serializable;

/**
 * Created by Samdeesh on 7/29/15.
 */
public class ChannelSyncAttributesDTO implements Serializable {

    private int    id;
    private String code;
    private String name;
    private String colorCode;
    private String shortName;
    private String sourceCode;

    public ChannelSyncAttributesDTO() {
    }

    public ChannelSyncAttributesDTO(ChannelSyncDTO channelSyncDTO) {
        this.id = channelSyncDTO.getChannelId();
        this.code = channelSyncDTO.getCode();
        this.name = channelSyncDTO.getName();
        this.colorCode = channelSyncDTO.getColorCode();
        this.shortName = channelSyncDTO.getShortName();
        this.sourceCode = channelSyncDTO.getSourceDTO().getCode();

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColorCode() {
        return colorCode;
    }

    public void setColorCode(String colorCode) {
        this.colorCode = colorCode;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getSourceCode() {
        return sourceCode;
    }

    public void setSourceCode(String sourceCode) {
        this.sourceCode = sourceCode;
    }
}
