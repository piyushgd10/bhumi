/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 11-Feb-2015
 *  @author parijat
 */
package com.uniware.core.api.shipping;

import com.uniware.core.entity.ShippingSourceConnectorParameter;

import java.io.Serializable;

public class ShippingSourceConnectorParameterDTO implements Serializable {

    public String name;
    public String displayName;
    public String displayPlaceHolder;
    public String type;
    public int    priority;

    public ShippingSourceConnectorParameterDTO() {
    }

    public ShippingSourceConnectorParameterDTO(ShippingSourceConnectorParameter sscp) {
        this.name = sscp.getName();
        this.displayName = sscp.getDisplayName();
        this.displayPlaceHolder = sscp.getDisplayPlaceHolder();
        this.type = sscp.getType().name();
        this.priority = sscp.getPriority();
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the displayName
     */
    public String getDisplayName() {
        return displayName;
    }

    /**
     * @param displayName the displayName to set
     */
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    /**
     * @return the displayPlaceHolder
     */
    public String getDisplayPlaceHolder() {
        return displayPlaceHolder;
    }

    /**
     * @param displayPlaceHolder the displayPlaceHolder to set
     */
    public void setDisplayPlaceHolder(String displayPlaceHolder) {
        this.displayPlaceHolder = displayPlaceHolder;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the priority
     */
    public int getPriority() {
        return priority;
    }

    /**
     * @param priority the priority to set
     */
    public void setPriority(int priority) {
        this.priority = priority;
    }
}
