/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 24-Jun-2014
 *  @author unicom
 */
package com.uniware.core.api.inventory;

import java.math.BigDecimal;

import com.uniware.core.entity.ItemType;
import com.uniware.core.entity.ItemTypeInventory;

public class InventoryDTO {

    private String     shelf;
    private String     itemTypeName;
    private String     imageUrl;
    private String     productPageUrl;
    private String     itemSKU;
    private int        quantity;
    private BigDecimal maxRetailPrice;
    private int        quantityInPicking;
    private String     facilityCode;
    private String     facilityName;
    private String     type;
    
    public InventoryDTO() {
        
    }

    public InventoryDTO(ItemTypeInventory inventory) {
        setShelf(inventory.getShelf().getCode());
        ItemType itemType = inventory.getItemType();
        setMaxRetailPrice(itemType.getMaxRetailPrice());
        setItemTypeName(itemType.getName());
        setItemSKU(itemType.getSkuCode());
        setImageUrl(itemType.getImageUrl());
        setProductPageUrl(itemType.getProductPageUrl());
        setQuantity(inventory.getQuantity());
        setFacilityCode(inventory.getFacility().getCode());
        setFacilityName(inventory.getFacility().getName());
        setType(inventory.getType().name());
    }

    public String getShelf() {
        return shelf;
    }

    public void setShelf(String shelf) {
        this.shelf = shelf;
    }

    public String getItemTypeName() {
        return itemTypeName;
    }

    public void setItemTypeName(String itemTypeName) {
        this.itemTypeName = itemTypeName;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getProductPageUrl() {
        return productPageUrl;
    }

    public void setProductPageUrl(String productPageUrl) {
        this.productPageUrl = productPageUrl;
    }

    public String getItemSKU() {
        return itemSKU;
    }

    public void setItemSKU(String itemSKU) {
        this.itemSKU = itemSKU;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getMaxRetailPrice() {
        return maxRetailPrice;
    }

    public void setMaxRetailPrice(BigDecimal maxRetailPrice) {
        this.maxRetailPrice = maxRetailPrice;
    }

    public int getQuantityInPicking() {
        return quantityInPicking;
    }

    public void setQuantityInPicking(int quantityInPicking) {
        this.quantityInPicking = quantityInPicking;
    }

    public String getFacilityCode() {
        return facilityCode;
    }

    public void setFacilityCode(String facilityCode) {
        this.facilityCode = facilityCode;
    }

    public String getFacilityName() {
        return facilityName;
    }

    public void setFacilityName(String facilityName) {
        this.facilityName = facilityName;
    }
    
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}
