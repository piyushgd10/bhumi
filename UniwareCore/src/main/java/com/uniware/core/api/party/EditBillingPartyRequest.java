/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 20, 2012
 *  @author praveeng
 */
package com.uniware.core.api.party;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.unifier.core.api.base.ServiceRequest;
import com.uniware.core.api.sequence.WsSequence;

/**
 * @author Sunny
 */
public class EditBillingPartyRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -8607921700580971155L;

    @NotNull
    @Valid
    private WsBillingParty    billingParty;

    @Valid
    private WsSequence        taxInvoiceSequence;

    @Valid
    private WsSequence        retailInvoiceSequence;

    public WsBillingParty getBillingParty() {
        return billingParty;
    }

    public void setBillingParty(WsBillingParty billingParty) {
        this.billingParty = billingParty;
    }

    public WsSequence getTaxInvoiceSequence() {
        return taxInvoiceSequence;
    }

    public void setTaxInvoiceSequence(WsSequence taxInvoiceSequence) {
        this.taxInvoiceSequence = taxInvoiceSequence;
    }

    public WsSequence getRetailInvoiceSequence() {
        return retailInvoiceSequence;
    }

    public void setRetailInvoiceSequence(WsSequence retailInvoiceSequence) {
        this.retailInvoiceSequence = retailInvoiceSequence;
    }
}
