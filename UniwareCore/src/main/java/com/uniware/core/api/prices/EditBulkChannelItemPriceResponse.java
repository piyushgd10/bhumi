/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *  @version     1.0, Dec 7, 2015
 *  @author shipra
 */
package com.uniware.core.api.prices;

import com.unifier.core.api.base.ServiceResponse;
import com.uniware.core.api.prices.dto.PriceDTO;

import java.util.HashMap;
import java.util.Map;

public class EditBulkChannelItemPriceResponse extends ServiceResponse {
    private Map<String, String> failedChannelProductIdToErrorMessage;
    private Map<String, PriceDTO> channelProductIdToPriceDTO = new HashMap<String, PriceDTO>();

    public Map<String, String> getFailedChannelProductIdToErrorMessage() {
        return failedChannelProductIdToErrorMessage;
    }

    public void setFailedChannelProductIdToErrorMessage(Map<String, String> failedChannelProductIdToErrorMessage) {
        this.failedChannelProductIdToErrorMessage = failedChannelProductIdToErrorMessage;
    }

    public Map<String, PriceDTO> getChannelProductIdToPriceDTO() {
        return channelProductIdToPriceDTO;
    }

}
