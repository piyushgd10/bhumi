/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 27-Mar-2014
 *  @author parijat
 */
package com.uniware.core.api.reconciliation;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author parijat
 */

public class GetChannelReconciliationMetadataRequest extends ServiceRequest {

    private static final long serialVersionUID = 8987783406500495068L;

    @NotBlank
    private String            channelSaleOrderCode;

    @NotBlank
    private String            channelSaleOrderItemCode;

    @NotBlank
    private String            channelCode;

    public String getChannelSaleOrderCode() {
        return channelSaleOrderCode;
    }

    public void setChannelSaleOrderCode(String channelSaleOrderCode) {
        this.channelSaleOrderCode = channelSaleOrderCode;
    }

    public String getChannelSaleOrderItemCode() {
        return channelSaleOrderItemCode;
    }

    public void setChannelSaleOrderItemCode(String channelSaleOrderItemCode) {
        this.channelSaleOrderItemCode = channelSaleOrderItemCode;
    }

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }
}
