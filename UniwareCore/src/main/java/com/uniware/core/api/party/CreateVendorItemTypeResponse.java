/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 24, 2012
 *  @author praveeng
 */
package com.uniware.core.api.party;

import com.unifier.core.api.base.ServiceResponse;

import com.uniware.core.api.party.dto.VendorItemTypeDTO;

/**
 * @author praveeng
 */
public class CreateVendorItemTypeResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 4458665417232943376L;
    private VendorItemTypeDTO vendorItemType;

    /**
     * @return the vendorItemType
     */
    public VendorItemTypeDTO getVendorItemType() {
        return vendorItemType;
    }

    /**
     * @param vendorItemType the vendorItemType to set
     */
    public void setVendorItemType(VendorItemTypeDTO vendorItemType) {
        this.vendorItemType = vendorItemType;
    }

}
