/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Oct 13, 2015
 *  @author akshay
 */
package com.uniware.core.api.channel;

import com.uniware.core.entity.Channel;
import com.uniware.core.entity.ChannelItemTypePrice;
import com.uniware.core.entity.ItemType;

public class ChannelPriceSyncDTO {
    
    private Channel                          channel;
    private ChannelItemTypePrice             channelItemTypePrice;
    private ItemType                         itemType;

    public ChannelPriceSyncDTO() {
    }

    public ChannelPriceSyncDTO(ChannelItemTypePrice citp, Channel c, ItemType itemType) {
        this.channel = c;
        this.channelItemTypePrice = citp;
        this.itemType = itemType;
    }

    public Channel getChannel() {
        return channel;
    }
    
    public String getChannelId() {
        return channel.getCode();
    }

    public ChannelItemTypePrice getChannelItemTypePrice() {
        return channelItemTypePrice;
    }

    public String getChannelSkuCode() {
        return channelItemTypePrice.getChannelItemType().getSellerSkuCode();
    }

    public String getChannelProductId() {
        return channelItemTypePrice.getChannelProductId();
    }

    public ItemType getItemType() {
        return itemType;
    }
}
