/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 29-Mar-2012
 *  @author ankit
 */
package com.uniware.core.api.shipping;

import com.unifier.core.api.base.ServiceResponse;

import java.util.ArrayList;
import java.util.List;

import com.uniware.core.api.saleorder.ShippingPackageDTO;

/**
 * @author ankit
 */
public class SearchShippingPackageResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long              serialVersionUID = -1174289477269305541L;
    private Long                           totalRecords;
    private final List<ShippingPackageDTO> elements         = new ArrayList<ShippingPackageDTO>();

    /**
     * @return the totalRecords
     */
    public Long getTotalRecords() {
        return totalRecords;
    }

    /**
     * @param totalRecords the totalRecords to set
     */
    public void setTotalRecords(Long totalRecords) {
        this.totalRecords = totalRecords;
    }

    /**
     * @return the elements
     */
    public List<ShippingPackageDTO> getElements() {
        return elements;
    }

}
