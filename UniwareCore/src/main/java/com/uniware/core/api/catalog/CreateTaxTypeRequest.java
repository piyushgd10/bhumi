/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 20-Jul-2012
 *  @author praveeng
 */
package com.uniware.core.api.catalog;

import com.unifier.core.api.base.ServiceRequest;

import javax.validation.Valid;

import com.uniware.core.api.tax.WsTaxType;

/**
 * @author praveeng
 */
public class CreateTaxTypeRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -6834447171335905440L;

    @Valid
    private WsTaxType         wsTaxType;

    /**
     * @return the wsTaxType
     */
    public WsTaxType getWsTaxType() {
        return wsTaxType;
    }

    /**
     * @param wsTaxType the wsTaxType to set
     */
    public void setWsTaxType(WsTaxType wsTaxType) {
        this.wsTaxType = wsTaxType;
    }

}
