/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 24-Mar-2014
 *  @author parijat
 */
package com.uniware.core.api.apiStatistics;


public class ApiAccessStatisticsDTO {

    private String  uniwareUserName;
    private String  apiUserName;
    private String  apiName;
    private String  apiVersion;
    private Integer successfulExecutionCount;
    private Integer failedExecutionCount;
    private String  lastExecutedDate;
    private String  lastFailedDate;

    public String getUniwareUserName() {
        return uniwareUserName;
    }

    public void setUniwareUserName(String uniwareUserName) {
        this.uniwareUserName = uniwareUserName;
    }

    public String getApiUserName() {
        return apiUserName;
    }

    public void setApiUserName(String apiUserName) {
        this.apiUserName = apiUserName;
    }

    public String getApiName() {
        return apiName;
    }

    public void setApiName(String apiName) {
        this.apiName = apiName;
    }

    public String getApiVersion() {
        return apiVersion;
    }

    public void setApiVersion(String apiVersion) {
        this.apiVersion = apiVersion;
    }

    public Integer getSuccessfulExecutionCount() {
        return successfulExecutionCount;
    }

    public void setSuccessfulExecutionCount(Integer successfulExecutionCount) {
        this.successfulExecutionCount = successfulExecutionCount;
    }

    public Integer getFailedExecutionCount() {
        return failedExecutionCount;
    }

    public void setFailedExecutionCount(Integer failedExecutionCount) {
        this.failedExecutionCount = failedExecutionCount;
    }

    public String getLastExecutedDate() {
        return lastExecutedDate;
    }

    public void setLastExecutedDate(String lastExecutedDate) {
        this.lastExecutedDate = lastExecutedDate;
    }

    public String getLastFailedDate() {
        return lastFailedDate;
    }

    public void setLastFailedDate(String lastFailedDate) {
        this.lastFailedDate = lastFailedDate;
    }

}
