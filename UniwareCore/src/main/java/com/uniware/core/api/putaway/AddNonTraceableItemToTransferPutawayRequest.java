/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 10-May-2013
 *  @author karunsingla
 */
package com.uniware.core.api.putaway;

import com.unifier.core.api.base.ServiceRequest;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

/**
 * <ul>
 * Parameters:
 * <li>{@code itemSKU}: sku code of item</li>
 * <li>{@code quantity}: item quantity</li>
 * <li>{@code putawayCode}: code of putaway in which gatepass items are to be added.</li>
 * <li>{@code userId}: id of user, set by context</li>
 * <li>{@code shelfCode}: shelf code on which putaway is to be done</li>
 * <li>{@code inventoryType}: inventory type of item</li>
 * </ul>
 */
public class AddNonTraceableItemToTransferPutawayRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 5631753270274442016L;

    @NotBlank
    private String            itemSKU;

    @Min(value = 1)
    @NotNull
    private Integer           quantity;

    @NotBlank
    private String            putawayCode;

    @NotNull
    private Integer           userId;

    private String            shelfCode;

    private String            inventoryType;

    public String getShelfCode() {
        return shelfCode;
    }

    public void setShelfCode(String shelfCode) {
        this.shelfCode = shelfCode;
    }

    public String getInventoryType() {
        return inventoryType;
    }

    public void setInventoryType(String inventoryType) {
        this.inventoryType = inventoryType;
    }

    public String getItemSKU() {
        return itemSKU;
    }

    public void setItemSKU(String itemSKU) {
        this.itemSKU = itemSKU;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getPutawayCode() {
        return putawayCode;
    }

    public void setPutawayCode(String putawayCode) {
        this.putawayCode = putawayCode;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }
}
