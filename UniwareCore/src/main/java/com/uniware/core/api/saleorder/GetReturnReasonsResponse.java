package com.uniware.core.api.saleorder;

import com.unifier.core.api.base.ServiceResponse;
import com.unifier.core.ui.SelectItem;

import java.util.List;

/**
 * Created by bhuvneshwarkumar on 07/08/15.
 */
public class GetReturnReasonsResponse extends ServiceResponse{
    private List<SelectItem> returnReasons;

    public List<SelectItem> getReturnReasons() {
        return returnReasons;
    }

    public void setReturnReasons(List<SelectItem> returnReasons) {
        this.returnReasons = returnReasons;
    }
}
