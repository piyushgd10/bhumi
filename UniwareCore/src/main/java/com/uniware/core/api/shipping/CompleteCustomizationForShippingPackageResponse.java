/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 14, 2013
 *  @author karunsingla
 */
package com.uniware.core.api.shipping;

import com.unifier.core.api.base.ServiceResponse;

public class CompleteCustomizationForShippingPackageResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 3568670420789271768L;

}
