/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 11, 2012
 *  @author praveeng
 */
package com.uniware.core.api.purchase;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.unifier.core.api.customfields.CustomFieldMetadataDTO;
import com.uniware.core.api.party.PartyAddressDTO;

/**
 * @author Sunny Agarwal
 */
public class PurchaseOrderDetailDTO {

    private Integer                      id;
    private String                       amendedPurchaseOrderCode;
    private String                       amendmentPurchaseOrderCode;
    private String                       name;
    private String                       code;
    private String                       type;
    private String                       fromParty;
    private String                       statusCode;
    private String                       vendorCode;
    private String                       vendorName;
    private Date                         created;
    private Date                         expiryDate;
    private Date                         deliveryDate;
    private String                       vendorAgreementName;
    private Integer                      inflowReceiptsCount;
    private List<CustomFieldMetadataDTO> customFieldValues;
    private List<PurchaseOrderItemDTO>   purchaseOrderItems = new ArrayList<PurchaseOrderItemDTO>();
    private PartyAddressDTO              partyAddressDTO;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAmendedPurchaseOrderCode() {
        return amendedPurchaseOrderCode;
    }

    public void setAmendedPurchaseOrderCode(String amendedPurchaseOrderCode) {
        this.amendedPurchaseOrderCode = amendedPurchaseOrderCode;
    }

    public String getAmendmentPurchaseOrderCode() {
        return amendmentPurchaseOrderCode;
    }

    public void setAmendmentPurchaseOrderCode(String amendmentPurchaseOrderCode) {
        this.amendmentPurchaseOrderCode = amendmentPurchaseOrderCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFromParty() {
        return fromParty;
    }

    public void setFromParty(String fromParty) {
        this.fromParty = fromParty;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getVendorCode() {
        return vendorCode;
    }

    public void setVendorCode(String vendorCode) {
        this.vendorCode = vendorCode;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    public Date getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(Date deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public String getVendorAgreementName() {
        return vendorAgreementName;
    }

    public void setVendorAgreementName(String vendorAgreementName) {
        this.vendorAgreementName = vendorAgreementName;
    }

    public Integer getInflowReceiptsCount() {
        return inflowReceiptsCount;
    }

    public void setInflowReceiptsCount(Integer inflowReceiptsCount) {
        this.inflowReceiptsCount = inflowReceiptsCount;
    }

    public List<CustomFieldMetadataDTO> getCustomFieldValues() {
        return customFieldValues;
    }

    public void setCustomFieldValues(List<CustomFieldMetadataDTO> customFieldValues) {
        this.customFieldValues = customFieldValues;
    }

    public List<PurchaseOrderItemDTO> getPurchaseOrderItems() {
        return purchaseOrderItems;
    }

    public void setPurchaseOrderItems(List<PurchaseOrderItemDTO> purchaseOrderItems) {
        this.purchaseOrderItems = purchaseOrderItems;
    }

    public PartyAddressDTO getPartyAddressDTO() {
        return partyAddressDTO;
    }

    public void setPartyAddressDTO(PartyAddressDTO partyAddressDTO) {
        this.partyAddressDTO = partyAddressDTO;
    }

}
