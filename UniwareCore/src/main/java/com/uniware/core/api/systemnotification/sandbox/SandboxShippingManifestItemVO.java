/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 25-Dec-2013
 *  @author sunny
 */
package com.uniware.core.api.systemnotification.sandbox;

import com.uniware.core.entity.ShippingManifestItem;

public class SandboxShippingManifestItemVO {

    private SandboxShippingPackageVO shippingPackage;

    public SandboxShippingManifestItemVO() {
        super();
    }

    public SandboxShippingManifestItemVO(ShippingManifestItem smi) {
        this.shippingPackage = new SandboxShippingPackageVO(smi.getShippingPackage());
    }

    public SandboxShippingPackageVO getShippingPackage() {
        return shippingPackage;
    }

    public void setShippingPackage(SandboxShippingPackageVO shippingPackage) {
        this.shippingPackage = shippingPackage;
    }

}
