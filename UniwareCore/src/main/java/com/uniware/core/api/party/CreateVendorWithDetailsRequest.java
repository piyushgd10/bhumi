/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 20, 2012
 *  @author praveeng
 */
package com.uniware.core.api.party;

import com.unifier.core.api.base.ServiceRequest;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * @author praveeng
 */
public class CreateVendorWithDetailsRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 6523220723010983877L;

    @NotNull
    @Valid
    private WsVendor          wsVendor;

    @NotNull
    @Valid
    private WsPartyAddress    shippingAddress;

    @NotNull
    @Valid
    private WsPartyAddress    billingAddress;

    @NotNull
    @Valid
    private WsPartyContact    contact;

    @NotNull
    @Valid
    private WsVendorAgreement agreement;

    /**
     * @return the wsVendor
     */
    public WsVendor getWsVendor() {
        return wsVendor;
    }

    /**
     * @param wsVendor the wsVendor to set
     */
    public void setWsVendor(WsVendor wsVendor) {
        this.wsVendor = wsVendor;
    }

    /**
     * @return the shippingAddress
     */
    public WsPartyAddress getShippingAddress() {
        return shippingAddress;
    }

    /**
     * @param shippingAddress the shippingAddress to set
     */
    public void setShippingAddress(WsPartyAddress shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    /**
     * @return the billingAddress
     */
    public WsPartyAddress getBillingAddress() {
        return billingAddress;
    }

    /**
     * @param billingAddress the billingAddress to set
     */
    public void setBillingAddress(WsPartyAddress billingAddress) {
        this.billingAddress = billingAddress;
    }

    /**
     * @return the contact
     */
    public WsPartyContact getContact() {
        return contact;
    }

    /**
     * @param contact the contact to set
     */
    public void setContact(WsPartyContact contact) {
        this.contact = contact;
    }

    /**
     * @return the agreement
     */
    public WsVendorAgreement getAgreement() {
        return agreement;
    }

    /**
     * @param agreement the agreement to set
     */
    public void setAgreement(WsVendorAgreement agreement) {
        this.agreement = agreement;
    }

}
