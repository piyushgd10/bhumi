/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 03-Jun-2012
 *  @author vibhu
 */
package com.uniware.core.api.channel;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author Sunny
 */
public class AddChannelConnectorRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long  serialVersionUID = 1417801739977690313L;

    @NotNull
    @Valid
    private WsChannelConnector channelConnector;

    public WsChannelConnector getChannelConnector() {
        return channelConnector;
    }

    public void setChannelConnector(WsChannelConnector channelConnector) {
        this.channelConnector = channelConnector;
    }

}
