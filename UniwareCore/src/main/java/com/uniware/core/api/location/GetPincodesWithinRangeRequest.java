/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 12-May-2015
 *  @author parijat
 */
package com.uniware.core.api.location;

import java.math.BigDecimal;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author parijat
 */
public class GetPincodesWithinRangeRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 1828683960821780082L;

    @NotBlank
    private String            pincode;

    @NotNull
    private BigDecimal        distance;

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public BigDecimal getDistance() {
        return distance;
    }

    public void setDistance(BigDecimal distance) {
        this.distance = distance;
    }


}
