/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 03-Mar-2014
 *  @author amit
 */
package com.uniware.core.script.error;

public class ChannelScriptError {
    public enum ScriptErrorCodes{
        INVALID_CREDENTIALS,
        CHANNEL_REFUSED_SYNC,
        CHANNEL_ERROR,
        SERVER_DOWN
    }

    private String                                 code;
    private String                                 message;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
    public String getMessage() {
        return this.message;
    }
    public void setMessage(String message) {
        this.message = message;
    }


   public ChannelScriptError(String code, String message) {
        this.code = code;
        this.message = message;
    }


}
