/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 03-Mar-2014
 *  @author amit
 */
package com.uniware.core.script.error;

import java.util.HashMap;
import java.util.Map;

public enum SaleOrderScriptError {
    ORDER_CANCELLED("ORDER_CANCELLED", "Order is already in Cancelled state", false),
    ORDER_SHIPPED("ORDER_SHIPPED", "Order is already in Shipped state", false),
    ORDER_NOT_PRESENT("ORDER_NOT_PRESENT", "Order not found on channel", false),
    ORDER_TO_BE_CANCELLED("ORDER_TO_BE_CANCELLED", "Order is about to cancel on panel", false),
    ORDER_ON_HOLD("ORDER_ON_HOLD", "Order is on hold on panel", false),
    ORDER_RETURN_REQUESTED("ORDER_RETURN_REQUESTED", "Return has been requested for order on panel.", false);

    private String                          code;
    private String                          message;
    private boolean                         systemNotificationRequired = true;
    private static Map<String, SaleOrderScriptError> codeToErrorMap             = new HashMap<String, SaleOrderScriptError>();

    static {
        for (SaleOrderScriptError e : SaleOrderScriptError.values()) {
            codeToErrorMap.put(e.code, e);
        }
    }

    SaleOrderScriptError(String code, String message, boolean systemNotificationRequired) {
        this.code = code;
        this.message = message;
        this.systemNotificationRequired = systemNotificationRequired;
    }

    public boolean isSystemNotificationRequired() {
        return this.systemNotificationRequired;
    }

    public String getMessage() {
        return this.message;
    }

    public static SaleOrderScriptError getScriptError(String code) {
        return codeToErrorMap.get(code);
    }
}