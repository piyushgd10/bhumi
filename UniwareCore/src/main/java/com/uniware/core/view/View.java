/*
 * Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 3/22/15 5:48 PM
 * @author amdalal
 */

package com.uniware.core.view;

import com.unifier.core.annotation.Level;

public enum View {

    SHIPPING_PACKAGE_VIEW("shippingPackageView", "shipment", Level.FACILITY);

    String collectionName;

    String uniqueField;

    Level  level;

    View(String name, String uniqueField, Level level) {
        this.collectionName = name;
        this.uniqueField = uniqueField;
        this.level = level;
    }

    public String getCollectionName() {
        return this.collectionName;
    }

    public String getUniqueField() {
        return this.uniqueField;
    }

    public Level getLevel() {
        return this.level;
    }
}