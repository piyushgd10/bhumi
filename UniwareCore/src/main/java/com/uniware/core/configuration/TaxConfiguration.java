/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jun 15, 2012
 *  @author singla
 */
package com.uniware.core.configuration;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import org.springframework.beans.factory.annotation.Autowired;

import com.unifier.core.annotation.Configuration;
import com.unifier.core.annotation.Level;
import com.unifier.core.configuration.IConfiguration;
import com.unifier.core.configuration.data.manager.IConfigurationDataManager;
import com.unifier.core.utils.TernarySearchTree;
import com.uniware.core.entity.TaxType;
import com.uniware.core.entity.TaxTypeConfiguration;

/**
 * @author singla
 */
@Configuration(name = "taxConfiguration", level = Level.TENANT)
public class TaxConfiguration implements IConfiguration {

    private List<TaxType>                                             taxTypes;
    private transient TernarySearchTree<TaxTypeVO>                    taxTypeIndexedTree;
    private transient Map<String, TaxType>                            codeToTaxTypes;
    private transient Map<Integer, TaxType>                           idToTaxTypes;
    private transient Map<Integer, Map<String, TaxTypeConfiguration>> taxTypeIdToStateCodeToConfigurations;

    @Autowired
    private transient IConfigurationDataManager                       configurationDataManager;

    public void addTaxType(TaxType taxType) {
        codeToTaxTypes.put(taxType.getCode().toLowerCase().trim(), taxType);
        idToTaxTypes.put(taxType.getId(), taxType);
        TaxTypeVO taxTypeVO = new TaxTypeVO(taxType);
        String code = taxType.getCode().toLowerCase();
        Scanner scanner = new Scanner(code);
        scanner.useDelimiter("\\W+");
        while (scanner.hasNext()) {
            taxTypeIndexedTree.put(scanner.next(), taxTypeVO);
        }
        scanner.close();
        taxTypeIndexedTree.put(code, taxTypeVO);

        for (TaxTypeConfiguration taxTypeConfiguration : taxType.getTaxTypeConfigurations()) {
            Map<String, TaxTypeConfiguration> stateCodeToConfigurations = taxTypeIdToStateCodeToConfigurations.get(taxType.getId());
            if (stateCodeToConfigurations == null) {
                stateCodeToConfigurations = new HashMap<String, TaxTypeConfiguration>();
                taxTypeIdToStateCodeToConfigurations.put(taxType.getId(), stateCodeToConfigurations);
            }
            if (!stateCodeToConfigurations.containsKey(taxTypeConfiguration.getStateCode())) {
                stateCodeToConfigurations.put(taxTypeConfiguration.getStateCode(), taxTypeConfiguration);
            }
        }
    }

    public TaxType getTaxTypeByCode(String taxTypeCode) {
        return codeToTaxTypes.get(taxTypeCode.toLowerCase().trim());
    }

    public TaxType getTaxTypeById(int taxTypeId) {
        return idToTaxTypes.get(taxTypeId);
    }

    public TaxTypeConfiguration getTaxTypeConfiguration(Integer taxTypeId, String stateCode) {
        Map<String, TaxTypeConfiguration> stateToConfigurations = taxTypeIdToStateCodeToConfigurations.get(taxTypeId);
        return stateToConfigurations != null ? stateToConfigurations.get(stateCode) : null;
    }

    /**
     * @param code
     * @return
     */
    public List<TaxTypeVO> lookupTaxTypes(String code) {
        List<TaxTypeVO> taxTypes = new ArrayList<TaxTypeVO>();
        taxTypes.addAll(taxTypeIndexedTree.matchPrefix(code.toLowerCase()));
        return taxTypes;
    }

    public static class TaxTypeVO implements Serializable {
        private Integer id;
        private String  code;
        private String  name;

        public TaxTypeVO(TaxType taxType) {
            this.id = taxType.getId();
            this.code = taxType.getCode();
            this.name = taxType.getName();
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

    }

    @Override
    public void load() {
        taxTypes = configurationDataManager.getAllTaxTypes();
        doLoad();
    }

    private void doLoad() {
        taxTypeIndexedTree = new TernarySearchTree<>();
        codeToTaxTypes = new HashMap<>();
        idToTaxTypes = new HashMap<>();
        taxTypeIdToStateCodeToConfigurations = new HashMap<>();
        for (TaxType taxType : taxTypes) {
            addTaxType(taxType);
        }
    }

    private void readObject(java.io.ObjectInputStream in) throws IOException, ClassNotFoundException {
        in.defaultReadObject();
        doLoad();
    }
}
