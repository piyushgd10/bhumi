/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Aug 21, 2012
 *  @author singla
 */
package com.uniware.core.utils;

import com.unifier.core.cache.CacheManager;
import com.uniware.core.cache.EnvironmentPropertiesCache;

public class EnvironmentProperties {

    public static final String APPROVED_PURCHASE_ORDERS_DIRECTORY_PATH = "approved.pos.directory.path";
    public static final String SHIPPING_MANIFEST_DIRECTORY_PATH        = "shipping.manifest.directory.path";
    public static final String RETURN_MANIFEST_DIRECTORY_PATH          = "return.manifest.directory.path";
    public static final String QC_REJECTION_REPORT_PATH                = "qc.rejection.report.path";
    public static final String GATEPASS_COMPLETION_REPORT              = "gatepass.completion.report.path";
    public static final String DESK_HELP_SEARCH_PATH                   = "desk.help.search.path";
    public static final String ALLOW_SERIALIZABLE_CUSTOM_FIELDS        = "allow.serializable.custom.fields";
    public static final String TEMPORARY_FILE_PATH                     = "temporary.file.path";

    public static String getGatepassCompletionReport() {
        return getProperty(GATEPASS_COMPLETION_REPORT);
    }

    public static String getApprovedPurchaseOrdersDirectoryPath() {
        return getProperty(APPROVED_PURCHASE_ORDERS_DIRECTORY_PATH);
    }

    public static String getShippingManifestDirectoryPath() {
        return getProperty(SHIPPING_MANIFEST_DIRECTORY_PATH);
    }

    public static String getReturnManifestDirectoryPath() {
        return getProperty(RETURN_MANIFEST_DIRECTORY_PATH);
    }

    public static String getQCRejectionReportPath() {
        return getProperty(QC_REJECTION_REPORT_PATH);
    }

    public static String getDeskHelpSearchPath() {
        return getProperty(DESK_HELP_SEARCH_PATH);
    }

    public static String getAllowSerializableCustomFields() {
        return getProperty(ALLOW_SERIALIZABLE_CUSTOM_FIELDS);
    }

    public static String getTemporaryFilePath() {
        return getProperty(TEMPORARY_FILE_PATH);
    }

    public static String getProperty(String name, String defaultValue) {
        EnvironmentPropertiesCache cache = CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class);
        return cache.getProperty(name, defaultValue);
    }

    public static String getProperty(String name) {
        EnvironmentPropertiesCache cache = CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class);
        return cache.getProperty(name);
    }
}
