/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 18-Jun-2014
 *  @author parijat
 */
package com.uniware.core.utils;

import java.util.HashMap;
import java.util.Map;

import com.unifier.core.api.audit.ActivityDto;

public class ActivityContext {

    private static ThreadLocal<ActivityContext> ctx = new ThreadLocal<>();

    private boolean                             enable;
    private Map<String, ActivityDto>            loggingDtos = new HashMap<String, ActivityDto>();

    public ActivityContext() {

    }

    public static ActivityContext current() {
        ActivityContext activityContext = ctx.get();
        if (activityContext == null) {
            activityContext = new ActivityContext();
            ctx.set(activityContext);
        }
        return activityContext;
    }

    public static void setActivityContext(ActivityContext context) {
        ctx.set(context);
    }

    public static void destory() {
        ctx.remove();
    }

    /**
     * @return the enable
     */
    public boolean isEnable() {
        return enable;
    }

    /**
     * @param enable the enable to set
     */
    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    /**
     * @return the loggingDtos
     */
    public Map<String, ActivityDto> getLoggingDtos() {
        return loggingDtos;
    }

    /**
     * @param loggingDtos the loggingDtos to set
     */
    public void setLoggingDtos(Map<String, ActivityDto> loggingDtos) {
        this.loggingDtos = loggingDtos;
    }
}
