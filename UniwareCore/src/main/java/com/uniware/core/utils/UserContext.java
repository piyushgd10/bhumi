package com.uniware.core.utils;

import com.uniware.core.entity.Facility;
import com.uniware.core.entity.Tenant;

public class UserContext {
    private static ThreadLocal<UserContext> ctx             = new ThreadLocal<UserContext>();

    private Integer                         tenantId;
    private Tenant                          tenant;
    private Facility                        facility;
    private String                          uniwareUserName;
    private Integer                         userId;
    private String                          apiUsername;
    private String                          apiVersion;
    private String                          threadInfo;
    private String                          logRoutingKey;
    private boolean                         traceLogEnabled = false;
    private String                          requestId;

    public UserContext() {
    }

    public UserContext(UserContext context) {
        this.tenantId = context.getTenantId();
        this.facility = context.getFacility();
        this.tenant = context.getTenant();
        this.uniwareUserName = context.getUniwareUserName();
        this.apiUsername = context.getApiUsername();
        this.apiVersion = context.getApiVersion();
        this.threadInfo = context.getThreadInfo();
        this.requestId = context.getRequestId();
        setLogRoutingKey(context.getLogRoutingKey());
        setTraceLogEnabled(context.isTraceLoggingEnabled());
    }

    public static UserContext current() {
        UserContext userContext = ctx.get();
        if (userContext == null) {
            userContext = new UserContext();
            ctx.set(userContext);
        }
        return userContext;
    }

    public static void setUserContext(UserContext context) {
        ctx.set(context);
    }

    public static void destroy() {
        ctx.remove();
    }

    public Integer getTenantId() {
        return tenantId;
    }

    public void setTenantId(Integer tenantId) {
        this.tenantId = tenantId;
    }

    public void setTenant(Tenant tenant) {
        this.tenant = tenant;
        this.tenantId = tenant.getId();
    }

    /**
     * @return the facilityId
     */
    public Integer getFacilityId() {
        return facility != null ? facility.getId() : null;
    }

    public void setFacility(Facility facility) {
        this.facility = facility;
    }

    /**
     * @return
     */
    public Facility getFacility() {
        return this.facility;
    }

    /**
     * @return
     */
    public Tenant getTenant() {
        return tenant;
    }

    /**
     * @return the uniwareUserName
     */
    public String getUniwareUserName() {
        return uniwareUserName;
    }

    /**
     * @param uniwareUserName the uniwareUserName to set
     */
    public void setUniwareUserName(String uniwareUserName) {
        this.uniwareUserName = uniwareUserName;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getApiUsername() {
        return apiUsername;
    }

    public void setApiUsername(String apiUsername) {
        this.apiUsername = apiUsername;
    }

    /**
     * @return the apiVersion
     */
    public String getApiVersion() {
        return apiVersion;
    }

    /**
     * @param apiVersion the apiVersion to set
     */
    public void setApiVersion(String apiVersion) {
        this.apiVersion = apiVersion;
    }

    public String getThreadInfo() {
        return threadInfo;
    }

    public void setThreadInfo(String threadInfo) {
        this.threadInfo = threadInfo;
    }

    public String getLogRoutingKey() {
        return logRoutingKey;
    }

    public void setLogRoutingKey(String logRoutingKey) {
        this.logRoutingKey = logRoutingKey;
    }

    public boolean isTraceLoggingEnabled() {
        return this.traceLogEnabled;
    }

    public void setTraceLogEnabled(boolean traceLogEnabled) {
        this.traceLogEnabled = traceLogEnabled;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

}
