/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 24-Apr-2014
 *  @author parijat
 */
package com.uniware.core.utils;

/**
 * Can be used to perform any activity post creation of an entity.
 * 
 * @author Sunny
 */
public class EntityPersistenceContext<T> {
    private static ThreadLocal<EntityPersistenceContext> ctx = new ThreadLocal<EntityPersistenceContext>();

    private T                                            entity;

    public EntityPersistenceContext() {
    }

    public static EntityPersistenceContext current() {
        EntityPersistenceContext context = ctx.get();
        if (context == null) {
            context = new EntityPersistenceContext();
            ctx.set(context);
        }
        return context;
    }

    public T getEntity() {
        return entity;
    }

    public void setEntity(T entity) {
        this.entity = entity;
    }

    public static void destroy() {
        ctx.remove();
    }
}
