/*
 * Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 3/22/15 4:26 PM
 * @author amdalal
 */

package com.uniware.core.utils;

import com.uniware.core.view.View;

import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public class ViewContext {

    private static ThreadLocal<ViewContext> ctx = new ThreadLocal<>();

    private Map<String, Set<String>> viewToDirtySaleOrderCodesMap = new ConcurrentHashMap<>();

    private ViewContext() {
        super();
        for (View view : View.values()) {
            viewToDirtySaleOrderCodesMap.put(view.getCollectionName(), Collections.newSetFromMap(new ConcurrentHashMap<String, Boolean>()));
        }
    }

    public ViewContext(ViewContext viewContext) {
        super();
        this.viewToDirtySaleOrderCodesMap = viewContext.viewToDirtySaleOrderCodesMap;
    }

    public static void setViewContext(ViewContext viewContext) {
        ctx.set(viewContext);
    }

    public static ViewContext current() {
        ViewContext viewContext = ctx.get();
        if (viewContext == null) {
            viewContext = new ViewContext();
            ctx.set(viewContext);
        }
        return viewContext;
    }

    public static void destroy() {
        ctx.remove();
    }

    public void addDirtySaleOrderCode(String saleOrderCode) {
        for (Map.Entry<String, Set<String>> e : viewToDirtySaleOrderCodesMap.entrySet()) {
            e.getValue().add(saleOrderCode);
        }
    }

    public Map<String, Set<String>> getDirtySaleOrderCodes() {
        return viewToDirtySaleOrderCodesMap;
    }

    public void removeSaleOrderCodes(Set<String> saleOrderCodes) {
        for (Map.Entry<String, Set<String>> entry : viewToDirtySaleOrderCodesMap.entrySet()) {
            entry.getValue().removeAll(saleOrderCodes);
        }
    }
}
