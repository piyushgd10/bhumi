/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Sep 19, 2012
 *  @author singla
 */
package com.uniware.core.utils;

import java.util.Date;

import com.unifier.core.utils.DateUtils;

/**
 * @author singla
 */
public class Constants {

    public static final long   LOGO_ALLOWED_SIZE                         = 307200;                                            // 300kb
    public static final String OBJECT_STORE_VIEW_PREPEND                 = "/object-store/view/";
    public static final String DB_CONNECTION_ID                          = "DB_CONNECTION_ID";
    public static final String DEFAULT_TOKEN_BASED_LOGIN_FILTER_URL      = "token_login_security_check";
    public static final String AUTH_REQUEST_PROCESSOR_FILTER_URL         = "auth_login_security_check";
    public static final String EXECUTE_JOBS_ON_HOST                      = "executeJobs";
    public static final String TRIGGER_JOBS_ON_HOST                      = "triggerJobs";
    public static final String DEFAULT                                   = "DEFAULT";

    public static final String SYSTEM_USER_EMAIL                         = "system@unicommerce.com";

    public static final String TOKEN_BASED_LOGIN                         = "token";
    public static final String SERVICE_TICKET_ID                         = "serviceTicketId";
    public static final String API_USERNAME                              = "apiUsername";
    public static final String API_PASSWORD                              = "apiPassword";

    public static final String CHANNEL_SHIPPING_LABEL_BUCKET_NAME        = "unicommerce-channel-shippinglabel";
    public static final String CHANNEL_SHIPPING_MANIFEST_BUCKET_NAME     = "unicommerce-channel-manifest";
    public static final String CHANNEL_RECONCILIATION_REPORT_BUCKET_NAME = "unicommerce-channel-reconciliation";
    public static final String TRACE_LOG_KEY                             = "TRACE_LOG_KEY";
    public static final String DATE_PATTERN                              = "yyyy-MM-dd";
    public static final String TRACE_LOG_KEY_VALUE                       = "trace.log.key.value";
    public static final String GST_SERVICE_CHARGE_TAX_CLASS_CODE         = "GST_SERVICE_CHARGE";
    public static final String AGEING_DATE_PATTERN                       = "yyyy-MM-dd";
    public static final String FROM_CATEGORY                             = "from cat";
    public static final Date   ITEM_SKU_OR_NONE_TRACEABILITY_AGEING_DATE = DateUtils.stringToDate("1970-01-01", "yyyy-MM-dd");

    public static enum EmailTemplateType {
        PURCHASE_ORDER_APPROVAL_EMAIL,
        QC_REJECTION_REPORT,
        SHIPPING_MANIFEST,
        PURCHASE_ORDER_CLOSURE_EMAIL,
        RETURN_MANIFEST,
        EXPORT_INVOICE,
        PACKAGE_DISPATCHED,
        PENDING_PURCHASE_ORDERS,
        OUTBOUND_GATEPASS,
        CHANNEL_SYNC_FAILED_NOTIFICATION,
        PENDING_SALE_ORDER_ITEMS,
        CHANNEL_INVENTORY_SYNC_FAILED_NOTIFICATION,
        CREATE_ASN_NOTIFICATION,
        GO_LIVE_NOTIFICATION,
        PENDING_SALE_ORDERS_EMAIL,
        PENDING_PICKUP_ORDERS_EMAIL
    }

}
