/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 24-Apr-2014
 *  @author parijat
 */
package com.uniware.core.utils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.unifier.core.api.audit.AuditItem;
import com.unifier.core.api.audit.AuditItemField;

/**
 * Context for auditing. This will be on current executing thread. Method level auditing will have activity log
 * corresponding to each methodName.
 * 
 * @author parijat
 */
public class AuditContext {
    private static ThreadLocal<AuditContext> ctx                                = new ThreadLocal<AuditContext>();

    /*private String                           tenantCode;
    private String                           facilityCode;
    private String                           uniwareUserName;
    private String                           apiUserName;*/
    private AuditContext                     parentContext                      = null;
    private String                           belongsToMethod;
    private Map<String, List<AuditItemField>> methodVarsMap                = new HashMap<String, List<AuditItemField>>();
    private Map<String, AuditItem>           entityIdentifierToAuditItems       = new HashMap<String, AuditItem>();
    private boolean                          auditEnabled                       = false;
    private String                            contextIdentifier;

    public AuditContext() {
    }

    public AuditContext(AuditContext parentContext) {
        this.parentContext = parentContext;
        auditEnabled = true;
    }

    public static AuditContext current() {
        AuditContext context = ctx.get();
        if (context == null) {
            context = new AuditContext();
            ctx.set(context);
        }
        return context;
    }

    public static void setAuditContext(AuditContext context) {
        ctx.set(context);
    }

    public static void destory() {
        ctx.remove();
    }

    /**
     * @return the parentContext
     */
    public AuditContext getParentContext() {
        return parentContext;
    }

    /**
     * @param parentContext the parentContext to set
     */
    public void setParentContext(AuditContext parentContext) {
        this.parentContext = parentContext;
    }

    /**
     * @return the belongsToMethod
     */
    public String getBelongsToMethod() {
        return belongsToMethod;
    }

    /**
     * @param belongsToMethod the belongsToMethod to set
     */
    public void setBelongsToMethod(String belongsToMethod) {
        this.belongsToMethod = belongsToMethod;
    }

    /**
     * @return the methodVarsMap
     */
    public Map<String, List<AuditItemField>> getMethodVarsMap() {
        return methodVarsMap;
    }

    /**
     * @param methodVarsMap the methodVarsMap to set
     */
    public void setMethodVarsMap(Map<String, List<AuditItemField>> methodVarsMap) {
        this.methodVarsMap = methodVarsMap;
    }

    /**
     * @return the entityIdentifierToAuditItems
     */
    public Map<String, AuditItem> getEntityIdentifierToAuditItems() {
        return entityIdentifierToAuditItems;
    }

    /**
     * @param entityIdentifierToAuditItems the entityIdentifierToAuditItems to set
     */
    public void setEntityIdentifierToAuditItems(Map<String, AuditItem> entityIdentifierToAuditItems) {
        this.entityIdentifierToAuditItems = entityIdentifierToAuditItems;
    }

    /**
     * @return the auditEnabled
     */
    public boolean isAuditEnabled() {
        return auditEnabled;
    }

    /**
     * @param auditEnabled the auditEnabled to set
     */
    public void setAuditEnabled(boolean auditEnabled) {
        this.auditEnabled = auditEnabled;
    }

    /**
     * @return the contextIdentifier
     */
    public String getContextIdentifier() {
        return contextIdentifier;
    }

    /**
     * @param contextIdentifier the contextIdentifier to set
     */
    public void setContextIdentifier(String contextIdentifier) {
        this.contextIdentifier = contextIdentifier;
    }

}
