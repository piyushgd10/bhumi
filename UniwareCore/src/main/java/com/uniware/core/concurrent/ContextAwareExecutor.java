/*
 *  Copyright 2013 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 14-Jun-2013
 *  @author sunny
 */
package com.uniware.core.concurrent;

import com.codahale.metrics.Gauge;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.SharedMetricRegistries;
import com.uniware.core.utils.UserContext;

import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class ContextAwareExecutor extends ThreadPoolExecutor {

    private static final String SERVICE_METRICS         = "service-metrics";
    private static final String CONTEXT_AWARE_EXECUTOR  = "ContextAwareExecutor";
    private static final String GET_THREAD_POOL_SIZE    = "threadPoolSize";
    private static final String GET_ACTIVE_THREAD_COUNT = "activeThreadCount";
    private static final String GET_QUEUE_SIZE          = "queueSize";

    private final String poolName;
    private final MetricRegistry serviceMetrics = SharedMetricRegistries.getOrCreate(SERVICE_METRICS);

    public ContextAwareExecutor(String poolName, ThreadPoolConfig config) {
        super(config.getCorePoolSize(), config.getMaxPoolSize(), config.getKeepAliveTimeInSec(), TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>(config.getQueueSize()),
                Executors.defaultThreadFactory());
        this.poolName = poolName;
        // Register gauge to log statistics of ThreadPoolExecutor in service-metrics registry
        registerGauge();
    }

    @Override
    public void execute(Runnable command) {
        super.execute(new ContextAwareRunnable(poolName, command));
    }

    @Override
    protected void afterExecute(Runnable r, Throwable t) {
        final Thread currThread = Thread.currentThread();
        //set name to id after execution
        currThread.setName(String.valueOf(currThread.getId()));
    }

    /**
     * Registers the Gauge Metrics for statistics of ThreadPoolExecutor like poolSize,
     * active threads count and Queue Size.
     */
    private void registerGauge() {
        serviceMetrics.register(MetricRegistry.name(CONTEXT_AWARE_EXECUTOR, poolName, GET_THREAD_POOL_SIZE),
                new Gauge<Integer>() {@Override public Integer getValue() {return getPoolSize();}});
        serviceMetrics.register(MetricRegistry.name(CONTEXT_AWARE_EXECUTOR, poolName, GET_ACTIVE_THREAD_COUNT),
                new Gauge<Integer>() {@Override public Integer getValue() {return getActiveCount();}});
        serviceMetrics.register(MetricRegistry.name(CONTEXT_AWARE_EXECUTOR, poolName, GET_QUEUE_SIZE),
                new Gauge<Integer>() {@Override public Integer getValue() {return getQueue().size();}});

    }



}