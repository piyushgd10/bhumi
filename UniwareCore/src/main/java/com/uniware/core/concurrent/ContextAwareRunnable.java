/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 18-Apr-2013
 *  @author unicom
 */
package com.uniware.core.concurrent;

import com.unifier.core.utils.ThreadContextUtils;
import com.uniware.core.utils.UserContext;
import com.uniware.core.utils.ViewContext;

/**
 * @author Sunny
 */
public class ContextAwareRunnable implements Runnable {

    private final UserContext userContext;
    private final ViewContext viewContext;
    private final Runnable    runnable;
    private String            poolName;

    public ContextAwareRunnable(String poolName, Runnable runnable) {
        this.userContext = new UserContext(UserContext.current());
        this.viewContext = new ViewContext(ViewContext.current());
        this.runnable = runnable;
        this.poolName = poolName;
    }

    @Override
    public void run() {
        UserContext.setUserContext(userContext);
        ViewContext.setViewContext(viewContext);
        ThreadContextUtils.setThreadMetadata(poolName, userContext);
        Thread.currentThread().setName(new StringBuilder(poolName).append('-').append(Thread.currentThread().getId()).append(':').append(userContext.getThreadInfo()).toString());
        runnable.run();
    }

    @Override
    public String toString() {
        return runnable.toString();
    }

}
