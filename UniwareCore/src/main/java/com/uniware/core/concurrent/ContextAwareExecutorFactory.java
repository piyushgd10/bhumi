/*
 *  Copyright 2012 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 28-Nov-2012
 *  @author sunny
 */
package com.uniware.core.concurrent;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.springframework.stereotype.Component;

import com.unifier.core.cache.CacheManager;
import com.unifier.core.utils.JsonUtils;
import com.unifier.core.utils.StringUtils;
import com.uniware.core.cache.EnvironmentPropertiesCache;

@Component("contextAwareExecutorFactory")
public class ContextAwareExecutorFactory {

    private Map<String, ContextAwareExecutor> executors = new ConcurrentHashMap<String, ContextAwareExecutor>();

    private Lock                              lock      = new ReentrantLock();

    public ContextAwareExecutor getExecutor(String name) {
        ContextAwareExecutor executor = executors.get(name);
        if (executor == null) {
            try {
                lock.lock();
                executor = executors.get(name);
                if (executor == null) {
                    String threadPoolConfigJson = CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class).getThreadPoolConfig(name);
                    ThreadPoolConfig config;
                    if (StringUtils.isNotBlank(threadPoolConfigJson)) {
                        config = JsonUtils.stringToJson(threadPoolConfigJson, ThreadPoolConfig.class);
                    } else {
                        config = new ThreadPoolConfig();
                    }
                    executor = new ContextAwareExecutor(name, config);
                    executors.put(name, executor);
                }
            } finally {
                lock.unlock();
            }
        }
        return executor;
    }

    public ContextAwareExecutor getExecutor(String name, int maxThreadCount) {
        ContextAwareExecutor executor = executors.get(name);
        if (executor == null) {
            try {
                lock.lock();
                executor = executors.get(name);
                if (executor == null) {
                    String threadPoolConfigJson = CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class).getThreadPoolConfig(name);
                    ThreadPoolConfig config;
                    if (StringUtils.isNotBlank(threadPoolConfigJson)) {
                        config = JsonUtils.stringToJson(threadPoolConfigJson, ThreadPoolConfig.class);
                    } else {
                        config = new ThreadPoolConfig();
                    }
                    config.setMaxPoolSize(maxThreadCount);
                    executor = new ContextAwareExecutor(name, config);
                    executors.put(name, executor);
                }
            } finally {
                lock.unlock();
            }
        }
        return executor;
    }

    public void resetExecutors() {
        for (ContextAwareExecutor executor : executors.values()) {
            executor.shutdown();
        }
        executors.clear();
    }
}
