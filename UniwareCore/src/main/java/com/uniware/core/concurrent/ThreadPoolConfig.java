/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 12, 2011
 *  @author Sunny
 */
package com.uniware.core.concurrent;

import com.google.gson.Gson;

public class ThreadPoolConfig {

    private static final int  DEFAULT_CORE_POOL_SIZE  = 2;
    private static final int  DEFAULT_MAX_POOL_SIZE   = 5;
    private static final long DEFAULT_KEEP_ALIVE_TIME = 60;
    private static final int  DEFAULT_QUEUE_SIZE      = Integer.MAX_VALUE;

    private int               corePoolSize;
    private int               maxPoolSize;
    private long              keepAliveTimeInSec;
    private int               queueSize;

    public ThreadPoolConfig() {
        this.corePoolSize = DEFAULT_CORE_POOL_SIZE;
        this.maxPoolSize = DEFAULT_MAX_POOL_SIZE;
        this.keepAliveTimeInSec = DEFAULT_KEEP_ALIVE_TIME;
        this.queueSize = DEFAULT_QUEUE_SIZE;
    }

    public ThreadPoolConfig(int corePoolSize, int maxPoolSize, long keepAliveTimeInSec, int queueSize) {
        this.corePoolSize = corePoolSize;
        this.maxPoolSize = maxPoolSize;
        this.keepAliveTimeInSec = keepAliveTimeInSec;
        this.queueSize = queueSize;
    }

    public int getCorePoolSize() {
        return corePoolSize;
    }

    public void setCorePoolSize(int corePoolSize) {
        this.corePoolSize = corePoolSize;
    }

    public int getMaxPoolSize() {
        return maxPoolSize;
    }

    public void setMaxPoolSize(int maxPoolSize) {
        this.maxPoolSize = maxPoolSize;
    }

    public long getKeepAliveTimeInSec() {
        return keepAliveTimeInSec;
    }

    public void setKeepAliveTimeInSec(long keepAliveTimeInSec) {
        this.keepAliveTimeInSec = keepAliveTimeInSec;
    }

    public int getQueueSize() {
        return queueSize;
    }

    public void setQueueSize(int queueSize) {
        this.queueSize = queueSize;
    }

    public static void main(String[] args) {
        ThreadPoolConfig t = new ThreadPoolConfig();
        System.out.println(new Gson().toJson(t));
    }
}
