/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 12, 2011
 *  @author singla
 */
package com.uniware.core.cache;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.google.gson.Gson;
import com.unifier.core.annotation.Cache;
import com.unifier.core.annotation.Level;
import com.unifier.core.cache.ICache;
import com.unifier.core.cache.data.manager.ICacheDataManager;
import com.uniware.core.entity.Facility;
import com.uniware.core.entity.FacilityAllocationRule;
import com.uniware.core.entity.FacilityProfile;
import com.uniware.core.utils.UserContext;

/**
 * @author singla
 */
@Cache(type = "facilityCache", level = Level.TENANT)
public class FacilityCache implements ICache {

    private final Map<String, Facility>        codeToFacilities                  = new HashMap<String, Facility>();
    private final Map<String, Facility>        alternateCodeToFacilities         = new HashMap<String, Facility>();
    private final Map<String, FacilityProfile> codeToFacilitieProfiles           = new HashMap<String, FacilityProfile>();
    private final Map<Integer, Facility>       idToFacilities                    = new HashMap<Integer, Facility>();
    private final List<Facility>               facilities                        = new ArrayList<Facility>();
    private final List<Integer>                facilityIds                       = new ArrayList<Integer>();
    private final List<String>                 facilityCodes                     = new ArrayList<String>();
    private final List<FacilityAllocationRule> facilityAllocationRules           = new ArrayList<FacilityAllocationRule>();
    private final List<Facility>               dropshipFacilities                = new ArrayList<Facility>();
    private final List<Facility>               stores                            = new ArrayList<Facility>();
    private final List<Integer>                facilitiesForChannelInventorySync = new ArrayList<>();
    private final List<Facility>               nonThirdPartyFacilities           = new ArrayList<>();
    private String                             facilitiesJson;
    private String                             allFacilitiesJson;

    @Autowired
    private transient ICacheDataManager        cacheDataManager;

    public Facility getFacilityByCode(String facilityCode) {
        return codeToFacilities.get(facilityCode.toUpperCase()) != null ? codeToFacilities.get(facilityCode.toUpperCase())
                : alternateCodeToFacilities.get(facilityCode.toUpperCase());
    }

    public Facility getFacilityById(Integer facilityId) {
        return idToFacilities.get(facilityId);
    }

    private void addFacilityProfile(FacilityProfile facilityProfile) {
        codeToFacilitieProfiles.put(facilityProfile.getFacilityCode(), facilityProfile);
    }

    private void addFacility(Facility facility) {
        codeToFacilities.put(facility.getCode().toUpperCase(), facility);
        if (facility.getAlternateCode() != null) {
            alternateCodeToFacilities.put(facility.getAlternateCode().toUpperCase(), facility);
        }
        idToFacilities.put(facility.getId(), facility);
        if (!facility.isInventorySynDisabled()) {
            facilitiesForChannelInventorySync.add(facility.getId());
        }
        if (facility.isEnabled()) {
            facilities.add(facility);
            facilityIds.add(facility.getId());
            facilityCodes.add(facility.getCode());
            if (Facility.Type.DROPSHIP.name().equals(facility.getType())) {
                dropshipFacilities.add(facility);
            } else if (Facility.Type.STORE.name().equals(facility.getType())) {
                stores.add(facility);
            }
            if (!Facility.Type.THIRD_PARTY_WAREHOUSE.name().equals(facility.getType())) {
                nonThirdPartyFacilities.add(facility);
            }
        }
    }

    public void freeze() {
        Map<String, String> facilityMap = new HashMap<String, String>();
        Map<String, String> allfacilityMap = new HashMap<String, String>();
        for (Facility facility : facilities) {
            facilityMap.put(facility.getCode(), facility.getDisplayName());
        }
        for (Map.Entry<String, Facility> entry : codeToFacilities.entrySet()) {
            allfacilityMap.put(entry.getKey(), entry.getValue().getDisplayName());
        }
        this.allFacilitiesJson = new Gson().toJson(allfacilityMap);
        this.facilitiesJson = new Gson().toJson(facilityMap);
    }

    public Facility getCurrentFacility() {
        return getFacilityById(UserContext.current().getFacilityId());
    }

    public String getCurrentFacilityCode() {
        Integer facilityId = UserContext.current().getFacilityId();
        if (facilityId != null) {
            return getFacilityById(facilityId).getCode();
        }
        return null;
    }

    public FacilityProfile getFacilityProfileByCode(String facilityCode) {
        return codeToFacilitieProfiles.get(facilityCode);
    }

    public List<Facility> getFacilities() {
        return this.facilities;
    }

    public List<Facility> getNonThirdPartyFacilities() {
        return this.nonThirdPartyFacilities;
    }

    public List<String> getFacilityCodes() {
        return this.facilityCodes;
    }

    public List<Integer> getFacilityIds() {
        return this.facilityIds;
    }

    public List<Integer> getFacilityIds(List<String> facilityCodes) {
        List<Integer> facilityIds = new ArrayList<Integer>();
        for (String facilityCode : facilityCodes) {
            Facility facility = getFacilityByCode(facilityCode);
            if (facility != null) {
                facilityIds.add(facility.getId());
            }
        }
        return facilityIds;
    }

    public void addFacilityAllocationRule(FacilityAllocationRule facilityAllocationRule) {
        facilityAllocationRules.add(facilityAllocationRule);
    }

    public List<FacilityAllocationRule> getFacilityAllocationRules() {
        return this.facilityAllocationRules;
    }

    public boolean hasDropshipFacilities() {
        return dropshipFacilities.size() > 0;
    }

    public List<Integer> getFacilitiesForChannelInventorySync() {
        return facilitiesForChannelInventorySync;
    }

    public List<Facility> getDropshipFacilities() {
        return this.dropshipFacilities;
    }

    public List<Facility> getStores() {
        return this.stores;
    }

    public String getFacilitiesJson() {
        return facilitiesJson;
    }

    public void setFacilitiesJson(String facilitiesJson) {
        this.facilitiesJson = facilitiesJson;
    }

    public String getAllFacilitiesJson() {
        return allFacilitiesJson;
    }

    public void setAllFacilitiesJson(String allFacilitiesJson) {
        this.allFacilitiesJson = allFacilitiesJson;
    }

    @Override
    public void load() {
        List<Facility> facilities = cacheDataManager.getFacilities();
        for (Facility facility : facilities) {
            addFacility(facility);
            if (Facility.Type.STORE.name().equals(facility.getType())) {
                FacilityProfile facilityProfile = cacheDataManager.getFacilityProfileByCode(facility.getCode());
                if (facilityProfile != null) {
                    addFacilityProfile(facilityProfile);
                }
            }
        }
        List<FacilityAllocationRule> facilityAllocationRules = cacheDataManager.getFacilityAllocationRules();
        for (FacilityAllocationRule facilityAllocationRule : facilityAllocationRules) {
            addFacilityAllocationRule(facilityAllocationRule);
        }
    }

}
