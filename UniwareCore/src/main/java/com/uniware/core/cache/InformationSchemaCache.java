/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 10-Feb-2014
 *  @author harsh
 */
package com.uniware.core.cache;

import com.unifier.core.annotation.Cache;
import com.unifier.core.annotation.Level;
import com.unifier.core.cache.ICache;
import com.unifier.core.cache.data.manager.ICacheDataManager;
import com.uniware.core.api.metadata.InformationSchemaDTO;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The InformationSchemaCache class keeps mappings of all tables in database to List of their columns.
 *
 * @author harsh
 */
@Cache(type = "informationschemacache", level = Level.GLOBAL, eager = true)
public class InformationSchemaCache implements ICache {

    @Autowired
    private transient ICacheDataManager cacheDataManager;

    private Map<String, List<String>> tableMetadata = new HashMap<>();

    @Override
    public void load() {
        for (InformationSchemaDTO obj : cacheDataManager.getInformationSchema()) {
            String table = obj.getTable().toLowerCase();
            List<String> columns = tableMetadata.get(table);
            if (columns == null) {
                columns = new ArrayList<String>();
                tableMetadata.put(table, columns);
            }
            columns.add(obj.getColumn());
        }
    }

    public List<String> getColumns(String table) {
        return tableMetadata.get(table.toLowerCase());
    }

}
