/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 12, 2011
 *  @author singla
 */
package com.uniware.core.cache;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.net.util.SubnetUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.unifier.core.annotation.Cache;
import com.unifier.core.annotation.Level;
import com.unifier.core.cache.ICache;
import com.unifier.core.cache.data.manager.ICacheDataManager;
import com.unifier.core.utils.CollectionUtils;
import com.unifier.core.utils.StringUtils;
import com.uniware.core.entity.Tenant;
import com.uniware.core.utils.UserContext;

/**
 * @author singla
 */
@Cache(type = "tenantCache", level = Level.GLOBAL, eager = true)
public class TenantCache implements ICache {

    private static final Logger            LOG                = LoggerFactory.getLogger(TenantCache.class);

    private final Map<String, Tenant>      codeToActiveTenants      = new HashMap<String, Tenant>();
    private final Map<String, Tenant>      accessUrlToActiveTenants = new HashMap<String, Tenant>();
    private final Map<String, Set<String>> accessIPWhitelist        = new HashMap<>();
    private final List<Tenant>             activeTenants            = new ArrayList<Tenant>();
    private final Map<String, Tenant>      accessUrlToTenants       = new HashMap<String, Tenant>(); // contains mapping for tenants with all status codes
    private final Map<String, Tenant>      codeToTenants            = new HashMap<String, Tenant>();// contains mapping for tenants with all status codes


    @Autowired
    private transient ICacheDataManager cacheDataManager;

    private void addTenant(Tenant tenant) {
        accessUrlToTenants.put(tenant.getAccessUrl().toLowerCase(), tenant);
        codeToTenants.put(tenant.getCode(), tenant);
        if (tenant.getStatusCode().equals(Tenant.StatusCode.ACTIVE)) {
            codeToActiveTenants.put(tenant.getCode().toLowerCase(), tenant);
            accessUrlToActiveTenants.put(tenant.getAccessUrl().toLowerCase(), tenant);
            activeTenants.add(tenant);
        }
        if (StringUtils.isNotBlank(tenant.getAccessIpWhitelistString())) {
            Set<String> accessIPs = new HashSet<>();
            for (String accessIP : tenant.getAccessIpWhitelistString().split(",")) {
                if (accessIP.indexOf('/') != -1) {
                    accessIPs.addAll(getSubnetAddresses(accessIP));
                } else {
                    accessIPs.add(accessIP);
                }
            }
            accessIPWhitelist.put(tenant.getCode(), accessIPs);
            LOG.info("Restricted access to tenant: {}, IP LIST:{}", tenant.getCode(), accessIPs);
        }
    }

    public Tenant getActiveTenantByAccessUrl(String accessUrl) {
        return accessUrlToActiveTenants.get(accessUrl.toLowerCase());
    }

    public Tenant getActiveTenantByCode(String code) {
        return codeToActiveTenants.get(code.toLowerCase());
    }

    public List<Tenant> getActiveTenants() {
        return this.activeTenants;
    }

    public Tenant getCurrentTenant() {
        return codeToActiveTenants.get(UserContext.current().getTenant().getCode().toLowerCase());
    }

    public Set<String> getAccessIPWhitelist(String tenantCode) {
        return accessIPWhitelist.get(tenantCode);
    }

    public Tenant getTenantByAccessUrl(String accessUrl) {
        return accessUrlToTenants.get(accessUrl.toLowerCase());
    }

    public Tenant getTenantByCode(String tenantCode) {
        return codeToTenants.get(tenantCode);
    }

    @Override
    public void load() {
        for (Tenant tenant : cacheDataManager.getAllTenants()) {
            addTenant(tenant);
        }
    }

    private static Set<String> getSubnetAddresses(String cidrNotation) {
        try {
            return CollectionUtils.asSet(new SubnetUtils(cidrNotation).getInfo().getAllAddresses());
        } catch (IllegalArgumentException e) {
            LOG.error("Exception while parsing IP list:" + cidrNotation, e);
            return Collections.emptySet();
        }
    }
}
