/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 12, 2011
 *  @author singla
 */
package com.uniware.core.cache;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.unifier.core.annotation.Cache;
import com.unifier.core.annotation.Level;
import com.unifier.core.cache.ICache;
import com.unifier.core.cache.data.manager.IAppPropertyManager;
import com.unifier.core.cache.data.manager.ICacheDataManager;
import com.unifier.core.entity.EnvironmentProperty;
import com.unifier.core.utils.StringUtils;
import com.uniware.core.utils.UserContext;

//import com.unifier.core.jms.KafkaTopic;

/**
 * @author singla
 */
@Cache(type = "environmentCache", level = Level.GLOBAL, eager = true)
public class EnvironmentPropertiesCache implements ICache {

    public static final String            APP_ROOT_PATH                                      = "app.root.path";
    public static final String            APP_CONTEXT_PATH                                   = "app.context.path";
    public static final String            APP_CONTENT_PATH                                   = "app.content.path";
    public static final String            ACCOUNTS_SERVER_URL                                = "accounts.server.url";
    public static final String            AUTH_SERVER_URL                                    = "auth.server.url";
    public static final String            TASKS_EXPORT_DIRECTORY_PATH                        = "tasks.export.directory.path";
    public static final String            TASKS_EXPORT_SERVER_URL                            = "tasks.export.server.url";
    public static final String            TASKS_IMPORT_DIRECTORY_PATH                        = "tasks.import.directory.path";
    public static final String            TASKS_IMPORT_SERVER_URL                            = "tasks.import.server.url";
    public static final String            DOCUMENT_SERVICE_USERNAME                          = "document.service.username";
    public static final String            DOCUMENT_SERVICE_PASSWORD                          = "document.service.password";
    public static final String            DOCUMENT_SERVICE_URL                               = "document.service.url";
    public static final String            TENANT_WITH_DUMMY_ORDER                            = "tenant.with.dummy.order";
    public static final String            TEMPORARY_FILE_PATH                                = "temporary.file.path";
    public static final String            STATIC_CONTENT_SERVER_URL                          = "static.content.server.url";
    public static final String            SYSTEM_NOTIFICATIONS_ENABLED                       = "system.notifications.enabled";
    public static final String            THREAD_POOL_CONFIG                                 = "thread.pool.config";
    public static final String            GOOGLE_ANALYTICS_CODE                              = "google.analytics.code";
    public static final String            SOAP_REQUEST_LOGGING_ENABLED                       = "soap.request.logging.enabled";
    public static final String            TENANT_SPECIFIC_MONGO_ENABLED                      = "tenant.specific.mongo.enabled";
    public static final String            ACTIVITY_LOGGING_ENABLED                           = "activity.logging.enabled";
    public static final String            AUDIT_LOGGING_ENABLED                              = "audit.logging.enabled";
    public static final String            EXPORT_VIA_REPLICATION_ENABLED                     = "export.via.replication.enabled";
    public static final String            DATATABLE_VIA_REPLICATION_ENABLED                  = "datatable.via.replication.enabled";
    public static final String            DASHBOARD_VIA_REPLICATION_ENABLED                  = "dashboard.via.replication.enabled";
    public static final String            MAX_NUMBER_OF_ITEMS_IN_MANIFEST                    = "max.number.of.items.in.manifest";
    public static final String            MAX_CHANNEL_ITEM_TYPE_MAPPING_ALLOWED              = "max.channel.item.type.mapping.allowed";
    public static final String            DEFAULT_LOCK_WAIT_TIMEOUT_SECONDS                  = "default.lock.wait.timeout.seconds";
    public static final String            CHECK_SNAPSHOT_IN_COMMON_MONGO_ENABLED             = "check.snapshot.in.common.mongo.enabled";
    public static final String            REFERRER_BROADCAST_DATE                            = "referrer.broadcast.date";
    public static final String            TRANSACTION_MONITORING_ENABLED                     = "transaction.monitoring.enabled";
    public static final String            API_STATISTICS_DISABLED                            = "api.statistics.disabled";
    public static final String            CUSTOMER_CENTRAL_URL                               = "customer.central.url";
    public static final String            UNIDESK_PROPERTIES                                 = "unidesk.properties";
    public static final String            UNIDESK_URL                                        = "unidesk.url";
    public static final String            IMPORT_JOB_LISTENER_COUNT                          = "import.job.listener.count";
    public static final String            EXPORT_JOB_LISTENER_COUNT                          = "export.job.listener.count";
    public static final String            RECURRENT_JOB_LISTENER_COUNT                       = "recurrent.job.listener.count";
    public static final String            SYNC_CHANNEL_ORDERS_LISTENER_COUNT                 = "sync.channel.orders.listener.count";
    public static final String            SYNC_CHANNEL_INVENTORY_LISTENER_COUNT              = "sync.channel.inventory.listener.count";
    public static final String            SYNC_CHANNEL_CATALOG_LISTENER_COUNT                = "sync.channel.catalog.listener.count";
    public static final String            SYNC_CHANNEL_WAREHOUSE_INVENTORY_LISTENER_COUNT    = "sync.channel.warehouse.inventory.listener.count";
    public static final String            SYNC_CHANNEL_RECONCILIATION_INVOICE_LISTENER_COUNT = "sync.channel.reconciliation.invoice.listener.count";
    public static final String            SYNC_CHANNEL_PRICE_LISTENER_COUNT                  = "sync.channel.price.listener.count";

    public static final String            METRICS_SERVER_HOST                                = "metrics.server.host";
    public static final String            METRICS_SERVER_PORT                                = "metrics.server.port";
    public static final String            GST_DATE                                           = "gst.date";

    public static final String            RESET_INVOICE_PREFIX_NOW                           = "reset.invoice.prefix.now";
    public static final String            APP_MAJOR_VERSION                                  = "app.major.version";
    public static final String            APP_MINOR_VERSION                                  = "app.minor.version";

    public static final String            TRACE_LOGGING_SERVER_URL                           = "trace.logging.server.url";

    private final Map<String, String>     properties                                         = new HashMap<>();

    @Autowired
    private transient ICacheDataManager   cacheDataManager;

    @Autowired
    private transient IAppPropertyManager appPropertyManager;

    public String getLoggingServerUrl() {
        return getProperty(TRACE_LOGGING_SERVER_URL);
    }

    private void addProperty(String name, String value) {
        properties.put(name, value);
    }

    public String getProperty(String name) {
        return properties.get(name);
    }

    public String getProperty(String name, String defaultValue) {
        String value = properties.get(name);
        return value != null ? value : defaultValue;
    }

    public String getAppRootPath() {
        return getProperty(APP_ROOT_PATH);
    }

    public boolean isSystemNotificationsEnabled() {
        return Boolean.parseBoolean(getProperty(SYSTEM_NOTIFICATIONS_ENABLED));
    }

    public boolean isApiStatisticsDisabled() {
        String property = getProperty(API_STATISTICS_DISABLED);
        return Boolean.parseBoolean(property);
    }

    public String getAccountsServerUrl() {
        return getProperty(ACCOUNTS_SERVER_URL);
    }

    public String getCustomerCentralUrl() {
        return getProperty(CUSTOMER_CENTRAL_URL);
    }

    public String getAuthServerUrl() {
        return getProperty(AUTH_SERVER_URL);
    }

    public String getExportDirectoryPath() {
        return getProperty(EnvironmentPropertiesCache.APP_ROOT_PATH) + "/static/files";
    }

    public String getImportDirectoryPath() {
        return getProperty(TASKS_IMPORT_DIRECTORY_PATH);
    }

    public String getImportLogDirectoryPath() {
        return getProperty(EnvironmentPropertiesCache.APP_ROOT_PATH) + "/static/files/import/logs";
    }

    public String getExportServerUrl() {
        return getAppContextPath() + "/files";
    }

    public String getImportLogServerUrl() {
        return getAppContextPath() + "/files/import/logs";
    }

    public String getAppContextPath() {
        return "https://" + UserContext.current().getTenant().getAccessUrl();
    }

    public String getAppContentPath() {
        return getProperty(APP_CONTENT_PATH);
    }

    public Boolean isResetInvoicePrefixNow() {
        return Boolean.parseBoolean(getProperty(RESET_INVOICE_PREFIX_NOW, "false"));
    }

    public String getDocumentServiceUsername() {
        return getProperty(DOCUMENT_SERVICE_USERNAME);
    }

    public String getDocumentServicePassword() {
        return getProperty(DOCUMENT_SERVICE_PASSWORD);
    }

    public String getDocumentServiceUrl() {
        return getProperty(DOCUMENT_SERVICE_URL);
    }

    public String getTemporaryFilePath() {
        return getProperty(TEMPORARY_FILE_PATH);
    }

    public String getStaticContentServerUrl() {
        return getProperty(STATIC_CONTENT_SERVER_URL);
    }

    public String getGoogleAnalyticsCode() {
        return getProperty(GOOGLE_ANALYTICS_CODE);
    }

    public String getThreadPoolConfig(String name) {
        return getProperty(THREAD_POOL_CONFIG + "." + name);
    }

    public String getTenantWithDummyOrder() {
        return getProperty(TENANT_WITH_DUMMY_ORDER);
    }

    public String getReferrerBroadcastDate() {
        return getProperty(REFERRER_BROADCAST_DATE);
    }

    public String getUnideskProperties() {
        return getProperty(UNIDESK_PROPERTIES);
    }

    public String getUnideskUrl() {
        return getProperty(UNIDESK_URL);
    }

    public boolean isSoapRequestLoggingEnabled() {
        return Boolean.parseBoolean(getProperty(SOAP_REQUEST_LOGGING_ENABLED));
    }

    public boolean isTenantSpecificMongoEnabled() {
        return Boolean.parseBoolean(getProperty(TENANT_SPECIFIC_MONGO_ENABLED));
    }

    public boolean isActivityLoggingEnabled() {
        return Boolean.parseBoolean(getProperty(ACTIVITY_LOGGING_ENABLED, "false"));
    }

    public boolean isAuditLoggingEnabled() {
        return Boolean.parseBoolean(getProperty(AUDIT_LOGGING_ENABLED, "false"));
    }

    public boolean isExportViaReplicationEnabled() {
        return Boolean.parseBoolean(getProperty(EXPORT_VIA_REPLICATION_ENABLED, "false"));
    }

    public boolean isDatatableViaReplicationEnabled() {
        return Boolean.parseBoolean(getProperty(DATATABLE_VIA_REPLICATION_ENABLED, "false"));
    }

    public boolean isDashboardViaReplicationEnabled() {
        return Boolean.parseBoolean(getProperty(DASHBOARD_VIA_REPLICATION_ENABLED, "false"));
    }

    public Integer getMaxNumberOfItemsInManifest() {
        return getProperty(MAX_NUMBER_OF_ITEMS_IN_MANIFEST) != null ? Integer.parseInt(getProperty(MAX_NUMBER_OF_ITEMS_IN_MANIFEST)) : 200;
    }

    public Integer getMaxChannelItemTypeMappingAllowed() {
        return getProperty(MAX_CHANNEL_ITEM_TYPE_MAPPING_ALLOWED) != null ? Integer.parseInt(getProperty(MAX_CHANNEL_ITEM_TYPE_MAPPING_ALLOWED)) : 500;
    }

    public int getImportJobListenerCount() {
        String importJobListenerCount = getProperty(IMPORT_JOB_LISTENER_COUNT);
        return StringUtils.isNotBlank(importJobListenerCount) ? Integer.parseInt(importJobListenerCount) : 5;
    }

    public int getExportJobListenerCount() {
        String exportJobListenerCount = getProperty(EXPORT_JOB_LISTENER_COUNT);
        return StringUtils.isNotBlank(exportJobListenerCount) ? Integer.parseInt(exportJobListenerCount) : 5;
    }

    public int getRecurrentJobListenerListenerCount() {
        String exportJobListenerCount = getProperty(RECURRENT_JOB_LISTENER_COUNT);
        return StringUtils.isNotBlank(exportJobListenerCount) ? Integer.parseInt(exportJobListenerCount) : 10;
    }

    public long getLockWaitTimeoutInSeconds() {
        String lockWaitTimeoutInSeconds = getProperty(DEFAULT_LOCK_WAIT_TIMEOUT_SECONDS);
        return StringUtils.isNotBlank(lockWaitTimeoutInSeconds) ? Long.parseLong(lockWaitTimeoutInSeconds) : 60;
    }

    public boolean isCheckSnapshotInCommonMongoEnabled() {
        return Boolean.parseBoolean(getProperty(CHECK_SNAPSHOT_IN_COMMON_MONGO_ENABLED, "true"));
    }

    public boolean isTransactionMonitoringEnabled() {
        return Boolean.parseBoolean(getProperty(TRANSACTION_MONITORING_ENABLED, "false"));
    }

    @Override
    public void load() {
        addProperty(APP_ROOT_PATH, appPropertyManager.getAppRootPath());
        List<EnvironmentProperty> properties = cacheDataManager.getEnvironmentProperties();
        for (EnvironmentProperty property : properties) {
            addProperty(property.getName(), property.getValue());
        }
    }

    public int getSyncChannelOrdersListenerCount() {
        String syncChannelOrdersListenerCount = getProperty(SYNC_CHANNEL_ORDERS_LISTENER_COUNT);
        return StringUtils.isNotBlank(syncChannelOrdersListenerCount) ? Integer.parseInt(syncChannelOrdersListenerCount) : 5;
    }

    public int getSyncChannelInventoryListenerCount() {
        String syncChannelInventoryListenerCount = getProperty(SYNC_CHANNEL_INVENTORY_LISTENER_COUNT);
        return StringUtils.isNotBlank(syncChannelInventoryListenerCount) ? Integer.parseInt(syncChannelInventoryListenerCount) : 10;
    }

    public int getSyncChannelCatalogListenerCount() {
        String syncChannelCatalogListenerCount = getProperty(SYNC_CHANNEL_CATALOG_LISTENER_COUNT);
        return StringUtils.isNotBlank(syncChannelCatalogListenerCount) ? Integer.parseInt(syncChannelCatalogListenerCount) : 5;
    }

    public int getSyncChannelWarehouseInventoryListenerCount() {
        String syncChannelWarehouseInventoryListenerCount = getProperty(SYNC_CHANNEL_WAREHOUSE_INVENTORY_LISTENER_COUNT);
        return StringUtils.isNotBlank(syncChannelWarehouseInventoryListenerCount) ? Integer.parseInt(syncChannelWarehouseInventoryListenerCount) : 5;
    }

    public int getSyncChannelReconciliationInvoiceListenerCount() {
        String syncChannelCatalogListenerCount = getProperty(SYNC_CHANNEL_RECONCILIATION_INVOICE_LISTENER_COUNT);
        return StringUtils.isNotBlank(syncChannelCatalogListenerCount) ? Integer.parseInt(syncChannelCatalogListenerCount) : 5;
    }

    public int getSyncChannelPriceListenerCount() {
        String syncChannelCatalogListenerCount = getProperty(SYNC_CHANNEL_PRICE_LISTENER_COUNT);
        return StringUtils.isNotBlank(syncChannelCatalogListenerCount) ? Integer.parseInt(syncChannelCatalogListenerCount) : 5;
    }

    //    public int getNumberOfPartitions(KafkaTopic topic) {
    //        int numPartitions;
    //        switch (topic) {
    //            case IMPORT:
    //                numPartitions = getImportJobListenerCount();
    //                break;
    //            case EXPORT:
    //                numPartitions = getExportJobListenerCount();
    //                break;
    //            case CATALOG_SYNC:
    //                numPartitions = getSyncChannelCatalogListenerCount();
    //                break;
    //            case INVENTORY_SYNC:
    //                numPartitions = getSyncChannelInventoryListenerCount();
    //                break;
    //            case ORDER_SYNC:
    //                numPartitions = getSyncChannelOrdersListenerCount();
    //                break;
    //            case JOB:
    //                numPartitions = getRecurrentJobListenerListenerCount();
    //                break;
    //            case PRICE_SYNC:
    //                numPartitions = getSyncChannelPriceListenerCount();
    //                break;
    //            case TENANT:
    //                numPartitions = 1;
    //                break;
    //            default:
    //                numPartitions = 5;
    //                break;
    //        }
    //        return numPartitions;
    //    }

    public String getMetricsServerHost() {
        return getProperty(METRICS_SERVER_HOST);
    }

    public int getMetricsServerPort() {
        String metricsServerPort = getProperty(METRICS_SERVER_PORT);
        return StringUtils.isNotBlank(metricsServerPort) ? Integer.parseInt(metricsServerPort) : 2003;
    }

    public String getGSTDate() {
        return properties.get(GST_DATE) != null ? properties.get(GST_DATE) : "2017-07-01";
    }

    public double getUniwareAppCurrentMinorVersion() {
        return Double.parseDouble(getProperty(APP_MINOR_VERSION));
    }

    public int getUniwareAppCurrentMajorVersion() {
        return Integer.parseInt(getProperty(APP_MAJOR_VERSION));
    }

}
