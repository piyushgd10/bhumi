/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 *  @version     1.0, Jan 20, 2012
 *  @author singla
 */
package com.uniware.core.cache;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;

import com.google.gson.Gson;
import com.unifier.core.annotation.Cache;
import com.unifier.core.annotation.Level;
import com.unifier.core.cache.ICache;
import com.unifier.core.cache.data.manager.ICacheDataManager;
import com.unifier.core.utils.StringUtils;
import com.uniware.core.entity.Country;
import com.uniware.core.entity.Location;
import com.uniware.core.entity.State;

/**
 * @author singla
 */
@Cache(type = "locationCache", level = Level.GLOBAL, eager = true)
public class LocationCache implements ICache {

    public static final String                          COUNTRY_CODE_INDIA           = "IN";
    private final       Map<String, Map<String, State>> countryToStateCodeToStates   = new HashMap<>();
    private final       Map<String, Map<String, State>> countryToStateNameToStates   = new HashMap<>();
    private final       Map<String, List<State>>        countryCodeToStates          = new HashMap<>();
    private final       Map<String, String>             countryCodeToStateJsons      = new HashMap<>();
    private final       Map<String, Country>            codeToCountries              = new HashMap<>();
    private final       Map<String, Country>            nameToCountries              = new HashMap<>();
    private final       List<Country>                   countries                    = new ArrayList<>();
    private final       Map<String, String>             countryCodeToNameMap         = new LinkedHashMap<>();
    private final       Map<String, Location>           pincodeToLocations           = new HashMap<>();
    private final       Map<String, Pattern>            countryCodeToPostcodePattern = new HashMap<>();
    private String countriesJson;

    @Autowired
    private transient ICacheDataManager cacheDataManager;

    private void addState(State state) {
        Map<String, State> stateByCode = countryToStateCodeToStates.get(state.getCountry().getCode().toUpperCase());
        if (stateByCode == null) {
            stateByCode = new HashMap<>();
            countryToStateCodeToStates.put(state.getCountry().getCode().toUpperCase(), stateByCode);
        }
        stateByCode.put(state.getIso2Code().toUpperCase(), state);
        if (StringUtils.isNotBlank(state.getAlternateStateCode())) {
            stateByCode.put(state.getAlternateStateCode().trim().toUpperCase(), state);
        }
        Map<String, State> stateByName = countryToStateNameToStates.get(state.getCountry().getCode().toUpperCase());
        if (stateByName == null) {
            stateByName = new HashMap<String, State>();
            countryToStateNameToStates.put(state.getCountry().getCode().toUpperCase(), stateByName);
        }
        stateByName.put(StringUtils.normalizeCacheKey(state.getName()), state);
        if (StringUtils.isNotBlank(state.getAlternateStateNames())) {
            for (String alternateName : state.getAlternateStateNames().split(",")) {
                alternateName = StringUtils.normalizeCacheKey(alternateName);
                if (!stateByName.containsKey(alternateName)) {
                    stateByName.put(alternateName, state);
                }
            }
        }
        List<State> states = countryCodeToStates.get(state.getCountry().getCode().toUpperCase());
        if (states == null) {
            states = new ArrayList<State>();
            countryCodeToStates.put(state.getCountry().getCode().toUpperCase(), states);
        }
        states.add(state);
    }

    private void addCountry(Country country) {
        codeToCountries.put(country.getCode().toUpperCase(), country);
        nameToCountries.put(StringUtils.normalizeCacheKey(country.getName()), country);
        countries.add(country);
        countryCodeToNameMap.put(country.getCode(), country.getName());
        if (country.getPostcodePattern() != null) {
            countryCodeToPostcodePattern.put(country.getCode(), Pattern.compile(country.getPostcodePattern()));
        }
    }

    private void addLocation(Location location) {
        pincodeToLocations.put(location.getPincode(), location);
    }

    public void freeze() {
        for (Map.Entry<String, List<State>> entry : countryCodeToStates.entrySet()) {
            Map<String, String> stateMap = new LinkedHashMap<String, String>();
            for (State state : entry.getValue()) {
                stateMap.put(state.getIso2Code(), state.getName());
            }
            this.countryCodeToStateJsons.put(entry.getKey(), new Gson().toJson(stateMap));
        }
        this.countriesJson = new Gson().toJson(countryCodeToNameMap);
    }

    /**
     * @return the stateJson
     */
    public String getStateJson() {
        return getStateJson(COUNTRY_CODE_INDIA);
    }

    public String getStateJson(String countryCode) {
        return this.countryCodeToStateJsons.get(countryCode.toUpperCase());
    }

    public State getStateByCode(String stateCode) {
        return getStateByCode(stateCode, COUNTRY_CODE_INDIA);
    }

    public State getStateByCode(String stateCode, String countryCode) {
        State state = null;
        Assert.notNull(stateCode);
        if (StringUtils.isBlank(countryCode)) {
            countryCode = COUNTRY_CODE_INDIA;
        }
        if (countryToStateCodeToStates.containsKey(countryCode.toUpperCase())) {
            state = countryToStateCodeToStates.get(countryCode.toUpperCase()).get(stateCode.toUpperCase());
            if (state == null) {
                state = countryToStateNameToStates.get(countryCode.toUpperCase()).get(StringUtils.normalizeCacheKey(stateCode));
            }
        }
        return state;
    }

    /**
     * @param countryCode
     * @return
     */
    public Country getCountryByCode(String countryCode) {
        Country country = codeToCountries.get(countryCode.toUpperCase());
        if (country == null) {
            country = nameToCountries.get(StringUtils.normalizeCacheKey(countryCode));
        }
        return country;
    }

    public Location getLocationByPincode(String pincode) {
        return pincodeToLocations.get(pincode);
    }

    public String getCountriesJson() {
        return countriesJson;
    }

    @Override
    public void load() {
        List<State> states = cacheDataManager.getStates();
        for (State state : states) {
            addState(state);
        }
        List<Country> countries = cacheDataManager.getCountries();
        for (Country country : countries) {
            addCountry(country);
        }

        List<Location> locations = cacheDataManager.getAllLocations();
        for (Location location : locations) {
            addLocation(location);
        }
    }

    public List<State> getStatesByCountryCode(String countryCode) {
        return countryCodeToStates.get(countryCode.toUpperCase());
    }

    public Pattern getPostcodePatternByCountryCode(String countryCode){
        return countryCodeToPostcodePattern.get(countryCode.toUpperCase());
    }

}
