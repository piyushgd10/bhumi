/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 12, 2011
 *  @author singla
 */
package com.uniware.core.cache;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.unifier.core.cache.CacheManager;
import com.unifier.core.entity.FacilityAccessResource;
import com.uniware.core.entity.Facility;
import org.springframework.beans.factory.annotation.Autowired;

import com.unifier.core.annotation.Cache;
import com.unifier.core.annotation.Level;
import com.unifier.core.api.advanced.RoleDTO;
import com.unifier.core.cache.ICache;
import com.unifier.core.cache.data.manager.ICacheDataManager;
import com.unifier.core.entity.AccessPattern;
import com.unifier.core.entity.AccessResource;
import com.unifier.core.entity.AccessResourceGroup;
import com.unifier.core.entity.Role;
import com.unifier.core.entity.RoleAccessResource;

/**
 * @author singla
 */
@Cache(type = "rolesCache", level = Level.TENANT, eager = true)
public class RolesCache implements ICache {

    private static final String                          IMPLICIT_ACCESS               = "IMPLICIT_ACCESS";
    private              Map<Facility.Type, Set<String>> facilityTypeToAccessResources = new HashMap<>();
    private              List<String>                    roles                         = new ArrayList<String>();
    private              List<RoleDTO>                   roleDTOs                      = new ArrayList<RoleDTO>();
    private final        Map<Integer, Role>              roleById                      = new HashMap<Integer, Role>();
    private final        Map<String, Role>               roleByCode                    = new HashMap<String, Role>();

    private final Map<String, AccessResource>       nameToAccessResource          = new HashMap<>();
    private final Map<String, AccessResource>       accessPatternToAccessResource = new HashMap<>();
    private final Map<Integer, AccessResourceGroup> idToAccessResourceGroups      = new HashMap<>();
    private final Map<String, Set<String>>          accessGroupToRoles            = new HashMap<>();

    private final Map<String, Set<String>>                     accessResourceToRoles               = new HashMap<>();
    private final Map<Facility.Type, Map<String, Set<String>>> facilityTypeToAccessResourceToRoles = new HashMap<>();

    private final Map<String, Set<String>>                     roleToAccessResources               = new HashMap<>();
    private final Map<Facility.Type, Map<String, Set<String>>> facilityTypeToRoleToAccessResources = new HashMap<>();

    private final Map<String, Set<String>>                     roleToAccessPatterns               = new HashMap<>();
    private final Map<Facility.Type, Map<String, Set<String>>> facilityTypeToRoleToAccessPatterns = new HashMap<>();

    private final Map<String, Set<String>>                     accessPatternToRoles               = new HashMap<>();
    private final Map<Facility.Type, Map<String, Set<String>>> facilityTypeToAccessPatternToRoles = new HashMap<>();

    @Autowired
    private transient ICacheDataManager cacheDataManager;

    private void addRole(Role role) {
        roles.add(role.getCode());
        roleById.put(role.getId(), role);
        roleByCode.put(role.getCode(), role);
        if (!role.isHidden()) {
            roleDTOs.add(new RoleDTO(role));
        }
    }

    private void addAccessResource(AccessResource accessResource, List<RoleAccessResource> rar) {
        nameToAccessResource.put(accessResource.getName(), accessResource);
        prepareAccessResourceToRoleMapping(accessResource, rar);
        for (Facility.Type faclityType : facilityTypeToAccessResources.keySet()) {
            if (IMPLICIT_ACCESS.equals(accessResource.getName()) || facilityTypeToAccessResources.get(faclityType).contains(accessResource.getName())) {
                prepareAccessResourceToRoleMapping(accessResource, rar, faclityType);
            }
        }
    }

    private void prepareAccessResourceToRoleMapping(AccessResource accessResource, List<RoleAccessResource> rar, Facility.Type facilityType) {
        // Access Resource to Roles
        Map<String, Set<String>> accessResourceToRoles = facilityTypeToAccessResourceToRoles.get(facilityType);
        if (accessResourceToRoles == null) {
            accessResourceToRoles = new HashMap<>();
            facilityTypeToAccessResourceToRoles.put(facilityType, accessResourceToRoles);
        }
        Set<String> accessResourcesRoles = new HashSet<>();
        accessResourceToRoles.put(accessResource.getName(), accessResourcesRoles);
        if (IMPLICIT_ACCESS.equals(accessResource.getName())) {
            accessResourcesRoles.addAll(roles);
        } else {
            for (RoleAccessResource roleAccessResource : rar) {
                if (roleAccessResource.isEnabled() && roleById.containsKey(roleAccessResource.getRole().getId())) {
                    accessResourcesRoles.add(roleById.get(roleAccessResource.getRole().getId()).getCode());
                }
            }
        }

        for (RoleAccessResource roleAccessResource : rar) {
            if (roleAccessResource.isEnabled() && roleById.containsKey(roleAccessResource.getRole().getId())) {
                String role = roleById.get(roleAccessResource.getRole().getId()).getCode();
                Map<String, Set<String>> roleToAccessResources = facilityTypeToRoleToAccessResources.get(facilityType);
                if (roleToAccessResources == null) {
                    roleToAccessResources = new HashMap<>();
                    facilityTypeToRoleToAccessResources.put(facilityType, roleToAccessResources);
                }
                Set<String> accessResources = roleToAccessResources.get(role);
                if (accessResources == null) {
                    accessResources = new HashSet<>();
                    accessResources.add(IMPLICIT_ACCESS);
                    roleToAccessResources.put(role, accessResources);
                }
                accessResources.add(accessResource.getName());

                Map<String, Set<String>> roleToAccessPatterns = facilityTypeToRoleToAccessPatterns.get(facilityType);
                if (roleToAccessPatterns == null) {
                    roleToAccessPatterns = new HashMap<>();
                    facilityTypeToRoleToAccessPatterns.put(facilityType, roleToAccessPatterns);
                }
                Set<String> roleAccessPatterns = roleToAccessPatterns.get(role);
                if (roleAccessPatterns == null) {
                    roleAccessPatterns = new HashSet<>();
                    roleToAccessPatterns.put(role, roleAccessPatterns);
                }
                for (AccessPattern accessPattern : accessResource.getAccessPatterns()) {
                    roleAccessPatterns.add(accessPattern.getUrlPattern());
                }
            }
        }

        for (AccessPattern accessPattern : accessResource.getAccessPatterns()) {
            Map<String, Set<String>> accessPatternToRoles = facilityTypeToAccessPatternToRoles.get(facilityType);
            if (accessPatternToRoles == null) {
                accessPatternToRoles = new HashMap<>();
                facilityTypeToAccessPatternToRoles.put(facilityType, accessPatternToRoles);
            }
            Set<String> accessPatternRoles = accessPatternToRoles.get(accessPattern.getUrlPattern());
            if (accessPatternRoles == null) {
                accessPatternRoles = new HashSet<>();
                accessPatternToRoles.put(accessPattern.getUrlPattern(), accessPatternRoles);
            }
            accessPatternRoles.addAll(accessResourcesRoles);
        }
    }

    private void prepareAccessResourceToRoleMapping(AccessResource accessResource, List<RoleAccessResource> rar) {
        // Access Resource to Roles
        Set<String> accessResourcesRoles = new HashSet<>();
        if (IMPLICIT_ACCESS.equals(accessResource.getName())) {
            accessResourcesRoles.addAll(roles);
        } else {
            for (RoleAccessResource roleAccessResource : rar) {
                if (roleAccessResource.isEnabled() && roleById.containsKey(roleAccessResource.getRole().getId())) {
                    accessResourcesRoles.add(roleById.get(roleAccessResource.getRole().getId()).getCode());
                }
            }
        }
        accessResourceToRoles.put(accessResource.getName(), accessResourcesRoles);

        // Role to Access Resources
        for (RoleAccessResource roleAccessResource : rar) {
            if (roleAccessResource.isEnabled() && roleById.containsKey(roleAccessResource.getRole().getId())) {
                String role = roleById.get(roleAccessResource.getRole().getId()).getCode();
                Set<String> accessResources = roleToAccessResources.get(role);
                if (accessResources == null) {
                    accessResources = new HashSet<>();
                    accessResources.add(IMPLICIT_ACCESS);
                    roleToAccessResources.put(role, accessResources);
                }
                accessResources.add(accessResource.getName());
                Set<String> roleAccessPatterns = roleToAccessPatterns.get(role);
                if (roleAccessPatterns == null) {
                    roleAccessPatterns = new HashSet<>();
                    roleToAccessPatterns.put(role, roleAccessPatterns);
                }
                for (AccessPattern accessPattern : accessResource.getAccessPatterns()) {
                    roleAccessPatterns.add(accessPattern.getUrlPattern());
                }
            }
        }

        if (accessResource.getAccessResourceGroup() != null) {
            String accessGroupName = idToAccessResourceGroups.get(accessResource.getAccessResourceGroup().getId()).getName();
            Set<String> accessGroupRoles = accessGroupToRoles.get(accessGroupName);
            if (accessGroupRoles == null) {
                accessGroupRoles = new HashSet<>();
                accessGroupToRoles.put(accessGroupName, accessGroupRoles);
            }
            accessGroupRoles.addAll(accessResourcesRoles);
        }
        for (AccessPattern accessPattern : accessResource.getAccessPatterns()) {
            accessPatternToAccessResource.put(accessPattern.getUrlPattern(), accessResource);
            Set<String> accessPatternRoles = accessPatternToRoles.get(accessPattern.getUrlPattern());
            if (accessPatternRoles == null) {
                accessPatternRoles = new HashSet<>();
                accessPatternToRoles.put(accessPattern.getUrlPattern(), accessPatternRoles);
            }
            accessPatternRoles.addAll(accessResourcesRoles);
        }
    }

    private void addAccessResourceGroup(AccessResourceGroup accessResourceGroup) {
        idToAccessResourceGroups.put(accessResourceGroup.getId(), accessResourceGroup);
    }

    public AccessResource getAccessResourceByName(String accessResourceName) {
        return nameToAccessResource.get(accessResourceName);
    }

    public Role getRole(int id) {
        return roleById.get(id);
    }

    public Role getRoleByCode(String code) {
        return roleByCode.get(code);
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    public List<RoleDTO> getRoleDTOs() {
        return roleDTOs;
    }

    public void setRoleDTOs(List<RoleDTO> roleDTOs) {
        this.roleDTOs = roleDTOs;
    }

    public Set<String> getRolesByAccessPattern(String accessPattern) {
        Facility currentFacility = CacheManager.getInstance().getCache(FacilityCache.class).getCurrentFacility();
        if (currentFacility != null) {
            Facility.Type facilityType = Facility.Type.valueOf(currentFacility.getType());
            return facilityTypeToAccessPatternToRoles.get(facilityType).get(accessPattern);
        }
        return accessPatternToRoles.get(accessPattern);
    }

    public Set<String> getAccessResourcesByRole(String role) {
        Facility currentFacility = CacheManager.getInstance().getCache(FacilityCache.class).getCurrentFacility();
        if (currentFacility != null) {
            Facility.Type facilityType = Facility.Type.valueOf(currentFacility.getType());
            return facilityTypeToRoleToAccessResources.get(facilityType).get(role);
        }
        return roleToAccessResources.get(role);
    }

    public Set<String> getAccessPatternsByRole(String role) {
        Facility currentFacility = CacheManager.getInstance().getCache(FacilityCache.class).getCurrentFacility();
        if (currentFacility != null) {
            Facility.Type facilityType = Facility.Type.valueOf(currentFacility.getType());
            return facilityTypeToRoleToAccessPatterns.get(facilityType).get(role);
        }
        return roleToAccessPatterns.get(role);
    }

    public Set<String> getRolesByAccessResource(String accessResource) {
        Facility currentFacility = CacheManager.getInstance().getCache(FacilityCache.class).getCurrentFacility();
        if (currentFacility != null) {
            Facility.Type facilityType = Facility.Type.valueOf(currentFacility.getType());
            return facilityTypeToAccessResourceToRoles.get(facilityType).get(accessResource);
        }
        return accessResourceToRoles.get(accessResource);
    }

    public Set<String> getRolesByAccessResourceGroup(String accessResourceGroup) {
        return accessGroupToRoles.get(accessResourceGroup);
    }

    public AccessResource getAccessResourceByRequestURI(String requestURI) {
        AccessResource accessResource = null;
        if (requestURI.equals("/")) {
            accessResource = accessPatternToAccessResource.get(requestURI);
        } else {
            if (requestURI.endsWith("/")) {
                requestURI = requestURI.substring(0, requestURI.length() - 1);
            }
            int lastIndex = requestURI.length();
            while (lastIndex != -1) {
                accessResource = accessPatternToAccessResource.get(requestURI);
                if (accessResource != null) {
                    break;
                }
                int nextIndex = requestURI.lastIndexOf('/', lastIndex - 1);
                requestURI = requestURI.substring(0, nextIndex + 1) + "*" + requestURI.substring(lastIndex);
                lastIndex = nextIndex;
            }
        }
        return accessResource;
    }

    public boolean isAccessResourcePresentInFacilityType(String accessResourceName, Facility.Type facilityType) {
        return facilityTypeToAccessResources.get(facilityType).contains(accessResourceName);
    }

    public Set<String> getAccessibleByRoles(String requestURI) {
        Set<String> accessibleByRoles = null;
        if (requestURI.equals("/")) {
            accessibleByRoles = getRolesByAccessPattern(requestURI);
        } else {
            if (requestURI.endsWith("/")) {
                requestURI = requestURI.substring(0, requestURI.length() - 1);
            }
            int lastIndex = requestURI.length();
            while (lastIndex != -1) {
                accessibleByRoles = getRolesByAccessPattern(requestURI);
                if (accessibleByRoles != null) {
                    break;
                }
                int nextIndex = requestURI.lastIndexOf('/', lastIndex - 1);
                requestURI = requestURI.substring(0, nextIndex + 1) + "*" + requestURI.substring(lastIndex);
                lastIndex = nextIndex;
            }
        }
        return accessibleByRoles;
    }

    @Override
    public void load() {
        List<FacilityAccessResource> facilityAccessResources = cacheDataManager.getFacilityAccessResources();
        for (FacilityAccessResource far : facilityAccessResources) {
            Set<String> accessResources = facilityTypeToAccessResources.get(far.getFacilityType());
            if (accessResources == null) {
                accessResources = new HashSet<>();
                facilityTypeToAccessResources.put(far.getFacilityType(), accessResources);
            }
            accessResources.add(far.getAccessResource().getName());
        }

        List<Role> roles = cacheDataManager.getRoles();
        for (Role role : roles) {
            addRole(role);
        }

        List<AccessResourceGroup> accessResourceGroups = cacheDataManager.getAccessResourceGroups();
        for (AccessResourceGroup accessResourceGroup : accessResourceGroups) {
            addAccessResourceGroup(accessResourceGroup);
        }

        List<AccessResource> accessResources = cacheDataManager.getAccessResources();
        for (AccessResource accessResource : accessResources) {
            List<RoleAccessResource> roleAccessResources = cacheDataManager.getRoleAccessResources(accessResource.getName());
            addAccessResource(accessResource, roleAccessResources);
        }
    }
}
