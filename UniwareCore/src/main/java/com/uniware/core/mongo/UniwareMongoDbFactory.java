/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *  @version     1.0, Sep 17, 2014
 *  @author amit
 */
package com.uniware.core.mongo;

import java.net.UnknownHostException;

import org.springframework.dao.DataAccessException;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;

import com.mongodb.DB;
import com.mongodb.Mongo;
import com.mongodb.MongoException;
import com.unifier.core.cache.CacheManager;
import com.uniware.core.cache.EnvironmentPropertiesCache;
import com.uniware.core.utils.UserContext;

/**
 * Factory to create {@link DB} instances from a {@link Mongo} instance.
 * 
 * @author amit
 */
public class UniwareMongoDbFactory extends SimpleMongoDbFactory {

    public UniwareMongoDbFactory(Mongo mongo, String dbName) throws MongoException, UnknownHostException {
        super(mongo, dbName);
    }

    /*
    	 * (non-Javadoc)
    	 * @see org.springframework.data.mongodb.MongoDbFactory#getDb(java.lang.String)
    	 */
    public DB getDb(String dbName) throws DataAccessException {
        boolean tenantSpecificMongoEnabled = CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class).isTenantSpecificMongoEnabled();
        return super.getDb(tenantSpecificMongoEnabled ? UserContext.current().getTenant().getCode() : dbName);
    }
}
