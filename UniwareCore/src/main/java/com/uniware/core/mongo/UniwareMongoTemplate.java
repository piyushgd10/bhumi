/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 17/09/14
 *  @author amit
 */

package com.uniware.core.mongo;

import java.net.UnknownHostException;

import org.springframework.data.mongodb.core.MongoTemplate;

import com.mongodb.Mongo;

/**
 * Uniware specific implementation of {@link org.springframework.data.mongodb.core.MongoTemplate}.
 */
public class UniwareMongoTemplate extends MongoTemplate {
    /**
     * Constructor used for a basic template configuration
     * 
     * @param mongo must not be {@literal null}.
     * @param databaseName must not be {@literal null} or empty.
     */
    public UniwareMongoTemplate(Mongo mongo, String databaseName) throws UnknownHostException {
        super(new UniwareMongoDbFactory(mongo, databaseName), null);
    }
}
