/*
 * Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 3/14/15 6:58 PM
 * @author amdalal
 */

package com.unifier.core.queryParser;

public class GenerateNoSqlQueryFromJsonRequest extends GenerateQueryRequest {

    private String queryString;

    public GenerateNoSqlQueryFromJsonRequest() {
        super();
    }

    public String getQueryString() {
        return queryString;
    }

    public void setQueryString(String queryString) {
        this.queryString = queryString;
    }
}
