/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 08/08/14
 *  @author amit
 */

package com.unifier.core.queryParser.nosql.mongo;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import com.unifier.core.expressions.Expression;
import com.unifier.core.queryParser.GenerateNoSqlQueryFromJsonRequest;
import com.unifier.core.queryParser.GenerateNoSqlQueryRequest;
import com.unifier.core.queryParser.GenerateQueryRequest;
import com.unifier.core.queryParser.IQueryParser;
import com.unifier.core.queryParser.nosql.NoSqlCriteria;
import com.unifier.core.queryParser.nosql.NoSqlFilter;
import com.unifier.core.queryParser.nosql.NoSqlQuery;
import com.unifier.core.queryParser.nosql.NoSqlSorter;
import com.unifier.core.utils.JsonUtils;

@Component(value = "mongoQueryParser")
public class MongoQueryParser implements IQueryParser {

    private static final Logger LOG = LoggerFactory.getLogger(MongoQueryParser.class);

    @Override
    public GenerateMongoQueryResponse generateQuery(GenerateNoSqlQueryRequest request) {
        return generateMongoQuery(request.getNoSqlQuery(), request);
    }

    @Override
    public GenerateMongoQueryResponse generateQuery(GenerateNoSqlQueryFromJsonRequest request) {
        NoSqlQuery mongoQuery = NoSqlQuery.compile(request.getQueryString());
        return generateMongoQuery(mongoQuery, request);
    }

    private GenerateMongoQueryResponse generateMongoQuery(NoSqlQuery mongoQuery, GenerateQueryRequest request) {
        GenerateMongoQueryResponse response = new GenerateMongoQueryResponse();
        if (!request.isEntityClassOptional()) {
            try {
                response.setClazz(Class.forName(mongoQuery.getEntity()));
            } catch (ClassNotFoundException e1) {
                LOG.error("No class found for " + mongoQuery.getEntity(), e1);
                return response;
            }
        }

        // Set filters
        org.springframework.data.mongodb.core.query.Query query = new org.springframework.data.mongodb.core.query.Query();
        for (NoSqlCriteria criteria : mongoQuery.getCriteria()) {
            Object value = Expression.compile(criteria.getValue()).evaluate(request.getQueryParams());
            query.addCriteria(NoSqlQuery.getCriteria(criteria.getName(), value, criteria.getOperator()));
        }
        for (NoSqlSorter sorter : mongoQuery.getSorters()) {
            query.with(NoSqlQuery.getSort(sorter.getOrderByField(), sorter.getDirection()));
        }

        if (request.getFilterList() != null) {
            for (GenerateQueryRequest.Filter filter : request.getFilterList()) {
                NoSqlFilter noSqlFilter = JsonUtils.stringToJson(filter.getCondition(), NoSqlFilter.class);
                NoSqlCriteria criteria = noSqlFilter.getCriteria();
                request.getQueryParams().put(filter.getId(), request.getFilterIdToValueMap().get(filter.getId()));
                Object value = Expression.compile(criteria.getValue()).evaluate(request.getQueryParams());
                query.addCriteria(NoSqlQuery.getCriteria(criteria.getName(), value, criteria.getOperator()));
            }
        }

        // Sorting criteria
        Sort sort = null;
        if (request.getSortList() != null && request.getSortList().size() > 0) {
            List<Sort.Order> orders = new ArrayList<>();
            for (GenerateQueryRequest.Sort sortColumn : request.getSortList()) {
                Sort.Direction direction = sortColumn.isDescending() ? Sort.Direction.DESC : Sort.Direction.ASC;
                orders.add(new Sort.Order(direction, sortColumn.getColumn()));
            }
            sort = new Sort(orders);
        }

        // Pagination
        Pageable pagination = null;
        if (request.getNumberOfResults() > 0) {
            pagination = new PageRequest(request.getStart() / request.getNumberOfResults(), request.getNumberOfResults(), sort);
        }
        response.setPagination(pagination);
        response.setQuery(query);
        return response;
    }
}
