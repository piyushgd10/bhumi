/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *  @version     1.0, 23-Aug-2013
 *  @author sunny
 */
package com.unifier.core.queryParser.nosql;

import com.unifier.core.utils.JsonUtils;

public class NoSqlCriteria {

    public enum Operator {
        IS,
        LIKE,
        LESS_THAN,
        LESS_THAN_EQUAL_TO,
        GREATER_THAN,
        GREATER_THAN_EQUAL_TO,
        BETWEEN,
        IN
    }

    private static final String COLON = ":";

    private String              name;
    private String              value;
    private Operator            operator;

    public static NoSqlCriteria compile(String criteriaJson) {
        return clean(JsonUtils.stringToJson(criteriaJson, NoSqlCriteria.class));
    }

    public static NoSqlCriteria clean(NoSqlCriteria noSqlCriteria) {
        // convert :x to #{#x} and :T(x) to #{T(x)}
        if (noSqlCriteria.getValue().contains(COLON)) {
            if (noSqlCriteria.getValue().startsWith(COLON + "T")) {
                noSqlCriteria.setValue(noSqlCriteria.getValue().replace(COLON, "#{") + "}");
            } else {
                noSqlCriteria.setValue(noSqlCriteria.getValue().replace(COLON, "#{#") + "}");
            }
        }
        return noSqlCriteria;
    }

    /**
     * public NoSqlCriteria() { super(); } public NoSqlCriteria(String name, String value, Operator operator) { super();
     * setName(name); setValue(value); setOperator(operator); } public NoSqlCriteria(String name, String value, String
     * operator) { this(name, value, Operator.valueOf(operator)); }
     **/

    public Operator getOperator() {
        return operator;
    }

    public void setOperator(Operator operator) {
        this.operator = operator;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Criteria [name=" + name + ", value=" + value + "]";
    }

}
