/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 08/08/14
 *  @author amit
 */

package com.unifier.core.queryParser;

public interface IQueryParser {

    GenerateQueryResponse generateQuery(GenerateNoSqlQueryFromJsonRequest request);

    GenerateQueryResponse generateQuery(GenerateNoSqlQueryRequest request);
}
