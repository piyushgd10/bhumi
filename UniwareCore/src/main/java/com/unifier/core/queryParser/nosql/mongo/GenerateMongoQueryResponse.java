/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 08/08/14
 *  @author amit
 */

package com.unifier.core.queryParser.nosql.mongo;

import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.query.Query;

import com.unifier.core.queryParser.GenerateQueryResponse;

@SuppressWarnings("rawtypes")
public class GenerateMongoQueryResponse extends GenerateQueryResponse {

    private Query    query;

    private Pageable pagination;

    private Class    clazz;

    public Query getQuery() {
        return query;
    }

    public void setQuery(Query query) {
        this.query = query;
    }

    public Pageable getPagination() {
        return pagination;
    }

    public void setPagination(Pageable pagination) {
        this.pagination = pagination;
    }

    public Class getClazz() {
        return clazz;
    }

    public void setClazz(Class clazz) {
        this.clazz = clazz;
    }
}
