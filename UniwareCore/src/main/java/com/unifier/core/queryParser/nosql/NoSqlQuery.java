/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 23-Aug-2013
 *  @author sunny
 */
package com.unifier.core.queryParser.nosql;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.mongodb.core.query.Criteria;

import com.unifier.core.queryParser.nosql.NoSqlCriteria.Operator;
import com.unifier.core.utils.DateUtils.DateRange;
import com.unifier.core.utils.JsonUtils;

public class NoSqlQuery {

    private String              entity;
    private List<String>        columns;
    private List<NoSqlCriteria> criteria = new ArrayList<>();
    private List<NoSqlSorter>   sorters  = new ArrayList<>();

    public static NoSqlQuery compile(String queryString) {
        NoSqlQuery noSqlQuery = JsonUtils.stringToJson(queryString, NoSqlQuery.class);
        for (NoSqlCriteria criteria : noSqlQuery.getCriteria()) {
            NoSqlCriteria.clean(criteria);
        }
        return noSqlQuery;
    }

    public String getEntity() {
        return entity;
    }

    public void setEntity(String entity) {
        this.entity = entity;
    }

    public List<String> getColumns() {
        return columns;
    }

    public void setColumns(List<String> columns) {
        this.columns = columns;
    }

    public List<NoSqlCriteria> getCriteria() {
        return criteria;
    }

    public void setCriteria(List<NoSqlCriteria> criteria) {
        this.criteria = criteria;
    }

    public List<NoSqlSorter> getSorters() {
        return sorters;
    }

    public void setSorters(List<NoSqlSorter> sorters) {
        this.sorters = sorters;
    }

    public static Criteria getCriteria(String name, Object value, Operator operator) {
        Criteria criteria = null;
        switch (operator) {
            case IS:
                criteria = Criteria.where(name).is(value);
                break;
            case LIKE:
                criteria = Criteria.where(name).regex(value.toString());
                break;
            case LESS_THAN:
                criteria = Criteria.where(name).lt(value);
                break;
            case LESS_THAN_EQUAL_TO:
                criteria = Criteria.where(name).lte(value);
                break;
            case GREATER_THAN:
                criteria = Criteria.where(name).gt(value);
                break;
            case GREATER_THAN_EQUAL_TO:
                criteria = Criteria.where(name).gte(value);
                break;
            case BETWEEN:
                DateRange range;
                try {
                    range = (DateRange) value;
                } catch (ClassCastException e) {
                    throw new RuntimeException("BETWEEN can only be used for DateRange");
                }
                criteria = new Criteria().andOperator(Criteria.where(name).gte(range.getStart()), Criteria.where(name).lte(range.getEnd()));
                break;
            case IN:
                criteria = Criteria.where(name).in((Collection<?>) value);
                break;
        }
        return criteria;
    }

    public static Sort getSort(String name, Direction direction) {
        return new Sort(direction, name);
    }

    @Override
    public String toString() {
        return "NoSqlQuery [entity=" + entity + ", columns=" + columns + ", criteria=" + criteria + ", sorters=" + sorters + "]";
    }
}
