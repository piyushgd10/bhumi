/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 08/08/14
 *  @author amit
 */

package com.unifier.core.queryParser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GenerateQueryRequest {

    private Map<String, Object> filterIdToValueMap = new HashMap<>();

    private List<Filter>        filterList         = new ArrayList<>();

    private List<Sort>          sortList           = new ArrayList<>();

    private int                 start;

    private int                 numberOfResults;

    private Map<String, Object> queryParams        = new HashMap<>();

    private boolean             entityClassOptional;

    public GenerateQueryRequest() {
        super();
    }

    public GenerateQueryRequest(boolean entityClassOptional) {
        super();
        setEntityClassOptional(entityClassOptional);
    }

    public Map<String, Object> getFilterIdToValueMap() {
        return filterIdToValueMap;
    }

    public void setFilterIdToValueMap(Map<String, Object> filterIdToValueMap) {
        this.filterIdToValueMap = filterIdToValueMap;
    }

    public List<Filter> getFilterList() {
        return filterList;
    }

    public void setFilterList(List<Filter> filterList) {
        this.filterList = filterList;
    }

    public List<Sort> getSortList() {
        return sortList;
    }

    public void setSortList(List<Sort> sortList) {
        this.sortList = sortList;
    }

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getNumberOfResults() {
        return numberOfResults;
    }

    public void setNumberOfResults(int numberOfResults) {
        this.numberOfResults = numberOfResults;
    }

    public Map<String, Object> getQueryParams() {
        return queryParams;
    }

    public void setQueryParams(Map<String, Object> queryParams) {
        this.queryParams = queryParams;
    }

    public boolean isEntityClassOptional() {
        return entityClassOptional;
    }

    public void setEntityClassOptional(boolean entityClassOptional) {
        this.entityClassOptional = entityClassOptional;
    }

    public static class Filter {

        private String id;

        private String condition;

        public Filter() {
            super();
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getCondition() {
            return condition;
        }

        public void setCondition(String condition) {
            this.condition = condition;
        }
    }

    public static class Sort {

        private String  column;

        private boolean descending;

        public Sort() {
            super();
        }

        public String getColumn() {
            return column;
        }

        public void setColumn(String column) {
            this.column = column;
        }

        public boolean isDescending() {
            return descending;
        }

        public void setDescending(boolean descending) {
            this.descending = descending;
        }
    }
}
