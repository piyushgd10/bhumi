/*
 * Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 3/14/15 6:49 PM
 * @author amdalal
 */

package com.unifier.core.queryParser;

import com.unifier.core.queryParser.nosql.NoSqlQuery;

public class GenerateNoSqlQueryRequest extends GenerateQueryRequest {

    private NoSqlQuery noSqlQuery;

    public GenerateNoSqlQueryRequest() {
        super();
    }

    public GenerateNoSqlQueryRequest(boolean entityClassOptional) {
        super(entityClassOptional);
    }

    public NoSqlQuery getNoSqlQuery() {
        return noSqlQuery;
    }

    public void setNoSqlQuery(NoSqlQuery noSqlQuery) {
        this.noSqlQuery = noSqlQuery;
    }
}
