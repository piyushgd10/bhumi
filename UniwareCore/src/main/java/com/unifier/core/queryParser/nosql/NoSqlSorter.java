/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *  @version     1.0, 23-Aug-2013
 *  @author sunny
 */
package com.unifier.core.queryParser.nosql;

import org.springframework.data.domain.Sort;

public class NoSqlSorter {

    private String         orderByField;
    private Sort.Direction direction;

    public NoSqlSorter() {
    }

    public NoSqlSorter(String orderByField, String direction) {
        this.orderByField = orderByField;
        this.direction = Sort.Direction.fromString(direction);
    }

    public String getOrderByField() {
        return orderByField;
    }

    public void setOrderByField(String orderByField) {
        this.orderByField = orderByField;
    }

    public Sort.Direction getDirection() {
        return direction;
    }

    public void setDirection(Sort.Direction direction) {
        this.direction = direction;
    }

    @Override
    public String toString() {
        return "NoSqlSorter [orderByField=" + orderByField + ", direction=" + direction + "]";
    }

}
