/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *  @version     1.0, 23-Aug-2013
 *  @author sunny
 */
package com.unifier.core.queryParser.nosql;

public class NoSqlFilter {

    private NoSqlCriteria criteria;

    public NoSqlCriteria getCriteria() {
        return criteria;
    }

    public void setCriteria(NoSqlCriteria criteria) {
        this.criteria = criteria;
    }
}
