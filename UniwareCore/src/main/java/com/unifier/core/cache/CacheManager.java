/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 12, 2011
 *  @author singla
 */
package com.unifier.core.cache;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Lock;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;

import com.unifier.core.annotation.Cache;
import com.unifier.core.annotation.Level;
import com.unifier.core.jms.IActiveMQConnector;
import com.unifier.core.jms.MessagingConstants;
import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.ReflectionUtils;
import com.uniware.core.cache.FacilityCache;
import com.uniware.core.entity.Facility;
import com.uniware.core.locking.ILockingService;
import com.uniware.core.locking.Namespace;
import com.uniware.core.utils.UserContext;

/**
 * @author singla
 */
public class CacheManager {

    private static final Logger                           LOG                       = LoggerFactory.getLogger(CacheManager.class);

    private static CacheManager                           _instance                 = new CacheManager();
    private final Map<String, Level>                      _cacheNameToLevels        = new HashMap<>();
    private final Map<String, Class<?>>                   _cacheNameToClass         = new HashMap<>();
    private final List<Class<?>>                          _eagerCaches              = new ArrayList<>();
    private final List<Class<?>>                          _eagerFacilityLevelCaches = new ArrayList<>();
    private final List<Class<?>>                          _eagerGlobalCaches        = new ArrayList<>();

    private Map<String, ICache>                           _globalCaches             = new ConcurrentHashMap<>();
    private Map<String, Map<String, ICache>>              _tenantCaches             = new ConcurrentHashMap<>();
    private Map<String, Map<String, Map<String, ICache>>> _facilityCaches           = new ConcurrentHashMap<>();

    private Map<Class<?>, Date>                           cacheToLastMarkDirtyTime  = new ConcurrentHashMap<>();

    @Autowired
    private ApplicationContext                            applicationContext;

    @Autowired
    private ILockingService                               lockingService;

    @Autowired
    @Qualifier("activeMQConnector1")
    private IActiveMQConnector                            activeMQConnector;

    private CacheManager() {
        try {
            Set<String> _cacheNames = new HashSet<>();
            for (Class<?> clazz : ReflectionUtils.getClassesAnnotatedWith(Cache.class, "com.**.cache")) {
                Cache cache = clazz.getAnnotation(Cache.class);
                if (_cacheNames.contains(cache.type())) {
                    LOG.error("Multiple caches cannot have the same name: {}", cache.type());
                    throw new RuntimeException("Multiple caches cannot have the same name: " + cache.type());
                } else {
                    _cacheNames.add(cache.type());
                }
                _cacheNameToLevels.put(cache.type(), cache.level());
                _cacheNameToClass.put(cache.type(), clazz);
                if (cache.eager()) {
                    switch (cache.level()) {
                        case FACILITY:
                            _eagerFacilityLevelCaches.add(clazz);
                            break;
                        case GLOBAL:
                            _eagerGlobalCaches.add(clazz);
                            break;
                        default:
                            _eagerCaches.add(clazz);
                    }
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    public static CacheManager getInstance() {
        return _instance;
    }

    private ICache getCacheInternal(String cacheName) {
        Level level = _cacheNameToLevels.get(cacheName);
        ICache cache = null;
        if (level == Level.TENANT) {
            Map<String, ICache> caches = _tenantCaches.get(UserContext.current().getTenant().getCode());
            if (caches != null) {
                cache = caches.get(cacheName);
            }
        } else if (level == Level.FACILITY) {
            String tenantCode = UserContext.current().getTenant().getCode();
            if (_facilityCaches.containsKey(tenantCode)) {
                Map<String, ICache> caches;
                if (UserContext.current().getFacility() == null) {
                    Facility firstFacility = CacheManager.getInstance().getCache(FacilityCache.class).getFacilities().get(0);
                    caches = _facilityCaches.get(tenantCode).get(firstFacility.getCode());
                } else {
                    caches = _facilityCaches.get(tenantCode).get(UserContext.current().getFacility().getCode());
                }
                if (caches != null) {
                    cache = caches.get(cacheName);
                }
            }
        } else {
            cache = _globalCaches.get(cacheName);
        }
        return cache;
    }

    public ICache getCache(String cacheName) {
        ICache cache = getCacheInternal(cacheName);
        if (cache == null) {
            Level level = _cacheNameToLevels.get(cacheName);
            String lockName = "cacheLock" + cacheName;
            Lock lock = lockingService.getLock(Namespace.CACHE_RELOAD, lockName, level);
            try {
                LOG.info("Acquiring cache lock for: {}", cacheName);
                long startTime = System.currentTimeMillis();
                lock.lock();
                LOG.info("Lock acquired for: {} in {} ms", cacheName, (System.currentTimeMillis() - startTime));
                cache = getCacheInternal(cacheName);
                if (cache == null) {
                    cache = loadCache(cacheName);
                }
            } finally {
                lock.unlock();
            }
        }
        return cache;
    }

    @SuppressWarnings("unchecked")
    public <T> T getCache(Class<T> cacheClass) {
        if (cacheClass.isAnnotationPresent(Cache.class)) {
            return (T) getCache(cacheClass.getAnnotation(Cache.class).type());
        } else {
            throw new IllegalArgumentException("@Cache annotation should be present for cache class:" + cacheClass.getName());
        }
    }

    public void doClearTenantCaches() {
        LOG.info("Clearing all caches for tenant: {}", UserContext.current().getTenant().getName());
        String tenantCode = UserContext.current().getTenant().getCode();
        for (Facility facility : CacheManager.getInstance().getCache(FacilityCache.class).getFacilities()) {
            if (_facilityCaches.containsKey(tenantCode)) {
                Map<String, ICache> facilityCaches = _facilityCaches.get(tenantCode).get(facility.getCode());
                if (facilityCaches != null) {
                    facilityCaches.clear();
                }
            }
        }
        Map<String, ICache> tenantCaches = _tenantCaches.get(UserContext.current().getTenant().getCode());
        if (tenantCaches != null) {
            tenantCaches.clear();
        }
        _globalCaches.clear();
        loadEagerGlobalCaches();
        loadEagerCaches();
        for (Facility facility : CacheManager.getInstance().getCache(FacilityCache.class).getFacilities()) {
            UserContext.current().setFacility(facility);
            if (_facilityCaches.containsKey(tenantCode)) {
                Map<String, ICache> facilityCaches = _facilityCaches.get(tenantCode).get(facility.getCode());
                if (facilityCaches != null) {
                    loadEagerFacilityLevelCaches();
                }
            }
        }
    }

    public void doMarkCacheDirty(String cacheName, Date requestTimestamp) {
        doMarkCacheDirty(_cacheNameToClass.get(cacheName), requestTimestamp);
    }

    private <T> void doMarkCacheDirty(Class<T> cacheClass, Date requestTimestamp) {
        if (cacheClass.isAnnotationPresent(Cache.class)) {
            Cache annotation = cacheClass.getAnnotation(Cache.class);
            Date lastMarkDirtyTimestamp = cacheToLastMarkDirtyTime.get(cacheClass);
            if (requestTimestamp != null && lastMarkDirtyTimestamp != null && lastMarkDirtyTimestamp.after(requestTimestamp)) {
                LOG.info("Skipping cache dirty event with timestamp: {} as {} was marked dirty at: {}", new Object[] { requestTimestamp, cacheClass, lastMarkDirtyTimestamp });
            } else {
                boolean removed = false;
                LOG.info("Marking cache as dirty: {}", annotation.type());
                if (annotation.level() == Level.TENANT) {
                    Map<String, ICache> caches = _tenantCaches.get(UserContext.current().getTenant().getCode());
                    if (caches != null) {
                        removed = true;
                        caches.remove(annotation.type());
                    }
                } else if (annotation.level() == Level.FACILITY) {
                    String tenantCode = UserContext.current().getTenant().getCode();
                    if (_facilityCaches.containsKey(tenantCode)) {
                        Map<String, ICache> caches = _facilityCaches.get(tenantCode).get(UserContext.current().getFacility().getCode());
                        if (caches != null) {
                            removed = true;
                            caches.remove(annotation.type());
                        }
                    }
                } else {
                    removed = true;
                    _globalCaches.remove(annotation.type());
                }
                cacheToLastMarkDirtyTime.put(cacheClass, DateUtils.getCurrentTime());
                if (removed) {
                    loadCache(cacheClass);
                }
            }
        } else {
            throw new IllegalArgumentException("@Cache annotation should be present for cache class:" + cacheClass.getName());
        }
    }

    public void markCacheDirty(String cacheName) {
        markCacheDirty(_cacheNameToClass.get(cacheName));
    }

    public <T> void markCacheDirty(Class<T> cacheClass) {
        if (cacheClass.isAnnotationPresent(Cache.class)) {
            Cache annotation = cacheClass.getAnnotation(Cache.class);
            Date cacheDirtyRequestTimestamp = DateUtils.getCurrentTime();
            doMarkCacheDirty(cacheClass, cacheDirtyRequestTimestamp);
            activeMQConnector.produceContextAwareMessage(MessagingConstants.Queue.CACHE_DIRTY,
                    new CacheDirtyEvent(CacheDirtyEvent.Type.CACHE, CacheDirtyEvent.Action.CLEAR_SPECIFIC, annotation.type(), cacheDirtyRequestTimestamp));
        } else {
            throw new IllegalArgumentException("@Cache annotation should be present for cache class:" + cacheClass.getName());
        }
    }

    public void clearTenantCaches() {
        activeMQConnector.produceContextAwareMessage(MessagingConstants.Queue.CACHE_DIRTY,
                new CacheDirtyEvent(CacheDirtyEvent.Type.CACHE, CacheDirtyEvent.Action.CLEAR_ALL, null, null));
    }

    public <T> ICache loadCache(String cacheName) {
        return loadCache(_cacheNameToClass.get(cacheName));
    }

    public <T> ICache loadCache(Class<T> cacheClass) {
        try {
            LOG.info("Loading {}", cacheClass.getName());
            long startTime = System.currentTimeMillis();
            ICache cache = (ICache) cacheClass.newInstance();
            applicationContext.getAutowireCapableBeanFactory().autowireBeanProperties(cache, AutowireCapableBeanFactory.AUTOWIRE_BY_TYPE, false);
            cache.load();
            setCache(cache);
            LOG.info("Done loading {} in {} ms", cacheClass.getName(), System.currentTimeMillis() - startTime);
            return cache;
        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        }
    }

    public <T> boolean isCacheLoaded(Class<T> cacheClass) {
        if (cacheClass.isAnnotationPresent(Cache.class)) {
            return isCacheLoaded(cacheClass.getAnnotation(Cache.class).type());
        } else {
            throw new IllegalArgumentException("@Cache annotation should be present for cache class:" + cacheClass.getName());
        }
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    public void loadEagerCaches() {
        for (Class clazz : _eagerCaches) {
            if (!isCacheLoaded(clazz)) {
                loadCache(clazz);
            }
        }
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    public void loadEagerFacilityLevelCaches() {
        for (Class clazz : _eagerFacilityLevelCaches) {
            if (!isCacheLoaded(clazz)) {
                loadCache(clazz);
            }
        }
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    public void loadEagerGlobalCaches() {
        for (Class clazz : _eagerGlobalCaches) {
            if (!isCacheLoaded(clazz)) {
                loadCache(clazz);
            }
        }
    }

    /**
     * @param cacheName
     * @return
     */
    private boolean isCacheLoaded(String cacheName) {
        Level level = _cacheNameToLevels.get(cacheName);
        Object cache = null;
        if (level == Level.TENANT) {
            Map<String, ICache> caches = _tenantCaches.get(UserContext.current().getTenant().getCode());
            if (caches != null) {
                cache = caches.get(cacheName);
            }
        } else if (level == Level.FACILITY) {
            Map<String, Map<String, ICache>> tenantFacilityCaches = _facilityCaches.get(UserContext.current().getTenant().getCode());
            if (tenantFacilityCaches != null) {
                Map<String, ICache> caches = tenantFacilityCaches.get(UserContext.current().getFacility().getCode());
                if (caches != null) {
                    cache = caches.get(cacheName);
                }
            }
        } else {
            cache = _globalCaches.get(cacheName);
        }
        return cache != null;
    }

    public void setCache(final ICache cache) {
        Class<? extends Object> cacheClass = cache.getClass();
        if (cacheClass.isAnnotationPresent(Cache.class)) {
            for (Method m : cacheClass.getDeclaredMethods()) {
                if ("freeze".equals(m.getName())) {
                    try {
                        m.invoke(cache);
                    } catch (Exception e) {
                        LOG.error("unable to freeze cache:" + cacheClass.getName(), e);
                    }
                }
            }
            Cache annotation = cacheClass.getAnnotation(Cache.class);
            if (annotation.level() == Level.TENANT) {
                Map<String, ICache> tenantCache = _tenantCaches.get(UserContext.current().getTenant().getCode());
                if (tenantCache == null) {
                    tenantCache = new ConcurrentHashMap<>();
                    _tenantCaches.put(UserContext.current().getTenant().getCode(), tenantCache);
                }
                tenantCache.put(annotation.type(), cache);
                _tenantCaches.put(UserContext.current().getTenant().getCode(), tenantCache);
            } else if (annotation.level() == Level.FACILITY) {
                Map<String, Map<String, ICache>> tenantFacilityCaches = _facilityCaches.get(UserContext.current().getTenant().getCode());
                if (tenantFacilityCaches == null) {
                    tenantFacilityCaches = new ConcurrentHashMap<>();
                    _facilityCaches.put(UserContext.current().getTenant().getCode(), tenantFacilityCaches);
                }
                Map<String, ICache> facilityCache = tenantFacilityCaches.get(UserContext.current().getFacility().getCode());
                if (facilityCache == null) {
                    facilityCache = new ConcurrentHashMap<>();
                    tenantFacilityCaches.put(UserContext.current().getFacility().getCode(), facilityCache);
                }
                facilityCache.put(annotation.type(), cache);
            } else {
                _globalCaches.put(annotation.type(), cache);
            }
        } else {
            throw new IllegalArgumentException("@Cache annotation should be present for cache class:" + cache.getClass().getName());
        }
    }
}
