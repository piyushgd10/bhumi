/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 11-Nov-2013
 *  @author sunny
 */
package com.unifier.core.cache.data.manager;

import java.util.List;

import com.unifier.core.entity.AccessResource;
import com.unifier.core.entity.AccessResourceGroup;
import com.unifier.core.entity.EnvironmentProperty;
import com.unifier.core.entity.FacilityAccessResource;
import com.unifier.core.entity.Role;
import com.unifier.core.entity.RoleAccessResource;
import com.uniware.core.api.metadata.InformationSchemaDTO;
import com.uniware.core.entity.*;

public interface ICacheDataManager {

    List<Facility> getFacilities();

    List<Tenant> getAllTenants();

    List<FacilityAllocationRule> getFacilityAllocationRules();

    List<Role> getRoles();

    List<AccessResourceGroup> getAccessResourceGroups();

    List<FacilityAccessResource> getFacilityAccessResources();

    List<AccessResource> getAccessResources();

    List<RoleAccessResource> getRoleAccessResources(String accessResourceName);

    List<State> getStates();

    List<Country> getCountries();

    List<EnvironmentProperty> getEnvironmentProperties();

    List<InformationSchemaDTO> getInformationSchema();

    /**
     * Return List of TenantSetupState in ascending order of "position"
     * 
     * @return
     */
    List<TenantSetupState> getTenantSetupStateMappings();

    FacilityProfile getFacilityProfileByCode(String code);

    List<Location> getAllLocations();
}
