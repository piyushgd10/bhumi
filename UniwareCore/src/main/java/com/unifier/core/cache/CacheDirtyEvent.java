/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 11-Nov-2013
 *  @author sunny
 */
package com.unifier.core.cache;

import java.io.Serializable;
import java.util.Date;

public class CacheDirtyEvent implements Serializable {

    public enum Type {
        CACHE,
        CONFIGURATION
    }

    public enum Action {
        CLEAR_ALL,
        CLEAR_SPECIFIC
    }

    public CacheDirtyEvent() {
    }

    public CacheDirtyEvent(Type type, Action action, String name, Date requestTimestamp) {
        this.type = type;
        this.action = action;
        this.name = name;
        this.requestTimestamp = requestTimestamp;
    }

    private Type   type;
    private Action action;
    private String name;
    private Date   requestTimestamp;

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public Action getAction() {
        return action;
    }

    public void setAction(Action action) {
        this.action = action;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getRequestTimestamp() {
        return requestTimestamp;
    }

    public void setRequestTimestamp(Date requestTimestamp) {
        this.requestTimestamp = requestTimestamp;
    }

    @Override
    public String toString() {
        return "CacheDirtyEvent{" + "type=" + type + ", action=" + action + ", name='" + name + '\'' + '}';
    }
}
