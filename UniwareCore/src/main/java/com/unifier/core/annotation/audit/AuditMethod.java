/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 23-Apr-2014
 *  @author parijat
 */
package com.unifier.core.annotation.audit;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author parijat Use this annotation over a method to mark it auditable.
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface AuditMethod {

    /**
     * The message that should be published as audit log. This will be a spring expression Only those entities can be
     * referred in this expression which will have annotation {@link AuditClass} The message can be formed out of
     * entities currently on method stack and also from method entities called within this method. Format for current
     * method is : entityName.fieldName.oldValue and entityName.fieldName.newValue. entityName is class name. Format for
     * nested method is : methodName.entityName.fieldName.oldValue and like wise newValue
     */
    String auditMessage() default "";

    /**
     * Unique method name with which method is identified
     */
    String methodName();
    
    /**
     * This tells the audit advice if we need to log the change log between old and new value
     */
    boolean logChangeAudit() default true;
}
