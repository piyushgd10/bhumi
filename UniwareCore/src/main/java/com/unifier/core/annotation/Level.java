package com.unifier.core.annotation;

public enum Level {
    GLOBAL,
    TENANT,
    FACILITY
};