/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 24-Apr-2014
 *  @author parijat
 */
package com.unifier.core.annotation.audit;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author parijat
 *
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface AuditClass {

    /**
     * Spring expression which will evaluate binded entity identifier for e.g for saleOrder code #{#entity.code} or
     * #{#entity.displayOrderCode} the object to be evaluated will always with alias entity in expression. Default
     * expression return id of the entity
     */
    String identifier() default "#{#entity.id}";
    
    /**
     * Spring expression which will evaluate parent entity identifier for e.g for saleOrder code from shipping package
     * #{#entity.saleOrder.code} the object to be evaluated will always with alias entity in expression. Default
     * expression return id of the entity
     */
    String groupIdentifier() default "#{#entity.id}";
}
