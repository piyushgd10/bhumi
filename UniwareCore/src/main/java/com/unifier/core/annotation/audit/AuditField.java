/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 24-Apr-2014
 *  @author parijat
 */
package com.unifier.core.annotation.audit;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.unifier.core.utils.StringUtils;

/**
 * @author parijat Annotation to used over field which needs to audited. This annotation will be intercepted in
 *         hibernate persistence layer
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface AuditField {

    /**
     * specific name of field to be put in audit logs. defaults to declared name
     */
    String fieldName() default StringUtils.EMPTY_STRING;

    String fieldValue() default StringUtils.EMPTY_STRING;

    /**
     * Audit log for new value of the field. Default to currentValue.toString(). Customization for change of address in
     * sale order e.g. #{#currentValue!=null?#currentValue.name.concat(' changed to
     * ').concat(#currentValue.addressLine1).concat(#currentValue.addressLine2).concat(#currentValue.city).concat(#currentValue.stateCode
     * ) .concat(#currentValue.countryCode).concat(#currentValue.pincode) }
     */
    String newValueExpression() default "#{#currentValue!=null?T(com.unifier.core.utils.JsonUtils).objectToString(#currentValue):''}";

    /**
     * Audit log for new value of the field. Default to previousValue.toString(). Customization will be same as for new
     * value expect for currentValue previousValue will be used.
     */
    String oldValueExpression() default "#{(#previousValue != null?T(com.unifier.core.utils.JsonUtils).objectToString(#previousValue):'')}";
}
