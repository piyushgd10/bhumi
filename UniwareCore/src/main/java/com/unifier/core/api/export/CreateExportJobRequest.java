/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 19, 2012
 *  @author singla
 */
package com.unifier.core.api.export;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Future;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author singla
 */
public class CreateExportJobRequest extends ServiceRequest {

    /**
     *
     */
    private static final long serialVersionUID = -9207488912146996814L;

    @NotEmpty
    private String exportJobTypeName;

    @NotEmpty
    private List<String> exportColums;

    @Valid
    private List<WsExportFilter> exportFilters;

    @NotNull
    private Integer userId;

    @Future
    private Date scheduleTime;

    @Email
    private String notificationEmail;

    @NotEmpty
    private String frequency;

    private String cronExpression;

    private String reportName;

    /**
     * @return the exportJobTypeName
     */
    public String getExportJobTypeName() {
        return exportJobTypeName;
    }

    /**
     * @param exportJobTypeName the exportJobTypeName to set
     */
    public void setExportJobTypeName(String exportJobTypeName) {
        this.exportJobTypeName = exportJobTypeName;
    }

    /**
     * @return the exportColums
     */
    public List<String> getExportColums() {
        return exportColums;
    }

    /**
     * @param exportColums the exportColums to set
     */
    public void setExportColums(List<String> exportColums) {
        this.exportColums = exportColums;
    }

    /**
     * @return the exportFilters
     */
    public List<WsExportFilter> getExportFilters() {
        return exportFilters;
    }

    /**
     * @param exportFilters the exportFilters to set
     */
    public void setExportFilters(List<WsExportFilter> exportFilters) {
        this.exportFilters = exportFilters;
    }

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return the scheduleTime
     */
    public Date getScheduleTime() {
        return scheduleTime;
    }

    /**
     * @param scheduleTime the scheduleTime to set
     */
    public void setScheduleTime(Date scheduleTime) {
        this.scheduleTime = scheduleTime;
    }

    /**
     * @return the notificationEmail
     */
    public String getNotificationEmail() {
        return notificationEmail;
    }

    /**
     * @param notificationEmail the notificationEmail to set
     */
    public void setNotificationEmail(String notificationEmail) {
        this.notificationEmail = notificationEmail;
    }

    /**
     * @return the frequency
     */
    public String getFrequency() {
        return frequency;
    }

    /**
     * @param frequency the frequency to set
     */
    public void setFrequency(String frequency) {
        this.frequency = frequency;
    }

    public String getCronExpression() {
        return cronExpression;
    }

    public void setCronExpression(String cronExpression) {
        this.cronExpression = cronExpression;
    }

    /**
     * @return the reportName
     */
    public String getReportName() {
        return reportName;
    }

    /**
     * @param reportName the reportName to set
     */
    public void setReportName(String reportName) {
        this.reportName = reportName;
    }

    @Override
    public String toString() {
        return "CreateExportJobRequest{" +
                "exportJobTypeName='" + exportJobTypeName + '\'' +
                ", exportColums=" + exportColums +
                ", exportFilters=" + exportFilters +
                '}';
    }
}
