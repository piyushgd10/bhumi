/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 24-May-2012
 *  @author vibhu
 */
package com.unifier.core.api.advanced;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author vibhu
 */
public class EditRoleResourceMappingResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 3112524759542290482L;

}
