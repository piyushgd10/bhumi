/*
 * Copyright 2017 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 17/11/17
 * @author piyush
 */
package com.unifier.core.api.myaccount;

public class GetMonthlyUsageHistoryResponse extends GetUsageHistoryResponse {
}
