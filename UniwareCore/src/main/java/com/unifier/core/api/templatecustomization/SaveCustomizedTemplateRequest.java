/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 21/05/14
 *  @author amit
 */

package com.unifier.core.api.templatecustomization;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;
import com.uniware.core.vo.PrintTemplateVO;

public class SaveCustomizedTemplateRequest extends ServiceRequest {

    private static final long        serialVersionUID = -2302099381167829555L;

    @NotNull
    private PrintTemplateVO.Type     templateType;
    
    @NotNull
    private String                   samplePrintTemplateCode;   

    @NotNull
    private List<ModifyParameterDTO> parameterDTOList = new ArrayList<>();

    public SaveCustomizedTemplateRequest() {
        super();
    }

    public PrintTemplateVO.Type getTemplateType() {
        return templateType;
    }

    public void setTemplateType(PrintTemplateVO.Type templateType) {
        this.templateType = templateType;
    }

    public List<ModifyParameterDTO> getParameterDTOList() {
        return parameterDTOList;
    }

    public void setParameterDTOList(List<ModifyParameterDTO> parameterDTOList) {
        this.parameterDTOList = parameterDTOList;
    }

    @Override
    public String toString() {
        return "SaveCustomizedTemplateRequest{" + "templateType=" + templateType + ", parameterDTOList=" + parameterDTOList + '}';
    }

    public String getSamplePrintTemplateCode() {
        return samplePrintTemplateCode;
    }

    public void setSamplePrintTemplateCode(String samplePrintTemplateCode) {
        this.samplePrintTemplateCode = samplePrintTemplateCode;
    }

    public static class ModifyParameterDTO {

        @NotBlank
        private String groupCode;

        @NotBlank
        private String parameterCode;

        @NotNull
        @Length(max = 500)
        private String parameterValue;

        public ModifyParameterDTO() {
            super();
        }

        public String getParameterCode() {
            return parameterCode;
        }

        public void setParameterCode(String parameterCode) {
            this.parameterCode = parameterCode;
        }

        public String getParameterValue() {
            return parameterValue;
        }

        public void setParameterValue(String parameterValue) {
            this.parameterValue = parameterValue;
        }

        public String getGroupCode() {
            return groupCode;
        }

        public void setGroupCode(String groupCode) {
            this.groupCode = groupCode;
        }

        @Override
        public String toString() {
            return "ModifyParameterDTO{" + "groupCode='" + groupCode + '\'' + ", parameterCode='" + parameterCode + '\'' + ", parameterValue='" + parameterValue + '\'' + '}';
        }
    }
}
