/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 23-Feb-2015
 *  @author akshay
 */
package com.unifier.core.api.myaccount;

import com.unifier.core.api.base.ServiceRequest;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.List;

public class CreatePaymentSaleOrderRequest extends ServiceRequest {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @NotNull
    @Valid
    private WsSaleOrder       saleOrder;

    private String            referrerCode;

    public String getReferrerCode() {
        return referrerCode;
    }

    public void setReferrerCode(String referrerCode) {
        this.referrerCode = referrerCode;
    }

    public WsSaleOrder getSaleOrder() {
        return saleOrder;
    }

    public void setSaleOrder(WsSaleOrder saleOrder) {
        this.saleOrder = saleOrder;
    }

    public class WsSaleOrder {

        private String                accountCode;

        @NotEmpty
        @Valid
        private List<WsSaleOrderItem> saleOrderItems;

        public String getAccountCode() {
            return accountCode;
        }

        public void setAccountCode(String accountCode) {
            this.accountCode = accountCode;
        }

        public List<WsSaleOrderItem> getSaleOrderItems() {
            return saleOrderItems;
        }

        public void setSaleOrderItems(List<WsSaleOrderItem> saleOrderItems) {
            this.saleOrderItems = saleOrderItems;
        }
    }

    public static class WsSaleOrderItem {

        @NotBlank
        private String productCode;

        @Min(value = 1)
        private int    quantity;

        public String getProductCode() {
            return productCode;
        }

        public void setProductCode(String productCode) {
            this.productCode = productCode;
        }

        public int getQuantity() {
            return quantity;
        }

        public void setQuantity(int quantity) {
            this.quantity = quantity;
        }
    }

}
