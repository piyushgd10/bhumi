/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jun 29, 2012
 *  @author singla
 */
package com.unifier.core.api.comments;

import com.unifier.core.api.base.ServiceRequest;

import org.hibernate.validator.constraints.NotBlank;


public class GetCommentsRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -506391645137455564L;

    @NotBlank
    private String            referenceIdentifier;

    /**
     * @return the referenceIdentifier
     */
    public String getReferenceIdentifier() {
        return referenceIdentifier;
    }

    /**
     * @param referenceIdentifier the referenceIdentifier to set
     */
    public void setReferenceIdentifier(String referenceIdentifier) {
        this.referenceIdentifier = referenceIdentifier;
    }
}
