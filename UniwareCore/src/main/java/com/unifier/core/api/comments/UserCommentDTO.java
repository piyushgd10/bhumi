/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 30-Jun-2012
 *  @author praveeng
 */
package com.unifier.core.api.comments;

import java.util.Date;

/**
 * @author praveeng
 */
public class UserCommentDTO {
    private Integer commentId;
    private String  comment;
    private Date    created;
    private String  username;
    private boolean removable;
    private boolean systemGenerated;

    /**
     * @return the commentId
     */
    public Integer getCommentId() {
        return commentId;
    }

    /**
     * @param commentId the commentId to set
     */
    public void setCommentId(Integer commentId) {
        this.commentId = commentId;
    }

    /**
     * @return the comment
     */
    public String getComment() {
        return comment;
    }

    /**
     * @param comment the comment to set
     */
    public void setComment(String comment) {
        this.comment = comment;
    }

    /**
     * @return the created
     */
    public Date getCreated() {
        return created;
    }

    /**
     * @param created the created to set
     */
    public void setCreated(Date created) {
        this.created = created;
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the removable
     */
    public boolean isRemovable() {
        return removable;
    }

    /**
     * @param removable the removable to set
     */
    public void setRemovable(boolean removable) {
        this.removable = removable;
    }

    /**
     * @return the systemGenerated
     */
    public boolean isSystemGenerated() {
        return systemGenerated;
    }

    /**
     * @param systemGenerated the systemGenerated to set
     */
    public void setSystemGenerated(boolean systemGenerated) {
        this.systemGenerated = systemGenerated;
    }

}
