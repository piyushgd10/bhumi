/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 21/05/14
 *  @author amit
 */

package com.unifier.core.api.templatecustomization;

import java.util.ArrayList;
import java.util.List;

public class CustomizationParameterGroupDTO {

    private String                          code;

    private String                          displayName;

    private int                             position;

    private List<CustomizationParameterDTO> parameterList = new ArrayList<>();

    public CustomizationParameterGroupDTO() {
        super();
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public List<CustomizationParameterDTO> getParameterList() {
        return parameterList;
    }

    public void setParameterList(List<CustomizationParameterDTO> parameterList) {
        this.parameterList = parameterList;
    }

    @Override
    public String toString() {
        return "CustomizationParameterGroupDTO{" + "code='" + code + '\'' + ", displayName='" + displayName + '\'' + ", position=" + position + ", parameterList=" + parameterList
                + '}';
    }
}
