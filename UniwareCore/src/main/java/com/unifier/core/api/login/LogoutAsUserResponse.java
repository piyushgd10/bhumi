/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 14-Apr-2013
 *  @author unicom
 */
package com.unifier.core.api.login;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author unicom
 */
public class LogoutAsUserResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 234242432342342L;

}
