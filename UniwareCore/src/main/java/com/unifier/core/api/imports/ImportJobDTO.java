/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 28, 2012
 *  @author praveeng
 */
package com.unifier.core.api.imports;

import java.util.Date;

import com.unifier.core.entity.ImportJob;

public class ImportJobDTO {

    private String  id;
    private String  name;
    private Date    scheduledTime;
    private String  statusCode;
    private boolean successful;
    private int     percentageComplete;
    private String  filePath;
    private String  logFilePath;
    private int     totalRows;
    private int     rowsProcessed;
    private int     successfulImportCount;
    private int     failedImportCount;

    public ImportJobDTO() {
    }

    public ImportJobDTO(ImportJob importJob) {
        this.id = importJob.getId();
        this.name = importJob.getImportJobTypeName();
        this.scheduledTime = importJob.getCreated();
        this.statusCode = importJob.getStatusCode().name();
        this.successful = importJob.isSuccessful();
        this.percentageComplete = (importJob.getCurrentMileStone() * 100) / importJob.getMileStoneCount();
        this.filePath = importJob.getFilePath();
        this.logFilePath = importJob.getLogFilePath();
        this.totalRows = importJob.getTotalRows();
        this.rowsProcessed = importJob.getRowsProcessed();
        this.successfulImportCount = importJob.getSuccessfulImportCount();
        this.failedImportCount = importJob.getFailedImportCount();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getScheduledTime() {
        return scheduledTime;
    }

    public void setScheduledTime(Date scheduledTime) {
        this.scheduledTime = scheduledTime;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public boolean isSuccessful() {
        return successful;
    }

    public void setSuccessful(boolean successful) {
        this.successful = successful;
    }

    public int getPercentageComplete() {
        return percentageComplete;
    }

    public void setPercentageComplete(int percentageComplete) {
        this.percentageComplete = percentageComplete;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getLogFilePath() {
        return logFilePath;
    }

    public void setLogFilePath(String logFilePath) {
        this.logFilePath = logFilePath;
    }

    public int getTotalRows() {
        return totalRows;
    }

    public void setTotalRows(int totalRows) {
        this.totalRows = totalRows;
    }

    public int getRowsProcessed() {
        return rowsProcessed;
    }

    public void setRowsProcessed(int rowsProcessed) {
        this.rowsProcessed = rowsProcessed;
    }

    public int getSuccessfulImportCount() {
        return successfulImportCount;
    }

    public void setSuccessfulImportCount(int successfulImportCount) {
        this.successfulImportCount = successfulImportCount;
    }

    public int getFailedImportCount() {
        return failedImportCount;
    }

    public void setFailedImportCount(int failedImportCount) {
        this.failedImportCount = failedImportCount;
    }
}
