/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Apr 18, 2012
 *  @author praveeng
 */
package com.unifier.core.api.email;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author praveeng
 */
public class EditEmailTemplateRequest extends ServiceRequest {

    /**
     *
     */
    private static final long serialVersionUID = 4350597188746720841L;
    private boolean           enabled;
    private String            type;
    private String            cc;
    private String            bcc;
    private String            subjectTemplate;
    private String            bodyTemplate;

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the cc
     */
    public String getCc() {
        return cc;
    }

    /**
     * @param cc the cc to set
     */
    public void setCc(String cc) {
        this.cc = cc;
    }

    /**
     * @return the bcc
     */
    public String getBcc() {
        return bcc;
    }

    /**
     * @param bcc the bcc to set
     */
    public void setBcc(String bcc) {
        this.bcc = bcc;
    }

    /**
     * @return the subjectTemplate
     */
    public String getSubjectTemplate() {
        return subjectTemplate;
    }

    /**
     * @param subjectTemplate the subjectTemplate to set
     */
    public void setSubjectTemplate(String subjectTemplate) {
        this.subjectTemplate = subjectTemplate;
    }

    /**
     * @return the bodyTemplate
     */
    public String getBodyTemplate() {
        return bodyTemplate;
    }

    /**
     * @param bodyTemplate the bodyTemplate to set
     */
    public void setBodyTemplate(String bodyTemplate) {
        this.bodyTemplate = bodyTemplate;
    }

    /**
     * @return the enabled
     */
    public boolean isEnabled() {
        return enabled;
    }

    /**
     * @param enabled the enabled to set
     */
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

}
