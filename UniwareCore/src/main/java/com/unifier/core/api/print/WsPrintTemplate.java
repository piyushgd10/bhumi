/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 *  @version     1.0, Mar 8, 2012
 *  @author praveeng
 */
package com.unifier.core.api.print;

import org.hibernate.validator.constraints.NotBlank;

/**
 * @author Sunny Agarwal
 */
public class WsPrintTemplate {

    @NotBlank
    private String  type;
    @NotBlank
    private String  name;
    private String  description;

    @NotBlank
    private String  template;
    private int     numberOfCopies;
    private String  printDialog;
    private String  pageSize;
    private String  pageMargins;
    private boolean landscape;
    private boolean global;

    public WsPrintTemplate() {
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    public int getNumberOfCopies() {
        return numberOfCopies;
    }

    public void setNumberOfCopies(int numberOfCopies) {
        this.numberOfCopies = numberOfCopies;
    }

    public String getPrintDialog() {
        return printDialog;
    }

    public void setPrintDialog(String printDialog) {
        this.printDialog = printDialog;
    }

    public String getPageSize() {
        return pageSize;
    }

    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }

    public String getPageMargins() {
        return pageMargins;
    }

    public void setPageMargins(String pageMargins) {
        this.pageMargins = pageMargins;
    }

    public boolean isLandscape() {
        return landscape;
    }

    public void setLandscape(boolean landscape) {
        this.landscape = landscape;
    }

    public boolean isGlobal() {
        return global;
    }

    public void setGlobal(boolean global) {
        this.global = global;
    }

}
