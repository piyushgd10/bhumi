/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 25-Mar-2013
 *  @author Pankaj
 */
package com.unifier.core.api.tasks;

import java.util.ArrayList;
import java.util.List;

import com.unifier.core.api.base.ServiceResponse;
import com.unifier.core.api.export.ExportJobDTO;

/**
 * @author Sunny
 */
public class UserExportJobsResponse extends ServiceResponse {

    /**
     *
     */
    private static final long        serialVersionUID = -2245847923440424109L;
    private final List<ExportJobDTO> exportJobs       = new ArrayList<>();

    public List<ExportJobDTO> getExportJobs() {
        return exportJobs;
    }
}
