/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *  @version     1.0, 31-July-2015
 *  @author akshaykochhar
 */
package com.unifier.core.api.myaccount;

import com.unifier.core.api.base.ServiceResponse;

/**
 * Created by akshaykochhar on 31/07/15.
 */
public class UpdateUserDigestResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 6233143929132553629L;
    private UserAccountDetailsDTO userAccountDetailsDTO;

    /**
     * @return the myAccountSettingDTO
     */
    public UserAccountDetailsDTO getUserAccountDetailsDTO() {
        return userAccountDetailsDTO;
    }

    /**
     * @param userAccountDetailsDTO the myAccountSettingDTO to set
     */
    public void setUserAccountDetailsDTO(UserAccountDetailsDTO userAccountDetailsDTO) {
        this.userAccountDetailsDTO = userAccountDetailsDTO;
    }
}
