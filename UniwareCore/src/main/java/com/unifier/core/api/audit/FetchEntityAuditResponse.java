/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 21-Jul-2014
 *  @author parijat
 */
package com.unifier.core.api.audit;

import java.util.List;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author parijat
 *
 */
public class FetchEntityAuditResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long    serialVersionUID = 5153044852841569743L;

    private List<EntityAuditDto> auditDtos;

    /**
     * @return the auditDtos
     */
    public List<EntityAuditDto> getAuditDtos() {
        return auditDtos;
    }

    /**
     * @param auditDtos the auditDtos to set
     */
    public void setAuditDtos(List<EntityAuditDto> auditDtos) {
        this.auditDtos = auditDtos;
    }

}
