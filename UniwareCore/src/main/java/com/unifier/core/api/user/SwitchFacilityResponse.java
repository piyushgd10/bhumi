/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Sep 29, 2012
 *  @author singla
 */
package com.unifier.core.api.user;

import com.unifier.core.api.base.ServiceResponse;

public class SwitchFacilityResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 4975810359432874825L;

    private boolean           currentUrlAccessible;

    /**
     * @return the currentUrlAccessible
     */
    public boolean isCurrentUrlAccessible() {
        return currentUrlAccessible;
    }

    /**
     * @param currentUrlAccessible the currentUrlAccessible to set
     */
    public void setCurrentUrlAccessible(boolean currentUrlAccessible) {
        this.currentUrlAccessible = currentUrlAccessible;
    }
}
