/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 *  @version     1.0, Mar 4, 2014
 *  @author amit
 */
package com.unifier.core.api.print;

/**
 * Minified version of {@code PrintTemplateDTO}. It has all the fields of {@code PrintTemplateDTO} except template.
 * 
 * @author Amit Dalal
 */
public class PrintTemplateMinDTO {

    private String  type;
    private String  name;
    private String  code;
    private String  description;
    private int     numberOfCopies;
    private String  printDialog;
    private String  pageSize;
    private String  pageMargins;
    private boolean landscape;
    private boolean enabled;
    private String  previewImagePath;
    private String  previewBigImagePath;
    private boolean selected;

    public PrintTemplateMinDTO() {
    }

    public PrintTemplateMinDTO(PrintTemplateDTO templateDTO) {
        setType(templateDTO.getType());
        setName(templateDTO.getName());
        setCode(templateDTO.getCode());
        setDescription(templateDTO.getDescription());
        setNumberOfCopies(templateDTO.getNumberOfCopies());
        setPrintDialog(templateDTO.getPrintDialog());
        setPageSize(templateDTO.getPageSize());
        setPageMargins(templateDTO.getPageMargins());
        setLandscape(templateDTO.isLandscape());
        setEnabled(templateDTO.isEnabled());
        setPreviewImagePath(templateDTO.getPreviewImagePath());
        setPreviewBigImagePath(templateDTO.getPreviewBigImagePath());
        setSelected(templateDTO.isSelected());
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getNumberOfCopies() {
        return numberOfCopies;
    }

    public void setNumberOfCopies(int numberOfCopies) {
        this.numberOfCopies = numberOfCopies;
    }

    public String getPrintDialog() {
        return printDialog;
    }

    public void setPrintDialog(String printDialog) {
        this.printDialog = printDialog;
    }

    public String getPageSize() {
        return pageSize;
    }

    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }

    public String getPageMargins() {
        return pageMargins;
    }

    public void setPageMargins(String pageMargins) {
        this.pageMargins = pageMargins;
    }

    public boolean isLandscape() {
        return landscape;
    }

    public void setLandscape(boolean landscape) {
        this.landscape = landscape;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getPreviewImagePath() {
        return previewImagePath;
    }

    public void setPreviewImagePath(String previewImagePath) {
        this.previewImagePath = previewImagePath;
    }

    public String getPreviewBigImagePath() {
        return previewBigImagePath;
    }

    public void setPreviewBigImagePath(String previewBigImagePath) {
        this.previewBigImagePath = previewBigImagePath;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        PrintTemplateMinDTO other = (PrintTemplateMinDTO) obj;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        if (type == null) {
            if (other.type != null)
                return false;
        } else if (!type.equals(other.type))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "PrintTemplateDTO [type=" + type + ", name=" + name + ", description=" + description + ", numberOfCopies=" + numberOfCopies + ", printDialog=" + printDialog
                + ", pageSize=" + pageSize + ", pageMargins=" + pageMargins + ", landscape=" + landscape + ", enabled=" + enabled + "]";
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

}
