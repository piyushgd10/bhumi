package com.unifier.core.api.myaccount;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by Samdeesh on 7/24/15.
 */
public class ProductApplicabilityDTO {

    private String     productCode;
    private String     productName;
    private String     productType;
    private String     description;
    private BigDecimal perUnitCharges;
    private BigDecimal sellingPrice;
    private boolean    payableByCredits;
    private Integer    validityInDays;
    private Date       expiryDate;
    private Integer    creditAmount;
    private String     termsAndConditions;

    public ProductApplicabilityDTO() {
        super();
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getPerUnitCharges() {
        return perUnitCharges;
    }

    public void setPerUnitCharges(BigDecimal perUnitCharges) {
        this.perUnitCharges = perUnitCharges;
    }

    public BigDecimal getSellingPrice() {
        return sellingPrice;
    }

    public void setSellingPrice(BigDecimal sellingPrice) {
        this.sellingPrice = sellingPrice;
    }

    public boolean isPayableByCredits() {
        return payableByCredits;
    }

    public void setPayableByCredits(boolean payableByCredits) {
        this.payableByCredits = payableByCredits;
    }

    public Integer getValidityInDays() {
        return validityInDays;
    }

    public void setValidityInDays(Integer validityInDays) {
        this.validityInDays = validityInDays;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    public Integer getCreditAmount() {
        return creditAmount;
    }

    public void setCreditAmount(Integer creditAmount) {
        this.creditAmount = creditAmount;
    }

    @Override
    public String toString() {
        return "ProductApplicabilityDTO{" + "productCode='" + productCode + '\'' + ",productName='" + productName + '\'' + ", productType='" + productType + '\''
                + ", description='" + description + '\'' + ", perUnitCharges=" + perUnitCharges + ", sellingPrice=" + sellingPrice + ", payableByCredits='" + payableByCredits
                + '\'' + ", validityInDays=" + validityInDays + '\'' + ", expiryDate=" + expiryDate + '\'' + ", creditAmount=" + creditAmount + '\'' + ", termsAndConditions=" + termsAndConditions + '}';
    }

    public String getTermsAndConditions() {
        return termsAndConditions;
    }

    public void setTermsAndConditions(String termsAndConditions) {
        this.termsAndConditions = termsAndConditions;
    }
}