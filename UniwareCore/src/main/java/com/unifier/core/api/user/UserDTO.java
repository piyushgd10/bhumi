/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 08-Feb-2012
 *  @author vibhu
 */
package com.unifier.core.api.user;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.unifier.core.cache.CacheManager;
import com.unifier.core.entity.Role;
import com.unifier.core.entity.User;
import com.unifier.core.entity.UserRole;
import com.uniware.core.cache.RolesCache;
import com.uniware.core.utils.UserContext;
import com.uniware.core.vo.UserProfileVO;

/**
 * @author vibhu
 */
public class UserDTO {

    private Integer             userId;
    private String              username;
    private String              name;
    private String              email;
    private String              mobile;
    private String              lastAccessedFrom;
    private boolean             verified;
    private boolean             enabled;
    private boolean             hidden;
    private Date                created;
    private Date                lastLoginTime;
    private List<String>        roles;
    private List<String>        tenantRoles;
    private List<FacilityRole>  facilityRoles;

    public UserDTO() {
    }

    public UserDTO(User user) {
        userId = user.getId();
        username = user.getUsername();
        name = user.getName();
        email = user.getEmail();
        mobile = user.getMobile();
        verified = true;
        enabled = user.isEnabled();
        hidden = user.isHidden();
        if(user.getLastAccessedFrom() != null){
            lastAccessedFrom = user.getLastAccessedFrom();
        }
        if(user.getLastLoginTime() != null){
            lastLoginTime = user.getLastLoginTime();
        }
        created = user.getCreated();
        roles = new ArrayList<String>();
        tenantRoles = new ArrayList<String>();
        facilityRoles = new ArrayList<FacilityRole>();
        Map<String, List<String>> facilityRolesMap = new HashMap<String, List<String>>();
        RolesCache cache = CacheManager.getInstance().getCache(RolesCache.class);
        for (UserRole userRole : user.getUserRoles()) {
            if (userRole.isEnabled() && !userRole.getRole().isHidden()) {
                if (userRole.getFacility() == null || userRole.getFacility().getId().equals(UserContext.current().getFacilityId())) {
                    roles.add(cache.getRole(userRole.getRole().getId()).getCode());
                }
                if (userRole.getRole().getLevel().equalsIgnoreCase(Role.Level.TENANT.name())) {
                    tenantRoles.add(cache.getRole(userRole.getRole().getId()).getCode());
                } else if (userRole.getRole().getLevel().equalsIgnoreCase(Role.Level.FACILITY.name())) {
                    if (facilityRolesMap.get(userRole.getFacility().getCode()) == null) {
                        facilityRolesMap.put(userRole.getFacility().getCode(), new ArrayList<String>());
                    }
                    facilityRolesMap.get(userRole.getFacility().getCode()).add(userRole.getRole().getCode());
                }
            }
        }
        facilityRoles = facilityRolesMapToList(facilityRolesMap);
        
    }

    public static class FacilityRole {

        private String       facilityCode;
        private List<String> roles;

        public FacilityRole() {

        }

        public FacilityRole(String facilityCode, List<String> roles) {
            this.facilityCode = facilityCode;
            this.roles = roles;
        }

        public String getFacilityCode() {
            return facilityCode;
        }

        public void setFacilityCode(String facilityCode) {
            this.facilityCode = facilityCode;
        }

        public List<String> getRoles() {
            return roles;
        }

        public void setRoles(List<String> roles) {
            this.roles = roles;
        }

        public void addRole(String role) {
            this.roles.add(role);
        }        
    
    }
    
    public Map<String,List<String>> facilityRolesListToMap(List<FacilityRole> facilityRoles) {
        Map<String,List<String>> facilityRolesMap = new HashMap<String,List<String>>();
        Iterator<FacilityRole> iterator = facilityRoles.iterator();
        while(iterator.hasNext()){
            FacilityRole facilityRole = iterator.next();
            facilityRolesMap.put(facilityRole.getFacilityCode(), facilityRole.getRoles());
        }
        return facilityRolesMap;
    }
    
    public List<FacilityRole> facilityRolesMapToList(Map<String,List<String>> facilityRolesMap) {
        List<FacilityRole> facilityRoles = new ArrayList<FacilityRole>();
        Iterator<Entry<String, List<String>>> iterator = facilityRolesMap.entrySet().iterator();
        while (iterator.hasNext()) {
            Entry<String, List<String>> row = iterator.next();
            facilityRoles.add(new FacilityRole(row.getKey(), row.getValue()));
        }
        return facilityRoles;
    }
    
    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the mobile
     */
    public String getMobile() {
        return mobile;
    }

    /**
     * @param mobile the mobile to set
     */
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    /**
     * @return the enabled
     */
    public boolean isEnabled() {
        return enabled;
    }

    /**
     * @param enabled the enabled to set
     */
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    /**
     * @return the roles
     */
    public List<String> getRoles() {
        return roles;
    }

    /**
     * @param roles the roles to set
     */
    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the tenantRoles
     */
    public List<String> getTenantRoles() {
        return tenantRoles;
    }

    /**
     * @param tenantRoles the tenantRoles to set
     */
    public void setTenantRoles(List<String> tenantRoles) {
        this.tenantRoles = tenantRoles;
    }

    /**
     * @return the facilityRoles
     */
    public List<FacilityRole> getFacilityRoles() {
        return facilityRoles;
    }

    /**
     * @param facilityRoles the facilityRoles to set
     */
    public void setFacilityRoles(List<FacilityRole> facilityRoles) {
        this.facilityRoles = facilityRoles;
    }

    public boolean isHidden() {
        return hidden;
    }

    public void setHidden(boolean hidden) {
        this.hidden = hidden;
    }

    public boolean isVerified() {
        return verified;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(Date lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    public String getLoginAccessedFrom() {
        return lastAccessedFrom;
    }

    public void setLoginAccessedFrom(String lastAccessedFrom) {
        this.lastAccessedFrom = lastAccessedFrom;
    }

}