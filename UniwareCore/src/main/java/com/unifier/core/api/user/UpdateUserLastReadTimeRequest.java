/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 26-Feb-2014
 *  @author unicom
 */
package com.unifier.core.api.user;

import javax.validation.constraints.NotNull;

import com.unifier.core.api.base.ServiceRequest;

public class UpdateUserLastReadTimeRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -2381575515505973735L;

    @NotNull
    private Integer           userId;

    @NotNull
    private Long              lastMessageReadTimeStamp;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Long getLastMessageReadTimeStamp() {
        return lastMessageReadTimeStamp;
    }

    public void setLastMessageReadTimeStamp(Long lastMessageReadTimeStamp) {
        this.lastMessageReadTimeStamp = lastMessageReadTimeStamp;
    }

}
