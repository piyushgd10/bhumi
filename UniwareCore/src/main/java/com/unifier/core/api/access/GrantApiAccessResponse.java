/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 17-Dec-2013
 *  @author akshay
 */
package com.unifier.core.api.access;

import com.unifier.core.api.base.ServiceResponse;

public class GrantApiAccessResponse extends ServiceResponse{

    /**
     * 
     */
    private static final long serialVersionUID = -6703988876212888287L;

    private String            redirectUrl;

    public String getRedirectUrl() {
        return redirectUrl;
    }

    public void setRedirectUrl(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }

}
