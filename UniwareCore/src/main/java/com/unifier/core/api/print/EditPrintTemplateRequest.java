/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 *  @version     1.0, Mar 8, 2012
 *  @author praveeng
 */
package com.unifier.core.api.print;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author Sunny Agarwal
 */
public class EditPrintTemplateRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 6233045698142868763L;

    @Valid
    @NotNull
    private WsPrintTemplate   wsPrintTemplate;

    public WsPrintTemplate getWsPrintTemplate() {
        return wsPrintTemplate;
    }

    public void setWsPrintTemplate(WsPrintTemplate wsPrintTemplate) {
        this.wsPrintTemplate = wsPrintTemplate;
    }

}
