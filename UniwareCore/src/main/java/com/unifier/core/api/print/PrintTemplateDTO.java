/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 *  @version     1.0, Mar 8, 2012
 *  @author praveeng
 */
package com.unifier.core.api.print;

import com.uniware.core.vo.SamplePrintTemplateVO;

import java.io.Serializable;

/**
 * @author Sunny Agarwal
 */
public class PrintTemplateDTO implements Serializable {

    private String  type;
    private String  name;
    private String  code;
    private String  description;
    private String  template;
    private int     numberOfCopies;
    private String  printDialog;
    private String  pageSize;
    private String  pageMargins;
    private boolean landscape;
    private boolean enabled;
    private String  previewImagePath;
    private String  previewBigImagePath;
    private boolean selected;
    private boolean global;

    public PrintTemplateDTO() {
    }

    public PrintTemplateDTO(SamplePrintTemplateVO sampleTemplate) {
        setName(sampleTemplate.getName());
        setType(sampleTemplate.getType());
        setCode(sampleTemplate.getCode());
        setDescription(sampleTemplate.getDescription());
        setTemplate(sampleTemplate.getTemplate());
        setNumberOfCopies(sampleTemplate.getNumberOfCopies());
        setPrintDialog(sampleTemplate.getPrintDialog());
        setPageMargins(sampleTemplate.getPageMargins());
        setPageSize(sampleTemplate.getPageSize());
        setLandscape(sampleTemplate.isLandscape());
        setEnabled(sampleTemplate.isEnabled());
        setPreviewImagePath(sampleTemplate.getPreviewImagePath());
        setPreviewBigImagePath(sampleTemplate.getPreviewBigImagePath());
        setGlobal(sampleTemplate.isGlobal());
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    public int getNumberOfCopies() {
        return numberOfCopies;
    }

    public void setNumberOfCopies(int numberOfCopies) {
        this.numberOfCopies = numberOfCopies;
    }

    public String getPrintDialog() {
        return printDialog;
    }

    public void setPrintDialog(String printDialog) {
        this.printDialog = printDialog;
    }

    public String getPageSize() {
        return pageSize;
    }

    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }

    public String getPageMargins() {
        return pageMargins;
    }

    public void setPageMargins(String pageMargins) {
        this.pageMargins = pageMargins;
    }

    public boolean isLandscape() {
        return landscape;
    }

    public void setLandscape(boolean landscape) {
        this.landscape = landscape;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getPreviewImagePath() {
        return previewImagePath;
    }

    public void setPreviewImagePath(String previewImagePath) {
        this.previewImagePath = previewImagePath;
    }

    public String getPreviewBigImagePath() {
        return previewBigImagePath;
    }

    public void setPreviewBigImagePath(String previewBigImagePath) {
        this.previewBigImagePath = previewBigImagePath;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        PrintTemplateDTO other = (PrintTemplateDTO) obj;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        if (type == null) {
            if (other.type != null)
                return false;
        } else if (!type.equals(other.type))
            return false;
        if (global != other.global) return false;
        return true;
    }

    @Override
    public String toString() {
        return "PrintTemplateDTO [type=" + type + ", name=" + name + ", description=" + description + ", template=" + template + ", numberOfCopies=" + numberOfCopies
                + ", printDialog=" + printDialog + ", pageSize=" + pageSize + ", pageMargins=" + pageMargins + ", landscape=" + landscape + ", enabled=" + enabled + ", global=" + global + "]";
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public boolean isGlobal() {
        return global;
    }

    public void setGlobal(boolean global) {
        this.global = global;
    }

}
