/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 12-Jun-2012
 *  @author praveeng
 */
package com.unifier.core.api.reports;

import com.unifier.core.api.base.ServiceRequest;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;


/**
 * @author praveeng
 */
public class RemoveWidgetRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 1747459474528402656L;

    @NotBlank
    private String            code;

    @NotNull
    private Integer           userId;

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

}
