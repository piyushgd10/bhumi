package com.unifier.core.api.myaccount;

import com.unifier.core.api.base.ServiceRequest;
import com.unifier.scraper.sl.parser.HttpNode;
import com.unifier.scraper.sl.runtime.HttpInstruction;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Samdeesh on 7/31/15.
 */
public class PipeToAccountsRequest extends ServiceRequest {

    private String                         url;
    private String                         method;
    private ArrayList<HttpNode.HttpParam>  params;
    private ArrayList<HttpNode.HttpHeader> headers;
    private String                         body;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public ArrayList<HttpNode.HttpParam> getParams() {
        return params;
    }

    public void setParams(ArrayList<HttpNode.HttpParam> params) {
        this.params = params;
    }

    public ArrayList<HttpNode.HttpHeader> getHeaders() {
        return headers;
    }

    public void setHeaders(ArrayList<HttpNode.HttpHeader> headers) {
        this.headers = headers;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
