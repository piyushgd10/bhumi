/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 22-May-2012
 *  @author vibhu
 */
package com.unifier.core.api.advanced;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author vibhu
 */
public class CreateRoleResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -2546279004744950151L;

    private String            roleCode;

    /**
     * @return the roleCode
     */
    public String getRoleCode() {
        return roleCode;
    }

    /**
     * @param roleCode the roleCode to set
     */
    public void setRoleCode(String roleCode) {
        this.roleCode = roleCode;
    }

}