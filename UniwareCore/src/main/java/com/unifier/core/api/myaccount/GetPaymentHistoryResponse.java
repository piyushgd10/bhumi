/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 23-Feb-2015
 *  @author akshay
 */
package com.unifier.core.api.myaccount;

import com.unifier.core.api.base.ServiceResponse;

import java.util.ArrayList;
import java.util.List;

public class GetPaymentHistoryResponse extends ServiceResponse {

    /**
     *
     */
    private static final long serialVersionUID = -4116179477583138679L;

    private List<PaymentHistoryDTO> paymentHistory = new ArrayList<PaymentHistoryDTO>();

    private Long resultCount;

    public List<PaymentHistoryDTO> getPaymentHistory() {
        return paymentHistory;
    }

    public void setPaymentHistory(List<PaymentHistoryDTO> paymentHistory) {
        this.paymentHistory = paymentHistory;
    }

    public Long getResultCount() {
        return resultCount;
    }

    public void setResultCount(Long resultCount) {
        this.resultCount = resultCount;
    }

    @Override public String toString() {
        return "GetPaymentHistoryResponse{" +
                "paymentHistory=" + paymentHistory +
                '}';
    }
}
