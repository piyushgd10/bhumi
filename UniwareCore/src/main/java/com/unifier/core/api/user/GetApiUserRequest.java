/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 25-Feb-2014
 *  @author unicom
 */
package com.unifier.core.api.user;

import javax.validation.constraints.NotNull;

import com.unifier.core.api.base.ServiceRequest;

public class GetApiUserRequest extends ServiceRequest{

    /**
     * 
     */
    private static final long serialVersionUID = -7029867158780806212L;

    @NotNull
    private Integer           userId;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }
}
