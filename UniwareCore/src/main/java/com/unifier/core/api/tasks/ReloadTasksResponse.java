/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Apr 28, 2015
 *  @author harsh
 */
package com.unifier.core.api.tasks;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author harsh
 */
public class ReloadTasksResponse extends ServiceResponse {

    private static final long serialVersionUID = 488928057254215577L;

}
