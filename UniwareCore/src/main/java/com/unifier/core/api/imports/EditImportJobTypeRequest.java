package com.unifier.core.api.imports;

import java.util.List;

import com.unifier.core.api.base.ServiceRequest;

public class EditImportJobTypeRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -4590904391225236889L;
    private String            name;
    private Boolean           enabled;
    private String            importJobConfig;
    private List<String>      importOptions;

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the enabled
     */
    public boolean isEnabled() {
        return enabled;
    }

    /**
     * @param enabled the enabled to set
     */
    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    /**
     * @return the importJobConfig
     */
    public String getImportJobConfig() {
        return importJobConfig;
    }

    /**
     * @param importJobConfig the importJobConfig to set
     */
    public void setImportJobConfig(String importJobConfig) {
        this.importJobConfig = importJobConfig;
    }

    /**
     * @return the importOptions
     */
    public List<String> getImportOptions() {
        return importOptions;
    }

    /**
     * @param importOptions the importOptions to set
     */
    public void setImportOptions(List<String> importOptions) {
        this.importOptions = importOptions;
    }

}