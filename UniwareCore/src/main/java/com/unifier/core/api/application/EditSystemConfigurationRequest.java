/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 *  @version     1.0, 23-Jul-2012
 *  @author praveeng
 */
package com.unifier.core.api.application;

import com.unifier.core.api.base.ServiceRequest;

import org.hibernate.validator.constraints.NotBlank;


/**
 * @author praveeng
 */
public class EditSystemConfigurationRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 6994909105141319969L;
    @NotBlank
    private String            name;

    @NotBlank
    private String            value;

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(String value) {
        this.value = value;
    }

}
