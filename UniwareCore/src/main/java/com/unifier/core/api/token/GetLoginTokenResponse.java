/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 22-May-2012
 *  @author vibhu
 */
package com.unifier.core.api.token;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author Sunny Agarwal
 */
public class GetLoginTokenResponse extends ServiceResponse {

    private static final long serialVersionUID = 2341768549869164155L;

    private String            token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

}