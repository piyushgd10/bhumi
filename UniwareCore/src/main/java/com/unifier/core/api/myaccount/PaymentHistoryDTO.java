/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 23-Feb-2015
 *  @author akshay
 */

package com.unifier.core.api.myaccount;

import java.math.BigDecimal;

public class PaymentHistoryDTO {

    private String     orderId;
    private String     transactionId;
    private String     invoiceCode;
    private String     statusCode;
    private BigDecimal paymentAmount;
    private BigDecimal pointsCredited = new BigDecimal(0);
    private String     paymentMode;
    private String     paymentReceiptLink;
    private Long       transactionDate;

    public PaymentHistoryDTO() {
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getInvoiceCode() {
        return invoiceCode;
    }

    public void setInvoiceCode(String invoiceCode) {
        this.invoiceCode = invoiceCode;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public BigDecimal getPaymentAmount() {
        return paymentAmount;
    }

    public void setPaymentAmount(BigDecimal paymentAmount) {
        this.paymentAmount = paymentAmount;
    }

    public BigDecimal getPointsCredited() {
        return pointsCredited;
    }

    public void setPointsCredited(BigDecimal pointsCredited) {
        this.pointsCredited = pointsCredited;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public Long getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Long transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getPaymentReceiptLink() {
        return paymentReceiptLink;
    }

    public void setPaymentReceiptLink(String paymentReceiptLink) {
        this.paymentReceiptLink = paymentReceiptLink;
    }

    @Override
    public String toString() {
        return "PaymentHistoryDTO{" + "orderId='" + orderId + '\'' + ", transactionId='" + transactionId + '\'' + ", statusCode='" + statusCode + '\'' + ", paymentAmount="
                + paymentAmount + ", pointsCredited=" + pointsCredited + ", paymentMode='" + paymentMode + '\'' + ", transactionDate=" + transactionDate + '}';
    }
}
