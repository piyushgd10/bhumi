/*
 * Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 8/4/15 4:44 PM
 * @author amdalal
 */

package com.unifier.core.api.external;

import com.unifier.core.api.base.ServiceRequest;

public class GetUnideskTokenRequest extends ServiceRequest {

    private static final long serialVersionUID = 312109924268655151L;

    private String            username;

    private String            name;

    private String            mobile;

    private String            productType;

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
