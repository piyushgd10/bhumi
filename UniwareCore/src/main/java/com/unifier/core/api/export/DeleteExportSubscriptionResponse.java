/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 04-Mar-2013
 *  @author unicom
 */
package com.unifier.core.api.export;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author unicom
 *
 */
public class DeleteExportSubscriptionResponse extends ServiceResponse{

    /**
     * 
     */
    private static final long serialVersionUID = -904757341332629165L;

}
