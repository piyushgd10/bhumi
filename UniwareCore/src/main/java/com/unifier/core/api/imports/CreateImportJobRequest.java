/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 21, 2012
 *  @author praveeng
 */
package com.unifier.core.api.imports;

import com.unifier.core.api.base.ServiceRequest;
import com.unifier.core.entity.ImportJobType;

import java.io.InputStream;
import java.util.Date;

import javax.validation.constraints.Future;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.URL;

/**
 * @author praveeng
 */
public class CreateImportJobRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 2656649580693756257L;

    @URL
    private String            fileUrl;

    @NotNull
    private InputStream       inputStream;

    @NotNull
    private String            fileName;

    @NotEmpty
    private String            importJobTypeName;

    @NotNull
    private Integer           userId;

    @Email
    private String            notificationEmail;

    @Future
    private Date              scheduleTime;

    @NotBlank
    private String            importOption     = ImportJobType.ImportOptions.CREATE_NEW_AND_UPDATE_EXISTING.name();

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return the notificationEmail
     */
    public String getNotificationEmail() {
        return notificationEmail;
    }

    /**
     * @param notificationEmail the notificationEmail to set
     */
    public void setNotificationEmail(String notificationEmail) {
        this.notificationEmail = notificationEmail;
    }

    /**
     * @return the scheduleTime
     */
    public Date getScheduleTime() {
        return scheduleTime;
    }

    /**
     * @param scheduleTime the scheduleTime to set
     */
    public void setScheduleTime(Date scheduleTime) {
        this.scheduleTime = scheduleTime;
    }

    /**
     * @return the importJobTypeName
     */
    public String getImportJobTypeName() {
        return importJobTypeName;
    }

    /**
     * @param importJobTypeName the importJobTypeName to set
     */
    public void setImportJobTypeName(String importJobTypeName) {
        this.importJobTypeName = importJobTypeName;
    }

    /**
     * @return the inputStream
     */
    public InputStream getInputStream() {
        return inputStream;
    }

    /**
     * @param inputStream the inputStream to set
     */
    public void setInputStream(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    /**
     * @return the importOption
     */
    public String getImportOption() {
        return importOption;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    /**
     * @param importOption the importOption to set
     */
    public void setImportOption(String importOption) {
        this.importOption = importOption;
    }

    @Override
    public String toString() {
        return "CreateImportJobRequest{" +
                "fileName='" + fileName + '\'' +
                ", importJobTypeName='" + importJobTypeName + '\'' +
                '}';
    }
}
