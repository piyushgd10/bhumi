/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 07-Apr-2015
 *  @author unicom
 */
package com.unifier.core.api.release;

import com.unifier.core.api.base.ServiceResponse;

public class MarkReleaseUpdateReadResponse extends ServiceResponse{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
}
