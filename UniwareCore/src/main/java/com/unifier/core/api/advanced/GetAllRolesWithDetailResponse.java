/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 09-Mar-2015
 *  @author unicommerce
 */
package com.unifier.core.api.advanced;

import java.util.List;

import com.unifier.core.api.base.ServiceResponse;

public class GetAllRolesWithDetailResponse extends ServiceResponse{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    List<RoleDTO> roleDTOs;
 
    public List<RoleDTO> getRoleDTOs(){
        return roleDTOs;
    }
    
    public void setRoleDTOs(List<RoleDTO> roleDTOs){
        this.roleDTOs = roleDTOs;
    }
}