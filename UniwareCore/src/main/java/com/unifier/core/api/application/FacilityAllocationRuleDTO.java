/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 01-Oct-2012
 *  @author vibhu
 */
package com.unifier.core.api.application;

import com.uniware.core.entity.FacilityAllocationRule;

/**
 * @author vibhu
 */
public class FacilityAllocationRuleDTO {
    private Integer id;
    private String  facilityCode;
    private String  name;
    private String  conditionExpressionText;
    private int     preference;
    private boolean enabled;

    /**
     * @param facilityAllocationRule
     */
    public FacilityAllocationRuleDTO(FacilityAllocationRule facilityAllocationRule) {
        this.id = facilityAllocationRule.getId();
        this.facilityCode = facilityAllocationRule.getFacility().getCode();
        this.name = facilityAllocationRule.getName();
        this.conditionExpressionText = facilityAllocationRule.getConditionExpressionText();
        this.preference = facilityAllocationRule.getPreference();
        this.enabled = facilityAllocationRule.isEnabled();
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the facilityName
     */
    public String getFacilityCode() {
        return facilityCode;
    }

    /**
     * @param facilityName the facilityName to set
     */
    public void setFacilityName(String facilityCode) {
        this.facilityCode = facilityCode;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the conditionExpressionText
     */
    public String getConditionExpressionText() {
        return conditionExpressionText;
    }

    /**
     * @param conditionExpressionText the conditionExpressionText to set
     */
    public void setConditionExpressionText(String conditionExpressionText) {
        this.conditionExpressionText = conditionExpressionText;
    }

    /**
     * @return the preference
     */
    public int getPreference() {
        return preference;
    }

    /**
     * @param preference the preference to set
     */
    public void setPreference(int preference) {
        this.preference = preference;
    }

    /**
     * @return the enabled
     */
    public boolean isEnabled() {
        return enabled;
    }

    /**
     * @param enabled the enabled to set
     */
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}
