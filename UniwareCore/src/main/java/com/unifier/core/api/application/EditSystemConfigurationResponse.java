/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 23-Jul-2012
 *  @author praveeng
 */
package com.unifier.core.api.application;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author praveeng
 */
public class EditSystemConfigurationResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 3609964976096684195L;

}
