package com.unifier.core.api.myaccount;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.unifier.core.api.base.ServiceResponse;
/**
 * Created by admin on 11/4/15.
 */
public class GetLastLoginDetailsResponse extends ServiceResponse {

    private static final long serialVersionUID = 7517254789393183979L;

    private String lastAccessedFrom;
    private Date   lastLoginTime;

    public String getLastAccessedFrom() {
        return lastAccessedFrom;
    }

    public void setLastAccessedFrom(String lastAccessedFrom) {
        this.lastAccessedFrom = lastAccessedFrom;
    }

    public Date getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(Date lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

}
