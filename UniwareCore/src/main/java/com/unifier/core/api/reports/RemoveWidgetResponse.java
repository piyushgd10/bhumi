/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 12-Jun-2012
 *  @author praveeng
 */
package com.unifier.core.api.reports;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author praveeng
 */
public class RemoveWidgetResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 1921173195218151266L;

}
