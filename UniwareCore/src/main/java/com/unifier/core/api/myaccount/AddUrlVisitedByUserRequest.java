/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 03-July-2015
 *  @author akshaykochhar
 */
package com.unifier.core.api.myaccount;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

public class AddUrlVisitedByUserRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -951972082785002090L;
    
    @NotBlank
    private String username;
    
    @NotBlank
    private String visitedUrl;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getVisitedUrl() {
        return visitedUrl;
    }

    public void setVisitedUrl(String visitedUrl) {
        this.visitedUrl = visitedUrl;
    }
    
}
