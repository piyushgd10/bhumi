/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Nov 22, 2012
 *  @author Pankaj
 */
package com.unifier.core.api.tasks;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author Pankaj
 */
public class UpdateTaskResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -7786154934644819475L;

    private String            task;

    /**
     * @return the task
     */
    public String getTask() {
        return task;
    }

    /**
     * @param task the task to set
     */
    public void setTask(String task) {
        this.task = task;
    }

}
