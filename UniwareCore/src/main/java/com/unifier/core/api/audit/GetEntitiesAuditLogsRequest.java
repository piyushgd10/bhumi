/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 25-Apr-2014
 *  @author parijat
 */
package com.unifier.core.api.audit;

import com.unifier.core.api.base.ServiceRequest;

public class GetEntitiesAuditLogsRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -8025895826716190272L;

    private boolean           filterByFacility;

    /**
     * @return the filterByFacility
     */
    public boolean isFilterByFacility() {
        return filterByFacility;
    }

    /**
     * @param filterByFacility the filterByFacility to set
     */
    public void setFilterByFacility(boolean filterByFacility) {
        this.filterByFacility = filterByFacility;
    }

}
