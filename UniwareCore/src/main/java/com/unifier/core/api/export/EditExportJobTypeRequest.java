/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 24-May-2012
 *  @author praveeng
 */
package com.unifier.core.api.export;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author praveeng
 */
public class EditExportJobTypeRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 8409968197048633114L;
    private String            name;
    private boolean           enabled;
    private String            exportJobConfig;

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the enabled
     */
    public boolean isEnabled() {
        return enabled;
    }

    /**
     * @param enabled the enabled to set
     */
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    /**
     * @return the exportJobConfig
     */
    public String getExportJobConfig() {
        return exportJobConfig;
    }

    /**
     * @param exportJobConfig the exportJobConfig to set
     */
    public void setExportJobConfig(String exportJobConfig) {
        this.exportJobConfig = exportJobConfig;
    }

}
