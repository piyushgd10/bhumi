/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 *  @version     1.0, Mar 8, 2012
 *  @author praveeng
 */
package com.unifier.core.api.print;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author Sunny Agarwal
 */
public class AddPrintTemplateRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 1552640476121715261L;

    @NotBlank
    private String            type;
    @NotBlank
    private String            name;

    private boolean           global;

    public AddPrintTemplateRequest() {
    }

    public AddPrintTemplateRequest(String type, String name, boolean global) {
        this.type = type;
        this.name = name;
        this.global = global;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isGlobal() {
        return global;
    }

    public void setGlobal(boolean global) {
        this.global = global;
    }

}
