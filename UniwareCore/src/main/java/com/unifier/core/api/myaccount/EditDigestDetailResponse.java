/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 29-Mar-2012
 *  @author vibhu
 */
package com.unifier.core.api.myaccount;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author AkshayAg
 */

public class EditDigestDetailResponse extends ServiceResponse {

    /**
     *
     */
    private static final long serialVersionUID = 7517254789393183999L;

    private UserAccountDetailsDTO userAccountDetailsDTO;

    /**
     * @return the myAccountSettingDTO
     */
    public UserAccountDetailsDTO getUserAccountDetailsDTO() {
        return userAccountDetailsDTO;
    }

    /**
     * @param userAccountDetailsDTO the myAccountSettingDTO to set
     */
    public void setUserAccountDetailsDTO(UserAccountDetailsDTO userAccountDetailsDTO) {
        this.userAccountDetailsDTO = userAccountDetailsDTO;
    }

}
