/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 *  @version     1.0, Mar 8, 2012
 *  @author praveeng
 */
package com.unifier.core.api.print;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author Sunny Agarwal
 */
public class GetSamplePrintTemplatesByTypeRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -7436745272575738331L;
    @NotBlank
    private String            type;

    public GetSamplePrintTemplatesByTypeRequest() {
    }

    public GetSamplePrintTemplatesByTypeRequest(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}
