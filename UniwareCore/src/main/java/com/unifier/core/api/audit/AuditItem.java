/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 24-Apr-2014
 *  @author parijat
 */
package com.unifier.core.api.audit;

import java.util.HashMap;
import java.util.Map;

/**
 * @author parijat This class is used as at entity level audit logging
 */
public class AuditItem {

    private String entityId;
    private String entityName;
    private String identifier;
    private String groupIdentifier;
    private Map<String, AuditItemField> auditFields = new HashMap<String, AuditItemField>();


    /**
     * @return the entityId
     */
    public String getEntityId() {
        return entityId;
    }

    /**
     * @param entityId the entityId to set
     */
    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    /**
     * @return the entityName
     */
    public String getEntityName() {
        return entityName;
    }

    /**
     * @param entityName the entityName to set
     */
    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    /**
     * @return the identifier
     */
    public String getIdentifier() {
        return identifier;
    }

    /**
     * @param identifier the identifier to set
     */
    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    /**
     * @return the groupIdentifier
     */
    public String getGroupIdentifier() {
        return groupIdentifier;
    }

    /**
     * @param groupIdentifier the groupIdentifier to set
     */
    public void setGroupIdentifier(String groupIdentifier) {
        this.groupIdentifier = groupIdentifier;
    }

    /**
     * @return the auditFields
     */
    public Map<String, AuditItemField> getAuditFields() {
        return auditFields;
    }

    /**
     * @param auditFields the auditFields to set
     */
    public void setAuditFields(Map<String, AuditItemField> auditFields) {
        this.auditFields = auditFields;
    }

}
