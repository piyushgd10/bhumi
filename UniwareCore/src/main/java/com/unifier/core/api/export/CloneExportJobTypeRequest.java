/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 03-Dec-2013
 *  @author akshay
 */
package com.unifier.core.api.export;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

public class CloneExportJobTypeRequest extends ServiceRequest{

    /**
     * 
     */
    private static final long serialVersionUID = 6307036883562297815L;
    @NotBlank
    private String            cloneExportJobTypeName;
    @NotBlank
    private String            exportJobTypeName;

    public String getCloneExportJobTypeName() {
        return cloneExportJobTypeName;
    }

    public void setCloneExportJobTypeName(String cloneExportJobTypeName) {
        this.cloneExportJobTypeName = cloneExportJobTypeName;
    }

    public String getExportJobTypeName() {
        return exportJobTypeName;
    }

    public void setExportJobTypeName(String exportJobTypeName) {
        this.exportJobTypeName = exportJobTypeName;
    }

}
