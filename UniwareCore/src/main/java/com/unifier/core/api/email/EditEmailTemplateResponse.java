/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Apr 18, 2012
 *  @author praveeng
 */
package com.unifier.core.api.email;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author praveeng
 */
public class EditEmailTemplateResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 9004598608450659544L;

}
