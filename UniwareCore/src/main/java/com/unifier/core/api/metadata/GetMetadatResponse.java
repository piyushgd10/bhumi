/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 21-Nov-2013
 *  @author sunny
 */
package com.unifier.core.api.metadata;

import com.unifier.core.api.base.ServiceResponse;

public class GetMetadatResponse<T> extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 6487173339670904526L;

    private T                 data;

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

}
