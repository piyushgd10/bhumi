/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 05/08/14
 *  @author amit
 */

package com.unifier.core.api.templatecustomization;

import com.unifier.core.api.base.ServiceRequest;
import com.uniware.core.vo.PrintTemplateVO;

import javax.validation.constraints.NotNull;
import java.util.HashMap;
import java.util.Map;

public class CustomizeTemplateRequest extends ServiceRequest {

    private static final long serialVersionUID = -2288216951761906020L;

    @NotNull
    private PrintTemplateVO.Type templateType;

    @NotNull
    private Map<String, String>  customizationParameterMap = new HashMap<>();
    
    private String               samplePrintTemplateCode;

    public CustomizeTemplateRequest() {
        super();
    }

    public PrintTemplateVO.Type getTemplateType() {
        return templateType;
    }

    public void setTemplateType(PrintTemplateVO.Type templateType) {
        this.templateType = templateType;
    }

    public Map<String, String> getCustomizationParameterMap() {
        return customizationParameterMap;
    }

    public void setCustomizationParameterMap(Map<String, String> customizationParameterMap) {
        this.customizationParameterMap = customizationParameterMap;
    }

    @Override
    public String toString() {
        return "CustomizeTemplateRequest{" + "templateType=" + templateType + ", customizationParameterMap=" + customizationParameterMap + '}';
    }

    public String getSamplePrintTemplateCode() {
        return samplePrintTemplateCode;
    }

    public void setSamplePrintTemplateCode(String samplePrintTemplateCode) {
        this.samplePrintTemplateCode = samplePrintTemplateCode;
    }
}