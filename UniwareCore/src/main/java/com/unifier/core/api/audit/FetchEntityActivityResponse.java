/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 25-Apr-2014
 *  @author parijat
 */
package com.unifier.core.api.audit;

import java.util.List;

import com.unifier.core.api.base.ServiceResponse;
import com.uniware.core.vo.ActivityMetaVO;

public class FetchEntityActivityResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 1598451239441708473L;

    private List<ActivityMetaVO> entityActivityLogs;

    /**
     * @return the auditDto
     */
    public List<ActivityMetaVO> getEntityActivityLogs() {
        return entityActivityLogs;
    }

    /**
     * @param auditDto the auditDto to set
     */
    public void setEntityActivityLogs(List<ActivityMetaVO> identifierActivityLogs) {
        this.entityActivityLogs = identifierActivityLogs;
    }


}
