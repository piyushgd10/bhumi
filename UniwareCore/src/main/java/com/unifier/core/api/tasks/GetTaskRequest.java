/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Nov 21, 2012
 *  @author Pankaj
 */
package com.unifier.core.api.tasks;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author Pankaj
 */
public class GetTaskRequest extends ServiceRequest {

    /**
     * 
     */

    private static final long serialVersionUID = -4695775922763505109L;

    private String            taskName;

    /**
     * @return the taskName
     */
    public String getTaskName() {
        return taskName;
    }

    /**
     * @param taskName the taskName to set
     */
    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

}