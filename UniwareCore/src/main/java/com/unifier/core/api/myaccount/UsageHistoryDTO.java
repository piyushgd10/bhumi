/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 23-Feb-2015
 *  @author akshay
 */
package com.unifier.core.api.myaccount;

import com.unifier.core.utils.DateUtils;

import java.math.BigDecimal;
import java.util.Date;

public class UsageHistoryDTO {

    public static class UsageDTO {
        private BigDecimal totalCharges;
        private BigDecimal perUnitCharge;
        private Long       billedUnits;

        public UsageDTO(BigDecimal totalCharges, Long billedUnits, BigDecimal perUnitCharge) {
            this.totalCharges = totalCharges;
            this.billedUnits = billedUnits;
            this.perUnitCharge = perUnitCharge;
        }

        public BigDecimal getTotalCharges() {
            return totalCharges;
        }

        public void setTotalCharges(BigDecimal totalCharges) {
            this.totalCharges = totalCharges;
        }

        public Long getBilledUnits() {
            return billedUnits;
        }

        public void setBilledUnits(Long billedUnits) {
            this.billedUnits = billedUnits;
        }

        public BigDecimal getPerUnitCharge() {
            return perUnitCharge;
        }

        public void setPerUnitCharge(BigDecimal perUnitCharge) {
            this.perUnitCharge = perUnitCharge;
        }

        @Override public String toString() {
            return "UsageDTO{" +
                    "totalCharges=" + totalCharges +
                    ", perUnitCharge=" + perUnitCharge +
                    ", billedUnits=" + billedUnits +
                    '}';
        }
    }

    public enum Type {
        INVOICE,
        MONTHLY_CHARGES,
        MONTHLY_USAGE
    }

    private Long invoiceDate;

    private UsageDTO usage;

    private String type;

    public UsageHistoryDTO() {
    }

    public UsageHistoryDTO(String dateString, BigDecimal totalCharges, Long billedUnits, BigDecimal perUnitCharges) {
        Date date = DateUtils.stringToDate(dateString, "MM-yyyy");
        this.invoiceDate = date.getTime();
        usage = new UsageDTO(totalCharges, billedUnits, perUnitCharges);
        this.type = Type.INVOICE.name();
    }

    public UsageHistoryDTO(Date date, BigDecimal totalCharges, Long billedUnits, BigDecimal perUnitCharges) {
        this.invoiceDate = date.getTime();
        usage = new UsageDTO(totalCharges, billedUnits, perUnitCharges);
        this.type = Type.INVOICE.name();
    }

    public Long getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(Long invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public UsageDTO getUsage() {
        return usage;
    }

    public void setUsage(UsageDTO usage) {
        this.usage = usage;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "UsageHistoryDTO{" +
                "invoiceDate=" + invoiceDate +
                ", usage=" + usage +
                ", type='" + type + '\'' +
                '}';
    }
}
