package com.unifier.core.api.myaccount;

import com.unifier.core.api.base.ServiceRequest;
import org.hibernate.validator.constraints.NotBlank;

public class GetPaymentHistoryRequest extends ServiceRequest {

    /**
     *
     */
    private static final long serialVersionUID = -7470883407520319839L;

    private int start;

    private int noOfResults = 50;

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getNoOfResults() {
        return noOfResults;
    }

    public void setNoOfResults(int noOfResults) {
        this.noOfResults = noOfResults;
    }

}
