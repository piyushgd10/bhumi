/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 08-Feb-2012
 *  @author vibhu
 */
package com.unifier.core.api.user;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.validator.constraints.NotEmpty;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author vibhu
 */
public class AddUserRolesRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID  = 1L;

    @NotEmpty
    private String            username;

    private List<String>      facilityCodes     = new ArrayList<String>();

    private List<String>      tenantRoleCodes   = new ArrayList<String>();

    private List<String>      facilityRoleCodes = new ArrayList<String>();

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the facilityCodes
     */
    public List<String> getFacilityCodes() {
        return facilityCodes;
    }

    /**
     * @param facilityCodes the facilityCodes to set
     */
    public void setFacilityCodes(List<String> facilityCodes) {
        this.facilityCodes = facilityCodes;
    }

    /**
     * @return the tenantRoleCodes
     */
    public List<String> getTenantRoleCodes() {
        return tenantRoleCodes;
    }

    /**
     * @param tenantRoleCodes the tenantRoleCodes to set
     */
    public void setTenantRoleCodes(List<String> tenantRoleCodes) {
        this.tenantRoleCodes = tenantRoleCodes;
    }

    /**
     * @return the facilityRoleCodes
     */
    public List<String> getFacilityRoleCodes() {
        return facilityRoleCodes;
    }

    /**
     * @param facilityRoleCodes the facilityRoleCodes to set
     */
    public void setFacilityRoleCodes(List<String> facilityRoleCodes) {
        this.facilityRoleCodes = facilityRoleCodes;
    }

    public void addFacilityCode(String facilityCode) {
        if (facilityCodes == null) {
            facilityCodes = new ArrayList<String>();
        }
        facilityCodes.add(facilityCode);
    }

    public void addFacilityRoleCode(String facilityRoleCode) {
        if (facilityRoleCodes == null) {
            facilityRoleCodes = new ArrayList<String>();
        }
        facilityRoleCodes.add(facilityRoleCode);
    }

    public void addTenantRoleCode(String tenantRoleCode) {
        if (tenantRoleCodes == null) {
            tenantRoleCodes = new ArrayList<String>();
        }
        tenantRoleCodes.add(tenantRoleCode);
    }

}