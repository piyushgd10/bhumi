/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 21, 2012
 *  @author praveeng
 */
package com.unifier.core.api.imports;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author praveeng
 */
public class CreateImportJobResponse extends ServiceResponse {

    /**
     *
     */
    private static final long serialVersionUID = -7738449598865280173L;
    private String            importJobId;

    public String getImportJobId() {
        return importJobId;
    }

    public void setImportJobId(String importJobId) {
        this.importJobId = importJobId;
    }

}
