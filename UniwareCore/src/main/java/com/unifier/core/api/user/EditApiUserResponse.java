/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 27-Feb-2014
 *  @author akshay
 */
package com.unifier.core.api.user;

import com.unifier.core.api.base.ServiceResponse;

public class EditApiUserResponse extends ServiceResponse {


    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private ApiUserDTO        apiUserDTO;

    public ApiUserDTO getApiUserDTO() {
        return apiUserDTO;
    }

    public void setApiUserDTO(ApiUserDTO apiUserDTO) {
        this.apiUserDTO = apiUserDTO;
    }
}
