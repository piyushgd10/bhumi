/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jun 29, 2012
 *  @author singla
 */
package com.unifier.core.api.comments;

import com.unifier.core.api.base.ServiceRequest;

import javax.validation.constraints.NotNull;


public class DeleteCommentRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 8510171696973368865L;

    @NotNull
    private Integer           userId;

    @NotNull
    private Integer           commentId;

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return the commentId
     */
    public Integer getCommentId() {
        return commentId;
    }

    /**
     * @param commentId the commentId to set
     */
    public void setCommentId(Integer commentId) {
        this.commentId = commentId;
    }
}
