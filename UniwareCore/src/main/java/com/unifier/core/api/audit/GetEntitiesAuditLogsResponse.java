/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 25-Apr-2014
 *  @author parijat
 */
package com.unifier.core.api.audit;

import java.util.List;

import com.unifier.core.api.base.ServiceResponse;

public class GetEntitiesAuditLogsResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long    serialVersionUID = 818187611738563002L;

    private List<EntityAuditDto> entitiesAuditLogs;

    /**
     * @return the entitiesAuditLogs
     */
    public List<EntityAuditDto> getEntitiesAuditLogs() {
        return entitiesAuditLogs;
    }

    /**
     * @param entitiesAuditLogs the entitiesAuditLogs to set
     */
    public void setEntitiesAuditLogs(List<EntityAuditDto> entitiesAuditLogs) {
        this.entitiesAuditLogs = entitiesAuditLogs;
    }

}
