/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 12-Jun-2012
 *  @author praveeng
 */
package com.unifier.core.api.reports;

import com.unifier.core.api.base.ServiceRequest;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;


/**
 * @author praveeng
 */
public class ReorderWidgetsRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -1698137337013755682L;

    @NotNull
    private Integer           userId;

    @Valid
    private List<WsWidget>    widgets          = new ArrayList<WsWidget>();

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return the widgets
     */
    public List<WsWidget> getWidgets() {
        return widgets;
    }

    /**
     * @param widgets the widgets to set
     */
    public void setWidgets(List<WsWidget> widgets) {
        this.widgets = widgets;
    }

    public static class WsWidget {
        @NotBlank
        private String  code;

        @NotNull
        private Integer sequence;

        /**
         * @return the code
         */
        public String getCode() {
            return code;
        }

        /**
         * @param code the code to set
         */
        public void setCode(String code) {
            this.code = code;
        }

        /**
         * @return the sequence
         */
        public Integer getSequence() {
            return sequence;
        }

        /**
         * @param sequence the sequence to set
         */
        public void setSequence(Integer sequence) {
            this.sequence = sequence;
        }

    }

}
