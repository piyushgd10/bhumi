/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 24-May-2012
 *  @author vibhu
 */
package com.unifier.core.api.advanced;

import com.unifier.core.api.base.ServiceRequest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.constraints.NotNull;

/**
 * @author vibhu
 */
public class EditRoleResourceMappingRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long     serialVersionUID        = 9035390907485164889L;

    @NotNull
    private Integer               roleId;

    private List<String>          newAccessResourceNames  = new ArrayList<String>();
    
    private Map<String, Boolean>  updateAccessResourceNames = new HashMap<String, Boolean>();

    private List<Integer>         newAccessResourceIds    = new ArrayList<Integer>();

    private Map<Integer, Boolean> updateAccessResourceIds = new HashMap<Integer, Boolean>();

    /**
     * @return the newAccessResourceIds
     */
    public List<Integer> getNewAccessResourceIds() {
        return newAccessResourceIds;
    }

    /**
     * @param newAccessResourceIds the newAccessResourceIds to set
     */
    public void setNewAccessResourceIds(List<Integer> newAccessResourceIds) {
        this.newAccessResourceIds = newAccessResourceIds;
    }

    /**
     * @return the updateAccessResourceIds
     */
    public Map<Integer, Boolean> getUpdateAccessResourceIds() {
        return updateAccessResourceIds;
    }

    /**
     * @param updateAccessResourceIds the updateAccessResourceIds to set
     */
    public void setUpdateAccessResourceIds(Map<Integer, Boolean> updateAccessResourceIds) {
        this.updateAccessResourceIds = updateAccessResourceIds;
    }

    /**
     * @return the roleId
     */
    public Integer getRoleId() {
        return roleId;
    }

    /**
     * @param roleId the roleId to set
     */
    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public List<String> getNewAccessResourceNames() {
        return newAccessResourceNames;
    }

    public void setNewAccessResourceNames(List<String> newAccessResourceNames) {
        this.newAccessResourceNames = newAccessResourceNames;
    }

    public Map<String, Boolean> getUpdateAccessResourceNames() {
        return updateAccessResourceNames;
    }

    public void setUpdateAccessResourceNames(Map<String, Boolean> updateAccessResourceNames) {
        this.updateAccessResourceNames = updateAccessResourceNames;
    }

}