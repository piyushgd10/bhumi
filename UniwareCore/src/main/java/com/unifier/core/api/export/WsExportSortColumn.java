/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 30-Oct-2013
 *  @author parijat
 */
package com.unifier.core.api.export;

import org.hibernate.validator.constraints.NotEmpty;

public class WsExportSortColumn {

    @NotEmpty
    private String  column;

    private boolean descending;

    /**
     * @return the column
     */
    public String getColumn() {
        return column;
    }

    /**
     * @param column the column to set
     */
    public void setColumn(String column) {
        this.column = column;
    }

    /**
     * @return the descending
     */
    public boolean isDescending() {
        return descending;
    }

    /**
     * @param descending the descending to set
     */
    public void setDescending(boolean descending) {
        this.descending = descending;
    }

}
