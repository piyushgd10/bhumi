/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Sep 29, 2012
 *  @author singla
 */
package com.unifier.core.api.user;

import com.unifier.core.api.base.ServiceRequest;

import javax.validation.constraints.NotNull;

public class SwitchFacilityRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 2346948687089153713L;

    @NotNull
    private Integer           userId;

    private String            facilityCode;

    private String            currentUrl;

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return the facilityCode
     */
    public String getFacilityCode() {
        return facilityCode;
    }

    /**
     * @param facilityCode the facilityCode to set
     */
    public void setFacilityCode(String facilityCode) {
        this.facilityCode = facilityCode;
    }

    /**
     * @return the currentUrl
     */
    public String getCurrentUrl() {
        return currentUrl;
    }

    /**
     * @param currentUrl the currentUrl to set
     */
    public void setCurrentUrl(String currentUrl) {
        this.currentUrl = currentUrl;
    }

}
