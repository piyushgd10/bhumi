/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 08-Feb-2012
 *  @author vibhu
 */
package com.unifier.core.api.user;

import java.util.List;
import java.util.Map;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.annotation.constraints.Mobile;
import com.unifier.core.api.base.ServiceRequest;
import com.unifier.core.api.user.UserDTO.FacilityRole;

/**
 * @author vibhu
 */
public class AddOrEditUserDetailRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long  serialVersionUID = 1L;

    @NotBlank
    private String             username;

    @NotBlank
    private String             name;

    @Mobile
    private String             mobile;

    private boolean            enabled;

    private List<String>       tenantRoles;

    private List<FacilityRole> facilityRoles;

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the enabled
     */
    public boolean isEnabled() {
        return enabled;
    }

    /**
     * @param enabled the enabled to set
     */
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public List<String> getTenantRoles() {
        return tenantRoles;
    }

    public void setTenantRoles(List<String> tenantRoles) {
        this.tenantRoles = tenantRoles;
    }

    public List<FacilityRole> getFacilityRoles() {
        return facilityRoles;
    }

    public void setFacilityRoles(List<FacilityRole> facilityRoles) {
        this.facilityRoles = facilityRoles;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

}
