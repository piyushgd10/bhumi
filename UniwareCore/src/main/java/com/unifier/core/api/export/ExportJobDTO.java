/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 28, 2012
 *  @author praveeng
 */
package com.unifier.core.api.export;

import java.util.Date;

import com.unifier.core.entity.ExportJob;

public class ExportJobDTO {

    private String  id;
    private String  name;
    private Date    scheduledTime;
    private String  statusCode;
    private boolean successful;
    private long    percentageComplete;
    private Integer taskDurationInSec;
    private long    exportCount;
    private String  exportFilePath;

    public ExportJobDTO() {
    }

    public ExportJobDTO(ExportJob exportJob) {
        this.id = exportJob.getId();
        this.name = exportJob.getExportJobTypeName();
        this.scheduledTime = exportJob.getCreated();
        this.statusCode = exportJob.getStatusCode().name();
        this.successful = exportJob.isSuccessful();
        this.percentageComplete = (exportJob.getCurrentMileStone() * 100) / exportJob.getMileStoneCount();
        this.taskDurationInSec = exportJob.getTaskDurationInSec();
        this.exportCount = exportJob.getExportCount();
        this.exportFilePath = exportJob.getExportFilePath();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getScheduledTime() {
        return scheduledTime;
    }

    public void setScheduledTime(Date scheduledTime) {
        this.scheduledTime = scheduledTime;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public boolean isSuccessful() {
        return successful;
    }

    public void setSuccessful(boolean successful) {
        this.successful = successful;
    }

    public long getPercentageComplete() {
        return percentageComplete;
    }

    public void setPercentageComplete(int percentageComplete) {
        this.percentageComplete = percentageComplete;
    }

    public Integer getTaskDurationInSec() {
        return taskDurationInSec;
    }

    public void setTaskDurationInSec(Integer taskDurationInSec) {
        this.taskDurationInSec = taskDurationInSec;
    }

    public long getExportCount() {
        return exportCount;
    }

    public void setExportCount(long exportCount) {
        this.exportCount = exportCount;
    }

    public String getExportFilePath() {
        return exportFilePath;
    }

    public void setExportFilePath(String exportFilePath) {
        this.exportFilePath = exportFilePath;
    }
}
