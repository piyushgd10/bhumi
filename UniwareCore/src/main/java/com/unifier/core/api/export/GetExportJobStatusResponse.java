/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 20-Jun-2012
 *  @author praveeng
 */
package com.unifier.core.api.export;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author praveeng
 */
public class GetExportJobStatusResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 1575130306658611983L;
    private String            status;
    private String            filePath;

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the filePath
     */
    public String getFilePath() {
        return filePath;
    }

    /**
     * @param filePath the filePath to set
     */
    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

}
