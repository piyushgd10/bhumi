/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 18-Jun-2014
 *  @author parijat
 */
package com.unifier.core.api.audit;

import java.util.ArrayList;
import java.util.List;

public class ActivityDto {

    private String       entityName;
    private String       identifier;
    private String       groupIdentifier;
    private String       activityType;
    private List<String> activityLogs = new ArrayList<>();

    public ActivityDto(String entityName, String identifier, String groupIdentifier, String activityType) {
        this.entityName = entityName;
        this.identifier = identifier;
        this.groupIdentifier = groupIdentifier;
        this.activityType = activityType;
    }

    /**
     * @return the entityName
     */
    public String getEntityName() {
        return entityName;
    }

    /**
     * @param entityName the entityName to set
     */
    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    /**
     * @return the identifier
     */
    public String getIdentifier() {
        return identifier;
    }

    /**
     * @param identifier the identifier to set
     */
    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    /**
     * @return the groupIdentifier
     */
    public String getGroupIdentifier() {
        return groupIdentifier;
    }

    /**
     * @param groupIdentifier the groupIdentifier to set
     */
    public void setGroupIdentifier(String groupIdentifier) {
        this.groupIdentifier = groupIdentifier;
    }

    /**
     * @return the activityType
     */
    public String getActivityType() {
        return activityType;
    }

    /**
     * @param activityType the activityType to set
     */
    public void setActivityType(String activityType) {
        this.activityType = activityType;
    }

    /**
     * @return the activityLogs
     */
    public List<String> getActivityLogs() {
        return activityLogs;
    }

    /**
     * @param activityLogs the activityLogs to set
     */
    public void setActivityLogs(List<String> activityLogs) {
        this.activityLogs = activityLogs;
    }

}
