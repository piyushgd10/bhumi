/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 05/08/14
 *  @author amit
 */

package com.unifier.core.api.templatecustomization;

import com.unifier.core.api.base.ServiceResponse;

public class CustomizeTemplateResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -8397039477145372655L;
    private String            customizedTemplateUrl;

    public CustomizeTemplateResponse() {
        super();
    }

    public String getCustomizedTemplateUrl() {
        return customizedTemplateUrl;
    }

    public void setCustomizedTemplateUrl(String customizedTemplateUrl) {
        this.customizedTemplateUrl = customizedTemplateUrl;
    }

    @Override
    public String toString() {
        return "CustomizeTemplateResponse{" + "customizedTemplateUrl='" + customizedTemplateUrl + '\'' + '}';
    }
}