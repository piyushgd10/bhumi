package com.unifier.core.api.release;

import com.unifier.core.api.base.ServiceResponse;

import java.util.Date;
import java.util.List;

public class GetAllReleaseUpdatesResponse extends ServiceResponse {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private List<ReleaseUpdateDTO> releaseUpdates;

    public List<ReleaseUpdateDTO> getReleaseUpdates() {
        return releaseUpdates;
    }

    public void setReleaseUpdates(List<ReleaseUpdateDTO> releaseUpdates) {
        this.releaseUpdates = releaseUpdates;
    }

    public static class ReleaseUpdateDTO {
        private int    versionId;
        private String titleKey;
        private String contentKey;
        private String url;
        private Date   releaseDate;

        public ReleaseUpdateDTO() {
        }

        public int getVersionId() {
            return versionId;
        }

        public void setVersionId(int versionId) {
            this.versionId = versionId;
        }

        public String getTitleKey() {
            return titleKey;
        }

        public void setTitleKey(String titleKey) {
            this.titleKey = titleKey;
        }

        public String getContentKey() {
            return contentKey;
        }

        public void setContentKey(String contentKey) {
            this.contentKey = contentKey;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public Date getReleaseDate() {
            return releaseDate;
        }

        public void setReleaseDate(Date releaseDate) {
            this.releaseDate = releaseDate;
        }
    }

}
