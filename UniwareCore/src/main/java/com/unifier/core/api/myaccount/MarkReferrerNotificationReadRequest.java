package com.unifier.core.api.myaccount;

import com.unifier.core.api.base.ServiceRequest;

public class MarkReferrerNotificationReadRequest extends ServiceRequest{

	/**
	 * 
	 */
	private static final long	serialVersionUID	= 8364080889569026065L;
	
	private String username;
	
	private boolean remindLater = false;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public boolean isRemindLater() {
		return remindLater;
	}

	public void setRemindLater(boolean remindLater) {
		this.remindLater = remindLater;
	}
}
