/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 08-Oct-2013
 *  @author parijat
 */
package com.unifier.core.api.user;

import com.unifier.core.api.base.ServiceResponse;

public class AddUserDatatableViewsResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

}
