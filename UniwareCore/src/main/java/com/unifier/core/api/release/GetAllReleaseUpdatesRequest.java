package com.unifier.core.api.release;

import javax.validation.constraints.NotNull;

import com.unifier.core.api.base.ServiceRequest;

public class GetAllReleaseUpdatesRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @NotNull
    private Integer           userId;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

}
