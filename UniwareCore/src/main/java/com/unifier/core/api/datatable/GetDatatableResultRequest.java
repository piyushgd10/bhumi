/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 8, 2013
 *  @author karunsingla
 */
package com.unifier.core.api.datatable;

import com.unifier.core.api.base.ServiceRequest;
import com.unifier.core.api.export.WsExportFilter;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author karunsingla
 */
public class GetDatatableResultRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long    serialVersionUID = 7685512586925616341L;

    @NotBlank
    private String               name;

    @NotEmpty
    private List<String>         columns;

    @Valid
    private List<WsExportFilter> filters;

    @Valid
    private List<WsSortColumn>   sortColumns;

    private boolean              fetchResultCount;

    private int                  start;

    private int                  noOfResults      = 50;

    @NotNull
    private Integer              userId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getColumns() {
        return columns;
    }

    public void setColumns(List<String> columns) {
        this.columns = columns;
    }

    public List<WsExportFilter> getFilters() {
        return filters;
    }

    public void setFilters(List<WsExportFilter> filters) {
        this.filters = filters;
    }

    public List<WsSortColumn> getSortColumns() {
        return sortColumns;
    }

    public void setSortColumns(List<WsSortColumn> sortColumns) {
        this.sortColumns = sortColumns;
    }

    public boolean isFetchResultCount() {
        return fetchResultCount;
    }

    public void setFetchResultCount(boolean fetchResultCount) {
        this.fetchResultCount = fetchResultCount;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getNoOfResults() {
        return noOfResults;
    }

    public void setNoOfResults(int noOfResults) {
        this.noOfResults = noOfResults;
    }
}
