package com.unifier.core.api.myaccount;

import com.unifier.core.api.base.ServiceResponse;

import java.util.Date;
import java.util.List;

/**
 * Created by Samdeesh on 8/7/15.
 */
public class GetSubscriptionsResponse extends ServiceResponse {

    private List<SubscriptionDTO> subscriptions;

    public List<SubscriptionDTO> getSubscriptions() {
        return subscriptions;
    }

    public void setSubscriptions(List<SubscriptionDTO> subscriptions) {
        this.subscriptions = subscriptions;
    }

    private static class SubscriptionDTO {

        private String statusCode;
        private String productCode;
        private String productType;
        private Date   validUpto;
        private Date   activationDate;
        private Date   deactivationDate;

        public SubscriptionDTO() {
            super();
        }

        public String getStatusCode() {
            return statusCode;
        }

        public void setStatusCode(String statusCode) {
            this.statusCode = statusCode;
        }

        public String getProductCode() {
            return productCode;
        }

        public void setProductCode(String productCode) {
            this.productCode = productCode;
        }

        public String getProductType() {
            return productType;
        }

        public void setProductType(String productType) {
            this.productType = productType;
        }

        public Date getValidUpto() {
            return validUpto;
        }

        public void setValidUpto(Date validUpto) {
            this.validUpto = validUpto;
        }

        public Date getActivationDate() {
            return activationDate;
        }

        public void setActivationDate(Date activationDate) {
            this.activationDate = activationDate;
        }

        public Date getDeactivationDate() {
            return deactivationDate;
        }

        public void setDeactivationDate(Date deactivationDate) {
            this.deactivationDate = deactivationDate;
        }

        @Override
        public String toString() {
            return "Subscriptions{" + "statusCode='" + statusCode + '\'' + ", productCode='" + productCode + '\'' + ", productType=" + productType + ", validUpto=" + validUpto
                    + ", activationDate='" + activationDate + '\'' + ", deactivationDate=" + deactivationDate + '}';
        }
    }

}
