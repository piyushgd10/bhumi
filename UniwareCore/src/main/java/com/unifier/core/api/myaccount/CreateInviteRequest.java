package com.unifier.core.api.myaccount;

import java.util.List;

import javax.validation.constraints.NotNull;

import com.unifier.core.api.base.ServiceRequest;

public class CreateInviteRequest extends ServiceRequest{

    /**
     * 
     */
    private static final long serialVersionUID = -9174378120189012830L;
    
    public static class InviteParamDTO{
    	@NotNull
        private String name;
        
        @NotNull
        private String email;
        
        @NotNull
        private String mobile;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

		public String getMobile() {
			return mobile;
		}

		public void setMobile(String mobile) {
			this.mobile = mobile;
		}
	
    }
    
    private String referrer;
    
    private String name;
    
    private List<InviteParamDTO> invites;

	public List<InviteParamDTO> getInvites() {
		return invites;
	}

	public void setInvites(List<InviteParamDTO> invites) {
		this.invites = invites;
	}
	
	public void addInvite(InviteParamDTO invite){
		this.invites.add(invite);
	}

	public String getReferrer() {
		return referrer;
	}

	public void setReferrer(String referrer) {
		this.referrer = referrer;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
