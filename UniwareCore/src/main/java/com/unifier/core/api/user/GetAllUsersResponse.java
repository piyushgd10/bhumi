/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 04-Mar-2015
 *  @author unicommerce
 */
package com.unifier.core.api.user;

import java.util.List;

import com.unifier.core.api.base.ServiceResponse;

public class GetAllUsersResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long   serialVersionUID = 1L;

    private List<UserSearchDTO> userDTOs;
    private Long                totalCount;

    public List<UserSearchDTO> getUserDTOs() {
        return userDTOs;
    }

    public void setUserDTOs(List<UserSearchDTO> userDTOs) {
        this.userDTOs = userDTOs;
    }

    public Long getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Long totalCount) {
        this.totalCount = totalCount;
    }
}
