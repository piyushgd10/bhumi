/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 22-May-2012
 *  @author vibhu
 */
package com.unifier.core.api.access;

import org.hibernate.validator.constraints.NotEmpty;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author Sunny Agarwal
 */
public class GrantApiAccessToVendorRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -3289695100703930859L;

    @NotEmpty
    private String            channel;
    @NotEmpty
    private String            redirectUrl;
    private String            ruParams;
    @NotEmpty
    private String            userName;

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getRedirectUrl() {
        return redirectUrl;
    }

    public void setRedirectUrl(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }

    public String getRuParams() {
        return ruParams;
    }

    public void setRuParams(String ruParams) {
        this.ruParams = ruParams;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

}