/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 *  @version     1.0, Mar 8, 2012
 *  @author praveeng
 */
package com.unifier.core.api.print;

import java.util.ArrayList;
import java.util.List;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author Sunny Agarwal
 */
public class GetSamplePrintTemplatesByTypeResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long            serialVersionUID     = -5676840594904674653L;

    private final List<PrintTemplateDTO> samplePrintTemplates = new ArrayList<PrintTemplateDTO>();

    public List<PrintTemplateDTO> getSamplePrintTemplates() {
        return samplePrintTemplates;
    }

}
