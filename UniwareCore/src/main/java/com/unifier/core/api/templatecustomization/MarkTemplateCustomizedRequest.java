/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 *  @version     1.0, May 25, 2015
 *  @author akshay
 */
package com.unifier.core.api.templatecustomization;

import javax.validation.constraints.NotNull;

import com.unifier.core.api.base.ServiceRequest;
import com.uniware.core.vo.PrintTemplateVO;

public class MarkTemplateCustomizedRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 5145970962890396964L;
    
    @NotNull
    private PrintTemplateVO.Type     templateType;

    public PrintTemplateVO.Type getTemplateType() {
        return templateType;
    }

    public void setTemplateType(PrintTemplateVO.Type templateType) {
        this.templateType = templateType;
    }
    
    
    
}
