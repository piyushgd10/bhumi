/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 03-July-2015
 *  @author akshaykochhar
 */
package com.unifier.core.api.myaccount;

import com.unifier.core.api.base.ServiceResponse;

public class AddUrlVisitedByUserResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 1683326179251946789L;

}
