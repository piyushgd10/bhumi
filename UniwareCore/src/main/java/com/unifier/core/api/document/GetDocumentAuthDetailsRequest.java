/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 27-May-2013
 *  @author praveeng
 */
package com.unifier.core.api.document;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author praveeng
 */
public class GetDocumentAuthDetailsRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -8838089240093565232L;

    @NotBlank
    private String            identifier;

    /**
     * @return the identifier
     */
    public String getIdentifier() {
        return identifier;
    }

    /**
     * @param identifier the identifier to set
     */
    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

}
