/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 20-Feb-2013
 *  @author unicom
 */
package com.unifier.core.api.user;

import java.util.Date;

import com.unifier.core.entity.User;

public class UserSearchDTO {

    private Integer id;
    private String  name;
    private String  userName;
    private Date    lastLoginTime;

    public UserSearchDTO(User user){
        this.id = user.getId();
        this.name = user.getName();
        this.userName = user.getUsername();
        this.lastLoginTime = user.getLastLoginTime();
    }
    
    public UserSearchDTO() {

    }
    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the userName
     */
    public String getUserName() {
        return userName;
    }

    /**
     * @param userName the userName to set
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Date getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(Date lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

}