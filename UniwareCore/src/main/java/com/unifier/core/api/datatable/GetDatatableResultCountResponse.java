package com.unifier.core.api.datatable;

import com.unifier.core.api.base.ServiceResponse;

/**
 * Created by harshpal on 14/09/16.
 */
public class GetDatatableResultCountResponse extends ServiceResponse {

    private static final long serialVersionUID = 6881991210179404548L;

    private Long              resultCount;

    public Long getResultCount() {
        return resultCount;
    }

    public void setResultCount(Long resultCount) {
        this.resultCount = resultCount;
    }
}
