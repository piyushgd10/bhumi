package com.unifier.core.api.myaccount;

import java.util.List;

import com.unifier.core.api.base.ServiceResponse;

/**
 * Created by Samdeesh on 7/24/15.
 */
public class GetProductApplicabilityResponse extends ServiceResponse {

    private static final long             serialVersionUID = 8512480871925559280L;
    private List<ProductApplicabilityDTO> productApplicabilityDTOs;
    private String                        activeUniwareProductType;

    public List<ProductApplicabilityDTO> getProductApplicabilityDTOs() {
        return productApplicabilityDTOs;
    }

    public void setProductApplicabilityDTOs(List<ProductApplicabilityDTO> productApplicabilityDTOs) {
        this.productApplicabilityDTOs = productApplicabilityDTOs;
    }

    public String getActiveUniwareProductType() {
        return activeUniwareProductType;
    }

    public void setActiveUniwareProductType(String activeUniwareProductType) {
        this.activeUniwareProductType = activeUniwareProductType;
    }
}