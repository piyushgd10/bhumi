/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 21/05/14
 *  @author amit
 */

package com.unifier.core.api.templatecustomization;

import com.unifier.core.api.base.ServiceResponse;

public class SaveCustomizedTemplateResponse extends ServiceResponse {

    private static final long serialVersionUID = 6294309674301013841L;

}