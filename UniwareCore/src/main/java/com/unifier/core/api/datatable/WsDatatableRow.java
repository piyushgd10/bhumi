/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 8, 2013
 *  @author karunsingla
 */
package com.unifier.core.api.datatable;

import java.util.Arrays;

/**
 * @author karunsingla
 */
public class WsDatatableRow {
    private long     rowNo;
    private Object[] values;

    public long getRowNo() {
        return rowNo;
    }

    public void setRowNo(long rowNo) {
        this.rowNo = rowNo;
    }

    public Object[] getValues() {
        return values;
    }

    public void setValues(Object[] values) {
        this.values = values;
    }

    @Override
    public String toString() {
        return "WsDatatableRow [rowNo=" + rowNo + ", values=" + Arrays.toString(values) + "]";
    }

}
