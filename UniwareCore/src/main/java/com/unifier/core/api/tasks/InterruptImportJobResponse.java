/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 25-Mar-2013
 *  @author Pankaj
 */
package com.unifier.core.api.tasks;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author Sunny
 */
public class InterruptImportJobResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 8694346768476101299L;

}
