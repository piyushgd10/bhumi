/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 22-May-2012
 *  @author vibhu
 */
package com.unifier.core.api.token;

import com.unifier.core.api.base.ServiceRequest;
import javax.validation.constraints.NotNull;

/**
 * @author Sunny Agarwal
 */
public class GetLoginTokenRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -3289695100703930859L;

    @NotNull
    private Integer           userId;

    private String            supportUsername;

    private String            username;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getSupportUsername() {
        return supportUsername;
    }

    public void setSupportUsername(String supportUsername) {
        this.supportUsername = supportUsername;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}