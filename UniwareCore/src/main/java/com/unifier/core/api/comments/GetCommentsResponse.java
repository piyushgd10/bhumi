/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jun 29, 2012
 *  @author singla
 */
package com.unifier.core.api.comments;

import com.unifier.core.api.base.ServiceResponse;

import java.util.ArrayList;
import java.util.List;


public class GetCommentsResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long    serialVersionUID = 3856954267639027729L;
    private List<UserCommentDTO> userCommentDTOs  = new ArrayList<UserCommentDTO>();

    /**
     * @return the userCommentDTOs
     */
    public List<UserCommentDTO> getUserCommentDTOs() {
        return userCommentDTOs;
    }

    /**
     * @param userCommentDTOs the userCommentDTOs to set
     */
    public void setUserCommentDTOs(List<UserCommentDTO> userCommentDTOs) {
        this.userCommentDTOs = userCommentDTOs;
    }
}
