/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 04-Mar-2015
 *  @author unicommerce
 */
package com.unifier.core.api.user;

import com.unifier.core.api.base.ServiceResponse;

public class GetUserDetailsResponse extends ServiceResponse{

    private UserDTO user;

    public UserDTO getUser() {
        return user;
    }

    public void setUser(UserDTO user) {
        this.user = user;
    }
}
