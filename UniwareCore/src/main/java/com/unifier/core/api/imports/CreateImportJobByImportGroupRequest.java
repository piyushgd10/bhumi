/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 21, 2012
 *  @author praveeng
 */
package com.unifier.core.api.imports;

import java.io.InputStream;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author Sunny
 */
public class CreateImportJobByImportGroupRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 2656649580693756257L;

    @NotNull
    private InputStream       inputStream;

    @NotBlank
    private String            fileName;

    @NotBlank
    private String            importGroup;

    @NotBlank
    private String            channelCode;

    public InputStream getInputStream() {
        return inputStream;
    }

    public void setInputStream(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getImportGroup() {
        return importGroup;
    }

    public void setImportGroup(String importGroup) {
        this.importGroup = importGroup;
    }

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

}
