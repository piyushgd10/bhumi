/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 29-Mar-2012
 *  @author vibhu
 */
package com.unifier.core.api.myaccount;

import org.hibernate.validator.constraints.Email;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author vibhu
 */
public class EditDigestDetailRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -1570687512225576932L;

    private String            tenantCode;

    private String            username;

    private boolean           emailDailyDigest;

    private boolean           emailWeeklyDigest;

    private boolean           emailChannelStatus;

    private boolean           emailHourlyOrderSummary;

    private boolean           smsDailyDigest;

    private boolean           smsWeeklyDigest;

    private boolean           smsChannelStatus;

    private boolean           smsHourlyOrderSummary;

    public String getTenantCode() {
        return tenantCode;
    }

    public void setTenantCode(String tenantCode) {
        this.tenantCode = tenantCode;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public boolean isEmailDailyDigest() {
        return emailDailyDigest;
    }

    public void setEmailDailyDigest(boolean emailDailyDigest) {
        this.emailDailyDigest = emailDailyDigest;
    }

    public boolean isEmailWeeklyDigest() {
        return emailWeeklyDigest;
    }

    public void setEmailWeeklyDigest(boolean emailWeeklyDigest) {
        this.emailWeeklyDigest = emailWeeklyDigest;
    }

    public boolean isEmailChannelStatus() {
        return emailChannelStatus;
    }

    public void setEmailChannelStatus(boolean emailChannelStatus) {
        this.emailChannelStatus = emailChannelStatus;
    }

    public boolean isEmailHourlyOrderSummary() {
        return emailHourlyOrderSummary;
    }

    public void setEmailHourlyOrderSummary(boolean emailHourlyOrderSummary) {
        this.emailHourlyOrderSummary = emailHourlyOrderSummary;
    }

    public boolean isSmsDailyDigest() {
        return smsDailyDigest;
    }

    public void setSmsDailyDigest(boolean smsDailyDigest) {
        this.smsDailyDigest = smsDailyDigest;
    }

    public boolean isSmsWeeklyDigest() {
        return smsWeeklyDigest;
    }

    public void setSmsWeeklyDigest(boolean smsWeeklyDigest) {
        this.smsWeeklyDigest = smsWeeklyDigest;
    }

    public boolean isSmsChannelStatus() {
        return smsChannelStatus;
    }

    public void setSmsChannelStatus(boolean smsChannelStatus) {
        this.smsChannelStatus = smsChannelStatus;
    }

    public boolean isSmsHourlyOrderSummary() {
        return smsHourlyOrderSummary;
    }

    public void setSmsHourlyOrderSummary(boolean smsHourlyOrderSummary) {
        this.smsHourlyOrderSummary = smsHourlyOrderSummary;
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

}
