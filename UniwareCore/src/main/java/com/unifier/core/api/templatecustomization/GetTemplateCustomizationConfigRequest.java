/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 09/05/14
 *  @author amit
 */

package com.unifier.core.api.templatecustomization;

import javax.validation.constraints.NotNull;

import com.unifier.core.api.base.ServiceRequest;
import com.uniware.core.vo.PrintTemplateVO;

public class GetTemplateCustomizationConfigRequest extends ServiceRequest {

    private static final long    serialVersionUID = 6917215962582797125L;

    @NotNull
    private PrintTemplateVO.Type templateType;

    public GetTemplateCustomizationConfigRequest() {
        super();
    }

    public PrintTemplateVO.Type getTemplateType() {
        return templateType;
    }

    public void setTemplateType(PrintTemplateVO.Type templateType) {
        this.templateType = templateType;
    }

    @Override
    public String toString() {
        return "GetTemplateCustomizationConfigRequest{" + "templateType=" + templateType + '}';
    }
}
