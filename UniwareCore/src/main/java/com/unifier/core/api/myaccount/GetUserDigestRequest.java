package com.unifier.core.api.myaccount;

import com.unifier.core.api.base.ServiceRequest;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Created by akshayag on 7/27/15.
 */
public class GetUserDigestRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 747814005081398619L;

    @NotBlank
    private String username;

    @NotBlank
    private String tenantCode;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getTenantCode() {
        return tenantCode;
    }

    public void setTenantCode(String tenantCode) {
        this.tenantCode = tenantCode;
    }
}
