/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 23-Feb-2015
 *  @author akshay
 */
package com.unifier.core.api.myaccount;

import com.unifier.core.api.base.ServiceResponse;

import java.math.BigDecimal;
import java.util.Date;

public class GetAccountBalanceResponse extends ServiceResponse {

    /**
     *
     */
    private static final long serialVersionUID = 3037430352104532878L;

    private Date              activationDate;
    private BigDecimal        perUnitCharges;
    private BigDecimal        creditsBalance;
    private String            packageType;
    private boolean           specialPlanActivated = false;
    private int               freeOrdersBalance;
    private int               freeOrdersCredited;
    private Date              freeOrdersExpiryDate;
    private Date              freeOrdersStartDate;
    private String            freeOrdersPackageType;

    public Date getActivationDate() {
        return activationDate;
    }

    public void setActivationDate(Date activationDate) {
        this.activationDate = activationDate;
    }

    public BigDecimal getPerUnitCharges() {
        return perUnitCharges;
    }

    public BigDecimal getCreditsBalance() {
        return creditsBalance;
    }

    public void setCreditsBalance(BigDecimal creditsBalance) {
        this.creditsBalance = creditsBalance;
    }

    public void setPerUnitCharges(BigDecimal perUnitCharges) {
        this.perUnitCharges = perUnitCharges;
    }

    public String getPackageType() {
        return packageType;
    }

    public void setPackageType(String packageType) {
        this.packageType = packageType;
    }

    public boolean isSpecialPlanActivated() {
        return specialPlanActivated;
    }

    public void setSpecialPlanActivated(boolean specialPlanActivated) {
        this.specialPlanActivated = specialPlanActivated;
    }

    public int getFreeOrdersBalance() {
        return freeOrdersBalance;
    }

    public void setFreeOrdersBalance(int freeOrdersBalance) {
        this.freeOrdersBalance = freeOrdersBalance;
    }

    public Date getFreeOrdersExpiryDate() {
        return freeOrdersExpiryDate;
    }

    public void setFreeOrdersExpiryDate(Date freeOrdersExpiryDate) {
        this.freeOrdersExpiryDate = freeOrdersExpiryDate;
    }

    public Date getFreeOrdersStartDate() {
        return freeOrdersStartDate;
    }

    public void setFreeOrdersStartDate(Date freeOrdersStartDate) {
        this.freeOrdersStartDate = freeOrdersStartDate;
    }

    public String getFreeOrdersPackageType() {
        return freeOrdersPackageType;
    }

    public void setFreeOrdersPackageType(String freeOrdersPackageType) {
        this.freeOrdersPackageType = freeOrdersPackageType;
    }

    public int getFreeOrdersCredited() {
        return freeOrdersCredited;
    }

    public void setFreeOrdersCredited(int freeOrdersCredited) {
        this.freeOrdersCredited = freeOrdersCredited;
    }
}
