/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 06-Jun-2012
 *  @author praveeng
 */
package com.unifier.core.api.export;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author praveeng
 */
public class EditExportJobResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 462821904495391454L;

}
