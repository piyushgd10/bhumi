/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 17-Dec-2013
 *  @author akshay
 */
package com.unifier.core.api.user;

import com.unifier.core.api.base.ServiceResponse;

public class GetAccessResourceResponse extends ServiceResponse {

    /**
     *
     */
    private static final long serialVersionUID = 133521L;

    private String accessResources;

    public String getAccessResources() {
        return accessResources;
    }

    public void setAccessResources(String accessResources) {
        this.accessResources = accessResources;
    }

}

