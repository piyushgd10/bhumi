/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 25-Apr-2014
 *  @author parijat
 */
package com.unifier.core.api.audit;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

public class FetchEntityActivityRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 8068079791762374016L;

    @NotBlank
    private String            entityName;

    @NotBlank
    private String            entityIdentifier;

    private boolean           sort             = true;

    /**
     * @return the entityName
     */
    public String getEntityName() {
        return entityName;
    }

    /**
     * @param entityName the entityName to set
     */
    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    /**
     * @return the entityIdentifier
     */
    public String getEntityIdentifier() {
        return entityIdentifier;
    }

    /**
     * @param entityIdentifier the entityIdentifier to set
     */
    public void setEntityIdentifier(String entityIdentifier) {
        this.entityIdentifier = entityIdentifier;
    }

    public boolean isSort() {
        return sort;
    }

    public void setSort(boolean sort) {
        this.sort = sort;
    }
}
