/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 10-Apr-2015
 *  @author unicom
 */
package com.unifier.core.api.release;

import com.unifier.core.api.base.ServiceResponse;

import java.util.Date;
import java.util.List;

public class GetUnreadReleaseUpdatesResponse extends ServiceResponse {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private ReleaseUpdateDTO releaseUpdate;

    public ReleaseUpdateDTO getReleaseUpdate() {
        return releaseUpdate;
    }

    public void setReleaseUpdate(ReleaseUpdateDTO releaseUpdate) {
        this.releaseUpdate = releaseUpdate;
    }

    public static class UpdateDTO {
        private String titleKey;
        private String imageUrl;
        private String url;
        private String contentKey;

        public String getTitleKey() {
            return titleKey;
        }

        public void setTitleKey(String titleKey) {
            this.titleKey = titleKey;
        }

        public String getImageUrl() {
            return imageUrl;
        }

        public void setImageUrl(String imageUrl) {
            this.imageUrl = imageUrl;
        }
        
        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getContentKey() {
            return contentKey;
        }

        public void setContentKey(String contentKey) {
            this.contentKey = contentKey;
        }

    }

    public static class ReleaseUpdateDTO {
        private int             versionId;
        private List<UpdateDTO> updates;
        private String          titleKey;
        private String          contentKey;
        private String          url;
        private Date            releaseDate;

        public int getVersionId() {
            return versionId;
        }

        public void setVersionId(int versionId) {
            this.versionId = versionId;
        }

        public List<UpdateDTO> getUpdates() {
            return updates;
        }

        public void setUpdates(List<UpdateDTO> updates) {
            this.updates = updates;
        }

        public String getTitleKey() {
            return titleKey;
        }

        public void setTitleKey(String titleKey) {
            this.titleKey = titleKey;
        }

        public String getContentKey() {
            return contentKey;
        }

        public void setContentKey(String contentKey) {
            this.contentKey = contentKey;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public Date getReleaseDate() {
            return releaseDate;
        }

        public void setReleaseDate(Date releaseDate) {
            this.releaseDate = releaseDate;
        }
    }

}
