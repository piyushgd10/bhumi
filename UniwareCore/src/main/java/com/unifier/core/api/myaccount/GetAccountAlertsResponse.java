/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 23-Feb-2015
 *  @author akshay
 */
package com.unifier.core.api.myaccount;

import com.unifier.core.api.base.ServiceResponse;

import java.util.ArrayList;
import java.util.List;

public class GetAccountAlertsResponse extends ServiceResponse {

    /**
     *
     */
    private static final long serialVersionUID = -8066817561456317164L;

    private List<AlertDTO> alerts = new ArrayList<AlertDTO>();

    public static class AlertDTO {

        private String name;
        private String message;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        @Override public String toString() {
            return "AlertDTO{" +
                    "name='" + name + '\'' +
                    ", message='" + message + '\'' +
                    '}';
        }
    }

    public List<AlertDTO> getAlerts() {
        return alerts;
    }

    public void setAlerts(List<AlertDTO> alerts) {
        this.alerts = alerts;
    }

    @Override public String toString() {
        return "GetAccountAlertsResponse{" +
                "alerts=" + alerts +
                '}';
    }
}
