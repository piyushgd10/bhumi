/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jun 29, 2012
 *  @author singla
 */
package com.unifier.core.api.comments;

import com.unifier.core.api.base.ServiceResponse;

public class DeleteCommentResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -6599043066249189711L;

}
