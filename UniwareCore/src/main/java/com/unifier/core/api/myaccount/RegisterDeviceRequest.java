package com.unifier.core.api.myaccount;

import com.unifier.core.api.base.ServiceRequest;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Created by Samdeesh on 8/21/15.
 */
public class RegisterDeviceRequest extends ServiceRequest {

    private String username;

    @NotBlank
    private String tenantCode;

    @NotBlank
    private String gcmDeviceIdOld;

    @NotBlank
    private String gcmDeviceIdNew;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getTenantCode() {
        return tenantCode;
    }

    public void setTenantCode(String tenantCode) {
        this.tenantCode = tenantCode;
    }

    public String getGcmDeviceIdOld() {
        return gcmDeviceIdOld;
    }

    public void setGcmDeviceIdOld(String gcmDeviceIdOld) {
        this.gcmDeviceIdOld = gcmDeviceIdOld;
    }

    public String getGcmDeviceIdNew() {
        return gcmDeviceIdNew;
    }

    public void setGcmDeviceIdNew(String gcmDeviceIdNew) {
        this.gcmDeviceIdNew = gcmDeviceIdNew;
    }
}
