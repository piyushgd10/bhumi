/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 *  @version     1.0, 23-Jul-2012
 *  @author praveeng
 */
package com.unifier.core.api.application;

import com.unifier.core.entity.SystemConfig;

/**
 * @author praveeng
 */
public class SystemConfigurationDTO {
    private String name;
    private String displayName;
    private String value;
    private String groupName;
    private String accessResourceName;
    private String type;

    public SystemConfigurationDTO() {

    }
    public SystemConfigurationDTO(SystemConfig systemConfig) {
        setName(systemConfig.getName());
        setDisplayName(systemConfig.getDisplayName());
        setValue(systemConfig.getValue());
        setGroupName(systemConfig.getGroupName());
        setType(systemConfig.getType());
        setAccessResourceName(systemConfig.getAccessResourceName());
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the displayName
     */
    public String getDisplayName() {
        return displayName;
    }

    /**
     * @param displayName the displayName to set
     */
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    /**
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * @return the groupName
     */
    public String getGroupName() {
        return groupName;
    }
    /**
     * @param groupName the groupName to set
     */
    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }


    /**
     * @return the type
     */
    public String getType() {
        return type;
    }
    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }
    /**
     * @param systemConfig
     */
    public String getAccessResourceName() {
        return accessResourceName;
    }
    public void setAccessResourceName(String accessResourceName) {
        this.accessResourceName = accessResourceName;
    }

}
