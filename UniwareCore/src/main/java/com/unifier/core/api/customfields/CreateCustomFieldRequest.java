/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 16-Aug-2012
 *  @author praveeng
 */
package com.unifier.core.api.customfields;

import javax.validation.constraints.Min;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author praveeng
 */
public class CreateCustomFieldRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -8759454456297151883L;

    @NotBlank
    private String            entity;

    @NotBlank
    private String            name;

    @NotBlank
    private String            displayName;

    @NotBlank
    private String            valueType;

    private String            possibleValues;

    private String            validatorPattern;

    @Min(value = 1)
    private Integer           length;

    private String            defaultValue;

    private boolean           required;

    /**
     * @return the entity
     */
    public String getEntity() {
        return entity;
    }

    /**
     * @param entity the entity to set
     */
    public void setEntity(String entity) {
        this.entity = entity;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the displayName
     */
    public String getDisplayName() {
        return displayName;
    }

    /**
     * @param displayName the displayName to set
     */
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    /**
     * @return the valueType
     */
    public String getValueType() {
        return valueType;
    }

    /**
     * @param valueType the valueType to set
     */
    public void setValueType(String valueType) {
        this.valueType = valueType;
    }

    /**
     * @return the possibleValues
     */
    public String getPossibleValues() {
        return possibleValues;
    }

    /**
     * @param possibleValues the possibleValues to set
     */
    public void setPossibleValues(String possibleValues) {
        this.possibleValues = possibleValues;
    }

    /**
     * @return the validatorPattern
     */
    public String getValidatorPattern() {
        return validatorPattern;
    }

    /**
     * @param validatorPattern the validatorPattern to set
     */
    public void setValidatorPattern(String validatorPattern) {
        this.validatorPattern = validatorPattern;
    }

    /**
     * @return the length
     */
    public Integer getLength() {
        return length;
    }

    /**
     * @param length the length to set
     */
    public void setLength(Integer length) {
        this.length = length;
    }

    /**
     * @return the defaultValue
     */
    public String getDefaultValue() {
        return defaultValue;
    }

    /**
     * @param defaultValue the defaultValue to set
     */
    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    /**
     * @return the required
     */
    public boolean isRequired() {
        return required;
    }

    /**
     * @param required the required to set
     */
    public void setRequired(boolean required) {
        this.required = required;
    }

}
