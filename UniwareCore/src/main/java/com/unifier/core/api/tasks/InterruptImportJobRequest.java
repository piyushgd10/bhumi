/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 17-Apr-2013
 *  @author unicom
 */
package com.unifier.core.api.tasks;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author Sunny
 */
public class InterruptImportJobRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -3622026572209031457L;

    private Integer           importJobId;

    public Integer getImportJobId() {
        return importJobId;
    }

    public void setImportJobId(Integer importJobId) {
        this.importJobId = importJobId;
    }

}
