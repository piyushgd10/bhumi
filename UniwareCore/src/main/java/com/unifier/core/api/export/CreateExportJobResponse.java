/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 19, 2012
 *  @author singla
 */
package com.unifier.core.api.export;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author singla
 */
public class CreateExportJobResponse extends ServiceResponse {

    /**
     *
     */
    private static final long serialVersionUID = 7769938586144166497L;

    private String exportJobId;
    private String jobCode;

    public String getExportJobId() {
        return exportJobId;
    }

    public void setExportJobId(String exportJobId) {
        this.exportJobId = exportJobId;
    }

    public String getJobCode() {
        return jobCode;
    }

    public void setJobCode(String jobCode) {
        this.jobCode = jobCode;
    }
}
