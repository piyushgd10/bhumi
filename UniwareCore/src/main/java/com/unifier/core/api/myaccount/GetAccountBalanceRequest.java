/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 23-Feb-2015
 *  @author akshay
 */
package com.unifier.core.api.myaccount;

import com.unifier.core.api.base.ServiceRequest;
import org.hibernate.validator.constraints.NotBlank;

public class GetAccountBalanceRequest extends ServiceRequest {

    /**
     *
     */
    private static final long serialVersionUID = -1203010091468026584L;

}
