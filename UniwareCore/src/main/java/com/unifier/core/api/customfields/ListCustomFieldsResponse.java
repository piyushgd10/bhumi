/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 16-Feb-2013
 *  @author pankaj
 */
package com.unifier.core.api.customfields;

import java.util.ArrayList;
import java.util.List;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author pankaj
 */
public class ListCustomFieldsResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long            serialVersionUID = 3335291915688068532L;

    private List<CustomFieldMetaDataDTO> customFields     = new ArrayList<CustomFieldMetaDataDTO>();

    /**
     * @return the customFields
     */
    public List<CustomFieldMetaDataDTO> getCustomFields() {
        return customFields;
    }

    /**
     * @param customFields the customFields to set
     */
    public void setCustomFields(List<CustomFieldMetaDataDTO> customFields) {
        this.customFields = customFields;
    }

    public static class CustomFieldMetaDataDTO {
        private String  entity;
        private String  displayName;
        private String  name;
        private String  defaultValue;
        private String  type;
        private boolean required;
        private boolean enabled;
        private String  possibleValues;
        private boolean serializable;
        private String  mappingFieldName;

        /**
         * @return the entity
         */
        public String getEntity() {
            return entity;
        }

        /**
         * @param enitity the enitity to set
         */
        public void setEntity(String entity) {
            this.entity = entity;
        }

        /**
         * @return the displayName
         */
        public String getDisplayName() {
            return displayName;
        }

        /**
         * @param displayName the displayName to set
         */
        public void setDisplayName(String displayName) {
            this.displayName = displayName;
        }

        /**
         * @return the name
         */
        public String getName() {
            return name;
        }

        /**
         * @param name the name to set
         */
        public void setName(String name) {
            this.name = name;
        }

        /**
         * @return the required
         */
        public boolean isRequired() {
            return required;
        }

        /**
         * @param required the required to set
         */
        public void setRequired(boolean required) {
            this.required = required;
        }

        /**
         * @return the defaultValue
         */
        public String getDefaultValue() {
            return defaultValue;
        }

        /**
         * @param defaultValue the defaultValue to set
         */
        public void setDefaultValue(String defaultValue) {
            this.defaultValue = defaultValue;
        }

        /**
         * @return the type
         */
        public String getType() {
            return type;
        }

        /**
         * @param type the type to set
         */
        public void setType(String type) {
            this.type = type;
        }

        /**
         * @return the enabled
         */
        public boolean isEnabled() {
            return enabled;
        }

        /**
         * @param enabled the enabled to set
         */
        public void setEnabled(boolean enabled) {
            this.enabled = enabled;
        }

        /**
         * @return the possibleValues
         */
        public String getPossibleValues() {
            return possibleValues;
        }

        /**
         * @param possibleValues the possibleValues to set
         */
        public void setPossibleValues(String possibleValues) {
            this.possibleValues = possibleValues;
        }

        /**
         * @return the serializable
         */
        public boolean isSerializable() {
            return serializable;
        }

        /**
         * @param serializable the serializable to set
         */
        public void setSerializable(boolean serializable) {
            this.serializable = serializable;
        }

        /**
         * @return the mappingFieldName
         */
        public String getMappingFieldName() {
            return mappingFieldName;
        }

        /**
         * @param mappingFieldName the mappingFieldName to set
         */
        public void setMappingFieldName(String mappingFieldName) {
            this.mappingFieldName = mappingFieldName;
        }

    }

}
