/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 25-Mar-2013
 *  @author Pankaj
 */
package com.unifier.core.api.tasks;

import javax.validation.constraints.NotNull;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author Pankaj
 */
public class EditScriptConfigRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 4634293032809970667L;

    @NotNull
    private String            name;
    private String            script;

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the script
     */
    public String getScript() {
        return script;
    }

    /**
     * @param script the script to set
     */
    public void setScript(String script) {
        this.script = script;
    }

}
