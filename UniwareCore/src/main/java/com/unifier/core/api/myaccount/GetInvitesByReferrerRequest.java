package com.unifier.core.api.myaccount;

import com.unifier.core.api.base.ServiceRequest;

public class GetInvitesByReferrerRequest extends ServiceRequest{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private String referrerCode;

    public String getReferrerCode() {
        return referrerCode;
    }

    public void setReferrerCode(String referrerCode) {
        this.referrerCode = referrerCode;
    }

}
