/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 21-Jan-2014
 *  @author parijat
 */
package com.unifier.core.api.customfields;

import java.util.List;

import com.unifier.core.api.base.ServiceResponse;

public class ListCustomFieldsVOResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -278525069360615640L;

    private List<CustomFieldMetadataDTO> customFieldMetadataDTOs;

    public List<CustomFieldMetadataDTO> getCustomFieldMetadataDTOs() {
        return customFieldMetadataDTOs;
    }

    public void setCustomFieldMetadataDTOs(List<CustomFieldMetadataDTO> customFieldMetadataDTOs) {
        this.customFieldMetadataDTOs = customFieldMetadataDTOs;
    }

}
