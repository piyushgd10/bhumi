package com.unifier.core.api.release;

import com.unifier.core.api.base.ServiceResponse;

public class GetUnreadReleaseUpdateCountResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private int               unreadUpdatedCount;

    public int getUnreadUpdatedCount() {
        return unreadUpdatedCount;
    }

    public void setUnreadUpdatedCount(int unreadUpdatedCount) {
        this.unreadUpdatedCount = unreadUpdatedCount;
    }

}
