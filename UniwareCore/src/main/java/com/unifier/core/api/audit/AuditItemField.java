/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 29-May-2014
 *  @author parijat
 */
package com.unifier.core.api.audit;

import com.uniware.core.vo.EntityAuditLogVO.FieldAuditVO;

/**
 * @author parijat
 *
 */
public class AuditItemField {
    private String fieldName;
    private String oldValue;
    private String newValue;
    private String auditMessage;

    public AuditItemField() {

    }

    public AuditItemField(FieldAuditVO fieldVO) {
        this.fieldName = fieldVO.getFieldName();
        this.oldValue = fieldVO.getOldValue();
        this.newValue = fieldVO.getNewValue();
    }

    /**
     * @return the fieldName
     */
    public String getFieldName() {
        return fieldName;
    }

    /**
     * @param fieldName the fieldName to set
     */
    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    /**
     * @return the oldValue
     */
    public String getOldValue() {
        return oldValue;
    }

    /**
     * @param oldValue the oldValue to set
     */
    public void setOldValue(String oldValue) {
        this.oldValue = oldValue;
    }

    /**
     * @return the newValue
     */
    public String getNewValue() {
        return newValue;
    }

    /**
     * @param newValue the newValue to set
     */
    public void setNewValue(String newValue) {
        this.newValue = newValue;
    }

    /**
     * @return the auditMessage
     */
    public String getAuditMessage() {
        return auditMessage;
    }

    /**
     * @param auditMessage the auditMessage to set
     */
    public void setAuditMessage(String auditMessage) {
        this.auditMessage = auditMessage;
    }

}
