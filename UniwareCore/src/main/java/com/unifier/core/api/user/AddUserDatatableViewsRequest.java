/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 08-Oct-2013
 *  @author parijat
 */
package com.unifier.core.api.user;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author parijat
 */
public class AddUserDatatableViewsRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long   serialVersionUID = -9067517965842388733L;

    private Integer             userId;

    @NotBlank
    private String              datatableName;

    @NotEmpty
    private String              name;
    
    @Valid
    private WSUserDataTableView dataTableView;

    @NotNull
    private Boolean             isDefaultView;

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return the datatableName
     */
    public String getDatatableName() {
        return datatableName;
    }

    /**
     * @param datatableName the datatableName to set
     */
    public void setDatatableName(String datatableName) {
        this.datatableName = datatableName;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the dataTableView
     */
    public WSUserDataTableView getDataTableView() {
        return dataTableView;
    }

    /**
     * @param dataTableView the dataTableView to set
     */
    public void setDataTableView(WSUserDataTableView dataTableView) {
        this.dataTableView = dataTableView;
    }

    /**
     * @return the isDefaultView
     */
    public Boolean getIsDefaultView() {
        return isDefaultView;
    }

    /**
     * @param isDefaultView the isDefaultView to set
     */
    public void setIsDefaultView(Boolean isDefaultView) {
        this.isDefaultView = isDefaultView;
    }
}
