/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 27-Jan-2014
 *  @author parijat
 */
package com.unifier.core.api.reports;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

public class GetUserWidgetsRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 5124798247838154161L;

    @NotBlank
    private String            widgetGroup;

    public String getWidgetGroup() {
        return widgetGroup;
    }

    public void setWidgetGroup(String widgetGroup) {
        this.widgetGroup = widgetGroup;
    }

}
