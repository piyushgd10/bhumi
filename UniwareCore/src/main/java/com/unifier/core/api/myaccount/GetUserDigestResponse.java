package com.unifier.core.api.myaccount;

import com.unifier.core.api.base.ServiceResponse;

/**
 * Created by akshayag on 7/27/15.
 */
public class GetUserDigestResponse extends ServiceResponse {

    private UserAccountDigestFlagsDTO userAccountDigestFlagsDTO;

    public UserAccountDigestFlagsDTO getUserAccountDigestFlagsDTO() {
        return userAccountDigestFlagsDTO;
    }

    public void setUserAccountDigestFlagsDTO(UserAccountDigestFlagsDTO userAccountDigestFlagsDTO) {
        this.userAccountDigestFlagsDTO = userAccountDigestFlagsDTO;
    }
}
