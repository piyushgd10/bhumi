/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 21/05/14
 *  @author amit
 */

package com.unifier.core.api.templatecustomization;

import java.util.List;
import java.util.Map;

public class CustomizationParameterDTO {

    private String              name;

    private String              code;

    private boolean             required;

    private String              type;

    private String              tooltipKey;

    private String              selectedValue;

    private List<String>        possibleValues;

    private String              parentFieldCode;

    private Map<String, Object> uiConfigMetadata;

    public CustomizationParameterDTO() {
        super();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public String getParentFieldCode() {
        return parentFieldCode;
    }

    public void setParentFieldCode(String parentFieldCode) {
        this.parentFieldCode = parentFieldCode;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTooltipKey() {
        return tooltipKey;
    }

    public void setTooltipKey(String tooltipKey) {
        this.tooltipKey = tooltipKey;
    }

    public String getSelectedValue() {
        return selectedValue;
    }

    public void setSelectedValue(String selectedValue) {
        this.selectedValue = selectedValue;
    }

    public Map<String, Object> getUiConfigMetadata() {
        return uiConfigMetadata;
    }

    public void setUiConfigMetadata(Map<String, Object> uiConfigMetadata) {
        this.uiConfigMetadata = uiConfigMetadata;
    }

    public List<String> getPossibleValues() {
        return possibleValues;
    }

    public void setPossibleValues(List<String> possibleValues) {
        this.possibleValues = possibleValues;
    }

    @Override
    public String toString() {
        return "CustomizationParameterDTO{" + "name='" + name + '\'' + ", code='" + code + '\'' + ", required=" + required + ", type='" + type + '\'' + ", selectedValue='"
                + selectedValue + '\'' + ", possibleValues=" + possibleValues + ", uiConfigMetadata=" + uiConfigMetadata + '}';
    }

}