/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 21-Feb-2013
 *  @author unicom
 */
package com.unifier.core.api.export;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author unicom
 */
public class AddExportSubscriberRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -6769911214531911261L;

    private String subscribingUsers[];
    private String exportJobId;

    /**
     * @return the subscribingUsers
     */
    public String[] getSubscribingUsers() {
        return subscribingUsers;
    }

    /**
     * @param subscribingUsers the subscribingUsers to set
     */
    public void setSubscribingUsers(String[] subscribingUsers) {
        this.subscribingUsers = subscribingUsers;
    }

    /**
     * @return the exportJobId
     */
    public String getExportJobId() {
        return exportJobId;
    }

    /**
     * @param exportJobId the exportJobId to set
     */
    public void setExportJobId(String exportJobId) {
        this.exportJobId = exportJobId;
    }

}
