/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 09/05/14
 *  @author amit
 */

package com.unifier.core.api.templatecustomization;

import java.util.ArrayList;
import java.util.List;

import com.unifier.core.api.base.ServiceResponse;

public class GetTemplateCustomizationConfigResponse extends ServiceResponse {

    private static final long                    serialVersionUID      = -3816682827717957920L;

    private List<CustomizationParameterGroupDTO> parameterGroupDTOList = new ArrayList<>();

    public GetTemplateCustomizationConfigResponse() {
        super();
    }

    public List<CustomizationParameterGroupDTO> getParameterGroupDTOList() {
        return parameterGroupDTOList;
    }

    public void setParameterGroupDTOList(List<CustomizationParameterGroupDTO> parameterGroupDTOList) {
        this.parameterGroupDTOList = parameterGroupDTOList;
    }

    @Override
    public String toString() {
        return "GetTemplateCustomizationConfigResponse{" + "parameterGroupDTOList=" + parameterGroupDTOList + '}';
    }
}
