/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 *  @version     1.0, Mar 8, 2012
 *  @author praveeng
 */
package com.unifier.core.api.print;

import java.util.List;
import java.util.Map;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author Sunny Agarwal
 */
public class GetAllSamplePrintTemplatesResponse extends ServiceResponse {

    private static final long                      serialVersionUID = -4737909047003890312L;
    
    private Map<String, List<PrintTemplateMinDTO>> typeToPrintTemplates;

    public Map<String, List<PrintTemplateMinDTO>> getTypeToPrintTemplates() {
        return typeToPrintTemplates;
    }

    public void setTypeToPrintTemplates(Map<String, List<PrintTemplateMinDTO>> typeToPrintTemplates) {
        this.typeToPrintTemplates = typeToPrintTemplates;
    }

}
