/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 16-Feb-2013
 *  @author pankaj
 */
package com.unifier.core.api.customfields;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author pankaj
 */
public class ListCustomFieldsRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -5615018269944705922L;

    @NotBlank
    private String            entity;

    /**
     * @return the entity
     */
    public String getEntity() {
        return entity;
    }

    /**
     * @param entity the entity to set
     */
    public void setEntity(String entity) {
        this.entity = entity;
    }

}
