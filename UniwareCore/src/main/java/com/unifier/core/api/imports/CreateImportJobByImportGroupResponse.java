/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 21, 2012
 *  @author praveeng
 */
package com.unifier.core.api.imports;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author praveeng
 */
public class CreateImportJobByImportGroupResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -7738449598865280173L;
    private String            importJobType;
    private String            csvFile;
    private Integer           importJobId;
    private Integer           taskId;

    public String getImportJobType() {
        return importJobType;
    }

    public void setImportJobType(String importJobType) {
        this.importJobType = importJobType;
    }

    public String getCsvFile() {
        return csvFile;
    }

    public void setCsvFile(String csvFile) {
        this.csvFile = csvFile;
    }

    public Integer getImportJobId() {
        return importJobId;
    }

    public void setImportJobId(Integer importJobId) {
        this.importJobId = importJobId;
    }

    public Integer getTaskId() {
        return taskId;
    }

    public void setTaskId(Integer taskId) {
        this.taskId = taskId;
    }

}
