/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 22-May-2012
 *  @author vibhu
 */
package com.unifier.core.api.advanced;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.unifier.core.entity.Role;

/**
 * @author vibhu
 */
public class RoleDTO implements Serializable {

    private Integer id;
    private String  code;
    private String  name;
    private String  level;
    private String  description;
    private boolean hidden;
    private Map<String, Boolean> accessResources = new HashMap<String, Boolean>();

    public RoleDTO(Role role){
        this.id = role.getId();
        this.code = role.getCode();
        this.name = role.getName();
        this.level = role.getLevel();
        this.hidden = role.isHidden();
        if(role.getDescription() != null){
            this.description = role.getDescription();
        }
    }
    
    public RoleDTO() {

    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the level
     */
    public String getLevel() {
        return level;
    }

    /**
     * @param level the level to set
     */
    public void setLevel(String level) {
        this.level = level;
    }

    public boolean isHidden() {
        return hidden;
    }

    public void setHidden(boolean hidden) {
        this.hidden = hidden;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Map<String, Boolean> getAccessResources() {
        return accessResources;
    }

    public void setAccessResources(Map<String, Boolean> accessResources) {
        this.accessResources = accessResources;
    }
    
    public void addAccessResource(String accessResourceName, boolean enabled) {
        accessResources.put(accessResourceName, enabled);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

}