/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 20-Jun-2012
 *  @author praveeng
 */
package com.unifier.core.api.export;

import com.unifier.core.api.base.ServiceRequest;

import org.hibernate.validator.constraints.NotEmpty;


/**
 * @author praveeng
 */
public class GetExportJobStatusRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -3389648153619077730L;

    @NotEmpty
    private String            jobCode;

    /**
     * @return the jobCode
     */
    public String getJobCode() {
        return jobCode;
    }

    /**
     * @param jobCode the jobCode to set
     */
    public void setJobCode(String jobCode) {
        this.jobCode = jobCode;
    }

}
