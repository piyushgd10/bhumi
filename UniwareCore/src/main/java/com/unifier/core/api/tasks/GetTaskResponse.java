/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Nov 21, 2012
 *  @author Pankaj
 */
package com.unifier.core.api.tasks;

import java.util.ArrayList;
import java.util.List;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author Pankaj
 */
public class GetTaskResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 1341819107938860594L;

    private GetTaskDTO        task;

    /**
     * @return the task
     */
    public GetTaskDTO getTask() {
        return task;
    }

    /**
     * @param task the task to set
     */
    public void setTask(GetTaskDTO task) {
        this.task = task;
    }

    public static class GetTaskDTO {
        private String                 name;
        private String                 taskClass;
        private String                 cronExpression;
        private boolean                enabled;
        private List<TaskParameterDTO> taskParameters = new ArrayList<TaskParameterDTO>();

        /**
         * @return the enabled
         */
        public boolean isEnabled() {
            return enabled;
        }

        /**
         * @param enabled the enabled to set
         */
        public void setEnabled(boolean enabled) {
            this.enabled = enabled;
        }

        /**
         * @return the taskParameter
         */
        public List<TaskParameterDTO> getTaskParameters() {
            return taskParameters;
        }

        /**
         * @param taskParameters the taskParameter to set
         */
        public void setTaskParameter(List<TaskParameterDTO> taskParameters) {
            this.taskParameters = taskParameters;
        }

        /**
         * @return the name
         */
        public String getName() {
            return name;
        }

        /**
         * @param name the name to set
         */
        public void setName(String name) {
            this.name = name;
        }

        /**
         * @return the taskClass
         */
        public String getTaskClass() {
            return taskClass;
        }

        /**
         * @param taskClass the taskClass to set
         */
        public void setTaskClass(String taskClass) {
            this.taskClass = taskClass;
        }

        public String getCronExpression() {
            return cronExpression;
        }

        public void setCronExpression(String cronExpression) {
            this.cronExpression = cronExpression;
        }
    }

    public static class TaskParameterDTO {
        private String name;
        private String value;

        /**
         * @return the name
         */
        public String getName() {
            return name;
        }

        /**
         * @param name the name to set
         */
        public void setName(String name) {
            this.name = name;
        }

        /**
         * @return the value
         */
        public String getValue() {
            return value;
        }

        /**
         * @param value the value to set
         */
        public void setValue(String value) {
            this.value = value;
        }

    }

}
