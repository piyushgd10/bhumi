/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 03-Dec-2013
 *  @author akshay
 */
package com.unifier.core.api.export;

import com.unifier.core.api.base.ServiceResponse;

public class CloneExportJobTypeResponse extends ServiceResponse{

    /**
     * 
     */
    private static final long serialVersionUID = -3936224122435505909L;

}
