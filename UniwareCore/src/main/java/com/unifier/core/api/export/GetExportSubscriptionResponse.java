/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 05-Jun-2012
 *  @author praveeng
 */
package com.unifier.core.api.export;

import com.unifier.core.api.base.ServiceResponse;
import com.unifier.core.export.config.ExportFilter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author praveeng
 */
public class GetExportSubscriptionResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long           serialVersionUID = -193143210057989982L;
    private List<RecurrentExportJobDTO> exportJobs       = new ArrayList<RecurrentExportJobDTO>();

    /**
     * @return the exportJobs
     */
    public List<RecurrentExportJobDTO> getExportJobs() {
        return exportJobs;
    }

    /**
     * @param exportJobs the exportJobs to set
     */
    public void setExportJobs(List<RecurrentExportJobDTO> exportJobs) {
        this.exportJobs = exportJobs;
    }

    public static class RecurrentExportJobDTO {
        private String             exportJobId;
        private Boolean            enabled;
        private String             exportJobTypeName;
        private String             accessResourceName;
        private Date               scheduledTime;
        private String             frequency;
        private List<String>       exportColumns = new ArrayList<String>();
        private List<ExportFilter> exportFilters = new ArrayList<ExportFilter>();
        private boolean            subscribed;
        private String             userName;
        private String             reportName;

        /**
         * @return the exportJobId
         */
        public String getExportJobId() {
            return exportJobId;
        }

        /**
         * @param exportJobId the exportJobId to set
         */
        public void setExportJobId(String exportJobId) {
            this.exportJobId = exportJobId;
        }

        /**
         * @return the enabled
         */
        public Boolean getEnabled() {
            return enabled;
        }

        /**
         * @param enabled the enabled to set
         */
        public void setEnabled(Boolean enabled) {
            this.enabled = enabled;
        }

        /**
         * @return the exportJobTypeName
         */
        public String getExportJobTypeName() {
            return exportJobTypeName;
        }

        /**
         * @param exportJobTypeName the exportJobTypeName to set
         */
        public void setExportJobTypeName(String exportJobTypeName) {
            this.exportJobTypeName = exportJobTypeName;
        }

        /**
         * @return the accessResourceName
         */
        public String getAccessResourceName() {
            return accessResourceName;
        }

        /**
         * @param accessResourceName the accessResourceName to set
         */
        public void setAccessResourceName(String accessResourceName) {
            this.accessResourceName = accessResourceName;
        }

        /**
         * @return the scheduledTime
         */
        public Date getScheduledTime() {
            return scheduledTime;
        }

        /**
         * @param scheduledTime the scheduledTime to set
         */
        public void setScheduledTime(Date scheduledTime) {
            this.scheduledTime = scheduledTime;
        }

        /**
         * @return the frequency
         */
        public String getFrequency() {
            return frequency;
        }

        /**
         * @param frequency the frequency to set
         */
        public void setFrequency(String frequency) {
            this.frequency = frequency;
        }

        /**
         * @return the exportColumns
         */
        public List<String> getExportColumns() {
            return exportColumns;
        }

        /**
         * @param exportColumns the exportColumns to set
         */
        public void setExportColumns(List<String> exportColumns) {
            this.exportColumns = exportColumns;
        }

        /**
         * @return the exportFilters
         */
        public List<ExportFilter> getExportFilters() {
            return exportFilters;
        }

        /**
         * @param exportFilters the exportFilters to set
         */
        public void setExportFilters(List<ExportFilter> exportFilters) {
            this.exportFilters = exportFilters;
        }

        /**
         * @return the subscribed
         */
        public boolean isSubscribed() {
            return subscribed;
        }

        /**
         * @param subscribed the subscribed to set
         */
        public void setSubscribed(boolean subscribed) {
            this.subscribed = subscribed;
        }

        /**
         * @return the userName
         */
        public String getUserName() {
            return userName;
        }

        /**
         * @param userName the userName to set
         */
        public void setUserName(String userName) {
            this.userName = userName;
        }

        /**
         * @return the reportName
         */
        public String getReportName() {
            return reportName;
        }

        /**
         * @param reportName the reportName to set
         */
        public void setReportName(String reportName) {
            this.reportName = reportName;
        }

    }

}
