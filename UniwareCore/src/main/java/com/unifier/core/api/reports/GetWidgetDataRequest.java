/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 12-Jun-2012
 *  @author praveeng
 */
package com.unifier.core.api.reports;

import java.util.HashMap;
import java.util.Map;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author praveeng
 */
public class GetWidgetDataRequest extends ServiceRequest {

    /**
     *
     */
    private static final long   serialVersionUID = 5556776613703602631L;
    private String              widgetCode;
    private Map<String, Object> requestMap       = new HashMap<String, Object>();
    private String              startDate;
    private String              endDate;
    private String              timeRange;
    private String              additionalCacheParam;

    public String getWidgetCode() {
        return widgetCode;
    }

    public void setWidgetCode(String widgetCode) {
        this.widgetCode = widgetCode;
    }

    public Map<String, Object> getRequestMap() {
        return requestMap;
    }

    public void setRequestMap(Map<String, Object> requestMap) {
        this.requestMap = requestMap;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getTimeRange() {
        return timeRange;
    }

    public void setTimeRange(String timeRange) {
        this.timeRange = timeRange;
    }

    public String getAdditionalCacheParam() {
        return additionalCacheParam;
    }

    public void setAdditionalCacheParam(String additionalCacheParam) {
        this.additionalCacheParam = additionalCacheParam;
    }
}
