/*
 * Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *    
 * @version     1.0, 8/4/15 4:45 PM
 * @author amdalal
 */

package com.unifier.core.api.external;

import com.unifier.core.api.base.ServiceResponse;

public class GetUnideskTokenResponse extends ServiceResponse {

    private static final long serialVersionUID = 1798954071800861771L;

    private String            mobile;

    private String            token;

    private String            productType;

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
