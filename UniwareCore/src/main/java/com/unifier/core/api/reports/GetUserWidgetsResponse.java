/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 27-Jan-2014
 *  @author parijat
 */
package com.unifier.core.api.reports;

import java.util.List;

import com.unifier.core.api.base.ServiceResponse;

public class GetUserWidgetsResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -4781815308171496550L;

    private List<WidgetDTO>   widgetDTOs;

    public List<WidgetDTO> getWidgetDTOs() {
        return widgetDTOs;
    }

    public void setWidgetDTOs(List<WidgetDTO> widgetDTOs) {
        this.widgetDTOs = widgetDTOs;
    }
}
