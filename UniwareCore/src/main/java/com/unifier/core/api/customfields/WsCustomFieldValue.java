/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jul 11, 2012
 *  @author singla
 */
package com.unifier.core.api.customfields;

import org.hibernate.validator.constraints.NotBlank;

/**
 * @author singla
 */
public class WsCustomFieldValue {

    @NotBlank
    private String name;

    private String value;

    public WsCustomFieldValue() {

    }

    /**
     * @param fieldName
     * @param fieldValue
     */
    public WsCustomFieldValue(String name, String value) {
        this.name = name;
        this.value = value;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(String value) {
        this.value = value;
    }
}
