/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 10-Feb-2014
 *  @author unicom
 */
package com.unifier.core.api.user;

import com.unifier.core.api.base.ServiceResponse;

public class CreateApiUserResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -556037978326804428L;

    private ApiUserDTO        apiUserDTO;

    public ApiUserDTO getApiUser() {
        return apiUserDTO;
    }

    public void setApiUser(ApiUserDTO apiUserDTO) {
        this.apiUserDTO = apiUserDTO;
    }
}
