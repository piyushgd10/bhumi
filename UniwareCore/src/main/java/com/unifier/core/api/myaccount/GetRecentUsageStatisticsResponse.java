/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 23-Feb-2015
 *  @author akshay
 */
package com.unifier.core.api.myaccount;

import com.unifier.core.api.base.ServiceResponse;

import java.util.List;

public class GetRecentUsageStatisticsResponse extends ServiceResponse {

    /**
     *
     */
    private static final long serialVersionUID = -8436096357190017096L;

    private List<UsageStatasticsDTO> recentUsages;

    public List<UsageStatasticsDTO> getRecentUsages() {
        return recentUsages;
    }

    public void setRecentUsages(List<UsageStatasticsDTO> recentUsages) {
        this.recentUsages = recentUsages;
    }
}
