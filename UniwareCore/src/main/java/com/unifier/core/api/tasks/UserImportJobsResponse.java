/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 25-Mar-2013
 *  @author Pankaj
 */
package com.unifier.core.api.tasks;

import java.util.ArrayList;
import java.util.List;

import com.unifier.core.api.base.ServiceResponse;
import com.unifier.core.api.imports.ImportJobDTO;

/**
 * @author Sunny
 */
public class UserImportJobsResponse extends ServiceResponse {

    /**
     *
     */
    private static final long        serialVersionUID = 5144607016983568417L;
    private final List<ImportJobDTO> importJobs       = new ArrayList<>();

    public List<ImportJobDTO> getImportJobs() {
        return importJobs;
    }
}
