/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 23-Feb-2015
 *  @author akshay
 */
package com.unifier.core.api.myaccount;

import com.unifier.core.api.base.ServiceRequest;
import com.unifier.core.utils.DateUtils.DateRange;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;

public class GetUsageHistoryRequest extends ServiceRequest {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @NotNull
    private DateRange daterange;

    public DateRange getDaterange() {
        return daterange;
    }

    public void setDaterange(DateRange daterange) {
        this.daterange = daterange;
    }

}
