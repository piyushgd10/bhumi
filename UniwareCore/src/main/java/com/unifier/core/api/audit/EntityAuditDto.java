/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 23-Apr-2014
 *  @author parijat
 */
package com.unifier.core.api.audit;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.uniware.core.vo.EntityAuditLogVO;
import com.uniware.core.vo.EntityAuditLogVO.FieldAuditVO;

/**
 * Dto which will used for tranfering data to UI. This is also entity level object.
 * 
 * @author parijat
 */
public class EntityAuditDto {

    private String               entityId;
    private String               identifier;
    private String               groupIdentifier;
    private String               entityName;
    private String               methodName;
    private String               methodAuditLog;
    private Date                 created;
    private List<AuditItemField> fields = new ArrayList<>();

    public EntityAuditDto(EntityAuditLogVO entityVO) {
        this.entityId = entityVO.getEntityId();
        this.identifier = entityVO.getIdentifier();
        this.groupIdentifier = entityVO.getGroupIdentifier();
        this.entityName = entityVO.getEntityName();
        this.methodName = entityVO.getMethodName();
        this.methodAuditLog = entityVO.getAuditLog();
        this.created = entityVO.getCreated();
        for (FieldAuditVO fieldAuditVO : entityVO.getFields()) {
            AuditItemField field = new AuditItemField(fieldAuditVO);
            fields.add(field);
        }
    }

    /**
     * @return the entityId
     */
    public String getEntityId() {
        return entityId;
    }

    /**
     * @param entityId the entityId to set
     */
    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    /**
     * @return the identifier
     */
    public String getIdentifier() {
        return identifier;
    }

    /**
     * @param identifier the identifier to set
     */
    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    /**
     * @return the groupIdentifier
     */
    public String getGroupIdentifier() {
        return groupIdentifier;
    }

    /**
     * @param groupIdentifier the groupIdentifier to set
     */
    public void setGroupIdentifier(String groupIdentifier) {
        this.groupIdentifier = groupIdentifier;
    }

    /**
     * @return the entityName
     */
    public String getEntityName() {
        return entityName;
    }

    /**
     * @param entityName the entityName to set
     */
    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    /**
     * @return the methodName
     */
    public String getMethodName() {
        return methodName;
    }

    /**
     * @param methodName the methodName to set
     */
    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    /**
     * @return the methodAuditLog
     */
    public String getMethodAuditLog() {
        return methodAuditLog;
    }

    /**
     * @param methodAuditLog the methodAuditLog to set
     */
    public void setMethodAuditLog(String methodAuditLog) {
        this.methodAuditLog = methodAuditLog;
    }

    /**
     * @return the created
     */
    public Date getCreated() {
        return created;
    }

    /**
     * @param created the created to set
     */
    public void setCreated(Date created) {
        this.created = created;
    }

    /**
     * @return the fields
     */
    public List<AuditItemField> getFields() {
        return fields;
    }

    /**
     * @param fields the fields to set
     */
    public void setFields(List<AuditItemField> fields) {
        this.fields = fields;
    }

}
