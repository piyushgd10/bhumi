/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 04-Mar-2013
 *  @author unicom
 */
package com.unifier.core.api.export;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author unicom
 */
public class DeleteExportSubscriptionRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 5965768090271198909L;

    private String  exportJobId;
    private Integer userId;

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return the exportJobId
     */
    public String getExportJobId() {
        return exportJobId;
    }

    /**
     * @param exportJobId the exportJobId to set
     */
    public void setExportJobId(String exportJobId) {
        this.exportJobId = exportJobId;
    }

}
