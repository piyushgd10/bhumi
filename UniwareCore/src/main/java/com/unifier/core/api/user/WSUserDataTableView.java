/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 30-Oct-2013
 *  @author parijat
 */
package com.unifier.core.api.user;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Min;

import com.unifier.core.api.export.WsExportColumn;
import com.unifier.core.api.export.WsExportFilter;
import com.unifier.core.api.export.WsExportSortColumn;

/**
 * @author parijat
 */
public class WSUserDataTableView {

    @Min(value = 24)
    private Integer                  resultsPerPage;

    @Valid
    private List<WsExportColumn>     columns;

    @Valid
    private List<WsExportFilter>     filters;

    @Valid
    private List<WsExportSortColumn> sortColumns;

    /**
     * @return the resultsPerPage
     */
    public Integer getResultsPerPage() {
        return resultsPerPage;
    }

    /**
     * @param resultsPerPage the resultsPerPage to set
     */
    public void setResultsPerPage(Integer resultsPerPage) {
        this.resultsPerPage = resultsPerPage;
    }

    /**
     * @return the columns
     */
    public List<WsExportColumn> getColumns() {
        return columns;
    }

    /**
     * @param columns the columns to set
     */
    public void setColumns(List<WsExportColumn> columns) {
        this.columns = columns;
    }

    /**
     * @return the filters
     */
    public List<WsExportFilter> getFilters() {
        return filters;
    }

    /**
     * @param filters the filters to set
     */
    public void setFilters(List<WsExportFilter> filters) {
        this.filters = filters;
    }

    /**
     * @return the sortColumns
     */
    public List<WsExportSortColumn> getSortColumns() {
        return sortColumns;
    }

    /**
     * @param sortColumns the sortColumns to set
     */
    public void setSortColumns(List<WsExportSortColumn> sortColumns) {
        this.sortColumns = sortColumns;
    }

}
