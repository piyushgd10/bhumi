/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jun 29, 2012
 *  @author singla
 */
package com.unifier.core.api.comments;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

public class AddCommentRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 7672219666883216254L;

    @NotNull
    private Integer           userId;

    @NotBlank
    private String            referenceIdentifier;

    @NotBlank
    private String            comment;

    private boolean           systemGenerated;

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return the referenceIdentifier
     */
    public String getReferenceIdentifier() {
        return referenceIdentifier;
    }

    /**
     * @param referenceIdentifier the referenceIdentifier to set
     */
    public void setReferenceIdentifier(String referenceIdentifier) {
        this.referenceIdentifier = referenceIdentifier;
    }

    /**
     * @return the comment
     */
    public String getComment() {
        return comment;
    }

    /**
     * @param comment the comment to set
     */
    public void setComment(String comment) {
        this.comment = comment;
    }

    /**
     * @return the systemGenerated
     */
    public boolean isSystemGenerated() {
        return systemGenerated;
    }

    /**
     * @param systemGenerated the systemGenerated to set
     */
    public void setSystemGenerated(boolean systemGenerated) {
        this.systemGenerated = systemGenerated;
    }

}
