/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 19, 2012
 *  @author singla
 */
package com.unifier.core.api.export;

import com.unifier.core.utils.DateUtils.DateRange;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.hibernate.validator.constraints.NotEmpty;


/**
 * @author singla
 */
public class WsExportFilter {

    @NotEmpty
    private String       id;
    private String       text;
    private List<String> selectedValues;
    private String       selectedValue;
    private Date         dateTime;
    @Valid
    private DateRange    dateRange;
    private Boolean      checked;

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the text
     */
    public String getText() {
        return text;
    }

    /**
     * @param text the text to set
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * @return the selectedValues
     */
    public List<String> getSelectedValues() {
        return selectedValues;
    }

    /**
     * @param selectedValues the selectedValues to set
     */
    public void setSelectedValues(List<String> selectedValues) {
        this.selectedValues = selectedValues;
    }

    /**
     * @return the selectedValue
     */
    public String getSelectedValue() {
        return selectedValue;
    }

    /**
     * @param selectedValue the selectedValue to set
     */
    public void setSelectedValue(String selectedValue) {
        this.selectedValue = selectedValue;
    }

    /**
     * @return the dateTime
     */
    public Date getDateTime() {
        return dateTime;
    }

    /**
     * @param dateTime the dateTime to set
     */
    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    /**
     * @return the dateRange
     */
    public DateRange getDateRange() {
        return dateRange;
    }

    /**
     * @param dateRange the dateRange to set
     */
    public void setDateRange(DateRange dateRange) {
        this.dateRange = dateRange;
    }

    /**
     * @return the checked
     */
    public Boolean getChecked() {
        return checked;
    }

    /**
     * @param checked the checked to set
     */
    public void setChecked(Boolean checked) {
        this.checked = checked;
    }

}
