/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 *  @version     1.0, Mar 8, 2012
 *  @author praveeng
 */
package com.unifier.core.api.print;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author Sunny Agarwal
 */
public class GetPrintTemplateByTypeRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -743673462015168435L;
    @NotBlank
    private String            type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}
