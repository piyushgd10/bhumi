/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 25-Mar-2013
 *  @author pankaj
 */
package com.unifier.core.api.tasks;

import com.uniware.core.vo.UniwareScriptVO;

/**
 * @author pankaj
 */
public class ScriptConfigDTO {

    private String script;
    private String name;
    private String version;

    public ScriptConfigDTO(UniwareScriptVO scriptConfig) {
        this.script = scriptConfig.getScript();
        this.name = scriptConfig.getName();
        this.version = scriptConfig.getVersion();
    }

    public ScriptConfigDTO() {

    }

    /**
     * @return the script
     */
    public String getScript() {
        return script;
    }

    /**
     * @param script the script to set
     */
    public void setScript(String script) {
        this.script = script;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

}
