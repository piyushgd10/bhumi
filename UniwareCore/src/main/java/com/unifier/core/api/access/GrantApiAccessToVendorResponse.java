/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 22-May-2012
 *  @author vibhu
 */
package com.unifier.core.api.access;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author Sunny Agarwal
 */
public class GrantApiAccessToVendorResponse extends ServiceResponse {

    private static final long serialVersionUID = 2341768549869164155L;

    private String            redirectUrl;

    public String getRedirectUrl() {
        return redirectUrl;
    }

    public void setRedirectUrl(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }

}