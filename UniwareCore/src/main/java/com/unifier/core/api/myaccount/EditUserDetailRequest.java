package com.unifier.core.api.myaccount;

import com.unifier.core.api.base.ServiceRequest;
import org.hibernate.validator.constraints.Email;

/**
 * Created by admin on 11/18/15.
 */
public class EditUserDetailRequest extends ServiceRequest {

    private static final long serialVersionUID = -1570687512225576934L;

    private String            tenantCode;

    private String            username;

    @Email
    private String            email;

    private String            mobile;

    private String            name;

    public String getTenantCode() {
        return tenantCode;
    }

    public void setTenantCode(String tenantCode) {
        this.tenantCode = tenantCode;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
