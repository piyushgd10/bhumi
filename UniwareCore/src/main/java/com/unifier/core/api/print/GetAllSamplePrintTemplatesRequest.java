/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 30-Apr-2013
 *  @author unicom
 */
package com.unifier.core.api.print;

import java.util.ArrayList;
import java.util.List;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author Sunny Agarwal
 */
public class GetAllSamplePrintTemplatesRequest extends ServiceRequest {

    private static final long serialVersionUID = -625349712830408785L;

    private List<String>      typeList         = new ArrayList<String>();

    public GetAllSamplePrintTemplatesRequest() {

    }

    public GetAllSamplePrintTemplatesRequest(List<String> typeList) {
        this.typeList = typeList;
    }

    public List<String> getTypeList() {
        return typeList;
    }

    public void setTypeList(List<String> typeList) {
        this.typeList = typeList;
    }
}