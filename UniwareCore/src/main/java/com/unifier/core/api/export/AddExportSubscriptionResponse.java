/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 21-Feb-2013
 *  @author unicom
 */
package com.unifier.core.api.export;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author unicom
 *
 */
public class AddExportSubscriptionResponse extends ServiceResponse{

    /**
     * 
     */
    private static final long serialVersionUID = 2038251316941122562L;

}
