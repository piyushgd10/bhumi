/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 8, 2013
 *  @author karunsingla
 */
package com.unifier.core.api.datatable;

import com.unifier.core.api.base.ServiceResponse;

import java.util.ArrayList;
import java.util.List;

public class GetDatatableResultResponse extends GetDatatableResultCountResponse  {

    /**
     * 
     */
    private static final long    serialVersionUID = 7762426320619503630L;
    private List<WsDatatableRow> rows             = new ArrayList<WsDatatableRow>();

    public List<WsDatatableRow> getRows() {
        return rows;
    }

    public void setRows(List<WsDatatableRow> rows) {
        this.rows = rows;
    }

}
