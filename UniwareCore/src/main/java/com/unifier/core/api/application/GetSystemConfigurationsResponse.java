package com.unifier.core.api.application;

import java.util.ArrayList;
import java.util.List;

import com.unifier.core.api.base.ServiceResponse;

/**
 * Created by Sagar Sahni on 24/08/17.
 */
public class GetSystemConfigurationsResponse extends ServiceResponse {

    private List<SystemConfigurationDTO> systemConfigurationDTOS = new ArrayList<>();

    public List<SystemConfigurationDTO> getSystemConfigurationDTOS() {
        return systemConfigurationDTOS;
    }

    public void setSystemConfigurationDTOS(List<SystemConfigurationDTO> systemConfigurationDTOS) {
        this.systemConfigurationDTOS = systemConfigurationDTOS;
    }

    public GetSystemConfigurationsResponse(List<SystemConfigurationDTO> systemConfigurationDTOS) {
        this.systemConfigurationDTOS = systemConfigurationDTOS;
    }

    public GetSystemConfigurationsResponse() {
    }
}
