/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 24-May-2012
 *  @author praveeng
 */
package com.unifier.core.api.export;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author praveeng
 */
public class EditExportJobTypeResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -5552725052944741048L;

}
