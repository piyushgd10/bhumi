/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 8, 2013
 *  @author karunsingla
 */
package com.unifier.core.api.datatable;

/**
 * @author karunsingla
 */
public class WsSortColumn {

    private String  column;
    private boolean descending;

    public WsSortColumn(String column, boolean descending) {
        super();
        this.column = column;
        this.descending = descending;
    }

    public WsSortColumn() {
        super();
    }

    public String getColumn() {
        return column;
    }

    public void setColumn(String column) {
        this.column = column;
    }

    public boolean isDescending() {
        return descending;
    }

    public void setDescending(boolean descending) {
        this.descending = descending;
    }

    /**
     * Helper method for query generation.
     * 
     * @return
     */
    public String getSortDirection() {
        return isDescending() ? " desc" : " asc";
    }

}
