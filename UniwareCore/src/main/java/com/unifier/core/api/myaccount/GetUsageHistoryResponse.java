/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 23-Feb-2015
 *  @author akshay
 */
package com.unifier.core.api.myaccount;

import com.unifier.core.api.base.ServiceResponse;

import java.util.ArrayList;
import java.util.List;

public class GetUsageHistoryResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -8436096357190017096L;
    
    private List<UsageHistoryDTO>     usageHistory = new ArrayList<UsageHistoryDTO>();

    public List<UsageHistoryDTO> getUsageHistory() {
        return usageHistory;
    }

    public void setUsageHistory(List<UsageHistoryDTO> usageHistory) {
        this.usageHistory = usageHistory;
    }

}
