package com.unifier.core.api.myaccount;

import javax.validation.constraints.NotNull;

import com.unifier.core.api.base.ServiceRequest;

public class ResendInviteRequest extends ServiceRequest{	
	
	/**
	 * 
	 */
	private static final long	serialVersionUID	= -2434266391898905402L;
	
	@NotNull
	private String inviteCode;

	public String getInviteCode() {
		return inviteCode;
	}

	public void setInviteCode(String inviteCode) {
		this.inviteCode = inviteCode;
	}

}
