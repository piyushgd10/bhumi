/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 10-Feb-2014
 *  @author unicom
 */
package com.unifier.core.api.user;

import javax.validation.constraints.NotNull;

import com.unifier.core.api.base.ServiceRequest;

public class CreateApiUserRequest extends ServiceRequest{

    /**
     * 
     */
    private static final long serialVersionUID = 5004164085349715873L;

    @NotNull
    private Integer           userId;

    @NotNull
    private String            name;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
