/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 17-Dec-2013
 *  @author akshay
 */
package com.unifier.core.api.access;

import org.hibernate.validator.constraints.NotEmpty;

import com.unifier.core.api.base.ServiceRequest;

public class GrantApiAccessRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @NotEmpty
    private String            redirectUrl;
    private String            ruParams;
    @NotEmpty
    private String            userName;

    public String getRedirectUrl() {
        return redirectUrl;
    }

    public void setRedirectUrl(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }

    public String getRuParams() {
        return ruParams;
    }

    public void setRuParams(String ruParams) {
        this.ruParams = ruParams;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

}
