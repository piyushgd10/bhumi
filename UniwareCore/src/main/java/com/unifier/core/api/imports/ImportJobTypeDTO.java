/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 *  @version     1.0, 27-July-2012
 *  @author akshay
 */

package com.unifier.core.api.imports;

import java.util.List;

/**
 * @author akshay
 */
public class ImportJobTypeDTO {
    private String       name;
    private boolean      enabled;
    private String       importJobConfig;
    private List<String> importOptions;

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the enabled
     */
    public boolean isEnabled() {
        return enabled;
    }

    /**
     * @param enabled the enabled to set
     */
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    /**
     * @return the importJobConfig
     */
    public String getImportJobConfig() {
        return importJobConfig;
    }

    /**
     * @param importJobConfig the importJobConfig to set
     */
    public void setImportJobConfig(String importJobConfig) {
        this.importJobConfig = importJobConfig;
    }

    /**
     * @return the importOptions
     */
    public List<String> getImportOptions() {
        return importOptions;
    }

    /**
     * @param importOptions the importOptions to set
     */
    public void setImportOptions(List<String> importOptions) {
        this.importOptions = importOptions;
    }

}