package com.unifier.core.api.myaccount;

import java.util.List;

import com.unifier.core.api.base.ServiceResponse;

public class CreateInviteResponse extends ServiceResponse{

	/**
	 * 
	 */
	private static final long	serialVersionUID	= 1L;

	public static class InviteStatusDTO{
    	
    	private String email;

    	private boolean failed;
    	
    	private String failedReason;
		
    	public String getEmail() {
			return email;
		}
		
    	public void setEmail(String email) {
			this.email = email;
		}
		
    	public boolean isFailed() {
			return failed;
		}
		
    	public void setFailed(boolean failed) {
			this.failed = failed;
		}
		
    	public String getFailedReason() {
			return failedReason;
		}
		
    	public void setFailedReason(String failedReason) {
			this.failedReason = failedReason;
		}
    }
    
    private List<InviteStatusDTO> invites;
	
    public List<InviteStatusDTO> getInvites() {
		return invites;
	}
	
    public void setInvites(List<InviteStatusDTO> invites) {
		this.invites = invites;
	}
}
