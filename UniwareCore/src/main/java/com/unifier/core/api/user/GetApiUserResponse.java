/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 25-Feb-2014
 *  @author unicom
 */
package com.unifier.core.api.user;

import java.util.List;

import com.unifier.core.api.base.ServiceResponse;

public class GetApiUserResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 4768097674711022627L;

    private String            productionUrl;

    private String            productionWsdl;

    private List<ApiUserDTO>  apiUsers;

    private String            licenseKey;

    public List<ApiUserDTO> getApiUsers() {
        return apiUsers;
    }

    public void setApiUsers(List<ApiUserDTO> apiUsers) {
        this.apiUsers = apiUsers;
    }

    public String getProductionUrl() {
        return productionUrl;
    }

    public void setProductionUrl(String productionUrl) {
        this.productionUrl = productionUrl;
    }

    public String getProductionWsdl() {
        return productionWsdl;
    }

    public void setProductionWsdl(String productionWsdl) {
        this.productionWsdl = productionWsdl;
    }

    public String getLicenseKey() {
        return licenseKey;
    }

    public void setLicenseKey(String licenseKey) {
        this.licenseKey = licenseKey;
    }

}
