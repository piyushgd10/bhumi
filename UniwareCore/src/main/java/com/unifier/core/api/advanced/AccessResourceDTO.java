/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 23-May-2012
 *  @author vibhu
 */
package com.unifier.core.api.advanced;

/**
 * @author vibhu
 */
public class AccessResourceDTO {

    private int    id;
    private String name;
    private String level;
    private String accessGroupName;

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the accessGroupName
     */
    public String getAccessGroupName() {
        return accessGroupName;
    }

    /**
     * @param accessGroupName the accessGroupName to set
     */
    public void setAccessGroupName(String accessGroupName) {
        this.accessGroupName = accessGroupName;
    }

    /**
     * @return the level
     */
    public String getLevel() {
        return level;
    }

    /**
     * @param level the level to set
     */
    public void setLevel(String level) {
        this.level = level;
    }
}