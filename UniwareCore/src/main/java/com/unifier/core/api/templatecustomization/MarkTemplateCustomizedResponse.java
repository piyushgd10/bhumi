/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 *  @version     1.0, May 25, 2015
 *  @author akshay
 */
package com.unifier.core.api.templatecustomization;

import com.unifier.core.api.base.ServiceResponse;

public class MarkTemplateCustomizedResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 1249951721394079426L;
    
}
