/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 12-Jun-2012
 *  @author praveeng
 */
package com.unifier.core.api.reports;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author Sunny
 */
public class GetWidgetDataResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 2550810293146572049L;
    private String            data;

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

}
