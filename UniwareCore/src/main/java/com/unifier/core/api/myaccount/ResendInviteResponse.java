package com.unifier.core.api.myaccount;

import com.unifier.core.api.base.ServiceResponse;
import com.unifier.core.api.myaccount.GetInvitesByReferrerResponse.InviteDTO;

public class ResendInviteResponse extends ServiceResponse{

	/**
	 * 
	 */
	private static final long	serialVersionUID	= 5466619712330435851L;

	private InviteDTO invite;

	public InviteDTO getInvite() {
		return invite;
	}

	public void setInvite(InviteDTO invite) {
		this.invite = invite;
	}
}
