/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 16-Aug-2012
 *  @author praveeng
 */
package com.unifier.core.api.customfields;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author praveeng
 */
public class CreateCustomFieldResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -3145341229337877512L;

}
