/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 26, 2015
 *  @author harshpal
 */
package com.unifier.core.api.myaccount;

import com.unifier.core.api.base.ServiceResponse;

public class CreateAccountResponse extends ServiceResponse {

    private static final long serialVersionUID = 3516747041142539192L;

    private String            accountCode;

    public String getAccountCode() {
        return accountCode;
    }

    public void setAccountCode(String accountCode) {
        this.accountCode = accountCode;
    }

}
