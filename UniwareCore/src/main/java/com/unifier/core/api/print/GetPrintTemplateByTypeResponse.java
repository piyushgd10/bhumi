/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 *  @version     1.0, Mar 8, 2012
 *  @author praveeng
 */
package com.unifier.core.api.print;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author Sunny Agarwal
 */
public class GetPrintTemplateByTypeResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -2131162360445535194L;

    private PrintTemplateDTO  printTemplate;

    public PrintTemplateDTO getPrintTemplate() {
        return printTemplate;
    }

    public void setPrintTemplate(PrintTemplateDTO printTemplate) {
        this.printTemplate = printTemplate;
    }

}
