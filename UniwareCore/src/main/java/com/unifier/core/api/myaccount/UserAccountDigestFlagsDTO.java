package com.unifier.core.api.myaccount;


/**
 * Created by akshayag on 7/27/15.
 */

public class UserAccountDigestFlagsDTO {

    private boolean emailDailyDigest;

    private boolean emailWeeklyDigest;

    private boolean emailChannelStatus;

    private boolean emailHourlyOrderSummary;

    private boolean smsDailyDigest;

    private boolean smsWeeklyDigest;

    private boolean smsChannelStatus;

    private boolean smsHourlyOrderSummary;

    public UserAccountDigestFlagsDTO() {
        
    }
    
    public boolean isEmailDailyDigest() {
        return emailDailyDigest;
    }

    public void setEmailDailyDigest(boolean emailDailyDigest) {
        this.emailDailyDigest = emailDailyDigest;
    }

    public boolean isEmailWeeklyDigest() {
        return emailWeeklyDigest;
    }

    public void setEmailWeeklyDigest(boolean emailWeeklyDigest) {
        this.emailWeeklyDigest = emailWeeklyDigest;
    }

    public boolean isEmailChannelStatus() {
        return emailChannelStatus;
    }

    public void setEmailChannelStatus(boolean emailChannelStatus) {
        this.emailChannelStatus = emailChannelStatus;
    }

    public boolean isEmailHourlyOrderSummary() {
        return emailHourlyOrderSummary;
    }

    public void setEmailHourlyOrderSummary(boolean emailHourlyOrderSummary) {
        this.emailHourlyOrderSummary = emailHourlyOrderSummary;
    }

    public boolean isSmsDailyDigest() {
        return smsDailyDigest;
    }

    public void setSmsDailyDigest(boolean smsDailyDigest) {
        this.smsDailyDigest = smsDailyDigest;
    }

    public boolean isSmsWeeklyDigest() {
        return smsWeeklyDigest;
    }

    public void setSmsWeeklyDigest(boolean smsWeeklyDigest) {
        this.smsWeeklyDigest = smsWeeklyDigest;
    }

    public boolean isSmsChannelStatus() {
        return smsChannelStatus;
    }

    public void setSmsChannelStatus(boolean smsChannelStatus) {
        this.smsChannelStatus = smsChannelStatus;
    }

    public boolean isSmsHourlyOrderSummary() {
        return smsHourlyOrderSummary;
    }

    public void setSmsHourlyOrderSummary(boolean smsHourlyOrderSummary) {
        this.smsHourlyOrderSummary = smsHourlyOrderSummary;
    }
}
