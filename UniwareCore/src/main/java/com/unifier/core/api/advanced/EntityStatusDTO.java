/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 29-Dec-2014
 *  @author akshaykochhar
 */
package com.unifier.core.api.advanced;

import java.io.Serializable;

public class EntityStatusDTO implements Serializable {

    private String code;
    private String description;
    
    public EntityStatusDTO(){}

    public EntityStatusDTO(String code, String description) {
        super();
        this.code = code;
        this.description = description;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
