/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 06-Jun-2012
 *  @author praveeng
 */
package com.unifier.core.api.export;

import com.unifier.core.api.base.ServiceRequest;

import javax.validation.constraints.NotNull;

/**
 * @author praveeng
 */
public class EditExportJobRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 7026558992074312042L;

    @NotNull
    private String exportJobId;

    @NotNull
    private Boolean enabled;

    /**
     * @return the exportJobId
     */
    public String getExportJobId() {
        return exportJobId;
    }

    /**
     * @param exportJobId the exportJobId to set
     */
    public void setExportJobId(String exportJobId) {
        this.exportJobId = exportJobId;
    }

    /**
     * @return the enabled
     */
    public Boolean getEnabled() {
        return enabled;
    }

    /**
     * @param enabled the enabled to set
     */
    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

}
