/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *  @version     1.0, 31-July-2015
 *  @author akshaykochhar
 */
package com.unifier.core.api.myaccount;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

/**
 * Created by akshaykochhar on 31/07/15.
 */
public class UpdateUserDigestRequest extends ServiceRequest {


    /**
     * 
     */
    private static final long serialVersionUID = 2651919731274767703L;
    @NotBlank
    private String            tenantCode;
    @NotBlank
    private String            username;
    
    private boolean           emailDailyDigest        = false;

    private boolean           emailWeeklyDigest       = false;

    private boolean           emailChannelStatus      = false;

    private boolean           emailHourlyOrderSummary = false;

    private boolean           smsDailyDigest          = false;

    private boolean           smsWeeklyDigest         = false;

    private boolean           smsChannelStatus        = false;

    private boolean           smsHourlyOrderSummary   = false;
    
    
    public UpdateUserDigestRequest() {
        
    }

    public UpdateUserDigestRequest(String tenantCode, String username, boolean emailDailyDigest, boolean emailWeeklyDigest, boolean emailChannelStatus,
            boolean emailHourlyOrderSummary, boolean smsDailyDigest, boolean smsWeeklyDigest, boolean smsChannelStatus, boolean smsHourlyOrderSummary) {
        super();
        this.tenantCode = tenantCode;
        this.username = username;
        this.emailDailyDigest = emailDailyDigest;
        this.emailWeeklyDigest = emailWeeklyDigest;
        this.emailChannelStatus = emailChannelStatus;
        this.emailHourlyOrderSummary = emailHourlyOrderSummary;
        this.smsDailyDigest = smsDailyDigest;
        this.smsWeeklyDigest = smsWeeklyDigest;
        this.smsChannelStatus = smsChannelStatus;
        this.smsHourlyOrderSummary = smsHourlyOrderSummary;
    }

    public String getTenantCode() {
        return tenantCode;
    }

    public void setTenantCode(String tenantCode) {
        this.tenantCode = tenantCode;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public boolean isEmailDailyDigest() {
        return emailDailyDigest;
    }

    public void setEmailDailyDigest(boolean emailDailyDigest) {
        this.emailDailyDigest = emailDailyDigest;
    }

    public boolean isEmailWeeklyDigest() {
        return emailWeeklyDigest;
    }

    public void setEmailWeeklyDigest(boolean emailWeeklyDigest) {
        this.emailWeeklyDigest = emailWeeklyDigest;
    }

    public boolean isEmailChannelStatus() {
        return emailChannelStatus;
    }

    public void setEmailChannelStatus(boolean emailChannelStatus) {
        this.emailChannelStatus = emailChannelStatus;
    }

    public boolean isEmailHourlyOrderSummary() {
        return emailHourlyOrderSummary;
    }

    public void setEmailHourlyOrderSummary(boolean emailHourlyOrderSummary) {
        this.emailHourlyOrderSummary = emailHourlyOrderSummary;
    }

    public boolean isSmsDailyDigest() {
        return smsDailyDigest;
    }

    public void setSmsDailyDigest(boolean smsDailyDigest) {
        this.smsDailyDigest = smsDailyDigest;
    }

    public boolean isSmsWeeklyDigest() {
        return smsWeeklyDigest;
    }

    public void setSmsWeeklyDigest(boolean smsWeeklyDigest) {
        this.smsWeeklyDigest = smsWeeklyDigest;
    }

    public boolean isSmsChannelStatus() {
        return smsChannelStatus;
    }

    public void setSmsChannelStatus(boolean smsChannelStatus) {
        this.smsChannelStatus = smsChannelStatus;
    }

    public boolean isSmsHourlyOrderSummary() {
        return smsHourlyOrderSummary;
    }

    public void setSmsHourlyOrderSummary(boolean smsHourlyOrderSummary) {
        this.smsHourlyOrderSummary = smsHourlyOrderSummary;
    }
}
