/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 06-Jun-2012
 *  @author praveeng
 */
package com.unifier.core.api.export;

import com.unifier.core.api.base.ServiceRequest;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * @author praveeng
 */
public class EditExportsSubscriptionRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 6977205265138582188L;
    @NotNull
    private Integer           userId;

    @Valid
    private List<ExportSubscription> exportSubscriptions = new ArrayList<ExportSubscription>();

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return the exportSubscriptions
     */
    public List<ExportSubscription> getExportSubscriptions() {
        return exportSubscriptions;
    }

    /**
     * @param exportSubscriptions the exportSubscriptions to set
     */
    public void setExportSubscriptions(List<ExportSubscription> exportSubscriptions) {
        this.exportSubscriptions = exportSubscriptions;
    }

    public static class ExportSubscription {
        @NotNull
        private String exportJobId;

        private boolean subscribed;

        /**
         * @return the exportJobId
         */
        public String getExportJobId() {
            return exportJobId;
        }

        /**
         * @param exportJobId the exportJobId to set
         */
        public void setExportJobId(String exportJobId) {
            this.exportJobId = exportJobId;
        }

        /**
         * @return the subscribed
         */
        public boolean isSubscribed() {
            return subscribed;
        }

        /**
         * @param subscribed the subscribed to set
         */
        public void setSubscribed(boolean subscribed) {
            this.subscribed = subscribed;
        }

    }
}
