/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 23-Feb-2015
 *  @author akshay
 */
package com.unifier.core.api.myaccount;

import java.math.BigDecimal;

public class UsageStatasticsDTO {

    private String     duration;
    private BigDecimal totalCharges;
    private Long       billedUnits;

    public UsageStatasticsDTO() {
        super();
    }

    public UsageStatasticsDTO(String duration, BigDecimal totalCharges, Long billedUnits) {
        super();
        this.duration = duration;
        this.totalCharges = totalCharges != null ? totalCharges : new BigDecimal(0);
        this.billedUnits = billedUnits != null ? billedUnits : new Long(0);
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public BigDecimal getTotalCharges() {
        return totalCharges;
    }

    public void setTotalCharges(BigDecimal totalCharges) {
        this.totalCharges = totalCharges;
    }

    public Long getBilledUnits() {
        return billedUnits;
    }

    public void setBilledUnits(Long billedUnits) {
        this.billedUnits = billedUnits;
    }

    @Override public String toString() {
        return "UsageStatasticsDTO{" +
                "duration='" + duration + '\'' +
                ", totalCharges=" + totalCharges +
                ", billedUnits=" + billedUnits +
                '}';
    }
}
