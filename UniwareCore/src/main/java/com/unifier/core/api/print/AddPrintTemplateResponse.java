/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 *  @version     1.0, Mar 8, 2012
 *  @author praveeng
 */
package com.unifier.core.api.print;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author Sunny Agarwal
 */
public class AddPrintTemplateResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 4535119157306040807L;

}
