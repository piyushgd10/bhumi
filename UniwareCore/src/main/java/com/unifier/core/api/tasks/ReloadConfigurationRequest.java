/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 25-Mar-2013
 *  @author Pankaj
 */
package com.unifier.core.api.tasks;

import org.hibernate.validator.constraints.NotBlank;

import com.unifier.core.api.base.ServiceRequest;

/**
 * @author Pankaj
 */
public class ReloadConfigurationRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 4634293032809970667L;

    @NotBlank
    private String            type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}
