package com.unifier.core.api.myaccount;

import java.math.BigDecimal;
import java.util.List;

import com.unifier.core.api.base.ServiceResponse;

public class GetInvitesByReferrerResponse extends ServiceResponse{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public static class InviteDTO {

    	private String code;

    	private String name;

    	private String email;
    	
    	private String mobile;

    	private String referrerCode;

    	private String status;
    	
    	private boolean expired;

    	private Integer resendCount;
    	
    	private BigDecimal referralBonus = BigDecimal.ZERO;

    	public InviteDTO() {

    	}

    	public String getName() {
    		return name;
    	}

    	public void setName(String name) {
    		this.name = name;
    	}

    	public String getEmail() {
    		return email;
    	}

    	public void setEmail(String email) {
    		this.email = email;
    	}

    	public String getReferrerCode() {
    		return referrerCode;
    	}

    	public void setReferrerCode(String referrerCode) {
    		this.referrerCode = referrerCode;
    	}

    	public Integer getResendCount() {
    		return resendCount;
    	}

    	public void setResendCount(Integer resendCount) {
    		this.resendCount = resendCount;
    	}

    	public String getCode() {
    		return code;
    	}

    	public void setCode(String code) {
    		this.code = code;
    	}

    	public String getStatus() {
    		return status;
    	}

    	public void setStatus(String status) {
    		this.status = status;
    	}

    	public boolean isExpired() {
    		return expired;
    	}

    	public void setExpired(boolean expired) {
    		this.expired = expired;
    	}

    	public BigDecimal getReferralBonus() {
    		return referralBonus;
    	}

    	public void setReferralBonus(BigDecimal referralBonus) {
    		this.referralBonus = referralBonus;
    	}

		public String getMobile() {
			return mobile;
		}

		public void setMobile(String mobile) {
			this.mobile = mobile;
		}
    }
    
    private List<InviteDTO> inviteDTOs;

    public List<InviteDTO> getInviteDTOs() {
        return inviteDTOs;
    }

    public void setInviteDTOs(List<InviteDTO> inviteDTOs) {
        this.inviteDTOs = inviteDTOs;
    }
    
    public void addInviteDTO(InviteDTO inviteDTO){
        this.inviteDTOs.add(inviteDTO);
    }
}
