/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 11-Nov-2013
 *  @author sunny
 */
package com.unifier.core.configuration.data.manager;

import java.util.List;

import com.uniware.core.entity.TaxType;

public interface IConfigurationDataManager {

    List<TaxType> getAllTaxTypes();

}
