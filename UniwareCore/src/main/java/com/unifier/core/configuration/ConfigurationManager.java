/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 12, 2011
 *  @author singla
 */
package com.unifier.core.configuration;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Lock;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;

import com.unifier.core.annotation.Configuration;
import com.unifier.core.annotation.Level;
import com.unifier.core.cache.CacheDirtyEvent;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.jms.IActiveMQConnector;
import com.unifier.core.jms.MessagingConstants;
import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.ReflectionUtils;
import com.uniware.core.cache.FacilityCache;
import com.uniware.core.entity.Facility;
import com.uniware.core.locking.ILockingService;
import com.uniware.core.locking.Namespace;
import com.uniware.core.utils.UserContext;

/**
 * @author singla
 */
public class ConfigurationManager {

    private static final Logger                                   LOG                               = LoggerFactory.getLogger(ConfigurationManager.class);
    private static ConfigurationManager                           _instance                         = new ConfigurationManager();
    private final Map<String, Level>                              _configurationNameToLevels        = new HashMap<String, Level>();
    private final Map<String, Class<?>>                           _configurationNameToClass         = new HashMap<String, Class<?>>();
    private final List<Class<?>>                                  _eagerConfigurations              = new ArrayList<>();
    private final List<Class<?>>                                  _eagerFacilityLevelConfigurations = new ArrayList<>();
    private final List<Class<?>>                                  _eagerGlobalConfigurations        = new ArrayList<>();

    private Map<String, IConfiguration>                           _globalConfigurations             = new ConcurrentHashMap<>();
    private Map<String, Map<String, IConfiguration>>              _tenantConfigurations             = new ConcurrentHashMap<>();
    private Map<String, Map<String, Map<String, IConfiguration>>> _facilityConfigurations           = new ConcurrentHashMap<>();

    private Map<Class<?>, Date>                                   configurationToLastMarkDirtyTime  = new ConcurrentHashMap<>();

    @Autowired
    private ApplicationContext                                    applicationContext;

    @Autowired
    private ILockingService                                       lockingService;

    @Autowired
    @Qualifier("activeMQConnector1")
    private IActiveMQConnector                                    activeMQConnector;

    private ConfigurationManager() {
        try {
            Set<String> _configurationNames = new HashSet<>();
            for (Class<?> clazz : ReflectionUtils.getClassesAnnotatedWith(Configuration.class, "com.**.configuration")) {
                Configuration configuration = clazz.getAnnotation(Configuration.class);
                if (_configurationNames.contains(configuration.name())) {
                    LOG.error("Multiple configurations cannot have the same name: {}", configuration.name());
                    throw new RuntimeException("Multiple configurations cannot have the same name: " + configuration.name());
                } else {
                    _configurationNames.add(configuration.name());
                }
                _configurationNameToLevels.put(configuration.name(), configuration.level());
                _configurationNameToClass.put(configuration.name(), clazz);
                if (configuration.eager()) {
                    switch (configuration.level()) {
                        case GLOBAL:
                            _eagerGlobalConfigurations.add(clazz);
                            break;
                        case FACILITY:
                            _eagerFacilityLevelConfigurations.add(clazz);
                            break;
                        default:
                            _eagerConfigurations.add(clazz);
                    }
                }
            }
        } catch (IOException | ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    public static ConfigurationManager getInstance() {
        return _instance;
    }

    private IConfiguration getConfigurationInternal(String configurationName) {
        Level level = _configurationNameToLevels.get(configurationName);
        IConfiguration configuration = null;
        if (level == Level.TENANT) {
            Map<String, IConfiguration> caches = _tenantConfigurations.get(UserContext.current().getTenant().getCode());
            if (caches != null) {
                configuration = caches.get(configurationName);
            }
        } else if (level == Level.FACILITY) {
            String tenantCode = UserContext.current().getTenant().getCode();
            if (_facilityConfigurations.containsKey(tenantCode)) {
                Map<String, IConfiguration> caches = null;
                if (UserContext.current().getFacility() == null) {
                    Facility firstFacility = CacheManager.getInstance().getCache(FacilityCache.class).getFacilities().get(0);
                    caches = _facilityConfigurations.get(tenantCode).get(firstFacility.getCode());
                } else {
                    caches = _facilityConfigurations.get(tenantCode).get(UserContext.current().getFacility().getCode());
                }
                if (caches != null) {
                    configuration = caches.get(configurationName);
                }
            }
        } else {
            configuration = _globalConfigurations.get(configurationName);
        }
        return configuration;
    }

    public IConfiguration getConfiguration(String configurationName) {
        IConfiguration configuration = getConfigurationInternal(configurationName);
        if (configuration == null) {
            Level level = _configurationNameToLevels.get(configurationName);
            String lockName = "configurationLock" + configurationName;
            Lock lock = lockingService.getLock(Namespace.CONFIGURATION_RELOAD, lockName, level);
            try {
                LOG.info("Acquiring configuration lock for: {}", configurationName);
                long startTime = System.currentTimeMillis();
                lock.lock();
                LOG.info("Lock acquired for: {} in {} ms", configurationName, (System.currentTimeMillis() - startTime));
                configuration = getConfigurationInternal(configurationName);
                if (configuration == null) {
                    configuration = loadConfiguration(configurationName);
                }
            } finally {
                lock.unlock();
                LOG.info("Released configuration lock for: {}", configurationName);
            }
        }
        return configuration;
    }

    @SuppressWarnings("unchecked")
    public <T> T getConfiguration(Class<T> configurationClass) {
        if (configurationClass.isAnnotationPresent(Configuration.class)) {
            return (T) getConfiguration(configurationClass.getAnnotation(Configuration.class).name());
        } else {
            throw new IllegalArgumentException("@Configuration annotation should be present for configuration class:" + configurationClass.getName());
        }
    }

    public void doClearTenantConfigurations() {
        String tenantCode = UserContext.current().getTenant().getCode();
        LOG.info("Clearing all configurations for tenant: {}", UserContext.current().getTenant().getName());
        for (Facility facility : CacheManager.getInstance().getCache(FacilityCache.class).getFacilities()) {
            if (_facilityConfigurations.containsKey(tenantCode)) {
                Map<String, IConfiguration> facilityCaches = _facilityConfigurations.get(tenantCode).get(facility.getCode());
                if (facilityCaches != null) {
                    facilityCaches.clear();
                }
            }
        }
        Map<String, IConfiguration> tenantCaches = _tenantConfigurations.get(UserContext.current().getTenant().getCode());
        if (tenantCaches != null) {
            tenantCaches.clear();
        }
        _globalConfigurations.clear();
        loadEagerGlobalConfigurations();
        loadEagerConfigurations();
        for (Facility facility : CacheManager.getInstance().getCache(FacilityCache.class).getFacilities()) {
            UserContext.current().setFacility(facility);
            if (_facilityConfigurations.containsKey(tenantCode)) {
                Map<String, IConfiguration> facilityCaches = _facilityConfigurations.get(tenantCode).get(facility.getCode());
                if (facilityCaches != null) {
                    loadEagerFacilityLevelConfigurations();
                }
            }
        }
    }

    public void doMarkConfigurationDirty(String configurationName, Date requestTimestamp) {
        doMarkConfigurationDirty(_configurationNameToClass.get(configurationName), requestTimestamp);
    }

    private <T> void doMarkConfigurationDirty(Class<T> configurationClass, Date requestTimestamp) {
        if (configurationClass.isAnnotationPresent(Configuration.class)) {
            Configuration annotation = configurationClass.getAnnotation(Configuration.class);
            Date lastMarkDirtyTimestamp = configurationToLastMarkDirtyTime.get(configurationClass);
            if (requestTimestamp != null && lastMarkDirtyTimestamp != null && lastMarkDirtyTimestamp.after(requestTimestamp)) {
                LOG.info("Skipping configuration dirty event with timestamp: {} as {} was marked dirty at: {}",
                        new Object[] { requestTimestamp, configurationClass, lastMarkDirtyTimestamp });
            } else {
                boolean removed = false;
                LOG.info("Marking configuration as dirty: {}", annotation.name());
                if (annotation.level() == Level.TENANT) {
                    Map<String, IConfiguration> caches = _tenantConfigurations.get(UserContext.current().getTenant().getCode());
                    if (caches != null) {
                        removed = true;
                        caches.remove(annotation.name());
                    }
                } else if (annotation.level() == Level.FACILITY) {
                    Facility currentFacility = UserContext.current().getFacility();
                    String tenantCode = UserContext.current().getTenant().getCode();
                    if (currentFacility != null && _facilityConfigurations.containsKey(tenantCode)) {
                        Map<String, IConfiguration> caches = _facilityConfigurations.get(tenantCode).get(currentFacility.getCode());
                        if (caches != null) {
                            removed = true;
                            caches.remove(annotation.name());
                        }
                    }
                } else {
                    removed = true;
                    _globalConfigurations.remove(annotation.name());
                }
                configurationToLastMarkDirtyTime.put(configurationClass, DateUtils.getCurrentTime());
                if (removed) {
                    loadConfiguration(configurationClass);
                }
            }
        } else {
            throw new IllegalArgumentException("@Configuration annotation should be present for configuration class:" + configurationClass.getName());
        }
    }

    public void clearTenantConfigurations() {
        activeMQConnector.produceContextAwareMessage(MessagingConstants.Queue.CACHE_DIRTY,
                new CacheDirtyEvent(CacheDirtyEvent.Type.CONFIGURATION, CacheDirtyEvent.Action.CLEAR_ALL, null, null));
    }

    public void markConfigurationDirty(String configurationName) {
        markConfigurationDirty(_configurationNameToClass.get(configurationName));
    }

    public <T> void markConfigurationDirty(Class<T> configurationClass) {
        if (configurationClass.isAnnotationPresent(Configuration.class)) {
            Configuration annotation = configurationClass.getAnnotation(Configuration.class);
            Date configurationDirtyRequestTimestamp = DateUtils.getCurrentTime();
            doMarkConfigurationDirty(configurationClass, configurationDirtyRequestTimestamp);
            activeMQConnector.produceContextAwareMessage(MessagingConstants.Queue.CACHE_DIRTY,
                    new CacheDirtyEvent(CacheDirtyEvent.Type.CONFIGURATION, CacheDirtyEvent.Action.CLEAR_SPECIFIC, annotation.name(), configurationDirtyRequestTimestamp));
        } else {
            throw new IllegalArgumentException("@Cache annotation should be present for cache class:" + configurationClass.getName());
        }
    }

    public <T> IConfiguration loadConfiguration(String configurationName) {
        return loadConfiguration(_configurationNameToClass.get(configurationName));
    }

    public <T> IConfiguration loadConfiguration(Class<T> configurationClass) {
        try {
            LOG.info("Loading configuration {}", configurationClass.getName());
            long startTime = System.currentTimeMillis();
            IConfiguration configuration = (IConfiguration) configurationClass.newInstance();
            applicationContext.getAutowireCapableBeanFactory().autowireBeanProperties(configuration, AutowireCapableBeanFactory.AUTOWIRE_BY_TYPE, false);
            configuration.load();
            setConfiguration(configuration);
            LOG.info("Done loading {} in {} ms", configurationClass.getName(), System.currentTimeMillis() - startTime);
            return configuration;
        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        }
    }

    public void loadEagerConfigurations() {
        for (Class<?> clazz : _eagerConfigurations) {
            loadConfiguration(clazz);
        }
    }

    public void loadEagerFacilityLevelConfigurations() {
        for (Class<?> clazz : _eagerFacilityLevelConfigurations) {
            loadConfiguration(clazz);
        }
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    public void loadEagerGlobalConfigurations() {
        for (Class clazz : _eagerGlobalConfigurations) {
            loadConfiguration(clazz);
        }
    }

    public void setConfiguration(IConfiguration configuration) {
        Class<? extends Object> configurationClass = configuration.getClass();
        if (configurationClass.isAnnotationPresent(Configuration.class)) {
            for (Method m : configurationClass.getDeclaredMethods()) {
                if ("freeze".equals(m.getName())) {
                    try {
                        m.invoke(configuration);
                    } catch (Exception e) {
                        LOG.error("unable to freeze configuration:" + configurationClass.getName(), e);
                    }
                }
            }
            Configuration annotation = configurationClass.getAnnotation(Configuration.class);
            if (annotation.level() == Level.TENANT) {
                Map<String, IConfiguration> tenantConfiguration = _tenantConfigurations.get(UserContext.current().getTenant().getCode());
                if (tenantConfiguration == null) {
                    tenantConfiguration = new ConcurrentHashMap<>();
                    _tenantConfigurations.put(UserContext.current().getTenant().getCode(), tenantConfiguration);
                }
                tenantConfiguration.put(annotation.name(), configuration);
            } else if (annotation.level() == Level.FACILITY) {
                Map<String, Map<String, IConfiguration>> tenantFacilityConfiguration = _facilityConfigurations.get(UserContext.current().getTenant().getCode());
                if (tenantFacilityConfiguration == null) {
                    tenantFacilityConfiguration = new ConcurrentHashMap<>();
                    _facilityConfigurations.put(UserContext.current().getTenant().getCode(), tenantFacilityConfiguration);
                }
                Map<String, IConfiguration> facilityConfiguration = tenantFacilityConfiguration.get(UserContext.current().getFacility().getCode());
                if (facilityConfiguration == null) {
                    facilityConfiguration = new ConcurrentHashMap<>();
                    tenantFacilityConfiguration.put(UserContext.current().getFacility().getCode(), facilityConfiguration);
                }
                facilityConfiguration.put(annotation.name(), configuration);
            } else {
                _globalConfigurations.put(annotation.name(), configuration);
            }
        } else {
            throw new IllegalArgumentException("@Configuration annotation should be present for configuration class:" + configuration.getClass().getName());
        }

    }

}
