package com.unifier.core.entity;

// Generated Mar 21, 2012 11:55:18 AM by Hibernate Tools 3.4.0.CR1

import java.util.Date;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "importJob")
public class ImportJob {

    public enum Status {
        SCHEDULED,
        RUNNING,
        FAILED,
        COMPLETE
    }

    private String  id;
    private String  userName;
    private String  importJobTypeName;
    private String  tenantCode;
    private String  facilityCode;
    private String  description;
    private Date    scheduledTime;
    private String  notificationEmail;
    private String  importOption;
    private Status  statusCode;
    private boolean successful;
    private int     mileStoneCount = 100;
    private int     currentMileStone;
    private String  message;
    private String  fileName;
    private String  filePath;
    private String  logFilePath;
    private int     totalRows;
    private int     rowsProcessed;
    private int     successfulImportCount;
    private int     failedImportCount;
    private Date    created;
    private Date    updated;

    public static class ImportResult {

    }

    public ImportJob() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getImportJobTypeName() {
        return importJobTypeName;
    }

    public void setImportJobTypeName(String importJobTypeName) {
        this.importJobTypeName = importJobTypeName;
    }

    public String getTenantCode() {
        return tenantCode;
    }

    public void setTenantCode(String tenantCode) {
        this.tenantCode = tenantCode;
    }

    public String getFacilityCode() {
        return facilityCode;
    }

    public void setFacilityCode(String facilityCode) {
        this.facilityCode = facilityCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getScheduledTime() {
        return scheduledTime;
    }

    public void setScheduledTime(Date scheduledTime) {
        this.scheduledTime = scheduledTime;
    }

    public String getNotificationEmail() {
        return notificationEmail;
    }

    public void setNotificationEmail(String notificationEmail) {
        this.notificationEmail = notificationEmail;
    }

    public String getImportOption() {
        return importOption;
    }

    public void setImportOption(String importOption) {
        this.importOption = importOption;
    }

    public Status getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Status statusCode) {
        this.statusCode = statusCode;
    }

    public boolean isSuccessful() {
        return successful;
    }

    public void setSuccessful(boolean successful) {
        this.successful = successful;
    }

    public int getMileStoneCount() {
        return mileStoneCount;
    }

    public void setMileStoneCount(int mileStones) {
        if (mileStones > 0) {
            mileStoneCount = mileStones;
        }
        currentMileStone = 0;
        statusCode = Status.RUNNING;
        message = "initializing...";
    }

    public int getCurrentMileStone() {
        return currentMileStone;
    }

    public void setCurrentMileStone(int currentMileStone) {
        this.currentMileStone = currentMileStone;
    }

    public void setMileStone(String currentStatus) {
        this.message = currentStatus;
        currentMileStone++;
    }

    public void setMileStone(String currentStatus, int milestones) {
        this.message = currentStatus;
        currentMileStone += milestones;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getLogFilePath() {
        return logFilePath;
    }

    public void setLogFilePath(String logFilePath) {
        this.logFilePath = logFilePath;
    }

    public int getTotalRows() {
        return totalRows;
    }

    public void setTotalRows(int totalRows) {
        this.totalRows = totalRows;
    }

    public int getRowsProcessed() {
        return rowsProcessed;
    }

    public void setRowsProcessed(int rowsProcessed) {
        this.rowsProcessed = rowsProcessed;
    }

    public int getSuccessfulImportCount() {
        return successfulImportCount;
    }

    public void setSuccessfulImportCount(int successfulImportCount) {
        this.successfulImportCount = successfulImportCount;
    }

    public int getFailedImportCount() {
        return failedImportCount;
    }

    public void setFailedImportCount(int failedImportCount) {
        this.failedImportCount = failedImportCount;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
}
