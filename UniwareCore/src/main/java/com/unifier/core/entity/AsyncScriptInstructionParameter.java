/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 15-Sep-2014
 *  @author unicom
 */
package com.unifier.core.entity;

import java.io.Serializable;

public class AsyncScriptInstructionParameter implements Serializable {

    private String                 name;
    private Object                 value;

    public AsyncScriptInstructionParameter() {
    }

    public AsyncScriptInstructionParameter(AsyncScriptInstruction instruction, String name, String value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }
}
