package com.unifier.core.entity;

import java.util.Date;
import com.unifier.core.api.export.CreateExportJobRequest;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "exportJob")
public class ExportJob {

    public enum Status {
        SCHEDULED,
        RUNNING,
        COMPLETE,
        FAILED,
        INTERRUPTED
    }

    public enum Frequency {
        ONETIME,
        RECURRENT
    }

    private String                 id;
    private String                 username;
    private String                 exportJobTypeName;
    private String                 tenantCode;
    private String                 facilityCode;
    private Date                   scheduledTime;
    private Integer                taskDurationInSec;
    private String                 frequency;
    private String                 cronExpression;
    private boolean                enabled        = true;
    private String                 notificationEmail;
    private String                 description;
    private CreateExportJobRequest request;
    private Status                 statusCode;
    private boolean                successful;
    private String                 message;
    private long                   mileStoneCount = 100;
    private long                   currentMileStone;
    private long                   exportCount;
    private String                 exportFilePath;
    private Date                   created;
    private Date                   updated;

    public ExportJob() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getExportJobTypeName() {
        return exportJobTypeName;
    }

    public void setExportJobTypeName(String exportJobTypeName) {
        this.exportJobTypeName = exportJobTypeName;
    }

    public String getTenantCode() {
        return tenantCode;
    }

    public void setTenantCode(String tenantCode) {
        this.tenantCode = tenantCode;
    }

    public String getFacilityCode() {
        return facilityCode;
    }

    public void setFacilityCode(String facilityCode) {
        this.facilityCode = facilityCode;
    }

    public String getFrequency() {
        return frequency;
    }

    public void setFrequency(String frequency) {
        this.frequency = frequency;
    }

    public String getCronExpression() {
        return cronExpression;
    }

    public void setCronExpression(String cronExpression) {
        this.cronExpression = cronExpression;
    }

    public String getNotificationEmail() {
        return notificationEmail;
    }

    public void setNotificationEmail(String notificationEmail) {
        this.notificationEmail = notificationEmail;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public CreateExportJobRequest getRequest() {
        return request;
    }

    public void setRequest(CreateExportJobRequest request) {
        this.request = request;
    }

    public Status getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Status statusCode) {
        this.statusCode = statusCode;
    }

    public boolean isSuccessful() {
        return successful;
    }

    public void setSuccessful(boolean successful) {
        this.successful = successful;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public long getMileStoneCount() {
        return mileStoneCount;
    }

    public void setMileStoneCount(long mileStones) {
        if (mileStones > 0) {
            mileStoneCount = mileStones;
        }
        currentMileStone = 0;
        statusCode = Status.RUNNING;
        message = "initializing...";
    }

    public long getCurrentMileStone() {
        return currentMileStone;
    }

    public void setCurrentMileStone(long currentMileStone) {
        this.currentMileStone = currentMileStone;
    }

    public void setMileStone(String currentStatus) {
        this.message = currentStatus;
        currentMileStone++;
    }

    public void setMileStone(String currentStatus, int milestones) {
        this.message = currentStatus;
        currentMileStone += milestones;
    }

    public long getExportCount() {
        return exportCount;
    }

    public void setExportCount(long exportCount) {
        this.exportCount = exportCount;
    }

    public Integer getTaskDurationInSec() {
        return taskDurationInSec;
    }

    public void setTaskDurationInSec(Integer taskDurationInSec) {
        this.taskDurationInSec = taskDurationInSec;
    }

    public String getExportFilePath() {
        return exportFilePath;
    }

    public void setExportFilePath(String exportFilePath) {
        this.exportFilePath = exportFilePath;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public Date getScheduledTime() {
        return scheduledTime;
    }

    public void setScheduledTime(Date scheduledTime) {
        this.scheduledTime = scheduledTime;
    }

    @Override public String toString() {
        return "ExportJob{" +
                "exportJobTypeName='" + exportJobTypeName + '\'' +
                ", facilityCode='" + facilityCode + '\'' +
                ", description='" + description + '\'' +
                ", username='" + username + '\'' +
                ", successful=" + successful +
                ", created=" + created +
                '}';
    }
}
