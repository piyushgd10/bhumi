package com.unifier.core.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;


@Entity
@Table(name = "search_types", uniqueConstraints = @UniqueConstraint(columnNames = "name"))
public class SearchTypes implements java.io.Serializable {
    
     public enum Type {
         SALE_ORDER,
         FAILED_SALE_ORDER,
         SHIPPING_PACKAGE,
         PURCHASE_ORDER,
         ITEM_TYPE,
         MANIFEST,
         INVOICE,
         VENDOR
     }
    /**
     * 
     */
    private static final long serialVersionUID = 1121856647143809407L;
    private Integer           id;
    private String            type;
    private String            name;  
    private AccessResource    accessResource;
    private String            landingPageUrl;
    private String            placeHolderText;
    private int               displayOrder;
    private boolean           allFacilityType;
    private boolean           hidden;
    private Date              created;
    private Date              updated;

    public SearchTypes() {
    }

    public SearchTypes(String name, AccessResource accessResource, String landingPageUrl, String placeHolderText, boolean allFaciltyType, Date created, Date updated) {
        this.name = name;
        this.accessResource = accessResource;
        this.landingPageUrl = landingPageUrl;
        this.placeHolderText = placeHolderText;
        this.allFacilityType = allFaciltyType;
        this.created = created;
        this.updated = updated;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "access_resource_id", nullable = false)
    public AccessResource getAccessResource() {
        return this.accessResource;
    }

    public void setAccessResource(AccessResource accessResource) {
        this.accessResource = accessResource;
    }

    @Column(name = "type", nullable = false, length = 45)
    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }
    
    @Column(name = "name", unique = true, nullable = false, length = 45)
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    @Column(name = "landing_page_url", nullable = false, length = 100)
    public String getLandingPageUrl() {
        return this.landingPageUrl;
    }

    public void setLandingPageUrl(String landingPageUrl) {
        this.landingPageUrl = landingPageUrl;
    }
    
    @Column(name = "place_holder", nullable = false, length = 100)
    public String getPlaceHolderText() {
        return this.placeHolderText;
    }

    public void setPlaceHolderText(String placeHolderText) {
        this.placeHolderText = placeHolderText;
    }
    
    @Column(name = "all_facility", nullable = false)
    public boolean isAllFacilityType() {
        return this.allFacilityType;
    }
    
    public void setAllFacilityType(boolean enabled) {
        this.allFacilityType = enabled;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created", nullable = false, length = 19)
    public Date getCreated() {
        return this.created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated", nullable = false, length = 19, insertable = false, updatable = false)
    public Date getUpdated() {
        return this.updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    @Column(name = "hidden", nullable = false)
    public boolean isHidden() {
        return hidden;
    }

    public void setHidden(boolean hidden) {
        this.hidden = hidden;
    }

    @Column(name = "display_order", nullable = false)
    public int getDisplayOrder() {
        return displayOrder;
    }

    public void setDisplayOrder(int displayOrder) {
        this.displayOrder = displayOrder;
    }

}
