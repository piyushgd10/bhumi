/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 26-Aug-2014
 *  @author unicom
 */
package com.unifier.core.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "asyncScriptInstruction")
public class AsyncScriptInstruction implements Serializable {

    public enum StatusCode {
        SCHEDULED,
        RUNNING,
        COMPLETE,
        RESCHEDULED,
        CANCELLED
    }

    private static final long                    serialVersionUID      = -2884212018196205157L;

    @Id
    private String                               id;
    private String                               name;
    private String                               taskClass;
    private String                               scriptName;
    private String                               instructionName;
    private Date                                 scheduledTime;
    private String                               result;
    private String                               statusCode;
    private String                               callback;
    private String                               tenantCode;
    private Set<AsyncScriptInstructionParameter> instructionParameters = new HashSet<>(0);
    private Date                                 created;
    private Date                                 updated;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTaskClass() {
        return this.taskClass;
    }

    public void setTaskClass(String taskClass) {
        this.taskClass = taskClass;
    }

    public String getScriptName() {
        return scriptName;
    }

    public void setScriptName(String scriptName) {
        this.scriptName = scriptName;
    }

    public String getInstructionName() {
        return instructionName;
    }

    public void setInstructionName(String instructionName) {
        this.instructionName = instructionName;
    }

    public Date getScheduledTime() {
        return scheduledTime;
    }

    public void setScheduledTime(Date scheduledTime) {
        this.scheduledTime = scheduledTime;
    }

    public String getResult() {
        return this.result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getStatusCode() {
        return this.statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getCallback() {
        return callback;
    }

    public void setCallback(String callback) {
        this.callback = callback;
    }

    public String getTenantCode() {
        return tenantCode;
    }

    public void setTenantCode(String tenantCode) {
        this.tenantCode = tenantCode;
    }

    public Date getCreated() {
        return this.created;
    }

    public Set<AsyncScriptInstructionParameter> getInstructionParameters() {
        return instructionParameters;
    }

    public void setInstructionParameters(Set<AsyncScriptInstructionParameter> instructionParameters) {
        this.instructionParameters = instructionParameters;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return this.updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public void addContextVariable(String key, Object value) {
        AsyncScriptInstructionParameter var = new AsyncScriptInstructionParameter();
        var.setName(key);
        var.setValue(value);
        instructionParameters.add(var);
    }

}
