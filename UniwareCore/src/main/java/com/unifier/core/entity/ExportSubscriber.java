package com.unifier.core.entity;

import org.springframework.data.mongodb.core.mapping.Document;
import java.util.Date;
import java.util.List;

@Document(collection = "subscribedExport")
public class ExportSubscriber implements java.io.Serializable {

    private String       id;
    private List<String> usernames;
    private String       exportJobId;
    private Date         created;
    private Date         updated;
    private boolean      subscribed;

    public ExportSubscriber() {
    }

    public ExportSubscriber(List<String> usernames, String exportJobId, Date created, Date updated, boolean subscribed) {
        this.usernames = usernames;
        this.exportJobId = exportJobId;
        this.created = created;
        this.updated = updated;
        this.subscribed = subscribed;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<String> getUsernames() {
        return usernames;
    }

    public void setUsernames(List<String> usernames) {
        this.usernames = usernames;
    }

    public String getExportJobId() {
        return exportJobId;
    }

    public void setExportJobId(String exportJobId) {
        this.exportJobId = exportJobId;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public boolean isSubscribed() {
        return subscribed;
    }

    public void setSubscribed(boolean subscribed) {
        this.subscribed = subscribed;
    }
}
