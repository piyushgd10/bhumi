package com.unifier.core.entity;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "saleOrderReconciliation")
public class SaleOrderReconciliation implements java.io.Serializable {

    private static final long serialVersionUID = -3527238394590105522L;

    public enum ReconciliationStatus {
        RECONCILED,
        AWAITING_PAYMENT,
        DISPUTED,
        UNRECONCILED,
        IRRECONCILIABLE,
        DISCARDED
    }

    public enum InvoicingStatus {
        AWAITING,
        CANCELLED,
        DONE
    }

    @Id
    private String          id;
    private String          reconciliationIdentifier;
    private String          channelCode;
    private String          tenantCode;
    private String          channelSaleOrderCode;
    private String          saleOrderCode;
    private String          channelProductId;
    private String          channelProductName;
    private String          invoiceCode;
    private String          invoiceDisplayCode;
    private int             quantity             = 0;
    private double          sellingPrice         = 0;
    private double          shippingCharges      = 0;
    private double          totalReconcileAmount = 0;
    private double          settlementPercentage = 0;
    private Double          itemCostPrice;
    private Double          expectedSettlementAmount;
    private String          reconciledBy;
    private String          statusCode           = ReconciliationStatus.AWAITING_PAYMENT.name();
    private InvoicingStatus invoicingStatus      = InvoicingStatus.AWAITING;
    private String          comments;
    private String          currencyCode;
    private Date            lastSettledDate;
    private Date            orderTransactionDate;
    private Date            created;
    private Date            updated;

    public SaleOrderReconciliation() {
    }

    public String getInvoiceDisplayCode() {
        return invoiceDisplayCode;
    }

    public void setInvoiceDisplayCode(String invoiceDisplayCode) {
        this.invoiceDisplayCode = invoiceDisplayCode;
    }

    public InvoicingStatus getInvoicingStatus() {
        return invoicingStatus;
    }

    public void setInvoicingStatus(InvoicingStatus invoicingStatus) {
        this.invoicingStatus = invoicingStatus;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getChannelSaleOrderCode() {
        return channelSaleOrderCode;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getSellingPrice() {
        return sellingPrice;
    }

    public void setSellingPrice(double sellingPrice) {
        this.sellingPrice = sellingPrice;
    }

    public double getShippingCharges() {
        return shippingCharges;
    }

    public void setShippingCharges(double shippingCharges) {
        this.shippingCharges = shippingCharges;
    }

    public void setChannelSaleOrderCode(String channelSaleOrderCode) {
        this.channelSaleOrderCode = channelSaleOrderCode;
    }

    public String getSaleOrderCode() {
        return saleOrderCode;
    }

    public void setSaleOrderCode(String saleOrderCode) {
        this.saleOrderCode = saleOrderCode;
    }

    public String getChannelProductId() {
        return channelProductId;
    }

    public void setChannelProductId(String channelProductId) {
        this.channelProductId = channelProductId;
    }

    public String getChannelProductName() {
        return channelProductName;
    }

    public void setChannelProductName(String channelProductName) {
        this.channelProductName = channelProductName;
    }

    public String getInvoiceCode() {
        return invoiceCode;
    }

    public void setInvoiceCode(String invoiceCode) {
        this.invoiceCode = invoiceCode;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    public String getTenantCode() {
        return tenantCode;
    }

    public void setTenantCode(String tenantCode) {
        this.tenantCode = tenantCode;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getReconciliationIdentifier() {
        return reconciliationIdentifier;
    }

    public void setReconciliationIdentifier(String reconciliationIdentifier) {
        this.reconciliationIdentifier = reconciliationIdentifier;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public Date getLastSettledDate() {
        return lastSettledDate;
    }

    public void setLastSettledDate(Date lastSettledDate) {
        this.lastSettledDate = lastSettledDate;
    }

    public Date getOrderTransactionDate() {
        return orderTransactionDate;
    }

    public void setOrderTransactionDate(Date orderTransactionDate) {
        this.orderTransactionDate = orderTransactionDate;
    }

    public double getTotalReconcileAmount() {
        return totalReconcileAmount;
    }

    public void setTotalReconcileAmount(double totalReconcileAmount) {
        this.totalReconcileAmount = totalReconcileAmount;
    }

    public double getSettlementPercentage() {
        return settlementPercentage;
    }

    public void setSettlementPercentage(double settlementPercentage) {
        this.settlementPercentage = settlementPercentage;
    }

    public Double getItemCostPrice() {
        return itemCostPrice;
    }

    public void setItemCostPrice(Double itemCostPrice) {
        this.itemCostPrice = itemCostPrice;
    }

    public Double getExpectedSettlementAmount() {
        return expectedSettlementAmount;
    }

    public void setExpectedSettlementAmount(Double expectedSettlementAmount) {
        this.expectedSettlementAmount = expectedSettlementAmount;
    }

    public String getReconciledBy() {
        return reconciledBy;
    }

    public void setReconciledBy(String reconciledBy) {
        this.reconciledBy = reconciledBy;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }
}
