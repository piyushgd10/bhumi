/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 26-Dec-2013
 *  @author akshay
 */
package com.unifier.core.entity;

import com.uniware.core.entity.Facility;
import com.uniware.core.entity.Product;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import java.util.Date;

@Entity
@Table(name = "facility_access_resource", uniqueConstraints = { @UniqueConstraint(columnNames = { "facility_type", "access_resource_name" }) })
public class FacilityAccessResource implements java.io.Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 8614345569192770863L;

    private Integer        id;
    private Facility.Type  facilityType;
    private AccessResource accessResource;
    private boolean        enabled;
    private Date           created;
    private Date           updated;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Enumerated(EnumType.STRING)
    @Column(name = "facility_type", nullable = false)
    public Facility.Type getFacilityType() {
        return facilityType;
    }

    public void setFacilityType(Facility.Type facilityType) {
        this.facilityType = facilityType;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "access_resource_name", nullable = false, referencedColumnName = "name")
    public AccessResource getAccessResource() {
        return accessResource;
    }

    public void setAccessResource(AccessResource accessResource) {
        this.accessResource = accessResource;
    }

    @Column(name = "enabled", nullable = false)
    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created", nullable = false, length = 19)
    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    @Column(name = "updated", nullable = false, length = 19, insertable = false, updatable = false)
    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

}
