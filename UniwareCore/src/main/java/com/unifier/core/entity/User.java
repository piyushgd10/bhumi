package com.unifier.core.entity;

import com.uniware.core.entity.Facility;
import com.uniware.core.entity.Tenant;
import com.uniware.core.entity.UserDatatableView;
import com.uniware.core.entity.Vendor;
import com.uniware.core.utils.UserContext;
import org.hibernate.annotations.OrderBy;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * User generated by hbm2java
 */
@Entity
@Table(name = "user", uniqueConstraints = @UniqueConstraint(columnNames = { "tenant_id", "username" }))
public class User implements java.io.Serializable {
	
	/**
     * 
     */
	private static final long		serialVersionUID	= 284390151714025827L;
	private Integer					id;
	private Tenant					tenant;
	private Facility				currentFacility;
	private String					username;
	private String					email;
	private String					userEmail;
	private String					normalizedEmail;
	private boolean					enabled				= true;
	private String					mobile;
    private boolean                 mobileVerified;
    private String                  name;
    private boolean                 blocked;
    private boolean                 hidden;
    private Date                    lastLoginTime;
    private String                  lastAccessedFrom;
    private Date                    created;
    private Date                    updated;
    private Set<Vendor>            vendors            = new HashSet<Vendor>(0);
    private Set<UserRole>          userRoles          = new HashSet<UserRole>(0);
    private Set<UserDatatableView> userDatatableViews = new HashSet<UserDatatableView>(0);

    public User() {
    }

    public User(int id) {
        this.id = id;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "tenant_id", nullable = false)
    public Tenant getTenant() {
        return this.tenant;
    }

    public void setTenant(Tenant tenant) {
        this.tenant = tenant;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "current_facility_id")
    public Facility getCurrentFacility() {
        return this.currentFacility;
    }

    public void setCurrentFacility(Facility currentFacility) {
        this.currentFacility = currentFacility;
    }

    @Column(name = "username", unique = true, nullable = false, length = 45)
    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Column(name = "email", length = 100)
    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Column(name = "user_email", length = 100)
    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    @Column(name = "normalized_email", length = 100)
    public String getNormalizedEmail() {
        return normalizedEmail;
    }

    public void setNormalizedEmail(String normalizedEmail) {
        this.normalizedEmail = normalizedEmail;
    }

    @Column(name = "enabled", nullable = false)
    public boolean isEnabled() {
        return this.enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Column(name = "mobile", length = 10)
    public String getMobile() {
        return this.mobile;
	}

    public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	
	@Column(name = "name", length = 100)
	public String getName() {
		return this.name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	@Column(name = "blocked", nullable = false)
	public boolean isBlocked() {
		return this.blocked;
	}
	
	public void setBlocked(boolean blocked) {
		this.blocked = blocked;
	}
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created", nullable = false, length = 19)
	public Date getCreated() {
		return this.created;
	}
	
	public void setCreated(Date created) {
		this.created = created;
	}
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated", nullable = false, length = 19, insertable = false, updatable = false)
	public Date getUpdated() {
		return this.updated;
	}
	
	public void setUpdated(Date updated) {
        this.updated = updated;
    }
	
	@Column(name = "last_accessed_from", nullable = false, length = 256)
    public String getLastAccessedFrom() {
        return lastAccessedFrom;
    }
    
    public void setLastAccessedFrom(String lastAccessedFrom) {
        this.lastAccessedFrom = lastAccessedFrom;
    }
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
    public Set<UserDatatableView> getUserDatatableViews() {
        return this.userDatatableViews;
    }
    
    public void setUserDatatableViews(Set<UserDatatableView> userDatatableViews) {
        this.userDatatableViews = userDatatableViews;
    }
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user", cascade = { CascadeType.PERSIST, CascadeType.MERGE })
    @OrderBy(clause = "role.id")
    public Set<UserRole> getUserRoles() {
        return this.userRoles;
    }
    
    public void setUserRoles(Set<UserRole> userRoles) {
        this.userRoles = userRoles;
    }
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
    public Set<Vendor> getVendors() {
        return vendors;
    }
    
    public void setVendors(Set<Vendor> vendors) {
        this.vendors = vendors;
    }
    
    @Column(name = "hidden", nullable = false)
    public boolean isHidden() {
        return hidden;
    }
    
    public void setHidden(boolean hidden) {
        this.hidden = hidden;
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "last_login_time", length = 19)
    public Date getLastLoginTime() {
        return lastLoginTime;
    }
    
    public void setLastLoginTime(Date lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }
    
	@Transient
	public Vendor getVendor() {
		Integer currentFacilityId = UserContext.current().getFacilityId();
		if (currentFacilityId != null) {
			for (Vendor vendor : getVendors()) {
				if (vendor.getFacility().getId().equals(currentFacilityId)) {
					return vendor;
				}
			}
		}
		return null;
	}
	
	@Transient
	public boolean isVendor() {
		return getVendor() != null;
	}
	
	@Transient
	public Set<Facility> getUserFacilities() {
		Set<Facility> facilities = new HashSet<Facility>();
		for (UserRole userRole : getUserRoles()) {
			facilities.add(userRole.getFacility());
		}
		return facilities;
	}

    @Column(name = "mobile_verified", nullable = false)
    public boolean isMobileVerified() {
        return mobileVerified;
    }

    public void setMobileVerified(boolean mobileVerified) {
        this.mobileVerified = mobileVerified;
    }
}
