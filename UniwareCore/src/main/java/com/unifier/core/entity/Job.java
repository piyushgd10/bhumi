package com.unifier.core.entity;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.unifier.core.annotation.Level;
import com.unifier.core.job.MisfireInstruction;

@Document(collection = "recurrentJob") public class Job {

    @Id private String id;

    private String tenantCode;

    private List<String> restrictedProductTypes;

    private List<String> restrictedFacilityTypes;

    private String code;

    private String name;

    private String taskClass;

    private Level executionLevel;

    private Date endTime;

    /**
     * Pipe separated cron expressions for multiple triggers
     */
    private String cronExpression;

    /**
     * http://quartz-scheduler.org/documentation/quartz-2.x/tutorials/tutorial-lesson-04
     */
    private MisfireInstruction misfireInstruction;

    private String lastExecResult;

    private Date lastExecTime;

    private int severity;

    private int notificationLevel;

    private String notificationEmail;

    private String notificationEmailTemplate;

    private boolean enabled;

    private Date created;

    private Date updated;

    private Map<String, String> taskParameters = new HashMap<>(1);

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTenantCode() {
        return tenantCode;
    }

    public void setTenantCode(String tenantCode) {
        this.tenantCode = tenantCode;
    }

    public List<String> getRestrictedProductTypes() {
        return restrictedProductTypes;
    }

    public void setRestrictedProductTypes(List<String> restrictedProductTypes) {
        this.restrictedProductTypes = restrictedProductTypes;
    }

    public List<String> getRestrictedFacilityTypes() {
        return restrictedFacilityTypes;
    }

    public void setRestrictedFacilityTypes(List<String> restrictedFacilityTypes) {
        this.restrictedFacilityTypes = restrictedFacilityTypes;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTaskClass() {
        return taskClass;
    }

    public void setTaskClass(String taskClass) {
        this.taskClass = taskClass;
    }

    public Date getEndTime() {
        return endTime;
    }

    public MisfireInstruction getMisfireInstruction() {
        return misfireInstruction;
    }

    public void setMisfireInstruction(MisfireInstruction misfireInstruction) {
        this.misfireInstruction = misfireInstruction;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getCronExpression() {
        return cronExpression;
    }

    public void setCronExpression(String cronExpression) {
        this.cronExpression = cronExpression;
    }

    public Level getExecutionLevel() {
        return executionLevel;
    }

    public void setExecutionLevel(Level executionLevel) {
        this.executionLevel = executionLevel;
    }

    public String getLastExecResult() {
        return lastExecResult;
    }

    public void setLastExecResult(String lastExecResult) {
        this.lastExecResult = lastExecResult;
    }

    public Date getLastExecTime() {
        return lastExecTime;
    }

    public void setLastExecTime(Date lastExecTime) {
        this.lastExecTime = lastExecTime;
    }

    public int getSeverity() {
        return severity;
    }

    public void setSeverity(int severity) {
        this.severity = severity;
    }

    public int getNotificationLevel() {
        return notificationLevel;
    }

    public void setNotificationLevel(int notificationLevel) {
        this.notificationLevel = notificationLevel;
    }

    public String getNotificationEmail() {
        return notificationEmail;
    }

    public void setNotificationEmail(String notificationEmail) {
        this.notificationEmail = notificationEmail;
    }

    public String getNotificationEmailTemplate() {
        return notificationEmailTemplate;
    }

    public void setNotificationEmailTemplate(String notificationEmailTemplate) {
        this.notificationEmailTemplate = notificationEmailTemplate;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public Map<String, String> getTaskParameters() {
        return taskParameters;
    }

    public void setTaskParameters(Map<String, String> taskParameters) {
        this.taskParameters = taskParameters;
    }
}
