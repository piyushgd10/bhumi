/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *  @version     1.0, Dec 21, 2011
 *  @author singla
 */
package com.unifier.core.entity;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.unifier.core.utils.CollectionUtils;

@Document(collection = "jobResult")
public class JobResult {

    public enum StatusCode {
        SUCCESSFUL,
        FAILED,
    }

    @Id
    private String     id;
    private String     tenantCode;
    private String     jobCode;
    private String facilityCode;
    private String     message;
    private StatusCode statusCode;
    private String     lastExecResult;
    private Date lastExecTime;
    private Map<String, Object> resultItems = new HashMap<>();

    public JobResult() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTenantCode() {
        return tenantCode;
    }

    public void setTenantCode(String tenantCode) {
        this.tenantCode = tenantCode;
    }

    public String getFacilityCode() {
        return facilityCode;
    }

    public void setFacilityCode(String facilityCode) {
        this.facilityCode = facilityCode;
    }

    public String getJobCode() {
        return jobCode;
    }

    public void setJobCode(String jobCode) {
        this.jobCode = jobCode;
    }

    public StatusCode getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(StatusCode statusCode) {
        this.statusCode = statusCode;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public Map<String, Object> getResultItems() {
        return resultItems;
    }

    public void setResultItems(Map<String, Object> resultItems) {
        this.resultItems = resultItems;
    }

    @JsonIgnore
    public List<Map.Entry<String, Object>> getResultItemsList() {
        return CollectionUtils.asList(resultItems.entrySet());
    }

    public void addResultItem(String name, Object resultItem) {
        this.resultItems.put(name, resultItem);
    }

    public String getLastExecResult() {
        return lastExecResult;
    }

    public void setLastExecResult(String lastExecResult) {
        this.lastExecResult = lastExecResult;
    }

    public Date getLastExecTime() {
        return lastExecTime;
    }

    public void setLastExecTime(Date lastExecTime) {
        this.lastExecTime = lastExecTime;
    }

    @Override
    public String toString() {
        StringBuilder response = new StringBuilder();
        response.append("Message: " + message + "<br>");
        if (resultItems.size() > 0) {
            for (Entry<String, Object> e : resultItems.entrySet()) {
                response.append(e.getKey() + ": " + e.getValue() + "<br>");
            }
        }

        return response.toString();
    }
}
