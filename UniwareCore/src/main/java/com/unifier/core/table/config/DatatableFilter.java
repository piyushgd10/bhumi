/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 8, 2013
 *  @author karunsingla
 */
package com.unifier.core.table.config;

import com.unifier.core.export.config.ExportFilter;

/**
 * @author karunsingla
 */
public class DatatableFilter extends ExportFilter {

    private String attachToColumn;

    public String getAttachToColumn() {
        return attachToColumn;
    }

    public void setAttachToColumn(String attachToColumn) {
        this.attachToColumn = attachToColumn;
    }
}
