/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 8, 2013
 *  @author karunsingla
 */
package com.unifier.core.table.config;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.unifier.core.export.config.ExportConfig;

/**
 * @author karunsingla
 */
public class DatatableConfig extends ExportConfig {

    private int    height;
    private String defaultSortBy;

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    @JsonIgnore
    public String getDefaultSortBy() {
        return defaultSortBy;
    }

    public void setDefaultSortBy(String defaultSortBy) {
        this.defaultSortBy = defaultSortBy;
    }

}
