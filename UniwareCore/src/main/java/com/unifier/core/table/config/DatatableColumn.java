/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 8, 2013
 *  @author karunsingla
 */
package com.unifier.core.table.config;

import com.unifier.core.export.config.ExportColumn;

/**
 * @author karunsingla
 */
public class DatatableColumn extends ExportColumn {

    public enum Type {

        DATE("date");

        String textValue;

        Type(String date) {
            this.textValue = date;
        }

        public String getTextValue() {
            return this.textValue;
        }
    }

    private int     width = 150;
    private boolean sortable;
    private String  align = "left";
    private String  type;
    private String  clientTemplate;

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public boolean isSortable() {
        return sortable;
    }

    public void setSortable(boolean sortable) {
        this.sortable = sortable;
    }

    public String getAlign() {
        return align;
    }

    public void setAlign(String align) {
        this.align = align;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getClientTemplate() {
        return clientTemplate;
    }

    public void setClientTemplate(String clientTemplate) {
        this.clientTemplate = clientTemplate;
    }
}
