/*
 * Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 3/14/15 6:01 PM
 * @author amdalal
 */

package com.unifier.core.export.config;

import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.unifier.core.api.datatable.WsSortColumn;
import com.unifier.core.table.config.DatatableColumn;
import com.unifier.core.utils.StringUtils;

public class ExportResultComparator implements Comparator<Map<String, Object>> {

    private List<WsSortColumn> sortColumnList;

    private ExportConfig       exportConfig;

    public ExportResultComparator(ExportConfig exportConfig, List<WsSortColumn> sortColumnList) {
        this.sortColumnList = sortColumnList;
        this.exportConfig = exportConfig;
    }

    @Override
    public int compare(Map<String, Object> o1, Map<String, Object> o2) {
        for (WsSortColumn wsSortColumn : sortColumnList) {
            DatatableColumn datatableColumn = (DatatableColumn) exportConfig.getExportColumnById(wsSortColumn.getColumn());
            if (StringUtils.isNotEmpty(datatableColumn.getType()) && datatableColumn.getType().equalsIgnoreCase(DatatableColumn.Type.DATE.getTextValue())) {
                Date obj1 = (Date) o1.get(wsSortColumn.getColumn());
                Date obj2 = (Date) o2.get(wsSortColumn.getColumn());
                return wsSortColumn.isDescending() ? obj2.compareTo(obj1) : obj1.compareTo(obj2);
            } else {
                Object obj1 = o1.get(wsSortColumn.getColumn());
                Object obj2 = o2.get(wsSortColumn.getColumn());
                return wsSortColumn.isDescending() ? obj2.toString().compareTo(obj1.toString()) : obj1.toString().compareTo(obj2.toString());
            }
        }
        return 0;
    }
}
