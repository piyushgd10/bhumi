/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 19, 2012
 *  @author singla
 */
package com.unifier.core.export.config;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.unifier.core.fileparser.DelimitedFileParser;
import com.unifier.core.utils.StringUtils;

/**
 * @author singla
 */
public class ExportConfig implements Serializable {

    private String                          name;

    private String                          displayName;

    private String                          type;

    private String                          accessResourceName;

    private String                          queryString;

    private List<ExportColumn>              exportColumns     = new ArrayList<ExportColumn>();

    private final List<ExportColumn>        hiddenColumns     = new ArrayList<ExportColumn>();

    private List<ExportFilter>              exportFilters     = new ArrayList<ExportFilter>();

    private List<ExportFilter>              hiddenFilters     = new ArrayList<>();

    private List<ExportFilter>              groupFilters      = new ArrayList<>();

    private ExportFilter                    primaryFilter;

    private final Map<String, ExportFilter> idToExportFilters = new HashMap<String, ExportFilter>();

    private final Map<String, ExportColumn> idToExportColumns = new HashMap<String, ExportColumn>();

    private String                          groupBy;

    private String                          resultCountQuery;

    private boolean                         resultCountNotSupported;

    private boolean                         noSql;

    private char                            delimiter         = DelimitedFileParser.DEFAULT_DELIMITER;

    private String                          viewCollectionName;

    /**
     * @return the queryString
     */
    @JsonIgnore
    public String getQueryString() {
        return queryString;
    }

    /**
     * @param queryString the queryString to set
     */
    public void setQueryString(String queryString) {
        this.queryString = queryString;
    }

    /**
     * @return the exportColumns
     */
    public List<ExportColumn> getExportColumns() {
        return exportColumns;
    }

    /**
     * @param exportColumns the exportColumns to set
     */
    public void setExportColumns(List<ExportColumn> exportColumns) {
        this.exportColumns = exportColumns;
    }

    /**
     * @return the exportFilters
     */
    public List<ExportFilter> getExportFilters() {
        return exportFilters;
    }

    /**
     * @param exportFilters the exportFilters to set
     */
    public void setExportFilters(List<ExportFilter> exportFilters) {
        this.exportFilters = exportFilters;
    }

    public List<ExportFilter> getHiddenFilters() {
        return hiddenFilters;
    }

    public void setHiddenFilters(List<ExportFilter> hiddenFilters) {
        this.hiddenFilters = hiddenFilters;
    }

    public List<ExportFilter> getGroupFilters() {
        return groupFilters;
    }

    public void setGroupFilters(List<ExportFilter> groupFilters) {
        this.groupFilters = groupFilters;
    }

    @JsonIgnore
    public void addExportFilter(ExportFilter exportFilter) {
        idToExportFilters.put(exportFilter.getId(), exportFilter);
        if (exportFilter.isGroupFilter()) {
            groupFilters.add(exportFilter);
        } else if (exportFilter.isHidden()) {
            hiddenFilters.add(exportFilter);
        } else {
            exportFilters.add(exportFilter);
        }
    }

    public ExportFilter getPrimaryFilter() {
        return primaryFilter;
    }

    public void setPrimaryFilter(ExportFilter primaryFilter) {
        this.primaryFilter = primaryFilter;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the displayName
     */
    public String getDisplayName() {
        return displayName;
    }

    /**
     * @param displayName the displayName to set
     */
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    /**
     * @param exportColumn
     */
    public void addExportColumn(ExportColumn exportColumn) {
        if (exportColumn.isHidden()) {
            hiddenColumns.add(exportColumn);
        } else {
            exportColumns.add(exportColumn);
        }
        idToExportColumns.put(exportColumn.getId(), exportColumn);
    }

    @JsonIgnore
    public ExportColumn getExportColumnById(String exportColumnId) {
        return idToExportColumns.get(exportColumnId);
    }

    /**
     * @param exportFilterId
     * @return
     */
    public ExportFilter getExportFilterById(String exportFilterId) {
        return idToExportFilters.get(exportFilterId);
    }

    /**
     * @return the groupBy
     */
    public String getGroupBy() {
        return groupBy;
    }

    /**
     * @param groupBy the groupBy to set
     */
    public void setGroupBy(String groupBy) {
        this.groupBy = groupBy;
    }

    /**
     * @return the accessResourceName
     */
    public String getAccessResourceName() {
        return accessResourceName;
    }

    /**
     * @param accessResourceName the accessResourceName to set
     */
    public void setAccessResourceName(String accessResourceName) {
        this.accessResourceName = accessResourceName;
    }

    /**
     * @return the hiddenColumns
     */
    public List<ExportColumn> getHiddenColumns() {
        return hiddenColumns;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @JsonIgnore
    public String getResultCountQuery() {
        return resultCountQuery;
    }

    public void setResultCountQuery(String resultCountQuery) {
        this.resultCountQuery = resultCountQuery;
    }

    public char getDelimiter() {
        return delimiter;
    }

    public void setDelimiter(char delimiter) {
        this.delimiter = delimiter;
    }

    public boolean isResultCountNotSupported() {
        return resultCountNotSupported;
    }

    public void setResultCountNotSupported(boolean resultCountNotSupported) {
        this.resultCountNotSupported = resultCountNotSupported;
    }

    public boolean isNoSql() {
        return noSql;
    }

    public void setNoSql(boolean noSql) {
        this.noSql = noSql;
    }

    public String getViewCollectionName() {
        return viewCollectionName;
    }

    public void setViewCollectionName(String viewCollectionName) {
        this.viewCollectionName = viewCollectionName;
    }

    public boolean isServeFromView() {
        return StringUtils.isNotBlank(getViewCollectionName());
    }
}
