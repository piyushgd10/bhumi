/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 19, 2012
 *  @author singla
 */
package com.unifier.core.export.config;

import java.io.Serializable;
import java.util.Map;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.unifier.core.expressions.Expression;

/**
 * @author singla
 */
public class ExportColumn {
    private String              id;
    private String              name;
    private String              value;
    private boolean             calculated;
    private Expression          calculationExpression;
    private boolean             hidden;
    private Map<String, Object> uiConfigParams;
    private boolean             nonSerializableCustomField;
    private String              tableAlias;
    private String              entity;
    private boolean             editable;
    private boolean             exportable = true;
    private String              fieldName;

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the value
     */
    @JsonIgnore
    public String getValue() {
        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * @return the calculated
     */
    public boolean isCalculated() {
        return calculated;
    }

    /**
     * @param calculated the calculated to set
     */
    public void setCalculated(boolean calculated) {
        this.calculated = calculated;
    }

    /**
     * @return the calculationExpression
     */
    public Expression getCalculationExpression() {
        return calculationExpression;
    }

    /**
     * @param calculationExpression the calculationExpression to set
     */
    public void setCalculationExpression(Expression calculationExpression) {
        this.calculationExpression = calculationExpression;
    }

    /**
     * @return the hidden
     */
    public boolean isHidden() {
        return hidden;
    }

    /**
     * @param hidden the hidden to set
     */
    public void setHidden(boolean hidden) {
        this.hidden = hidden;
    }

    /**
     * @return the nonSerializableCustomField
     */
    public boolean isNonSerializableCustomField() {
        return nonSerializableCustomField;
    }

    /**
     * @param nonSerializableCustomField the nonSerializableCustomField to set
     */
    public void setNonSerializableCustomField(boolean nonSerializableCustomField) {
        this.nonSerializableCustomField = nonSerializableCustomField;
    }

    /**
     * @return the tableAlias
     */
    public String getTableAlias() {
        return tableAlias;
    }

    /**
     * @param tableAlias the tableAlias to set
     */
    public void setTableAlias(String tableAlias) {
        this.tableAlias = tableAlias;
    }

    /**
     * @return the entity
     */
    public String getEntity() {
        return entity;
    }

    /**
     * @param entity the entity to set
     */
    public void setEntity(String entity) {
        this.entity = entity;
    }

    public boolean isEditable() {
        return editable;
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
    }

    public boolean isExportable() {
        return exportable;
    }

    public void setExportable(boolean exportable) {
        this.exportable = exportable;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public Map<String, Object> getUiConfigParams() {
        return uiConfigParams;
    }

    public void setUiConfigParams(Map<String, Object> uiConfigParams) {
        this.uiConfigParams = uiConfigParams;
    }
}
