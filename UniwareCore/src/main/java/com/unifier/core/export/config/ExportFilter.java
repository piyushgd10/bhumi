/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 19, 2012
 *  @author singla
 */
package com.unifier.core.export.config;

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.unifier.core.expressions.Expression;
import com.unifier.core.ui.SelectItem;

/**
 * @author singla
 */
public class ExportFilter {

    public enum ValueType {
        SELECT,
        MULTISELECT,
        TEXT,
        DATETIME,
        DATERANGE,
        BOOLEAN
    }

    private String           id;

    private String           name;

    private String           condition;

    private String           conditionForView;

    private ValueType        type;

    private Expression       valueExpression;

    private List<SelectItem> selectItems;

    private Set<String>      selectValues;

    private boolean          primary;

    private boolean          required;

    private boolean          defaultDateRangeRestrictionExempted;

    private boolean          groupFilter;

    private boolean          nonSerializableCustomField;

    private boolean          hidden;

    private String           hiddenValue;

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the condition
     */
    @JsonIgnore
    public String getCondition() {
        return condition;
    }

    /**
     * @param condition the condition to set
     */
    public void setCondition(String condition) {
        this.condition = condition;
    }

    public String getConditionForView() {
        return conditionForView;
    }

    public void setConditionForView(String conditionForView) {
        this.conditionForView = conditionForView;
    }

    /**
     * @return the type
     */
    public ValueType getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(ValueType type) {
        this.type = type;
    }

    /**
     * @return the valueExpression
     */
    @JsonIgnore
    public Expression getValueExpression() {
        return valueExpression;
    }

    /**
     * @param valueExpression the valueExpression to set
     */
    public void setValueExpression(Expression valueExpression) {
        this.valueExpression = valueExpression;
    }

    /**
     * @return the selectItems
     */
    public List<SelectItem> getSelectItems() {
        return selectItems;
    }

    /**
     * @param selectItems the selectItems to set
     */
    public void setSelectItems(List<SelectItem> selectItems) {
        this.selectItems = selectItems;
        this.selectValues = new HashSet<String>();
        for (SelectItem item : selectItems) {
            this.selectValues.add(item.getValue());
        }
    }

    /**
     * @return the selectValues
     */
    @JsonIgnore
    public Set<String> getSelectValues() {
        return selectValues;
    }

    /**
     * @param selectValues the selectValues to set
     */
    public void setSelectValues(Set<String> selectValues) {
        this.selectValues = selectValues;
    }

    public boolean isRequired() {
        return required;
    }

    /**
     * Is this a primary filter. Used while processing dirty sale orders. This filter has the capability of searching
     * sale order by code.
     * 
     * @return
     */
    public boolean isPrimary() {
        return primary;
    }

    public void setPrimary(boolean primary) {
        this.primary = primary;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public boolean isDefaultDateRangeRestrictionExempted() {
        return defaultDateRangeRestrictionExempted;
    }

    public void setDefaultDateRangeRestrictionExempted(boolean defaultDateRangeRestrictionExempted) {
        this.defaultDateRangeRestrictionExempted = defaultDateRangeRestrictionExempted;
    }

    public boolean isGroupFilter() {
        return groupFilter;
    }

    public void setGroupFilter(boolean groupFilter) {
        this.groupFilter = groupFilter;
    }

    public boolean isNonSerializableCustomField() {
        return nonSerializableCustomField;
    }

    public void setNonSerializableCustomField(boolean nonSerializableCustomField) {
        this.nonSerializableCustomField = nonSerializableCustomField;
    }

    public boolean isHidden() {
        return hidden;
    }

    public void setHidden(boolean hidden) {
        this.hidden = hidden;
    }

    public String getHiddenValue() {
        return hiddenValue;
    }

    public void setHiddenValue(String hiddenValue) {
        this.hiddenValue = hiddenValue;
    }
}
