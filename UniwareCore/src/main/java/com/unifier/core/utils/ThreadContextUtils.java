package com.unifier.core.utils;

import org.apache.logging.log4j.ThreadContext;

import com.uniware.core.utils.Constants;
import com.uniware.core.utils.UserContext;

public abstract class ThreadContextUtils {

    public static void setThreadMetadata(String threadNamePrefix, UserContext userContext) {
        StringBuilder threadNameBuilder = new StringBuilder(threadNamePrefix).append(Thread.currentThread().getId()).append(':');
        if (userContext.getTenant() != null) {
            threadNameBuilder.append(userContext.getTenant().getCode());
        }
        Thread.currentThread().setName(threadNameBuilder.toString());
        // let the logs go to tenantCode.log
        if (userContext.isTraceLoggingEnabled()) {
            ThreadContext.put(Constants.TRACE_LOG_KEY_VALUE, userContext.getLogRoutingKey());
            ThreadContext.put(Constants.TRACE_LOG_KEY, Constants.TRACE_LOG_KEY);
        } else {
            userContext.setLogRoutingKey(userContext.getTenant().getCode());
            ThreadContext.put(Constants.TRACE_LOG_KEY, userContext.getTenant().getCode());
        }
    }
}
