-- Ankur | added signature to manifest.
alter table shipping_manifest add column signature_link varchar(255) default null after closed;
alter table shipping_manifest add column signee_name varchar(100) default null after signature_link;
alter table shipping_manifest add column signee_mobile varchar(50) default null after signee_name;

-- Sagar | GST Changes.
ALTER TABLE shipping_package
  ADD COLUMN `return_invoice_id` int(10) unsigned DEFAULT NULL,
  ADD CONSTRAINT `fk_shipping_package_return_invoice_id` FOREIGN KEY (`return_invoice_id`) REFERENCES `invoice` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE sale_order_item
  ADD COLUMN `return_invoice_item_id` int(10) unsigned DEFAULT NULL;
ALTER TABLE reverse_pickup
  ADD COLUMN `return_invoice_id` int(10) unsigned DEFAULT NULL,
  ADD CONSTRAINT `fk_reverse_pickup_return_invoice_id` FOREIGN KEY (`return_invoice_id`) REFERENCES `invoice` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE state add column `type` enum('STATE','UNION_TERRITORY') DEFAULT 'STATE';
ALTER TABLE tax_type_configuration_range
  ADD COLUMN central_gst DECIMAL(6, 3) NOT NULL,
  ADD COLUMN state_gst DECIMAL(6, 3) NOT NULL,
  ADD COLUMN union_territory_gst DECIMAL(6, 3) NOT NULL,
  ADD COLUMN integrated_gst DECIMAL(6, 3) NOT NULL,
  ADD COLUMN compensation_cess DECIMAL(6, 3) NOT NULL;
ALTER TABLE item_type ADD COLUMN gst_tax_type_id int(10) DEFAULT NULL;
ALTER TABLE category ADD COLUMN gst_tax_type_id int(10) DEFAULT NULL;
ALTER TABLE tax_type ADD COLUMN hsn_code VARCHAR(45) DEFAULT NULL;
ALTER TABLE tax_type_configuration MODIFY state_code VARCHAR(45) DEFAULT NULL;
INSERT IGNORE into environment_property values(null, "gst.date", "2017-07-01");
ALTER TABLE invoice
  ADD COLUMN type enum('SALE', 'PURCHASE', 'STOCK_TRANSFER', 'SALE_RETURN', 'PURCHASE_RETURN', 'STOCK_TRANSFER_RETURN' ) DEFAULT 'SALE';
update import_job_type set import_job_config = '<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<import-config>\n    <columns>\n        <column name=\"Category Code\" source=\"Category Code\" description=\"category code\" required=\"true\" />\n        <column name=\"Product Code\" source=\"Product Code\" description=\"product code\" required=\"true\" groupBy=\"true\" requiredInUpdate=\"true\" />\n        <column name=\"Name\" source=\"Name\" description=\"item type name\" required=\"true\" />\n        <column name=\"Description\" source=\"Description\" description=\"description\" />\n        <column name=\"Scan Identifier\" source=\"Scan Identifier\" description=\"scan identifier\" />\n        <column name=\"Length (mm)\" source=\"Length (mm)\" description=\"length in millimeters\" />\n        <column name=\"Width (mm)\" source=\"Width (mm)\" description=\"width in millimeters\" />\n        <column name=\"height (mm)\" source=\"height (mm)\" description=\"height in millimeters\" />\n        <column name=\"Weight (gms)\" source=\"Weight (gms)\" description=\"weight in grams\" />\n        <column name=\"ean\" source=\"ean\" description=\"International Article Number or European Article Number\" />\n        <column name=\"upc\" source=\"upc\" description=\"Universal Product Code\" />\n        <column name=\"isbn\" source=\"isbn\" description=\"International Standard Book Number\" />\n        <column name=\"color\" source=\"color\" description=\"color\" />\n        <column name=\"brand\" source=\"brand\" description=\"brand\" />\n        <column name=\"size\" source=\"size\" description=\"size\" />\n        <column name=\"Requires Customization\" source=\"Requires Customization\" description=\"SKU requires customization\" />\n        <column name=\"Min Order Size\" source=\"Min Order Size\" description=\"minimum order size\" />\n        <column name=\"Tax Type Code\" source=\"Tax Type Code\" description=\"tax type code\" />\n        <column name=\"GST Tax Type Code\" source=\"GST Tax Type Code\" description=\"gst tax type code\" />\n        <column name=\"HSN Code\" source=\"HSN Code\" description=\"hsn code\"  />\n        <column name=\"Tags\" source=\"Tags\" description=\"tags\" />\n        <column name=\"TAT\" source=\"TAT\" description=\"TAT\" />\n        <column name=\"Image Url\" source=\"Image Url\" description=\"image url\" />\n        <column name=\"Product Page URL\" source=\"Product Page URL\" description=\"Product Page URL\" />\n        <column name=\"Item Detail Fields\" source=\"Item Detail Fields\" description=\"Item Detail Fields (comma seperated)\" />\n        <column name=\"Cost Price\" source=\"Cost Price\" description=\"Cost Price\" />\n        <column name=\"MRP\" source=\"MRP\" description=\"Max Retail Price\" />\n        <column name=\"Base Price\" source=\"Base Price\" description=\"Selling Price\" />\n        <column name=\"Enabled\" source=\"Enabled\" description=\"Enabled\" />\n        <custom-fields entity=\"com.uniware.core.entity.ItemType\" />\n        <column name=\"resync inventory\" source=\"Resync Inventory\" description=\"1 if inventory of this item type needs to resynced on channels\" />\n        <column name=\"Type\" source=\"Type\" description=\"SIMPLE or BUNDLE, by default SIMPLE\" />\n        <column name=\"Component Product Code\" source=\"Component Product Code\" description=\"product code of the bundle component\" />\n        <column name=\"Component Quantity\" source=\"Component Quantity\" description=\"quantity of the component in bundle\" />\n        <column name=\"Component Price\" source=\"Component Price\" description=\"price of the component in bundle\" />\n    </columns>\n</import-config>\n' where name = "Item Master";
update import_job_type set import_job_config = '<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<import-config>\n    <columns>\n        <column name=\"Category Code\" source=\"Category Code\" description=\"category code\" required=\"true\" />\n        <column name=\"Category Name\" source=\"Category Name\" description=\"category name\" required=\"true\" />\n        <column name=\"Tax Type Code\" source=\"Tax Type Code\" description=\"tax type code\" required=\"true\" />\n        <column name=\"HSN Code\" source=\"HSN Code\" description=\"hsn code\"  />\n        <column name=\"GST Tax Type Code\" source=\"GST Tax Type Code\" description=\"gst tax type code\" required=\"true\" />\n        <column name=\"Item Detail Fields\" source=\"Item Detail Fields\" description=\"Item Detail Fields (comma seperated)\" />\n    </columns>\n</import-config>\n' where name = "Category";

insert into environment_property values (null,'app.major.version','1');
insert into environment_property values (null,'app.minor.version','8');

ALTER TABLE tax_type ADD COLUMN gst tinyint(1) DEFAULT 0;

-- Piyush | datatable invoice sequence
insert ignore into export_job_type select null, t.id, 'DATATABLE INVOICE SEQUENCE', 'DATATABLE INVOICE SEQUENCE', null, '', 1, 0, 'MINIMAL', null, now(), now(), 'TABLE' from tenant t;
update export_job_type set export_job_config = '<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<table-config name=\"DATATABLE INVOICE SEQUENCE\" height=\"1400\">\n    <queryString>\n        <![CDATA[from sequence s where s.name in (\'SALE_INVOICE\',\'PURCHASE_INVOICE\',\'STOCK_TRANSFER_INVOICE\',\'SALE_RETURN_INVOICE\',\'PURCHASE_RETURN_INVOICE\',\'STOCK_TRANSFER_RETURN_INVOICE\',\'DELIVERY_CHALLAN\') and s.facility_id = :facilityId order by s.id]]>\n    </queryString>\n    <columns>\n        <column id=\"name\" name=\"Sequence Name\" value=\"s.name\" width=\"100\" sortable=\"true\" align=\"left\" />\n        <column id=\"prefix\" name=\"Prefix\" value=\"s.prefix\" width=\"100\" sortable=\"true\" align=\"left\" />\n        <column id=\"currentValue\" name=\"Current Value\" value=\"s.current_value\" width=\"100\" sortable=\"true\" align=\"left\" />\n    </columns>\n</table-config>\n' where name = "DATATABLE INVOICE SEQUENCE";
insert ignore into user_datatable_view select null, concat(t.id,'-DATATABLE INVOICE SEQUENCE-ALL'), t.id, null, 'DATATABLE INVOICE SEQUENCE','All','ALL','{"resultsPerPage":25,"columns":[],"filters":[],"sortColumns":[{"column":"updated","descending":true}]}',95,1,now(),now(),'MINIMAL' from tenant t;
-- done

-- Piyush | sequence queries
insert ignore into sequence select null, concat(t.id,'-',f.id,'-SALE_INVOICE'), t.id, f.id, 'SALE_INVOICE','FACILITY',0,1,'INVS',null,null,1,5,'NEVER', now(), now() from tenant t, facility f, party p where f.id = p.id and p.tenant_id = t.id;
insert ignore into sequence select null, concat(t.id,'-',f.id,'-SALE_RETURN_INVOICE'), t.id, f.id, 'SALE_RETURN_INVOICE','FACILITY',0,1,'INVSR',null,null,1,5,'NEVER', now(), now() from tenant t, facility f, party p where f.id = p.id and p.tenant_id = t.id;
insert ignore into sequence select null, concat(t.id,'-',f.id,'-DELIVERY_CHALLAN'), t.id, f.id, 'DELIVERY_CHALLAN','FACILITY',0,1,'DCH',null,null,1,5,'NEVER', now(), now() from tenant t, facility f, party p where f.id = p.id and p.tenant_id = t.id;
insert ignore into sequence select null, concat(t.id,'-',f.id,'-STOCK_TRANSFER_INVOICE'), t.id, f.id, 'STOCK_TRANSFER_INVOICE','FACILITY',0,1,'INVT',null,null,1,5,'NEVER', now(), now() from tenant t, facility f, party p where f.id = p.id and p.tenant_id = t.id;
insert ignore into sequence select null, concat(t.id,'-',f.id,'-STOCK_TRANSFER_RETURN_INVOICE'), t.id, f.id, 'STOCK_TRANSFER_RETURN_INVOICE','FACILITY',0,1,'INVTR',null,null,1,5,'NEVER', now(), now() from tenant t, facility f, party p where f.id = p.id and p.tenant_id = t.id;
insert ignore into sequence select null, concat(t.id,'-',f.id,'-PURCHASE_INVOICE'), t.id, f.id, 'PURCHASE_INVOICE','FACILITY',0,1,'INVP',null,null,1,5,'NEVER', now(), now() from tenant t, facility f, party p where f.id = p.id and p.tenant_id = t.id;
insert ignore into sequence select null, concat(t.id,'-',f.id,'-PURCHASE_RETURN_INVOICE'), t.id, f.id, 'PURCHASE_RETURN_INVOICE','FACILITY',0,1,'INVPR',null,null,1,5,'NEVER', now(), now() from tenant t, facility f, party p where f.id = p.id and p.tenant_id = t.id;
-- done

-- Piyush | columns in gatepass
ALTER TABLE outbound_gate_pass ADD COLUMN `invoice_id` int(10) unsigned DEFAULT NULL, ADD COLUMN `return_invoice_id` int(10) unsigned DEFAULT NULL, ADD CONSTRAINT `fk_outbound_gate_pass_invoice_id` FOREIGN KEY (`invoice_id`) REFERENCES `invoice` (`id`) ON DELETE CASCADE ON UPDATE CASCADE, ADD CONSTRAINT `fk_outbound_gate_pass_return_invoice_id` FOREIGN KEY (`return_invoice_id`) REFERENCES `invoice` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
insert ignore into access_pattern select null, ar.id, '/data/admin/system/facility/sequence/edit', now(), now() from access_resource ar where ar.name = 'ADMIN_WAREHOUSE';
-- done

-- Piyush | datatable gatepass invoice
insert ignore into export_job_type select null, t.id, 'DATATABLE GATEPASS INVOICE', 'DATATABLE GATEPASS INVOICE', null, '', 1, 0, 'MINIMAL', null, now(), now(), 'TABLE' from tenant t;
insert ignore into user_datatable_view select null, concat(t.id,'-DATATABLE GATEPASS INVOICE-ALL'), t.id, null, 'DATATABLE GATEPASS INVOICE','All','ALL','{"resultsPerPage":25,"columns":[],"filters":[],"sortColumns":[{"column":"updated","descending":true}]}',95,1,now(),now(),'MINIMAL' from tenant t;

alter table outbound_gate_pass_item add column `invoice_item_id` int(10) unsigned DEFAULT NULL;
alter table outbound_gate_pass_item add column `return_invoice_item_id` int(10) unsigned DEFAULT NULL;

ALTER TABLE purchase_order_item
  ADD COLUMN central_gst DECIMAL(6, 3) NOT NULL,
  ADD COLUMN state_gst DECIMAL(6, 3) NOT NULL,
  ADD COLUMN union_territory_gst DECIMAL(6, 3) NOT NULL,
  ADD COLUMN integrated_gst DECIMAL(6, 3) NOT NULL,
  ADD COLUMN compensation_cess DECIMAL(6, 3) NOT NULL;

-- Sagar | Default Gst Tax Type
insert into tax_type select null, t.id, "DEFAULT_GST", "DEFAULT_GST", now(), now(), null, 1 from tenant t;
insert into tax_type_configuration select null, tt.id, null, now(), now() from tax_type tt where tt.code = "DEFAULT_GST";
insert into tax_type_configuration_range select null, ttc.id, 0,0,0,0,0,999999999.99,now(),now(),0,0,0,0,0 from tax_type_configuration ttc, tax_type tt where ttc.tax_type_id = tt.id and tt.code = "DEFAULT_GST";

alter table party add column `gst_number` varchar(45) DEFAULT NULL;

-- Amit | June 20, 2017 | GstStateCode
alter table state add column `gst_code` VARCHAR(10) DEFAULT null, add UNIQUE KEY `unique_gst_code` (gst_code) ;
update state set gst_code = "35" where iso2_code = "AN" and country_code = "IN";
update state set gst_code = "28" where iso2_code = "AP" and country_code = "IN";
update state set gst_code = "37" where iso2_code = "AD" and country_code = "IN";
update state set gst_code = "12" where iso2_code = "AR" and country_code = "IN";
update state set gst_code = "18" where iso2_code = "AS" and country_code = "IN";
update state set gst_code = "10" where iso2_code = "BR" and country_code = "IN";
update state set gst_code = "04" where iso2_code = "CH" and country_code = "IN";
update state set gst_code = "22" where iso2_code = "CT" and country_code = "IN";
update state set gst_code = "26" where iso2_code = "DN" and country_code = "IN";
update state set gst_code = "25" where iso2_code = "DD" and country_code = "IN";
update state set gst_code = "07" where iso2_code = "DL" and country_code = "IN";
update state set gst_code = "30" where iso2_code = "GA" and country_code = "IN";
update state set gst_code = "24" where iso2_code = "GJ" and country_code = "IN";
update state set gst_code = "06" where iso2_code = "HR" and country_code = "IN";
update state set gst_code = "02" where iso2_code = "HP" and country_code = "IN";
update state set gst_code = "01" where iso2_code = "JK" and country_code = "IN";
update state set gst_code = "20" where iso2_code = "JH" and country_code = "IN";
update state set gst_code = "29" where iso2_code = "KA" and country_code = "IN";
update state set gst_code = "32" where iso2_code = "KL" and country_code = "IN";
update state set gst_code = "31" where iso2_code = "LD" and country_code = "IN";
update state set gst_code = "23" where iso2_code = "MP" and country_code = "IN";
update state set gst_code = "27" where iso2_code = "MH" and country_code = "IN";
update state set gst_code = "14" where iso2_code = "MN" and country_code = "IN";
update state set gst_code = "17" where iso2_code = "ML" and country_code = "IN";
update state set gst_code = "15" where iso2_code = "MZ" and country_code = "IN";
update state set gst_code = "13" where iso2_code = "NL" and country_code = "IN";
update state set gst_code = "21" where iso2_code = "OR" and country_code = "IN";
update state set gst_code = "34" where iso2_code = "PY" and country_code = "IN";
update state set gst_code = "03" where iso2_code = "PB" and country_code = "IN";
update state set gst_code = "08" where iso2_code = "RJ" and country_code = "IN";
update state set gst_code = "11" where iso2_code = "SK" and country_code = "IN";
update state set gst_code = "33" where iso2_code = "TN" and country_code = "IN";
update state set gst_code = "36" where iso2_code = "TL" and country_code = "IN";
update state set gst_code = "16" where iso2_code = "TR" and country_code = "IN";
update state set gst_code = "09" where iso2_code = "UP" and country_code = "IN";
update state set gst_code = "05" where iso2_code = "UT" and country_code = "IN";
update state set gst_code = "19" where iso2_code = "WB" and country_code = "IN";
-- done
update state set type = 'UNION_TERRITORY' where code in ('IN-AN' , 'IN-LD', 'IN-DD', 'IN-DN', 'IN-CH');
insert ignore into system_configuration select null, concat(t.id,'-gatepass.invoicing.required'), null, t.id, 'gatepass.invoicing.required', 'Gatepass Invoicing Required', '0', 'checkbox','MINIMAL','SYSTEM', 10, now(), now() from tenant t;

insert ignore into access_pattern select null, ar.id, '/material/dashboard/gatepass', now(), now() from access_resource ar where ar.name = 'OUTBOUND_GATEPASS';
insert ignore into access_pattern select null, ar.id, '/material/dashboard/gatepassInvoice', now(), now() from access_resource ar where ar.name = 'OUTBOUND_GATEPASS';

-- Amit | June 20, 2017 | Signature image in party
ALTER TABLE party ADD COLUMN `signature_binary_object_id` INT(10) UNSIGNED DEFAULT NULL, ADD CONSTRAINT `fk_party_signature_binary_object_id1` FOREIGN KEY (`signature_binary_object_id`) REFERENCES `binary_object` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
-- done
