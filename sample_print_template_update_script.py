#!/usr/bin/python

import pymongo
from pymongo import MongoClient
import sys
import subprocess
import os
import time
import MySQLdb
import datetime
import json
import decimal
import httplib
import urllib2
import urllib
import base64

future_to_server = []
servers = []
mongo_connection = MongoClient("common2.mongo.unicommerce.infra", 27017)
db1 = mongo_connection["uniware"]
cursor = db1.tenantProfile.find({"tenantType": "LITE"})
for tenant in cursor:
    serverName = tenant[u'serverName']
    tenantCode = tenant[u'tenantCode']
    db2 = mongo_connection["uniwareConfig"]
    cursor2 = db2.serverDetails.find({'name':serverName})
    serverDetail = cursor2.next()
    mongo_servers = serverDetail[u'tenantSpecificMongoHosts']
    for server in mongo_servers:
        linuxCMD = "/usr/bin/mongo " + server + " --eval 'printjson(rs.isMaster())' | grep 'primary' | cut -d'\"' -f4"
        mongo_server = os.popen(linuxCMD).read().split(":")[0]
        if mongo_server:
            break
    print tenantCode, mongo_server
    mongo_connection2 = MongoClient(mongo_server, 27017)
    db3 = mongo_connection2[tenantCode]
    db3.eval("function (tenantCode) {db.printTemplate.find({'samplePrintTemplateCode' : 'INVOICE_CUSTOM_TEMPLATE', 'tenantCode' : tenantCode}).forEach(function(doc){db.printTemplate.update({_id:doc._id},{$set:{'samplePrintTemplateCode' : 'INVOICE_TYPE_D'}},{upsert:false , multi:true})}); db.samplePrintTemplate.update({'code': 'INVOICE_CUSTOM_TEMPLATE', 'tenantCode': tenantCode},{$set:{'enabled': false}})}", tenantCode)
print 'Done!'