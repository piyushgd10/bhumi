#!/bin/bash
if [ ${#} -lt "1" ]; then
   echo -e "\E[31mUsage: `basename $0`  [SQL FILE]  [SERVER NAME...]  PAGE...\n"
   echo -e " \E[37mMain modes of operation::\E[33m"
   echo -e "   `basename $0`        Provide name of the SQL file you want to execute, 
                    server(s) provided as comma separated name(s),                                                                                                 e.g. '`basename $0` x.sql Demo' => '`basename $0` x.sql Demo,Xerion,Tyrian'\n\E[37m"
   exit 1
fi
{ . "/home/ec2-user/scripts/validateServerNames" $1; echo $? > /tmp/false.status ; }
FSTATUS=`cat /tmp/false.status`
if [ "$FSTATUS" -ne "0" ]; then
  exit 1
fi

function executeMongo() {
    SERVER_NAME=$1
    SERVER_IP_VAR="${SERVER_NAME}_IP"
    SERVER_MYSQL_VAR="${SERVER_NAME}_MYSQL"
    MONGO_HOST="mongodb.ucdn.in"
    MYSQL_HOST="${!SERVER_MYSQL_VAR}"
    if [ "$MYSQL_HOST" == "" ]; then
        MYSQL_HOST=${!SERVER_IP_VAR}
    fi
    
    # Importing Scripts
    sudo rm -rf /tmp/scriptData.csv
    sudo rm -rf /tmp/scriptDataMongo.csv
    ssh -t -i ~/.ssh/Build.pem build@$MYSQL_HOST "sudo rm -rf /tmp/scriptData.csv"
    DUMP_SCRIPTS="select 'com.uniware.core.vo.ScriptVO', t.code, sc.name, hex(sc.script), sc.http_proxy_server, sc.created, sc.updated, 1 from script_config sc, tenant t where sc.tenant_id = t.id INTO OUTFILE '/tmp/scriptData.csv'  FIELDS TERMINATED BY ',' ENCLOSED BY '\\\"' LINES TERMINATED BY '\n'"
    ssh -t -i ~/.ssh/Build.pem build@$MYSQL_HOST "mysql -uroot -puniware uniware -e \"$DUMP_SCRIPTS\""
    scp -i ~/.ssh/Build.pem build@$MYSQL_HOST:/tmp/scriptData.csv /tmp/scriptData.csv
    
    echo "_class,tenantCode,name,script,httpProxyServer,created,updated,process" > /tmp/scriptDataMongo.csv
    cat /tmp/scriptData.csv >> /tmp/scriptDataMongo.csv
    
    echo "[ScriptConfig] Mongo import file created at /tmp/scriptDataMongo.csv"
    mongoimport --host $MONGO_HOST -d uniware -c script --type csv --file /tmp/scriptDataMongo.csv --headerline --upsert --upsertFields "tenantCode,name"
    echo "[ScriptConfig] /tmp/scriptDataMongo.csv imported to mongo"

    echo "[ScriptConfig] Creating indexes "
    mongo --host $MONGO_HOST uniware -eval 'function hex2a(hex) {var str = "";for (var i = 0; i < hex.length; i += 2)str += String.fromCharCode(parseInt(hex.substr(i, 2), 16));return str;}; db.script.find({"process": 1}).forEach(function(doc){doc.script=hex2a(doc.script); doc.created = new ISODate(doc.created); doc.updated = new ISODate(doc.updated); if (doc.httpProxyServer == "\\N") {doc.httpProxyServer = null;}; db.script.save(doc)});';
    mongo --host $MONGO_HOST uniware --eval "db.script.createIndex({'tenantCode' :  1, 'name' : 1}, {unique : true}); db.script.createIndex({'tenantCode' :  1);"
    mongo --host $MONGO_HOST uniware --eval "db.script.update({}, {\$unset: {process :''}}, {multi:true});"
    echo "[ScriptConfig] Done!"

}

. /home/ec2-user/servers.conf
if [ "$1" = "All" ]; then
  for line in $(cat servers.conf|grep _IP)
  do
    SERVER_NAME=`echo $line|awk -F "=" '{print $1}'|awk -F "_" '{print $1}'`
    executeMongo $SERVER_NAME
  done
else
  export IFS=','
  for SERVER_NAME in $1
  do
    executeMongo $SERVER_NAME
  done
fi