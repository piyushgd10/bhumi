/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, May 11, 2012
 *  @author singla
 */
package com.unifier.web.taglibs.security;

import com.unifier.core.cache.CacheManager;
import com.unifier.core.utils.StringUtils;
import com.unifier.web.security.UniwareUser;
import com.unifier.web.utils.WebContextUtils;
import com.uniware.core.cache.RolesCache;

import java.util.Set;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;


/**
 * @author singla
 */
public class AuthorizeTag extends TagSupport {

    private String            accessGroup;
    private String            accessResource;

    /**
     * 
     */
    private static final long serialVersionUID = 3562664184142879278L;

    /* (non-Javadoc)
     * @see javax.servlet.jsp.tagext.TagSupport#doStartTag()
     */
    @Override
    public int doStartTag() throws JspException {
        UniwareUser currentUser = WebContextUtils.getCurrentUser();
        if (currentUser == null) {
            return SKIP_BODY;
        }
        Set<String> accessibleByRoles = null;
        RolesCache cache = CacheManager.getInstance().getCache(RolesCache.class);
        if (StringUtils.isNotBlank(accessResource)) {
            accessibleByRoles = cache.getRolesByAccessResource(accessResource);
        } else if (StringUtils.isNotBlank(accessGroup)) {
            accessibleByRoles = cache.getRolesByAccessResourceGroup(accessGroup);
        }
        if (accessibleByRoles != null) {
            return currentUser.hasAnyRole(accessibleByRoles) ? EVAL_BODY_INCLUDE : SKIP_BODY;
        } else {
            return SKIP_BODY;
        }
    }

    /**
     * @return the accessGroup
     */
    public String getAccessGroup() {
        return accessGroup;
    }

    /**
     * @param accessGroup the accessGroup to set
     */
    public void setAccessGroup(String accessGroup) {
        this.accessGroup = accessGroup;
    }

    /**
     * @return the accessResource
     */
    public String getAccessResource() {
        return accessResource;
    }

    /**
     * @param accessResource the accessResource to set
     */
    public void setAccessResource(String accessResource) {
        this.accessResource = accessResource;
    }

}
