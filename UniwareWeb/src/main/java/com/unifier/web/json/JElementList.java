/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 03-Jan-2012
 *  @author vibhu
 */
package com.unifier.web.json;

import java.util.ArrayList;
import java.util.List;

public class JElementList<E> {

    private List<E> elements;

    public JElementList() {
        this.elements = new ArrayList<E>();
    }

    public JElementList(List<E> elements) {
        this.elements = elements;
    }

    public void addElement(E element) {
        elements.add(element);
    }

    public void addAllElements(List<? extends E> paramCollection) {
        elements.addAll(paramCollection);
    }

    public List<E> getElements() {
        return elements;
    }

    public void setElements(List<E> elements) {
        this.elements = elements;
    }
}
