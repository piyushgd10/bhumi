package com.unifier.web.resteasy;

import com.unifier.core.api.base.ServiceResponse;
import com.unifier.core.api.validation.WsError;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.jboss.resteasy.spi.ReaderException;

import com.uniware.core.api.validation.WsResponseCode;

/**
 * Implementation of {@link ExceptionMapper} to send down a "400 Bad Request" in the event unparsable JSON is received.
 */
@Provider
public class JsonParseExceptionMapper implements ExceptionMapper<ReaderException> {
    // uncomment when baseline is 1.6
    //@Override
    @Override
    public Response toResponse(ReaderException exception) {
        ServiceResponse response = new ServiceResponse();
        String message = exception.getCause().getMessage();
        Matcher m = Pattern.compile("\'[^\']*\'").matcher(message);
        String inputValue = null, field = null;
        if (m.find()) {
            inputValue = m.group();
        }
        m = Pattern.compile("\\[(\"[^\\]]*\")\\]").matcher(message);
        if (m.find()) {
            field = m.group(1);
        }

        List<WsError> errors = new ArrayList<WsError>();
        errors.add(new WsError(WsResponseCode.INVALID_REQUEST.code(), exception.getCause().getMessage()));
        if (field != null && inputValue != null) {
            errors.get(0).setDescription(inputValue + " : not a valid value for " + field);
        } else {
            errors.get(0).setDescription("please fill valid value");
        }
        response.setErrors(errors);
        return Response.ok().entity(response).type("application/json").build();
    }
}