/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 10, 2011
 *  @author singla
 */
package com.unifier.web.security;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.provider.ClientDetails;

import com.unifier.core.entity.User;
import com.unifier.core.entity.UserRole;

/**
 * @author singla
 */
public class UniwareAppUser implements ClientDetails {

    /**
     * 
     */
    private static final long         serialVersionUID  = -8433616480185276340L;
    private User                      user;
    private final Map<String, String> roleToAuthorities = new HashMap<>();
    private final Set<String>         authorities       = new HashSet<>();

    public UniwareAppUser(User user) {
        this.user = user;
        for (UserRole userRole : user.getUserRoles()) {
            addAuthority(userRole.getRole().getCode());
        }
    }

    private void addAuthority(String roleCode) {
        if (!roleToAuthorities.containsKey(roleCode.toLowerCase())) {
            roleToAuthorities.put(roleCode.toLowerCase(), roleCode);
            authorities.add(roleCode);
        }
    }

    @Override
    public String getClientId() {
        return user.getUsername();
    }

    @Override
    public Set<String> getResourceIds() {
        return authorities;
    }

    @Override
    public boolean isSecretRequired() {
        return false;
    }

    @Override
    public String getClientSecret() {
        return null;
    }

    @Override
    public boolean isScoped() {
        return false;
    }

    @Override
    public Set<String> getScope() {
        return null;
    }

    @Override
    public Set<String> getAuthorizedGrantTypes() {
        return null;
    }

    @Override
    public Set<String> getRegisteredRedirectUri() {
        return null;
    }

    @Override
    public Collection<GrantedAuthority> getAuthorities() {
        return null;
    }

    @Override
    public Integer getAccessTokenValiditySeconds() {
        return null;
    }

    @Override
    public Integer getRefreshTokenValiditySeconds() {
        return null;
    }

    @Override
    public boolean isAutoApprove(String scope) {
        return false;
    }

    @Override
    public Map<String, Object> getAdditionalInformation() {
        return null;
    }
}
