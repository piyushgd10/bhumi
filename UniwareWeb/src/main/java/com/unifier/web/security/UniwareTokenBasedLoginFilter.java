/*
 *  Copyright 2013 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 10-Dec-2013
 *  @author sunny
 */
package com.unifier.web.security;

import java.io.IOException;
import java.util.Arrays;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.security.authentication.AuthenticationDetailsSource;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.event.InteractiveAuthenticationSuccessEvent;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.util.Assert;
import org.springframework.web.filter.GenericFilterBean;

import com.unifier.core.entity.User;
import com.unifier.core.utils.StringUtils;
import com.unifier.services.users.IUsersService;
import com.uniware.core.utils.Constants;

public class UniwareTokenBasedLoginFilter extends GenericFilterBean implements ApplicationEventPublisherAware {

    private static final String SUPPORT_USER       = "SUPPORT_USER";
    private RedirectStrategy    redirectStrategy   = new DefaultRedirectStrategy();
    private String              filterProcessesUrl = Constants.DEFAULT_TOKEN_BASED_LOGIN_FILTER_URL;

    @Autowired
    private IUsersService       usersService;

    protected Object getPreAuthenticatedPrincipal(HttpServletRequest request) {
        String token = request.getParameter(Constants.TOKEN_BASED_LOGIN);
        if (StringUtils.isNotBlank(token)) {
            try {
                String[] tokenValues = usersService.decodeToken(token);
                if (tokenValues.length >= 2) {
                    if (logger.isDebugEnabled()) {
                        logger.debug("TokenValues: " + Arrays.toString(tokenValues));
                        logger.debug("Token: " + token);
                    }
                    long tokenExpiryTime;
                    try {
                        tokenExpiryTime = new Long(tokenValues[1]).longValue();
                    } catch (NumberFormatException nfe) {
                        logger.error("token[1] did not contain a valid number (contained '" + tokenValues[1] + "')");
                        return null;
                    }

                    if (isTokenExpired(tokenExpiryTime)) {
                        logger.error("token has expired");
                        return null;
                    }

                    User user = usersService.getUserByUsername(tokenValues[0]);
                    if (user != null) {
                        String expectedSignature = usersService.makeTokenSignature(tokenExpiryTime, user.getUsername());
                        if (!expectedSignature.equals(tokenValues[2])) {
                            logger.error("signatures do not match");
                            return null;
                        }
                        if (tokenValues.length == 4) {
                            request.getSession().setAttribute(SUPPORT_USER, tokenValues[3]);
                        }
                        return user.getUsername();
                    }
                }
            } catch (Exception e) {
                logger.error("Error decoding token: " + token, e);
            }
        }
        return null;
    }

    protected boolean isTokenExpired(long tokenExpiryTime) {
        return tokenExpiryTime < System.currentTimeMillis();
    }

    private ApplicationEventPublisher                          eventPublisher              = null;
    private AuthenticationDetailsSource<HttpServletRequest, ?> authenticationDetailsSource = new WebAuthenticationDetailsSource();
    private AuthenticationManager                              authenticationManager       = null;

    /**
     * Check whether all required properties have been set.
     */
    @Override
    public void afterPropertiesSet() {
        try {
            super.afterPropertiesSet();
        } catch (ServletException e) {
            // convert to RuntimeException for passivity on afterPropertiesSet signature
            throw new RuntimeException(e);
        }
        Assert.notNull(authenticationManager, "An AuthenticationManager must be set");
    }

    /**
     * Try to authenticate a pre-authenticated user with Spring Security if the user has not yet been authenticated.
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        if (logger.isDebugEnabled()) {
            logger.debug("Checking secure context token: " + SecurityContextHolder.getContext().getAuthentication());
        }

        if (!requiresAuthentication((HttpServletRequest) request)) {
            chain.doFilter(request, response);
            return;
        }

        doAuthenticate((HttpServletRequest) request, (HttpServletResponse) response);
        redirectStrategy.sendRedirect((HttpServletRequest) request, (HttpServletResponse) response, "/orders");
    }

    /**
     * Do the actual authentication for a pre-authenticated user.
     */
    private void doAuthenticate(HttpServletRequest request, HttpServletResponse response) {
        Authentication authResult;

        Object principal = getPreAuthenticatedPrincipal(request);

        if (principal == null) {
            if (logger.isDebugEnabled()) {
                logger.debug("No pre-authenticated principal found in request");
            }

            return;
        }

        if (logger.isDebugEnabled()) {
            logger.debug("preAuthenticatedPrincipal = " + principal + ", trying to authenticate");
        }

        try {
            PreAuthenticatedAuthenticationToken authRequest = new PreAuthenticatedAuthenticationToken(principal, "");
            authRequest.setDetails(authenticationDetailsSource.buildDetails(request));
            authResult = authenticationManager.authenticate(authRequest);
            successfulAuthentication(request, response, authResult);
        } catch (AuthenticationException failed) {
            unsuccessfulAuthentication(request, response, failed);
            throw failed;
        }
    }

    private boolean requiresAuthentication(HttpServletRequest request) {
        String uri = request.getRequestURI();
        int pathParamIndex = uri.indexOf(';');

        if (pathParamIndex > 0) {
            // strip everything after the first semi-colon
            uri = uri.substring(0, pathParamIndex);
        }

        if ("".equals(request.getContextPath())) {
            return uri.endsWith(filterProcessesUrl);
        }

        return uri.endsWith(request.getContextPath() + filterProcessesUrl);
    }

    /**
     * Puts the <code>Authentication</code> instance returned by the authentication manager into the secure context.
     */
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, Authentication authResult) {
        if (logger.isDebugEnabled()) {
            logger.debug("Authentication success: " + authResult);
        }
        SecurityContextHolder.getContext().setAuthentication(authResult);
        // Fire event
        if (this.eventPublisher != null) {
            eventPublisher.publishEvent(new InteractiveAuthenticationSuccessEvent(authResult, this.getClass()));
        }
    }

    /**
     * Ensures the authentication object in the secure context is set to null when authentication fails.
     * <p>
     * Caches the failure exception as a request attribute
     */
    protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response, AuthenticationException failed) {
        SecurityContextHolder.clearContext();

        if (logger.isDebugEnabled()) {
            logger.debug("Cleared security context due to exception", failed);
        }
        request.setAttribute(WebAttributes.AUTHENTICATION_EXCEPTION, failed);
    }

    /**
     * @param anApplicationEventPublisher The ApplicationEventPublisher to use
     */
    @Override
    public void setApplicationEventPublisher(ApplicationEventPublisher anApplicationEventPublisher) {
        this.eventPublisher = anApplicationEventPublisher;
    }

    /**
     * @param authenticationDetailsSource The AuthenticationDetailsSource to use
     */
    public void setAuthenticationDetailsSource(AuthenticationDetailsSource<HttpServletRequest, ?> authenticationDetailsSource) {
        Assert.notNull(authenticationDetailsSource, "AuthenticationDetailsSource required");
        this.authenticationDetailsSource = authenticationDetailsSource;
    }

    protected AuthenticationDetailsSource<HttpServletRequest, ?> getAuthenticationDetailsSource() {
        return authenticationDetailsSource;
    }

    /**
     * @param authenticationManager The AuthenticationManager to use
     */
    public void setAuthenticationManager(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
    }

    public void setFilterProcessesUrl(String filterProcessesUrl) {
        this.filterProcessesUrl = filterProcessesUrl;
    }

}
