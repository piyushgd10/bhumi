/*
 *  Copyright 2013 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *  @version     1.0, 21-July-2015
 *  @author akshay
 */
package com.unifier.web.security;

import java.io.IOException;

import javax.security.auth.callback.UnsupportedCallbackException;

import com.unifier.web.services.APIUserDetailsService;
import org.apache.ws.security.WSPasswordCallback;
import org.apache.ws.security.WSUsernameTokenPrincipal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.dao.DataAccessException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserCache;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.util.Assert;
import org.springframework.ws.soap.security.callback.CleanupCallback;
import org.springframework.ws.soap.security.support.SpringSecurityUtils;
import org.springframework.ws.soap.security.wss4j.callback.AbstractWsPasswordCallbackHandler;
import org.springframework.ws.soap.security.wss4j.callback.UsernameTokenPrincipalCallback;

import com.uniware.core.utils.UserContext;

/**
 * Created by akshaykochhar on 21/07/15.
 */
public class SecurityPasswordValidationHandler extends AbstractWsPasswordCallbackHandler implements InitializingBean {

    private static final Logger LOG = LoggerFactory.getLogger(SecurityPasswordValidationHandler.class);

    private UserCache           userCache;

    private UserDetailsService  userDetailsService;

    /** Sets the users cache. Not required, but can benefit performance. */
    public void setUserCache(UserCache userCache) {
        this.userCache = userCache;
    }

    /** Sets the Spring Security user details service. Required. */
    public void setUserDetailsService(UserDetailsService userDetailsService) {
        this.userDetailsService = userDetailsService;
    }

    public void afterPropertiesSet() throws Exception {
        Assert.notNull(userDetailsService, "userDetailsService is required");
        Assert.notNull(userCache, "userCache is required");
    }

    @Override
    protected void handleUsernameToken(WSPasswordCallback callback) throws IOException, UnsupportedCallbackException {
        String identifier = callback.getIdentifier();
        UserDetails user = loadUserDetails(identifier);
        if (user != null) {
            SpringSecurityUtils.checkUserValidity(user);
            callback.setPassword(user.getPassword());
        }
    }

    @Override
    protected void handleUsernameTokenPrincipal(UsernameTokenPrincipalCallback callback) throws IOException, UnsupportedCallbackException {
        UserDetails user = loadUserDetails(callback.getPrincipal().getName());
        WSUsernameTokenPrincipal principal = callback.getPrincipal();
        UsernamePasswordAuthenticationToken authRequest = new UsernamePasswordAuthenticationToken(principal, principal.getPassword(), user.getAuthorities());
        if (logger.isDebugEnabled()) {
            logger.debug("Authentication success: " + authRequest.toString());
        }
        SecurityContextHolder.getContext().setAuthentication(authRequest);
    }

    @Override
    protected void handleCleanup(CleanupCallback callback) throws IOException, UnsupportedCallbackException {
        SecurityContextHolder.clearContext();
    }

    private UserDetails loadUserDetails(String username) throws DataAccessException {
        UserDetails user = userCache.getUserFromCache(username);
        if (user == null) {
            try {
                user = userDetailsService.loadUserByUsername(username);
            } catch (UsernameNotFoundException notFound) {
                if (logger.isDebugEnabled()) {
                    logger.debug("Username '" + username + "' not found");
                }
                return null;
            }
            userCache.putUserInCache(user);
        }
        UserContext.current().setApiUsername(user.getUsername());
        if (user instanceof APIUserDetailsService.ApiUserDetails) {
            UserContext.current().setUserId(((APIUserDetailsService.ApiUserDetails) user).getApiUser().getUser().getId());
        }
        return user;
    }
}
