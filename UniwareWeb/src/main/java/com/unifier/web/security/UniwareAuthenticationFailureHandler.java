/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 10, 2011
 *  @author singla
 */
package com.unifier.web.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.unifier.core.api.base.ServiceResponse;
import com.unifier.core.api.validation.WsError;
import com.uniware.core.api.validation.WsResponseCode;

/**
 * @author Sunny
 */
@Component("uniwareAuthenticationFailureHandler")
public class UniwareAuthenticationFailureHandler extends SimpleUrlAuthenticationFailureHandler {

    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, ServletException {
        ServiceResponse serviceResponse = new ServiceResponse();
        serviceResponse.addError(new WsError(WsResponseCode.AUTHENTICATION_FAILURE.code(), WsResponseCode.AUTHENTICATION_FAILURE.message()));
        // CORS "pre-flight" request
        if (request.getHeader("Access-Control-Request-Method") != null && "OPTIONS".equals(request.getMethod())) {
            response.addHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
            response.addHeader("Access-Control-Allow-Headers", "X-Requested-With, Origin, X-Csrftoken, Content-Type, Accept");
        }
        response.getWriter().print(new Gson().toJson(serviceResponse));
        response.getWriter().flush();
    }
}
