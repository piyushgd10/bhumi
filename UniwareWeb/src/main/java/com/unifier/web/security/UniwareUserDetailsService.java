/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 10, 2011
 *  @author singla
 */
package com.unifier.web.security;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.entity.User;
import com.unifier.services.user.notification.IUserNotificationService;
import com.unifier.services.users.IUsersService;
import com.uniware.core.cache.RolesCache;
import com.uniware.core.utils.UserContext;
import com.uniware.core.vo.UserNotificationTypeVO.NotificationType;
import com.uniware.core.vo.UserProfileVO;
import com.uniware.dao.user.notification.IUserProfileMao;
import com.uniware.services.configuration.UserNotificationTypeConfiguration;

/**
 * @author singla
 */
public class UniwareUserDetailsService implements UserDetailsService {

    @Autowired
    private IUsersService            usersService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException, DataAccessException {
        User user = usersService.identifyUser(username);
        if (user != null) {
            UserContext.current().setUniwareUserName(user.getUsername());
            UserContext.current().setUserId(user.getId());
            UniwareUser uniwareUser = new UniwareUser(user);
            RolesCache rolesCache = CacheManager.getInstance().getCache(RolesCache.class);
            UserNotificationTypeConfiguration configuration = ConfigurationManager.getInstance().getConfiguration(UserNotificationTypeConfiguration.class);
            List<NotificationType> notificationTypes = new ArrayList<NotificationType>();
            for (NotificationType notificationType : NotificationType.values()) {
                String accessResource = configuration.getUserNotificationTypeByType(notificationType).getAccessResourceName();
                if (uniwareUser.hasAnyRole(rolesCache.getRolesByAccessResource(accessResource))) {
                    notificationTypes.add(notificationType);
                }
            }
            uniwareUser.setNotificationTypes(notificationTypes);
            return uniwareUser;
        } else {
            throw new UsernameNotFoundException("invalid username/email:" + username);
        }
    }
}
