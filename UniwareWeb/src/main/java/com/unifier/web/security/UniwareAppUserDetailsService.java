/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 10, 2011
 *  @author singla
 */
package com.unifier.web.security;

import com.unifier.core.entity.User;
import org.springframework.beans.factory.annotation.Autowired;

import com.unifier.services.users.IUsersService;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.ClientRegistrationException;

/**
 * @author singla
 */
public class UniwareAppUserDetailsService implements ClientDetailsService {

    @Autowired
    private IUsersService            usersService;

    @Override public ClientDetails loadClientByClientId(String clientId) throws ClientRegistrationException {
        User user = usersService.getUserByUsername(clientId);
        return new UniwareAppUser(user);
    }

    //    /* (non-Javadoc)
//     * @see org.springframework.security.core.userdetails.UserDetailsService#loadUserByUsername(java.lang.String)
//     */
//    @Override
//    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException, DataAccessException {
//        User user = usersService.getUserByUsername(username);
//        if (user != null) {
//            UserContext.current().setUniwareUserName(user.getUsername());
//            UniwareUser uniwareUser = new UniwareUser(user);
//            GetOnetimeTasksResponse response = onetimeTasksService.getOnetimeTasks(new GetOnetimeTasksRequest(user.getId()));
//            uniwareUser.setScheduledExportJobs(response.getScheduledExportJobs());
//            uniwareUser.setScheduledImportJobs(response.getScheduledImportJobs());
//            uniwareUser.setCompletedExportJobs(response.getCompletedExportJobs());
//            uniwareUser.setCompletedImportJobs(response.getCompletedImportJobs());
//            RolesCache rolesCache = CacheManager.getInstance().getCache(RolesCache.class);
//            UserNotificationTypeConfiguration configuration = ConfigurationManager.getInstance().getConfiguration(UserNotificationTypeConfiguration.class);
//            List<NotificationType> notificationTypes = new ArrayList<NotificationType>();
//            for (NotificationType notificationType : NotificationType.values()) {
//                String accessResource = configuration.getUserNotificationTypeByType(notificationType).getAccessResourceName();
//                if (uniwareUser.hasAnyRole(rolesCache.getRolesByAccessResource(accessResource))) {
//                    notificationTypes.add(notificationType);
//                }
//            }
//            uniwareUser.setNotificationTypes(notificationTypes);
//            return uniwareUser;
//        } else {
//            throw new UsernameNotFoundException("invalid email:" + username);
//        }
//    }
}
