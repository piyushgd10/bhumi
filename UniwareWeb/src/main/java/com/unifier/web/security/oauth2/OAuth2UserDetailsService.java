/*
*  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
*  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
*  
*  @version     1.0, 29/08/14
*  @author sunny
*/

package com.unifier.web.security.oauth2;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.unifier.core.entity.User;
import com.unifier.core.entity.UserRole;
import com.unifier.services.users.IUsersService;
import com.unifier.web.security.UniwareAuthority;
import com.uniware.core.utils.UserContext;

public class OAuth2UserDetailsService implements UserDetailsService {

    @Autowired
    private IUsersService usersService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = usersService.identifyUser(username);
        if (user != null) {
            UserContext.current().setUniwareUserName(user.getUsername());
            UserContext.current().setUserId(user.getId());
            return new OAuth2UserDetails(user);
        } else {
            throw new UsernameNotFoundException("invalid email:" + username);
        }
    }

    public static class OAuth2UserDetails implements UserDetails {

        /**
         *
         */
        private static final long           serialVersionUID = 7769615631911735128L;
        private User                        user;
        private final Set<GrantedAuthority> authorities      = new HashSet<GrantedAuthority>();

        public OAuth2UserDetails(User user) {
            loadUser(user);
            for (UserRole userRole : user.getUserRoles()) {
                authorities.add(new UniwareAuthority(userRole.getRole().getCode()));
            }
        }

        public void loadUser(User user) {
            this.user = user;
        }

        @Override
        public Collection<? extends GrantedAuthority> getAuthorities() {
            return authorities;
        }

        @Override
        public String getPassword() {
            return null;
        }

        @Override
        public String getUsername() {
            return user.getUsername();
        }

        @Override
        public boolean isAccountNonExpired() {
            return true;
        }

        @Override
        public boolean isAccountNonLocked() {
            return true;
        }

        @Override
        public boolean isCredentialsNonExpired() {
            return true;
        }

        @Override
        public boolean isEnabled() {
            return user.isEnabled();
        }

    }
}