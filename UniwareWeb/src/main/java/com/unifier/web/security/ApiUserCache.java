/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *  @version     1.0, 21-July-2015
 *  @author akshay
 */
package com.unifier.web.security;

import org.springframework.security.core.userdetails.UserCache;
import org.springframework.security.core.userdetails.UserDetails;

import com.unifier.core.utils.InMemoryEvictionCache;
import com.uniware.core.utils.UserContext;

/**
 * Created by akshaykochhar on 21/07/15.
 */
public class ApiUserCache implements UserCache {

    private static final int                   TTL          = 600;

    private InMemoryEvictionCache<UserDetails> apiUserCache = new InMemoryEvictionCache<>();

    @Override
    public UserDetails getUserFromCache(String username) {
        return apiUserCache.get(UserContext.current().getTenant().getCode() + username);
    }

    @Override
    public void putUserInCache(UserDetails user) {
        String tenantCode = UserContext.current().getTenant().getCode();
        apiUserCache.set(tenantCode + user.getUsername(), TTL, user);
    }

    @Override
    public void removeUserFromCache(String username) {
        apiUserCache.remove(UserContext.current().getTenant().getCode() + username);
    }
}
