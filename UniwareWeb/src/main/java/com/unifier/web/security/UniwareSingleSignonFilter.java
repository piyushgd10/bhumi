/*
 *  Copyright 2013 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 10-Dec-2013
 *  @author sunny
 */
package com.unifier.web.security;

import java.io.IOException;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.entity.ApiUser;
import com.unifier.core.transport.http.HttpSender;
import com.unifier.core.utils.JsonUtils;
import com.uniware.core.cache.EnvironmentPropertiesCache;
import com.uniware.core.utils.UserContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.security.authentication.AuthenticationDetailsSource;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.event.InteractiveAuthenticationSuccessEvent;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.util.Assert;
import org.springframework.web.filter.GenericFilterBean;

import com.unifier.core.entity.User;
import com.unifier.core.utils.StringUtils;
import com.unifier.services.users.IUsersService;
import com.uniware.core.utils.Constants;

public class UniwareSingleSignonFilter extends GenericFilterBean implements ApplicationEventPublisherAware {

    private static final Logger           LOG                = LoggerFactory.getLogger(UniwareSingleSignonFilter.class);
    private              RedirectStrategy redirectStrategy   = new DefaultRedirectStrategy();
    private              String           filterProcessesUrl = Constants.AUTH_REQUEST_PROCESSOR_FILTER_URL;

    private ApplicationEventPublisher                          eventPublisher              = null;
    private AuthenticationDetailsSource<HttpServletRequest, ?> authenticationDetailsSource = new WebAuthenticationDetailsSource();
    private AuthenticationManager                              authenticationManager       = null;
    private HttpSender                                         httpSender                  = new HttpSender();

    @Autowired
    private IUsersService usersService;

    protected Object getPreAuthenticatedPrincipal(HttpServletRequest request) {
        if (request.getParameter(Constants.SERVICE_TICKET_ID) != null) {
            String authServerUrl = CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class).getAuthServerUrl();
            Map<String, String> params = new HashMap<>();
            params.put("serviceTicketId", request.getParameter(Constants.SERVICE_TICKET_ID));
            params.put("serviceName", UserContext.current().getTenant().getCode());
            Map<String, String> headers = new HashMap<>();
            headers.put("Content-Type", "application/json");
            String requestBody = JsonUtils.objectToString(params);
            try {
                String response = httpSender.executePost(authServerUrl + "/data/service/ticket/validate", requestBody, headers);
                LOG.info("AuthServerUrl: {}, body: {}, response: {}", new Object[] { authServerUrl, requestBody, response });
                JsonObject responseJson = ((JsonElement) JsonUtils.stringToJson(response)).getAsJsonObject();
                if (responseJson.get("successful").getAsBoolean()) {
                    String email = responseJson.get("email").getAsString();
                    User user = usersService.getUserByEmail(email);
                    if (user != null) {
                        LOG.warn("Invalid user: {}", email);
                        return user.getUsername();
                    }
                } else {
                    LOG.warn("Invalid response from auth server: {}", response);
                }
            } catch (Exception e) {
                LOG.error("Error connecting auth server", e);
               // return "karun@unicommerce.com"; // XXX hack
                return null;
            }
        } else if (request.getParameter(Constants.API_USERNAME) != null) {
            // Only for dev setup
            Enumeration e =  request.getHeaderNames();
            LOG.info("IP:" + request.getRemoteAddr());
            LOG.info("Host:" + request.getRemoteHost());
            while (e.hasMoreElements()) {
                String headerName = e.nextElement().toString();
                LOG.info("Header- " + headerName + ":" + request.getHeader(headerName));
            }
            ApiUser apiUser = usersService.getApiUserByUsername(request.getParameter(Constants.API_USERNAME));
            if (apiUser != null && apiUser.getPassword().equals(request.getParameter(Constants.API_PASSWORD))) {
                return apiUser.getUser().getUsername();
            }
        }
        return null;
    }

    /**
     * Check whether all required properties have been set.
     */
    @Override
    public void afterPropertiesSet() {
        try {
            super.afterPropertiesSet();
        } catch (ServletException e) {
            // convert to RuntimeException for passivity on afterPropertiesSet signature
            throw new RuntimeException(e);
        }
        Assert.notNull(authenticationManager, "An AuthenticationManager must be set");
    }

    /**
     * Try to authenticate a pre-authenticated user with Spring Security if the user has not yet been authenticated.
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        if (logger.isDebugEnabled()) {
            logger.debug("Checking secure context token: " + SecurityContextHolder.getContext().getAuthentication());
        }

        if (!requiresAuthentication((HttpServletRequest) request)) {
            chain.doFilter(request, response);
            return;
        }

        doAuthenticate((HttpServletRequest) request, (HttpServletResponse) response);
        redirectStrategy.sendRedirect((HttpServletRequest) request, (HttpServletResponse) response, "/");
    }

    /**
     * Do the actual authentication for a pre-authenticated user.
     */
    private void doAuthenticate(HttpServletRequest request, HttpServletResponse response) {
        Authentication authResult;

        Object principal = getPreAuthenticatedPrincipal(request);

        if (principal == null) {
            if (logger.isDebugEnabled()) {
                logger.debug("No pre-authenticated principal found in request");
            }

            return;
        }

        if (logger.isDebugEnabled()) {
            logger.debug("preAuthenticatedPrincipal = " + principal + ", trying to authenticate");
        }

        try {
            PreAuthenticatedAuthenticationToken authRequest = new PreAuthenticatedAuthenticationToken(principal, "");
            authRequest.setDetails(authenticationDetailsSource.buildDetails(request));
            authResult = authenticationManager.authenticate(authRequest);
            successfulAuthentication(request, response, authResult);
        } catch (AuthenticationException failed) {
            unsuccessfulAuthentication(request, response, failed);
            throw failed;
        }
    }

    private boolean requiresAuthentication(HttpServletRequest request) {
        String uri = request.getRequestURI();
        int pathParamIndex = uri.indexOf(';');

        if (pathParamIndex > 0) {
            // strip everything after the first semi-colon
            uri = uri.substring(0, pathParamIndex);
        }

        if ("".equals(request.getContextPath())) {
            return uri.endsWith(filterProcessesUrl);
        }

        return uri.endsWith(request.getContextPath() + filterProcessesUrl);
    }

    /**
     * Puts the <code>Authentication</code> instance returned by the authentication manager into the secure context.
     */
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, Authentication authResult) {
        if (logger.isDebugEnabled()) {
            logger.debug("Authentication success: " + authResult);
        }
        SecurityContextHolder.getContext().setAuthentication(authResult);
        // Fire event
        if (this.eventPublisher != null) {
            eventPublisher.publishEvent(new InteractiveAuthenticationSuccessEvent(authResult, this.getClass()));
        }
    }

    /**
     * Ensures the authentication object in the secure context is set to null when authentication fails.
     * <p>
     * Caches the failure exception as a request attribute
     */
    protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response, AuthenticationException failed) {
        SecurityContextHolder.clearContext();

        if (logger.isDebugEnabled()) {
            logger.debug("Cleared security context due to exception", failed);
        }
        request.setAttribute(WebAttributes.AUTHENTICATION_EXCEPTION, failed);
    }

    /**
     * @param anApplicationEventPublisher The ApplicationEventPublisher to use
     */
    @Override
    public void setApplicationEventPublisher(ApplicationEventPublisher anApplicationEventPublisher) {
        this.eventPublisher = anApplicationEventPublisher;
    }

    /**
     * @param authenticationDetailsSource The AuthenticationDetailsSource to use
     */
    public void setAuthenticationDetailsSource(AuthenticationDetailsSource<HttpServletRequest, ?> authenticationDetailsSource) {
        Assert.notNull(authenticationDetailsSource, "AuthenticationDetailsSource required");
        this.authenticationDetailsSource = authenticationDetailsSource;
    }

    protected AuthenticationDetailsSource<HttpServletRequest, ?> getAuthenticationDetailsSource() {
        return authenticationDetailsSource;
    }

    /**
     * @param authenticationManager The AuthenticationManager to use
     */
    public void setAuthenticationManager(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
    }

    public void setFilterProcessesUrl(String filterProcessesUrl) {
        this.filterProcessesUrl = filterProcessesUrl;
    }

}
