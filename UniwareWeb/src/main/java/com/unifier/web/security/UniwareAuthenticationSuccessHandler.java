/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 10, 2011
 *  @author singla
 */
package com.unifier.web.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.RequestCache;
import org.springframework.security.web.savedrequest.SavedRequest;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.unifier.core.api.base.ServiceResponse;
import com.unifier.core.entity.User;
import com.unifier.core.utils.DateUtils;
import com.unifier.services.users.IUsersService;

/**
 * @author Sunny
 */
@Component("uniwareAuthenticationSuccessHandler")
public class UniwareAuthenticationSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {

    @Autowired
    private IUsersService userService;

    private RequestCache  requestCache = new HttpSessionRequestCache();

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        AuthentionSuccessResponse serviceResponse = new AuthentionSuccessResponse();
        
        // Update user last login time
        User user = userService.getUserByUsername(authentication.getName());
        user.setLastLoginTime(DateUtils.getCurrentTime());
        user.setLastAccessedFrom(((WebAuthenticationDetails) authentication.getDetails()).getRemoteAddress());
        userService.updateUser(user);
        
        SavedRequest savedRequest = requestCache.getRequest(request, response);
        if (savedRequest != null) {
            serviceResponse.setTargetUrl(savedRequest.getRedirectUrl());
        }
        serviceResponse.setSuccessful(true);
        // CORS "pre-flight" request
        if (request.getHeader("Access-Control-Request-Method") != null && "OPTIONS".equals(request.getMethod())) {
            response.addHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
            response.addHeader("Access-Control-Allow-Headers", "X-Requested-With, Origin, X-Csrftoken, Content-Type, Accept");
        }
        response.getWriter().print(new Gson().toJson(serviceResponse));
        response.getWriter().flush();
    }

    public static class AuthentionSuccessResponse extends ServiceResponse {

        /**
         * 
         */
        private static final long serialVersionUID = 1121875717769139160L;

        private String            targetUrl;

        public String getTargetUrl() {
            return targetUrl;
        }

        public void setTargetUrl(String targetUrl) {
            this.targetUrl = targetUrl;
        }

    }

}
