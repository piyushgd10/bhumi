/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 10, 2011
 *  @author singla
 */
package com.unifier.web.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.unifier.core.api.base.ServiceResponse;
import com.unifier.core.api.validation.WsError;
import com.uniware.core.api.validation.WsResponseCode;

/**
 * @author Sunny
 */
@Component("uniwareAuthenticationEntryPoint")
public class UniwareAuthenticationEntryPoint implements AuthenticationEntryPoint {

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException, ServletException {
        ServiceResponse serviceResponse = new ServiceResponse();
        serviceResponse.addError(new WsError(WsResponseCode.USER_NOT_LOGGED_IN.code(), WsResponseCode.USER_NOT_LOGGED_IN.message()));
        response.getWriter().print(new Gson().toJson(serviceResponse));
        response.getWriter().flush();
    }

}
