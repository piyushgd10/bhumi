/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 10, 2011
 *  @author singla
 */
package com.unifier.web.security;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.springframework.security.core.CredentialsContainer;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.entity.Role;
import com.unifier.core.entity.User;
import com.unifier.core.entity.UserRole;
import com.unifier.core.utils.StringUtils;
import com.unifier.web.configuration.LayoutConfiguration;
import com.unifier.web.configuration.LayoutConfiguration.InnerLink;
import com.unifier.web.configuration.LayoutConfiguration.SideTab;
import com.unifier.web.configuration.LayoutConfiguration.SideTabGroup;
import com.unifier.web.configuration.LayoutConfiguration.Tab;
import com.unifier.web.configuration.LayoutConfiguration.TabGroup;
import com.unifier.web.utils.WebContextUtils;
import com.uniware.core.cache.FacilityCache;
import com.uniware.core.cache.RolesCache;
import com.uniware.core.entity.Facility;
import com.uniware.core.entity.UserDatatableView;
import com.uniware.core.utils.UserContext;
import com.uniware.core.vo.UserNotificationTypeVO.NotificationType;
import com.uniware.services.configuration.DatatableViewConfiguration;

/**
 * @author singla
 */
public class UniwareUser implements UserDetails, CredentialsContainer {

    /**
     * 
     */
    private static final long                   serialVersionUID     = -5083949793465570706L;
    /**
     * 
     */
    private User                                primaryUser;
    private User                                user;
    private final Map<String, UniwareAuthority> roleToAuthorities    = new HashMap<String, UniwareAuthority>();
    private final Set<GrantedAuthority>         authorities          = new HashSet<GrantedAuthority>();
    private LayoutConfiguration                 accessibleLayout;
    private Integer                             currentFacilityId;
    private final Set<Integer>                  accessibleFacilities = new TreeSet<Integer>(new Comparator<Integer>() {

                                                                         @Override
                                                                         public int compare(Integer facilityId1, Integer facilityId2) {
                                                                             FacilityCache facilityCache = CacheManager.getInstance().getCache(FacilityCache.class);
                                                                             Facility facility1 = facilityCache.getFacilityById(facilityId1);
                                                                             Facility facility2 = facilityCache.getFacilityById(facilityId2);
                                                                             return (facility1 != null && facility2 != null) ? facility1.getCode().compareTo(facility2.getCode())
                                                                                     : 0;
                                                                         }
                                                                     });
    private boolean                             accessibleAllFacilities;
    private String                              accessibleLayoutJson;
    private String                              fileUploadToken;
    private List<NotificationType>              notificationTypes;

    public UniwareUser(User user) {
        loadUser(user);
    }

    public void loadUser(User user) {
        clearPrivileges();
        this.user = user;
        RolesCache cache = CacheManager.getInstance().getCache(RolesCache.class);
        Role role;
        accessibleAllFacilities = false;
        FacilityCache facilityCache = CacheManager.getInstance().getCache(FacilityCache.class);
        boolean hasMultipleFacilities = facilityCache.getFacilities().size() > 1;
        if (user.getUserRoles().size() > 0) {
            for (UserRole userRole : user.getUserRoles()) {
                if (userRole.getFacility() != null) {
                    if (facilityCache.getFacilityById(userRole.getFacility().getId()) != null) {
                        accessibleFacilities.add(userRole.getFacility().getId());
                    }
                } else if (hasMultipleFacilities) {
                    accessibleAllFacilities = true;
                }
            }
            if ((user.getCurrentFacility() != null && !accessibleFacilities.contains(user.getCurrentFacility().getId()))
                    || (user.getCurrentFacility() == null && !accessibleAllFacilities)) {
                if (accessibleFacilities.size() > 0) {
                    currentFacilityId = accessibleFacilities.iterator().next();
                }
            } else {
                if (user.getCurrentFacility() != null) {
                    currentFacilityId = user.getCurrentFacility().getId();
                }
            }
            for (UserRole userRole : user.getUserRoles()) {
                if (currentFacilityId == null && userRole.getFacility() == null) {
                    role = cache.getRole(userRole.getRole().getId());
                    addAuthority(role.getCode());
                } else if (currentFacilityId != null && userRole.getFacility() != null && userRole.getFacility().getId().equals(currentFacilityId)) {
                    role = cache.getRole(userRole.getRole().getId());
                    addAuthority(role.getCode());
                }
            }
        }
        UserContext.current().setFacility(CacheManager.getInstance().getCache(FacilityCache.class).getFacilityById(currentFacilityId));
        if (hasRole(Role.Code.VENDOR.name()) && user.getVendor() == null) {
            roleToAuthorities.remove(Role.Code.VENDOR.name().toLowerCase());
        }
        if (roleToAuthorities.size() > 0) {
            addAuthority(Role.Code.REGISTERED.name());
            accessibleLayout = loadAccessibleLayout();
        }

    }

    private void clearPrivileges() {
        this.user = null;
        this.currentFacilityId = null;
        this.accessibleFacilities.clear();
        this.accessibleAllFacilities = false;
        this.authorities.clear();
        this.roleToAuthorities.clear();
        this.accessibleLayout = null;
    }

    /**
     * @return the primaryUser
     */
    public User getPrimaryUser() {
        return primaryUser;
    }

    private LayoutConfiguration loadAccessibleLayout() {
        RolesCache cache = CacheManager.getInstance().getCache(RolesCache.class);
        LayoutConfiguration accessibleLayout = new LayoutConfiguration();
        accessibleLayout.setRequestUriToTabs(new HashMap<String, Tab>());
        LayoutConfiguration layout = (LayoutConfiguration) WebContextUtils.getServletContext().getAttribute("layout");
        for (TabGroup tabGroup : layout.getTabGroups()) {
            TabGroup aTabGroup = new TabGroup(tabGroup);
            for (Tab tab : tabGroup.getTabs()) {
                if (StringUtils.isEmpty(tab.getHref())) {
                    Tab aTab = new Tab(tab);
                    for (SideTabGroup sideTabGroup : tab.getSideTabGroups()) {
                        SideTabGroup aSideTabGroup = new SideTabGroup();
                        for (SideTab sideTab : sideTabGroup.getSideTabs()) {
                            Set<String> accessibleByRoles = cache.getAccessibleByRoles(sideTab.getHref());
                            if (accessibleByRoles != null && hasAnyRole(accessibleByRoles)) {
                                SideTab aSideTab = new SideTab(sideTab);
                                if (!sideTab.getInnerLinks().isEmpty()) {
                                    for (InnerLink innerLink : sideTab.getInnerLinks()) {
                                        if (hasAnyRole(cache.getAccessibleByRoles(innerLink.getHref()))) {
                                            aSideTab.getInnerLinks().add(innerLink);
                                        }
                                    }
                                }
                                if (StringUtils.isEmpty(aTab.getHref())) {
                                    aTab.setHref(aSideTab.getHref());
                                }
                                accessibleLayout.getRequestUriToTabs().put(aSideTab.getHref(), aTab);
                                aSideTabGroup.getSideTabs().add(aSideTab);
                            }
                        }
                        if (aSideTabGroup.getSideTabs().size() > 0) {
                            aSideTabGroup.setName(sideTabGroup.getName());
                            aTab.getSideTabGroups().add(aSideTabGroup);
                        }
                    }
                    if (StringUtils.isNotEmpty(aTab.getHref())) {
                        accessibleLayout.getRequestUriToTabs().put(aTab.getHref(), aTab);
                        aTabGroup.getTabs().add(aTab);
                    }
                } else {
                    Set<String> accessibleByRoles = cache.getAccessibleByRoles(tab.getHref());
                    if (accessibleByRoles != null && hasAnyRole(accessibleByRoles)) {
                        Tab aTab = new Tab(tab);
                        aTabGroup.getTabs().add(aTab);
                        accessibleLayout.getRequestUriToTabs().put(aTab.getHref(), aTab);
                    }
                }
            }
            if (aTabGroup.getTabs().size() > 0) {
                if (aTabGroup.isAdmin()) {
                    accessibleLayout.setAdmin(true);
                }
                accessibleLayout.getTabGroups().add(aTabGroup);
            }
        }
        this.accessibleLayoutJson = new Gson().toJson(accessibleLayout);
        return accessibleLayout;
    }

    private void addAuthority(String roleCode) {
        if (!roleToAuthorities.containsKey(roleCode.toLowerCase())) {
            UniwareAuthority authority = new UniwareAuthority(roleCode);
            roleToAuthorities.put(roleCode.toLowerCase(), authority);
            authorities.add(authority);
        }
    }

    public boolean hasRole(String role) {
        return roleToAuthorities.containsKey(role.toLowerCase());
    }

    public boolean hasAnyRole(String roles) {
        return hasAnyRole(StringUtils.split(roles));
    }

    public boolean hasAnyRole(Collection<String> roles) {
        if (roles != null) {
            for (String role : roles) {
                if (hasRole(role.toLowerCase())) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean hasAllRoles(Collection<String> roles) {
        for (String role : roles) {
            if (!hasRole(role)) {
                return false;
            }
        }
        return true;
    }

    public boolean hasResource(String accessResource) {
        Set<String> roles = CacheManager.getInstance().getCache(RolesCache.class).getRolesByAccessResource(accessResource);
        if (roles != null) {
            for (String role : roles) {
                if (hasRole(role.toLowerCase())) {
                    return true;
                }
            }
        }
        return false;
    }

    public User getUser() {
        return this.user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public void eraseCredentials() {
    }

    @Override
    public Collection<GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return null;
    }

    @Override
    public String getUsername() {
        return this.user.getUsername();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return !user.isBlocked();
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return this.user.isEnabled();
    }

    public Set<String> getRoles() {
        return roleToAuthorities.keySet();
    }

    /**
     * @return the accessibleLayout
     */
    public LayoutConfiguration getAccessibleLayout() {
        return accessibleLayout;
    }

    /**
     * @return the roleToAuthorities
     */
    public Map<String, UniwareAuthority> getRoleToAuthorities() {
        return roleToAuthorities;
    }

    /**
     * @return the accessibleFacilities
     */
    public Set<Integer> getAccessibleFacilities() {
        return accessibleFacilities;
    }

    /**
     * @return the currentFacilityId
     */
    public Integer getCurrentFacilityId() {
        return currentFacilityId;
    }

    public void loginAsUser(User user) {
        if (this.primaryUser == null) {
            this.primaryUser = this.getUser();
            this.loadUser(user);
        }
    }

    public void exitLoginAsUser() {
        if (this.primaryUser != null) {
            this.loadUser(this.primaryUser);
            this.primaryUser = null;
        }
    }

    public boolean isAccessibleAllFacilities() {
        return accessibleAllFacilities;
    }

    public String getDatatableViewsJson(String datatableType) {
        DatatableViewConfiguration configuration = ConfigurationManager.getInstance().getConfiguration(DatatableViewConfiguration.class);
        List<UserDatatableView> datatableViews = configuration.getDatatableViewsByType(datatableType);
        List<UserDatatableView> userViews = new ArrayList<UserDatatableView>();
        Set<String> alreadyAddedViews = new HashSet<String>();
        if (datatableViews != null) {
            for (UserDatatableView datatableView : datatableViews) {
                if (!alreadyAddedViews.contains(datatableView.getName())) {
                    userViews.add(datatableView);
                    alreadyAddedViews.add(datatableView.getName());
                }
            }
        }
        for (UserDatatableView datatableView : getUser().getUserDatatableViews()) {
            if (!alreadyAddedViews.contains(datatableView.getName())) {
                userViews.add(datatableView);
                alreadyAddedViews.add(datatableView.getName());
            }
        }
        return new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create().toJson(userViews);
    }

    /**
     * @return the accessibleLayoutJson
     */
    public String getAccessibleLayoutJson() {
        return accessibleLayoutJson;
    }

    /**
     * @return the fileUploadToken
     */
    public String getFileUploadToken() {
        return fileUploadToken;
    }

    /**
     * @param fileUploadToken the fileUploadToken to set
     */
    public void setFileUploadToken(String fileUploadToken) {
        this.fileUploadToken = fileUploadToken;
    }

    public List<NotificationType> getNotificationTypes() {
        return notificationTypes;
    }

    public void setNotificationTypes(List<NotificationType> notificationTypes) {
        this.notificationTypes = notificationTypes;
    }

}
