/*
*  Copyright 2015 Unicommerce eSolutions (P) Limited . All Rights Reserved.
*  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
*  
*  @version     1.0, 07/08/15
*  @author sunny
*/

package com.unifier.web.security.oauth2;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.mapping.GrantedAuthoritiesMapper;
import org.springframework.security.core.authority.mapping.NullAuthoritiesMapper;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.transport.http.HttpSender;
import com.unifier.core.utils.JsonUtils;
import com.uniware.core.cache.EnvironmentPropertiesCache;

public class OAuth2AuthenticationProvider implements AuthenticationProvider {

    private static final Logger      LOG               = LoggerFactory.getLogger(OAuth2AuthenticationProvider.class);

    private UserDetailsService       userDetailsService;

    private HttpSender               httpSender        = new HttpSender();
    private GrantedAuthoritiesMapper authoritiesMapper = new NullAuthoritiesMapper();

    /**
     * Performs authentication with the same contract as
     * {@link org.springframework.security.authentication.AuthenticationManager#authenticate(org.springframework.security.core.Authentication)}
     * .
     *
     * @param authentication the authentication request object.
     * @return a fully authenticated object including credentials. May return <code>null</code> if the
     *         <code>AuthenticationProvider</code> is unable to support authentication of the passed
     *         <code>Authentication</code> object. In such a case, the next <code>AuthenticationProvider</code> that
     *         supports the presented <code>Authentication</code> class will be tried.
     * @throws org.springframework.security.core.AuthenticationException if authentication fails.
     */
    @Override public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String username = (authentication.getPrincipal() == null) ? "NONE_PROVIDED" : authentication.getName();
        UserDetails user = userDetailsService.loadUserByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException(username + " not found");
        } else {
            String authServerUrl = CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class).getAuthServerUrl();
            Map<String, String> params = new HashMap<>();
            params.put("username", username);
            params.put("password", (String) authentication.getCredentials());
            Map<String, String> headers = new HashMap<>();
            headers.put("Content-Type", "application/json");
            String requestBody = JsonUtils.objectToString(params);
            try {
                String response = httpSender.executePost(authServerUrl + "/data/service/credentials/validate", requestBody, headers);
                LOG.info("AuthServerUrl: {}, body: {}, response: {}", new Object[] { authServerUrl, requestBody, response });
                JsonObject responseJson = ((JsonElement) JsonUtils.stringToJson(response)).getAsJsonObject();
                if (responseJson.get("successful").getAsBoolean()) {
                    UsernamePasswordAuthenticationToken result = new UsernamePasswordAuthenticationToken(user.getUsername(),
                            authentication.getCredentials(), authoritiesMapper.mapAuthorities(user.getAuthorities()));
                    result.setDetails(authentication.getDetails());
                    return result;
                }
            } catch (Exception e) {
                LOG.error("Error connecting auth server", e);
            }
        }
        return null;
    }

    /**
     * Returns <code>true</code> if this <Code>AuthenticationProvider</code> supports the indicated
     * <Code>Authentication</code> object.
     * <p>
     * Returning <code>true</code> does not guarantee an <code>AuthenticationProvider</code> will be able to
     * authenticate the presented instance of the <code>Authentication</code> class. It simply indicates it can support
     * closer evaluation of it. An <code>AuthenticationProvider</code> can still return <code>null</code> from the
     * {@link #authenticate(org.springframework.security.core.Authentication)} method to indicate another
     * <code>AuthenticationProvider</code> should be tried.
     * </p>
     * <p>
     * Selection of an <code>AuthenticationProvider</code> capable of performing authentication is conducted at runtime
     * the <code>ProviderManager</code>.
     * </p>
     *
     * @param authentication
     * @return <code>true</code> if the implementation can more closely evaluate the <code>Authentication</code> class
     *         presented
     */
    @Override
    public boolean supports(Class<?> authentication) {
        return (UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication));
    }

    public UserDetailsService getUserDetailsService() {
        return userDetailsService;
    }

    public void setUserDetailsService(UserDetailsService userDetailsService) {
        this.userDetailsService = userDetailsService;
    }
}
