/*
*  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
*  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
*  
*  @version     1.0, 29/08/14
*  @author sunny
*/

package com.unifier.web.security.oauth2.token;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2RefreshToken;
import org.springframework.security.oauth2.common.util.SerializationUtils;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.AuthenticationKeyGenerator;
import org.springframework.security.oauth2.provider.token.DefaultAuthenticationKeyGenerator;
import org.springframework.security.oauth2.provider.token.TokenStore;

import com.unifier.core.utils.DateUtils;
import com.uniware.core.entity.OAuthAccessToken;
import com.uniware.core.entity.OAuthRefreshToken;
import com.uniware.services.oauth.IOAuthService;

public class UniwareTokenStore implements TokenStore {

    private static final Logger        LOG                        = LoggerFactory.getLogger(UniwareTokenStore.class);

    @Autowired
    private IOAuthService              oAuthService;

    private AuthenticationKeyGenerator authenticationKeyGenerator = new DefaultAuthenticationKeyGenerator();

    public void setAuthenticationKeyGenerator(AuthenticationKeyGenerator authenticationKeyGenerator) {
        this.authenticationKeyGenerator = authenticationKeyGenerator;
    }

    public OAuth2AccessToken getAccessToken(OAuth2Authentication authentication) {
        String key = authenticationKeyGenerator.extractKey(authentication);
        OAuthAccessToken oAuthAccessToken = oAuthService.getOAuthAccessTokenByAuthenticationId(key);
        OAuth2AccessToken accessToken = null;
        if (oAuthAccessToken != null) {
            accessToken = deserializeAccessToken(oAuthAccessToken.getToken());
        }
        if (accessToken != null && !key.equals(authenticationKeyGenerator.extractKey(readAuthentication(accessToken.getValue())))) {
            removeAccessToken(accessToken.getValue());
            // Keep the store consistent (maybe the same user is represented by this authentication but the details have
            // changed)
            storeAccessToken(accessToken, authentication);
        }
        return accessToken;
    }

    public void storeAccessToken(OAuth2AccessToken token, OAuth2Authentication authentication) {
        String refreshToken = null;
        if (token.getRefreshToken() != null) {
            refreshToken = token.getRefreshToken().getValue();
        }

        if (readAccessToken(token.getValue()) != null) {
            removeAccessToken(token.getValue());
        }

        OAuthAccessToken oAuthAccessToken = new OAuthAccessToken();
        oAuthAccessToken.setTokenId(extractTokenKey(token.getValue()));
        oAuthAccessToken.setToken(serializeAccessToken(token));
        oAuthAccessToken.setAuthenticationId(authenticationKeyGenerator.extractKey(authentication));
        oAuthAccessToken.setUsername(authentication.isClientOnly() ? null : authentication.getName());
        oAuthAccessToken.setClientId(authentication.getOAuth2Request().getClientId());
        oAuthAccessToken.setAuthentication(serializeAuthentication(authentication));
        oAuthAccessToken.setRefreshToken(refreshToken);
        oAuthAccessToken.setCreated(DateUtils.getCurrentTime());
        oAuthService.save(oAuthAccessToken);
    }

    public OAuth2AccessToken readAccessToken(String tokenValue) {
        OAuthAccessToken oAuthAccessToken = oAuthService.getOAuthAccessTokenByTokenId(extractTokenKey(tokenValue));
        OAuth2AccessToken accessToken = null;
        if (oAuthAccessToken != null) {
            accessToken = deserializeAccessToken(oAuthAccessToken.getToken());
        }
        return accessToken;
    }

    public void removeAccessToken(OAuth2AccessToken token) {
        removeAccessToken(token.getValue());
    }

    public void removeAccessToken(String tokenValue) {
        oAuthService.deleteOAuthAccessTokenByTokenId(extractTokenKey(tokenValue));
    }

    public OAuth2Authentication readAuthentication(OAuth2AccessToken token) {
        return readAuthentication(token.getValue());
    }

    public OAuth2Authentication readAuthentication(String token) {
        OAuth2Authentication authentication = null;
        OAuthAccessToken oAuthAccessToken = oAuthService.getOAuthAccessTokenByTokenId(extractTokenKey(token));
        if (oAuthAccessToken != null) {
            authentication = deserializeAuthentication(oAuthAccessToken.getAuthentication());
        }
        return authentication;
    }

    public void storeRefreshToken(OAuth2RefreshToken refreshToken, OAuth2Authentication authentication) {
        OAuthRefreshToken oAuthRefreshToken = new OAuthRefreshToken();
        oAuthRefreshToken.setTokenId(extractTokenKey(refreshToken.getValue()));
        oAuthRefreshToken.setToken(serializeRefreshToken(refreshToken));
        oAuthRefreshToken.setAuthentication(serializeAuthentication(authentication));
        oAuthRefreshToken.setCreated(DateUtils.getCurrentTime());
        oAuthService.save(oAuthRefreshToken);
    }

    public OAuth2RefreshToken readRefreshToken(String token) {
        OAuth2RefreshToken refreshToken = null;
        OAuthRefreshToken oAuthRefreshToken = oAuthService.getOAUthRefreshTokenByTokenId(extractTokenKey(token));
        if (oAuthRefreshToken != null) {
            refreshToken = deserializeRefreshToken(oAuthRefreshToken.getToken());
        }
        return refreshToken;
    }

    public void removeRefreshToken(OAuth2RefreshToken token) {
        removeRefreshToken(token.getValue());
    }

    public void removeRefreshToken(String token) {
        oAuthService.deleteOAuthRefreshTokenByTokenId(extractTokenKey(token));
    }

    public OAuth2Authentication readAuthenticationForRefreshToken(OAuth2RefreshToken token) {
        return readAuthenticationForRefreshToken(token.getValue());
    }

    public OAuth2Authentication readAuthenticationForRefreshToken(String value) {
        OAuth2Authentication authentication = null;
        OAuthRefreshToken oAuthRefreshToken = oAuthService.getOAUthRefreshTokenByTokenId(extractTokenKey(value));
        if (oAuthRefreshToken != null) {
            try {
                authentication = deserializeAuthentication(oAuthRefreshToken.getAuthentication());
            } catch (IllegalArgumentException e) {
                LOG.warn("Failed to deserialize access token for " + value, e);
                removeRefreshToken(value);
            }
        }
        return authentication;
    }

    public void removeAccessTokenUsingRefreshToken(OAuth2RefreshToken refreshToken) {
        removeAccessTokenUsingRefreshToken(refreshToken.getValue());
    }

    public void removeAccessTokenUsingRefreshToken(String refreshToken) {
        oAuthService.deleteOAuthAccessTokenByRefreshToken(refreshToken);
    }

    public Collection<OAuth2AccessToken> findTokensByClientId(String clientId) {
        //        List<OAuth2AccessToken> accessTokens = new ArrayList<OAuth2AccessToken>();
        //
        //        try {
        //            accessTokens = jdbcTemplate.query(selectAccessTokensFromClientIdSql, new SafeAccessTokenRowMapper(), clientId);
        //        } catch (EmptyResultDataAccessException e) {
        //            if (LOG.isInfoEnabled()) {
        //                LOG.info("Failed to find access token for clientId " + clientId);
        //            }
        //        }
        //        accessTokens = removeNulls(accessTokens);
        //
        //        return accessTokens;
        return null;
    }

    public Collection<OAuth2AccessToken> findTokensByClientIdAndUserName(String clientId, String userName) {
        //        List<OAuth2AccessToken> accessTokens = new ArrayList<OAuth2AccessToken>();
        //
        //        try {
        //            accessTokens = jdbcTemplate.query(selectAccessTokensFromUserNameAndClientIdSql, new SafeAccessTokenRowMapper(), userName, clientId);
        //        } catch (EmptyResultDataAccessException e) {
        //            if (LOG.isInfoEnabled()) {
        //                LOG.info("Failed to find access token for userName " + userName);
        //            }
        //        }
        //        accessTokens = removeNulls(accessTokens);
        //
        //        return accessTokens;
        return null;
    }

    protected String extractTokenKey(String value) {
        if (value == null) {
            return null;
        }
        MessageDigest digest;
        try {
            digest = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            throw new IllegalStateException("MD5 algorithm not available.  Fatal (should be in the JDK).");
        }

        try {
            byte[] bytes = digest.digest(value.getBytes("UTF-8"));
            return String.format("%032x", new BigInteger(1, bytes));
        } catch (UnsupportedEncodingException e) {
            throw new IllegalStateException("UTF-8 encoding not available.  Fatal (should be in the JDK).");
        }
    }

    protected byte[] serializeAccessToken(OAuth2AccessToken token) {
        return SerializationUtils.serialize(token);
    }

    protected byte[] serializeRefreshToken(OAuth2RefreshToken token) {
        return SerializationUtils.serialize(token);
    }

    protected byte[] serializeAuthentication(OAuth2Authentication authentication) {
        return SerializationUtils.serialize(authentication);
    }

    protected OAuth2AccessToken deserializeAccessToken(byte[] token) {
        return SerializationUtils.deserialize(token);
    }

    protected OAuth2RefreshToken deserializeRefreshToken(byte[] token) {
        return SerializationUtils.deserialize(token);
    }

    protected OAuth2Authentication deserializeAuthentication(byte[] authentication) {
        return SerializationUtils.deserialize(authentication);
    }

}
