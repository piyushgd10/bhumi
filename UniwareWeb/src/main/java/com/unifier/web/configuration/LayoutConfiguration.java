/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jul 12, 2012
 *  @author singla
 */
package com.unifier.web.configuration;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.unifier.core.utils.FileUtils;
import com.unifier.core.utils.StringUtils;
import com.unifier.core.utils.XMLParser;
import com.unifier.core.utils.XMLParser.Element;

public class LayoutConfiguration {

    private final List<TabGroup> tabGroups = new ArrayList<TabGroup>();
    private Map<String, Tab>     requestUriToTabs;
    private boolean              admin;

    /**
     * @return the tabs
     */
    public List<TabGroup> getTabGroups() {
        return tabGroups;
    }

    /**
     * @return the requestUriToTabs
     */
    public Map<String, Tab> getRequestUriToTabs() {
        return requestUriToTabs;
    }

    /**
     * @param requestUriToTabs the requestUriToTabs to set
     */
    public void setRequestUriToTabs(Map<String, Tab> requestUriToTabs) {
        this.requestUriToTabs = requestUriToTabs;
    }

    public static class Tab {
        private String                   name;
        private String                   href;
        private String                   icon;
        private boolean                  hidden;
        private final List<SideTabGroup> sideTabGroups = new ArrayList<SideTabGroup>();

        public Tab() {

        }

        public Tab(Tab other) {
            this.name = other.name;
            this.href = other.href;
            this.icon = other.icon;
            this.hidden = other.hidden;
        }

        /**
         * @return the name
         */
        public String getName() {
            return name;
        }

        /**
         * @param name the name to set
         */
        public void setName(String name) {
            this.name = name;
        }

        /**
         * @return the href
         */
        public String getHref() {
            return href;
        }

        /**
         * @param href the href to set
         */
        public void setHref(String href) {
            this.href = href;
        }

        /**
         * @return the sideTabGroups
         */
        public List<SideTabGroup> getSideTabGroups() {
            return sideTabGroups;
        }

        /**
         * @return the icon
         */
        public String getIcon() {
            return icon;
        }

        /**
         * @param icon the icon to set
         */
        public void setIcon(String icon) {
            this.icon = icon;
        }

        public boolean isHidden() {
            return hidden;
        }

        public void setHidden(boolean hidden) {
            this.hidden = hidden;
        }
    }

    public static class TabGroup {
        private String          name;
        private boolean         admin;
        private final List<Tab> tabs = new ArrayList<Tab>();

        public TabGroup() {

        }

        public TabGroup(TabGroup other) {
            this.name = other.name;
            this.admin = other.admin;
        }

        /**
         * @return the name
         */
        public String getName() {
            return name;
        }

        /**
         * @return the admin
         */
        public boolean isAdmin() {
            return admin;
        }

        /**
         * @param admin the admin to set
         */
        public void setAdmin(boolean admin) {
            this.admin = admin;
        }

        /**
         * @param name the name to set
         */
        public void setName(String name) {
            this.name = name;
        }

        /**
         * @return the sideTabs
         */
        public List<Tab> getTabs() {
            return tabs;
        }
    }

    public static class SideTabGroup {
        private String              name;
        private final List<SideTab> sideTabs = new ArrayList<SideTab>();

        /**
         * @return the name
         */
        public String getName() {
            return name;
        }

        /**
         * @param name the name to set
         */
        public void setName(String name) {
            this.name = name;
        }

        /**
         * @return the sideTabs
         */
        public List<SideTab> getSideTabs() {
            return sideTabs;
        }
    }

    public static class SideTab {

        private String  name;
        private String  href;
        private boolean hidden;
        private String  icon;
        private List<InnerLink> innerLinks = new ArrayList<InnerLink>();

        public SideTab() {
        }

        public SideTab(String name, String href) {
            this.name = name;
            this.href = href;
        }

        public SideTab(SideTab other) {
            this.name = other.name;
            this.href = other.href;
            this.hidden = other.hidden;
            this.icon = other.icon;
        }

        /**
         * @return the hidden
         */
        public boolean isHidden() {
            return hidden;
        }

        /**
         * @param hidden the hidden to set
         */
        public void setHidden(boolean hidden) {
            this.hidden = hidden;
        }

        /**
         * @return the name
         */
        public String getName() {
            return name;
        }

        /**
         * @param name the name to set
         */
        public void setName(String name) {
            this.name = name;
        }

        /**
         * @return the href
         */
        public String getHref() {
            return href;
        }

        /**
         * @param href the href to set
         */
        public void setHref(String href) {
            this.href = href;
        }

        /**
         * @return the icon
         */
        public String getIcon() {
            return icon;
        }

        /**
         * @param icon the icon to set
         */
        public void setIcon(String icon) {
            this.icon = icon;
        }

        public List<InnerLink> getInnerLinks() {
            return innerLinks;
        }

        public void setInnerLinks(List<InnerLink> innerLinks) {
            this.innerLinks = innerLinks;
        }

    }

    public static class InnerLink {

        private String name;
        private String href;

        public InnerLink() {

        }

        public InnerLink(String name, String href) {
            this.name = name;
            this.href = href;
        }


        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getHref() {
            return href;
        }

        public void setHref(String href) {
            this.href = href;
        }

    }

    public static LayoutConfiguration load(InputStream inputStream) throws IOException {
        String layoutXml = FileUtils.getFileAsString(inputStream);
        Element rootElement = XMLParser.parse(layoutXml);
        LayoutConfiguration configuration = new LayoutConfiguration();
        for (Element eTabGroup : rootElement.list("tab-group")) {
            TabGroup tabGroup = new TabGroup();
            tabGroup.setName(eTabGroup.attribute("name"));
            if (eTabGroup.attribute("admin") != null) {
                tabGroup.setAdmin(Boolean.parseBoolean(eTabGroup.attribute("admin")));
            }
            for (Element eTab : eTabGroup.list("tab")) {
                Tab tab = new Tab();
                tab.setName(eTab.attribute("name"));
                tab.setHref(eTab.attribute("href"));
                tab.setIcon(eTab.attribute("icon"));
                if (StringUtils.isNotEmpty(eTab.attribute("hidden"))) {
                    tab.setHidden(Boolean.parseBoolean(eTab.attribute("hidden")));
                }
                for (Element eSideTabGroup : eTab.list("side-tab-group")) {
                    SideTabGroup sideTabGroup = new SideTabGroup();
                    sideTabGroup.setName(eSideTabGroup.attribute("name"));
                    for (Element eSideTab : eSideTabGroup.list("side-tab")) {
                        SideTab sideTab = new SideTab();
                        sideTab.setName(eSideTab.attribute("name"));
                        sideTab.setHref(eSideTab.attribute("href"));
                        sideTab.setIcon(eSideTab.attribute("icon"));
                        if (StringUtils.isNotEmpty(eSideTab.attribute("hidden"))) {
                            sideTab.setHidden(Boolean.parseBoolean(eSideTab.attribute("hidden")));
                        }
                        for (Element innerLink : eSideTab.list("inner-link")) {
                            InnerLink link = new InnerLink();
                            link.setName(innerLink.attribute("name"));
                            link.setHref(innerLink.attribute("href"));
                            sideTab.getInnerLinks().add(link);
                        }
                        sideTabGroup.getSideTabs().add(sideTab);
                    }
                    tab.getSideTabGroups().add(sideTabGroup);
                }
                tabGroup.getTabs().add(tab);
            }
            configuration.getTabGroups().add(tabGroup);
        }
        return configuration;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }
}
