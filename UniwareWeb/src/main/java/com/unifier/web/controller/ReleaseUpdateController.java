package com.unifier.web.controller;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.unifier.core.api.release.GetAllReleaseUpdatesRequest;
import com.unifier.core.api.release.GetAllReleaseUpdatesResponse;
import com.unifier.core.api.release.GetUnreadReleaseUpdateCountRequest;
import com.unifier.core.api.release.GetUnreadReleaseUpdateCountResponse;
import com.unifier.core.api.release.GetUnreadReleaseUpdatesRequest;
import com.unifier.core.api.release.GetUnreadReleaseUpdatesResponse;
import com.unifier.core.api.release.MarkReleaseUpdateReadRequest;
import com.unifier.core.api.release.MarkReleaseUpdateReadResponse;
import com.unifier.services.release.impl.ReleaseUpdateServiceImpl;
import com.unifier.web.utils.WebContextUtils;
import com.uniware.web.aspect.RefreshCurrentUser;

@Controller
@Path("/data/release/")
public class ReleaseUpdateController {

    @Autowired
    private ReleaseUpdateServiceImpl releaseUpdateServiceImpl;

    @Produces(MediaType.APPLICATION_JSON)
    @Path("get")
    @POST
    public GetUnreadReleaseUpdatesResponse getReleaseUpdates(GetUnreadReleaseUpdatesRequest request) {
        request.setUsername(WebContextUtils.getCurrentUser().getUsername());
        return releaseUpdateServiceImpl.getReleaseUpdates(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("getAll")
    @POST
    public GetAllReleaseUpdatesResponse getAllReleaseUpdates(GetAllReleaseUpdatesRequest request) {
        request.setUserId(WebContextUtils.getCurrentUser().getUser().getId());
        return releaseUpdateServiceImpl.getAllReleaseUpdates(request);
    }

    @Path("close")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @RefreshCurrentUser
    public MarkReleaseUpdateReadResponse closeReleaseUpdates(MarkReleaseUpdateReadRequest request) {
        request.setUsername(WebContextUtils.getCurrentUser().getUsername());
        return releaseUpdateServiceImpl.markUpdateRead(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("count")
    @POST
    public GetUnreadReleaseUpdateCountResponse getUnreadReleaseUpdates(GetUnreadReleaseUpdateCountRequest request) {
        request.setUsername(WebContextUtils.getCurrentUser().getUsername());
        return releaseUpdateServiceImpl.getUnreadReleaseUpdatesCount(request);
    }
}
