/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 *  @version     1.0, 02-Feb-2012
 *  @author vibhu
 */
package com.unifier.web.controller;

import com.google.gson.Gson;
import com.unifier.core.entity.ImportJobType;
import com.unifier.core.utils.JsonUtils;
import com.unifier.services.admin.access.IAccessControlService;
import com.unifier.services.application.IApplicationSetupService;
import com.unifier.services.export.IExportService;
import com.unifier.services.imports.IImportService;
import com.uniware.dao.shipping.IPaymentReconciliationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.ws.rs.Path;

@Controller
@Path("/data/admin/")
public class SystemController {

    @Autowired
    private IApplicationSetupService applicationSetupService;

    @Autowired
    private IExportService exportService;

    @Autowired
    private IImportService importService;

    @Autowired
    IAccessControlService accessService;

    @Autowired
    private IPaymentReconciliationService paymentReconciliationService;

    @RequestMapping(value = { "/admin/configuration/facilityAllocationRules" })
    public String facilityAllocationRules(ModelMap modelMap) {
        modelMap.addAttribute("facilityAllocationRules", new Gson().toJson(applicationSetupService.getFacilityAllocationRules()));
        return "admin/configuration/facilityAllocationRules";
    }

    @RequestMapping(value = "/admin/system/emailTemplates")
    public String emailTemplates(ModelMap modelMap) {
        return "admin/system/emailTemplates";
    }

    @RequestMapping(value = "/procure/createInventory")
    public String createInventory() {
        return "procure/createInventory";
    }

    @RequestMapping(value = "/procure/addItemDetails")
    public String addItemDetails() {
        return "procure/addItemDetails";
    }

    @RequestMapping(value = "/admin/system/exportJobType")
    public String exportJobType(ModelMap modelMap) {
        modelMap.addAttribute("exportJobTypes", exportService.getExportJobTypes());
        return "admin/system/exportJobType";
    }

    @RequestMapping(value = "/admin/system/importJobType")
    public String importJobType(ModelMap modelMap) {
        modelMap.addAttribute("importJobTypes", importService.getAllImportJobTypes());
        modelMap.addAttribute("importOptions", new Gson().toJson(ImportJobType.ImportOptions.values()));
        return "admin/system/importJobType";
    }

    @RequestMapping("/admin/system/paymentReconciliation")
    public String paymentReconciliation(ModelMap modelMap) {
        modelMap.addAttribute("paymentReconciliations", JsonUtils.objectToString(paymentReconciliationService.getPaymentReconciliations()));
        return "admin/system/paymentReconciliation";
    }
}
