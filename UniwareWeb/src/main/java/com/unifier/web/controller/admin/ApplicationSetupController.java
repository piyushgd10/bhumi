/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 *  @version     1.0, 23-Jul-2012
 *  @author praveeng
 */
package com.unifier.web.controller.admin;

import com.unifier.core.api.application.GetSystemConfigurationsRequest;
import com.unifier.core.api.application.GetSystemConfigurationsResponse;
import java.io.IOException;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.uniware.core.api.tenant.ReactivateTenantRequest;
import com.uniware.core.api.tenant.ReactivateTenantResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.google.gson.Gson;
import com.unifier.core.api.application.EditSystemConfigurationRequest;
import com.unifier.core.api.application.EditSystemConfigurationResponse;
import com.unifier.core.api.customfields.CreateCustomFieldRequest;
import com.unifier.core.api.customfields.CreateCustomFieldResponse;
import com.unifier.core.api.customfields.EditCustomFieldPossibleValuesRequest;
import com.unifier.core.api.customfields.EditCustomFieldPossibleValuesResponse;
import com.unifier.core.api.customfields.ListCustomFieldsResponse;
import com.unifier.core.api.tasks.EditScriptConfigRequest;
import com.unifier.core.api.tasks.EditScriptConfigResponse;
import com.unifier.core.api.tasks.ScriptConfigDTO;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.entity.CustomFieldMetadata;
import com.unifier.services.application.IApplicationSetupService;
import com.unifier.services.customfields.ICustomFieldService;
import com.uniware.core.api.tenant.CleanupTenantDataRequest;
import com.uniware.core.api.tenant.CleanupTenantDataResponse;
import com.uniware.core.api.tenant.MarkTenantLiveRequest;
import com.uniware.core.api.tenant.MarkTenantLiveResponse;
import com.uniware.services.configuration.CustomFieldsMetadataConfiguration;
import com.uniware.services.script.service.IScriptService;
import com.uniware.services.tenant.ITenantService;

@Controller
@Path("/data/admin/")
public class ApplicationSetupController {

    @Autowired
    IApplicationSetupService applicationSetupService;

    @Autowired
    ICustomFieldService      customFieldService;

    @Autowired
    private ITenantService   tenantService;

    @Autowired
    private IScriptService   scriptService;

    @RequestMapping("/admin/super/systemConfiguration")
    public String systemConfiguration(ModelMap map) {
        map.addAttribute("systemConfiguration", new Gson().toJson(applicationSetupService.getSystemConfiguration()));
        return "/admin/super/systemConfiguration";
    }

    @Path("super/systemConfiguration/getAll")
    public GetSystemConfigurationsResponse getSystemConfigurations(GetSystemConfigurationsRequest request) {
        GetSystemConfigurationsResponse response = new GetSystemConfigurationsResponse(applicationSetupService.getSystemConfiguration());
        return response;
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("super/systemConfiguration/edit")
    @POST
    public EditSystemConfigurationResponse editSystemConfiguration(EditSystemConfigurationRequest request) {
        return applicationSetupService.editSystemConfiguration(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("super/customField/create")
    @POST
    public CreateCustomFieldResponse createCustomField(CreateCustomFieldRequest request) throws IOException, ClassNotFoundException {
        return customFieldService.createCustomField(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("super/customField/edit")
    @POST
    public EditCustomFieldPossibleValuesResponse editCustomField(EditCustomFieldPossibleValuesRequest request) {
        return customFieldService.editCustomField(request);
    }

    @RequestMapping("/admin/super/viewCustomField")
    public String viewCustomFields(ModelMap map) throws ClassNotFoundException, IOException {
        map.addAttribute("entities", ConfigurationManager.getInstance().getConfiguration(CustomFieldsMetadataConfiguration.class).getCustomizableEntitiesJson());
        map.addAttribute("valueTypes", new Gson().toJson(CustomFieldMetadata.ValueType.values()));
        return "/admin/super/viewCustomField";
    }

    @RequestMapping("/admin/super/script")
    public String viewScript(ModelMap map) throws ClassNotFoundException, IOException {
        return "/admin/super/script";
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("super/customField/list")
    @GET
    public ListCustomFieldsResponse listCustomFields() throws IOException, ClassNotFoundException {
        ListCustomFieldsResponse response = customFieldService.listCustomFields();
        return response;
    }
    
    @Produces(MediaType.APPLICATION_JSON)
    @Path("super/script/get")
    @POST
    public ScriptConfigDTO getScriptType(@QueryParam("name") String name) {
        return scriptService.getScriptByName(name);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("super/script/edit")
    @POST
    public EditScriptConfigResponse editScriptConfig(EditScriptConfigRequest request) {
        return scriptService.editScriptConfig(request);

    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("super/tenant/cleanupData")
    @POST
    public CleanupTenantDataResponse cleanupTenantData(CleanupTenantDataRequest request) {
        return tenantService.cleanupTenantData(request);

    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("super/tenant/goLive")
    @POST
    public MarkTenantLiveResponse markTenantLive(MarkTenantLiveRequest request) {
        return tenantService.markTenantLive(request);

    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("tenant/reactivate")
    @POST
    public ReactivateTenantResponse reactivateTenant(ReactivateTenantRequest request) {
        return tenantService.reactivateTenant(request);
    }

}
