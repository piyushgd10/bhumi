/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 *  @version     1.0, 06-Feb-2012
 *  @author vibhu
 */
package com.unifier.web.controller.admin;

import com.google.gson.Gson;
import com.itextpdf.text.PageSize;
import com.unifier.core.cache.CacheManager;
import com.unifier.services.pdf.impl.PdfDocumentServiceImpl.PrintOptions.PrintDialog;
import com.uniware.services.cache.GlobalSamplePrintTemplateCache;
import com.uniware.services.cache.SamplePrintTemplateCache;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Controller
public class PrintConfigController {

    @RequestMapping("/admin/printing/printingTemplate")
    public String printingTemplate(ModelMap map) {
        PrintDialog[] printDialogs = PrintDialog.values();
        map.addAttribute("printDialogs", new Gson().toJson(printDialogs));
        map.addAttribute("printTemplates", CacheManager.getInstance().getCache(SamplePrintTemplateCache.class).getAllSamplePrintTemplates());
        List<String> pageSizes = new ArrayList<String>();
        for (Field pageSize : PageSize.class.getDeclaredFields()) {
            pageSizes.add(pageSize.getName());
        }
        Collections.sort(pageSizes);
        map.addAttribute("pageSizes", new Gson().toJson(pageSizes));

        return "admin/printing/printingTemplate";
    }

}