/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 19-Mar-2012
 *  @author vibhu
 */
package com.unifier.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.unifier.services.export.IExportService;
import com.unifier.services.user.notification.IUserNotificationService;

@Controller
public class TasksController {

    @Autowired
    IUserNotificationService alertsService;

    @Autowired
    IExportService exportService;

    public static final String IMPORT_PAGE        = "tasks/import";
    public static final String EXPORT_PAGE        = "tasks/export";
    public static final String RECURRENT_PAGE     = "tasks/recurrentTask";
    public static final String ALERTS_PAGE        = "tasks/alerts";
    public static final String NOTIFICATIONS_PAGE = "tasks/notifications";

    @RequestMapping(value = { "/tasks/import" })
    public String importConfig(ModelMap map) {
        return IMPORT_PAGE;
    }

    @RequestMapping("/tasks/export")
    public String exportConfig(ModelMap map) {
        return EXPORT_PAGE;
    }

    @RequestMapping("/tasks/recurrentTask")
    public String recurrentTask() {
        return RECURRENT_PAGE;
    }

    //    @RequestMapping("/tasks/alerts")
    //    public String alerts(ModelMap modelMap) {
    //        GetAlertsSubscriptionResponse response = alertsService.getAlertsBySubscriber(WebContextUtils.getCurrentUser().getUser().getId());
    //        modelMap.addAttribute("alerts", new Gson().toJson(response));
    //
    //        return ALERTS_PAGE;
    //    }

    //    @RequestMapping("/tasks/exportSubscribe")
    //    public String recurrentExports(ModelMap modelMap) {
    //        UniwareUser uniwareUser = WebContextUtils.getCurrentUser();
    //        GetExportSubscriptionResponse response = exportService.getExportJobsBySubscriber(uniwareUser.getUser().getId());
    //        Iterator<RecurrentExportJobDTO> it = response.getExportJobs().iterator();
    //        while (it.hasNext()) {
    //            RecurrentExportJobDTO exportJob = it.next();
    //            if (!uniwareUser.hasResource(exportJob.getAccessResourceName())) {
    //                it.remove();
    //            }
    //        }
    //        modelMap.addAttribute("exports", new Gson().toJson(response));
    //        return "/tasks/exportSubscribe";
    //    }

    @RequestMapping("/tasks/notifications")
    public String notifications() {
        return NOTIFICATIONS_PAGE;
    }
}