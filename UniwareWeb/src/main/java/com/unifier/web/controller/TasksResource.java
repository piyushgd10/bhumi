/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 19-Mar-2012
 *  @author vibhu
 */
package com.unifier.web.controller;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;

import com.unifier.core.api.datatable.GetDatatableResultCountRequest;
import com.unifier.core.api.datatable.GetDatatableResultCountResponse;
import org.quartz.SchedulerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.Gson;
import com.unifier.core.api.datatable.GetDatatableResultRequest;
import com.unifier.core.api.datatable.GetDatatableResultResponse;
import com.unifier.core.api.export.AddExportSubscriberRequest;
import com.unifier.core.api.export.AddExportSubscriptionResponse;
import com.unifier.core.api.export.CreateExportJobRequest;
import com.unifier.core.api.export.CreateExportJobResponse;
import com.unifier.core.api.export.DeleteExportSubscriptionRequest;
import com.unifier.core.api.export.DeleteExportSubscriptionResponse;
import com.unifier.core.api.export.EditExportJobRequest;
import com.unifier.core.api.export.EditExportJobResponse;
import com.unifier.core.api.export.EditExportsSubscriptionRequest;
import com.unifier.core.api.export.EditExportsSubscriptionRequest.ExportSubscription;
import com.unifier.core.api.export.EditExportsSubscriptionResponse;
import com.unifier.core.api.export.GetExportSubscriptionResponse;
import com.unifier.core.api.imports.CreateImportJobRequest;
import com.unifier.core.api.imports.CreateImportJobResponse;
import com.unifier.core.api.user.UserSearchDTO;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.entity.AccessResource;
import com.unifier.core.entity.ExportJob;
import com.unifier.core.entity.ImportJobType;
import com.unifier.core.entity.ImportJobType.ImportOptions;
import com.unifier.core.entity.Job;
import com.unifier.core.entity.JobResult;
import com.unifier.core.entity.User;
import com.unifier.core.export.config.ExportConfig;
import com.unifier.core.utils.StringUtils;
import com.unifier.services.export.IExportService;
import com.unifier.services.imports.IImportService;
import com.unifier.services.imports.ImportJobTypeColumnDTO;
import com.unifier.services.imports.ImportJobTypeDTO;
import com.unifier.services.job.IJobResultService;
import com.unifier.services.job.IJobService;
import com.unifier.services.tasks.TaskDTO;
import com.unifier.services.tasks.TasksDTO;
import com.unifier.web.security.UniwareUser;
import com.unifier.web.utils.WebContextUtils;
import com.uniware.core.api.notification.UpdateNotificationRequest;
import com.uniware.core.api.notification.UpdateNotificationResponse;
import com.uniware.core.entity.Product;
import com.uniware.core.utils.UserContext;
import com.uniware.services.configuration.ExportJobConfiguration;
import com.uniware.services.configuration.ImportJobConfiguration;
import com.uniware.services.notification.INotificationService;

@Controller
@Path("/data/tasks/")
public class TasksResource {

    private static final Logger  LOG = LoggerFactory.getLogger(TasksResource.class);

    @Autowired
    private IExportService       exportService;

    @Autowired
    private IImportService       importService;

    @Autowired
    private IJobService          jobService;

    @Autowired
    private INotificationService notificationService;

    @Autowired
    private IJobResultService    jobResultService;

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/recurrentTasks/fetch")
    @GET
    public TasksDTO getRecurrentTasks() {
        TasksDTO tasks = new TasksDTO();
        for (Job job : jobService.getGlobalRecurrentJobs()) {
            doAddRecurrentTask(tasks, job);
        }
        if (Product.Type.ENTERPRISE.name().equals(UserContext.current().getTenant().getProduct().getCode())) {
            for (Job job : jobService.getRecurrentJobsForTenant()) {
                doAddRecurrentTask(tasks, job);
            }
        }
        return tasks;
    }

    private void doAddRecurrentTask(TasksDTO tasks, Job job) {
        TaskDTO recurrentTask = new TaskDTO();
        recurrentTask.setId(job.getCode());
        recurrentTask.setName(job.getName());
        recurrentTask.setType(TaskDTO.Type.RECURRENT.name());
        recurrentTask.setDelay(job.getCronExpression());
        JobResult jobResult = jobResultService.get(job.getCode(), job.getExecutionLevel());
        if (jobResult != null) {
            recurrentTask.setLastExecTime(jobResult.getLastExecTime());
        }
        recurrentTask.setEnabled(job.isEnabled());
        recurrentTask.setCurrentStatus(null);
        recurrentTask.setPercentageCompleted(0);
        recurrentTask.setTaskResult(null);
        recurrentTask.setLastExecResult(job.getLastExecResult());
        tasks.getRecurrentTasks().put(recurrentTask.getId(), recurrentTask);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/recurrentTask/run")
    @GET
    public Map<String, TaskDTO> runRecurrentTask(@QueryParam("taskId") String jobCode) {
        try {
            jobService.runJob(jobCode);
        } catch (SchedulerException e) {
            LOG.error("Failed to run job: {}", jobCode, e);
        }
        Map<String, TaskDTO> runtimeTasks = new HashMap<>();
        return runtimeTasks;
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("export/config/get")
    @GET
    public ExportConfig getConfig(@QueryParam("exportConfigName") String exportConfigName) {
        ExportConfig exportConfig = ConfigurationManager.getInstance().getConfiguration(ExportJobConfiguration.class).getExportConfigByName(exportConfigName);
        if (WebContextUtils.getCurrentUser().hasResource(exportConfig.getAccessResourceName())) {
            return exportConfig;
        } else {
            throw new WebApplicationException(HttpURLConnection.HTTP_FORBIDDEN);
        }
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("export/job/create")
    @POST
    public CreateExportJobResponse createExportJob(CreateExportJobRequest request) {
        ExportConfig exportConfig = ConfigurationManager.getInstance().getConfiguration(ExportJobConfiguration.class).getExportConfigByName(request.getExportJobTypeName());
        UniwareUser uniwareUser = WebContextUtils.getCurrentUser();
        if (uniwareUser.hasResource(exportConfig.getAccessResourceName())) {
            User user = WebContextUtils.getCurrentUser().getUser();
            request.setUserId(user.getId());
            // send email to logged in user
            if (StringUtils.isBlank(request.getNotificationEmail())) {
                request.setNotificationEmail(user.getEmail());
            }
            String frequency = request.getFrequency();
            if (StringUtils.isBlank(frequency) || ExportJob.Frequency.ONETIME.name().equals(frequency)) {
                request.setFrequency(ExportJob.Frequency.ONETIME.name());
            } else if (ExportJob.Frequency.RECURRENT.name().equals(frequency) && uniwareUser.hasResource(AccessResource.Name.SUBSCRIBE_EXPORT.name())) {
                request.setFrequency(ExportJob.Frequency.RECURRENT.name());
            } else {
                throw new WebApplicationException(HttpURLConnection.HTTP_FORBIDDEN);
            }
            return exportService.createExportJob(request);
        } else {
            throw new WebApplicationException(HttpURLConnection.HTTP_FORBIDDEN);
        }
    }

    @RequestMapping("/data/import/job/create")
    @ResponseBody
    public String createImportJob(@RequestParam("file") MultipartFile file, @RequestParam("name") String name,
            @RequestParam(value = "importOption", defaultValue = "CREATE_NEW_AND_UPDATE_EXISTING", required = false) String importOption) throws IOException {
        ImportJobType importJobType = ConfigurationManager.getInstance().getConfiguration(ImportJobConfiguration.class).getImportJobTypeByName(name);
        if (WebContextUtils.getCurrentUser().hasResource(importJobType.getAccessResourceName())) {
            CreateImportJobRequest request = new CreateImportJobRequest();
            request.setInputStream(file.getInputStream());
            request.setFileName(file.getName());
            request.setImportOption(importOption);
            request.setImportJobTypeName(name);
            User user = WebContextUtils.getCurrentUser().getUser();
            request.setUserId(user.getId());
            if (StringUtils.isNotEmpty(user.getEmail())) {
                request.setNotificationEmail(user.getEmail());
            }
            CreateImportJobResponse response = importService.createImportJob(request);
            return new Gson().toJson(response);
        } else {
            throw new WebApplicationException(HttpURLConnection.HTTP_FORBIDDEN);
        }
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/import/jobtype/fetch")
    @GET
    public ImportJobTypeDTO getImportJobType(@QueryParam("jobTypeName") String jobTypeName) throws IOException {
        ImportJobType importJobType = ConfigurationManager.getInstance().getConfiguration(ImportJobConfiguration.class).getImportJobTypeByName(jobTypeName);
        if (importJobType != null && WebContextUtils.getCurrentUser().hasResource(importJobType.getAccessResourceName())) {
            return ConfigurationManager.getInstance().getConfiguration(ImportJobConfiguration.class).getImportJobTypeConfigByName(jobTypeName);
        } else {
            throw new WebApplicationException(HttpURLConnection.HTTP_FORBIDDEN);
        }
    }

    @RequestMapping("/import/jobType/sampleCSV")
    public void getImportCSVFormat(@RequestParam(value = "jobTypeName") String jobTypeName, @RequestParam(value = "importOption") String importOption, HttpServletResponse response)
            throws IOException {
        response.setContentType("application/csv");
        response.addHeader("content-disposition", "attachment; filename=\"" + jobTypeName + ".csv\"");
        StringBuilder builder = new StringBuilder();
        Set<String> columns = new HashSet<>();
        ImportJobTypeDTO importJobTypeConfig = ConfigurationManager.getInstance().getConfiguration(ImportJobConfiguration.class).getImportJobTypeConfigByName(jobTypeName);
        for (ImportJobTypeColumnDTO column : importJobTypeConfig.getSourceColumns()) {
            if (!columns.contains(column.getSource())) {
                if (ImportOptions.UPDATE_EXISTING.name().equals(importOption)) {
                    builder.append(column.getSource() + (column.isRequiredInUpdate() ? "*" : "")).append(importJobTypeConfig.getDelimiter());
                } else {
                    builder.append(column.getSource()).append(column.isRequired() ? "*" : "").append(importJobTypeConfig.getDelimiter());
                }
                columns.add(column.getSource());
            }
        }
        builder.deleteCharAt(builder.length() - 1);
        response.getOutputStream().write(builder.toString().getBytes());
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("exports/subscription/update")
    @POST
    public EditExportsSubscriptionResponse updateExportsSubscription(EditExportsSubscriptionRequest request) {
        UniwareUser currentUser = WebContextUtils.getCurrentUser();
        request.setUserId(currentUser.getUser().getId());
        Iterator<ExportSubscription> it = request.getExportSubscriptions().iterator();
        while (it.hasNext()) {
            ExportSubscription exportSubscription = it.next();
            ExportJob exportJob = exportService.getExportJobById(exportSubscription.getExportJobId().toString());
            if (exportJob != null && !currentUser.hasResource(exportService.getExportJobTypeByName(exportJob.getExportJobTypeName()).getAccessResourceName())) {
                it.remove();
            }
        }
        return exportService.updateExportsSubscription(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("exports/subscription/delete")
    @POST
    public DeleteExportSubscriptionResponse deleteExportsSubscription(DeleteExportSubscriptionRequest request) {
        return exportService.deleteExportSubxcription(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("exports/config/subscribeUsers")
    @POST
    public AddExportSubscriptionResponse addExportSubscriber(AddExportSubscriberRequest request) {
        return exportService.addExportSubscriber(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("export/subscribedUsers")
    @GET
    public List<UserSearchDTO> getSubscribedUsers(@QueryParam("exportJobId") String exportJobId) {
        return jobService.getSubscribedUsers(exportJobId);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("export/recurrent/edit")
    @POST
    public EditExportJobResponse disableExportJob(EditExportJobRequest request) {
        UniwareUser currentUser = WebContextUtils.getCurrentUser();
        if (currentUser.hasResource("ADMIN_EXPORT_JOB")) {
            return exportService.editExportJob(request);
        } else {
            throw new WebApplicationException(HttpURLConnection.HTTP_FORBIDDEN);
        }
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("export/data")
    @POST
    public GetDatatableResultResponse getDatatableResultResponse(GetDatatableResultRequest request) {
        UniwareUser currentUser = WebContextUtils.getCurrentUser();
        request.setUserId(currentUser.getUser().getId());
        return exportService.getDatatableResult(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("notifications/retryOrSkip")
    @POST
    public UpdateNotificationResponse retryOrSkipNotification(UpdateNotificationRequest request) {
        return notificationService.retryOrSkipNotification(request);
    }

    @RequestMapping("/tasks/exportSubscribe")
    public String recurrentExports(ModelMap modelMap) {
        return "/tasks/exportSubscribe";
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("export/getExportsForCurrentUser")
    @GET
    public GetExportSubscriptionResponse getExportsForCurrentUser() {
        UniwareUser uniwareUser = WebContextUtils.getCurrentUser();
        GetExportSubscriptionResponse response = exportService.getExportJobsBySubscriber(uniwareUser.getUser().getUsername());
        Iterator<GetExportSubscriptionResponse.RecurrentExportJobDTO> it = response.getExportJobs().iterator();
        while (it.hasNext()) {
            GetExportSubscriptionResponse.RecurrentExportJobDTO exportJob = it.next();
            if (!uniwareUser.hasResource(exportJob.getAccessResourceName())) {
                it.remove();
            }
        }
        return response;
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("export/data/resultcount")
    @POST
    public GetDatatableResultCountResponse getDatatableResultCountResponse(GetDatatableResultCountRequest request) {
        UniwareUser currentUser = WebContextUtils.getCurrentUser();
        request.setUserId(currentUser.getUser().getId());
        return exportService.getDatatableResultCount(request);
    }
}
