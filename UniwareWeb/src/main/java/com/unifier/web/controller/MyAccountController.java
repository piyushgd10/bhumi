/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 29-Mar-2012
 *  @author vibhu
 */
package com.unifier.web.controller;

import com.unifier.core.api.myaccount.AddUrlVisitedByUserRequest;
import com.unifier.core.api.myaccount.AddUrlVisitedByUserResponse;
import com.unifier.core.api.myaccount.CreateInviteRequest;
import com.unifier.core.api.myaccount.CreateInviteResponse;
import com.unifier.core.api.myaccount.CreatePaymentSaleOrderRequest;
import com.unifier.core.api.myaccount.CreatePaymentSaleOrderResponse;
import com.unifier.core.api.myaccount.EditUserDetailRequest;
import com.unifier.core.api.myaccount.EditUserDetailResponse;
import com.unifier.core.api.myaccount.EditDigestDetailRequest;
import com.unifier.core.api.myaccount.EditDigestDetailResponse;
import com.unifier.core.api.myaccount.GetAccountAlertsRequest;
import com.unifier.core.api.myaccount.GetAccountAlertsResponse;
import com.unifier.core.api.myaccount.GetAccountBalanceRequest;
import com.unifier.core.api.myaccount.GetAccountBalanceResponse;
import com.unifier.core.api.myaccount.GetInvitesByReferrerRequest;
import com.unifier.core.api.myaccount.GetInvitesByReferrerResponse;
import com.unifier.core.api.myaccount.GetMonthlyUsageHistoryRequest;
import com.unifier.core.api.myaccount.GetMonthlyUsageHistoryResponse;
import com.unifier.core.api.myaccount.GetPaymentHistoryRequest;
import com.unifier.core.api.myaccount.GetPaymentHistoryResponse;
import com.unifier.core.api.myaccount.GetProductApplicabilityRequest;
import com.unifier.core.api.myaccount.GetProductApplicabilityResponse;
import com.unifier.core.api.myaccount.GetRecentUsageStatisticsRequest;
import com.unifier.core.api.myaccount.GetRecentUsageStatisticsResponse;
import com.unifier.core.api.myaccount.GetSubscriptionsRequest;
import com.unifier.core.api.myaccount.GetSubscriptionsResponse;
import com.unifier.core.api.myaccount.GetUsageHistoryRequest;
import com.unifier.core.api.myaccount.GetUsageHistoryResponse;
import com.unifier.core.api.myaccount.GetUserDigestRequest;
import com.unifier.core.api.myaccount.GetUserDigestResponse;
import com.unifier.core.api.myaccount.MarkReferrerNotificationReadRequest;
import com.unifier.core.api.myaccount.MarkReferrerNotificationReadResponse;
import com.unifier.core.api.myaccount.ResendInviteRequest;
import com.unifier.core.api.myaccount.ResendInviteResponse;
import com.unifier.services.users.IUsersService;
import com.unifier.web.security.UniwareUser;
import com.unifier.web.utils.WebContextUtils;
import com.uniware.core.api.validation.SendOTPRequest;
import com.uniware.core.api.validation.SendOTPResponse;
import com.uniware.core.api.validation.VerifyOTPRequest;
import com.uniware.core.api.validation.VerifyOTPResponse;
import com.uniware.core.utils.UserContext;
import com.uniware.services.accounts.IAccountService;
import com.uniware.web.aspect.RefreshCurrentUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Controller
@Path("/data/myaccount/")
public class MyAccountController {

    public static final String CHANGE_PASSWORD_PAGE = "myaccount/changePassword";

    @Autowired
    private IUsersService      userService;

    @Autowired
    private IAccountService    accountService;

    @RequestMapping("/myaccount/changePassword")
    public String editPassword(ModelMap modelMap) {
        return CHANGE_PASSWORD_PAGE;
    }

    @RequestMapping("/myaccount/myInfo")
    public String myInfo(ModelMap modelMap) {
        return "myaccount/myInfo";
    }

    @RequestMapping("/verifyemail")
    public String verifyEmailToken(ModelMap modelMap) {
        return "index";
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("sendOTP")
    @POST
    public SendOTPResponse sendOTP(SendOTPRequest request) {
        return userService.sendOTP(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("verifyOTP")
    @POST
    @RefreshCurrentUser
    public VerifyOTPResponse verifyOTP(VerifyOTPRequest request) {
        return userService.verifyOTP(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("getUserDigestFlags")
    @POST
    public GetUserDigestResponse getUserDigestFlags(GetUserDigestRequest request) {
        request.setUsername(WebContextUtils.getCurrentUser().getUsername());
        request.setTenantCode(UserContext.current().getTenant().getCode());
        return userService.getUserDigestFlags(request);

    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("detail/markReferrerNotification")
    @POST
    @RefreshCurrentUser
    public MarkReferrerNotificationReadResponse markReferrerNotification(MarkReferrerNotificationReadRequest request) {
        request.setUsername(WebContextUtils.getCurrentUser().getUsername());
        return userService.markReferrerNotificationRead(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("digestDetail/update")
    @POST
    @RefreshCurrentUser
    public EditDigestDetailResponse editDigestDetail(EditDigestDetailRequest request) {
        UniwareUser currentUser = WebContextUtils.getCurrentUser();
        request.setUsername(currentUser.getUsername());
        return userService.editDigestDetail(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("userDetail/update")
    @POST
    @RefreshCurrentUser
    public EditUserDetailResponse editUserDetail(EditUserDetailRequest request) {
        UniwareUser currentUser = WebContextUtils.getCurrentUser();
        request.setUsername(currentUser.getUsername());
        return userService.editUserDetail(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("balance/get")
    @POST
    public GetAccountBalanceResponse getAccountBalance(GetAccountBalanceRequest request) {
        return accountService.getAccountBalance(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("paymentHistory/get")
    @POST
    public GetPaymentHistoryResponse getPaymentHistory(GetPaymentHistoryRequest request) {
        return accountService.getPaymentHistory(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("usageHistory/get")
    @POST
    public GetUsageHistoryResponse getUsageHistory(GetUsageHistoryRequest request) {
        return accountService.getUsageHistory(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("usageHistory/monthly/get")
    @POST
    public GetMonthlyUsageHistoryResponse getMonthlyUsageHistory(GetMonthlyUsageHistoryRequest request) {
        return accountService.getMonthlyUsageHistory(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("alerts/get")
    @POST
    public GetAccountAlertsResponse getAccountAlerts(GetAccountAlertsRequest request) {
        return accountService.getAccountAlerts(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("usageStatistics/get")
    @POST
    public GetRecentUsageStatisticsResponse getRecentUsageStatistics(GetRecentUsageStatisticsRequest request) {
        return accountService.getRecentUsageStatistics(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("saleOrder/create")
    @POST
    public CreatePaymentSaleOrderResponse createSaleOrder(CreatePaymentSaleOrderRequest request) {
        return accountService.createSaleOrder(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("invite/create")
    @POST
    public CreateInviteResponse createInvite(CreateInviteRequest request) {
        return accountService.createInvite(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("invites/get")
    @POST
    public GetInvitesByReferrerResponse getInvites(GetInvitesByReferrerRequest request) {
        return accountService.getAllInvitesByReferrer(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("invite/resend")
    @POST
    public ResendInviteResponse resendInvite(ResendInviteRequest request) {
        return accountService.resendInvite(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("visitedUrl/add")
    @POST
    @RefreshCurrentUser
    public AddUrlVisitedByUserResponse addUrlVisitedByUser(AddUrlVisitedByUserRequest request) {
        request.setUsername(WebContextUtils.getCurrentUser().getUsername());
        return userService.addUrlVisitedByUser(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("applicableProducts/get")
    @POST
    public GetProductApplicabilityResponse getProductApplicability(GetProductApplicabilityRequest request) {
        return accountService.getProductApplicability(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("subscriptions/get")
    @POST
    public GetSubscriptionsResponse getSubscriptions(GetSubscriptionsRequest request) {
        return accountService.getSubscriptions(request);
    }
}
