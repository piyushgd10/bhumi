/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 03-Apr-2014
 *  @author harsh
 */
package com.unifier.web.controller;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.uniware.core.api.channel.FetchPendencyRequest;
import com.uniware.core.api.channel.FetchPendencyResponse;
import com.uniware.core.api.channel.GetChannelInventorySyncStatusRequest;
import com.uniware.core.api.channel.GetChannelInventorySyncStatusResponse;
import com.uniware.core.api.channel.GetChannelInventoryUpdateSnapshotRequest;
import com.uniware.core.api.channel.GetChannelInventoryUpdateSnapshotResponse;
import com.uniware.core.api.channel.SyncChannelItemTypeInventoryRequest;
import com.uniware.core.api.channel.SyncChannelItemTypeInventoryResponse;
import com.uniware.core.api.channel.UpdateInventoryByChannelProductIdRequest;
import com.uniware.core.api.channel.UpdateInventoryByChannelProductIdResponse;
import com.uniware.core.api.channel.UpdatePendencyRequest;
import com.uniware.core.api.channel.UpdatePendencyResponse;
import com.uniware.core.api.inventory.GetInventorySyncRequest;
import com.uniware.core.api.inventory.GetInventorySyncResponse;
import com.uniware.core.api.inventory.UpdateInventoryOnAllChannelsRequest;
import com.uniware.core.api.inventory.UpdateInventoryOnAllChannelsResponse;
import com.uniware.core.api.inventory.UpdateInventoryOnChannelRequest;
import com.uniware.core.api.inventory.UpdateInventoryOnChannelResponse;
import com.uniware.services.channel.IChannelInventorySyncService;
import com.uniware.services.channel.saleorder.IChannelPendencySyncService;
import com.uniware.services.inventory.IInventoryService;

@Controller
@Path("/data/channel/")
public class ChannelInventoryResource {

    @Autowired
    private IChannelInventorySyncService channelInventorySyncService;

    @Autowired
    private IChannelPendencySyncService  channelPendencySyncService;
    
    @Autowired
    private IInventoryService            inventoryService;

    @Produces(MediaType.APPLICATION_JSON)
    @Path("fetchPendency")
    @POST
    public FetchPendencyResponse fetchPendency(FetchPendencyRequest request) {
        return channelPendencySyncService.fetchPendency(request);
    }
    
    @Produces(MediaType.APPLICATION_JSON)
    @Path("inventorySyncStatus")
    @POST
    public GetInventorySyncResponse getInventorySyncStatuses(GetInventorySyncRequest request) {
        return inventoryService.getInventorySync(request);
    }
    
    @Produces(MediaType.APPLICATION_JSON)
    @Path("inventory/syncAll")
    @POST
    public UpdateInventoryOnAllChannelsResponse updateInventoryOnAllChannels(UpdateInventoryOnAllChannelsRequest request) {
        return channelInventorySyncService.updateInventoryOnAllChannels(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("updatePendency")
    @POST
    public UpdatePendencyResponse updatePendency(UpdatePendencyRequest request) {
        return channelPendencySyncService.updatePendency(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("updateAllInventory")
    @POST
    public UpdateInventoryOnChannelResponse updateAllInventoryOnChannel(UpdateInventoryOnChannelRequest request) {
        return channelInventorySyncService.updateAllInventoryOnChannel(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("updatePendingInventory")
    @POST
    public UpdateInventoryOnChannelResponse updatePendingInventoryOnChannel(UpdateInventoryOnChannelRequest request) {
        return channelInventorySyncService.updateInventoryOnChannel(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("getChannelInventorySyncStatus")
    @POST
    public GetChannelInventorySyncStatusResponse getChannelInventorySyncStatus(GetChannelInventorySyncStatusRequest request) {
        return channelInventorySyncService.getChannelInventorySyncStatus(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("getChannelInventoryUpdateSnapshot")
    @POST
    public GetChannelInventoryUpdateSnapshotResponse getChannelInventoryUpdateSnapshotResponse(GetChannelInventoryUpdateSnapshotRequest request) {
        return channelInventorySyncService.getInventoryUpdateSnapshotById(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("syncChannelItemTypeInventory")
    @POST
    public SyncChannelItemTypeInventoryResponse syncChannelItemTypeInventory(SyncChannelItemTypeInventoryRequest request) {
        return channelInventorySyncService.syncChannelItemTypeInventory(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("updateInventory/channelProductId")
    @POST
    public UpdateInventoryByChannelProductIdResponse updateInventoryByChannelProductId(UpdateInventoryByChannelProductIdRequest request) {
        return channelInventorySyncService.updateInventoryByChannelProductId(request);
    }
    
}
