/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 18-Jul-2014
 *  @author parijat
 */
package com.unifier.web.controller;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.unifier.core.api.audit.FetchEntityActivityRequest;
import com.unifier.core.api.audit.FetchEntityActivityResponse;
import com.uniware.services.audit.IActivityLogService;

/**
 * Resource for entity activity data
 * 
 * @author parijat
 */
@Controller
@Path("/data/audit/")
public class ActivityResource {

    @Autowired
    private IActivityLogService activityLogService;

    @Produces(MediaType.APPLICATION_JSON)
    @Path("entity/get")
    @POST
    public FetchEntityActivityResponse getAuditForIdentifier(FetchEntityActivityRequest auditRequest) {
        return activityLogService.getActivityLogsbyEntityId(auditRequest);
    }

}
