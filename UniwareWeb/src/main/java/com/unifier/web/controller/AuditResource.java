/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 30-May-2014
 *  @author parijat
 */
package com.unifier.web.controller;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.unifier.core.api.audit.FetchEntityAuditRequest;
import com.unifier.core.api.audit.FetchEntityAuditResponse;
import com.unifier.core.api.audit.GetEntitiesAuditLogsRequest;
import com.unifier.core.api.audit.GetEntitiesAuditLogsResponse;
import com.uniware.services.audit.IAuditService;

/**
 * Resource for entity and methods audit data
 * 
 * @author parijat
 */
@Controller
@Path("/data/audit/")
public class AuditResource {

    @Autowired
    private IAuditService auditService;

    @Produces(MediaType.APPLICATION_JSON)
    @Path("entity/get")
    @POST
    public FetchEntityAuditResponse getAuditForIdentifier(FetchEntityAuditRequest auditRequest) {
        return auditService.getEntityAuditLog(auditRequest);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("entity/getAll")
    @POST
    public GetEntitiesAuditLogsResponse getAllAuditLogs(GetEntitiesAuditLogsRequest logsRequest) {
        return auditService.getEntitiesAuditLog(logsRequest);
    }
}
