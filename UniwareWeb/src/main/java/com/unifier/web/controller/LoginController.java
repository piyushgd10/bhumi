/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 21-Dec-2011
 *  @author ankit
 */
package com.unifier.web.controller;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.unifier.core.api.login.LoginAsUserRequest;
import com.unifier.core.api.login.LoginAsUserResponse;
import com.unifier.core.api.login.LogoutAsUserResponse;
import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.entity.User;
import com.unifier.services.users.IUsersService;
import com.unifier.web.security.UniwareUser;
import com.unifier.web.utils.WebContextUtils;
import com.uniware.core.api.validation.WsResponseCode;

@Controller
@Path("/data/login/")
public class LoginController {

    @Autowired
    private IUsersService usersService;

    @Produces(MediaType.APPLICATION_JSON)
    @Path("loginAsUser")
    @POST
    public LoginAsUserResponse loginAsUser(LoginAsUserRequest request) {
        LoginAsUserResponse response = new LoginAsUserResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            UniwareUser user = WebContextUtils.getCurrentUser();
            User loginUser = usersService.getUserByUsername(request.getUserName());
            if (loginUser != null && !loginUser.isHidden() && !user.getUsername().equals(loginUser.getUsername())) {
                user.loginAsUser(loginUser);
                response.setSuccessful(true);
            } else {
                context.addError(WsResponseCode.INVALID_USER_ID, "Username Not Valid");
                response.setSuccessful(false);
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Path("logoutAsUser")
    @GET
    public LogoutAsUserResponse exitLoginAsUser() {
        LogoutAsUserResponse response = new LogoutAsUserResponse();
        UniwareUser user = WebContextUtils.getCurrentUser();
        user.exitLoginAsUser();
        response.setSuccessful(true);
        return response;
    }
}