/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 10, 2012
 *  @author praveeng
 */
package com.unifier.web.controller.admin;

import java.net.HttpURLConnection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;

import com.uniware.services.dashboard.IDashboardWidgetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.unifier.core.api.base.ServiceResponse;
import com.unifier.core.api.reports.AddWidgetRequest;
import com.unifier.core.api.reports.AddWidgetResponse;
import com.unifier.core.api.reports.GetUserWidgetsRequest;
import com.unifier.core.api.reports.GetUserWidgetsResponse;
import com.unifier.core.api.reports.GetWidgetDataRequest;
import com.unifier.core.api.reports.RemoveWidgetRequest;
import com.unifier.core.api.reports.RemoveWidgetResponse;
import com.unifier.core.api.reports.ReorderWidgetsRequest;
import com.unifier.core.api.reports.ReorderWidgetsResponse;
import com.unifier.core.api.reports.WidgetDTO;
import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.entity.DashboardWidget;
import com.unifier.core.utils.StringUtils;
import com.unifier.scraper.sl.exception.ScriptCompilationException;
import com.unifier.scraper.sl.exception.ScriptExecutionException;
import com.unifier.services.report.IReportService;
import com.unifier.web.security.UniwareUser;
import com.unifier.web.utils.WebContextUtils;
import com.uniware.services.configuration.DashboardWidgetConfiguration;

@Controller
@Path("/data/admin/")
public class DashboardResource {

    @Autowired
    private IReportService          reportService;
    @Autowired
    private IDashboardWidgetService dashboardWidgetService;

    @Produces(MediaType.APPLICATION_JSON)
    @Path("widget/get")
    @POST
    public String getChart(GetWidgetDataRequest request) throws ScriptExecutionException, ScriptCompilationException {
        DashboardWidgetConfiguration configuration = ConfigurationManager.getInstance().getConfiguration(DashboardWidgetConfiguration.class);
        DashboardWidget dashboardWidget = configuration.getDashboardWidgetByCode(request.getWidgetCode());
        UniwareUser uniwareUser = WebContextUtils.getCurrentUser();
        if (uniwareUser.hasResource(dashboardWidget.getAccessResourceName())) {
            return dashboardWidgetService.getDashboardWidgetData(request, dashboardWidget);
        } else {
            throw new WebApplicationException(HttpURLConnection.HTTP_FORBIDDEN);
        }
    }

    @RequestMapping("/data/admin/widget/get/{code}")
    @ResponseBody
    public String getChart(@PathVariable("code") String code, HttpServletRequest httpServletRequest) throws ScriptExecutionException, ScriptCompilationException {
        DashboardWidgetConfiguration configuration = ConfigurationManager.getInstance().getConfiguration(DashboardWidgetConfiguration.class);
        DashboardWidget dashboardWidget = configuration.getDashboardWidgetByCode(code);
        UniwareUser uniwareUser = WebContextUtils.getCurrentUser();
        if (uniwareUser.hasResource(dashboardWidget.getAccessResourceName())) {
            GetWidgetDataRequest request = new GetWidgetDataRequest();
            request.setWidgetCode(code);
            request.getRequestMap().putAll(WebContextUtils.constructRequestParamMap(httpServletRequest));
            // additional cache parameter which should be used for caching the dashboard
            request.setAdditionalCacheParam(httpServletRequest.getParameter("acpm"));
            return dashboardWidgetService.getDashboardWidgetData(request, dashboardWidget);
        } else {
            throw new WebApplicationException(HttpURLConnection.HTTP_FORBIDDEN);
        }
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/widgets/getAll")
    @GET
    public List<WidgetDTO> getAllDashboardWidgets() {
        return ConfigurationManager.getInstance().getConfiguration(DashboardWidgetConfiguration.class).getAllDashboardWidgets();
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("groups/widget/get")
    @GET
    public Map<String, List<WidgetDTO>> getWidgetGroups() {
        return ConfigurationManager.getInstance().getConfiguration(DashboardWidgetConfiguration.class).getWidgetsGroupByWidgetGroup();
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("widgets/user/get")
    @GET
    public List<WidgetDTO> getUserWidgets() {
        Integer userId = WebContextUtils.getCurrentUser().getUser().getId();
        List<WidgetDTO> widgets = reportService.getUserWidgets(userId);
        for (Iterator<WidgetDTO> iterator = widgets.iterator(); iterator.hasNext();) {
            WidgetDTO widgetDTO = iterator.next();
            if (widgetDTO.getWidgetGroup().endsWith("_NEW")) {
                iterator.remove();
            }
        }
        return widgets;
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("widgets/user/get")
    @POST
    public GetUserWidgetsResponse getUserWidgetsByGroup(GetUserWidgetsRequest request) {
        GetUserWidgetsResponse response = new GetUserWidgetsResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            List<WidgetDTO> widgetDTOs = reportService.getUserWidgets(WebContextUtils.getCurrentUser().getUser().getId());
            for (Iterator<WidgetDTO> iterator = widgetDTOs.iterator(); iterator.hasNext();) {
                WidgetDTO widgetDTO = iterator.next();
                if (!StringUtils.isBlank(widgetDTO.getWidgetGroup()) && !widgetDTO.getWidgetGroup().equalsIgnoreCase(request.getWidgetGroup())) {
                    iterator.remove();
                }
            }
            response.setWidgetDTOs(widgetDTOs);
            response.setSuccessful(true);
        } else {
            response.setSuccessful(false);
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("widget/reorder")
    @POST
    public ReorderWidgetsResponse reorderWidgets(ReorderWidgetsRequest request) {
        request.setUserId(WebContextUtils.getCurrentUser().getUser().getId());
        return reportService.reorderWidgets(request);
    }

    @Path("widget/add")
    @POST
    public AddWidgetResponse addWidget(AddWidgetRequest request) {
        request.setUserId(WebContextUtils.getCurrentUser().getUser().getId());
        return reportService.addWidget(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("widget/remove")
    @POST
    public RemoveWidgetResponse removeWidget(RemoveWidgetRequest request) {
        request.setUserId(WebContextUtils.getCurrentUser().getUser().getId());
        return reportService.removeWidget(request);
    }

    public static void main(String[] args) {
        ServiceResponse response = new ServiceResponse();
        String s = new Gson().toJson(response);
        response.setMessage(s);
        System.out.println(new Gson().toJson(response));
    }
}
