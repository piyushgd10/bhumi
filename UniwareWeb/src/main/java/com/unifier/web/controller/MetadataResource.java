/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 19-Mar-2012
 *  @author vibhu
 */
package com.unifier.web.controller;

import static com.uniware.core.cache.EnvironmentPropertiesCache.TRACE_LOGGING_SERVER_URL;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.unifier.core.api.advanced.GetAllRolesWithDetailResponse;
import com.unifier.core.api.advanced.RoleDTO;
import com.unifier.core.api.customfields.CustomFieldMetadataDTO;
import com.unifier.core.api.customfields.ListCustomFieldsRequest;
import com.unifier.core.api.customfields.ListCustomFieldsVOResponse;
import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.entity.Role;
import com.unifier.core.entity.User;
import com.unifier.core.transport.http.HttpSender;
import com.unifier.core.ui.SelectItem;
import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.JsonUtils;
import com.unifier.core.utils.StringUtils;
import com.unifier.services.release.IReleaseUpdateService;
import com.unifier.services.tenantprofile.mao.ITenantProfileMao;
import com.unifier.services.users.IUsersService;
import com.unifier.services.vo.ShippingProviderVO;
import com.unifier.services.vo.TenantProfileVO;
import com.unifier.web.security.UniwareUser;
import com.unifier.web.utils.WebContextUtils;
import com.uniware.core.api.channel.ChannelDetailDTO;
import com.uniware.core.api.common.GenericServiceResponse;
import com.uniware.core.api.facility.GetAllFacilityDetailsResponse;
import com.uniware.core.api.facility.GetCurrentUserFacilitiesResponse;
import com.uniware.core.api.facility.GetUserFacilitiesResponse;
import com.uniware.core.api.returns.ReshipmentActionsDTO;
import com.uniware.core.api.tenant.RefreshTenantSetupStateRequest;
import com.uniware.core.api.tenant.RefreshTenantSetupStateResponse;
import com.uniware.core.api.tenant.StartTraceLogSessionResponse;
import com.uniware.core.api.warehouse.FacilityDTO;
import com.uniware.core.cache.EnvironmentPropertiesCache;
import com.uniware.core.cache.FacilityCache;
import com.uniware.core.cache.LocationCache;
import com.uniware.core.cache.RolesCache;
import com.uniware.core.entity.Facility;
import com.uniware.core.entity.Feature;
import com.uniware.core.entity.ReshipmentAction;
import com.uniware.core.entity.ShippingProvider.ShippingServiceablity;
import com.uniware.core.entity.Source;
import com.uniware.core.utils.UserContext;
import com.uniware.core.vo.PrintTemplateVO;
import com.uniware.core.vo.UserProfileVO;
import com.uniware.services.cache.ChannelCache;
import com.uniware.services.cache.PrintTemplateCache;
import com.uniware.services.cache.ReturnsCache;
import com.uniware.services.cache.SequenceCache;
import com.uniware.services.cache.StatusCache;
import com.uniware.services.cache.UICustomizationsCache;
import com.uniware.services.configuration.CurrencyConfiguration;
import com.uniware.services.configuration.CustomFieldsMetadataConfiguration;
import com.uniware.services.configuration.CustomFieldsMetadataConfiguration.CustomFieldMetadataVO;
import com.uniware.services.configuration.ShippingConfiguration;
import com.uniware.services.configuration.ShippingFacilityLevelConfiguration;
import com.uniware.services.configuration.ShippingSourceConfiguration;
import com.uniware.services.configuration.SystemConfiguration;
import com.uniware.services.configuration.TenantSourceConfiguration;
import com.uniware.services.configuration.TenantSystemConfiguration;
import com.uniware.services.configuration.data.manager.ProductConfiguration;
import com.uniware.services.tenant.ITenantSetupService;
import com.uniware.services.warehouse.IFacilityService;
import com.uniware.web.metadata.CustomChannelMetadataDTO;
import com.uniware.web.metadata.UserMetadataDTO;
import com.uniware.web.metadata.UserMetadataDTO.FeatureDTO;
import com.uniware.web.metadata.UserMetadataDTO.TenantDTO;
import com.uniware.web.metadata.UserMetadataDTO.UserDTO;

@RestController
@RequestMapping(value = "/data")
public class MetadataResource {

    private static final Logger   LOG = LoggerFactory.getLogger(MetadataResource.class);

    @Autowired
    private IFacilityService      facilityService;

    @Autowired
    private ITenantSetupService   tenantSetupService;

    @Autowired
    private ITenantProfileMao     tenantProfileMao;

    @Autowired
    private IReleaseUpdateService releaseService;

    @Autowired
    private IUsersService         usersService;

    @RequestMapping("/meta")
    public UserMetadataDTO getUserMetadata() {
        UserMetadataDTO metadata = new UserMetadataDTO();
        TenantDTO tenant = new TenantDTO(UserContext.current().getTenant());
        TenantProfileVO tenantProfile = tenantProfileMao.getTenantByCode(UserContext.current().getTenant().getCode());
        tenant.setPaymentStatus(tenantProfile.getPaymentStatus().name());
        tenant.setPaymentStatusAcknowledgeDate(tenantProfile.getPayementStatusAcknowledgeDate());
        tenant.setBaseCurrency(ConfigurationManager.getInstance().getConfiguration(TenantSystemConfiguration.class).getBaseCurrency());
        metadata.setTenant(tenant);
        UniwareUser uniwareUser = WebContextUtils.getCurrentUser();
        Integer facilityId = uniwareUser.getCurrentFacilityId();
        if (facilityId != null) {
            Facility facility = CacheManager.getInstance().getCache(FacilityCache.class).getFacilityById(facilityId);
            com.uniware.web.metadata.UserMetadataDTO.FacilityDTO facilityDTO = new com.uniware.web.metadata.UserMetadataDTO.FacilityDTO(facility);
            metadata.setFacility(facilityDTO);
        }
        User u = uniwareUser.getUser();
        UserProfileVO userProfile = usersService.getUserProfileByUsername(u.getUsername());
        Integer lastVersionRead = userProfile.getLastReleaseUpdateVersionRead();
        boolean updatesAvailable = lastVersionRead.compareTo(releaseService.getLatestReleaseUpdateVersion().getMajorVersion()) < 0;
        UserDTO user = new UserDTO(u.getName(), u.getUsername(), u.getEmail(), u.getUserEmail(), u.getMobile(), u.isVendor(), uniwareUser.hasRole(Role.Code.DROPSHIPPER.name()),
                u.getCreated(), userProfile.isVerified(), u.isMobileVerified(), updatesAvailable, userProfile.getVisitedUrls());
        Date referrerBroadcastDate = DateUtils.stringToDate(CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class).getReferrerBroadcastDate(),
                "yyyy-MM-dd'T'HH:mm:ss");
        if (DateUtils.getCurrentDate().after(referrerBroadcastDate)) {
            user.setReferrerNotificationReadTime(userProfile.getReferrerNotificationReadTime());
            user.setReferrerNotificationRead(userProfile.isReferrerNotificationRead());
        } else {
            user.setReferrerNotificationRead(true);
        }
        RolesCache rolesCache = CacheManager.getInstance().getCache(RolesCache.class);
        Set<String> userAccessResources = new HashSet<String>();
        Set<String> userAccessPatterns = new HashSet<String>();
        for (String role : uniwareUser.getRoles()) {
            Set<String> accessResources = rolesCache.getAccessResourcesByRole(role.toUpperCase());
            if (accessResources != null) {
                userAccessResources.addAll(accessResources);
            }
            Set<String> accessPatterns = rolesCache.getAccessPatternsByRole(role.toUpperCase());
            if (accessPatterns != null) {
                userAccessPatterns.addAll(accessPatterns);
            }
        }
        user.setAccessResources(userAccessResources);
        user.setAccessPatterns(userAccessPatterns);
        metadata.setUser(user);
        metadata.setLayoutConfiguration(uniwareUser.getAccessibleLayout());
        if (uniwareUser.getPrimaryUser() != null) {
            User pu = uniwareUser.getPrimaryUser();
            UserProfileVO pUserProfile = usersService.getUserProfileByUsername(pu.getUsername());
            Integer primaryUserLastVersionRead = pUserProfile.getLastReleaseUpdateVersionRead();
            boolean unreadUpdates = primaryUserLastVersionRead.compareTo(releaseService.getLatestReleaseUpdateVersion().getMajorVersion()) < 0;
            UserDTO primaryUser = new UserDTO(pu.getName(), pu.getUsername(), pu.getEmail(), pu.getUserEmail(), pu.getMobile(), pu.isVendor(),
                    uniwareUser.hasRole(Role.Code.DROPSHIPPER.name()), u.getCreated(), pUserProfile.isVerified(), pu.isMobileVerified(), unreadUpdates,
                    pUserProfile.getVisitedUrls());
            Set<String> primaryUserAccessResources = new HashSet<String>();
            Set<String> primaryUserAccessPatterns = new HashSet<String>();
            for (String role : uniwareUser.getRoles()) {
                Set<String> accessResources = rolesCache.getAccessResourcesByRole(role.toUpperCase());
                if (accessResources != null) {
                    primaryUserAccessResources.addAll(accessResources);
                }
                Set<String> accessPatterns = rolesCache.getAccessPatternsByRole(role.toUpperCase());
                if (accessPatterns != null) {
                    primaryUserAccessPatterns.addAll(accessPatterns);
                }
            }
            primaryUser.setAccessResources(primaryUserAccessResources);
            primaryUser.setAccessPatterns(primaryUserAccessPatterns);
            metadata.setPrimaryUser(primaryUser);
        }
        List<Feature> features = ConfigurationManager.getInstance().getConfiguration(ProductConfiguration.class).getAllFeatures();
        List<FeatureDTO> availableFeatures = new ArrayList<>();
        if (features != null) {
            for (Feature feature : features) {
                availableFeatures.add(new FeatureDTO(feature.getCode(), feature.getName()));
            }
        }
        metadata.setFeatures(availableFeatures);
        metadata.setSetupStatus(tenantProfile.getSetupStatus().name());
        boolean invoiceConfigured = true;
        PrintTemplateVO invoiceTemplate = CacheManager.getInstance().getCache(PrintTemplateCache.class).getPrintTemplateByName(PrintTemplateVO.Type.INVOICE.name());
        if (invoiceTemplate != null && !invoiceTemplate.isUserCustomized()) {
            invoiceConfigured = false;
        }
        metadata.setInvoiceConfigured(invoiceConfigured);
        // unidesk won't show up for hidden users.
        if (!u.isHidden()) {
            metadata.setUnideskUrl(CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class).getUnideskUrl());
        }
        return metadata;
    }

    @RequestMapping("/layout")
    public String getLayout() {
        UniwareUser uniwareUser = WebContextUtils.getCurrentUser();
        return uniwareUser.getAccessibleLayoutJson();
    }

    @RequestMapping("/userFacilities")
    public GetUserFacilitiesResponse getUserFacilities() {
        GetUserFacilitiesResponse response = new GetUserFacilitiesResponse();
        UniwareUser uniwareUser = WebContextUtils.getCurrentUser();
        List<FacilityDTO> userFacilities = facilityService.getUserFacilities(uniwareUser.getAccessibleFacilities());
        FacilityCache facilityCache = CacheManager.getInstance().getCache(FacilityCache.class);
        response.setFacilities(userFacilities);
        response.setAccessibleAllFacilities(uniwareUser.isAccessibleAllFacilities());
        if (uniwareUser.getCurrentFacilityId() != null) {
            Facility currentFacility = facilityCache.getFacilityById(uniwareUser.getCurrentFacilityId());
            if (currentFacility != null) {
                response.setCurrentFacilityCode(currentFacility.getCode());
            }
        }
        response.setSuccessful(true);
        return response;
    }

    @RequestMapping("/user/facilities")
    public GetCurrentUserFacilitiesResponse getFacilitiesForCurrentUser() {
        GetCurrentUserFacilitiesResponse response = new GetCurrentUserFacilitiesResponse();
        UniwareUser uniwareUser = WebContextUtils.getCurrentUser();
        response.setAccessibleAllFacilities(uniwareUser.isAccessibleAllFacilities());
        List<Facility> userFacilities = facilityService.getFacilitiesById(uniwareUser.getAccessibleFacilities());
        for (Facility facility : userFacilities) {
            response.getFacilityDTOList().add(new GetCurrentUserFacilitiesResponse.FacilityDTO(facility));
            if (uniwareUser.getCurrentFacilityId() != null && uniwareUser.getCurrentFacilityId().equals(facility.getId())) {
                response.setCurrentFacilityCode(facility.getCode());
            }
        }
        response.setSuccessful(true);
        return response;
    }

    @RequestMapping("/meta/holidays")
    public String getHolidays() {
        return JsonUtils.objectToString(facilityService.getAllAnnualHolidays());
    }

    @RequestMapping("/meta/states")
    public String getStates() {
        return CacheManager.getInstance().getCache(LocationCache.class).getStateJson();
    }

    @RequestMapping("/meta/countries")
    public String getCountries() {
        return CacheManager.getInstance().getCache(LocationCache.class).getCountriesJson();
    }

    @RequestMapping("/meta/facilities")
    public String getFacilities() {
        return CacheManager.getInstance().getCache(FacilityCache.class).getFacilitiesJson();
    }

    @RequestMapping("/meta/allFacilities")
    public GetAllFacilityDetailsResponse getFacilityDetails() {
        return facilityService.getAllFacilityDetails();
    }

    @RequestMapping("/meta/systemConfiguration/{names:.+}")
    public GenericServiceResponse getSystemConfigurationByName(@PathVariable("names") String names) {
        GenericServiceResponse response = new GenericServiceResponse();
        Map<String, String> systemConfigurations = new HashMap<String, String>();
        for (String name : StringUtils.split(names)) {
            systemConfigurations.put(name, ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).getProperty(name));
        }
        response.setPayload(systemConfigurations);
        response.setSuccessful(true);
        return response;
    }

    @RequestMapping("/meta/uiCustomOptions/{name:.+}")
    public List<SelectItem> getUICustomListOptions(@PathVariable("name") String name) {
        List<SelectItem> customListOptions = CacheManager.getInstance().getCache(UICustomizationsCache.class).getUICustomListOptions(name);
        if (customListOptions == null) {
            customListOptions = new ArrayList<SelectItem>();
        }
        return customListOptions;
    }

    @RequestMapping("/meta/sequencePrefix/{name}")
    public String getSequencePrefixByName(@PathVariable("name") String name) {
        return CacheManager.getInstance().getCache(SequenceCache.class).getPrefixByName(name);
    }

    @RequestMapping("/meta/customFieldConfig")
    public ListCustomFieldsVOResponse getCustomFieldConfigByType(@RequestBody ListCustomFieldsRequest request) {
        ListCustomFieldsVOResponse response = new ListCustomFieldsVOResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            List<CustomFieldMetadataDTO> customFieldMetadataDTOs = new ArrayList<CustomFieldMetadataDTO>();
            List<CustomFieldMetadataVO> customFieldMetadataVOs = ConfigurationManager.getInstance().getConfiguration(
                    CustomFieldsMetadataConfiguration.class).getCustomFieldsByEntity(request.getEntity());
            if (customFieldMetadataVOs != null) {
                for (CustomFieldMetadataVO vo : customFieldMetadataVOs) {
                    customFieldMetadataDTOs.add(
                            new CustomFieldMetadataDTO(vo.getFieldName(), vo.getDefaultValue(), vo.getValueType(), vo.getDisplayName(), vo.isRequired(), vo.getPossibleValues()));
                }
            }
            response.setCustomFieldMetadataDTOs(customFieldMetadataDTOs);
            response.setSuccessful(true);
        } else {
            response.setSuccessful(false);
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @RequestMapping("/meta/inflowReceiptStatuses")
    public List<String> getInflowReceiptStatuses() {
        return CacheManager.getInstance().getCache(StatusCache.class).getInflowReceiptStatuses();
    }

    @RequestMapping("/meta/returnManifestStatuses")
    public List<String> getReturnManifestStatuses() {
        return CacheManager.getInstance().getCache(StatusCache.class).getReturnManifestStatuses();
    }

    @RequestMapping("/meta/reshipmentActions")
    public List<ReshipmentAction> getReshipmentActions() {
        return CacheManager.getInstance().getCache(ReturnsCache.class).getReshipmentActions();
    }

    @RequestMapping("/meta/sources")
    public String getSources() {
        return new Gson().toJson(ConfigurationManager.getInstance().getConfiguration(TenantSourceConfiguration.class).getAllowedSources());
    }

    @RequestMapping("/meta/disallowedSources")
    public String getDisallowedSources() {
        return new Gson().toJson(ConfigurationManager.getInstance().getConfiguration(TenantSourceConfiguration.class).getDisallowedSources());
    }

    @RequestMapping("/meta/shippingSources")
    public String getShippingSources() {
        return ConfigurationManager.getInstance().getConfiguration(ShippingSourceConfiguration.class).getShippingSourcesJson();
    }

    @RequestMapping("/meta/shippingServiceabilities")
    public String getShippingServiceabilities() {
        Map<String, String> nameToCodeMap = new HashMap<>();
        for (ShippingServiceablity serviceablity : ShippingServiceablity.values()) {
            nameToCodeMap.put(serviceablity.name(), serviceablity.getName());
        }
        return new Gson().toJson(nameToCodeMap);
    }

    @RequestMapping("/meta/customChannels")
    public List<CustomChannelMetadataDTO> getChannels() {
        List<CustomChannelMetadataDTO> customChannels = new ArrayList<>();
        for (ChannelDetailDTO channel : CacheManager.getInstance().getCache(ChannelCache.class).getChannels()) {
            if (Source.Code.CUSTOM.name().equals(channel.getSource().getCode()) && channel.isEnabled()) {
                customChannels.add(new CustomChannelMetadataDTO(channel));
            }
        }
        return customChannels;
    }

    @RequestMapping("/meta/globalShippingProviders")
    public List<ShippingProviderVO> getGlobelShippingProviders() {
        return ConfigurationManager.getInstance().getConfiguration(ShippingConfiguration.class).getAllGlobalShippingProviders();
    }

    @RequestMapping("/meta/currencies")
    public String getCurrencies() {
        return ConfigurationManager.getInstance().getConfiguration(CurrencyConfiguration.class).getCurrenciesJson();
    }

    @RequestMapping("/meta/paymentMethods")
    public String getPaymentMethods() {
        return ConfigurationManager.getInstance().getConfiguration(ShippingConfiguration.class).getPaymentMethodJson();
    }

    @RequestMapping("/meta/shippingProvidersByShippingMethodCode/{methodCode}")
    public String getShippingProvidersByShippingMethod(@PathVariable("methodCode") String methodCode) {
        return new Gson().toJson(
                ConfigurationManager.getInstance().getConfiguration(ShippingFacilityLevelConfiguration.class).getShippingProvidersByShippingMethodCode(methodCode));
    }

    @RequestMapping("/meta/shippingPackageTypes")
    public String getShippingPackageTypes() {
        return ConfigurationManager.getInstance().getConfiguration(ShippingFacilityLevelConfiguration.class).getShippingPackageTypesJson();
    }

    @RequestMapping("/meta/providerToMethodToShippingProviderMethodType")
    public String getProviderToMethodToShippingProviderMethodType() {
        return ConfigurationManager.getInstance().getConfiguration(ShippingFacilityLevelConfiguration.class).getProviderToMethodToShippingProviderMethodTypeJson();
    }

    @RequestMapping("/meta/shippingMethods")
    public String getShippingMethods() {
        return ConfigurationManager.getInstance().getConfiguration(ShippingConfiguration.class).getShippingMethodsJson();
    }

    @RequestMapping("/meta/shipmentTrackingStatuses")
    public String getShipmentTrackingStatuses() {
        return ConfigurationManager.getInstance().getConfiguration(ShippingConfiguration.class).getShipmentTrackingStatusJson();
    }

    @RequestMapping("/meta/startSession/{logFileName:.+}")
    public StartTraceLogSessionResponse startSession(@PathVariable("logFileName") String logFileName) {
        StartTraceLogSessionResponse startTraceLogSessionResponse = new StartTraceLogSessionResponse();
        String loggingServerUrl = null;
        try {
            EnvironmentPropertiesCache cache = CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class);
            loggingServerUrl = cache.getProperty(TRACE_LOGGING_SERVER_URL) + "/" + UserContext.current().getTenant().getCode() + "/create";

            final HttpSender sender = new HttpSender();
            Map<String, String> logRequestParams = new HashMap<>();
            logRequestParams.put("logRoutingKey", logFileName);

            String response = sender.executePost(loggingServerUrl, logRequestParams, null);
            JsonObject traceLogServerResponseJson = ((JsonObject) JsonUtils.stringToJson(response));

            if (!traceLogServerResponseJson.get("successful").getAsBoolean()) {
                LOG.error(response);
                return startTraceLogSessionResponse;
            }
            else {
                loggingServerUrl = traceLogServerResponseJson.get("logRedirectUrl").getAsString();
            }
        } catch (Exception e) {
            LOG.error("Error while creating logs, {}", e);
            return startTraceLogSessionResponse;
        }
        startTraceLogSessionResponse.setSuccessful(true);
        startTraceLogSessionResponse.setLoggingServerUrl(loggingServerUrl);
        return startTraceLogSessionResponse;
    }

    @RequestMapping("/meta/refreshState")
    public RefreshTenantSetupStateResponse refreshTenantSetupState(@RequestBody RefreshTenantSetupStateRequest request) {
        request.setCode(UserContext.current().getTenant().getCode());
        return tenantSetupService.refreshSetupState(request);
    }

    @RequestMapping("/meta/roles")
    public GetAllRolesWithDetailResponse getAllRolesWithDetail() {
        GetAllRolesWithDetailResponse response = new GetAllRolesWithDetailResponse();
        UniwareUser uniwareUser = WebContextUtils.getCurrentUser();
        List<RoleDTO> roleDTOs = new ArrayList<RoleDTO>();
        boolean addTenantRoles = true;
        if (uniwareUser.getAccessibleFacilities().size() < 2) {
            addTenantRoles = false;
        }
        for (RoleDTO roleDTO : CacheManager.getInstance().getCache(RolesCache.class).getRoleDTOs()) {
            if (roleDTO.getLevel().equals("FACILITY")) {
                roleDTOs.add(roleDTO);
            } else if (addTenantRoles) {
                roleDTOs.add(roleDTO);
            }
        }
        response.setRoleDTOs(roleDTOs);
        response.setSuccessful(true);
        return response;
    }

    @RequestMapping("/meta/allReshipmentActions")
    public List<ReshipmentActionsDTO> getAllReshipmentActions() {
        List<ReshipmentActionsDTO> reshipmentActionsDTOs = new ArrayList<ReshipmentActionsDTO>();
        List<SelectItem> customListOptions = CacheManager.getInstance().getCache(UICustomizationsCache.class).getUICustomListOptions("returnsAwaitingAction:actionCode");
        if (customListOptions == null) {
            List<ReshipmentAction> reshipmentActions = CacheManager.getInstance().getCache(ReturnsCache.class).getReshipmentActions();
            for (ReshipmentAction reshipmentAction : reshipmentActions) {
                ReshipmentActionsDTO reshipmentActionsDTO = new ReshipmentActionsDTO();
                reshipmentActionsDTO.setCode(reshipmentAction.getCode());
                reshipmentActionsDTO.setDescription(reshipmentAction.getDescription());
                reshipmentActionsDTOs.add(reshipmentActionsDTO);
            }
        } else {
            for (SelectItem selectItem : customListOptions) {
                ReshipmentActionsDTO reshipmentActionsDTO = new ReshipmentActionsDTO();
                reshipmentActionsDTO.setCode(selectItem.getValue());
                reshipmentActionsDTO.setDescription(selectItem.getName());
                reshipmentActionsDTOs.add(reshipmentActionsDTO);
            }
        }
        return reshipmentActionsDTOs;
    }

}
