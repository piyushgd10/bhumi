/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 12-May-2015
 *  @author parijat
 */
package com.unifier.web.controller;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.uniware.core.api.location.GetPincodesWithinRangeRequest;
import com.uniware.core.api.location.GetPincodesWithinRangeResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.uniware.services.location.ILocationService;

@Controller
@Path("/data/location/")
public class LocationResource {

    @Autowired
    private ILocationService locationService;

    @Produces(MediaType.APPLICATION_JSON)
    @Path("proximityLocations")
    @POST
    public GetPincodesWithinRangeResponse getProximityByDistance(GetPincodesWithinRangeRequest request) {
        return locationService.getAllPincodesWithinRange(request);
    }
}
