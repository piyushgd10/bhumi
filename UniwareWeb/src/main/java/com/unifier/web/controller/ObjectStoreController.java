/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jan 21, 2012
 *  @author singla
 */
package com.unifier.web.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.unifier.core.api.base.ServiceResponse;
import com.unifier.core.api.validation.WsError;
import com.unifier.core.entity.BinaryObject;
import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.FileUtils;
import com.unifier.services.binary.IBinaryObjectService;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.utils.Constants;

/**
 * @author singla
 */
@Controller
@RequestMapping("/object-store/")
public class ObjectStoreController {

    @Autowired
    private IBinaryObjectService binaryObjectService;

    @RequestMapping("files")
    public String files() {
        return "files";
    }

    @RequestMapping("upload")
    @ResponseBody
    public UploadFileResponse uploadObject(@RequestParam("file") MultipartFile file) {
        UploadFileResponse response = new UploadFileResponse();
        if (file.getSize() > Constants.LOGO_ALLOWED_SIZE) {
            List<WsError> errors = new ArrayList<WsError>();
            errors.add(new WsError(WsResponseCode.FILE_EXCEEDS_MAX_ALLOWED_SIZE.code(), "File exceeds max allowed size of 300Kb."));
            response.setErrors(errors);
            response.setSuccessful(false);
            return response;
        }
        try {
            BinaryObject binaryObject = new BinaryObject();
            binaryObject.setContentType(file.getContentType());
            binaryObject.setContent(FileUtils.toByteArray(file.getInputStream()));
            binaryObject.setSize(file.getSize());
            binaryObject.setName(file.getOriginalFilename());
            binaryObject.setCreated(DateUtils.getCurrentTime());
            binaryObject = binaryObjectService.addBinaryObject(binaryObject);
            response.setBinaryObjectId(binaryObject.getId());
            response.setFileURL(Constants.OBJECT_STORE_VIEW_PREPEND + response.getBinaryObjectId());
            response.setSuccessful(true);
        } catch (IOException e) {
            List<WsError> errors = new ArrayList<WsError>();
            errors.add(new WsError(1, "UPLOAD_FAILED", e.getMessage()));
            response.setErrors(errors);
            response.setSuccessful(false);
        }
        return response;
    }

    @RequestMapping("view/{objectId}")
    public void viewObject(@PathVariable("objectId") Integer binaryObjectId, HttpServletResponse response) throws IOException {
        BinaryObject binaryObject = binaryObjectService.getBinaryObjectById(binaryObjectId);
        if (binaryObject != null) {
            response.setContentLength((int) binaryObject.getSize());
            response.setContentType(binaryObject.getContentType());
            response.setHeader("Cache-Control", "public, max-age=31536000");
            FileUtils.write(binaryObject.getContent(), response.getOutputStream());
        }
    }

    public static class UploadFileResponse extends ServiceResponse {

        /**
         * 
         */
        private static final long serialVersionUID = 6753508684230374144L;

        private String              fileURL;

        private Integer             binaryObjectId;

        public String getFileURL() {
            return fileURL;
        }

        public void setFileURL(String fileURL) {
            this.fileURL = fileURL;
        }

        /**
         * @return the binaryObjectId
         */
        public Integer getBinaryObjectId() {
            return binaryObjectId;
        }

        /**
         * @param binaryObjectId the binaryObjectId to set
         */
        public void setBinaryObjectId(Integer binaryObjectId) {
            this.binaryObjectId = binaryObjectId;
        }
    }
}
