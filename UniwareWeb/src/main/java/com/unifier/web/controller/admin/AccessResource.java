/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 22-May-2012
 *  @author vibhu
 */
package com.unifier.web.controller.admin;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.unifier.core.api.advanced.CreateRoleRequest;
import com.unifier.core.api.advanced.CreateRoleResponse;
import com.unifier.core.api.advanced.EditRoleResourceMappingRequest;
import com.unifier.core.api.advanced.EditRoleResourceMappingResponse;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.utils.StringUtils;
import com.unifier.services.admin.access.IAccessControlService;
import com.unifier.web.security.UniwareUser;
import com.unifier.web.utils.WebContextUtils;
import com.uniware.core.cache.RolesCache;

@Controller
@Path("/data/admin/super/")
public class AccessResource {

    @Autowired
    IAccessControlService accessService;

    @Produces(MediaType.APPLICATION_JSON)
    @Path("role/create")
    @POST
    public CreateRoleResponse createUser(CreateRoleRequest request) {
        request.setCode(StringUtils.underscorify(request.getRoleName()));
        CreateRoleResponse response = accessService.createRole(request);
        if (response.isSuccessful()) {
            reloadUser();
        }
        return response;
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("role/edit")
    @POST
    public EditRoleResourceMappingResponse editRoleResourceMapping(EditRoleResourceMappingRequest request) {
        EditRoleResourceMappingResponse response = accessService.editRoleResourceMapping(request);
        if (response.isSuccessful()) {
            reloadUser();
        }
        return response;
    }

    private void reloadUser() {
        CacheManager.getInstance().markCacheDirty(RolesCache.class);
        UniwareUser user = WebContextUtils.getCurrentUser();
        user.loadUser(user.getUser());
    }

}