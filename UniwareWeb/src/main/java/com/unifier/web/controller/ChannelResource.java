/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIALUse is subject to license terms.
 *  
 *  @version     1.0, Dec 10, 2011
 *  @author singla
 */
package com.unifier.web.controller;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.uniware.core.api.channel.EditChannelAssociatedFacilityRequest;
import com.uniware.core.api.channel.EditChannelAssociatedFacilityResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.uniware.core.api.channel.AddChannelConnectorRequest;
import com.uniware.core.api.channel.AddChannelConnectorResponse;
import com.uniware.core.api.channel.AddChannelRequest;
import com.uniware.core.api.channel.AddChannelResponse;
import com.uniware.core.api.channel.ChangeChannelInventorySyncStatusRequest;
import com.uniware.core.api.channel.ChangeChannelInventorySyncStatusResponse;
import com.uniware.core.api.channel.ChangeChannelOrderSyncStatusRequest;
import com.uniware.core.api.channel.ChangeChannelOrderSyncStatusResponse;
import com.uniware.core.api.channel.ChangeChannelPricingSyncStatusRequest;
import com.uniware.core.api.channel.ChangeChannelPricingSyncStatusResponse;
import com.uniware.core.api.channel.ChangeChannelReconciliationSyncStatusRequest;
import com.uniware.core.api.channel.ChangeChannelReconciliationSyncStatusResponse;
import com.uniware.core.api.channel.CreateChannelItemTypeRequest;
import com.uniware.core.api.channel.CreateChannelItemTypeResponse;
import com.uniware.core.api.channel.DeleteChannelItemTypeRequest;
import com.uniware.core.api.channel.DeleteChannelItemTypeResponse;
import com.uniware.core.api.channel.DisableChannelItemTypeRequest;
import com.uniware.core.api.channel.DisableChannelItemTypeResponse;
import com.uniware.core.api.channel.EditChannelRequest;
import com.uniware.core.api.channel.EditChannelResponse;
import com.uniware.core.api.channel.ForceSyncSaleOrderStatusRequest;
import com.uniware.core.api.channel.ForceSyncSaleOrderStatusResponse;
import com.uniware.core.api.channel.GetChannelActivitiesRequest;
import com.uniware.core.api.channel.GetChannelActivitiesResponse;
import com.uniware.core.api.channel.GetChannelCatalogSyncStatusRequest;
import com.uniware.core.api.channel.GetChannelCatalogSyncStatusResponse;
import com.uniware.core.api.channel.GetChannelDetailsRequest;
import com.uniware.core.api.channel.GetChannelDetailsResponse;
import com.uniware.core.api.channel.GetChannelOrderSummaryRequest;
import com.uniware.core.api.channel.GetChannelOrderSummaryResponse;
import com.uniware.core.api.channel.GetChannelOrderSyncStatusRequest;
import com.uniware.core.api.channel.GetChannelOrderSyncStatusResponse;
import com.uniware.core.api.channel.GetChannelProductSummaryRequest;
import com.uniware.core.api.channel.GetChannelProductSummaryResponse;
import com.uniware.core.api.channel.GetChannelSaleOrderStatusRequest;
import com.uniware.core.api.channel.GetChannelSaleOrderStatusResponse;
import com.uniware.core.api.channel.GetChannelSyncRequest;
import com.uniware.core.api.channel.GetChannelSyncResponse;
import com.uniware.core.api.channel.GetChannelsForSaleOrderImportRequest;
import com.uniware.core.api.channel.GetChannelsForSaleOrderImportResponse;
import com.uniware.core.api.channel.GoToChannelLandingPageRequest;
import com.uniware.core.api.channel.GoToChannelLandingPageResponse;
import com.uniware.core.api.channel.LinkChannelItemTypeRequest;
import com.uniware.core.api.channel.LinkChannelItemTypeResponse;
import com.uniware.core.api.channel.PreConfigureChannelConnectorRequest;
import com.uniware.core.api.channel.PreConfigureChannelConnectorResponse;
import com.uniware.core.api.channel.ReenableInventorySyncRequest;
import com.uniware.core.api.channel.ReenableInventorySyncResponse;
import com.uniware.core.api.channel.RelistChannelItemTypeRequest;
import com.uniware.core.api.channel.RelistChannelItemTypeResponse;
import com.uniware.core.api.channel.SyncChannelCatalogRequest;
import com.uniware.core.api.channel.SyncChannelCatalogResponse;
import com.uniware.core.api.channel.SyncChannelOrdersRequest;
import com.uniware.core.api.channel.SyncChannelOrdersResponse;
import com.uniware.core.api.channel.SyncChannelReconciliationInvoiceRequest;
import com.uniware.core.api.channel.SyncChannelReconciliationInvoiceResponse;
import com.uniware.core.api.channel.TestChannelInventoryUpdateFormulaRequest;
import com.uniware.core.api.channel.TestChannelInventoryUpdateFormulaResponse;
import com.uniware.core.api.channel.UnlinkChannelItemTypeRequest;
import com.uniware.core.api.channel.UnlinkChannelItemTypeResponse;
import com.uniware.core.api.channel.UpdateChannelItemTypeTaxRequest;
import com.uniware.core.api.channel.UpdateChannelItemTypeTaxResponse;
import com.uniware.core.api.prices.GetChannelPricingSyncStatusRequest;
import com.uniware.core.api.prices.GetChannelPricingSyncStatusResponse;
import com.uniware.core.api.prices.PushAllPricesOnChannelRequest;
import com.uniware.core.api.prices.PushAllPricesOnChannelResponse;
import com.uniware.core.api.saleorder.GetOrderSyncRequest;
import com.uniware.core.api.saleorder.GetOrderSyncResponse;
import com.uniware.core.entity.ChannelItemType;
import com.uniware.services.channel.IChannelCatalogService;
import com.uniware.services.channel.IChannelCatalogSyncService;
import com.uniware.services.channel.IChannelReconciliationInvoiceSyncService;
import com.uniware.services.channel.IChannelService;
import com.uniware.services.channel.saleorder.IChannelOrderSyncService;
import com.uniware.services.channel.saleorder.status.IChannelOrderStatusSyncService;
import com.uniware.services.pricing.IPricingFacade;
import com.uniware.services.saleorder.ISaleOrderService;

@Controller
@Path("/data/channel/")
public class ChannelResource {

    @Autowired
    private IChannelService channelService;

    @Autowired
    private IChannelCatalogService channelCatalogService;

    @Autowired
    private IChannelOrderSyncService channelOrderSyncService;

    @Autowired
    private IChannelCatalogSyncService channelCatalogSyncService;

    @Autowired
    private IChannelReconciliationInvoiceSyncService channelReconciliationInvoiceSyncService;

    @Autowired
    private IChannelOrderStatusSyncService channelOrderStatusSyncService;

    @Autowired
    private ISaleOrderService saleOrderService;

    @Autowired
    private IPricingFacade pricingService;

    @Produces(MediaType.APPLICATION_JSON)
    @Path("getChannels")
    @POST
    public GetChannelSyncResponse getChannelSync(GetChannelSyncRequest request) {
        return channelService.getChannelSync(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("orderSyncStatus")
    @POST
    public GetOrderSyncResponse getOrderSyncStatuses(GetOrderSyncRequest request) {
        return saleOrderService.getOrderSyncStatuses(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("getChannelOrderSyncStatus")
    @POST
    public GetChannelOrderSyncStatusResponse getChannelOrderSyncStatus(GetChannelOrderSyncStatusRequest request) {
        return channelOrderSyncService.getChannelOrderSyncStatus(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("getChannelDetails")
    @POST
    public GetChannelDetailsResponse getChannelDetailsByChannelCode(GetChannelDetailsRequest request) {
        return channelService.getChannelDetailsByChannelCode(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("getChannelProductSummary")
    @POST
    public GetChannelProductSummaryResponse getChannelProductSummaryByChannelCode(GetChannelProductSummaryRequest request) {
        return channelService.getChannelProductSummaryByChannelCode(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("getChannelOrderSummary")
    @POST
    public GetChannelOrderSummaryResponse getChannelOrderSummaryByChannelCode(GetChannelOrderSummaryRequest request) {
        return channelService.getChannelOrderSummaryByChannelCode(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("goToChannelLandingPage")
    @POST
    public GoToChannelLandingPageResponse goToChannelLandingPage(GoToChannelLandingPageRequest request) {
        return channelService.goToChannelLandingPage(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("preConfigureChannel")
    @POST
    public PreConfigureChannelConnectorResponse preConfigureChannel(PreConfigureChannelConnectorRequest request) {
        return channelService.preConfigureChannelConnector(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("syncChannelOrders")
    @POST
    public SyncChannelOrdersResponse syncChannelOrders(SyncChannelOrdersRequest request) {
        return channelOrderSyncService.syncChannelOrders(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("addChannel")
    @POST
    public AddChannelResponse addChannel(AddChannelRequest request) {
        return channelService.addChannel(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("addChannelConnector")
    @POST
    public AddChannelConnectorResponse addChannelConnector(AddChannelConnectorRequest request) {
        return channelService.addChannelConnector(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("editChannel")
    @POST
    public EditChannelResponse editChannel(EditChannelRequest request) {
        return channelService.editChannel(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("changeOrderSyncStatus")
    @POST
    public ChangeChannelOrderSyncStatusResponse changeOrderSyncStatus(ChangeChannelOrderSyncStatusRequest request) {
        return channelService.changeOrderSyncStatus(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("changeInventorySyncStatus")
    @POST
    public ChangeChannelInventorySyncStatusResponse changeInventorySyncStatus(ChangeChannelInventorySyncStatusRequest request) {
        return channelService.changeInventorySyncStatus(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("changeReconciliationSyncStatus")
    @POST
    public ChangeChannelReconciliationSyncStatusResponse changeReconciliationSyncStatus(ChangeChannelReconciliationSyncStatusRequest request) {
        return channelService.changeReconciliationSyncStatus(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("changePricingSyncStatus")
    @POST
    public ChangeChannelPricingSyncStatusResponse changePricingSyncStatus(ChangeChannelPricingSyncStatusRequest request) {
        return channelService.changeChannelPricingSyncStatus(request);

    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("linkChannelItemType")
    @POST
    public LinkChannelItemTypeResponse linkChannelItemType(LinkChannelItemTypeRequest request) {
        return channelCatalogService.linkChannelItemType(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("unlinkChannelItemType")
    @POST
    public UnlinkChannelItemTypeResponse unlinkChannelItemType(UnlinkChannelItemTypeRequest request) {
        return channelCatalogService.unlinkChannelItemType(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("deleteChannelItemType")
    @POST
    public DeleteChannelItemTypeResponse deleteChannelItemType(DeleteChannelItemTypeRequest request) {
        return channelCatalogService.deleteChannelItemType(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("relistChannelItemType")
    @POST
    public RelistChannelItemTypeResponse relistChannelItemType(RelistChannelItemTypeRequest request) {
        return channelCatalogService.relistChannelItemType(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("disableChannelItemType")
    @POST
    public DisableChannelItemTypeResponse disableChannelItemType(DisableChannelItemTypeRequest request) {
        return channelCatalogService.disableChannelItemType(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("editChannelItemType")
    @POST
    public CreateChannelItemTypeResponse createChannelItemType(CreateChannelItemTypeRequest request) {
        request.setSyncId(ChannelItemType.SyncedBy.MANUAL.toString());
        return channelCatalogService.createChannelItemType(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("getChannelsForSaleOrderImport")
    @POST
    public GetChannelsForSaleOrderImportResponse getChannelsForSaleOrderImport(GetChannelsForSaleOrderImportRequest request) {
        return channelService.getChannelsForOrderImport(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("syncChannelCatalog")
    @POST
    public SyncChannelCatalogResponse syncChannelCatalog(SyncChannelCatalogRequest request) {
        return channelCatalogSyncService.syncChannelCatalog(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("syncReconciliationInvoice")
    @POST
    public SyncChannelReconciliationInvoiceResponse syncChannelReconciliationInvoice(SyncChannelReconciliationInvoiceRequest request) {
        return channelReconciliationInvoiceSyncService.syncChannelReconciliationInvoices(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("reenableInventorySync")
    @POST
    public ReenableInventorySyncResponse reenableInventorySync(ReenableInventorySyncRequest request) {
        return channelCatalogService.reenableInventorySync(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("getChannelCatalogSyncStatus")
    @POST
    public GetChannelCatalogSyncStatusResponse getChannelCatalogSyncStatus(GetChannelCatalogSyncStatusRequest request) {
        return channelCatalogSyncService.getChannelCatalogSyncStatus(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("syncChannelSaleOrderStatus")
    @POST
    public ForceSyncSaleOrderStatusResponse forceSyncSaleOrderStatus(ForceSyncSaleOrderStatusRequest request) {
        return channelOrderStatusSyncService.forceSyncSaleOrderStatus(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("getChannelSaleOrderStatus")
    @POST
    public GetChannelSaleOrderStatusResponse getChannelSaleOrderStatus(GetChannelSaleOrderStatusRequest request) {
        return channelOrderStatusSyncService.getChannelSaleOrderStatus(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("testAndSaveChannelInventoryUpdateFormula")
    @POST
    public TestChannelInventoryUpdateFormulaResponse testAndSaveChannelInventoryUpdateFormula(TestChannelInventoryUpdateFormulaRequest request) {
        return channelService.testChannelInventoryUpdateFormula(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("updateChannelItemTypeTaxes")
    @POST
    public UpdateChannelItemTypeTaxResponse updateChannelItemTypeTaxes(UpdateChannelItemTypeTaxRequest request) {
        return channelCatalogService.updateChannelItemTypeTaxes(request);
    }
    
    @Produces(MediaType.APPLICATION_JSON)
    @Path("fetchActivities")
    @POST
    public GetChannelActivitiesResponse getChannelActivities(GetChannelActivitiesRequest request) {
        return channelService.getChannelActivities(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("getPriceSyncStatus")
    @POST
    public GetChannelPricingSyncStatusResponse getChannelPricingSyncStatusResponse(GetChannelPricingSyncStatusRequest request) {
        return pricingService.getChannelPricingSyncStatus(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("pushPricesOnChannel")
    @POST
    public PushAllPricesOnChannelResponse pushAllPricesOnChannel(PushAllPricesOnChannelRequest request) {
        return pricingService.pushAllPricesOnChannel(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("editassociatedfacility")
    @POST
    public EditChannelAssociatedFacilityResponse editChannelAssociatedFacility(EditChannelAssociatedFacilityRequest request){
        return channelService.editChannelAssociatedFacility(request);
    }
}