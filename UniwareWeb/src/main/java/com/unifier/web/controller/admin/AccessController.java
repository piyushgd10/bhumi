/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 21-May-2012
 *  @author vibhu
 */
package com.unifier.web.controller.admin;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.google.gson.Gson;
import com.unifier.core.api.advanced.RoleDTO;
import com.unifier.core.entity.Role;
import com.unifier.services.admin.access.IAccessControlService;
import com.unifier.web.utils.WebContextUtils;

@Controller
public class AccessController {

    @Autowired
    IAccessControlService accessService;

    @RequestMapping("/admin/super/roleToAccessResource")
    public String roleToAccessResource(ModelMap modelMap) {
        Map<String, RoleDTO> rolesMap = new HashMap<String, RoleDTO>();
        for (RoleDTO roleDTO : accessService.getRoleDTOs()) {
            if (!roleDTO.isHidden()) {
                rolesMap.put(roleDTO.getCode(), roleDTO);
            }
        }
        rolesMap.remove(Role.Code.SUPER.name());
        modelMap.addAttribute("roles", new Gson().toJson(rolesMap));
        modelMap.addAttribute("accessResources", new Gson().toJson(accessService.getAccessibleAccessResourceDTOs(WebContextUtils.getCurrentUser().getUser().getId())));
        return "admin/super/roleToAccessResource";
    }
}