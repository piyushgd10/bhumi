/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIALUse is subject to license terms.
 *  
 *  @version     1.0, Dec 10, 2011
 *  @author singla
 */
package com.unifier.web.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.unifier.core.utils.JsonUtils;
import com.unifier.web.utils.WebContextUtils;
import com.uniware.core.api.channel.PostConfigureChannelConnectorRequest;
import com.uniware.core.api.channel.PostConfigureChannelConnectorResponse;
import com.uniware.services.channel.IChannelService;

@Controller
public class ChannelController {

    @Autowired
    private IChannelService channelService;

    @RequestMapping("/data/channel/postConfigureChannel")
    public String postConfigureChannel(ModelMap modelMap) {
        PostConfigureChannelConnectorRequest request = new PostConfigureChannelConnectorRequest();
        Map<String, String> requestParams = WebContextUtils.constructRequestParamMap(WebContextUtils.getRequest());
        if (!requestParams.isEmpty()) {
            request.setRequestParams(requestParams);
            PostConfigureChannelConnectorResponse response = channelService.postConfigureChannelConnector(request);
            if (response.isSuccessful()) {
                modelMap.addAttribute("response", JsonUtils.objectToString(response));
            }
        }
        return "channel/sources";
    }

    @RequestMapping("/data/channel/postConfigureChannel/{channelCode}/{connector}")
    public String postConfigureChannel(@PathVariable("channelCode") String channelCode, @PathVariable("connector") String connector, ModelMap modelMap) {
        PostConfigureChannelConnectorRequest request = new PostConfigureChannelConnectorRequest();
        Map<String, String> requestParams = WebContextUtils.constructRequestParamMap(WebContextUtils.getRequest());
        if (!requestParams.isEmpty()) {
            requestParams.put("channelCode", channelCode);
            requestParams.put("connector", connector);
            request.setRequestParams(requestParams);
            PostConfigureChannelConnectorResponse response = channelService.postConfigureChannelConnector(request);
            if (response.isSuccessful()) {
                modelMap.addAttribute("response", JsonUtils.objectToString(response));
            }
        }
        return "channel/sources";
    }


    @RequestMapping("/channel/channelInventoryUpdates")
    public String searchChannelInventory() {
        return "/channel/channelInventoryUpdates";
    }

    @RequestMapping("/channel/sourceShippingProvider")
    public String searchSourceShippingProvider() {
        return "/channel/sourceShippingProvider";
    }

    @RequestMapping("/channel/pendency")
    public String pendency() {
        return "/channel/pendency";
    }

    @RequestMapping("/channel/unmappedSku")
    public String unmappedSku() {
        return "/channel/unmappedSku";
    }

    @RequestMapping("/channel/sources")
    public String sources() {
        return "channel/sources";
    }

    @RequestMapping("/channel/productMapping")
    public String productMapping() {
        return "/channel/productMapping";
    }

    @RequestMapping("/channel/priceMaster/get")
    public String getPricingInfo() {
        return "/channel/priceMaster";
    }

    @RequestMapping("/channel/channelPaymentReconciliation")
    public String reconciliationOrders() {
        return "/channel/channelPaymentReconciliation";
    }

}