/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 05-Mar-2014
 *  @author parijat
 */
package com.unifier.web.controller;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.unifier.core.api.user.UpdateUserLastReadTimeRequest;
import com.unifier.core.api.user.UpdateUserLastReadTimeResponse;
import com.unifier.services.user.notification.IUserNotificationService;
import com.unifier.services.users.IUsersService;
import com.unifier.web.utils.WebContextUtils;
import com.uniware.core.api.usernotification.DeleteAllNotificationsRequest;
import com.uniware.core.api.usernotification.DeleteAllNotificationsResponse;
import com.uniware.core.api.usernotification.DeleteNotificationRequest;
import com.uniware.core.api.usernotification.DeleteNotificationResponse;
import com.uniware.core.api.usernotification.GetUnreadNotificationCountRequest;
import com.uniware.core.api.usernotification.GetUnreadNotificationCountResponse;
import com.uniware.core.api.usernotification.GetUserNotificationsRequest;
import com.uniware.core.api.usernotification.GetUserNotificationsRespone;

@Controller
@Path("/data/notifications/")
public class NotificationsController {

    @Autowired
    private IUserNotificationService userNotificationService;

    @Autowired
    private IUsersService            usersService;

    @Produces(MediaType.APPLICATION_JSON)
    @Path("messages/get")
    @POST
    public GetUserNotificationsRespone getUserNotification(GetUserNotificationsRequest request) {
        request.setNotificationTypes(WebContextUtils.getCurrentUser().getNotificationTypes());
        return userNotificationService.getUserNotifications(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("messages/delete")
    @POST
    public DeleteNotificationResponse deleteNotification(DeleteNotificationRequest request) {
        return userNotificationService.removeNotification(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("messages/count")
    @POST
    public GetUnreadNotificationCountResponse getUnreadNotificationCount(GetUnreadNotificationCountRequest request) {
        request.setUsername(WebContextUtils.getCurrentUser().getUsername());
        return userNotificationService.getUnreadMessageCount(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("messages/deleteAll")
    @POST
    public DeleteAllNotificationsResponse deleteAllNotifications(DeleteAllNotificationsRequest request) {
        return userNotificationService.removeAllNotifications(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("update/lastReadTime")
    @POST
    public UpdateUserLastReadTimeResponse updateUserLastReadTime(UpdateUserLastReadTimeRequest request) {
        request.setUserId(WebContextUtils.getCurrentUser().getUser().getId());
        return usersService.updateUserNotificationReadTime(request);
    }

}
