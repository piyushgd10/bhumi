/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Oct 13, 2015
 *  @author akshay
 */
package com.unifier.web.controller.admin;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.uniware.core.api.recommendation.ApproveRecommendationRequest;
import com.uniware.core.api.recommendation.ApproveRecommendationResponse;
import com.uniware.core.api.recommendation.CancelRecommendationRequest;
import com.uniware.core.api.recommendation.CancelRecommendationResponse;
import com.uniware.core.api.recommendation.CreateRecommendationRequest;
import com.uniware.core.api.recommendation.CreateRecommendationResponse;
import com.uniware.core.api.recommendation.GetRecommendationProvidersRequest;
import com.uniware.core.api.recommendation.GetRecommendationProvidersResponse;
import com.uniware.core.api.recommendation.GetRecommendationTypesRequest;
import com.uniware.core.api.recommendation.GetRecommendationTypesResponse;
import com.uniware.core.api.recommendation.GetRecommendationsRequest;
import com.uniware.core.api.recommendation.GetRecommendationsResponse;
import com.uniware.core.api.recommendation.GetSubscribedRecommendationProvidersRequest;
import com.uniware.core.api.recommendation.GetSubscribedRecommendationProvidersResponse;
import com.uniware.core.api.recommendation.ListRecommendationsRequest;
import com.uniware.core.api.recommendation.RejectRecommendationRequest;
import com.uniware.core.api.recommendation.RejectRecommendationResponse;
import com.uniware.core.api.recommendation.SubscribeRecommendationsRequest;
import com.uniware.core.api.recommendation.SubscriptionRecommendationsResponse;
import com.uniware.core.api.recommendation.UnsubscribeRecommendationRequest;
import com.uniware.core.api.recommendation.UnsubscribeRecommendationResponse;
import com.uniware.services.recommendation.IRecommendationManagementService;
import com.uniware.services.recommendation.IRecommendationSubscriptionService;

@Controller
@Path("/data/")
public class RecommendationResource {

    @Autowired
    private IRecommendationManagementService   recommendationManagementService;

    @Autowired
    private IRecommendationSubscriptionService recommendationSubscriptionService;

    @Produces(MediaType.APPLICATION_JSON)
    @Path("recommendation/cancel")
    @POST
    public CancelRecommendationResponse cancelRecommendation(CancelRecommendationRequest request) {
        return recommendationManagementService.cancelRecommendation(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("recommendation/create")
    @POST
    public CreateRecommendationResponse createRecommendation(CreateRecommendationRequest request) {
        return recommendationManagementService.createRecommendation(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("recommendation/approve")
    @POST
    public ApproveRecommendationResponse approveOrRejectRecommendation(ApproveRecommendationRequest request) {
        return recommendationManagementService.approveRecommendation(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("recommendation/reject")
    @POST
    public RejectRecommendationResponse rejectRecommendation(RejectRecommendationRequest request) {
        return recommendationManagementService.rejectRecommendation(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("recommendation/get")
    @POST
    public GetRecommendationsResponse getRecommendations(GetRecommendationsRequest request) {
        return recommendationManagementService.getRecommendations(request);
    }
    
    @Produces(MediaType.APPLICATION_JSON)
    @Path("recommendations/list")
    @POST
    public GetRecommendationsResponse listRecommendations(ListRecommendationsRequest request) {
        return recommendationManagementService.listRecommendations(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("recommendation/types/get")
    @POST
    public GetRecommendationTypesResponse getRecommendationTypes(GetRecommendationTypesRequest request) {
        return recommendationSubscriptionService.getRecommendationTypes(request);
    }
    
    @Produces(MediaType.APPLICATION_JSON)
    @Path("recommendation/providers/get")
    @POST
    public GetRecommendationProvidersResponse getRecommendationProviders(GetRecommendationProvidersRequest request) {
        return recommendationSubscriptionService.getRecommendationProviders(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("recommendation/subscriptions/get")
    @POST
    public GetSubscribedRecommendationProvidersResponse getSubscribedRecommendationProviders(GetSubscribedRecommendationProvidersRequest request) {
        return recommendationSubscriptionService.getSubscribedRecommendationProviders(request);
    }
    
    @Produces(MediaType.APPLICATION_JSON)
    @Path("recommendation/subscription/add")
    @POST
    public SubscriptionRecommendationsResponse addSubscription(SubscribeRecommendationsRequest request) {
        return recommendationSubscriptionService.addSubscription(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("recommendation/subscription/remove")
    @POST
    public UnsubscribeRecommendationResponse removeSubscription(UnsubscribeRecommendationRequest request) {
        return recommendationSubscriptionService.removeSubscription(request);
    }
    
}
