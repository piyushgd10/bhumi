/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Oct 13, 2015
 *  @author akshay
 */
package com.unifier.web.controller;

import com.uniware.core.api.prices.CheckChannelItemTypePriceRequest;
import com.uniware.core.api.prices.CheckChannelItemTypePriceResponse;
import com.uniware.core.api.prices.EditChannelItemTypePriceRequest;
import com.uniware.core.api.prices.EditChannelItemTypePriceResponse;
import com.uniware.core.api.prices.GetChannelItemTypePriceRequest;
import com.uniware.core.api.prices.GetChannelItemTypePriceResponse;
import com.uniware.core.api.prices.GetChannelItemTypePricesRequest;
import com.uniware.core.api.prices.GetChannelItemTypePricesResponse;
import com.uniware.core.api.prices.GetHistoricalChannelItemTypePriceRequest;
import com.uniware.core.api.prices.GetUIPricePropertiesByChannelRequest;
import com.uniware.core.api.prices.GetUIPricePropertiesByChannelResponse;
import com.uniware.core.api.prices.PullChannelItemTypePriceRequest;
import com.uniware.core.api.prices.PullChannelItemTypePriceResponse;
import com.uniware.core.api.prices.PushChannelItemTypePriceRequest;
import com.uniware.core.api.prices.PushChannelItemTypePriceResponse;
import com.uniware.services.pricing.IPricingFacade;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Controller
@Path("/data/")
public class PricingController {

    @Autowired
    private IPricingFacade pricing;

    @Produces(MediaType.APPLICATION_JSON)
    @Path("channel/prices/fetch")
    @POST
    public GetChannelItemTypePriceResponse getChannelItemTypePrice(final GetChannelItemTypePriceRequest request) {
        return pricing.getChannelItemTypePrice(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("channel/prices/fetchHistorical")
    @POST
    public GetChannelItemTypePriceResponse getHistoricalChannelItemTypePrice(final GetHistoricalChannelItemTypePriceRequest request) {
        return pricing.getHistoricalChannelItemTypePrice(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("channel/prices/edit")
    @POST
    public EditChannelItemTypePriceResponse editChannelItemTypePrice(final EditChannelItemTypePriceRequest request) {
        return pricing.editChannelItemType(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("channel/prices/localEdit")
    @POST
    public EditChannelItemTypePriceResponse localEditChannelItemTypePrice(final EditChannelItemTypePriceRequest request) {
        return pricing.localEditChannelItemType(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("channel/prices/check")
    @POST
    public CheckChannelItemTypePriceResponse checkChannelItemTypePrice(final CheckChannelItemTypePriceRequest request) {
        return pricing.checkChannelItemTypePrice(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("channel/prices/push")
    @POST
    public PushChannelItemTypePriceResponse pushPricesToChannel(final PushChannelItemTypePriceRequest request) {
        return pricing.pushPricesToChannel(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("channel/prices/pull")
    @POST
    public PullChannelItemTypePriceResponse pullPricesFromChannel(final PullChannelItemTypePriceRequest request) {
        return pricing.pullPricesFromChannel(request);
    }
    
    @Produces(MediaType.APPLICATION_JSON)
    @Path("channel/prices/get")
    @POST
    public GetChannelItemTypePricesResponse getChannelItemTypePrices(final GetChannelItemTypePricesRequest request) {
        return pricing.getChannelItemTypePrices(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("channel/priceProperties")
    @POST
    public GetUIPricePropertiesByChannelResponse getUIPricePropertiesByChannel(final GetUIPricePropertiesByChannelRequest request) {
        return pricing.getUIPricePropertiesByChannel(request);
    }

}