/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 *  @version     1.0, 14-Apr-2012
 *  @author vibhu
 */
package com.unifier.web.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.unifier.core.api.email.EditEmailTemplateRequest;
import com.unifier.core.api.email.EditEmailTemplateResponse;
import com.unifier.core.api.email.EmailTemplateDTO;
import com.unifier.core.api.export.CloneExportJobTypeRequest;
import com.unifier.core.api.export.CloneExportJobTypeResponse;
import com.unifier.core.api.export.EditExportJobTypeRequest;
import com.unifier.core.api.export.EditExportJobTypeResponse;
import com.unifier.core.api.export.ExportJobTypeDTO;
import com.unifier.core.api.external.GetUnideskTokenRequest;
import com.unifier.core.api.external.GetUnideskTokenResponse;
import com.unifier.core.api.imports.EditImportJobTypeRequest;
import com.unifier.core.api.imports.EditImportJobTypeResponse;
import com.unifier.core.api.imports.ImportJobTypeDTO;
import com.unifier.core.api.tasks.GetTaskRequest;
import com.unifier.core.api.tasks.GetTaskResponse;
import com.unifier.core.api.tasks.UpdateTaskRequest;
import com.unifier.core.api.tasks.UpdateTaskResponse;
import com.unifier.core.api.user.AddOrEditUserDetailRequest;
import com.unifier.core.api.user.AddOrEditUserDetailResponse;
import com.unifier.core.api.user.AddUserRolesRequest;
import com.unifier.core.api.user.AddUserRolesResponse;
import com.unifier.core.api.user.AdvanceSearchUsersRequest;
import com.unifier.core.api.user.AdvanceSearchUsersResponse;
import com.unifier.core.api.user.CreateApiUserRequest;
import com.unifier.core.api.user.CreateApiUserResponse;
import com.unifier.core.api.user.CreateUserRequest;
import com.unifier.core.api.user.CreateUserResponse;
import com.unifier.core.api.user.DeleteApiUserRequest;
import com.unifier.core.api.user.DeleteApiUserResponse;
import com.unifier.core.api.user.EditApiUserRequest;
import com.unifier.core.api.user.EditApiUserResponse;
import com.unifier.core.api.user.GetAllUsersRequest;
import com.unifier.core.api.user.GetAllUsersResponse;
import com.unifier.core.api.user.GetApiUserRequest;
import com.unifier.core.api.user.GetApiUserResponse;
import com.unifier.core.api.user.GetUserDetailsRequest;
import com.unifier.core.api.user.GetUserDetailsResponse;
import com.unifier.core.api.user.RemoveUserRoleRequest;
import com.unifier.core.api.user.RemoveUserRoleResponse;
import com.unifier.core.api.user.UserDTO;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.entity.EmailTemplate;
import com.unifier.core.entity.ExportJobType;
import com.unifier.core.entity.ImportJobType;
import com.unifier.core.entity.User;
import com.unifier.core.utils.StringUtils;
import com.unifier.services.email.IEmailService;
import com.unifier.services.export.IExportService;
import com.unifier.services.imports.IImportService;
import com.unifier.services.job.IJobService;
import com.unifier.services.tenantprofile.service.ITenantProfileService;
import com.unifier.services.users.IUsersService;
import com.unifier.web.utils.WebContextUtils;
import com.uniware.core.api.admin.shipping.EditPaymentReconciliationRequest;
import com.uniware.core.api.admin.shipping.EditPaymentReconciliationResponse;
import com.uniware.core.api.customer.CreateCustomerRequest;
import com.uniware.core.api.customer.CreateCustomerResponse;
import com.uniware.core.api.customer.EditCustomerRequest;
import com.uniware.core.api.customer.EditCustomerResponse;
import com.uniware.core.api.customer.GetCustomerRequest;
import com.uniware.core.api.customer.GetCustomerResponse;
import com.uniware.core.cache.LocationCache;
import com.uniware.core.cache.TenantCache;
import com.uniware.core.entity.UserDatatableView;
import com.uniware.core.utils.UserContext;
import com.uniware.dao.shipping.IPaymentReconciliationService;
import com.uniware.services.configuration.DatatableViewConfiguration;
import com.uniware.services.configuration.ExportJobConfiguration;
import com.uniware.services.customer.ICustomerService;
import com.uniware.services.external.IUnideskService;

@Controller
@Path("/data/admin/system/")
public class SystemResource {

    @Autowired
    private IUsersService                 usersService;

    @Autowired
    private IEmailService                 emailService;

    @Autowired
    private IExportService                exportService;

    @Autowired
    private IImportService                importService;

    @Autowired
    private IJobService                   jobService;

    @Autowired
    private ICustomerService              customerService;

    @Autowired
    private ITenantProfileService         tenantProfileService;

    @Autowired
    private IPaymentReconciliationService paymentReconciliationService;

    @Autowired
    private IUnideskService               unideskService;

    private static final String           URL_WSDL = "/services/soap/uniware16.wsdl";

    private static final String           URL_API  = "/services/soap/?version=1.6";

    @Produces(MediaType.APPLICATION_JSON)
    @Path("user/show")
    @POST
    public UserDTO showUser(@QueryParam("username") String username) {
        return usersService.getUserDTOByUsername(username);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("user/create")
    @POST
    public CreateUserResponse createUser(CreateUserRequest request) {
        return usersService.createUser(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("user/removeRole")
    @POST
    public RemoveUserRoleResponse removeUserRole(RemoveUserRoleRequest request) {
        return usersService.removeUserRole(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("user/addRoles")
    @POST
    public AddUserRolesResponse addUserRoles(AddUserRolesRequest request) {
        return usersService.addUserRoles(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("user/detail/edit")
    @POST
    public AddOrEditUserDetailResponse editUserDetail(AddOrEditUserDetailRequest request) {
        AddOrEditUserDetailResponse editUserDetail = usersService.editUserDetail(request);
        return editUserDetail;
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("emailTemplate/get")
    @GET
    public EmailTemplateDTO getEmailTemplate(@QueryParam("type") String type) {
        EmailTemplate template = emailService.getEmailTemplateByType(type);
        EmailTemplateDTO templateDTO = new EmailTemplateDTO();
        templateDTO.setType(template.getType());
        templateDTO.setEnabled(template.isEnabled());
        if (StringUtils.isNotEmpty(template.getCcEmail())) {
            templateDTO.setCc(StringUtils.split(template.getCcEmail()));
        }
        if (StringUtils.isNotEmpty(template.getBccEmail())) {
            templateDTO.setBcc(StringUtils.split(template.getBccEmail()));
        }
        templateDTO.setSubjectTemplate(template.getSubjectTemplate());
        templateDTO.setBodyTemplate(template.getBodyTemplate());
        return templateDTO;
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("emailTemplate/edit")
    @POST
    public EditEmailTemplateResponse editEmailTemplate(EditEmailTemplateRequest request) {
        EditEmailTemplateResponse response = emailService.editEmailTemplate(request);
        return response;
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("exportJobType/get")
    @GET
    public ExportJobTypeDTO getExportJobType(@QueryParam("name") String name) {
        ExportJobTypeDTO exportJobTypeDTO = new ExportJobTypeDTO();
        ExportJobType exportJobType = exportService.getExportJobTypeByName(name);
        exportJobTypeDTO.setName(exportJobType.getName());
        exportJobTypeDTO.setEnabled(exportJobType.isEnabled());
        exportJobTypeDTO.setExportJobConfig(exportJobType.getExportJobConfig());
        exportJobTypeDTO.setType(exportJobType.getType());
        return exportJobTypeDTO;
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("exportJobType/edit")
    @POST
    public EditExportJobTypeResponse editExportJobType(EditExportJobTypeRequest request) {
        EditExportJobTypeResponse response = exportService.editExportJobType(request);
        return response;
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("importJobType/get")
    @GET
    public ImportJobTypeDTO getImportJobType(@QueryParam("name") String name) {
        ImportJobTypeDTO importJobTypeDTO = new ImportJobTypeDTO();
        ImportJobType importJobType = importService.getImportJobTypeByName(name);
        importJobTypeDTO.setName(importJobType.getName());
        importJobTypeDTO.setEnabled(importJobType.isEnabled());
        importJobTypeDTO.setImportJobConfig(importJobType.getImportJobConfig());
        importJobTypeDTO.setImportOptions(StringUtils.split(importJobType.getImportOptions(), ","));
        return importJobTypeDTO;

    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("importJobType/edit")
    @POST
    public EditImportJobTypeResponse editImportJobType(EditImportJobTypeRequest request) {
        return importService.editImportJobType(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("task/get")
    @POST
    public GetTaskResponse getTask(GetTaskRequest request) {
        return jobService.getJob(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("task/update")
    @POST
    public UpdateTaskResponse updateTask(UpdateTaskRequest request) {
        // TODO: not supported as of now
        return null;

    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("states/get")
    @GET
    public String getStatesJson(@QueryParam("countryCode") String countryCode) {
        String statesJson = CacheManager.getInstance().getCache(LocationCache.class).getStateJson(countryCode);
        return statesJson != null ? statesJson : StringUtils.EMPTY_STRING;
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("paymentsReconciliation/edit")
    @POST
    public EditPaymentReconciliationResponse editPaymentReconciliation(EditPaymentReconciliationRequest request) {
        return paymentReconciliationService.editPaymentReconciliation(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("payments/reconcile")
    @GET
    public void reconcileShipmentPayments(@QueryParam("paymentMethodCode") String paymentMethodCode) {
        paymentReconciliationService.reconcileShipmentPayments(paymentMethodCode);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("customer/create")
    @POST
    public CreateCustomerResponse createCustomer(CreateCustomerRequest request) {
        return customerService.createCustomer(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("customer/detail/get")
    @POST
    public GetCustomerResponse getCustomerByCode(GetCustomerRequest request) {
        return customerService.getCustomerByCode(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("customer/update")
    @POST
    public EditCustomerResponse editCustomer(EditCustomerRequest request) {
        EditCustomerResponse response = customerService.editCustomer(request);
        return response;
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("exportConfig/datatableConfigs")
    @GET
    public String getDatatableConfigs() {
        ExportJobConfiguration config = ConfigurationManager.getInstance().getConfiguration(ExportJobConfiguration.class);
        if (config != null) {
            return new Gson().toJson(config.getDatatableConfigs());
        }
        return StringUtils.EMPTY_STRING;
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("datatableViews")
    @GET
    public String getUserDatatableView(@QueryParam("name") String name) {
        DatatableViewConfiguration configuration = ConfigurationManager.getInstance().getConfiguration(DatatableViewConfiguration.class);
        List<UserDatatableView> datatableViews = configuration.getDatatableViewsByType(name);
        List<UserDatatableView> userViews = new ArrayList<UserDatatableView>();
        Set<String> alreadyAddedViews = new HashSet<String>();
        if (datatableViews != null) {
            for (UserDatatableView datatableView : datatableViews) {
                if (!alreadyAddedViews.contains(datatableView.getName())) {
                    userViews.add(datatableView);
                    alreadyAddedViews.add(datatableView.getName());
                }
            }
        }
        return new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create().toJson(userViews);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("exportJobType/clone")
    @POST
    public CloneExportJobTypeResponse cloneExportJobType(CloneExportJobTypeRequest request) {
        return exportService.cloneExportJobType(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("apiUser/create")
    @POST
    public CreateApiUserResponse createApiUser(CreateApiUserRequest request) {
        request.setUserId(WebContextUtils.getCurrentUser().getUser().getId());
        return usersService.createApiUser(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("apiUser/get")
    @POST
    public GetApiUserResponse getApiUser(GetApiUserRequest request) {
        request.setUserId(WebContextUtils.getCurrentUser().getUser().getId());
        GetApiUserResponse response = usersService.getApiUser(request);
        response.setLicenseKey(tenantProfileService.getTenantProfileByCode(UserContext.current().getTenant().getCode()).getLicenseKey());
        response.setProductionUrl("https://" + WebContextUtils.getRequest().getServerName() + URL_API);
        response.setProductionWsdl("https://" + WebContextUtils.getRequest().getServerName() + URL_WSDL);
        return response;
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("apiUser/delete")
    @POST
    public DeleteApiUserResponse deleteApiUser(DeleteApiUserRequest request) {
        return usersService.deleteApiUser(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("apiUser/edit")
    @POST
    public EditApiUserResponse editApiUser(EditApiUserRequest request) {
        return usersService.editApiUser(request);
    }

    public static void main(String[] args) {
        HashMap<SystemResource, Integer> map = new HashMap<>();
        map.put(new SystemResource(), 12);
        System.out.println(map.get("abd"));
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("user/detail")
    @POST
    public GetUserDetailsResponse getUserDetails(GetUserDetailsRequest request) {
        return usersService.getUserWithDetailByUsername(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @POST
    @Path("users/get")
    public GetAllUsersResponse getAllUsers(GetAllUsersRequest request) {
        return usersService.getAllUsers(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("user/add")
    @POST
    public AddOrEditUserDetailResponse addUser(AddOrEditUserDetailRequest request) {
        return usersService.addUser(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("user/edit")
    @POST
    public AddOrEditUserDetailResponse editUser(AddOrEditUserDetailRequest request) {
        return usersService.editUserDetail(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("users/search")
    @POST
    public AdvanceSearchUsersResponse searchUser(AdvanceSearchUsersRequest request) {
        return usersService.searchUsers(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("unidesk/getToken")
    @POST
    public GetUnideskTokenResponse getUnideskToken(GetUnideskTokenRequest request) {
        User currentUser = WebContextUtils.getCurrentUser().getUser();
        Integer id = UserContext.current().getTenantId();
        request.setProductType(CacheManager.getInstance().getCache(TenantCache.class).getCurrentTenant().getProduct().getCode());
        request.setUsername(currentUser.getUsername());
        request.setName(currentUser.getName());
        request.setMobile(currentUser.getMobile());
        return unideskService.getUnideskToken(request);
    }
}
