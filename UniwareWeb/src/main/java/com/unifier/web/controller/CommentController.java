/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 30-Jun-2012
 *  @author praveeng
 */
package com.unifier.web.controller;

import com.unifier.core.api.comments.GetCommentsRequest;
import com.unifier.core.api.comments.GetCommentsResponse;
import com.unifier.services.comments.ICommentsService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.google.gson.Gson;

@Controller
public class CommentController {

    @Autowired
    private ICommentsService commentsService;

    @RequestMapping("/comment")
    public String comments(@RequestParam("referenceIdentifier") String referenceIdentifier, ModelMap map) {
        GetCommentsRequest request = new GetCommentsRequest();
        request.setReferenceIdentifier(referenceIdentifier);
        GetCommentsResponse response = commentsService.getComments(request);

        map.addAttribute("comments", new Gson().toJson(response.getUserCommentDTOs()));
        return "/comment/comment";
    }
}
