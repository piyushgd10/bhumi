/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 *  @version     1.0, Apr 6, 2012
 *  @author praveeng
 */
package com.unifier.web.controller.admin;

import com.unifier.core.api.print.AddPrintTemplateRequest;
import com.unifier.core.api.print.AddPrintTemplateResponse;
import com.unifier.core.api.print.EditPrintTemplateRequest;
import com.unifier.core.api.print.EditPrintTemplateResponse;
import com.unifier.core.api.print.GetAllSamplePrintTemplatesRequest;
import com.unifier.core.api.print.GetAllSamplePrintTemplatesResponse;
import com.unifier.core.api.print.GetSamplePrintTemplatesByTypeRequest;
import com.unifier.core.api.print.GetSamplePrintTemplatesByTypeResponse;
import com.unifier.core.api.print.PrintTemplateDTO;
import com.unifier.core.cache.CacheManager;
import com.unifier.services.printing.IPrintConfigService;
import com.unifier.services.printing.IGlobalPrintConfigService;
import com.uniware.core.vo.SamplePrintTemplateVO;
import com.uniware.services.cache.GlobalSamplePrintTemplateCache;
import com.uniware.services.cache.SamplePrintTemplateCache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;

@Controller
@Path("/data/admin")
public class PrintConfigResource {

    @Autowired
    private IPrintConfigService printConfigService;

    @Autowired
    private  IGlobalPrintConfigService globalPrintConfigService;
    /**
     * Show all templates
     * @param request
     * @return
     */
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/template/getAllTemplates")
    @POST
    public GetAllSamplePrintTemplatesResponse getAllSamplePrintTemplates(GetAllSamplePrintTemplatesRequest request) {
        return printConfigService.getAllSamplePrintTemplates(request);
    }
    
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/template/getTemplatesByType")
    @POST
    public GetSamplePrintTemplatesByTypeResponse getSamplePrintTemplates(GetSamplePrintTemplatesByTypeRequest request) {
        return printConfigService.getSamplePrintTemplatesByType(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/template/editTemplate")
    @POST
    public EditPrintTemplateResponse editPrintTemplate(EditPrintTemplateRequest request) {
        return printConfigService.editPrintTemplate(request);
    }

    /**
     * Select and Save
     * @param request
     * @return
     */
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/template/addTemplate")
    @POST
    public AddPrintTemplateResponse addPrintTemplate(AddPrintTemplateRequest request) {
        return printConfigService.addPrintTemplate(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/printing/template/global/get")
    @GET
    public PrintTemplateDTO getSamplePrintTemplateByTypeAndName(@QueryParam("type") String type, @QueryParam("name") String name) {
        SamplePrintTemplateVO template = printConfigService.getSamplePrintTemplateByTypeAndName(type, name);
        if (template == null) template = globalPrintConfigService.getGlobalSamplePrintTemplateByTypeAndName(type, name);
        return new PrintTemplateDTO(template);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/printing/template/samples/get")
    @GET
    public List<PrintTemplateDTO> getSampleTemplates(@QueryParam("type") String type) {
        List<PrintTemplateDTO> templates = CacheManager.getInstance().getCache(SamplePrintTemplateCache.class).getSamplePrintTemplatesByType(type);
        if (templates == null) {
            templates = new ArrayList<PrintTemplateDTO>();
        }
        return templates;
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/printing/template/global/edit")
    @POST
    public EditPrintTemplateResponse editGlobalTemplate(EditPrintTemplateRequest request) {
        EditPrintTemplateResponse response = printConfigService.editPrintTemplate(request);
        return response;
    }
}
