/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIALUse is subject to license terms.
 *  
 *  @version     1.0, Dec 10, 2011
 *  @author singla
 */
package com.unifier.web.controller.admin;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.config.Configuration;
import org.apache.logging.log4j.core.config.LoggerConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.unifier.core.api.base.ServiceResponse;
import com.unifier.core.api.tasks.ReloadConfigurationRequest;
import com.unifier.core.api.tasks.ReloadConfigurationResponse;
import com.unifier.core.api.tasks.ReloadTasksResponse;
import com.uniware.services.reload.IReloadService;

/**
 * @author singla
 */
@Controller
@Path("/data/admin/")
public class AdminController {

    @Autowired
    private IReloadService reloadService;

    @Produces(MediaType.APPLICATION_JSON)
    @Path("cache/reload")
    @GET
    public ReloadConfigurationResponse reloadConfiguration(@QueryParam("type") String reloadType) {
        ReloadConfigurationRequest request = new ReloadConfigurationRequest();
        request.setType(reloadType);
        return reloadService.reload(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("tasks/reload")
    @GET
    public ReloadTasksResponse reloadConfiguration() {
        throw new RuntimeException("Not supported");
        //return reloadService.reloadTasks();
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("change/log")
    @GET
    public ServiceResponse changeLogLevel(@QueryParam("level") String toLevel) {
        LoggerContext ctx = (LoggerContext) LogManager.getContext(false);
        Configuration config = ctx.getConfiguration();
        LoggerConfig loggerConfig = config.getLoggerConfig(LogManager.ROOT_LOGGER_NAME);
        loggerConfig.setLevel(Level.toLevel(toLevel));
        ctx.updateLoggers();
        return new ServiceResponse(true, "log level changed to " + toLevel);
    }
}
