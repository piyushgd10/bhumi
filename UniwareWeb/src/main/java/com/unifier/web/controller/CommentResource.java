/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 30-Jun-2012
 *  @author praveeng
 */
package com.unifier.web.controller;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.unifier.core.api.comments.AddCommentRequest;
import com.unifier.core.api.comments.AddCommentResponse;
import com.unifier.core.api.comments.DeleteCommentRequest;
import com.unifier.core.api.comments.DeleteCommentResponse;
import com.unifier.core.api.comments.GetCommentsRequest;
import com.unifier.core.api.comments.GetCommentsResponse;
import com.unifier.services.comments.ICommentsService;
import com.unifier.web.utils.WebContextUtils;

@Controller
@Path("/data/comment/")
public class CommentResource {

    @Autowired
    private ICommentsService commentsService;

    @Produces(MediaType.APPLICATION_JSON)
    @Path("add")
    @POST
    public AddCommentResponse addComment(AddCommentRequest request) {
        request.setUserId(WebContextUtils.getCurrentUser().getUser().getId());
        return commentsService.addComment(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("delete")
    @POST
    public DeleteCommentResponse commentDTO(DeleteCommentRequest request) {
        request.setUserId(WebContextUtils.getCurrentUser().getUser().getId());
        return commentsService.deleteComment(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("get")
    @POST
    public GetCommentsResponse getComments(GetCommentsRequest request) {
        return commentsService.getComments(request);
    }

}
