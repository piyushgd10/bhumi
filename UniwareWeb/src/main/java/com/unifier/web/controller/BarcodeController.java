/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jan 21, 2012
 *  @author singla
 */
package com.unifier.web.controller;

import java.awt.Color;
import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import net.sourceforge.barbecue.Barcode;
import net.sourceforge.barbecue.BarcodeException;
import net.sourceforge.barbecue.BarcodeImageHandler;
import net.sourceforge.barbecue.linear.code128.Code128Barcode;
import net.sourceforge.barbecue.linear.code39.Code39Barcode;
import net.sourceforge.barbecue.output.OutputException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author singla
 */
@Controller
public class BarcodeController {

    private static final Logger LOG = LoggerFactory.getLogger(BarcodeController.class);

    @RequestMapping("/barcode/{barcodeFont}/{barHeight}/{barWidth}/{barcodeText}")
    public void viewObject(@PathVariable("barcodeFont") String barcodeFont, @PathVariable("barcodeText") String barcodeText, @PathVariable("barHeight") Integer barHeight,
            @PathVariable("barWidth") Integer barWidth, HttpServletResponse response) throws IOException {
        try {
            Barcode barcode = null;
            if ("code128c".equals(barcodeFont)) {
                barcode = new Code128Barcode(barcodeText, Code128Barcode.C);
            } else {
                barcode = new Code39Barcode(barcodeText, false, true);
            }
            barcode.setBarHeight(barHeight);
            barcode.setBarWidth(barWidth);
            barcode.setBackground(Color.WHITE);
            BarcodeImageHandler.writePNG(barcode, response.getOutputStream());
        } catch (BarcodeException e) {
            LOG.error("Unable to draw barcode", e);
        } catch (OutputException e) {
            LOG.error("Unable to draw barcode", e);
        }
    }
}
