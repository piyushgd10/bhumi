/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 26-Feb-2014
 *  @author akshay
 */
package com.unifier.web.controller;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.stereotype.Controller;

import com.unifier.core.api.user.UpdateUserLastReadTimeRequest;
import com.unifier.core.api.user.UpdateUserLastReadTimeResponse;
import com.unifier.services.users.IUsersService;
import com.unifier.web.security.UniwareUser;
import com.unifier.web.utils.WebContextUtils;
import com.uniware.core.api.broadcast.GetBroadcastedMessagesRequest;
import com.uniware.core.api.broadcast.GetBroadcastedMessagesResponse;
import com.uniware.services.broadcast.IBroadcastService;
import com.uniware.web.messaging.simp.annotation.SendToCurrentUser;

@Controller
@Path("/data/broadcast/")
public class BroadcastController {

    @Autowired
    private IBroadcastService broadcastService;

    @Autowired
    private IUsersService     userService;

    @SendToCurrentUser
    @MessageMapping("/data/broadcast/messages/get")
    @Produces(MediaType.APPLICATION_JSON)
    @Path("messages/get")
    @POST
    public GetBroadcastedMessagesResponse getBroadcastedMessages(GetBroadcastedMessagesRequest request) {
        UniwareUser uniwareUser = WebContextUtils.getCurrentUser();
        if (uniwareUser != null) {
            request.setUsername(uniwareUser.getUsername());
            return broadcastService.getBroadcastedMessages(request);
        }
        return null;
    }

    @SendToCurrentUser
    @MessageMapping("/data/broadcast/update/lastReadTime")
    @Produces(MediaType.APPLICATION_JSON)
    @Path("update/lastReadTime")
    @POST
    public UpdateUserLastReadTimeResponse updateUserLastReadTime(UpdateUserLastReadTimeRequest request) {
        UniwareUser uniwareUser = WebContextUtils.getCurrentUser();
        if (uniwareUser != null) {
            request.setUserId(uniwareUser.getUser().getId());
            return userService.updateUserLastReadTime(request);
        }
        return null;
    }

}
