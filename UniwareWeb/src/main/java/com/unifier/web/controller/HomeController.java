/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIALUse is subject to license terms.
 *  
 *  @version     1.0, Dec 10, 2011
 *  @author singla
 */
package com.unifier.web.controller;

import java.io.IOException;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import com.unifier.core.api.export.ExportJobDTO;
import com.unifier.core.api.imports.ImportJobDTO;
import com.uniware.core.cache.FacilityCache;
import com.uniware.core.entity.Facility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.unifier.core.api.tasks.UserExportJobsResponse;
import com.unifier.core.api.tasks.UserImportJobsResponse;
import com.unifier.core.api.user.AddUserDatatableViewsRequest;
import com.unifier.core.api.user.AddUserDatatableViewsResponse;
import com.unifier.core.api.user.SwitchFacilityRequest;
import com.unifier.core.api.user.SwitchFacilityResponse;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.entity.ExportJob;
import com.unifier.core.entity.ImportJob;
import com.unifier.core.entity.User;
import com.unifier.core.export.config.ExportConfig;
import com.unifier.core.utils.ReflectionUtils;
import com.unifier.core.utils.StringUtils;
import com.unifier.services.export.IExportService;
import com.unifier.services.imports.IImportService;
import com.unifier.services.user.notification.IUserNotificationService;
import com.unifier.services.users.IUsersService;
import com.unifier.web.security.UniwareUser;
import com.unifier.web.utils.WebContextUtils;
import com.uniware.core.api.tenant.CreateTenantRequest;
import com.uniware.core.api.tenant.GetTenantSetupStateRequest;
import com.uniware.core.api.tenant.GetTenantSetupStateResponse;
import com.uniware.core.api.usernotification.GetUserNotificationsRequest;
import com.uniware.core.api.usernotification.GetUserNotificationsRespone;
import com.uniware.core.cache.EnvironmentPropertiesCache;
import com.uniware.core.cache.RolesCache;
import com.uniware.core.utils.UserContext;
import com.uniware.services.configuration.ExportJobConfiguration;
import com.uniware.services.tenant.ITenantSetupService;

/**
 * @author singla
 */
@Controller
@Path("/data/user/")
public class HomeController {

    @Autowired
    private IUsersService usersService;

    @Autowired
    private ITenantSetupService tenantSetupService;

    @Autowired
    private IUserNotificationService userNotificationService;

    @Autowired
    private IExportService exportService;

    @Autowired
    private IImportService importService;

    @RequestMapping("")
    public String homePage(HttpServletResponse response, ModelMap modelMap) throws IOException {
        modelMap.put("time", System.currentTimeMillis());
        return "index";
    }

    @RequestMapping("/login")
    public ModelAndView loginPage(HttpServletResponse response, ModelMap modelMap) throws IOException {
        return new ModelAndView("redirect:" + CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class).getAuthServerUrl());
    }

    @RequestMapping("embedLogin")
    public String embedLoginPage(HttpServletResponse response, ModelMap modelMap) throws IOException {
        modelMap.put("time", System.currentTimeMillis());
        return "embedLogin";
    }

    @RequestMapping("/error/{errorCode}")
    public String errorPage(@PathVariable("errorCode") String errorCode) {
        return "error/" + errorCode;
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("switchfacility")
    @POST
    public SwitchFacilityResponse switchFacility(SwitchFacilityRequest request) {
        request.setUserId(WebContextUtils.getCurrentUser().getUser().getId());
        SwitchFacilityResponse response = usersService.switchFacility(request);
        if (response.isSuccessful()) {
            User user = usersService.getUserByUsername(WebContextUtils.getCurrentUser().getUsername());
            WebContextUtils.getCurrentUser().loadUser(user);
            UserContext.current().setFacility(CacheManager.getInstance().getCache(FacilityCache.class).getCurrentFacility());
            if (StringUtils.isNotBlank(request.getCurrentUrl())) {
                Set<String> accessibleByRoles = CacheManager.getInstance().getCache(RolesCache.class).getAccessibleByRoles(request.getCurrentUrl());
                response.setCurrentUrlAccessible(accessibleByRoles != null && WebContextUtils.getCurrentUser().hasAnyRole(accessibleByRoles));
            }
        }
        return response;
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("importJobs")
    @GET
    public UserImportJobsResponse getImportJobs() {
        UserImportJobsResponse response = new UserImportJobsResponse();
        for (ImportJob importJob : importService.getUserImports(WebContextUtils.getCurrentUser().getUsername())) {
            response.getImportJobs().add(new ImportJobDTO(importJob));
        }
        response.setSuccessful(true);
        return response;
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("exportJobs")
    @GET
    public UserExportJobsResponse getExportJobs() {
        UserExportJobsResponse response = new UserExportJobsResponse();
        for (ExportJob exportJob : exportService.getUserExports(WebContextUtils.getCurrentUser().getUsername())) {
            response.getExportJobs().add(new ExportJobDTO(exportJob));
        }
        response.setSuccessful(true);
        return response;
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("userNotifications")
    @GET
    public GetUserNotificationsRespone getUserNotifications() {
        UniwareUser user = WebContextUtils.getCurrentUser();
        GetUserNotificationsRequest request = new GetUserNotificationsRequest();
        request.setNotificationTypes(user.getNotificationTypes());
        return userNotificationService.getUserNotifications(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("datatableView/create")
    @POST
    public AddUserDatatableViewsResponse createUserDatatableView(AddUserDatatableViewsRequest request) {
        ExportConfig exportConfig = ConfigurationManager.getInstance().getConfiguration(ExportJobConfiguration.class).getExportConfigByName(request.getDatatableName());
        UniwareUser uniwareUser = WebContextUtils.getCurrentUser();
        if (uniwareUser.hasResource(exportConfig.getAccessResourceName())) {
            User user = WebContextUtils.getCurrentUser().getUser();
            request.setUserId(user.getId());
            AddUserDatatableViewsResponse response = usersService.addUserDatatableViews(request);
            return response;
        } else {
            throw new WebApplicationException(HttpURLConnection.HTTP_FORBIDDEN);
        }
    }

    @RequestMapping("/datatableViews")
    public String datatableView() {
        return "tasks/tableViews";
    }

    @RequestMapping(value = "/tenant/new/home", method = RequestMethod.POST)
    public String newTenantLandingView(CreateTenantRequest request, ModelMap map) {
        map.addAttribute("userName", request.getUsername());
        map.addAttribute("password", request.getPassword());
        map.addAttribute("tenantCode", request.getCode());
        map.addAttribute("totalStates", tenantSetupService.getTenantSetupStateMappings().size());
        return "newTenant";
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("tenant/new/status")
    @POST
    public GetTenantSetupStateResponse getTenantStatus(GetTenantSetupStateRequest request) {
        return tenantSetupService.getTenantStateByTenantCode(request);
    }

    public static void main(String[] args) throws ClassNotFoundException, IOException {
        List<Class<?>> clazzes = ReflectionUtils.getClassesAnnotatedWith(Controller.class, "com.uniware.web.controller.admin");
        for (Class<?> clazz : clazzes) {
            Path path = clazz.getAnnotation(Path.class);
            if (path != null) {
                for (Method method : clazz.getMethods()) {
                    Path path1 = method.getAnnotation(Path.class);
                    if (path1 != null) {
                        System.out.println(path.value().substring(5) + path1.value());
                    }
                }
            }
        }
    }
}