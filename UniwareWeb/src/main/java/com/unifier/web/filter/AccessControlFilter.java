/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 28, 2012
 *  @author singla
 */
package com.unifier.web.filter;

import java.io.IOException;
import java.util.Set;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.filter.OncePerRequestFilter;

import com.google.gson.Gson;
import com.unifier.core.api.base.ServiceResponse;
import com.unifier.core.api.validation.WsError;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.entity.AccessResource;
import com.unifier.core.utils.EncryptionUtils;
import com.unifier.web.security.UniwareUser;
import com.unifier.web.utils.WebContextUtils;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.cache.FacilityCache;
import com.uniware.core.cache.RolesCache;
import com.uniware.core.entity.Facility;
import com.uniware.core.utils.UserContext;

/**
 * @author singla
 */
public class AccessControlFilter extends OncePerRequestFilter {

    private static final String SUPPORT_USER = "SUPPORT_USER";

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        if (!(request instanceof HttpServletRequest)) {
            throw new ServletException("This filter can only process HttpServletRequest requests");
        }
        UniwareUser uniwareUser = WebContextUtils.getCurrentUser();

        // This means this request is bypassed by SpringSecurity and hence should be bypassed here
        if (uniwareUser != null) {
            UserContext userContext = UserContext.current();
            Facility facility = CacheManager.getInstance().getCache(FacilityCache.class).getFacilityById(uniwareUser.getCurrentFacilityId());
            userContext.setFacility(facility);

            // Log user and tenant in access logs
            request.setAttribute("userId", uniwareUser.getUser().getId());
            request.setAttribute("username", uniwareUser.getUser().getUsername());

            // Log user and tenant in server logs
            String actualUsername = (String) request.getSession().getAttribute(SUPPORT_USER);
            if (actualUsername == null) {
                actualUsername = uniwareUser.getUsername();
            }
            String tenantCode = UserContext.current().getTenant().getCode();
            userContext.setUniwareUserName(uniwareUser.getUsername());
            userContext.setUserId(uniwareUser.getUser().getId());
            userContext.setThreadInfo(new StringBuilder(tenantCode).append(':').append(actualUsername).append(':').append(request.getRequestURI()).toString());
            Thread.currentThread().setName(new StringBuilder("HTTP-").append(Thread.currentThread().getId()).append(':').append(userContext.getThreadInfo()).toString());
            RolesCache rolesCache = CacheManager.getInstance().getCache(RolesCache.class);
            Set<String> accessibleByRoles = rolesCache.getAccessibleByRoles(WebContextUtils.getRequestURI(request));
            if (accessibleByRoles != null && uniwareUser.hasAnyRole(accessibleByRoles)) {
                filterChain.doFilter(request, response);
            } else if (accessibleByRoles == null) {
                ServiceResponse serviceResponse = new ServiceResponse();
                serviceResponse.addError(new WsError(WsResponseCode.INVALID_URI.code(), WsResponseCode.INVALID_URI.message()));
                response.getWriter().print(new Gson().toJson(serviceResponse));
                response.getWriter().flush();
            } else {
                ServiceResponse serviceResponse = new ServiceResponse();
                AccessResource accessResource = rolesCache.getAccessResourceByRequestURI(WebContextUtils.getRequestURI(request));
                serviceResponse.addError(new WsError(WsResponseCode.ACCESS_DENIED.code(),
                        WsResponseCode.ACCESS_DENIED.message() + (accessResource != null ? ", AccessResource needed: " + accessResource.getName() : "")));
                response.getWriter().print(new Gson().toJson(serviceResponse));
                response.getWriter().flush();
            }
        } else {
            filterChain.doFilter(request, response);
        }
    }

    public static void main(String[] args) {
        System.out.println(EncryptionUtils.md5Encode("unicom", "rahul@unicommerce.com"));
    }
}
