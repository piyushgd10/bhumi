/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Oct 5, 2012
 *  @author singla
 */
package com.unifier.web.filter;

import java.io.IOException;
import java.util.List;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.filter.OncePerRequestFilter;

import com.unifier.core.cache.CacheManager;
import com.unifier.core.utils.StringUtils;
import com.uniware.core.cache.FacilityCache;
import com.uniware.core.entity.Facility;
import com.uniware.core.utils.UserContext;

public class RestAPIRequestIdentifierFilter extends OncePerRequestFilter {

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        if (!(request instanceof HttpServletRequest)) {
            throw new ServletException("This filter can only process HttpServletRequest requests");
        }
        List<Facility> facilities = CacheManager.getInstance().getCache(FacilityCache.class).getFacilities();
        if (facilities.size() > 1) {
            Facility facility = null;
            if (StringUtils.isNotBlank(request.getHeader("facility"))) {
                facility = CacheManager.getInstance().getCache(FacilityCache.class).getFacilityByCode(request.getHeader("facility"));
            }
            if (facility != null) {
                UserContext.current().setFacility(facility);
            } else {
                UserContext.current().setFacility(null);
            }
        } else if (facilities.size() == 1) {
            UserContext.current().setFacility(facilities.get(0));
        }
        filterChain.doFilter(request, response);
    }
}
