/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 28, 2012
 *  @author singla
 */
package com.unifier.web.filter;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

import org.springframework.util.FileCopyUtils;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.util.WebUtils;

import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.utils.StringUtils;

/**
 * @author singla
 */
public class ReloadTypeFilter extends OncePerRequestFilter {

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        if (!(request instanceof HttpServletRequest)) {
            throw new ServletException("This filter can only process HttpServletRequest requests");
        }
        Set<String> reloadTypes = new HashSet<String>();
        if (StringUtils.isNotEmpty(request.getRequestURI())) {
            //reloadTypes = ConfigurationManager.getInstance().getConfiguration(ReloadConfiguration.class).getReloadTypeByUrl(request.getRequestURI());
        }

        if (reloadTypes != null) {
            BodyResponseWrapper responseWrapper = new BodyResponseWrapper(response);
            filterChain.doFilter(request, responseWrapper);
            byte[] body = responseWrapper.toByteArray();
            String responseString = new String(body, response.getCharacterEncoding());
            if (responseString.contains("\"successful\":true")) {
                for (String reloadType : reloadTypes) {
                    if (reloadType.contains("Configuration")) {
                        ConfigurationManager.getInstance().markConfigurationDirty(reloadType);
                    } else {
                        CacheManager.getInstance().markCacheDirty(reloadType);
                    }
                }
            }
            copyBodyToResponse(body, response);
        }
    }

    private void copyBodyToResponse(byte[] body, HttpServletResponse response) throws IOException {
        if (body.length > 0) {
            response.setContentLength(body.length);
            FileCopyUtils.copy(body, response.getOutputStream());
        }
    }

    /**
     * {@link HttpServletRequest} wrapper that buffers all content written to the {@linkplain #getOutputStream() output
     * stream} and {@linkplain #getWriter() writer}, and allows this content to be retrieved via a
     * {@link #toByteArray() byte array}.
     */
    private static class BodyResponseWrapper extends HttpServletResponseWrapper {

        private final ByteArrayOutputStream content      = new ByteArrayOutputStream();

        private final ServletOutputStream   outputStream = new ResponseServletOutputStream();

        private PrintWriter                 writer;

        private BodyResponseWrapper(HttpServletResponse response) {
            super(response);
        }

        @Override
        public void setContentLength(int len) {
        }

        @Override
        public ServletOutputStream getOutputStream() {
            return this.outputStream;
        }

        @Override
        public PrintWriter getWriter() throws IOException {
            if (this.writer == null) {
                String characterEncoding = getCharacterEncoding();
                this.writer = (characterEncoding != null ? new ResponsePrintWriter(characterEncoding) : new ResponsePrintWriter(WebUtils.DEFAULT_CHARACTER_ENCODING));
            }
            return this.writer;
        }

        @Override
        public void resetBuffer() {
            this.content.reset();
        }

        @Override
        public void reset() {
            super.reset();
            resetBuffer();
        }

        private byte[] toByteArray() {
            return this.content.toByteArray();
        }

        private class ResponseServletOutputStream extends ServletOutputStream {

            @Override
            public void write(int b) throws IOException {
                content.write(b);
            }

            @Override
            public void write(byte[] b, int off, int len) throws IOException {
                content.write(b, off, len);
            }
        }

        private class ResponsePrintWriter extends PrintWriter {

            private ResponsePrintWriter(String characterEncoding) throws UnsupportedEncodingException {
                super(new OutputStreamWriter(content, characterEncoding));
            }

            @Override
            public void write(char buf[], int off, int len) {
                super.write(buf, off, len);
                super.flush();
            }

            @Override
            public void write(String s, int off, int len) {
                super.write(s, off, len);
                super.flush();
            }

            @Override
            public void write(int c) {
                super.write(c);
                super.flush();
            }
        }
    }

}
