/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 10, 2011
 *  @author singla
 */
package com.unifier.web.utils;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.unifier.core.utils.StringUtils;

/**
 * @author singla
 */
public class PathResolver {

    private Map<String, String> jsPaths  = new HashMap<String, String>();
    private Map<String, String> cssPaths = new HashMap<String, String>();

    public enum ResourceType {
        CSS("css"),
        JAVASCRIPT("js");

        private String type;

        private ResourceType(String type) {
            this.type = type;
        }

        public String type() {
            return type;
        }
    }

    public String css(String initialPath) {
        String cssPath = cssPaths.get(initialPath);
        if (cssPath == null) {
            cssPath = getPath(initialPath, ResourceType.CSS);
            cssPaths.put(initialPath, cssPath);
        }
        return getStaticPath(cssPath);
    }

    public String js(String initialPath) {
        String jsPath = jsPaths.get(initialPath);
        if (jsPath == null) {
            jsPath = getPath(initialPath, ResourceType.JAVASCRIPT);
            jsPaths.put(initialPath, jsPath);
        }
        return getStaticPath(jsPath);
    }

    public String resources(String initialPath) {
        return getStaticPath(initialPath);
    }

    public String getScheme() {
        HttpServletRequest request = WebContextUtils.getRequest();
        StringBuilder builder = new StringBuilder();
        builder.append("https").append("://").append(request.getServerName());
        if (request.getServerPort() != 80 || request.getServerPort() != 443) {
            builder.append(':').append(request.getServerPort());
        }
        builder.append(request.getContextPath());
        return builder.toString();
    }

    public String getHttp() {
        return getHttpPath();
    }

    public String getHttps() {
        return getHttpPath();
    }

    public static String getHttpPath() {
        HttpServletRequest request = WebContextUtils.getRequest();
        StringBuilder builder = new StringBuilder();
        builder.append("https").append("://").append(request.getServerName());
        if (request.getServerPort() != 80 && request.getServerPort() != 443) {
            builder.append(':').append(request.getServerPort());
        }
        builder.append(request.getContextPath());
        return builder.toString();
    }

    public static String getHttpsPath() {
        HttpServletRequest request = WebContextUtils.getRequest();
        StringBuilder builder = new StringBuilder();
        builder.append("https://").append(request.getServerName());
        if (request.getServerPort() != 80 && request.getServerPort() != 443) {
            builder.append(':').append(request.getServerPort());
        }
        builder.append(request.getContextPath());
        return builder.toString();

    }

    private String getPath(String resourcePath, ResourceType resourceType) {
        if (resourcePath.startsWith("secure")) {
            resourcePath = resourcePath.substring(resourcePath.indexOf('/') + 1);
        }
        InputStream input = WebContextUtils.getServletContext().getResourceAsStream("/static/" + resourceType.type() + "/" + resourcePath);
        String md5Checksum;
        try {
            md5Checksum = MD5ChecksumUtils.getMD5Checksum(input);
        } catch (Exception e) {
            throw new IllegalArgumentException("Invalid resource path:" + resourcePath, e);
        }
        String result = resourceType.type() + "/" + md5Checksum + "/" + resourcePath;
        return result;
    }

    public String encode(Object path) {
        return encode(path.toString());
    }

    public String encode(String path) {
        try {
            if (path.startsWith("http://")) {
                return URLEncoder.encode(path, "UTF-8");
            } else {
                return URLEncoder.encode(getHttpPath() + path, "UTF-8");
            }
        } catch (UnsupportedEncodingException e) {
            return getHttpPath() + path;
        }
    }

    public String getEncodedMessage(Object message) {
        return getEncodedMessage(message.toString());
    }

    public String getEncodedMessage(String message) {
        try {
            return URLEncoder.encode(message, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            return message;
        }
    }

    private static String getStaticPath(String path) {
        HttpServletRequest req = WebContextUtils.getRequest();
        String environment = System.getProperty("env");
        if (StringUtils.isNotEmpty(environment)) {
            return new StringBuilder().append("https").append("://").append(environment).append(".ucdn.in/").append(path).toString();
        } else {
            return new StringBuilder().append("/").append(path).toString();
        }
    }
}
