/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 10, 2011
 *  @author singla
 */
package com.unifier.web.utils;

import java.io.InputStream;
import java.security.MessageDigest;

import org.springframework.security.authentication.encoding.Md5PasswordEncoder;

/**
 * @author singla
 */
public class MD5ChecksumUtils {

    private static final String HEXES = "0123456789ABCDEF";

    private static byte[] createChecksum(InputStream fis) throws Exception {
        byte[] buffer = new byte[1024];
        MessageDigest complete = MessageDigest.getInstance("MD5");
        int numRead;
        do {
            numRead = fis.read(buffer);
            if (numRead > 0) {
                complete.update(buffer, 0, numRead);
            }
        } while (numRead != -1);
        fis.close();
        return complete.digest();
    }

    private static String getHex(byte[] raw) {
        final StringBuilder hex = new StringBuilder(2 * raw.length);
        for (final byte b : raw) {
            hex.append(HEXES.charAt((b & 0xF0) >> 4)).append(HEXES.charAt((b & 0x0F)));
        }
        return hex.toString();
    }

    public static String getMD5Checksum(InputStream fis) throws Exception {
        return getHex(createChecksum(fis));
    }

    public static String md5Encode(String text, String salt) {
        return new Md5PasswordEncoder().encodePassword(text, salt);
    }

    public static boolean isCodeValid(String code, String text, String salt) {
        return new Md5PasswordEncoder().isPasswordValid(code, text, salt);
    }
}
