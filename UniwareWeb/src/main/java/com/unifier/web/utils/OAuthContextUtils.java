package com.unifier.web.utils;

import com.unifier.web.security.UniwareUser;
import com.unifier.web.security.oauth2.OAuth2UserDetailsService.OAuth2UserDetails;
import com.unifier.web.security.oauth2.OAuth2UserDetailsService;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * Created by admin on 7/9/15.
 */
public class OAuthContextUtils {

    public static OAuth2UserDetails getCurrentUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Object user = authentication == null ? null : authentication.getPrincipal();
        if (user != null && user instanceof OAuth2UserDetails) {
            return (OAuth2UserDetails) user;
        }
        return null;
    }
}
