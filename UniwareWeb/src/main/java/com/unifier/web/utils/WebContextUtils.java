/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 10, 2011
 *  @author singla
 */
package com.unifier.web.utils;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.context.ContextLoader;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.unifier.web.security.UniwareUser;
import com.unifier.web.services.APIUserDetailsService.ApiUserDetails;

/**
 * @author singla
 */
public class WebContextUtils {

    public enum SessionAttributeName {
        RELOAD_TYPES("reloadTypes");

        private final String attributeName;

        private SessionAttributeName(String attributeName) {
            this.attributeName = attributeName;
        }

        public String attributeName() {
            return this.attributeName;
        }
    }

    public static HttpServletRequest getRequest() {
        return ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
    }

    public static ServletContext getServletContext() {
        return ContextLoader.getCurrentWebApplicationContext().getServletContext();
    }

    @SuppressWarnings("unchecked")
    public static Map<String, String> constructRequestParamMap(HttpServletRequest request) {
        Map<String, String[]> params = request.getParameterMap();
        Set<String> paramKeys = params.keySet();

        Map<String, String> parameters = new HashMap<String, String>();
        for (String param : paramKeys) {
            String[] values = params.get(param);
            if (values != null && values.length > 0) {
                parameters.put(param, values[0]);
            }
        }
        return parameters;
    }

    public static UniwareUser getCurrentUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Object user = authentication == null ? null : authentication.getPrincipal();
        if (user != null && user instanceof UniwareUser) {
            return (UniwareUser) user;
        }
        return null;
    }

    public static ApiUserDetails getCurrentApiUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Object user = authentication == null ? null : authentication.getPrincipal();
        if (user != null && user instanceof ApiUserDetails) {
            return (ApiUserDetails) user;
        }
        return null;
    }

    public static void removeSessionAttribute(SessionAttributeName name) {
        getRequest().getSession(true).removeAttribute(name.attributeName());
    }

    public static void setSessionAttribute(SessionAttributeName name, Object value) {
        getRequest().getSession(true).setAttribute(name.attributeName(), value);
    }

    @SuppressWarnings("unchecked")
    public static <T> T getSessionAttribute(SessionAttributeName name, Class<T> type) {
        HttpSession session = getRequest().getSession(true);
        return (T) session.getAttribute(name.attributeName());
    }

    public static String getRequestURI(HttpServletRequest request) {
        String requestURI = request.getRequestURI();
        int index = requestURI.indexOf(";jsessionid");
        if (index != -1) {
            requestURI = requestURI.substring(0, index);
        }
        return requestURI;
    }
}
