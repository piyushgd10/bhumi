/*
 *  Copyright 2013 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 14-Feb-2013
 *  @author vibhu
 */
package com.unifier.web.listener;

import com.unifier.core.cache.CacheManager;

public class WebCache {

    private static WebCache _instance = new WebCache();

    public static WebCache getInstance() {
        return _instance;
    }

    public Object getCache(String cacheName) {
        return CacheManager.getInstance().getCache(cacheName);
    }

}
