/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 11, 2011
 *  @author singla
 */
package com.unifier.web.listener;

import java.io.IOException;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.logging.log4j.ThreadContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.template.Template;
import com.unifier.core.transport.http.HttpClientFactory;
import com.unifier.core.utils.DateUtils;
import com.unifier.services.startup.IStartupService;
import com.unifier.web.configuration.LayoutConfiguration;
import com.unifier.web.utils.PathResolver;
import com.unifier.web.utils.WebContextUtils;
import com.uniware.core.http.HttpClientNameDecorator;

/**
 * @author singla
 */
public class StartupContextListener implements ServletContextListener {

    private static final Logger LOG = LoggerFactory.getLogger(StartupContextListener.class);

    /* (non-Javadoc)
     * @see javax.servlet.ServletContextListener#contextInitialized(javax.servlet.ServletContextEvent)
     */
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        ThreadContext.put("username", "system");
        ThreadContext.put("tenant", "-");
        LOG.info("Context initialized event called...");
        WebApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(sce.getServletContext());
        try {
            Template.addUtility("cache", CacheManager.getInstance());
            Template.addUtility("configuration", ConfigurationManager.getInstance());

            // Set the WEB-INF path separately as it is only available at web layer.
            String appRootPath = WebContextUtils.getServletContext().getRealPath("/");
            LOG.info("Setting the APP ROOT path to: {}", appRootPath);

            // Set http client name decorator
            HttpClientFactory.setHttpClientNameDecorator(new HttpClientNameDecorator());

            context.getAutowireCapableBeanFactory().autowireBeanProperties(CacheManager.getInstance(), AutowireCapableBeanFactory.AUTOWIRE_BY_TYPE, false);
            context.getAutowireCapableBeanFactory().autowireBeanProperties(ConfigurationManager.getInstance(), AutowireCapableBeanFactory.AUTOWIRE_BY_TYPE, false);

            IStartupService startupService = context.getBean(IStartupService.class);
            startupService.loadStartupResources();
        } catch (Exception e) {
            LOG.error("error while initializing application:", e);
            throw new RuntimeException(e);
        }
        sce.getServletContext().setAttribute("path", new PathResolver());
        sce.getServletContext().setAttribute("cache", WebCache.getInstance());
        sce.getServletContext().setAttribute("configuration", WebConfiguration.getInstance());
        sce.getServletContext().setAttribute("dateUtils", new DateUtils());
        try {
            sce.getServletContext().setAttribute("layout", LayoutConfiguration.load(sce.getServletContext().getResourceAsStream("/WEB-INF/layout.xml")));
        } catch (IOException e) {
            LOG.error("error while loading layout configuration:", e);
        }
    }

    /* (non-Javadoc)
     * @see javax.servlet.ServletContextListener#contextDestroyed(javax.servlet.ServletContextEvent)
     */
    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        // TODO Auto-generated method stub

    }

}
