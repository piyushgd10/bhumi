/*
 *  Copyright 2013 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 14-Feb-2013
 *  @author vibhu
 */
package com.unifier.web.listener;

import com.unifier.core.configuration.ConfigurationManager;

public class WebConfiguration {

    private static WebConfiguration _instance = new WebConfiguration();

    public static WebConfiguration getInstance() {
        return _instance;
    }

    public Object getConfiguration(String configurationName) {
        return ConfigurationManager.getInstance().getConfiguration(configurationName);
    }
}
