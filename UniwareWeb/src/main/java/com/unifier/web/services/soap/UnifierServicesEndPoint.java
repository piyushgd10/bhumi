/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Aug 24, 2012
 *  @author singla
 */
package com.unifier.web.services.soap;

import com.unifier.core.api.export.CreateExportJobRequest;
import com.unifier.core.api.export.CreateExportJobResponse;
import com.unifier.core.api.export.GetExportJobStatusRequest;
import com.unifier.core.api.export.GetExportJobStatusResponse;
import com.unifier.core.api.imports.CreateImportJobRequest;
import com.unifier.core.api.imports.CreateImportJobResponse;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.entity.User;
import com.unifier.core.utils.FileUtils;
import com.unifier.services.export.IExportService;
import com.unifier.services.imports.IImportService;
import com.uniware.core.utils.UserContext;
import com.uniware.services.configuration.BaseSystemConfiguration;

import com.uniware.services.configuration.TenantSystemConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import java.io.File;

@Endpoint
public class UnifierServicesEndPoint {

    @Autowired
    private IExportService exportService;

    @Autowired
    private IImportService importService;

    @PayloadRoot(localPart = "CreateExportJobRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    public CreateExportJobResponse createExportJob(@RequestPayload CreateExportJobRequest request) {
        User user = ConfigurationManager.getInstance().getConfiguration(TenantSystemConfiguration.class).getSystemUser();
        request.setUserId(user.getId());
        return exportService.createExportJob(request);
    }

    @PayloadRoot(localPart = "GetExportJobStatusRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    public GetExportJobStatusResponse getExportJobStatus(@RequestPayload GetExportJobStatusRequest request) {
        return exportService.getExportJobStatus(request);
    }



    @PayloadRoot(localPart = "CreateImportJobRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload

    public CreateImportJobResponse createImportJob(@RequestPayload CreateImportJobRequest request) {
        request.setUserId(UserContext.current().getUserId());
        return importService.createImportJob(request);
    }
}
