/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Oct 5, 2012
 *  @author singla
 */
package com.unifier.web.services.soap;

import com.unifier.core.utils.StringUtils;

import java.io.IOException;
import java.util.Collections;
import java.util.Map;

import javax.xml.transform.Result;
import javax.xml.transform.Source;

import org.springframework.oxm.Marshaller;
import org.springframework.oxm.Unmarshaller;
import org.springframework.oxm.XmlMappingException;
import org.springframework.oxm.jibx.JibxMarshaller;

import com.uniware.core.utils.UserContext;

public class JibxVersionedMarshaller implements Marshaller, Unmarshaller {

    private Map<String, JibxMarshaller> marshallers = Collections.emptyMap();

    private String                      defaultVersion;

    @Override
    public Object unmarshal(Source source) throws IOException, XmlMappingException {
        return getMarshaller().unmarshal(source);
    }

    private JibxMarshaller getMarshaller() {
        String apiVersion = UserContext.current().getApiVersion();
        if (StringUtils.isNotBlank(apiVersion) && marshallers.containsKey(apiVersion)) {
            return marshallers.get(apiVersion);
        } else {
            return marshallers.get(defaultVersion);
        }
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return getMarshaller().supports(clazz);
    }

    @Override
    public void marshal(Object graph, Result result) throws IOException, XmlMappingException {
        getMarshaller().marshal(graph, result);
    }

    /**
     * @return the marshallers
     */
    public Map<String, JibxMarshaller> getMarshallers() {
        return marshallers;
    }

    /**
     * @param marshallers the marshallers to set
     */
    public void setMarshallers(Map<String, JibxMarshaller> marshallers) {
        this.marshallers = marshallers;
    }

    /**
     * @return the defaultVersion
     */
    public String getDefaultVersion() {
        return defaultVersion;
    }

    /**
     * @param defaultVersion the defaultVersion to set
     */
    public void setDefaultVersion(String defaultVersion) {
        this.defaultVersion = defaultVersion;
    }

}
