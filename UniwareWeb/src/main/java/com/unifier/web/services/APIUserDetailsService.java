/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jun 30, 2012
 *  @author singla
 */
package com.unifier.web.services;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.unifier.core.entity.ApiUser;
import com.unifier.core.entity.UserRole;
import com.unifier.services.users.IUsersService;
import com.unifier.web.security.UniwareAuthority;
import com.uniware.core.utils.UserContext;

public class APIUserDetailsService implements UserDetailsService {

    @Autowired
    private final IUsersService usersService;

    public APIUserDetailsService(IUsersService usersService) {
        this.usersService = usersService;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        ApiUser apiUser = usersService.getApiUserByUsername(username);
        if (apiUser != null) {
            return new ApiUserDetails(apiUser);
        } else {
            throw new UsernameNotFoundException("invalid email:" + username);
        }
    }

    public static class ApiUserDetails implements UserDetails {

        /**
         * 
         */
        private static final long           serialVersionUID = 7769615631911735128L;
        private ApiUser                     apiUser;
        private final Set<GrantedAuthority> authorities      = new HashSet<GrantedAuthority>();

        public ApiUserDetails(ApiUser apiUser) {
            loadUser(apiUser);
            for (UserRole userRole : apiUser.getUser().getUserRoles()) {
                authorities.add(new UniwareAuthority(userRole.getRole().getCode()));
            }
        }

        public void loadUser(ApiUser apiUser) {
            this.apiUser = apiUser;
        }

        @Override
        public Collection<? extends GrantedAuthority> getAuthorities() {
            return authorities;
        }

        @Override
        public String getPassword() {
            return apiUser.getPassword();
        }

        @Override
        public String getUsername() {
            return apiUser.getUsername();
        }

        public ApiUser getApiUser(){
            return apiUser;
        }

        @Override
        public boolean isAccountNonExpired() {
            return true;
        }

        @Override
        public boolean isAccountNonLocked() {
            return true;
        }

        @Override
        public boolean isCredentialsNonExpired() {
            return true;
        }

        @Override
        public boolean isEnabled() {
            return apiUser.isEnabled();
        }

    }

}
