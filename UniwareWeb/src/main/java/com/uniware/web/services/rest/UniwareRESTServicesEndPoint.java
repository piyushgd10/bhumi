/*
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  @version     1.0, Dec 17, 2011
 *  @author singla
 */
package com.uniware.web.services.rest;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;

import com.unifier.core.annotation.Level;
import com.unifier.core.api.export.CreateExportJobRequest;
import com.unifier.core.api.export.CreateExportJobResponse;
import com.unifier.core.api.export.GetExportJobStatusRequest;
import com.unifier.core.api.export.GetExportJobStatusResponse;
import com.unifier.core.api.tasks.EditScriptConfigRequest;
import com.unifier.core.api.tasks.EditScriptConfigResponse;
import com.unifier.core.api.tasks.ReloadConfigurationRequest;
import com.unifier.core.api.tasks.ReloadConfigurationResponse;
import com.unifier.core.api.token.GetLoginTokenRequest;
import com.unifier.core.api.token.GetLoginTokenResponse;
import com.unifier.core.api.validation.WsError;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.entity.AccessResource.Name;
import com.unifier.core.entity.ApiUser;
import com.unifier.core.entity.User;
import com.unifier.services.export.IExportService;
import com.unifier.services.users.IUsersService;
import com.unifier.web.utils.WebContextUtils;
import com.uniware.core.api.catalog.CreateItemTypeRequest;
import com.uniware.core.api.catalog.CreateItemTypeResponse;
import com.uniware.core.api.catalog.CreateOrEditCategoryRequest;
import com.uniware.core.api.catalog.CreateOrEditCategoryResponse;
import com.uniware.core.api.catalog.CreateOrEditFacilityItemTypeRequest;
import com.uniware.core.api.catalog.CreateOrEditFacilityItemTypeResponse;
import com.uniware.core.api.catalog.CreateOrEditItemTypeRequest;
import com.uniware.core.api.catalog.CreateOrEditItemTypeResponse;
import com.uniware.core.api.catalog.EditItemTypeRequest;
import com.uniware.core.api.catalog.EditItemTypeResponse;
import com.uniware.core.api.catalog.GetItemTypeRequest;
import com.uniware.core.api.catalog.GetItemTypeResponse;
import com.uniware.core.api.catalog.GetServiceabilityRequest;
import com.uniware.core.api.catalog.GetServiceabilityResponse;
import com.uniware.core.api.customer.CreateCustomerRequest;
import com.uniware.core.api.customer.CreateCustomerResponse;
import com.uniware.core.api.customer.EditCustomerRequest;
import com.uniware.core.api.customer.EditCustomerResponse;
import com.uniware.core.api.customizations.MarkItemDamagedOutboundQCRequest;
import com.uniware.core.api.customizations.MarkItemDamagedOutboundQCResponse;
import com.uniware.core.api.cyclecount.BlockShelvesForCycleCountRequest;
import com.uniware.core.api.cyclecount.BlockShelvesForCycleCountResponse;
import com.uniware.core.api.cyclecount.CompleteCountForShelfRequest;
import com.uniware.core.api.cyclecount.CompleteCountForShelfResponse;
import com.uniware.core.api.cyclecount.GetActiveCycleCountRequest;
import com.uniware.core.api.cyclecount.GetBadItemsForCycleCountRequest;
import com.uniware.core.api.cyclecount.GetBadItemsForShelfRequest;
import com.uniware.core.api.cyclecount.GetBadItemsForShelfResponse;
import com.uniware.core.api.cyclecount.GetCycleCountResponse;
import com.uniware.core.api.cyclecount.GetErrorItemsForCycleCountResponse;
import com.uniware.core.api.cyclecount.GetSubCycleCountShelvesRequest;
import com.uniware.core.api.cyclecount.GetSubCycleCountShelvesResponse;
import com.uniware.core.api.cyclecount.GetZonesForCycleCountRequest;
import com.uniware.core.api.cyclecount.GetZonesForCycleCountResponse;
import com.uniware.core.api.cyclecount.RecountShelfRequest;
import com.uniware.core.api.cyclecount.RecountShelfResponse;
import com.uniware.core.api.cyclecount.SearchCycleCountShelfRequest;
import com.uniware.core.api.cyclecount.SearchCycleCountShelfResponse;
import com.uniware.core.api.cyclecount.StartCountingForShelfRequest;
import com.uniware.core.api.cyclecount.StartCountingForShelfResponse;
import com.uniware.core.api.cyclecount.UnblockShelvesRequest;
import com.uniware.core.api.cyclecount.UnblockShelvesResponse;
import com.uniware.core.api.dual.company.AutoCompletePutawayRequest;
import com.uniware.core.api.dual.company.AutoCompletePutawayResponse;
import com.uniware.core.api.dual.company.ReceiveReturnFromRetailRequest;
import com.uniware.core.api.dual.company.ReceiveReturnFromRetailResponse;
import com.uniware.core.api.dual.company.ReceiveReversePickupFromRetailRequest;
import com.uniware.core.api.dual.company.ReceiveReversePickupFromRetailResponse;
import com.uniware.core.api.dual.company.ReceiveShippingPackageFromWholesaleRequest;
import com.uniware.core.api.dual.company.ReceiveShippingPackageFromWholesaleResponse;
import com.uniware.core.api.facility.CreateFacilityRequest;
import com.uniware.core.api.facility.CreateFacilityResponse;
import com.uniware.core.api.inflow.AddEditVendorInvoiceItemRequest;
import com.uniware.core.api.inflow.AddEditVendorInvoiceItemResponse;
import com.uniware.core.api.inflow.AddItemToInflowReceiptRequest;
import com.uniware.core.api.inflow.AddItemToInflowReceiptResponse;
import com.uniware.core.api.inflow.CreateInflowReceiptRequest;
import com.uniware.core.api.inflow.CreateInflowReceiptResponse;
import com.uniware.core.api.inflow.CreateVendorCreditInvoiceRequest;
import com.uniware.core.api.inflow.CreateVendorCreditInvoiceResponse;
import com.uniware.core.api.inflow.GetInflowReceiptRequest;
import com.uniware.core.api.inflow.GetInflowReceiptResponse;
import com.uniware.core.api.inflow.GetInflowReceiptsRequest;
import com.uniware.core.api.inflow.GetInflowReceiptsResponse;
import com.uniware.core.api.inventory.AddOrEditItemLabelsRequest;
import com.uniware.core.api.inventory.AddOrEditItemLabelsResponse;
import com.uniware.core.api.inventory.GenerateItemLabelsRequest;
import com.uniware.core.api.inventory.GenerateItemLabelsResponse;
import com.uniware.core.api.inventory.GetInventorySnapshotRequest;
import com.uniware.core.api.inventory.GetInventorySnapshotResponse;
import com.uniware.core.api.inventory.GetItemTypeInventoryRequest;
import com.uniware.core.api.inventory.GetItemTypeInventoryResponse;
import com.uniware.core.api.inventory.InventoryAdjustmentRequest;
import com.uniware.core.api.inventory.InventoryAdjustmentResponse;
import com.uniware.core.api.inventory.MarkItemTypeQuantityFoundRequest;
import com.uniware.core.api.inventory.MarkItemTypeQuantityFoundResponse;
import com.uniware.core.api.inventory.MarkItemTypeQuantityNotFoundRequest;
import com.uniware.core.api.inventory.MarkItemtypeQuantityNotFoundResponse;
import com.uniware.core.api.inventory.SearchItemTypeNotFoundQuantityRequest;
import com.uniware.core.api.inventory.SearchItemTypeNotFoundQuantityResponse;
import com.uniware.core.api.invoice.CreateInvoiceRequest;
import com.uniware.core.api.invoice.CreateInvoiceResponse;
import com.uniware.core.api.invoice.CreateInvoiceWithDetailsRequest;
import com.uniware.core.api.invoice.CreateInvoiceWithDetailsResponse;
import com.uniware.core.api.invoice.CreateShippingPackageInvoiceRequest;
import com.uniware.core.api.invoice.GetInvoiceDetailsRequest;
import com.uniware.core.api.invoice.GetInvoiceDetailsResponse;
import com.uniware.core.api.invoice.GetInvoiceLabelRequest;
import com.uniware.core.api.invoice.GetInvoiceLabelResponse;
import com.uniware.core.api.item.GetItemDetailRequest;
import com.uniware.core.api.item.GetItemDetailResponse;
import com.uniware.core.api.item.GetItemTypeDetailRequest;
import com.uniware.core.api.item.GetItemTypeDetailResponse;
import com.uniware.core.api.jabong.JabongReceiveItemFromWholesaleRequest;
import com.uniware.core.api.jabong.JabongReceiveItemFromWholesaleResponse;
import com.uniware.core.api.jabong.JabongWholesaleHandleShipmentReturnRequest;
import com.uniware.core.api.jabong.JabongWholesaleHandleShipmentReturnResponse;
import com.uniware.core.api.ledger.BulkInventoryLedgerSummaryRequest;
import com.uniware.core.api.ledger.BulkInventoryLedgerSummaryResponse;
import com.uniware.core.api.ledger.FetchInventoryLedgerRequest;
import com.uniware.core.api.ledger.FetchInventoryLedgerResponse;
import com.uniware.core.api.ledger.FetchInventoryLedgerSummaryRequest;
import com.uniware.core.api.ledger.FetchInventoryLedgerSummaryResponse;
import com.uniware.core.api.material.AddItemToGatePassRequest;
import com.uniware.core.api.material.AddItemToGatePassResponse;
import com.uniware.core.api.material.CompleteGatePassRequest;
import com.uniware.core.api.material.CompleteGatePassResponse;
import com.uniware.core.api.material.CreateGatePassRequest;
import com.uniware.core.api.material.CreateGatePassResponse;
import com.uniware.core.api.material.DiscardGatePassRequest;
import com.uniware.core.api.material.DiscardGatePassResponse;
import com.uniware.core.api.material.EditGatePassRequest;
import com.uniware.core.api.material.EditGatePassResponse;
import com.uniware.core.api.material.GetGatepassScannableItemDetailsRequest;
import com.uniware.core.api.material.GetGatepassScannableItemDetailsResponse;
import com.uniware.core.api.material.RemoveItemFromGatePassRequest;
import com.uniware.core.api.material.RemoveItemFromGatePassResponse;
import com.uniware.core.api.packer.PickShippingPackageRequest;
import com.uniware.core.api.packer.PickShippingPackageResponse;
import com.uniware.core.api.party.CreateOrEditVendorItemTypeRequest;
import com.uniware.core.api.party.CreateOrEditVendorItemTypeResponse;
import com.uniware.core.api.party.CreateOrEditVendorRequest;
import com.uniware.core.api.party.CreateOrEditVendorResponse;
import com.uniware.core.api.party.CreateVendorDetailsRequest;
import com.uniware.core.api.party.CreateVendorDetailsResponse;
import com.uniware.core.api.party.CreateVendorItemTypeRequest;
import com.uniware.core.api.party.CreateVendorItemTypeResponse;
import com.uniware.core.api.party.CreateVendorRequest;
import com.uniware.core.api.party.CreateVendorResponse;
import com.uniware.core.api.party.EditVendorDetailsRequest;
import com.uniware.core.api.party.EditVendorDetailsResponse;
import com.uniware.core.api.party.EditVendorItemTypeRequest;
import com.uniware.core.api.party.EditVendorItemTypeResponse;
import com.uniware.core.api.party.EditVendorRequest;
import com.uniware.core.api.party.EditVendorResponse;
import com.uniware.core.api.party.GetVendorItemTypesRequest;
import com.uniware.core.api.party.GetVendorItemTypesResponse;
import com.uniware.core.api.party.WsGenericVendor;
import com.uniware.core.api.party.WsPartyContact;
import com.uniware.core.api.party.WsVendor;
import com.uniware.core.api.party.WsVendorAgreement;
import com.uniware.core.api.picker.EditPicklistRequest;
import com.uniware.core.api.picker.EditPicklistResponse;
import com.uniware.core.api.picker.GetPicklistDetailRequest;
import com.uniware.core.api.picker.GetPicklistDetailResponse;
import com.uniware.core.api.picker.SubmitPickBucketRequest;
import com.uniware.core.api.picker.SubmitPickBucketResponse;
import com.uniware.core.api.prices.EditChannelItemTypePriceRequest;
import com.uniware.core.api.prices.EditChannelItemTypePriceResponse;
import com.uniware.core.api.purchase.AddOrEditPurchaseOrderItemsRequest;
import com.uniware.core.api.purchase.AddOrEditPurchaseOrderItemsResponse;
import com.uniware.core.api.purchase.ApprovePurchaseOrderRequest;
import com.uniware.core.api.purchase.ApprovePurchaseOrderResponse;
import com.uniware.core.api.purchase.ClosePurchaseOrderRequest;
import com.uniware.core.api.purchase.ClosePurchaseOrderResponse;
import com.uniware.core.api.purchase.CreateASNRequest;
import com.uniware.core.api.purchase.CreateASNResponse;
import com.uniware.core.api.purchase.CreateApprovedPurchaseOrderRequest;
import com.uniware.core.api.purchase.CreateApprovedPurchaseOrderResponse;
import com.uniware.core.api.purchase.CreatePurchaseOrderRequest;
import com.uniware.core.api.purchase.CreatePurchaseOrderResponse;
import com.uniware.core.api.purchase.GetBackOrderItemsRequest;
import com.uniware.core.api.purchase.GetBackOrderItemsResponse;
import com.uniware.core.api.purchase.GetPurchaseOrderRequest;
import com.uniware.core.api.purchase.GetPurchaseOrderResponse;
import com.uniware.core.api.purchase.GetPurchaseOrdersRequest;
import com.uniware.core.api.purchase.GetPurchaseOrdersResponse;
import com.uniware.core.api.purchase.GetVendorPurchaseOrderDetailRequest;
import com.uniware.core.api.purchase.GetVendorPurchaseOrderDetailResponse;
import com.uniware.core.api.purchase.GetVendorPurchaseOrdersRequest;
import com.uniware.core.api.purchase.GetVendorPurchaseOrdersResponse;
import com.uniware.core.api.putaway.AddTraceableItemToTransferPutawayRequest;
import com.uniware.core.api.putaway.AddTraceableItemToTransferPutawayResponse;
import com.uniware.core.api.putaway.CompletePutawayRequest;
import com.uniware.core.api.putaway.CompletePutawayResponse;
import com.uniware.core.api.putaway.CreatePutawayListRequest;
import com.uniware.core.api.putaway.CreatePutawayListResponse;
import com.uniware.core.api.putaway.CreatePutawayRequest;
import com.uniware.core.api.putaway.CreatePutawayResponse;
import com.uniware.core.api.putaway.GetPutawayRequest;
import com.uniware.core.api.putaway.GetPutawayResponse;
import com.uniware.core.api.putaway.SplitPutawayItemRequest;
import com.uniware.core.api.putaway.SplitPutawayItemResponse;
import com.uniware.core.api.returns.CreateReversePickupRequest;
import com.uniware.core.api.returns.CreateReversePickupResponse;
import com.uniware.core.api.returns.MarkSaleOrderReturnedRequest;
import com.uniware.core.api.returns.MarkSaleOrderReturnedResponse;
import com.uniware.core.api.saleorder.AcceptSaleOrderItemAlternateRequest;
import com.uniware.core.api.saleorder.AcceptSaleOrderItemAlternateResponse;
import com.uniware.core.api.saleorder.CancelSaleOrderRequest;
import com.uniware.core.api.saleorder.CancelSaleOrderResponse;
import com.uniware.core.api.saleorder.CreateSaleOrderItemAlternateRequest;
import com.uniware.core.api.saleorder.CreateSaleOrderItemAlternateResponse;
import com.uniware.core.api.saleorder.CreateSaleOrderRequest;
import com.uniware.core.api.saleorder.CreateSaleOrderResponse;
import com.uniware.core.api.saleorder.EditSaleOrderAddressRequest;
import com.uniware.core.api.saleorder.EditSaleOrderAddressResponse;
import com.uniware.core.api.saleorder.EditSaleOrderItemMetadataRequest;
import com.uniware.core.api.saleorder.EditSaleOrderItemMetadataResponse;
import com.uniware.core.api.saleorder.EditSaleOrderMetadataRequest;
import com.uniware.core.api.saleorder.EditSaleOrderMetadataResponse;
import com.uniware.core.api.saleorder.GetSaleOrderRequest;
import com.uniware.core.api.saleorder.GetSaleOrderResponse;
import com.uniware.core.api.saleorder.HoldSaleOrderItemsRequest;
import com.uniware.core.api.saleorder.HoldSaleOrderItemsResponse;
import com.uniware.core.api.saleorder.HoldSaleOrderRequest;
import com.uniware.core.api.saleorder.HoldSaleOrderResponse;
import com.uniware.core.api.saleorder.ModifyPacketSaleOrderRequest;
import com.uniware.core.api.saleorder.ModifyPacketSaleOrderResponse;
import com.uniware.core.api.saleorder.SearchSaleOrderRequest;
import com.uniware.core.api.saleorder.SearchSaleOrderResponse;
import com.uniware.core.api.saleorder.SetSaleOrderPriorityRequest;
import com.uniware.core.api.saleorder.SetSaleOrderPriorityResponse;
import com.uniware.core.api.saleorder.UnblockSaleOrderItemsInventoryRequest;
import com.uniware.core.api.saleorder.UnblockSaleOrderItemsInventoryResponse;
import com.uniware.core.api.saleorder.UnholdSaleOrderItemsRequest;
import com.uniware.core.api.saleorder.UnholdSaleOrderItemsResponse;
import com.uniware.core.api.saleorder.UnholdSaleOrderRequest;
import com.uniware.core.api.saleorder.UnholdSaleOrderResponse;
import com.uniware.core.api.saleorder.VerifySaleOrderRequest;
import com.uniware.core.api.saleorder.VerifySaleOrderResponse;
import com.uniware.core.api.shipping.AddShipmentsToBatchRequest;
import com.uniware.core.api.shipping.AddShipmentsToBatchResponse;
import com.uniware.core.api.shipping.AddShippingPackagesToManifestRequest;
import com.uniware.core.api.shipping.AddShippingPackagesToManifestResponse;
import com.uniware.core.api.shipping.AllocateShippingProviderRequest;
import com.uniware.core.api.shipping.AllocateShippingProviderResponse;
import com.uniware.core.api.shipping.CloseShippingManifestRequest;
import com.uniware.core.api.shipping.CloseShippingManifestResponse;
import com.uniware.core.api.shipping.CompleteCustomizationForShippingPackageRequest;
import com.uniware.core.api.shipping.CompleteCustomizationForShippingPackageResponse;
import com.uniware.core.api.shipping.CreateInvoiceAndAllocateShippingProviderRequest;
import com.uniware.core.api.shipping.CreateInvoiceAndAllocateShippingProviderResponse;
import com.uniware.core.api.shipping.CreateInvoiceAndGenerateLabelRequest;
import com.uniware.core.api.shipping.CreateInvoiceAndGenerateLabelResponse;
import com.uniware.core.api.shipping.CreateShippingManifestRequest;
import com.uniware.core.api.shipping.CreateShippingManifestResponse;
import com.uniware.core.api.shipping.DispatchSaleOrderItemsRequest;
import com.uniware.core.api.shipping.DispatchSaleOrderItemsResponse;
import com.uniware.core.api.shipping.DispatchShippingPackageRequest;
import com.uniware.core.api.shipping.DispatchShippingPackageResponse;
import com.uniware.core.api.shipping.EditShippingPackageRequest;
import com.uniware.core.api.shipping.EditShippingPackageResponse;
import com.uniware.core.api.shipping.GetShippingManifestRequest;
import com.uniware.core.api.shipping.GetShippingManifestResponse;
import com.uniware.core.api.shipping.GetShippingPackageDetailRequest;
import com.uniware.core.api.shipping.GetShippingPackageDetailResponse;
import com.uniware.core.api.shipping.GetShippingPackagesRequest;
import com.uniware.core.api.shipping.GetShippingPackagesResponse;
import com.uniware.core.api.shipping.GetShippingProviderParametersRequest;
import com.uniware.core.api.shipping.GetShippingProviderParametersResponse;
import com.uniware.core.api.shipping.MarkSaleOrderItemsDeliveredRequest;
import com.uniware.core.api.shipping.MarkSaleOrderItemsDeliveredResponse;
import com.uniware.core.api.shipping.MergeShippingPackagesRequest;
import com.uniware.core.api.shipping.MergeShippingPackagesResponse;
import com.uniware.core.api.shipping.SearchShippingPackageRequest;
import com.uniware.core.api.shipping.SearchShippingPackageResponse;
import com.uniware.core.api.shipping.SplitShippingPackageRequest;
import com.uniware.core.api.shipping.SplitShippingPackageResponse;
import com.uniware.core.api.shipping.UpdateTrackingStatusRequest;
import com.uniware.core.api.shipping.UpdateTrackingStatusResponse;
import com.uniware.core.api.shipping.VerifySaleOrderItemsPODCodeRequest;
import com.uniware.core.api.shipping.VerifySaleOrderItemsPODCodeResponse;
import com.uniware.core.api.tax.GetTaxTypeConfigurationRequest;
import com.uniware.core.api.tax.GetTaxTypeConfigurationResponse;
import com.uniware.core.api.templatecustomization.GetCustomizedDummyTemplateRequest;
import com.uniware.core.api.templatecustomization.GetCustomizedDummyTemplateResponse;
import com.uniware.core.api.tenant.ChangeProductTypeRequest;
import com.uniware.core.api.tenant.ChangeProductTypeResponse;
import com.uniware.core.api.tenant.CreateTenantRequest;
import com.uniware.core.api.tenant.CreateTenantResponse;
import com.uniware.core.api.tenant.DeactivateTenantRequest;
import com.uniware.core.api.tenant.DeactivateTenantResponse;
import com.uniware.core.api.tenant.GetTenantTransactionDetailsRequest;
import com.uniware.core.api.tenant.GetTenantTransactionDetailsResponse;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.api.warehouse.EditFacilityRequest;
import com.uniware.core.api.warehouse.EditFacilityResponse;
import com.uniware.core.api.warehouse.SearchItemTypesRequest;
import com.uniware.core.api.warehouse.SearchItemTypesResponse;
import com.uniware.core.entity.Vendor;
import com.uniware.core.utils.UserContext;
import com.uniware.services.catalog.ICatalogService;
import com.uniware.services.configuration.TenantSystemConfiguration;
import com.uniware.services.customer.ICustomerService;
import com.uniware.services.customizations.impl.LenskartServiceImpl;
import com.uniware.services.cyclecount.ICycleCountService;
import com.uniware.services.dual.company.IDualCompanyService;
import com.uniware.services.inflow.IInflowService;
import com.uniware.services.inflow.IVendorInvoiceService;
import com.uniware.services.inventory.IInventoryService;
import com.uniware.services.invoice.IInvoiceService;
import com.uniware.services.item.IItemService;
import com.uniware.services.jabong.IJabongService;
import com.uniware.services.ledger.IInventoryLedgerService;
import com.uniware.services.lookup.ILookupService;
import com.uniware.services.material.IMaterialService;
import com.uniware.services.packer.IPackerService;
import com.uniware.services.picker.IPickerService;
import com.uniware.services.pricing.IPricingFacade;
import com.uniware.services.purchase.IPurchaseService;
import com.uniware.services.putaway.IPutawayService;
import com.uniware.services.reload.IReloadService;
import com.uniware.services.returns.IReturnsService;
import com.uniware.services.saleorder.ISaleOrderService;
import com.uniware.services.script.service.IScriptService;
import com.uniware.services.shipping.IDispatchService;
import com.uniware.services.shipping.IShippingAdminService;
import com.uniware.services.shipping.IShippingInvoiceService;
import com.uniware.services.shipping.IShippingProviderService;
import com.uniware.services.shipping.IShippingService;
import com.uniware.services.tax.ITaxTypeService;
import com.uniware.services.templateCustomization.ITemplateCustomizationService;
import com.uniware.services.tenant.ITenantInvoiceService;
import com.uniware.services.tenant.ITenantService;
import com.uniware.services.tenant.ITenantSetupService;
import com.uniware.services.vendor.IVendorService;
import com.uniware.services.warehouse.IFacilityService;
import com.uniware.web.security.annotation.UniwareEndPointAccess;

/**
 * @author Sunny
 */
@Controller
@Path("/services/rest/v1")
public class UniwareRESTServicesEndPoint {

    @Autowired
    private ISaleOrderService             saleOrderService;

    @Autowired
    private ICatalogService               catalogService;

    @Autowired
    private IDualCompanyService           dualCompanyService;

    @Autowired
    private IJabongService                jabongService;

    @Autowired
    private IPurchaseService              purchaseService;

    @Autowired
    private IInventoryService             inventoryService;

    @Autowired
    private IVendorService                vendorService;

    @Autowired
    private IShippingService              shippingService;

    @Autowired
    private ILookupService                lookupService;

    @Autowired
    private IReturnsService               returnsService;

    @Autowired
    private ITenantSetupService           tenantSetupService;

    @Autowired
    private IUsersService                 usersService;

    @Autowired
    private ICustomerService              customerService;

    @Autowired
    private IFacilityService              facilityService;

    @Autowired
    private IItemService                  itemService;

    @Autowired
    private IInflowService                inflowService;

    @Autowired
    private IDispatchService              dispatchService;

    @Autowired
    private IReloadService                reloadService;

    @Autowired
    private IShippingProviderService      shippingProviderService;

    @Autowired
    private IShippingAdminService         shippingAdminService;

    @Autowired
    private IMaterialService              materialService;

    @Autowired
    private LenskartServiceImpl           lenskartService;

    @Autowired
    private ITenantInvoiceService         tenantInvoiceService;

    @Autowired
    private IScriptService                scriptService;

    @Autowired
    private ITemplateCustomizationService templateCustomizationService;

    @Autowired
    private IPackerService                packerService;

    @Autowired
    private ITaxTypeService               taxTypeService;

    @Autowired
    private IInvoiceService               invoiceService;

    @Autowired
    private ITenantService                tenantService;

    @Autowired
    private IVendorInvoiceService         vendorInvoiceService;

    @Autowired
    private IPutawayService               putawayService;

    @Autowired
    private IPickerService                pickerService;

    @Autowired
    private IPricingFacade                pricingService;

    @Autowired
    private IShippingInvoiceService       iShippingInvoiceService;

    @Autowired
    private IExportService                exportService;

    @Autowired
    private ICycleCountService            cycleCountService;

    @Autowired
    private IShippingInvoiceService       shippingInvoiceService;

    @Autowired
    private IInventoryLedgerService       inventoryLedgerService;

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/oms/saleOrder/create")
    @POST
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.IMPORT_SALE_ORDERS)
    public CreateSaleOrderResponse createSaleOrder(CreateSaleOrderRequest request) {
        return saleOrderService.createSaleOrder(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/oms/saleOrder/cancel")
    @POST
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.SALE_ORDER_CANCEL_ITEM)
    public CancelSaleOrderResponse cancelSaleOrder(CancelSaleOrderRequest request) {
        return saleOrderService.cancelSaleOrder(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/oms/saleOrder/hold")
    @POST
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.SALE_ORDER_HOLD_UNHOLD)
    public HoldSaleOrderResponse holdSaleOrder(HoldSaleOrderRequest request) {
        return saleOrderService.holdSaleOrder(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/oms/saleOrder/holdSaleOrderItems")
    @POST
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.SALE_ORDER_HOLD_UNHOLD)
    public HoldSaleOrderItemsResponse holdSaleOrderItems(HoldSaleOrderItemsRequest request) {
        return saleOrderService.holdSaleOrderItems(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/oms/saleorder/get")
    @POST
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.SALE_ORDER_CANCEL_ITEM)
    public GetSaleOrderResponse getSaleOrder(GetSaleOrderRequest request) {
        return saleOrderService.getSaleOrder(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/catalog/itemType/create")
    @POST
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.ADMIN_CATALOG)
    public CreateItemTypeResponse createItemType(CreateItemTypeRequest request) {
        return catalogService.createItemType(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/catalog/itemType/edit")
    @POST
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.ADMIN_CATALOG)
    public EditItemTypeResponse editItemType(EditItemTypeRequest request) {
        return catalogService.editItemType(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/catalog/itemType/createOrEdit")
    @POST
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.ADMIN_CATALOG)
    public CreateOrEditItemTypeResponse createOrEditItemType(CreateOrEditItemTypeRequest request) {
        CreateOrEditItemTypeResponse response = catalogService.createOrEditItemType(request);
        return response;
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/catalog/facilityItemType/createOrEdit")
    @POST
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.ADMIN_CATALOG)
    public CreateOrEditFacilityItemTypeResponse createOrEditFacilityItemType(CreateOrEditFacilityItemTypeRequest request) {
        CreateOrEditFacilityItemTypeResponse response = catalogService.createOrEditFacilityItemType(request);
        return response;
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/system/facility/edit")
    @POST
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.CREATE_TENANT)
    public EditFacilityResponse editFacility(EditFacilityRequest request) {
        return facilityService.editFacility(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/oms/saleOrder/unhold")
    @POST
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.SALE_ORDER_HOLD_UNHOLD)
    public UnholdSaleOrderResponse unholdSaleOrder(UnholdSaleOrderRequest request) {
        return saleOrderService.unholdSaleOrder(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/oms/saleOrder/unholdSaleOrderItems")
    @POST
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.SALE_ORDER_HOLD_UNHOLD)
    public UnholdSaleOrderItemsResponse unholdSaleItemsOrder(UnholdSaleOrderItemsRequest request) {
        return saleOrderService.unholdSaleOrderItems(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/inventory/putaway/autoComplete")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.DUAL_COMPANY_API)
    public AutoCompletePutawayResponse autoCompletePutaway(AutoCompletePutawayRequest request) {
        return dualCompanyService.autoCompletePutaway(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/jabong/jabongReceiveItemFromWholesale")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.DUAL_COMPANY_API)
    public JabongReceiveItemFromWholesaleResponse jabongReceiveItemFromWholesale(JabongReceiveItemFromWholesaleRequest request) {
        return jabongService.receiveItemFromWholesale(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/jabong/jabongWholesaleHandleShipmentReturn")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.DUAL_COMPANY_API)
    public JabongWholesaleHandleShipmentReturnResponse jabongWholesaleHandleShipmentReturn(JabongWholesaleHandleShipmentReturnRequest request) {
        return jabongService.jabongWholesaleHandleShipmentReturn(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/oms/saleOrder/search")
    @POST
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.LOOKUP_SALE_ORDER)
    public SearchSaleOrderResponse searchSaleOrderRequest(SearchSaleOrderRequest request) {
        return saleOrderService.searchSaleOrders(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/oms/saleOrder/edit")
    @POST
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.SALE_ORDER_ADDRESS_EDIT)
    public EditSaleOrderAddressResponse editSaleOrder(EditSaleOrderAddressRequest request) {
        return saleOrderService.editSaleOrderAddress(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/dualCompany/receiveShippingPackageFromWholesale")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.DUAL_COMPANY_API)
    public ReceiveShippingPackageFromWholesaleResponse receiveShippingPackageFromWholesale(ReceiveShippingPackageFromWholesaleRequest request) {
        return dualCompanyService.receiveShippingPackageFromWholesale(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/inventory/inventorySnapshot/get")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.LOOKUP_INVENTORY)
    public GetInventorySnapshotResponse getInventorySnapshot(GetInventorySnapshotRequest request) {
        return inventoryService.getInventorySnapshot(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/inventory/adjust")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.IMPORT_INVENTORY_ADJUSTMENT)
    public InventoryAdjustmentResponse adjustInventory(InventoryAdjustmentRequest request) {
        ApiUser apiUser = usersService.getApiUserByUsername(UserContext.current().getApiUsername());
        User user = usersService.getUserByUsername(apiUser.getUser().getUsername());
        request.setUserId(user.getId());
        return inventoryService.adjustInventory(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/purchase/purchaseOrder/create")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.PROCUREMENT)
    public CreatePurchaseOrderResponse createPurchaseOrder(CreatePurchaseOrderRequest request) {
        User user = ConfigurationManager.getInstance().getConfiguration(TenantSystemConfiguration.class).getSystemUser();
        request.setUserId(user.getId());
        return purchaseService.createPurchaseOrder(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/purchase/purchaseOrderItems/add")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.PROCUREMENT)
    public AddOrEditPurchaseOrderItemsResponse addPurchaseOrderItems(AddOrEditPurchaseOrderItemsRequest request) {
        return purchaseService.addOrEditPurchaseOrderItems(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/purchase/purchaseOrder/approve")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.PROCUREMENT)
    public ApprovePurchaseOrderResponse approvePurchaseOrder(ApprovePurchaseOrderRequest request) {
        User user = ConfigurationManager.getInstance().getConfiguration(TenantSystemConfiguration.class).getSystemUser();
        request.setUserId(user.getId());
        return purchaseService.approvePurchaseOrder(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/product/category/addOrEdit")
    @POST
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.ADMIN_CATALOG)
    public CreateOrEditCategoryResponse createOrEditCategory(CreateOrEditCategoryRequest request) {
        return catalogService.createOrEditCategory(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/purchase/vendor/create")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.VENDOR_CREATE)
    public CreateVendorDetailsResponse createVendorDetails(CreateVendorDetailsRequest request) {
        CreateVendorDetailsResponse response = new CreateVendorDetailsResponse();
        WsVendor wsVendor = request.getWsVendor();
        WsGenericVendor wsGenericVendor = new WsGenericVendor(wsVendor);
        wsGenericVendor.setShippingAddress(request.getShippingAddress());
        wsGenericVendor.setBillingAddress(request.getBillingAddress());
        if (request.getAgreement() != null) {
            List<WsVendorAgreement> vendorAgreements = new ArrayList<WsVendorAgreement>();
            vendorAgreements.add(request.getAgreement());
            wsGenericVendor.setVendorAgreements(vendorAgreements);
        }
        for (WsPartyContact partyContact : request.getContacts()) {
            wsGenericVendor.getPartyContacts().add(partyContact);
        }
        CreateVendorRequest createVendorRequest = new CreateVendorRequest();
        createVendorRequest.setVendor(wsGenericVendor);
        CreateVendorResponse createVendorResponse = vendorService.createVendor(createVendorRequest);
        if (!createVendorResponse.isSuccessful()) {
            response.addErrors(createVendorResponse.getErrors());
        } else {
            response.setSuccessful(true);
        }
        return response;
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/purchase/vendor/edit")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.VENDOR_CREATE)
    public EditVendorDetailsResponse editVendorDetails(EditVendorDetailsRequest request) {
        EditVendorDetailsResponse response = new EditVendorDetailsResponse();
        WsVendor wsVendor = request.getWsVendor();
        WsGenericVendor wsGenericVendor = new WsGenericVendor(wsVendor);
        wsGenericVendor.setShippingAddress(request.getShippingAddress());
        wsGenericVendor.setBillingAddress(request.getBillingAddress());
        if (request.getAgreement() != null) {
            List<WsVendorAgreement> vendorAgreements = new ArrayList<WsVendorAgreement>();
            vendorAgreements.add(request.getAgreement());
            wsGenericVendor.setVendorAgreements(vendorAgreements);
        }
        for (WsPartyContact partyContact : request.getContacts()) {
            wsGenericVendor.getPartyContacts().add(partyContact);
        }
        EditVendorRequest editVendorRequest = new EditVendorRequest();
        editVendorRequest.setVendor(wsGenericVendor);
        EditVendorResponse editVendorResponse = vendorService.editVendor(editVendorRequest);
        if (!editVendorResponse.isSuccessful()) {
            response.addErrors(editVendorResponse.getErrors());
        } else {
            response.setSuccessful(true);
        }
        return response;
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/purchase/purchaseOrder/createApproved")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.PROCUREMENT)
    public CreateApprovedPurchaseOrderResponse createApprovedPurchaseOrder(CreateApprovedPurchaseOrderRequest request) {
        User user = ConfigurationManager.getInstance().getConfiguration(TenantSystemConfiguration.class).getSystemUser();
        request.setUserId(user.getId());
        return purchaseService.createApprovedPurchaseOrder(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/dualCompany/receiveReturnFromRetail")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.DUAL_COMPANY_API)
    public ReceiveReturnFromRetailResponse receiveReturnFromRetail(ReceiveReturnFromRetailRequest request) {
        return dualCompanyService.receiveReturnFromRetail(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/oms/saleOrder/createSaleOrderItemAlternate")
    @POST
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.SALE_ORDER_ALTERNATE_ACCEPT)
    public CreateSaleOrderItemAlternateResponse createSaleOrderItemAlternate(CreateSaleOrderItemAlternateRequest request) {
        request.setUserId(ConfigurationManager.getInstance().getConfiguration(TenantSystemConfiguration.class).getSystemUser().getId());
        return saleOrderService.createSaleOrderItemAlternate(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/oms/saleOrder/acceptSaleOrderItemAlternate")
    @POST
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.SALE_ORDER_ALTERNATE_ACCEPT)
    public AcceptSaleOrderItemAlternateResponse acceptSaleOrderItemAlternate(AcceptSaleOrderItemAlternateRequest request) {
        return saleOrderService.acceptSaleOrderItemAlternate(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/purchase/vendorItemType/create")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.VENDOR_CATALOG)
    public CreateVendorItemTypeResponse createVendorItemType(CreateVendorItemTypeRequest request) {
        return vendorService.createVendorItemType(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/oms/shippingManifest/get")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.SHIPPING)
    public GetShippingManifestResponse getShippingManifest(GetShippingManifestRequest request) {
        return dispatchService.getShippingManifest(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/product/itemType/search")
    @POST
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.ADMIN_CATALOG)
    public SearchItemTypesResponse searchItemType(SearchItemTypesRequest request) {
        return catalogService.searchItemTypes(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/product/itemType/searchBySku")
    @POST
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.ADMIN_CATALOG)
    public GetItemTypeDetailResponse searchItemTypeBySku(GetItemTypeDetailRequest request) {
        return lookupService.getItemTypeDetails(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/oms/reversePickup/create")
    @POST
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.REVERSE_PICKUP_CREATE)
    public CreateReversePickupResponse createReversePickup(CreateReversePickupRequest request) {
        return returnsService.createReversePickup(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/oms/saleOrder/editSaleOrderMetadata")
    @POST
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.SALE_ORDER_METADATA_EDIT)
    public EditSaleOrderMetadataResponse editSaleOrderMetadata(EditSaleOrderMetadataRequest request) {
        return saleOrderService.editSaleOrderMetadata(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/system/tenant/create")
    @POST
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.CREATE_TENANT)
    public CreateTenantResponse createTenant(CreateTenantRequest request) {
        ApiUser apiUser = usersService.getApiUserByUsername(UserContext.current().getApiUsername());
        User user = usersService.getUserByUsername(apiUser.getUser().getUsername());
        request.setCurrentUserId(user.getId());
        return tenantSetupService.createTenantRequestHandler(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/system/facility/create")
    @POST
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.CREATE_TENANT)
    public CreateFacilityResponse createFacility(CreateFacilityRequest request) {
        ApiUser apiUser = usersService.getApiUserByUsername(UserContext.current().getApiUsername());
        User user = usersService.getUserByUsername(apiUser.getUser().getUsername());
        request.setUsername(user.getUsername());
        return facilityService.createFacility(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/purchase/getVendorPurchaseOrders")
    @POST
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.VENDOR_API)
    public GetVendorPurchaseOrdersResponse getVendorPurchaseOrders(GetVendorPurchaseOrdersRequest request) {
        ApiUser apiUser = usersService.getApiUserByUsername(UserContext.current().getApiUsername());
        User user = usersService.getUserByUsername(apiUser.getUser().getUsername());
        List<Integer> vendorIds = new ArrayList<Integer>();
        for (Vendor vendor : user.getVendors()) {
            vendorIds.add(vendor.getId());
        }
        request.setVendorIds(vendorIds);
        return purchaseService.getVendorPurchaseOrders(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/purchase/getVendorPurchaseOrderDetail")
    @POST
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.VENDOR_API)
    public GetVendorPurchaseOrderDetailResponse getVendorPurchaseOrderDetail(GetVendorPurchaseOrderDetailRequest request) {
        ApiUser apiUser = usersService.getApiUserByUsername(UserContext.current().getApiUsername());
        User user = usersService.getUserByUsername(apiUser.getUser().getUsername());
        List<Integer> vendorIds = new ArrayList<Integer>();
        for (Vendor vendor : user.getVendors()) {
            vendorIds.add(vendor.getId());
        }
        request.setVendorIds(vendorIds);
        return purchaseService.getVendorPurchaseOrderDetail(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/oms/customer/create")
    @POST
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.VENDOR_CREATE)
    public CreateCustomerResponse createCustomer(CreateCustomerRequest request) {
        String customerCode = request.getCustomer().getCode();
        if (request.getCustomer().getBillingAddress() != null) {
            request.getCustomer().getBillingAddress().setPartyCode(customerCode);
        }
        if (request.getCustomer().getShippingAddress() != null) {
            request.getCustomer().getShippingAddress().setPartyCode(customerCode);
        }
        if (request.getCustomer().getPartyContacts() != null) {
            for (WsPartyContact partyContact : request.getCustomer().getPartyContacts()) {
                partyContact.setPartyCode(customerCode);
            }
        }
        return customerService.createCustomer(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/oms/customer/edit")
    @POST
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.VENDOR_CREATE)
    public EditCustomerResponse editCustomer(EditCustomerRequest request) {
        String customerCode = request.getCustomer().getCode();
        if (request.getCustomer().getBillingAddress() != null) {
            request.getCustomer().getBillingAddress().setPartyCode(customerCode);
        }
        if (request.getCustomer().getShippingAddress() != null) {
            request.getCustomer().getShippingAddress().setPartyCode(customerCode);
        }
        if (request.getCustomer().getPartyContacts() != null) {
            for (WsPartyContact partyContact : request.getCustomer().getPartyContacts()) {
                partyContact.setPartyCode(customerCode);
            }
        }
        return customerService.editCustomer(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/oms/updateShipmentTrackingStatus")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.UPDATE_TRACKING_STATUS)
    public UpdateTrackingStatusResponse updateShipmentTrackingStatus(UpdateTrackingStatusRequest request) {
        return shippingService.updateTrackingStatus(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/purchase/vendorItemType/createOrEdit")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.VENDOR_CATALOG)
    public CreateOrEditVendorItemTypeResponse createOrEditVendorItemType(CreateOrEditVendorItemTypeRequest request) {
        ApiUser apiUser = usersService.getApiUserByUsername(UserContext.current().getApiUsername());
        request.setUserId(apiUser.getUser().getId());
        return vendorService.createOrEditVendorItemType(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/purchase/purchaseOrder/close")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.PROCUREMENT)
    public ClosePurchaseOrderResponse closePurchaseOrder(ClosePurchaseOrderRequest request) {
        User user = ConfigurationManager.getInstance().getConfiguration(TenantSystemConfiguration.class).getSystemUser();
        request.setUserId(user.getId());
        return purchaseService.closePurchaseOrder(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/purchase/getVendorBackOrderItems")
    @POST
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.VENDOR_API)
    public GetBackOrderItemsResponse getVendorBackOrderItems(GetBackOrderItemsRequest request) {
        ApiUser apiUser = usersService.getApiUserByUsername(UserContext.current().getApiUsername());
        Set<Vendor> vendors = apiUser.getUser().getVendors();
        if (vendors.isEmpty()) {
            GetBackOrderItemsResponse response = new GetBackOrderItemsResponse();
            response.addError(new WsError(WsResponseCode.INVALID_USER_ID.code(), WsResponseCode.INVALID_USER_ID.message()));
            return response;
        } else {
            Vendor vendor = vendors.iterator().next();
            request.setVendorId(vendor.getId());
            UserContext.current().setFacility(vendor.getFacility());
            return purchaseService.getBackOrderItems(request);
        }
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/purchase/vendorItemType/edit")
    @POST
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.VENDOR_API)
    public EditVendorItemTypeResponse editVendorItemType(EditVendorItemTypeRequest request) {
        ApiUser apiUser = usersService.getApiUserByUsername(UserContext.current().getApiUsername());
        if (!apiUser.getUser().getVendors().isEmpty()) {
            Vendor vendor = vendorService.getVendorById(apiUser.getUser().getVendors().iterator().next().getId());
            request.getVendorItemType().setVendorCode(vendor.getCode());
            UserContext.current().setFacility(vendor.getFacility());
            request.setUserId(apiUser.getUser().getId());
            request.setActionType("API");
            return vendorService.editVendorItemType(request);
        } else {
            EditVendorItemTypeResponse response = new EditVendorItemTypeResponse();
            response.addError(new WsError(WsResponseCode.INVALID_USER_ID.code(), WsResponseCode.INVALID_USER_ID.message()));
            return response;
        }
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/purchase/vendor/createOrEdit")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.MINIMAL)
    public CreateOrEditVendorResponse createOrEditVendor(CreateOrEditVendorRequest request) {
        return vendorService.createOrEditVendor(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/product/item/get")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.LOOKUP_SALE_ORDER)
    public GetItemDetailResponse getItemDetail(GetItemDetailRequest request) {
        return itemService.getItemDetail(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/oms/saleOrder/editSaleOrderItemMetadata")
    @POST
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.SALE_ORDER_METADATA_EDIT)
    public EditSaleOrderItemMetadataResponse editSaleOrderItemMetadata(EditSaleOrderItemMetadataRequest request) {
        return saleOrderService.editSaleOrderItemMetadata(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/purchase/purchaseOrder/getPurchaseOrders")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.PROCUREMENT)
    public GetPurchaseOrdersResponse getPurchaseOrders(GetPurchaseOrdersRequest request) {
        return purchaseService.getPurchaseOrders(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/purchase/purchaseOrder/getPurchaseOrderDetails")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.PROCUREMENT)
    public GetPurchaseOrderResponse getPurchaseOrderDetails(GetPurchaseOrderRequest request) {
        return purchaseService.getPurchaseOrderDetail(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/purchase/inflowReceipt/getInflowReceipts")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.INFLOW_GRN_SEARCH)
    public GetInflowReceiptsResponse getInflowReceipts(GetInflowReceiptsRequest request) {
        return inflowService.getInflowReceipts(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/purchase/inflowReceipt/getInflowReceipt")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.INFLOW_GRN_SEARCH)
    public GetInflowReceiptResponse getInflowReceipts(GetInflowReceiptRequest request) {
        return inflowService.getInflowReceipt(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/system/apiUser/getLoginToken")
    @POST
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.MINIMAL)
    public GetLoginTokenResponse getLoginToken(GetLoginTokenRequest request) {
        ApiUser apiUser = usersService.getApiUserByUsername(UserContext.current().getApiUsername());
        request.setUserId(apiUser.getUser().getId());
        return usersService.getLoginToken(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/system/script/edit")
    @POST
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.MINIMAL)
    public EditScriptConfigResponse editScriptConfig(EditScriptConfigRequest request) {
        return scriptService.editScriptConfig(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/system/configuration/reload")
    @POST
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.MINIMAL)
    public ReloadConfigurationResponse reloadConfiguration(ReloadConfigurationRequest request) {
        return reloadService.reload(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/oms/shippingPackage/getShippingPackages")
    @POST
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.MINIMAL)
    public GetShippingPackagesResponse getShippingPackages(GetShippingPackagesRequest request) {
        GetShippingPackagesResponse response = new GetShippingPackagesResponse();
        ApiUser apiUser = usersService.getApiUserByUsername(UserContext.current().getApiUsername());
        User user = apiUser.getUser();
        if (user.getUserFacilities().size() == 1) {
            UserContext.current().setFacility(user.getUserFacilities().iterator().next());
            response = shippingService.getShippingPackages(request);
        } else {
            response.addError(new WsError(WsResponseCode.INVALID_API_REQUEST.message()));
        }
        return response;
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/oms/shippingPackage/getShippingPackageDetails")
    @POST
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.MINIMAL)
    public GetShippingPackageDetailResponse getShippingPackageDetails(GetShippingPackageDetailRequest request) {
        GetShippingPackageDetailResponse response = new GetShippingPackageDetailResponse();
        ApiUser apiUser = usersService.getApiUserByUsername(UserContext.current().getApiUsername());
        User user = apiUser.getUser();
        if (user.getUserFacilities().size() == 1) {
            UserContext.current().setFacility(user.getUserFacilities().iterator().next());
            response = shippingService.getShippingPackageDetails(request);
        } else {
            response.addError(new WsError(WsResponseCode.INVALID_API_REQUEST.message()));
        }
        return response;
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/oms/shippingPackage/createInvoiceAndGenerateLabel")
    @POST
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.CUSTOMER_INVOICE)
    public CreateInvoiceAndGenerateLabelResponse createInvoiceAndGenerateLabel(CreateInvoiceAndGenerateLabelRequest request) {
        ApiUser apiUser = usersService.getApiUserByUsername(UserContext.current().getApiUsername());
        User user = apiUser.getUser();
        CreateInvoiceAndGenerateLabelResponse response = new CreateInvoiceAndGenerateLabelResponse();
        if (user.getUserFacilities().size() == 1) {
            UserContext.current().setFacility(user.getUserFacilities().iterator().next());
            request.setUserId(user.getId());
            response = shippingProviderService.createInvoiceAndGenerateLabel(request);
        } else {
            response.addError(new WsError(WsResponseCode.INVALID_API_REQUEST.message()));
        }
        return response;
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/oms/shippingPackage/createInvoiceAndAllocateShippingProvider")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.CUSTOMER_INVOICE)
    public CreateInvoiceAndAllocateShippingProviderResponse createInvoiceAndAllocateShippingProvider(CreateInvoiceAndAllocateShippingProviderRequest request) {
        ApiUser apiUser = usersService.getApiUserByUsername(UserContext.current().getApiUsername());
        User user = apiUser.getUser();
        request.setUserId(user.getId());
        return shippingProviderService.createInvoiceAndAllocateShippingProvider(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/oms/shippingPackage/allocateShippingProvider")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.CUSTOMER_INVOICE)
    public AllocateShippingProviderResponse allocateShippingProvider(AllocateShippingProviderRequest request) {
        return shippingProviderService.allocateShippingProvider(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/oms/shippingManifest/create")
    @POST
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.MINIMAL)
    public CreateShippingManifestResponse createShippingManifest(CreateShippingManifestRequest request) {
        CreateShippingManifestResponse response = new CreateShippingManifestResponse();
        ApiUser apiUser = usersService.getApiUserByUsername(UserContext.current().getApiUsername());
        User user = apiUser.getUser();
        if (user.getUserFacilities().size() == 1) {
            UserContext.current().setFacility(user.getUserFacilities().iterator().next());
            request.setUserId(user.getId());
            response = dispatchService.createShippingManifest(request);
        } else {
            response.addError(new WsError(WsResponseCode.INVALID_API_REQUEST.message()));
        }
        return response;
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/oms/shippingPackage/dispatch")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.MINIMAL)
    public DispatchShippingPackageResponse dispatchShippingPackage(DispatchShippingPackageRequest request) {
        return dispatchService.dispatchReadyToShipShippingPackage(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/oms/shippingManifest/addShippingPackage")
    @POST
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.MINIMAL)
    public AddShippingPackagesToManifestResponse addShippingPackageToManifestRequest(AddShippingPackagesToManifestRequest request) {
        AddShippingPackagesToManifestResponse response = new AddShippingPackagesToManifestResponse();
        ApiUser apiUser = usersService.getApiUserByUsername(UserContext.current().getApiUsername());
        User user = apiUser.getUser();
        if (user.getUserFacilities().size() == 1) {
            UserContext.current().setFacility(user.getUserFacilities().iterator().next());
            response = dispatchService.addShippingPackagesToManifest(request);
        } else {
            response.addError(new WsError(WsResponseCode.INVALID_API_REQUEST.message()));
        }
        return response;
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/oms/shippingManifest/close")
    @POST
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.MINIMAL)
    public CloseShippingManifestResponse closeShippingManifest(CloseShippingManifestRequest request) {
        CloseShippingManifestResponse response = new CloseShippingManifestResponse();
        ApiUser apiUser = usersService.getApiUserByUsername(UserContext.current().getApiUsername());
        User user = apiUser.getUser();
        if (user.getUserFacilities().size() == 1) {
            UserContext.current().setFacility(user.getUserFacilities().iterator().next());
            response = dispatchService.closeShippingManifest(request);
        } else {
            response.addError(new WsError(WsResponseCode.INVALID_API_REQUEST.message()));
        }
        return response;
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/oms/saleOrder/getServiceability")
    @POST
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.EXPORT_LOCATIONS)
    public GetServiceabilityResponse getServiceability(GetServiceabilityRequest request) {
        return shippingProviderService.getServiceability(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/product/item/addOrEditItemLabels")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.INFLOW_GRN_CREATE_LABELS)
    public AddOrEditItemLabelsResponse createSaleOrder(AddOrEditItemLabelsRequest request) {
        return inventoryService.addOrEditItemLabels(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/purchase/gatepass/edit")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.MATERIAL_MANAGEMENT)
    public EditGatePassResponse editGatePass(EditGatePassRequest request) {
        return materialService.editGatePass(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/purchase/vendorItemType/getVendorItemTypes")
    @POST
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.VENDOR_API)
    public GetVendorItemTypesResponse getVendorItemTypes(GetVendorItemTypesRequest request) {
        ApiUser apiUser = usersService.getApiUserByUsername(UserContext.current().getApiUsername());
        if (!apiUser.getUser().getVendors().isEmpty()) {
            Vendor vendor = vendorService.getVendorById(apiUser.getUser().getVendors().iterator().next().getId());
            request.setVendorCode(vendor.getCode());
            UserContext.current().setFacility(vendor.getFacility());
            return vendorService.getVendorItemTypes(request);
        } else {
            GetVendorItemTypesResponse response = new GetVendorItemTypesResponse();
            response.addError(new WsError(WsResponseCode.INVALID_USER_ID.code(), WsResponseCode.INVALID_USER_ID.message()));
            return response;
        }
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/dualCompany/receiveReversePickupFromRetail")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.REVERSE_PICKUP_CREATE)
    public ReceiveReversePickupFromRetailResponse receiveReversePickupFromRetail(ReceiveReversePickupFromRetailRequest request) {
        ApiUser apiUser = usersService.getApiUserByUsername(UserContext.current().getApiUsername());
        request.setUserId(apiUser.getUser().getId());
        return dualCompanyService.receiveReversePickupFromRetail(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/oms/shippingPackage/split")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.SHIPPING)
    public SplitShippingPackageResponse splitShippingPackage(SplitShippingPackageRequest request) {
        ApiUser apiUser = usersService.getApiUserByUsername(UserContext.current().getApiUsername());
        request.setUserId(apiUser.getUser().getId());
        return shippingService.splitShippingPackage(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/oms/saleOrder/verify")
    @POST
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.VERIFY_PENDING_ORDERS)
    public VerifySaleOrderResponse verifySaleOrder(VerifySaleOrderRequest request) {
        return saleOrderService.verifySaleOrder(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/product/item/markItemDamagedOutboundQC")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.SHIPPING)
    public MarkItemDamagedOutboundQCResponse verifySaleOrder(MarkItemDamagedOutboundQCRequest request) {
        return lenskartService.markItemDamagedOutboundQC(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/system/tenant/getTenantTransactionDetails")
    @POST
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.MINIMAL)
    public GetTenantTransactionDetailsResponse getTenantTransactionDetails(GetTenantTransactionDetailsRequest request) {
        return tenantInvoiceService.getTenantTransactionDetails(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/purchase/asn/create")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.ASN_CREATE)
    public CreateASNResponse createASN(CreateASNRequest request) {
        return purchaseService.createASN(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/system/template/getCustomizedDummyTemplate")
    @POST
    @UniwareEndPointAccess(level = Level.GLOBAL, accessResource = Name.ADMIN_TEMPLATE)
    public GetCustomizedDummyTemplateResponse getCustomizedDummyTemplate(GetCustomizedDummyTemplateRequest request) {
        return templateCustomizationService.getCustomizedDummyTemplate(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/oms/shippingPackage/edit")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.IMPORT_SALE_ORDERS)
    public EditShippingPackageResponse editShippingPackage(EditShippingPackageRequest request) {
        return shippingService.editShippingPackage(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/purchase/vendor/create")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.VENDOR_CREATE)
    public CreateVendorResponse createVendor(CreateVendorRequest request) {
        return vendorService.createVendor(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/purchase/vendor/edit")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.VENDOR_CREATE)
    public EditVendorResponse editVendor(EditVendorRequest request) {
        return vendorService.editVendor(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/oms/saleOrder/setPriority")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.MINIMAL)
    public SetSaleOrderPriorityResponse setSaleOrderPriority(SetSaleOrderPriorityRequest request) {
        return saleOrderService.setSaleOrderPriority(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/purchase/gatepass/create")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.MATERIAL_MANAGEMENT)
    public CreateGatePassResponse createGatePass(CreateGatePassRequest request) {
        ApiUser apiUser = usersService.getApiUserByUsername(UserContext.current().getApiUsername());
        request.setUserId(apiUser.getUser().getId());
        return materialService.createGatePass(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/purchase/gatepass/addItem")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.MATERIAL_MANAGEMENT)
    public AddItemToGatePassResponse addItemToGatepass(AddItemToGatePassRequest request) {
        return materialService.addItemToGatePass(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/purchase/gatepass/complete")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.MATERIAL_MANAGEMENT)
    public CompleteGatePassResponse completeGatepass(CompleteGatePassRequest request) {
        ApiUser apiUser = usersService.getApiUserByUsername(UserContext.current().getApiUsername());
        request.setUserId(apiUser.getUser().getId());
        return materialService.completeGatePass(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/product/item/generateItemLabels")
    @POST
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.VENDOR_API)
    public GenerateItemLabelsResponse generateItemLabels(GenerateItemLabelsRequest request) {
        ApiUser apiUser = usersService.getApiUserByUsername(UserContext.current().getApiUsername());
        if (apiUser.getUser().getVendors().isEmpty()) {
            GenerateItemLabelsResponse response = new GenerateItemLabelsResponse();
            response.addError(new WsError(WsResponseCode.INVALID_USER_ID.code(), "No valid user found"));
            return response;
        } else {
            Vendor vendor = vendorService.getVendorById(apiUser.getUser().getVendors().iterator().next().getId());
            request.setVendorId(vendor.getId());
            UserContext.current().setFacility(vendor.getFacility());
            return inventoryService.generateItemLabels(request);
        }
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/oms/shippingPackage/pick")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.MINIMAL)
    public PickShippingPackageResponse pickShippingPackage(PickShippingPackageRequest request) {
        return packerService.pickShippingPackage(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/oms/saleOrder/unblockSaleOrderItemsInventory")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.SALE_ORDER_HOLD_UNHOLD)
    public UnblockSaleOrderItemsInventoryResponse unblockSaleOrderItemsInventory(UnblockSaleOrderItemsInventoryRequest request) {
        return saleOrderService.unblockSaleOrderItemsInventory(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/oms/shippingPackage/modify")
    @POST
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.SHIPPING)
    public ModifyPacketSaleOrderResponse modifyPacket(ModifyPacketSaleOrderRequest request) {
        return saleOrderService.modifySaleOrderItems(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/product/itemTypeInventory/get")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.LOOKUP_INVENTORY)
    public GetItemTypeInventoryResponse getItemTypeInventory(GetItemTypeInventoryRequest request) {
        return lookupService.getItemTypeInventory(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/system/taxType/getTaxTypeConfigurations")
    @POST
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.MINIMAL)
    public GetTaxTypeConfigurationResponse getTaxTypeConfigurations(GetTaxTypeConfigurationRequest request) {
        return taxTypeService.getTaxTypeConfigurations(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/oms/shippingPackage/search")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.LOOKUP_SHIPPING_PACKAGE)
    public SearchShippingPackageResponse searchShippingPackage(SearchShippingPackageRequest request) {
        return shippingService.searchShippingPackages(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/oms/shippingPackage/createInvoice")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.CUSTOMER_INVOICE)
    public CreateInvoiceResponse createInvoice(CreateInvoiceRequest request) {
        ApiUser apiUser = usersService.getApiUserByUsername(UserContext.current().getApiUsername());
        request.setUserId(apiUser.getUser().getId());
        request.setCommitBlockedInventory(true);
        CreateShippingPackageInvoiceRequest createShippingPackageInvoiceRequest = new CreateShippingPackageInvoiceRequest(request);
        CreateInvoiceResponse invoiceResponse = iShippingInvoiceService.createShippingPackageInvoice(createShippingPackageInvoiceRequest);
        return invoiceResponse;
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/oms/shippingPackage/getInvoiceLabel")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.CUSTOMER_INVOICE)
    public GetInvoiceLabelResponse getInvoiceLabel(GetInvoiceLabelRequest request) {
        return iShippingInvoiceService.getInvoiceLabel(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/purchase/inflowReceipt/create")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.INFLOW_GRN_CREATE)
    public CreateInflowReceiptResponse createInflowReceipt(CreateInflowReceiptRequest request) {
        ApiUser apiUser = usersService.getApiUserByUsername(UserContext.current().getApiUsername());
        request.getWsGRN().setUserId(apiUser.getUser().getId());
        return inflowService.createInflowReceipt(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/purchase/inflowReceipt/addItem")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.INFLOW_GRN_CREATE)
    public AddItemToInflowReceiptResponse addItemToInflowReceipt(AddItemToInflowReceiptRequest request) {
        return inflowService.addItemToInflowReceipt(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/system/tenant/deactivate")
    @POST
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.CREATE_TENANT)
    public DeactivateTenantResponse deactivateTenant(DeactivateTenantRequest request) {
        return tenantService.deactivateTenant(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/oms/shippingPackage/addShipmentsToBatch")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.SHIPMENT_BATCH)
    public AddShipmentsToBatchResponse addShipmentsToBatch(AddShipmentsToBatchRequest request) {
        return shippingService.addShipmentsToBatch(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/purchase/vendor/createVendorCreditInvoice")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.VENDOR_INVOICE)
    public CreateVendorCreditInvoiceResponse createVendorCreditInvoice(CreateVendorCreditInvoiceRequest request) {
        ApiUser apiUser = usersService.getApiUserByUsername(UserContext.current().getApiUsername());
        request.setUserId(apiUser.getUser().getId());
        return vendorInvoiceService.createCreditVendorInvoice(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/purchase/vendor/addEditVendorInvoiceItem")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.VENDOR_INVOICE)
    public AddEditVendorInvoiceItemResponse addEditVendorInvoiceItem(AddEditVendorInvoiceItemRequest request) {
        return vendorInvoiceService.addEditVendorInvoiceItem(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/system/tenant/upgrade")
    @POST
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.CREATE_TENANT)
    public ChangeProductTypeResponse upgradeTenant(ChangeProductTypeRequest request) {
        return tenantService.changeProductType(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/shipping/shippingProvider/getShippingProviderParameters")
    @POST
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.MINIMAL)
    public GetShippingProviderParametersResponse getShippingProviderParameters(GetShippingProviderParametersRequest request) {
        return shippingAdminService.getShippingProviderConnectorParameter(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/catalog/itemType/get")
    @POST
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.ADMIN_CATALOG)
    public GetItemTypeResponse getItemType(@RequestPayload GetItemTypeRequest request) {
        return catalogService.getItemType(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/createInvoiceWithDetails")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.MINIMAL)
    public CreateInvoiceWithDetailsResponse createInvoiceWithDetails(CreateInvoiceWithDetailsRequest request) {
        ApiUser apiUser = usersService.getApiUserByUsername(UserContext.current().getApiUsername());
        User user = usersService.getUserByUsername(apiUser.getUser().getUsername());
        request.setUserId(user.getId());
        return iShippingInvoiceService.createInvoiceWithDetails(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/saleOrderItem/verifyPOD")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.STORE_PICKUP)
    public VerifySaleOrderItemsPODCodeResponse verifySaleOrderItemsPODCode(VerifySaleOrderItemsPODCodeRequest request) {
        return dispatchService.verifySaleOrderItemsPODCode(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/saleOrderItem/dispatch")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.MINIMAL)
    public DispatchSaleOrderItemsResponse dispatchSaleOrderItems(DispatchSaleOrderItemsRequest request) {
        return dispatchService.dispatchSaleOrderItems(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/saleOrderItem/markDelivered")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.SHIPPING)
    public MarkSaleOrderItemsDeliveredResponse markSaleOrderItemsDelivered(MarkSaleOrderItemsDeliveredRequest request) {
        return dispatchService.markSaleOrderItemsDelivered(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/saleOrder/markReturned")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.SALE_ORDER_RETURN)
    public MarkSaleOrderReturnedResponse markSaleOrderReturned(MarkSaleOrderReturnedRequest request) {
        ApiUser apiUser = usersService.getApiUserByUsername(UserContext.current().getApiUsername());
        request.setUserId(apiUser.getUser().getId());
        return returnsService.markSaleOrderReturned(request);
    }

    ///data/reports/markQuantityFound
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/inventory/markQuantityFound")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.MARK_QUANTITY_FOUND)
    public MarkItemTypeQuantityFoundResponse markItemTypeQuantityFound(MarkItemTypeQuantityFoundRequest request) {
        return inventoryService.markItemTypeQuantityFound(request);
    }

    ///data/reports/markQuantityNotFound
    @Path("/inventory/markQuantityNotFound")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.MARK_QUANTITY_NOT_FOUND)
    public MarkItemtypeQuantityNotFoundResponse markItemTypeQuantityNotFound(MarkItemTypeQuantityNotFoundRequest request) {
        return inventoryService.markItemTypeQuantityNotFound(request);
    }

    ///data/reports/showItemTypeQuantityNotFoundDetails?itemSkuCode=108553
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/inventory/showItemTypeQuantityNotFoundDetails")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.LOOKUP_ITEM_TYPE)
    public SearchItemTypeNotFoundQuantityResponse itemQuantityNotFoundDetails(SearchItemTypeNotFoundQuantityRequest request) {
        return lookupService.itemQuantityNotFoundDetails(request);
    }

    ///data/material/gatepass/scan/item/	to scan item code in Gate pass
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/purchase/gatepass/scan/item")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.MATERIAL_MANAGEMENT)
    public GetGatepassScannableItemDetailsResponse getGatepassScannableItemDetails(GetGatepassScannableItemDetailsRequest request) {
        return materialService.getGatepassScannableItemDetail(request);
    }

    ///data/material/gatepass/add/item
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/purchase/gatepass/add/item")
    @POST
    public AddItemToGatePassResponse addItemToGatePass(AddItemToGatePassRequest request) {
        return materialService.addItemToGatePass(request);
    }

    ///data/material/gatepass/item/remove/	to remove added itemcode from Gate pass
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/purchase/gatepass/item/remove")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.MATERIAL_MANAGEMENT)
    public RemoveItemFromGatePassResponse removeItemFromGatePass(RemoveItemFromGatePassRequest request) {
        return materialService.removeItemFromGatePass(request);
    }

    ///data/material/gatepass/discard/	to Discard gate pass through POS
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/purchase/gatepass/discard")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.MATERIAL_MANAGEMENT)
    public DiscardGatePassResponse discardGatePass(DiscardGatePassRequest request) {
        ApiUser apiUser = usersService.getApiUserByUsername(UserContext.current().getApiUsername());
        request.setUserId(apiUser.getUser().getId());
        return materialService.discardGatePass(request);
    }

    ///data/material/gatepass/create/	To Create gate pass
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/purchase/gatepass/create")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.MATERIAL_MANAGEMENT)
    public CreateGatePassResponse createGatepass(CreateGatePassRequest request) {
        ApiUser apiUser = usersService.getApiUserByUsername(UserContext.current().getApiUsername());
        request.setUserId(apiUser.getUser().getId());
        return materialService.createGatePass(request);
    }

    ///data/putaway/item/split
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/inventory/putway/item/split")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.PUTAWAY_COMPLETE)
    public SplitPutawayItemResponse splitPutawayItem(SplitPutawayItemRequest request) {
        return putawayService.splitPutawayItem(request);
    }

    ///data/putaway/traceableItem/add
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/inventory/putway/traceableItem/add")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.PUTAWAY_TRANSFER)
    public AddTraceableItemToTransferPutawayResponse addTraceableItemToTransferPutaway(AddTraceableItemToTransferPutawayRequest request) {
        ApiUser apiUser = usersService.getApiUserByUsername(UserContext.current().getApiUsername());
        request.setUserId(apiUser.getUser().getId());
        return putawayService.addTraceableItemToTransferPutaway(request);
    }

    ///data/putaway/createPutawayList
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/inventory/putway/createPutawayList")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.PUTAWAY_CREATE)
    public CreatePutawayListResponse createPutawayList(CreatePutawayListRequest request) {
        ApiUser apiUser = usersService.getApiUserByUsername(UserContext.current().getApiUsername());
        request.setUserId(apiUser.getUser().getId());
        return putawayService.createPutawayList(request);
    }

    ///data/putaway/complete
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/inventory/putway/complete")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.PUTAWAY_COMPLETE)
    public CompletePutawayResponse completePutaway(CompletePutawayRequest request) {
        return putawayService.completePutaway(request);
    }

    ///data/putaway/get
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/inventory/putway/get")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.MINIMAL)
    public GetPutawayResponse getPutaway(GetPutawayRequest request) {
        return putawayService.getPutaway(request);
    }

    ///data/putaway/create
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/inventory/putway/create")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.PUTAWAY_CREATE)
    public CreatePutawayResponse createPutaway(CreatePutawayRequest request) {
        ApiUser apiUser = usersService.getApiUserByUsername(UserContext.current().getApiUsername());
        request.setUserId(apiUser.getUser().getId());
        return putawayService.createPutaway(request);
    }

    ///data/shipping/packages/merge/
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/oms/shippingPackage/merge")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.MERGE_SHIPMENTS)
    public MergeShippingPackagesResponse mergeShippingPackages(MergeShippingPackagesRequest request) {
        request.setUserId(WebContextUtils.getCurrentUser().getUser().getId());
        return shippingService.mergeShippingPackages(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/oms/shippingPackage/markCustomizationComplete")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.SHIPPING)
    public CompleteCustomizationForShippingPackageResponse completeCustomizationForShippingPackage(CompleteCustomizationForShippingPackageRequest request) {
        return shippingService.completeCustomizationForShippingPackage(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/oms/shippingPackage/picklist/edit")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.PICKLIST_EDIT)
    public EditPicklistResponse editPicklist(EditPicklistRequest editPicklistRequest) {
        return packerService.editPicklist(editPicklistRequest);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/oms/shippingPackage/outboundqc/damage")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.SHIPPING)
    public MarkItemDamagedOutboundQCResponse markItemDamagedOutboundQC(MarkItemDamagedOutboundQCRequest request) {
        return lenskartService.markItemDamagedOutboundQC(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("channel/prices/edit")
    @POST
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.PRICE_UPDATE)
    public EditChannelItemTypePriceResponse editChannelItemType(final EditChannelItemTypePriceRequest request) {
        return pricingService.editChannelItemType(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/pickBucket/submit")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.PICKER)
    public SubmitPickBucketResponse submitPickBucket(SubmitPickBucketRequest request) {
        User user = usersService.getUserByUsername(UserContext.current().getUniwareUserName());
        request.setUserId(user.getId());
        return pickerService.submitPickBucket(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/picklist/detail/get")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.PICKLIST_VIEW)
    public GetPicklistDetailResponse getPicklistDetail(GetPicklistDetailRequest request) {
        return pickerService.getPicklistDetail(request);
    }

    @Path("export/job/create")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.EXPORT)
    public CreateExportJobResponse createExportJob(CreateExportJobRequest request) {
        User user = usersService.getUserByUsername(UserContext.current().getUniwareUserName());
        request.setUserId(user.getId());
        return exportService.createExportJob(request);

    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("export/job/status")
    @POST
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.EXPORT)
    public GetExportJobStatusResponse getExportJobStatus(GetExportJobStatusRequest request) {
        return exportService.getExportJobStatus(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/cyclecount/active/get")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.CYCLE_COUNT_VIEW)
    public GetCycleCountResponse getCycleCount(GetActiveCycleCountRequest request) {
        return cycleCountService.getActiveCycleCount(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/cyclecount/shelf/block")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.COUNT_SHELF)
    public BlockShelvesForCycleCountResponse blockShelfForCycleCount(BlockShelvesForCycleCountRequest request) {
        return cycleCountService.blockShelvesForCycleCountRequest(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/cyclecount/shelf/unblock")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.COUNT_SHELF)
    public UnblockShelvesResponse unBlockShelfForCycleCount(UnblockShelvesRequest request) {
        return cycleCountService.unblockShelves(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/cyclecount/shelf/start")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.COUNT_SHELF)
    public StartCountingForShelfResponse startCountingForShelf(StartCountingForShelfRequest request) {
        return cycleCountService.startCountingForShelf(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/cyclecount/shelf/submit")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.COUNT_SHELF)
    public CompleteCountForShelfResponse completeCountForShelf(CompleteCountForShelfRequest request) {
        return cycleCountService.completeCountForShelf(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/cyclecount/shelf/recount")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.COUNT_SHELF)
    public RecountShelfResponse recountShelf(RecountShelfRequest request) {
        return cycleCountService.recountShelf(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/cyclecount/shelf/search")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.CYCLE_COUNT_VIEW)
    public SearchCycleCountShelfResponse searchCycleCountShelf(SearchCycleCountShelfRequest request) {
        return cycleCountService.searchCycleCountShelf(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/cyclecount/subcyclecount/get")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.CYCLE_COUNT_VIEW)
    public GetSubCycleCountShelvesResponse getSubCycleCount(GetSubCycleCountShelvesRequest request) {
        return cycleCountService.getSubCycleCount(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/cyclecount/erroritems/get")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.CYCLE_COUNT_VIEW)
    public GetErrorItemsForCycleCountResponse getErrorItemsForCycleCount(GetBadItemsForCycleCountRequest request) {
        return cycleCountService.getErrorItemsForCycleCount(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/cyclecount/shelf/erroritems/get")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.CYCLE_COUNT_VIEW)
    public GetBadItemsForShelfResponse getErrorItemsForCycleCount(GetBadItemsForShelfRequest request) {
        return cycleCountService.getErrorItemsForShelf(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/zones/get")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.MINIMAL)
    public GetZonesForCycleCountResponse getZonesForCycleCount(GetZonesForCycleCountRequest request) {
        return cycleCountService.getZonesForCycleCount(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/invoice/details/get")
    @POST
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.MINIMAL)
    public GetInvoiceDetailsResponse getInvoiceDetails(GetInvoiceDetailsRequest request) {
        return shippingInvoiceService.getInvoiceDetails(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/ledger/get")
    @POST
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.LOOKUP_INVENTORY_LEDGER)
    public FetchInventoryLedgerResponse fetchInventoryledger(FetchInventoryLedgerRequest request) {
        return inventoryLedgerService.fetchInventoryLedger(request);
    }


    @Produces(MediaType.APPLICATION_JSON)
    @Path("/ledger/summary/get")
    @POST
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.LOOKUP_INVENTORY_LEDGER)
    public FetchInventoryLedgerSummaryResponse fetchInventoryLedgerSummary(FetchInventoryLedgerSummaryRequest request) {
        return inventoryLedgerService.fetchInventoryLedgerSummary(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/ledger/summary/bulk")
    @POST
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.LOOKUP_INVENTORY_LEDGER)
    public BulkInventoryLedgerSummaryResponse bulkInventoryLedgerSummary(BulkInventoryLedgerSummaryRequest request) {
        return inventoryLedgerService.bulkInventoryLedgerSummary(request);
    }
}
