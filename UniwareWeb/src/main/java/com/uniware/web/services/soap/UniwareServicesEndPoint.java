/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 17, 2011
 *  @author singla
 */
package com.uniware.web.services.soap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.unifier.core.annotation.Level;
import com.unifier.core.api.tasks.EditScriptConfigRequest;
import com.unifier.core.api.tasks.EditScriptConfigResponse;
import com.unifier.core.api.tasks.ReloadConfigurationRequest;
import com.unifier.core.api.tasks.ReloadConfigurationResponse;
import com.unifier.core.api.token.GetLoginTokenRequest;
import com.unifier.core.api.token.GetLoginTokenResponse;
import com.unifier.core.api.user.CreateUserRequest;
import com.unifier.core.api.user.CreateUserResponse;
import com.unifier.core.api.user.GetAccessResourceRequest;
import com.unifier.core.api.user.GetAccessResourceResponse;
import com.unifier.core.api.validation.WsError;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.entity.AccessResource.Name;
import com.unifier.core.entity.ApiUser;
import com.unifier.core.entity.User;
import com.unifier.core.template.Template;
import com.unifier.services.users.IUsersService;
import com.unifier.services.vo.TenantProfileVO;
import com.uniware.core.api.broadcast.RefreshBroadcastMessageRequest;
import com.uniware.core.api.broadcast.RefreshBroadcastMessageResponse;
import com.uniware.core.api.catalog.AddOrEditShippingProviderLocationRequest;
import com.uniware.core.api.catalog.AddOrEditShippingProviderLocationResponse;
import com.uniware.core.api.catalog.CreateItemTypeRequest;
import com.uniware.core.api.catalog.CreateItemTypeResponse;
import com.uniware.core.api.catalog.CreateOrEditCategoryRequest;
import com.uniware.core.api.catalog.CreateOrEditCategoryResponse;
import com.uniware.core.api.catalog.CreateOrEditFacilityItemTypeRequest;
import com.uniware.core.api.catalog.CreateOrEditFacilityItemTypeResponse;
import com.uniware.core.api.catalog.CreateOrEditItemTypeRequest;
import com.uniware.core.api.catalog.CreateOrEditItemTypeResponse;
import com.uniware.core.api.catalog.EditItemTypeRequest;
import com.uniware.core.api.catalog.EditItemTypeResponse;
import com.uniware.core.api.catalog.GetItemTypeRequest;
import com.uniware.core.api.catalog.GetItemTypeResponse;
import com.uniware.core.api.catalog.GetServiceabilityRequest;
import com.uniware.core.api.catalog.GetServiceabilityResponse;
import com.uniware.core.api.channel.GetConnectorStatusRequest;
import com.uniware.core.api.channel.GetConnectorStatusResponse;
import com.uniware.core.api.customer.CreateCustomerRequest;
import com.uniware.core.api.customer.CreateCustomerResponse;
import com.uniware.core.api.customer.EditCustomerRequest;
import com.uniware.core.api.customer.EditCustomerResponse;
import com.uniware.core.api.customizations.MarkItemDamagedOutboundQCRequest;
import com.uniware.core.api.customizations.MarkItemDamagedOutboundQCResponse;
import com.uniware.core.api.dual.company.AutoCompletePutawayRequest;
import com.uniware.core.api.dual.company.AutoCompletePutawayResponse;
import com.uniware.core.api.dual.company.ReceiveReturnFromRetailRequest;
import com.uniware.core.api.dual.company.ReceiveReturnFromRetailResponse;
import com.uniware.core.api.dual.company.ReceiveReversePickupFromRetailRequest;
import com.uniware.core.api.dual.company.ReceiveReversePickupFromRetailResponse;
import com.uniware.core.api.dual.company.ReceiveShippingPackageFromWholesaleRequest;
import com.uniware.core.api.dual.company.ReceiveShippingPackageFromWholesaleResponse;
import com.uniware.core.api.facility.CreateFacilityRequest;
import com.uniware.core.api.facility.CreateFacilityResponse;
import com.uniware.core.api.inflow.AddEditVendorInvoiceItemRequest;
import com.uniware.core.api.inflow.AddEditVendorInvoiceItemResponse;
import com.uniware.core.api.inflow.AddItemDetailsRequest;
import com.uniware.core.api.inflow.AddItemDetailsResponse;
import com.uniware.core.api.inflow.AddItemToInflowReceiptRequest;
import com.uniware.core.api.inflow.AddItemToInflowReceiptResponse;
import com.uniware.core.api.inflow.CreateInflowReceiptRequest;
import com.uniware.core.api.inflow.CreateInflowReceiptResponse;
import com.uniware.core.api.inflow.CreateVendorCreditInvoiceRequest;
import com.uniware.core.api.inflow.CreateVendorCreditInvoiceResponse;
import com.uniware.core.api.inflow.GetInflowReceiptRequest;
import com.uniware.core.api.inflow.GetInflowReceiptResponse;
import com.uniware.core.api.inflow.GetInflowReceiptsRequest;
import com.uniware.core.api.inflow.GetInflowReceiptsResponse;
import com.uniware.core.api.inventory.AddOrEditItemLabelsRequest;
import com.uniware.core.api.inventory.AddOrEditItemLabelsResponse;
import com.uniware.core.api.inventory.GenerateItemLabelsRequest;
import com.uniware.core.api.inventory.GenerateItemLabelsResponse;
import com.uniware.core.api.inventory.GetInventorySnapshotRequest;
import com.uniware.core.api.inventory.GetInventorySnapshotResponse;
import com.uniware.core.api.inventory.GetItemTypeInventoryRequest;
import com.uniware.core.api.inventory.GetItemTypeInventoryResponse;
import com.uniware.core.api.inventory.InventoryAdjustmentRequest;
import com.uniware.core.api.inventory.InventoryAdjustmentResponse;
import com.uniware.core.api.invoice.ChangeInvoiceCodeRequest;
import com.uniware.core.api.invoice.ChangeInvoiceCodeResponse;
import com.uniware.core.api.invoice.CreateInvoiceBySaleOrderCodeRequest;
import com.uniware.core.api.invoice.CreateInvoiceBySaleOrderCodeResponse;
import com.uniware.core.api.invoice.CreateInvoiceRequest;
import com.uniware.core.api.invoice.CreateInvoiceResponse;
import com.uniware.core.api.invoice.CreateInvoiceWithDetailsRequest;
import com.uniware.core.api.invoice.CreateInvoiceWithDetailsResponse;
import com.uniware.core.api.invoice.CreateShippingPackageInvoiceRequest;
import com.uniware.core.api.invoice.GetInvoiceDetailsByCodeRequest;
import com.uniware.core.api.invoice.GetInvoiceDetailsByCodeResponse;
import com.uniware.core.api.invoice.GetInvoiceDetailsRequest;
import com.uniware.core.api.invoice.GetInvoiceDetailsResponse;
import com.uniware.core.api.invoice.GetInvoiceLabelRequest;
import com.uniware.core.api.invoice.GetInvoiceLabelResponse;
import com.uniware.core.api.invoice.WsTaxInformation;
import com.uniware.core.api.item.GetItemDetailRequest;
import com.uniware.core.api.item.GetItemDetailResponse;
import com.uniware.core.api.item.GetItemTypeDetailRequest;
import com.uniware.core.api.item.GetItemTypeDetailResponse;
import com.uniware.core.api.jabong.JabongReceiveItemFromWholesaleRequest;
import com.uniware.core.api.jabong.JabongReceiveItemFromWholesaleResponse;
import com.uniware.core.api.jabong.JabongWholesaleHandleShipmentReturnRequest;
import com.uniware.core.api.jabong.JabongWholesaleHandleShipmentReturnResponse;
import com.uniware.core.api.jabong.ReleaseShelfRequest;
import com.uniware.core.api.jabong.ReleaseShelfResponse;
import com.uniware.core.api.material.AddItemToGatePassRequest;
import com.uniware.core.api.material.AddItemToGatePassResponse;
import com.uniware.core.api.material.AddNonTraceableGatePassItemRequest;
import com.uniware.core.api.material.AddNonTraceableGatePassItemResponse;
import com.uniware.core.api.material.AddOrEditNonTraceableGatePassItemRequest;
import com.uniware.core.api.material.AddOrEditNonTraceableGatePassItemResponse;
import com.uniware.core.api.material.CompleteGatePassRequest;
import com.uniware.core.api.material.CompleteGatePassResponse;
import com.uniware.core.api.material.CreateGatePassRequest;
import com.uniware.core.api.material.CreateGatePassResponse;
import com.uniware.core.api.material.EditGatePassRequest;
import com.uniware.core.api.material.EditGatePassResponse;
import com.uniware.core.api.material.SearchGatePassRequest;
import com.uniware.core.api.material.SearchGatePassResponse;
import com.uniware.core.api.o2o.GetAvailableFacilitiesRequest;
import com.uniware.core.api.o2o.GetAvailableFacilitiesResponse;
import com.uniware.core.api.o2o.GetFacilitiesSLARequest;
import com.uniware.core.api.o2o.GetFacilitiesSLAResponse;
import com.uniware.core.api.o2o.GetFacilityDetailsRequest;
import com.uniware.core.api.o2o.GetFacilityDetailsResponse;
import com.uniware.core.api.o2o.GetFacilitySLARequest;
import com.uniware.core.api.o2o.GetFacilitySLAResponse;
import com.uniware.core.api.o2o.GetTenantDetailsRequest;
import com.uniware.core.api.o2o.GetTenantDetailsResponse;
import com.uniware.core.api.packer.PickShippingPackageRequest;
import com.uniware.core.api.packer.PickShippingPackageResponse;
import com.uniware.core.api.party.CreateOrEditVendorItemTypeRequest;
import com.uniware.core.api.party.CreateOrEditVendorItemTypeResponse;
import com.uniware.core.api.party.CreateOrEditVendorRequest;
import com.uniware.core.api.party.CreateOrEditVendorResponse;
import com.uniware.core.api.party.CreateVendorDetailsRequest;
import com.uniware.core.api.party.CreateVendorDetailsResponse;
import com.uniware.core.api.party.CreateVendorItemTypeRequest;
import com.uniware.core.api.party.CreateVendorItemTypeResponse;
import com.uniware.core.api.party.CreateVendorRequest;
import com.uniware.core.api.party.CreateVendorResponse;
import com.uniware.core.api.party.EditVendorDetailsRequest;
import com.uniware.core.api.party.EditVendorDetailsResponse;
import com.uniware.core.api.party.EditVendorItemTypeRequest;
import com.uniware.core.api.party.EditVendorItemTypeResponse;
import com.uniware.core.api.party.EditVendorRequest;
import com.uniware.core.api.party.EditVendorResponse;
import com.uniware.core.api.party.GetVendorItemTypesRequest;
import com.uniware.core.api.party.GetVendorItemTypesResponse;
import com.uniware.core.api.party.WsGenericVendor;
import com.uniware.core.api.party.WsPartyContact;
import com.uniware.core.api.party.WsVendor;
import com.uniware.core.api.party.WsVendorAgreement;
import com.uniware.core.api.prices.EditChannelItemTypePriceRequest;
import com.uniware.core.api.prices.EditChannelItemTypePriceResponse;
import com.uniware.core.api.prices.GetChannelItemTypePricesRequest;
import com.uniware.core.api.prices.GetChannelItemTypePricesResponse;
import com.uniware.core.api.purchase.AddOrEditPurchaseOrderItemsRequest;
import com.uniware.core.api.purchase.AddOrEditPurchaseOrderItemsResponse;
import com.uniware.core.api.purchase.ApprovePurchaseOrderRequest;
import com.uniware.core.api.purchase.ApprovePurchaseOrderResponse;
import com.uniware.core.api.purchase.ClosePurchaseOrderRequest;
import com.uniware.core.api.purchase.ClosePurchaseOrderResponse;
import com.uniware.core.api.purchase.CreateASNRequest;
import com.uniware.core.api.purchase.CreateASNResponse;
import com.uniware.core.api.purchase.CreateApprovedPurchaseOrderRequest;
import com.uniware.core.api.purchase.CreateApprovedPurchaseOrderResponse;
import com.uniware.core.api.purchase.CreatePurchaseOrderRequest;
import com.uniware.core.api.purchase.CreatePurchaseOrderResponse;
import com.uniware.core.api.purchase.EditAdvanceShippingNoticeRequest;
import com.uniware.core.api.purchase.EditAdvanceShippingNoticeResponse;
import com.uniware.core.api.purchase.GetBackOrderItemsRequest;
import com.uniware.core.api.purchase.GetBackOrderItemsResponse;
import com.uniware.core.api.purchase.GetPurchaseOrderRequest;
import com.uniware.core.api.purchase.GetPurchaseOrderResponse;
import com.uniware.core.api.purchase.GetPurchaseOrdersRequest;
import com.uniware.core.api.purchase.GetPurchaseOrdersResponse;
import com.uniware.core.api.purchase.GetVendorPurchaseOrderDetailRequest;
import com.uniware.core.api.purchase.GetVendorPurchaseOrderDetailResponse;
import com.uniware.core.api.purchase.GetVendorPurchaseOrdersRequest;
import com.uniware.core.api.purchase.GetVendorPurchaseOrdersResponse;
import com.uniware.core.api.putaway.CompletePutawayRequest;
import com.uniware.core.api.putaway.CompletePutawayResponse;
import com.uniware.core.api.putaway.CreatePutawayListRequest;
import com.uniware.core.api.putaway.CreatePutawayListResponse;
import com.uniware.core.api.putaway.CreatePutawayRequest;
import com.uniware.core.api.putaway.CreatePutawayResponse;
import com.uniware.core.api.recommendation.CancelRecommendationRequest;
import com.uniware.core.api.recommendation.CancelRecommendationResponse;
import com.uniware.core.api.recommendation.CreateRecommendationRequest;
import com.uniware.core.api.recommendation.CreateRecommendationResponse;
import com.uniware.core.api.recommendation.GetRecommendationsRequest;
import com.uniware.core.api.recommendation.GetRecommendationsResponse;
import com.uniware.core.api.returns.CreateReversePickupRequest;
import com.uniware.core.api.returns.CreateReversePickupResponse;
import com.uniware.core.api.returns.MarkSaleOrderReturnedRequest;
import com.uniware.core.api.returns.MarkSaleOrderReturnedResponse;
import com.uniware.core.api.reversepickup.AddReversePickupSaleOrderItemsToPutawayRequest;
import com.uniware.core.api.reversepickup.AddReversePickupSaleOrderItemsToPutawayResponse;
import com.uniware.core.api.saleorder.AcceptSaleOrderItemAlternateRequest;
import com.uniware.core.api.saleorder.AcceptSaleOrderItemAlternateResponse;
import com.uniware.core.api.saleorder.AddSaleOrderToManifestRequest;
import com.uniware.core.api.saleorder.AddSaleOrderToManifestResponse;
import com.uniware.core.api.saleorder.CancelSaleOrderRequest;
import com.uniware.core.api.saleorder.CancelSaleOrderResponse;
import com.uniware.core.api.saleorder.CreateSaleOrderItemAlternateRequest;
import com.uniware.core.api.saleorder.CreateSaleOrderItemAlternateResponse;
import com.uniware.core.api.saleorder.CreateSaleOrderRequest;
import com.uniware.core.api.saleorder.CreateSaleOrderResponse;
import com.uniware.core.api.saleorder.DispatchSaleOrderRequest;
import com.uniware.core.api.saleorder.DispatchSaleOrderResponse;
import com.uniware.core.api.saleorder.EditSaleOrderAddressRequest;
import com.uniware.core.api.saleorder.EditSaleOrderAddressResponse;
import com.uniware.core.api.saleorder.EditSaleOrderItemMetadataRequest;
import com.uniware.core.api.saleorder.EditSaleOrderItemMetadataResponse;
import com.uniware.core.api.saleorder.EditSaleOrderMetadataRequest;
import com.uniware.core.api.saleorder.EditSaleOrderMetadataResponse;
import com.uniware.core.api.saleorder.GetFailedSaleOrdersRequest;
import com.uniware.core.api.saleorder.GetFailedSaleOrdersResponse;
import com.uniware.core.api.saleorder.GetSaleOrderRequest;
import com.uniware.core.api.saleorder.GetSaleOrderResponse;
import com.uniware.core.api.saleorder.GetShippingPackagesByStatusCodeRequest;
import com.uniware.core.api.saleorder.GetShippingPackagesByStatusCodeResponse;
import com.uniware.core.api.saleorder.GetUnverifiedSaleOrdersRequest;
import com.uniware.core.api.saleorder.GetUnverifiedSaleOrdersResponse;
import com.uniware.core.api.saleorder.HoldSaleOrderItemsRequest;
import com.uniware.core.api.saleorder.HoldSaleOrderItemsResponse;
import com.uniware.core.api.saleorder.HoldSaleOrderRequest;
import com.uniware.core.api.saleorder.HoldSaleOrderResponse;
import com.uniware.core.api.saleorder.ModifyPacketSaleOrderRequest;
import com.uniware.core.api.saleorder.ModifyPacketSaleOrderResponse;
import com.uniware.core.api.saleorder.SearchSaleOrderRequest;
import com.uniware.core.api.saleorder.SearchSaleOrderResponse;
import com.uniware.core.api.saleorder.SetSaleOrderPriorityRequest;
import com.uniware.core.api.saleorder.SetSaleOrderPriorityResponse;
import com.uniware.core.api.saleorder.SwitchSaleOrderItemFacilityRequest;
import com.uniware.core.api.saleorder.SwitchSaleOrderItemFacilityResponse;
import com.uniware.core.api.saleorder.UnblockSaleOrderItemsInventoryRequest;
import com.uniware.core.api.saleorder.UnblockSaleOrderItemsInventoryResponse;
import com.uniware.core.api.saleorder.UnholdSaleOrderItemsRequest;
import com.uniware.core.api.saleorder.UnholdSaleOrderItemsResponse;
import com.uniware.core.api.saleorder.UnholdSaleOrderRequest;
import com.uniware.core.api.saleorder.UnholdSaleOrderResponse;
import com.uniware.core.api.saleorder.VerifySaleOrderRequest;
import com.uniware.core.api.saleorder.VerifySaleOrderResponse;
import com.uniware.core.api.shipping.AddShipmentsToBatchRequest;
import com.uniware.core.api.shipping.AddShipmentsToBatchResponse;
import com.uniware.core.api.shipping.AddShippingPackageToManifestRequest;
import com.uniware.core.api.shipping.AddShippingPackageToManifestResponse;
import com.uniware.core.api.shipping.AddShippingPackagesToManifestRequest;
import com.uniware.core.api.shipping.AddShippingPackagesToManifestResponse;
import com.uniware.core.api.shipping.AllocateShippingProviderRequest;
import com.uniware.core.api.shipping.AllocateShippingProviderResponse;
import com.uniware.core.api.shipping.CloseShippingManifestRequest;
import com.uniware.core.api.shipping.CloseShippingManifestResponse;
import com.uniware.core.api.shipping.CreateBatchRequest;
import com.uniware.core.api.shipping.CreateBatchResponse;
import com.uniware.core.api.shipping.CreateInvoiceAndAllocateShippingProviderBySaleOrderRequest;
import com.uniware.core.api.shipping.CreateInvoiceAndAllocateShippingProviderBySaleOrderResponse;
import com.uniware.core.api.shipping.CreateInvoiceAndAllocateShippingProviderRequest;
import com.uniware.core.api.shipping.CreateInvoiceAndAllocateShippingProviderResponse;
import com.uniware.core.api.shipping.CreateInvoiceAndGenerateLabelRequest;
import com.uniware.core.api.shipping.CreateInvoiceAndGenerateLabelResponse;
import com.uniware.core.api.shipping.CreateShippingManifestRequest;
import com.uniware.core.api.shipping.CreateShippingManifestResponse;
import com.uniware.core.api.shipping.DispatchSaleOrderItemsRequest;
import com.uniware.core.api.shipping.DispatchSaleOrderItemsResponse;
import com.uniware.core.api.shipping.DispatchShippingPackageRequest;
import com.uniware.core.api.shipping.DispatchShippingPackageResponse;
import com.uniware.core.api.shipping.EditShippingPackageRequest;
import com.uniware.core.api.shipping.EditShippingPackageResponse;
import com.uniware.core.api.shipping.ForceDispatchShippingPackageRequest;
import com.uniware.core.api.shipping.ForceDispatchShippingPackageResponse;
import com.uniware.core.api.shipping.GetManifestRequest;
import com.uniware.core.api.shipping.GetManifestResponse;
import com.uniware.core.api.shipping.GetShippingManifestLabelRequest;
import com.uniware.core.api.shipping.GetShippingManifestLabelResponse;
import com.uniware.core.api.shipping.GetShippingManifestRequest;
import com.uniware.core.api.shipping.GetShippingManifestResponse;
import com.uniware.core.api.shipping.GetShippingPackageDetailRequest;
import com.uniware.core.api.shipping.GetShippingPackageDetailResponse;
import com.uniware.core.api.shipping.GetShippingPackageShipmentDetailsRequest;
import com.uniware.core.api.shipping.GetShippingPackageShipmentDetailsResponse;
import com.uniware.core.api.shipping.GetShippingPackagesRequest;
import com.uniware.core.api.shipping.GetShippingPackagesResponse;
import com.uniware.core.api.shipping.GetShippingProviderParametersRequest;
import com.uniware.core.api.shipping.GetShippingProviderParametersResponse;
import com.uniware.core.api.shipping.MarkSaleOrderItemsDeliveredRequest;
import com.uniware.core.api.shipping.MarkSaleOrderItemsDeliveredResponse;
import com.uniware.core.api.shipping.MarkShippingPackageDeliveredRequest;
import com.uniware.core.api.shipping.MarkShippingPackageDeliveredResponse;
import com.uniware.core.api.shipping.ResendPODCodeRequest;
import com.uniware.core.api.shipping.ResendPODCodeResponse;
import com.uniware.core.api.shipping.SearchShippingPackageRequest;
import com.uniware.core.api.shipping.SearchShippingPackageResponse;
import com.uniware.core.api.shipping.SplitShippingPackageRequest;
import com.uniware.core.api.shipping.SplitShippingPackageResponse;
import com.uniware.core.api.shipping.UpdateTrackingStatusRequest;
import com.uniware.core.api.shipping.UpdateTrackingStatusResponse;
import com.uniware.core.api.shipping.VerifySaleOrderItemsPODCodeRequest;
import com.uniware.core.api.shipping.VerifySaleOrderItemsPODCodeResponse;
import com.uniware.core.api.shipping.VerifyShippingPackagePODCodeRequest;
import com.uniware.core.api.shipping.VerifyShippingPackagePODCodeResponse;
import com.uniware.core.api.tax.GetTaxTypeConfigurationRequest;
import com.uniware.core.api.tax.GetTaxTypeConfigurationResponse;
import com.uniware.core.api.templatecustomization.GetCustomizedDummyTemplateRequest;
import com.uniware.core.api.templatecustomization.GetCustomizedDummyTemplateResponse;
import com.uniware.core.api.tenant.AllowSourceRequest;
import com.uniware.core.api.tenant.AllowSourceResponse;
import com.uniware.core.api.tenant.ChangeProductTypeRequest;
import com.uniware.core.api.tenant.ChangeProductTypeResponse;
import com.uniware.core.api.tenant.CreateTenantRequest;
import com.uniware.core.api.tenant.CreateTenantResponse;
import com.uniware.core.api.tenant.DeactivateTenantRequest;
import com.uniware.core.api.tenant.DeactivateTenantResponse;
import com.uniware.core.api.tenant.GetTenantTransactionDetailsRequest;
import com.uniware.core.api.tenant.GetTenantTransactionDetailsResponse;
import com.uniware.core.api.tenant.PingUniwareRequest;
import com.uniware.core.api.tenant.PingUniwareResponse;
import com.uniware.core.api.tenant.ReactivateTenantRequest;
import com.uniware.core.api.tenant.ReactivateTenantResponse;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.api.warehouse.EditFacilityRequest;
import com.uniware.core.api.warehouse.EditFacilityResponse;
import com.uniware.core.api.warehouse.SearchItemTypesRequest;
import com.uniware.core.api.warehouse.SearchItemTypesResponse;
import com.uniware.core.entity.ItemTypeInventory;
import com.uniware.core.entity.SaleOrder;
import com.uniware.core.entity.ShippingPackage;
import com.uniware.core.entity.Vendor;
import com.uniware.core.utils.UserContext;
import com.uniware.core.vo.PrintTemplateVO;
import com.uniware.services.broadcast.IBroadcastService;
import com.uniware.services.cache.PrintTemplateCache;
import com.uniware.services.catalog.ICatalogService;
import com.uniware.services.channel.IChannelService;
import com.uniware.services.configuration.TenantSystemConfiguration;
import com.uniware.services.customer.ICustomerService;
import com.uniware.services.customizations.impl.LenskartServiceImpl;
import com.uniware.services.dual.company.IDualCompanyService;
import com.uniware.services.inflow.IInflowService;
import com.uniware.services.inflow.IVendorInvoiceService;
import com.uniware.services.inventory.IInventoryService;
import com.uniware.services.invoice.IInvoiceService;
import com.uniware.services.item.IItemService;
import com.uniware.services.jabong.IJabongService;
import com.uniware.services.lookup.ILookupService;
import com.uniware.services.material.IMaterialService;
import com.uniware.services.packer.IPackerService;
import com.uniware.services.pickup.IStoreService;
import com.uniware.services.pricing.IPricingFacade;
import com.uniware.services.purchase.IPurchaseService;
import com.uniware.services.putaway.IPutawayService;
import com.uniware.services.recommendation.IRecommendationManagementService;
import com.uniware.services.recommendation.impl.RecommendationManagemetServiceImpl.RecommendationType;
import com.uniware.services.reload.IReloadService;
import com.uniware.services.returns.IReturnsService;
import com.uniware.services.saleorder.IDispatchSaleOrderService;
import com.uniware.services.saleorder.ISaleOrderService;
import com.uniware.services.script.service.IScriptService;
import com.uniware.services.shipping.IDispatchService;
import com.uniware.services.shipping.IShippingAdminService;
import com.uniware.services.shipping.IShippingInvoiceService;
import com.uniware.services.shipping.IShippingProviderService;
import com.uniware.services.shipping.IShippingService;
import com.uniware.services.tax.ITaxTypeService;
import com.uniware.services.templateCustomization.ITemplateCustomizationService;
import com.uniware.services.tenant.ITenantInvoiceService;
import com.uniware.services.tenant.ITenantService;
import com.uniware.services.tenant.ITenantSetupService;
import com.uniware.services.vendor.IVendorService;
import com.uniware.services.warehouse.IFacilityService;
import com.uniware.web.security.annotation.UniwareEndPointAccess;

/**
 * @author singla
 */
@Endpoint
public class UniwareServicesEndPoint {

    @Autowired
    private ISaleOrderService                saleOrderService;

    @Autowired
    private ICatalogService                  catalogService;

    @Autowired
    private IDualCompanyService              dualCompanyService;

    @Autowired
    private IJabongService                   jabongService;

    @Autowired
    private IPurchaseService                 purchaseService;

    @Autowired
    private IInventoryService                inventoryService;

    @Autowired
    private IVendorService                   vendorService;

    @Autowired
    private IShippingService                 shippingService;

    @Autowired
    private ILookupService                   lookupService;

    @Autowired
    private IReturnsService                  returnsService;

    @Autowired
    private ITenantSetupService              tenantSetupService;

    @Autowired
    private IUsersService                    usersService;

    @Autowired
    private ICustomerService                 customerService;

    @Autowired
    private IFacilityService                 facilityService;

    @Autowired
    private IItemService                     itemService;

    @Autowired
    private IInflowService                   inflowService;

    @Autowired
    private IDispatchService                 dispatchService;

    @Autowired
    private IReloadService                   reloadService;

    @Autowired
    private IShippingProviderService         shippingProviderService;

    @Autowired
    private IShippingAdminService            shippingAdminService;

    @Autowired
    private IMaterialService                 materialService;

    @Autowired
    private LenskartServiceImpl              lenskartService;

    @Autowired
    private ITenantInvoiceService            tenantInvoiceService;

    @Autowired
    private IScriptService                   scriptService;

    @Autowired
    private ITemplateCustomizationService    templateCustomizationService;

    @Autowired
    private IPackerService                   packerService;

    @Autowired
    private ITaxTypeService                  taxTypeService;

    @Autowired
    private IInvoiceService                  invoiceService;

    @Autowired
    private ITenantService                   tenantService;

    @Autowired
    private IVendorInvoiceService            vendorInvoiceService;

    @Autowired
    private IBroadcastService                broadcastService;

    @Autowired
    private IPutawayService                  putawayService;

    @Autowired
    private IStoreService                    pickupService;

    @Autowired
    private IDispatchSaleOrderService        dispatchSaleOrderService;

    @Autowired
    private IChannelService                  channelService;

    @Autowired
    private IPricingFacade                   pricing;

    @Autowired
    private IRecommendationManagementService recommendationManagementService;

    @Autowired
    private IShippingInvoiceService          shippingInvoiceService;

    @PayloadRoot(localPart = "CreateSaleOrderRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.IMPORT_SALE_ORDERS)
    public CreateSaleOrderResponse createSaleOrder(@RequestPayload CreateSaleOrderRequest request) {
        return saleOrderService.createSaleOrder(request);
    }

    @PayloadRoot(localPart = "CancelSaleOrderRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.SALE_ORDER_CANCEL_ITEM)
    public CancelSaleOrderResponse cancelSaleOrder(@RequestPayload CancelSaleOrderRequest request) {
        // XXX Temporary hack for TMS, remove ASAP
        List<String> saleOrderItemCodes = saleOrderService.getSaleOrderItemCodes(request.getSaleOrderCode());
        if (saleOrderItemCodes != null && saleOrderItemCodes.size() == 1) {
            request.setSaleOrderItemCodes(saleOrderItemCodes);
        }
        return saleOrderService.cancelSaleOrder(request);
    }

    @PayloadRoot(localPart = "HoldSaleOrderRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.SALE_ORDER_HOLD_UNHOLD)
    public HoldSaleOrderResponse holdSaleOrder(@RequestPayload HoldSaleOrderRequest request) {
        return saleOrderService.holdSaleOrder(request);
    }

    @PayloadRoot(localPart = "HoldSaleOrderItemsRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.SALE_ORDER_HOLD_UNHOLD)
    public HoldSaleOrderItemsResponse holdSaleOrderItems(@RequestPayload HoldSaleOrderItemsRequest request) {
        return saleOrderService.holdSaleOrderItems(request);
    }

    @PayloadRoot(localPart = "GetSaleOrderRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.SALE_ORDER_CANCEL_ITEM)
    public GetSaleOrderResponse getSaleOrder(@RequestPayload GetSaleOrderRequest request) {
        return saleOrderService.getSaleOrder(request);
    }

    @PayloadRoot(localPart = "CreateItemTypeRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.ADMIN_CATALOG)
    public CreateItemTypeResponse createItemType(@RequestPayload CreateItemTypeRequest request) {
        return catalogService.createItemType(request);
    }

    @PayloadRoot(localPart = "EditItemTypeRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.ADMIN_CATALOG)
    public EditItemTypeResponse editItemType(@RequestPayload EditItemTypeRequest request) {
        return catalogService.editItemType(request);
    }

    @PayloadRoot(localPart = "CreateOrEditItemTypeRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.ADMIN_CATALOG)
    public CreateOrEditItemTypeResponse createOrEditItemType(@RequestPayload CreateOrEditItemTypeRequest request) {
        CreateOrEditItemTypeResponse response = catalogService.createOrEditItemType(request);
        return response;
    }

    @PayloadRoot(localPart = "CreateOrEditFacilityItemTypeRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.ADMIN_CATALOG)
    public CreateOrEditFacilityItemTypeResponse createOrEditFacilityItemType(@RequestPayload CreateOrEditFacilityItemTypeRequest request) {
        CreateOrEditFacilityItemTypeResponse response = catalogService.createOrEditFacilityItemType(request);
        return response;
    }

    @PayloadRoot(localPart = "EditFacilityRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.CREATE_TENANT)
    public EditFacilityResponse editFacility(@RequestPayload EditFacilityRequest request) {
        return facilityService.editFacility(request);
    }

    @PayloadRoot(localPart = "UnholdSaleOrderRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.SALE_ORDER_HOLD_UNHOLD)
    public UnholdSaleOrderResponse unholdSaleOrder(@RequestPayload UnholdSaleOrderRequest request) {
        return saleOrderService.unholdSaleOrder(request);
    }

    @PayloadRoot(localPart = "UnholdSaleOrderItemsRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.SALE_ORDER_HOLD_UNHOLD)
    public UnholdSaleOrderItemsResponse unholdSaleItemsOrder(@RequestPayload UnholdSaleOrderItemsRequest request) {
        return saleOrderService.unholdSaleOrderItems(request);
    }

    @PayloadRoot(localPart = "AutoCompletePutawayRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.DUAL_COMPANY_API)
    public AutoCompletePutawayResponse autoCompletePutaway(@RequestPayload AutoCompletePutawayRequest request) {
        return dualCompanyService.autoCompletePutaway(request);
    }

    @PayloadRoot(localPart = "  ReceiveItemFromWholesaleRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.DUAL_COMPANY_API)
    public JabongReceiveItemFromWholesaleResponse jabongReceiveItemFromWholesale(@RequestPayload JabongReceiveItemFromWholesaleRequest request) {
        return jabongService.receiveItemFromWholesale(request);
    }

    @PayloadRoot(localPart = "ReleaseShelfRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.LOOKUP_SALE_ORDER)
    public ReleaseShelfResponse releaseShelf(@RequestPayload ReleaseShelfRequest request) {
        return jabongService.releaseShelf(request);
    }

    @PayloadRoot(localPart = "JabongWholesaleHandleShipmentReturnRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.DUAL_COMPANY_API)
    public JabongWholesaleHandleShipmentReturnResponse jabongWholesaleHandleShipmentReturn(@RequestPayload JabongWholesaleHandleShipmentReturnRequest request) {
        return jabongService.jabongWholesaleHandleShipmentReturn(request);
    }

    @PayloadRoot(localPart = "SearchSaleOrderRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.LOOKUP_SALE_ORDER)
    public SearchSaleOrderResponse searchSaleOrderRequest(@RequestPayload SearchSaleOrderRequest request) {
        return saleOrderService.searchSaleOrders(request);
    }

    @PayloadRoot(localPart = "EditSaleOrderAddressRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.SALE_ORDER_ADDRESS_EDIT)
    public EditSaleOrderAddressResponse editSaleOrder(@RequestPayload EditSaleOrderAddressRequest request) {
        return saleOrderService.editSaleOrderAddress(request);
    }

    @PayloadRoot(localPart = "ReceiveShippingPackageFromWholesaleRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.DUAL_COMPANY_API)
    public ReceiveShippingPackageFromWholesaleResponse receiveShippingPackageFromWholesale(@RequestPayload ReceiveShippingPackageFromWholesaleRequest request) {
        return dualCompanyService.receiveShippingPackageFromWholesale(request);
    }

    @PayloadRoot(localPart = "GetInventorySnapshotRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.LOOKUP_INVENTORY)
    public GetInventorySnapshotResponse getInventorySnapshot(@RequestPayload GetInventorySnapshotRequest request) {
        return inventoryService.getInventorySnapshot(request);
    }

    @PayloadRoot(localPart = "InventoryAdjustmentRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.IMPORT_INVENTORY_ADJUSTMENT)
    public InventoryAdjustmentResponse adjustInventory(@RequestPayload InventoryAdjustmentRequest request) {
        ApiUser apiUser = usersService.getApiUserByUsername(UserContext.current().getApiUsername());
        User user = usersService.getUserByUsername(apiUser.getUser().getUsername());
        request.setUserId(user.getId());
        return inventoryService.adjustInventory(request);
    }

    @PayloadRoot(localPart = "CreatePurchaseOrderRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.PROCUREMENT)
    public CreatePurchaseOrderResponse createPurchaseOrder(@RequestPayload CreatePurchaseOrderRequest request) {
        User user = ConfigurationManager.getInstance().getConfiguration(TenantSystemConfiguration.class).getSystemUser();
        request.setUserId(user.getId());
        return purchaseService.createPurchaseOrder(request);
    }

    @PayloadRoot(localPart = "AddOrEditPurchaseOrderItemsRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.PROCUREMENT)
    public AddOrEditPurchaseOrderItemsResponse addPurchaseOrderItems(@RequestPayload AddOrEditPurchaseOrderItemsRequest request) {
        return purchaseService.addOrEditPurchaseOrderItems(request);
    }

    @PayloadRoot(localPart = "ApprovePurchaseOrderRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.PROCUREMENT)
    public ApprovePurchaseOrderResponse approvePurchaseOrder(@RequestPayload ApprovePurchaseOrderRequest request) {
        User user = ConfigurationManager.getInstance().getConfiguration(TenantSystemConfiguration.class).getSystemUser();
        request.setUserId(user.getId());
        return purchaseService.approvePurchaseOrder(request);
    }

    @PayloadRoot(localPart = "CreateOrEditCategoryRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.ADMIN_CATALOG)
    public CreateOrEditCategoryResponse createOrEditCategory(@RequestPayload CreateOrEditCategoryRequest request) {
        return catalogService.createOrEditCategory(request);
    }

    @PayloadRoot(localPart = "CreateVendorDetailsRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.VENDOR_CREATE)
    public CreateVendorDetailsResponse createVendorDetails(@RequestPayload CreateVendorDetailsRequest request) {
        CreateVendorDetailsResponse response = new CreateVendorDetailsResponse();
        WsVendor wsVendor = request.getWsVendor();
        WsGenericVendor wsGenericVendor = new WsGenericVendor(wsVendor);
        wsGenericVendor.setShippingAddress(request.getShippingAddress());
        wsGenericVendor.setBillingAddress(request.getBillingAddress());
        if (request.getAgreement() != null) {
            List<WsVendorAgreement> vendorAgreements = new ArrayList<WsVendorAgreement>();
            vendorAgreements.add(request.getAgreement());
            wsGenericVendor.setVendorAgreements(vendorAgreements);
        }
        for (WsPartyContact partyContact : request.getContacts()) {
            wsGenericVendor.getPartyContacts().add(partyContact);
        }
        CreateVendorRequest createVendorRequest = new CreateVendorRequest();
        createVendorRequest.setVendor(wsGenericVendor);
        CreateVendorResponse createVendorResponse = vendorService.createVendor(createVendorRequest);
        if (!createVendorResponse.isSuccessful()) {
            response.addErrors(createVendorResponse.getErrors());
        } else {
            response.setSuccessful(true);
        }
        return response;
    }

    @PayloadRoot(localPart = "EditVendorDetailsRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.VENDOR_CREATE)
    public EditVendorDetailsResponse editVendorDetails(@RequestPayload EditVendorDetailsRequest request) {
        EditVendorDetailsResponse response = new EditVendorDetailsResponse();
        WsVendor wsVendor = request.getWsVendor();
        WsGenericVendor wsGenericVendor = new WsGenericVendor(wsVendor);
        wsGenericVendor.setShippingAddress(request.getShippingAddress());
        wsGenericVendor.setBillingAddress(request.getBillingAddress());
        if (request.getAgreement() != null) {
            List<WsVendorAgreement> vendorAgreements = new ArrayList<WsVendorAgreement>();
            vendorAgreements.add(request.getAgreement());
            wsGenericVendor.setVendorAgreements(vendorAgreements);
        }
        for (WsPartyContact partyContact : request.getContacts()) {
            wsGenericVendor.getPartyContacts().add(partyContact);
        }
        EditVendorRequest editVendorRequest = new EditVendorRequest();
        editVendorRequest.setVendor(wsGenericVendor);
        EditVendorResponse editVendorResponse = vendorService.editVendor(editVendorRequest);
        if (!editVendorResponse.isSuccessful()) {
            response.addErrors(editVendorResponse.getErrors());
        } else {
            response.setSuccessful(true);
        }
        return response;
    }

    @PayloadRoot(localPart = "CreateApprovedPurchaseOrderRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.PROCUREMENT)
    public CreateApprovedPurchaseOrderResponse createApprovedPurchaseOrder(@RequestPayload CreateApprovedPurchaseOrderRequest request) {
        User user = ConfigurationManager.getInstance().getConfiguration(TenantSystemConfiguration.class).getSystemUser();
        request.setUserId(user.getId());
        return purchaseService.createApprovedPurchaseOrder(request);
    }

    @PayloadRoot(localPart = "ReceiveReturnFromRetailRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.DUAL_COMPANY_API)
    public ReceiveReturnFromRetailResponse receiveReturnFromRetail(@RequestPayload ReceiveReturnFromRetailRequest request) {
        return dualCompanyService.receiveReturnFromRetail(request);
    }

    @PayloadRoot(localPart = "CreateSaleOrderItemAlternateRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.SALE_ORDER_ALTERNATE_ACCEPT)
    public CreateSaleOrderItemAlternateResponse createSaleOrderItemAlternate(@RequestPayload CreateSaleOrderItemAlternateRequest request) {
        request.setUserId(ConfigurationManager.getInstance().getConfiguration(TenantSystemConfiguration.class).getSystemUser().getId());
        return saleOrderService.createSaleOrderItemAlternate(request);
    }

    @PayloadRoot(localPart = "AcceptSaleOrderItemAlternateRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.SALE_ORDER_ALTERNATE_ACCEPT)
    public AcceptSaleOrderItemAlternateResponse acceptSaleOrderItemAlternate(@RequestPayload AcceptSaleOrderItemAlternateRequest request) {
        return saleOrderService.acceptSaleOrderItemAlternate(request);
    }

    @PayloadRoot(localPart = "CreateVendorItemTypeRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.VENDOR_CATALOG)
    public CreateVendorItemTypeResponse createVendorItemType(@RequestPayload CreateVendorItemTypeRequest request) {
        return vendorService.createVendorItemType(request);
    }

    @PayloadRoot(localPart = "GetShippingManifestRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.SHIPPING)
    public GetShippingManifestResponse getShippingManifest(@RequestPayload GetShippingManifestRequest request) {
        return dispatchService.getShippingManifest(request);
    }

    @PayloadRoot(localPart = "GetManifestRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.SHIPPING)
    public GetManifestResponse GetManifestResponse(@RequestPayload GetManifestRequest request) {
        GetManifestResponse response = new GetManifestResponse();
        GetShippingManifestRequest manifestRequest = new GetShippingManifestRequest();
        manifestRequest.setShippingManifestCode(request.getShippingManifestCode());
        GetShippingManifestResponse manifestResponse = new GetShippingManifestResponse();
        ApiUser apiUser = usersService.getApiUserByUsername(UserContext.current().getApiUsername());
        User user = apiUser.getUser();
        if (user.getUserFacilities().size() == 1) {
            UserContext.current().setFacility(user.getUserFacilities().iterator().next());
            manifestResponse = dispatchService.getShippingManifest(manifestRequest);
            if (manifestResponse.isSuccessful()) {
                response.setSuccessful(true);
                response.setShippingManifest(manifestResponse.getShippingManifest());
            } else {
                response.setErrors(manifestResponse.getErrors());
            }
        } else {
            response.addError(new WsError(WsResponseCode.INVALID_API_REQUEST.code(), WsResponseCode.INVALID_API_REQUEST.message()));
        }
        return response;
    }

    @PayloadRoot(localPart = "SearchItemTypesRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.ADMIN_CATALOG)
    public SearchItemTypesResponse searchItemType(@RequestPayload SearchItemTypesRequest request) {
        return catalogService.searchItemTypes(request);
    }

    @PayloadRoot(localPart = "GetItemTypeDetailRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.ADMIN_CATALOG)
    public GetItemTypeDetailResponse searchItemTypeBySku(@RequestPayload GetItemTypeDetailRequest request) {
        return lookupService.getItemTypeDetails(request);
    }

    @PayloadRoot(localPart = "CreateReversePickupRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.REVERSE_PICKUP_CREATE)
    public CreateReversePickupResponse createReversePickup(@RequestPayload CreateReversePickupRequest request) {
        return returnsService.createReversePickup(request);
    }

    @PayloadRoot(localPart = "EditSaleOrderMetadataRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.SALE_ORDER_METADATA_EDIT)
    public EditSaleOrderMetadataResponse editSaleOrderMetadata(@RequestPayload EditSaleOrderMetadataRequest request) {
        return saleOrderService.editSaleOrderMetadata(request);
    }

    @PayloadRoot(localPart = "CreateTenantRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.CREATE_TENANT)
    public CreateTenantResponse createTenant(@RequestPayload CreateTenantRequest request) {
        ApiUser apiUser = usersService.getApiUserByUsername(UserContext.current().getApiUsername());
        User user = usersService.getUserByUsername(apiUser.getUser().getUsername());
        request.setCurrentUserId(user.getId());
        return tenantSetupService.createTenantRequestHandler(request);
    }

    @PayloadRoot(localPart = "CreateFacilityRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.CREATE_TENANT)
    public CreateFacilityResponse createFacility(@RequestPayload CreateFacilityRequest request) {
        ApiUser apiUser = usersService.getApiUserByUsername(UserContext.current().getApiUsername());
        User user = usersService.getUserByUsername(apiUser.getUser().getUsername());
        request.setUsername(user.getUsername());
        return facilityService.createFacility(request);
    }

    @PayloadRoot(localPart = "GetVendorPurchaseOrdersRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.VENDOR_API)
    public GetVendorPurchaseOrdersResponse getVendorPurchaseOrders(@RequestPayload GetVendorPurchaseOrdersRequest request) {
        ApiUser apiUser = usersService.getApiUserByUsername(UserContext.current().getApiUsername());
        User user = usersService.getUserByUsername(apiUser.getUser().getUsername());
        List<Integer> vendorIds = new ArrayList<Integer>();
        for (Vendor vendor : user.getVendors()) {
            vendorIds.add(vendor.getId());
        }
        request.setVendorIds(vendorIds);
        return purchaseService.getVendorPurchaseOrders(request);
    }

    @PayloadRoot(localPart = "GetVendorPurchaseOrderDetailRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.VENDOR_API)
    public GetVendorPurchaseOrderDetailResponse getVendorPurchaseOrderDetail(@RequestPayload GetVendorPurchaseOrderDetailRequest request) {
        ApiUser apiUser = usersService.getApiUserByUsername(UserContext.current().getApiUsername());
        User user = usersService.getUserByUsername(apiUser.getUser().getUsername());
        List<Integer> vendorIds = new ArrayList<Integer>();
        for (Vendor vendor : user.getVendors()) {
            vendorIds.add(vendor.getId());
        }
        request.setVendorIds(vendorIds);
        return purchaseService.getVendorPurchaseOrderDetail(request);
    }

    @PayloadRoot(localPart = "CreateCustomerRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.VENDOR_CREATE)
    public CreateCustomerResponse createCustomer(@RequestPayload CreateCustomerRequest request) {
        String customerCode = request.getCustomer().getCode();
        if (request.getCustomer().getBillingAddress() != null) {
            request.getCustomer().getBillingAddress().setPartyCode(customerCode);
        }
        if (request.getCustomer().getShippingAddress() != null) {
            request.getCustomer().getShippingAddress().setPartyCode(customerCode);
        }
        if (request.getCustomer().getPartyContacts() != null) {
            for (WsPartyContact partyContact : request.getCustomer().getPartyContacts()) {
                partyContact.setPartyCode(customerCode);
            }
        }
        return customerService.createCustomer(request);
    }

    @PayloadRoot(localPart = "EditCustomerRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.VENDOR_CREATE)
    public EditCustomerResponse editCustomer(@RequestPayload EditCustomerRequest request) {
        String customerCode = request.getCustomer().getCode();
        if (request.getCustomer().getBillingAddress() != null) {
            request.getCustomer().getBillingAddress().setPartyCode(customerCode);
        }
        if (request.getCustomer().getShippingAddress() != null) {
            request.getCustomer().getShippingAddress().setPartyCode(customerCode);
        }
        if (request.getCustomer().getPartyContacts() != null) {
            for (WsPartyContact partyContact : request.getCustomer().getPartyContacts()) {
                partyContact.setPartyCode(customerCode);
            }
        }
        return customerService.editCustomer(request);
    }

    @PayloadRoot(localPart = "UpdateTrackingStatusRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.UPDATE_TRACKING_STATUS)
    public UpdateTrackingStatusResponse updateShipmentTrackingStatus(@RequestPayload UpdateTrackingStatusRequest request) {
        return shippingService.updateTrackingStatus(request);
    }

    @PayloadRoot(localPart = "CreateOrEditVendorItemTypeRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.VENDOR_CATALOG)
    public CreateOrEditVendorItemTypeResponse createOrEditVendorItemType(@RequestPayload CreateOrEditVendorItemTypeRequest request) {
        ApiUser apiUser = usersService.getApiUserByUsername(UserContext.current().getApiUsername());
        request.setUserId(apiUser.getUser().getId());
        return vendorService.createOrEditVendorItemType(request);
    }

    @PayloadRoot(localPart = "ClosePurchaseOrderRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.PROCUREMENT)
    public ClosePurchaseOrderResponse closePurchaseOrder(@RequestPayload ClosePurchaseOrderRequest request) {
        User user = ConfigurationManager.getInstance().getConfiguration(TenantSystemConfiguration.class).getSystemUser();
        request.setUserId(user.getId());
        return purchaseService.closePurchaseOrder(request);
    }

    @PayloadRoot(localPart = "GetBackOrderItemsRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.VENDOR_API)
    public GetBackOrderItemsResponse getVendorBackOrderItems(@RequestPayload GetBackOrderItemsRequest request) {
        ApiUser apiUser = usersService.getApiUserByUsername(UserContext.current().getApiUsername());
        Set<Vendor> vendors = apiUser.getUser().getVendors();
        if (vendors.isEmpty()) {
            GetBackOrderItemsResponse response = new GetBackOrderItemsResponse();
            response.addError(new WsError(WsResponseCode.INVALID_USER_ID.code(), WsResponseCode.INVALID_USER_ID.message()));
            return response;
        } else {
            Vendor vendor = vendors.iterator().next();
            request.setVendorId(vendor.getId());
            UserContext.current().setFacility(vendor.getFacility());
            return purchaseService.getBackOrderItems(request);
        }
    }

    @PayloadRoot(localPart = "EditVendorItemTypeRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.VENDOR_API)
    public EditVendorItemTypeResponse editVendorItemType(@RequestPayload EditVendorItemTypeRequest request) {
        ApiUser apiUser = usersService.getApiUserByUsername(UserContext.current().getApiUsername());
        if (!apiUser.getUser().getVendors().isEmpty()) {
            Vendor vendor = vendorService.getVendorById(apiUser.getUser().getVendors().iterator().next().getId());
            request.getVendorItemType().setVendorCode(vendor.getCode());
            UserContext.current().setFacility(vendor.getFacility());
            request.setUserId(apiUser.getUser().getId());
            request.setActionType("API");
            return vendorService.editVendorItemType(request);
        } else {
            EditVendorItemTypeResponse response = new EditVendorItemTypeResponse();
            response.addError(new WsError(WsResponseCode.INVALID_USER_ID.code(), WsResponseCode.INVALID_USER_ID.message()));
            return response;
        }
    }

    @PayloadRoot(localPart = "CreateOrEditVendorRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.MINIMAL)
    public CreateOrEditVendorResponse createOrEditVendor(@RequestPayload CreateOrEditVendorRequest request) {
        return vendorService.createOrEditVendor(request);
    }

    @PayloadRoot(localPart = "GetItemDetailRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.LOOKUP_SALE_ORDER)
    public GetItemDetailResponse getItemDetail(@RequestPayload GetItemDetailRequest request) {
        return itemService.getItemDetail(request);
    }

    @PayloadRoot(localPart = "EditSaleOrderItemMetadataRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.SALE_ORDER_METADATA_EDIT)
    public EditSaleOrderItemMetadataResponse editSaleOrderItemMetadata(@RequestPayload EditSaleOrderItemMetadataRequest request) {
        return saleOrderService.editSaleOrderItemMetadata(request);
    }

    @PayloadRoot(localPart = "GetPurchaseOrdersRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.PROCUREMENT)
    public GetPurchaseOrdersResponse getPurchaseOrders(@RequestPayload GetPurchaseOrdersRequest request) {
        return purchaseService.getPurchaseOrders(request);
    }

    @PayloadRoot(localPart = "GetPurchaseOrderDetailRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.PROCUREMENT)
    public GetPurchaseOrderResponse getPurchaseOrderDetails(@RequestPayload GetPurchaseOrderRequest request) {
        return purchaseService.getPurchaseOrderDetail(request);
    }

    @PayloadRoot(localPart = "GetInflowReceiptsRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.INFLOW_GRN_SEARCH)
    public GetInflowReceiptsResponse getInflowReceipts(@RequestPayload GetInflowReceiptsRequest request) {
        return inflowService.getInflowReceipts(request);
    }

    @PayloadRoot(localPart = "GetInflowReceiptDetailRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.INFLOW_GRN_SEARCH)
    public GetInflowReceiptResponse getInflowReceipts(@RequestPayload GetInflowReceiptRequest request) {
        return inflowService.getInflowReceipt(request);
    }

    @PayloadRoot(localPart = "GetLoginTokenRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.MINIMAL)
    public GetLoginTokenResponse getLoginToken(@RequestPayload GetLoginTokenRequest request) {
        ApiUser apiUser = usersService.getApiUserByUsername(UserContext.current().getApiUsername());
        request.setUserId(apiUser.getUser().getId());
        return usersService.getLoginToken(request);
    }

    @PayloadRoot(localPart = "EditScriptConfigRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.MINIMAL)
    public EditScriptConfigResponse editScriptConfig(@RequestPayload EditScriptConfigRequest request) {
        return scriptService.editScriptConfig(request);
    }

    @PayloadRoot(localPart = "ReloadConfigurationRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.MINIMAL)
    public ReloadConfigurationResponse reloadConfiguration(@RequestPayload ReloadConfigurationRequest request) {
        return reloadService.reload(request);
    }

    @PayloadRoot(localPart = "GetShippingPackagesRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.MINIMAL)
    public GetShippingPackagesResponse getShippingPackages(@RequestPayload GetShippingPackagesRequest request) {
        GetShippingPackagesResponse response = new GetShippingPackagesResponse();
        ApiUser apiUser = usersService.getApiUserByUsername(UserContext.current().getApiUsername());
        User user = apiUser.getUser();
        if (user.getUserFacilities().size() == 1) {
            UserContext.current().setFacility(user.getUserFacilities().iterator().next());
            response = shippingService.getShippingPackages(request);
        } else {
            response.addError(new WsError(WsResponseCode.INVALID_API_REQUEST.message()));
        }
        return response;
    }

    @PayloadRoot(localPart = "GetShippingPackageDetailRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.MINIMAL)
    public GetShippingPackageDetailResponse getShippingPackageDetails(@RequestPayload GetShippingPackageDetailRequest request) {
        GetShippingPackageDetailResponse response = new GetShippingPackageDetailResponse();
        ApiUser apiUser = usersService.getApiUserByUsername(UserContext.current().getApiUsername());
        User user = apiUser.getUser();
        if (user.getUserFacilities().size() == 1) {
            UserContext.current().setFacility(user.getUserFacilities().iterator().next());
            response = shippingService.getShippingPackageDetails(request);
        } else {
            response.addError(new WsError(WsResponseCode.INVALID_API_REQUEST.message()));
        }
        return response;
    }

    @PayloadRoot(localPart = "CreateInvoiceAndGenerateLabelRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.CUSTOMER_INVOICE)
    public CreateInvoiceAndGenerateLabelResponse createInvoiceAndGenerateLabel(@RequestPayload CreateInvoiceAndGenerateLabelRequest request) {
        ApiUser apiUser = usersService.getApiUserByUsername(UserContext.current().getApiUsername());
        User user = apiUser.getUser();
        CreateInvoiceAndGenerateLabelResponse response = new CreateInvoiceAndGenerateLabelResponse();
        if (user.getUserFacilities().size() == 1) {
            UserContext.current().setFacility(user.getUserFacilities().iterator().next());
            request.setUserId(user.getId());
            response = shippingProviderService.createInvoiceAndGenerateLabel(request);
        } else {
            response.addError(new WsError(WsResponseCode.INVALID_API_REQUEST.message()));
        }
        return response;
    }

    @PayloadRoot(localPart = "GetShippingPackageShipmentDetailsRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.CUSTOMER_INVOICE)
    public GetShippingPackageShipmentDetailsResponse getShippingPackageShipmentDetails(@RequestPayload GetShippingPackageShipmentDetailsRequest request) {
        ApiUser apiUser = usersService.getApiUserByUsername(UserContext.current().getApiUsername());
        User user = apiUser.getUser();
        GetShippingPackageShipmentDetailsResponse response = new GetShippingPackageShipmentDetailsResponse();
        if (user.getUserFacilities().size() == 1) {
            UserContext.current().setFacility(user.getUserFacilities().iterator().next());
            response = shippingProviderService.getShippingPackageShipmentDetails(request);
        } else {
            response.addError(new WsError(WsResponseCode.INVALID_API_REQUEST.code(), WsResponseCode.INVALID_API_REQUEST.message()));
        }
        return response;
    }

    @PayloadRoot(localPart = "CreateInvoiceAndAllocateShippingProviderRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.CUSTOMER_INVOICE)
    public CreateInvoiceAndAllocateShippingProviderResponse createInvoiceAndAllocateShippingProvider(@RequestPayload CreateInvoiceAndAllocateShippingProviderRequest request) {
        ApiUser apiUser = usersService.getApiUserByUsername(UserContext.current().getApiUsername());
        User user = apiUser.getUser();
        request.setUserId(user.getId());
        return shippingProviderService.createInvoiceAndAllocateShippingProvider(request);
    }

    @PayloadRoot(localPart = "ForceDispatchShippingPackageRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.FORCE_DISPATCH_SHIPPING_PACKAGE)
    public ForceDispatchShippingPackageResponse forceDispatchShippingPackage(@RequestPayload ForceDispatchShippingPackageRequest request) {
        ApiUser apiUser = usersService.getApiUserByUsername(UserContext.current().getApiUsername());
        User user = apiUser.getUser();
        ForceDispatchShippingPackageResponse response = new ForceDispatchShippingPackageResponse();
        if (user.getUserFacilities().size() == 1) {
            UserContext.current().setFacility(user.getUserFacilities().iterator().next());
            response = dispatchService.forceDispatchShippingPackage(request);
        } else {
            response.addError(new WsError(WsResponseCode.INVALID_API_REQUEST.code(), WsResponseCode.INVALID_API_REQUEST.message()));
        }
        return response;
    }

    @PayloadRoot(localPart = "AllocateShippingProviderRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.CUSTOMER_INVOICE)
    public AllocateShippingProviderResponse allocateShippingProvider(@RequestPayload AllocateShippingProviderRequest request) {
        return shippingProviderService.allocateShippingProvider(request);
    }

    @PayloadRoot(localPart = "CreateShippingManifestRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.MINIMAL)
    public CreateShippingManifestResponse createShippingManifest(@RequestPayload CreateShippingManifestRequest request) {
        CreateShippingManifestResponse response = new CreateShippingManifestResponse();
        ApiUser apiUser = usersService.getApiUserByUsername(UserContext.current().getApiUsername());
        User user = apiUser.getUser();
        if (user.getUserFacilities().size() == 1) {
            UserContext.current().setFacility(user.getUserFacilities().iterator().next());
            request.setUserId(user.getId());
            response = dispatchService.createShippingManifest(request);
        } else {
            response.addError(new WsError(WsResponseCode.INVALID_API_REQUEST.message()));
        }
        return response;
    }

    @PayloadRoot(localPart = "DispatchShippingPackageRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.MINIMAL)
    public DispatchShippingPackageResponse dispatchShippingPackage(@RequestPayload DispatchShippingPackageRequest request) {
        return dispatchService.dispatchReadyToShipShippingPackage(request);
    }

    @PayloadRoot(localPart = "DispatchSaleOrderItemsRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.MINIMAL)
    public DispatchSaleOrderItemsResponse dispatchSaleOrderItems(@RequestPayload DispatchSaleOrderItemsRequest request) {
        return dispatchService.dispatchSaleOrderItems(request);
    }

    @PayloadRoot(localPart = "AddShippingPackagesToManifestRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.MINIMAL)
    public AddShippingPackagesToManifestResponse addShippingPackageToManifestRequest(@RequestPayload AddShippingPackagesToManifestRequest request) {
        AddShippingPackagesToManifestResponse response = new AddShippingPackagesToManifestResponse();
        ApiUser apiUser = usersService.getApiUserByUsername(UserContext.current().getApiUsername());
        User user = apiUser.getUser();
        if (user.getUserFacilities().size() == 1) {
            UserContext.current().setFacility(user.getUserFacilities().iterator().next());
            response = dispatchService.addShippingPackagesToManifest(request);
        } else {
            response.addError(new WsError(WsResponseCode.INVALID_API_REQUEST.message()));
        }
        return response;
    }

    @PayloadRoot(localPart = "CloseShippingManifestRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.MINIMAL)
    public CloseShippingManifestResponse closeShippingManifest(@RequestPayload CloseShippingManifestRequest request) {
        CloseShippingManifestResponse response = new CloseShippingManifestResponse();
        ApiUser apiUser = usersService.getApiUserByUsername(UserContext.current().getApiUsername());
        User user = apiUser.getUser();
        if (user.getUserFacilities().size() == 1) {
            UserContext.current().setFacility(user.getUserFacilities().iterator().next());
            response = dispatchService.closeShippingManifest(request);
        } else {
            response.addError(new WsError(WsResponseCode.INVALID_API_REQUEST.message()));
        }
        return response;
    }

    @PayloadRoot(localPart = "GetServiceabilityRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.EXPORT_LOCATIONS)
    public GetServiceabilityResponse getServiceability(@RequestPayload GetServiceabilityRequest request) {
        return shippingProviderService.getServiceability(request);
    }

    @PayloadRoot(localPart = "AddOrEditItemLabelsRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.INFLOW_GRN_CREATE_LABELS)
    public AddOrEditItemLabelsResponse createSaleOrder(@RequestPayload AddOrEditItemLabelsRequest request) {
        return inventoryService.addOrEditItemLabels(request);
    }

    @PayloadRoot(localPart = "EditGatePassRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.MATERIAL_MANAGEMENT)
    public EditGatePassResponse editGatePass(@RequestPayload EditGatePassRequest request) {
        return materialService.editGatePass(request);
    }

    @PayloadRoot(localPart = "GetVendorItemTypesRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.VENDOR_API)
    public GetVendorItemTypesResponse getVendorItemTypes(@RequestPayload GetVendorItemTypesRequest request) {
        ApiUser apiUser = usersService.getApiUserByUsername(UserContext.current().getApiUsername());
        if (!apiUser.getUser().getVendors().isEmpty()) {
            Vendor vendor = vendorService.getVendorById(apiUser.getUser().getVendors().iterator().next().getId());
            request.setVendorCode(vendor.getCode());
            UserContext.current().setFacility(vendor.getFacility());
            return vendorService.getVendorItemTypes(request);
        } else {
            GetVendorItemTypesResponse response = new GetVendorItemTypesResponse();
            response.addError(new WsError(WsResponseCode.INVALID_USER_ID.code(), WsResponseCode.INVALID_USER_ID.message()));
            return response;
        }
    }

    @PayloadRoot(localPart = "ReceiveReversePickupFromRetailRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.REVERSE_PICKUP_CREATE)
    public ReceiveReversePickupFromRetailResponse receiveReversePickupFromRetail(@RequestPayload ReceiveReversePickupFromRetailRequest request) {
        ApiUser apiUser = usersService.getApiUserByUsername(UserContext.current().getApiUsername());
        request.setUserId(apiUser.getUser().getId());
        return dualCompanyService.receiveReversePickupFromRetail(request);
    }

    @PayloadRoot(localPart = "SplitShippingPackageRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.SHIPPING)
    public SplitShippingPackageResponse splitShippingPackage(@RequestPayload SplitShippingPackageRequest request) {
        ApiUser apiUser = usersService.getApiUserByUsername(UserContext.current().getApiUsername());
        request.setUserId(apiUser.getUser().getId());
        return shippingService.splitShippingPackage(request);
    }

    @PayloadRoot(localPart = "VerifySaleOrderRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.VERIFY_PENDING_ORDERS)
    public VerifySaleOrderResponse verifySaleOrder(@RequestPayload VerifySaleOrderRequest request) {
        return saleOrderService.verifySaleOrder(request);
    }

    @PayloadRoot(localPart = "MarkItemDamagedOutboundQCRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.SHIPPING)
    public MarkItemDamagedOutboundQCResponse verifySaleOrder(@RequestPayload MarkItemDamagedOutboundQCRequest request) {
        return lenskartService.markItemDamagedOutboundQC(request);
    }

    @PayloadRoot(localPart = "GetTenantTransactionDetailsRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.MINIMAL)
    public GetTenantTransactionDetailsResponse getTenantTransactionDetails(@RequestPayload GetTenantTransactionDetailsRequest request) {
        return tenantInvoiceService.getTenantTransactionDetails(request);
    }

    @PayloadRoot(localPart = "CreateASNRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.ASN_CREATE)
    public CreateASNResponse createASN(@RequestPayload CreateASNRequest request) {
        return purchaseService.createASN(request);
    }

    @PayloadRoot(localPart = "GetCustomizedDummyTemplateRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.GLOBAL, accessResource = Name.ADMIN_TEMPLATE)
    public GetCustomizedDummyTemplateResponse getCustomizedDummyTemplate(@RequestPayload GetCustomizedDummyTemplateRequest request) {
        return templateCustomizationService.getCustomizedDummyTemplate(request);
    }

    @PayloadRoot(localPart = "EditShippingPackageRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.IMPORT_SALE_ORDERS)
    public EditShippingPackageResponse editShippingPackage(@RequestPayload EditShippingPackageRequest request) {
        return shippingService.editShippingPackage(request);
    }

    @PayloadRoot(localPart = "CreateVendorRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.VENDOR_CREATE)
    public CreateVendorResponse createVendor(@RequestPayload CreateVendorRequest request) {
        return vendorService.createVendor(request);
    }

    @PayloadRoot(localPart = "EditVendorRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.VENDOR_CREATE)
    public EditVendorResponse editVendor(@RequestPayload EditVendorRequest request) {
        return vendorService.editVendor(request);
    }

    @PayloadRoot(localPart = "SetSaleOrderPriorityRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.MINIMAL)
    public SetSaleOrderPriorityResponse setSaleOrderPriority(@RequestPayload SetSaleOrderPriorityRequest request) {
        return saleOrderService.setSaleOrderPriority(request);
    }

    @PayloadRoot(localPart = "CreateGatePassRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.MATERIAL_MANAGEMENT)
    public CreateGatePassResponse createGatePass(@RequestPayload CreateGatePassRequest request) {
        ApiUser apiUser = usersService.getApiUserByUsername(UserContext.current().getApiUsername());
        request.setUserId(apiUser.getUser().getId());
        return materialService.createGatePass(request);
    }

    @PayloadRoot(localPart = "AddItemToGatePassRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.MATERIAL_MANAGEMENT)
    public AddItemToGatePassResponse addItemToGatepass(@RequestPayload AddItemToGatePassRequest request) {
        return materialService.addItemToGatePass(request);
    }

    @PayloadRoot(localPart = "AddOrEditNonTraceableGatePassItemRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.MATERIAL_MANAGEMENT)
    public AddOrEditNonTraceableGatePassItemResponse addItemToGatepass(@RequestPayload AddOrEditNonTraceableGatePassItemRequest request) {
        return materialService.addOrEditNonTraceableGatePassItem(request);
    }

    @PayloadRoot(localPart = "AddNonTraceableGatePassItemRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.MATERIAL_MANAGEMENT)
    public AddNonTraceableGatePassItemResponse addItemToGatepass(@RequestPayload AddNonTraceableGatePassItemRequest request) {
        return materialService.addNonTraceableGatePassItem(request);
    }

    @PayloadRoot(localPart = "CompleteGatePassRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.MATERIAL_MANAGEMENT)
    public CompleteGatePassResponse completeGatepass(@RequestPayload CompleteGatePassRequest request) {
        ApiUser apiUser = usersService.getApiUserByUsername(UserContext.current().getApiUsername());
        request.setUserId(apiUser.getUser().getId());
        return materialService.completeGatePass(request);
    }

    @PayloadRoot(localPart = "GenerateItemLabelsRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.VENDOR_API)
    public GenerateItemLabelsResponse generateItemLabels(@RequestPayload GenerateItemLabelsRequest request) {
        ApiUser apiUser = usersService.getApiUserByUsername(UserContext.current().getApiUsername());
        if (apiUser.getUser().getVendors().isEmpty()) {
            GenerateItemLabelsResponse response = new GenerateItemLabelsResponse();
            response.addError(new WsError(WsResponseCode.INVALID_USER_ID.code(), "No valid user found"));
            return response;
        } else {
            Vendor vendor = vendorService.getVendorById(apiUser.getUser().getVendors().iterator().next().getId());
            request.setVendorId(vendor.getId());
            UserContext.current().setFacility(vendor.getFacility());
            return inventoryService.generateItemLabels(request);
        }
    }

    @PayloadRoot(localPart = "PickShippingPackageRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.MINIMAL)
    public PickShippingPackageResponse pickShippingPackage(@RequestPayload PickShippingPackageRequest request) {
        return packerService.pickShippingPackage(request);
    }

    @PayloadRoot(localPart = "UnblockSaleOrderItemsInventoryRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.SALE_ORDER_HOLD_UNHOLD)
    public UnblockSaleOrderItemsInventoryResponse unblockSaleOrderItemsInventory(@RequestPayload UnblockSaleOrderItemsInventoryRequest request) {
        return saleOrderService.unblockSaleOrderItemsInventory(request);
    }

    @PayloadRoot(localPart = "ModifyPacketSaleOrderRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.SHIPPING)
    public ModifyPacketSaleOrderResponse modifyPacket(@RequestPayload ModifyPacketSaleOrderRequest request) {
        return saleOrderService.modifySaleOrderItems(request);
    }

    @PayloadRoot(localPart = "GetItemTypeInventoryRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.LOOKUP_INVENTORY)
    public GetItemTypeInventoryResponse getItemTypeInventory(@RequestPayload GetItemTypeInventoryRequest request) {
        return lookupService.getItemTypeInventory(request);
    }

    @PayloadRoot(localPart = "GetTaxTypeConfigurationRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.MINIMAL)
    public GetTaxTypeConfigurationResponse getTaxTypeConfigurations(@RequestPayload GetTaxTypeConfigurationRequest request) {
        return taxTypeService.getTaxTypeConfigurations(request);
    }

    @PayloadRoot(localPart = "SearchShippingPackageRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.LOOKUP_SHIPPING_PACKAGE)
    public SearchShippingPackageResponse searchShippingPackage(@RequestPayload SearchShippingPackageRequest request) {
        return shippingService.searchShippingPackages(request);
    }

    @PayloadRoot(localPart = "CreateInvoiceRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.CUSTOMER_INVOICE)
    public CreateInvoiceResponse createInvoice(@RequestPayload CreateInvoiceRequest request) {
        ApiUser apiUser = usersService.getApiUserByUsername(UserContext.current().getApiUsername());
        request.setUserId(apiUser.getUser().getId());
        request.setCommitBlockedInventory(true);
        request.setSkipDetailing(false);
        CreateShippingPackageInvoiceRequest createShippingPackageInvoiceRequest = new CreateShippingPackageInvoiceRequest(request);
        CreateInvoiceResponse invoiceResponse = shippingInvoiceService.createShippingPackageInvoice(createShippingPackageInvoiceRequest);
        return invoiceResponse;
    }

    @PayloadRoot(localPart = "GetInvoiceLabelRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.CUSTOMER_INVOICE)
    public GetInvoiceLabelResponse getInvoiceLabel(@RequestPayload GetInvoiceLabelRequest request) {
        return shippingInvoiceService.getInvoiceLabel(request);
    }

    @PayloadRoot(localPart = "CreateInflowReceiptRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.INFLOW_GRN_CREATE)
    public CreateInflowReceiptResponse createInflowReceipt(@RequestPayload CreateInflowReceiptRequest request) {
        ApiUser apiUser = usersService.getApiUserByUsername(UserContext.current().getApiUsername());
        request.getWsGRN().setUserId(apiUser.getUser().getId());
        return inflowService.createInflowReceipt(request);
    }

    @PayloadRoot(localPart = "AddItemToInflowReceiptRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.INFLOW_GRN_CREATE)
    public AddItemToInflowReceiptResponse addItemToInflowReceipt(@RequestPayload AddItemToInflowReceiptRequest request) {
        return inflowService.addItemToInflowReceipt(request);
    }

    @PayloadRoot(localPart = "DeactivateTenantRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.CREATE_TENANT)
    public DeactivateTenantResponse deactivateTenant(@RequestPayload DeactivateTenantRequest request) {
        return tenantService.deactivateTenant(request);
    }

    @PayloadRoot(localPart = "ReactivateTenantRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.CREATE_TENANT, openForInactiveTenants = true)
    public ReactivateTenantResponse reactivateTenant(@RequestPayload ReactivateTenantRequest request) {
        return tenantService.reactivateTenant(request);
    }

    @PayloadRoot(localPart = "AddShipmentsToBatchRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.SHIPMENT_BATCH)
    public AddShipmentsToBatchResponse addShipmentsToBatch(@RequestPayload AddShipmentsToBatchRequest request) {
        return shippingService.addShipmentsToBatch(request);
    }

    @PayloadRoot(localPart = "CreateVendorCreditInvoiceRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.VENDOR_INVOICE)
    public CreateVendorCreditInvoiceResponse createVendorCreditInvoice(@RequestPayload CreateVendorCreditInvoiceRequest request) {
        ApiUser apiUser = usersService.getApiUserByUsername(UserContext.current().getApiUsername());
        request.setUserId(apiUser.getUser().getId());
        return vendorInvoiceService.createCreditVendorInvoice(request);
    }

    @PayloadRoot(localPart = "AddEditVendorInvoiceItemRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.VENDOR_INVOICE)
    public AddEditVendorInvoiceItemResponse addEditVendorInvoiceItem(@RequestPayload AddEditVendorInvoiceItemRequest request) {
        return vendorInvoiceService.addEditVendorInvoiceItem(request);
    }

    @PayloadRoot(localPart = "GetShippingProviderParametersRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.MINIMAL)
    public GetShippingProviderParametersResponse getShippingProviderParameters(@RequestPayload GetShippingProviderParametersRequest request) {
        return shippingAdminService.getShippingProviderConnectorParameter(request);
    }

    @PayloadRoot(localPart = "CreateBatchRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.SHIPMENT_BATCH)
    public CreateBatchResponse createBatch(@RequestPayload CreateBatchRequest request) {
        return shippingService.createBatch(request);
    }

    @PayloadRoot(localPart = "EditAdvanceShippingNoticeRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.VENDOR)
    public EditAdvanceShippingNoticeResponse editASNMetadata(@RequestPayload EditAdvanceShippingNoticeRequest request) {
        return purchaseService.editAdvanceShippingNoticeMetadata(request);
    }

    @PayloadRoot(localPart = "MarkSaleOrderReturnedRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.SALE_ORDER_RETURN)
    public MarkSaleOrderReturnedResponse markSaleOrderReturned(@RequestPayload MarkSaleOrderReturnedRequest request) {
        ApiUser apiUser = usersService.getApiUserByUsername(UserContext.current().getApiUsername());
        request.setUserId(apiUser.getUser().getId());
        // XXX Temporary hack for TMS, remove ASAP
        List<String> saleOrderItemCodes = saleOrderService.getSaleOrderItemCodes(request.getSaleOrderCode());
        if (saleOrderItemCodes != null && saleOrderItemCodes.size() == 1) {
            List<MarkSaleOrderReturnedRequest.WsSaleOrderItem> saleOrderItems = new ArrayList<>(1);
            saleOrderItems.add(new MarkSaleOrderReturnedRequest.WsSaleOrderItem(saleOrderItemCodes.get(0), ItemTypeInventory.Type.BAD_INVENTORY.name()));
            request.setSaleOrderItems(saleOrderItems);
        }
        return returnsService.markSaleOrderReturned(request);
    }

    @PayloadRoot(localPart = "CreateUserRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.ADMIN_USER)
    public CreateUserResponse createUser(@RequestPayload CreateUserRequest request) {
        return usersService.createUser(request);
    }

    @PayloadRoot(localPart = "SwitchSaleOrderItemFacilityRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.MINIMAL)
    public SwitchSaleOrderItemFacilityResponse switchSaleOrderItemFacility(@RequestPayload SwitchSaleOrderItemFacilityRequest request) {
        return saleOrderService.switchSaleOrderItemFacility(request);
    }

    @PayloadRoot(localPart = "SearchGatePassRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.MATERIAL_MANAGEMENT)
    public SearchGatePassResponse searchGatePass(@RequestPayload SearchGatePassRequest request) {
        return materialService.searchGatePass(request);
    }

    @PayloadRoot(localPart = "RefreshBroadcastMessageRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.MINIMAL)
    public RefreshBroadcastMessageResponse refreshBroadcastMessage(@RequestPayload RefreshBroadcastMessageRequest request) {
        return broadcastService.refreshBroadcastMessage(request);
    }

    @PayloadRoot(localPart = "CompletePutawayRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.PUTAWAY_COMPLETE)
    public CompletePutawayResponse completePutaway(@RequestPayload CompletePutawayRequest request) {
        return putawayService.completePutaway(request);
    }

    @PayloadRoot(localPart = "CreatePutawayListRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.PUTAWAY_CREATE)
    public CreatePutawayListResponse createPutawayList(@RequestPayload CreatePutawayListRequest request) {
        ApiUser apiUser = usersService.getApiUserByUsername(UserContext.current().getApiUsername());
        request.setUserId(apiUser.getUser().getId());
        return putawayService.createPutawayList(request);
    }

    @PayloadRoot(localPart = "CreatePutawayRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.PUTAWAY_CREATE)
    public CreatePutawayResponse createPutaway(@RequestPayload CreatePutawayRequest request) {
        ApiUser apiUser = usersService.getApiUserByUsername(UserContext.current().getApiUsername());
        request.setUserId(apiUser.getUser().getId());
        return putawayService.createPutaway(request);
    }

    @PayloadRoot(localPart = "AddReversePickupSaleOrderItemsToPutawayRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.PUTAWAY_CREATE)
    public AddReversePickupSaleOrderItemsToPutawayResponse createPutaway(@RequestPayload AddReversePickupSaleOrderItemsToPutawayRequest request) {
        ApiUser apiUser = usersService.getApiUserByUsername(UserContext.current().getApiUsername());
        request.setUserId(apiUser.getUser().getId());
        return putawayService.addReversePickupSaleOrderItemsToPutaway(request);
    }

    @PayloadRoot(localPart = "ChangeProductTypeRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.MINIMAL)
    public ChangeProductTypeResponse changeProductType(@RequestPayload ChangeProductTypeRequest request) {
        return tenantService.changeProductType(request);
    }

    @PayloadRoot(localPart = "GetItemTypeRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.ADMIN_CATALOG)
    public GetItemTypeResponse getItemType(@RequestPayload GetItemTypeRequest request) {
        return catalogService.getItemType(request);
    }

    @PayloadRoot(localPart = "AllowSourceRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.CREATE_TENANT)
    public AllowSourceResponse allowSource(@RequestPayload AllowSourceRequest request) {
        return tenantService.addTenantSource(request);
    }

    @PayloadRoot(localPart = "AddOrEditShippingProviderLocationRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.IMPORT_SHIPPING_PROVIDER_LOCATION)
    public AddOrEditShippingProviderLocationResponse addOrEditShippingProviderLocation(@RequestPayload AddOrEditShippingProviderLocationRequest request) {
        return shippingService.addOrEditShippingProviderLocation(request);
    }

    @PayloadRoot(localPart = "GetAvailableFacilitiesRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.MINIMAL)
    public GetAvailableFacilitiesResponse getAvailableFacilities(@RequestPayload GetAvailableFacilitiesRequest request) {
        GetAvailableFacilitiesResponse response = pickupService.getAvailableFacilities(request);
        return response;
    }

    @PayloadRoot(localPart = "GetFacilityDetailsRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.MINIMAL)
    public GetFacilityDetailsResponse getFacilityDetails(@RequestPayload GetFacilityDetailsRequest request) {
        return pickupService.getFacilityDetails(request);
    }

    @PayloadRoot(localPart = "MarkShippingPackageDeliveredRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.SHIPPING)
    public MarkShippingPackageDeliveredResponse markPackageDelivered(@RequestPayload MarkShippingPackageDeliveredRequest request) {
        return dispatchService.markShippingPackageDelivered(request);
    }

    @PayloadRoot(localPart = "MarkSaleOrderItemsDeliveredRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.SHIPPING)
    public MarkSaleOrderItemsDeliveredResponse markSaleOrderItemsDelivered(@RequestPayload MarkSaleOrderItemsDeliveredRequest request) {
        return dispatchService.markSaleOrderItemsDelivered(request);
    }

    @PayloadRoot(localPart = "GetAccessResourceRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.MINIMAL)
    public GetAccessResourceResponse getUserAccessResourcesUsername(@RequestPayload GetAccessResourceRequest request) {
        return usersService.getAccessResourcesByUsername(request);
    }

    @PayloadRoot(localPart = "GetFacilitiesSLARequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.MINIMAL)
    public GetFacilitiesSLAResponse getFacilitiesSLA(@RequestPayload GetFacilitiesSLARequest request) {
        return pickupService.getFacilitiesSLA(request);
    }

    @PayloadRoot(localPart = "ResendPODCodeRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.MINIMAL)
    public ResendPODCodeResponse resendPODCode(@RequestPayload ResendPODCodeRequest request) {
        return dispatchService.resendPODCode(request);
    }

    @PayloadRoot(localPart = "AddItemDetailsRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.ITEM_DETAILING)
    public AddItemDetailsResponse addItemDetail(@RequestPayload AddItemDetailsRequest request) {
        return inflowService.addItemDetails(request);
    }

    @PayloadRoot(localPart = "CreateInvoiceWithDetailsRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.CUSTOMER_INVOICE)
    public CreateInvoiceWithDetailsResponse createInvoiceWithDetails(@RequestPayload CreateInvoiceWithDetailsRequest request) {
        ApiUser apiUser = usersService.getApiUserByUsername(UserContext.current().getApiUsername());
        User user = usersService.getUserByUsername(apiUser.getUser().getUsername());
        request.setUserId(user.getId());
        // XXX Temporary hack for TMS, remove ASAP
        //        List<String> saleOrderItemCodes = saleOrderService.getSaleOrderItemCodes(request.getSaleOrderCode());
        //        if (saleOrderItemCodes != null && saleOrderItemCodes.size() == 1 && request.getInvoice().getInvoiceItems().size() == 1 && request.getInvoice().getInvoiceItems().get(0).getSaleOrderItems().size() == 1) {
        //            request.getInvoice().getInvoiceItems().get(0).getSaleOrderItems().get(0).setSaleOrderItemCode(saleOrderItemCodes.get(0));
        //        }
        return shippingInvoiceService.createInvoiceWithDetails(request);
    }

    @PayloadRoot(localPart = "GetInvoiceDetailsRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.MINIMAL)
    public GetInvoiceDetailsResponse getInvoiceDetails(@RequestPayload GetInvoiceDetailsRequest request) {
        return shippingInvoiceService.getInvoiceDetails(request);
    }

    @PayloadRoot(localPart = "GetInvoiceDetailsByCodeRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.MINIMAL)
    public GetInvoiceDetailsByCodeResponse getInvoiceDetailsByCode(@RequestPayload GetInvoiceDetailsByCodeRequest request) {
        return shippingInvoiceService.getInvoiceDetailsByCode(request);
    }

    @PayloadRoot(localPart = "VerifyShippingPackagePODCodeRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.STORE_PICKUP)
    public VerifyShippingPackagePODCodeResponse verifyShippingPackagePODCode(@RequestPayload VerifyShippingPackagePODCodeRequest request) {
        return dispatchService.verifyShippingPackagePODCode(request);
    }

    @PayloadRoot(localPart = "VerifySaleOrderItemsPODCodeRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.STORE_PICKUP)
    public VerifySaleOrderItemsPODCodeResponse verifySaleOrderItemsPODCode(@RequestPayload VerifySaleOrderItemsPODCodeRequest request) {
        return dispatchService.verifySaleOrderItemsPODCode(request);
    }

    @PayloadRoot(localPart = "GetShippingManifestLabelRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.SHIPPING)
    public GetShippingManifestLabelResponse getShippingManifestLabel(@RequestPayload GetShippingManifestLabelRequest request) {
        GetShippingManifestLabelResponse response = new GetShippingManifestLabelResponse();
        Template template = CacheManager.getInstance().getCache(PrintTemplateCache.class).getTemplateByType(PrintTemplateVO.Type.SHIPPING_MANIFEST.name());
        ApiUser apiUser = usersService.getApiUserByUsername(UserContext.current().getApiUsername());
        User user = apiUser.getUser();
        if (user.getUserFacilities().size() == 1) {
            UserContext.current().setFacility(user.getUserFacilities().iterator().next());
            String shippingManifest = dispatchService.getShippingManifestOutput(request.getShippingManifestCode(), template);
            String shippingManifestEncoded = dispatchService.generateManifestPdfLabel(request.getShippingManifestCode(), shippingManifest);
            response.setShippingManifest(shippingManifestEncoded);
            response.setSuccessful(true);
        } else {
            response.addError(new WsError(WsResponseCode.INVALID_API_REQUEST.message()));
        }
        return response;
    }

    @PayloadRoot(localPart = "ChangeInvoiceCodeRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.MINIMAL)
    public ChangeInvoiceCodeResponse changeInvoiceCode(@RequestPayload ChangeInvoiceCodeRequest request) {
        return shippingInvoiceService.changeInvoiceCode(request);
    }

    @PayloadRoot(localPart = "GetShippingPackagesByStatusCodeRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.IMPORT_SALE_ORDERS)
    public GetShippingPackagesByStatusCodeResponse getShippingPackagesByStatusCode(@RequestPayload GetShippingPackagesByStatusCodeRequest request) {
        return saleOrderService.getShippingPackageByStatusCode(request);
    }

    @PayloadRoot(localPart = "GetFailedSaleOrdersRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.IMPORT_SALE_ORDERS)
    public GetFailedSaleOrdersResponse getFailedSaleOrders(@RequestPayload GetFailedSaleOrdersRequest request) {
        return saleOrderService.getFailedSaleOrders(request);
    }

    @PayloadRoot(localPart = "GetUnverifiedSaleOrdersRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.IMPORT_SALE_ORDERS)
    public GetUnverifiedSaleOrdersResponse getUnverifiedSaleOrders(@RequestPayload GetUnverifiedSaleOrdersRequest request) {
        return saleOrderService.getUnverifiedSaleOrders(request);
    }

    @PayloadRoot(localPart = "GetFacilitySLARequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.MINIMAL)
    public GetFacilitySLAResponse getFacilitySLA(@RequestPayload GetFacilitySLARequest request) {
        return pickupService.getFacilitySLA(request);
    }

    @PayloadRoot(localPart = "PingUniwareRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.MINIMAL)
    public PingUniwareResponse pingUniware(@RequestPayload PingUniwareRequest request) {
        PingUniwareResponse response = new PingUniwareResponse();
        response.setSuccessful(true);
        return response;
    }

    @PayloadRoot(localPart = "AddSaleOrderToManifestRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.MINIMAL)
    public AddSaleOrderToManifestResponse addSaleOrderToManifest(@RequestPayload AddSaleOrderToManifestRequest request) {
        AddSaleOrderToManifestResponse response = new AddSaleOrderToManifestResponse();
        ShippingPackage shippingPackage = shippingService.getValidShippingPackage(request.getSaleOrderCode(), request.getSaleOrderItemCodes());
        if (shippingPackage == null) {
            SaleOrder saleOrder = saleOrderService.getSaleOrderByCode(request.getSaleOrderCode());
            response.addError(new WsError(WsResponseCode.INVALID_SHIPPING_PACKAGE_CODE.code(),
                    "Sale order item codes do not correspond to a package," + "Current sale Order Status" + saleOrder.getStatusCode()));
        } else {
            AddShippingPackageToManifestRequest addShippingPackageToManifestRequest = new AddShippingPackageToManifestRequest();
            addShippingPackageToManifestRequest.setShippingManifestCode(request.getShippingManifestCode());
            addShippingPackageToManifestRequest.setAwbOrShipmentCode(shippingPackage.getCode());
            AddShippingPackageToManifestResponse addShippingPackageToManifestResponse = dispatchService.addShippingPackageToManifest(addShippingPackageToManifestRequest);
            response.setResponse(addShippingPackageToManifestResponse);
        }
        return response;
    }

    @PayloadRoot(localPart = "DispatchSaleOrderRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.MINIMAL)
    public DispatchSaleOrderResponse dispatchSaleOrder(@RequestPayload DispatchSaleOrderRequest request) {
        return dispatchSaleOrderService.dispatchSaleOrder(request);
    }

    @PayloadRoot(localPart = "CreateInvoiceBySaleOrderCodeRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.CUSTOMER_INVOICE)
    public CreateInvoiceBySaleOrderCodeResponse createInvoiceBySaleOrder(@RequestPayload CreateInvoiceBySaleOrderCodeRequest request) {
        CreateInvoiceBySaleOrderCodeResponse response = new CreateInvoiceBySaleOrderCodeResponse();
        ShippingPackage shippingPackage = shippingService.getValidShippingPackage(request.getSaleOrderCode(), request.getSaleOrderItemCodes());
        if (shippingPackage == null) {
            response.addError(new WsError(WsResponseCode.INVALID_SHIPPING_PACKAGE_CODE.code(), "Sale order item codes do not correspond to a package"));
        } else {
            ApiUser apiUser = usersService.getApiUserByUsername(UserContext.current().getApiUsername());
            CreateShippingPackageInvoiceRequest createShippingPackageInvoiceRequest = new CreateShippingPackageInvoiceRequest();
            createShippingPackageInvoiceRequest.setUserId(apiUser.getId());
            createShippingPackageInvoiceRequest.setCommitBlockedInventory(true);
            createShippingPackageInvoiceRequest.setShippingPackageCode(shippingPackage.getCode());

            CreateInvoiceRequest createInvoiceRequest = new CreateInvoiceRequest();
            createInvoiceRequest.setUserId(apiUser.getUser().getId());
            createInvoiceRequest.setCommitBlockedInventory(true);
            createInvoiceRequest.setShippingPackageCode(shippingPackage.getCode());
            if (request.getTaxInformation() != null) {
                Map<String, WsTaxInformation.ProductTax> channelProductIdToTax = new HashMap<>(request.getTaxInformation().getProductTaxes().size());
                for (WsTaxInformation.ProductTax productTax : request.getTaxInformation().getProductTaxes()) {
                    channelProductIdToTax.put(productTax.getChannelProductId(), productTax);
                }
                createInvoiceRequest.setChannelProductIdToTax(channelProductIdToTax);
            }
            CreateInvoiceResponse createInvoiceResponse = shippingInvoiceService.createShippingPackageInvoice(createShippingPackageInvoiceRequest);
            response.setResponse(createInvoiceResponse);
            if (createInvoiceResponse.isSuccessful()) {
                response.setInvoiceCode(createInvoiceResponse.getInvoiceCode());
                response.setShippingPackageCode(createInvoiceResponse.getShippingPackageCode());
            }
        }
        return response;
    }

    @PayloadRoot(localPart = "CreateInvoiceAndAllocateShippingProviderBySaleOrderRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.CUSTOMER_INVOICE)
    public CreateInvoiceAndAllocateShippingProviderBySaleOrderResponse createInvoiceAndAllocateShippingProviderBySaleOrder(
            @RequestPayload CreateInvoiceAndAllocateShippingProviderBySaleOrderRequest request) {
        CreateInvoiceAndAllocateShippingProviderBySaleOrderResponse response = new CreateInvoiceAndAllocateShippingProviderBySaleOrderResponse();
        ShippingPackage shippingPackage = shippingService.getValidShippingPackage(request.getSaleOrderCode(), request.getSaleOrderItemCodes());
        if (shippingPackage == null) {
            SaleOrder saleOrder = saleOrderService.getSaleOrderByCode(request.getSaleOrderCode());
            response.addError(new WsError(WsResponseCode.INVALID_SHIPPING_PACKAGE_CODE.code(),
                    "Sale order item codes do not correspond to a package : " + "Current sale Order Status:" + saleOrder.getStatusCode()));
        } else {
            ApiUser apiUser = usersService.getApiUserByUsername(UserContext.current().getApiUsername());
            CreateInvoiceAndAllocateShippingProviderRequest createInvoiceAndAllocateShippingProviderRequest = new CreateInvoiceAndAllocateShippingProviderRequest();
            createInvoiceAndAllocateShippingProviderRequest.setUserId(apiUser.getUser().getId());
            createInvoiceAndAllocateShippingProviderRequest.setShippingPackageCode(shippingPackage.getCode());
            createInvoiceAndAllocateShippingProviderRequest.setTaxInformation(request.getTaxInformation());
            CreateInvoiceAndAllocateShippingProviderResponse createInvoiceAndAllocateShippingProviderResponse = shippingProviderService.createInvoiceAndAllocateShippingProvider(
                    createInvoiceAndAllocateShippingProviderRequest);
            response.setResponse(createInvoiceAndAllocateShippingProviderResponse);
            if (createInvoiceAndAllocateShippingProviderResponse.isSuccessful()) {
                response.setInvoiceCode(createInvoiceAndAllocateShippingProviderResponse.getInvoiceCode());
                response.setShippingPackageCode(createInvoiceAndAllocateShippingProviderResponse.getShippingPackageCode());
                response.setShippingProviderCode(createInvoiceAndAllocateShippingProviderResponse.getShippingProviderCode());
                response.setTrackingNumber(createInvoiceAndAllocateShippingProviderResponse.getTrackingNumber());
                response.setShippingLabelLink(createInvoiceAndAllocateShippingProviderResponse.getShippingLabelLink());
            }
        }
        return response;
    }

    @PayloadRoot(localPart = "GetConnectorStatusRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.CHANNELS_VIEW)
    public GetConnectorStatusResponse getConnectorStatusResponse(@RequestPayload GetConnectorStatusRequest connectorStatusRequest) {
        return channelService.getChannelConnectorStatus();
    }

    @PayloadRoot(localPart = "EditChannelItemTypePriceRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.PRICE_UPDATE)
    public EditChannelItemTypePriceResponse editChannelItemType(@RequestPayload EditChannelItemTypePriceRequest request) {
        return pricing.editChannelItemType(request);
    }

    @PayloadRoot(localPart = "CancelPriceRecommendationRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.RECOMMENDATION)
    public CancelRecommendationResponse cancelRecommendation(@RequestPayload CancelRecommendationRequest request) {
        return recommendationManagementService.cancelRecommendation(request);
    }

    @PayloadRoot(localPart = "CreatePriceRecommendationRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.RECOMMENDATION)
    public CreateRecommendationResponse createPriceRecommendation(@RequestPayload CreateRecommendationRequest request) {
        request.getRecommendationBody().setRecommendationType(RecommendationType.PRICE.name());
        ;
        return recommendationManagementService.createRecommendation(request);
    }

    @PayloadRoot(localPart = "CreateInventoryRecommendationRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.RECOMMENDATION)
    public CreateRecommendationResponse createInventoryRecommendation(@RequestPayload CreateRecommendationRequest request) {
        return recommendationManagementService.createRecommendation(request);
    }

    @PayloadRoot(localPart = "GetPriceRecommendationsRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.RECOMMENDATION)
    public GetRecommendationsResponse getPriceRecommendation(@RequestPayload GetRecommendationsRequest request) {
        return recommendationManagementService.getRecommendations(request);
    }

    @PayloadRoot(localPart = "GetChannelItemTypePricesRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.RECOMMENDATION)
    public GetChannelItemTypePricesResponse getChannelItemTypePrices(@RequestPayload GetChannelItemTypePricesRequest request) {
        return pricing.getChannelItemTypePrices(request);
    }

    @PayloadRoot(localPart = "GetTenantDetailsRequest", namespace = "http://uniware.unicommerce.com/services/")
    @ResponsePayload
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.MINIMAL)
    public GetTenantDetailsResponse getTenantDetails(@RequestPayload GetTenantDetailsRequest request) {
        GetTenantDetailsResponse response = new GetTenantDetailsResponse();
        GetTenantDetailsResponse.TenantDTO tenant = new GetTenantDetailsResponse.TenantDTO();
        tenant.setTenantCode("abc");
        tenant.setCompanyName("ABC Pvt Ltd");
        tenant.setSellerType(TenantProfileVO.SellerType.HYPERLOCAL);
        tenant.setShippingMethod(TenantProfileVO.ShippingMethod.PREPAID);
        tenant.setCutoffTime(1400);
        response.setTenant(tenant);
        response.setSuccessful(true);
        return response;
    }
}
