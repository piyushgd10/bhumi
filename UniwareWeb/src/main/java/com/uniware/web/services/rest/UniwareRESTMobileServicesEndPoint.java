/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 23-Jun-2015
 *  @author parijat
 */
package com.uniware.web.services.rest;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;

import com.unifier.core.annotation.Level;
import com.unifier.core.api.base.ServiceRequest;
import com.unifier.core.api.datatable.GetDatatableResultRequest;
import com.unifier.core.api.datatable.GetDatatableResultResponse;
import com.unifier.core.api.myaccount.GetAccountBalanceRequest;
import com.unifier.core.api.myaccount.GetAccountBalanceResponse;
import com.unifier.core.api.myaccount.GetPaymentHistoryRequest;
import com.unifier.core.api.myaccount.GetPaymentHistoryResponse;
import com.unifier.core.api.myaccount.GetProductApplicabilityRequest;
import com.unifier.core.api.myaccount.GetProductApplicabilityResponse;
import com.unifier.core.api.myaccount.GetRecentUsageStatisticsRequest;
import com.unifier.core.api.myaccount.GetRecentUsageStatisticsResponse;
import com.unifier.core.api.myaccount.GetUsageHistoryRequest;
import com.unifier.core.api.myaccount.GetUsageHistoryResponse;
import com.unifier.core.api.myaccount.PipeToAccountsRequest;
import com.unifier.core.api.myaccount.RegisterDeviceRequest;
import com.unifier.core.api.myaccount.RegisterDeviceResponse;
import com.unifier.core.api.reports.GetWidgetDataRequest;
import com.unifier.core.api.user.SwitchFacilityRequest;
import com.unifier.core.api.user.SwitchFacilityResponse;
import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.entity.AccessResource.Name;
import com.unifier.core.entity.DashboardWidget;
import com.unifier.core.entity.Role;
import com.unifier.core.entity.User;
import com.unifier.core.entity.UserRole;
import com.unifier.core.export.config.ExportConfig;
import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.StringUtils;
import com.unifier.scraper.sl.exception.ScriptCompilationException;
import com.unifier.scraper.sl.exception.ScriptExecutionException;
import com.unifier.services.export.IExportService;
import com.unifier.services.release.IReleaseUpdateService;
import com.unifier.services.search.IGlobalSearchService;
import com.unifier.services.tenantprofile.service.ITenantProfileService;
import com.unifier.services.users.IUsersService;
import com.unifier.services.vo.TenantProfileVO;
import com.unifier.web.security.UniwareUser;
import com.uniware.core.api.admin.pickset.GetAllPicksetsResponse;
import com.uniware.core.api.catalog.GetItemTypeRequest;
import com.uniware.core.api.catalog.GetItemTypeResponse;
import com.uniware.core.api.catalog.dto.ItemTypeLookupDTO;
import com.uniware.core.api.channel.GetChannelDetailsRequest;
import com.uniware.core.api.channel.GetChannelDetailsResponse;
import com.uniware.core.api.channel.GetChannelInventorySyncStatusRequest;
import com.uniware.core.api.channel.GetChannelInventorySyncStatusResponse;
import com.uniware.core.api.channel.GetChannelSyncAttributesRequest;
import com.uniware.core.api.channel.GetChannelSyncAttributesResponse;
import com.uniware.core.api.channel.SyncChannelItemTypeInventoryRequest;
import com.uniware.core.api.channel.SyncChannelItemTypeInventoryResponse;
import com.uniware.core.api.channel.UpdateInventoryByChannelProductIdRequest;
import com.uniware.core.api.channel.UpdateInventoryByChannelProductIdResponse;
import com.uniware.core.api.common.GenericServiceResponse;
import com.uniware.core.api.cyclecount.BlockShelvesForCycleCountRequest;
import com.uniware.core.api.cyclecount.BlockShelvesForCycleCountResponse;
import com.uniware.core.api.cyclecount.CompleteCountForShelfRequest;
import com.uniware.core.api.cyclecount.CompleteCountForShelfResponse;
import com.uniware.core.api.cyclecount.GetActiveCycleCountRequest;
import com.uniware.core.api.cyclecount.GetBadItemsForCycleCountRequest;
import com.uniware.core.api.cyclecount.GetBadItemsForShelfRequest;
import com.uniware.core.api.cyclecount.GetBadItemsForShelfResponse;
import com.uniware.core.api.cyclecount.GetCycleCountResponse;
import com.uniware.core.api.cyclecount.GetErrorItemsForCycleCountResponse;
import com.uniware.core.api.cyclecount.GetSubCycleCountShelvesRequest;
import com.uniware.core.api.cyclecount.GetSubCycleCountShelvesResponse;
import com.uniware.core.api.cyclecount.GetZonesForCycleCountRequest;
import com.uniware.core.api.cyclecount.RecountShelfRequest;
import com.uniware.core.api.cyclecount.RecountShelfResponse;
import com.uniware.core.api.cyclecount.SearchCycleCountShelfRequest;
import com.uniware.core.api.cyclecount.SearchCycleCountShelfResponse;
import com.uniware.core.api.cyclecount.StartCountingForShelfRequest;
import com.uniware.core.api.cyclecount.StartCountingForShelfResponse;
import com.uniware.core.api.cyclecount.UnblockShelvesRequest;
import com.uniware.core.api.cyclecount.UnblockShelvesResponse;
import com.uniware.core.api.facility.GetCurrentUserFacilitiesRequest;
import com.uniware.core.api.facility.GetCurrentUserFacilitiesResponse;
import com.uniware.core.api.inventory.InventoryAdjustmentRequest;
import com.uniware.core.api.inventory.InventoryAdjustmentResponse;
import com.uniware.core.api.inventory.UpdateInventoryOnChannelRequest;
import com.uniware.core.api.inventory.UpdateInventoryOnChannelResponse;
import com.uniware.core.api.item.GetItemDetailRequest;
import com.uniware.core.api.item.GetItemDetailResponse;
import com.uniware.core.api.material.AddItemToGatePassRequest;
import com.uniware.core.api.material.AddItemToGatePassResponse;
import com.uniware.core.api.material.AddNonTraceableGatePassItemRequest;
import com.uniware.core.api.material.AddNonTraceableGatePassItemResponse;
import com.uniware.core.api.material.AddOrEditNonTraceableGatePassItemRequest;
import com.uniware.core.api.material.AddOrEditNonTraceableGatePassItemResponse;
import com.uniware.core.api.material.CompleteGatePassRequest;
import com.uniware.core.api.material.CompleteGatePassResponse;
import com.uniware.core.api.material.CreateGatePassRequest;
import com.uniware.core.api.material.CreateGatePassResponse;
import com.uniware.core.api.material.DiscardGatePassRequest;
import com.uniware.core.api.material.DiscardGatePassResponse;
import com.uniware.core.api.material.GetGatepassScannableItemDetailsRequest;
import com.uniware.core.api.material.GetGatepassScannableItemDetailsResponse;
import com.uniware.core.api.material.GetGatepassSummaryRequest;
import com.uniware.core.api.material.GetGatepassSummaryResponse;
import com.uniware.core.api.material.RemoveItemFromGatePassRequest;
import com.uniware.core.api.material.RemoveItemFromGatePassResponse;
import com.uniware.core.api.material.RemoveNonTraceableGatePassItemRequest;
import com.uniware.core.api.material.RemoveNonTraceableGatePassItemResponse;
import com.uniware.core.api.packer.AcceptPutbackItemRequest;
import com.uniware.core.api.packer.AcceptPutbackItemResponse;
import com.uniware.core.api.packer.ReceivePicklistRequest;
import com.uniware.core.api.packer.ReceivePicklistResponse;
import com.uniware.core.api.party.PartySearchDTO;
import com.uniware.core.api.picker.AssignPicklistToUserRequest;
import com.uniware.core.api.picker.AssignPicklistToUserResponse;
import com.uniware.core.api.picker.CompletePickBatchRequest;
import com.uniware.core.api.picker.CompletePickBatchResponse;
import com.uniware.core.api.picker.CompletePickingRequest;
import com.uniware.core.api.picker.CompletePickingResponse;
import com.uniware.core.api.picker.CompletePicklistRequest;
import com.uniware.core.api.picker.CompletePicklistResponse;
import com.uniware.core.api.picker.CreatePickBatchRequest;
import com.uniware.core.api.picker.CreatePickBatchResponse;
import com.uniware.core.api.picker.GetPickBucketDetailRequest;
import com.uniware.core.api.picker.GetPickBucketDetailResponse;
import com.uniware.core.api.picker.GetPicklistDetailRequest;
import com.uniware.core.api.picker.GetPicklistDetailResponse;
import com.uniware.core.api.picker.ReceiveNonTraceablePicklistItemRequest;
import com.uniware.core.api.picker.ReceiveNonTraceablePicklistItemResponse;
import com.uniware.core.api.picker.ReceivePickBucketItemRequest;
import com.uniware.core.api.picker.ReceivePickBucketItemResponse;
import com.uniware.core.api.picker.ReceivePickBucketRequest;
import com.uniware.core.api.picker.ReceivePickBucketResponse;
import com.uniware.core.api.picker.ScanItemAtStagingRequest;
import com.uniware.core.api.picker.ScanItemAtStagingResponse;
import com.uniware.core.api.picker.SubmitPickBucketRequest;
import com.uniware.core.api.picker.SubmitPickBucketResponse;
import com.uniware.core.api.prices.CheckChannelItemTypePriceRequest;
import com.uniware.core.api.prices.CheckChannelItemTypePriceResponse;
import com.uniware.core.api.prices.EditChannelItemTypePriceRequest;
import com.uniware.core.api.prices.EditChannelItemTypePriceResponse;
import com.uniware.core.api.prices.GetChannelItemTypePriceRequest;
import com.uniware.core.api.prices.GetChannelItemTypePriceResponse;
import com.uniware.core.api.putaway.AddNonTraceableItemToTransferPutawayRequest;
import com.uniware.core.api.putaway.AddNonTraceableItemToTransferPutawayResponse;
import com.uniware.core.api.putaway.AddTraceableItemToTransferPutawayRequest;
import com.uniware.core.api.putaway.AddTraceableItemToTransferPutawayResponse;
import com.uniware.core.api.putaway.CompletePutawayItemsRequest;
import com.uniware.core.api.putaway.CompletePutawayItemsResponse;
import com.uniware.core.api.putaway.CompletePutawayRequest;
import com.uniware.core.api.putaway.CompletePutawayResponse;
import com.uniware.core.api.putaway.CreatePutawayListRequest;
import com.uniware.core.api.putaway.CreatePutawayListResponse;
import com.uniware.core.api.putaway.CreatePutawayRequest;
import com.uniware.core.api.putaway.CreatePutawayResponse;
import com.uniware.core.api.putaway.GetPutawayRequest;
import com.uniware.core.api.putaway.GetPutawayResponse;
import com.uniware.core.api.putaway.GetPutawayTypesRequest;
import com.uniware.core.api.putaway.GetPutawayTypesResponse;
import com.uniware.core.api.putaway.SearchDirectReturnsRequest;
import com.uniware.core.api.putaway.SearchDirectReturnsResponse;
import com.uniware.core.api.recommendation.ApproveRecommendationRequest;
import com.uniware.core.api.recommendation.ApproveRecommendationResponse;
import com.uniware.core.api.recommendation.GetRecommendationsRequest;
import com.uniware.core.api.recommendation.GetRecommendationsResponse;
import com.uniware.core.api.recommendation.ListRecommendationsRequest;
import com.uniware.core.api.recommendation.RejectRecommendationRequest;
import com.uniware.core.api.recommendation.RejectRecommendationResponse;
import com.uniware.core.api.returns.AddDirectReturnedSaleOrderItemsToPutawayRequest;
import com.uniware.core.api.returns.AddReturnedSaleOrderItemsToPutawayResponse;
import com.uniware.core.api.saleorder.GetSaleOrderMobileResponse;
import com.uniware.core.api.saleorder.GetSaleOrderRequest;
import com.uniware.core.api.saleorder.GetSaleOrderResponse;
import com.uniware.core.api.saleorder.GetSaleOrderSummaryMobileResponse;
import com.uniware.core.api.saleorder.VerifySaleOrdersRequest;
import com.uniware.core.api.saleorder.VerifySaleOrdersResponse;
import com.uniware.core.api.saleorder.display.GetSaleOrderLineItemsRequest;
import com.uniware.core.api.saleorder.display.GetSaleOrderLineItemsResponse;
import com.uniware.core.api.saleorder.display.GetSaleOrderSummaryRequest;
import com.uniware.core.api.saleorder.display.GetSaleOrderSummaryResponse;
import com.uniware.core.api.search.GlobalSearchRequest;
import com.uniware.core.api.search.GlobalSearchResponse;
import com.uniware.core.api.shipping.AddShippingPackageToManifestRequest;
import com.uniware.core.api.shipping.AddShippingPackageToManifestResponse;
import com.uniware.core.api.shipping.AddSignatureToShippingManifestRequest;
import com.uniware.core.api.shipping.AddSignatureToShippingManifestResponse;
import com.uniware.core.api.shipping.CloseShippingManifestRequest;
import com.uniware.core.api.shipping.CloseShippingManifestResponse;
import com.uniware.core.api.shipping.CreateShippingManifestRequest;
import com.uniware.core.api.shipping.CreateShippingManifestResponse;
import com.uniware.core.api.shipping.FetchShippingManifestPdfRequest;
import com.uniware.core.api.shipping.FetchShippingManifestPdfResponse;
import com.uniware.core.api.shipping.GetChannelsForManifestRequest;
import com.uniware.core.api.shipping.GetChannelsForManifestResponse;
import com.uniware.core.api.shipping.GetEligibleShippingProvidersForManifestRequest;
import com.uniware.core.api.shipping.GetEligibleShippingProvidersForManifestResponse;
import com.uniware.core.api.shipping.GetShippingManifestRequest;
import com.uniware.core.api.shipping.GetShippingManifestResponse;
import com.uniware.core.api.shipping.RemoveManifestItemRequest;
import com.uniware.core.api.shipping.RemoveManifestItemResponse;
import com.uniware.core.api.systemConfig.GetSystemConfigurationByNameRequest;
import com.uniware.core.api.systemConfig.GetSystemConfigurationByNameResponse;
import com.uniware.core.api.tenant.GetTenantProfileRequest;
import com.uniware.core.api.tenant.GetTenantProfileResponse;
import com.uniware.core.api.tenant.GetTenantProfileResponse.TenantProfileDTO;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.api.warehouse.GetAllShelfCodesRequest;
import com.uniware.core.api.warehouse.GetAllShelfCodesResponse;
import com.uniware.core.cache.EnvironmentPropertiesCache;
import com.uniware.core.cache.FacilityCache;
import com.uniware.core.cache.RolesCache;
import com.uniware.core.cache.TenantCache;
import com.uniware.core.entity.Channel;
import com.uniware.core.entity.Facility;
import com.uniware.core.entity.Feature;
import com.uniware.core.entity.Tenant;
import com.uniware.core.utils.UserContext;
import com.uniware.core.vo.UserProfileVO;
import com.uniware.services.accounts.IAccountService;
import com.uniware.services.admin.pickset.IPicksetService;
import com.uniware.services.cache.ChannelCache;
import com.uniware.services.catalog.ICatalogService;
import com.uniware.services.channel.IChannelInventorySyncService;
import com.uniware.services.channel.IChannelService;
import com.uniware.services.configuration.DashboardWidgetConfiguration;
import com.uniware.services.configuration.ExportJobConfiguration;
import com.uniware.services.configuration.SystemConfiguration;
import com.uniware.services.configuration.data.manager.ProductConfiguration;
import com.uniware.services.cyclecount.ICycleCountService;
import com.uniware.services.dashboard.IDashboardWidgetService;
import com.uniware.services.inventory.IInventoryService;
import com.uniware.services.item.IItemService;
import com.uniware.services.material.IMaterialService;
import com.uniware.services.packer.IPackerService;
import com.uniware.services.party.IPartyService;
import com.uniware.services.picker.IPickerService;
import com.uniware.services.pricing.IPricingFacade;
import com.uniware.services.putaway.IPutawayService;
import com.uniware.services.recommendation.IRecommendationManagementService;
import com.uniware.services.saleorder.ISaleOrderDisplayService;
import com.uniware.services.saleorder.ISaleOrderService;
import com.uniware.services.shipping.IDispatchService;
import com.uniware.services.shipping.IShippingService;
import com.uniware.services.staging.IShipmentStagingService;
import com.uniware.services.warehouse.IFacilityService;
import com.uniware.services.warehouse.IShelfService;
import com.uniware.web.metadata.MobileUserMetadataDTO;
import com.uniware.web.metadata.UserMetadataDTO;
import com.uniware.web.security.annotation.UniwareEndPointAccess;

/**
 * @author parijat
 */
@Controller
@Path("/services/rest/mobile/v1/")
public class UniwareRESTMobileServicesEndPoint {

    @Autowired
    private IPicksetService                  picksetService;

    @Autowired
    private ITenantProfileService            tenantProfileService;

    @Autowired
    private ISaleOrderDisplayService         saleOrderDisplayService;

    @Autowired
    private IExportService                   exportService;

    @Autowired
    private IUsersService                    usersService;

    @Autowired
    private ISaleOrderService                saleOrderService;

    @Autowired
    private ICatalogService                  catalogService;

    @Autowired
    private IFacilityService                 facilityService;

    @Autowired
    private IAccountService                  accountService;

    @Autowired
    private IDashboardWidgetService          dashboardWidgetService;

    @Autowired
    private IGlobalSearchService             searchService;

    @Autowired
    private IChannelService                  channelService;

    @Autowired
    private IInventoryService                inventoryService;

    @Autowired
    private IPricingFacade                   pricingService;

    @Autowired
    private IChannelInventorySyncService     channelInventorySyncService;

    @Autowired
    private IRecommendationManagementService recommendationManagementService;

    @Autowired
    private IDispatchService                 dispatchService;

    @Autowired
    private IShippingService                 shippingService;

    @Autowired
    private IPutawayService                  putawayService;

    @Autowired
    private IShelfService                    shelfService;

    @Autowired
    private IReleaseUpdateService            releaseService;

    @Autowired
    private IMaterialService                 materialService;

    @Autowired
    private IPartyService                    partyService;

    @Autowired
    private IItemService                     itemService;

    @Autowired
    private IPickerService                   pickerService;

    @Autowired
    private ICycleCountService               cycleCountService;

    @Autowired
    private IPackerService                   packerService;

    @Autowired
    private IShipmentStagingService          shipmentStagingService;

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/tenant/validate")
    @POST
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.MINIMAL, openForInactiveTenants = true)
    public GetTenantProfileResponse getTenantProfile(GetTenantProfileRequest request) {
        GetTenantProfileResponse response = new GetTenantProfileResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            TenantProfileVO profileVO = tenantProfileService.getTenantProfileByAccessUrl(request.getAccessUrl());
            if (profileVO == null) {
                context.addError(WsResponseCode.INVALID_TENANT, WsResponseCode.INVALID_TENANT.message());
            } else {
                Tenant tenant = CacheManager.getInstance().getCache(TenantCache.class).getTenantByAccessUrl(UserContext.current().getTenant().getAccessUrl());
                String accountCode = tenant.getAccountCode();
                String tenantStatus = tenant.getStatusCode().toString();
                TenantProfileDTO dto = response.new TenantProfileDTO(profileVO.getTenantCode(), profileVO.getAccessUrl(), profileVO.getTenantType(), profileVO.getMobile(),
                        profileVO.getStatusCode(), profileVO.getCurrentVersion(), accountCode);
                List<Feature> features = ConfigurationManager.getInstance().getConfiguration(ProductConfiguration.class).getAllFeatures();
                List<GetTenantProfileResponse.FeatureDTO> availableFeatures = new ArrayList<>();
                if (features != null) {
                    for (Feature feature : features) {
                        availableFeatures.add(response.new FeatureDTO(feature.getCode(), feature.getName()));
                    }
                }
                response.setTenantStatus(tenantStatus);
                response.setFeatures(availableFeatures);
                response.setProfile(dto);
                response.setSuccessful(true);
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("export/data")
    @POST
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.IMPLICIT_ACCESS)
    public GetDatatableResultResponse getDatatableResult(GetDatatableResultRequest request) {
        User user = usersService.getUserByUsername(UserContext.current().getUniwareUserName());
        request.setUserId(user.getId());
        Collection<? extends GrantedAuthority> authorities = SecurityContextHolder.getContext().getAuthentication().getAuthorities();
        HashSet<String> accessResources = new HashSet<>();
        RolesCache rolesCache = CacheManager.getInstance().getCache(RolesCache.class);
        for (GrantedAuthority authority : authorities) {
            Set<String> resources = rolesCache.getAccessResourcesByRole(authority.getAuthority());
            accessResources.addAll(resources);
        }
        ExportConfig exportConfig = ConfigurationManager.getInstance().getConfiguration(ExportJobConfiguration.class).getExportConfigByName(request.getName());
        if (exportConfig != null) {
            if (accessResources.contains(exportConfig.getAccessResourceName())) {
                return exportService.getDatatableResult(request);
            } else {
                throw new WebApplicationException(HttpURLConnection.HTTP_FORBIDDEN);
            }
        } else {
            throw new WebApplicationException(HttpURLConnection.HTTP_FORBIDDEN);
        }
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("saleorder/get")
    @POST
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.LOOKUP_SALE_ORDER)
    public GetSaleOrderMobileResponse getSaleOrder(GetSaleOrderRequest request) {
        GetSaleOrderResponse response = saleOrderService.getSaleOrder(request);
        GetSaleOrderMobileResponse mobileResponse = new GetSaleOrderMobileResponse(response);
        GetChannelDetailsRequest channelRequest = new GetChannelDetailsRequest();
        if (response.getSaleOrderDTO() != null) {
            channelRequest.setChannelCode(response.getSaleOrderDTO().getChannel());
            GetChannelDetailsResponse channelResponse = channelService.getChannelDetailsByChannelCode(channelRequest);
            mobileResponse.setColorCode(channelResponse.getChannelDetailDTO().getColorCode());
            mobileResponse.setShortName(channelResponse.getChannelDetailDTO().getShortName());
            mobileResponse.setSourceCode(channelResponse.getChannelDetailDTO().getSource().getCode());
        }
        return mobileResponse;
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("product/get")
    @POST
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.ADMIN_CATALOG)
    public GetItemTypeResponse getItemType(GetItemTypeRequest request) {
        return catalogService.getItemType(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("fetchSummary")
    @POST
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.LOOKUP_SALE_ORDER)
    public GetSaleOrderSummaryMobileResponse getSaleOrderSummary(GetSaleOrderSummaryRequest request) {
        GetSaleOrderSummaryResponse response = saleOrderDisplayService.getSaleOrderSummary(request);
        GetSaleOrderSummaryMobileResponse mobileResponse = new GetSaleOrderSummaryMobileResponse(response);
        if (response.getSaleOrderSummary() != null) {
            Channel channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelByName(response.getSaleOrderSummary().getChannel());
            mobileResponse.setColorCode(channel.getColorCode());
            mobileResponse.setShortName(channel.getShortName());
            mobileResponse.setSourceCode(channel.getSourceCode());
        }
        return mobileResponse;
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("user/facilities")
    @POST
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.MINIMAL)
    public GetCurrentUserFacilitiesResponse getFacilitiesForCurrentUser(GetCurrentUserFacilitiesRequest request) {
        GetCurrentUserFacilitiesResponse response = new GetCurrentUserFacilitiesResponse();
        User user = usersService.getUserByUsername(UserContext.current().getUniwareUserName());
        boolean accessibleAllFacilities = false;
        Integer currentFacilityId = null;
        Set<Integer> accessibleFacilities = new TreeSet<Integer>(new Comparator<Integer>() {

            @Override
            public int compare(Integer facilityId1, Integer facilityId2) {
                FacilityCache facilityCache = CacheManager.getInstance().getCache(FacilityCache.class);
                Facility facility1 = facilityCache.getFacilityById(facilityId1);
                Facility facility2 = facilityCache.getFacilityById(facilityId2);
                return (facility1 != null && facility2 != null) ? facility1.getCode().compareTo(facility2.getCode()) : 0;
            }
        });
        FacilityCache facilityCache = CacheManager.getInstance().getCache(FacilityCache.class);
        boolean hasMultipleFacilities = facilityCache.getFacilities().size() > 1;
        if (user.getUserRoles().size() > 0) {
            for (UserRole userRole : user.getUserRoles()) {
                if (userRole.getFacility() != null) {
                    if (facilityCache.getFacilityById(userRole.getFacility().getId()) != null) {
                        accessibleFacilities.add(userRole.getFacility().getId());
                    }
                } else if (hasMultipleFacilities) {
                    accessibleAllFacilities = true;
                }
            }

            if ((user.getCurrentFacility() != null && !accessibleFacilities.contains(user.getCurrentFacility().getId()))
                    || (user.getCurrentFacility() == null && !accessibleAllFacilities)) {
                if (accessibleFacilities.size() > 0) {
                    currentFacilityId = accessibleFacilities.iterator().next();
                }
            } else {
                if (user.getCurrentFacility() != null) {
                    currentFacilityId = user.getCurrentFacility().getId();
                }
            }
        }

        List<Facility> userFacilities = facilityService.getFacilitiesById(accessibleFacilities);
        for (Facility facility : userFacilities) {
            response.getFacilityDTOList().add(new GetCurrentUserFacilitiesResponse.FacilityDTO(facility));
            if (currentFacilityId != null && currentFacilityId.equals(facility.getId())) {
                response.setCurrentFacilityCode(facility.getCode());
            }
        }
        response.setAccessibleAllFacilities(accessibleAllFacilities);
        response.setSuccessful(true);
        return response;
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("switchfacility")
    @POST
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.MINIMAL)
    public SwitchFacilityResponse switchFacility(SwitchFacilityRequest request) {
        User user = usersService.getUserByUsername(UserContext.current().getUniwareUserName());
        request.setUserId(user.getId());
        return usersService.switchFacility(request);

    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("paymenthistory")
    @POST
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.MINIMAL)
    public GetPaymentHistoryResponse getPaymentHistory(GetPaymentHistoryRequest request) {
        return accountService.getPaymentHistory(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("usagehistory")
    @POST
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.MINIMAL)
    public GetUsageHistoryResponse getUsageHistory(GetUsageHistoryRequest request) {
        return accountService.getUsageHistory(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("widget/get")
    @POST
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.MINIMAL)
    public String getChart(GetWidgetDataRequest request) throws ScriptExecutionException, ScriptCompilationException {
        DashboardWidgetConfiguration configuration = ConfigurationManager.getInstance().getConfiguration(DashboardWidgetConfiguration.class);
        DashboardWidget dashboardWidget = configuration.getDashboardWidgetByCode(request.getWidgetCode());
        Collection<? extends GrantedAuthority> authorities = SecurityContextHolder.getContext().getAuthentication().getAuthorities();
        HashSet<String> accessResources = new HashSet<>();
        RolesCache rolesCache = CacheManager.getInstance().getCache(RolesCache.class);
        for (GrantedAuthority authority : authorities) {
            Set<String> resources = rolesCache.getAccessResourcesByRole(authority.getAuthority());
            accessResources.addAll(resources);
        }
        if (accessResources.contains(dashboardWidget.getAccessResourceName())) {
            return dashboardWidgetService.getDashboardWidgetData(request, dashboardWidget);
        } else {
            throw new WebApplicationException(HttpURLConnection.HTTP_FORBIDDEN);
        }
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("productApplicability/get")
    @POST
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.MINIMAL, openForInactiveTenants = true)
    public GetProductApplicabilityResponse getProductApplicability(GetProductApplicabilityRequest request) {
        return accountService.getProductApplicability(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("globalSearch")
    @POST
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.IMPLICIT_ACCESS)
    public GlobalSearchResponse globalSearch(GlobalSearchRequest request) {
        return searchService.globalSearch(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("getChannels")
    @POST
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.CHANNELS_VIEW)
    public GetChannelSyncAttributesResponse getChannelSyncAttributes(GetChannelSyncAttributesRequest request) {
        return channelService.getChannelSyncAttributes(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("balance/get")
    @POST
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.ACCOUNT_VIEW)
    public GetAccountBalanceResponse getAccountBalance(GetAccountBalanceRequest request) {
        return accountService.getAccountBalance(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("p/accounts")
    @POST
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.MINIMAL, openForInactiveTenants = true)
    public String pipeToAccounts(PipeToAccountsRequest request) {
        return accountService.pipeToAccounts(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("usageStatistics/get")
    @POST
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.MINIMAL)
    public GetRecentUsageStatisticsResponse getRecentUsageStatistics(GetRecentUsageStatisticsRequest request) {
        return accountService.getRecentUsageStatistics(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("register/device")
    @POST
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.MINIMAL)
    public RegisterDeviceResponse registerDevice(RegisterDeviceRequest request) {
        User user = usersService.getUserByUsername(UserContext.current().getUniwareUserName());
        request.setUsername(user.getUsername());
        return usersService.registerDevice(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("channel/prices/fetch")
    @POST
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.PRICE_VIEW)
    public GetChannelItemTypePriceResponse getChannelItemTypePrice(GetChannelItemTypePriceRequest request) {
        return pricingService.getChannelItemTypePrice(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("channel/prices/check")
    @POST
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.PRICE_VIEW)
    public CheckChannelItemTypePriceResponse checkChannelItemTypePrice(CheckChannelItemTypePriceRequest request) {
        return pricingService.checkChannelItemTypePrice(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("channel/prices/edit")
    @POST
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.PRICE_UPDATE)
    public EditChannelItemTypePriceResponse editChannelItemType(final EditChannelItemTypePriceRequest request) {
        return pricingService.editChannelItemType(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("fetchLineItems")
    @POST
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.LOOKUP_SALE_ORDER)
    public GetSaleOrderLineItemsResponse getSaleOrderLineItems(GetSaleOrderLineItemsRequest request) {
        return saleOrderDisplayService.getSaleOrderLineItems(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/inventory/adjust")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.INFLOW_INVENTORY_ADJUST)
    public InventoryAdjustmentResponse adjustInventory(InventoryAdjustmentRequest request) {
        User user = usersService.getUserByUsername(UserContext.current().getUniwareUserName());
        request.setUserId(user.getId());
        return inventoryService.adjustInventory(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("updateInventory/channelProductId")
    @POST
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.CHANNELS_VIEW)
    public UpdateInventoryByChannelProductIdResponse updateInventoryByChannelProductId(UpdateInventoryByChannelProductIdRequest request) {
        return channelInventorySyncService.updateInventoryByChannelProductId(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("updatePendingInventory")
    @POST
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.CHANNELS_VIEW)
    public UpdateInventoryOnChannelResponse updatePendingInventoryOnChannel(UpdateInventoryOnChannelRequest request) {
        return channelInventorySyncService.updateInventoryOnChannel(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("getChannelInventorySyncStatus")
    @POST
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.CHANNELS_VIEW)
    public GetChannelInventorySyncStatusResponse getChannelInventorySyncStatus(GetChannelInventorySyncStatusRequest request) {
        return channelInventorySyncService.getChannelInventorySyncStatus(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("syncChannelItemTypeInventory")
    @POST
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.CHANNELS_VIEW)
    public SyncChannelItemTypeInventoryResponse syncChannelItemTypeInventory(SyncChannelItemTypeInventoryRequest request) {
        return channelInventorySyncService.syncChannelItemTypeInventory(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("saleorder/verify")
    @POST
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.VERIFY_PENDING_ORDERS)
    public VerifySaleOrdersResponse verifySaleOrders(VerifySaleOrdersRequest request) {
        return saleOrderService.verifySaleOrders(request);

    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("recommendation/approve")
    @POST
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.RECOMMENDATION)
    public ApproveRecommendationResponse approveRecommendation(ApproveRecommendationRequest request) {
        return recommendationManagementService.approveRecommendation(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("recommendation/get")
    @POST
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.RECOMMENDATION)
    public GetRecommendationsResponse getRecommendation(GetRecommendationsRequest request) {
        return recommendationManagementService.getRecommendations(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("recommendation/reject")
    @POST
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.RECOMMENDATION)
    public RejectRecommendationResponse rejectRecommendation(RejectRecommendationRequest request) {
        return recommendationManagementService.rejectRecommendation(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("recommendations/list")
    @POST
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.RECOMMENDATION)
    public GetRecommendationsResponse listRecommendations(ListRecommendationsRequest request) {
        return recommendationManagementService.listRecommendations(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("lookup/itemTypes")
    @GET
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.MINIMAL)
    public List<ItemTypeLookupDTO> lookupItemTypes(@QueryParam("keyword") String keyword) {
        return catalogService.lookupItemTypes(keyword);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("meta/systemConfiguration")
    @GET
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.MINIMAL)
    public GenericServiceResponse getSystemConfigurationByName(@QueryParam("name") String name) {
        GenericServiceResponse response = new GenericServiceResponse();
        Map<String, String> systemConfigurations = new HashMap<String, String>();
        systemConfigurations.put(name, ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).getProperty(name));
        response.setPayload(systemConfigurations);
        response.setSuccessful(true);
        return response;
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/meta/systemConfigurations")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.MINIMAL)
    public GetSystemConfigurationByNameResponse getSystemConfigurationsByName(GetSystemConfigurationByNameRequest request) {
        GetSystemConfigurationByNameResponse response = new GetSystemConfigurationByNameResponse();
        for (String name : request.getConfigNames()) {
            response.getConfigurations().put(name, ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).getProperty(name));
        }
        response.setSuccessful(true);
        return response;
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("shippingManifest/fetchChannels")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.MANIFEST)
    public GetChannelsForManifestResponse getChannelsForManifest(GetChannelsForManifestRequest request) {
        return dispatchService.getChannelsForManifest(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("shippingManifest/fetchShippingProviders")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.MANIFEST)
    public GetEligibleShippingProvidersForManifestResponse getShippingProvidersForManifest(GetEligibleShippingProvidersForManifestRequest request) {
        return dispatchService.getShippingProvidersForManifest(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("shippingManifest/create")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.MANIFEST)
    public CreateShippingManifestResponse createShippingManifest(CreateShippingManifestRequest request) {
        User user = usersService.getUserByUsername(UserContext.current().getUniwareUserName());
        request.setUserId(user.getId());
        return dispatchService.createShippingManifest(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("shippingManifest/addShippingPackage")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.MANIFEST)
    public AddShippingPackageToManifestResponse addShippingPackageToManifest(AddShippingPackageToManifestRequest request) {
        return dispatchService.addShippingPackageToManifest(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("shippingManifest/close")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.MANIFEST)
    public CloseShippingManifestResponse closeShippingManifest(CloseShippingManifestRequest request) {
        return dispatchService.closeShippingManifest(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("shippingManifest/fetchPdf")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.MANIFEST)
    public FetchShippingManifestPdfResponse fetchShippingManifestPdf(FetchShippingManifestPdfRequest request) throws IOException {
        return shippingService.fetchShippingManifestPdf(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("shippingManifest/removeShippingPackage")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.MANIFEST)
    public RemoveManifestItemResponse removeManifestItem(RemoveManifestItemRequest request) {
        return dispatchService.removeManifestItem(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("shippingManifest/get")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.MANIFEST)
    public GetShippingManifestResponse getShippingManifest(GetShippingManifestRequest request) {
        return dispatchService.getShippingManifest(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("shippingManifest/addSignature")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.MANIFEST)
    public AddSignatureToShippingManifestResponse addSignatureToShippingManifest(AddSignatureToShippingManifestRequest request) {
        return shippingService.addSignatureToShippingManifest(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("complete/putaway/items")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.PUTAWAY_COMPLETE)
    public CompletePutawayItemsResponse completeItemPutaway(CompletePutawayItemsRequest request) {
        return putawayService.completePutawayItems(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("complete/putaway")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.PUTAWAY_COMPLETE)
    public CompletePutawayResponse completePutaway(CompletePutawayRequest request) {
        return putawayService.completePutaway(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("putaway/traceableItem/add")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.PUTAWAY_TRANSFER)
    public AddTraceableItemToTransferPutawayResponse addTraceableItemToPutaway(AddTraceableItemToTransferPutawayRequest request) {
        User user = usersService.getUserByUsername(UserContext.current().getUniwareUserName());
        request.setUserId(user.getId());
        return putawayService.addTraceableItemToTransferPutaway(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("putaway/nontraceableItem/add")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.PUTAWAY_TRANSFER)
    public AddNonTraceableItemToTransferPutawayResponse addNonTraceableItemToPutaway(AddNonTraceableItemToTransferPutawayRequest request) {
        User user = usersService.getUserByUsername(UserContext.current().getUniwareUserName());
        request.setUserId(user.getId());
        return putawayService.addNonTraceableItemToTransferPutaway(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("putaway/create")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.PUTAWAY_CREATE)
    public CreatePutawayResponse createPutaway(CreatePutawayRequest request) {
        User user = usersService.getUserByUsername(UserContext.current().getUniwareUserName());
        request.setUserId(user.getId());
        return putawayService.createPutaway(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("putaway/createPutawayList")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.PUTAWAY_CREATE)
    public CreatePutawayListResponse createPutawayList(CreatePutawayListRequest request) {
        User user = usersService.getUserByUsername(UserContext.current().getUniwareUserName());
        request.setUserId(user.getId());
        return putawayService.createPutawayList(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("putaway/get")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.PUTAWAY_CREATE)
    public GetPutawayResponse getPutaway(GetPutawayRequest request) {
        return putawayService.getPutaway(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("directReturns/search")
    @POST
    public SearchDirectReturnsResponse searchDirectReturns(SearchDirectReturnsRequest request) {
        return putawayService.searchDirectReturns(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("directReturns/add")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.PUTAWAY_CREATE)
    public AddReturnedSaleOrderItemsToPutawayResponse addItemsToPutaway(AddDirectReturnedSaleOrderItemsToPutawayRequest request) {
        User user = usersService.getUserByUsername(UserContext.current().getUniwareUserName());
        request.setUserId(user.getId());
        return putawayService.addDirectReturnsSaleOrderItemsToPutaway(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("putaway/types/get")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.PUTAWAY_CREATE)
    public GetPutawayTypesResponse getPutawayTypes(GetPutawayTypesRequest request) {
        return putawayService.getPutawayTypes(new GetPutawayTypesRequest());
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("getAllShelves")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.MINIMAL)
    public GetAllShelfCodesResponse getAllShelf(GetAllShelfCodesRequest request) {
        return shelfService.getAllShelfCodes(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("mobileUserMeta")
    @POST
    @UniwareEndPointAccess(level = Level.TENANT, accessResource = Name.MINIMAL)
    public MobileUserMetadataDTO getMobileUserMeta(ServiceRequest request) {
        MobileUserMetadataDTO metadata = new MobileUserMetadataDTO();
        UniwareUser uniwareUser = new UniwareUser(usersService.getUserByUsername(UserContext.current().getUniwareUserName()));
        Integer facilityId = uniwareUser.getCurrentFacilityId();
        if (facilityId != null) {
            Facility facility = CacheManager.getInstance().getCache(FacilityCache.class).getFacilityById(facilityId);
            com.uniware.web.metadata.UserMetadataDTO.FacilityDTO facilityDTO = new com.uniware.web.metadata.UserMetadataDTO.FacilityDTO(facility);
            metadata.setFacility(facilityDTO);
        }
        User u = uniwareUser.getUser();
        UserProfileVO userProfile = usersService.getUserProfileByUsername(u.getUsername());
        Integer lastVersionRead = userProfile.getLastReleaseUpdateVersionRead();
        boolean updatesAvailable = lastVersionRead.compareTo(releaseService.getLatestReleaseUpdateVersion().getMajorVersion()) < 0;
        UserMetadataDTO.UserDTO user = new UserMetadataDTO.UserDTO(u.getName(), u.getUsername(), u.getEmail(), u.getUserEmail(), u.getMobile(), u.isVendor(),
                uniwareUser.hasRole(Role.Code.DROPSHIPPER.name()), u.getCreated(), userProfile.isVerified(), u.isMobileVerified(), updatesAvailable, userProfile.getVisitedUrls());
        Date referrerBroadcastDate = DateUtils.stringToDate(CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class).getReferrerBroadcastDate(),
                "yyyy-MM-dd'T'HH:mm:ss");
        if (DateUtils.getCurrentDate().after(referrerBroadcastDate)) {
            user.setReferrerNotificationReadTime(userProfile.getReferrerNotificationReadTime());
            user.setReferrerNotificationRead(userProfile.isReferrerNotificationRead());
        } else {
            user.setReferrerNotificationRead(true);
        }
        RolesCache rolesCache = CacheManager.getInstance().getCache(RolesCache.class);
        Set<String> userAccessResources = new HashSet<String>();
        Set<String> userAccessPatterns = new HashSet<String>();
        for (String role : uniwareUser.getRoles()) {
            Set<String> accessResources = rolesCache.getAccessResourcesByRole(role.toUpperCase());
            if (accessResources != null) {
                userAccessResources.addAll(accessResources);
            }
            Set<String> accessPatterns = rolesCache.getAccessPatternsByRole(role.toUpperCase());
            if (accessPatterns != null) {
                userAccessPatterns.addAll(accessPatterns);
            }
        }
        user.setAccessResources(userAccessResources);
        user.setAccessPatterns(userAccessPatterns);
        metadata.setUser(user);
        return metadata;
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("purchase/gatepass/create")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.MATERIAL_MANAGEMENT)
    public CreateGatePassResponse createGatePass(CreateGatePassRequest request) {
        User user = usersService.getUserByUsername(UserContext.current().getUniwareUserName());
        request.setUserId(user.getId());
        return materialService.createGatePass(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("purchase/gatepass/summary/get")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.MATERIAL_MANAGEMENT)
    public GetGatepassSummaryResponse getGatepassSummary(GetGatepassSummaryRequest request) {
        return materialService.getGatepassSummary(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("purchase/gatepass/scanItem")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.MATERIAL_MANAGEMENT)
    public GetGatepassScannableItemDetailsResponse getGatepassScannableItemDetails(GetGatepassScannableItemDetailsRequest request) {
        return materialService.getGatepassScannableItemDetail(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("purchase/gatepass/addItem")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.MATERIAL_MANAGEMENT)
    public AddItemToGatePassResponse addItemToGatepass(AddItemToGatePassRequest request) {
        return materialService.addItemToGatePass(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("purchase/gatepass/removeItem")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.MATERIAL_MANAGEMENT)
    public RemoveItemFromGatePassResponse removeItemFromGatePass(RemoveItemFromGatePassRequest request) {
        return materialService.removeItemFromGatePass(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("purchase/gatepass/addnontraceableitem")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.MATERIAL_MANAGEMENT)
    public AddNonTraceableGatePassItemResponse addNonTraceableGatePassItem(AddNonTraceableGatePassItemRequest request) {
        return materialService.addNonTraceableGatePassItem(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("purchase/gatepass/removenontraceableitem")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.MATERIAL_MANAGEMENT)
    public RemoveNonTraceableGatePassItemResponse removeNonTraceableGatePassItem(RemoveNonTraceableGatePassItemRequest request) {
        return materialService.removeNonTraceableGatePassItem(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("purchase/gatepass/editnontraceableitem")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.MATERIAL_MANAGEMENT)
    public AddOrEditNonTraceableGatePassItemResponse addOrEditNonTraceableGatePassItem(AddOrEditNonTraceableGatePassItemRequest request) {
        return materialService.addOrEditNonTraceableGatePassItem(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("purchase/gatepass/complete")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.MATERIAL_MANAGEMENT)
    public CompleteGatePassResponse completeGatepass(CompleteGatePassRequest request) {
        User user = usersService.getUserByUsername(UserContext.current().getUniwareUserName());
        request.setUserId(user.getId());
        return materialService.completeGatePass(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("purchase/gatepass/discard")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.MATERIAL_MANAGEMENT)
    public DiscardGatePassResponse discardGatePass(DiscardGatePassRequest request) {
        User user = usersService.getUserByUsername(UserContext.current().getUniwareUserName());
        request.setUserId(user.getId());
        return materialService.discardGatePass(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("meta/facilities")
    @GET
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.MINIMAL)
    public String getFacilities() {
        return CacheManager.getInstance().getCache(FacilityCache.class).getFacilitiesJson();
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("data/lookup/party")
    @GET
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.MATERIAL_MANAGEMENT)
    public List<PartySearchDTO> lookupParty(@QueryParam("keyword") String keyword) {
        return partyService.lookupParty(keyword);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("product/item/get")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.LOOKUP_SALE_ORDER)
    public GetItemDetailResponse getItemDetail(GetItemDetailRequest request) {
        return itemService.getItemDetail(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/picklist/assign")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.PICKER)
    public AssignPicklistToUserResponse assignPicklistToUser(AssignPicklistToUserRequest request) {
        if (StringUtils.isBlank(request.getUsername())) {
            User user = usersService.getUserByUsername(UserContext.current().getUniwareUserName());
            request.setUsername(user.getUsername());
        }
        return pickerService.assignPicklistToUser(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/picklist/detail/get")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.PICKLIST_VIEW)
    public GetPicklistDetailResponse getPicklistDetail(GetPicklistDetailRequest request) {
        return pickerService.getPicklistDetail(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/cyclecount/active/get")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.CYCLE_COUNT_VIEW)
    public GetCycleCountResponse getCycleCount(GetActiveCycleCountRequest request) {
        return cycleCountService.getActiveCycleCount(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/cyclecount/shelf/block")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.COUNT_SHELF)
    public BlockShelvesForCycleCountResponse blockShelfForCycleCount(BlockShelvesForCycleCountRequest request) {
        return cycleCountService.blockShelvesForCycleCountRequest(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/cyclecount/shelf/unblock")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.COUNT_SHELF)
    public UnblockShelvesResponse unBlockShelfForCycleCount(UnblockShelvesRequest request) {
        return cycleCountService.unblockShelves(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/cyclecount/shelf/start")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.COUNT_SHELF)
    public StartCountingForShelfResponse startCountingForShelf(StartCountingForShelfRequest request) {
        return cycleCountService.startCountingForShelf(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/cyclecount/shelf/submit")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.COUNT_SHELF)
    public CompleteCountForShelfResponse completeCountForShelf(CompleteCountForShelfRequest request) {
        return cycleCountService.completeCountForShelf(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/cyclecount/shelf/recount")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.COUNT_SHELF)
    public RecountShelfResponse recountShelf(RecountShelfRequest request) {
        return cycleCountService.recountShelf(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/cyclecount/shelf/search")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.CYCLE_COUNT_VIEW)
    public SearchCycleCountShelfResponse searchCycleCountShelf(SearchCycleCountShelfRequest request) {
        return cycleCountService.searchCycleCountShelf(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/cyclecount/subcyclecount/get")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.CYCLE_COUNT_VIEW)
    public GetSubCycleCountShelvesResponse getSubCycleCount(GetSubCycleCountShelvesRequest request) {
        return cycleCountService.getSubCycleCount(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/cyclecount/erroritems/get")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.CYCLE_COUNT_VIEW)
    public GetErrorItemsForCycleCountResponse getErrorItemsForCycleCount(GetBadItemsForCycleCountRequest request) {
        return cycleCountService.getErrorItemsForCycleCount(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/cyclecount/shelf/erroritems/get")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.CYCLE_COUNT_VIEW)
    public GetBadItemsForShelfResponse getErrorItemsForCycleCount(GetBadItemsForShelfRequest request) {
        return cycleCountService.getErrorItemsForShelf(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/zones/get")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.MINIMAL)
    public GetAllPicksetsResponse getZonesForCycleCount(GetZonesForCycleCountRequest request) {
        GetAllPicksetsResponse response = new GetAllPicksetsResponse();
        picksetService.getAllPickSets().forEach(response::addZone);
        response.setSuccessful(true);
        return response;
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/pickBucket/detail/get")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.PICKLIST_VIEW)
    public GetPickBucketDetailResponse getPickBucketDetail(GetPickBucketDetailRequest request) {
        return shipmentStagingService.getPickBucketDetail(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/pickBucket/receive")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.PICKLIST_RECEIVE)
    public ReceivePickBucketResponse receivePickBucket(ReceivePickBucketRequest request) {
        return shipmentStagingService.receivePickBucketAtStaging(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/picklist/receive")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.PICKLIST_RECEIVE)
    public ReceivePicklistResponse receivePicklist(ReceivePicklistRequest request) {
        return packerService.receivePicklist(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/picklist/putback/item/accept")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.PICKLIST_RECEIVE)
    public AcceptPutbackItemResponse acceptPutbackItem(AcceptPutbackItemRequest request) {
        return packerService.acceptPutbackItem(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/pickBatch/create")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.PICKER)
    public CreatePickBatchResponse createPickBatch(CreatePickBatchRequest request) {
        User user = usersService.getUserByUsername(UserContext.current().getUniwareUserName());
        request.setUserId(user.getId());
        return pickerService.createPickBatch(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/pickBucket/submit")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.PICKER)
    public SubmitPickBucketResponse submitPickBucket(SubmitPickBucketRequest request) {
        User user = usersService.getUserByUsername(UserContext.current().getUniwareUserName());
        request.setUserId(user.getId());
        return pickerService.submitPickBucket(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/pickBucket/item/receive")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.PICKLIST_RECEIVE)
    public ReceivePickBucketItemResponse receivePickBucketItem(ReceivePickBucketItemRequest request) {
        return shipmentStagingService.receivePickBucketItem(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/picklist/item/receive")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.PICKLIST_RECEIVE)
    public ScanItemAtStagingResponse receivePicklistItem(ScanItemAtStagingRequest request) {
        return shipmentStagingService.scanItemAtStaging(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/picklist/nontraceable/item/receive")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.PICKLIST_RECEIVE)
    public ReceiveNonTraceablePicklistItemResponse receiveNonTraceablePicklistItem(ReceiveNonTraceablePicklistItemRequest request) {
        return shipmentStagingService.receiveNonTraceablePicklistItem(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/picking/complete")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.PICKER)
    public CompletePickingResponse completePicking(CompletePickingRequest request) {
        User user = usersService.getUserByUsername(UserContext.current().getUniwareUserName());
        request.setUserId(user.getId());
        return pickerService.completePicking(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/picklist/complete")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.PICKLIST_RECEIVE)
    public CompletePicklistResponse completePicklist(CompletePicklistRequest request) {
        return packerService.completePicklist(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/pickBatch/complete")
    @POST
    @UniwareEndPointAccess(level = Level.FACILITY, accessResource = Name.PICKLIST_RECEIVE)
    public CompletePickBatchResponse completePickBatch(CompletePickBatchRequest request) {
        return shipmentStagingService.completePickBatch(request);
    }

}
