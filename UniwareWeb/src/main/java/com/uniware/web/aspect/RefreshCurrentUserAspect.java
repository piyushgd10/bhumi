/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, May 20, 2015
 *  @author akshay
 */
package com.uniware.web.aspect;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import com.unifier.core.entity.User;
import com.unifier.services.users.IUsersService;
import com.unifier.web.security.UniwareUser;
import com.unifier.web.utils.WebContextUtils;
import com.uniware.core.vo.UserProfileVO;
import com.uniware.dao.user.notification.IUserProfileMao;

@Order(1000)
@Aspect
@Component
public class RefreshCurrentUserAspect {

    @Autowired
    private IUsersService userService;
    
    @Autowired
    private IUserProfileMao userProfileMao;
    
    @After("@annotation(com.uniware.web.aspect.RefreshCurrentUser)")
    public void trigger() {
        UniwareUser uniwareUser = WebContextUtils.getCurrentUser();
        User user = userService.getUserByUsername(WebContextUtils.getCurrentUser().getUsername());
        uniwareUser.setUser(user);
    }
}
