/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 20, 2015
 *  @author harsh
 */
package com.uniware.web.aspect;

import com.unifier.core.cache.CacheManager;
import com.uniware.core.cache.EnvironmentPropertiesCache;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import com.unifier.core.api.base.ServiceResponse;
import com.uniware.services.apiStatistics.IApiAccessStatisticsService;

/**
 * @author harsh
 */
@Order(1000)
@Aspect
@Component
public class ApiAccessStatisticsAspect {

    private static final Logger         LOG = LoggerFactory.getLogger(ApiAccessStatisticsAspect.class);

    @Autowired
    private IApiAccessStatisticsService apiUserAccessStatisticsService;

    @Around("execution(* *(..)) && @annotation(com.uniware.web.security.annotation.UniwareEndPointAccess)")
    public Object logApiAccess(ProceedingJoinPoint pjp) throws Throwable {
        long s = System.currentTimeMillis();
        Object response = pjp.proceed();
        MethodSignature ms = (MethodSignature) pjp.getSignature();
        String apiName = ms.getParameterTypes()[0].getSimpleName();
        if (!CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class).isApiStatisticsDisabled()) {
            long diffMs = (System.currentTimeMillis() - s);
            if (response != null && response instanceof ServiceResponse) {
                apiUserAccessStatisticsService.saveApiAccessStatistics(apiName, diffMs, ((ServiceResponse) response).isSuccessful());
            }
        }
        long diffMs = (System.currentTimeMillis() - s);
        LOG.info("API : {} Time taken : {} ms", apiName, diffMs);
        return response;
    }
}
