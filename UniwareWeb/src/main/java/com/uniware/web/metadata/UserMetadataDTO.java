/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 15, 2011
 *  @author singla
 */
package com.uniware.web.metadata;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.unifier.web.configuration.LayoutConfiguration;
import com.uniware.core.entity.Facility;
import com.uniware.core.entity.PartyAddressType;
import com.uniware.core.entity.Tenant;
import com.uniware.core.entity.Tenant.Mode;

/**
 * @author Sunny
 */
public class UserMetadataDTO {

    private TenantDTO           tenant;
    private FacilityDTO         facility;
    private UserDTO             user;
    private UserDTO             primaryUser;
    private LayoutConfiguration layoutConfiguration;
    private List<FeatureDTO>    features;
    private String              setupStatus;
    private boolean             invoiceConfigured;
    private String              unideskUrl;
    private String              serverName;

    public FacilityDTO getFacility() {
        return this.facility;
    }

    public void setFacility(FacilityDTO facilityDTO) {
        this.facility = facilityDTO;
    }

    public UserDTO getUser() {
        return user;
    }

    public void setUser(UserDTO user) {
        this.user = user;
    }

    public UserDTO getPrimaryUser() {
        return primaryUser;
    }

    public void setPrimaryUser(UserDTO primaryUser) {
        this.primaryUser = primaryUser;
    }

    public TenantDTO getTenant() {
        return tenant;
    }

    public void setTenant(TenantDTO tenant) {
        this.tenant = tenant;
    }

    public LayoutConfiguration getLayoutConfiguration() {
        return layoutConfiguration;
    }

    public void setLayoutConfiguration(LayoutConfiguration layoutConfiguration) {
        this.layoutConfiguration = layoutConfiguration;
    }

    public List<FeatureDTO> getFeatures() {
        return features;
    }

    public void setFeatures(List<FeatureDTO> applicableFeatures) {
        this.features = applicableFeatures;
    }

    public String getSetupStatus() {
        return setupStatus;
    }

    public void setSetupStatus(String setupStatus) {
        this.setupStatus = setupStatus;
    }

    public boolean isInvoiceConfigured() {
        return invoiceConfigured;
    }

    public void setInvoiceConfigured(boolean invoiceConfigured) {
        this.invoiceConfigured = invoiceConfigured;
    }

    public String getUnideskUrl() {
        return unideskUrl;
    }

    public void setUnideskUrl(String unideskUrl) {
        this.unideskUrl = unideskUrl;
    }

    public String getServerName() {
        return serverName;
    }

    public void setServerName(String serverName) {
        this.serverName = serverName;
    }

    public static class TenantDTO {
        private String  code;
        private String  name;
        private String  tenantCode;
        private String  accountCode;
        private boolean active;
        private String  baseCurrency;
        private Mode    mode;
        private String  logoUrl;
        private String  productCode;
        private String  paymentStatus;
        private Date    paymentStatusAcknowledgeDate;
        private Date    created;

        public TenantDTO() {
        }

        public TenantDTO(Tenant tenant) {
            code = tenant.getCode();
            name = tenant.getName();
            accountCode = tenant.getAccountCode();
            tenantCode = tenant.getCode();
            active = Tenant.StatusCode.ACTIVE.equals(tenant.getStatusCode());
            mode = tenant.getMode();
            logoUrl = tenant.getLogoUrl();
            productCode = tenant.getProduct().getCode();
            setCreated(tenant.getCreated());
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getBaseCurrency() {
            return baseCurrency;
        }

        public void setBaseCurrency(String baseCurrency) {
            this.baseCurrency = baseCurrency;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getTenantCode() {
            return tenantCode;
        }

        public void setTenantCode(String tenantCode) {
            this.tenantCode = tenantCode;
        }

        public String getAccountCode() {
            return accountCode;
        }

        public void setAccountCode(String accountCode) {
            this.accountCode = accountCode;
        }

        public boolean isActive() {
            return active;
        }

        public void setActive(boolean active) {
            this.active = active;
        }

        public Mode getMode() {
            return mode;
        }

        public void setMode(Mode mode) {
            this.mode = mode;
        }

        public String getLogoUrl() {
            return logoUrl;
        }

        public void setLogoUrl(String logoUrl) {
            this.logoUrl = logoUrl;
        }

        public String getProductCode() {
            return productCode;
        }

        public void setProductCode(String productCode) {
            this.productCode = productCode;
        }

        public Date getCreated() {
            return created;
        }

        public void setCreated(Date created) {
            this.created = created;
        }

        public String getPaymentStatus() {
            return paymentStatus;
        }

        public void setPaymentStatus(String paymentStatus) {
            this.paymentStatus = paymentStatus;
        }

        public Date getPaymentStatusAcknowledgeDate() {
            return paymentStatusAcknowledgeDate;
        }

        public void setPaymentStatusAcknowledgeDate(Date paymentStatusAcknowledgeDate) {
            this.paymentStatusAcknowledgeDate = paymentStatusAcknowledgeDate;
        }
    }

    public static class FacilityDTO {

        private String code;
        private String displayName;
        private String stateCode;

        public FacilityDTO() {
            super();
        }

        public FacilityDTO(Facility facility) {
            if (facility != null) {
                this.code = facility.getCode();
                this.displayName = facility.getDisplayName();
                this.stateCode = facility.getPartyAddressByType(PartyAddressType.Code.BILLING.name()).getStateCode();
            }
        }

        public String getCode() {
            return this.code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getDisplayName() {
            return displayName;
        }

        public void setDisplayName(String displayName) {
            this.displayName = displayName;
        }

        public String getStateCode() {
            return stateCode;
        }

        public void setStateCode(String stateCode) {
            this.stateCode = stateCode;
        }
    }

    public static class UserDTO {
        private String      name;
        private String      userName;
        private String      email;
        private String      userEmail;
        private String      mobile;
        private boolean     vendorAssociated;
        private boolean     dropShipper;
        private boolean     verified;
        private boolean     mobileVerified;
        private boolean     updatesAvailable;
        private boolean     referrerNotificationRead;
        private Set<String> accessResources;
        private Set<String> accessPatterns;
        private Date        created;
        private Date        referrerNotificationReadTime;
        private Set<String> visitedUrls = new HashSet<>();

        public UserDTO() {
            super();
        }

        public UserDTO(String name, String userName, String email, String userEmail, String mobile, boolean vendor, boolean dropShipper, Date created, boolean verified,
                boolean mobileVerified, boolean updatesAvailable, Set<String> visitedUrls) {
            super();
            this.userName = userName;
            this.name = name;
            this.email = email;
            this.userEmail = userEmail;
            this.mobile = mobile;
            this.vendorAssociated = vendor;
            this.dropShipper = dropShipper;
            setCreated(created);
            this.verified = verified;
            this.mobileVerified = mobileVerified;
            this.updatesAvailable = updatesAvailable;
            this.visitedUrls = visitedUrls;
        }

        public UserDTO(String name, String userName, String email, String userEmail, String mobile, boolean vendor, boolean dropShipper, Date created, boolean verified,
                boolean mobileVerified, boolean updatesAvailable, boolean referrerNotificationRead, Date referrerNotificationReadTime, Set<String> visitedUrls) {
            super();
            this.userName = userName;
            this.name = name;
            this.email = email;
            this.userEmail = userEmail;
            this.mobile = mobile;
            this.vendorAssociated = vendor;
            this.dropShipper = dropShipper;
            setCreated(created);
            this.verified = verified;
            this.mobileVerified = mobileVerified;
            this.updatesAvailable = updatesAvailable;
            this.visitedUrls = visitedUrls;
            this.referrerNotificationRead = referrerNotificationRead;
            this.referrerNotificationReadTime = referrerNotificationReadTime;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getUserEmail() {
            return userEmail;
        }

        public void setUserEmail(String userEmail) {
            this.userEmail = userEmail;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public Set<String> getAccessResources() {
            return accessResources;
        }

        public void setAccessResources(Set<String> accessResources) {
            this.accessResources = accessResources;
        }

        public Set<String> getAccessPatterns() {
            return accessPatterns;
        }

        public void setAccessPatterns(Set<String> accessPatterns) {
            this.accessPatterns = accessPatterns;
        }

        public boolean isVendorAssociated() {
            return vendorAssociated;
        }

        public void setVendorAssociated(boolean vendorAssociated) {
            this.vendorAssociated = vendorAssociated;
        }

        public boolean isDropShipper() {
            return dropShipper;
        }

        public void setDropShipper(boolean dropShipper) {
            this.dropShipper = dropShipper;
        }

        public Date getCreated() {
            return created;
        }

        public void setCreated(Date created) {
            this.created = created;
        }

        public boolean isVerified() {
            return verified;
        }

        public void setVerified(boolean verified) {
            this.verified = verified;
        }

        public boolean isMobileVerified() {
            return mobileVerified;
        }

        public void setMobileVerified(boolean mobileVerified) {
            this.mobileVerified = mobileVerified;
        }

        public boolean isUpdatesAvailable() {
            return updatesAvailable;
        }

        public void setUpdatesAvailable(boolean updatesAvailable) {
            this.updatesAvailable = updatesAvailable;
        }

        public boolean isReferrerNotificationRead() {
            return referrerNotificationRead;
        }

        public void setReferrerNotificationRead(boolean referrerNotificationRead) {
            this.referrerNotificationRead = referrerNotificationRead;
        }

        public Date getReferrerNotificationReadTime() {
            return referrerNotificationReadTime;
        }

        public void setReferrerNotificationReadTime(Date referrerNotificationReadTime) {
            this.referrerNotificationReadTime = referrerNotificationReadTime;
        }

        public Set<String> getVisitedUrls() {
            return visitedUrls;
        }

        public void setVisitedUrls(Set<String> visitedUrls) {
            this.visitedUrls = visitedUrls;
        }

    }

    public static class FeatureDTO {

        public FeatureDTO() {
        }

        public FeatureDTO(String code, String name) {
            super();
            this.code = code;
            this.name = name;
        }

        private String code;
        private String name;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

    }
}
