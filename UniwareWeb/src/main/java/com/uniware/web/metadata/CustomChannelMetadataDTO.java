package com.uniware.web.metadata;

import com.uniware.core.api.channel.ChannelDetailDTO;

/**
 * Created by karunsingla on 31/10/14.
 */
public class CustomChannelMetadataDTO {
    private String code;
    private String name;
    private String packageType;

    public CustomChannelMetadataDTO() {
    }

    public CustomChannelMetadataDTO(ChannelDetailDTO channelDetailDTO) {
        this.setCode(channelDetailDTO.getCode());
        this.setName(channelDetailDTO.getName());
        this.setPackageType(channelDetailDTO.getPackageType());
    }

    public String getPackageType() {
        return packageType;
    }

    public void setPackageType(String packageType) {
        this.packageType = packageType;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
