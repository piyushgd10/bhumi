/*
 * Copyright 2017 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 18/04/17
 * @author piyush
 */
package com.uniware.web.metadata;

public class MobileUserMetadataDTO {

    public UserMetadataDTO.FacilityDTO facility;

    public UserMetadataDTO.UserDTO user;

    public UserMetadataDTO.FacilityDTO getFacility() {
        return facility;
    }

    public void setFacility(UserMetadataDTO.FacilityDTO facility) {
        this.facility = facility;
    }

    public UserMetadataDTO.UserDTO getUser() {
        return user;
    }

    public void setUser(UserMetadataDTO.UserDTO user) {
        this.user = user;
    }
}
