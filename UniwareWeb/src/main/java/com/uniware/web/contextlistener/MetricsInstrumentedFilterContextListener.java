/*
*  @version     1.0, 03/10/16
*  @author sunny
*/

package com.uniware.web.contextlistener;

import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.SharedMetricRegistries;
import com.codahale.metrics.servlet.InstrumentedFilterContextListener;

public class MetricsInstrumentedFilterContextListener extends InstrumentedFilterContextListener {

    private static final String        SYSTEM_METRICS = "system-metrics";
    public static final MetricRegistry REGISTRY       = SharedMetricRegistries.getOrCreate(SYSTEM_METRICS);

    @Override
    protected MetricRegistry getMetricRegistry() {
        return REGISTRY;
    }
}
