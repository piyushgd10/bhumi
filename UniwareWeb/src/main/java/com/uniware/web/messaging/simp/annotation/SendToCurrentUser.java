/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 20-Jan-2015
 *  @author sunny
 */
package com.uniware.web.messaging.simp.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface SendToCurrentUser {

	/**
	 * One or more destinations to send a message to. If left unspecified, a
	 * default destination is selected based on the destination of the input
	 * message being handled.
	 * @see org.springframework.messaging.simp.annotation.support.SendToMethodReturnValueHandler
	 */
	String[] value() default {""};

	/**
	 * Whether messages should be sent to all sessions associated with the user
	 * or only to the session of the input message being handled.
	 *
	 * <p>By default this is set to {@code false} in which case messages are
	 * broadcast to a single session.
     */
    boolean broadcast() default false;

}
