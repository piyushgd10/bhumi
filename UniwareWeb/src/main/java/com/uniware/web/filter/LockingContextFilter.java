package com.uniware.web.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import com.uniware.services.locking.LockingContext;

/**
 * Creates and destroys a locking context to be used by annotation based locking framework
 * 
 * @author sunny
 */
public class LockingContextFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        LockingContext lockingContext = LockingContext.current();
        try {
            filterChain.doFilter(servletRequest, servletResponse);
        } finally {
            lockingContext.clear();
        }
    }

    @Override
    public void destroy() {
    }

}
