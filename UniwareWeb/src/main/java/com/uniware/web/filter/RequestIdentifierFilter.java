/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Sep 27, 2012
 *  @author singla
 */
package com.uniware.web.filter;

import java.io.IOException;
import java.util.Set;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.ThreadContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.filter.OncePerRequestFilter;

import com.unifier.core.cache.CacheManager;
import com.unifier.core.utils.StringUtils;
import com.uniware.core.cache.TenantCache;
import com.uniware.core.entity.Tenant;
import com.uniware.core.utils.Constants;
import com.uniware.core.utils.UserContext;
import com.uniware.core.utils.ViewContext;

public class RequestIdentifierFilter extends OncePerRequestFilter {

    private static final Logger LOG = LoggerFactory.getLogger(RequestIdentifierFilter.class);

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        if (!(request instanceof HttpServletRequest)) {
            throw new ServletException("This filter can only process HttpServletRequest requests");
        }

        //CORS "pre-flight" request // Added by Narendra Sisodiya
        if (request.getHeader("Access-Control-Request-Method") != null && "OPTIONS".equals(request.getMethod())) {
            response.addHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
            response.addHeader("Access-Control-Allow-Headers", "X-Requested-With, Origin, X-Csrftoken, Content-Type, Accept");
        }
        String serverName = request.getServerName().toLowerCase();
        if (serverName.endsWith(".unicommerce.com") || serverName.endsWith(".unicommerce.info")) {
            TenantCache tenantCache = CacheManager.getInstance().getCache(TenantCache.class);
            Tenant tenant = tenantCache.getTenantByAccessUrl(request.getServerName());
            if (tenant == null) {
                response.sendError(HttpServletResponse.SC_NOT_FOUND);
            } else {
                request.setAttribute("tenant", tenant.getCode());
                Set<String> accessIPWhitelist = tenantCache.getAccessIPWhitelist(tenant.getCode());
                if (accessIPWhitelist != null && !accessIPWhitelist.contains(request.getRemoteAddr()) && !request.getRemoteAddr().startsWith("10.")) {
                    LOG.info("Blocking IP " + request.getRemoteAddr() + " from access to " + request.getServerName());
                    response.sendError(HttpServletResponse.SC_FORBIDDEN);
                } else {
                    try {
                        UserContext userContext = UserContext.current();
                        userContext.setTenant(tenant);
                        userContext.setThreadInfo(new StringBuilder(userContext.getTenant().getCode()).append(':').append(request.getRequestURI()).toString());
                        Thread.currentThread().setName(
                                new StringBuilder("HTTP-").append(Thread.currentThread().getId()).append(':').append(userContext.getThreadInfo()).toString());
                        String traceLogKey = request.getParameter("tlk");

                        if (StringUtils.isNotEmpty(traceLogKey)) {
                            // kekda is ON
                            userContext.setTraceLogEnabled(true);
                            userContext.setLogRoutingKey(traceLogKey);
                            ThreadContext.put(Constants.TRACE_LOG_KEY_VALUE, traceLogKey);
                            ThreadContext.put(Constants.TRACE_LOG_KEY, Constants.TRACE_LOG_KEY);
                        } else {
                            userContext.setLogRoutingKey(tenant.getCode());
                            ThreadContext.put(Constants.TRACE_LOG_KEY, tenant.getCode());
                        }
                        filterChain.doFilter(request, response);
                        request.setAttribute("thread-info", UserContext.current().getThreadInfo());
                    } finally {
                        UserContext.destroy();
                        ViewContext.destroy();
                        ThreadContext.clear();
                    }
                }
            }
        } else {
            filterChain.doFilter(request, response);
        }
    }
}
