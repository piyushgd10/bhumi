/*
 *  Copyright 2013 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 01-May-2013
 *  @author Sunny Agarwal
 */
package com.uniware.web.interceptor;

import java.util.Set;

import com.uniware.core.cache.FacilityCache;
import com.uniware.core.entity.Facility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.SimpMessageType;
import org.springframework.messaging.support.ChannelInterceptorAdapter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContextImpl;

import com.unifier.core.api.base.ServiceResponse;
import com.unifier.core.api.validation.WsError;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.entity.AccessResource;
import com.unifier.web.security.UniwareUser;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.cache.RolesCache;
import com.uniware.core.cache.TenantCache;
import com.uniware.core.entity.Tenant;
import com.uniware.core.utils.UserContext;
import com.uniware.core.utils.ViewContext;
import com.uniware.web.publisher.websocket.UniwareSimpMessagingTemplate;

public class UniwareInboundWebSocketInterceptor extends ChannelInterceptorAdapter {

    private static final Logger          LOG = LoggerFactory.getLogger(UniwareInboundWebSocketInterceptor.class);

    @Autowired
    private UniwareSimpMessagingTemplate template;

    @Override
    public Message<?> preSend(Message<?> message, MessageChannel channel) {
        SimpMessageHeaderAccessor headers = SimpMessageHeaderAccessor.wrap(message);
        UniwareUser uniwareUser = null;
        Authentication authentication = (Authentication) headers.getUser();
        if (authentication != null) {
            uniwareUser = (UniwareUser) authentication.getPrincipal();
            SecurityContext securityContext = new SecurityContextImpl();
            securityContext.setAuthentication(authentication);
            SecurityContextHolder.setContext(securityContext);
        }

        if (uniwareUser != null) {
            UserContext userContext = UserContext.current();
            String username = uniwareUser.getUsername();
            Tenant tenant = CacheManager.getInstance().getCache(TenantCache.class).getActiveTenantByCode(uniwareUser.getUser().getTenant().getCode());
            String tenantCode = tenant.getCode();
            userContext.setTenant(tenant);
            Facility facility = CacheManager.getInstance().getCache(FacilityCache.class).getFacilityById(uniwareUser.getCurrentFacilityId());
            userContext.setFacility(facility);

            // Log user and tenant in server logs
            userContext.setUniwareUserName(username);
            userContext.setUserId(uniwareUser.getUser().getId());
            userContext.setThreadInfo(new StringBuilder(tenantCode).append(':').append(username).append(':').append(headers.getDestination()).toString());
            Thread.currentThread().setName(new StringBuilder("HTTP-").append(Thread.currentThread().getId()).append(':').append(userContext.getThreadInfo()).toString());
            if (!SimpMessageType.MESSAGE.equals(headers.getMessageType())) {
                return message;
            }
            userContext.setRequestId(headers.getNativeHeader("callback_id").get(0));
            String destination = headers.getDestination().substring(headers.getDestination().indexOf("/app") + 4);
            RolesCache rolesCache = CacheManager.getInstance().getCache(RolesCache.class);
            Set<String> accessibleByRoles = rolesCache.getAccessibleByRoles(destination);
            if (accessibleByRoles != null && uniwareUser.hasAnyRole(accessibleByRoles)) {
                return message;
            } else if (accessibleByRoles == null) {
                ServiceResponse serviceResponse = new ServiceResponse();
                serviceResponse.addError(new WsError(WsResponseCode.INVALID_URI.code(), WsResponseCode.INVALID_URI.message()));
                template.convertAndSendToCurrentUser(serviceResponse);
            } else {
                ServiceResponse serviceResponse = new ServiceResponse();
                AccessResource accessResource = rolesCache.getAccessResourceByRequestURI(destination);
                serviceResponse.addError(new WsError(WsResponseCode.ACCESS_DENIED.code(), WsResponseCode.ACCESS_DENIED.message()
                        + (accessResource != null ? ", AccessResource needed: " + accessResource.getName() : "")));
                template.convertAndSendToCurrentUser(serviceResponse);
            }
        }
        return message;
    }

    @Override
    public void postSend(Message<?> message, MessageChannel channel, boolean sent) {
        super.postSend(message, channel, sent);
        SecurityContextHolder.clearContext();
        ViewContext.destroy();
    }
}
