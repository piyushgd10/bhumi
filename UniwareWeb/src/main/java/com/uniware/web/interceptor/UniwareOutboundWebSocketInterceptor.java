/*
 *  Copyright 2013 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 01-May-2013
 *  @author Sunny Agarwal
 */
package com.uniware.web.interceptor;

import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.ChannelInterceptorAdapter;

public class UniwareOutboundWebSocketInterceptor extends ChannelInterceptorAdapter {

    @Override
    public Message<?> preSend(Message<?> message, MessageChannel channel) {
        //        SimpMessageHeaderAccessor headers = SimpMessageHeaderAccessor.wrap(message);
        //        headers.addNativeHeader("callback_id", UserContext.current().getRequestId());
        //        return MessageBuilder.withPayload(message.getPayload()).setHeaders(headers).build();
        return message;
    }

}
