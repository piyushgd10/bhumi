/*
 *  Copyright 2013 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 01-May-2013
 *  @author Sunny Agarwal
 */
package com.uniware.web.interceptor;

import java.lang.reflect.Method;
import java.util.Collections;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

import javax.xml.transform.dom.DOMSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.oxm.UnmarshallingFailureException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.ws.context.MessageContext;
import org.springframework.ws.server.endpoint.MethodEndpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.soap.SoapBody;
import org.springframework.ws.soap.SoapFault;
import org.springframework.ws.soap.SoapHeaderElement;
import org.springframework.ws.soap.SoapMessage;
import org.springframework.ws.soap.server.SoapEndpointInterceptor;
import org.springframework.ws.soap.server.endpoint.SimpleSoapExceptionResolver;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.unifier.core.annotation.Level;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.utils.StringUtils;
import com.unifier.services.users.IUsersService;
import com.uniware.core.cache.FacilityCache;
import com.uniware.core.cache.RolesCache;
import com.uniware.core.entity.Facility;
import com.uniware.core.entity.Tenant;
import com.uniware.core.utils.UserContext;
import com.uniware.services.apiStatistics.IApiAccessStatisticsService;
import com.uniware.web.security.annotation.UniwareEndPointAccess;

@Component("uniwareAccessControlInterceptor")
public class UniwareAccessControlInterceptor extends SimpleSoapExceptionResolver implements SoapEndpointInterceptor {

    private static final Logger LOG = LoggerFactory.getLogger(UniwareAccessControlInterceptor.class);
    @Autowired
    private IApiAccessStatisticsService apiUserAccessStatisticsService;

    @Autowired
    private IUsersService               usersService;

    private long                startTime;

    @Override
    public boolean handleRequest(MessageContext messageContext, Object endpoint) throws Exception {
        startTime = System.currentTimeMillis();
        messageContext.getRequest().getPayloadSource();
        Method method = ((MethodEndpoint) endpoint).getMethod();
        String threadName = Thread.currentThread().getName() + method.getAnnotation(PayloadRoot.class).localPart();
        Thread.currentThread().setName(threadName);
        UserContext.current().setThreadInfo(threadName);
        UniwareEndPointAccess uniwareEndpointAccess = ((MethodEndpoint) endpoint).getMethod().getAnnotation(UniwareEndPointAccess.class);
        if (uniwareEndpointAccess != null) {
            if (!uniwareEndpointAccess.openForInactiveTenants()) {
                if (UserContext.current().getTenant().getStatusCode().equals(Tenant.StatusCode.INACTIVE)) {
                    recordFault(endpoint, messageContext, "Illegal Access, tenant is inactive", false);
                    return false;
                }
            }
            if (!hasResource(uniwareEndpointAccess.accessResource().name())) {
                recordFault(endpoint, messageContext, "Access denied", false);
                return false;
            } else if (Level.FACILITY == uniwareEndpointAccess.level() && UserContext.current().getFacilityId() == null) {
                NodeList nodeList = ((DOMSource) messageContext.getRequest().getPayloadSource()).getNode().getChildNodes();
                int i = 0;
                while (i < nodeList.getLength()) {
                    Node node = nodeList.item(i);
                    if ("FacilityContext".equals(node.getLocalName())) {
                        String facilityCode = node.getFirstChild().getNodeValue();
                        if (StringUtils.isNotBlank(facilityCode)) {
                            Facility facility = CacheManager.getInstance().getCache(FacilityCache.class).getFacilityByCode(facilityCode.trim());
                            UserContext.current().setFacility(facility);
                        }
                    }
                    i++;
                }
                if (UserContext.current().getFacilityId() == null) {
                    recordFault(endpoint, messageContext, "Illegal Access, facility is required", false);
                    return false;
                }
            }
        }
        return true;
    }

    public Set<String> getUserRoles() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null) {
            Set<String> roles = new HashSet<String>(authentication.getAuthorities().size());
            for (GrantedAuthority auth : authentication.getAuthorities()) {
                roles.add(auth.getAuthority());
            }
            return roles;
        }
        return Collections.emptySet();
    }

    public boolean hasResource(String accessResource) {
        Set<String> userRoles = getUserRoles();
        Set<String> roles = CacheManager.getInstance().getCache(RolesCache.class).getRolesByAccessResource(accessResource);
        if (roles != null) {
            for (String role : roles) {
                if (userRoles.contains(role)) {
                    return true;
                }
            }
        }
        return false;
    }

    private void recordFault(Object endpoint, MessageContext messageContext, String message, boolean isException) {
        addFaultToResponse(messageContext, message, isException);
        PayloadRoot payloadRoot = ((MethodEndpoint) endpoint).getMethod().getAnnotation(PayloadRoot.class);
        long diffMs = System.currentTimeMillis() - startTime;
        if (payloadRoot != null) {
            apiUserAccessStatisticsService.saveApiAccessStatistics(payloadRoot.localPart(), diffMs, false);
        }
        diffMs = System.currentTimeMillis() - startTime;
        LOG.info("API : {} Time taken : {} ms", payloadRoot.localPart(), diffMs);
    }

    private void addFaultToResponse(MessageContext messageContext, String message, boolean isException) {
        SoapBody soapBody = ((SoapMessage) messageContext.getResponse()).getSoapBody();
        if (isException) {
            soapBody.addServerOrReceiverFault(message, Locale.ENGLISH);
        } else {
            soapBody.addClientOrSenderFault(message, Locale.ENGLISH);
        }
    }

    @Override
    public boolean handleResponse(MessageContext messageContext, Object endpoint) throws Exception {
        return false;
    }

    @Override
    public boolean handleFault(MessageContext messageContext, Object endpoint) throws Exception {
        return false;
    }

    @Override
    public void afterCompletion(MessageContext messageContext, Object endpoint, Exception ex) throws Exception {

    }

    @Override
    public boolean understands(SoapHeaderElement header) {
        return false;
    }

    @Override
    protected void customizeFault(MessageContext messageContext, Object endpoint, Exception ex, SoapFault fault) {
        if (ex instanceof UnmarshallingFailureException) {
            addFaultToResponse(messageContext, ex.getMessage(), true);
        } else {
            LOG.error("Error with endpoint Stack trace: ", ex);
            recordFault(endpoint, messageContext, "An internal error occurred", true);
        }
    }
}
