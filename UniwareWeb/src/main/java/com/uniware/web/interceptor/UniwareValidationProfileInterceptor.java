/*
 *  Copyright 2013 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 01-May-2013
 *  @author Sunny Agarwal
 */
package com.uniware.web.interceptor;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.ext.Provider;

import org.jboss.resteasy.annotations.interception.ServerInterceptor;
import org.jboss.resteasy.core.ResourceMethod;
import org.jboss.resteasy.core.ServerResponse;
import org.jboss.resteasy.spi.Failure;
import org.jboss.resteasy.spi.HttpRequest;
import org.jboss.resteasy.spi.interception.PreProcessInterceptor;

import com.google.gson.Gson;
import com.unifier.core.configuration.ConfigurationManager;
import com.uniware.services.configuration.ValidationProfileConfiguration;

@Provider
@ServerInterceptor
public class UniwareValidationProfileInterceptor implements PreProcessInterceptor {

    @Override
    public ServerResponse preProcess(HttpRequest request, ResourceMethod method) throws Failure, WebApplicationException {
        if (request.getHttpHeaders().getRequestHeader("VALIDATION_PROFILE") != null) {
            Class<?> requestClazz = method.getMethod().getParameterTypes()[0];
            ServerResponse response = new ServerResponse();
            response.setEntity(new Gson().toJson(ConfigurationManager.getInstance().getConfiguration(ValidationProfileConfiguration.class).getValidationProfile(requestClazz)));
            return response;
        }
        return null;
    }
}
