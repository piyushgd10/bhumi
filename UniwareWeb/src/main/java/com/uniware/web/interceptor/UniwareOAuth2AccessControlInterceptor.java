/*
 *  Copyright 2013 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 01-May-2013
 *  @author Sunny Agarwal
 */
package com.uniware.web.interceptor;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.ext.Provider;

import com.unifier.core.entity.User;
import com.uniware.core.cache.TenantCache;
import com.uniware.core.entity.Tenant;
import org.jboss.resteasy.annotations.interception.ServerInterceptor;
import org.jboss.resteasy.core.ResourceMethod;
import org.jboss.resteasy.core.ServerResponse;
import org.jboss.resteasy.spi.Failure;
import org.jboss.resteasy.spi.HttpRequest;
import org.jboss.resteasy.spi.interception.PreProcessInterceptor;
import org.jboss.resteasy.util.HttpResponseCodes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import com.unifier.core.annotation.Level;
import com.unifier.core.cache.CacheManager;
import com.unifier.services.users.IUsersService;
import com.uniware.core.cache.RolesCache;
import com.uniware.core.entity.Tenant;
import com.uniware.core.utils.UserContext;
import com.uniware.web.security.annotation.UniwareEndPointAccess;

@Provider
@ServerInterceptor
public class UniwareOAuth2AccessControlInterceptor implements PreProcessInterceptor {

    @Autowired
    private IUsersService usersService;

    @Override
    public ServerResponse preProcess(HttpRequest request, ResourceMethod method) throws Failure, WebApplicationException {
        UniwareEndPointAccess uniwareEndpointAccess = method.getMethod().getAnnotation(UniwareEndPointAccess.class);
        String errorMessage = null;
        boolean accessDenied = false;
        if (uniwareEndpointAccess != null) {
            if(!uniwareEndpointAccess.openForInactiveTenants()) {
                if (UserContext.current().getTenant().getStatusCode().equals(Tenant.StatusCode.INACTIVE)) {
                    accessDenied = true;
                    errorMessage = "Illegal Access, tenant is inactive";
                }
            }
            if (!hasResource(uniwareEndpointAccess.accessResource().name())) {
                accessDenied = true;
                errorMessage = "Access denied";
            } else if (Level.FACILITY == uniwareEndpointAccess.level() && UserContext.current().getFacilityId() == null) {
                accessDenied = true;
                errorMessage = "Illegal Access, facility is required";
            }
            if (accessDenied) {
                ServerResponse response = new ServerResponse();
                response.setStatus(HttpResponseCodes.SC_FORBIDDEN);
                response.setEntity(errorMessage);
                return response;
            }
        }
        return null;
    }

    public Set<String> getUserRoles() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null) {
            Set<String> roles = new HashSet<String>(authentication.getAuthorities().size());
            for (GrantedAuthority auth : authentication.getAuthorities()) {
                roles.add(auth.getAuthority());
            }
            UserContext.current().setUniwareUserName((String) authentication.getPrincipal());
            UserContext.current().setUserId(usersService.getUserByUsername(UserContext.current().getUniwareUserName()).getId());
            return roles;
        }
        return Collections.emptySet();
    }

    public boolean hasResource(String accessResource) {
        Set<String> userRoles = getUserRoles();
        Set<String> roles = CacheManager.getInstance().getCache(RolesCache.class).getRolesByAccessResource(accessResource);
        if (roles != null) {
            for (String role : roles) {
                if (userRoles.contains(role)) {
                    return true;
                }
            }
        }
        return false;
    }

}
