/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 03-Jun-2014
 *  @author harsh
 */
package com.uniware.web.interceptor;

import java.io.StringWriter;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.stream.StreamResult;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ws.context.MessageContext;
import org.springframework.ws.server.EndpointInterceptor;
import org.springframework.xml.transform.TransformerObjectSupport;

import com.unifier.core.cache.CacheManager;
import com.uniware.core.cache.EnvironmentPropertiesCache;

public class SoapLoggingInterceptor extends TransformerObjectSupport implements EndpointInterceptor {

    private static final Logger LOG = LoggerFactory.getLogger(SoapLoggingInterceptor.class);

    private static ThreadLocal<Long> startTime = new ThreadLocal<>();

    @Override
    public boolean handleRequest(MessageContext messageContext, Object endpoint) throws Exception {
        EnvironmentPropertiesCache cache = CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class);
        if (cache.isSoapRequestLoggingEnabled()) {
            startTime.set(System.currentTimeMillis());
        }
        return true;
    }

    @Override
    public boolean handleResponse(MessageContext messageContext, Object endpoint) throws Exception {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean handleFault(MessageContext messageContext, Object endpoint) throws Exception {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void afterCompletion(MessageContext messageContext, Object endpoint, Exception ex) throws Exception {
        EnvironmentPropertiesCache cache = CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class);
        if (cache.isSoapRequestLoggingEnabled()) {
            LOG.info("Request: {}, Response: {}, Time Taken: {} ms", getMessage(messageContext.getRequest().getPayloadSource()),
                    getMessage(messageContext.getResponse().getPayloadSource()), System.currentTimeMillis() - startTime.get());
        }

    }

    private Transformer createNonIndentingTransformer() throws TransformerConfigurationException {
        Transformer transformer = createTransformer();
        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
        transformer.setOutputProperty(OutputKeys.INDENT, "no");
        return transformer;
    }

    private String getMessage(Source source) throws TransformerException {
        if (source != null) {
            Transformer transformer = createNonIndentingTransformer();
            StringWriter writer = new StringWriter();
            transformer.transform(source, new StreamResult(writer));
            return writer.toString().replaceAll("\\n", "");
        }
        return null;
    }

}
