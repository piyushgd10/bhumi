/*
 *  Copyright 2013 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 11-Nov-2013
 *  @author sunny
 */
package com.uniware.web.cache.data.manager;

import org.springframework.stereotype.Component;

import com.unifier.core.cache.data.manager.IAppPropertyManager;
import com.unifier.web.utils.WebContextUtils;

@Component("appPropertyManager")
public class AppPropertyManager implements IAppPropertyManager {

    @Override
    public String getAppRootPath() {
        return WebContextUtils.getServletContext().getRealPath("/");
    }

}
