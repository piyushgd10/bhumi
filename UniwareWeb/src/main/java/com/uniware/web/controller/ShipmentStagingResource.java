/*
 * Copyright 2016 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 10/9/16 8:57 PM
 * @author bhuvneshwarkumar
 */

package com.uniware.web.controller;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.uniware.core.api.picker.ScanShipmentAtStagingRequest;
import com.uniware.core.api.picker.ScanShipmentAtStagingResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.uniware.core.api.picker.ScanItemAtStagingRequest;
import com.uniware.core.api.picker.ScanItemAtStagingResponse;
import com.uniware.services.staging.IShipmentStagingService;

/**
 * Created by bhuvneshwarkumar on 10/09/16.
 */
@Controller
@Path("/data/oms/staging/")
public class ShipmentStagingResource {

    @Autowired
    private IShipmentStagingService shipmentStagingService;

    @Produces(MediaType.APPLICATION_JSON)
    @Path("picklist/item/scan")
    @POST
    public ScanItemAtStagingResponse scanItemAtStaging(ScanItemAtStagingRequest request) {
        return shipmentStagingService.scanItemAtStaging(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("picklist/shipment/scan")
    @POST
    public ScanShipmentAtStagingResponse scanShipmentAtStaging(ScanShipmentAtStagingRequest request) {
        return shipmentStagingService.scanShipmentAtStaging(request);
    }
}
