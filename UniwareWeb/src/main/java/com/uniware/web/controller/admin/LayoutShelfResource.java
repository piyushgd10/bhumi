/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Apr 18, 2012
 *  @author praveeng
 */
package com.uniware.web.controller.admin;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.unifier.core.cache.CacheManager;
import com.unifier.core.template.Template;
import com.unifier.core.utils.FileUtils;
import com.unifier.core.utils.StringUtils;
import com.uniware.core.api.inflow.PrintLabelsRequest;
import com.uniware.core.entity.Shelf;
import com.uniware.core.vo.PrintTemplateVO;
import com.uniware.services.cache.PrintTemplateCache;
import com.uniware.services.warehouse.IShelfService;

@Controller
@RequestMapping("/data/admin/layout")
public class LayoutShelfResource {
    @Autowired
    private IShelfService shelfService;

    @RequestMapping(value = "/shelf/print", method = RequestMethod.POST)
    @ResponseBody
    public void shelfLabels(PrintLabelsRequest request, HttpServletResponse response) throws IOException {
        List<Shelf> shelfs = shelfService.getShelfs(StringUtils.split(request.getItemCodes()));
        if (shelfs != null) {
            response.setContentType("application/csv");
            response.addHeader("content-disposition", "attachment; filename=\"shelflabels.csv\"");
            Template template = CacheManager.getInstance().getCache(PrintTemplateCache.class).getTemplateByType(PrintTemplateVO.Type.SHELF_CSV.name());
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("shelfs", shelfs);
            String output = template.evaluate(params);
            FileUtils.write(output.getBytes(), response.getOutputStream());
        }
    }
}
