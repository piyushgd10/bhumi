/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 16/04/14
 *  @author amit
 */

package com.uniware.web.controller;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.uniware.core.api.cms.GetContentForKeyRequest;
import com.uniware.core.api.cms.GetContentForKeyResponse;
import com.uniware.core.api.cms.GetContentForNamespaceRequest;
import com.uniware.core.api.cms.GetContentForNamespaceResponse;
import com.uniware.core.api.cms.GetVersionForNamespaceRequest;
import com.uniware.core.api.cms.GetVersionForNamespaceResponse;
import com.uniware.core.api.cms.SaveContentRequest;
import com.uniware.core.api.cms.SaveContentResponse;
import com.uniware.services.cms.IContentService;

@Controller
@Path("/data/cms/")
public class ContentResource {

    @Autowired
    private IContentService contentService;

    @Path("get/key")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public GetContentForKeyResponse getContent(GetContentForKeyRequest request) {
        return contentService.getContentForKey(request);
    }

    @Path("get/ns")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public GetContentForNamespaceResponse getContentForNamespace(GetContentForNamespaceRequest request) {
        return contentService.getContentForNamespace(request);
    }

    @Path("get/ns/version")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public GetVersionForNamespaceResponse getVersionForNamespace(GetVersionForNamespaceRequest request) {
        return contentService.getVersionForNamespace(request);
    }

    @Path("save")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public SaveContentResponse saveContent(SaveContentRequest request) {
        return contentService.saveContent(request);
    }
}
