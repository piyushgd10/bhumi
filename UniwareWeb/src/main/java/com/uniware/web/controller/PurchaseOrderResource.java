/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 1, 2012
 *  @author praveeng
 */
package com.uniware.web.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.Gson;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.template.Template;
import com.unifier.services.pdf.IPdfDocumentService;
import com.unifier.services.pdf.impl.PdfDocumentServiceImpl.PrintOptions;
import com.unifier.web.utils.WebContextUtils;
import com.uniware.core.api.inflow.GetInflowReceiptItemsRequest;
import com.uniware.core.api.inflow.GetInflowReceiptItemsResponse;
import com.uniware.core.api.inflow.GetPOInflowReceiptsRequest;
import com.uniware.core.api.inflow.GetPOInflowReceiptsResponse;
import com.uniware.core.api.party.SearchVendorItemTypesRequest;
import com.uniware.core.api.party.SearchVendorItemTypesResponse;
import com.uniware.core.api.purchase.AddOrEditPurchaseOrderItemsRequest;
import com.uniware.core.api.purchase.AddOrEditPurchaseOrderItemsResponse;
import com.uniware.core.api.purchase.AmendPurchaseOrderRequest;
import com.uniware.core.api.purchase.AmendPurchaseOrderResponse;
import com.uniware.core.api.purchase.ApprovePurchaseOrderRequest;
import com.uniware.core.api.purchase.ApprovePurchaseOrderResponse;
import com.uniware.core.api.purchase.BulkApprovePurchaseOrderRequest;
import com.uniware.core.api.purchase.BulkApprovePurchaseOrderResponse;
import com.uniware.core.api.purchase.CancelPurchaseOrderRequest;
import com.uniware.core.api.purchase.CancelPurchaseOrderResponse;
import com.uniware.core.api.purchase.ClonePurchaseOrderRequest;
import com.uniware.core.api.purchase.ClonePurchaseOrderResponse;
import com.uniware.core.api.purchase.ClosePurchaseOrderRequest;
import com.uniware.core.api.purchase.ClosePurchaseOrderResponse;
import com.uniware.core.api.purchase.CreatePurchaseOrderRequest;
import com.uniware.core.api.purchase.CreatePurchaseOrderResponse;
import com.uniware.core.api.purchase.EmailPurchaseOrderRequest;
import com.uniware.core.api.purchase.EmailPurchaseOrderResponse;
import com.uniware.core.api.purchase.GetNextPurchaseOrderCodeResponse;
import com.uniware.core.api.purchase.GetPurchaseOrderActivitiesRequest;
import com.uniware.core.api.purchase.GetPurchaseOrderActivitiesResponse;
import com.uniware.core.api.purchase.GetPurchaseOrderRequest;
import com.uniware.core.api.purchase.GetPurchaseOrderResponse;
import com.uniware.core.api.purchase.LookupPurchaseOrderLineItemsRequest;
import com.uniware.core.api.purchase.LookupPurchaseOrderLineItemsResponse;
import com.uniware.core.api.purchase.PurchaseOrderDTO;
import com.uniware.core.api.purchase.PurchaseOrderItemDTO;
import com.uniware.core.api.purchase.RejectPurchaseOrderRequest;
import com.uniware.core.api.purchase.RejectPurchaseOrderResponse;
import com.uniware.core.api.purchase.SearchPurchaseOrderRequest;
import com.uniware.core.api.purchase.SearchPurchaseOrderResponse;
import com.uniware.core.api.purchase.SendPurchaseOrderForApprovalRequest;
import com.uniware.core.api.purchase.SendPurchaseOrderForApprovalResponse;
import com.uniware.core.api.purchase.UpdatePurchaseOrderMetadataRequest;
import com.uniware.core.api.purchase.UpdatePurchaseOrderMetadataResponse;
import com.uniware.core.api.purchaseOrder.display.GetPurchaseOrderSummaryRequest;
import com.uniware.core.api.purchaseOrder.display.GetPurchaseOrderSummaryResponse;
import com.uniware.core.entity.PurchaseOrder;
import com.uniware.core.vo.PrintTemplateVO;
import com.uniware.core.vo.SamplePrintTemplateVO;
import com.uniware.services.cache.PrintTemplateCache;
import com.uniware.services.cache.SamplePrintTemplateCache;
import com.uniware.services.inflow.IInflowService;
import com.uniware.services.purchase.IPurchaseOrderDisplayService;
import com.uniware.services.purchase.IPurchaseService;
import com.uniware.services.vendor.IVendorService;

@Controller
@Path("/data/po/")
public class PurchaseOrderResource {

    @Autowired
    IPurchaseService                     purchaseService;

    @Autowired
    IVendorService                       vendorService;

    @Autowired
    IPdfDocumentService                  pdfDocumentService;

    @Autowired
    private IInflowService               inflowService;

    @Autowired
    private IPurchaseOrderDisplayService purchaseOrderDisplayService;

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/create")
    @POST
    public CreatePurchaseOrderResponse createPurchaseOrder(CreatePurchaseOrderRequest request) {
        request.setUserId(WebContextUtils.getCurrentUser().getUser().getId());
        return purchaseService.createPurchaseOrder(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/next/code")
    @GET
    public GetNextPurchaseOrderCodeResponse getNextPurchaseOrderCode() {
        return purchaseService.getNextPurchaseOrderCode();
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/fetch")
    @POST
    public GetPurchaseOrderResponse getPurchaseOrder(GetPurchaseOrderRequest request) {
        return purchaseService.getPurchaseOrderDetail(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/vendorItemType/search")
    @POST
    public SearchVendorItemTypesResponse listVendorItemTypes(SearchVendorItemTypesRequest request) {
        return vendorService.searchVendorItemType(request);
    }

    @RequestMapping("/data/po/lookupPurchaseOrderLineItems/sampleCSV")
    public void getImportCSVFormat(HttpServletResponse response) throws IOException {
        response.setContentType("application/csv");
        response.addHeader("content-disposition", "attachment; filename=\"lookupPurchaseOrderLineItems.csv\"");
        response.getOutputStream().write("Item SKU Code,MRP,Units,Selling Price,Tax,Discount".getBytes());
    }

    @RequestMapping(value = "/data/po/lookupPurchaseOrderLineItems", method = RequestMethod.POST)
    @ResponseBody
    public String lookupPurchaseOrderLineItems(@RequestParam("vendorCode") String vendorCode, @RequestParam("file") MultipartFile file) throws IOException {
        LookupPurchaseOrderLineItemsRequest request = new LookupPurchaseOrderLineItemsRequest();
        request.setVendorCode(vendorCode);
        request.setInputStream(file.getInputStream());
        LookupPurchaseOrderLineItemsResponse response = purchaseService.lookupPurchaseOrderLineItems(request);
        return new Gson().toJson(response);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/create/item")
    @POST
    public AddOrEditPurchaseOrderItemsResponse addPurchaseOrderItem(AddOrEditPurchaseOrderItemsRequest request) {
        return purchaseService.addOrEditPurchaseOrderItems(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/update/item")
    @POST
    public AddOrEditPurchaseOrderItemsResponse editPurchaseOrderItem(AddOrEditPurchaseOrderItemsRequest request) {
        return purchaseService.addOrEditPurchaseOrderItemsReadOnly(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/cancel")
    @POST
    public CancelPurchaseOrderResponse cancelPurchaseOrder(CancelPurchaseOrderRequest request) {
        request.setUserId(WebContextUtils.getCurrentUser().getUser().getId());
        return purchaseService.cancelPurchaseOrder(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/reject")
    @POST
    public RejectPurchaseOrderResponse rejectPurchaseOrder(RejectPurchaseOrderRequest request) {
        request.setUserId(WebContextUtils.getCurrentUser().getUser().getId());
        return purchaseService.rejectPurchaseOrder(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/approve")
    @POST
    public ApprovePurchaseOrderResponse approvePurchaseOrder(ApprovePurchaseOrderRequest request) {
        request.setUserId(WebContextUtils.getCurrentUser().getUser().getId());
        return purchaseService.approvePurchaseOrder(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/bulk/approve")
    @POST
    public BulkApprovePurchaseOrderResponse bulkApprovePurchaseOrders(BulkApprovePurchaseOrderRequest request) {
        request.setUserId(WebContextUtils.getCurrentUser().getUser().getId());
        return purchaseService.bulkApprovePurchaseOrders(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("poItem/createRow")
    @GET
    public PurchaseOrderItemDTO createPurchaseOrderItemRow() {
        PurchaseOrderItemDTO purchaseOrderItem = new PurchaseOrderItemDTO();
        return purchaseOrderItem;
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("vendor/purchaseOrders")
    @GET
    public List<PurchaseOrderDTO> getVendorPurchaseOrders(@QueryParam("vendorCode") String vendorCode) {
        return purchaseService.getVendorPurchaseOrders(vendorCode);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("waiting")
    @GET
    public List<PurchaseOrderDTO> getPurchaseOrdersWaitingForApproval() {
        return purchaseService.getPurchaseOrdersWaitingForApproval();
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("active")
    @GET
    public List<PurchaseOrderDTO> getActivePurchaseOrders() {
        return purchaseService.getActivePurchaseOrders();
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/send/email")
    @POST
    public EmailPurchaseOrderResponse sendPurchaseOrder(EmailPurchaseOrderRequest request) {
        return purchaseService.emailPurchaseOrder(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/fetchSummary")
    @POST
    public GetPurchaseOrderSummaryResponse getPurchaseOrderSummary(GetPurchaseOrderSummaryRequest request) {
        return purchaseOrderDisplayService.getPurchaseOrderSummary(request);
    }

    @RequestMapping("/po/show")
    public void printPurchaseOrder(@RequestParam(value = "code", required = true) String purchaseOrderCode, HttpServletResponse response) throws IOException {
        PurchaseOrder purchaseOrder = purchaseService.getPurchaseOrderDetailedByCode(purchaseOrderCode);
        if (purchaseOrder != null) {
            response.setContentType("application/pdf");
            Template template = CacheManager.getInstance().getCache(PrintTemplateCache.class).getTemplateByType(PrintTemplateVO.Type.PURCHASE_ORDER.name());
            PrintTemplateVO purchaseOrderTemplate = CacheManager.getInstance().getCache(PrintTemplateCache.class).getPrintTemplateByType(PrintTemplateVO.Type.PURCHASE_ORDER);
            SamplePrintTemplateVO samplePrintTemplate = CacheManager.getInstance().getCache(SamplePrintTemplateCache.class).getSamplePrintTemplate(
                    purchaseOrderTemplate.getSamplePrintTemplateCode());
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("purchaseOrder", purchaseOrder);
            String purchaseOrderHtml = template.evaluate(params);
            pdfDocumentService.writeHtmlToPdf(response.getOutputStream(), purchaseOrderHtml, new PrintOptions(samplePrintTemplate));
        }
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/searchPO")
    @POST
    public SearchPurchaseOrderResponse searchPurchaseOrders(SearchPurchaseOrderRequest request) {
        return purchaseService.searchPurchaseOrders(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/close")
    @POST
    public ClosePurchaseOrderResponse closePurchaseOrder(ClosePurchaseOrderRequest request) {
        request.setUserId(WebContextUtils.getCurrentUser().getUser().getId());
        return purchaseService.closePurchaseOrder(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/sendForApproval")
    @POST
    public SendPurchaseOrderForApprovalResponse sendPurchaseOrderForApproval(SendPurchaseOrderForApprovalRequest request) {
        request.setUserId(WebContextUtils.getCurrentUser().getUser().getId());
        return purchaseService.sendPurchaseOrderForApproval(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("inflowReceipts/fetch")
    @POST
    public GetPOInflowReceiptsResponse getInflowReceipts(GetPOInflowReceiptsRequest request) {
        return purchaseService.getInflowReceipts(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/inflowReceiptsItems/fetch")
    @POST
    public GetInflowReceiptItemsResponse getInflowReceiptItems(GetInflowReceiptItemsRequest request) {
        return inflowService.getInflowReceiptItems(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/update/metadata")
    @POST
    public UpdatePurchaseOrderMetadataResponse updatePurchaseOrderMetadata(UpdatePurchaseOrderMetadataRequest request) {
        return purchaseService.updatePurchaseOrderMetadata(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/amend")
    @POST
    public AmendPurchaseOrderResponse amendPurchaseOrder(AmendPurchaseOrderRequest request) {
        request.setUserId(WebContextUtils.getCurrentUser().getUser().getId());
        return purchaseService.amendPurchaseOrder(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/clone")
    @POST
    public ClonePurchaseOrderResponse clonePurchaseOrder(ClonePurchaseOrderRequest request) {
        request.setUserId(WebContextUtils.getCurrentUser().getUser().getId());
        return purchaseService.clonePurchaseOrder(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/fetchActivities")
    @POST
    public GetPurchaseOrderActivitiesResponse clonePurchaseOrder(GetPurchaseOrderActivitiesRequest request) {
        return purchaseService.getPurchaseOrderActivities(request);
    }
}
