/*
 *  Copyright 2013 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 27-May-2013
 *  @author praveeng
 */
package com.uniware.web.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.InputStreamBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.Gson;
import com.unifier.core.api.base.ServiceResponse;
import com.unifier.core.api.document.GetDocumentAuthDetailsRequest;
import com.unifier.core.api.document.GetDocumentAuthDetailsResponse;
import com.unifier.core.api.document.ListDocumentsRequest;
import com.unifier.core.api.document.ListDocumentsResponse;
import com.unifier.core.api.document.ListDocumentsResponse.DocumentDTO;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.transport.http.HttpClientFactory;
import com.unifier.core.transport.http.HttpSender;
import com.unifier.core.transport.http.HttpTransportException;
import com.unifier.core.utils.EncryptionUtils;
import com.unifier.core.utils.FileUtils;
import com.unifier.core.utils.JsonUtils;
import com.unifier.core.utils.StringUtils;
import com.unifier.web.security.UniwareUser;
import com.unifier.web.utils.WebContextUtils;
import com.uniware.core.cache.EnvironmentPropertiesCache;
import com.uniware.core.utils.UserContext;
import com.uniware.services.document.IDocumentService;

@Controller
@Path("/data/document")
public class DocumentResource {

    @Autowired
    private IDocumentService documentService;

    @RequestMapping("/data/document/upload")
    @ResponseBody
    public String uploadDoc(@RequestParam("file") MultipartFile file, @RequestParam("identifier") String identifier) throws IOException, HttpTransportException {
        GetDocumentAuthDetailsRequest request = new GetDocumentAuthDetailsRequest();
        request.setIdentifier(identifier);
        GetDocumentAuthDetailsResponse authDetailsResponse = getDocumentAuthDetails(request);
        ServiceResponse response = new ServiceResponse();
        if (authDetailsResponse.isSuccessful()) {
            HttpClient httpClient = HttpClientFactory.buildHttpClient(true);
            UniwareUser currentUser = WebContextUtils.getCurrentUser();
            String url = authDetailsResponse.getUrl() + "/document/upload?identifier=" + authDetailsResponse.getIdentifier() + "&token=" + authDetailsResponse.getToken()
                    + "&checksum=" + authDetailsResponse.getChecksum() + "&username=" + currentUser.getUsername() + "&maxFileSize=2";
            HttpPost post = new HttpPost(url);
            MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
            entity.addPart(file.getName(), new InputStreamBody(file.getInputStream(), file.getName()));
            post.setEntity(entity);
            httpClient.execute(post);
            response.setSuccessful(true);
        }
        return new Gson().toJson(response);
    }

    @SuppressWarnings("unchecked")
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/list")
    @POST
    public ListDocumentsResponse listDocuments(ListDocumentsRequest request) {
        ListDocumentsResponse response = new ListDocumentsResponse();
        GetDocumentAuthDetailsRequest authDetailsRequest = new GetDocumentAuthDetailsRequest();
        authDetailsRequest.setIdentifier(request.getIdentifier());
        GetDocumentAuthDetailsResponse authDetailsResponse = getDocumentAuthDetails(authDetailsRequest);
        if (authDetailsResponse.isSuccessful()) {
            UniwareUser currentUser = WebContextUtils.getCurrentUser();
            HttpSender sender = new HttpSender(true);
            String url = authDetailsResponse.getUrl() + "/documents/list?identifier=" + authDetailsResponse.getIdentifier() + "&token=" + authDetailsResponse.getToken()
                    + "&checksum=" + authDetailsResponse.getChecksum() + "&username=" + currentUser.getUsername();
            try {
                String documentJson = sender.executeGet(url, null);
                List<DocumentDTO> documents = new ArrayList<DocumentDTO>();
                documents = JsonUtils.stringToJson(documentJson, documents.getClass());
                response.setDocuments(documents);
                response.setSuccessful(true);
            } catch (HttpTransportException e) {
                e.printStackTrace();
            }
        }
        return response;
    }

    @RequestMapping("/data/document/download/{downloadLocation}")
    public void downloadDoc(@PathVariable("downloadLocation") String downloadLocation, HttpServletResponse response) throws IOException, HttpTransportException {
        HttpSender sender = new HttpSender(true);
        String url = CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class).getDocumentServiceUrl() + "/files/" + downloadLocation;
        HttpResponse downloadResponse = sender.processGetRequest(url, null, null);
        HttpEntity httpEntity = downloadResponse.getEntity();
        InputStream inputStream = null;
        try {
            inputStream = httpEntity.getContent();
            FileUtils.copyStream(inputStream, response.getOutputStream());
            Header contentType = downloadResponse.getFirstHeader("Content-Type");
            response.setContentType(contentType.getValue().split(";")[0].trim());
            Header contentDisposition = downloadResponse.getFirstHeader("content-disposition");
            response.addHeader(contentDisposition.getName(), contentDisposition.getValue());
        } finally {
            FileUtils.safelyCloseInputStream(inputStream);
        }
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/auth/details/get")
    @POST
    public GetDocumentAuthDetailsResponse getDocumentAuthDetails(GetDocumentAuthDetailsRequest request) {
        UniwareUser currentUser = WebContextUtils.getCurrentUser();
        String fileUploadToken = currentUser.getFileUploadToken();
        if (StringUtils.isBlank(fileUploadToken)) {
            synchronized (currentUser) {
                fileUploadToken = currentUser.getFileUploadToken();
                if (StringUtils.isBlank(fileUploadToken)) {
                    fileUploadToken = documentService.getFileUploadToken();
                    currentUser.setFileUploadToken(fileUploadToken);
                }
            }
        }
        GetDocumentAuthDetailsResponse response = new GetDocumentAuthDetailsResponse();
        response.setToken(fileUploadToken);
        String identifier = request.getIdentifier() + "-" + UserContext.current().getTenant().getCode();
        response.setIdentifier(identifier);
        response.setChecksum(getChecksum(identifier, fileUploadToken, currentUser.getUsername()));
        response.setUrl(CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class).getDocumentServiceUrl());
        response.setSuccessful(true);
        return response;
    }

    private String getChecksum(String identifier, String fileUploadToken, String username) {
        String fileUploadServerApiKey = CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class).getDocumentServicePassword();
        return EncryptionUtils.md5Encode(identifier + fileUploadToken + fileUploadServerApiKey + username);
    }

}
