/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jan 12, 2012
 *  @author singla
 */
package com.uniware.web.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.*;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.unifier.services.pdf.impl.PdfDocumentServiceImpl;
import com.uniware.core.api.shipping.*;
import com.uniware.services.invoice.IInvoiceService;
import com.uniware.services.shipping.IShippingInvoiceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.itextpdf.text.DocumentException;
import com.unifier.core.api.print.PrintTemplateDTO;
import com.unifier.core.api.validation.WsError;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.exception.TemplateCompilationException;
import com.unifier.core.template.Template;
import com.unifier.core.transport.http.HttpSender;
import com.unifier.core.transport.http.HttpTransportException;
import com.unifier.core.utils.EncryptionUtils;
import com.unifier.core.utils.FileUtils;
import com.unifier.core.utils.PdfUtils;
import com.unifier.core.utils.StringUtils;
import com.unifier.services.pdf.IPdfDocumentService;
import com.unifier.services.pdf.impl.PdfDocumentServiceImpl.PrintOptions;
import com.unifier.web.utils.WebContextUtils;
import com.uniware.core.api.print.PrintPreviewRequest;
import com.uniware.core.api.saleorder.CreateShippingPackageRequest;
import com.uniware.core.api.saleorder.CreateShippingPackageResponse;
import com.uniware.core.entity.Channel;
import com.uniware.core.entity.ShippingPackage;
import com.uniware.core.entity.Source;
import com.uniware.core.entity.Source.ShipmentLabelAggregationFormat;
import com.uniware.core.entity.Source.ShipmentLabelFormat;
import com.uniware.core.utils.UserContext;
import com.uniware.core.vo.PrintTemplateVO;
import com.uniware.core.vo.SamplePrintTemplateVO;
import com.uniware.dao.shipping.IPaymentReconciliationService;
import com.uniware.services.cache.ChannelCache;
import com.uniware.services.cache.GlobalSamplePrintTemplateCache;
import com.uniware.services.cache.PrintTemplateCache;
import com.uniware.services.cache.SamplePrintTemplateCache;
import com.uniware.services.configuration.SourceConfiguration;
import com.uniware.services.shipping.IDispatchService;
import com.uniware.services.shipping.IRegulatoryService;
import com.uniware.services.shipping.IShippingProviderService;
import com.uniware.services.shipping.IShippingService;
import com.uniware.services.tenant.ITenantService;
import com.uniware.web.messaging.simp.annotation.SendToCurrentUser;

/**
 * @author singla
 */
@Controller
@Path("/data/oms/shipment/")
public class ShipmentResource {

    private static final Logger           LOG        = LoggerFactory.getLogger(ShipmentResource.class);
    public static final String            PAGE_BREAK = "<div style=\"page-break-before: always;height:1px;\"></div>";

    @Autowired
    private IShippingService              shippingService;

    @Autowired
    private IShippingProviderService      shippingProviderService;

    @Autowired
    private IPdfDocumentService           pdfDocumentService;

    @Autowired
    private IPaymentReconciliationService paymentReconciliationService;

    @Autowired
    private IRegulatoryService            regulatoryService;

    @Autowired
    private IDispatchService              dispatchService;

    @Autowired
    private IInvoiceService               invoiceService;

    @Autowired
    private IShippingInvoiceService       shippingInvoiceService;

    @Produces(MediaType.APPLICATION_JSON)
    @Path("provider/allocate")
    @POST
    public AllocateShippingProviderResponse allocateShippingProvider(AllocateShippingProviderRequest allocateShippingProviderRequest) {
        return shippingProviderService.allocateShippingProvider(allocateShippingProviderRequest);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("bulk/provider/allocate")
    @POST
    public BulkAllocateShippingProviderResponse bulkAllocateShippingProvider(BulkAllocateShippingProviderRequest allocateShippingProviderRequest) {
        allocateShippingProviderRequest.setUserId(WebContextUtils.getCurrentUser().getUser().getId());
        try {
            return shippingProviderService.bulkAllocateShippingProvider(allocateShippingProviderRequest);
        } catch (Throwable e) {
            BulkAllocateShippingProviderResponse response = new BulkAllocateShippingProviderResponse();
            response.addError(new WsError(90001, e.getMessage()));
            return response;
        }
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("refetchShippingLabel")
    @POST
    public ReAllocateShippingProviderResponse reallocateShippingProvider(ReAllocateShippingProviderRequest request) {
        return shippingProviderService.reallocateShippingProvider(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("refreshShippingLabel")
    @POST
    public RefreshShippingLabelResponse refreshShippingLabel(RefreshShippingLabelRequest request) {
        return shippingProviderService.refreshShippingLabel(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("bulk/invoice/provider/allocate")
    @POST
    public BulkCreateInvoiceAndAllocateShippingProviderResponse bulkAllocateShippingProvider(BulkCreateInvoiceAndAllocateShippingProviderRequest request) {
        request.setUserId(WebContextUtils.getCurrentUser().getUser().getId());
        try {
            return shippingProviderService.bulkCreateInvoiceAndAllocateShippingProvider(request);
        } catch (Throwable e) {
            BulkCreateInvoiceAndAllocateShippingProviderResponse response = new BulkCreateInvoiceAndAllocateShippingProviderResponse();
            response.addError(new WsError(90001, e.getMessage()));
            return response;
        }
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("awb/assign")
    @POST
    public AssignManualTrackingNumberResponse assignManualTrackingNumber(AssignManualTrackingNumberRequest request) {
        return shippingProviderService.assignManualTrackingNumber(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("invoice/provider/allocate")
    @POST
    public CreateInvoiceAndAllocateShippingProviderResponse createInvoiceAndAllocateShippingProvider(
            CreateInvoiceAndAllocateShippingProviderRequest createInvoiceAndAllocateShippingProviderRequest) {
        try {
            createInvoiceAndAllocateShippingProviderRequest.setUserId(WebContextUtils.getCurrentUser().getUser().getId());
            return shippingProviderService.createInvoiceAndAllocateShippingProvider(createInvoiceAndAllocateShippingProviderRequest);
        } catch (Throwable e) {
            LOG.error("Error while createInvoiceAndAllocateShippingProvider:", e);
            CreateInvoiceAndAllocateShippingProviderResponse response = new CreateInvoiceAndAllocateShippingProviderResponse();
            response.addError(new WsError(90001, e.getMessage()));
            return response;
        }
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("markReadyForPickup")
    @POST
    public MarkShippingPackageReadyForPickupResponse markPackageReadyForPickup(MarkShippingPackageReadyForPickupRequest request) {
        request.setUserId(WebContextUtils.getCurrentUser().getUser().getId());
        return shippingProviderService.markShippingPackageReadyForPickup(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("resendPODCode")
    @POST
    public ResendPODCodeResponse resendPODCode(ResendPODCodeRequest request) {
        return dispatchService.resendPODCode(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("markDelivered")
    @POST
    public MarkShippingPackageDeliveredResponse markShippingPackageDelivered(MarkShippingPackageDeliveredRequest request) {
        return dispatchService.markShippingPackageDelivered(request);
    }

    @RequestMapping("/oms/shipment/show/{shippingPackageCodes}")
    public void printShipmentLabels(@PathVariable("shippingPackageCodes") String shippingPackageCodes, HttpServletResponse response)
            throws IOException, URISyntaxException, DocumentException, HttpTransportException {
        List<String> shippingPackageCodeList = StringUtils.split(shippingPackageCodes, "-");
        GenerateShippingLabelsRequest request = new GenerateShippingLabelsRequest();
        request.setShippingPackageCodes(shippingPackageCodeList);
        GenerateShippingLabelsResponse generateShippingLabelsResponse = shippingService.generateShippingLabels(request);
        if (generateShippingLabelsResponse.isSuccessful()) {
            List<File> shippingLabels = generateShippingLabelsResponse.getShippingLabels();
            if (shippingLabels.size() > 1 && ShipmentLabelFormat.PDF.equals(generateShippingLabelsResponse.getShipmentLabelFormat())) {
                pdfDocumentService.merge(shippingLabels, response.getOutputStream());
            } else if (shippingLabels.size() == 1) {
                FileInputStream fis = null;
                try {
                    fis = new FileInputStream(shippingLabels.get(0));
                    FileUtils.copyStream(fis, response.getOutputStream());
                } finally {
                    FileUtils.safelyCloseInputStream(fis);
                }
            }
            switch (generateShippingLabelsResponse.getShipmentLabelFormat()) {
                case PDF:
                    response.setContentType("application/pdf");
                    break;
                case CSV:
                    response.setContentType("application/csv");
                    response.addHeader("content-disposition", "attachment; filename=\"" + generateShippingLabelsResponse.getDownloadFileName() + "\"");
                    break;
                case HTML:
                    response.setContentType("text/html");
                    break;
            }
        }
    }

    @RequestMapping("/oms/shipment/printInvoiceAndLabel/{shippingPackageCode}")
    public void printInvoiceAndLabel(@PathVariable("shippingPackageCode") String shippingPackageCode, HttpServletResponse response) throws Exception {
        String invoiceHtml = shippingInvoiceService.getInvoiceHtmlByShippingPackageCode(shippingPackageCode);
        GenerateShippingLabelsRequest generateShippingLabelsRequest = new GenerateShippingLabelsRequest();
        generateShippingLabelsRequest.setShippingPackageCodes(Arrays.asList(shippingPackageCode));
        GenerateShippingLabelsResponse generateShippingLabelsResponse = shippingService.generateShippingLabels(generateShippingLabelsRequest);
        LOG.info("Generate shipping label response: {}", generateShippingLabelsResponse);
        if (generateShippingLabelsResponse.isSuccessful()) {
            File shippingLabel = generateShippingLabelsResponse.getShippingLabels().get(0);
            if (ShipmentLabelFormat.PDF.equals(generateShippingLabelsResponse.getShipmentLabelFormat())) {
                File invoice = null;
                boolean blankInvoice = false;
                if (StringUtils.isNotBlank(invoiceHtml)) {
                    PrintTemplateVO invoiceTemplate = CacheManager.getInstance().getCache(PrintTemplateCache.class).getPrintTemplateByType(PrintTemplateVO.Type.INVOICE);
                    SamplePrintTemplateVO samplePrintTemplate = CacheManager.getInstance().getCache(SamplePrintTemplateCache.class).getSamplePrintTemplate(
                            invoiceTemplate.getSamplePrintTemplateCode());
                    invoice = new File("/tmp/" + EncryptionUtils.md5Encode(shippingPackageCode + UserContext.current().getTenant().getCode()) + "invoice.pdf");
                    try (FileOutputStream fos = new FileOutputStream(invoice)) {
                        pdfDocumentService.writeHtmlToPdf(fos, invoiceHtml, new PdfDocumentServiceImpl.PrintOptions(samplePrintTemplate));
                    } catch (Exception ex) {
                        LOG.warn("Error generating invoice pdf");
                        // Blank Document, Eh??
                        blankInvoice = true;
                    }
                } else {
                    blankInvoice = true;
                }
                if (!blankInvoice) {
                    List<File> inputFiles = Arrays.asList(invoice, shippingLabel);
                    pdfDocumentService.merge(inputFiles, response.getOutputStream());
                } else {
                    FileInputStream fis = null;
                    try {
                        fis = new FileInputStream(shippingLabel);
                        FileUtils.copyStream(fis, response.getOutputStream());
                    } finally {
                        FileUtils.safelyCloseInputStream(fis);
                    }
                }
                response.setContentType("application/pdf");
            } else if (ShipmentLabelFormat.CSV.equals(generateShippingLabelsResponse.getShipmentLabelFormat())) {
                throw new IllegalArgumentException("Cannot convert CSV to PDF");
            } else if (ShipmentLabelFormat.HTML.equals(generateShippingLabelsResponse.getShipmentLabelFormat())) {
                String outFileName = "/tmp/" + EncryptionUtils.md5Encode(shippingPackageCode + UserContext.current().getTenant().getCode() + "invoice-label") + ".html";
                String invoiceFile = "/tmp/" + EncryptionUtils.md5Encode(shippingPackageCode + UserContext.current().getTenant().getCode() + "invoice") + ".html";
                FileUtils.writeToFile(invoiceFile, invoiceHtml);
                List<String> inputFiles = new ArrayList<>(2);
                inputFiles.add(invoiceFile);
                inputFiles.add(shippingLabel.getAbsolutePath());
                FileUtils.concatFiles(outFileName, inputFiles, PAGE_BREAK);
                FileInputStream fis = null;
                try {
                    fis = new FileInputStream(outFileName);
                    FileUtils.copyStream(fis, response.getOutputStream());
                } finally {
                    FileUtils.safelyCloseInputStream(fis);
                }
                response.setContentType("text/html");
            }
        } else {
            LOG.error("Unable to generate PDF label, error: {}", generateShippingLabelsResponse.getErrors().toString());
            throw new RuntimeException(generateShippingLabelsResponse.getErrors().toString());
        }
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("manifest/fetchChannels")
    @POST
    public GetChannelsForManifestResponse getChannelsForManifest(GetChannelsForManifestRequest request) {
        return dispatchService.getChannelsForManifest(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("manifest/fetchShippingProviders")
    @POST
    public GetEligibleShippingProvidersForManifestResponse getShippingProvidersForManifest(GetEligibleShippingProvidersForManifestRequest request) {
        return dispatchService.getShippingProvidersForManifest(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("manifest/create")
    @POST
    public CreateShippingManifestResponse createShippingManifestRequest(CreateShippingManifestRequest createShippingManifestRequest) {
        createShippingManifestRequest.setUserId(WebContextUtils.getCurrentUser().getUser().getId());
        return dispatchService.createShippingManifest(createShippingManifestRequest);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("manifest/addShippingPackage")
    @POST
    public AddShippingPackageToManifestResponse addShippingPackageToManifest(AddShippingPackageToManifestRequest request) {
        return dispatchService.addShippingPackageToManifest(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("manifest/fetchSummary")
    @POST
    public GetManifestSummaryResponse fetchShippingManifestSummary(GetManifestSummaryRequest request) {
        return dispatchService.fetchShippingManifestSummary(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("manifest/fetchProvidersSummary")
    @POST
    public GetManifestShippingProvidersSummaryResponse fetchManifestShippingProvidersSummary(GetManifestShippingProvidersSummaryRequest request) {
        return dispatchService.fetchManifestShippingProvidersSummary(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("manifest/fetchPackageDetails")
    @POST
    public GetShippingManifestItemDetailsResponse fetchManifestShippingPackageDetails(GetShippingManifestItemDetailsRequest request) {
        return dispatchService.getShippingManifestItemDetails(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("manifest/fetch")
    @POST
    public GetShippingManifestResponse getShippingManifest(GetShippingManifestRequest getShippingManifestRequest) {
        return dispatchService.getShippingManifest(getShippingManifestRequest);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("manifest/getCurrentChannelManifest")
    @POST
    public GetCurrentChannelShippingManifestResponse getCurrentChannelShippingManifest(GetCurrentChannelShippingManifestRequest request) {
        return dispatchService.getCurrentChannelShippingManifest(request);
    }

    @SendToCurrentUser
    @MessageMapping("/data/oms/shipment/manifest/close")
    @Produces(MediaType.APPLICATION_JSON)
    @Path("manifest/close")
    @POST
    public CloseShippingManifestResponse closeShippingManifest(final CloseShippingManifestRequest closeShippingManifestRequest) {
        return dispatchService.closeShippingManifest(closeShippingManifestRequest);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("manifest/checkIfClosing")
    @POST
    public CheckIfShippingManifestClosingResponse checkIfShippingManifestClosing(CheckIfShippingManifestClosingRequest request) {
        return dispatchService.checkIfShippingManifestClosing(request);
    }

    @RequestMapping("/oms/shipment/manifest/download/{manifestCode}")
    public void downloadShippingManifest(@PathVariable("manifestCode") String manifestCode, HttpServletResponse response) throws IOException {
        if (StringUtils.isNotEmpty(manifestCode)) {
            response.setContentType("application/csv");
            response.addHeader("content-disposition", "attachment; filename=\"ShippingManifest-" + manifestCode + ".csv\"");
            Template template = CacheManager.getInstance().getCache(PrintTemplateCache.class).getTemplateByType(PrintTemplateVO.Type.SHIPPING_MANIFEST_CSV.name());
            String output = dispatchService.getShippingManifestOutput(manifestCode, template);
            FileUtils.write(output.getBytes(), response.getOutputStream());
        }
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("manifest/search")
    @POST
    public SearchManifestResponse searchManifest(SearchManifestRequest request) {
        return dispatchService.searchManifest(request);
    }

    @RequestMapping("/oms/shipment/stateRegulatoryForms/print/{shippingPackageCodes}")
    public void printRegulatoryForms(@PathVariable("shippingPackageCodes") String shippingPackageCodes, HttpServletResponse response)
            throws IOException, URISyntaxException, DocumentException, HttpTransportException {
        List<File> regulatoryForms = new ArrayList<>();
        List<String> shippingPackageCodeList = StringUtils.split(shippingPackageCodes, "-");
        for (String shippingPackageCode : shippingPackageCodeList) {
            ShippingPackage shippingPackage = shippingService.getShippingPackageDetailedByCode(shippingPackageCode);
            if (StringUtils.isNotBlank(shippingPackage.getRegulatoryFormCodes())) {
                String[] forms = shippingPackage.getRegulatoryFormCodes().split(",");
                for (String formCodeAndName : forms) {
                    String formCode = formCodeAndName.split(":")[0];
                    String stateRegulatoryFormHtml = getStateRegulatoryFormHtml(formCode, shippingPackageCode);
                    if (StringUtils.isNotBlank(stateRegulatoryFormHtml)) {
                        File regulatoryForm = new File("/tmp/"
                                + EncryptionUtils.md5Encode(shippingPackage.getCode() + "regulatoryForm" + formCode + UserContext.current().getTenant().getCode()) + ".pdf");
                        PrintTemplateDTO printTemplateDTO = CacheManager.getInstance().getCache(SamplePrintTemplateCache.class).getPrintTemplateByTypeAndName(
                                PrintTemplateVO.Type.STATE_REGULATORY_FORM.name(), formCode);
                        if (printTemplateDTO == null) {
                            printTemplateDTO = CacheManager.getInstance().getCache(GlobalSamplePrintTemplateCache.class).getPrintTemplateByTypeAndName(
                                    PrintTemplateVO.Type.STATE_REGULATORY_FORM.name(), formCode);
                        }
                        try (FileOutputStream fos = new FileOutputStream(regulatoryForm)) {
                            pdfDocumentService.writeHtmlToPdf(fos, stateRegulatoryFormHtml, new PrintOptions(printTemplateDTO));
                        }
                        regulatoryForms.add(regulatoryForm);
                    }
                }
            }
        }
        if (!regulatoryForms.isEmpty()) {
            pdfDocumentService.merge(regulatoryForms, response.getOutputStream());
        }
    }

    @RequestMapping("/oms/shipment/stateRegulatoryForm/print/{formCode}/{shippingPackageCode}")
    public void printStateRegulatoryForm(@PathVariable("formCode") String formCode, @PathVariable("shippingPackageCode") String shippingPackageCode, HttpServletResponse response)
            throws IOException {
        String stateRegulatoryFormHtml = getStateRegulatoryFormHtml(formCode, shippingPackageCode);
        if (stateRegulatoryFormHtml != null) {
            response.setContentType("application/pdf");
            PrintTemplateDTO printTemplateDTO = CacheManager.getInstance().getCache(SamplePrintTemplateCache.class).getPrintTemplateByTypeAndName(
                    PrintTemplateVO.Type.STATE_REGULATORY_FORM.name(), formCode);
            if (printTemplateDTO == null) {
                printTemplateDTO = CacheManager.getInstance().getCache(GlobalSamplePrintTemplateCache.class).getPrintTemplateByTypeAndName(
                        PrintTemplateVO.Type.STATE_REGULATORY_FORM.name(), formCode);
            }
            pdfDocumentService.writeHtmlToPdf(response.getOutputStream(), stateRegulatoryFormHtml, new PrintOptions(printTemplateDTO));
        }
    }

    private String getStateRegulatoryFormHtml(String formCode, String shippingPackageCode) {
        Template template = CacheManager.getInstance().getCache(SamplePrintTemplateCache.class).getTemplateByTypeAndName(PrintTemplateVO.Type.STATE_REGULATORY_FORM.name(),
                formCode);
        if (template == null) {
            template = CacheManager.getInstance().getCache(GlobalSamplePrintTemplateCache.class).getTemplateByTypeAndName(PrintTemplateVO.Type.STATE_REGULATORY_FORM.name(),
                    formCode);
        }
        return regulatoryService.getStateRegulatoryFormHtml(shippingPackageCode, template);
    }

    @RequestMapping("/oms/shipment/manifest/print/{shippingManifestCode}")
    public void printShippingManifest(@PathVariable("shippingManifestCode") String shippingManifestCode, HttpServletResponse response) throws IOException {
        Template template = CacheManager.getInstance().getCache(PrintTemplateCache.class).getTemplateByType(PrintTemplateVO.Type.SHIPPING_MANIFEST.name());
        String manifestHtml = dispatchService.getManifestHtml(shippingManifestCode, template);
        if (manifestHtml != null) {
            response.setContentType("application/pdf");
            PrintTemplateVO manifestTemplate = CacheManager.getInstance().getCache(PrintTemplateCache.class).getPrintTemplateByType(PrintTemplateVO.Type.SHIPPING_MANIFEST);
            SamplePrintTemplateVO samplePrintTemplate = CacheManager.getInstance().getCache(SamplePrintTemplateCache.class).getSamplePrintTemplate(
                    manifestTemplate.getSamplePrintTemplateCode());
            pdfDocumentService.writeHtmlToPdf(response.getOutputStream(), manifestHtml, new PrintOptions(samplePrintTemplate));
        }
    }

    @RequestMapping("/admin/printing/template/shipmentLabel/preview")
    @ResponseBody
    public void previewShipmentLabel(PrintPreviewRequest request, HttpServletResponse response) throws IOException, ClassNotFoundException {
        ShippingPackage shippingPackage = shippingService.getShippingPackageDetailedByCode(request.getObjectCode());
        if (shippingPackage != null && shippingPackage.getShippingProviderCode() != null) {
            Template template = null;
            try {
                template = Template.compile(request.getName(), request.getTemplate());
            } catch (TemplateCompilationException ex) {
            }
            String shippingLabelHtml = shippingService.prepareShippingLabelText(shippingPackage.getCode(), template);
            response.setContentType("application/pdf");
            pdfDocumentService.writeHtmlToPdf(response.getOutputStream(), shippingLabelHtml,
                    new PrintOptions(request.getPrintDialog(), request.getNumberOfCopies(), request.isLandscape(), request.getPageSize(), request.getPageMargins()));
        }
    }

    @RequestMapping("/admin/printing/template/shippingManifest/preview")
    @ResponseBody
    public void previewShippingManifest(PrintPreviewRequest request, HttpServletResponse response) throws IOException, ClassNotFoundException {
        Template template = CacheManager.getInstance().getCache(PrintTemplateCache.class).getTemplateByType(PrintTemplateVO.Type.SHIPPING_MANIFEST.name());
        String manifestHtml = dispatchService.getManifestHtml(request.getObjectCode(), template);
        if (manifestHtml != null) {
            response.setContentType("application/pdf");
            PrintTemplateVO manifestTemplate = CacheManager.getInstance().getCache(PrintTemplateCache.class).getPrintTemplateByType(PrintTemplateVO.Type.SHIPPING_MANIFEST);
            SamplePrintTemplateVO samplePrintTemplate = CacheManager.getInstance().getCache(SamplePrintTemplateCache.class).getSamplePrintTemplate(
                    manifestTemplate.getSamplePrintTemplateCode());
            pdfDocumentService.writeHtmlToPdf(response.getOutputStream(), manifestHtml, new PrintOptions(samplePrintTemplate));
        }

    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("manifest/discard")
    @POST
    public DiscardShippingManifestResponse discardShippingManifest(DiscardShippingManifestRequest request) {
        request.setUserId(WebContextUtils.getCurrentUser().getUser().getId());
        return dispatchService.discardShippingManifest(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("manifest/editMetadata")
    @POST
    public EditShippingManifestMetadataResponse editShipingManifestMetadata(EditShippingManifestMetadataRequest request) {
        return dispatchService.editShipingManifestMetadata(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("collectedAmount/edit")
    @POST
    public PaymentReconciliationResponse editCollectedAmount(PaymentReconciliationRequest request) {
        return paymentReconciliationService.reconcileShipmentPayment(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("allShipments/get")
    @POST
    public GetSalesOrderPackagesResponse getShippingPackages(GetSalesOrderPackagesRequest request) {
        return shippingService.getShippingPackages(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("markReconciled")
    @POST
    public MarkPackagesReconciledResponse markPackagesReconciled(MarkPackagesReconciledRequest request) {
        return paymentReconciliationService.markPackagesReconciled(request);
    }

    /**
     * Updates status to COMPLETE from PENDING_CUSTOMIZATION
     *
     * @param request containing the shipping package code
     * @return response for complete customization request
     */
    @Produces(MediaType.APPLICATION_JSON)
    @Path("markCustomizationComplete")
    @POST
    public CompleteCustomizationForShippingPackageResponse completeCustomizationForShippingPackage(CompleteCustomizationForShippingPackageRequest request) {
        return shippingService.completeCustomizationForShippingPackage(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("shipmentStatusUpdate")
    @POST
    public UpdateShipmentTrackingStatusResponse updateShipmentTrackingStatus(UpdateShipmentTrackingStatusRequest request) {
        return shippingService.updateShipmentTrackingStatus(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("manifest/removeShippingPackage")
    @POST
    public RemoveManifestItemResponse removeManifestItem(RemoveManifestItemRequest request) {
        return dispatchService.removeManifestItem(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("batch/add")
    @POST
    public AddShipmentsToBatchResponse addShipmentsToBatch(AddShipmentsToBatchRequest request) {
        return shippingService.addShipmentsToBatch(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("batch/getPendingBatch")
    @POST
    public GetPendingShipmentBatchResponse getPendingShipmentBatch(GetPendingShipmentBatchRequest request) {
        return shippingService.getPendingShipmentBatch(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("getItemsForDetailing")
    @POST
    public GetItemsForDetailingResponse getItemsForDetailing(GetItemsForDetailingRequest request) {
        return shippingService.getItemsForDetailing(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("getShippingPackageTypes")
    @POST
    public GetShippingPackageTypesResponse getShippingPackageTypes(GetShippingPackageTypesRequest request) {
        return shippingService.getShippingPackageTypes(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("forceDispatch")
    @POST
    public ForceDispatchShippingPackageResponse forceDispatchShippingPackage(ForceDispatchShippingPackageRequest request) {
        request.setUserId(WebContextUtils.getCurrentUser().getUser().getId());
        return dispatchService.forceDispatchShippingPackage(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("createShippingPackage")
    @POST
    public CreateShippingPackageResponse createShippingPackageForSelectedSaleOrderItems(CreateShippingPackageRequest request) {
        return shippingService.createShippingPackageForSelectedSaleOrderItems(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("dispatch")
    @POST
    public AutoDispatchShippingPackageResponse autoDispatchShippingPackage(AutoDispatchShippingPackageRequest request) {
        request.setUserId(WebContextUtils.getCurrentUser().getUser().getId());
        return dispatchService.autoDispatchShippingPackage(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("verifyPOD")
    @POST
    public VerifyShippingPackagePODCodeResponse verifyPOD(VerifyShippingPackagePODCodeRequest request) {
        return dispatchService.verifyShippingPackagePODCode(request);
    }
}
