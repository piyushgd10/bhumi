/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Sep 19, 2012
 *  @author singla
 */
package com.uniware.web.controller.admin;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.uniware.core.api.warehouse.EditPartySequenceRequest;
import com.uniware.core.api.warehouse.EditPartySequenceResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.unifier.web.utils.WebContextUtils;
import com.uniware.core.api.facility.CreateFacilityProfileRequest;
import com.uniware.core.api.facility.CreateFacilityRequest;
import com.uniware.core.api.facility.CreateFacilityResponse;
import com.uniware.core.api.facility.GetSellerDetailsFromTinRequest;
import com.uniware.core.api.facility.GetSellerDetailsFromTinResponse;
import com.uniware.core.api.location.GetLocationForPincodeRequest;
import com.uniware.core.api.location.GetLocationForPincodeResponse;
import com.uniware.core.api.warehouse.EditFacilityRequest;
import com.uniware.core.api.warehouse.EditFacilityResponse;
import com.uniware.core.api.warehouse.GetFacilityRequest;
import com.uniware.core.api.warehouse.GetFacilityResponse;
import com.uniware.services.location.ILocationService;
import com.uniware.services.party.IPartyService;
import com.uniware.services.warehouse.IFacilityService;

@Controller
@Path("/data/admin/system/")
public class FacilityAdminController {

    @Autowired
    private IPartyService    partyService;

    @Autowired
    private IFacilityService facilityService;

    @Autowired
    private ILocationService locationService;

    @Produces(MediaType.APPLICATION_JSON)
    @Path("facility/detail/get")
    @POST
    public GetFacilityResponse facilityJson(GetFacilityRequest request) {
        return facilityService.getFacilityByCode(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("facility/create")
    @POST
    public CreateFacilityResponse createFacility(CreateFacilityRequest req) {
        req.setUsername(WebContextUtils.getCurrentUser().getUsername());
        return facilityService.createFacility(req);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("facility/profile/create")
    @POST
    public CreateFacilityResponse createFacilityProfile(CreateFacilityProfileRequest req) {
        return facilityService.createFacilityProfile(req);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("facility/profile/edit")
    @POST
    public EditFacilityResponse editFacilityProfile(CreateFacilityProfileRequest req) {
        return facilityService.editFacilityProfile(req);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("facility/detail/edit")
    @POST
    public EditFacilityResponse editFacility(EditFacilityRequest req) {
        return facilityService.editFacility(req);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("facility/sequence/edit")
    @POST
    public EditPartySequenceResponse editFacility(EditPartySequenceRequest req) {
        return facilityService.editFacilitySequence(req);
    }
    
    @Produces(MediaType.APPLICATION_JSON)
    @Path("tin/details/get")
    @POST
    public GetSellerDetailsFromTinResponse getTinInformation(GetSellerDetailsFromTinRequest request) {
        return facilityService.getSellerDetailsUsingTin(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("location/pincode")
    @POST
    public GetLocationForPincodeResponse getLocationbyPincode(GetLocationForPincodeRequest request) {
        GetLocationForPincodeResponse response = new GetLocationForPincodeResponse();
        response.setLocation(locationService.getLocationByPincode(request.getPincode()));
        response.setSuccessful(true);
        return response;
    }

}
