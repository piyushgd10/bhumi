/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 06-Feb-2012
 *  @author vibhu
 */
package com.uniware.web.controller.admin;

import com.google.gson.Gson;
import com.unifier.web.json.JElementList;
import com.uniware.core.api.admin.pickset.CreatePickAreaRequest;
import com.uniware.core.api.admin.pickset.CreatePickAreaResponse;
import com.uniware.core.api.admin.pickset.CreatePicksetRequest;
import com.uniware.core.api.admin.pickset.CreatePicksetResponse;
import com.uniware.core.api.admin.pickset.EditPicksetRequest;
import com.uniware.core.api.admin.pickset.EditPicksetResponse;
import com.uniware.core.api.admin.pickset.GetPickSetsByTypeRequest;
import com.uniware.core.api.admin.pickset.GetPickSetsByTypeResponse;
import com.uniware.core.api.admin.pickset.PickAreaDTO;
import com.uniware.core.api.admin.pickset.PicksetDTO;
import com.uniware.core.api.admin.pickset.UpdatePicksetSectionsRequest;
import com.uniware.core.api.admin.pickset.UpdatePicksetSectionsResponse;
import com.uniware.core.api.warehouse.CreateSectionRequest;
import com.uniware.core.api.warehouse.CreateSectionResponse;
import com.uniware.core.api.warehouse.EditSectionRequest;
import com.uniware.core.api.warehouse.EditSectionResponse;
import com.uniware.core.api.warehouse.SectionDTO;
import com.uniware.core.entity.PickSet;
import com.uniware.core.entity.Section;
import com.uniware.services.admin.pickset.IPicksetService;
import com.uniware.services.catalog.ICatalogService;
import com.uniware.services.warehouse.ISectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@Path("/data/admin/picklogic/")
public class PickLogicController {

    @Autowired
    private ISectionService sectionService;

    @Autowired
    private IPicksetService picksetService;

    @Autowired
    private ICatalogService catalogService;

    @RequestMapping("/admin/picklogic/sections")
    public String sections(ModelMap modelMap) {
        return "admin/picklogic/sections";
    }

    @RequestMapping("/admin/picklogic/picksets")
    public String picksets(ModelMap modelMap) {
        return "admin/picklogic/picksets";
    }

    @RequestMapping("/admin/picklogic/picksetToSections")
    public String picksetToSections(ModelMap modelMap) {
        Map<Integer, PicksetDTO> picksetMap = new HashMap<>();
        Map<Integer, PickAreaDTO> pickAreaMap = new HashMap<>();

        PicksetDTO picksetDTO;
        for (PickSet pickset : picksetService.getPicksets()) {
            picksetDTO = new PicksetDTO(pickset);
            List<Integer> sectionIds = new ArrayList<>(pickset.getSections().size());
            for (Section section : pickset.getSections()) {
                sectionIds.add(section.getId());
            }
            picksetDTO.setSectionIds(sectionIds);
            picksetMap.put(picksetDTO.getId(), picksetDTO);
        }
        Map<Integer, SectionDTO> sectionMap = new HashMap<>();
        for (Section section : sectionService.getSections()) {
            sectionMap.put(section.getId(), new SectionDTO(section));
        }
        picksetService.getPickAreas().stream().map(PickAreaDTO::new).forEach(dto -> pickAreaMap.put(dto.getId(), dto));
        modelMap.addAttribute("pickAreas", new Gson().toJson(pickAreaMap));
        modelMap.addAttribute("picksets", new Gson().toJson(picksetMap));
        modelMap.addAttribute("sections", new Gson().toJson(sectionMap));
        return "admin/picklogic/picksetToSections";
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("section/create")
    @POST
    public CreateSectionResponse createSection(CreateSectionRequest createSectionRequest) {
        CreateSectionResponse response = sectionService.createSection(createSectionRequest);
        return response;
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("section/edit")
    @POST
    public EditSectionResponse editSection(EditSectionRequest editSectionRequest) {
        EditSectionResponse response = sectionService.editSection(editSectionRequest);
        return response;
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("sections/get")
    @GET
    public JElementList<SectionDTO> sections() {
        List<Section> sections = sectionService.getSections();
        JElementList<SectionDTO> elements = new JElementList<>();
        for (Section section : sections) {
            elements.addElement(new SectionDTO(section));
        }
        return elements;
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("pickset/create")
    @POST
    public CreatePicksetResponse createPickset(CreatePicksetRequest createPicksetRequest) {
        CreatePicksetResponse response = picksetService.createPickset(createPicksetRequest);
        return response;
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("pickset/edit")
    @POST
    public EditPicksetResponse editPickset(EditPicksetRequest editPicksetRequest) {
        EditPicksetResponse response = picksetService.editPickset(editPicksetRequest);
        return response;
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("picksetsections/update")
    @POST
    public UpdatePicksetSectionsResponse updatePicksetSections(UpdatePicksetSectionsRequest updatePicksetSectionsRequest) {
        UpdatePicksetSectionsResponse response = picksetService.updatePicksetSections(updatePicksetSectionsRequest);
        return response;
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("pickarea/create")
    @POST
    public CreatePickAreaResponse createPickArea(CreatePickAreaRequest createPicksetRequest) {
        return picksetService.createPickArea(createPicksetRequest);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("picksets/get")
    @POST
    public GetPickSetsByTypeResponse getStagingPicksets(GetPickSetsByTypeRequest request) {
        return picksetService.getPickSetDTOsByType(request);
    }
}
