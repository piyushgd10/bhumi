/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jan 12, 2012
 *  @author singla
 */
package com.uniware.web.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.unifier.core.api.validation.ValidationContext;
import com.uniware.core.api.saleorder.*;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.entity.SaleOrder;
import com.uniware.services.saleorder.ISaleOrderProcessingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.Gson;
import com.unifier.core.cache.CacheManager;
import com.unifier.web.utils.WebContextUtils;
import com.uniware.core.api.catalog.LookupSaleOrderLineItemsRequest;
import com.uniware.core.api.catalog.LookupSaleOrderLineItemsResponse;
import com.uniware.core.api.channel.GetShippingPackageTaxSummaryRequest;
import com.uniware.core.api.channel.GetShippingPackageTaxSummaryResponse;
import com.uniware.core.api.returns.MarkSaleOrderReturnedRequest;
import com.uniware.core.api.returns.MarkSaleOrderReturnedResponse;
import com.uniware.core.api.saleorder.display.GetSaleOrderActivitiesRequest;
import com.uniware.core.api.saleorder.display.GetSaleOrderActivitiesResponse;
import com.uniware.core.api.saleorder.display.GetSaleOrderInvoicesRequest;
import com.uniware.core.api.saleorder.display.GetSaleOrderInvoicesResponse;
import com.uniware.core.api.saleorder.display.GetSaleOrderLineItemDetailsRequest;
import com.uniware.core.api.saleorder.display.GetSaleOrderLineItemDetailsResponse;
import com.uniware.core.api.saleorder.display.GetSaleOrderLineItemsRequest;
import com.uniware.core.api.saleorder.display.GetSaleOrderLineItemsResponse;
import com.uniware.core.api.saleorder.display.GetSaleOrderPriceSummaryRequest;
import com.uniware.core.api.saleorder.display.GetSaleOrderPriceSummaryResponse;
import com.uniware.core.api.saleorder.display.GetSaleOrderReturnsRequest;
import com.uniware.core.api.saleorder.display.GetSaleOrderReturnsResponse;
import com.uniware.core.api.saleorder.display.GetSaleOrderShippingPackagesRequest;
import com.uniware.core.api.saleorder.display.GetSaleOrderShippingPackagesResponse;
import com.uniware.core.api.saleorder.display.GetSaleOrderSummaryRequest;
import com.uniware.core.api.saleorder.display.GetSaleOrderSummaryResponse;
import com.uniware.core.cache.FacilityCache;
import com.uniware.core.utils.UserContext;
import com.uniware.services.channel.saleorder.IChannelOrderSyncService;
import com.uniware.services.returns.IReturnsService;
import com.uniware.services.saleorder.ISaleOrderDisplayService;
import com.uniware.services.saleorder.ISaleOrderService;

/**
 * @author singla
 */
@Controller
@Path("/data/oms/saleorder/")
public class SaleOrderResource {

    @Autowired
    private ISaleOrderService        saleOrderService;

    @Autowired
    private ISaleOrderDisplayService saleOrderDisplayService;

    @Autowired
    private IReturnsService          returnsService;

    @Autowired
    private IChannelOrderSyncService channelOrderSyncService;

    @Autowired
    private ISaleOrderProcessingService saleOrderProcessingService;

    @Produces(MediaType.APPLICATION_JSON)
    @Path("fetch")
    @POST
    public GetSaleOrderResponse getSaleOrder(GetSaleOrderRequest getOrderRequest) {
        if (UserContext.current().getFacilityId() == null) {
            getOrderRequest.setFacilityCodes(CacheManager.getInstance().getCache(FacilityCache.class).getFacilityCodes());
        } else {
            List<String> facilityCodes = new ArrayList<String>();
            facilityCodes.add(CacheManager.getInstance().getCache(FacilityCache.class).getCurrentFacility().getCode());
            getOrderRequest.setFacilityCodes(facilityCodes);
        }
        return saleOrderService.getSaleOrder(getOrderRequest);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("fetchSummary")
    @POST
    public GetSaleOrderSummaryResponse getSaleOrderSummary(GetSaleOrderSummaryRequest request) {
        return saleOrderDisplayService.getSaleOrderSummary(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("fetchPriceSummary")
    @POST
    public GetSaleOrderPriceSummaryResponse getSaleOrderPriceSummary(GetSaleOrderPriceSummaryRequest request) {
        return saleOrderDisplayService.getSaleOrderPriceSummary(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("fetchLineItems")
    @POST
    public GetSaleOrderLineItemsResponse getSaleOrderLineItems(GetSaleOrderLineItemsRequest request) {
        return saleOrderDisplayService.getSaleOrderLineItems(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("fetchLineItemDetails")
    @POST
    public GetSaleOrderLineItemDetailsResponse getSaleOrderLineItemDetails(GetSaleOrderLineItemDetailsRequest request) {
        return saleOrderDisplayService.getSaleOrderLineItemDetails(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("fetchInvoiceDetails")
    @POST
    public GetSaleOrderInvoicesResponse getSaleOrderInvoiceDetails(GetSaleOrderInvoicesRequest request) {
        return saleOrderDisplayService.getSaleOrderInvoices(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("fetchShippingPackageDetails")
    @POST
    public GetSaleOrderShippingPackagesResponse getSaleOrderShippingPackageDetails(GetSaleOrderShippingPackagesRequest request) {
        return saleOrderDisplayService.getSaleOrderShippingPackages(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("fetchShippingPackageTaxSummary")
    @POST
    public GetShippingPackageTaxSummaryResponse getShippingPackageTaxSummary(GetShippingPackageTaxSummaryRequest request) {
        return saleOrderDisplayService.getShippingPackageTaxSummary(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("fetchReturns")
    @POST
    public GetSaleOrderReturnsResponse getSaleOrderReturns(GetSaleOrderReturnsRequest request) {
        return saleOrderDisplayService.getSaleOrderReturns(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("fetchActivities")
    @POST
    public GetSaleOrderActivitiesResponse getSaleOrderActivities(GetSaleOrderActivitiesRequest request) {
        return saleOrderDisplayService.getSaleOrderActivities(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("hold")
    @POST
    public HoldSaleOrderItemsResponse holdSaleOrderItems(HoldSaleOrderItemsRequest request) {
        return saleOrderService.holdSaleOrderItems(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("unhold")
    @POST
    public UnholdSaleOrderItemsResponse unholdSaleOrderItems(UnholdSaleOrderItemsRequest request) {
        return saleOrderService.unholdSaleOrderItems(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("cancel")
    @POST
    public CancelSaleOrderResponse cancelSaleOrderItems(CancelSaleOrderRequest request) {
        return saleOrderService.cancelSaleOrder(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("releaseInventory")
    @POST
    public UnblockSaleOrderItemsInventoryResponse unblockSaleOrderItemsInventory(UnblockSaleOrderItemsInventoryRequest request) {
        return saleOrderService.unblockSaleOrderItemsInventory(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("change/packet")
    @POST
    public ModifyPacketSaleOrderResponse modifySaleOrderItems(ModifyPacketSaleOrderRequest request) {
        return saleOrderService.modifySaleOrderItems(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("addresses/edit")
    @POST
    public EditSaleOrderAddressesResponse editSaleOrderAddresses(EditSaleOrderAddressesRequest request) {
        return saleOrderService.editSaleOrderAddresses(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("metadata/edit")
    @POST
    public EditSaleOrderMetadataResponse editSaleOrderMetadata(EditSaleOrderMetadataRequest request) {
        return saleOrderService.editSaleOrderMetadata(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("alternate/get")
    @POST
    public GetSaleOrderItemAlternateResponse getSaleOrderItemAlternateRequest(GetSaleOrderItemAlternateRequest request) {
        return saleOrderService.getSaleOrderItemAlternateRequest(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("alternate/getOrder")
    @POST
    public GetSaleOrderAlternateResponse getSaleOrderAlternate(GetSaleOrderAlternateRequest request) {
        return saleOrderService.getSaleOrderAlternate(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("alternate/accept")
    @POST
    public AcceptSaleOrderItemAlternateResponse acceptSaleOrderItemAlternate(AcceptSaleOrderItemAlternateRequest request) {
        return saleOrderService.acceptSaleOrderItemAlternate(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("alternate/acceptOrder")
    @POST
    public AcceptSaleOrderAlternateResponse acceptSaleOrderAlternate(AcceptSaleOrderAlternateRequest request) {
        return saleOrderService.acceptSaleOrderAlternate(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("priority/edit")
    @POST
    public SetSaleOrderPriorityResponse setSaleOrderPriority(SetSaleOrderPriorityRequest request) {
        return saleOrderService.setSaleOrderPriority(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("facility/switch")
    @POST
    public SwitchSaleOrderItemFacilityResponse switchSaleOrderItemFacility(SwitchSaleOrderItemFacilityRequest request) {
        return saleOrderService.switchSaleOrderItemFacility(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("verify")
    @POST
    public VerifySaleOrdersResponse verifySaleOrders(VerifySaleOrdersRequest request) {
        return saleOrderService.verifySaleOrders(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("create")
    @POST
    public CreateSaleOrderResponse createSaleOrder(CreateSaleOrderRequest request) {
        return saleOrderService.createSaleOrder(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("splitLogisticCharges")
    @POST
    public AddOrEditSaleOrderItemsResponse divideSOItemsLogisticCharges(AddOrEditSaleOrderItemsRequest request) {
        return saleOrderService.divideSOItemsLogisticCharges(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("deleteFailedOrders")
    @POST
    public DeleteFailedSaleOrdersResponse deleteAllFailedOrders(DeleteFailedSaleOrdersRequest request) {
        return saleOrderService.removeAllFailedSaleOrders(request);
    }

    @POST
    @Path("delete")
    @Produces(MediaType.APPLICATION_JSON)
    public DeleteSaleOrderResponse deleteSaleOrders(DeleteSaleOrderRequest request) {
        return saleOrderService.deleteSaleOrder(request);
    }

    @POST
    @Path("markReturned")
    @Produces(MediaType.APPLICATION_JSON)
    public MarkSaleOrderReturnedResponse markSaleOrderReturned(MarkSaleOrderReturnedRequest request) {
        request.setUserId(WebContextUtils.getCurrentUser().getUser().getId());
        return returnsService.markSaleOrderReturned(request);
    }

    @POST
    @Path("getErrors")
    @Produces(MediaType.APPLICATION_JSON)
    public GetErrorsForFailedOrdersResponse getErrorsFromFailedOrders(GetErrorsForFailedOrdersRequest request) {
        return saleOrderService.getErrorsFromSaleOrderVOs(request);
    }

    @POST
    @Path("fixAddressErrors")
    @Produces(MediaType.APPLICATION_JSON)
    public FixAddressErrorInFailedOrderResponse fixAddressErrors(FixAddressErrorInFailedOrderRequest request) {
        return saleOrderService.fixAddressError(request);
    }

    @POST
    @Path("generateNext")
    @Produces(MediaType.APPLICATION_JSON)
    public GenerateSaleOrderNextSequenceResponse generateNext(GenerateSaleOrderNextSequenceRequest request) {
        return saleOrderService.generateNext(request);
    }

    @RequestMapping("/oms/saleorder/lookupSaleOrderLineItems/sampleCSV")
    public void getImportCSVFormat(HttpServletResponse response) throws IOException {
        response.setContentType("application/csv");
        response.addHeader("content-disposition", "attachment; filename=\"lookupSaleOrderLineItems.csv\"");
        response.getOutputStream().write("Item SKU,Quantity,Selling Price".getBytes());
    }

    @RequestMapping("/data/oms/saleorder/lookupSaleOrderLineItems")
    @ResponseBody
    public String lookupSaleOrderLineItems(@RequestParam("file") MultipartFile file) throws IOException {
        LookupSaleOrderLineItemsRequest request = new LookupSaleOrderLineItemsRequest();
        request.setInputStream(file.getInputStream());
        LookupSaleOrderLineItemsResponse response = saleOrderService.lookupSaleOrderLineItems(request);
        return new Gson().toJson(response);
    }

    @POST
    @Path("refresh")
    @Produces(MediaType.APPLICATION_JSON)
    public RefreshSaleOrderResponse refreshSaleOrder(RefreshSaleOrderRequest request) {
        return channelOrderSyncService.refreshSaleOrder(request);
    }

    @POST
    @Path("markComplete")
    @Produces(MediaType.APPLICATION_JSON)
    public MarkSaleOrderItemsCompleteResponse markSaleOrderComplete(MarkSaleOrderItemsCompleteRequest request) {
        request.setUserId(WebContextUtils.getCurrentUser().getUser().getId());
        return saleOrderService.markSaleOrderItemsComplete(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("markUnfulfillable")
    @POST
    public MarkSaleOrderItemsUnfulfillableResponse markSaleOrderItemsUnfulfillable(MarkSaleOrderItemsUnfulfillableRequest request) {
        return saleOrderService.markSaleOrderItemsUnfulfillable(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("cancellationReasons")
    @POST
    public GetCancellationReasonsResponse getCancellationReasons(GetCancellationReasonsRequest request){
        return saleOrderService.getCancellationReasons(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("returnReasons")
    @POST
    public GetReturnReasonsResponse getReturnReasons(GetReturnReasonsRequest request){
        return saleOrderService.getReturnReasons(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/allocate/inventory")
    @POST
    public AllocateInventoryCreatePackageResponse allocateInventoryAndCreatePackage(AllocateInventoryRequest request) {
        AllocateInventoryCreatePackageResponse response = new AllocateInventoryCreatePackageResponse();
        ValidationContext context = request.validate();
        SaleOrder saleOrder = saleOrderService.getSaleOrderByCode(request.getSaleOrderCode());
        if (null == saleOrder) {
            context.addError(WsResponseCode.INVALID_SALE_ORDER_CODE, "Invalid sale order code");
        } else if (!SaleOrder.StatusCode.CREATED.name().equals(saleOrder.getStatusCode())) {
            context.addError(WsResponseCode.INVALID_SALE_ORDER_STATE, "Sale order not in CREATED state");
        } else {
            AllocateInventoryCreatePackageRequest allocateInventory = new AllocateInventoryCreatePackageRequest();
            allocateInventory.setSaleOrderId(saleOrder.getId());
            response = saleOrderProcessingService.allocateInventoryAndCreatePackage(allocateInventory);
            if (!response.isSuccessful()) {
                context.addErrors(response.getErrors());
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        response.setSuccessful(!context.hasErrors());
        return response;
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("allocateInventoryAndGenerateInvoice")
    @POST
    public AllocateInventoryAndGenerateInvoiceResponse allocateInventoryAndGenerateInvoice(AllocateInventoryAndGenerateInvoiceRequest request) {
        request.setUserId(WebContextUtils.getCurrentUser().getUser().getId());
        return saleOrderProcessingService.allocateInventoryAndGenerateInvoice(request);
    }
}
