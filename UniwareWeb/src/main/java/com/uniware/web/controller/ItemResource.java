/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 *  @version     1.0, 15-May-2012
 *  @author vibhu
 */
package com.uniware.web.controller;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.uniware.core.api.item.GetAllFacilityItemDetailRequest;
import com.uniware.core.api.item.GetAllFacilityItemDetailResponse;
import com.uniware.core.api.item.GetItemDetailRequest;
import com.uniware.core.api.item.GetItemDetailResponse;
import com.uniware.services.item.IItemService;

@Controller
@Path("/data/item/")
public class ItemResource {

    @Autowired
    IItemService itemService;

    @Produces(MediaType.APPLICATION_JSON)
    @Path("fetch")
    @POST
    public GetItemDetailResponse getItemDetail(GetItemDetailRequest request) {
        return itemService.getItemDetail(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("allFacility/fetch")
    @POST
    public GetAllFacilityItemDetailResponse getAllFacilityItemDetail(GetAllFacilityItemDetailRequest request) {
        return itemService.getItemDetailAcrossFacility(request);
    }

}