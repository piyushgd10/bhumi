/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 04-Aug-2012
 *  @author praveeng
 */
package com.uniware.web.controller.admin;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.google.gson.Gson;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.web.json.JElementList;
import com.uniware.core.api.admin.shipping.AddAwbNumberRequest;
import com.uniware.core.api.admin.shipping.AddAwbNumberResponse;
import com.uniware.core.api.admin.shipping.AddShippingProviderMethodsRequest;
import com.uniware.core.api.admin.shipping.AddShippingProviderMethodsResponse;
import com.uniware.core.api.admin.shipping.CreateProviderAllocationRuleRequest;
import com.uniware.core.api.admin.shipping.CreateProviderAllocationRuleResponse;
import com.uniware.core.api.admin.shipping.DeleteAwbNumberRequest;
import com.uniware.core.api.admin.shipping.DeleteAwbNumberResponse;
import com.uniware.core.api.admin.shipping.DeleteProviderAllocationRuleRequest;
import com.uniware.core.api.admin.shipping.DeleteProviderAllocationRuleResponse;
import com.uniware.core.api.admin.shipping.EditProviderAllocationRuleRequest;
import com.uniware.core.api.admin.shipping.EditProviderAllocationRuleResponse;
import com.uniware.core.api.admin.shipping.EditShippingProviderMethodRequest;
import com.uniware.core.api.admin.shipping.EditShippingProviderMethodResponse;
import com.uniware.core.api.admin.shipping.EditShippingServiceabilityRequest;
import com.uniware.core.api.admin.shipping.EditShippingServiceabilityResposne;
import com.uniware.core.api.admin.shipping.GetAllShippingProviderResponse;
import com.uniware.core.api.admin.shipping.GetShippingProviderRequest;
import com.uniware.core.api.admin.shipping.GetShippingProviderResponse;
import com.uniware.core.api.admin.shipping.GetShippingProvidersResponse;
import com.uniware.core.api.admin.shipping.ReorderProviderAllocationRulesRequest;
import com.uniware.core.api.admin.shipping.ReorderProviderAllocationRulesResponse;
import com.uniware.core.api.admin.shipping.SearchAwbNumberRequest;
import com.uniware.core.api.admin.shipping.SearchAwbNumberResponse;
import com.uniware.core.api.admin.shipping.ShippingProviderDetailDTO;
import com.uniware.core.api.admin.shipping.ViewAwbNumberRequest;
import com.uniware.core.api.admin.shipping.ViewAwbNumberResponse;
import com.uniware.core.api.shipping.AddShippingProviderConnectorRequest;
import com.uniware.core.api.shipping.AddShippingProviderConnectorResponse;
import com.uniware.core.api.shipping.AddShippingProviderRequest;
import com.uniware.core.api.shipping.AddShippingProviderResponse;
import com.uniware.core.api.shipping.EditShippingProviderRequest;
import com.uniware.core.api.shipping.EditShippingProviderResponse;
import com.uniware.core.api.shipping.GetShippingProviderSyncStatusRequest;
import com.uniware.core.api.shipping.GetShippingProviderSyncStatusResponse;
import com.uniware.core.api.shipping.SyncShipmentTrackingStatusRequest;
import com.uniware.core.api.shipping.SyncShipmentTrackingStatusResponse;
import com.uniware.core.api.shipping.SyncTrackingStatusForAWBRequest;
import com.uniware.core.api.shipping.SyncTrackingStatusForAWBResponse;
import com.uniware.core.api.warehouse.CreateShippingPackageTypeRequest;
import com.uniware.core.api.warehouse.CreateShippingPackageTypeResponse;
import com.uniware.core.api.warehouse.EditShippingPackageTypeRequest;
import com.uniware.core.api.warehouse.EditShippingPackageTypeResponse;
import com.uniware.core.api.warehouse.ShippingPackageTypeDTO;
import com.uniware.core.entity.ShippingPackageType;
import com.uniware.services.configuration.ShippingConfiguration;
import com.uniware.services.shipping.IShipmentTrackingService;
import com.uniware.services.shipping.IShippingAdminService;

@Controller
@Path("/data/admin/shipping/")
public class ShippingAdminResource {

    @Autowired
    private IShippingAdminService shippingAdminService;

    @Autowired
    private IShipmentTrackingService shipmentTrackingService;

    @Produces(MediaType.APPLICATION_JSON)
    @Path("provider/get")
    @POST
    public GetShippingProviderResponse provider(GetShippingProviderRequest request) {
        return shippingAdminService.getShippingProviderByCode(request);
    }
    
    @Produces(MediaType.APPLICATION_JSON)
    @Path("providers/get")
    @GET
    public GetAllShippingProviderResponse providers() {
        List<ShippingProviderDetailDTO> providers = shippingAdminService.getAllShippingProvidersDetailDTO();
        GetAllShippingProviderResponse response = new GetAllShippingProviderResponse();
        response.setShippingProviders(providers);
        response.setSuccessful(true);
        return response;
    }
    
    @Produces(MediaType.APPLICATION_JSON)
    @Path("get/allProviders")
    @GET
    public GetShippingProvidersResponse getAllProviders() {
        return shippingAdminService.getAllShippingProviders();
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("get/trackingEnabledShippingProviders")
    @GET
    public String getAllTrackingEnabledShippingProviderCodes() {
        return new Gson().toJson(ConfigurationManager.getInstance().getConfiguration(ShippingConfiguration.class).getTrackingEnabledShippingProviderCodes());
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("add/awb")
    @POST
    public AddAwbNumberResponse addAwbNumbers(AddAwbNumberRequest request) {
        AddAwbNumberResponse response = shippingAdminService.addAwbNumbers(request);
        return response;
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("search/awb")
    @POST
    public SearchAwbNumberResponse searchAwbNumbers(SearchAwbNumberRequest request) {
        SearchAwbNumberResponse response = shippingAdminService.searchAwbNumbers(request);
        return response;
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("viewExisting/awb")
    @POST
    public ViewAwbNumberResponse ViewAwbNumbers(ViewAwbNumberRequest request) {
        return shippingAdminService.viewExistingAwbNumbers(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("delete/awb")
    @POST
    public DeleteAwbNumberResponse deleteAwbNumbers(DeleteAwbNumberRequest request) {
        return shippingAdminService.deleteAwbNumbers(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("provider/edit")
    @POST
    public EditShippingProviderResponse editShippingProvider(EditShippingProviderRequest request) {
        EditShippingProviderResponse response = shippingAdminService.updateShippingProvider(request);
        return response;
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("provider/add")
    @POST
    public AddShippingProviderResponse addShippingProvider(AddShippingProviderRequest request) {
        return shippingAdminService.addShippingProvider(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("spackagetypes/get")
    @GET
    public JElementList<ShippingPackageTypeDTO> sPackageTypes() {
        List<ShippingPackageType> sPackageTypes = shippingAdminService.getShippingPackageTypes();
        JElementList<ShippingPackageTypeDTO> elements = new JElementList<ShippingPackageTypeDTO>();
        for (ShippingPackageType sPackageType : sPackageTypes) {
            elements.addElement(new ShippingPackageTypeDTO(sPackageType));
        }
        return elements;
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("spackagetype/create")
    @POST
    public CreateShippingPackageTypeResponse createShippingPackageType(CreateShippingPackageTypeRequest request) {
        CreateShippingPackageTypeResponse response = shippingAdminService.createShippingPackageType(request);
        return response;
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("spackagetype/edit")
    @POST
    public EditShippingPackageTypeResponse editShippingPackageType(EditShippingPackageTypeRequest editShippingPackageTypeRequest) {
        EditShippingPackageTypeResponse response = shippingAdminService.editShippingPackageType(editShippingPackageTypeRequest);
        return response;
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("provider/method/edit")
    @POST
    public EditShippingProviderMethodResponse editShippingProviderMethod(EditShippingProviderMethodRequest request) {
        EditShippingProviderMethodResponse response = shippingAdminService.editShippingProviderMethod(request);
        return response;
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("provider/method/add")
    @POST
    public AddShippingProviderMethodsResponse addShippingProviderMethod(AddShippingProviderMethodsRequest request) {
        return shippingAdminService.addShippingProviderMethods(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("provider/connector/add")
    @POST
    public AddShippingProviderConnectorResponse addShippingProviderConnector(AddShippingProviderConnectorRequest request) {
        return shippingAdminService.addShippingProviderConnector(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("provider/serviceability/edit")
    @POST
    public EditShippingServiceabilityResposne editShippingProviderServiceability(EditShippingServiceabilityRequest request) {
        return shippingAdminService.editShippingProviderServiceability(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("provider/allocationRule/create")
    @POST
    public CreateProviderAllocationRuleResponse createProviderAllocationRule(CreateProviderAllocationRuleRequest request) {
        return shippingAdminService.createProviderAllocationRule(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("provider/allocationRule/edit")
    @POST
    public EditProviderAllocationRuleResponse editProviderAllocationRule(EditProviderAllocationRuleRequest request) {
        return shippingAdminService.editProviderAllocationRule(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("provider/allocationRule/delete")
    @POST
    public DeleteProviderAllocationRuleResponse deleteProviderAllocationRule(DeleteProviderAllocationRuleRequest request) {
        return shippingAdminService.deleteProviderAllocationRule(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("provider/allocationRules/reorder")
    @POST
    public ReorderProviderAllocationRulesResponse reorderProviderAllocationRules(ReorderProviderAllocationRulesRequest request) {
        return shippingAdminService.reorderProviderAllocationRules(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("syncShipmentsTrackingStatus")
    @POST
    public SyncShipmentTrackingStatusResponse syncShippingProviderTrackingStatus(SyncShipmentTrackingStatusRequest request) {
        return shipmentTrackingService.syncTrackingStatuses(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("syncStatusForProvider")
    @POST
    public GetShippingProviderSyncStatusResponse getSyncStatusForProvider(GetShippingProviderSyncStatusRequest request) {
        GetShippingProviderSyncStatusResponse response = new GetShippingProviderSyncStatusResponse();
        response.setStatusDTO(shipmentTrackingService.getTrackingSyncStatusByProvider(request.getShippingProviderCode()));
        response.setSuccessful(true);
        return response;
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("syncTrackingStatusForAWB")
    @POST
    public SyncTrackingStatusForAWBResponse syncShippingProviderTrackingStatus(SyncTrackingStatusForAWBRequest request) {
        return shipmentTrackingService.syncStatusForAWB(request);
    }
}
