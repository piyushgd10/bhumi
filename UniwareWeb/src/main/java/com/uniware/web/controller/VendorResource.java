/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 11-May-2012
 *  @author praveeng
 */
package com.uniware.web.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.unifier.core.api.validation.WsError;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.entity.User;
import com.unifier.core.template.Template;
import com.unifier.services.pdf.IPdfDocumentService;
import com.unifier.services.pdf.impl.PdfDocumentServiceImpl.PrintOptions;
import com.unifier.web.utils.WebContextUtils;
import com.uniware.core.api.inflow.GetInflowReceiptRequest;
import com.uniware.core.api.inflow.GetInflowReceiptResponse;
import com.uniware.core.api.inflow.SearchReceiptRequest;
import com.uniware.core.api.inflow.SearchReceiptResponse;
import com.uniware.core.api.material.GetGatePassRequest;
import com.uniware.core.api.material.GetGatePassResponse;
import com.uniware.core.api.material.SearchGatePassRequest;
import com.uniware.core.api.material.SearchGatePassResponse;
import com.uniware.core.api.party.SearchVendorItemTypesRequest;
import com.uniware.core.api.party.SearchVendorItemTypesResponse;
import com.uniware.core.api.purchase.AdvanceShippingNoticeDTO;
import com.uniware.core.api.purchase.CreateASNRequest;
import com.uniware.core.api.purchase.CreateASNResponse;
import com.uniware.core.api.purchase.EditAdvanceShippingNoticeRequest;
import com.uniware.core.api.purchase.EditAdvanceShippingNoticeResponse;
import com.uniware.core.api.purchase.GetBackOrderItemsRequest;
import com.uniware.core.api.purchase.GetBackOrderItemsResponse;
import com.uniware.core.api.purchase.GetPurchaseOrderRequest;
import com.uniware.core.api.purchase.GetPurchaseOrderResponse;
import com.uniware.core.api.purchase.SearchASNRequest;
import com.uniware.core.api.purchase.SearchASNResponse;
import com.uniware.core.api.purchase.SearchPurchaseOrderRequest;
import com.uniware.core.api.purchase.SearchPurchaseOrderResponse;
import com.uniware.core.entity.OutboundGatePass;
import com.uniware.core.entity.PurchaseOrder;
import com.uniware.core.entity.Vendor;
import com.uniware.core.vo.PrintTemplateVO;
import com.uniware.core.vo.SamplePrintTemplateVO;
import com.uniware.services.cache.PrintTemplateCache;
import com.uniware.services.cache.SamplePrintTemplateCache;
import com.uniware.services.inflow.IInflowService;
import com.uniware.services.material.IMaterialService;
import com.uniware.services.purchase.IPurchaseService;
import com.uniware.services.vendor.IVendorService;

@Controller
@Path("/data/vendor/")
public class VendorResource {

    @Autowired
    private IPurchaseService    purchaseService;

    @Autowired
    private IVendorService      vendorService;

    @Autowired
    private IInflowService      inflowService;

    @Autowired
    IMaterialService            materialService;

    @Autowired
    private IPdfDocumentService pdfDocumentService;

    @Produces(MediaType.APPLICATION_JSON)
    @Path("asn/create")
    @POST
    public CreateASNResponse createASN(CreateASNRequest request) {
        User user = WebContextUtils.getCurrentUser().getUser();
        request.setVendorCode(user.getVendor().getCode());
        return purchaseService.createASN(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("asn/search")
    @POST
    public SearchASNResponse searchASN(SearchASNRequest request) {
        User user = WebContextUtils.getCurrentUser().getUser();
        request.setVendorId(user.getVendor().getId());
        return purchaseService.searchASN(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("asn/metadata/edit")
    @POST
    public EditAdvanceShippingNoticeResponse searchASN(EditAdvanceShippingNoticeRequest request) {
        EditAdvanceShippingNoticeResponse response = new EditAdvanceShippingNoticeResponse();
        User user = WebContextUtils.getCurrentUser().getUser();
        AdvanceShippingNoticeDTO asn = purchaseService.getASN(request.getAdvanceShippingNoticeCode());
        if (!asn.getVendorId().equals(user.getVendor().getId())) {
            response = new EditAdvanceShippingNoticeResponse();
            response.addError(new WsError("Invalid ASN Code"));
            return response;
        }
        response = purchaseService.editAdvanceShippingNoticeMetadata(request);
        return response;
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("asn/view")
    @GET
    public AdvanceShippingNoticeDTO getASN(@QueryParam("asnCode") String asnCode) {
        return purchaseService.getASN(asnCode);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("po/search")
    @POST
    public SearchPurchaseOrderResponse searchPurchaseOrders(SearchPurchaseOrderRequest request) {
        User user = WebContextUtils.getCurrentUser().getUser();
        request.setVendorId(user.getVendor().getId());
        return purchaseService.searchPurchaseOrders(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("po/fetch")
    @POST
    public GetPurchaseOrderResponse getPurchaseOrder(GetPurchaseOrderRequest request) {
        GetPurchaseOrderResponse response = purchaseService.getPurchaseOrderDetail(request);
        if (response.isSuccessful()) {
            User user = WebContextUtils.getCurrentUser().getUser();
            Vendor vendor = vendorService.getVendorByUserId(user.getId());
            if (!vendor.getCode().equals(response.getVendorCode())) {
                response = new GetPurchaseOrderResponse();
                response.addError(new WsError("Invalid Purchase Order Code"));
                return response;
            }
        }
        return response;
    }

    @RequestMapping("/vendor/po/show")
    public void printPurchaseOrder(@RequestParam(value = "code", required = true) String purchaseOrderCode, HttpServletResponse response) throws IOException {
        PurchaseOrder purchaseOrder = purchaseService.getPurchaseOrderDetailedByCode(purchaseOrderCode);
        User user = WebContextUtils.getCurrentUser().getUser();
        Vendor vendor = vendorService.getVendorByUserId(user.getId());
        if (purchaseOrder != null && purchaseOrder.getVendor().getId().equals(vendor.getId())) {
            response.setContentType("application/pdf");
            Template template = CacheManager.getInstance().getCache(PrintTemplateCache.class).getTemplateByType(PrintTemplateVO.Type.PURCHASE_ORDER.name());
            PrintTemplateVO purchaseOrderTemplate = CacheManager.getInstance().getCache(PrintTemplateCache.class).getPrintTemplateByType(PrintTemplateVO.Type.PURCHASE_ORDER);
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("purchaseOrder", purchaseOrder);
            String purchaseOrderHtml = template.evaluate(params);
            SamplePrintTemplateVO samplePrintTemplate = CacheManager.getInstance().getCache(SamplePrintTemplateCache.class).getSamplePrintTemplate(purchaseOrderTemplate.getSamplePrintTemplateCode());
            pdfDocumentService.writeHtmlToPdf(response.getOutputStream(), purchaseOrderHtml, new PrintOptions(samplePrintTemplate));
        }
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("receipt/search")
    @POST
    public SearchReceiptResponse searchReceipt(SearchReceiptRequest request) {
        User user = WebContextUtils.getCurrentUser().getUser();
        request.setVendorId(user.getVendor().getId());
        return inflowService.searchReceipt(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("receipt/fetch")
    @POST
    public GetInflowReceiptResponse getPurchaseOrderToReceive(GetInflowReceiptRequest request) {
        GetInflowReceiptResponse response = inflowService.getInflowReceipt(request);
        if (response.isSuccessful()) {
            User user = WebContextUtils.getCurrentUser().getUser();
            Vendor vendor = vendorService.getVendorByUserId(user.getId());
            if (!vendor.getCode().equals(response.getVendorCode())) {
                response = new GetInflowReceiptResponse();
                response.addError(new WsError("Invalid GRN Code"));
                return response;
            }
        }
        return response;
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("gatepass/search")
    @POST
    public SearchGatePassResponse searchGatePass(SearchGatePassRequest request) {
        User user = WebContextUtils.getCurrentUser().getUser();
        request.setVendorId(user.getVendor().getId());
        return materialService.searchGatePass(request);
    }

    @RequestMapping("/vendor/gatepass/print/{gatePassCode}")
    public void printGatePass(@PathVariable("gatePassCode") String gatePassCode, HttpServletResponse response) throws IOException {
        Template template = CacheManager.getInstance().getCache(PrintTemplateCache.class).getTemplateByType(PrintTemplateVO.Type.OUTBOUND_GATE_PASS.name());
        String gatePassHtml = materialService.getGatePassHtml(gatePassCode, template);
        OutboundGatePass gatePass = materialService.getGatePassByCode(gatePassCode, false);
        Integer vendorId = WebContextUtils.getCurrentUser().getUser().getVendor().getId();
        if (gatePassHtml != null && gatePass.getToParty().getId().equals(vendorId)) {
            response.setContentType("application/pdf");
            PrintTemplateVO gatePassTemplate = CacheManager.getInstance().getCache(PrintTemplateCache.class).getPrintTemplateByType(PrintTemplateVO.Type.OUTBOUND_GATE_PASS);
            SamplePrintTemplateVO samplePrintTemplate = CacheManager.getInstance().getCache(SamplePrintTemplateCache.class).getSamplePrintTemplate(gatePassTemplate.getSamplePrintTemplateCode());
            pdfDocumentService.writeHtmlToPdf(response.getOutputStream(), gatePassHtml, new PrintOptions(samplePrintTemplate));
        }
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("gatepass/get")
    @POST
    public GetGatePassResponse getGatepass(GetGatePassRequest request) {
        return materialService.getGatePass(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("backorders/get")
    @POST
    public GetBackOrderItemsResponse getBackOrderItems(GetBackOrderItemsRequest request) {
        User user = WebContextUtils.getCurrentUser().getUser();
        request.setVendorId(user.getVendor().getId());
        return purchaseService.getBackOrderItems(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("vendorItemType/search")
    @POST
    public SearchVendorItemTypesResponse searchVendorItemType(SearchVendorItemTypesRequest request) {
        User user = WebContextUtils.getCurrentUser().getUser();
        request.setVendorId(user.getVendor().getId());
        return vendorService.searchVendorItemType(request);
    }

}
