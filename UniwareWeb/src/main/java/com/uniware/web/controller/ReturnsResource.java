/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 16-Mar-2012
 *  @author vibhu
 */
package com.uniware.web.controller;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.unifier.web.json.JElementList;
import com.unifier.web.utils.WebContextUtils;
import com.uniware.core.api.returns.AddShippingPackageToReturnManifestRequest;
import com.uniware.core.api.returns.AddShippingPackageToReturnManifestResponse;
import com.uniware.core.api.returns.AssignShippingProviderToReversePickupRequest;
import com.uniware.core.api.returns.AssignShippingProviderToReversePickupResponse;
import com.uniware.core.api.returns.CancelReversePickupRequest;
import com.uniware.core.api.returns.CancelReversePickupResponse;
import com.uniware.core.api.returns.CloseReturnManifestRequest;
import com.uniware.core.api.returns.CloseReturnManifestResponse;
import com.uniware.core.api.returns.CompleteReversePickupRequest;
import com.uniware.core.api.returns.CompleteReversePickupResponse;
import com.uniware.core.api.returns.ConfirmShippingPackageReturnToOriginRequest;
import com.uniware.core.api.returns.ConfirmShippingPackageReturnToOriginResponse;
import com.uniware.core.api.returns.CreateReshipmentRequest;
import com.uniware.core.api.returns.CreateReshipmentResponse;
import com.uniware.core.api.returns.CreateReturnManifestRequest;
import com.uniware.core.api.returns.CreateReturnManifestResponse;
import com.uniware.core.api.returns.CreateReversePickupRequest;
import com.uniware.core.api.returns.CreateReversePickupResponse;
import com.uniware.core.api.returns.DiscardReturnManifestRequest;
import com.uniware.core.api.returns.DiscardReturnManifestResponse;
import com.uniware.core.api.returns.EditReturnManifestItemRequest;
import com.uniware.core.api.returns.EditReturnManifestItemResponse;
import com.uniware.core.api.returns.GetEligibleShippingProvidersForReturnManifestRequest;
import com.uniware.core.api.returns.GetEligibleShippingProvidersForReturnManifestResponse;
import com.uniware.core.api.returns.GetReturnManifestItemDetailsRequest;
import com.uniware.core.api.returns.GetReturnManifestItemDetailsResponse;
import com.uniware.core.api.returns.GetReturnManifestShipmentsSummaryRequest;
import com.uniware.core.api.returns.GetReturnManifestShipmentsSummaryResponse;
import com.uniware.core.api.returns.GetReturnManifestSummaryRequest;
import com.uniware.core.api.returns.GetReturnManifestSummaryResponse;
import com.uniware.core.api.returns.RedispatchReversePickupRequest;
import com.uniware.core.api.returns.RedispatchReversePickupResponse;
import com.uniware.core.api.returns.RemoveReturnManifestItemRequest;
import com.uniware.core.api.returns.RemoveReturnManifestItemResponse;
import com.uniware.core.api.returns.ReshipShippingPackageRequest;
import com.uniware.core.api.returns.ReshipShippingPackageResponse;
import com.uniware.core.api.returns.ReshipmentDTO;
import com.uniware.core.api.returns.SearchAwaitingShippingPackageRequest;
import com.uniware.core.api.returns.SearchAwaitingShippingPackageResponse;
import com.uniware.core.api.reversepickup.EditReversePickupMetadataRequest;
import com.uniware.core.api.reversepickup.EditReversePickupMetadataResponse;
import com.uniware.core.api.warehouse.GetReversePickupRequest;
import com.uniware.core.api.warehouse.GetReversePickupResponse;
import com.uniware.services.returns.IReturnsService;

@Controller
@Path("/data/oms/returns/")
public class ReturnsResource {

    @Autowired
    IReturnsService returnsService;

    @Produces(MediaType.APPLICATION_JSON)
    @Path("reversePickup/create")
    @POST
    public CreateReversePickupResponse createReversePickup(CreateReversePickupRequest request) {
        return returnsService.createReversePickup(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("reversePickup/cancel")
    @POST
    public CancelReversePickupResponse cancelReversePickup(CancelReversePickupRequest request) {
        return returnsService.cancelReversePickup(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("reversePickup/redispatch")
    @POST
    public RedispatchReversePickupResponse redispatchReversePickup(RedispatchReversePickupRequest request) {
        return returnsService.redispatchReversePickup(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("shipment/redispatch")
    @POST
    public ReshipShippingPackageResponse reshipShippingPackage(ReshipShippingPackageRequest request) {
        return returnsService.reshipShippingPackage(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("awaitingAction")
    @POST
    public SearchAwaitingShippingPackageResponse searchAwaitingShippingPackages(SearchAwaitingShippingPackageRequest request) {
        return returnsService.searchAwaitingShippingPackages(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("pendingReshipments")
    @GET
    public JElementList<ReshipmentDTO> getPendingReshipments() {
        JElementList<ReshipmentDTO> pendingReshipments = new JElementList<ReshipmentDTO>();
        pendingReshipments.addAllElements(returnsService.getPendingReshipments());

        return pendingReshipments;
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("reshipment/create")
    @POST
    public CreateReshipmentResponse createReshipment(CreateReshipmentRequest request) {
        return returnsService.createReshipment(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("reversePickup/assignProvider")
    @POST
    public AssignShippingProviderToReversePickupResponse assignShippingProviderToReversePickup(AssignShippingProviderToReversePickupRequest request) {
        return returnsService.assignShippingProviderToReversePickup(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("reversePickup/search")
    @POST
    public GetReversePickupResponse getReversePickup(GetReversePickupRequest request) {
        return returnsService.getReversePickup(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("reversePickup/metadata/edit")
    @POST
    public EditReversePickupMetadataResponse editReversePickupMetadata(EditReversePickupMetadataRequest request) {
        return returnsService.editReversePickupMetadata(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("reversePickup/complete")
    @POST
    public CompleteReversePickupResponse completeReversePickup(CompleteReversePickupRequest request) {
        request.setUserId(WebContextUtils.getCurrentUser().getUser().getId());
        return returnsService.completeReversePickup(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("rto/confirm")
    @POST
    public ConfirmShippingPackageReturnToOriginResponse confirmShippingPackageReturnToOrigin(ConfirmShippingPackageReturnToOriginRequest request) {
        request.setUserId(WebContextUtils.getCurrentUser().getUser().getId());
        return returnsService.confirmShippingPackageReturn(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("manifest/create")
    @POST
    public CreateReturnManifestResponse createReturnManifest(CreateReturnManifestRequest request) {
        request.setUserId(WebContextUtils.getCurrentUser().getUser().getId());
        return returnsService.createReturnManifest(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("manifest/addShippingPackage")
    @POST
    public AddShippingPackageToReturnManifestResponse addShippingPackageToReturnManifest(AddShippingPackageToReturnManifestRequest request) {
        return returnsService.addShippingPackageToReturnManifest(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("manifest/fetchSummary")
    @POST
    public GetReturnManifestSummaryResponse getShippingManifestSummary(GetReturnManifestSummaryRequest request) {
        return returnsService.getReturnManifestSummary(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("manifest/fetchItemDetails")
    @POST
    public GetReturnManifestItemDetailsResponse getReturnManifestItemDetails(GetReturnManifestItemDetailsRequest request) {
        return returnsService.getReturnManifestItemDetails(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("manifest/removeItem")
    @POST
    public RemoveReturnManifestItemResponse removeReturnManifestItem(RemoveReturnManifestItemRequest request) {
        return returnsService.removeReturnManifestItem(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("manifest/fetchShipmentsSummary")
    @POST
    public GetReturnManifestShipmentsSummaryResponse getReturnManifestShipmentsSummary(GetReturnManifestShipmentsSummaryRequest request) {
        return returnsService.getReturnManifestShipmentsSummary(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("manifest/close")
    @POST
    public CloseReturnManifestResponse getShippingManifest(CloseReturnManifestRequest request) {
        return returnsService.closeReturnManifest(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("manifest/discard")
    @POST
    public DiscardReturnManifestResponse discardShippingManifest(DiscardReturnManifestRequest request) {
        request.setUserId(WebContextUtils.getCurrentUser().getUser().getId());
        return returnsService.discardReturnManifest(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("manifest/editManifestItem")
    @POST
    public EditReturnManifestItemResponse editReturnManifestItem(EditReturnManifestItemRequest request) {
        return returnsService.editReturnManifestItem(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("manifest/fetchShippingProviders")
    @POST
    public GetEligibleShippingProvidersForReturnManifestResponse getEligibleShippingProvidersForReturnManifest(GetEligibleShippingProvidersForReturnManifestRequest request) {
        return returnsService.getShippingProvidersForReturnManifest(request);
    }

}
