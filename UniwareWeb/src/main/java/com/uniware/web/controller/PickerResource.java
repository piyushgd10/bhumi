/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 14, 2011
 *  @author singla
 */
package com.uniware.web.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.uniware.core.api.picker.GetPutbackAcceptedItemsForPicklistRequest;
import com.uniware.core.api.picker.GetPutbackAcceptedItemsForPicklistResponse;
import com.uniware.core.api.picker.GetPutbackItemCountBySkuRequest;
import com.uniware.core.api.picker.GetPutbackItemCountBySkuResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.unifier.core.cache.CacheManager;
import com.unifier.core.template.Template;
import com.unifier.core.utils.FileUtils;
import com.unifier.core.utils.StringUtils;
import com.unifier.services.pdf.IPdfDocumentService;
import com.unifier.services.pdf.impl.PdfDocumentServiceImpl.PrintOptions;
import com.unifier.web.utils.WebContextUtils;
import com.uniware.core.api.picker.CreateAutoPicklistRequest;
import com.uniware.core.api.picker.CreateBulkPickBucketRequest;
import com.uniware.core.api.picker.CreateBulkPickBucketResponse;
import com.uniware.core.api.picker.CreateManualPicklistRequest;
import com.uniware.core.api.picker.CreatePicklistResponse;
import com.uniware.core.api.picker.DisablePickBucketRequest;
import com.uniware.core.api.picker.DisablePickBucketResponse;
import com.uniware.core.api.picker.EnablePickBucketRequest;
import com.uniware.core.api.picker.EnablePickBucketResponse;
import com.uniware.core.api.picker.GetPickSetWiseItemCountsRequest;
import com.uniware.core.api.picker.GetPickSetWiseItemCountsResponse;
import com.uniware.core.api.picker.GetPicklistRequest;
import com.uniware.core.api.picker.GetPicklistResponse;
import com.uniware.core.api.picker.GetPicksetsWithPackageCountRequest;
import com.uniware.core.api.picker.GetPicksetsWithPackageCountResponse;
import com.uniware.core.api.picker.SearchPicklistRequest;
import com.uniware.core.api.picker.SearchPicklistResponse;
import com.uniware.core.entity.PickBucket;
import com.uniware.core.vo.PrintTemplateVO;
import com.uniware.core.vo.SamplePrintTemplateVO;
import com.uniware.services.cache.PrintTemplateCache;
import com.uniware.services.cache.SamplePrintTemplateCache;
import com.uniware.services.picker.IPickerService;
import com.uniware.services.tenant.ITenantService;

/**
 * @author singla
 */

@Controller
@Path("/data/oms/picker/")
public class PickerResource {
    @Autowired
    private IPickerService      pickerService;

    @Autowired
    private IPdfDocumentService pdfDocumentService;

    @Autowired
    private ITenantService      tenantService;

    @Produces(MediaType.APPLICATION_JSON)
    @Path("picklist/manual/create")
    @POST
    public CreatePicklistResponse createManualPicklist(CreateManualPicklistRequest createPicklistRequest) {
        createPicklistRequest.setUserId(WebContextUtils.getCurrentUser().getUser().getId());
        return pickerService.createManualPicklist(createPicklistRequest);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("picklist/auto/create")
    @POST
    public CreatePicklistResponse createAutoPicklist(CreateAutoPicklistRequest createPicklistRequest) {
        createPicklistRequest.setUserId(WebContextUtils.getCurrentUser().getUser().getId());
        return pickerService.createAutoPicklist(createPicklistRequest);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("picklist/fetch")
    @POST
    public GetPicklistResponse getPicklist(GetPicklistRequest getPicklistRequest) {
        GetPicklistResponse picklist = pickerService.getPicklist(getPicklistRequest);
        return picklist;
    }

    @RequestMapping("/oms/picker/picklist/show/{picklistCode}")
    public void printPicklist(@PathVariable("picklistCode") String picklistCode, HttpServletResponse response) throws IOException {
        String picklistHtml = pickerService.preparePicklistHtml(StringUtils.split(picklistCode, "-"));
        if (StringUtils.isNotBlank(picklistHtml)) {
            response.setContentType("application/pdf");
            PrintTemplateVO picklistTemplate = CacheManager.getInstance().getCache(PrintTemplateCache.class).getPrintTemplateByType(PrintTemplateVO.Type.PICKLIST);
            SamplePrintTemplateVO samplePrintTemplate = CacheManager.getInstance().getCache(SamplePrintTemplateCache.class).getSamplePrintTemplate(
                    picklistTemplate.getSamplePrintTemplateCode());
            pdfDocumentService.writeHtmlToPdf(response.getOutputStream(), picklistHtml, new PrintOptions(samplePrintTemplate));
        }
    }

    @RequestMapping("/oms/picker/picklist/preview/{shippingPackageCodes}")
    public void printPicklistPackages(@PathVariable("shippingPackageCodes") String shippingPackageCodes, HttpServletResponse response) throws IOException {
        String picklistHtml = pickerService.preparePicklistPreviewHtml(StringUtils.split(shippingPackageCodes, "-"));
        if (StringUtils.isNotBlank(picklistHtml)) {
            response.setContentType("application/pdf");
            PrintTemplateVO picklistTemplate = CacheManager.getInstance().getCache(PrintTemplateCache.class).getPrintTemplateByType(PrintTemplateVO.Type.PICKLIST_PREVIEW);
            SamplePrintTemplateVO samplePrintTemplate = CacheManager.getInstance().getCache(SamplePrintTemplateCache.class).getSamplePrintTemplate(
                    picklistTemplate.getSamplePrintTemplateCode());
            pdfDocumentService.writeHtmlToPdf(response.getOutputStream(), picklistHtml, new PrintOptions(samplePrintTemplate));
        }
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("picklist/search")
    @POST
    public SearchPicklistResponse searchPicklist(SearchPicklistRequest request) {
        return pickerService.searchPicklist(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("picklist/picksetsWithPackagesCount")
    @POST
    public GetPicksetsWithPackageCountResponse getPicksetsWithPackageCount(GetPicksetsWithPackageCountRequest request) {
        return pickerService.getPicksetsWithPackageCount(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("pickset/count")
    @POST
    public GetPickSetWiseItemCountsResponse getPickSetWisePackageCounts(GetPickSetWiseItemCountsRequest request) {
        return pickerService.getPickSetWisePackageCounts(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("pickbuckets/bulk/create")
    @POST
    public CreateBulkPickBucketResponse createPickBuckets(CreateBulkPickBucketRequest request) {
        return pickerService.createBulkPickBuckets(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("pickbuckets/enable")
    @POST
    public EnablePickBucketResponse enablePickBucket(EnablePickBucketRequest request) {
        return pickerService.enablePickBucket(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("pickbuckets/disable")
    @POST
    public DisablePickBucketResponse disablePickBucket(DisablePickBucketRequest request) {
        return pickerService.disablePickBucket(request);
    }

    @RequestMapping("/data/oms/picker/pick-bucket/print/{codes}")
    public void shelfLabels(@PathVariable("codes") String codes, HttpServletResponse response) throws IOException {
        List<PickBucket> pickBuckets = pickerService.getPickBuckets(StringUtils.split(codes));
        if (pickBuckets != null) {
            response.setContentType("application/csv");
            response.addHeader("content-disposition", "attachment; filename=\"" + tenantService.getDownloadFileName("pickBuckets") + "\"");
            Template template = CacheManager.getInstance().getCache(PrintTemplateCache.class).getTemplateByType(PrintTemplateVO.Type.PICK_BUCKET_CSV.name());
            Map<String, Object> params = new HashMap<>();
            params.put("pickBuckets", pickBuckets);
            String output = template.evaluate(params);
            FileUtils.write(output.getBytes(), response.getOutputStream());
        }
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("putbackitems/get")
    @POST
    public GetPutbackAcceptedItemsForPicklistResponse getPutbackAcceptedItemsForPicklist(GetPutbackAcceptedItemsForPicklistRequest request) {
        return pickerService.getPutbackAcceptedItemsForPicklist(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("putbackitems/count")
    @POST
    public GetPutbackItemCountBySkuResponse getPutbackItemCountBySku(GetPutbackItemCountBySkuRequest request) {
        return pickerService.getPutbackItemCountBySku(request);
    }
}
