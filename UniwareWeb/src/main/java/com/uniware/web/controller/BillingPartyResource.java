/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 11-May-2012
 *  @author praveeng
 */
package com.uniware.web.controller;

import com.uniware.core.api.party.GetBillingPartyAndFacilityWithGSTINRequest;
import com.uniware.core.api.party.GetBillingPartyAndFacilityWithGSTINResponse;
import com.uniware.services.party.IPartyService;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.uniware.core.api.party.CreateBillingPartyRequest;
import com.uniware.core.api.party.CreateBillingPartyResponse;
import com.uniware.core.api.party.EditBillingPartyRequest;
import com.uniware.core.api.party.EditBillingPartyResponse;
import com.uniware.core.api.party.GetBillingPartyRequest;
import com.uniware.core.api.party.GetBillingPartyResponse;
import com.uniware.services.billing.party.IBillingPartyService;

@Controller
@Path("/data/admin/system/billingParty/")
public class BillingPartyResource {

    @Autowired
    private IBillingPartyService billingPartyService;

    @Autowired
    private IPartyService partyService;

    @Produces(MediaType.APPLICATION_JSON)
    @Path("get")
    @POST
    public GetBillingPartyResponse getBillingParty(GetBillingPartyRequest request) {
        return billingPartyService.getBillingPartyByCode(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("create")
    @POST
    public CreateBillingPartyResponse createBillingParty(CreateBillingPartyRequest request) {
        return billingPartyService.createBillingParty(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("edit")
    @POST
    public EditBillingPartyResponse editBillingParty(EditBillingPartyRequest request) {
        return billingPartyService.editBillingParty(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("accounts/get")
    @POST
    public GetBillingPartyAndFacilityWithGSTINResponse getBillingPartyAndFacilityWithAdresses(GetBillingPartyAndFacilityWithGSTINRequest request) {
        return partyService.getBillingPartyAndFacilityWithAdresses(request);
    }

}
