/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Apr 18, 2012
 *  @author praveeng
 */
package com.uniware.web.controller.admin;

import com.unifier.core.cache.CacheManager;
import com.unifier.core.template.Template;
import com.unifier.core.utils.FileUtils;
import com.unifier.core.utils.StringUtils;
import com.unifier.services.pdf.IPdfDocumentService;
import com.unifier.web.json.JElementList;
import com.uniware.core.api.warehouse.CreateShelfTypeRequest;
import com.uniware.core.api.warehouse.CreateShelfTypeResponse;
import com.uniware.core.api.warehouse.CreateShelvesWithPredefinedCodesRequest;
import com.uniware.core.api.warehouse.CreateShelvesWithPredefinedCodesResponse;
import com.uniware.core.api.warehouse.EditShelfTypeRequest;
import com.uniware.core.api.warehouse.EditShelfTypeResponse;
import com.uniware.core.api.warehouse.GetActiveStockingShelvesCountResponse;
import com.uniware.core.api.warehouse.SearchShelfRequest;
import com.uniware.core.api.warehouse.SearchShelfResponse;
import com.uniware.core.api.warehouse.ShelfTypeDTO;
import com.uniware.core.entity.Shelf;
import com.uniware.core.entity.ShelfType;
import com.uniware.core.vo.PrintTemplateVO;
import com.uniware.services.cache.PrintTemplateCache;
import com.uniware.services.warehouse.IShelfService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@Path("/data/admin/layout/")
public class LayoutResource {
    @Autowired
    private IShelfService shelfService;

    @Autowired
    private IPdfDocumentService pdfDocumentService;

    @Produces(MediaType.APPLICATION_JSON)
    @Path("shelftypes/get")
    @GET
    public JElementList<ShelfTypeDTO> shelfTypes() {
        List<ShelfType> shelfTypes = shelfService.getShelfTypes();
        JElementList<ShelfTypeDTO> elements = new JElementList<ShelfTypeDTO>();
        for (ShelfType shelfType : shelfTypes) {
            elements.addElement(new ShelfTypeDTO(shelfType));
        }
        return elements;
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("shelftype/create")
    @POST
    public CreateShelfTypeResponse createShelfType(CreateShelfTypeRequest request) {
        CreateShelfTypeResponse response = shelfService.createShelfType(request);
        return response;
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("shelftype/edit")
    @POST
    public EditShelfTypeResponse editShelfType(EditShelfTypeRequest editShelfTypeRequest) {
        EditShelfTypeResponse response = shelfService.editShelfType(editShelfTypeRequest);
        return response;
    }

    @RequestMapping("/admin/layout/shelf/print/{shelfCodes}")
    public void shelfLabels(@PathVariable("shelfCodes") String shelfCodes, HttpServletResponse response) throws IOException {
        List<Shelf> shelfs = shelfService.getShelfs(StringUtils.split(shelfCodes));
        if (shelfs != null) {
            response.setContentType("application/csv");
            response.addHeader("content-disposition", "attachment; filename=\"shelflabels.csv\"");
            Template template = CacheManager.getInstance().getCache(PrintTemplateCache.class).getTemplateByType(PrintTemplateVO.Type.SHELF_CSV.name());
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("shelfs", shelfs);
            String output = template.evaluate(params);
            FileUtils.write(output.getBytes(), response.getOutputStream());
        }
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("shelfs/search")
    @POST
    public SearchShelfResponse searchShelfs(SearchShelfRequest request) {
        SearchShelfResponse response = shelfService.searchShelfs(request);
        return response;
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("shelves/predefined/create")
    @POST
    public CreateShelvesWithPredefinedCodesResponse createShelfWithPredefinedCodes(CreateShelvesWithPredefinedCodesRequest request) {
        return shelfService.createShelfWithPredefinedCodes(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("shelves/active/count")
    @GET
    public GetActiveStockingShelvesCountResponse getTotalActiveShelfCount() {
        return shelfService.getTotalActiveShelfCount();
    }
}
