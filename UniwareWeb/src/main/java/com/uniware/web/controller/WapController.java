/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 05-Jul-2012
 *  @author vibhu
 */
package com.uniware.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class WapController {

    @RequestMapping("/login/wapLogin")
    public String wapLoginPage() {
        return "wapLogin";
    }

    @RequestMapping("/wap")
    public String homePage() {
        return "wapIndex";
    }

    @RequestMapping(value = { "/wap/wapGrnCreate" })
    public String wapGrnCreate() {
        return "/inflow/wapGrnCreate";
    }

    @RequestMapping(value = { "/wap/wapGrnQC" })
    public String wapGrnQC() {
        return "/inflow/wapGrnQC";
    }

    @RequestMapping(value = { "/wap/wapPicking" })
    public String wapPicking() {
        return "/picking/wapPicking";
    }

    @RequestMapping(value = { "/wap/wapPutaway" })
    public String wapPutaway() {
        return "/putaway/wapPutaway";
    }

    @RequestMapping(value = { "/wap/wapSearchItem" })
    public String wapScanItem() {
        return "/reports/wapSearchItem";
    }

    @RequestMapping(value = { "/wap/wapResolveBadInventory" })
    public String wapResolveBadInventory() {
        return "/material/wapResolveBadInventory";
    }

    @RequestMapping(value = { "/wap/wapGatePassCreate" })
    public String wapGatePassCreate() {
        return "/material/wapGatePassCreate";
    }
}
