/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 30-Jun-2012
 *  @author vibhu
 */
package com.uniware.web.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.unifier.core.cache.CacheManager;
import com.unifier.core.template.Template;
import com.unifier.services.pdf.IPdfDocumentService;
import com.unifier.services.pdf.impl.PdfDocumentServiceImpl.PrintOptions;
import com.unifier.web.utils.WebContextUtils;
import com.uniware.core.api.material.AddItemToGatePassRequest;
import com.uniware.core.api.material.AddItemToGatePassResponse;
import com.uniware.core.api.material.AddNonTraceableGatePassItemRequest;
import com.uniware.core.api.material.AddNonTraceableGatePassItemResponse;
import com.uniware.core.api.material.AddOrEditNonTraceableGatePassItemRequest;
import com.uniware.core.api.material.AddOrEditNonTraceableGatePassItemResponse;
import com.uniware.core.api.material.CompleteGatePassRequest;
import com.uniware.core.api.material.CompleteGatePassResponse;
import com.uniware.core.api.material.CreateGatePassRequest;
import com.uniware.core.api.material.CreateGatePassResponse;
import com.uniware.core.api.material.CreateInboundGatePassRequest;
import com.uniware.core.api.material.CreateInboundGatePassResponse;
import com.uniware.core.api.material.DiscardGatePassRequest;
import com.uniware.core.api.material.DiscardGatePassResponse;
import com.uniware.core.api.material.EditGatePassRequest;
import com.uniware.core.api.material.EditGatePassResponse;
import com.uniware.core.api.material.EditInboundGatePassRequest;
import com.uniware.core.api.material.EditInboundGatePassResponse;
import com.uniware.core.api.material.EditNonTraceableGatePassItemRequest;
import com.uniware.core.api.material.EditNonTraceableGatePassItemResponse;
import com.uniware.core.api.material.EditTraceableOutboundGatepassItemRequest;
import com.uniware.core.api.material.EditTraceableOutboundGatepassItemResponse;
import com.uniware.core.api.material.GetGatePassRequest;
import com.uniware.core.api.material.GetGatePassResponse;
import com.uniware.core.api.material.GetGatepassScannableItemDetailsRequest;
import com.uniware.core.api.material.GetGatepassScannableItemDetailsResponse;
import com.uniware.core.api.material.GetGatepassScannableItemSkuDetailRequest;
import com.uniware.core.api.material.GetGatepassScannableItemSkuDetailResponse;
import com.uniware.core.api.material.GetGatepassSummaryRequest;
import com.uniware.core.api.material.GetGatepassSummaryResponse;
import com.uniware.core.api.material.GetInboundGatePassRequest;
import com.uniware.core.api.material.GetInboundGatePassResponse;
import com.uniware.core.api.material.GetOutboundGatepassTypesRequest;
import com.uniware.core.api.material.GetOutboundGatepassTypesResponse;
import com.uniware.core.api.material.RemoveItemFromGatePassRequest;
import com.uniware.core.api.material.RemoveItemFromGatePassResponse;
import com.uniware.core.api.material.RemoveNonTraceableGatePassItemRequest;
import com.uniware.core.api.material.RemoveNonTraceableGatePassItemResponse;
import com.uniware.core.api.material.SearchGatePassRequest;
import com.uniware.core.api.material.SearchGatePassResponse;
import com.uniware.core.vo.PrintTemplateVO;
import com.uniware.core.vo.SamplePrintTemplateVO;
import com.uniware.services.cache.PrintTemplateCache;
import com.uniware.services.cache.SamplePrintTemplateCache;
import com.uniware.services.material.IMaterialService;

@Controller
@Path("/data/material/")
public class MaterialManagementResource {

    @Autowired
    IMaterialService            materialService;

    @Autowired
    private IPdfDocumentService pdfDocumentService;

    @Produces(MediaType.APPLICATION_JSON)
    @Path("gatepass/get")
    @POST
    public GetGatePassResponse getGatepass(GetGatePassRequest request) {
        return materialService.getGatePass(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("gatepass/create")
    @POST
    public CreateGatePassResponse createGatepass(CreateGatePassRequest request) {
        request.setUserId(WebContextUtils.getCurrentUser().getUser().getId());
        return materialService.createGatePass(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("gatepass/item/remove")
    @POST
    public RemoveItemFromGatePassResponse removeItemFromGatePass(RemoveItemFromGatePassRequest request) {
        return materialService.removeItemFromGatePass(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("gatepass/edit")
    @POST
    public EditGatePassResponse editGatepass(EditGatePassRequest request) {
        return materialService.editGatePass(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("gatepass/complete")
    @POST
    public CompleteGatePassResponse completeGatePass(CompleteGatePassRequest request) {
        request.setUserId(WebContextUtils.getCurrentUser().getUser().getId());
        return materialService.completeGatePass(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("gatepass/add/item")
    @POST
    public AddItemToGatePassResponse addItemToGatePass(AddItemToGatePassRequest request) {
        return materialService.addItemToGatePass(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("gatepass/nontraceable/items")
    @POST
    public AddOrEditNonTraceableGatePassItemResponse addItemToGatePass(AddOrEditNonTraceableGatePassItemRequest request) {
        return materialService.addOrEditNonTraceableGatePassItem(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("gatepass/nontraceable/items/add")
    @POST
    public AddNonTraceableGatePassItemResponse addNonTraceableGatePassItem(AddNonTraceableGatePassItemRequest request) {
        return materialService.addNonTraceableGatePassItem(request);
    }
    
    @Produces(MediaType.APPLICATION_JSON)
    @Path("gatepass/nontraceable/items/edit")
    @POST
    public EditNonTraceableGatePassItemResponse editItemToGatePass(EditNonTraceableGatePassItemRequest request) {
        return materialService.editNonTraceableGatePassItem(request);
    }
    
    @Produces(MediaType.APPLICATION_JSON)
    @Path("gatepass/nontraceable/items/remove")
    @POST
    public RemoveNonTraceableGatePassItemResponse removeNonTraceableGatePassItem(RemoveNonTraceableGatePassItemRequest request) {
        return materialService.removeNonTraceableGatePassItem(request);
    }
    
    @Produces(MediaType.APPLICATION_JSON)
    @Path("gatepass/search")
    @POST
    public SearchGatePassResponse searchGatePass(SearchGatePassRequest request) {
        return materialService.searchGatePass(request);
    }

    @RequestMapping("/material/gatepass/print/{gatePassCode}")
    public void printGatePass(@PathVariable("gatePassCode") String gatePassCode, HttpServletResponse response) throws IOException {
        Template template = CacheManager.getInstance().getCache(PrintTemplateCache.class).getTemplateByType(PrintTemplateVO.Type.OUTBOUND_GATE_PASS.name());
        String gatePassHtml = materialService.getGatePassHtml(gatePassCode, template);
        if (gatePassHtml != null) {
            response.setContentType("application/pdf");
            PrintTemplateVO gatePassTemplate = CacheManager.getInstance().getCache(PrintTemplateCache.class).getPrintTemplateByType(PrintTemplateVO.Type.OUTBOUND_GATE_PASS);
            SamplePrintTemplateVO samplePrintTemplate = CacheManager.getInstance().getCache(SamplePrintTemplateCache.class).getSamplePrintTemplate(
                    gatePassTemplate.getSamplePrintTemplateCode());
            pdfDocumentService.writeHtmlToPdf(response.getOutputStream(), gatePassHtml, new PrintOptions(samplePrintTemplate));
        }
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("gatepass/discard")
    @POST
    public DiscardGatePassResponse discardGatePass(DiscardGatePassRequest request) {
        request.setUserId(WebContextUtils.getCurrentUser().getUser().getId());
        return materialService.discardGatePass(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("gatepass/inbound/create")
    @POST
    public CreateInboundGatePassResponse createInboundGatepass(CreateInboundGatePassRequest request) {
        request.setUserId(WebContextUtils.getCurrentUser().getUser().getId());
        return materialService.createInboundGatePass(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("gatepass/inbound/edit")
    @POST
    public EditInboundGatePassResponse editInboundGatepass(EditInboundGatePassRequest request) {
        request.setUserId(WebContextUtils.getCurrentUser().getUser().getId());
        return materialService.editInboundGatePass(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("gatepass/inbound/get")
    @POST
    public GetInboundGatePassResponse editInboundGatepass(GetInboundGatePassRequest request) {
        return materialService.getInboundGatePass(request);
    }
    
    @Produces(MediaType.APPLICATION_JSON)
    @Path("gatepass/types/get")
    @POST
    public GetOutboundGatepassTypesResponse getOutboundGatepassTypes(GetOutboundGatepassTypesRequest request) {
        return materialService.getOutboundGatepassTypes(request);
    }
    
    @Produces(MediaType.APPLICATION_JSON)
    @Path("gatepass/summary/get")
    @POST
    public GetGatepassSummaryResponse getGatepassSummary(GetGatepassSummaryRequest request) {
        return materialService.getGatepassSummary(request);
    }
    
    @Produces(MediaType.APPLICATION_JSON)
    @Path("gatepass/scan/item")
    @POST
    public GetGatepassScannableItemDetailsResponse getGatepassScannableItemDetails(GetGatepassScannableItemDetailsRequest request) {
        return materialService.getGatepassScannableItemDetail(request);
    }
    
    @Produces(MediaType.APPLICATION_JSON)
    @Path("gatepass/scan/itemSKU")
    @POST
    public GetGatepassScannableItemSkuDetailResponse getGatepassScannableItemSkuDetails(GetGatepassScannableItemSkuDetailRequest request) {
        return materialService.getGatepassScannableItemSkuDetails(request);
    }
    
    @Produces(MediaType.APPLICATION_JSON)
    @Path("gatepass/traceable/item/edit")
    @POST
    public EditTraceableOutboundGatepassItemResponse editTraceableOutboundGatepassItem(EditTraceableOutboundGatepassItemRequest request) {
        return materialService.editTraceableOutboundGatepassItem(request);
    }
}
