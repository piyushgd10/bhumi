/*
 *  Copyright 2013 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Sep 17, 2013
 *  @author sunny
 */
package com.uniware.web.controller;

import com.uniware.core.api.reconciliation.GetSaleOrderReconciliationItemRequest;
import com.uniware.core.api.reconciliation.GetSaleOrderReconciliationItemResponse;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.uniware.core.api.reconciliation.MarkChannelPaymentsReconciledResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.uniware.core.api.reconciliation.CreateChannelPaymentReconciliationRequest;
import com.uniware.core.api.reconciliation.CreateChannelPaymentReconciliationResponse;
import com.uniware.core.api.reconciliation.GetChannelPaymentReconciliationMetadataResponse;
import com.uniware.core.api.reconciliation.GetChannelReconciliationInvoiceItemsRequest;
import com.uniware.core.api.reconciliation.GetChannelReconciliationInvoiceItemsResponse;
import com.uniware.core.api.reconciliation.GetChannelReconciliationMetadataRequest;
import com.uniware.core.api.reconciliation.GetOrderReconciliationDetailRequest;
import com.uniware.core.api.reconciliation.GetOrderReconciliationDetailResponse;
import com.uniware.core.api.reconciliation.GetReconciliationInvoiceDetailRequest;
import com.uniware.core.api.reconciliation.GetReconciliationInvoiceDetailResponse;
import com.uniware.core.api.reconciliation.MarkChannelPaymentsDisputedRequest;
import com.uniware.core.api.reconciliation.MarkChannelPaymentsDisputedResponse;
import com.uniware.core.api.reconciliation.MarkChannelPaymentsReconciliedRequest;
import com.uniware.services.reconciliation.IReconciliationService;

@Controller
@Path("/data/reconciliation/")
public class ReconciliationResource {

    @Autowired
    private IReconciliationService reconciliationService;

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/preReconciliationData")
    @POST
    @Deprecated
    public GetChannelPaymentReconciliationMetadataResponse getPreReconciliationData(GetChannelReconciliationMetadataRequest request) {
        return new GetChannelPaymentReconciliationMetadataResponse();
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/orderReconciliationItems")
    @POST
    public GetSaleOrderReconciliationItemResponse getSaleOrderReconciliationItems(GetSaleOrderReconciliationItemRequest request) {
        return reconciliationService.getSaleOrderReconciliationItems(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/invoiceItemsByIdentifier")
    @POST
    public GetChannelReconciliationInvoiceItemsResponse getChannelReconciliationInvoiceItems(GetChannelReconciliationMetadataRequest request) {
        return reconciliationService.getChannnelReconciliationInvoiceItemsByIdentifier(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/invoiceItems")
    @POST
    public GetChannelReconciliationInvoiceItemsResponse getChannelReconciliationInvoiceItemsByInvoiceCode(GetChannelReconciliationInvoiceItemsRequest request) {
        return reconciliationService.getChannnelReconciliationInvoiceItemsByInvoiceCode(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/orderReconciliationDetail")
    @POST
    @Deprecated
    public GetOrderReconciliationDetailResponse getChannelReconciliationInvoiceDetail(GetOrderReconciliationDetailRequest request) {
        return new GetOrderReconciliationDetailResponse();
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/invoiceDetail")
    @POST
    public GetReconciliationInvoiceDetailResponse getChannelReconciliationInvoiceDetail(GetReconciliationInvoiceDetailRequest request) {
        return reconciliationService.getReconciliationInvoiceByCode(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("reconcileChannelPayment")
    @POST
    public CreateChannelPaymentReconciliationResponse manualReconcileOrder(CreateChannelPaymentReconciliationRequest request) {
        return reconciliationService.manualReconciledOrderItem(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("markChannelPaymentsReconcilied")
    @POST
    public MarkChannelPaymentsReconciledResponse markChannelPaymentsReconcilied(MarkChannelPaymentsReconciliedRequest request) {
        return reconciliationService.markChannelPaymentsReconcilied(request);
    }
    @Produces(MediaType.APPLICATION_JSON)
    @Path("markChannelPaymentsDisputed")
    @POST
    public MarkChannelPaymentsDisputedResponse markChannelPaymentsDisputed(MarkChannelPaymentsDisputedRequest request) {
        return reconciliationService.markChannelPaymentsDisputed(request);
    }
}
