/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jan 5, 2012
 *  @author singla
 */
package com.uniware.web.controller;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.unifier.core.api.user.UserSearchDTO;
import com.unifier.services.search.IGlobalSearchService;
import com.unifier.services.users.IUsersService;
import com.uniware.core.api.admin.shipping.ShippingProviderDTO;
import com.uniware.core.api.catalog.dto.CategorySearchDTO;
import com.uniware.core.api.catalog.dto.ItemTypeLookupDTO;
import com.uniware.core.api.channel.ChannelItemTypeLookupDTO;
import com.uniware.core.api.channel.SourceSearchDTO;
import com.uniware.core.api.party.PartySearchDTO;
import com.uniware.core.api.party.dto.BillingPartySearchDTO;
import com.uniware.core.api.party.dto.CustomerDTO;
import com.uniware.core.api.party.dto.CustomerSearchDTO;
import com.uniware.core.api.party.dto.VendorItemTypeDTO;
import com.uniware.core.api.warehouse.FacilityDTO;
import com.uniware.core.configuration.TaxConfiguration.TaxTypeVO;
import com.uniware.services.billing.party.IBillingPartyService;
import com.uniware.services.cache.VendorCache.VendorVO;
import com.uniware.services.catalog.ICatalogService;
import com.uniware.services.channel.IChannelCatalogService;
import com.uniware.services.channel.IChannelService;
import com.uniware.services.customer.ICustomerService;
import com.uniware.services.party.IPartyService;
import com.uniware.services.script.service.IScriptService;
import com.uniware.services.shipping.IShippingProviderService;
import com.uniware.services.tax.ITaxTypeService;
import com.uniware.services.vendor.IVendorService;
import com.uniware.services.warehouse.IFacilityService;

/**
 * @author singla
 */
@Path("/data/lookup/")
@Controller
public class AutocompleteSearchResource {

    @Autowired
    private ICatalogService          catalogService;

    @Autowired
    private IVendorService           vendorService;

    @Autowired
    private IChannelService          channelService;

    @Autowired
    private IUsersService            usersService;

    @Autowired
    private ITaxTypeService          taxTypeService;

    @Autowired
    private IPartyService            partyService;

    @Autowired
    private ICustomerService         customerService;

    @Autowired
    private IShippingProviderService shippingProviderService;

    @Autowired
    private IFacilityService         facilityService;

    @Autowired
    private IGlobalSearchService     searchService;

    @Autowired
    private IBillingPartyService     billingPartyService;

    @Autowired
    private IScriptService           scriptService;

    @Autowired
    private IChannelCatalogService   channelCatalogService;

    @Produces(MediaType.APPLICATION_JSON)
    @Path("vendors")
    @GET
    public List<VendorVO> lookupVendors(@QueryParam("name") String name) {
        return catalogService.lookupVendors(name);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("vendorItemTypes")
    @GET
    public List<VendorItemTypeDTO> lookupVendorItemTypes(@QueryParam("keyword") String keyword, @QueryParam("vendorCode") String vendorCode) {
        return vendorService.lookupVendorItemTypes(keyword, vendorCode);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("itemTypes")
    @GET
    public List<ItemTypeLookupDTO> lookupItemTypes(@QueryParam("keyword") String keyword) {
        return catalogService.lookupItemTypes(keyword);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("linkedChannelItemTypes")
    @GET
    public List<ChannelItemTypeLookupDTO> lookupChannelItemTypes(@QueryParam("channelCode") String channelCode, @QueryParam("keyword") String keyword) {
        return channelCatalogService.lookupLinkedChannelItemTypes(channelCode, keyword);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("allItemTypes")
    @GET
    public List<ItemTypeLookupDTO> lookupAllItemTypes(@QueryParam("keyword") String keyword) {
        return catalogService.lookupAllItemTypes(keyword);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("party")
    @GET
    public List<PartySearchDTO> lookupParty(@QueryParam("keyword") String keyword) {
        return partyService.lookupParty(keyword);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("users")
    @GET
    public List<UserSearchDTO> lookupUsers(@QueryParam("name") String name, @QueryParam("email") String email, @QueryParam("nameOrEmail") String nameOrEmail) {
        if (name != null) {
            return usersService.lookupUsersByName(name);
        }
        if (email != null) {
            return usersService.lookupUsersByUsername(email);
        }
        return usersService.lookupUsers(nameOrEmail);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("taxTypes")
    @GET
    public List<TaxTypeVO> lookupTaxTypes(@QueryParam("code") String code) {
        return taxTypeService.lookupTaxTypes(code);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("customers")
    @GET
    public List<CustomerSearchDTO> lookupCustomer(@QueryParam("keyword") String keyword) {
        return customerService.lookupCustomers(keyword);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("customerDetails")
    @GET
    public List<CustomerDTO> lookupCustomers(@QueryParam("keyword") String keyword) {
        return customerService.getCustomersByCodeEmailOrMobile(keyword);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("providers")
    @GET
    public List<ShippingProviderDTO> lookupProviders(@QueryParam("keyword") String keyword) {
        return shippingProviderService.lookupProviders(keyword);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("sources")
    @GET
    public List<SourceSearchDTO> lookupSource(@QueryParam("keyword") String keyword) {
        return channelService.lookupSource(keyword);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("facilities")
    @GET
    public List<FacilityDTO> lookUpFacility(@QueryParam("keyword") String keyword) {
        return facilityService.lookUpFacilities(keyword);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("billingParty")
    @GET
    public List<BillingPartySearchDTO> lookupBillingParty(@QueryParam("keyword") String keyword) {
        return billingPartyService.lookupBillingParty(keyword);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("helpResourceTopics")
    @GET
    public String lookupHelpResourceTopics(@QueryParam("term") String term) {
        return searchService.searchHelpResourceTopics(term);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("scripts")
    @GET
    public List<String> lookupScripts(@QueryParam("keyword") String keyword) {
        return scriptService.lookupScripts(keyword);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("categories")
    @GET
    public List<CategorySearchDTO> lookupCategories(@QueryParam("keyword") String keyword) {
        return catalogService.lookupCategories(keyword);
    }
}
