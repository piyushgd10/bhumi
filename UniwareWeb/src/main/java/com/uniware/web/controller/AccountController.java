/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 18, 2012
 *  @author praveeng
 */
package com.uniware.web.controller;

import javax.ws.rs.Path;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/account")
@Path("/data/account")
public class AccountController {

    @RequestMapping("/packagesToReconcile")
    public String getPackagesToReconcile() {
        return "/account/packagesToReconcile";
    }

}
