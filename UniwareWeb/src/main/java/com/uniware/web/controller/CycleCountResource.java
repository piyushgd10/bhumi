package com.uniware.web.controller;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.uniware.core.api.cyclecount.AddShelfToSubCycleCountRequest;
import com.uniware.core.api.cyclecount.AddShelfToSubCycleCountResponse;
import com.uniware.core.api.cyclecount.BlockShelvesForCycleCountRequest;
import com.uniware.core.api.cyclecount.BlockShelvesForCycleCountResponse;
import com.uniware.core.api.cyclecount.CloseCycleCountRequest;
import com.uniware.core.api.cyclecount.CloseCycleCountResponse;
import com.uniware.core.api.cyclecount.CompleteCountForShelfRequest;
import com.uniware.core.api.cyclecount.CompleteCountForShelfResponse;
import com.uniware.core.api.cyclecount.CreateCycleCountRequest;
import com.uniware.core.api.cyclecount.CreateCycleCountResponse;
import com.uniware.core.api.cyclecount.CreateManualSubCycleCountRequest;
import com.uniware.core.api.cyclecount.CreateManualSubCycleCountResponse;
import com.uniware.core.api.cyclecount.CreateSemiAutomaticSubCycleCountRequest;
import com.uniware.core.api.cyclecount.CreateSubCycleCountRequest;
import com.uniware.core.api.cyclecount.CreateSubCycleCountResponse;
import com.uniware.core.api.cyclecount.EditCycleCountRequest;
import com.uniware.core.api.cyclecount.EditCycleCountResponse;
import com.uniware.core.api.cyclecount.GetActiveCycleCountRequest;
import com.uniware.core.api.cyclecount.GetBadItemsForCycleCountRequest;
import com.uniware.core.api.cyclecount.GetBadItemsForShelfRequest;
import com.uniware.core.api.cyclecount.GetBadItemsForShelfResponse;
import com.uniware.core.api.cyclecount.GetCycleCountResponse;
import com.uniware.core.api.cyclecount.GetErrorItemsForCycleCountResponse;
import com.uniware.core.api.cyclecount.GetItemRequest;
import com.uniware.core.api.cyclecount.GetItemResponse;
import com.uniware.core.api.cyclecount.GetShelfDetailsRequest;
import com.uniware.core.api.cyclecount.GetShelfDetailsResponse;
import com.uniware.core.api.cyclecount.GetSubCycleCountShelvesRequest;
import com.uniware.core.api.cyclecount.GetSubCycleCountShelvesResponse;
import com.uniware.core.api.cyclecount.GetZonesForCycleCountRequest;
import com.uniware.core.api.cyclecount.GetZonesForCycleCountResponse;
import com.uniware.core.api.cyclecount.RecountShelfRequest;
import com.uniware.core.api.cyclecount.RecountShelfResponse;
import com.uniware.core.api.cyclecount.RemoveShelvesFromCycleCountRequest;
import com.uniware.core.api.cyclecount.RemoveShelvesFromCycleCountResponse;
import com.uniware.core.api.cyclecount.SearchCycleCountShelfRequest;
import com.uniware.core.api.cyclecount.SearchCycleCountShelfResponse;
import com.uniware.core.api.cyclecount.StartCountingForShelfRequest;
import com.uniware.core.api.cyclecount.StartCountingForShelfResponse;
import com.uniware.core.api.cyclecount.UnblockShelvesRequest;
import com.uniware.core.api.cyclecount.UnblockShelvesResponse;
import com.uniware.services.cyclecount.ICycleCountService;

@SuppressWarnings("SpringAutowiredFieldsWarningInspection")
@Controller
@Path("/data/cyclecount/")
public class CycleCountResource {

    @Autowired
    private ICycleCountService cycleCountService;

    @Produces(MediaType.APPLICATION_JSON)
    @Path("active/get")
    @POST
    public GetCycleCountResponse getCycleCount(GetActiveCycleCountRequest request) {
        return cycleCountService.getActiveCycleCount(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("create")
    @POST
    public CreateCycleCountResponse createCycleCount(CreateCycleCountRequest request) {
        return cycleCountService.createCycleCount(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("edit")
    @POST
    public EditCycleCountResponse editCycleCount(EditCycleCountRequest request) {
        return cycleCountService.editCycleCount(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("subcyclecount/create")
    @POST
    public CreateSubCycleCountResponse createSubCycleCount(CreateSubCycleCountRequest request) {
        return cycleCountService.createAutomaticSubCycleCount(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("subcyclecount/manual/create")
    @POST
    public CreateManualSubCycleCountResponse createManualSubCycleCount(CreateManualSubCycleCountRequest request) {
        return cycleCountService.createManualSubCycleCount(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("subcyclecount/semiauto/create")
    @POST
    public CreateSubCycleCountResponse createSemiAutomaticSubCycleCount(CreateSemiAutomaticSubCycleCountRequest request) {
        return cycleCountService.createSemiAutomaticSubCycleCount(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("shelf/block")
    @POST
    public BlockShelvesForCycleCountResponse blockShelfForCycleCountResponse(BlockShelvesForCycleCountRequest request) {
        return cycleCountService.blockShelvesForCycleCountRequest(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("shelf/start")
    @POST
    public StartCountingForShelfResponse blockShelfForCycleCountResponse(StartCountingForShelfRequest request) {
        return cycleCountService.startCountingForShelf(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("zones/get")
    @POST
    public GetZonesForCycleCountResponse getZonesForCycleCount(GetZonesForCycleCountRequest request) {
        return cycleCountService.getZonesForCycleCount(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("shelf/submit")
    @POST
    public CompleteCountForShelfResponse completeCountForShelf(CompleteCountForShelfRequest request) {
        return cycleCountService.completeCountForShelf(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("shelf/get")
    @POST
    public GetShelfDetailsResponse getPutawaysAndPackagesPendingForShelf(GetShelfDetailsRequest request) {
        return cycleCountService.getShelfDetails(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/subcyclecount/shelf/add")
    @POST
    public AddShelfToSubCycleCountResponse addShelfToSubCycleCount(AddShelfToSubCycleCountRequest request) {
        return cycleCountService.addShelfToSubCycleCount(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/shelf/unblock")
    @POST
    public UnblockShelvesResponse unblockShelf(UnblockShelvesRequest request) {
        return cycleCountService.unblockShelves(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/shelf/remove")
    @POST
    public RemoveShelvesFromCycleCountResponse removeShelfFromSubCycleCount(RemoveShelvesFromCycleCountRequest request) {
        return cycleCountService.removeShelvesFromCycleCount(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/shelf/search")
    @POST
    public SearchCycleCountShelfResponse searchCycleCountShelf(SearchCycleCountShelfRequest request) {
        return cycleCountService.searchCycleCountShelf(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/shelf/recount")
    @POST
    public RecountShelfResponse recountShelf(RecountShelfRequest request) {
        return cycleCountService.recountShelf(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/subcyclecount/get")
    @POST
    public GetSubCycleCountShelvesResponse getSubCycleCount(GetSubCycleCountShelvesRequest request) {
        return cycleCountService.getSubCycleCount(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/item/get")
    @POST
    public GetItemResponse getItem(GetItemRequest request) {
        return cycleCountService.getItem(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/erroritems/get")
    @POST
    public GetErrorItemsForCycleCountResponse getErrorItemsForCycleCount(GetBadItemsForCycleCountRequest request) {
        return cycleCountService.getErrorItemsForCycleCount(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/shelf/erroritems/get")
    @POST
    public GetBadItemsForShelfResponse getErrorItemsForCycleCount(GetBadItemsForShelfRequest request) {
        return cycleCountService.getErrorItemsForShelf(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("close")
    @POST
    public CloseCycleCountResponse closeCycleCount(CloseCycleCountRequest request) {
        return cycleCountService.closeCycleCount(request);
    }

}
