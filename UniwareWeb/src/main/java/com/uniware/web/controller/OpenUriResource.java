/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 25-Mar-2014
 *  @author amit
 */
package com.uniware.web.controller;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.unifier.core.cache.CacheManager;
import com.uniware.core.api.metadata.AppVersionRequest;
import com.uniware.core.api.metadata.AppVersionResponse;
import com.uniware.core.cache.EnvironmentPropertiesCache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.unifier.core.cache.data.manager.ICacheDataManager;
import com.unifier.core.entity.EnvironmentProperty;
import com.unifier.services.printing.IPrintConfigService;
import com.uniware.core.api.print.GetPrintableTemplateByFilenamePrefixRequest;
import com.uniware.core.api.print.GetPrintableTemplateByFilenamePrefixResponse;
import com.uniware.core.api.print.GetPrintableTemplateRequest;
import com.uniware.core.api.print.GetPrintableTemplateResponse;
import com.uniware.services.tenant.ITenantService;
import com.uniware.web.api.ping.ServiceStatusResponse;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@Path("/open")
public class OpenUriResource {

    private static final Logger LOG = LoggerFactory.getLogger(OpenUriResource.class);

    @Autowired
    private IPrintConfigService printConfigService;

    @Autowired
    private ITenantService tenantService;

    @Autowired
    private ICacheDataManager cacheDataManager;

    @SuppressWarnings("deprecation")
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/getPrintableTemplate")
    @POST
    public GetPrintableTemplateResponse getPrintableTemplate(GetPrintableTemplateRequest request) {
        return printConfigService.getPrintableTemplate(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/getPrintableTemplate/filename")
    @POST
    public GetPrintableTemplateByFilenamePrefixResponse getPrintableTemplate(GetPrintableTemplateByFilenamePrefixRequest request) {
        return printConfigService.getPrintableTemplateByFileNamePrefix(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/service/status")
    @GET
    public ServiceStatusResponse getServiceStatus() {
        ServiceStatusResponse response = new ServiceStatusResponse();
        try {
            List<EnvironmentProperty> properties = cacheDataManager.getEnvironmentProperties();
            response.setSuccessful(properties != null);
        } catch (Throwable t) {
            LOG.error("Service status health check failed", t);
            response.setSuccessful(false);
        }
        return response;
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("app/version")
    @POST
    public AppVersionResponse getCurrentAppVersion(AppVersionRequest request) {
        EnvironmentPropertiesCache cache = CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class);
        return new AppVersionResponse(cache.getUniwareAppCurrentMajorVersion(), cache.getUniwareAppCurrentMinorVersion());
    }
}
