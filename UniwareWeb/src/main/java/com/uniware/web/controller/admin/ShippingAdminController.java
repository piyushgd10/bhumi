/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 *  @version     1.0, 06-Feb-2012
 *  @author vibhu
 */
package com.uniware.web.controller.admin;

import com.google.gson.Gson;
import com.uniware.services.shipping.IShippingAdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ShippingAdminController {

    @Autowired
    private IShippingAdminService shippingAdminService;

    @RequestMapping("/admin/shipping/packageTypes")
    public String boxTypes(ModelMap modelMap) {
        return "admin/shipping/packageTypes";
    }

    @RequestMapping("/admin/shipping/allocationRules")
    public String allocationRules(ModelMap modelMap) {
        modelMap.addAttribute("allocationRules", new Gson().toJson(shippingAdminService.getShippingProviderAllocationRules()));
        modelMap.addAttribute("providers", new Gson().toJson(shippingAdminService.getEnabledShippingProviders()));
        return "admin/shipping/allocationRules";
    }

}
