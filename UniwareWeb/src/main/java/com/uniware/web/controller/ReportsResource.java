/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 *  @version     1.0, Apr 24, 2012
 *  @author praveeng
 */
package com.uniware.web.controller;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.unifier.core.cache.CacheManager;
import com.unifier.services.report.IReportService;
import com.uniware.core.api.catalog.CreateShippingProviderLocationRequest;
import com.uniware.core.api.catalog.CreateShippingProviderLocationResponse;
import com.uniware.core.api.catalog.EditShippingProviderLocationRequest;
import com.uniware.core.api.catalog.EditShippingProviderLocationResponse;
import com.uniware.core.api.catalog.SearchShippingProviderLocationRequest;
import com.uniware.core.api.catalog.SearchShippingProviderLocationResponse;
import com.uniware.core.api.inflow.AddItemDetailsRequest;
import com.uniware.core.api.inflow.AddItemDetailsResponse;
import com.uniware.core.api.inflow.ViewBarcodesRequest;
import com.uniware.core.api.inventory.MarkItemTypeQuantityFoundRequest;
import com.uniware.core.api.inventory.MarkItemTypeQuantityFoundResponse;
import com.uniware.core.api.inventory.MarkItemTypeQuantityNotFoundRequest;
import com.uniware.core.api.inventory.MarkItemtypeQuantityNotFoundResponse;
import com.uniware.core.api.inventory.SearchInventoryRequest;
import com.uniware.core.api.inventory.SearchInventoryResponse;
import com.uniware.core.api.inventory.SearchInventorySnapshotRequest;
import com.uniware.core.api.inventory.SearchInventorySnapshotResponse;
import com.uniware.core.api.inventory.SearchItemTypeInventoryRequest;
import com.uniware.core.api.inventory.SearchItemTypeInventoryResponse;
import com.uniware.core.api.inventory.SearchItemTypeNotFoundQuantityResponse;
import com.uniware.core.api.item.GetItemTypeDetailRequest;
import com.uniware.core.api.item.GetItemTypeDetailResponse;
import com.uniware.core.api.saleorder.SearchSaleOrderItemRequest;
import com.uniware.core.api.saleorder.SearchSaleOrderItemResponse;
import com.uniware.core.api.saleorder.SearchSaleOrderRequest;
import com.uniware.core.api.saleorder.SearchSaleOrderResponse;
import com.uniware.core.api.shipping.SearchShippingPackageRequest;
import com.uniware.core.api.shipping.SearchShippingPackageResponse;
import com.uniware.core.cache.FacilityCache;
import com.uniware.core.entity.Item;
import com.uniware.core.utils.UserContext;
import com.uniware.services.inflow.IInflowService;
import com.uniware.services.inventory.IInventoryService;
import com.uniware.services.lookup.ILookupService;
import com.uniware.services.saleorder.ISaleOrderService;
import com.uniware.services.shipping.IShippingProviderService;
import com.uniware.services.shipping.IShippingService;

@Controller
@RequestMapping("/data/reports/")
@Path("/data/reports/")
public class ReportsResource {

    @Autowired
    IInventoryService        inventoryService;

    @Autowired
    ISaleOrderService        saleOrderService;

    @Autowired
    IShippingService         shippingService;

    @Autowired
    ILookupService           lookupService;

    @Autowired
    IInflowService           inflowService;

    @Autowired
    IShippingProviderService shippingProviderService;

    @Autowired
    IReportService           reportService;

    @Produces(MediaType.APPLICATION_JSON)
    @Path("search/order")
    @POST
    public SearchSaleOrderResponse searchOrders(SearchSaleOrderRequest request) {
        if (UserContext.current().getFacilityId() == null) {
            request.setFacilityCodes(CacheManager.getInstance().getCache(FacilityCache.class).getFacilityCodes());
        } else {
            List<String> facilityCodes = new ArrayList<String>();
            facilityCodes.add(CacheManager.getInstance().getCache(FacilityCache.class).getCurrentFacility().getCode());
            request.setFacilityCodes(facilityCodes);
        }
        return saleOrderService.searchSaleOrders(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("search/saleOrderItem")
    @POST
    public SearchSaleOrderItemResponse searchOrderItems(SearchSaleOrderItemRequest request) {
        return saleOrderService.searchSaleOrderItems(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("search/inventory")
    @POST
    public SearchInventoryResponse searchInventory(SearchInventoryRequest request) {
        return lookupService.searchInventory(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("search/shippingPackages")
    @POST
    public SearchShippingPackageResponse searchShippingPackage(SearchShippingPackageRequest request) {
        return shippingService.searchShippingPackages(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("search/itemType/fetch")
    @POST
    public GetItemTypeDetailResponse getItemTypeDetails(GetItemTypeDetailRequest request) {
        return lookupService.getItemTypeDetails(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("lookups/itemCustomField/edit")
    @POST
    public AddItemDetailsResponse editItemCustomField(AddItemDetailsRequest request) {
        return inflowService.addItemDetails(request);
    }

    @ResponseBody
    @Path("search/itemType/barcodes/view")
    @POST
    public String getAvailableBarcodes(ViewBarcodesRequest request) {
        List<String> itemCodes = new ArrayList<String>();
        List<Item> items = inventoryService.getItemsByItemTypeByStatus(request.getItemSku(), request.getStatus(), request.getOffset(), request.getLimit());
        for (Item item : items) {
            itemCodes.add(item.getCode());
        }
        return new Gson().toJson(itemCodes);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("search/shippingProviderLocation")
    @POST
    public SearchShippingProviderLocationResponse searchShippingProviderLocation(SearchShippingProviderLocationRequest request) {
        return shippingProviderService.searchShippingProviderLocation(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("create/shippingProviderLocation")
    @POST
    public CreateShippingProviderLocationResponse createShippingProviderLocation(CreateShippingProviderLocationRequest request) {
        return shippingService.createShippingProviderLocation(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("edit/shippingProviderLocation")
    @POST
    public EditShippingProviderLocationResponse editShippingProviderLocation(EditShippingProviderLocationRequest request) {
        return shippingService.editShippingProviderLocation(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("search/inventorySnapshot")
    @POST
    public SearchInventorySnapshotResponse searchInvenotrySnapshot(SearchInventorySnapshotRequest request) {
        return lookupService.searchInventorySnapshot(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("showItemTypeQuantityNotFoundDetails")
    @GET
    public SearchItemTypeNotFoundQuantityResponse itemQuantityNotFoundDetails(@QueryParam("itemSkuCode") String itemSkuCode) {
        return lookupService.itemQuantityNotFoundDetails(itemSkuCode);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("searchInventory")
    @POST
    public SearchItemTypeInventoryResponse searchItemtypeInventory(SearchItemTypeInventoryRequest request) {
        return lookupService.searchItemtypeInventory(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("markQuantityFound")
    @POST
    public MarkItemTypeQuantityFoundResponse markItemTypeQuantityFound(MarkItemTypeQuantityFoundRequest request) {
        return inventoryService.markItemTypeQuantityFound(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("markQuantityNotFound")
    @POST
    public MarkItemtypeQuantityNotFoundResponse markItemTypeQuantityNotFound(MarkItemTypeQuantityNotFoundRequest request) {
        return inventoryService.markItemTypeQuantityNotFound(request);
    }
}
