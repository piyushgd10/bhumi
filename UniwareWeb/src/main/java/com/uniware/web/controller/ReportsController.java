/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 06-Feb-2012
 *  @author vibhu
 */
package com.uniware.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.ws.rs.Path;

@Controller
@Path("/data/reports/")
public class ReportsController {

    public static final String LOOKUP_ITEM_PAGE = "reports/searchItemNew";

    @RequestMapping("/reports/createPOSOrder")
    public String editPOSSaleOrder() {
        return "inflow/lenskart/pos";
    }

    @RequestMapping("/reports/searchItem")
    public String searchItem() {
        return LOOKUP_ITEM_PAGE;
    }

    @RequestMapping("/reports/searchItemType")
    public String searchItemType() {
        return "reports/searchItemType";
    }

}
