/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 18, 2012
 *  @author praveeng
 */
package com.uniware.web.controller;

import com.unifier.core.api.access.GrantApiAccessRequest;
import com.unifier.core.api.access.GrantApiAccessResponse;
import com.unifier.core.api.access.GrantApiAccessToVendorRequest;
import com.unifier.core.api.access.GrantApiAccessToVendorResponse;
import com.unifier.services.users.IUsersService;
import com.unifier.web.utils.WebContextUtils;
import com.uniware.core.utils.UserContext;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserCache;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/api/access")
@Path("/data/api/access")
public class ApiAccessController {

    @Autowired
    private IUsersService usersService;

    @Autowired
    private UserCache     apiUserCache;

    @RequestMapping("/requestAccess")
    public String requestAPIAccess(ModelMap modelMap) {
        modelMap.addAttribute("tenant", UserContext.current().getTenant().getName());
        return "/account/apiAccess";
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/grantAccess")
    @POST
    public GrantApiAccessToVendorResponse grantAPIAccess(GrantApiAccessToVendorRequest request) {
        String username = WebContextUtils.getCurrentUser().getUsername();
        request.setUserName(username);
        GrantApiAccessToVendorResponse response = usersService.grantApiAccess(request);
        apiUserCache.removeUserFromCache(username);
        return response;
    }

    @RequestMapping("/requestDropshipAccess")
    public String requestDropshipAPIAccess(ModelMap modelMap) {
        modelMap.addAttribute("tenant", UserContext.current().getTenant().getName());
        return "/account/dropshipApiAccess";
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/dropship/grantAccess")
    @POST
    public GrantApiAccessResponse grantAPIAccess(GrantApiAccessRequest request) {
        request.setUserName(WebContextUtils.getCurrentUser().getUsername());
        return usersService.grantApiAccess(request);
    }
}
