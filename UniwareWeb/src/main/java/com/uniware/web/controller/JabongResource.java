/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jul 5, 2012
 *  @author singla
 */
package com.uniware.web.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.unifier.core.api.comments.AddCommentRequest;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.template.Template;
import com.unifier.core.utils.FileUtils;
import com.unifier.services.comments.ICommentsService;
import com.unifier.services.comments.impl.CommentsServiceImpl;
import com.unifier.web.utils.WebContextUtils;
import com.uniware.core.api.jabong.JabongCreateItemsForPORequest;
import com.uniware.core.api.jabong.JabongCreateItemsForPOResponse;
import com.uniware.core.api.jabong.JabongQualityAcceptItemRequest;
import com.uniware.core.api.jabong.JabongQualityAcceptItemResponse;
import com.uniware.core.api.jabong.JabongQualityRejectItemRequest;
import com.uniware.core.api.jabong.JabongQualityRejectItemResponse;
import com.uniware.core.api.jabong.JabongWholesaleCreateShipmentRequest;
import com.uniware.core.api.jabong.JabongWholesaleCreateShipmentResponse;
import com.uniware.core.cache.FacilityCache;
import com.uniware.core.entity.Facility;
import com.uniware.core.vo.PrintTemplateVO;
import com.uniware.services.cache.PrintTemplateCache;
import com.uniware.services.inventory.IInventoryService;
import com.uniware.services.jabong.IJabongService;

@Controller
@Path("/data/jabong/")
@RequestMapping("/jabong/")
public class JabongResource {

    @Autowired
    private IJabongService    jabongService;

    @Autowired
    private ICommentsService  commentsService;

    @Autowired
    private IInventoryService inventoryService;

    @RequestMapping("inflow/items/print")
    public void printInflowReceiptItem(@RequestParam(value = "code", required = true) String purchaseOrderCode, HttpServletResponse httpResponse) throws IOException {
        JabongCreateItemsForPORequest request = new JabongCreateItemsForPORequest();
        request.setPurchaseOrderCode(purchaseOrderCode);
        JabongCreateItemsForPOResponse response = jabongService.jabongCreateItemsForPO(request);
        if (response.isSuccessful()) {
            httpResponse.setContentType("application/csv");
            httpResponse.addHeader("content-disposition", "attachment; filename=\"itemlabels.csv\"");
            Template template = CacheManager.getInstance().getCache(PrintTemplateCache.class).getTemplateByType(PrintTemplateVO.Type.ITEM_LABEL.name());
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("items", response.getItems());
            String output = template.evaluate(params);
            FileUtils.write(output.getBytes(), httpResponse.getOutputStream());
        }
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("item/accept")
    @POST
    public JabongQualityAcceptItemResponse acceptItem(JabongQualityAcceptItemRequest request) {
        JabongQualityAcceptItemResponse response = jabongService.jabongQualityAcceptItem(request);
        if (response.isSuccessful()) {
            addComment(request.getItemCode(), true, request.getAcceptReason() != null ? request.getAcceptReason() : "");
        }
        return response;
    }

    private void addComment(String itemCode, boolean qcPass, String qcPassComment) {
        Facility currentFacilty = CacheManager.getInstance().getCache(FacilityCache.class).getCurrentFacility();
        AddCommentRequest commentRequest = new AddCommentRequest();
        commentRequest.setComment("Item quality checked - " + (qcPass ? "Accepted " + qcPassComment : "Rejected"));
        commentRequest.setReferenceIdentifier(new StringBuilder(CommentsServiceImpl.Identifier.Item.prefix()).append(itemCode).append("-").append(currentFacilty.getCode()).toString());
        commentRequest.setUserId(WebContextUtils.getCurrentUser().getUser().getId());
        commentRequest.setSystemGenerated(true);
        commentsService.addComment(commentRequest);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("item/reject")
    @POST
    public JabongQualityRejectItemResponse rejectItem(JabongQualityRejectItemRequest request) {
        JabongQualityRejectItemResponse response = jabongService.jabongQualityRejectItem(request);
        if (response.isSuccessful()) {
            addComment(request.getItemCode(), false, "");
        }
        return response;
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("shipment/create")
    @POST
    public JabongWholesaleCreateShipmentResponse acceptItem(JabongWholesaleCreateShipmentRequest request) {
        JabongWholesaleCreateShipmentResponse response = jabongService.jabongWholesaleCreateShipment(request);
        return response;
    }

    @RequestMapping("inflow/items/print/labels/{itemCode}")
    public void printLabels(@PathVariable("itemCode") String itemCode, HttpServletResponse response) throws IOException {
        response.setContentType("application/csv");
        response.addHeader("content-disposition", "attachment; filename=\"itemlabels.csv\"");
        String output = jabongService.getItemLabelHtml(itemCode);
        FileUtils.write(output.getBytes(), response.getOutputStream());
    }
}
