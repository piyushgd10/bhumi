/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIALUse is subject to license terms.
 *  
 *  @version     1.0, Dec 10, 2011
 *  @author singla
 */
package com.uniware.web.controller;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.unifier.core.api.validation.WsError;
import com.unifier.web.utils.WebContextUtils;
import com.uniware.core.api.shipping.EditShippingPackageRequest;
import com.uniware.core.api.shipping.EditShippingPackageResponse;
import com.uniware.core.api.shipping.EditShippingPackageTrackingNumberRequest;
import com.uniware.core.api.shipping.EditShippingPackageTrackingNumberResponse;
import com.uniware.core.api.shipping.MergeShippingPackagesRequest;
import com.uniware.core.api.shipping.MergeShippingPackagesResponse;
import com.uniware.core.api.shipping.SplitShippingPackageRequest;
import com.uniware.core.api.shipping.SplitShippingPackageResponse;
import com.uniware.core.api.warehouse.SearchShipmentRequest;
import com.uniware.core.api.warehouse.SearchShipmentResponse;
import com.uniware.services.shipping.IShippingService;

/**
 * @author singla
 */
@Controller
@Path("/data/shipping/")
public class ShippingController {

    private static final Logger LOG              = LoggerFactory.getLogger(ShippingController.class);

    @Autowired
    private IShippingService    shippingService;

    public static final String  CREATE_MANIFEST  = "shipping/createManifest";
    public static final String  EDIT_MANIFEST    = "shipping/editManifest";
    public static final String  RECENT_MANIFESTS = "shipping/recentManifests";
    public static final String  SCAN_SHIPMENT    = "shipping/scanShipment";
    public static final String  SEARCH_MANIFEST  = "shipping/searchManifest";

    @RequestMapping("/shipping/createManifest")
    public String createManifest() {
        return CREATE_MANIFEST;
    }

    @RequestMapping("/shipping/editManifest")
    public String editManifest() {
        return EDIT_MANIFEST;
    }

    @RequestMapping("/shipping/recentManifests")
    public String recentManifests() {
        return RECENT_MANIFESTS;
    }

    @RequestMapping("/shipping/searchManifest")
    public String searchManifest() {
        return SEARCH_MANIFEST;
    }

    @RequestMapping("/shipping/scanShipment")
    public String scanShipment() {
        return SCAN_SHIPMENT;
    }

    @RequestMapping("/shipping/thirdPartyShipment")
    public String scanThirdPartyShipment() {
        return "inflow/thirdparty/thirdPartyScanShipment";
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("search")
    @POST
    public SearchShipmentResponse searchShipment(SearchShipmentRequest request) {
        return shippingService.searchShipment(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("splitPackage")
    @POST
    public SplitShippingPackageResponse splitShippingPackage(SplitShippingPackageRequest request) {
        request.setUserId(WebContextUtils.getCurrentUser().getUser().getId());
        return shippingService.splitShippingPackage(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/package/update")
    @POST
    public EditShippingPackageResponse editShippingPackage(EditShippingPackageRequest request) {
        try {
            return shippingService.editShippingPackage(request);
        } catch (Throwable e) {
            LOG.error("Exception while updating package", e);
            EditShippingPackageResponse response = new EditShippingPackageResponse();
            response.addError(new WsError(90001, e.getMessage()));
            return response;
        }
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/package/detail/update")
    @POST
    public EditShippingPackageResponse editShippingPackageDetail(EditShippingPackageRequest request) {
        return shippingService.editShippingPackageDetail(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/package/detail/update/customFields")
    @POST
    public EditShippingPackageResponse editShippingPackageCustomFields(EditShippingPackageRequest request) {
        return shippingService.editShippingPackageCustomFields(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/package/detail/update/awbNumber")
    @POST
    public EditShippingPackageTrackingNumberResponse editAwbNumber(EditShippingPackageTrackingNumberRequest request) {
        request.setUserId(WebContextUtils.getCurrentUser().getUser().getId());
        return shippingService.editShippingPackageTrackingNumber(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/packages/merge")
    @POST
    public MergeShippingPackagesResponse mergeShippingPackages(MergeShippingPackagesRequest request) {
        request.setUserId(WebContextUtils.getCurrentUser().getUser().getId());
        return shippingService.mergeShippingPackages(request);
    }

}