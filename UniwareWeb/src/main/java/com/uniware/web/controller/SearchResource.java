/*
 *  Copyright 2013 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Sep 17, 2013
 *  @author sunny
 */
package com.uniware.web.controller;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.unifier.services.search.IGlobalSearchService;
import com.uniware.core.api.search.GetAllSearchTypesResponse;
import com.uniware.core.api.search.GlobalSearchRequest;
import com.uniware.core.api.search.GlobalSearchResponse;
import com.uniware.core.utils.UserContext;

@Controller
@Path("/data/search/")
public class SearchResource {

    @Autowired
    private IGlobalSearchService searchService;

    @Produces(MediaType.APPLICATION_JSON)
    @Path("getAllSearchTypes")
    @GET
    public GetAllSearchTypesResponse getAllSearchTypes() {
        String productCode = UserContext.current().getTenant().getProduct().getCode();
        return searchService.getAllSearchTypes(productCode);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("globalSearch")
    @POST
    public GlobalSearchResponse globalSearch(GlobalSearchRequest request) {
        return searchService.globalSearch(request);
    }
}
