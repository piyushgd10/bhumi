/*
 *  Copyright 2013 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 22-Apr-2013
 *  @author praveeng
 */
package com.uniware.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class VendorInvoiceController {

    @RequestMapping("/vendorInvoice/createVendorInvoice")
    public String createVendorInvoice() {
        return "/vendorInvoice/createVendorInvoice";
    }
}
