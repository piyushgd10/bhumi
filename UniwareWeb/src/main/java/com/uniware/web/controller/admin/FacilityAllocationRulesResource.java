/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Oct 22, 2012
 *  @author Pankaj
 */
package com.uniware.web.controller.admin;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.uniware.core.api.facility.CreateFacilityAllocationRuleRequest;
import com.uniware.core.api.facility.CreateFacilityAllocationRuleResponse;
import com.uniware.core.api.facility.DeleteFacilityAllocationRuleRequest;
import com.uniware.core.api.facility.DeleteFacilityAllocationRuleResponse;
import com.uniware.core.api.facility.EditFacilityAllocationRuleRequest;
import com.uniware.core.api.facility.EditFacilityAllocationRuleResponse;
import com.uniware.core.api.facility.ReorderFacilityAllocationRulesRequest;
import com.uniware.core.api.facility.ReorderFacilityAllocationRulesResponse;
import com.uniware.services.warehouse.IFacilityService;

@Controller
@Path("/data/admin/configuration/")
public class FacilityAllocationRulesResource {

    @Autowired
    IFacilityService facilityService;

    @Produces(MediaType.APPLICATION_JSON)
    @Path("facilityAllocationRule/create")
    @POST
    public CreateFacilityAllocationRuleResponse createAllocationRule(CreateFacilityAllocationRuleRequest request) {
        CreateFacilityAllocationRuleResponse response = new CreateFacilityAllocationRuleResponse();
        response = facilityService.createAllocationRule(request);
        return response;

    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("facilityAllocationRule/edit")
    @POST
    public EditFacilityAllocationRuleResponse editAllocationRule(EditFacilityAllocationRuleRequest request) {
        EditFacilityAllocationRuleResponse response = new EditFacilityAllocationRuleResponse();
        response = facilityService.editAllocationRule(request);
        return response;

    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("facilityAllocationRule/delete")
    @POST
    public DeleteFacilityAllocationRuleResponse deleteAllocationRule(DeleteFacilityAllocationRuleRequest request) {
        DeleteFacilityAllocationRuleResponse response = new DeleteFacilityAllocationRuleResponse();
        response = facilityService.deleteAllocationRule(request);
        return response;
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("facilityAllocationRule/reorder")
    @POST
    public ReorderFacilityAllocationRulesResponse reorderAllocationRules(ReorderFacilityAllocationRulesRequest request) {
        return facilityService.reorderAllocationRules(request);
    }

}
