/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 26-Mar-2012
 *  @author vibhu
 */
package com.uniware.web.controller;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.uniware.core.api.saleorder.CreateSaleOrderAlternateRequest;
import com.uniware.core.api.saleorder.CreateSaleOrderAlternateResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.unifier.core.cache.CacheManager;
import com.unifier.web.utils.WebContextUtils;
import com.uniware.core.api.party.ActivateVendorAgreementRequest;
import com.uniware.core.api.party.ActivateVendorAgreementResponse;
import com.uniware.core.api.party.AddOrEditVendorAgreementRequest;
import com.uniware.core.api.party.AddOrEditVendorAgreementResponse;
import com.uniware.core.api.party.CreatePartyAddressRequest;
import com.uniware.core.api.party.CreatePartyAddressResponse;
import com.uniware.core.api.party.CreatePartyContactRequest;
import com.uniware.core.api.party.CreatePartyContactResponse;
import com.uniware.core.api.party.CreateVendorAgreementRequest;
import com.uniware.core.api.party.CreateVendorAgreementResponse;
import com.uniware.core.api.party.CreateVendorItemTypeRequest;
import com.uniware.core.api.party.CreateVendorItemTypeResponse;
import com.uniware.core.api.party.CreateVendorRequest;
import com.uniware.core.api.party.CreateVendorResponse;
import com.uniware.core.api.party.DeletePartyContactRequest;
import com.uniware.core.api.party.DeletePartyContactResponse;
import com.uniware.core.api.party.EditPartyAddressRequest;
import com.uniware.core.api.party.EditPartyAddressResponse;
import com.uniware.core.api.party.EditPartyContactRequest;
import com.uniware.core.api.party.EditPartyContactResponse;
import com.uniware.core.api.party.EditVendorItemTypeRequest;
import com.uniware.core.api.party.EditVendorItemTypeResponse;
import com.uniware.core.api.party.EditVendorRequest;
import com.uniware.core.api.party.EditVendorResponse;
import com.uniware.core.api.party.GetVendorRequest;
import com.uniware.core.api.party.GetVendorResponse;
import com.uniware.core.api.party.SearchVendorItemTypesRequest;
import com.uniware.core.api.party.SearchVendorItemTypesResponse;
import com.uniware.core.api.party.SearchVendorRequest;
import com.uniware.core.api.party.SearchVendorResponse;
import com.uniware.core.api.party.dto.VendorAgreementDTO;
import com.uniware.core.api.purchase.AddItemsToPurchaseCartRequest;
import com.uniware.core.api.purchase.AddItemsToPurchaseCartResponse;
import com.uniware.core.api.purchase.AddUpdateReorderConfigRequest;
import com.uniware.core.api.purchase.AddUpdateReorderConfigResponse;
import com.uniware.core.api.purchase.CheckoutPurchaseCartRequest;
import com.uniware.core.api.purchase.CheckoutPurchaseCartResponse;
import com.uniware.core.api.purchase.GetBackOrderItemsRequest;
import com.uniware.core.api.purchase.GetBackOrderItemsResponse;
import com.uniware.core.api.purchase.GetReorderItemsRequest;
import com.uniware.core.api.purchase.GetReorderItemsResponse;
import com.uniware.core.api.purchase.PurchaseCartDTO;
import com.uniware.core.api.purchase.RemovePurchaseCartItemsRequest;
import com.uniware.core.api.purchase.RemovePurchaseCartItemsResponse;
import com.uniware.core.api.purchase.RemoveVendorItemsFromCartRequest;
import com.uniware.core.api.purchase.RemoveVendorItemsFromCartResponse;
import com.uniware.core.api.purchase.SearchInflowReceiptRequest;
import com.uniware.core.api.purchase.SearchInflowReceiptResponse;
import com.uniware.core.api.purchase.SearchItemTypeBackOrdersRequest;
import com.uniware.core.api.purchase.SearchItemTypeBackOrdersResponse;
import com.uniware.core.api.purchase.SearchItemTypeForCartRequest;
import com.uniware.core.api.purchase.SearchItemTypeForCartResponse;
import com.uniware.core.api.saleorder.CreateSaleOrderItemAlternateRequest;
import com.uniware.core.api.saleorder.CreateSaleOrderItemAlternateResponse;
import com.uniware.core.api.warehouse.SearchItemTypesRequest;
import com.uniware.core.api.warehouse.SearchItemTypesResponse;
import com.uniware.services.cache.VendorCache;
import com.uniware.services.party.IPartyService;
import com.uniware.services.purchase.IPurchaseService;
import com.uniware.services.saleorder.ISaleOrderService;
import com.uniware.services.vendor.IVendorService;

@Controller
@Path("/data/procure/")
public class ProcureResource {

    @Autowired
    IVendorService    vendorService;

    @Autowired
    IPartyService     partyService;

    @Autowired
    IPurchaseService  purchaseService;

    @Autowired
    ISaleOrderService saleOrderService;

    @Produces(MediaType.APPLICATION_JSON)
    @Path("backorders/get")
    @POST
    public GetBackOrderItemsResponse getBackOrderItems(GetBackOrderItemsRequest request) {
        return purchaseService.getBackOrderItems(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("vendor/search")
    @POST
    public SearchVendorResponse searchVendor(SearchVendorRequest request) {
        return vendorService.searchVendor(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("vendor/detail/get")
    @POST
    public GetVendorResponse getVendor(GetVendorRequest request) {
        return vendorService.getVendorByCode(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("vendor/create")
    @POST
    public CreateVendorResponse createVendor(CreateVendorRequest request) {
        CreateVendorResponse response = vendorService.createVendor(request);
        if (response.isSuccessful()) {
            CacheManager.getInstance().markCacheDirty(VendorCache.class);
        }
        return response;
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("vendor/update")
    @POST
    public EditVendorResponse editVendor(EditVendorRequest request) {
        EditVendorResponse response = vendorService.editVendor(request);
        if (response.isSuccessful()) {
            CacheManager.getInstance().markCacheDirty(VendorCache.class);
        }
        return response;
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("vendor/address/create")
    @POST
    public CreatePartyAddressResponse createAddress(CreatePartyAddressRequest request) {
        return partyService.createAddress(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("vendor/address/edit")
    @POST
    public EditPartyAddressResponse editAddress(EditPartyAddressRequest request) {
        return partyService.editAddress(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("vendor/contact/create")
    @POST
    public CreatePartyContactResponse createContact(CreatePartyContactRequest request) {
        return partyService.createContact(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("vendor/contact/edit")
    @POST
    public EditPartyContactResponse editContact(EditPartyContactRequest request) {
        return partyService.editContact(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("vendor/contact/delete")
    @POST
    public DeletePartyContactResponse deleteContact(DeletePartyContactRequest request) {
        return partyService.deleteContact(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("vendor/agreement/create")
    @POST
    public CreateVendorAgreementResponse createVendorAgreement(CreateVendorAgreementRequest request) {
        return vendorService.createAgreement(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("vendor/agreement/edit")
    @POST
    public AddOrEditVendorAgreementResponse editVendorAgreement(AddOrEditVendorAgreementRequest request) {
        return vendorService.addOrEditAgreement(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("vendor/agreement/activate")
    @POST
    public ActivateVendorAgreementResponse activate(ActivateVendorAgreementRequest request) {
        return vendorService.approveVendorAgreement(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("vendorItemType/search")
    @POST
    public SearchVendorItemTypesResponse searchVendorItemType(SearchVendorItemTypesRequest request) {
        return vendorService.searchVendorItemType(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("vendorItemType/edit")
    @POST
    public EditVendorItemTypeResponse editVendorItemType(EditVendorItemTypeRequest request) {
        request.setUserId(WebContextUtils.getCurrentUser().getUser().getId());
        request.setActionType("VendorUI");
        return vendorService.editVendorItemType(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("catalog/search")
    @POST
    public SearchItemTypesResponse listItems(SearchItemTypesRequest request) {
        return vendorService.searchItemTypesByKeyword(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("vendor/itemType/create")
    @POST
    public CreateVendorItemTypeResponse createVendorItemType(CreateVendorItemTypeRequest request) {
        return vendorService.createVendorItemType(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("vendor/get/agreements")
    @GET
    public List<VendorAgreementDTO> getVendorAgreement(@QueryParam("vendorCode") String vendorCode) {
        return vendorService.getValidVendorAgreements(vendorCode);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("itemType/lookup")
    @POST
    public SearchItemTypeForCartResponse getVendorAgreement(SearchItemTypeForCartRequest request) {
        request.setUserId(WebContextUtils.getCurrentUser().getUser().getId());
        return purchaseService.searchItemType(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("itemType/addToCart")
    @POST
    public AddItemsToPurchaseCartResponse addItemsToPurchaseCart(AddItemsToPurchaseCartRequest request) {
        request.setUserId(WebContextUtils.getCurrentUser().getUser().getId());
        return purchaseService.addItemsToPurchaseCart(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("getCart")
    @GET
    public PurchaseCartDTO getPurchaseCart() {
        Integer userId = WebContextUtils.getCurrentUser().getUser().getId();
        return purchaseService.getPurchaseCart(userId);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/cart/removeItem")
    @POST
    public RemovePurchaseCartItemsResponse removePurchaseCartItem(RemovePurchaseCartItemsRequest request) {
        request.setUserId(WebContextUtils.getCurrentUser().getUser().getId());
        return purchaseService.removePurchaseCartItem(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/cart/checkout")
    @POST
    public CheckoutPurchaseCartResponse checkoutPurchaseCart(CheckoutPurchaseCartRequest request) {
        request.setUserId(WebContextUtils.getCurrentUser().getUser().getId());
        return purchaseService.checkoutPurchaseCart(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/grn/search")
    @POST
    public SearchInflowReceiptResponse searchInflowReceipt(SearchInflowReceiptRequest request) {
        return purchaseService.searchInflowReceipt(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("backorders/itemType/search")
    @POST
    public SearchItemTypeBackOrdersResponse getItemTypeBackOrders(SearchItemTypeBackOrdersRequest request) {
        return purchaseService.getItemTypeBackOrders(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("alternate/saleOrderItem/create")
    @POST
    public CreateSaleOrderItemAlternateResponse createSaleOrderItemAlternate(CreateSaleOrderItemAlternateRequest request) {
        request.setUserId(WebContextUtils.getCurrentUser().getUser().getId());
        return saleOrderService.createSaleOrderItemAlternate(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("alternate/saleOrder/create")
    @POST
    public CreateSaleOrderAlternateResponse createSaleOrderAlternate(CreateSaleOrderAlternateRequest request) {
        request.setUserId(WebContextUtils.getCurrentUser().getUser().getId());
        return saleOrderService.createSaleOrderAlternate(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("reorders/get")
    @POST
    public GetReorderItemsResponse getBackOrderItems(GetReorderItemsRequest request) {
        return purchaseService.getReorderItemsNew(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/cart/removeVendorItems")
    @POST
    public RemoveVendorItemsFromCartResponse removeVendorItemsFromCart(RemoveVendorItemsFromCartRequest request) {
        request.setUserId(WebContextUtils.getCurrentUser().getUser().getId());
        return purchaseService.removeVendorItemsFromCart(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("reorders/update")
    @POST
    public AddUpdateReorderConfigResponse addUpdateReorderConfig(AddUpdateReorderConfigRequest request) {
        return purchaseService.persistReorderConfig(request);
    }
}
