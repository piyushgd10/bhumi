/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIALUse is subject to license terms.
 *  
 *  @version     1.0, Dec 10, 2011
 *  @author singla
 */
package com.uniware.web.controller;

import com.unifier.core.configuration.ConfigurationManager;
import com.uniware.core.entity.PurchaseOrder;
import com.uniware.core.entity.Vendor;
import com.uniware.services.configuration.CustomFieldsMetadataConfiguration;
import com.uniware.services.vendor.IVendorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author singla
 */
@Controller
public class ProcureController {

    public static final String VENDOR_PAGE    = "/procure/searchVendor";
    public static final String SEARCH_PO_PAGE = "/procure/searchPO";

    @Autowired
    IVendorService vendorService;

    @RequestMapping(value = { "/procure/searchVendor" })
    public String searchVendor(ModelMap map) {
        map.addAttribute("customFieldsJson",
                ConfigurationManager.getInstance().getConfiguration(CustomFieldsMetadataConfiguration.class).getCustomFieldsByEntityJson(Vendor.class.getName()));
        return VENDOR_PAGE;
    }

    @RequestMapping("/procure/purchaseOrder")
    public String purchaseOrder(ModelMap map) {
        map.addAttribute("customFieldsJson",
                ConfigurationManager.getInstance().getConfiguration(CustomFieldsMetadataConfiguration.class).getCustomFieldsByEntityJson(PurchaseOrder.class.getName()));
        return "/procure/purchaseOrder";
    }

    @RequestMapping("/procure/vendorItemType")
    public String vendorItemType(@RequestParam(value = "code", required = false) String code, ModelMap map) {
        if (code != null) {
            Vendor vendor = vendorService.getVendorByCode(code);
            map.addAttribute("vendorName", vendor.getName());
            map.addAttribute("vendorCode", vendor.getCode());
        }
        return "/procure/vendorItemType";
    }

    @RequestMapping("/procure/backOrders")
    public String backOrders() {
        return "/procure/backOrders";
    }

    @RequestMapping("/procure/poItems")
    public String addPurchaseOrderItem() {
        return "/procure/poItems";
    }

    @RequestMapping("/procure/poWaiting")
    public String purchareOrderWaitingForApproval() {
        return "/procure/poWaiting";
    }

    @RequestMapping("/procure/addToCart")
    public String lookup() {
        return "/procure/addToCart";
    }

    @RequestMapping("/procure/cart")
    public String cart() {
        return "/procure/viewCart";
    }

    @RequestMapping("/procure/searchPO")
    public String searchPurchaseOrders() {
        return "/procure/searchPO";
    }

    @RequestMapping("/procure/grnSearch")
    public String searchInflowReceipt() {
        return "/procure/grnSearch";
    }

    @RequestMapping("/procure/reorders")
    public String getReorders() {
        return "/procure/reorders";
    }

    @RequestMapping("/procure/reorderConfig")
    public String getReorderConfig() {
        return "/procure/reorderConfig";
    }
}