/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 02-Jun-2012
 *  @author praveeng
 */
package com.uniware.web.controller.admin;

import com.unifier.core.utils.FileUtils;
import com.unifier.core.utils.StringUtils;
import com.unifier.web.utils.WebContextUtils;
import com.uniware.core.api.inflow.AddItemDetailsRequest;
import com.uniware.core.api.inflow.AddItemDetailsResponse;
import com.uniware.core.api.inflow.GetItemDetailFieldsRequest;
import com.uniware.core.api.inflow.GetItemDetailFieldsResponse;
import com.uniware.core.api.inventory.MarkTraceableInventoryDamagedRequest;
import com.uniware.core.api.inventory.MarkTraceableInventoryDamagedResponse;
import com.uniware.core.api.transition.CreateInventoryRequest;
import com.uniware.core.api.transition.CreateInventoryResponse;
import com.uniware.core.api.transition.CreateItemsRequest;
import com.uniware.core.api.transition.CreateItemsResponse;
import com.uniware.core.api.transition.DiscardAllItemsRequest;
import com.uniware.core.api.transition.DiscardAllItemsResponse;
import com.uniware.core.api.transition.DiscardItemCodeRequest;
import com.uniware.core.api.transition.DiscardItemCodeResponse;
import com.uniware.core.api.transition.ItemTypeDTO;
import com.uniware.core.entity.Item;
import com.uniware.services.inflow.IInflowService;
import com.uniware.services.inventory.IInventoryService;
import com.uniware.services.tenant.ITenantService;
import com.uniware.services.transition.ITransitionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.io.IOException;

@Controller
@Path("/data/admin/system/")
public class TransitionResource {
    @Autowired
    private IInventoryService inventoryService;

    @Autowired
    private IInflowService inflowService;

    @Autowired
    private ITransitionService transitionService;

    @Autowired
    private ITenantService tenantService;

    @Produces(MediaType.APPLICATION_JSON)
    @Path("items/create")
    @POST
    public CreateItemsResponse createItems(CreateItemsRequest request) {
        request.setStatusCode(Item.StatusCode.IN_TRANSITION.name());
        request.setUserId(WebContextUtils.getCurrentUser().getUser().getId());
        return transitionService.createItems(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("inventory/create")
    @POST
    public CreateInventoryResponse createInventory(CreateInventoryRequest request) {
        return transitionService.createInventory(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("inventory/get/details/itemType")
    @GET
    public ItemTypeDTO getItemTypeDetails(@QueryParam("itemTypeId") Integer itemTypeId) {
        return transitionService.getItemTypeDetails(itemTypeId);
    }

    @RequestMapping("/admin/system/items/print/{itemCodes}")
    public void printItemCodes(@PathVariable("itemCodes") String itemCodes, HttpServletResponse response) throws IOException {
        response.setContentType("application/csv");
        response.addHeader("content-disposition", "attachment; filename=\"" + tenantService.getDownloadFileName("itemlabels") + "\"");
        String output = inventoryService.getItemLabelHtml(StringUtils.split(itemCodes));
        FileUtils.write(output.getBytes(), response.getOutputStream());
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("item/customFields/get")
    @POST
    public GetItemDetailFieldsResponse getGetItemDetailFields(GetItemDetailFieldsRequest request) {
        return inflowService.getItemDetailFields(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("itemsDetails/add")
    @POST
    public AddItemDetailsResponse addItemDetails(AddItemDetailsRequest request) {
        return inflowService.addItemDetails(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("itemCode/discard")
    @POST
    public DiscardItemCodeResponse discardItemCode(DiscardItemCodeRequest request) {
        return transitionService.discardItemCode(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("itemCodes/discardAll")
    @POST
    public DiscardAllItemsResponse discardAllItems(DiscardAllItemsRequest request) {
        return transitionService.discardAllItems(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("goodInventoryItemCode/damage")
    @POST
    public MarkTraceableInventoryDamagedResponse markTraceableInventoryDamaged(MarkTraceableInventoryDamagedRequest request) {
        return inventoryService.markTraceableInventoryDamaged(request);
    }
}
