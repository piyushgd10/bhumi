/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jan 12, 2012
 *  @author singla
 */
package com.uniware.web.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.unifier.web.utils.WebContextUtils;
import com.uniware.core.api.packer.AcceptPackagePutbackRequest;
import com.uniware.core.api.packer.AcceptPackagePutbackResponse;
import com.uniware.core.api.packer.AllocateItemInPackageRequest;
import com.uniware.core.api.packer.AllocateItemInPackageResponse;
import com.uniware.core.api.packer.GetPacklistRemainingQuantityRequest;
import com.uniware.core.api.packer.GetPacklistRemainingQuantityResponse;
import com.uniware.core.api.packer.MarkPicklistItemsNotFoundRequest;
import com.uniware.core.api.packer.MarkPicklistItemsNotFoundResponse;
import com.uniware.core.api.packer.ReceivePickBucketForWebRequest;
import com.uniware.core.api.packer.ReceivePickBucketForWebResponse;
import com.uniware.core.api.packer.RemoveItemFromPicklistRequest;
import com.uniware.core.api.packer.RemoveItemFromPicklistResponse;
import com.uniware.core.api.picker.CompletePicklistRequest;
import com.uniware.core.api.picker.CompletePicklistResponse;
import com.uniware.core.api.picker.EditPicklistRequest;
import com.uniware.core.api.picker.EditPicklistResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.template.Template;
import com.unifier.core.utils.FileUtils;
import com.unifier.core.utils.StringUtils;
import com.unifier.services.pdf.IPdfDocumentService;
import com.unifier.services.pdf.impl.PdfDocumentServiceImpl.PrintOptions;
import com.uniware.core.api.customizations.MarkItemDamagedOutboundQCRequest;
import com.uniware.core.api.customizations.MarkItemDamagedOutboundQCResponse;
import com.uniware.core.api.packer.AcceptPutbackItemRequest;
import com.uniware.core.api.packer.AcceptPutbackItemResponse;
import com.uniware.core.api.packer.AcceptPutbackRequest;
import com.uniware.core.api.packer.AcceptPutbackResponse;
import com.uniware.core.api.packer.AllocateItemRequest;
import com.uniware.core.api.packer.AllocateItemResponse;
import com.uniware.core.api.packer.GetPacklistRequest;
import com.uniware.core.api.packer.GetPacklistResponse;
import com.uniware.core.api.packer.PickShippingPackageRequest;
import com.uniware.core.api.packer.PickShippingPackageResponse;
import com.uniware.core.api.packer.PrintPicklistItemRequest;
import com.uniware.core.api.packer.PrintPicklistItemResponse;
import com.uniware.core.api.packer.ReceivePicklistRequest;
import com.uniware.core.api.packer.ReceivePicklistResponse;
import com.uniware.core.api.packer.SuggestItemAllocationRequest;
import com.uniware.core.api.packer.SuggestItemAllocationResponse;
import com.uniware.core.api.picker.CompletePickBatchRequest;
import com.uniware.core.api.picker.CompletePickBatchResponse;
import com.uniware.core.api.picker.ReceivePickBucketRequest;
import com.uniware.core.api.picker.ReceivePickBucketResponse;
import com.uniware.core.api.shipping.SendShippingPackageForCustomizationRequest;
import com.uniware.core.api.shipping.SendShippingPackageForCustomizationResponse;
import com.uniware.core.vo.PrintTemplateVO;
import com.uniware.core.vo.SamplePrintTemplateVO;
import com.uniware.services.cache.PrintTemplateCache;
import com.uniware.services.cache.SamplePrintTemplateCache;
import com.uniware.services.configuration.SystemConfiguration;
import com.uniware.services.customizations.impl.LenskartServiceImpl;
import com.uniware.services.packer.IPackerService;
import com.uniware.services.shipping.IShippingService;
import com.uniware.services.staging.IShipmentStagingService;
import com.uniware.services.tenant.ITenantService;

/**
 * @author singla
 */
@Controller
@Path("/data/oms/packer/")
public class PackerResource {

    @Autowired
    private IPackerService          packerService;

    @Autowired
    private LenskartServiceImpl     lenskartService;

    @Autowired
    private IShippingService        shippingService;

    @Autowired
    private IPdfDocumentService     pdfDocumentService;

    @Autowired
    private ITenantService          tenantService;

    @Autowired
    private IShipmentStagingService shipmentStagingService;

    @Produces(MediaType.APPLICATION_JSON)
    @Path("packlist/fetch")
    @POST
    public GetPacklistResponse getPacklist(GetPacklistRequest getPacklistRequest) {
        return packerService.getPacklist(getPacklistRequest);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("allocate")
    @POST
    public AllocateItemInPackageResponse allocateItem(AllocateItemInPackageRequest allocateItemRequest) {
        allocateItemRequest.setUserId(WebContextUtils.getCurrentUser().getUser().getId());
        return packerService.allocateItemInPackage(allocateItemRequest);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("suggest/allocation")
    @POST
    public SuggestItemAllocationResponse suggestItemAllocation(SuggestItemAllocationRequest request) {
        return packerService.suggestItemAllocation(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("picklist/remove/item")
    @POST
    public RemoveItemFromPicklistResponse removeItemFromPicklist(RemoveItemFromPicklistRequest request) {
        request.setUserId(WebContextUtils.getCurrentUser().getUser().getId());
        return packerService.removeItemFromPicklist(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("picklist/items/marknotfound")
    @POST
    public MarkPicklistItemsNotFoundResponse markPicklistItemsNotFound(MarkPicklistItemsNotFoundRequest request) {
        request.setUserId(WebContextUtils.getCurrentUser().getUser().getId());
        return packerService.markPicklistItemsNotFound(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("package/putback")
    @POST
    public AcceptPackagePutbackResponse acceptPackagePutback(AcceptPackagePutbackRequest request) {
        return packerService.acceptPackagePutback(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("packlist/putback")
    @POST
    public AcceptPutbackResponse acceptPutback(AcceptPutbackRequest request) {
        return packerService.acceptPutback(request);
    }

    //TODO
//    @Produces(MediaType.APPLICATION_JSON)
//    @Path("pickBucket/receive")
//    @POST
//    public ReceivePickBucketResponse receivePickBucketAtInvoicing(ReceivePickBucketRequest request) {
//        return packerService.receivePickBucketAtInvoicing(request);
//    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("picklist/complete")
    @POST
    public CompletePicklistResponse completePicklist(CompletePicklistRequest request) {
        return packerService.completePicklist(request);
    }


    @Produces(MediaType.APPLICATION_JSON)
    @Path("pickBatch/complete")
    @POST
    public CompletePickBatchResponse completePickBatch(CompletePickBatchRequest request) {
        return shipmentStagingService.completePickBatch(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("putbackitem/accept")
    @POST
    public AcceptPutbackItemResponse acceptPutbackItem(AcceptPutbackItemRequest request) {
        return packerService.acceptPutbackItem(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("workorder")
    @POST
    public SendShippingPackageForCustomizationResponse sendShippingPackageForCustomization(SendShippingPackageForCustomizationRequest request) {
        return shippingService.sendShippingPackageForCustomization(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("invoice/print")
    @POST
    public PickShippingPackageResponse pickShippingPackage(PickShippingPackageRequest request) {
        return packerService.pickShippingPackage(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("outboundqc/damage")
    @POST
    public MarkItemDamagedOutboundQCResponse markItemDamagedOutboundQC(MarkItemDamagedOutboundQCRequest request) {
        return lenskartService.markItemDamagedOutboundQC(request);
    }

    @RequestMapping("/oms/packer/show/{shippingPackageCodes}")
    public void showWorkOrder(@PathVariable("shippingPackageCodes") String shippingPackageCodes, HttpServletResponse response) throws IOException {
        StringBuilder builder = new StringBuilder();
        for (String shippingPackageCode : StringUtils.split(shippingPackageCodes, "-")) {
            Template template = CacheManager.getInstance().getCache(PrintTemplateCache.class).getTemplateByType(PrintTemplateVO.Type.WORKORDER.name());
            String workOrderHtml = packerService.getWorkorderHtml(shippingPackageCode, template);
            if (StringUtils.isNotBlank(workOrderHtml)) {
                builder.append(workOrderHtml);
            }
        }
        if (builder.length() > 0) {
            response.setContentType("application/pdf");
            PrintTemplateVO workOrderTemplate = CacheManager.getInstance().getCache(PrintTemplateCache.class).getPrintTemplateByType(PrintTemplateVO.Type.WORKORDER);
            SamplePrintTemplateVO samplePrintTemplate = CacheManager.getInstance().getCache(SamplePrintTemplateCache.class).getSamplePrintTemplate(
                    workOrderTemplate.getSamplePrintTemplateCode());
            pdfDocumentService.writeHtmlToPdf(response.getOutputStream(), builder.toString(), new PrintOptions(samplePrintTemplate));
        }
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("picklist/receive")
    @POST
    public ReceivePicklistResponse receivePicklist(ReceivePicklistRequest request) {
        return packerService.receivePicklist(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("picklist/edit")
    @POST
    public EditPicklistResponse editPicklist(EditPicklistRequest editPicklistRequest) {
        return packerService.editPicklist(editPicklistRequest);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("packlist/remaining")
    @POST
    public GetPacklistRemainingQuantityResponse getPacklistRemainingQuantity(GetPacklistRemainingQuantityRequest request) {
        return packerService.getPacklistRemainingQuantity(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("pickBucket/receive")
    @POST
    public ReceivePickBucketForWebResponse receivePickBucketAtInvoicing(ReceivePickBucketForWebRequest request) {
        return packerService.receivePickBucketAtInvoicing(request);
    }

    @RequestMapping("/oms/picklist/receive/print/{soCode}/{soiCode}")
    public void printPicklistItem(@PathVariable("soCode") String soCode, @PathVariable("soiCode") String soiCode, HttpServletResponse response) throws IOException {
        if (ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).downloadCsvOnReceivePicklist()) {
            response.setContentType("application/csv");
            response.addHeader("content-disposition", "attachment; filename=\"" + tenantService.getDownloadFileName("picklist-item") + "\"");
            PrintPicklistItemRequest request = new PrintPicklistItemRequest();
            request.setSaleOrderCode(soCode);
            request.setSaleOrderItemCode(soiCode);
            PrintPicklistItemResponse picklistItemResponse = packerService.printPicklistItem(request);
            if (picklistItemResponse.isSuccessful()) {
                Template template = CacheManager.getInstance().getCache(PrintTemplateCache.class).getTemplateByType(PrintTemplateVO.Type.PICKLIST_ITEM_CSV.name());
                Map<String, Object> params = new HashMap<>();
                params.put("saleOrder", picklistItemResponse.getSaleOrder());
                params.put("saleOrderItem", picklistItemResponse.getSaleOrderItem());
                params.put("serialNumberInOrder", picklistItemResponse.getSerialNumberInOrder());
                params.put("serialNumberInPackage", picklistItemResponse.getSerialNumberInPackage());
                FileUtils.write(template.evaluate(params).getBytes(), response.getOutputStream());
            }
        } else {
            response.sendError(HttpServletResponse.SC_FORBIDDEN);
        }
    }
}
