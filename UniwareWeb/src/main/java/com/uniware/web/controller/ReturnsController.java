/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 06-Feb-2012
 *  @author vibhu
 */
package com.uniware.web.controller;

import com.unifier.core.cache.CacheManager;
import com.unifier.core.template.Template;
import com.unifier.services.pdf.IPdfDocumentService;
import com.unifier.services.pdf.impl.PdfDocumentServiceImpl.PrintOptions;
import com.uniware.core.vo.PrintTemplateVO;
import com.uniware.core.vo.SamplePrintTemplateVO;
import com.uniware.services.cache.PrintTemplateCache;
import com.uniware.services.cache.SamplePrintTemplateCache;
import com.uniware.services.returns.IReturnsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
public class ReturnsController {

    @Autowired
    IReturnsService returnsService;

    @Autowired
    private IPdfDocumentService pdfDocumentService;

    @RequestMapping("/returns/reversePickup/print/{reversePickupCode}")
    public void showInvoice(@PathVariable("reversePickupCode") String reversePickupCode, HttpServletResponse response) throws IOException {
        Template template = CacheManager.getInstance().getCache(PrintTemplateCache.class).getTemplateByType(PrintTemplateVO.Type.REVERSE_PICKUP.name());
        String redispatchHtml = returnsService.getRedispatchHtml(reversePickupCode, template);

        response.setContentType("application/pdf");
        PrintTemplateVO invoiceTemplate = CacheManager.getInstance().getCache(PrintTemplateCache.class).getPrintTemplateByType(PrintTemplateVO.Type.REVERSE_PICKUP);
        SamplePrintTemplateVO samplePrintTemplate = CacheManager.getInstance().getCache(SamplePrintTemplateCache.class).getSamplePrintTemplate(
                invoiceTemplate.getSamplePrintTemplateCode());
        pdfDocumentService.writeHtmlToPdf(response.getOutputStream(), redispatchHtml, new PrintOptions(samplePrintTemplate));
    }
}
