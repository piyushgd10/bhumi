/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 *  @version     1.0, Apr 4, 2012
 *  @author praveeng
 */
package com.uniware.web.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.unifier.services.pdf.impl.PdfDocumentServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.template.Template;
import com.unifier.core.utils.FileUtils;
import com.unifier.core.utils.StringUtils;
import com.unifier.services.pdf.IPdfDocumentService;
import com.unifier.services.pdf.impl.PdfDocumentServiceImpl.PrintOptions;
import com.unifier.web.utils.WebContextUtils;
import com.uniware.core.api.inflow.AddEditVendorInvoiceItemRequest;
import com.uniware.core.api.inflow.AddEditVendorInvoiceItemResponse;
import com.uniware.core.api.inflow.AddInflowReceiptToVendorInvoiceRequest;
import com.uniware.core.api.inflow.AddInflowReceiptToVendorInvoiceResponse;
import com.uniware.core.api.inflow.AddItemDetailsRequest;
import com.uniware.core.api.inflow.AddItemDetailsResponse;
import com.uniware.core.api.inflow.AddItemToInflowReceiptRequest;
import com.uniware.core.api.inflow.AddItemToInflowReceiptResponse;
import com.uniware.core.api.inflow.AddItemToInflowReceiptWapRequest;
import com.uniware.core.api.inflow.AddItemToInflowReceiptWapResponse;
import com.uniware.core.api.inflow.AddOrEditInflowReceiptItemRequest;
import com.uniware.core.api.inflow.AddOrEditInflowReceiptItemResponse;
import com.uniware.core.api.inflow.AddSaleOrderItemDetailRequest;
import com.uniware.core.api.inflow.AddSaleOrderItemDetailResponse;
import com.uniware.core.api.inflow.CompleteInflowReceiptItemsQCRequest;
import com.uniware.core.api.inflow.CompleteInflowReceiptItemsQCResponse;
import com.uniware.core.api.inflow.CompleteVendorInvoiceRequest;
import com.uniware.core.api.inflow.CompleteVendorInvoiceResponse;
import com.uniware.core.api.inflow.CreateDebitForCreditVendorInvoiceRequest;
import com.uniware.core.api.inflow.CreateDebitForCreditVendorInvoiceResponse;
import com.uniware.core.api.inflow.CreateInflowReceiptRequest;
import com.uniware.core.api.inflow.CreateInflowReceiptResponse;
import com.uniware.core.api.inflow.CreateItemLabelsRequest;
import com.uniware.core.api.inflow.CreateItemLabelsResponse;
import com.uniware.core.api.inflow.CreateReceiptItemCodesRequest;
import com.uniware.core.api.inflow.CreateReceiptItemCodesResponse;
import com.uniware.core.api.inflow.CreateVendorCreditInvoiceRequest;
import com.uniware.core.api.inflow.CreateVendorCreditInvoiceResponse;
import com.uniware.core.api.inflow.CreateVendorDebitInvoiceRequest;
import com.uniware.core.api.inflow.CreateVendorDebitInvoiceResponse;
import com.uniware.core.api.inflow.DiscardInflowReceiptRequest;
import com.uniware.core.api.inflow.DiscardInflowReceiptResponse;
import com.uniware.core.api.inflow.DiscardTraceableInflowReceiptItemRequest;
import com.uniware.core.api.inflow.DiscardTraceableInflowReceiptItemResponse;
import com.uniware.core.api.inflow.DiscardVendorInvoiceRequest;
import com.uniware.core.api.inflow.DiscardVendorInvoiceResponse;
import com.uniware.core.api.inflow.EditInflowReceiptRequest;
import com.uniware.core.api.inflow.EditInflowReceiptResponse;
import com.uniware.core.api.inflow.EditVendorInvoiceRequest;
import com.uniware.core.api.inflow.EditVendorInvoiceResponse;
import com.uniware.core.api.inflow.GetInflowReceiptRequest;
import com.uniware.core.api.inflow.GetInflowReceiptResponse;
import com.uniware.core.api.inflow.GetItemDetailFieldsRequest;
import com.uniware.core.api.inflow.GetItemDetailFieldsResponse;
import com.uniware.core.api.inflow.GetPOInflowReceiptsRequest;
import com.uniware.core.api.inflow.GetPOInflowReceiptsResponse;
import com.uniware.core.api.inflow.GetVendorInvoiceRequest;
import com.uniware.core.api.inflow.GetVendorInvoiceResponse;
import com.uniware.core.api.inflow.GetVendorInvoicesRequest;
import com.uniware.core.api.inflow.GetVendorInvoicesResponse;
import com.uniware.core.api.inflow.PrintLabelsRequest;
import com.uniware.core.api.inflow.RejectNonTraceableItemRequest;
import com.uniware.core.api.inflow.RejectNonTraceableItemResponse;
import com.uniware.core.api.inflow.RejectTraceableItemRequest;
import com.uniware.core.api.inflow.RejectTraceableItemResponse;
import com.uniware.core.api.inflow.RemoveVendorInvoiceItemRequest;
import com.uniware.core.api.inflow.RemoveVendorInvoiceItemResponse;
import com.uniware.core.api.inflow.SearchReceiptRequest;
import com.uniware.core.api.inflow.SearchReceiptResponse;
import com.uniware.core.api.inflow.SendInflowReceiptForQCRequest;
import com.uniware.core.api.inflow.SendInflowReceiptForQCResponse;
import com.uniware.core.api.inflow.UpdateTraceableInflowReceiptItemRequest;
import com.uniware.core.api.inflow.UpdateTraceableInflowReceiptItemResponse;
import com.uniware.core.api.inventory.AddItemLabelsRequest;
import com.uniware.core.api.inventory.AddItemLabelsResponse;
import com.uniware.core.api.inventory.InventoryAdjustmentRequest;
import com.uniware.core.api.inventory.InventoryAdjustmentResponse;
import com.uniware.core.api.purchase.GetPurchaseOrderRequest;
import com.uniware.core.api.purchase.GetPurchaseOrderResponse;
import com.uniware.core.entity.InflowReceipt;
import com.uniware.core.entity.InflowReceiptItem;
import com.uniware.core.entity.Item;
import com.uniware.core.vo.PrintTemplateVO;
import com.uniware.core.vo.SamplePrintTemplateVO;
import com.uniware.services.cache.PrintTemplateCache;
import com.uniware.services.cache.SamplePrintTemplateCache;
import com.uniware.services.configuration.SystemConfiguration;
import com.uniware.services.configuration.SystemConfiguration.TraceabilityLevel;
import com.uniware.services.inflow.IInflowService;
import com.uniware.services.inflow.IVendorInvoiceService;
import com.uniware.services.inventory.IInventoryService;
import com.uniware.services.purchase.IPurchaseService;
import com.uniware.services.tenant.ITenantService;

@Controller
@Path("/data/inflow/")
public class InflowResource {

    @Autowired
    private IInventoryService     inventoryService;

    @Autowired
    private IInflowService        inflowService;

    @Autowired
    private IVendorInvoiceService vendorInvoiceService;

    @Autowired
    private IPurchaseService      purchaseService;

    @Autowired
    private IPdfDocumentService   pdfDocumentService;

    @Autowired
    private ITenantService        tenantService;

    private static final String   ITEM_LABEL_DOWNLOAD_FILE_PREFIX = "itemlabels";

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/inventory/adjust")
    @POST
    public InventoryAdjustmentResponse adjustInventory(InventoryAdjustmentRequest request) {
        request.setUserId(WebContextUtils.getCurrentUser().getUser().getId());
        return inventoryService.adjustInventory(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("po/fetch")
    @POST
    public GetPurchaseOrderResponse getPurchaseOrderToReceive(GetPurchaseOrderRequest request) {
        return purchaseService.getPurchaseOrderDetail(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("po/receive")
    @POST
    public CreateInflowReceiptResponse createInflowReceipt(CreateInflowReceiptRequest request) {
        request.getWsGRN().setUserId(WebContextUtils.getCurrentUser().getUser().getId());
        return inflowService.createInflowReceipt(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("edit/grn")
    @POST
    public EditInflowReceiptResponse editInflowReceipt(EditInflowReceiptRequest request) {
        request.getWsGRN().setUserId(WebContextUtils.getCurrentUser().getUser().getId());
        return inflowService.editInflowReceipt(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("po/receive/item")
    @POST
    public AddOrEditInflowReceiptItemResponse addOrEditInflowReceiptItem(AddOrEditInflowReceiptItemRequest request) {
        return inflowService.addOrEditInflowReceiptItem(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("po/receive/sendForQC")
    @POST
    public SendInflowReceiptForQCResponse sendInflowReceiptForQC(SendInflowReceiptForQCRequest request) {
        return inflowService.sendInflowReceiptForQC(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("po/receive/createItems")
    @POST
    public CreateReceiptItemCodesResponse createItemCodes(CreateReceiptItemCodesRequest request) {
        return inflowService.createItemCodes(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("po/inflowReceipts/fetch")
    @POST
    public GetPOInflowReceiptsResponse getAllInflowReceipts(GetPOInflowReceiptsRequest request) {
        return inflowService.getAllInflowReceipts(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("receipt/fetch")
    @POST
    public GetInflowReceiptResponse getPurchaseOrderToReceive(GetInflowReceiptRequest request) {
        return inflowService.getInflowReceipt(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("receipt/item/nontraceable/edit")
    @POST
    public RejectNonTraceableItemResponse editNonTraceableInflowReceiptItem(RejectNonTraceableItemRequest request) {
        return inflowService.rejectNonTraceableItem(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("receipt/item/add")
    @POST
    public AddItemToInflowReceiptResponse addItemToInflowReceipt(AddItemToInflowReceiptRequest request) {
        return inflowService.addItemToInflowReceipt(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("receipt/item/traceable/edit")
    @POST
    public UpdateTraceableInflowReceiptItemResponse editInflowReceiptItem(UpdateTraceableInflowReceiptItemRequest request) {
        return inflowService.updateTraceableItem(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("item/reject")
    @POST
    public RejectTraceableItemResponse rejectTraceableItem(RejectTraceableItemRequest request) {
        return inflowService.rejectTraceableItem(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("receipt/search")
    @POST
    public SearchReceiptResponse searchReceipt(SearchReceiptRequest request) {
        return inflowService.searchReceipt(request);
    }

    @RequestMapping("/inflow/items/print/{receiptItemId}")
    public void printInflowReceiptItem(@PathVariable("receiptItemId") Integer receiptItemId, HttpServletResponse response) throws IOException {
        TraceabilityLevel traceabilityLevel = ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).getTraceabilityLevel();
        if (traceabilityLevel == TraceabilityLevel.ITEM) {
            List<Item> items = inflowService.getItemsByReceiptItemId(receiptItemId);
            if (items != null) {
                response.setContentType("application/csv");
                response.addHeader("content-disposition", "attachment; filename=\"" + tenantService.getDownloadFileName(ITEM_LABEL_DOWNLOAD_FILE_PREFIX) + "\"");
                Template template = CacheManager.getInstance().getCache(PrintTemplateCache.class).getTemplateByType(PrintTemplateVO.Type.ITEM_LABEL.name());
                Map<String, Object> params = new HashMap<String, Object>();
                params.put("items", items);
                String output = template.evaluate(params);
                FileUtils.write(output.getBytes(), response.getOutputStream());
            }
        } else {
            InflowReceiptItem inflowReceiptItem = inflowService.getInflowReceiptItemById(receiptItemId);
            response.setContentType("application/csv");
            response.addHeader("content-disposition", "attachment; filename=\"" + tenantService.getDownloadFileName(ITEM_LABEL_DOWNLOAD_FILE_PREFIX) + "\"");
            Template template = CacheManager.getInstance().getCache(PrintTemplateCache.class).getTemplateByType(PrintTemplateVO.Type.ITEM_LABEL.name());
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("inflowReceiptItem", inflowReceiptItem);
            String output = template.evaluate(params);
            FileUtils.write(output.getBytes(), response.getOutputStream());
        }
    }

    @RequestMapping("/inflow/items/print/labels/{itemCodes}")
    public void printLabels(@PathVariable("itemCodes") String itemCodes, HttpServletResponse response) throws IOException {
        response.setContentType("application/csv");
        response.addHeader("content-disposition", "attachment; filename=\"" + tenantService.getDownloadFileName(ITEM_LABEL_DOWNLOAD_FILE_PREFIX) + "\"");
        String output = inventoryService.getItemLabelHtml(StringUtils.split(itemCodes));
        FileUtils.write(output.getBytes(), response.getOutputStream());
    }

    @RequestMapping("/inflow/items/printing/labels")
    @ResponseBody
    public void printLabelsRequest(PrintLabelsRequest request, HttpServletResponse hrequest, HttpServletResponse response) throws IOException, ClassNotFoundException {
        response.setContentType("application/csv");
        response.addHeader("content-disposition", "attachment; filename=\"" + tenantService.getDownloadFileName(ITEM_LABEL_DOWNLOAD_FILE_PREFIX) + "\"");
        String output = inventoryService.getItemLabelHtml(StringUtils.split(request.getItemCodes().toString()));
        FileUtils.write(output.getBytes(), response.getOutputStream());
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("qc/complete")
    @POST
    public CompleteInflowReceiptItemsQCResponse completeInflowReceiptItemsQC(CompleteInflowReceiptItemsQCRequest request) {
        request.setUserEmail(WebContextUtils.getCurrentUser().getUser().getEmail());
        return inflowService.completeInflowReceiptItemsQC(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("discardBarcode")
    @POST
    public DiscardTraceableInflowReceiptItemResponse discardTraceableInflowReceiptItem(DiscardTraceableInflowReceiptItemRequest request) {
        return inflowService.discardTraceableInflowReceiptItem(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("item/detail/get")
    @POST
    public GetItemDetailFieldsResponse getItemDetails(GetItemDetailFieldsRequest request) {
        return inflowService.getItemDetailFields(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("item/detail/add")
    @POST
    public AddItemDetailsResponse addItemDetails(AddItemDetailsRequest request) {
        return inflowService.addItemDetails(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("item/createLabels")
    @POST
    public CreateItemLabelsResponse createItemLabels(CreateItemLabelsRequest request) {
        return inventoryService.createItemLabels(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("receipt/discard")
    @POST
    public DiscardInflowReceiptResponse discardShippingManifest(DiscardInflowReceiptRequest request) {
        request.setUserId(WebContextUtils.getCurrentUser().getUser().getId());
        return inflowService.discardInflowReceipt(request);
    }

    @Path("item/addLabels")
    @POST
    public AddItemLabelsResponse addItemLabels(AddItemLabelsRequest request) {
        return inventoryService.addItemLabels(request);
    }

    @RequestMapping("/inflow/receipt/rejectionReport/download/{inflowReceiptCode}")
    public void downloadQCRejectionReport(@PathVariable("inflowReceiptCode") String inflowReceiptCode, HttpServletResponse response) throws IOException {
        InflowReceipt inflowReceipt = inflowService.getDetailedInflowReceiptByCode(inflowReceiptCode);
        Template template = CacheManager.getInstance().getCache(PrintTemplateCache.class).getTemplateByType(PrintTemplateVO.Type.QC_REJECTION_REPORT.name());
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("inflowReceipt", inflowReceipt);
        String inflowReceiptReportHtml = template.evaluate(params);

        PrintTemplateVO qcRejectionReportTemplate = CacheManager.getInstance().getCache(PrintTemplateCache.class).getPrintTemplateByType(PrintTemplateVO.Type.QC_REJECTION_REPORT);
        SamplePrintTemplateVO samplePrintTemplate = CacheManager.getInstance().getCache(SamplePrintTemplateCache.class).getSamplePrintTemplate(
                qcRejectionReportTemplate.getSamplePrintTemplateCode());
        response.setContentType("application/pdf");
        pdfDocumentService.writeHtmlToPdf(response.getOutputStream(), inflowReceiptReportHtml, new PrintOptions(samplePrintTemplate));
    }

    @Path("wap/item/add")
    @POST
    public AddItemToInflowReceiptWapResponse addItemToInflowReceipt(AddItemToInflowReceiptWapRequest request) {
        return inflowService.addItemToInflowReceiptWap(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("saleOrderItem/detail/add")
    @POST
    public AddSaleOrderItemDetailResponse addSaleOrderItemDetails(AddSaleOrderItemDetailRequest request) {
        return inflowService.addSaleOrderItemDetails(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("vendorInvoice/credit/create")
    @POST
    public CreateVendorCreditInvoiceResponse createCreditVendorInvoice(CreateVendorCreditInvoiceRequest request) {
        request.setUserId(WebContextUtils.getCurrentUser().getUser().getId());
        return vendorInvoiceService.createCreditVendorInvoice(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("vendorInvoice/debit/create")
    @POST
    public CreateVendorDebitInvoiceResponse createDebitVendorInvoice(CreateVendorDebitInvoiceRequest request) {
        request.setUserId(WebContextUtils.getCurrentUser().getUser().getId());
        return vendorInvoiceService.createDebitVendorInvoice(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("vendorInvoice/detail/get")
    @POST
    public GetVendorInvoiceResponse getVendorInvoice(GetVendorInvoiceRequest request) {
        return vendorInvoiceService.getVendorInvoice(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("vendorInvoices/get")
    @POST
    public GetVendorInvoicesResponse getVendorInvoices(GetVendorInvoicesRequest request) {
        return vendorInvoiceService.getVendorInvoices(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("vendorInvoice/inflowReceipt/add")
    @POST
    public AddInflowReceiptToVendorInvoiceResponse addInflowReceiptToVendorInvoice(AddInflowReceiptToVendorInvoiceRequest request) {
        return vendorInvoiceService.addInflowReceiptToVendorInvoice(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("vendorInvoice/discard")
    @POST
    public DiscardVendorInvoiceResponse discardVendorInvoice(DiscardVendorInvoiceRequest request) {
        return vendorInvoiceService.discardVendorInvoice(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("vendorInvoice/complete")
    @POST
    public CompleteVendorInvoiceResponse completeVendorInvoice(CompleteVendorInvoiceRequest request) {
        return vendorInvoiceService.completeVendorInvoice(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("vendorInvoice/debit/autoCreate")
    @POST
    public CreateDebitForCreditVendorInvoiceResponse completeVendorInvoice(CreateDebitForCreditVendorInvoiceRequest request) {
        request.setUserId(WebContextUtils.getCurrentUser().getUser().getId());
        return vendorInvoiceService.createDebitForCreditVendorInvoice(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("vendorInvoice/addOrEditItem")
    @POST
    public AddEditVendorInvoiceItemResponse addEditVendorInvoiceItem(AddEditVendorInvoiceItemRequest request) {
        return vendorInvoiceService.addEditVendorInvoiceItem(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("vendorInvoice/removeItem")
    @POST
    public RemoveVendorInvoiceItemResponse removeVendorInvoiceItem(RemoveVendorInvoiceItemRequest request) {
        return vendorInvoiceService.removeVendorInvoiceItem(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("vendorInvoice/detail/edit")
    @POST
    public EditVendorInvoiceResponse editVendorInvoice(EditVendorInvoiceRequest request) {
        return vendorInvoiceService.editVendorInvoice(request);
    }

    @RequestMapping("/inflowReceipt/print/{inflowReceiptCode}")
    public void printInflowReceipt(@PathVariable("inflowReceiptCode") String inflowReceiptCode, HttpServletResponse response) throws IOException {
        StringBuilder builder = new StringBuilder();
        String inflowReceiptHtml = inflowService.getInflowReceiptHtml(inflowReceiptCode);
        if (StringUtils.isNotBlank(inflowReceiptHtml)) {
            builder.append(inflowReceiptHtml);
            response.setContentType("application/pdf");
            PrintTemplateVO inflowReceiptTemplate = CacheManager.getInstance().getCache(PrintTemplateCache.class).getPrintTemplateByType(PrintTemplateVO.Type.INFLOW_RECEIPT);
            SamplePrintTemplateVO samplePrintTemplate = CacheManager.getInstance().getCache(SamplePrintTemplateCache.class).getSamplePrintTemplate(
                    inflowReceiptTemplate.getSamplePrintTemplateCode());
            pdfDocumentService.writeHtmlToPdf(response.getOutputStream(), builder.toString(), new PrintOptions(samplePrintTemplate));
        }
    }

}
