/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 07/05/14
 *  @author amit
 */

package com.uniware.web.controller;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.unifier.core.api.templatecustomization.CustomizeTemplateRequest;
import com.unifier.core.api.templatecustomization.CustomizeTemplateResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.unifier.core.api.templatecustomization.GetTemplateCustomizationConfigRequest;
import com.unifier.core.api.templatecustomization.GetTemplateCustomizationConfigResponse;
import com.unifier.core.api.templatecustomization.MarkTemplateCustomizedRequest;
import com.unifier.core.api.templatecustomization.MarkTemplateCustomizedResponse;
import com.unifier.core.api.templatecustomization.SaveCustomizedTemplateRequest;
import com.unifier.core.api.templatecustomization.SaveCustomizedTemplateResponse;
import com.uniware.services.templateCustomization.ITemplateCustomizationService;

@Controller
@Path("/data/template/customize/")
public class TemplateCustomizationResource {

    @Autowired
    private ITemplateCustomizationService templateCustomizationService;

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Path("get/config")
    public GetTemplateCustomizationConfigResponse getTemplateCustomizationConfig(GetTemplateCustomizationConfigRequest request) {
        return templateCustomizationService.getTemplateCustomizationConfig(request);
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Path("save")
    public SaveCustomizedTemplateResponse saveCustomizedTemplate(SaveCustomizedTemplateRequest request) {
        return templateCustomizationService.saveCustomizedTemplate(request);
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Path("do")
    public CustomizeTemplateResponse customizeTemplate (CustomizeTemplateRequest request) {
        return templateCustomizationService.customizeTemplate(request);
    }
    
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Path("mark")
    public MarkTemplateCustomizedResponse markTemplateCustomized(MarkTemplateCustomizedRequest request) {
        return templateCustomizationService.markTemplateCustomized(request);
    }
}
