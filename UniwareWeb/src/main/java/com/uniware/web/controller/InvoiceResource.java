/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jan 12, 2012
 *  @author singla
 */
package com.uniware.web.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.unifier.core.cache.CacheManager;
import com.unifier.core.utils.StringUtils;
import com.unifier.services.pdf.IPdfDocumentService;
import com.unifier.services.pdf.impl.PdfDocumentServiceImpl;
import com.unifier.services.pdf.impl.PdfDocumentServiceImpl.PrintOptions;
import com.unifier.web.utils.WebContextUtils;
import com.uniware.core.api.invoice.BulkCreateInvoiceRequest;
import com.uniware.core.api.invoice.BulkCreateInvoiceResponse;
import com.uniware.core.api.invoice.CreateInvoiceRequest;
import com.uniware.core.api.invoice.CreateInvoiceResponse;
import com.uniware.core.api.invoice.CreateShippingPackageInvoiceRequest;
import com.uniware.core.api.invoice.GetTaxDetailsRequest;
import com.uniware.core.api.invoice.GetTaxDetailsResponse;
import com.uniware.core.api.invoice.UpdateInvoiceRequest;
import com.uniware.core.api.invoice.UpdateInvoiceResponse;
import com.uniware.core.api.print.PrintPreviewRequest;
import com.uniware.core.vo.PrintTemplateVO;
import com.uniware.core.vo.SamplePrintTemplateVO;
import com.uniware.services.cache.PrintTemplateCache;
import com.uniware.services.cache.SamplePrintTemplateCache;
import com.uniware.services.invoice.IInvoiceService;
import com.uniware.services.shipping.IShippingInvoiceService;

/**
 * @author singla
 */
@Controller
@Path("/data/oms/invoice/")
public class InvoiceResource {

    @Autowired
    private IInvoiceService invoiceService;

    @Autowired
    private IPdfDocumentService pdfDocumentService;

    @Autowired
    private IShippingInvoiceService shippingInvoiceService;

    @Produces(MediaType.APPLICATION_JSON)
    @Path("create")
    @POST
    public CreateInvoiceResponse getOrCreateInvoice(CreateInvoiceRequest request) {
        request.setUserId(WebContextUtils.getCurrentUser().getUser().getId());
        request.setCommitBlockedInventory(true);
        CreateShippingPackageInvoiceRequest createShippingPackageInvoiceRequest = new CreateShippingPackageInvoiceRequest(request);
        CreateInvoiceResponse invoiceResponse = shippingInvoiceService.createShippingPackageInvoice(createShippingPackageInvoiceRequest);
        return invoiceResponse;
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("bulk/create")
    @POST
    public BulkCreateInvoiceResponse getOrCreateInvoice(BulkCreateInvoiceRequest request) {
        request.setUserId(WebContextUtils.getCurrentUser().getUser().getId());
        return shippingInvoiceService.bulkCreateInvoice(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("update")
    @POST
    public UpdateInvoiceResponse updateInvoice(UpdateInvoiceRequest request) {
        return shippingInvoiceService.updateInvoice(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("getTaxDetails")
    @POST
    public GetTaxDetailsResponse getTaxDetails(GetTaxDetailsRequest request) {
        return invoiceService.getTaxDetails(request);
    }

    @RequestMapping("/oms/invoice/show")
    public void printInvoices(@RequestParam("invoiceCodes") String invoiceCodes, HttpServletResponse response) throws IOException {
        PrintTemplateVO invoiceTemplate = CacheManager.getInstance().getCache(PrintTemplateCache.class).getPrintTemplateByType(PrintTemplateVO.Type.INVOICE);
        SamplePrintTemplateVO samplePrintTemplate = CacheManager.getInstance().getCache(SamplePrintTemplateCache.class).getSamplePrintTemplate(
                invoiceTemplate.getSamplePrintTemplateCode());
        StringBuilder builder = new StringBuilder("<document>");
        boolean hasValidInvoice = false;
        for (String invoiceCode : StringUtils.split(invoiceCodes, "\\|")) {
            String invoiceHtml = shippingInvoiceService.getInvoiceHtml(invoiceCode);
            if (StringUtils.isNotBlank(invoiceHtml)) {
                builder.append(invoiceHtml);
                if (!samplePrintTemplate.isXml()) {
                    builder.append(PdfDocumentServiceImpl.PAGE_BREAK);
                }
                hasValidInvoice = true;
            }
        }
        builder.append("</document>");
        if (hasValidInvoice) {
            response.setContentType("application/pdf");
            if (samplePrintTemplate.isXml()){
                pdfDocumentService.writeHtmlToPdf(response.getOutputStream(), builder.toString(), new PrintOptions(samplePrintTemplate));
            } else {
                pdfDocumentService.writeHtmlToPdf(response.getOutputStream(), builder.delete(builder.length() - PdfDocumentServiceImpl.PAGE_BREAK.length(), builder.length()).toString(), new PrintOptions(samplePrintTemplate));
            }
        }
    }

    @RequestMapping("/admin/printing/template/invoice/preview")
    @ResponseBody
    public void previewInvoice(PrintPreviewRequest request, HttpServletResponse hrequest, HttpServletResponse response) throws IOException, ClassNotFoundException {
        String invoiceHtml = shippingInvoiceService.getInvoiceHtml(request.getObjectCode());
        if (invoiceHtml != null) {
            response.setContentType("application/pdf");
            pdfDocumentService.writeHtmlToPdf(response.getOutputStream(), invoiceHtml,
                    new PrintOptions(request.getPrintDialog(), request.getNumberOfCopies(), request.isLandscape(), request.getPageSize(), request.getPageMargins()));
        }
    }
}
