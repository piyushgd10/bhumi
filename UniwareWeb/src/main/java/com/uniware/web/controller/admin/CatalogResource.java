/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 *  @version     1.0, Apr 1, 2012
 *  @author praveeng
 */
package com.uniware.web.controller.admin;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.uniware.core.api.catalog.GetItemTypeByItemRequest;
import com.uniware.core.api.catalog.GetItemTypeByItemResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;

import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.utils.StringUtils;
import com.unifier.web.json.JElementList;
import com.uniware.core.api.catalog.CreateCategoryRequest;
import com.uniware.core.api.catalog.CreateCategoryResponse;
import com.uniware.core.api.catalog.CreateItemTypeRequest;
import com.uniware.core.api.catalog.CreateItemTypeResponse;
import com.uniware.core.api.catalog.CreateOrEditFacilityItemTypeRequest;
import com.uniware.core.api.catalog.CreateOrEditFacilityItemTypeResponse;
import com.uniware.core.api.catalog.CreateTagRequest;
import com.uniware.core.api.catalog.CreateTagResponse;
import com.uniware.core.api.catalog.EditCategoryRequest;
import com.uniware.core.api.catalog.EditCategoryResponse;
import com.uniware.core.api.catalog.EditItemTypeRequest;
import com.uniware.core.api.catalog.EditItemTypeResponse;
import com.uniware.core.api.catalog.GetCategoryRequest;
import com.uniware.core.api.catalog.GetCategoryResponse;
import com.uniware.core.api.catalog.GetItemTypeRequest;
import com.uniware.core.api.catalog.GetItemTypeResponse;
import com.uniware.core.api.catalog.TaxTypeDTO;
import com.uniware.core.api.catalog.dto.CategoryDTO;
import com.uniware.core.api.channel.CreateChannelItemTypeRequest;
import com.uniware.core.api.channel.CreateChannelItemTypeResponse;
import com.uniware.core.api.channel.CreateItemTypeFromChannelItemTypeRequest;
import com.uniware.core.api.channel.CreateItemTypeFromChannelItemTypeResponse;
import com.uniware.core.api.channel.GetChannelItemTypeDetailsRequest;
import com.uniware.core.api.channel.GetChannelItemTypeDetailsResponse;
import com.uniware.core.api.inventory.ReassessInventoryForItemTypeRequest;
import com.uniware.core.api.inventory.ReassessInventoryForItemTypeResponse;
import com.uniware.core.api.tax.CreateEditTaxTypesConfigurationRequest;
import com.uniware.core.api.tax.CreateTaxTypeConfigurationResponse;
import com.uniware.core.api.tax.EditTaxTypeRequest;
import com.uniware.core.api.tax.EditTaxTypeResponse;
import com.uniware.core.api.tax.GetAllTaxTypesResponse;
import com.uniware.core.api.tax.GetTaxTypeConfigurationRequest;
import com.uniware.core.api.tax.GetTaxTypeConfigurationResponse;
import com.uniware.core.api.tax.GetTaxTypeStatesResponse;
import com.uniware.core.api.warehouse.SearchFacilityItemTypeRequest;
import com.uniware.core.api.warehouse.SearchFacilityItemTypeResponse;
import com.uniware.core.api.warehouse.SearchItemTypesRequest;
import com.uniware.core.api.warehouse.SearchItemTypesResponse;
import com.uniware.core.api.warehouse.SectionDTO;
import com.uniware.core.entity.Category;
import com.uniware.core.entity.ChannelItemType;
import com.uniware.core.entity.Tag;
import com.uniware.core.entity.TaxType;
import com.uniware.services.catalog.ICatalogService;
import com.uniware.services.channel.IChannelCatalogService;
import com.uniware.services.inventory.IInventoryService;
import com.uniware.services.tax.ITaxTypeService;

@Controller
@Path("/data/")
public class CatalogResource {

    private static final String    GST_TYPE = "GST";
    @Autowired
    private ICatalogService        catalogService;

    @Autowired
    private IChannelCatalogService channelCatalogService;

    @Autowired
    private ITaxTypeService        taxTypeService;

    @Autowired
    private IInventoryService      inventoryService;

    @Produces(MediaType.APPLICATION_JSON)
    @Path("catalog/allTags")
    @GET
    public JElementList<Tag> getAllTags() {
        List<Tag> tags = catalogService.getAllTags();
        JElementList<Tag> tagList = new JElementList<Tag>();
        tagList.getElements().addAll(tags);
        return tagList;
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("catalog/addTag")
    @POST
    public CreateTagResponse addTag(CreateTagRequest request) {

        CreateTagResponse response = new CreateTagResponse();
        ValidationContext context = request.validate();
        if (context.hasErrors()) {
            response.setSuccessful(false);
            response.setErrors(context.getErrors());
        } else {
            try {
                Tag tag = catalogService.addTag(request.getName());
                response.setMessage("Added Successfully");
                response.setSuccessful(true);
                response.setId(tag.getId());
                response.setName(tag.getName());
            } catch (DataIntegrityViolationException e) {
                e.printStackTrace();
                response.setMessage("Already exists");
                response.setSuccessful(false);
                response.setErrors(context.getErrors());
            }
        }
        return response;

    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("catalog/removeTag")
    @POST
    public CreateTagResponse removeTag(CreateTagRequest request) {

        CreateTagResponse response = new CreateTagResponse();
        ValidationContext context = request.validate();
        if (context.hasErrors()) {
            response.setSuccessful(false);
            response.setErrors(context.getErrors());
        } else {

            catalogService.removeTag(request.getName());
            response.setMessage("Removed Successfully");
            response.setSuccessful(true);
        }
        return response;
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("catalog/search")
    @POST
    public SearchItemTypesResponse listItems(SearchItemTypesRequest request) {
        return catalogService.searchItemTypes(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("catalog/itemType/create")
    @POST
    public CreateItemTypeResponse createItemType(CreateItemTypeRequest request) {
        return catalogService.createItemType(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("catalog/itemType/update")
    @POST
    public EditItemTypeResponse editItemType(EditItemTypeRequest request) {
        EditItemTypeResponse response = catalogService.editItemType(request);
        return response;
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("catalog/itemType/byItemCode")
    @POST
    public GetItemTypeByItemResponse getItemTypeByItem(GetItemTypeByItemRequest request) {
        return catalogService.getItemTypeByItem(request);
    }


    @Produces(MediaType.APPLICATION_JSON)
    @Path("catalog/itemType/reassessInventory")
    @POST
    public ReassessInventoryForItemTypeResponse reassessInventoryForItemType(ReassessInventoryForItemTypeRequest request) {
        return inventoryService.reassessInventoryForItemType(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("catalog/category/create")
    @POST
    public CreateCategoryResponse createCategory(CreateCategoryRequest request) {
        return catalogService.createCategory(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("catalog/category/update")
    @POST
    public EditCategoryResponse editCategory(EditCategoryRequest request) {
        return catalogService.editCategory(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("catalog/get/categories")
    @GET
    public GetCategoryResponse listCategories() {
        return catalogService.listCategories(new GetCategoryRequest());
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("catalog/get/itemType")
    @POST
    public GetItemTypeResponse getItemType(GetItemTypeRequest request) {
        return catalogService.getItemType(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("catalog/get/category")
    @GET
    public CategoryDTO getCategory(@QueryParam("code") String code) {
        Category category = catalogService.getCategoryByCode(code);
        return new CategoryDTO(category);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("catalog/get/section")
    @GET
    public List<SectionDTO> listSection() {
        return catalogService.listSections();
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/catalog/taxType/get")
    @GET
    public TaxTypeDTO getTaxtType(@QueryParam("code") String code) {
        TaxType taxType = taxTypeService.getTaxTypeByCode(code);
        return new TaxTypeDTO(taxType);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("catalog/taxType/edit")
    @POST
    public EditTaxTypeResponse editTaxType(EditTaxTypeRequest request) {
        return taxTypeService.editTaxType(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("catalog/taxTypeConfiguration/get")
    @GET
    public GetTaxTypeConfigurationResponse getTaxTypeConfiguration(@QueryParam("taxTypeCode") String taxTypeCode) {
        GetTaxTypeConfigurationRequest request = new GetTaxTypeConfigurationRequest();
        request.setTaxTypeCode(taxTypeCode);
        GetTaxTypeConfigurationResponse response = taxTypeService.getTaxTypeConfigurations(request);
        return response;
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("catalog/taxTypes/get")
    @GET
    public GetAllTaxTypesResponse getAllTaxTypes(@QueryParam("type") String type) {
        GetAllTaxTypesResponse response = new GetAllTaxTypesResponse();
        boolean fetchGst = StringUtils.isNotBlank(type) && GST_TYPE.equals(type);
        response.setAllTaxTypes(taxTypeService.getTaxTypes(fetchGst));
        response.setSuccessful(true);
        return response;
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("catalog/taxTypeConfiguration/edit")
    @POST
    public CreateTaxTypeConfigurationResponse editTaxTypesConfiguration(CreateEditTaxTypesConfigurationRequest request) {
        request.setEdit(true);
        return taxTypeService.createTaxTypeConfiguration(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("catalog/taxTypeConfiguration/create")
    @POST
    public CreateTaxTypeConfigurationResponse createTaxTypesConfiguration(CreateEditTaxTypesConfigurationRequest request) {
        return taxTypeService.createTaxTypeConfiguration(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("catalog/taxTypeConfiguration/states")
    @GET
    public GetTaxTypeStatesResponse getTaxTypeStates() {
        return taxTypeService.getStatesForTaxTypes();
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("catalog/facilityItemType/search")
    @POST
    public SearchFacilityItemTypeResponse searchFacilityItemType(SearchFacilityItemTypeRequest request) {
        return catalogService.searchFacilityItemType(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("catalog/facilityItemType/create")
    @POST
    public CreateOrEditFacilityItemTypeResponse createOrEditFacilityItemType(CreateOrEditFacilityItemTypeRequest request) {
        return catalogService.createOrEditFacilityItemType(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("catalog/createChannelItemType")
    @POST
    public CreateChannelItemTypeResponse createChannelItemType(CreateChannelItemTypeRequest request) {
        request.setSyncId(ChannelItemType.SyncedBy.MANUAL.toString());
        return channelCatalogService.createChannelItemType(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("catalog/channelItemType/get")
    @POST
    public GetChannelItemTypeDetailsResponse getChannelItemTypeDetails(GetChannelItemTypeDetailsRequest request) {
        return channelCatalogService.getChannelItemTypeDetails(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("catalog/createItemTypeFromChannelItemType")
    @POST
    public CreateItemTypeFromChannelItemTypeResponse createItemTypeFromChannelItemType(CreateItemTypeFromChannelItemTypeRequest request) {
        return channelCatalogService.createItemTypeFromChannelItemType(request);
    }
}
