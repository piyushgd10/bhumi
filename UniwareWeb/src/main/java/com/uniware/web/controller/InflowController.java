/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 29, 2012
 *  @author praveeng
 */
package com.uniware.web.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.template.Template;
import com.unifier.services.pdf.IPdfDocumentService;
import com.unifier.services.pdf.impl.PdfDocumentServiceImpl.PrintOptions;
import com.uniware.core.entity.InboundGatePass;
import com.uniware.core.entity.VendorInvoice;
import com.uniware.core.vo.PrintTemplateVO;
import com.uniware.core.vo.SamplePrintTemplateVO;
import com.uniware.services.cache.PrintTemplateCache;
import com.uniware.services.cache.SamplePrintTemplateCache;
import com.uniware.services.configuration.CustomFieldsMetadataConfiguration;
import com.uniware.services.inflow.IInflowService;
import com.uniware.services.inflow.IVendorInvoiceService;
import com.uniware.services.warehouse.ISectionService;
import com.uniware.services.warehouse.IShelfService;

@Controller
public class InflowController {

    public static final String    GRN_CREATE_PAGE = "inflow/grnCreate";
    public static final String    GRN_QC_PAGE     = "inflow/grnQC";
    public static final String    GRN_SEARCH_PAGE = "inflow/searchReceipt";

    @Autowired
    private IShelfService         shelfService;

    @Autowired
    private ISectionService       sectionService;

    @Autowired
    private IInflowService        inflowService;

    @Autowired
    private IVendorInvoiceService vendorInvoiceService;

    @Autowired
    private IPdfDocumentService   pdfDocumentService;

    @RequestMapping(value = { "/inflow/grn/view" })
    public String grnView() {
        return "inflow/grnView";
    }

    @RequestMapping(value = { "/inflow/poLabels/create" })
    public String createPOLabels() {
        return "inflow/jabong/createPOLabels";
    }

    @RequestMapping(value = { "/inflow/poLabel/qc" })
    public String poLabelQC() {
        return "inflow/jabong/poLabelQC";
    }

    @RequestMapping(value = { "/inflow/shipment/create" })
    public String createShipment() {
        return "inflow/jabong/createShipment";
    }

    @RequestMapping(value = { "/inflow/vendorInvoice" })
    public String vendorInvoice(ModelMap map) {
        map.addAttribute("customFieldsJson",
                ConfigurationManager.getInstance().getConfiguration(CustomFieldsMetadataConfiguration.class).getCustomFieldsByEntityJson(VendorInvoice.class.getName()));

        return "inflow/vendorInvoice";
    }

    @RequestMapping(value = { "/inflow/grn/create" })
    public String grnCreate() {
        return GRN_CREATE_PAGE;
    }

    @RequestMapping(value = { "/inflow/grn/createLabels" })
    public String grnCreateLabels() {
        return "inflow/grnCreateLabels";
    }

    @RequestMapping(value = { "/inflow/grn/qc" })
    public String grnQC() {
        return GRN_QC_PAGE;
    }

    @RequestMapping("/inflow/searchReceipt")
    public String searchReceipt() {
        return GRN_SEARCH_PAGE;
    }

    @RequestMapping("/inflow/searchActivePO")
    public String searchActivePO() {
        return "inflow/searchActivePO";
    }

    @RequestMapping("/inflow/warehouseCapacity")
    public String searchShelf(ModelMap modelMap) {
        modelMap.addAttribute("shelfTypes", shelfService.getShelfTypes());
        modelMap.addAttribute("sections", sectionService.getEnabledSections());
        return "inflow/warehouseCapacity";
    }

    @RequestMapping("/inflow/adjustInventory")
    public String searchVendor() {
        return "inflow/adjustInventory";
    }

    @RequestMapping("/inflow/scanItem")
    public String scanItem() {
        return "inflow/scanItem";
    }

    @RequestMapping("/inflow/gatePassEntry")
    public String getPassEntry(ModelMap map) {
        map.addAttribute("customFieldsJson",
                ConfigurationManager.getInstance().getConfiguration(CustomFieldsMetadataConfiguration.class).getCustomFieldsByEntityJson(InboundGatePass.class.getName()));
        return "inflow/gatePassEntry";
    }

    @RequestMapping("/inflow/vendorInvoice/print/{vendorInvoiceCode}")
    public void printVendorInvoice(@PathVariable("vendorInvoiceCode") String vendorInvoiceCode, HttpServletResponse response) throws IOException {
        Template template = CacheManager.getInstance().getCache(PrintTemplateCache.class).getTemplateByType(PrintTemplateVO.Type.VENDOR_INVOICE.name());
        String vendorInvoiceHtml = vendorInvoiceService.getVendorInvoiceHtml(vendorInvoiceCode, template);
        if (vendorInvoiceHtml != null) {
            response.setContentType("application/pdf");
            PrintTemplateVO gatePassTemplate = CacheManager.getInstance().getCache(PrintTemplateCache.class).getPrintTemplateByType(PrintTemplateVO.Type.VENDOR_INVOICE);
            SamplePrintTemplateVO samplePrintTemplate = CacheManager.getInstance().getCache(SamplePrintTemplateCache.class).getSamplePrintTemplate(
                    gatePassTemplate.getSamplePrintTemplateCode());
            pdfDocumentService.writeHtmlToPdf(response.getOutputStream(), vendorInvoiceHtml, new PrintOptions(samplePrintTemplate));
        }
    }

}