/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 11-May-2012
 *  @author praveeng
 */
package com.uniware.web.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.unifier.core.api.user.SignupVendorRequest;
import com.unifier.core.api.user.SignupVendorResponse;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.entity.User;
import com.unifier.core.utils.EncryptionUtils;
import com.unifier.web.utils.WebContextUtils;
import com.uniware.core.entity.AdvanceShippingNotice;
import com.uniware.core.entity.Vendor;
import com.uniware.services.configuration.CustomFieldsMetadataConfiguration;
import com.uniware.services.vendor.IVendorService;

@Controller
@Path("/data/")
public class VendorController {
    public static final String CREATE_ASN_PAGE = "vendor/createASN";

    @Autowired
    private IVendorService     vendorService;

    @RequestMapping("/vendor/createASN")
    public String createASN(ModelMap map) {
        map.addAttribute("customFieldsJson",
                ConfigurationManager.getInstance().getConfiguration(CustomFieldsMetadataConfiguration.class).getCustomFieldsByEntityJson(AdvanceShippingNotice.class.getName()));
        return CREATE_ASN_PAGE;
    }

    @RequestMapping("/vendor/searchASN")
    public String searchASN() {
        return "/vendor/searchASN";
    }

    @RequestMapping("/vendor/viewASN")
    public String viewASN() {
        return "/vendor/viewASN";
    }

    @RequestMapping("/vendor/searchPO")
    public String searchPO() {
        return "/vendor/searchPO";
    }

    @RequestMapping("/vendor/poItems")
    public String searchPoItems() {
        return "/vendor/poItems";
    }

    @RequestMapping("/vendor/createLabels")
    public String createLabels() {
        return "/vendor/createLabels";
    }

    @RequestMapping("/vendor/help/*")
    public String vendorHelpPages() {
        return "/inflow/jabong/help";
    }

    @RequestMapping("/vendor/grnSearch")
    public String searchGrn() {
        return "/vendor/grnSearch";
    }

    @RequestMapping(value = { "/vendor/grn/view" })
    public String grnView() {
        return "/vendor/grnView";
    }

    @RequestMapping(value = { "/vendor/searchGatepass" })
    public String searchGatepass() {
        return "/vendor/searchGatepass";
    }

    @RequestMapping(value = { "/vendor/createGatepass" })
    public String createGatepass() {
        return "/vendor/createGatepass";
    }

    @RequestMapping(value = { "/vendor/backorders" })
    public String searchBackorders() {
        return "/vendor/backorders";
    }

    @RequestMapping(value = { "/vendor/vendorItemType" })
    public String searchVendorCatalog() {
        return "/vendor/vendorItemType";
    }

    @RequestMapping(value = { "/vendor/viewPendingOrderItems" })
    public String viewPendingItems() {
        return "/vendor/viewPendingOrderItems";
    }

    @RequestMapping(value = { "/vendor/searchInventory" })
    public String viewInventory() {
        return "/vendor/inventory";
    }

    @RequestMapping("/signup/vendor")
    public String signupVendor(ModelMap modelMap, HttpServletRequest request) {
        Map<String, String> requestParams = WebContextUtils.constructRequestParamMap(request);
        String vendorId = requestParams.get("vendorId");
        String timestamp = requestParams.get("timestamp");
        String checksum = requestParams.get("checksum");

        Vendor vendor = vendorService.getVendorById(Integer.parseInt(vendorId));
        User user = vendor.getUser();
        if (user != null) {
            modelMap.addAttribute("registered", true);
        }

        String encryptionCode = EncryptionUtils.md5Encode(vendorId + timestamp, "unicom");
        if (encryptionCode.equals(checksum)) {
            modelMap.addAttribute("signupVendorRequest", new SignupVendorRequest());
            modelMap.addAttribute("vendor", vendor);
            return "/vendor/vendorSignup";
        }
        return null;
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("vendor/createLogin")
    @POST
    public SignupVendorResponse signupVendor(SignupVendorRequest request) {
        return vendorService.signupVendor(request);
    }
}
