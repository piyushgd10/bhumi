/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 06-Feb-2012
 *  @author vibhu
 */
package com.uniware.web.controller.admin;

import com.uniware.services.warehouse.ISectionService;
import com.uniware.services.warehouse.IShelfService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class LayoutController {

    @Autowired
    private IShelfService shelfService;

    @Autowired
    private ISectionService sectionService;

    @RequestMapping("/admin/layout/shelfTypes")
    public String shelfTypes(ModelMap modelMap) {
        return "admin/layout/shelfTypes";
    }

    @RequestMapping("/admin/layout/searchShelf")
    public String searchShelf(ModelMap modelMap) {
        modelMap.addAttribute("shelfTypes", shelfService.getShelfTypes());
        modelMap.addAttribute("sections", sectionService.getEnabledSections());
        return "admin/layout/searchShelf";
    }

    @RequestMapping("/admin/layout/createShelf")
    public String createShelf(ModelMap modelMap) {
        modelMap.addAttribute("shelfTypes", shelfService.getShelfTypes());
        modelMap.addAttribute("sections", sectionService.getEnabledSections());
        return "admin/layout/createShelf";
    }
}
