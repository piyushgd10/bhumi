/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 14-May-2012
 *  @author vibhu
 */
package com.uniware.web.controller;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.uniware.core.api.item.AddNonTraceablePicklistItemsToPutawayRequest;
import com.uniware.core.api.item.AddNonTraceablePicklistItemsToPutawayResponse;
import com.uniware.core.api.item.AddPutbackPendingItemToPutawayRequest;
import com.uniware.core.api.item.AddPutbackPendingItemToPutawayResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.unifier.web.utils.WebContextUtils;
import com.uniware.core.api.gatepass.AddGatePassItemsToPutawayRequest;
import com.uniware.core.api.gatepass.AddGatePassItemsToPutawayResponse;
import com.uniware.core.api.item.AddInspectedNotBadItemToPutawayRequest;
import com.uniware.core.api.item.AddInspectedNotBadItemToPutawayResponse;
import com.uniware.core.api.putaway.AddInflowReceiptItemsToPutawayRequest;
import com.uniware.core.api.putaway.AddInflowReceiptItemsToPutawayResponse;
import com.uniware.core.api.putaway.AddNonTraceableItemToTransferPutawayRequest;
import com.uniware.core.api.putaway.AddNonTraceableItemToTransferPutawayResponse;
import com.uniware.core.api.putaway.AddTraceableItemToTransferPutawayRequest;
import com.uniware.core.api.putaway.AddTraceableItemToTransferPutawayResponse;
import com.uniware.core.api.putaway.CheckItemForPutawayRequest;
import com.uniware.core.api.putaway.CheckItemForPutawayResponse;
import com.uniware.core.api.putaway.CompleteItemPutawayRequest;
import com.uniware.core.api.putaway.CompleteItemPutawayResponse;
import com.uniware.core.api.putaway.CompletePutawayRequest;
import com.uniware.core.api.putaway.CompletePutawayResponse;
import com.uniware.core.api.putaway.CreatePutawayListRequest;
import com.uniware.core.api.putaway.CreatePutawayListResponse;
import com.uniware.core.api.putaway.CreatePutawayRequest;
import com.uniware.core.api.putaway.CreatePutawayResponse;
import com.uniware.core.api.putaway.DiscardPutawayRequest;
import com.uniware.core.api.putaway.DiscardPutawayResponse;
import com.uniware.core.api.putaway.EditPutawayItemRequest;
import com.uniware.core.api.putaway.EditPutawayItemResponse;
import com.uniware.core.api.putaway.GetPutawayRequest;
import com.uniware.core.api.putaway.GetPutawayResponse;
import com.uniware.core.api.putaway.GetPutawayTypesRequest;
import com.uniware.core.api.putaway.GetPutawayTypesResponse;
import com.uniware.core.api.putaway.GetShelfRequest;
import com.uniware.core.api.putaway.GetShelfResponse;
import com.uniware.core.api.putaway.ReleaseInventoryAndRepackageRequest;
import com.uniware.core.api.putaway.ReleaseInventoryAndRepackageResponse;
import com.uniware.core.api.putaway.SearchDirectReturnsRequest;
import com.uniware.core.api.putaway.SearchDirectReturnsResponse;
import com.uniware.core.api.putaway.SplitPutawayItemRequest;
import com.uniware.core.api.putaway.SplitPutawayItemResponse;
import com.uniware.core.api.returns.AddDirectReturnedSaleOrderItemsToPutawayRequest;
import com.uniware.core.api.returns.AddReturnedSaleOrderItemsToPutawayResponse;
import com.uniware.core.api.returns.AddReturnsSaleOrderItemsToPutawayRequest;
import com.uniware.core.api.returns.AddReturnsSaleOrderItemsToPutawayResponse;
import com.uniware.core.api.reversepickup.AddReversePickupSaleOrderItemsToPutawayRequest;
import com.uniware.core.api.reversepickup.AddReversePickupSaleOrderItemsToPutawayResponse;
import com.uniware.core.api.shipping.AddCancelledSaleOrderItemsToPutawayRequest;
import com.uniware.core.api.shipping.AddCancelledSaleOrderItemsToPutawayResponse;
import com.uniware.services.item.IItemService;
import com.uniware.services.putaway.IPutawayService;

@Controller
@Path("/data/putaway/")
public class PutawayResource {

    @Autowired
    IPutawayService putawayService;

    @Autowired
    IItemService    itemService;

    @Produces(MediaType.APPLICATION_JSON)
    @Path("create")
    @POST
    public CreatePutawayResponse createPutaway(CreatePutawayRequest request) {
        request.setUserId(WebContextUtils.getCurrentUser().getUser().getId());
        return putawayService.createPutaway(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("get")
    @POST
    public GetPutawayResponse getPutaway(GetPutawayRequest request) {
        return putawayService.getPutaway(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("shelf/get")
    @POST
    public GetShelfResponse getShelf(GetShelfRequest request) {
        return putawayService.getShelf(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("shipping/cancelled/items/add")
    @POST
    public AddCancelledSaleOrderItemsToPutawayResponse addCancelledSaleOrderItemsToPutaway(AddCancelledSaleOrderItemsToPutawayRequest request) {
        request.setUserId(WebContextUtils.getCurrentUser().getUser().getId());
        return putawayService.addCancelledSaleOrderItemsToPutaway(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("add/inflowReceiptItems")
    @POST
    public AddInflowReceiptItemsToPutawayResponse getPutaway(AddInflowReceiptItemsToPutawayRequest request) {
        request.setUserId(WebContextUtils.getCurrentUser().getUser().getId());
        return putawayService.addInflowReceiptItemsToPutaway(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("createPutawayList")
    @POST
    public CreatePutawayListResponse createPutawayList(CreatePutawayListRequest request) {
        request.setUserId(WebContextUtils.getCurrentUser().getUser().getId());
        return putawayService.createPutawayList(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("returns/items/add")
    @POST
    public AddReturnsSaleOrderItemsToPutawayResponse addReturnsSaleOrderItemsToPutaway(AddReturnsSaleOrderItemsToPutawayRequest request) {
        request.setUserId(WebContextUtils.getCurrentUser().getUser().getId());
        return putawayService.addReturnsSaleOrderItemsToPutaway(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("complete")
    @POST
    public CompletePutawayResponse completeInflowPutaway(CompletePutawayRequest request) {
        return putawayService.completePutaway(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("items/complete")
    @POST
    public CompleteItemPutawayResponse completeItemPutaway(CompleteItemPutawayRequest request) {
        return putawayService.completeItemPutaway(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("item/check")
    @POST
    public CheckItemForPutawayResponse checkItemPutaway(CheckItemForPutawayRequest request) {
        return putawayService.checkItemForPutaway(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("item/edit")
    @POST
    public EditPutawayItemResponse editPutawayItem(EditPutawayItemRequest request) {
        return putawayService.editPutawayItem(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("discard")
    @POST
    public DiscardPutawayResponse discardShippingManifest(DiscardPutawayRequest request) {
        request.setUserId(WebContextUtils.getCurrentUser().getUser().getId());
        return putawayService.discardPutaway(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("reversePickup/items/add")
    @POST
    public AddReversePickupSaleOrderItemsToPutawayResponse addReversePickupSaleOrderItemsToPutaway(AddReversePickupSaleOrderItemsToPutawayRequest request) {
        request.setUserId(WebContextUtils.getCurrentUser().getUser().getId());
        return putawayService.addReversePickupSaleOrderItemsToPutaway(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("gatePass/items/add")
    @POST
    public AddGatePassItemsToPutawayResponse addGatePassItemsToPutaway(AddGatePassItemsToPutawayRequest request) {
        request.setUserId(WebContextUtils.getCurrentUser().getUser().getId());
        return putawayService.addGatePassItemsToPutaway(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("inspectedNotBadItem/add")
    @POST
    public AddInspectedNotBadItemToPutawayResponse addInspectedNotBadItemToPutaway(AddInspectedNotBadItemToPutawayRequest request) {
        request.setUserId(WebContextUtils.getCurrentUser().getUser().getId());
        return putawayService.addInspectedNotBadItemToPutaway(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("traceableItem/add")
    @POST
    public AddTraceableItemToTransferPutawayResponse addTraceableItemToTransferPutaway(AddTraceableItemToTransferPutawayRequest request) {
        request.setUserId(WebContextUtils.getCurrentUser().getUser().getId());
        return putawayService.addTraceableItemToTransferPutaway(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("nonTraceableItem/add")
    @POST
    public AddNonTraceableItemToTransferPutawayResponse addNonTraceableItemToTransferPutaway(AddNonTraceableItemToTransferPutawayRequest request) {
        request.setUserId(WebContextUtils.getCurrentUser().getUser().getId());
        return putawayService.addNonTraceableItemToTransferPutaway(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("directReturns/add")
    @POST
    public AddReturnedSaleOrderItemsToPutawayResponse addItemsToPutaway(AddDirectReturnedSaleOrderItemsToPutawayRequest request) {
        request.setUserId(WebContextUtils.getCurrentUser().getUser().getId());
        return putawayService.addDirectReturnsSaleOrderItemsToPutaway(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("directReturns/search")
    @POST
    public SearchDirectReturnsResponse searchDirectReturns(SearchDirectReturnsRequest request) {
        return putawayService.searchDirectReturns(request);
    }
    
    @Produces(MediaType.APPLICATION_JSON)
    @Path("releaseAndRepackage")
    @POST
    public ReleaseInventoryAndRepackageResponse releaseInventoryAndRepackage(ReleaseInventoryAndRepackageRequest request) {
        request.setUserId(WebContextUtils.getCurrentUser().getUser().getId());
        return putawayService.releaseInventoryAndRepackage(request);
    }
    
    @Produces(MediaType.APPLICATION_JSON)
    @Path("types/get")
    @GET
    public GetPutawayTypesResponse getPutawayTypes() {
        GetPutawayTypesRequest request = new GetPutawayTypesRequest();
        request.setWebOnly(true);
        return putawayService.getPutawayTypes(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("putbackitem/add")
    @POST
    public AddPutbackPendingItemToPutawayResponse addPutbackPendingItemToPutaway(AddPutbackPendingItemToPutawayRequest request) {
        request.setUserId(WebContextUtils.getCurrentUser().getUser().getId());
        return putawayService.addPutbackPendingItemToPutaway(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("item/split")
    @POST
    public SplitPutawayItemResponse splitPutawayItem(SplitPutawayItemRequest request) {
        return putawayService.splitPutawayItem(request);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("picklist/putbackitem/add")
    @POST
    public AddNonTraceablePicklistItemsToPutawayResponse addNonTraceablePicklistItemsToPutaway(AddNonTraceablePicklistItemsToPutawayRequest request) {
        request.setUserId(WebContextUtils.getCurrentUser().getUser().getId());
        return putawayService.addNonTraceablePicklistItemsToPutaway(request);
    }
}