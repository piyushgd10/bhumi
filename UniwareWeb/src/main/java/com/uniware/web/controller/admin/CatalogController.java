/*
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 * 
 *  @version     1.0, 06-Feb-2012
 *  @author vibhu
 */
package com.uniware.web.controller.admin;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.google.gson.Gson;
import com.unifier.core.utils.FileUtils;
import com.unifier.core.utils.StringUtils;
import com.uniware.core.api.catalog.FacilityDTO;
import com.uniware.core.api.catalog.PrintItemTypeBarcodesRequest;
import com.uniware.core.api.catalog.PrintItemTypeBarcodesResponse;
import com.uniware.core.entity.Facility;
import com.uniware.core.entity.Tag;
import com.uniware.services.catalog.ICatalogService;
import com.uniware.services.tax.ITaxTypeService;
import com.uniware.services.tenant.ITenantService;
import com.uniware.services.warehouse.IFacilityService;

@Controller
public class CatalogController {

    @Autowired
    private ICatalogService  catalogService;

    @Autowired
    private ITaxTypeService  taxTypeService;

    @Autowired
    private IFacilityService facilityService;

    @Autowired
    private ITenantService   tenantService;

    public void setCatalogService(ICatalogService catalogService) {
        this.catalogService = catalogService;
    }

    @RequestMapping("/catalog/searchItemtype")
    public String searchItemtype() {
        return "catalog/searchItemtype";
    }

    @RequestMapping("/catalog/addItemType")
    public String addItemtype(ModelMap map) {
        map.addAttribute("categories", catalogService.listCategories().getCategories());
        map.addAttribute("taxTypes", taxTypeService.getTaxTypes());
        return "catalog/createEditItemType";
    }

    @RequestMapping("/catalog/editItemType")
    public String editItemtype(ModelMap map) {
        map.addAttribute("categories", catalogService.listCategories().getCategories());
        map.addAttribute("taxTypes", taxTypeService.getTaxTypes());
        return "catalog/createEditItemType";
    }

    @RequestMapping("/catalog/tags")
    public String customTags(ModelMap map) {
        List<Tag> tags = catalogService.getAllTags();
        map.addAttribute("tags", tags);
        return "catalog/addTag";
    }

    @RequestMapping("/catalog/categories")
    public String searchCategory(ModelMap map) {
        map.addAttribute("taxTypes", taxTypeService.getTaxTypes());
        map.addAttribute("sections", catalogService.listSections());
        return "catalog/categories";
    }

    @RequestMapping("/catalog/taxTypes")
    public String taxTypes(ModelMap map) {
        map.addAttribute("taxTypes", new Gson().toJson(taxTypeService.getTaxTypes()));
        return "catalog/taxTypes";
    }

    @RequestMapping("/catalog/taxTypeConfiguration")
    public String taxTypeConfiguration(ModelMap map) {
        map.addAttribute("taxTypes", taxTypeService.getTaxTypes());
        return "catalog/taxTypeConfiguration";
    }

    @RequestMapping("/catalog/searchFacilityItemType")
    public String searchFacilityItemtype(ModelMap map) {
        List<FacilityDTO> dto = new ArrayList<FacilityDTO>();
        for (Facility facility : facilityService.getAllEnabledFacilities()) {
            if (facility.getType().equalsIgnoreCase(Facility.Type.DROPSHIP.name())) {
                FacilityDTO facilityDTO = new FacilityDTO();
                facilityDTO.setCode(facility.getCode());
                facilityDTO.setDisplayName(facility.getDisplayName());
                dto.add(facilityDTO);
            }
        }
        map.addAttribute("facilities", new Gson().toJson(dto));
        return "catalog/searchFacilityItemtype";
    }

    @RequestMapping("/data/itemType/printBarcodes")
    public void printItemTypeCodes(@RequestParam Map<String, String> params, HttpServletResponse response) throws IOException {
        Map<String, Integer> skuToQtyMap = new HashMap<String, Integer>();
        for (Entry<String, String> e : params.entrySet()) {
            skuToQtyMap.put(e.getKey().trim(), Integer.parseInt(e.getValue().trim()));
        }
        PrintItemTypeBarcodesRequest printBarcodeReq = new PrintItemTypeBarcodesRequest();
        printBarcodeReq.setSkuToQtyMap(skuToQtyMap);
        response.setContentType("application/csv");
        response.addHeader("content-disposition", "attachment; filename=\"" + tenantService.getDownloadFileName("item-type-label") + "\"");
        PrintItemTypeBarcodesResponse catalogResponse = catalogService.printItemTypeBarcodes(printBarcodeReq);
        if (StringUtils.isNotBlank(catalogResponse.getOutputCsv())) {
            FileUtils.write(catalogResponse.getOutputCsv().getBytes(), response.getOutputStream());
        }
    }
}
