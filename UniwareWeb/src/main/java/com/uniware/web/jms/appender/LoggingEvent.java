package com.uniware.web.jms.appender;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by digvijaysharma on 19/05/17.
 */
public class LoggingEvent implements Serializable {

    private static final long serialVersionUID = 3101112988791851324L;

    private String            level;

    private String            threadName;

    private String            messageClassName;

    private String            message;

    private String            logRoutingKey;

    private Date              requestTimestamp;

    private Throwable         throwable;

    public Throwable getThrowable() {
        return throwable;
    }

    public void setThrowable(Throwable throwable) {
        this.throwable = throwable;
    }

    public String getMessageClassName() {
        return messageClassName;
    }

    public void setMessageClassName(String messageClassName) {
        this.messageClassName = messageClassName;
    }

    public Date getRequestTimestamp() {
        return requestTimestamp;
    }

    public void setRequestTimestamp(Date requestTimestamp) {
        this.requestTimestamp = requestTimestamp;
    }

    public String getLogRoutingKey() {
        return logRoutingKey;
    }

    public void setLogRoutingKey(String logRoutingKey) {
        this.logRoutingKey = logRoutingKey;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getThreadName() {
        return threadName;
    }

    public void setThreadName(String threadName) {
        this.threadName = threadName;
    }

    @Override
    public String toString() {
        return "LoggingEvent{" + "level='" + level + '\'' + ", threadName='" + threadName + '\'' + ", messageClassName='" + messageClassName + '\'' + ", message='" + message + '\''
                + ", logRoutingKey='" + logRoutingKey + '\'' + ", requestTimestamp=" + requestTimestamp + '}';
    }
}
