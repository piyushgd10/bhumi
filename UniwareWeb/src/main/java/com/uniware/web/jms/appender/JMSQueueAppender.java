package com.uniware.web.jms.appender;

import com.unifier.core.jms.MessagingConstants;
import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.StringUtils;
import com.uniware.core.utils.Constants;
import com.uniware.services.messaging.jms.ActiveMQConnector;
import org.apache.log4j.Logger;
import org.apache.logging.log4j.core.Filter;
import org.apache.logging.log4j.core.Layout;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.appender.AbstractAppender;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.config.plugins.PluginAttribute;
import org.apache.logging.log4j.core.config.plugins.PluginElement;
import org.apache.logging.log4j.core.config.plugins.PluginFactory;
import org.apache.logging.log4j.core.layout.PatternLayout;

import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;

/**
 * Created by digvijaysharma on 19/05/17.
 */

@Plugin(name = "JmsAppender", category = "Core", elementType = "appender", printObject = true)
public class JMSQueueAppender extends AbstractAppender {

    private static final Logger LOG = Logger.getLogger(JMSQueueAppender.class);

    private ActiveMQConnector   activeMQConnector;

    public ActiveMQConnector getActiveMQConnector() {
        return new ActiveMQConnector(System.getProperty("traceLogActiveMQBrokerUsername"), System.getProperty("traceLogActiveMQBrokerPassword"),
                System.getProperty("traceLogActiveMQBrokerUrl"), getMaxConnections(), getActiveMQPrefetch(), true);
    }

    private int getMaxConnections() {
        int maxNoOfConnections = 1;
        String maxNoOfConnectionsProperty = System.getProperty("traceLogActiveMQMaxConnections");
        if (StringUtils.isNotBlank(maxNoOfConnectionsProperty)) {
            maxNoOfConnections = Integer.parseInt(maxNoOfConnectionsProperty);
        }
        return maxNoOfConnections;
    }

    private int getActiveMQPrefetch() {
        int activeMQQueuePrefetch = 5;
        String activeMQQueuePrefetchProperty = System.getProperty("traceLogActiveMQQueuePrefetch");
        if (StringUtils.isNotBlank(activeMQQueuePrefetchProperty)) {
            activeMQQueuePrefetch = Integer.parseInt(activeMQQueuePrefetchProperty);
        }
        return activeMQQueuePrefetch;
    }

    public JMSQueueAppender(String name, Filter filter, PatternLayout layout, boolean ignoreExceptions) {
        super(name, filter, layout, ignoreExceptions);
        this.activeMQConnector = getActiveMQConnector();
    }

    @Override
    public void append(LogEvent logEvent) {
        LoggingEvent loggingEvent = new LoggingEvent();
        String logRoutingKey = logEvent.getContextMap().get(Constants.TRACE_LOG_KEY_VALUE);
        if (logRoutingKey != null) {
            loggingEvent.setLogRoutingKey(logRoutingKey);
            loggingEvent.setMessage(logEvent.getMessage().getFormattedMessage());
            loggingEvent.setLevel(logEvent.getLevel().name());
            loggingEvent.setThreadName(logEvent.getThreadName());
            loggingEvent.setRequestTimestamp(DateUtils.getCurrentTime());
            loggingEvent.setMessageClassName(logEvent.getSource().getFileName());
            Throwable logThrowable = logEvent.getThrown();
            if(logThrowable!=null){
                StringWriter stringWriter = new StringWriter();
                PrintWriter printWriter = new PrintWriter(stringWriter);
                logThrowable.printStackTrace(printWriter);
                loggingEvent.setMessage(loggingEvent.getMessage()+",Error:"+stringWriter.toString());
            }
            activeMQConnector.produceContextAwareMessage(MessagingConstants.Queue.LOGGING_QUEUE, loggingEvent, logRoutingKey);
        } else {
            LOG.error("No LOG_ROUTING_KEY present in the logging event");
        }
    }

    @PluginFactory
    public static JMSQueueAppender createAppender(@PluginAttribute("name") String name, @PluginElement("Layout") Layout<? extends Serializable> layout,
            @PluginElement("Filter") final Filter filter, @PluginAttribute("otherAttribute") String otherAttribute) {
        if (name == null) {
            LOG.error("No name provided for JMSQueueAppender");
            return null;
        }
        return new JMSQueueAppender(name, filter, (PatternLayout) layout, true);
    }
}