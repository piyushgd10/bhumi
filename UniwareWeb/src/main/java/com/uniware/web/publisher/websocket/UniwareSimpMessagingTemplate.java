/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 19-Jan-2015
 *  @author sunny
 */
package com.uniware.web.publisher.websocket;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.MessagingException;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.SimpMessageType;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import com.uniware.core.utils.UserContext;

@Component
public class UniwareSimpMessagingTemplate {

    private SimpMessagingTemplate template;

    @Autowired
    private ApplicationContext    applicationContext;

    public void convertAndSendToAll(Object payload) throws MessagingException {
        convertAndSend("/topic/" + UserContext.current().getTenant().getCode(), payload);
    }

    public void convertAndSendToFacility(String destination, Object payload) throws MessagingException {
        Map<String, Object> headers = new HashMap<String, Object>();
        headers.put("callback_id", UserContext.current().getRequestId());
        getTemplate().convertAndSend("/topic/facility" + destination, payload, createHeaders(), null);
    }

    public void convertAndSend(String destination, Object payload) throws MessagingException {
        Map<String, Object> headers = new HashMap<String, Object>();
        headers.put("callback_id", UserContext.current().getRequestId());
        getTemplate().convertAndSend("/topic" + destination, payload, createHeaders(), null);
    }

    public void convertAndSendToCurrentUser(Object payload) throws MessagingException {
        convertAndSendToCurrentUser("", payload);
    }

    public void convertAndSendToCurrentUser(String destination, Object payload) throws MessagingException {
        convertAndSendToUser(UserContext.current().getUniwareUserName(), destination, payload);
    }

    public void convertAndSendToUser(String user, String destination, Object payload) throws MessagingException {
        convertAndSendToUser(user, destination, payload, null);
    }

    public void convertAndSendToUser(String user, String destination, Object payload, MessageHeaders headers) throws MessagingException {
        Assert.notNull(user, "User must not be null");
        user = StringUtils.replace(user, "/", "%2F");
        getTemplate().convertAndSend(template.getUserDestinationPrefix() + user + "/topic" + destination, payload, headers != null ? headers : createHeaders(), null);
    }

    private MessageHeaders createHeaders() {
        SimpMessageHeaderAccessor headerAccessor = SimpMessageHeaderAccessor.create(SimpMessageType.MESSAGE);
        headerAccessor.addNativeHeader("callback_id", UserContext.current().getRequestId());
        headerAccessor.setLeaveMutable(true);
        return headerAccessor.getMessageHeaders();
    }

    public SimpMessagingTemplate getTemplate() {
        if (template == null) {
            synchronized (this) {
                if (template == null) {
                    template = applicationContext.getBean(SimpMessagingTemplate.class);
                }
            }
        }
        return template;
    }

    public void setTemplate(SimpMessagingTemplate template) {
        this.template = template;
    }

}
