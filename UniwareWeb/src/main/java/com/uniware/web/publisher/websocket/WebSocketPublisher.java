/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 23-May-2014
 *  @author sunny
 */
package com.uniware.web.publisher.websocket;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.uniware.services.publisher.IMessageBroadcaster;
import com.uniware.services.publisher.Message;
import com.uniware.services.publisher.Publisher;

@Component
public class WebSocketPublisher implements Publisher {

    private final String                 name = "WebSocketPublisher";

    @Autowired
    private UniwareSimpMessagingTemplate template;

    @Autowired
    private IMessageBroadcaster          messageBroadcaster;

    @PostConstruct
    public void init() {
        messageBroadcaster.addPublisher(this);
    }

    @Override
    public void publish(Message message) {
        switch (message.getType()) {
            case FACILITY:
                template.convertAndSendToFacility(message.getDestination(), message.getPayload());
                break;
            case TENANT:
                template.convertAndSend(message.getDestination(), message.getPayload());
                break;
        }
    }

    @Override
    public String getName() {
        return "WebSocketPublisher";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        WebSocketPublisher other = (WebSocketPublisher) obj;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        return true;
    }

}
