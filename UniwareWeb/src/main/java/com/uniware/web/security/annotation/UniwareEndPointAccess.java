/*
 *  Copyright 2013 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 01-May-2013
 *  @author Sunny Agarwal
 */
package com.uniware.web.security.annotation;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import com.unifier.core.annotation.Level;
import com.unifier.core.entity.AccessResource.Name;

@Target({ METHOD })
@Retention(RUNTIME)
public @interface UniwareEndPointAccess {

    Level level();

    Name accessResource();

    boolean openForInactiveTenants() default false;
}
