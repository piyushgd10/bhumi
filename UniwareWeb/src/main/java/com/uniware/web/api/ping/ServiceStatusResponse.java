package com.uniware.web.api.ping;

import com.unifier.core.api.base.ServiceResponse;

public class ServiceStatusResponse extends ServiceResponse {

    private static final long serialVersionUID = 8234668090221095336L;

    private String version;

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
}
