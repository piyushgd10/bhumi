<%@ include file="/tagIncludes.jsp"%>
<tiles:insertDefinition name=".procurePage">
	<tiles:putAttribute name="title" value="Uniware - Purchase Order" />
	<tiles:putAttribute name="rightPane">
		<div class="greybor headlable ovrhid main-box-head">
			<h2 class="edithead head-textfields">Purchase Order</h2>
		</div>
		<div class="greybor round_bottom main-boform-cont ovrhid">
			<table>
				<tr>
					<td class="searchLabel" >Choose Vendor</td>
					<td ><input type="text" class="textfield" id="vendorCode" value="${vendorName }" /></td>
					<td id="vendorAgreementText" class="searchLabel"></td>
					<td id="vendorAgreement"></td>
				</tr>
			</table><br/>
			<div id="purchaseOrderCustomFields"></div>
		</div>
		<div id="existingPurchaseOrderDiv"></div>
	</tiles:putAttribute>
	<tiles:putAttribute name="deferredScript">
		<script type="text/html" id="agreementTemplate">
			<select id="agreement" class="mar-15-left ovrhid">
				<option value="">--Select an Agreement--</option>
				<# if(obj.length == 1) { #>
					<option value="<#=obj[0].name#>" selected="selected"><#=obj[0].name#></option>
				<# } else { #>
					<# for(var i=0;i<obj.length;i++) { #>
					<option value="<#=obj[i].name#>"><#=obj[i].name#></option>				
					<# } #>
				<# } #>
			</select>						
		</script>
		<script id="existingPurchaseOrder" type="text/html">
	<div class="greybor headlable round_top ovrhid mar-15-top">
			<h4 class="edithead">Existing Purchase Order</h4>
	</div>
	<div class="greybor form-edit-table-cont round_bottom">
	<table class="uniTable" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<th>Code</th>
			<th>Status Code</th>
			<th>Vendor Agreement</th>
			<th>From Party</th>
			<th>Created On</th>
		</tr>
		<# for(var i=0; i<obj.length; i++){ var po = obj[i];#>
			<tr>
				<td><a href="/procure/poItems?legacy=1&poCode=<#=po.code#>"><#=po.code#></a></td>
				<td><#=po.statusCode#></td>
				<td><#=po.vendorAgreement#></td>
				<td><#=po.fromParty#></td>
				<td><#=(new Date(po.created)).toDateTime()#></td>	
			</tr>
		<# } #>
	</table>
	</div>

	</script>
		<script id="customFieldTemplate" type="text/html">
		<table>
		<# if (obj && obj.length > 0) { #>
				<tr>
				<# for(var i=0;i<obj.length;i++) { var customField = obj[i]; #>
		 			<td align="left" class="searchLabel"><#=customField.displayName#></td>
					<td align="left" class="searchLabel">
						<# if (customField.valueType == 'text') { #>
							<input id="<#=customField.fieldName#>" type="text" class="w150 custom poUpdateItem" value="<#=customField.defaultValue#>" style="width:70%;"/>
						<# } else if (customField.valueType == 'date') { #>
							<input id="<#=customField.fieldName#>" type="text" class="w150 custom poUpdateItem datefield " value="<#= customField.defaultValue ? new Date(customField.fieldValue).toPaddedDate() : '' #>" style="width:70%;"/>
						<# } else if (customField.valueType == 'select') { #>
							<select id="<#=customField.fieldName#>" class="w150 custom poUpdateItem" style="width:70%;">
							<# for (var j=0;j<customField.possibleValues.length;j++) { #>
								<option value="<#=customField.possibleValues[j]#>" <#= customField.possibleValues[j] == customField.defaultValue ? selected="selected" : '' #> ><#=customField.possibleValues[j]#></option>
							<# } #>
							</select>
						<# } else if (customField.valueType == 'checkbox') { #>
							<input id="<#=customField.fieldName#>" type="checkbox" class="custom poUpdateItem" <#=customField.defaultValue ? checked="checked" : '' #> style="width:70%;"/>
						<# } #>
					</td>
					<# if (i == obj.length - 1 && i % 2 == 0) { #>
						<td></td><td></td>
					<# } else if (i != 0 && i % 2 != 0) { #>
						</tr><tr>
					<# } #>
				<# } #>
				</tr>
            <# } #>
			<tr class="mar-15-top">
				<td></td>
				<td colspan="3">		
					<input type="submit" class="btn btn-primary" id="createPO" value="Create Purchase Order" style="margin-top:10px;"/>
				</td>
		 	</tr>
		</table>
	</script>
		<script type="text/javascript">
		Uniware.PurchaseOrderPage = function() {
			var self = this;
			this.vendorCode = null;			
			this.customFields = ${customFieldsJson};
			this.init = function() {
				this.listVendors();
				var vendorCode = $('#vendorCode').id;
				if(vendorCode != null && vendorCode != ''){
					self.search(vendorCode);		
				}
			};
			
			this.listVendors = function(){
			    $("#vendorCode").autocomplete({
			    	minLength: 2,
			    	mustMatch : true,
					autoFocus: true,
					source: function( request, response ) {
						Uniware.Ajax.getJson("/data/lookup/vendors?name=" + request.term, function(data) {
							response( $.map( data, function( item ) {
								return {
									label: item.name,
									vendorCode : item.code
								}
							}));
						});
					},
					select: function( event, ui ) {
						$('#vendorAgreementText').html("Vendor Agreement");
						var vendorCode = ui.item.vendorCode
						self.vendorCode = vendorCode;
						self.listAgreements(vendorCode);
						self.displayCustomFields(self.customFields);
						self.existingPurchaseOrders();
					}
			    });
			};
			 
			this.listAgreements = function(vendorCode){
				$("#searching").removeClass('hidden');
				Uniware.Ajax.getJson("/data/procure/vendor/get/agreements?vendorCode=" + vendorCode, function(response) {
					$('#vendorAgreement').html(template("agreementTemplate", response))
					$('#createPO').click(self.createPO);
					$("#searching").addClass('hidden');
				});
			};
			
			this.displayCustomFields = function(customFields){
				$("#purchaseOrderCustomFields").html(template("customFieldTemplate",customFields));
				$(".datefield").datepicker({
					dateFormat : 'dd/mm/yy'
				});
			};
			
			this.createPO = function(){
				var requestObject = {
					'vendorCode': self.vendorCode,
					'vendorAgreementName': $('#agreement').val(),
				}
				Uniware.Utils.addCustomFieldsToRequest(requestObject, $('.custom'));
			 	Uniware.Ajax.postJson("/data/po/create", JSON.stringify(requestObject), function(response) {
			 		if(response.successful == true){
			 			document.location.href = "/procure/poItems?legacy=1&poCode=" + response.purchaseOrderCode;	
			 		}else{
			 			Uniware.Utils.showError(response.errors[0].description);
			 		}
			 		
				});
			};
			
			this.existingPurchaseOrders = function() {
				Uniware.Ajax.getJson("/data/po/vendor/purchaseOrders?vendorCode=" + self.vendorCode, function(response) {
					if(response.length >0){
						$('#existingPurchaseOrderDiv').html(template("existingPurchaseOrder", response));	
					}
				});
			}
		};
					
		$(document).ready(function() {
			window.page = new Uniware.PurchaseOrderPage();
			window.page.init();
		});
	</script>
	</tiles:putAttribute>
</tiles:insertDefinition>
