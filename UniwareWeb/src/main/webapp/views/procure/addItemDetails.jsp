<%@ include file="/tagIncludes.jsp"%>
<tiles:insertDefinition name=".procurePage">
	<tiles:putAttribute name="title" value="Uniware - Add Item Details" />
	<tiles:putAttribute name="rightPane">
		<div class="greybor headlable ovrhid main-box-head">
			<h2 class="edithead head-textfields">Add Item Details</h2>
		</div>
		<div class="round_bottom ovrhid pad-15">
			<div class="lfloat20">
				<div class="searchLabel">Scan Item Code</div>
				<input type="text" id="itemCode" size="20" autocomplete="off"/>
			</div>
			
		<div id="searching" class="lfloat10 hidden" style="margin-top:25px;">
			<img src="/img/icons/refresh-animated.gif"/>
		</div>
		<div class="clear"></div><br/>
		<table id="customFields"></table>
		</div>
		

	</tiles:putAttribute>
	<tiles:putAttribute name="deferredScript">
		<script id="customFieldsTemplate" type="text/html">
			<# if(obj.length >0) { #>
				<# for(var i=0; i<obj.length; i++){ var customField = obj[i]; #>
						<div class="formLeft lfloat"><#=customField.displayName#></div>
						<div class="formRight lfloat">
							<input type="text" class="customField" name="<#=customField.name#>" value="<#=customField.value#>"/>
						</div>
						<div class="clear"></div>
				<# } #>
				<div class="clear"></div>
				<br/>
				<div class="formLeft lfloat">&#160;</div>
				<div class="formRight lfloat">
					<input type="submit" class=" btn btn-success" id="addItemDetails" value="update"/>
				</div>
			<# }else { #>
				<div  style="margin:15px;"><em class="f15">No Custom Fields</em></div>
			<# } #>
		</script>
		<script type="text/javascript">
			Uniware.addItemDetails = function() {
				var self = this;
				this.itemCode;
				
				this.init = function(){
					$('#itemCode').keyup(self.search);
				};
				
				this.search = function(event){
					if (event.which == 13) {
						var itemCode = $('#itemCode').val();
						$("#searching").removeClass('hidden');
						var req = {
								itemCode : $('#itemCode').val()
						}
						Uniware.Ajax.postJson("/data/admin/system/item/customFields/get" , JSON.stringify(req), function(response) {
							if(response.successful){
								self.itemCode = itemCode;
								$('#customFields').html(template("customFieldsTemplate", response.customFields));
								$('.customField:first').focus();
								$('.customField').each(function(index){
										$(this).keyup(function(event){
											if(event.which == 13){
												var field = $('.customField:eq(' + (index+1) + ')');
												if(field.length > 0){
													field.focus();	
												}else{
													self.addItemDetails();
												}
											}
										});
								});
								$("#searching").addClass('hidden');
							    $('#addItemDetails').click(self.addItemDetails);	
							}else{
								Uniware.Utils.showError(response.errors[0].description + ": ''"  +$('#itemCode').val() + "''");
								$('#itemCode').val("")
								$('#customFields').html(template("customFieldsTemplate", {}));
							}
							$("#searching").addClass('hidden');
						});
					}
				};
				
				this.addItemDetails = function(){
					var req = {
							itemCode: self.itemCode,
							itemCustomValues:[]
					};
					
					$('.customField').each(function(index){
						var itemCustomValue = {}; 
						itemCustomValue.name = $(this).attr("name");
						itemCustomValue.value = $(this).val();
						req.itemCustomValues[req.itemCustomValues.length] = itemCustomValue;
					});
					
					Uniware.Ajax.postJson("/data/admin/system/itemsDetails/add", JSON.stringify(req),  function(response) {
							if(response.successful){
								$('#customFields').html(template("customFieldsTemplate", {}));
								$('#itemCode').val("").focus();
								Uniware.Utils.addNotification("Item has been updated");
							}else{
								Uniware.Utils.showError(response.errors[0].description);
							}
						
					});
				};
				
			}
				
			
			$(document).ready(function() {
				window.page = new Uniware.addItemDetails();
				window.page.init();
			});
		</script>
	</tiles:putAttribute>
</tiles:insertDefinition>
