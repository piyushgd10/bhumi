<%@ include file="/tagIncludes.jsp"%>
<tiles:insertDefinition name=".procurePage">
	<tiles:putAttribute name="title" value="Uniware - Reorder" />
	<tiles:putAttribute name="rightPane">
		<form onsubmit="javascript : return false;">
			<div class="headlable round_top ovrhid main-box-head">
				<h2 class="edithead head-textfields">Reorders</h2>
			</div>
			<div class="ovrhid pad-15">
				<div class="lfloat20">
					<div class="searchLabel">Vendor Name</div>
					<input type="text" id="vendorName" size="20" autocomplete="off" />
				</div>
				<div class="lfloat20">
					<div class="searchLabel">Item Type Name/Code Contains</div>
					<input type="text" id="itemTypeName" size="30" autocomplete="off" />
				</div>
				<div class="lfloat20">
					<div class="searchLabel">Category</div>
					<select id="category" style="width:150px;">
					<option value="">--ALL--</option>
					<c:forEach items="${cache.getCache('categoryCache').categories}" var="category">
						<option value="${category.code }">${category.name}</option>
					</c:forEach>
					</select>
				</div>
				<div class="lfloat20">	
					<div class="searchLabel">&#160;</div>
					<input id="search" value="search" type="submit" class="btn btn-primary" />
				</div>
			</div>
		</form>
		
		<div id="suggestionBody">
		</div>
	</tiles:putAttribute>
	<tiles:putAttribute name="deferredScript">
	<script type="text/html" id="suggestionTemplate">
		<# if(obj.itemTypes != null) {#>
			<div id="addToCart" class=" btn btn-primary rfloat20" style="margin:10px 40px;">Add to Cart</div>
			<table class="uniTable mar-15-top" cellpadding="0" cellspacing="0">
				<tr>
					<th width="30" align="center"><input type="checkbox" id="selectAll" /></th>
					<th width="50">S No.</th>
					<th>Item Name</th>
					<th>Item SKU</th>
					<th width="7%">Quantity Deficit</th>
					<th width="7%">Quantity in cart</th>
					<th width="7%">Item Bucket Size</th>
					<th width="15%">Vendor</th>
					<th width="5%">Reorder Quantity</th>
					<th width="5%">Quantity to order</th>
					<th width="5%">Unit Price</th>
				</tr>
				<# for (var j=0;j<obj.itemTypes.length;j++) {var poItem = obj.itemTypes[j];var qtyToAdd = poItem.reorderQuantity;#>
				<# if (obj.cart[poItem.skuCode]) { qtyToAdd = qtyToAdd - obj.cart[poItem.skuCode]; } #>
				<tr>
					<td align="center"><input type="checkbox" id="check-<#=j#>" class="item-check" <#=poItem.vendorItemTypes.length > 0 && qtyToAdd > 0 ? '' : disabled="disabled"#>/></th>
					<td><#=j+1#></td>
					<td>
						<#=poItem.name#>
						<#=Uniware.Utils.getItemDetail(poItem.itemTypeImageUrl, poItem.itemTypePageUrl)#>
					</td>
					<td class="ovrhid" title="<#=poItem.skuCode#>"><#=poItem.skuCode#></td>
					<td align="center"><#=poItem.waitingQuantity#></td>
					<td align="center" class="bold"><#=obj.cart[poItem.skuCode]#></td>
					<td align="center"><#=poItem.minOrderSize#></td>
					<# if (poItem.vendorItemTypes.length == 0) { #>
						<td colspan="3">
							<em class="f12">No Vendor for this item type</em>
						</td>
					<# } else if (qtyToAdd <= 0) { #>
						<td colspan="3">
							<em class="f12">Quantity already added in cart</em>
						</td>
					<# } else { #>
						<td>
						<select class="vendor field-width" id="vendor-<#=j#>">
						<# for (var k=0;k<poItem.vendorItemTypes.length;k++) { var vit = poItem.vendorItemTypes[k]; #>
							<option value="<#=k#>"><#=vit.vendorName#> (<#=vit.unitPrice#>)</option>
						<# } #>
						</select>
						</td>
						<td><input class="qtyToRaise" id="<#=j#>" type="text" value="<#=qtyToAdd > 0 ? qtyToAdd : 0#>" style="width:80%;"/></td>
						<td><div id="qty-<#=j#>"><#=qtyToAdd > 0 ? Math.ceil(qtyToAdd/poItem.minOrderSize)*poItem.minOrderSize : 0#></div></td>
						<td><input id="unitPrice-<#=j#>" type="text" value="<#=poItem.vendorItemTypes[0].unitPrice#>" style="width:80%;"/></td>
					<# } #>
				</tr>
				<# } #>
			</table><br/>
		<# } else { #>
			<div class="f15" style="padding:25px;"><em>No Reorder Items Found to Add to Cart</em></div>
		<# } #>
	</script>	
	<script type="text/javascript">
		Uniware.ReorderPage = function() {
			var self = this;
			this.purchaseCart = null;
			this.cartQuantity = {};
			this.reorders = null;
			this.vendor = null;
			
			this.init = function() {
				self.listVendors();
				self.getCart();
			};
			
			this.listVendors = function(){
			    $("#vendorName").autocomplete({
			    	minLength: 2,
			    	mustMatch : true,
					autoFocus: true,
					source: function( request, response ) {
						Uniware.Ajax.getJson("/data/lookup/vendors?name=" + request.term, function(data) {
							response( $.map( data, function( item ) {
								return {
									label: item.name,
								}
							}));
						});
					}
			    });
			};
			
			this.getCart = function(){
				Uniware.Ajax.getJson("/data/procure/getCart", function(response) {
					self.purchaseCart = response;
					for (var vendorId in response.vendorToPurchaseCartItems) {
						var vendorPO = response.vendorToPurchaseCartItems[vendorId];
						for (var i=0;i<vendorPO.purchaseCartItems.length;i++) {
							var poItem = vendorPO.purchaseCartItems[i];
							self.cartQuantity[poItem.itemSku] = self.cartQuantity[poItem.itemSku] ? self.cartQuantity[poItem.itemSku] + poItem.quantity : poItem.quantity;
						}
					}
					$('#search').click(self.search);
				});
			};
			
			
			this.search = function() {
				//verify vendor
				var vendorName = $("#vendorName").val();
				if(vendorName != ""){
					Uniware.Ajax.getJson("/data/lookup/vendors?name=" + vendorName, function(data) {
						if(data.length > 0){
							if(vendorName.toLowerCase() == data[0].name.toLowerCase()){
								self.searchReorders(data[0].id);
							}else{
								Uniware.Utils.showError("Please select a valid vendor");
							}
						}else{
							Uniware.Utils.showError("Please select a valid vendor");
						}
					});	
				}else{
					self.searchReorders();
				}
			};
			
			this.searchReorders = function(vendorId){
				$("#searching").removeClass('hidden');
				var req={
						vendorId: vendorId,
						itemTypeName: $('#itemTypeName').val(),
						categoryCode: $('#category').val()					
				}
				
				Uniware.Ajax.postJson("/data/procure/reorders/get", JSON.stringify(req), function(response) {
					if (response.itemTypes.length > 0) {
						self.reorders = response;
						self.reorders.cart = self.cartQuantity;
						$("#suggestionBody").html(template("suggestionTemplate", self.reorders));
						$(".qtyToRaise").keyup(self.setQuantityToOrder);
						Uniware.Utils.applyHover();
						$("div#addToCart").click(self.addItemsToCart);
						$("input#selectAll").click(function(){
							var state = $("input#selectAll").is(":checked");
							$("input.item-check:not(:disabled)").attr('checked', state);
						});
						$("select.vendor").change(function(){
							var rowId = parseInt($(this).attr('id').substring($(this).attr('id').indexOf('-') + 1));
							var reorderItem = self.reorders.itemTypes[rowId];
							
							
							var vendor = reorderItem.vendorItemTypes[$("#vendor-"+rowId).val()];
							$("#unitPrice-"+rowId).val(vendor.unitPrice);
						});
					} else {
						$("#suggestionBody").html(template("suggestionTemplate", {}));
					}
					$("#searching").addClass('hidden');
				});
			};
			
			this.setQuantityToOrder = function(){
				var rowId = $(this).attr('id');
				var qtyToRaise = $(this).val();
				var reorderItem = self.reorders.itemTypes[rowId];
				$('#qty-' + rowId).html(Math.ceil(qtyToRaise/reorderItem.minOrderSize)*reorderItem.minOrderSize);
				
			};
			
			this.addItemsToCart = function() {
				var requestObject = {
					purchaseCartItems : []
				};
				var checkedRows = $("input.item-check:checked");
				if (checkedRows.length == 0) {
					alert("No Items selected");
					return;
				}
				checkedRows.each(function(){
					var rowId = parseInt($(this).attr('id').substring($(this).attr('id').indexOf('-') + 1));
					var reorderItem = self.reorders.itemTypes[rowId];
					
					var cartItem = {};
					cartItem.vendorCode = reorderItem.vendorItemTypes[$("#vendor-"+rowId).val()].vendorCode;
					cartItem.itemSku = reorderItem.skuCode;
					cartItem.quantity = $("#qty-"+rowId).html();
					cartItem.unitPrice = $("#unitPrice-"+rowId).val();
			        requestObject.purchaseCartItems.push(cartItem);
				});
				
			 	Uniware.Ajax.postJson("/data/procure/itemType/addToCart", JSON.stringify(requestObject), function(response) {
			 		if(response.successful == true) {
			 			Uniware.Utils.addNotification('Items Added to Cart');
			 			$("#suggestionBody").html('');
			 			self.init();
			 		} else {
			 			Uniware.Utils.showError(response.errors[0].description);
			 		}
				}, true);
			};
		};
		
		$(document).ready(function() {
			window.page = new Uniware.ReorderPage();
			window.page.init();
		});
	</script>
	</tiles:putAttribute>
</tiles:insertDefinition>			
