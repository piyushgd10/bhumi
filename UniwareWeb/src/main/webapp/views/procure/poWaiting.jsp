<%@ include file="/tagIncludes.jsp"%>
<tiles:insertDefinition name=".procurePage">
	<tiles:putAttribute name="title" value="Uniware - Purchase Order Waiting for Approval" />
	<tiles:putAttribute name="rightPane">
		<div class="greybor headlable round_top ovrhid">
			<h2 class="edithead">Purchase Orders Waiting For Approval</h2>
		</div>
		<div id="purchaseOrdersDiv"></div>
	</tiles:putAttribute>
	<tiles:putAttribute name="deferredScript">

	<script id="purchaseOrder" type="text/html">
	<div class="form-edit-table-cont" style="padding:0 10px;">
		<#if(obj.length == 0) { #>
			<div  style="margin:15px;"><em class="f15">No Purchase Orders to Approve</em></div>
		<# } else { #>
		<uc:security accessResource="PO_APPROVE">
		<br/>
		<div id="approvePOs" class=" rfloat20 btn btn-primary">Approve Purchase Orders</div>
		<div class="clear"/>
		</br>
		</uc:security>
		<table class="dataTable" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td width="8%"><input type="checkbox" id="selectAll"/></td>
			<td width="20%">Code</td>
			<td>Vendor Name</td>
			<td>From Party</td>
			<td>Created On</td>
		</tr>
		<# for(var i=0; i<obj.length; i++){ var po = obj[i];#>
			<tr>
				<td><input type="checkbox" id="select-<#=po.code#>" class="pocheck"/></td>
				<td><a href="/procure/poItems?legacy=1&poCode=<#=po.code#>"><#=po.code#></a></td>
				<td><#=po.vendorName#></td>
				<td><#=po.fromParty#></td>
				<td><#=(new Date(po.created)).toDateTime()#></td>
			</tr>
		<# } #>
		</table>
		<# } #>
	</div>
	</script>
	
	<script type="text/javascript">
		Uniware.POWaiting = function() {
			var self = this;
			this.init = function() {
				Uniware.Ajax.getJson("/data/po/waiting", function(response) {
					$('#purchaseOrdersDiv').html(template("purchaseOrder", response));
					$("input#selectAll").click(function(){
						var state = $("input#selectAll").is(":checked");
						$("input.pocheck").attr('checked', state);
					});
					$('#approvePOs').click(self.approvePOs);
				});
			};
			
			this.approvePOs = function(){
				var req = {
					'purchaseOrderCodes' : []	
				};
				var checkedRows = $("input.pocheck:checked");
				if (checkedRows.length == 0) {
					alert("No Purchase Order selected");
					return;
				}
				checkedRows.each(function(){
					var poCode = $(this).attr('id').substring($(this).attr('id').indexOf('-') + 1);
					req.purchaseOrderCodes.push(poCode);
				});
				Uniware.Ajax.postJson("/data/po/bulk/approve", JSON.stringify(req), function(response) {
			 		if(response.successful == true) {
			 			Uniware.Utils.addNotification('Purchase Orders have been approved');
			 			self.init();
			 		} else {
			 			Uniware.Utils.showError(response.errors[0].description);
			 		}
				}, true);
			};
		};
					
		$(document).ready(function() {
			window.page = new Uniware.POWaiting();
			window.page.init();
		});
	</script>
	</tiles:putAttribute>
</tiles:insertDefinition>
