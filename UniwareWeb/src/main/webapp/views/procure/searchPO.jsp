<%@ include file="/tagIncludes.jsp"%>
<tiles:insertDefinition name=".procurePage">
	<tiles:putAttribute name="title" value="Uniware - Purchase Orders" />
	<tiles:putAttribute name="rightPane">
		<div id="pageBar">
			<div class="pageHeading"><span class="mainHeading">Purchase Orders<span class="pipe">/</span></span><span class="subHeading"></span></div>
			<div class="pageControls"></div>
			<div class="clear"></div> 
		</div>
		<div id="tableContainer">
			<table id="flexme1" style="display: none"></table>
		</div>
	</tiles:putAttribute>
	<tiles:putAttribute name="deferredScript">
    <sec:authentication property="principal" var="user" />
	<script type="text/javascript" src="${path.js('jquery/jquery.dataTables.min.js')}"></script>
	<script type="text/javascript">
		Uniware.SearchPurchaseOrdersPage = function() {
			var self = this;
			this.name = 'DATATABLE SEARCH PURCHASE ORDERS';
			this.pageConfig = null;
			this.table = null;
			this.datatableViews = ${user.getDatatableViewsJson("DATATABLE SEARCH PURCHASE ORDERS")};
			this.init = function(){
				if(this.datatableViews.length){
					this.headConfig = new Uniware.HeadConfig(this.datatableViews,function(datatableObject){
						self.pageConfig.setView(datatableObject);
					});
				}
				$("#tableContainer").on('click','.cancel',self.closePO);
				this.pageConfig = new Uniware.PageConfig(this.name,'#flexme1',this.datatableViews[0]);
			};
			
			this.closePO = function(event){
				if(!confirm("Do you really want to close this purchase order?")){
					return false;	
				}else{
					var tr = $(event.target).parents('tr');
					var rowNumber = $(tr).attr('id').split('-')[1];
					var data = self.pageConfig.getRecord(rowNumber);
					var req = {
	 				    	purchaseOrderCode: self.pageConfig.getColumnValue(data,'code')
	 				}
					Uniware.Ajax.postJson("/data/po/close", JSON.stringify(req), function(response) {
						if(response.successful == true){
							Uniware.Utils.addNotification("Purchase Order has been closed");
							$(tr[0]).html(template("purchaseOrderRow", response.purchaseOrder));
						}else{
							Uniware.Utils.showError(response.errors[0].description);
						}
					}, true);	
				}
			};
		};
						
		$(document).ready(function() {
			window.page = new Uniware.SearchPurchaseOrdersPage();
			window.page.init();
		});
	</script>
	</tiles:putAttribute>
</tiles:insertDefinition>

