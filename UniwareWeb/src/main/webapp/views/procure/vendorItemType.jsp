<%@ include file="/tagIncludes.jsp"%>
<tiles:insertDefinition name=".procurePage">
	<tiles:putAttribute name="title" value="Uniware - Vendor Catalog" />
	<tiles:putAttribute name="rightPane">
		<div id="pageBar">
			<div class="pageHeading"><span class="mainHeading">Vendor Catalog<span class="pipe">/</span></span><span class="subHeading"></span></div>
			<div class="pageControls"></div>
			<div class="btn btn-success rfloat20" id="addVendorType" style="margin: 10px 5px;">Add Vendor Item Mapping</div>
			<div class="clear"></div> 
		</div>
		<div id="vendorItemTypeDiv" class="lb-over">
			<div class="lb-over-inner round_all">
				<div style="margin: 40px;line-height: 30px;">
					<div class="pageHeading lfloat">Create Vendor ItemType</div>
					<div id="vendorItemTypeDiv_close" class="link rfloat">close</div>
					<div class="clear"></div> 	
					<div id="vendorItemTypeTemplateDiv" style="margin:20px 0;">
					</div>
				</div>
			</div>
		</div>
		<div id="vendorItemTypeEditDiv" class="lb-over">
			<div class="lb-over-inner round_all">
				<div style="margin: 40px;line-height: 30px;">
					<div class="pageHeading lfloat">Edit Vendor ItemType</div>
					<div id="vendorItemTypeEditDiv_close" class="link rfloat">close</div>
					<div class="clear"></div> 	
					<div id="vendorEditItemTypeTemplateDiv" style="margin:20px 0;">
					</div>
				</div>
			</div>
		</div>
		<div id="tableContainer">
			<table id="flexme1" style="display: none"></table>
		</div>
	</tiles:putAttribute>
	<tiles:putAttribute name="deferredScript">
    <sec:authentication property="principal" var="user" />
	<script type="text/javascript" src="${path.js('jquery/jquery.dataTables.min.js')}"></script>
	<script type="text/javascript">
		Uniware.SearchVendorCatalogPage = function() {
			var self = this;
			this.name = 'DATATABLE SEARCH VENDOR CATALOG';
			this.pageConfig = null;
			this.table = null;
			this.vendorCode = null;
			this.editRow = null;
			this.datatableViews = ${user.getDatatableViewsJson("DATATABLE SEARCH VENDOR CATALOG")};
			this.init = function(){
				$("#vendorName").autocomplete(self.getAutoCompleteObj());
				$('#addVendorType').click(self.addVendorItemType);
				if(this.datatableViews.length){
					this.headConfig = new Uniware.HeadConfig(this.datatableViews,function(datatableObject){
						self.pageConfig.setView(datatableObject);
					});
				}
				var vendorCode = '${param['code']}';
            	if (vendorCode != '') {
            		var configObject = this.datatableViews[0].configJson;
            		if (typeof configObject != 'object') {
            			configObject = $.parseJSON(this.datatableViews[0].configJson);
            		}
            		configObject['filters'].push({
            			'id' : 'vendorCodeFilter',
            			'text' : vendorCode
            		});
            		if (typeof configObject == 'object') {
            			this.datatableViews[0].configJson = JSON.stringify(configObject);
            		}
            	}
				this.pageConfig = new Uniware.PageConfig(this.name,'#flexme1',this.datatableViews[0]);
				$('#tableContainer').on('click', '.editItemType', self.editVendorItemType);
			};
			
			this.getAutoCompleteObj = function(){
				return {
				    	mustMatch : true,
						autoFocus: true,
						source: function( request, response ) {
							Uniware.Ajax.getJson("/data/lookup/vendors?name=" + request.term, function(data) {
								response( $.map( data.elements, function( item ) {
									return {
										label: item.name,
										vendor: item
									}
								}));
							});
						},
						select: function(event, ui){
							self.vendorCode = ui.item.vendor.code
						}
				 };
			};
			
			this.editVendorItemType = function(event) {
				var tr = $(event.target).parents('tr');
				var rowNumber = $(tr).attr('id').split('-')[1];
				var data = self.pageConfig.getRecord(rowNumber);
				var obj = {
						'vendorName' : self.pageConfig.getColumnValue(data,'vendorName'),
						'vendorSkuCode' : self.pageConfig.getColumnValue(data,'vendorSkuCode'),
						'unitPrice' : self.pageConfig.getColumnValue(data,'unitPrice'),
						'enabled' : self.pageConfig.getColumnValue(data,'enabled')
				};
				self.editRow = data;
				$("#vendorEditItemTypeTemplateDiv").html(template("editVendorItemTypeTemplate",obj));
				$("#updateVendorItemType").click(self.update);
				Uniware.LightBox.show("#vendorItemTypeEditDiv");
			};
			
			this.update = function() {
				var vendorSkuCode =$("#editVendorSkuCode").val();
				var unitPrice = $("#editUnitPrice").val();
				var data = self.editRow;
				var requestObject = {'vendorItemType' :{
					'vendorCode' : self.pageConfig.getColumnValue(data,'vendorCode'),
					'itemTypeSkuCode' : self.pageConfig.getColumnValue(data,'productCode'),
					'vendorSkuCode' : vendorSkuCode,
					'unitPrice' : unitPrice,
					'enabled' :$("#enabled").is(':checked')
				}};
				self.editRow = null;
				Uniware.Ajax.postJson("/data/procure/vendorItemType/edit", JSON.stringify(requestObject), function(response) {
					if (response.successful == false) {
						Uniware.Utils.showError(response.errors[0].description);
						Uniware.LightBox.hide();
					} else {
						Uniware.Utils.addNotification('Item type "'+ self.pageConfig.getColumnValue(data,'itemType') + '" has been updated');
						Uniware.LightBox.hide();
						self.pageConfig.reload();
					}
				});
			}
			
			
			this.addVendorItemType = function() {
				$("#vendorItemTypeTemplateDiv").html(template("vendorItemTypeTemplate"));
				self.searchItemType();
				$("#vendorCode").autocomplete(self.getAutoCompleteObj());
				$("#submitVendorItemType").click(self.createVendorItemType);
				Uniware.LightBox.show('#vendorItemTypeDiv');
			};
			
			this.searchItemType = function(){
			    $("#itemType").autocomplete({
			    	minLength: 2,
			    	mustMatch : true,
					autoFocus: true,
					source: function( request, response ) {
						var requestObject = {
								keyword : $('#itemType').val()
						}
						Uniware.Ajax.postJson("/data/procure/catalog/search", JSON.stringify(requestObject), function(data) {
							response( $.map( data.elements, function( item ) {
								return {
									label: item.name + " - " + item.skuCode,
									value: item.name + " - " + item.skuCode,
									code: item.skuCode
								}
							}));
						});
					},   
					select: function( event, ui ) {
						$('#itemType').attr("skuCode", ui.item.code);
					}
			    });	
			};
			
			this.getVendorItemTypeRequest = function() {
				var vendorItemType = {};
				vendorItemType.vendorCode = self.vendorCode;
				vendorItemType.vendorSkuCode = $("#vendorSkuCode").val();
				vendorItemType.unitPrice = $("#unitPrice").val();
				vendorItemType.itemTypeSkuCode = $('#itemType').attr("skuCode")
				return {'vendorItemType':vendorItemType};
			};
			
			
			this.createVendorItemType = function() {
				var req = self.getVendorItemTypeRequest();
				Uniware.Ajax.postJson('/data/procure/vendor/itemType/create', JSON.stringify(req), function(response) {
					if (response.successful == false) {
						$("#error").html(response.errors[0].description).css('visibility', 'visible');
					} else {
						Uniware.Utils.addNotification('Vendor Item Type has been added');
						Uniware.LightBox.hide();
						self.pageConfig.reload();
					}
				});
			};
		};
						
		$(document).ready(function() {
			window.page = new Uniware.SearchVendorCatalogPage();
			window.page.init();
		});
	</script>
	<script type="text/html" id="vendorItemTypeTemplate">
		<form onsubmit="javascript : return false;">
			<div class="formLeft150 lfloat">Vendor</div>
			<div class="formRight lfloat"><input type="text" id="vendorCode"  size="30"/></div>
			<div class="clear"></div>

            <div class="formLeft150 lfloat">Item SKU Code</div>
            <div class="formRight lfloat"><input type="text" id="itemType"  size="30"/></div>
            <div class="clear"></div>

            <div class="formLeft150 lfloat">Vendor SKU Code</div>
            <div class="formRight lfloat"><input type="text" id="vendorSkuCode" autocomplete="off"  size="30"/></div>
            <div class="clear"></div>

			<div class="formLeft150 lfloat">Vendor Unit Price</div>
			<div class="formRight lfloat"><input type="text" id="unitPrice" autocomplete="off"  size="30" /></div>
			<div class="clear"></div>


			<br />
			<div class="formRight lfloat">
				<input type="submit" id="submitVendorItemType" class=" btn btn-small btn-primary lfloat" value="submit"/>
				<div id="error" class="errorField lfloat20"></div>
			</div>
			<div class="clear"></div>
		</form>
	</script>
	<script type="text/html" id="editVendorItemTypeTemplate">
			<div class="formLeft150 lfloat">Vendor Name</div>
			<div class="formRight lfloat"><#=obj.vendorName#></div>
			<div class="clear"></div>

			<div class="formLeft150 lfloat">Vendor Unit Price</div>
			<div class="formRight lfloat"><input type="text" id="editUnitPrice" value="<#=obj.unitPrice#>" size="30"/></div>
			<div class="clear"></div>
			
			<div class="formLeft150 lfloat">Vendor SKU Code</div>
			<div class="formRight lfloat"><input type="text" id="editVendorSkuCode" autocomplete="off" value="<#=obj.vendorSkuCode#>" size="30"/></div>
			<div class="clear"></div>

			<div class="formLeft150 lfloat">Enabled</div>
			<div class="formRight lfloat"><input type="checkbox" id="enabled" autocomplete="off" <#= obj.enabled == true ? checked="checked" : ""#> size="30"/></div>
			<div class="clear"></div>
			<br />

			<div class="formRight lfloat">
				<input type="submit" id="updateVendorItemType" class=" btn btn-small btn-primary lfloat" value="submit"/>
				<div id="error" class="errorField lfloat20"></div>
			</div>
			<div class="clear"></div>
	</script>
	</tiles:putAttribute>
</tiles:insertDefinition>
