<%@ include file="/tagIncludes.jsp"%>
<tiles:insertDefinition name=".procurePage">
	<tiles:putAttribute name="title" value="Uniware - Create Purchase Order" />
	<tiles:putAttribute name="rightPane">
		<div id="purchaseOrderDiv"></div>
	</tiles:putAttribute>

	<tiles:putAttribute name="deferredScript">
		<link href="${path.css('handsontable/jquery.handsontable.css')}" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="${path.js('handsontable/jquery.handsontable.js')}"></script>
		<script id="purchaseOrderTemplate" type="text/html">
		<div class="greybor headlable ovrhid main-box-head">
			<h2 class="edithead head-textfields">Purchase Order</h2>
		</div>
		<div class="greybor round_bottom main-boform-cont pad-15-top ovrhid mar-15-bot">
		<table width="100%" border="0" cellspacing="1" cellpadding="3" class="fields-table">
			<# if (obj.amendedPurchaseOrderCode) { #>
			<tr>
				<td colspan="4" class="f12 ucase">
					<a href="/procure/poItems?legacy=1&poCode=<#=obj.amendedPurchaseOrderCode#>">Amendment for Purchase Order <#=obj.amendedPurchaseOrderCode#></a>
				</td>
			</tr>
			<# } #>
			<tr> 
				<td>Purchase Order Code</td>
		  		<td><#=obj.code#></td>
        		<td>Vendor Name</td>
        		<td>
					<# if (obj.isCloning) { #>
						<input type="text" value="<#=obj.vendorName#>" id="vendorName"/>
					<# } else { #>
						<#=obj.vendorName#>
					<# } #>
				</td>    
			</tr>
			<tr>
		  		<td>Type</td>
		  		<td>
					<# if (obj.statusCode == 'CREATED') { #>
					<c:set var="reasons" value="${cache.getCache('uiCustomizations').getUICustomListOptions('poItems:purchaseOrderType')}"></c:set>
					<c:choose>
						<c:when test="${reasons ne null}">
							<select id="purchaseOrderType">
							<c:forEach items="${reasons}" var="reason">
								<option value="${reason.value}" <#=obj.type == '${reason.value}' ? selected="selected" : ''#>><c:out value="${reason.name}" /></option>
							</c:forEach>
							</select>
						</c:when>
						<c:otherwise>
							<select id="purchaseOrderType">
								<option value="CART" <#=obj.type == 'CART' ? selected="selected" : ''#>>CART</option>
								<option value="MANUAL" <#=obj.type == 'MANUAL' ? selected="selected" : ''#>>MANUAL</option>
							</select>
						</c:otherwise>
					</c:choose>
						
					<# } else { #>
						<#=obj.type#>
					<# } #>
				</td>
		  		<td>Status</td>
		  		<td>
					<#=obj.statusCode#>
					<# if (obj.amendedPurchaseOrderCode) { #>
						- <a href="/procure/poItems?legacy=1&poCode=<#=obj.amendedPurchaseOrderCode#>"><#=obj.amendedPurchaseOrderCode#></a>
					<# } #>
				</td>
  			</tr>
			<tr>
		 		<td>From Party</td>
		  		<td><#=obj.fromParty#></td>
				<td>Agreement</td>
				<td><#=obj.vendorAgreementName#></td
  			</tr>
			<tr>
				<td>Created On</td>
	        	<td><#=(new Date(obj.created)).toDateTime()#></td>
		 		<td>Expiry Date</td>
		  		<td>
					<# if (obj.isAmending != true && (obj.statusCode == 'CREATED' || obj.statusCode == 'WAITING_FOR_APPROVAL')) { #>
						<input id="expiryDate" type="text" class="datefield w150 poUpdateItem" value="<#=obj.expiryDate ? new Date(obj.expiryDate).toPaddedDate() : ''#>"/>
					<# } else { #>
						<#= obj.expiryDate ? new Date(obj.expiryDate).toPaddedDate() : '' #>
					<# } #>
				</td>
			</tr>
			<tr>
				<td>Delivery Date</td>
		  		<td colspan="3">
					<# if (obj.isAmending != true && (obj.statusCode == 'CREATED' || obj.statusCode == 'WAITING_FOR_APPROVAL')) { #>
						<input id="deliveryDate" type="text" class="datefield w150 poUpdateItem" value="<#=obj.deliveryDate ? new Date(obj.deliveryDate).toPaddedDate() : ''#>"/>
					<# } else { #>
						<#= obj.deliveryDate ? new Date(obj.deliveryDate).toPaddedDate() : '' #>
					<# } #>
				</td>
  			</tr>
			<# if (obj.customFieldValues && obj.customFieldValues.length > 0) { #>
				<tr>
				<# for(var i=0;i<obj.customFieldValues.length;i++) { var customField = obj.customFieldValues[i]; #>
		 			<td><#=customField.displayName#></td>
					<td>
					<# if (Uniware.Utils.editPOMetadata && obj.isAmending != true && (obj.statusCode == 'CREATED' || obj.statusCode == 'WAITING_FOR_APPROVAL' || obj.statusCode == 'APPROVED')) { #>
						<# if (customField.valueType == 'text') { #>
							<input id="<#=customField.fieldName#>" type="text" class="w150 custom poUpdateItem" value="<#=customField.fieldValue#>"/>
						<# } else if (customField.valueType == 'date') { #>
							<input id="<#=customField.fieldName#>" type="text" class="w150 custom poUpdateItem datefield" value="<#= customField.fieldValue ? new Date(customField.fieldValue).toPaddedDate() : '' #>"/>
						<# } else if (customField.valueType == 'select') { #>
							<select id="<#=customField.fieldName#>" class="w150 custom poUpdateItem">
							<# for (var j=0;j<customField.possibleValues.length;j++) { #>
								<option value="<#=customField.possibleValues[j]#>" <#= customField.possibleValues[j] == customField.fieldValue ? selected="selected" : '' #> ><#=customField.possibleValues[j]#></option>
							<# } #>
							</select>
						<# } else if (customField.valueType == 'checkbox') { #>
							<input id="<#=customField.fieldName#>" type="checkbox" class="custom poUpdateItem" <#=customField.fieldValue ? checked="checked" : '' #>/>
						<# } #>
					<# } else { #>
						<# if (customField.valueType == 'date') { #>
							<#= customField.fieldValue ? new Date(customField.fieldValue).toPaddedDate() : '' #>
						<# } else if (customField.valueType == 'checkbox') { #>
							<input type="checkbox" disabled="disabled" <#=customField.fieldValue ? checked="checked" : '' #>/>
						<# } else { #>
							<#=customField.fieldValue#>
						<# } #>
					<# } #>
					</td>
					<# if (i == obj.customFieldValues.length - 1 && i % 2 == 0) { #>
						<td></td><td></td>
					<# } else if (i != 0 && i % 2 != 0) { #>
						</tr><tr>
					<# } #>
				<# } #>
				</tr>
            <# } #>
			<tr>
		 		<td>&#160;</td>
		  		<td colspan="3">
					<div id="poUpdateDiv" class="hidden">
						<input type="submit" class="btn btn-small btn-primary lfloat" value="update" id="updatePO"></input>
						<div class="link lfloat10" id="cancelUpdate" style="margin-top:4px">cancel</span>
						<div class="clear"/>
					</div>
				</td>
  			</tr>
		</table>
		
		<div>
			<div id="failerLog" class="lfloat channelTab handCrsr channelTabActive" style="margin-left:0px;">
                <div class="lfloat channelLeftTab" ></div>
                <div class="lfloat channelTabText f15" style="width:100px;font-weight:normal;">Order Items</div>
                <div class="lfloat channelRightTab"></div>
           </div>
		</div>
		<# if(obj.statusCode != 'CREATED' && obj.statusCode != 'WAITING_FOR_APPROVAL') { #>
            <div class="btn-group rfloat10">
                <button class="btn btn-small dropdown-toggle" data-toggle="dropdown">clone <span class="caret"></span></button>
                <ul class="dropdown-menu pull-right">
                  <li><a href="#" class="clone" id="pending">Pending Items</a></li>
                  <li><a href="#" class="clone" id="rejected">Rejected Items</a></li>
                  <li><a href="#" class="clone" id="allPOItems">All Items</a></li>
                </ul>
            </div>
		<# } #>

		<# if(obj.statusCode == 'APPROVED') { #>
			<uc:security accessResource="PO_APPROVE">
			<div id="div_<#=obj.code#>" class="btn btn-small rfloat10 email">resend email</div>
			<# if (obj.inflowReceiptsCount == 0) { #>
				<div class="btn btn-small rfloat10" id="prepareAmendment">amend</div>
			<# } #>
			<div class="btn btn-small rfloat10" id="closePO">cancel</div>
			</uc:security>
		<# } #>
		<# if(obj.statusCode != 'CREATED') { #>
		<a href="/po/show?legacy=1&code=<#=obj.code#>" target="_blank">
			<div class="btn btn-small rfloat10">print</div>
		</a>
		<# } #>
		<div class="clear"></div>
		<div style="overflow-x:scroll;border-right:1px solid #ccc;border-left:1px solid #ccc;">
			<div id="dataTable" style="overflow: visible;"></div>
			<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
		</div>
		<div id="footerRows"></div>
		<div class="clear"></div>
		<br/>
		<# if(obj.statusCode == 'CREATED') { #>
			<# if (obj.isAmending) { #>
				<input type="submit" class="btn btn-small btn-primary  lfloat" id="createAmendment" value="Create Amendment & Send For Approval"></input>
				<div class="lfloat10 link" style="margin-top:5px;" id="cancelAmendment">cancel amendment</div>
				<div class="clear"/>
			<# } else if (obj.isCloning) { #>
				<input type="submit" class="btn btn-small btn-primary  lfloat" id="clonePO" value="Create & Send For Approval"></input>
				<div class="lfloat10 link" style="margin-top:5px;" id="cancelCloning">cancel cloning</div>
				<div class="clear"/>
			<# } else { #>
				<input type="submit" class="btn btn-success  lfloat20" id="savePO" value="Save"></input>
				<div class="hidden" id="actionsPOBtn">
					<input type="submit" class="btn btn-primary  lfloat20" id="cancel" value="Cancel Purchase Order"></input>
					<uc:security accessResource="PROCUREMENT_SEND_FOR_APPROVAL">
						<input type="submit" class="btn btn-primary  lfloat20" id="sendForApproval" value="Send For Approval"></input>
					</uc:security>
				</div>
			<# } #>
		<# } #>
		<# if(obj.statusCode == 'WAITING_FOR_APPROVAL') { #>
			<input type="submit" class="btn btn-primary  lfloat20" id="reject" value="Reject"></input>
		<# } #>
		<uc:security accessResource="PO_APPROVE">
		<# if(obj.statusCode == 'WAITING_FOR_APPROVAL') { #>
			<input type="submit" class="btn btn-primary  lfloat20" id="approve" value="Approve"></input>
		<# } #>
		</uc:security>
		<div class="clear"></div>
		<div id="purchaseOrderComments" class="round_all"></div>
		<div id="inflowReceipts" class="hidden">
		</div>
		
		</div>
	</script>
	<script id="poHistory" type="text/html">
		<table>
			<tr>
				<td>Field</td>
				<td>Old Status</td>
				<td>New Status</td>
				<td>Username</td>
				<td>Timestamp</td>
			</tr>
			<# for (var i=0;i<obj.historyItems;i++) { var history = obj.historyItems[i]; #>
			<tr>
				<td><#=history.field#></td>
				<td><#=history.oldStatus#></td>
				<td><#=history.newStatus#></td>
				<td><#=history.username#></td>
				<td><#=new Date(history.timestamp).toDateTime()#></td>
			</tr>
			<# } #>
		</table>
	</script>
	<script type="text/javascript">
		<uc:security accessResource="PRICE_EDIT_AT_PO">
			Uniware.Utils.editPrice = true;
		</uc:security>
		<uc:security accessResource="EDIT_PURCHASE_ORDER_METADATA">
			Uniware.Utils.editPOMetadata = true;
		</uc:security>
		
		Uniware.poItemsPage = function() {
			var self = this;
			this.aPosEditing;
			this.purchaseOrderCode = '${param['poCode']}';
			this.purchaseOrderData = null;
			this.poItems = null;
			this.oldPurchaseOrderData = null;
			this.poTotalPrice;
			this.poSubtotalPrice;
			this.poTotalTax;
			this.priceExclusive = ${configuration.getConfiguration('systemConfiguration').isVendorPricesTaxExclusive()};
			this.vendorCode;
			this.vendorName;
			this.facilityCode = '${cache.getCache('facilityCache').getCurrentFacility().getCode()}';
			this.init = function() {
				var req = {
					'purchaseOrderCode' : self.purchaseOrderCode
				}
				Uniware.Ajax.postJson("/data/po/fetch", JSON.stringify(req), function(response) {
					if (response.successful == true) {
						self.purchaseOrderData = response;
						self.poItems = self.purchaseOrderData.purchaseOrderItems;
						self.vendorCode = self.purchaseOrderData.vendorCode;
						self.vendorName = self.purchaseOrderData.vendorName;
						self.render();
					} else {
						Uniware.Utils.showError(response.errors[0].description);
					}
				});
			};
			
			this.render = function() {
				$('#purchaseOrderDiv').html(template("purchaseOrderTemplate", self.purchaseOrderData));
				self.renderPOItems();				
				Uniware.Utils.renderComments("#purchaseOrderComments", 'PO-' + (self.isCloning ? self.oldPurchaseOrderData.code : self.purchaseOrderData.code) + '-' + self.facilityCode);
				
				$(".datefield").datepicker({
					dateFormat : 'dd/mm/yy'
				});
				
				$(".poUpdateItem,#purchaseOrderType").click(function() {
					$('#poUpdateDiv').removeClass('hidden');
					$('#cancelUpdate').click(function() {
						self.render();
					});
					$('#updatePO').click(self.updatePOData);
				});
				$("div.email").click(self.sendEmail);
				//$("#purchaseOrderItemsMenu").click(self.showPurchaseOrderItems);
				//$("#inflowReceiptsMenu").click(self.getInflowReceipts);
				$('#closePO').click(self.closePO);
				$('#approve').click(self.approve);
				$('#cancel').click(self.cancelPO);
				$('#reject').click(self.reject);
				$('#sendForApproval').click(self.sendForApproval);
				$('#prepareAmendment').click(self.prepareAmendment);
				$('#cancelAmendment,#cancelCloning').click(self.cancelAmendment);
				$('#createAmendment').click(self.createAmendment);
				$('#clonePO').click(self.clonePO);
				$('#clonePOButton').click(function(){
					var button = $(this);
					var l = button.offset().left + button.outerWidth() - $("#clonePO-drop").outerWidth();
					var t = button.offset().top + button.outerHeight() - 40;
					$('#clonePO-drop').css({'top' : t + 'px', 'left' : l + 'px'}).toggle();
				});
				
				$('.clone').click(self.startCloningPO);
				$("#vendorName").autocomplete({
			    	minLength: 2,
			    	mustMatch : true,
					autoFocus: true,
					source: function( request, response ) {
						Uniware.Ajax.getJson("/data/lookup/vendors?name=" + request.term, function(data) {
							response( $.map( data, function( item ) {
								return {
									label: item.name,
									vendorCode : item.code
								}
							}));
						});
					},
					select: function( event, ui ) {
						self.vendorCode = ui.item.vendorCode;
						self.vendorName = ui.item.label;
					}
			    });
			};
			
			this.renderPOItems = function(){
				self.renderSheet();
				self.renderFooter();
			};
			
			this.renderSheet = function(){
				this.items;
				this.itemsMap;
				var tableSettings = {};
				tableSettings.columns = self.columns();
				tableSettings.minSpareRows = 0;
				tableSettings.rowHeaders = true;
				tableSettings.stretchH = 'all';
				if(self.purchaseOrderData.statusCode != 'CREATED' && self.purchaseOrderData.statusCode != 'WAITING_FOR_APPROVAL') {
					tableSettings.columns.splice(4,0,{
						data : "pendingQuantity",
						label:"Pending<br/>Quantity",
						type: 'numeric',
						format: '0,0'
					},{
						data : "rejectedQuantity",
						label:"Rejected<br/>Quantity",
						type: 'numeric',
						format: '0,0'
					});
				}
				
				if(self.purchaseOrderData.statusCode == 'CREATED') {
					tableSettings.minSpareRows = 1;
				}
				
				tableSettings.tableId = '#dataTable';
				tableSettings.data = self.poItems;
				tableSettings.cellCallBack = self.cellCallBack;
				self.loadTable(tableSettings);
			};
			
			this.renderFooter = function() {
				self.poTotalPrice = 0;
				self.poSubtotalPrice = 0;
				self.poTotalTax = 0;
				
				for(var i=0;i<self.poItems.length; i++){
					self.poTotalPrice += self.poItems[i].total;
					self.poSubtotalPrice += self.poItems[i].subtotal;
					self.poTotalTax += self.poItems[i].tax;
				}
				
				var footerSettings = {};
				var data = [["Subtotal",'',self.poSubtotalPrice],
				            ["Tax",'',self.poTotalTax],
				            ["Total",'',self.poTotalPrice],
				            ["Logistic Charges",self.purchaseOrderData.logisticChargesDivisionMethod,self.purchaseOrderData.logisticCharges],
                            ["Grand Total",'', self.poTotalPrice + self.purchaseOrderData.logisticCharges]];
				footerSettings.colHeaders = false;
				footerSettings.columns =  [{
					readOnly : true,
					style : {'fontWeight':'bold'},
					align: 'right'
				}, {
					style : {'fontWeight':'bold'},
					align: 'right'
				}, {
					align: 'right',
					type: 'numeric',
					format: '0,0.00'
				}];
				footerSettings.data = data;
				footerSettings.minSpareRows = 0;
				footerSettings.colWidths = ['70','15','15'];
				footerSettings.tableId = '#footerRows';
				footerSettings.cellCallBack = self.footerCallBack;
				footerSettings.stretchH = 'all';
				
				self.footerSheet = new Uniware.SpreadSheet(footerSettings);
				self.footerSheet.addChangeEvent(2, self.footerChange);
				self.footerSheet.init();
			};
			
			this.footerChange = function(row, col, oldValue, newValue) {
				if(oldValue == newValue) return;
				var logisticCharges = self.footerSheet.getDataAtCell(3,2);
				self.purchaseOrderData.logisticCharges = logisticCharges;
				
				var logisticChargesDivisionMethod = self.footerSheet.getDataAtCell(3,1);
				if(logisticChargesDivisionMethod == 'EQUALLY' || logisticChargesDivisionMethod == "QUANTITY" || logisticChargesDivisionMethod == "ROW_TOTAL"){
					self.updatePO(row, col, oldValue, newValue);
				}else{
					Uniware.Utils.showError("Please select a division method first");
				}
			};
			
			this.columns = function() {
				return [ {
					type : 'autocomplete',
					data : "itemTypeName",
					label : "Product Name",
					options : {items : 10, minLength : 3},
					source : self.autocompleteProducts,
					onSelect :  self.onProductSelect, 
					strict : false
				}, {
					data : "itemSKU",
					label: "Product Code",
					readOnly : true
				}, {
					data : "vendorSkuCode",
					label: "Vendor SKU",
					readOnly : true
				}, {
					data : "quantity",
					label: "Quantity",
					type: 'numeric'
				}, {
					data : "unitPrice",
					label:"Unit Price<br/>" + (self.priceExclusive ? '(Tax Exclusive)' : '(Tax Inclusive)'),
					readOnly: !Uniware.Utils.editPrice,
					type: 'numeric',
					format: '0,0.00'
				},  {
					data : "discount",
					label:"Discount<br/>" + (self.priceExclusive ? '(Tax Exclusive)' : '(Tax Inclusive)'),
					type: 'numeric',
					readOnly: true,
					format: '0,0.00'
				}, {
					data : "discountPercentage",
					label:"Discount %",
					type: 'numeric',
					format: '0,0.00'
				}, {
					data : "subtotal",
					label:"Subtotal",
					readOnly : true,
					type: 'numeric',
					format: '0,0.00'
				}, {
					type : 'autocomplete',
					data : "taxType",
					label : "Tax Class",
					options : {items : 10, minLength : 2},
					source : self.autocompleteTaxTypes,
					onSelect :  self.onTaxTypeSelect, 
					strict : false
				}, {
					data : "taxPercentage",
					label:"Tax %",
					readOnly : true,
					type: 'numeric',
					format: '0,0.00'
				}, {
					data : "tax",
					label:"Tax",
					readOnly : true,
					type: 'numeric',
					format: '0,0.00'
				},{
					data : "logisticCharges",
					label: "Logistic<br/>Charges",
					type: 'numeric',
					readOnly : true,
					format: '0,0.00'
				},{
					data : "total",
					label:"Total Price",
					readOnly : true,
					type: 'numeric',
					format: '0,0.00'
				} ];
			}
			
			this.autocompleteProducts = function(request, process) {
				self.items = [];
				self.itemsMap = {};
				Uniware.Ajax.getJson("/data/lookup/vendorItemTypes?vendorCode=" +self.vendorCode + "&keyword=" + request, function(data) {
					$.each(data, function(i, item) {
						self.itemsMap[item.name + "-" + item.itemSKU] = item;
						self.items.push(item.name + "-" + item.itemSKU);
					});
					process(self.items);
				});
			};
			
			this.onProductSelect = function(row, col, prop, val) {
				var itemSKU = self.itemsMap[val].itemSKU;
				var itemName = self.itemsMap[val].name;
				var vendorSkuCode = self.itemsMap[val].vendorSkuCode;
				var unitPrice = self.itemsMap[val].unitPrice;
				
				self.spreadSheet.setDataAtRowProp(row, 'itemTypeName', itemName);
				self.spreadSheet.setDataAtRowProp(row, 'itemSKU', itemSKU);
				self.spreadSheet.setDataAtRowProp(row, 'vendorSkuCode', vendorSkuCode);
				self.spreadSheet.setDataAtRowProp(row, 'unitPrice', unitPrice);
				self.spreadSheet.setDataAtRowProp(row, 'quantity', 0);
				self.spreadSheet.setDataAtRowProp(row, 'discount', 0);
				self.spreadSheet.setDataAtRowProp(row, 'discountPercentage', 0);
				
				var quantity = self.spreadSheet.getDataAtRowProp('quantity', row);
				self.spreadSheet.setDataAtRowProp(row, 'total', Math.multiply(unitPrice, quantity));
			};
			
			this.autocompleteTaxTypes = function(request, process) {
				self.items = [];
				self.itemsMap = {};
				Uniware.Ajax.getJson("/data/lookup/taxTypes?code=" + request, function(data) {
					$.each(data, function(i, item) {
						self.itemsMap[item.code] = item;
						self.items.push(item.code);
					});
					process(self.items);
				});
			};
			
			this.onTaxTypeSelect = function(row, col, prop, val) {
				var taxType = self.itemsMap[val].code;
				self.spreadSheet.setDataAtRowProp(row, 'taxType', taxType);
				var data = self.spreadSheet.getData();
				var poItem = data[row];
				if(poItem == null || poItem.itemSKU != null && poItem.quantity != null && poItem.unitPrice != null &&  poItem.discountPercentage != null){
					self.updatePO();
				}
			};
			
			this.onDivisionMethodSelect = function(row, col, prop, val) {
				self.footerSheet.setDataAtCell(row, col, val);
				if(self.purchaseOrderData.logisticChargesDivisionMethod != val){
					self.purchaseOrderData.logisticChargesDivisionMethod = val;
					self.updatePO();	
				}
			};
			
			this.loadTable = function(tableSettings) {
				self.spreadSheet = new Uniware.SpreadSheet(tableSettings);
				self.spreadSheet.addChangeEvent('unitPrice', self.change);
				self.spreadSheet.addChangeEvent('discount', self.change);
				self.spreadSheet.addChangeEvent('discountPercentage', self.change);
				self.spreadSheet.addChangeEvent('quantity', self.change);
				self.spreadSheet.addChangeEvent('logisticCharges', self.change);
				self.spreadSheet.init();
				$('#savePO').click(self.savePO);
			};
			
			this.cellCallBack = function(row, col, prop) {
				var poItem = self.poItems[row];
				var cellProperties = {};
				if(poItem != null && poItem.id != null && poItem.id != 0) {
					if(self.purchaseOrderData.statusCode != 'CREATED' || (prop != 'logisticCharges' && prop != 'quantity' && prop != 'unitPrice' && prop != 'discount' && prop != 'discountPercentage')){
						cellProperties.readOnly = true;
					}
				}
				return cellProperties;
			};
			
			this.footerCallBack = function(row, col, prop) {
				var cellProperties = {};
				if(self.purchaseOrderData.statusCode != 'CREATED' || (row != 3 && (col == 1 || col == 2))) {
					cellProperties.readOnly = true;
				} else if(row == 3 && col == 1){
					cellProperties.type = 'autocomplete';
					cellProperties.strict = true;
					cellProperties.options = {minLength : 0},
					cellProperties.source = ["EQUALLY","QUANTITY","ROW_TOTAL"];
					cellProperties.onSelect = self.onDivisionMethodSelect;
				}
				return cellProperties;
			};
			
			this.change = function(rowIndex, columnLabel, oldValue, newValue) {
				if(oldValue == newValue) return;

				var unitPrice = self.spreadSheet.getDataAtRowProp('unitPrice', rowIndex);
				var discount = self.spreadSheet.getDataAtRowProp('discount', rowIndex);
				var discountPercentage = self.spreadSheet.getDataAtRowProp('discountPercentage', rowIndex);
				
				if(columnLabel == 'discountPercentage') {
					value = Math.getPercentageAmount(unitPrice,discountPercentage);
					discount = isNaN(value) ? 0 : value;
				} else if(columnLabel == 'discount') {
					value = Math.getPercentageRate(unitPrice, discount);
					discountPercentage = isNaN(value) ? 0 : value;
				}
				if(discount > unitPrice){
					Uniware.Utils.showError("discount cannnot be greater than unit price");
					self.spreadSheet.setDataAtRowProp(rowIndex, columnLabel, oldValue);
					return;
				}
				self.spreadSheet.setDataAtRowProp(rowIndex, 'discount', discount);
				self.spreadSheet.setDataAtRowProp(rowIndex, 'discountPercentage', discountPercentage);
				
				var data = self.spreadSheet.getData();
				var poItem = data[rowIndex];
				if(poItem == null || poItem.itemSKU != null && poItem.quantity != null && poItem.unitPrice != null &&  poItem.discountPercentage != null){
					self.updatePO(rowIndex, columnLabel, oldValue, newValue);
				}
			};
			
			this.updatePO = function(rowIndex, columnLabel, oldValue, newValue) {
				var data = self.spreadSheet.getData();
				var req = {
						purchaseOrderCode : self.purchaseOrderData.code,
						logisticCharges : self.purchaseOrderData.logisticCharges,
						logisticChargesDivisionMethod : self.purchaseOrderData.logisticChargesDivisionMethod,
						purchaseOrderItems: []
				};
				
				if(self.purchaseOrderData.isCloning || self.purchaseOrderData.isAmending){
					req.vendorCode = self.vendorCode;
					req.purchaseOrderCode = null;
				}
					
				var poItems = [];
				for(var i=0; i<data.length; i++){
					if(!self.spreadSheet.isRowEmpty(i) && data[i].quantity > 0){
						var purchaseOrderItem = {};						
						purchaseOrderItem.itemSKU = data[i].itemSKU;
						purchaseOrderItem.quantity = data[i].quantity;
						purchaseOrderItem.unitPrice = data[i].unitPrice;
						purchaseOrderItem.discountPercentage = data[i].discountPercentage;
						purchaseOrderItem.taxTypeCode = data[i].taxType;
						poItems.push(purchaseOrderItem);	
					}
				}
					
				req.purchaseOrderItems = poItems;
				
				Uniware.Ajax.postJson("/data/po/update/item", JSON.stringify(req), function(response) {
					if (response.successful == true) {
						self.poItems = response.purchaseOrderItems;
						self.purchaseOrderData.logisticCharges = response.logisticCharges;
						self.purchaseOrderData.logisticChargesDivisionMethod = response.logisticChargesDivisionMethod;
						self.renderPOItems();
					} else {
						Uniware.Utils.showError(response.errors[0].description);
					}
				});	
			
			};
			
			this.savePO = function(){
				var data = self.spreadSheet.getData();
				var req = {
					purchaseOrderCode : self.purchaseOrderCode,
					logisticCharges : self.purchaseOrderData.logisticCharges,
					logisticChargesDivisionMethod : self.purchaseOrderData.logisticChargesDivisionMethod,
					purchaseOrderItems: []
				};
				
				var poItems = [];
				
				for(var i=0; i<data.length; i++){
					if(!self.spreadSheet.isRowEmpty(i)){
						var purchaseOrderItem = {};						
						purchaseOrderItem.itemSKU = data[i].itemSKU;
						purchaseOrderItem.quantity = data[i].quantity;
						purchaseOrderItem.unitPrice = data[i].unitPrice;
						purchaseOrderItem.discountPercentage = data[i].discountPercentage;
						purchaseOrderItem.taxTypeCode = data[i].taxType;
						poItems.push(purchaseOrderItem);	
					}
				}
					
				req.purchaseOrderItems = poItems;
				Uniware.Ajax.postJson("/data/po/create/item", JSON.stringify(req), function(response) {
					if (response.successful == true) {
						self.purchaseOrderData.logisticCharges = response.logisticCharges;
						self.purchaseOrderData.logisticChargesDivisionMethod = response.logisticChargesDivisionMethod;
						self.poItems = response.purchaseOrderItems;
						self.renderPOItems();
						Uniware.Utils.addNotification("Purchase Order has been saved");
					} else {
						Uniware.Utils.showError(response.errors[0].description);
					}
				},true);
				$('#actionsPOBtn').removeClass('hidden');
			};
			
			this.sendEmail = function() {
				var code = $(this).attr('id').split("_")[1];
				var req = {
					'purchaseOrderCode' : code
				};
				Uniware.Ajax.postJson("/data/po/send/email", JSON.stringify(req), function(response) {
					if (response.successful == true) {
						Uniware.Utils.addNotification("Purchase Order email has been sent successfully");
						self.init();
					} else {
						Uniware.Utils.showError(response.errors[0].description);
					}
				});
			};
			
			this.startCloningPO = function() {
				self.clonePOItems($(this).attr("id"));
			};
			
			this.clonePOItems = function(clonePOItemType) {
				self.oldPurchaseOrderData = $.extend(true, {}, self.purchaseOrderData);
				self.purchaseOrderData.statusCode = 'CREATED';
				var poItems = self.poItems;
				for (i=0; i<poItems.length; i++){
					if (clonePOItemType == 'pending' && poItems[i].pendingQuantity > 0) {
						poItems[i].quantity = poItems[i].pendingQuantity;
						poItems[i].pendingQuantity = 0;
					} else if(clonePOItemType == 'rejected' && poItems[i].rejectedQuantity > 0) {
						poItems[i].quantity = poItems[i].rejectedQuantity;
						poItems[i].rejectedQuantity = 0;
					} else if (clonePOItemType == 'allPOItems') {
					} else {
						self.poItems.remove(poItems[i]);
						i--;
					}
				}
				self.purchaseOrderData.isCloning = true;
				self.purchaseOrderData.id = null;
				self.purchaseOrderData.code = null;
				self.purchaseOrderData.inflowReceiptsCount = 0;
				self.isCloning = true;
				self.render();
			};
			
			this.clonePO = function() {
				var action = confirm('Are you sure?');
				if (action == false)
					return;
				if ($("#vendorName").val() != self.vendorName) {
					self.vendorCode = "";
				}
				var req = {
					'purchaseOrderCode' : self.purchaseOrderCode,
					'logisticCharges' : self.purchaseOrderData.logisticCharges,
					'logisticChargesDivisionMethod' : self.purchaseOrderData.logisticChargesDivisionMethod,
					'purchaseOrderItems' : [],
					'purchaseOrderMetadata' : {},
					'vendorCode' : self.vendorCode
				};
				if ($("#expiryDate").val() != '') {
					req.purchaseOrderMetadata.expiryDate = Date.fromPaddedDate($("#expiryDate").val()).getTime();
				}
				if ($("#deliveryDate").val() != '') {
					req.purchaseOrderMetadata.deliveryDate = Date.fromPaddedDate($("#deliveryDate").val()).getTime();
				}
				Uniware.Utils.addCustomFieldsToRequest(req.purchaseOrderMetadata, $('.custom'));
				for (var i=0;i<self.poItems.length;i++) {
					var poItem = self.poItems[i];
					if (poItem.quantity) {
						req.purchaseOrderItems.push({
							'itemSKU' : poItem.itemSKU,
							'quantity' : poItem.quantity,
							'unitPrice' : poItem.unitPrice,
							'discountPercentage' : poItem.discountPercentage
						});
					}
				}
				Uniware.Ajax.postJson('/data/po/clone', JSON.stringify(req), function(response) {
					if (response.successful) {
						self.purchaseOrderCode = response.purchaseOrderCode;
						self.init();
						Uniware.Utils.addNotification('New Purchase order have been created successfully.');
					} else {
						Uniware.Utils.showError(response.errors[0].description);
					}
				}, true);
			};
			
			this.createAmendment = function() {
				var action = confirm('Are you sure?');
				if (action == false)
					return;
				var req = {
					'purchaseOrderCode' : self.purchaseOrderCode,
					'logisticCharges' : self.purchaseOrderData.logisticCharges,
					'logisticChargesDivisionMethod' : self.purchaseOrderData.logisticChargesDivisionMethod,
					'purchaseOrderItems' : []
				};
				for (var i=0;i<self.poItems.length;i++) {
					var poItem = self.poItems[i];
					if (poItem.quantity) {
						req.purchaseOrderItems.push({
							'itemSKU' : poItem.itemSKU,
							'quantity' : poItem.quantity,
							'unitPrice' : poItem.unitPrice,
							'discountPercentage' : poItem.discountPercentage
						});
					}
				}
				Uniware.Ajax.postJson('/data/po/amend', JSON.stringify(req), function(response) {
					if (response.successful) {
						self.purchaseOrderCode = response.purchaseOrderCode;
						self.init();
						Uniware.Utils.addNotification('Purchase order have been amended successfully.');
					} else {
						Uniware.Utils.showError(response.errors[0].description);
					}
				}, true);
			};
			
			this.cancelAmendment = function() {
				self.purchaseOrderData = $.extend(true, {}, self.oldPurchaseOrderData);
				self.poItems = self.purchaseOrderData.purchaseOrderItems;
				self.isAmending = null;
				self.render();
			};
			
			this.prepareAmendment = function() {
				self.oldPurchaseOrderData = $.extend(true, {}, self.purchaseOrderData);
				self.isAmending = true;
				self.purchaseOrderData.isAmending = true;
				self.purchaseOrderData.statusCode = 'CREATED';
				self.render();
			};
			
			this.updatePOData = function() {
				var req = {
					'purchaseOrderCode' : self.purchaseOrderCode,
					'purchaseOrderMetadata' : {}
				}
				if ($("#expiryDate").length > 0 && $("#expiryDate").val() != '') {
					req.purchaseOrderMetadata.expiryDate = Date.fromPaddedDate($("#expiryDate").val()).getTime();
				}
				if ($("#deliveryDate").length > 0 && $("#deliveryDate").val() != '') {
					req.purchaseOrderMetadata.deliveryDate = Date.fromPaddedDate($("#deliveryDate").val()).getTime();
				}
				req.purchaseOrderMetadata.type = self.purchaseOrderData.type;
				if ($('#purchaseOrderType').length > 0) {
					req.purchaseOrderMetadata.type = $('#purchaseOrderType').val();
				}
				req.purchaseOrderMetadata.customFieldValues = [];
				Uniware.Utils.addCustomFieldsToRequest(req.purchaseOrderMetadata, $('.custom'));
				Uniware.Ajax.postJson('/data/po/update/metadata', JSON.stringify(req), function(response) {
					if (response.successful) {
						self.init();
						Uniware.Utils.addNotification('Purchase order details have been updated successfully.');
					} else {
						Uniware.Utils.showError(response.errors[0].description);
					}
				}, true);
			};

			this.remove = function(event) {
				var tr = $(event.target).parents('tr');
				var index = tr[0].id.split("-")[1];
				var poItem = self.poItems[index];
				self.poItems.remove(poItem);
				self.render();
				$("#addPOItem").trigger('click');
			};
			
			this.cancelPO = function() {
				var req = {
					purchaseOrderCode : self.purchaseOrderCode,
				}
				Uniware.Ajax.postJson("/data/po/cancel", JSON.stringify(req), function(response) {
					if (response.successful == true) {
						Uniware.Utils.addNotification("Purchase Order has been Cancelled");
						self.init();
					} else {
						Uniware.Utils.showError(response.errors[0].description);
					}
				});
			};

			this.reject = function() {
				var req = {
					purchaseOrderCode : self.purchaseOrderCode,
				}
				Uniware.Ajax.postJson("/data/po/reject", JSON.stringify(req), function(response) {
					if (response.successful == true) {
						Uniware.Utils.addNotification("Purchase Order has been Rejected");
						self.init();
					} else {
						Uniware.Utils.showError(response.errors[0].description);
					}
				});
			};

			this.sendForApproval = function() {
				var req = {
					purchaseOrderCode : self.purchaseOrderCode,
				}
				Uniware.Ajax.postJson("/data/po/sendForApproval", JSON.stringify(req), function(response) {
					if (response.successful == true) {
						Uniware.Utils.addNotification("Purchase Order has been sent for Approval");
						self.init();
					} else {
						Uniware.Utils.showError(response.errors[0].description);
					}
				});
			};

			this.approve = function() {
				var req = {
					purchaseOrderCode : self.purchaseOrderCode,
				}
				Uniware.Ajax.postJson("/data/po/approve", JSON.stringify(req), function(response) {
					if (response.successful == true) {
						Uniware.Utils.addNotification("Purchase Order has been approved");
						self.init();
					} else {
						Uniware.Utils.showError(response.errors[0].description);
					}
				}, true);
			};

			this.closePO = function() {
				if (!confirm("Do you really want to cancel this purchase order?")) {
					return false;
				} else {
					var req = {
						purchaseOrderCode : self.purchaseOrderCode,
					}
					Uniware.Ajax.postJson("/data/po/close", JSON.stringify(req), function(response) {
						if (response.successful == true) {
							Uniware.Utils.addNotification("Purchase Order has been cancelled");
							self.init();
						} else {
							Uniware.Utils.showError(response.errors[0].description);
						}
					}, true);
				}
			};
		};

		$(document).ready(function() {
			window.page = new Uniware.poItemsPage();
			window.page.init();
		});
	</script>
	</tiles:putAttribute>
</tiles:insertDefinition>
