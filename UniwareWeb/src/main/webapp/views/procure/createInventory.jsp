<%@ include file="/tagIncludes.jsp"%>
<tiles:insertDefinition name=".procurePage">
	<tiles:putAttribute name="title" value="Uniware - Create Inventory" />
	<tiles:putAttribute name="rightPane">
		<div class="greybor headlable ovrhid main-box-head">
			<h2 class="edithead head-textfields">Create Inventory</h2>
		</div>

		<div class="formLabel greybor round_bottom main-boform-cont pad-15-top ovrhid">
			<div class="formLeft lfloat">Item Type</div>
			<div class="formRight lfloat">
				<input type="text" id="itemType">
				<div style="font-style: italic; font-size: smaller;" id="goodInventoryCount"></div>
			</div>
			<div id="createInventoryDiv"></div>
		</div>
		

	</tiles:putAttribute>
	<tiles:putAttribute name="deferredScript">
		<script id="createInventoryTemplate" type="text/html">
		<div class="clear"></div>
		<div class="lfloat" style="width:49%; margin-right: 2%;">
			<div class="clear"></div>
			<div class="formLeft lfloat">Category</div>
			<div class="formRight lfloat f15">
				<#=obj.category#>
			</div>

			<div class="clear"></div>
			<div class="formLeft lfloat">Item Type Name</div>
			<div class="formRight lfloat f15">
				<#=obj.itemTypeName#>
			</div>

			<div class="clear"></div>
			<div class="formLeft lfloat">Product Code</div>
			<div class="formRight lfloat f15">
				<#=obj.itemSKU#> 
				<#=Uniware.Utils.getItemDetail(obj.itemTypeImageUrl, obj.itemTypePageUrl)#>
			</div>

			<div class="clear"></div>
			<div class="formLeft lfloat">Vendor</div>
			<div class="formRight lfloat">
				<select id="vendorCode">
					<# for(var i=0; i<obj.vendorItemTypes.length;i++){ var vendorItemType = obj.vendorItemTypes[i]; #>
						<option value="<#=vendorItemType.vendorCode#>"><#=vendorItemType.vendorSkuCode #> # <#=vendorItemType.vendorName#></option>
					<# } #>
				</select>
				<#if(obj.vendorItemTypes.length == 0){#>
					<div style="color:red;font-size:12px;margin-top:8px;">
					<c:out value="No Vendor is available for this Item Type"/> 
					</div>
				<#}#>
				</div>
			
			<div class="clear"></div>
			<div class="formLeft lfloat">Shelf</div>
			<div class="formRight lfloat">
				<input type="text" id="shelfCode">
			</div>

			<div class="clear"></div>
			<div class="formLeft lfloat">Quantity</div>
			<div class="formRight lfloat">
				<input type="text" id="quantity">
			</div>

			<#if(obj.expirable) {#>
				<div class="clear"></div>
				<div class="formLeft lfloat">Manufacturing Date</div>
				<div class="formRight lfloat">
					<input type="date" id="manufacturingDate">
				</div>
			<#}#>

			<div class="clear"></div>
			<br/>
			<div class="formLeft lfloat">&#160;</div>
			<div class="formRight lfloat">
				<input type="submit" class="btn btn-primary" id="createItems" value="Create Items"/>
			</div>
			<div class="clear"></div>
			<br/>
			<br/>
			<#if(obj.shelfToItemTypeCount && obj.itemTypeCount >0){#>
			<div class="clear"></div>
			<div class="formLeft lfloat">Existing Shelfs</div>
			<div class="formRight lfloat">
				<table border="1">
					<tr>
					<th>Shelf</th>
					<th>Quantity</th>
					</tr>
				<#for(var shelf in obj.shelfToItemTypeCount){#>
					<tr>
						<td><#=shelf#></td>
						<td><#=obj.shelfToItemTypeCount[shelf]#></td>
					</tr>
				<#}#>
				</table>
			</div>			
			<#}#>
		</div>
		<div class="lfloat">
			<#if(obj.inTransitionCount >0) {#>
				<div class="clear"></div>
				<div class="formLeft lfloat">Items In Transition</div>
				<div class="formRight lfloat f15">
					<#=obj.inTransitionCount#>
				</div>
				<div class="btn btn-small btn-success lfloat10" style="margin-top: 4px;" id="addToInventory">Add To Inventory</div>
				<div class="clear"></div>
				<div class="formLeft bold">Discard Item Code</div>
				<div class="clear"></div>
				<div class="formLeft lfloat">Scan Item Code</div>
				<div class="formRight lfloat20">
					<input type="text" id="discardItem">
				</div>
				<div class="clear"></div>
				<div class="formLeft lfloat"/>
				<div class="formRight">
					<div class="italic bold lfloat">OR</div>
					<div class="btn btn-small btn-danger lfloat20" style="margin-top: 4px;" id="discardAll">Discard All</div>
				</div>
			<#}#>
		</div>
		</script>
		<script type="text/javascript">
			Uniware.createInventoryPage = function() {
				var self = this;
				this.itemTypeSkuCode;
				
				this.init = function() {
					self.searchItemType();
				};
				
				this.searchItemType = function(){
				    $("#itemType").autocomplete({
				    	minLength: 4,	
				    	mustMatch : true,
						autoFocus: true,
						source: function( request, response ) {
							$('#createInventoryDiv').html("");
							Uniware.Ajax.getJson("/data/lookup/itemTypes?keyword=" + request.term, function(data) {
								response( $.map( data, function( item ) {
									return {
										label: item.name + " - " + item.itemSKU,
										value: item.name,
										itemType: item
									}
								}));
							});
						},
						select: function( event, ui ) {
							self.itemTypeSkuCode = ui.item.itemType.itemSKU;
							self.itemTypeId = ui.item.itemType.itemTypeId;
							self.expirable = ui.item.itemType.manufacturingDate;
							self.getItemTypeDetails(self.itemTypeId, event);
						}
				    });
				};
				
				this.getItemTypeDetails = function(itemTypeId, event){
					Uniware.Ajax.getJson("/data/admin/system/inventory/get/details/itemType?itemTypeId=" + encodeURIComponent(itemTypeId), function(data) {
						if(data.goodInventoryCount > 0){
							$('#goodInventoryCount').html("Good Inventory Count: " + data.goodInventoryCount);							
						}else{
							$('#goodInventoryCount').html("");
						}
						$('#createInventoryDiv').html(template("createInventoryTemplate", data));
						Uniware.Utils.applyHover();
						$('#createItems').click(self.createItems);
						
						if(data.inTransitionCount >0){
							$('#addToInventory').click(self.addToInventory);
							$('#discardItem').keyup(self.discardItem);
							if (!event) {
								$('#discardItem').focus();
							}
							
							$('#discardAll').click(self.discardAllItems);
						}
						
						$('#shelfCode').keyup(function(event){
							if (event.which == 13) {
								$('#quantity').focus();
							}
						});
					});
				};
				
				this.discardItem = function(event){
					if (event.which == 13) {
						var requestObj = {
								itemCode: $(this).val(),
								itemSKU: self.itemTypeSkuCode
						}
						Uniware.Ajax.postJson("/data/admin/system/itemCode/discard", JSON.stringify(requestObj), function(response){
							if(response.successful){
								Uniware.Utils.addNotification("Item has been discarded");
								self.getItemTypeDetails(self.itemTypeId);
							}else{
								Uniware.Utils.showError(response.errors[0].description + ": " +$('#discardItem').val());
								$('#discardItem').val('');
							}
						}, true);
					}
				};
				
				this.discardAllItems = function(){
					if(confirm("Are your sure?")){
						var requestObj = {
								itemSKU: self.itemTypeSkuCode
						}
						Uniware.Ajax.postJson("/data/admin/system/itemCodes/discardAll", JSON.stringify(requestObj), function(response){
							if(response.successful){
								Uniware.Utils.addNotification("All Items in transition have been discarded");
								self.getItemTypeDetails(self.itemTypeId);
							}else{
								Uniware.Utils.showError(response.errors[0].description);
							}
						}, true);	
					}
				};
				
				this.createItems = function(){
					if(self.expirable) {
						var req = {
							itemTypeSkuCode: self.itemTypeSkuCode,
							vendorCode: $('#vendorCode').val(),
							shelfCode: $('#shelfCode').val(),
							quantity: $('#quantity').val(),
							manufacturingDate: $('#manufacturingDate').val()
						};
					}else{
						var req = {
							itemTypeSkuCode: self.itemTypeSkuCode,
							vendorCode: $('#vendorCode').val(),
							shelfCode: $('#shelfCode').val(),
							quantity: $('#quantity').val()
						};
					}

					
					Uniware.Ajax.postJson("/data/admin/system/items/create", JSON.stringify(req), function(response) {
						if(response.successful){
							self.getItemTypeDetails(self.itemTypeId);
							Uniware.Utils.addNotification("Items have been created");
							Uniware.Utils.printIFrame('/admin/system/items/print/'+ response.itemCodes);
						}else{
							Uniware.Utils.showError(response.errors[0].description);
						}
					}, true);
				};
				
				
				this.addToInventory = function(){
					var req = {
						itemTypeSkuCode: self.itemTypeSkuCode,
					};
					Uniware.Ajax.postJson("/data/admin/system/inventory/create", JSON.stringify(req), function(response) {
						if(response.successful){
							self.getItemTypeDetails(self.itemTypeId);
							Uniware.Utils.addNotification("Items have been added to the inventory");
						}else{
							Uniware.Utils.showError(response.errors[0].description);
						}
					}, true);
				};
				
			}
				
			$(document).ready(function() {
				window.page = new Uniware.createInventoryPage();
				window.page.init();
			});
		</script>
	</tiles:putAttribute>
</tiles:insertDefinition>
