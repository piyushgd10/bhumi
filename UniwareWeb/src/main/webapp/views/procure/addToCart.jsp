<%@ include file="/tagIncludes.jsp"%>
<tiles:insertDefinition name=".procurePage">
	<tiles:putAttribute name="title" value="Uniware - Add Items to Cart" />
	<tiles:putAttribute name="rightPane">
		<div>
			<form onsubmit="javascript : return false;">
				<div class="greybor headlable ovrhid main-box-head">
					<h2 class="edithead head-textfields">Search Items</h2>
				</div>
				<div class="round_bottom ovrhid pad-15">
					<div class="lfloat20">
						<div class="searchLabel">Vendor Name</div>
						<input type="text" id="vendorName" size="20" autocomplete="off" />
					</div>
					<div class="lfloat20">
						<div class="searchLabel">Item Type Name/Code Contains</div>
						<input type="text" id="itemTypeName" size="30" autocomplete="off" />
					</div>
					<div class="lfloat20">
						<div class="searchLabel">Category</div>
						<select id="category" style="width:150px;">
						<option value="">--ALL--</option>
						<c:forEach items="${cache.getCache('categoryCache').categories}" var="category">
							<option value="${category.code }">${category.name}</option>
						</c:forEach>
						</select>
					</div>
					
					<div class="lfloat20" style="margin-top:20px;">	
						<input id="search" value="search" type="submit" class="btn btn-primary" />
					</div>
				<div id="searching" class="lfloat10 hidden" style="margin-top:25px;">
					<img src="/img/icons/refresh-animated.gif"/>
				</div>
				</div>
			</form>
		</div>
		<form onsubmit="javascript: return false;">
			<table id="dataTable" class="dataTable"></table>
		</form>
	</tiles:putAttribute>
	<tiles:putAttribute name="deferredScript">
	<script type="text/javascript" src="${path.js('jquery/jquery.dataTables.min.js')}"></script>
	<script id="itemTypeRow" type="text/html">
		<td>
			<#=obj.name#>
			<#=Uniware.Utils.getItemDetail(obj.itemTypeImageUrl, obj.itemTypePageUrl)#>
		</td>
		<td class="sellerSkuCode ovrhid" title="<#=obj.skuCode#>"><#=obj.skuCode#></td>
		<# if (obj.vendorItemTypes.length == 0) { #>
		<td colspan="6">
			<em class="f12">No Vendor for this item type</em>
		</td>
		<# } else { #>
		<# var selectedVendorIndex = 0; #>
		<td>
			<select class="vendor field-width">
				<# for(var i=0; i<obj.vendorItemTypes.length; i++) { var vendorItemType = obj.vendorItemTypes[i]; #>
					<# if(vendorItemType.vendorId == obj.vendor) { selectedVendorIndex = i; #>
						<option value="<#=i#>" selected="selected"><#=vendorItemType.vendorName#> (<#=vendorItemType.unitPrice#>)</option>
					<# } else { #>
						<option value="<#=i#>"><#=vendorItemType.vendorName#> (<#=vendorItemType.unitPrice#>)</option>
					<# } #>
					
				<# } #>
			</select>
		</td>
		<td class="vendorSkuCode ovrhid" title="<#=obj.vendorItemTypes[selectedVendorIndex].vendorSkuCode#>"><#=obj.vendorItemTypes[selectedVendorIndex].vendorSkuCode#></td>
		<td align="center" class="<#=obj.addedQuantity > 0 ? 'bold' : ''#>"><#=obj.addedQuantity#></td>
		<td><input type="text" class="quantity" value="<#=obj.quantity#>" style="width:70%;"></input></td>
		
		<td><input type="text" class="unitPrice"  style="width:70%;" value="<#=obj.vendorItemTypes[selectedVendorIndex].unitPrice#>"></input></td>
		<td class="section-operations">
			<div type="submit" class="btn btn-small lfloat20 action">Add To Cart</div>
		</td>
		<# } #>
	</script>	
	<script type="text/javascript">
		Uniware.AddToCartPage = function() {
			
			var self = this;
			this.table = null;
			this.cartQuantity = {};
			this.itemTypes = null;
			this.vendor =null;
			
			this.init = function() {
				self.listVendors();
				self.getCart();
			};
			
			this.listVendors = function(){
			    $("#vendorName").autocomplete({
			    	minLength: 2,
			    	mustMatch : true,
					autoFocus: true,
					source: function( request, response ) {
						Uniware.Ajax.getJson("/data/lookup/vendors?name=" + encodeURIComponent(request.term), function(data) {
							response( $.map( data, function( item ) {
								return {
									label: item.name + "-" + item.code,
									value: item.name
								}
							}));
						});
					}
			    });
			};
			
			this.getCart = function(){
				Uniware.Ajax.getJson("/data/procure/getCart", function(response) {
					self.purchaseCart = response;
					for (var vendorId in response.vendorToPurchaseCartItems) {
						var vendorPO = response.vendorToPurchaseCartItems[vendorId];
						for (var i=0;i<vendorPO.purchaseCartItems.length;i++) {
							var poItem = vendorPO.purchaseCartItems[i];
							self.cartQuantity[poItem.itemSku] = self.cartQuantity[poItem.itemSku] ? self.cartQuantity[poItem.itemSku] + poItem.quantity : poItem.quantity;
						}
					}
					$('#search').click(self.search);
				});
			};
							
			this.search = function() {
				//verify vendor
				var vendorName = $("#vendorName").val();
				if(vendorName != ""){
					Uniware.Ajax.getJson("/data/lookup/vendors?name=" + encodeURIComponent(vendorName), function(data) {
						if(data.length > 0){
							if(vendorName.toLowerCase() == data[0].name.toLowerCase()){
								self.vendor = data[0].id;
								self.searchItemTypes();
							}else{
								Uniware.Utils.showError("Please select a valid vendor");
							}
						}else{
							Uniware.Utils.showError("Please select a valid vendor");
						}
					});	
				}else{
					self.vendor = null;
					self.searchItemTypes();
				}
			};
			
			this.searchItemTypes = function(){
				$("#searching").removeClass('hidden');
				var req={
						vendorId: self.vendor,
						itemTypeName: $('#itemTypeName').val(),
						categoryCode: $('#category').val()					
				}
				Uniware.Ajax.postJson("/data/procure/itemType/lookup", JSON.stringify(req), function(response) {
					if(response.successful == true){
							self.itemTypes = response.itemTypes;
							var dtEL = $('#dataTable');
							if (Uniware.Utils.isDataTable(dtEL)) {
								var dtTable = dtEL.dataTable();
								dtTable.fnClearTable();
								dtTable.fnAddData(response.itemTypes);
								
							} else {
								self.table = dtEL.dataTable({
									"sPaginationType": "full_numbers",
									"bFilter": false,
									"aaData" : response.itemTypes,
									"aoColumns" : [ {
										"sTitle" : "Item Name",
										"mDataProp" : "name"
									}, {
										"sTitle" : "Product Code",
										"mDataProp" : "skuCode"
									},{ 
										"sTitle" : "Vendor",
										"mDataProp": function (aData) {
											return '';
										},
										"sWidth": "12%"
									},{ 
										"sTitle" : "Vendor SKU",
										"mDataProp": function (aData) {
											return '';
										},
										"sWidth": "10%"
									}, {
										"sTitle" : "Quantity in Cart",
										"mDataProp": function (aData) {
											return '';
										},
										"sWidth": "5%"
									},{
										"sTitle" : "Quantity to Add",
										"mDataProp": function (aData) {
											return '';
										},
										"sWidth": "5%"
									},{
										"sTitle" : "Unit Price",
										"mDataProp": function (aData) {
											return '';
										},
										"sWidth": "7%"
									}, {
										"sTitle" : "Operations",
										"mDataProp": function (aData) {
											return '';
										},
										"sWidth": "10%"
									},],
									"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
										aData.vendor = self.vendor;
										aData.addedQuantity = self.cartQuantity[aData.skuCode];
										if(aData.addedQuantity == null){
											aData.addedQuantity = 0;
										}
										$(nRow).html(template("itemTypeRow", aData));
										return nRow;
									}
								});
																
								$("#dataTable").on('click', 'div.action', function(event) {
									self.addToCart(event);
								});
								
								$("#dataTable").on('change', '.vendor', function(event) {
									self.changeVendor(event);
								});
								Uniware.Utils.applyHover("#dataTable");
							}
							if(response.totalRecords > 500){
								Uniware.Utils.addNotification("Displaying 500 of " + response.totalRecords +  " records. Filter further to narrow down your search");	
							}
					}
					$("#searching").addClass('hidden');
				});
			};
			
			this.changeVendor = function(event){
				var aPos = self.table.fnGetPosition($(event.target).parent().get(0));
				var aData = self.table.fnGetData(aPos[0]);
				var index = $(event.target).val();
				var tr = $(event.target).parents('tr');
				$(tr[0]).find('.unitPrice').val(aData.vendorItemTypes[index].unitPrice);
				$(tr[0]).find('.vendorSkuCode').html(aData.vendorItemTypes[index].vendorSkuCode);
				$(tr[0]).find('.vendorSkuCode').attr('title' , aData.vendorItemTypes[index].vendorSkuCode);
			};
			
			this.addToCart = function(event) {
				var aPos = self.table.fnGetPosition($(event.target).parent().get(0));
				var aData = self.table.fnGetData(aPos[0]);
				var tr = $(event.target).parents('tr');
				var index = $(tr[0]).find('.vendor').val();
				var quantity = $(tr[0]).find('.quantity').val();
				var unitPrice = $(tr[0]).find('.unitPrice').val();
				
				var requestObject = {
					purchaseCartItems : [{
						'quantity' : quantity,
						'unitPrice' : unitPrice,
						'vendorCode' : aData.vendorItemTypes[index].vendorCode,
						'itemSku': aData.skuCode
					}]
				};
				
				Uniware.Ajax.postJson("/data/procure/itemType/addToCart", JSON.stringify(requestObject), function(response) {
					if (response.successful == false) {
						Uniware.Utils.showError(response.errors[0].description);
					} else {
						self.cartQuantity[aData.skuCode] = parseInt(aData.addedQuantity) + parseInt(quantity);
						self.table.fnUpdate(aData, aPos[0]);
						Uniware.Utils.addNotification("Item has been added to cart");
					}
				});
			};
		};
						
		$(document).ready(function() {
			window.page = new Uniware.AddToCartPage();
			window.page.init();
		});
	</script>
	</tiles:putAttribute>
</tiles:insertDefinition>
