<%@ include file="/tagIncludes.jsp"%>
<tiles:insertDefinition name=".procurePage">
	<tiles:putAttribute name="title" value="Uniware - Vendors" />
	<tiles:putAttribute name="rightPane">
		<div id="pageBar">
			<div class="pageHeading" id="vendorCreateLink"><span class="mainHeading">Vendors<span class="pipe">/</span></span><span class="subHeading"></span></div>
			<div class="pageControls" id="pageViews"></div>
			<uc:security accessResource="VENDOR_CREATE">
				<div id="createVendor" class="btn btn-small btn-primary rfloat" style="margin: 10px 5px;">add vendor</div>
			</uc:security>
			<div class="clear"></div> 
		</div>
		<div id="tableContainer">
			<table id="flexme1" style="display: none"></table>
		</div>
		<div id="createVendorDiv"></div>
		<div id="addressDiv" class="lb-over">
			<div class="lb-over-inner round_all">
				<div style="margin: 40px;line-height: 30px;">
					<div class="pageHeading lfloat">Address</div>
					<div id="addressDiv_close" class="link rfloat">close</div>
					<div class="clear"></div>
					<div id="addressTemplateDiv" style="margin:20px 0;">
					</div>
				</div>
			</div>
		</div>
		<div id="contactDiv" class="lb-over">
			<div class="lb-over-inner round_all">
				<div style="margin: 40px;line-height: 30px;">
					<div class="pageHeading lfloat">Contact</div>
					<div id="contactDiv_close" class="link rfloat">close</div>
					<div class="clear"></div>
					<div id="contactTemplateDiv" style="margin:20px 0;">
					</div>
				</div>
			</div>
		</div>
		<div id="agreementDiv" class="lb-over">
			<div class="lb-over-inner round_all_b" style="width:605px">
				<div class="lb-over-inner-head ovrhid round_top_b">
					<div class="pageHeading lfloat">Agreement</div>
					<div id="agreementDiv_close" class="link rfloat">close</div>
				</div>
				<div class="clear"></div>
				<div id="agreementTemplateDiv" class="lb-over-innder-cont"></div>
			</div>
		</div>
		<div id="vendorConfigured" style="margin:10px;" class="hidden">
			<div class="lfloat"><img src="/img/icons/hold.png" style="width:18px;"/></div>
			<div class="alert lfloat10">Please configure Vendor details.<br/>Billing & Shipping Addresses are mandatory.</div>
			<div class="clear"></div>
		</div>
		
		<c:set var="states" value="${cache.getCache('locationCache').stateJson}"></c:set>
		<div id="vendorDetails" class="detailsbox lfloat vendorDetails hidden" style="width:49%;margin-right:2%;">
			<script type="text/html" id="vendorTemplate">
			<div class="greybor headlable round_top ovrhid"><h4 class="edithead">Vendor Details</h4></div>
			<div class="greybor formLabel vendorDetailEdit round_bottom">
                <table width="100%" border="0" cellpadding="2" cellspacing="0">
                  <tr>
                    <td width="200">Name</td>
                    <td><input id="vendorName" type="text" class="table-editable view field-width" value="<#=obj.detail.name#>"></td>
                  </tr>
                  <uc:security accessResource="VENDOR_LOGIN_MANAGEMENT">
				    <tr>
                        <td>Uniware Vendor Login</td>
                        <td>
						  <# if (obj.signupLink) { #>
							 <# var url= obj.signupLink + '&legacy=1'; #>
							 <a href="<#=url#>" target="_top">CREATE LOGIN</a>
						  <# } else { #>
						  <uc:security accessResource="ADMIN_USER">
							 <a href="/admin/system/users?legacy=1&username=<#=obj.signedUsername#>">
						  </uc:security>
							 <#=obj.signedUsername#>
						  <uc:security accessResource="ADMIN_USER">
							 </a>
						  </uc:security>
						  <# } #>
					   </td>
                    </tr>
                  </uc:security>
				  <tr>
                    <td>Code</td>
                    <td><span id="vendorCode" style="margin-left: 6px;"><#=obj.detail.code#></span></td>
                  </tr>
				  <tr>
                    <td>Enabled</td>
                    <td>
						<div class="lfloat" style="width:15px;">
							<input id="enabled" type="checkbox" class="view field-width" <#=obj.enabled ? checked="checked" : '' #>></input>
						</div>
					</td>
                  </tr>
                  <tr>
                    <td>Website</td>
                    <td><input id="website" type="text" class="table-editable view field-width" value="<#=obj.detail.website#>"></td>
                  </tr>
                  <tr>
                    <td>PAN</td>
                    <td><input id="pan" type="text" class="table-editable view field-width" value="<#=obj.detail.pan#>"></td>
                  </tr>
                  <tr>
                    <td>TIN</td>
                    <td><input id="tin" type="text" class="table-editable view field-width" value="<#=obj.detail.tin#>"></td>
                  </tr>
                  <tr>
                    <td>Central Sales Tax</td>
                    <td><input id="cstNumber" type="text" class="table-editable view field-width" value="<#=obj.detail.cstNumber#>"></td>
                  </tr>
                  <tr>
                    <td>Service Tax</td>
                    <td><input id="stNumber" type="text" class="table-editable view field-width" value="<#=obj.detail.stNumber#>"></td>
                  </tr>
				  <tr>
                    <td>Purchase Expiry Period</td>
                    <td><input id="purchaseExpiryPeriod" type="text" class="table-editable view field-width" value="<#=obj.purchaseExpiryPeriod#>"></td>
                  </tr>
				  <tr>
                    <td>Accepts C-Form</td>
                    <td>
						<div class="lfloat" style="width:15px;">
						<input id="acceptsCForm" type="checkbox" class="view field-width" <#=obj.acceptsCForm ? checked="checked" : '' #>></input>
						</div>
					</td>
                  </tr>
				  <tr>
                    <td>Tax Exempted</td>
                    <td>
						<div class="lfloat" style="width:15px;">
						<input id="taxExempted" type="checkbox" class="view field-width" <#=obj.taxExempted ? checked="checked" : '' #>></input>
						</div>
					</td>
                  </tr>
				  <tr>
                    <td>Registered Dealer</td>
                    <td>
						<div class="lfloat" style="width:15px;">
						<input id="registeredDealer" type="checkbox" class="view field-width" <#=obj.registeredDealer ? checked="checked" : '' #>></input>
						</div>
					</td>
                  </tr>
					<# for(var i=0;obj.customFieldValues && i<obj.customFieldValues.length;i++) { var customField = obj.customFieldValues[i]; #>
					<tr>
						<td><#=customField.fieldName#></td>
						<td>
						<# if (customField.valueType == 'text') { #>
							<input id="<#=customField.fieldName#>" type="text" size="15" class="custom table-editable view field-width" value="<#=customField.fieldValue#>"/>
						<# } else if (customField.valueType == 'date') { #>
							<input id="<#=customField.fieldName#>" type="text" size="15" class="custom datefield table-editable view field-width" value="<#= customField.fieldValue ? new Date(customField.fieldValue).toPaddedDate() : '' #>"/>
						<# } else if (customField.valueType == 'select') { #>
							<select id="<#=customField.fieldName#>" class="w150 custom table-editable view field-width">
								<# for (var j=0;j<customField.possibleValues.length;j++) { #>
								<option value="<#=customField.possibleValues[j]#>" <#= customField.possibleValues[j] == customField.fieldValue ? selected="selected" : '' #> ><#=customField.possibleValues[j]#></option>
								<# } #>
							</select>
						<# } else if (customField.valueType == 'checkbox') { #>
							<div class="lfloat" style="width:15px;">
							<input id="<#=customField.fieldName#>" type="checkbox" class="custom view field-width" <#=customField.fieldValue ? checked="checked" : '' #>/>
							</div>
						<# } #>
						</td>
					</tr>
					<# } #>
                  <tr>
					<td></td>
                    <td>
						<# if (Uniware.Utils.vendorUpdateAccess) { #>
						<div class="formRight lfloat" id="actionRow">
                           <div id="save" class="btn btn-small btn-success lfloat">Save</div>
                           <div id="cancel" class="lfloat10 link" style="margin-top:5px;">Discard Changes</div>
                        </div>
						<# } #>
					</td>
                  </tr>
                </table>
            </div>
			</script>
		</div>
		<div id="vendorAddressDiv" class="detailsbox lfloat vendorDetails hidden" style="width:49%;">
			<script type="text/html" id="vAddressTemplate">
			<div class="greybor headlable round_top ovrhid">
				<h4 class="edithead">Vendor Address</h4>
				<# if (obj.avlAddressTypes.length > 0 && Uniware.Utils.vendorUpdateAccess) { #>
					<div id="addMoreAddress" title="Add more address type" class="btn btn-small rfloat">
						+ Add more
					</div>
				<# } #>
			</div>
			<div class="greybor vendorDetailEdit round_bottom overauto" id="address">
					<# for (var i in obj.partyAddresses) { var add = obj.partyAddresses[i]; #>
						<div class="greybor lfloat round_all address-box">
							<div class="vendor-address-head round_top ovrhid">
								<div class="lfloat">
									<#=add.addressTypeName#>&#160;
								</div>
								<# if (Uniware.Utils.vendorUpdateAccess) { #>
									<div id="<#=add.addressType#>" class="editAddress editable rfloat" title="Edit <#=add.addressTypeName#>"></div>
								<# } #>
							</div>	
							<div class="clear"></div>
							<div class="vendor-add-cont">
							<#=add.addressLine1#><br/>
							<#=add.addressLine2#><br/>
							<#=add.city#> - <#=add.pincode#><br/>
							<#=add.stateName#>,&nbsp;<#=add.countryCode#><br/>
							<#=add.phone#><br/>
							</div>
						</div>
					<# } #>
			</div>
			</script>
		</div>
		<div class="clear"></div>
		<div id="vendorContactDiv" class="detailsbox vendorDetails hidden">
			<script type="text/html" id="vContactTemplate">
			<div class="greybor headlable round_top ovrhid">
				<h4 class="edithead">Vendor Contact</h4>
				<# if (obj.avlContactTypes.length > 0 && Uniware.Utils.vendorUpdateAccess) { #>
					<div id="addMoreContact" title="Add more contact type" class="btn btn-small rfloat">
						+ Add more
					</div>
				<# } #>
			</div>
			<div class="greybor form-edit-table-cont round_bottom" id="contact">
				<table class="uniTable" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<th>Type</th>
						<th>Name</th>
						<th>Email</th>
						<th>Mobile</th>
						<th>Fax</th>
						<th>Action</th>
					</tr>
				
					<# for (var i in obj.partyContacts) { var con = obj.partyContacts[i]; #>
						<tr>
							<td><#=con.contactTypeName#> Contact&#160;</td>
							<td><#=con.name#></td>
							<td><span style="word-wrap:break-word;"><#=con.email#></span></td>
							<td><#=con.phone#></td>
							<td><#=con.fax#></td>
							<td>
								<# if (Uniware.Utils.vendorUpdateAccess) { #>
									<div id="<#=con.contactType#>" class="editContact editable lfloat" title="Edit <#=con.contactType#> Contact" style="margin-top:4px;"></div>
								<# } #>
							</td>
						</tr>
					<# } #>
				</table>
			</div>
			</script>
		</div>
		
		<div id="vendorAgreementDiv" class="detailsbox vendorDetails hidden">
			<script id="vAgreementTemplate" type="text/html">
			<div class="greybor headlable round_top ovrhid">
				<h4 class="edithead">Vendor Agreements</h4>
				<# if (Uniware.Utils.vendorUpdateAccess) { #>
				<div id="addMoreAgreement" title="Add more agreements" class="btn btn-small rfloat">
					+ Add more
				</div>
				<# } #>
			</div>
			<div class="greybor round_bottom form-edit-table-cont">
				<table class="uniTable" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<th>Name</th>
						<th>Start Date</th>
						<th>End Date</th>
						<th>Status</th>
						<th>Operation</th>
					</tr>
					<tr id="noResultTR" class="<# if(obj.vendorAgreements.length > 0) { #> hidden <# } #>">
						<td colspan="5"><em>No Agreements</em></td>
					</tr>
					<# for ( var i = 0; i < obj.vendorAgreements.length; i++) { var vendorAgreement = obj.vendorAgreements[i]; #>
						<tr>
							<td class="first-col"><div id="<#=i#>" class="editAgreement link"><#=vendorAgreement.name#></div></td>
							<td><#=new Date(vendorAgreement.startTime).toDate()#></td>
							<td><#=new Date(vendorAgreement.endTime).toDate()#></td>
							<td><#=vendorAgreement.vendorAgreementStatus#></td>
							<#if(vendorAgreement.vendorAgreementStatus == 'CREATED') {#>
								<td><div class=" btn lfloat activate">Activate</div>
							<# } else {#>
								<td></td>
							<# } #>
						</tr>
					<# } #>
				</table>
			</div>
			</script>
		</div>
	</tiles:putAttribute>
	<tiles:putAttribute name="deferredScript">
    <sec:authentication property="principal" var="user" />
	<script type="text/javascript" src="${path.js('jquery/jquery.dataTables.min.js')}"></script>
	<script id="createVendorTemplate" type="text/html">
		<input type="hidden" id="vendorId" value="${vendor.id}">
					<table cellpadding="4">
						<tr>
							<td class="searchLabel" >Name</td>
							<td><input type="text" id="name"></td>
							<td class="searchLabel" >Code</td>
							<td><input type="text" id="code"></td>
						</tr>
						<tr>
							<td class="searchLabel" >PAN</td>
							<td><input type="text" id="pan"></td>
							<td class="searchLabel" >TIN</td>
							<td><input type="text" id="tin"></td>
						</tr>
						<tr>
							<td class="searchLabel" >Central Sales Tax</td>
							<td><input type="text" id="cstNumber"></td>
							<td class="searchLabel" >Sales Tax</td>
							<td><input type="text" id="stNumber"></td>
						</tr>
						<tr>
							<td class="searchLabel" >Purchase Expiry Period</td>
							<td><input type="text" id="purchaseExpiryPeriod"></td>
							<td class="searchLabel" >Website</td>
							<td><input type="text" id="website"></td>
						</tr>
						<tr>
							<td class="searchLabel" >Tax Exempted</td>
							<td><input type="checkbox" id="taxExempted"></td>
							<td class="searchLabel" >Registered Dealer</td>
							<td><input type="checkbox" id="registeredDealer"></td>
						</tr>
					</table>
		<table cellpadding="4">
		<# if (obj && obj.length > 0) { #>
				<tr>
				<# for(var i=0;i<obj.length;i++) { var customField = obj[i]; #>
		 			<td align="left" class="searchLabel"><#=customField.displayName#></td>
					<td align="left" class="searchLabel">
						<# if (customField.valueType == 'text') { #>
							<input id="<#=customField.fieldName#>" type="text" class="w150 custom poUpdateItem" value="<#=customField.fieldValue#>" style="width:70%;"/>
						<# } else if (customField.valueType == 'date') { #>
							<input id="<#=customField.fieldName#>" type="text" class="w150 custom poUpdateItem datefield" value="<#= customField.fieldValue ? new Date(customField.fieldValue).toPaddedDate() : '' #>" style="width:70%;"/>
						<# } else if (customField.valueType == 'select') { #>
							<select id="<#=customField.fieldName#>" class="w150 custom poUpdateItem" style="width:70%;">
							<# for (var j=0;j<customField.possibleValues.length;j++) { #>
								<option value="<#=customField.possibleValues[j]#>" <#= customField.possibleValues[j] == customField.fieldValue ? selected="selected" : '' #> ><#=customField.possibleValues[j]#></option>
							<# } #>
							</select>
						<# } else if (customField.valueType == 'checkbox') { #>
							<input id="<#=customField.fieldName#>" type="checkbox" class="custom poUpdateItem" <#=customField.fieldValue ? checked="checked" : '' #> style="width:70%;"/>
						<# } #>
					</td>
					<# if (i == obj.length - 1 && i % 2 == 0) { #>
						<td></td><td></td>
					<# } else if (i != 0 && i % 2 != 0) { #>
						</tr><tr>
					<# } #>
				<# } #>
				</tr>
            <# } #>
			<tr class="mar-15-top">
				<td></td>
				<td colspan="3">
					<input type="submit" class="btn btn-primary" id="addVendor" value="Create" style="margin-top:10px;"/>
				</td>
		 	</tr>
		</table>
	</script>
	<script id="vendorLink" type="text/html">
        <a href="/procure/searchVendor?legacy=1" style="text-decoration:none">
            <span class="mainHeading subHeaded">Vendors<span class="pipe">/</span></span>
        </a> 
        <span class="subHeading"><#=obj#></span>
    </script>
	<script type="text/javascript">
		<uc:security accessResource="VENDOR_UPDATE">
			Uniware.Utils.vendorUpdateAccess = true;
		</uc:security>
		Uniware.SearchVendorCatalogPage = function() {
			Uniware.Utils.countries = ${cache.getCache("locationCache").getCountriesJson()};
		    Uniware.Utils.currentFacilityCountry = '${cache.getCache("facilityCache").getCurrentFacility().getPartyAddressByType('SHIPPING').getCountryCode()}';
			var self = this;
			var url = window.location.href;
			this.name = 'DATATABLE SEARCH VENDORS';
			this.pageConfig = null;
			this.table = null;
			this.vendorCode = null;
			this.editRow = null;
			this.states = ${states};
			this.vendor = null;
			this.vendorCode = null;
			this.linkType = null;
			this.allAddressTypes = ${cache.getCache("partyCache").getAddressTypeJson()};
			this.allContactTypes = ${cache.getCache("partyCache").getContactTypeJson()};
			this.customFields = ${customFieldsJson};
			this.datatableViews = ${user.getDatatableViewsJson("DATATABLE SEARCH VENDORS")};
			this.init = function(){
				var vendorCode = Uniware.Utils.getParameterByName('vendorCode');
				if(vendorCode){
					self.vendorCode = vendorCode;
					self.getVendorDetails();
				}else{
					if(this.datatableViews.length){
						this.headConfig = new Uniware.HeadConfig(this.datatableViews,function(datatableObject){
							self.pageConfig.setView(datatableObject);
						});
					}
					$("#createVendor").click(self.createVendor);
					this.pageConfig = new Uniware.PageConfig(this.name,'#flexme1',this.datatableViews[0]);
					$("#tableContainer").on('click', '.editVendor' ,self.editVendor);
				}
			};
			
			this.editVendor = function(event) {
				var tr = $(event.target).parents('tr');
				var rowNumber = $(tr).attr('id').split('-')[1];
				var data = self.pageConfig.getRecord(rowNumber);
				self.vendorCode = self.pageConfig.getColumnValue(data,'vendorCode');
				self.getVendorDetails();
				self.linkType = 'Edit';
				$("#vendorCreateLink").html(template("vendorLink",self.linkType));
				$("#createVendorDiv").html("");
				$("#tableContainer").hide();
				$("#createVendor").hide();
			};
			
			this.createVendor = function() {
				self.linkType = 'Create';
				$("#vendorCreateLink").html(template("vendorLink",self.linkType));
				$("#tableContainer").hide();
				$("#createVendor").hide();
				$("#createVendorDiv").html(template("createVendorTemplate",self.customFields));
				$("#addVendor").click(self.addVendor);
			};
			
			this.addVendor = function () {
				var requestObject =  { 'vendor' :{
					'name' : $('#name').val(),
					'code' : $('#code').val(),
					'pan' : $('#pan').val(),
					'tin' : $('#tin').val(),
					'cstNumber' : $('#cstNumber').val(),
					'purchaseExpiryPeriod' : $("#purchaseExpiryPeriod").val(),
					'stNumber' : $('#stNumber').val(),
					'website' : $('#website').val(),
					'enabled' : true,
					'taxExempted': $('#taxExempted').is(':checked'),
					'registeredDealer': $('#registeredDealer').is(':checked')
				}};
				Uniware.Utils.addCustomFieldsToRequest(requestObject.vendor, $('.custom'));
				var requestData = JSON.stringify(requestObject);
				
				var url = "/data/procure/vendor/create";
				
				Uniware.Ajax.postJson(url, requestData, function(response) {
					if(response.successful == true){
						self.vendorCode = response.vendor.code;
						Uniware.Utils.addNotification('Vendor has been saved');
						self.getVendorDetails();
					}else{
						Uniware.Utils.showError(response.errors[0].description);													
					}
				});
			};
			
			this.getDateRange = function(){
				var dates = $( "#startTime, #endTime" ).datepicker({
					dateFormat : 'dd/mm/yy',
					changeMonth: true,
					changeYear: true,
					numberOfMonths: 1,
					minDate: 0,
					onSelect: function( selectedDate ) {
						var option = this.id == "startTime" ? "minDate" : "maxDate",
							instance = $( this ).data( "datepicker" ),
							date = $.datepicker.parseDate(
								instance.settings.dateFormat ||
								$.datepicker._defaults.dateFormat,
								selectedDate, instance.settings );
						dates.not( this ).datepicker( "option", option, date );
					}
				});
			};
			
			this.editVendorDetails = function(){
				$("#addressTemplateDiv").html("");
				$("#contactTemplateDiv").html("");
				$("#actionRow").addClass('invisible');
				$("input.view[type!='checkbox']").focus(function(){
					$(this).removeClass('view table-editable');
					$("#actionRow").removeClass('invisible');
					$("#save").click(self.updateVendor);
					$("#cancel").click(self.getVendorDetails);
				}).blur(function(){
					$(this).addClass('view table-editable');
				});
				$("input.view:checkbox").change(function(){
					$("#actionRow").removeClass('invisible');
					$("#save").click(self.updateVendor);
					$("#cancel").click(self.getVendorDetails);
				});
			};
			
			this.getVendorDetails = function(){
				var vendorCode = self.vendorCode;
				var requestObject = {
					'code' : vendorCode 
				}
				Uniware.Ajax.postJson('/data/procure/vendor/detail/get', JSON.stringify(requestObject), function(data) {
					data.vendor.avlContactTypes = []; 
					data.vendor.avlAddressTypes = [];
					self.avlAddressTypesMap = {};
					for(var contact in self.allContactTypes){
						if(!data.vendor.partyContacts[contact]) {
							data.vendor.avlContactTypes.push(self.allContactTypes[contact]);
						}
					}
					for(var address in self.allAddressTypes){
						if(!data.vendor.partyAddresses[address]) {
							data.vendor.avlAddressTypes.push(self.allAddressTypes[address]);
							self.avlAddressTypesMap[address] = self.allAddressTypes[address];
						}
					}
					
					self.vendor = data.vendor;
					if (self.vendor.configured) {
						$("#vendorConfigured").addClass('hidden');
					} else {
						$("#vendorConfigured").removeClass('hidden');
					}
					$("#createVendorDiv").html("");
					$("#pageViews").html("");
					if(self.linkType == 'Create') {
						self.linkType = 'Edit';
						$("#vendorCreateLink").html(template("vendorLink",self.linkType));
					}
					$('.vendorDetails').removeClass('hidden');
					self.loadVendorDetails(data);
					self.loadVendorContact(data);
					self.loadVendorAgreement(data);
					self.loadVendorAddress(data);
					self.loadVendorAgreement(data);
				});
			};
			
			this.loadVendorAddress = function(data){
				$("#vendorAddressDiv").html(template("vAddressTemplate", data.vendor));
				$(".editAddress").click(self.editVendorAddress);
				$("#addMoreAddress").click(self.addMoreAddress);
			};
			
			this.loadVendorDetails = function(data){
				$("#vendorDetails").html(template("vendorTemplate", data.vendor));
				self.editVendorDetails();
			};
			
			this.loadVendorContact = function(data){
				$("#vendorContactDiv").html(template("vContactTemplate", data.vendor));
				$(".editContact").click(self.editVendorContact);
				$("#addMoreContact").click(self.addMoreContact);
			};
			
			this.loadVendorAgreement = function(data){
				$("#vendorAgreementDiv").html(template("vAgreementTemplate", data.vendor));
				$("#addMoreAgreement").click(self.addMoreAgreement);
				$(".editAgreement").click(self.editVendorAgreement);
				$(".activate").click(self.activateAgreement);
			};
			
			this.addMoreAddress = function(){
				var obj = {};
				obj.states = self.states;
				obj.addressTypes = self.vendor.avlAddressTypes;
				obj.address = {};
				obj.address.countryCode = Uniware.Utils.currentFacilityCountry;
				obj.otherAddressTypes =   [];
				obj.otherAddressType = null;
				for(var addr in self.vendor.partyAddresses) {
					if(!self.avlAddressTypesMap[addr]) {
						obj.otherAddressTypes.push(self.allAddressTypes[addr]);
					}
				}
				$("#addressTemplateDiv").html(template("addressTemplate", obj));
				self.changeCreateAddress(obj);
				Uniware.LightBox.show('#addressDiv',function(){
					self.getStatesList(obj.address.countryCode,obj.address.stateCode);
				    self.attachCountryChangeEvent();
				});
			};
			
			this.changeCreateAddress = function(obj) {
				$('#otherAddressType').change(function() {
					obj.otherAddressType = $(this).val();
					if(obj.otherAddressType != 'none') {
						obj.address = self.vendor.partyAddresses[obj.otherAddressType];
					} else {
						obj.address = {};
						obj.address.countryCode = Uniware.Utils.currentFacilityCountry;
					}
					$("#addressTemplateDiv").html(template("addressTemplate", obj));
					Uniware.LightBox.show('#addressDiv',function(){
					    self.getStatesList(obj.address.countryCode,obj.address.stateCode);
					    self.attachCountryChangeEvent();
					});
					self.changeCreateAddress(obj);
				});
				$("#submit").click(function(){
					self.createAddress();
				});
			};
			
			this.addMoreAgreement = function(){
				var obj = {};
				obj.vendorAgreements = self.vendor.vendorAgreements;
				$("#agreementTemplateDiv").html(template("agreementTemplate", obj));
				$("#submitAgreement").click(function(){
					self.createAgreement();
				});
				self.getDateRange();
				Uniware.LightBox.show('#agreementDiv');
			};
			
			this.addMoreContact = function(){
				var obj = {};
				obj.contactTypes = self.vendor.avlContactTypes;
				obj.editable = false;
				$("#contactTemplateDiv").html(template("contactTemplate", obj));
				$("#submitContact").click(function(){
					self.createContact();
				});
				Uniware.LightBox.show('#contactDiv');
			};
			
			this.editVendorAddress = function(){
				var addressId = $(this).attr('id');
				var obj = {};
				obj.address = self.vendor.partyAddresses[addressId];
				obj.currentAddressId = addressId;
				obj.states = self.states;
				obj.otherAddressTypes =   [];
				obj.otherAddressType = null;
				obj.addressTypes = [];
				for(var addr in self.allAddressTypes) {
					if(addr == addressId) {
						obj.addressTypes = [self.allAddressTypes[addr]];							
					}
				}
				for(var addr in self.vendor.partyAddresses) {
					if(addr != addressId)
					obj.otherAddressTypes.push(self.allAddressTypes[addr]);
				}
				$("#addressTemplateDiv").html(template("addressTemplate", obj));
				self.changeEditAddress(obj);
				Uniware.LightBox.show('#addressDiv',function(){
				    self.getStatesList(obj.address.countryCode,obj.address.stateCode);
				    self.attachCountryChangeEvent();
				});
			};
			
			this.changeEditAddress = function(obj) {
				$('#otherAddressType').change(function() {
					obj.otherAddressType = $(this).val();
					if(obj.otherAddressType != 'none') {
						obj.address = self.vendor.partyAddresses[obj.otherAddressType];
					} else {
						obj.address = self.vendor.partyAddresses[obj.currentAddressId];
					}
					$("#addressTemplateDiv").html(template("addressTemplate", obj));
					Uniware.LightBox.show('#addressDiv',function(){
					    self.getStatesList(obj.address.countryCode,obj.address.stateCode);
					    self.attachCountryChangeEvent();
					});
					self.changeEditAddress(obj);
				});
				$("#submit").click(function(){
					self.editAddress();
				});
			};
			
			this.attachCountryChangeEvent = function(){
				var container = $('#addressTemplateDiv');
				container.find('#country').bind('change',function(){
					var countryCode = $(this).find('option:selected').val();
					self.getStatesList(countryCode);
				});
			};
			
			this.getStatesList = function(countryCode,state){
				Uniware.Ajax.getJson("/data/admin/system/states/get?countryCode=" + countryCode, function(data) {
					if(data==null){
						$('#addressTemplateDiv').find('#state').replaceWith('<input type="text" id="state" style="width:116px;" autocomplete="off"/>');
						$('#addressTemplateDiv').find('#state').focus();
					}
					else{
						$('#addressTemplateDiv').find('#state').replaceWith('<select id="state" class="w130"></select>');
						var html = template("countryStateMap", data);
						$("#addressTemplateDiv").find('#state').html(html);
					}
					if(typeof state != "undefined"){
						$('#addressTemplateDiv').find('#state').val(state);
					}
				});
 			};
			
			
			this.editVendorContact = function(){
				var contactId = $(this).attr('id');
				var obj = self.vendor.partyContacts[contactId];
				obj.contactTypes = [];
				for(var contact in self.allContactTypes) {
					if(contact == contactId) {
						obj.contactTypes = [self.allContactTypes[contact]];							
					}
				}
				obj.editable = true;
				$("#contactTemplateDiv").html(template("contactTemplate", obj));
				$("#submitContact").click(function(){
					self.editContact();
				});
				$("#deleteContact").click(function(){
					self.deleteContact();
				});
				Uniware.LightBox.show('#contactDiv');
			};
			
			this.editVendorAgreement = function(){
				var index = $(this).attr('id');
				var obj = self.vendor.vendorAgreements[index];
				//TODO
				//obj.contactTypes = [contactId];
				
				$("#agreementTemplateDiv").html(template("agreementTemplate", obj));
				$("#submitAgreement").click(function(){
					self.editAgreement();
				});
				self.getDateRange();
				Uniware.LightBox.show('#agreementDiv');
			};
			
			this.activateAgreement = function(){
				var index = $(this).closest('tr')[0].sectionRowIndex-2;
				var obj = self.vendor.vendorAgreements[index];
				var req = {
					vendorAgreementId: obj.id
				};
									
				Uniware.Ajax.postJson('/data/procure/vendor/agreement/activate', JSON.stringify(req), function(response) {
					if (response.successful == false) {
						Uniware.Utils.showError(response.errors[0].description);
					} else {
						Uniware.Utils.addNotification('Vendor Agreement has been activated');
						self.getVendorDetails();
					}
				});
			};
			
			this.getAddressRequest = function() {
				var partyAddress = {};
				partyAddress.partyCode = self.vendor.code;
				partyAddress.addressType = $("#addressType").val();
				partyAddress.addressLine1 = $("#address1").val();
				partyAddress.addressLine2 = $("#address2").val();
				partyAddress.city = $("#city").val();
				partyAddress.stateCode = $("#state").val();
				partyAddress.pincode = $("#pincode").val();
				partyAddress.phone = $("#phone").val();
				partyAddress.countryCode = $("#country").val();
				return {'partyAddress' : partyAddress};
			};
			
			this.createAddress = function() {
				$("#errorAddress").addClass('invisible');
				var req = self.getAddressRequest();
				Uniware.Ajax.postJson('/data/procure/vendor/address/create', JSON.stringify(req), function(response) {
					if (response.successful == false) {
						$("#errorAddress").html(response.errors[0].description.split('|')[0]).removeClass('invisible');
					} else {
						Uniware.Utils.addNotification('Vendor Address has been added');
						self.vendor.partyAddresses[response.partyAddressDTO.addressType] = response.partyAddressDTO;
						self.getVendorDetails();
						Uniware.LightBox.hide();
					}
				});
			};
			
			this.editAddress = function() {
				$("#errorAddress").addClass('invisible');
				var req = self.getAddressRequest();
				Uniware.Ajax.postJson('/data/procure/vendor/address/edit', JSON.stringify(req), function(response) {
					if (response.successful == false) {
						$("#errorAddress").html(response.errors[0].description.split('|')[0]).removeClass('invisible');
					} else {
						Uniware.Utils.addNotification('Vendor Address has been edited');
						self.vendor.partyAddresses[response.partyAddressDTO.addressType] = response.partyAddressDTO;
						self.getVendorDetails();
						Uniware.LightBox.hide();
					}
				});
			};
			
			this.getContactRequest = function() {
				var partyContact = {};
				partyContact.partyCode = self.vendor.code;
				partyContact.contactType = $("#contactType").val();
				partyContact.name = $("#contactName").val();
				partyContact.email = $("#contactEmail").val();
				partyContact.phone = $("#contactPhone").val();
				partyContact.fax = $("#contactFax").val();
				return {'partyContact' : partyContact};
			};
			
			this.createContact = function() {
				$("#errorContact").addClass('invisible');
				var req = self.getContactRequest();
				Uniware.Ajax.postJson('/data/procure/vendor/contact/create', JSON.stringify(req), function(response) {
					if (response.successful == false) {
						$("#errorContact").html(response.errors[0].description.split('|')[0]).removeClass('invisible');
					} else {
						Uniware.Utils.addNotification('Vendor Contact has been added');
						self.getVendorDetails();
						Uniware.LightBox.hide();
					}
				});
			};
			
			this.editContact = function() {
				$("#errorContact").addClass('invisible');
				var req = self.getContactRequest();
				Uniware.Ajax.postJson('/data/procure/vendor/contact/edit', JSON.stringify(req), function(response) {
					if (response.successful == false) {
						$("#errorContact").html(response.errors[0].description.split('|')[0]).removeClass('invisible');
					} else {
						Uniware.Utils.addNotification('Vendor Contact has been edited');
						self.vendor.partyContacts[response.partyContactDTO.contactType] = response.partyContactDTO;
						self.getVendorDetails();
						Uniware.LightBox.hide();
					}
				});
			};
			
			this.deleteContact = function() {
				$("#errorContact").addClass('invisible');
				var req = {};
				req.partyCode = self.vendor.code;
				req.contactType = $("#contactType").val();
				Uniware.Ajax.postJson('/data/procure/vendor/contact/delete', JSON.stringify(req), function(response) {
					if (response.successful == false) {
						$("#errorContact").html(response.errors[0].description.split('|')[0]).removeClass('invisible');
					} else {
						Uniware.Utils.addNotification('Vendor Contact has been deleted');
						self.getVendorDetails();
						Uniware.LightBox.hide();
					}
				});
			};
			
			this.getAgreementRequest = function() {
				var req = {};
				req.vendorAgreement = {};
				req.vendorAgreement.vendorCode = self.vendor.code;
				req.vendorAgreement.startTime = Date.fromPaddedDate($("#startTime").val()).getTime();
				req.vendorAgreement.endTime = Date.fromPaddedDate($("#endTime").val()).getTime();
				req.vendorAgreement.name = $("#agreementName").val();
				req.vendorAgreement.agreementText = $("#agreementText").val();
				return req;
			};
			
			this.createAgreement = function() {
				$("#error").addClass('invisible');
				var req = self.getAgreementRequest();
				Uniware.Ajax.postJson('/data/procure/vendor/agreement/create', JSON.stringify(req), function(response) {
					if (response.successful == false) {
						$("#error").html(response.errors[0].description.split('|')[0]).removeClass('invisible');
					} else {
						Uniware.Utils.addNotification('Vendor Agreement has been added');
						self.getVendorDetails();
						Uniware.LightBox.hide();
					}
				});
			};
			
			this.editAgreement = function() {
				$("#error").addClass('invisible');
				var req = self.getAgreementRequest();
				Uniware.Ajax.postJson('/data/procure/vendor/agreement/edit', JSON.stringify(req), function(response) {
					if (response.successful == false) {
						$("#error").html(response.errors[0].description.split('|')[0]).removeClass('invisible');
					} else {
						Uniware.Utils.addNotification('Vendor Agreement has been edited');
						self.getVendorDetails();
						Uniware.LightBox.hide();
					}
				});
			};
			
			this.updateVendor = function(){
				var req = { 'vendor' : {
					'name' : $('#vendorName').val(),
					'code' : $('#vendorCode').html(),
					'pan' : $('#pan').val(),
					'tin' : $('#tin').val(),
					'cstNumber' : $('#cstNumber').val(),
					'stNumber' : $('#stNumber').val(),
					'website' : $('#website').val(),
					'purchaseExpiryPeriod' : $("#purchaseExpiryPeriod").val(),
					'acceptsCForm' : $("#acceptsCForm").is(":checked"),
					'enabled' : $('#enabled').is(":checked"),
					'taxExempted': $("#taxExempted").is(":checked"),
					'registeredDealer': $('#registeredDealer').is(':checked')
				}};
				Uniware.Utils.addCustomFieldsToRequest(req.vendor, $('.custom'));
				Uniware.Ajax.postJson("/data/procure/vendor/update", JSON.stringify(req), function(response) {
					if(response.successful == true) {
						Uniware.Utils.addNotification('Vendor details have been updated.');
						self.getVendorDetails();
					} else {
						Uniware.Utils.showError(response.errors[0].description);													
					}
				});
			};
			
		};				
		$(document).ready(function() {
			window.page = new Uniware.SearchVendorCatalogPage();
			window.page.init();
		});
	</script>
	<script id="countryStateMap" type="text/html">
            <# for(var code in obj) { #>
				<option value="<#=code#>"><#=obj[code]#></option>				
		    <# } #>
        </script> 
		<script type="text/html" id="addressTemplate">
			<div class="formLeft150 lfloat">Address Type</div>
			<div class="formRight lfloat">
				<# if(obj.addressTypes.length == 1) { #>
					<input id="addressType" type="hidden" value="<#=obj.addressTypes[0].code#>" />
					<input type="text" id="addressType" size="15" autocomplete="off" value="<#=obj.addressTypes[0].name#>" readOnly="readOnly" class="w250"/>
				<# } else { #>
					<select id="addressType" class="w250">
						<# for(var i =0; i< obj.addressTypes.length; i++) { #>
							<option value="<#=obj.addressTypes[i].code#>"><#=obj.addressTypes[i].name#></option>				
						<# } #>
					</select>
				<# } #>
			</div>
			<div class="clear"></div>

			<# if(obj.otherAddressTypes.length > 0) { #>
				<div class="formLeft150 lfloat">Copy From</div>
				<div class="formRight lfloat">
					<select id="otherAddressType" class="w250">
						<option value="none">-NONE-</option>
						<# for(var i=0;i<obj.otherAddressTypes.length;i++) { #>
							<option value="<#=obj.otherAddressTypes[i].code#>" <# if (obj.otherAddressTypes[i].code == obj.otherAddressType) { #> selected="selected" <# } #>><#=obj.otherAddressTypes[i].name#></option>				
						<# } #>
					</select>
				</div>
				<div class="clear"></div>
			<# } #>

			<div class="formLeft150 lfloat">Address Line1</div>
			<div class="formRight lfloat"><input type="text" id="address1" size="15" autocomplete="off" value="<#=obj.address.addressLine1#>" class="w250"/></div>
			<div class="clear"></div>

			<div class="formLeft150 lfloat">Address Line2</div>
			<div class="formRight lfloat"><input type="text" id="address2" size="15" autocomplete="off" value="<#=obj.address.addressLine2#>" class="w250"/></div>
			<div class="clear"></div>

			<div class="formLeft150 lfloat">City</div>
			<div class="formRight lfloat"><input type="text" id="city" size="15" autocomplete="off" value="<#=obj.address.city#>" class="w250"/></div>
			<div class="clear"></div>

			<div class="formLeft150 lfloat">Country/State</div>
			<div class="formRight lfloat">
                <select id="country" class="w130">
                <# for(var code in Uniware.Utils.countries) { #>
					<option value="<#=code#>" <# if (obj.address.countryCode && code == obj.address.countryCode) { #> selected="selected" <# } #> ><#=Uniware.Utils.countries[code]#></option>
				<# } #>
                </select>
			</div>
			<div class="lflaot10" style="margin-top:5px;"><input type="text" id="state" style="width:116px;" autocomplete="off"/></div>
			<div class="clear"></div>

			<div class="formLeft150 lfloat">Pincode</div>
			<div class="formRight lfloat"><input type="text" id="pincode" size="15" autocomplete="off" value="<#=obj.address.pincode#>"  class="w250"/></div>
			<div class="clear"></div>

			<div class="formLeft150 lfloat">Phone</div>
			<div class="formRight lfloat"><input type="text" id="phone" size="15" autocomplete="off" value="<#=obj.address.phone#>" class="w250"/></div>
			<div class="clear"></div>

			<br /> 
			<div class="formLeft150 lfloat">&#160;</div>
			<div class="formRight lfloat">
				<div id="submit" class=" btn btn-small btn-primary lfloat">submit</div>
			</div>
			<div class="clear"/>
			<div id="errorAddress" class="errorField lfloat20 invisible"></div>
			<div class="clear"></div>
		</script>
		
		<script type="text/html" id="contactTemplate">
			<div class="formLeft150 lfloat">Contact Type</div>
			<div class="formRight lfloat">
				<# if(obj.contactTypes.length == 1) { #>
					<input id="contactType" type="hidden" value="<#=obj.contactTypes[0].code#>" />
					<input type="text" id="contact" size="15" autocomplete="off" value="<#=obj.contactTypes[0].name#>" readOnly="readOnly" />
				<# } else { #>				
					<select id="contactType">
						<# for(var i=0; i < obj.contactTypes.length; i++) { #>
							<option value="<#=obj.contactTypes[i].code#>"><#=obj.contactTypes[i].name#></option>				
						<# } #>
					</select>
				<# } #>
			</div>
			<div class="clear"></div>

			<div class="formLeft150 lfloat">Name</div>
			<div class="formRight lfloat"><input type="text" id="contactName" size="15" autocomplete="off" value="<#=obj.name#>" /></div>
			<div class="clear"></div>

			<div class="formLeft150 lfloat">Email</div>
			<div class="formRight lfloat"><textarea id="contactEmail"><#=Uniware.Utils.escapeHtml(obj.email)#></textarea></div>
			<div class="clear"></div>

			<div class="formLeft150 lfloat">Phone</div>
			<div class="formRight lfloat"><input type="text" id="contactPhone" size="15" autocomplete="off" value="<#=obj.phone#>" /></div>
			<div class="clear"></div>

			<div class="formLeft150 lfloat">Fax</div>
			<div class="formRight lfloat"><input type="text" id="contactFax" size="15" autocomplete="off" value="<#=obj.fax#>"  /></div>
			<div class="clear"></div>

			<br /> <br />
			<div class="formLeft150 lfloat">&#160;</div>
			<div class="formRight lfloat">
				<div id="submitContact" class=" btn btn-small btn-primary lfloat">submit</div>
				<# if(obj.editable) { #>
					<div id="deleteContact" class=" btn btn-small btn-danger lfloat10">delete</div>
				<# } #>
			</div>
			<div class="clear"/>
			<div id="errorContact" class="errorField lfloat20 invisible"></div>
			<div class="clear"></div>
		</script>
		<script type="text/html" id="agreementTemplate">
		<table width="100%" cellpadding="0" cellspacing="0" border="0">
			<tbody>
				<tr>
					<td class="lb-over-form-cont" width="20%"><label>From:</label></td>
					<td class="lb-over-form-cont">
						<input type="text" id="agreementId" value="<#=obj.id#>" style="display:none;"/>
						<div class="lfloat">
							<input type="text" id="startTime" value="<#=obj.startTime ? new Date(obj.startTime).toPaddedDate() : ''#>" class="datefield" style="width:140px;"/>
						</div>
						<div class="lfloat20" style="margin-left:100px;"><label>To:</label></div>
						<div class="rfloat20">
							<input type="text" id="endTime" value="<#=obj.endTime ? new Date(obj.endTime).toPaddedDate() : ''#>" class="datefield"  style="width:140px;"/>
						</div>
					</td>
				</tr>
				<tr>
					<td class="lb-over-form-cont">Name:</td>
					<td class="lb-over-form-cont"><input type="text" id="agreementName" value="<#=obj.name#>" class="field-width"/></td>
				</tr>
				<tr>
					<td valign="top" class="lb-over-form-cont">Full Text:</td>
					<td class="lb-over-form-cont"><textarea id="agreementText" rows="10" name="textData" class="inputBox field-width"><#=Uniware.Utils.escapeHtml(obj.agreementText)#></textarea></td>
				</tr>
				<tr>
					<td></td>
					<td>
						<div id="submitAgreement" class=" btn btn-small btn-primary lfloat">submit</div>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<div id="error" class="errorField lfloat20 invisible"></div>
					</td>
				</tr>
			</tbody>
		</table>
		</script>
	</tiles:putAttribute>
</tiles:insertDefinition>
