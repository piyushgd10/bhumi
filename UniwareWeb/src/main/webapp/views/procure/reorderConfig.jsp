<%@ include file="/tagIncludes.jsp"%>
<%@ include file="/tagIncludes.jsp"%>
<tiles:insertDefinition name="admin.catalogPage">
	<tiles:putAttribute name="title" value="Uniware - Products Reorders Configuration" />
	<tiles:putAttribute name="rightPane">
		<div id="pageBar">
			<div class="pageHeading"><span class="mainHeading">Products Reorder Configuration<span class="pipe">/</span></span><span class="subHeading"></span></div>
			<div class="pageControls"></div>
			<div class="rfloat uploadButtonWrapper">
				<input type="file" name="file" id="importFile" style="height:1px;width:1px;" />	
				<em>Select File</em>
				<div id="upload" class="btn btn-small">
					upload&#160;&#160;<i class="icon-upload-alt"></i>
				</div>
			</div>
			<div id="addReorderConfigButton" class="btn btn-small btn-primary rfloat" style="margin: 10px 5px;">Add Reorder Config</div>
			<div class="clear"></div><br/>
		</div>
		<div id="dataTable">
			<table id="flexme1" style="display: none"></table>
		</div>
		<div id="editReordersConfigFiv" class="lb-over">
			<div class="lb-over-inner round_all">
				<div style="margin: 40px;line-height: 30px;">
					<div class="pageHeading lfloat">Edit Product Reorder Configuration</div>
					<div id="editReordersConfigFiv_close" class="link rfloat">Close</div>
					<div class="clear"></div> 	
					<div id="editReorderConfigTemplateDiv" style="margin:20px 0;"></div>
				</div>
			</div>
		</div>
		<div id="addReordersConfigFiv" class="lb-over">
			<div class="lb-over-inner round_all">
				<div style="margin: 40px;line-height: 30px;">
					<div class="pageHeading lfloat">Add Product Reorder Configuration</div>
					<div id="addReordersConfigFiv_close" class="link rfloat">Close</div>
					<div class="clear"></div> 	
					<div id="addReorderConfigTemplateDiv" style="margin:20px 0;"></div>
				</div>
			</div>
		</div>
	</tiles:putAttribute>
	<tiles:putAttribute name="deferredScript">
    <sec:authentication property="principal" var="user" />
	<script type="text/javascript" src="${path.js('jquery/jquery.dataTables.min.js')}"></script>
	<script type="text/javascript" src="${path.js('jquery/ajaxfileupload.js')}"></script>
	<script type="text/html" id="reorderConfigsTemplate">
			<div class="formLeft150 lfloat">Item Name</div>
			<div class="formRight lfloat"><#=obj.itemName#></div>
			<div class="clear"></div>			

			<div class="formLeft150 lfloat">SKU Code</div>
			<div class="formRight lfloat"><#=obj.itemSku#></div>
			<div class="clear"></div>

			<div class="formLeft150 lfloat">Reorder Threshold</div>
			<div class="formRight lfloat"><input type="text" id="threshold" size="30" value="<#=obj.reorderThreshold#>"/></div>
			<div class="clear"></div>

			<div class="formLeft150 lfloat">Reorder Quantity</div>
			<div class="formRight lfloat"><input type="text" id="quantity" size="30" value="<#=obj.reorderQuantity#>"/></div>
			<div class="clear"></div>
			<br />
			
			<div class="formLeft150 lfloat"></div>
			<div class="formRight lfloat">
				<div class="formRight lfloat">
					<input type="submit" id="updateReorderConfig" class=" btn btn-small btn-primary lfloat" value="submit"/>
					<div id="error" class="errorField lfloat20"></div>
				</div>
			</div>
			<div class="clear"></div>
			
	</script>
	<script type="text/html" id="addReorderConfigsTemplate">
			<div class="formLeft150 lfloat">Item SKU Code</div>
			<div class="formRight lfloat"><input type="text" id="skuCode" size="30" value=""/></div>
			<div class="clear"></div>			

			<div class="formLeft150 lfloat">Reorder Threshold</div>
			<div class="formRight lfloat"><input type="text" id="addThreshold" size="30" value=""/></div>
			<div class="clear"></div>

			<div class="formLeft150 lfloat">Reorder Quantity</div>
			<div class="formRight lfloat"><input type="text" id="addQuantity" size="30" value=""/></div>
			<div class="clear"></div>
			<br />
			
			<div class="formLeft150 lfloat"></div>
			<div class="formRight lfloat">
				<div class="formRight lfloat">
					<input type="submit" id="addReorderConfig" class=" btn btn-small btn-primary lfloat" value="submit"/>
					<div id="error" class="errorField lfloat20"></div>
				</div>
			</div>
			<div class="clear"></div>
			
	</script>
	<script type="text/javascript">
		Uniware.ProductMappingPage = function() {
			var self = this;
			this.name = 'DATATABLE ITEM REORDER CONFIG';
			this.pageConfig = null;
			this.table = null;
			this.itemSKU;
			this.datatableViews = ${user.getDatatableViewsJson("DATATABLE ITEM REORDER CONFIG")};
			this.init = function(){
				this.pageConfig = new Uniware.PageConfig(this.name,'#flexme1',this.datatableViews[0]);
				if(this.datatableViews.length){
					this.headConfig = new Uniware.HeadConfig(this.datatableViews,function(datatableObject){
						self.pageConfig.setView(datatableObject);
					},this.pageConfig);
				}
				$('#dataTable').on('click', '.edit', self.editChannelItemType);
				$("#upload").click(function() {
					$('input#importFile[type=file]').trigger('click').change(function(e){
						self.uploadFile(e);
					});
				});
				$('#addReorderConfigButton').click(self.addReorderConfigType);
			};

			this.addReorderConfigType = function() {
				$("#addReorderConfigTemplateDiv").html(template("addReorderConfigsTemplate"));
				$("#skuCode").autocomplete(self.getAutoCompleteObj());
				$("#addReorderConfig").click(self.addReorderConfig);
				Uniware.LightBox.show("#addReordersConfigFiv", function() {
					$('#skuCode').focus();
				});
			}
			
			this.editChannelItemType = function(event) {
	        	var tr = $(event.target).parents('tr');
				var rowNumber = $(tr).attr('id').split('-')[1];
				var data = self.pageConfig.getRecord(rowNumber);
				self.editRow = data;
				var rowValues = {};
				rowValues.itemSku = self.pageConfig.getColumnValue(data,'itemSku');
				rowValues.itemName = self.pageConfig.getColumnValue(data,'itemName');
				rowValues.facility = self.pageConfig.getColumnValue(data,'facility');
				rowValues.reorderThreshold = self.pageConfig.getColumnValue(data,'threshold');
				rowValues.reorderQuantity = self.pageConfig.getColumnValue(data,'reorderQuantity');
				self.editRow = data;
				$("#editReorderConfigTemplateDiv").html(template("reorderConfigsTemplate",rowValues));
				$("#updateReorderConfig").click(self.updateReorderConfig);
				$('.make-switch').bootstrapSwitch();
				Uniware.LightBox.show("#editReordersConfigFiv", function() {
					$('#threshold').focus();
				});
	        };
	        
	        this.uploadFile = function(event){
                var importFile = $('#importFile').val();
                if(importFile == null || importFile == ""){
                    Uniware.Utils.addNotification("Please enter a file path");
                    event.stopPropagation();
                }else{
                    var url = "/data/import/job/create?name=Reorder Configuration" + "&importOption=CREATE_NEW";
                    $.ajaxFileUpload({
                        url : url,
                        secureuri : true,
                        fileElementId : 'importFile',
                        dataType : 'json',
                        success : function(data, status) {
                            if(data.successful == true){
                                Uniware.Utils.addNotification("Import Job created successfully. Import Job Id:" + data.importJobId + " ,Task Id:" + data.taskId);
                            }else{
                                Uniware.Utils.showError(data.errors[0].description);    
                            }
                        }
                    }); 
                }
            };
	        
	        this.updateReorderConfig = function(){
	        	var req = {
	    	        	'itemSkuCode' :self.pageConfig.getColumnValue(self.editRow,'itemSku'),
	       				'reorderQuantity' : $("#quantity").val(),
	       				'reorderThreshold' : $("#threshold").val()
	        	};
	        	self.itemSKU = null;
				Uniware.Ajax.postJson("/data/procure/reorders/update", JSON.stringify(req), function(response) {
					if (response.successful == false) {
						Uniware.Utils.showError(response.errors[0].description);
						Uniware.LightBox.hide();
					} else {
						Uniware.LightBox.hide();
						Uniware.Utils.addNotification('Reorders configuration successfully updated.');
						self.pageConfig.reload();
					}
				});
	        };

	        this.addReorderConfig = function(){
				var req = {
						'itemSkuCode' : $("#skuCode").val(),
		       			'reorderQuantity' : $("#addQuantity").val(),
		       			'reorderThreshold' : $("#addThreshold").val()
				};
				Uniware.Ajax.postJson("/data/procure/reorders/update", JSON.stringify(req), function(response) {
					if (response.successful == false) {
						Uniware.Utils.showError(response.errors[0].description);
						Uniware.LightBox.hide();
					} else {
						Uniware.LightBox.hide();
						Uniware.Utils.addNotification('Reorders configuration successfully updated.');
						self.pageConfig.reload();
					}
				});
	        };


	        this.getAutoCompleteObj = function() {
				return {
			    	minLength: 4,	
			    	mustMatch : true,
					autoFocus: true,
					source: function( request, response ) {
						Uniware.Ajax.getJson("/data/lookup/itemTypes?keyword=" + request.term, function(data) {
							response( $.map( data, function( item ) {
								return {
									label: item.name + " - " + item.itemSKU,
									value: item.itemSKU,
								}
							}));
						});
					}
			    };
			};

	        this.exportCurrentDataViewMine = function() {
	    		var req = {"exportJobTypeName":"DATATABLE ITEM REORDER CONFIG",
	    	    		"exportColums":["itemSku","itemName","facility","threshold","reorderQuantity"]
	    			}
	    		req.exportFilters = self.pageConfig.currentTableObject.activeFilters;
	    		Uniware.Ajax.postJson("/data/tasks/export/job/create", JSON.stringify(req), function(response) {
	    			if (response.successful == false) {
	    				Uniware.Utils.showError(response.errors[0].description);
	    			} else {
	    				Uniware.Utils.addNotification('An Export Job has been created, Please check the Running Jobs.<br/>An Email will be sent on job completion.');
	    			}
	    		}, true);
	    	};
		};
		$(document).ready(function() {
			window.page = new Uniware.ProductMappingPage();
			window.page.init();
		});
	</script>
	</tiles:putAttribute>
</tiles:insertDefinition>
