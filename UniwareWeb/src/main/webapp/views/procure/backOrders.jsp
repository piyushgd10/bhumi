<%@ include file="/tagIncludes.jsp"%>
<tiles:insertDefinition name=".procurePage">
	<tiles:putAttribute name="title" value="Uniware - Back Orders - Waiting for Inventory" />
	<tiles:putAttribute name="rightPane">
		<div>
		<form onsubmit="javascript : return false;">
			<div class="greybor headlable ovrhid main-box-head">
				<h2 class="edithead head-textfields">Back Orders - Waiting for Inventory</h2>
			</div>
			<div class="round_bottom ovrhid pad-15">
				<div class="lfloat20">
					<div class="searchLabel">Vendor Name</div>
					<input type="text" id="vendorName" size="20" autocomplete="off" />
				</div>
				<div class="lfloat20">
					<div class="searchLabel">Item Type Name/Code Contains</div>
					<input type="text" id="itemTypeName" size="30" autocomplete="off" />
				</div>
				<div class="lfloat20">
					<div class="searchLabel">Category</div>
					<select id="category" style="width:150px;">
					<option value="">--ALL--</option>
					<c:forEach items="${cache.getCache('categoryCache').categories}" var="category">
						<option value="${category.code }">${category.name}</option>
					</c:forEach>
					</select>
				</div>
				<div class="lfloat20" style="margin-top:20px;">	
					<input id="search" value="search" type="submit" class=" btn btn-primary" />
				</div>
				<div id="searching" class="lfloat10 hidden" style="margin-top:25px;">
					<img src="/img/icons/refresh-animated.gif"/>
				</div>
			</div>
			<div id="warningNoVendors" class="lfloat20 hidden">
			
			</div>
		</form>
		</div>
		<div id="backOrders"></div>
	</tiles:putAttribute>
	<tiles:putAttribute name="deferredScript">
	<script type="text/javascript" src="${path.js('jquery/jquery.dataTables.min.js')}"></script>
	<script id="tableTemplate" type="text/html">
		<table id="dataTable" class="dataTable">
			<div id="addToCart" class=" btn btn-primary rfloat20" style="margin:10px 40px;">Add to Cart</div>
		</table>
	</script>
	<script id="suggestionTemplate" type="text/html">
		<# var qtyToAdd = obj.waitingQuantity;#>
		<# if (obj.addedQuantity) { qtyToAdd = qtyToAdd - obj.addedQuantity; } #>
		<td>
			<input type="checkbox" id="check" class="item-check" <#=obj.vendorItemTypes.length > 0 && qtyToAdd > 0 ? '' : disabled="disabled"#> ></input>
		</td>
		<td class="breakWord">
			<#=obj.name#>
			<#=Uniware.Utils.getItemDetail(obj.itemTypeImageUrl, obj.itemTypePageUrl)#>
		</td>
		<td class="sellerSkuCode ovrhid breakWord" title="<#=obj.skuCode#>"><#=obj.skuCode#></td>
		<td align="center"><#=obj.waitingQuantity#></td>
		<td align="center" class="bold"><#=obj.addedQuantity#></td>
		<# if (obj.vendorItemTypes.length == 0) { #>
			<td colspan="4">
				<em class="f12">No Vendor for this item type</em>
			</td>
		<# } else if (qtyToAdd <= 0) { #>
			<td colspan="4">
				<em class="f12">Quantity already added in cart</em>
			</td>
		<# } else { #>
			<# var selectedVendorIndex = 0; #>
			<td>
				<select class="vendor field-width" id="vendor-<#=obj.index#>">
					<# for(var i=0; i<obj.vendorItemTypes.length; i++) { var vendorItemType = obj.vendorItemTypes[i]; #>
						<# if(vendorItemType.vendorId == obj.vendor) { selectedVendorIndex = i; #>
							<option value="<#=i#>" selected="selected"><#=vendorItemType.vendorName#> (<#=vendorItemType.unitPrice#>)</option>
						<# } else { #>
							<option value="<#=i#>"><#=vendorItemType.vendorName#> (<#=vendorItemType.unitPrice#>)</option>
						<# } #>
					
					<# } #>
				</select>
			</td>
			<td><input id="qty-<#=obj.index#>" type="text" value="<#=qtyToAdd > 0 ? qtyToAdd : 0#>" style="width:80%;"/></td>
			<td><input id="unitPrice-<#=obj.index#>" type="text" class="unitPrice"  style="width:70%;" value="<#=obj.vendorItemTypes[selectedVendorIndex].unitPrice#>"></input></td>
		<# } #>
	</script>	
	<script type="text/javascript">
		Uniware.BackOrderPage = function() {
			var self = this;
			this.purchaseCart = null;
			this.cartQuantity = {};
			this.backOrders = null;
			this.vendor = null;
			this.table = null;
			
			this.init = function() {
				self.listVendors();
				$('#search').click(self.search);
				
				var req = {
					'noVendors' : true
				};
				Uniware.Ajax.postJson("/data/procure/backorders/get", JSON.stringify(req), function(response) {
			 		if(response.successful == true && response.elements && response.elements.length > 0) {
			 			$('#warningNoVendors').removeClass('hidden').html('There are ' + response.elements.length + ' backorder SKUs with No Vendors').addClass('alert alert-danger');
			 			$('#warningNoVendors').addClass('link').click(function(){
			 				self.searchBackOrders(null, true);
			 			});
			 			
			 		} else {
			 			// TODO
			 		}
				});
				
			};
						
			this.listVendors = function(){
			    $("#vendorName").autocomplete({
			    	minLength: 2,
			    	mustMatch : true,
					autoFocus: true,
					source: function( request, response ) {
						Uniware.Ajax.getJson("/data/lookup/vendors?name=" + encodeURIComponent(request.term), function(data) {
							response( $.map( data, function( item ) {
								return {
									label: item.name,
								}
							}));
						});
					}
			    });
			};
			
			this.getCart = function(vendor){
				Uniware.Ajax.getJson("/data/procure/getCart", function(response) {
					self.purchaseCart = response;
					self.cartQuantity = {};
					for (var vendorId in response.vendorToPurchaseCartItems) {
						var vendorPO = response.vendorToPurchaseCartItems[vendorId];
						for (var i=0;i<vendorPO.purchaseCartItems.length;i++) {
							var poItem = vendorPO.purchaseCartItems[i];
							self.cartQuantity[poItem.itemSku] = self.cartQuantity[poItem.itemSku] ? self.cartQuantity[poItem.itemSku] + poItem.quantity : poItem.quantity;
						}
					}
					self.searchBackOrders(vendor);
				});
			};
			
			
			this.search = function() {
				//verify vendor
				var vendorName = $("#vendorName").val();
				if(vendorName != "") {
					Uniware.Ajax.getJson("/data/lookup/vendors?name=" + encodeURIComponent(vendorName), function(data) {
						if(data.length > 0){
							if(vendorName.toLowerCase() == data[0].name.toLowerCase()){
								self.searchCart(data[0].id);
							}else{
								Uniware.Utils.showError("Please select a valid vendor");
							}
						}else{
							Uniware.Utils.showError("Please select a valid vendor");
						}
					});	
				} else {
					self.searchCart();
				}
			};
			
			this.searchCart = function(vendor) {
				self.getCart(vendor);				
			};
			
			this.searchBackOrders = function(vendorId, noVendors){
				var pipeline = new Uniware.pipelineData();
				$("#searching").removeClass('hidden');
				$("#backOrders").html(template('tableTemplate'));
				var req = {
					vendorId: vendorId,
					itemTypeName: $('#itemTypeName').val(),
					categoryCode: $('#category').val()					
				}
				if (noVendors && noVendors === true) {
					req.noVendors = 'true';
				}
				pipeline.requestObject = req;

				var requestData = JSON.stringify(req);
				var dtEL = $('#dataTable');
				if (Uniware.Utils.isDataTable(dtEL)) {
					var dtTable = dtEL.dataTable();
					dtTable.fnDestroy();
					dtTable = undefined;
				}
				self.table = dtEL.dataTable({
					"bServerSide" : true,
					"sAjaxSource" : "/data/procure/backorders/get",
					"bAutoWidth" : false,
					"bSort" : false,
					"bFilter" : false,
					"iDisplayLength": 250,
					"sPaginationType" : "full_numbers",
					"aoColumns" : [ {
						"sTitle" : "",
						"mDataProp" : function(aData) {
							return '<input type="checkbox"/>'
						},
						"sWidth" : "5%"
					}, {
						"sTitle" : "Item Name",
						"mDataProp": "name",
						"sWidth" : "20%" 
					}, {
						"sTitle" : "Item Sku",
						"mDataProp" : "skuCode",
						"sWidth" : "15%"
					},{ 
						"sTitle" : "Quantity Waiting",
						"mDataProp": "waitingQuantity",
						"sWidth" : "10%"
					}, {
						"sTitle" : "Quantity in Cart",
						"mDataProp": function (aData) {
							return '';
						},
						"sWidth" : "10%"
					},{ 
						"sTitle" : "Vendor",
						"mDataProp": function (aData) {
							return '';
						},
						"sWidth" : "20%"
					},{
						"sTitle" : "Quantity to Raise",
						"mDataProp": function (aData) {
							return '';
						},
						"sWidth" : "8%"
					},{
						"sTitle" : "Unit Price",
						"mDataProp": function (aData) {
							return '';
						},
						"sWidth" : "12%"
					}],

					"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
						aData.addedQuantity = self.cartQuantity[aData.skuCode];
						aData.index = iDisplayIndexFull;
						if(aData.addedQuantity == null){
							aData.addedQuantity = 0;
						}
						$(nRow).html(template("suggestionTemplate", aData));
						return nRow;
					},
					
					"fnServerData" : pipeline.fnDataTablesPipeline,
				});
				$($('#dataTable thead th')[0]).html('<input id="selectAll" type="checkbox"/>');
				$('#selectAll').change(function(){
					var state = $("input#selectAll").is(":checked");
					$("input.item-check:not(:disabled)").attr('checked', state);
				});								
				$("#dataTable").on('change', '.vendor', function(event) {
					self.changeVendor(event);
				});
				Uniware.Utils.applyHover("#dataTable");
				$("#searching").addClass('hidden');
				$("div#addToCart").click(self.addItemsToCart);
			};
			
			this.changeVendor = function(event){
				var aPos = self.table.fnGetPosition($(event.target).parent().get(0));
				var aData = self.table.fnGetData(aPos[0]);
				var rowId = aData.index;
				var vendorId = $("#vendor-"+rowId).val();
				var unitPrice = aData.vendorItemTypes[vendorId].unitPrice;
				$("#unitPrice-"+rowId).val(unitPrice);
				/* var index = $(event.target).val();
				var tr = $(event.target).parents('tr');
				$(tr[0]).find('.unitPrice').val(aData.vendorItemTypes[index].unitPrice); */
			};
			
			this.addItemsToCart = function() {
				var requestObject = {
					purchaseCartItems : []
				};
				var checkedRows = $("input.item-check:checked");
				if (checkedRows.length == 0) {
					alert("No Items selected");
					return;
				}
				checkedRows.each(function(){
					var aPos = self.table.fnGetPosition($(this).parent().get(0));
					var aData = self.table.fnGetData(aPos[0]);
					var tr = $(this).parents('tr');
					var rowId = aData.index;
					var vendorId = $("#vendor-"+rowId).val();
					var cartItem = {};
					cartItem.vendorCode = aData.vendorItemTypes[vendorId].vendorCode;
					cartItem.itemSku = aData.skuCode;
					cartItem.quantity = $("#qty-"+rowId).val();
					cartItem.unitPrice = $("#unitPrice-"+rowId).val();
			        requestObject.purchaseCartItems.push(cartItem);
				});
				
			 	Uniware.Ajax.postJson("/data/procure/itemType/addToCart", JSON.stringify(requestObject), function(response) {
			 		if(response.successful == true) {
			 			$("#backOrders").html('');
			 			$('#search').trigger('click');
			 			Uniware.Utils.addNotification('Items Added to Cart');
			 		} else {
			 			Uniware.Utils.showError(response.errors[0].description);
			 		}
				}, true);
			};
		};
		
		$(document).ready(function() {
			window.page = new Uniware.BackOrderPage();
			window.page.init();
		});
	</script>
	</tiles:putAttribute>
</tiles:insertDefinition>			
