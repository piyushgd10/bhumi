<%@ include file="/tagIncludes.jsp"%>
<tiles:insertDefinition name=".procurePage">
	<tiles:putAttribute name="title" value="Uniware - Purchase Cart" />
	<tiles:putAttribute name="rightPane">
		<div class="greybor headlable ovrhid main-box-head pageHeadBar">
			<h2 class="edithead head-textfields">Purchase Cart</h2>
			<div class="btn btn-danger rfloat10" id="removeFromCart">Remove</div>
			<div class="btn btn-info  rfloat10" id="checkout">Create Purchase Orders</div>
			<div class="clear"></div>
		</div>
		<div id="viewCartWrapper" class="mar-10-rht mar-10-lft">
			<div style="margin:15px 0px 5px 0;" class="hidden" id="selectAllWrapper">
				<div class="lfloat"><div id="selectAllCheckboxes" class="custCheckBox"></div>&nbsp;&nbsp;Select All</div>
				<div class="rfloat f15">Total Purchase Orders: <span id="numbPO"></span> &nbsp;&nbsp; Total Amount: <span id="poTotal"></span></div>
				<div class="clear"></div>
			</div>	
			<div id="purchaseCartDiv"></div>	
		</div>
	</tiles:putAttribute>
	<tiles:putAttribute name="deferredScript">
	<script id="purchaseCartTemplate" type="text/html">
		<# var length = 0;#>
		 <# var totalAmt=0; #>
		
		<# for(var vendorId in obj.vendorToPurchaseCartItems) { var vendorPurchaseCartItems = obj.vendorToPurchaseCartItems[vendorId]; length++; #>
		<div class="vendorNameRow f15">
			<div class="lfloat venorNameText">Vendor : <a href="/procure/searchVendor?legacy=1?vendorCode=<#= vendorPurchaseCartItems.vendorCode#>"><#= vendorPurchaseCartItems.vendorName#></a></div>
			<div class="rfloat"> 
				<# if (vendorPurchaseCartItems.vendorAgreements.length > 0) {#> 
					Agreement : 
					<select class="agreementId-<#= vendorPurchaseCartItems.vendorCode#>">
						<option value="">--Select an agreement--</option>
						<# for(var i=0; i<vendorPurchaseCartItems.vendorAgreements.length; i++) { var agreement = vendorPurchaseCartItems.vendorAgreements[i];#>
							<option value="<#=agreement.name#>" <#=(i == 0 ? 'selected="selected"' : '')#>><#=agreement.name#></option>
						<# } #>		
					</select>
                <# } #>
			</div>
			<div class="clear"></div>
		</div>
		<table id="dataTable" class="round_all vendorCartTable">
			<tr>
				<th class="custCheckBoxWrapper" align="left"><div class="checkBox-head-<#=vendorPurchaseCartItems.vendorCode#> custCheckBox custCheckBox-head" vendorCode="<#= vendorPurchaseCartItems.vendorCode#>"></div></th>
				<th width="30%" align="left">Item Type</th>
				<th align="left">SKU</th>
				<th align="right">Quantity</th>
				<th align="right">Unit Price</th>
				<th align="right">Total</th>
			</tr>
			
            <# var total=0; #>
            <# for(var i=0; i<vendorPurchaseCartItems.purchaseCartItems.length; i++) { var item = vendorPurchaseCartItems.purchaseCartItems[i];#>
				<tr id="<#=vendorId#>-<#=i#>">
				<td ><div class="checkBox-<#=vendorPurchaseCartItems.vendorCode#> custCheckBox itemCheckBox" vendorCode="<#=vendorPurchaseCartItems.vendorCode#>" itemSku="<#=item.itemSku#>" unitPrice="<#=item.unitPrice#>" quantity="<#=item.quantity#>"></div>&nbsp;&nbsp;<#=i+1#></td>
				<td><#=item.itemTypeName#></td>
				<td><#=item.itemSku#></td>
				<td align="right"><#=item.quantity#></td>
				<td align="right"><#=Uniware.Utils.formatPrice(item.unitPrice)#></td>
				<td align="right"><#=Uniware.Utils.formatPrice(item.quantity*item.unitPrice)#></td>
                <# total = total + item.quantity*item.unitPrice;#>
				<# totalAmt = totalAmt + item.quantity*item.unitPrice;#>
				</tr>	
			<# } #>
			<tr>
				<td colspan="6" align="right">Total Amount: <#=Uniware.Utils.formatPrice(total) #></td>
			</tr>
		</table><br/>
		<# } #>

		<# if(length == 0) { $('#selectAllWrapper').addClass('hidden'); #>
			<div id="no-data" class="f15" style="padding:25px;"><em>No Items in Cart</em></div>
		<# }else{ $('#selectAllWrapper').removeClass('hidden'); $('#numbPO').html(length); $('#poTotal').html(Uniware.Utils.formatPrice(totalAmt)); #>
			
		<#} #>		
	</script>
	<script id="purchaseOrdersTemplate" type="text/html">
		<div class="pageHeading" style="margin:20px;">Purchase Orders Generated, Waiting Approval</div>
		<table id="dataTable" class="dataTable round_all" style="width:98%;">
			<tr bgcolor="#F5F5F5">
				<td>Vendor Name</td>
				<td>Purchase Order Number</td>
				<td>Purchase Order Code</td>
			<tr>
		<# for(var i=0;i<obj.purchaseOrders.length;i++) { var po = obj.purchaseOrders[i]; #>
			<tr bgcolor="#F5F5F5">
				<td><#=po.vendorName#></td>
				<td><#=po.purchaseOrderCode#></td>
				<td><a href="/procure/poItems?legacy=1&poCode=<#=po.purchaseOrderCode#>"><#=po.purchaseOrderCode#></a></td>
			<tr>
		<# } #>
		</table>
	</script>	
	<script type="text/javascript">
		Uniware.Utils.sendForApproval = false;
		<uc:security accessResource="PROCUREMENT_SEND_FOR_APPROVAL">
			Uniware.Utils.sendForApproval = true;
		</uc:security>
		Uniware.CheckoutPurchaseCartPage = function() {
			var self = this;
			this.purchaseCart = null;
			
			this.init = function() {
				self.search();
				$("#viewCartWrapper").on('click', '.custCheckBox', function() {
					$(this).toggleClass('custCheckBox-checked');
					if ($(this).hasClass('itemCheckBox')) {
						var checkBoxName = $(this).attr('vendorCode');
						if(!$(this).hasClass('custCheckBox-checked')) {
							$('.checkBox-head-'+checkBoxName).removeClass('custCheckBox-checked');
							$('#selectAllCheckboxes').removeClass('custCheckBox-checked');
						}
					}
				});
				
				$("#viewCartWrapper").on('click', '.custCheckBox-head', function() {
					var checkBoxName = $(this).attr('vendorCode');
					if($(this).hasClass('custCheckBox-checked')) {
						$('.checkBox-'+checkBoxName).addClass('custCheckBox-checked');
					} else {
						$('.checkBox-'+checkBoxName).removeClass('custCheckBox-checked');
					}
				});
				
				$("#viewCartWrapper").on('click', '#selectAllCheckboxes', function() {
					if($(this).hasClass('custCheckBox-checked')){
						$('.custCheckBox').addClass('custCheckBox-checked');
					}else{
						$('.custCheckBox').removeClass('custCheckBox-checked');
					}
				});
				
			};
			
			this.search = function() {
				Uniware.Ajax.getJson("/data/procure/getCart", function(response) {
					self.purchaseCart = response;
					self.loadCart();
				});
			};
			
			
			this.loadCart = function(){
				var totalPOAmt = 0;
				$('#purchaseCartDiv').html(template("purchaseCartTemplate" , self.purchaseCart));
				$('#checkout').click(self.checkout);
				$('#removeFromCart').click(self.removeFromCart);
			};
			
			this.emptyCart = function() {
				var action = confirm('Are you sure?');
				if (action == false)
					return;
				Uniware.Ajax.postJson("/data/procure/cart/purge", JSON.stringify({}), function(response) {
					if (response.successful == false) {
						Uniware.Utils.showError(response.errors[0].description);
					} else {
						$('#purchaseCartDiv').html(template("purchaseCartTemplate" , {}));
					}
				}, true);
			};
			
			this.removeFromCart = function(event) {
				var action = confirm('Are you sure?');
				if (action == false)
					return;

				var requestObject = {
						removePurchaseCartItems: []
				};
				
				$(".itemCheckBox.custCheckBox-checked").each(function() {
					requestObject.removePurchaseCartItems.push({'itemSku': $(this).attr('itemSku'), 'quantity': $(this).attr('quantity'), 'vendorCode': $(this).attr('vendorCode')});
				});
				
				Uniware.Ajax.postJson("/data/procure/cart/removeItem", JSON.stringify(requestObject), function(response) {
					if (response.successful == false) {
						Uniware.Utils.showError(response.errors[0].description);
					} else {
						self.search();
					}
				}, true);
				
			};
			
			
			this.checkout = function(event) {
				var action = confirm('Are you sure?');
				if (action == false)
					return;
				var requestObject = {
						checkoutPurchaseCartItems: []
				};
				var vendorPurchaseItems = {};
				
				$(".itemCheckBox.custCheckBox-checked").each(function() {
					var vendorCode = $(this).attr('vendorCode');
					var vendorPurchaseItem = vendorPurchaseItems[vendorCode];
					if (!vendorPurchaseItem) {
						vendorPurchaseItem = {
							vendorCode : vendorCode,
							vendorAgreementName : $(".agreementId-" + vendorCode).val(),
							sendForApproval : Uniware.Utils.sendForApproval,
							checkoutPurchaseCartVendorItems: []
						};
						vendorPurchaseItems[vendorCode] = vendorPurchaseItem;
						requestObject.checkoutPurchaseCartItems.push(vendorPurchaseItem);
					}
					vendorPurchaseItem.checkoutPurchaseCartVendorItems.push({
                    	itemSku: $(this).attr('itemSku'), 
                    	quantity: $(this).attr('quantity'), 
                    	unitPrice: $(this).attr('unitPrice')
                   });
                });
				
				Uniware.Ajax.postJson("/data/procure/cart/checkout", JSON.stringify(requestObject), function(response) {
					if (response.successful == false) {
						Uniware.Utils.showError(response.errors[0].description);
					} else {
						Uniware.Utils.addNotification("Purchase Orders created successfully.");
						$('#selectAllWrapper').addClass('hidden');
						$('#purchaseCartDiv').html(template("purchaseOrdersTemplate" , response));
					}
				}, true);
				
			};
		};
						
		$(document).ready(function() {
			window.page = new Uniware.CheckoutPurchaseCartPage();
			window.page.init();
		});
	</script>
	</tiles:putAttribute>
</tiles:insertDefinition>
