<%@ include file="/tagIncludes.jsp"%>
<tiles:insertDefinition name=".wapPage">
	<tiles:putAttribute name="title" value="Uniware - WAP Home Page" />
	<sec:authentication property="principal" var="user" />
	<tiles:putAttribute name="body">
	<div style="font-size:11px;">
		<div>
			<br/><br/>
			<a href="/wap/wapGrnCreate">Add Items to GRN</a><br/>
			<a href="/wap/wapGrnQC">Reject Items in GRN</a><br/>
			<a href="/wap/wapPicking">Pick Items</a><br/>
			<a href="/wap/wapPutaway">Putaway Items</a><br/>
			<a href="/wap/wapSearchItem">Search Item</a><br/>
			<a href="/wap/wapGatePassCreate">Add Items to GatePass</a><br/>
		</div>	
	</div>
	</tiles:putAttribute>
</tiles:insertDefinition>