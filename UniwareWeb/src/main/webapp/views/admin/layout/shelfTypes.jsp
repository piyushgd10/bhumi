<%@ include file="/tagIncludes.jsp"%>
<tiles:insertDefinition name="admin.layoutPage">
	<tiles:putAttribute name="title" value="Uniware - All Shelf Types" />
	<tiles:putAttribute name="rightPane">
		<div>
			<div class="greybor headlable ovrhid main-box-head">
				<h2 class="edithead head-textfields">Shelf Types</h2>
			</div>
			<div class="ovrhid pad-15">	
				<div class=" btn btn-small btn-primary rfloat" id="create">+add shelf type</div>
			</div>
			<div id="newShelfTypeDiv" class="lb-over">
				<div class="lb-over-inner round_all">
					<div style="margin: 40px;">
					<form onsubmit="javascript : return false;">
						<div class="pageHeading lfloat">Create new shelf type</div>
						<div id="newShelfTypeDiv_close" class="link rfloat">close</div>
						<div class="clear"></div>
						<br /> <br />
						<div>
							<div class="searchLabel">Code</div>
							<input id="sTypeCode" size="15" type="text" autocomplete="off" maxlength="10" />
						</div>
						<br />
						<div>
							<div class="searchLabel">Length (in mm)</div>
							<input id="sTypeLength" size="15" type="text" autocomplete="off" maxlength="10" />
						</div>
						<br />
						<div>
							<div class="searchLabel">Width (in mm)</div>
							<input id="sTypeWidth" size="15" type="text" autocomplete="off" maxlength="10" />
						</div>
						<br />
						<div>
							<div class="searchLabel">Height (in mm)</div>
							<input id="sTypeHeight" size="15" type="text" autocomplete="off" maxlength="10" />
						</div>
						<div class="clear"></div>
						<br /> <br />
						<input type="submit" id="sTypeSubmit" class=" btn btn-small btn-primary lfloat" value="submit"/>
						<div id="error" class="errorField lfloat" style="margin-left: 20px;"></div>
						<div class="clear"></div>
						<br />
					</form>
					</div>
				</div>
			</div>
		</div>
		<div class="clear"></div>
		<div class="greybor form-edit-table-cont round_bottom pad-15">
			<table id="dataTable" class="dataTable"></table>
		</div>

	</tiles:putAttribute>
	<tiles:putAttribute name="deferredScript">
		<script type="text/javascript" src="${path.js('jquery/jquery.dataTables.min.js')}"></script>
		<script type="text/html" id="shelfTypeRow">
			<td class="shelftype-code"><#=obj.code#></td>
			<td class="shelftype-length"><#=obj.length#></td>
			<td class="shelftype-width"><#=obj.width#></td>
			<td class="shelftype-height"><#=obj.height#></td>
			<td class="shelftype-status"><#=(obj.enabled ? "Yes" : "No")#></td>
			<td class="shelftype-operations">
				<# if (obj.editable) { #>
					<div class="action btn btn-small lfloat  hidden"><#=(obj.enabled ? "Disable" : "Enable")#></div>
				<# } #>				
			</td>
		</script>
		<script type="text/javascript">
			Uniware.ShelfTypePage = function() {
				var self = this;
				this.table = null;
				
				this.cols = [ {
					"sTitle" : "Code",
					"mDataProp" : "code"
				}, {
					"sTitle" : "Length (mm)",
					"mDataProp" : "length"
				}, {
					"sTitle" : "Width (mm)",
					"mDataProp" : "width"
				}, {
					"sTitle" : "Height (mm)",
					"mDataProp" : "height"
				}, {
					"sTitle" : "Enabled",
					"mDataProp": function (aData) {
						return '';
					}
				}, {
					"sTitle" : "Actions",
					"mDataProp": function (aData) {
						return '';
					}
				} ];

				this.changeActive = function(event) {
					var aPos = self.table.fnGetPosition($(event.target).parent().get(0));
					var aData = self.table.fnGetData(aPos[0]);
					
					var requestObject = {
						'code' : aData.code,
						'enabled' : !aData.enabled
					};
					var requestData = JSON.stringify(requestObject)
					Uniware.Ajax.postJson("/data/admin/layout/shelftype/edit", requestData, function(response) {
						if (response.successful == false) {
							// should not happen TODO
						} else {
							aData.enabled = !aData.enabled;
							self.table.fnUpdate(aData, aPos[0]);
							Uniware.Utils.addNotification('Shelf Type "'+ aData.code + '" has been ' + (aData.enabled ? 'enabled' :'disabled'));
						}
					});
				};
				
				this.init = function() {
					Uniware.Ajax.getJson("/data/admin/layout/shelftypes/get", function(data) {
						if (data.elements && data.elements.length > 0) {
							self.table = $("#dataTable").dataTable({
								"sPaginationType": "full_numbers",
								"bSort": false,
								"aLengthMenu": [[25, 50, -1], [25, 50, "All"]],
								"iDisplayLength": 25,
								"aaData" : data.elements,
								"aoColumns" : self.cols,
								"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
									$(nRow).html(template("shelfTypeRow", aData));
									return nRow;
								}
							});
							$("#dataTable tbody").on('mouseover mouseout', 'tr', function(event) {
								$(this).find('div.action').toggleClass('hidden');
							});
							$("#dataTable tbody").on('click', 'div.action', function(event) {
								self.changeActive(event);
							});
						} else {
						}
					});
				};
			};
			
			$(document).ready(function() {
				$("#create").click(function(){
					Uniware.LightBox.show('#newShelfTypeDiv', function() {$("#sTypeCode").focus();});
				});
				
				
				$("#sTypeSubmit").click(function(){
					$("#error").addClass('invisible');
					var requestObject = {
						'code' : $("#sTypeCode").val(),
						'length' : $("#sTypeLength").val(),
						'width' : $("#sTypeWidth").val(),
						'height' : $("#sTypeHeight").val()
					};
					Uniware.Ajax.postJson("/data/admin/layout/shelftype/create", JSON.stringify(requestObject), function(response) {
						if (response.successful == false) {
							$("#error").html(response.errors[0].description.split('|')[0]).removeClass('invisible');
						} else {
							var sType = response.shelfTypeDTO;
							window.sTypePage.table.fnAddData(sType);
							Uniware.Utils.addNotification('Shelf Type "'+ sType.code + '" has been added');
							Uniware.LightBox.hide();
						}
					});
				});
				
				window.sTypePage = new Uniware.ShelfTypePage();
				window.sTypePage.init();
			});
		</script>
	</tiles:putAttribute>
</tiles:insertDefinition>
