<%@ include file="/tagIncludes.jsp"%>
<tiles:insertDefinition name="admin.layoutPage">
	<tiles:putAttribute name="title" value="Uniware - Search Shelf" />
	<tiles:putAttribute name="rightPane">
		<div>
			<form onsubmit="javascript : return false;">
				<div class="greybor headlable ovrhid main-box-head">
				<h2 class="edithead head-textfields">Search Shelfs</h2></div>
				<div class="greybor round_bottom main-boform-cont pad-15-top overhid">
				<div class="lfloat">
					<div class="formLabel">shelf type</div>
					<select id="shelfType" class="selectfield" style="width: 220px;">
						<option value="0" selected="selected">-- Select a Shelf Type --</option>
						<c:forEach items="${shelfTypes}" var="shelfType">
							<option value="${shelfType.code}"><c:out value="${shelfType.code}" /></option>
						</c:forEach>
					</select>
				</div>
				<div class="lfloat20">
					<div class="formLabel">section</div>
					<select id="section" class="selectfield" style="width: 220px;">
						<option value="0" selected="selected">-- Select a Section --</option>
						<c:forEach items="${sections}" var="section">
							<option value="${section.code}"><c:out value="${section.name} - ${section.code}" /></option>
						</c:forEach>
					</select>
				</div>
				<div class="lfloat20">
					<div class="formLabel">name contains</div>
					<input id="name" size="20" type="text" autocomplete="off" />
				</div>
				<div class="lfloat20">
					<div class="formLabel">&#160;</div>
					<input type="submit" class="btn btn-primary" id="search" value="search"/>
				</div>
				<div class="clear"></div>
				<div id="error" class="errorField lfloat" style="margin: 10px;"></div>
				<div id="searching" class="lfloat hidden" style="margin: 10px;">
					<img src="/img/icons/refresh-animated.gif" />
				</div>
				<div class="clear"></div>
			</div>
			</form>
		</div>
		<div id="shelfs" style="margin:20px;">
		
		</div>

	</tiles:putAttribute>
	<tiles:putAttribute name="deferredScript">
		<script id="shelfsTemplate" type="text/html">
			<#if(obj && obj.length != 0) {#>
				<div class="formLabel lfloat"><#=obj.length#> shelfs exist.</div>
				<div id="print" class="btn btn-small rfloat">print</div>
				<div class="clear"></div>
				<br/>
				<#for(var i=0;i<obj.length;i++) {#>
					<div class="barcode lfloat printBarcodeCont printThis" style="margin:10px;"><#=obj[i]#></div>
				<# } #>
			<# } else { #>
				<div class="formLabel">No shelf found</div>
			<# } #>
			
		</script>
		<script type="text/javascript">
		Uniware.SearchShelfPage = function() {
			var self = this;
			this.shelfCodes = null;
			this.init = function(){
				$('#search').click(self.search);
			};
			
			this.search = function(){
				$("#error").addClass('invisible');
				$("#searching").removeClass('hidden');
				
				var requestObject = {
					'shelfTypeCode' : $("#shelfType").val(),
					'sectionCode' : $("#section").val(),
					'codeContains' : $("#name").val()
				};
				$("#shelfs").html('');
				Uniware.Ajax.postJson("/data/admin/layout/shelfs/search", JSON.stringify(requestObject), function(response) {
					if (response.successful == false) {
						Uniware.Utils.showError(response.errors[0].description);
					} else {
						self.shelfCodes = response.shelfCodes;
						$("#shelfs").html(template("shelfsTemplate", response.shelfCodes));
						Uniware.Utils.barcode();
						$('#print').click(self.print);
						$('.printThis').click(self.printThis);
					}
					$("#searching").addClass('hidden');
				});
			};
			
			this.print = function(){
				Uniware.Utils.printIFrame('/admin/layout/shelf/print/'+ self.shelfCodes);
			};
			
			this.printThis = function(){
				var index = $('.printThis').index(this);
				Uniware.Utils.printIFrame('/admin/layout/shelf/print/'+  self.shelfCodes[index]);
			};
		};
		
		
		$(document).ready(function() {
			window.page = new Uniware.SearchShelfPage();
			window.page.init();
		});
		</script>
	</tiles:putAttribute>	
</tiles:insertDefinition>
