<%@ include file="/tagIncludes.jsp"%>
<tiles:insertDefinition name="admin.layoutPage">
	<tiles:putAttribute name="title" value="Uniware - Create Shelfs" />
	<tiles:putAttribute name="rightPane">
		<div>
			<form onsubmit="javascript : return false;">
				<div class="greybor headlable ovrhid main-box-head">
					<h2 class="edithead head-textfields">Generate Shelfs</h2>
				</div>
				<div class="greybor round_bottom main-boform-cont pad-15-top overhid">
					<div class="lfloat">
						<div class="formLabel">shelf type</div>
						<select id="shelfType" class="selectfield" style="width: 220px;">
							<option value="0" selected="selected">-- select a Shelf Type --</option>
							<c:forEach items="${shelfTypes}" var="shelfType">
								<option value="${shelfType.code}">
									<c:out value="${shelfType.code}" />
								</option>
							</c:forEach>
						</select>
					</div>
					<div class="lfloat20">
						<div class="formLabel">section</div>
						<select id="section" class="selectfield" style="width: 220px;">
							<option value="0" selected="selected">-- select a Section --</option>
							<c:forEach items="${sections}" var="section">
								<option value="${section.code}">
									<c:out value="${section.name} - ${section.code}" />
								</option>
							</c:forEach>
						</select>
					</div>
					<div class="clear"></div>
					<br />
					<div class="formLabel">shelf codes</div>
					<div id="textArea">
						<textarea id="shelfCodesValue" cols="100" rows="5"></textarea>
					</div>
					<br /> <input type="submit" class="btn btn-primary lfloat" id="generate" value="generate" />
					<div id="generating" class="lfloat hidden" style="margin: 10px;">
						<img src="/img/icons/refresh-animated.gif" />
					</div>
					<div class="clear"></div>
				</div>
			</form>
		</div>
		<div id="shelfs" style="margin: 20px;"></div>

	</tiles:putAttribute>
	<tiles:putAttribute name="deferredScript">
		<script id="shelfsTemplate" type="text/html">
			<div id="print" class=" btn rfloat">print</div>
			<div class="clear"></div>
			<br/>
			<#for(var i=0;i<obj.length;i++) {#>
				<div class="barcode lfloat printBarcodeCont printThis" style="margin:10px;"><#=obj[i]#></div>
			<# } #>
		</script>
		<script type="text/javascript">
			Uniware.CreateShelfPage = function() {
				var self = this;
				this.init = function() {
					$('#generate').click(self.generateShelfCode);
				};

				this.generateShelfCode = function() {
					$('#error').removeClass('invisible');
					var shelfCodes = $('#shelfCodesValue').val();
					var reqObject = {
						'shelfTypeCode' : $('#shelfType').val(),
						'sectionCode' : $('#section').val(),
						'shelfCodes' : shelfCodes.split('\n')
					};
					Uniware.Ajax.postJson("/data/admin/layout/shelves/predefined/create", JSON.stringify(reqObject), function(response) {
						if (response.successful == false) {
							Uniware.Utils.showError(response.errors[0].description);
						} else {
							self.shelfCodes = response.shelfCodes;
							$('#shelfs').html(template("shelfsTemplate", self.shelfCodes));
							Uniware.Utils.barcode();
							Uniware.Utils.addNotification(self.shelfCodes.length + ' shelfs has been generated');
							var form = document.createElement("form");
							form.setAttribute("method", "post");
							form.setAttribute("action", "/data/admin/layout/shelf/print");
							form.setAttribute("target","formresult");
							form.setAttribute("name", "request");
							form.setAttribute("id", "formRequest");
							var hiddenField = document.createElement("input");
							hiddenField.setAttribute("name","itemCodes");
							hiddenField.setAttribute("type","hidden");
							hiddenField.setAttribute("value",self.shelfCodes);
							form.appendChild(hiddenField);
							document.body.appendChild(form);
							form.submit();
							$('form#formRequest').remove();
							$('#print').click(self.print);
							$('.printThis').click(self.printThis);
						}
					}, true);
				};

				this.print = function() {
					var form = document.createElement("form");
					form.setAttribute("method", "post");
					form.setAttribute("action", "/data/admin/layout/shelf/print");
					form.setAttribute("target","formresult");
					form.setAttribute("name", "request");
					form.setAttribute("id", "formRequest");
					var hiddenField = document.createElement("input");
					hiddenField.setAttribute("name","itemCodes");
					hiddenField.setAttribute("type","hidden");
					hiddenField.setAttribute("value",self.shelfCodes);
					form.appendChild(hiddenField);
					document.body.appendChild(form);
					form.submit();
					$('form#formRequest').remove();
				};

				this.printThis = function() {
					var index = $('.printThis').index(this);
					var shelfCode=self.shelfCodes[index];
					var form = document.createElement("form");
					form.setAttribute("method", "post");
					form.setAttribute("action", "/data/admin/layout/shelf/print");
					form.setAttribute("target","formresult");
					form.setAttribute("name", "request");
					form.setAttribute("id", "formRequest");
					var hiddenField = document.createElement("input");
					hiddenField.setAttribute("name","itemCodes");
					hiddenField.setAttribute("type","hidden");
					hiddenField.setAttribute("value",self.shelfCodes[index]);
					form.appendChild(hiddenField);
					document.body.appendChild(form);
					form.submit();
					$('form#formRequest').remove();
				};

			};
			$(document).ready(function() {
				window.page = new Uniware.CreateShelfPage();
				window.page.init();
			});
		</script>
	</tiles:putAttribute>
</tiles:insertDefinition>
