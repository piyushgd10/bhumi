<%@include file="/tagIncludes.jsp"%>
<tiles:insertDefinition name="admin.systemPage">
 	<tiles:putAttribute name="title" value="Uniware - Import Job Type Configuraton" />
 	<tiles:putAttribute name="rightPane">
 	<div class="greybor headlable ovrhid main-box-head">
			<h2 class="edithead head-textfields">Import Job Type Configuration</h2>
		</div>
		<div class="greybor round_bottom">	
			<div style="margin: 15px;">
			<select id="importJobTypeName">
				<option value="">--select a configuration--</option>
				<c:forEach items="${importJobTypes}" var="importJobType">
					<option>${importJobType.name }</option>
				</c:forEach>
			</select>
			</div>
			<div id="importJobTypeDiv" >
			</div>
		</div>

		
 	</tiles:putAttribute>
 	<tiles:putAttribute name ="deferredScript">
 		<script type="text/javascript" src="${path.js('unifier/codemirror.js')}"></script>
		<link href="${path.css('unifier/codemirror.css')}" rel="stylesheet" type="text/css" />
 		<script type="text/html" id="importJobType">
		<form onSubmit="Javascript : return false; ">
			<div class="clear"></div>
			<div class="formLeft lfloat">Enabled</div>
			<div class="formRight lfloat">
				<input type="checkbox" id="enabled"<#=(obj.enabled ?checked='checked': '')#> /> <br />
			</div>
			
			<div class="clear"></div>
			<div class="formLeft lfloat">Import Options</div>
			<div class="formRight lfloat">
				<# for(var i=0; i<window.page.allImportOptions.length; i++){ var importOption = window.page.allImportOptions[i];#>
					<input type="checkbox" id="<#=importOption#>" class="importOption" <#=($.inArray(importOption,obj.importOptions) != -1 ?checked='checked': '')#>/> <#=importOption#><br />
				<# } #> 
			</div>

			<div class="clear"></div>
			<div class="lfloat10">
				<textarea id="importJobConfig"></textarea>
				<br />
			</div>

			<div class="clear"></div>
			<br />
			<div class="lfloat">&#160;</div>
			<div class="lfloat10">
				<input type="submit" class=" btn btn-success" id="update" value="Update" />
			</div>

			<div class="clear"></div>
			</div>
		</form>
		</script>
		<script type="text/javascript">
		Uniware.ImportJobTypePage = function() {
			var self = this;
			this.allImportOptions = ${importOptions};
			this.importJobType = null;
			this.init = function() {
				$('#importJobTypeName').change(self.getImportJobType);
			};
			
			this.getImportJobType = function() {
				var importJobTypeName = $('#importJobTypeName').val();
				if(importJobTypeName != null) { 
					Uniware.Ajax.getJson("/data/admin/system/importJobType/get?name=" + importJobTypeName ,function(response){
						self.loadImportJobType(response);
					});
				} else {
					  $('#importjobTypeDiv').html("");
				}
			};
			this.loadImportJobType = function(data){
				self.importJobType = data;
				$('#importJobTypeDiv').html(template("importJobType",data));
				var myCodeMirror = CodeMirror.fromTextArea(document.getElementById("importJobConfig"), {
					rtlMoveVisually: true,
					autoFocus: true,
					indentUnit: 4
			      });
				myCodeMirror.setValue(data.importJobConfig);
				myCodeMirror.setSize(1000,310);
				$('#update').click(function() {
					myCodeMirror.save();
					self.onUpdateSave();
				});
			};
			this.onUpdateSave = function() {
				var req = {
					name:self.importJobType.name,
					enabled:$('#enabled').is(':checked'),
					importJobConfig:$('#importJobConfig').val(),
					importOptions:[]
				};
				
				$('.importOption').each(function(){
					if($(this).is(':checked')){
						req.importOptions.push($(this).attr('id'));	
					}
				});
				
				Uniware.Ajax.postJson("/data/admin/system/importJobType/edit",JSON.stringify(req),function(response) {
					if(response.successful == true) {
						Uniware.Utils.addNotification("Import Job Type Configuration has been successfully updated");
					} else {
						Uniware.Utils.showError(response.errors[0].description);
					}
				});
			};
			
		};
		
			
		$(document).ready(function(){
			window.page = new Uniware.ImportJobTypePage();
			window.page.init();
		});
			
		</script>
 	
 	</tiles:putAttribute>
 </tiles:insertDefinition>
