<%@ include file="/tagIncludes.jsp"%>
<tiles:insertDefinition name="admin.systemPage">
	<tiles:putAttribute name="title" value="Uniware - All Alerts" />
	<tiles:putAttribute name="rightPane">
		<style>
			.dataTables_wrapper{
				border-top: 0px solid #DDDDDD;
				box-shadow: 0 0px 0px 0px #DDDDDD;
			}
			
			td input.edit{
				width:1%;
			}
		</style>
		<div>
			<div class="greybor headlable ovrhid main-box-head">
				<h2 class="edithead head-textfields">Alerts Configurations</h2>
			</div>
		</div>
		<div style="margin-top:5px; overflow: hidden; background:#f0eee8; padding:10px;">
			<div id="dataDiv" class="round_all" style="padding:5px; background:#f9f7f1">
				<table id="dataTable" width="100%" border="0" cellspacing="0" cellpadding="0" class="alert-table dataTable">
				</table>	
			</div>
		</div>
	</tiles:putAttribute>
	<tiles:putAttribute name="deferredScript">
		<script type="text/javascript" src="${path.js('jquery/jquery.dataTables.min.js')}"></script>
		<script type="text/html" id="alertRow">
		<td>
			<div class="alert-head"><#=obj.name#></div>
			<span class="alert-small"><#=obj.description#></span>
		</td>
		<# if(obj.isEditing == true) {#>
			<td>
				<select id="frequency" class="edit">
					<#for(var j=0; j<window.page.frequencies.length; j++){ var freq = window.page.frequencies[j];#>
						<option <#=obj.frequency == freq ? selected="selected": ""#>><#=freq#></option>
					<#}#>
				</select>
			</td>
			<td><input type="checkbox" id="enabled" <#=obj.enabled ? checked='checked':''#> style="margin-left:8px;"/></td>
			<td class="alertAction">
				<input type="submit" class="btn  update" value="update"></input>
				<span class="link cancel">cancel</span>
			</td>
		<#} else {#>
			<td><span class="table-editable round_all"><#=obj.frequency#></span></td>
			<td><span class="table-editable round_all"><input type="checkbox" class="edit" disabled="disabled" <#=obj.enabled ? checked='checked':''#>/></span></td>
			<td></td>
		<# } #>
		</script>
		<script type="text/javascript">
			Uniware.Alerts = function() {
				var self = this;
				this.table;
				this.alerts = ${alerts};
				this.alerts.editable=true;
				this.frequencies = ['HOURLY', 'DAILY', 'WEEKLY'];
				
				this.init = function() {
					var dtEL = $('#dataTable');
					self.table = dtEL.dataTable({
						"sPaginationType": "full_numbers",
						"bFilter": false,
						"aaData" : self.alerts,
						"aoColumns" : [ {
							"sTitle" : "",
							"mDataProp" : "name",
						}, {
							"sTitle" : "Frequency",
							"mDataProp" : "frequency"
						}, {
							"sTitle" : "Enabled",
							"mDataProp" : "enabled"
						},{
							"sTitle" : "Operations",
							"mDataProp": function (aData) {
								return "";
							}
						}],
						"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull) {
							$(nRow).html(template("alertRow", aData));
							return nRow;
						}
					});
					
					$(".alertAction").on('mouseover mouseout', function(event) {
						$(this).find('div.action').toggleClass('hidden');
					});
					
					$("#dataTable tbody").on('click', 'span.table-editable', function(event) {
						var tr = $(event.target).parents('tr');
						var aPos = self.table.fnGetPosition(tr[0]);
						var aData = self.table.fnGetData(aPos);
						if (aData.isEditing) {
							
						} else {
							if (self.aPosEditing != null && self.aPosEditing != aPos) {
								var aEditData = self.table.fnGetData(self.aPosEditing);
								aEditData.isEditing = null;
								self.table.fnUpdate(aEditData, self.aPosEditing);
							}
							self.aPosEditing = aPos;
							aData.isEditing = true;
							self.table.fnUpdate(aData, aPos);
						}
					});
					
					$("#dataTable tbody").on('click', 'span.cancel', function(event) {
						var aEditData = self.table.fnGetData(self.aPosEditing);
						aEditData.isEditing = null;
						self.table.fnUpdate(aEditData, self.aPosEditing);
					});
					
					$("#dataTable tbody").on('click', 'input.update', function(event) {
						self.update(event);
					});
					
				};
				
				this.update = function(event) {
					var tr = $(event.target).parents('tr');
					var aPos = self.table.fnGetPosition(tr[0]);
					var aData = self.table.fnGetData(aPos);
					
					var requestObject = {
						'alertId' : aData.alertId,
						'frequency': $('#frequency').val(),
						'enabled' : $('#enabled').is(':checked')
					};
					
					Uniware.Ajax.postJson("/data/admin/system/alerttype/edit", JSON.stringify(requestObject), function(response) {
						if (response.successful == false) {
							Uniware.Utils.showError(response.errors[i].description);
						} else {
							aData = response.alertTypeDTO;
							self.table.fnUpdate(aData, aPos);
							Uniware.Utils.addNotification('Alert "'+ aData.name + '" has been updated');
						}
					});
				};
				
			};
			
			$(document).ready(function() {
				window.page = new Uniware.Alerts();
				window.page.init();
			});
		</script>
	</tiles:putAttribute>
</tiles:insertDefinition>
