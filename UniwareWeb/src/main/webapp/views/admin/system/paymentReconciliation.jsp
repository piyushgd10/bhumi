<%@ include file="/tagIncludes.jsp"%>
<tiles:insertDefinition name="admin.shippingPage">
	<tiles:putAttribute name="title" value="Uniware - Payment Reconciliation" />
	<tiles:putAttribute name="rightPane">
		<div>
			<div class="greybor headlable ovrhid main-box-head">
				<h2 class="edithead head-textfields">Payment Reconciliation Settings</h2></div>
		</div>
		<div class="clear"></div>
		<div class="greybor form-edit-table-cont round_bottom pad-15">
			<form onsubmit="javascript : return false;">
				<table id="dataTable" class="dataTable"></table>
			</form>
		</div>

	</tiles:putAttribute>
	<tiles:putAttribute name="deferredScript">
		<script type="text/javascript" src="${path.js('jquery/jquery.dataTables.min.js')}"></script>
		<script type="text/html" id="reconciliationRow">
			<td class="paymentMethod"><#=obj.paymentMethodCode#></td>
				<td>
			<# if (obj.isEditing) { #>
				<input type="text" name="tolerance" value="<#=obj.tolerance#>" class="edit"></input>
				<# } else { #>
				<span class="round_all table-editable"><#=obj.tolerance#></span>
				<# } #>
			</td>
			<td><#=(obj.enabled ? "Yes" : "No")#></td>
			<td>
			<# if (obj.isEditing) { #>
				<input type="submit" class="btn  update" value="update"></input>
				<span class="link cancel">cancel</span>
			<# } else { #>
				<div class="action btn btn-small lfloat  hidden"><#=(obj.enabled ? "Disable" : "Enable")#></div>
			<# } #>
			</td>
		</script>
		<script type="text/javascript">
			Uniware.PaymentReconciliationPage = function() {
				var self = this;
				this.table = null;
				this.aPosEditing = null;
				this.paymentReconciliations = ${paymentReconciliations};
				
				this.cols = [{
					"sTitle" : "Code",
					"mDataProp" : "paymentMethodCode"
				}, {
					"sTitle" : "Tolerance",
					"mDataProp" : "tolerance"
				}, {
					"sTitle" : "Enabled",
					"mDataProp" : "enabled"
				}, {
					"sTitle" : "Actions",
					"mDataProp": function (aData) {
						return "";
					}
				} ];

				this.changeActive = function(event) {
					var aPos = self.table.fnGetPosition($(event.target).parent().get(0));
					var aData = self.table.fnGetData(aPos[0]);
					
					var requestObject = {
						'paymentMethodCode' : aData.paymentMethodCode,
						'tolerance' : aData.tolerance,
						'enabled' : !aData.enabled
					};
					Uniware.Ajax.postJson("/data/admin/system/paymentsReconciliation/edit", JSON.stringify(requestObject), function(response) {
						if (response.successful == false) {
							// should not happen TODO
						} else {
							aData.enabled = !aData.enabled;
							self.table.fnUpdate(aData, aPos[0]);
							Uniware.Utils.addNotification('Payment Reconciliation for "'+ aData.paymentMethodCode + '" has been ' + (aData.enabled ? 'enabled' :'disabled'));
						}
					});
				};
				
				this.init = function() {
					self.table = $("#dataTable").dataTable({
						"sPaginationType": "full_numbers",
						"bSort": false,
						"aLengthMenu": [[25, 50, -1], [25, 50, "All"]],
						"iDisplayLength": 25,
						"aaData" : self.paymentReconciliations,
						"aoColumns" : self.cols,
						"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
							$(nRow).html(template("reconciliationRow", aData));
							return nRow;
						}
					});
					
					$("#dataTable tbody").on('click', 'span.table-editable', function(event) {
						var tr = $(event.target).parents('tr');
						var aPos = self.table.fnGetPosition(tr[0]);
						var aData = self.table.fnGetData(aPos);
						if (aData.isEditing) {
							
						} else {
							if (self.aPosEditing != null && self.aPosEditing != aPos) {
								var aEditData = self.table.fnGetData(self.aPosEditing);
								aEditData.isEditing = null;
								self.table.fnUpdate(aEditData, self.aPosEditing);
							}
							self.aPosEditing = aPos;
							aData.isEditing = true;
							self.table.fnUpdate(aData, aPos);
						}
					});
					
					$("#dataTable tbody").on('mouseover mouseout', 'tr', function(event) {
						$(this).find('div.action').toggleClass('hidden');
					});
					
					$("#dataTable tbody").on('click', 'div.action', function(event) {
						self.changeActive(event);
					});
					
					$("#dataTable tbody").on('click', 'span.cancel', function(event) {
						var aEditData = self.table.fnGetData(self.aPosEditing);
						aEditData.isEditing = null;
						self.table.fnUpdate(aEditData, self.aPosEditing);
					});
					$("#dataTable tbody").on('click', 'input.update', function(event) {
						var tr = $(event.target).parents('tr');
						var aPos = self.table.fnGetPosition(tr[0]);
						var aData = self.table.fnGetData(aPos);
						
						var paymentMethodCode = aData.paymentMethodCode;
						var tolerance = $(tr).find('input')[0].value;
						var requestObject = {
							'paymentMethodCode' : paymentMethodCode,
							'tolerance' : tolerance
						};
						Uniware.Ajax.postJson("/data/admin/system/paymentsReconciliation/edit", JSON.stringify(requestObject), function(response) {
							if (response.successful) {
								aData.isEditing = null;
								aData.paymentMethodCode = paymentMethodCode;
								aData.tolerance = tolerance;
								self.table.fnUpdate(aData, aPos);
								Uniware.Utils.addNotification("Updated Successfully");
								Uniware.Ajax.getJson("/data/admin/system/payments/reconcile?paymentMethodCode=" + paymentMethodCode, function(){});		
							} else {
								Uniware.Utils.showError(response.errors[0].description);
							}
						});
					});
				};
			};
			
			$(document).ready(function() {
				window.page = new Uniware.PaymentReconciliationPage();
				window.page.init();
			});
		</script>
	</tiles:putAttribute>
</tiles:insertDefinition>
