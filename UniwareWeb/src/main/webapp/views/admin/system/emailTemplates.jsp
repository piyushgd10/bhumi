<%@ include file="/tagIncludes.jsp"%>
<tiles:insertDefinition name="admin.systemPage">
	<tiles:putAttribute name="title" value="Uniware - Email Template Configuration" />
	<tiles:putAttribute name="rightPane">
		<div class="greybor headlable ovrhid main-box-head">
			<h2 class="edithead head-textfields">Email Template Configuration</h2>
		</div>
		<div class="greybor round_bottom">	
			<div style="margin: 15px;">
			<select id="type">
				<option value="">--select a template--</option>
				<c:forEach items="${configuration.getConfiguration('emailConfiguration').emailTemplates }" var="emailTemplate">
					<option>${emailTemplate.type }</option>
				</c:forEach>
			</select>
			</div>
			<div id="emailTemplateDiv" >
			</div>
		</div>

	</tiles:putAttribute>
	
	<tiles:putAttribute name="deferredScript">
		<script type="text/html" id="emailTemplate">
			<form onsubmit="javascript : return false;">
				<div class="formLabel greybor round_bottom main-boform-cont pad-15-top ovrhid">
					<div class="formLeft lfloat">Enabled</div>
					<div class="formRight lfloat">	
						<input type="checkbox" id="enabled" <#=(obj.enabled ? checked='checked': '')#> />
						<br/>
					</div>
					<div class="clear"></div>

					<div class="formLeft lfloat">CC</div>
					<div class="formRight lfloat">	
						<textarea id="ccEmail" style="height: 50px; width: 600px;"><#=obj.cc#></textarea>
						<br/>
						<div style="font-style: italic; font-size: smaller;">(Enter comma seperated values)</div>
					</div>
					<div class="clear"></div>
					
					<div class="formLeft lfloat">BCC</div>
					<div class="formRight lfloat">	
						<textarea id="bccEmail" style="height: 50px; width: 600px;"><#=obj.bcc#></textarea>
						<br/>
						<div style="font-style: italic; font-size: smaller;">(Enter comma seperated values)</div>
					</div>
					<div class="clear"></div>

					<div class="formLeft lfloat">Subject</div>
					<div class="formRight lfloat">	
						<input id="subjectTemplate" style="width: 600px;" value="<#=Uniware.Utils.escapeHtml(obj.subjectTemplate)#>" />
						<br/>
					</div>
					<div class="clear"></div>

					<div class="formLeft lfloat">Body</div>
					<div class="formRight lfloat">	
						<textarea id="bodyTemplate" style="height: 100px; width: 600px;"><#=Uniware.Utils.escapeHtml(obj.bodyTemplate)#></textarea>
						<br/>
					</div>
					<div class="clear"></div>

					<br/>
					<div class="formLeft lfloat">&#160;</div>
					<div class="formRight lfloat">
						<input type="submit" class=" btn btn-success" id="update" value="Update"/>
					</div>
					<div class="clear"></div>
				</div>
			</form>
		</script>	
		<script type="text/javascript">
			Uniware.EmailTemplatePage = function(){
				var self = this;
				this.emailTemplate = null;
				this.init = function() {
					$('#type').change(self.getTemplate);
				};
			
				this.getTemplate = function(){
					var type = $('#type').val();
					if(type != ""){
						Uniware.Ajax.getJson("/data/admin/system/emailTemplate/get?type=" + type, function(response) {
							self.loadEmailTemplate(response);
						});	
					}else{
						$('#emailTemplateDiv').html("");
					}
				};
				
				this.loadEmailTemplate = function(data){
					self.emailTemplate = data;
					$('#emailTemplateDiv').html(template("emailTemplate", data));
					$("#update").click(self.saveTemplate);
				};
				
				
				this.saveTemplate = function() {
					var req = {
						'enabled' : $('#enabled').is(':checked'),
						'type' : self.emailTemplate.type,
					    'cc' : $('#ccEmail').val(),
					    'bcc' : $('#bccEmail').val(),
					    'subjectTemplate' : $('#subjectTemplate').val(),
					    'bodyTemplate' : $('#bodyTemplate').val()
					};
					Uniware.Ajax.postJson("/data/admin/system/emailTemplate/edit", JSON.stringify(req) , function(response) {
						if (response.successful == true) {
							Uniware.Utils.addNotification("Email Template has been successfully updated");
						} else {
							Uniware.Utils.showError(response.errors[0].description);
						}
					});
				};
			};

			$(document).ready(function() {
				window.page = new Uniware.EmailTemplatePage();
				window.page.init();
				window.page.getTemplate();
			});
		</script>
	</tiles:putAttribute>
</tiles:insertDefinition>	
