<%@ include file="/tagIncludes.jsp"%>
<tiles:insertDefinition name="admin.systemPage">
	<tiles:putAttribute name="title" value="Uniware - Export Job Type Configuration" />
	<tiles:putAttribute name="rightPane">
		<div class="greybor headlable ovrhid main-box-head">
			<h2 class="edithead head-textfields">Export Job Type Configuration</h2>
		</div>
		<div id="cloneExportDiv" class="lb-over">
			<div class="lb-over-inner round_all">
				<div style="margin: 40px; line-height: 30px;">
					<div class="pageHeading lfloat">Clone Export Job Type</div>
					<div id="cloneExportDiv_close" class="link rfloat20">close</div>
					<div class="clear"></div>
					<div id="clonedExportNameDiv"></div>
					<div class="clear"></div>
					<div id="error" class="invisible errorField"></div>
				</div>
			</div>
		</div>
		<div class="greybor round_bottom">	
			<div style="margin: 15px;">
			<select id="exportJobTypeName">
				<option value="">--select a configuration--</option>
				<c:forEach items="${exportJobTypes}" var="exportJobType">
					<option value="${exportJobType.name }">${exportJobType.name }</option>
				</c:forEach>
			</select>
			<div class=" btn btn-small btn-primary formRight rfloat20 cloneExport hidden" style="margin:10px 22px 0 0;">Clone Export</div>
        	<div class="clear"></div>
			</div>
			<div id="exportJobTypeDiv" >
			</div>
		</div>

	</tiles:putAttribute>
	
	<tiles:putAttribute name="deferredScript">
		<script type="text/javascript" src="${path.js('unifier/codemirror.js')}"></script>
		<link href="${path.css('unifier/codemirror.css')}" rel="stylesheet" type="text/css" />
		<script type="text/html" id="exportJobType">
			<form onsubmit="javascript : return false;">
				<div class="formLabel main-boform-cont ovrhid">
					<div class="clear"></div>
					<div class="formLeft lfloat">Enabled</div>
					<div class="formRight lfloat">	
						<input type="checkbox" id="enabled" <#= (obj.enabled ? checked='checked': '')#> />
						<br/><br/>
					</div>

					<div class="clear"></div>
					<div class="lfloat">	
						<textarea id="exportJobConfig"></textarea>
						<br/>
					</div>
					
					<div class="clear"></div>
					<br/>
					<div class="lfloat">&#160;</div>
					<div class="lfloat10">
						<input type="submit" class=" btn btn-success" id="update" value="Update"/>
					</div>

					<div class="clear"></div>
				</div>
			</form>
		</script>
		<script type="text/html" id="cloneExportTemplate">
				<div class="formLeft150 lfloat">Name</div>
				<div class="formRight lfloat"><input type="text" id="clonedExportJobTypeName" value="Copy of <#=obj.name#>" /></div>
				<div class="clear"></div>
				<div class="formLeft150 lfloat"><br/></div>
				<div><br/><input type="button" class="btn btn-primary" id="createExportClone" value="submit"/></div>
				<div class="clear"></div>
		</script>	
		<script type="text/javascript">
			Uniware.ExportJobTypePage = function(){
				var self = this;
				this.exportJobType = null;
				this.init = function() {
					$('#exportJobTypeName').change(self.getExportJobType);
					var exportJobTypeName = Uniware.Utils.getParameterByName('exportJobType');
					if(exportJobTypeName != null) {
						$("#exportJobTypeName").val(exportJobTypeName);
					}
				};
			
				this.getExportJobType = function(){
					var exportJobTypeName = $('#exportJobTypeName').val();
					if(exportJobTypeName != ""){
						$('.cloneExport').click(self.cloneExportJobType);
						Uniware.Ajax.getJson("/data/admin/system/exportJobType/get?name=" + exportJobTypeName, function(response) {
							self.loadExportJobType(response);
						});	
					}else{
						$('#exportJobTypeDiv').html("");
					}
				};
				
				this.loadExportJobType = function(data){
					self.exportJobType = data;
					if(self.exportJobType.type == 'EXPORT') {
						$('.cloneExport').removeClass('hidden');
					} else {
						$('.cloneExport').addClass('hidden');
					}
					$('#exportJobTypeDiv').html(template("exportJobType", data));
					$('#exportJobConfig').val(data.exportJobConfig);	
					var myCodeMirror = CodeMirror.fromTextArea(document.getElementById("exportJobConfig"), {
						rtlMoveVisually: true,
						autoFocus: true,
						indentUnit: 4
				      });
					myCodeMirror.setValue(data.exportJobConfig);
					myCodeMirror.setSize(1000,360);
					$("#update").click(function() {
						myCodeMirror.save();
						self.saveExportJobType();
					});
				};
				
				
				this.saveExportJobType = function(){
					var req = {
							name:self.exportJobType.name,
							enabled:$('#enabled').is(':checked'),
							exportJobConfig:$('#exportJobConfig').val()						    
						}
					Uniware.Ajax.postJson("/data/admin/system/exportJobType/edit", JSON.stringify(req) , function(response) {
						if(response.successful == true){
							Uniware.Utils.addNotification("Export Job Type Configuration has been successfully updated");
						}else{
							Uniware.Utils.showError(response.errors[0].description);
						}
					});
					
					
				};
				
				this.cloneExportJobType = function() {
					$('#clonedExportNameDiv').html(template("cloneExportTemplate", self.exportJobType));
					Uniware.LightBox.show("#cloneExportDiv");
					$('#createExportClone').click(self.createExportJobType);
				};
				
				this.createExportJobType = function() {
					var cloneExportJobTypeRequest = {
						'cloneExportJobTypeName' : $("#clonedExportJobTypeName").val(),
						'exportJobTypeName' : self.exportJobType.name
					};
					Uniware.Ajax.postJson("/data/admin/system/exportJobType/clone", JSON.stringify(cloneExportJobTypeRequest) , function(response) {
						if(response.successful == true){
							Uniware.Utils.addNotification("Export Job Type Configuration has been successfully updated");
							window.location.href = "/admin/system/exportJobType?legacy=1&exportJobType=" + $("#clonedExportJobTypeName").val();
							Uniware.LightBox.hide("#cloneExportDiv");
						} else {
							$("#error").removeClass('invisible');
							$("#error").html(response.errors[0].description);
						}
					});
				};
			};

			$(document).ready(function() {
				window.page = new Uniware.ExportJobTypePage();
				window.page.init();
				window.page.getExportJobType();
			});
		</script>
	</tiles:putAttribute>
</tiles:insertDefinition>	
