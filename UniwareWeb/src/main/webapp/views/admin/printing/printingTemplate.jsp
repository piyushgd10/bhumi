<%@ include file="/tagIncludes.jsp"%>
<tiles:insertDefinition name="admin.printingPage">
    <tiles:putAttribute name="title" value="Uniware - Print Configuration" />
    <tiles:putAttribute name="rightPane">
        <div id="mainImageDiv" class="lb-over">
            <div class="lb-over-inner round_all_b" style="width:850px;">
                <div class="lb-over-inner-head ovrhid round_top_b mar-15-bot">
                    <div class="pageHeading lfloat">Template Gallery</div>
                    <div id="mainImageDiv_close" class="link rfloat">close</div>
                </div>
                <div class="ovrhid">
                    <div class="invoice_thumbs_cont lfloat">
                        <div class="subhead">Select a Template</div>
                        <div id="sampleTemplatesDiv" class="ovry" style="height:570px"></div>
                    </div>
                    <div  id="mainImageTemplateDiv" class="rfloat20 ovrhid" style="width:650px"></div>
                </div>
            </div>
        </div>
        <div id="getObjectCodeDiv" class="lb-over">
            <div class="lb-over-inner round_all">
                <div style="margin: 40px;line-height: 30px;">
                    <div class="pageHeading lfloat" id="objectNameHeading"></div>
                    <div id="getObjectCodeDiv_close" class="link rfloat">close</div>
                    <div class="clear"></div>
                    <div style="margin:20px 0;">
                        <input type="text" id="objectCode"/>
                    </div>
                </div>
            </div>
        </div>  
        <div class="greybor headlable ovrhid main-box-head">
            <h2 class="edithead head-textfields">Printing Templates</h2>
        </div>
        <div class="greybor round_bottom">  
            <div style="margin: 15px;">
            <select id="template">
                <option value="">--select a template--</option>
                <c:forEach items="${printTemplates}" var="printTemplate">
                    <option value="${printTemplate.type}-${printTemplate.name}">${printTemplate.type} - ${printTemplate.name}</option>
                </c:forEach>
            </select>
            </div>
            <div id="templateDiv">
            </div>
        </div>

    </tiles:putAttribute>
    
    <tiles:putAttribute name="deferredScript">
    	<script type="text/javascript" src="${path.js('codemirror/velocity.all.js')}"></script>
		<link href="${path.css('codemirror/codemirror.css')}" rel="stylesheet" type="text/css" />
        <script type="text/html" id="globalTemplate">
            <# if(!obj.autoConfig) { #>
            <div class="ovrhid">    
            <div class="rfloat10 ip-browse-temp-butt" id="brs-temp">Browse Template Gallery</div>
            </div>
            <# } #>
            <div>
                <div>
                    <div class="ip-toolbar-cont lfloat">No of copies</div>
                    <select id="copies"  class="slct-copies lfloat" style="width:50px;">
                        <# for(var i=1; i<=10; i++) { #>
                            <option <#= obj.numberOfCopies == i ? selected='selected' :'' #>><#=i#></option>
                        <# } #>
                    </select>
                    <div class="ip-toolbar-cont lfloat20">Print Dialog</div>
                    <select id="printDialog"  class="slct-copies lfloat w100">
                        <# for(var i=0; i<obj.printDialogs.length; i++) { #>
                            <option <#= obj.printDialog == obj.printDialogs[i] ? selected='selected' :'' #>><#=obj.printDialogs[i]#></option>
                        <# } #>
                    </select>
                    <div class="ip-toolbar-cont lfloat20">Page Sizes</div>
                    <select id="pageSize"  class="slct-copies lfloat w100">
                        <# for(var i=0; i<obj.pageSizes.length; i++) { #>
                            <option <#= obj.pageSize == obj.pageSizes[i] ? selected='selected' :'' #>><#=obj.pageSizes[i]#></option>
                        <# } #>
                    </select>
                    <div class="ip-toolbar-cont lfloat20">Page Margins</div>
                    <input class="lfloat w100" id="pageMargins" value="<#=obj.pageMargins#>"/>

                    <div class="ip-toolbar-cont lfloat10">Landscape</div>
                    <input type="checkbox" class="lfloat" id="landscape" <#=(obj.landscape == 1 ? 'checked' : '')#>/>

                    <div class="ip-toolbar-cont lfloat">Global</div>
                    <input type="checkbox" class="lfloat" id="global" onclick="return false" <#=(obj.global == 1 ? 'checked' : '')#>/>
                    <div class="ip-toolbar-cont lfloat">Enabled</div>
                    <input type="checkbox" class="lfloat" id="enabled" onclick="return false" <#=(obj.enabled == 1 ? 'checked' : '')#>/>
                    <# if(!obj.autoConfig) { #>
                    <div id="preview" class="btn btn-small btn-info rfloat10">Preview</div>
                    <# } #>
                    <div id="save" class="btn btn-small btn-success rfloat10 ">Save</div>
                </div>
                <div class="clear"></div>
                <div class="ip-edit-field">
                    <textarea id="velocityTemplate" class="height_450 field-width"><#=Uniware.Utils.escapeHtml(obj.template)#></textarea>
                </div>
            </div>
        </script>
        <script type="text/html" id="sampleTemplates">
        <# for(var i=0; i<obj.length; i++){ var sampleTemplate = obj[i]; #>
            <div class="lfloat thumb-box">
            <div class="round_all thhumb">
                <img id="st-<#=i#>" class="thumImage" src='${path.resources("img/templates/thumb/<#=sampleTemplate.name#>.jpg")}' width="100" height="60"/><br/>
            </div>
            <div align="center"><#=sampleTemplate.name#></div>
            </div>
        <# } #>
        </script>
        <script type="text/html" id="mainImageBox">
                <div class="subhead lfloat">Preview Template</div>
                <div id="<#=obj.index#>" class=" btn btn-small btn-primary rfloat mar-15-bot select">Use Template</div>
                <div class="round_all chart ip-bigImage">           
                    <img class="sampleImage" src='${path.resources("img/templates/large/<#=obj.name#>.jpg")}' width="620"/>
                </div>
        </script>
            
        <script type="text/javascript">
            Uniware.PrintingTemplatePage = function(){
                var self = this;
                this.printDialogs = ${printDialogs};
                this.pageSizes = ${pageSizes};
                this.sampleTemplates = null;
                this.globalTemplate = null;
                this.previewUrl = null;
                
                this.init = function() {
                    $('#template').change(self.getTemplate);
                };
                
                this.getTemplate = function(){
                    var templateName = $('#template').val();
                    var type = templateName.split('-')[0];
                    var name = templateName.split('-')[1];
                    if(templateName != ""){
                        Uniware.Ajax.getJson("/data/admin/printing/template/global/get?name=" + name + '&type=' + type, function(data){
                            data.printDialogs = self.printDialogs;
                            data.pageSizes = self.pageSizes;
                            self.globalTemplate = data;
                            $('#templateDiv').html(template("globalTemplate", data));
        					var myCodeMirror = CodeMirror.fromTextArea(document.getElementById("velocityTemplate"), {
        						rtlMoveVisually: true,
        						indentUnit: 4,
        						lineNumbers: true,
        				        autofocus: true,
        				    });
        					myCodeMirror.setValue(data.template);	
        					myCodeMirror.setSize(1000,390);
                            $('#save').click(function() {
                            	myCodeMirror.save();
                            	self.saveTemplate();
                            });
                            
                            if(!data.autoConfig){
                                Uniware.Ajax.getJson("/data/admin/printing/template/samples/get?name=" + templateName, function(response) {
                                    self.sampleTemplates = response;
                                    $("#brs-temp").click(self.loadThumbTemplates);
                                });
                                $("#preview").click(function() {
                                	myCodeMirror.save();
                                	self.getObjectCode();
                                });
                            }
                        }); 
                    }else{
                        $('#templateDiv').html("");
                    }
                };
                
                this.loadThumbTemplates = function(){
                    $('#sampleTemplatesDiv').html(template("sampleTemplates", self.sampleTemplates));
                    $(".thumImage").click(self.previewThumb);
                    $("#st-0").trigger('click');
                    Uniware.LightBox.show('#mainImageDiv');
                };
                
                this.previewThumb = function(){
                    if (self.currentThumb) {
                        $("#"+self.currentThumb).parent().removeClass('activeThumb');
                    }
                    $(this).parent().addClass('activeThumb');
                    self.currentThumb = $(this).attr('id');
                    var i = this.id.split('-')[1];
                    self.sampleTemplates[i].index = i;
                    $("#mainImageTemplateDiv").html(template("mainImageBox", self.sampleTemplates[i]));
                    $(".select").click(self.loadSelectedTemplate);
                };
                
                this.loadSelectedTemplate = function(){
                    var index = this.id;
                    var globalTemplate = $.extend({}, self.globalTemplate);
                    globalTemplate.template =  self.sampleTemplates[index].template;
                    $('#templateDiv').html(template("globalTemplate", globalTemplate));
                    $('#save').click(self.saveTemplate);
                    if(!globalTemplate.autoConfig){
                        $("#preview").click(self.getObjectCode);
                        $("#brs-temp").click(self.loadThumbTemplates);
                    }
                    Uniware.LightBox.hide();
                };
                
                this.getObjectCode = function(){
                    var templateName = $('#template').val().split('-')[0];
                    if(templateName == 'INVOICE'){
                        $('#objectNameHeading').html("Enter Shipping Package Code");    
                        self.previewUrl = "/admin/printing/template/invoice/preview";
                    }else if(templateName == 'SHIPMENT_LABEL'){
                        $('#objectNameHeading').html("Enter Shipping Package Code");
                        self.previewUrl = "/admin/printing/template/shipmentLabel/preview";
                    }else if(templateName == 'SHIPPING_MANIFEST'){
                        $('#objectNameHeading').html("Enter Shipping Manifest Code");   
                        self.previewUrl = "/admin/printing/template/shippingManifest/preview";
                    }
                    
                    Uniware.LightBox.show('#getObjectCodeDiv', function() {
                        $('#objectCode').focus();
                    });
                    $('#objectCode').keyup(self.previewTemplate);
                };
                
                this.previewTemplate = function(event){
                        if(event.which == 13){
                            Uniware.LightBox.hide();
                            var form = document.createElement("form");
                            form.setAttribute("method", "post");
                            form.setAttribute("action",self.previewUrl);
                            form.setAttribute("name", "request");
                            
                            var hiddenField = document.createElement("input");              
                            hiddenField.setAttribute("name", "objectCode");
                            hiddenField.setAttribute("type", "hidden");
                            hiddenField.setAttribute("value", $(this).val());
                            form.appendChild(hiddenField);
                            
                            var hiddenField = document.createElement("input");              
                            hiddenField.setAttribute("name", "name");
                            hiddenField.setAttribute("type", "hidden");
                            hiddenField.setAttribute("value", self.globalTemplate.name);
                            form.appendChild(hiddenField);
                            
                            var hiddenField = document.createElement("input");              
                            hiddenField.setAttribute("name", "template");
                            hiddenField.setAttribute("type", "hidden");
                            hiddenField.setAttribute("value", $('#velocityTemplate').val());
                            form.appendChild(hiddenField);
                            
                            var hiddenField = document.createElement("input");              
                            hiddenField.setAttribute("name", "numberOfCopies");
                            hiddenField.setAttribute("type", "hidden");
                            hiddenField.setAttribute("value", $('#copies').val());
                            form.appendChild(hiddenField);
                            
                            var hiddenField = document.createElement("input");              
                            hiddenField.setAttribute("name", "landscape");
                            hiddenField.setAttribute("type", "hidden");
                            hiddenField.setAttribute("value", $('#landscape').is(':checked'));
                            form.appendChild(hiddenField);
                            
                            var hiddenField = document.createElement("input");              
                            hiddenField.setAttribute("name", "printDialog");
                            hiddenField.setAttribute("type", "hidden");
                            hiddenField.setAttribute("value",  $('#printDialog').val());
                            form.appendChild(hiddenField);
                            
                            var hiddenField = document.createElement("input");              
                            hiddenField.setAttribute("name", "pageSize");
                            hiddenField.setAttribute("type", "hidden");
                            hiddenField.setAttribute("value",  $('#pageSize').val());
                            form.appendChild(hiddenField);
                            
                            var hiddenField = document.createElement("input");              
                            hiddenField.setAttribute("name", "pageMargins");
                            hiddenField.setAttribute("type", "hidden");
                            hiddenField.setAttribute("value",  $('#pageMargins').val());
                            form.appendChild(hiddenField);
                            
                            // setting form target to a window named 'formresult'
                            form.setAttribute("target", "formresult");
                            document.body.appendChild(form);

                            window.open("", 'formresult');
                            form.submit();
                            $('form').remove();
                        };
                    
                };
                
                this.saveTemplate = function(){
                    var req = {
                        type: self.globalTemplate.type,
                        name: self.globalTemplate.name, 
                        template:$('#velocityTemplate').val(),
                        numberOfCopies: $('#copies').val(),
                        printDialog: $('#printDialog').val(),
                        pageSize:$('#pageSize').val(),
                        pageMargins:$('#pageMargins').val(),
                        landscape: $('#landscape').is(':checked'),
                        global: $('#global').is(':checked'),
                    }
                    Uniware.Ajax.postJson("/data/admin/printing/template/global/edit", JSON.stringify({'wsPrintTemplate' : req}), function(response) {
                        if(response.successful == true){
                            Uniware.Utils.addNotification(self.globalTemplate.name + " template has been updated");
                            $('#template').trigger('change');
                        }else{
                            Uniware.Utils.showError(response.errors[0].description);
                        }
                    });
                };
                
            };
                
            $(document).ready(function() {
                window.page = new Uniware.PrintingTemplatePage();
                window.page.init();
            });
        </script>
    </tiles:putAttribute>
</tiles:insertDefinition>   
