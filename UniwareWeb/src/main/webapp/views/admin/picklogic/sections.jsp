<%@ include file="/tagIncludes.jsp"%>
<tiles:insertDefinition name="admin.picklogicPage">
	<tiles:putAttribute name="title" value="Uniware - All Sections" />
	<tiles:putAttribute name="rightPane">
		<div class="greybor headlable ovrhid main-box-head">
			<h2 class="edithead head-textfields">Sections</h2>
		</div>
		<div class="clear"></div>
		<div class="greybor form-edit-table-cont round_bottom pad-15">
			<form onsubmit="javascript : return false;">
				<table id="dataTable" class="dataTable"></table>
			</form>
		</div>

	</tiles:putAttribute>
	<tiles:putAttribute name="deferredScript">
		<script type="text/javascript" src="${path.js('jquery/jquery.dataTables.min.js')}"></script>
		<script type="text/html" id="sectionRow">
			<td><#=obj.code#></td>
			<td>
				<# if (obj.isEditing) { #>
				<input type="text" name="name" value="<#=obj.name#>" class="edit"></input>
				<# } else { #>
				<span class="round_all <#=obj.editable ? 'table-editable' : ''#>">
					<# if (obj.name == "") { #>
						<em>Add name</em>
					<# } else { #>
						<#=obj.name#>
					<# } #>
				</span>
				<# } #>
			</td>
			<td><#=(obj.enabled ? "Yes" : "No")#></td>
			<td>
			<# if (obj.isEditing) { #>
				<input type="submit" class="btn  update" value="update"></input>
				<span class="link cancel">cancel</span>
			<# } else if (obj.editable) { #>
				<div class="action btn btn-small lfloat  hidden"><#=(obj.enabled ? "Disable" : "Enable")#></div>
			<# } #>
			</td>
		</script>
		<script type="text/javascript">
			Uniware.SectionsPage = function() {
				var self = this;
				this.table = null;
				this.aPosEditing = null;
				
				this.cols = [ {
					"sTitle" : "Code",
					"mDataProp" : "code"
				}, {
					"sTitle" : "Name",
					"mDataProp" : "name"
				}, {
					"sTitle" : "Enabled",
					"mDataProp" : "enabled"
				}, {
					"sTitle" : "Operations",
					"mDataProp": function (aData) {
						return "";
					}
				} ];

				this.changeActive = function(event) {
					var aPos = self.table.fnGetPosition($(event.target).parent().get(0));
					var aData = self.table.fnGetData(aPos[0]);
					
					var requestObject = {
						'code' : aData.code,
						'name' : aData.name,
						'enabled' : !aData.enabled
					};
					Uniware.Ajax.postJson("/data/admin/picklogic/section/edit", JSON.stringify(requestObject), function(response) {
						if (response.successful == false) {
							// should not happen TODO
						} else {
							aData.enabled = !aData.enabled;
							self.table.fnUpdate(aData, aPos[0]);
							Uniware.Utils.addNotification('Section "'+ aData.code + '" has been ' + (aData.enabled ? 'enabled' :'disabled'));
						}
					});
				};
				
				this.init = function() {
					Uniware.Ajax.getJson("/data/admin/picklogic/sections/get", function(data) {
						if (data.elements && data.elements.length > 0) {
							self.table = $("#dataTable").dataTable({
								"sPaginationType": "full_numbers",
								"bSort": false,
								"bAutoWidth": false,
								"bPaginate" : false,
								"bInfo" : false,
								"aaData" : data.elements,
								"aoColumns" : self.cols,
								"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
									$(nRow).html(template("sectionRow", aData));
									return nRow;
								}
							});
							$("#dataTable tbody").on('mouseover mouseout', 'tr', function(event) {
								$(this).find('div.action').toggleClass('hidden');
							});
							$("#dataTable tbody").on('click', 'span.table-editable', function(event) {
								var tr = $(event.target).parents('tr');
								var aPos = self.table.fnGetPosition(tr[0]);
								var aData = self.table.fnGetData(aPos);
								if (aData.isEditing) {
									
								} else {
									if (self.aPosEditing != null && self.aPosEditing != aPos) {
										var aEditData = self.table.fnGetData(self.aPosEditing);
										aEditData.isEditing = null;
										self.table.fnUpdate(aEditData, self.aPosEditing);
									}
									self.aPosEditing = aPos;
									aData.isEditing = true;
									self.table.fnUpdate(aData, aPos);
									$(tr).find('input.edit').focus();
								}
							});
							$("#dataTable tbody").on('click', 'div.action', function(event) {
								self.changeActive(event);
							});
							$("#dataTable tbody").on('click', 'span.cancel', function(event) {
								var aEditData = self.table.fnGetData(self.aPosEditing);
								aEditData.isEditing = null;
								self.table.fnUpdate(aEditData, self.aPosEditing);
							});
							$("#dataTable tbody").on('click', 'input.update', function(event) {
								var tr = $(event.target).parents('tr');
								var aPos = self.table.fnGetPosition(tr[0]);
								var aData = self.table.fnGetData(aPos);
								
								var name = $(tr).find('input').val();
								var requestObject = {
									'code' : aData.code,
									'name' : name,
									'enabled' : aData.enabled
								};
								Uniware.Ajax.postJson("/data/admin/picklogic/section/edit", JSON.stringify(requestObject), function(response) {
									if (response.successful == false) {
										// should not happen TODO
									} else {
										aData.name = name;
										aData.isEditing = null;
										self.table.fnUpdate(aData, aPos);
										Uniware.Utils.addNotification('Section "'+ aData.code + '" name has been changed to ' + aData.name);
									}
								});
							});
						} else {
						}
					});
				};
			};
			
			$(document).ready(function() {
				window.sectionsPage = new Uniware.SectionsPage();
				window.sectionsPage.init();
			});
		</script>
	</tiles:putAttribute>
</tiles:insertDefinition>
