<%@ include file="/tagIncludes.jsp"%>
<tiles:insertDefinition name="admin.picklogicPage">
	<tiles:putAttribute name="title" value="Uniware - Zone to Sections Mapping" />
	<tiles:putAttribute name="rightPane">
		<div>
			<form onsubmit="javascript : return false;">
				<div class="greybor headlable ovrhid main-box-head">
					<h2 class="edithead head-textfields">Zone&#8658;Sections</h2>
				</div>
				<div class="ovrhid pad-15">
					<div class=" btn btn-small btn-primary rfloat" id="createSection" title="create new section">+ Add Section</div>
					<div class=" btn btn-small btn-primary rfloat20" id="createZone" title="create new zone">+ Add Zone</div>
					<div class=" btn btn-small btn-primary rfloat20" id="createZoneGroup" title="create new zone gruop">+ Add Zone Group</div>
				</div>
				<div id="ltDiv" class="lb-over">
					<div class="lb-over-inner round_all">
						<div id="tempDiv" style="margin: 40px;line-height: 32px;">
						</div>
					</div>
				</div>
			</form>
			<div class="clear"></div>
			<br/>
			<div id="map"></div>
		</div>

	</tiles:putAttribute>
	<tiles:putAttribute name="deferredScript">
		<script type="text/html" id="mapping">
			<div id="actionRow" class="invisible">
				<div id="cancel" style="margin-top:5px;" class="rfloat link">cancel</div>
				<div id="save" class="rfloat20 btn btn-primary ">Save</div>
				<div class="clear"></div>
			</div>
			<table id="mapTable" class="uniTable" cellpadding="10" style="line-height:26px;">
				<tr><th align="left">Zone</th><th align="left">Type</th><th align="left">ZoneGroup</th><th align="left">Sections</th></tr>
				<# for (var id in obj.picksets) { var pickset = obj.picksets[id]; var sectionMap = pickset.currentIds; #>
					<tr style="border-bottom:1px solid #EEE;">
						<td width="200"><span class="formLabel"><#=pickset.name#></span></td>
						<td width="50"><span class="formLabel"><#=pickset.type#></span></td>
						<td width="100"><span class="formLabel"><#=pickset.pickAreaName#></span></td>
						<td>
							<# if (sectionMap && sectionMap.length > 0) { #>
								<ol id="zoneSections<#=id#>" class="sections" style="margin:5px;">
									<# for (var j=0;j<sectionMap.length;j++) { var section = obj.sections[sectionMap[j]]; #>
									<li id="section-<#=section.id#>" class="round_all"><#=(section.name == '' ? section.code : section.name)#></li>
									<# } #>
								</ol>
								<# } else { #>
									<em>No section in this zone</em>
									<# } #>
						</td>
					</tr>
					<# } #>
			</table>
		</script>
		<script type="text/html" id="sectionTemplate">
			<div class="pageHeading lfloat">Create new section</div>
			<div id="ltDiv_close" class="link rfloat">close</div>
			<div class="clear"></div>
			<br />

			<div class="formLeft150 lfloat">Code</div>
			<div class="formRight lfloat"><input id="secCode" size="15" class="ucase" autocomplete="off" maxlength="3" /></div>
			<div class="clear"></div>

			<div class="formLeft150 lfloat">Name</div>
			<div class="formRight lfloat"><input id="secName" size="15" autocomplete="off" /></div>
			<div class="clear"></div>

			<div class="formLeft150 lfloat">Zone</div>
			<div class="formRight lfloat">
				<select id="zoneType">
					<# for(var id in obj) { var pickset = obj[id]; #>
						<option value="<#=pickset.id#>" <#=pickset.editable ? '' : 'selected=selected'#>><#=pickset.name#></option>
						<# } #>
				</select>
			</div>
			<div class="clear"></div>

			<div class="clear"></div>
			<br />
			<div class="formLeft150 lfloat">&#160;</div>
			<div class="formRight lfloat"><input type="submit" id="secSubmit" class=" btn btn-small btn-primary lfloat" value="submit"/></div>
			<div class="clear"></div>

			<div id="error" class="errorField lfloat" style="margin-left: 20px;"></div>
			<div class="clear"></div>
			<br />
		</script>
		<script type="text/html" id="zoneTemplate">
			<div class="pageHeading lfloat">Create new zone</div>
			<div id="ltDiv_close" class="link rfloat">close</div>
			<div class="clear"></div>
			<br />

			<div class="formLeft150 lfloat">Name</div>
			<div class="formRight lfloat"><input id="zoneName" size="15" class="ucase" autocomplete="off" /></div>
			<div class="clear"></div>

			<div class="formLeft150 lfloat">Capacity</div>
			<div class="formRight lfloat"><input id="zoneCapacity" size="15" autocomplete="off"/></div>
			<div class="clear"></div>

			<div class="formLeft150 lfloat">Type</div>
			<div class="formRight lfloat">
				<select id="zoneType">
					<option value="STAGING">STAGING</option>
					<option value="STOCKING" selected=selected>STOCKING</option>
					<option value="NON_SELLABLE">NON SELLABLE</option>
				</select>
			</div>
			<div class="clear"></div>

			<div class="formLeft150 lfloat">ZoneGroup</div>
			<div class="formRight lfloat">
				<select id="pickAreaName">
					<# for(var id in obj.pickAreas) { var pickArea = obj.pickAreas[id]; #>
							<%--<option value="<#=pickArea.id#>" <#='selected=selected'#>><#=pickArea.name'#></option>--%>
						<option value="<#=pickArea.name#>" <#='selected=selected'#>> <#=pickArea.name#></option>
						<# } #>
				</select>
			</div>
			<div class="clear"></div>

			<br />
			<div class="formLeft150 lfloat">&#160;</div>
			<div class="formRight lfloat"><input type="submit" id="zoneSubmit" class=" btn btn-small btn-primary lfloat" value="submit"/></div>
			<div class="clear"></div>

			<div id="error" class="errorField" style="margin-left: 20px;"></div>
			<br />
		</script>
		<script type="text/html" id="zoneGroupTemplate">
			<div class="pageHeading lfloat">Create new zone group</div>
			<div id="ltDiv_close" class="link rfloat">close</div>
			<div class="clear"></div>
			<br />

			<div class="formLeft150 lfloat">Name</div>
			<div class="formRight lfloat"><input id="zoneGroupName" size="15" class="ucase" autocomplete="off" /></div>
			<div class="clear"></div>

			<br />
			<div class="formLeft150 lfloat">&#160;</div>
			<div class="formRight lfloat"><input type="submit" id="zoneGroupSubmit" class=" btn btn-small btn-primary lfloat" value="submit"/></div>
			<div class="clear"></div>

			<div id="error" class="errorField" style="margin-left: 20px;"></div>
			<br />
		</script>
		<script type="text/javascript">
            Uniware.SectionMapping = function() {
                var self = this;
                this.data = {};
                this.data.pickAreas = ${pickAreas};
                this.data.picksets = ${picksets};
                this.data.sections = ${sections};

                this.init = function() {
                    $("#createZone").click(function(){
                        $("#tempDiv").html(template("zoneTemplate", {
                            pickSets : self.data.picksets,
                            pickAreas : self.data.pickAreas
                        }));
                        Uniware.LightBox.show('#ltDiv', function() {$("#zoneName").focus();});

                        $("#zoneSubmit").click(function(){
                            $("#error").css('visibility', 'hidden');
                            var requestObject = {
                                'name' : $("#zoneName").val(),
                                'pickAreaName' : $("#pickAreaName").val(),
                                'pickSetType' : $("#zoneType").val(),
                                'capacity' : $("#zoneCapacity").val()
                            };
                            Uniware.Ajax.postJson("/data/admin/picklogic/pickset/create", JSON.stringify(requestObject), function(response) {
                                if (response.successful == false) {
                                    $("#error").html(response.errors[0].description).css('visibility', 'visible');
                                } else {
                                    var zone = response.picksetDTO;
                                    self.data.picksets[self.data.picksets.length] = zone;
                                    Uniware.Utils.addNotification('Zone "'+ zone.name + '" has been added');
                                    Uniware.LightBox.hide();
                                    self.initMap();
                                }
                            });
                        });
                    });

                    $("#createZoneGroup").click(function(){
                        $("#tempDiv").html(template("zoneGroupTemplate", self.data.pickAreas));
                        Uniware.LightBox.show('#ltDiv', function() {$("#zoneGroupName").focus();});

                        $("#zoneGroupSubmit").click(function(){
                            $("#error").css('visibility', 'hidden');
                            var requestObject = {
                                'name' : $("#zoneGroupName").val(),
                            };
                            Uniware.Ajax.postJson("/data/admin/picklogic/pickarea/create", JSON.stringify(requestObject), function(response) {
                                if (response.successful == false) {
                                    $("#error").html(response.errors[0].description).css('visibility', 'visible');
                                } else {
                                    var pickAreaName = response.pickAreaName;
                                    self.data.pickAreas[self.data.pickAreas.length] = pickAreaName;
                                    Uniware.Utils.addNotification('ZoneGroup "'+ pickAreaName + '" has been added');
                                    Uniware.LightBox.hide();
                                    self.initMap();
                                }
                            });
                        });
                    });

                    $("#createSection").click(function(){
                        $("#tempDiv").html(template("sectionTemplate", self.data.picksets));
                        Uniware.LightBox.show('#ltDiv', function() {$("#secCode").focus();});

                        $("#secSubmit").click(function(){
                            $("#error").css('visibility', 'hidden');
                            var requestObject = {
                                'code' : $("#secCode").val(),
                                'name' : $("#secName").val(),
                                'picksetId' : parseInt($("#zoneType").val())
                            };
                            Uniware.Ajax.postJson("/data/admin/picklogic/section/create", JSON.stringify(requestObject), function(response) {
                                if (response.successful == false) {
                                    $("#error").html(response.errors[0].description).css('visibility', 'visible');
                                } else {
                                    var section = response.sectionDTO;
                                    self.data.sections[section.id] = section;
                                    self.data.picksets[section.picksetId].sectionIds.push(section.id);
                                    self.initMap();
                                    Uniware.Utils.addNotification('Section "'+ section.code + '" has been added');
                                    Uniware.LightBox.hide();
                                }
                            });
                        });
                    });

                    this.initMap();
                };

                this.initMap = function() {
                    var id, pickset, section;
                    for (id in self.data.picksets) {
                        pickset = self.data.picksets[id];
                        pickset.currentIds = $.extend([], pickset.sectionIds);
                    }
                    for (id in self.data.sections) {
                        section = self.data.sections[id];
                        section.currentId = section.picksetId;
                    }
                    self.render();
                };

                this.render = function() {
                    $("#map").html(template("mapping", self.data));
                    $(".sections").selectable();

                    var objPicksets = {"name": "MOVE TO","items": {}};
                    for (id in self.data.picksets) {
                        var pickset = self.data.picksets[id];
                        objPicksets.items[pickset.id] = {"name": pickset.name};
                    }
                    $.contextMenu({
                        selector: '.ui-selected',
                        callback: function(newId, options) {
                            var newPicksetId = parseInt(newId);
                            $(".ui-selected").each(function(){
                                var sectionId = parseInt($(this).attr('id').substring($(this).attr('id').indexOf('-') + 1));
                                var section = self.data.sections[sectionId];
                                var picksetId = section.currentId;
                                if (newPicksetId != picksetId) {
                                    var pickset = self.data.picksets[picksetId];
                                    pickset.currentIds.remove(sectionId);

                                    self.data.picksets[newPicksetId].currentIds.push(sectionId);
                                    section.currentId = newPicksetId;
                                }
                            });
                            self.render();
                            $("#actionRow").removeClass('invisible');
                        },
                        items: {
                            "fold1": objPicksets
                        }
                    });

                    $("#cancel").click(function(){
                        self.initMap();
                    });
                    $("#save").click(function(){
                        var sectionMap = self.data.sections;
                        var req = {picksetSectionDTOs : []};
                        for (var id in sectionMap) {
                            var section = sectionMap[id];
                            if (section.currentId != section.picksetId) {
                                req.picksetSectionDTOs.push({'sectionId' : section.id, 'picksetId' : section.currentId});
                            }
                        }
                        Uniware.Ajax.postJson("/data/admin/picklogic/picksetsections/update", JSON.stringify(req), function(response) {
                            if (response.successful == false) {
                                // TODO
                            } else {
                                var picksetSections = response.picksetSectionDTOs;
                                var i, picksetSection, section, pickset;
                                for(var i=0;i<picksetSections.length;i++) {
                                    picksetSection = picksetSections[i];
                                    section = self.data.sections[picksetSection.sectionId];

                                    self.data.picksets[section.picksetId].sectionIds.remove(section.id);
                                    section.picksetId = picksetSection.picksetId;
                                    self.data.picksets[section.picksetId].sectionIds.push(section.id);
                                }
                                self.initMap();
                                Uniware.Utils.addNotification('Zone mappings have been saved');
                            }
                        });
                    });
                };
            };

            $(document).ready(function() {
                window.page = new Uniware.SectionMapping();
                window.page.init();
            });
		</script>
	</tiles:putAttribute>
</tiles:insertDefinition>
