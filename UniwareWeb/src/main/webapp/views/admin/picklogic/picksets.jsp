<%@ include file="/tagIncludes.jsp"%>
<tiles:insertDefinition name="admin.picklogicPage">
	<tiles:putAttribute name="title" value="Uniware - All Zones" />
	<tiles:putAttribute name="rightPane">
		<div class="greybor headlable ovrhid main-box-head">
			<h2 class="edithead head-textfields">Zones</h2>
		</div>
		<div class="clear"></div>
		<div class="greybor form-edit-table-cont round_bottom pad-15">
			<form onsubmit="javascript : return false;">
				<table id="dataTable" class="dataTable"></table>
			</form>
		</div>

	</tiles:putAttribute>
	<tiles:putAttribute name="deferredScript">
		<script type="text/javascript" src="${path.js('jquery/jquery.dataTables.min.js')}"></script>
		<script type="text/html" id="zoneRow">
			<td><span class="round_all <#=obj.editable ? 'table-editable' : ''#>"><#=obj.name#></span></td>
			<td class="zone-capacity"><span class="round_all <#=obj.editable ? 'table-editable' : ''#>"><#=obj.capacity#></span></td>
			<td class="zone-status"><#=(obj.enabled ? "Yes" : "No")#></td>
			<td class="zone-operations">
				<# if (obj.editable) { #>
					<div class="action btn btn-small lfloat  hidden"><#=(obj.enabled ? "Disable" : "Enable")#></div>
					<# } #>
			</td>
		</script>
		<script type="text/html" id="zoneRowEdit">
			<td>
				<input type="text" id="name" value="<#=obj.name#>" class="edit"></input>
			</td>
			<td class="zone-capacity">
				<input type="text" id="capacity" value="<#=obj.capacity#>" class="edit"></input>
			</td>
			<td class="zone-status"><#=(obj.enabled ? "Yes" : "No")#></td>
			<td class="zone-operations">
				<input type="submit" class="btn  update" value="update"></input>
				<span class="link cancel">cancel</span>
			</td>
		</script>
		<script type="text/javascript">
            Uniware.ZonesPage = function() {
                var self = this;
                this.table = null;
                this.aPosEditing = null;

                this.cols = [ {
                    "sTitle" : "Name",
                    "mDataProp" : "name"
                }, {
                    "sTitle" : "Capacity",
                    "mDataProp" : "capacity"
                },{
                    "sTitle" : "Enabled",
                    "mDataProp" : "enabled"
                }, {
                    "sTitle" : "Operations",
                    "mDataProp": function (aData) {
                        return "";
                    }
                } ];

                this.changeActive = function(event) {
                    var aPos = self.table.fnGetPosition($(event.target).parent().get(0));
                    var aData = self.table.fnGetData(aPos[0]);

                    var requestObject = {
                        'id' : aData.id,
                        'name' : aData.name,
                        'capacity' : aData.capacity,
                        'enabled' : !aData.enabled
                    };
                    var requestData = JSON.stringify(requestObject)
                    Uniware.Ajax.postJson("/data/admin/picklogic/pickset/edit", requestData, function(response) {
                        if (response.successful == false) {

                        } else {
                            aData.enabled = !aData.enabled;
                            self.table.fnUpdate(aData, aPos[0]);
                            Uniware.Utils.addNotification('Zone "'+ aData.name + '" has been ' + (aData.enabled ? 'enabled' :'disabled'));
                        }
                    });
                };

                this.init = function() {
                    var requestObject = {
                    };
                    var requestData = JSON.stringify(requestObject)
                    Uniware.Ajax.postJson("/data/admin/picklogic/picksets/get", requestData, function(data) {
                        if (data.picksets && data.picksets.length > 0) {
                            self.table = $("#dataTable").dataTable({
                                "bPaginate" : false,
                                "aaData" : data.picksets,
                                "aoColumns" : self.cols,
                                "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                                    $(nRow).html(template(aData.isEditing ? "zoneRowEdit" : "zoneRow", aData));
                                    return nRow;
                                }
                            });
                            $("#dataTable tbody").on('mouseover mouseout', 'tr', function(event) {
                                $(this).find('div.action').toggleClass('hidden');
                            });
                            $("#dataTable tbody").on('click', 'span.table-editable', function(event) {
                                var tr = $(event.target).parents('tr');
                                var aPos = self.table.fnGetPosition(tr[0]);
                                var aData = self.table.fnGetData(aPos);
                                if (aData.isEditing) {

                                } else {
                                    if (self.aPosEditing != null && self.aPosEditing != aPos) {
                                        var aEditData = self.table.fnGetData(self.aPosEditing);
                                        aEditData.isEditing = null;
                                        self.table.fnUpdate(aEditData, self.aPosEditing);
                                    }
                                    self.aPosEditing = aPos;
                                    aData.isEditing = true;
                                    self.table.fnUpdate(aData, aPos);
                                    $(tr).find('input.edit').focus();
                                }
                            });
                            $("#dataTable tbody").on('click', 'div.action', function(event) {
                                self.changeActive(event);
                            });
                            $("#dataTable tbody").on('click', 'span.cancel', function(event) {
                                var aEditData = self.table.fnGetData(self.aPosEditing);
                                aEditData.isEditing = null;
                                self.table.fnUpdate(aEditData, self.aPosEditing);
                            });
                            $("#dataTable tbody").on('click', 'input.update', function(event) {
                                var tr = $(event.target).parents('tr');
                                var aPos = self.table.fnGetPosition(tr[0]);
                                var aData = self.table.fnGetData(aPos);

                                var name = $('input#name').val();
                                var capacity = $('input#capacity').val();
                                var requestObject = {
                                    'id' : aData.id,
                                    'name' : name,
                                    'capacity' : capacity,
                                    'enabled' : aData.enabled
                                };
                                Uniware.Ajax.postJson("/data/admin/picklogic/pickset/edit", JSON.stringify(requestObject), function(response) {
                                    if (response.successful == false) {
                                        Uniware.Utils.showError(response.errors[0].description);
                                    } else {
                                        aData.isEditing = null;
                                        Uniware.Utils.copyProperties(response.picksetDTO, aData);
                                        self.table.fnUpdate(aData, aPos);
                                        Uniware.Utils.addNotification('\"' + aData.name + '" Zone details has been saved');
                                    }
                                });
                            });
                        } else {
                        }
                    });
                };
            };

            $(document).ready(function() {
                $("#create").click(function(){
                    Uniware.LightBox.show('#newZoneDiv', function() {$("#zoneName").focus();});
                });


                $("#secSubmit").click(function(){
                    var name = $("#zoneName").val();
                    var capacity = $("#zoneCapacity").val();
                    if (name == '') {
                        $("#error").html('name field is empty').css('visibility', 'visible');
                        return;
                    }
                    if (capacity == '') {
                        $("#error").html('capacity field is empty').css('visibility', 'visible');
                        return;
                    }

                    $("#error").css('visibility', 'hidden');

                    var requestObject = {
                        'name' : name,
                        'capacity' : capacity
                    };
                    Uniware.Ajax.postJson("/data/admin/picklogic/pickset/create", JSON.stringify(requestObject), function(response) {
                        if (response.successful == false) {
                            $("#error").html(response.errors[0].description).css('visibility', 'visible');
                        } else {
                            var zone = response.picksetDTO;
                            window.ZonesPage.table.fnAddData(zone);
                            Uniware.Utils.addNotification('Zone "'+ zone.name + '" has been added');
                            Uniware.LightBox.hide();
                        }
                    });
                });

                window.ZonesPage = new Uniware.ZonesPage();
                window.ZonesPage.init();
            });
		</script>
	</tiles:putAttribute>
</tiles:insertDefinition>
