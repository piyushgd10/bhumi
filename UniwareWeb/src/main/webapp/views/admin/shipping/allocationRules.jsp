<%@ include file="/tagIncludes.jsp"%>
<tiles:insertDefinition name="admin.shippingPage">
	<tiles:putAttribute name="title" value="Uniware -allocation Rules" />
	<tiles:putAttribute name="rightPane">
		<div class="greybor headlable ovrhid main-box-head">
			<h2 class="edithead head-textfields">Allocation Rules</h2>
		</div>
		<div class="ovrhid pad-15">
			<div class="lfloat"><em class="f12">Please Drag &amp; Drop to change the preference</em></div>	
			<div class="btn btn-small btn-primary rfloat" id="create">+ Add Allocation Rules</div>
		</div>
		<div class="greybor form-edit-table-cont round_bottom pad-15">
			<div id="allocationData"></div>
		</div>
		<div id="newAllocationRuleDiv" class="lb-over">
			<div class="lb-over-inner round_all">
				<div style="margin: 40px;">
				<form onsubmit="javascript : return false;">
					<div class="pageHeading lfloat">Allocation rule</div>
					<div id="newAllocationRuleDiv_close" class="link rfloat">close</div>
					<div class="clear"></div>
					<div style="margin:20px 0;" id="allocationFormCont">
						
					</div>	
				</form>
				</div>
			</div>
		</div>
	</tiles:putAttribute>
	<tiles:putAttribute name="deferredScript">
		<script type="text/html" id="allocationForm">
			<div class="searchLabel formLeft150 lfloat">Name</div>
			<div class="searchLabel formRight lfloat">
				<# if (obj.id != null) { #>
				<#=obj.name#>
				<# } else { #>
				<input id="allocationName" size="15"  autocomplete="off" class="w200" value="<#=obj.name#>"/>
				<# } #>
			</div>
			<div class="clear"></div>
			<div class="searchLabel formLeft150 lfloat">Condition Expression</div>
			<div class="formRight lfloat"><textarea id="allocationConditionExpression" size="15"  autocomplete="off" class="w200"><#=Uniware.Utils.escapeHtml(obj.conditionExpressionText)#></textarea></div>
			<div class="clear"></div>
			<div class="searchLabel formLeft150 lfloat">Allocation Criteria</div>
			<div class="formRight lfloat">
				<select id="allocateCriteria" class="w200">
					<option value="">Select One</option>
					<option value="ALLOCATE">Allocate</option>
					<option value="NOT_ALLOCATE">Not allocate</option>
					<option value="FORCE_ALLOCATE">Force allocate</option>
				</select>
			</div>
			<div class="clear"></div>
			<div class="searchLabel formLeft150 lfloat">Shipping Provider</div>
			<div class="formRight lfloat">
				<select id="provider" class="w200">
					<option value="0">Select One</option>
					<#for(var i=0; i<obj.shippingProviders.length; i++) { var provider = obj.shippingProviders[i];#>
						<option <#= obj.shippingProviderId == provider.id ? selected="selected" : ""#> value="<#=provider.id#>"><#=provider.name#></option>
					<# } #>
				</select>
			</div>
			<div class="clear"></div>
			<div class="searchLabel formLeft150 lfloat">Enable</div>
			<div class="formRight lfloat">
				<input id="enabled" type="checkbox" <#= obj.enabled == true ? checked="checked" : ""#>/>
			</div>
			
			<div class="clear"></div>
			<br />
			<div class="formLeft150 lfloat">&#160;</div>
			<div class="formRight lfloat">
				<input type="submit" id="<#= obj.id != null ? "allocationRuleEdit" : "allocationRuleSubmit"#>" class=" btn btn-small btn-primary lfloat" value="submit"/>
			</div>
			<div class="clear"></div>
			<br />
			<div id="error" class="errorField lfloat" style="margin-left: 20px;"></div>
			<div class="clear"></div>
		</script>
		<script type="text/html" id="allocationDataTemplate">
			<table class="uniTable greybor" border="0" cellspacing="0" cellpadding="0" style="font-size:13px;" id="allocationDataTable">
				<tr class="bold ui-state-disabled" style="opacity:1">
					<th><strong>Name</strong></th>
					<th width="30%"><strong>Condition Expression</strong></th>
					<th><strong>Allocation Criteria</strong></th>
					<th><strong>Shipping Provider </strong></th>
					<th><strong>Preference </strong></th>
					<th><strong>Action</strong></th>
				</tr>
				<#if(obj.length == 0) { #>
					<tr>
						<td colspan="6"><div  style="margin:15px;"><em class="f15">No allocation rules</em></div></td>
					</tr>
				<# } else { #>
					<# for ( var i=0; i<obj.length; i++) { var allocationRule = obj[i]; #>
						<tr style="cursor:pointer" id="<#=allocationRule.name#>" class="rule">
							<td class="first-col"><#=allocationRule.name#></td>
							<td style="word-wrap:break-word;"><#=allocationRule.conditionExpressionText#></td>
							<td><#=allocationRule.allocationCriteria#></td>
							<td>
								<# for (var j=0; j<obj.shippingProviders.length; j++) { if (allocationRule.shippingProviderId == obj.shippingProviders[j].id) { #>
									<#=obj.shippingProviders[j].name#>
								<# }} #>
							</td>
							<td><#=(allocationRule.preference)#></td>
							<td><div id="<#=i#>" class="btn btn-mini lfloat editAction">Edit</div><div id="remove-<#=i#>" class="btn btn-mini btn-danger lfloat10 deleteAction">Delete</div></td>
						</tr>
					<# } #>
				<# } #>
			</table>
		</script>
		<script type="text/javascript">
		
		Uniware.AllocationRules = function() {
			var self = this;
			this.allocationRules = ${allocationRules};
			this.shippingProviders = ${providers};
			this.editingPos;
		
			this.init = function() {
				self.allocationRules.shippingProviders = self.shippingProviders;
				self.loadAllocationTable();
				$("#create").click(function(){
					$("#allocationFormCont").html(template("allocationForm", self.allocationRules));
					Uniware.LightBox.show('#newAllocationRuleDiv');
					$("#allocationRuleSubmit").click(self.allocationFormSubmit);
				});
			}
			
			this.loadAllocationTable = function(){
				$("#allocationData").html(template("allocationDataTemplate", self.allocationRules));
				$(".editAction").click(self.allocationRulesEdit);		
				$(".deleteAction").click(self.allocationRulesDelete);
				$("#allocationDataTable").sortable({ items: "tr:not(.ui-state-disabled)", stop: self.reorderAllocationDataRows });
			}
			
			this.allocationFormSubmit = function(){
				var maxPreference = 0;
				for (i=0; i<self.allocationRules.length; i++){
					maxPreference = maxPreference < self.allocationRules[i].preference ? self.allocationRules[i].preference : maxPreference;
				}
				$("#error").css('visibility', 'hidden');			
				var wsRequestObject = {
					'name' : $("#allocationName").val(),
					'conditionExpressionText' : $("#allocationConditionExpression").val(),
					'allocationCriteria' : $("#allocateCriteria").val(),
					'shippingProviderId' : $("#provider").val(),
					'enabled' : $('#enabled').is(':checked'),
					'preference' : (maxPreference + 1)
				};
				Uniware.Ajax.postJson("/data/admin/shipping/provider/allocationRule/create", JSON.stringify({'wsProviderAllocationRule':wsRequestObject}), function(response) {
					if (response.successful == false) {
						$("#error").html(response.errors[0].description).css('visibility', 'visible');
					} else {
						var allocationRule = response.providerAllocationRuleDTO;
						self.allocationRules.push(allocationRule);
						self.loadAllocationTable();
						Uniware.Utils.addNotification('Allocation Rule "'+ allocationRule.name + '" has been added');
						Uniware.LightBox.hide();
					}
				});
			}
			
			this.allocationRulesEdit = function(){
				self.editingPos = $(this).attr('id');
				var allocationRule = self.allocationRules[self.editingPos];
				allocationRule.shippingProviders =self.shippingProviders;
				$("#allocationFormCont").html(template("allocationForm", allocationRule));
				Uniware.LightBox.show('#newAllocationRuleDiv');
				$("#allocateCriteria").val(self.allocationRules[self.editingPos].allocationCriteria);
				$("#allocationRuleEdit").click(self.allocationFormEdit);
			}
			
			this.allocationFormEdit = function(){
				$("#error").css('visibility', 'hidden');			
				var wsRequestObject = {
					'name' : self.allocationRules[self.editingPos].name,
					'conditionExpressionText' : $("#allocationConditionExpression").val(),
					'allocationCriteria' : $("#allocateCriteria").val(),
					'shippingProviderId' : $("#provider").val(),
					'preference' :self.allocationRules[self.editingPos].preference,
					'enabled' : $('#enabled').is(':checked')
				};
				Uniware.Ajax.postJson("/data/admin/shipping/provider/allocationRule/edit", JSON.stringify({'wsProviderAllocationRule':wsRequestObject}), function(response) {
					if (response.successful == false) {
						$("#error").html(response.errors[0].description).css('visibility', 'visible');
					} else {
						var allocationRule = response.providerAllocationRuleDTO;
						self.allocationRules[self.editingPos] = allocationRule;
						self.loadAllocationTable();
						Uniware.Utils.addNotification('Allocation Rule "'+ allocationRule.name + '" has been updated');
						Uniware.LightBox.hide();
					}
				});
			}
			
			this.allocationRulesDelete = function(){
				self.editingPos = $(this).attr('id').split('-')[1];
				var allocationRule = self.allocationRules[self.editingPos].name;
				var req = {
					'providerAllocationRuleName' : allocationRule
				};
				if(confirm('Are you sure?')){
					Uniware.Ajax.postJson("/data/admin/shipping/provider/allocationRule/delete", JSON.stringify(req), function(response) {
						if (response.successful == false) {
							$("#error").html(response.errors[0].description).css('visibility', 'visible');
						} else {
							self.allocationRules.splice(self.editingPos,1);	
							self.loadAllocationTable();
							Uniware.Utils.addNotification('Allocation Rule "'+ allocationRule + '" has been removed');
						}
					});
				}
			};
			
			this.reorderAllocationDataRows = function(){
				var req = {};
				req.allocationRuleNameToPreferenceMap = {};
				var sequence = 0;
				$(this).find('tr.rule').each(function(){
					req.allocationRuleNameToPreferenceMap[$(this).attr('id')] = ++sequence;
				});
				Uniware.Ajax.postJson('/data/admin/shipping/provider/allocationRules/reorder', JSON.stringify(req), function(response) {
					if(response.successful){
						window.location.href = window.location.href;
					} else {
						Uniware.Utils.showError(response.errors[0].description);
					}
				});
			};
		};
		
		$(document).ready(function() {
			window.page = new Uniware.AllocationRules();
			window.page.init();
		});	
		</script>		
	</tiles:putAttribute>
</tiles:insertDefinition>
