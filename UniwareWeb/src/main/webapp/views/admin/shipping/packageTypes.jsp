<%@ include file="/tagIncludes.jsp"%>
<tiles:insertDefinition name="admin.shippingPage">
	<tiles:putAttribute name="title" value="Uniware - Shipping Package Types" />
	<tiles:putAttribute name="rightPane">
		<div>
			<div class="greybor headlable ovrhid main-box-head">
				<h2 class="edithead head-textfields">Shipping Package Types</h2>
			</div>
			<div class="ovrhid pad-15">	
				<div class=" btn btn-small btn-primary rfloat" id="create">+add package type</div>
			</div>
			<div id="newShippingPackageDiv" class="lb-over">
				<div class="lb-over-inner round_all">
					<div style="margin: 40px;">
					<form onsubmit="javascript : return false;">
						<div class="pageHeading lfloat">Create new package type</div>
						<div id="newShippingPackageDiv_close" class="link rfloat">close</div>
						<div class="clear"></div>
						<div style="margin:20px 0;">
							<div class="searchLabel formLeft150 lfloat">Code</div>
							<div class="formRight lfloat"><input id="sPackageCode" size="15"  type="text" autocomplete="off" maxlength="10" /></div>
							<div class="clear"></div>
							<div class="searchLabel formLeft150 lfloat">Box Length (in mm)</div>
							<div class="formRight lfloat"><input id="sPackageLength" size="15"  type="text" autocomplete="off" maxlength="10" /></div>
							<div class="clear"></div>
							<div class="searchLabel formLeft150 lfloat">Box Width (in mm)</div>
							<div class="formRight lfloat"><input id="sPackageWidth" size="15"  type="text" autocomplete="off" maxlength="10" /></div>
							<div class="clear"></div>
							<div class="searchLabel formLeft150 lfloat">Box Height (in mm)</div>
							<div class="formRight lfloat"><input id="sPackageHeight" size="15"  type="text" autocomplete="off" maxlength="10" /></div>
							<div class="clear"></div>
							<div class="searchLabel formLeft150 lfloat">Box Weight (in gm)</div>
							<div class="formRight lfloat"><input id="sPackageWeight" size="15" type="text" autocomplete="off" maxlength="10" /></div>
							<div class="clear"></div>
							<div class="searchLabel formLeft150 lfloat">Packing Cost</div>
							<div class="formRight lfloat"><input id="sPackingCost" size="15" type="text" autocomplete="off" maxlength="10" /></div>
							<div class="clear"></div>
							<br />
							<div class="formLeft150 lfloat">&#160;</div>
							<div class="formRight lfloat">
								<input type="submit" id="sPackageSubmit" class=" btn btn-small btn-primary lfloat" value="submit"/>
							</div>
							<div class="clear"></div>
						</div>	
						<br />
						<div id="error" class="errorField lfloat" style="margin-left: 20px;"></div>
						<div class="clear"></div>
					</form>
					</div>
				</div>
			</div>
		</div>
		<div class="clear"></div>
		<div class="greybor form-edit-table-cont round_bottom pad-15">
			<form onsubmit="javascript : return false;">
				<table id="dataTable" class="dataTable"></table>
			</form>
		</div>

	</tiles:putAttribute>
	<tiles:putAttribute name="deferredScript">
		<script type="text/javascript" src="${path.js('jquery/jquery.dataTables.min.js')}"></script>
		<script type="text/html" id="packageRow">
			<td class="package-code"><#=obj.code#></td>
			<td><span class="round_all table-editable"><#=obj.boxLength#></span></td>
			<td><span class="round_all table-editable"><#=obj.boxWidth#></span></td>
			<td><span class="round_all table-editable"><#=obj.boxHeight#></span></td>
			<td><span class="round_all table-editable"><#=obj.boxWeight#></span></td>
			<td><span class="round_all table-editable"><#=obj.packingCost#></span></td>
			<td class="package-status">			
				<# if(obj.enabled) { #>
					<img src="/img/icons/tick.png"/></td>
				<# } else { #>
					<img src="/img/icons/hold.png"/>
				<# } #>
			</td>
			<td class="package-operations">
			<# if (obj.editable) { #>
				<div class="action btn btn-small lfloat  hidden"><#=(obj.enabled ? "Disable" : "Enable")#></div>
			<# } #>
			</td>
		</script>
		<script type="text/html" id="packageRowEdit">
			<td class="package-code"><#=obj.code#></td>
			<td><input type="text" id="boxLength" value="<#=obj.boxLength#>" class="edit"></input></td>
			<td><input type="text" id="boxWidth" value="<#=obj.boxWidth#>" class="edit"></input></td>
			<td><input type="text" id="boxHeight" value="<#=obj.boxHeight#>" class="edit"></input></td>
			<td><input type="text" id="boxWeight" value="<#=obj.boxWeight#>" class="edit"></input></td>
			<td><input type="text" id="packingCost" value="<#=obj.packingCost#>" class="edit"></input></td>
			<td class="package-status">			
				<# if(obj.enabled) { #>
					<img src="/img/icons/tick.png"/></td>
				<# } else { #>
					<img src="/img/icons/hold.png"/>
				<# } #>
			</td>
			<td class="section-operations">
				<input type="submit" class="btn btn-small update" value="update"></input>
				<span class="link cancel">cancel</span>
			</td>
		</script>
		<script type="text/javascript">
			Uniware.ShippingPackageTypePage = function() {
				var self = this;
				this.table = null;
				this.aPosEditing = null;
				
				this.cols = [ {
					"sTitle" : "Code",
					"mDataProp" : "code"
				}, {
					"sTitle" : "Length (mm)",
					"mDataProp" : "boxLength"
				}, {
					"sTitle" : "Width (mm)",
					"mDataProp" : "boxWidth"
				}, {
					"sTitle" : "Height (mm)",
					"mDataProp" : "boxHeight"
				}, {
					"sTitle" : "Weight (gm)",
					"mDataProp" : "boxWeight"
				}, {
					"sTitle" : "Packing Cost",
					"mDataProp" : "packingCost"
				}, {
					"sTitle" : "Enabled",
					"mDataProp": function (aData) {
						return "";
					}
				}, {
					"sTitle" : "Actions",
					"mDataProp": function (aData) {
						return "";
					}
				} ];

				this.changeActive = function(event) {
					var aPos = self.table.fnGetPosition($(event.target).parent().get(0));
					var aData = self.table.fnGetData(aPos[0]);
					
					var requestObject = {
						'code' : aData.code,
						'boxLength' : aData.boxLength,
						'boxWidth' : aData.boxWidth,
						'boxHeight' : aData.boxHeight,
						'boxWeight' : aData.boxWeight,
						'packingCost' : aData.packingCost,
						'enabled' : !aData.enabled
					};
					Uniware.Ajax.postJson("/data/admin/shipping/spackagetype/edit", JSON.stringify(requestObject), function(response) {
						if (response.successful == false) {
							Uniware.Utils.showError(response.errors[0].description);
						} else {
							for(var key in response.shippingPackageTypeDTO) {
								aData[key] = response.shippingPackageTypeDTO[key];
							}
							self.table.fnUpdate(aData, aPos[0]);
							Uniware.Utils.addNotification('Shipping Package Type "'+ aData.code + '" has been ' + (aData.enabled ? 'enabled' :'disabled'));
						}
					});
				};
				
				this.init = function() {
					Uniware.Ajax.getJson("/data/admin/shipping/spackagetypes/get", function(data) {
						if (data.elements && data.elements.length > 0) {
							self.table = $("#dataTable").dataTable({
								"sPaginationType": "full_numbers",
								"bSort": false,
								"aLengthMenu": [[25, 50, -1], [25, 50, "All"]],
								"iDisplayLength": 25,
								"aaData" : data.elements,
								"aoColumns" : self.cols,
								"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
									$(nRow).html(template(aData.isEditing ? "packageRowEdit" : "packageRow", aData));
									if (!aData.enabled) {
										$(nRow).addClass('disabled');
									} else {
										$(nRow).removeClass('disabled');
									}
									return nRow;
								}
							});
							$("#dataTable tbody").on('mouseover mouseout', 'tr', function(event) {
								$(this).find('div.action').toggleClass('hidden');
							});
							$("#dataTable tbody").on('click', 'div.action', function(event) {
								self.changeActive(event);
							});
							$("#dataTable tbody").on('click', 'span.table-editable', function(event) {
								var tr = $(event.target).parents('tr');
								var aPos = self.table.fnGetPosition(tr[0]);
								var aData = self.table.fnGetData(aPos);
								if (aData.isEditing) {
									
								} else {
									if (self.aPosEditing != null && self.aPosEditing != aPos) {
										var aEditData = self.table.fnGetData(self.aPosEditing);
										aEditData.isEditing = null;
										self.table.fnUpdate(aEditData, self.aPosEditing);
									}
									self.aPosEditing = aPos;
									aData.isEditing = true;
									self.table.fnUpdate(aData, aPos);
									$(tr).find('input.edit').focus();
								}
							});
							
							$("#dataTable tbody").on('click', 'span.cancel', function(event) {
								var aEditData = self.table.fnGetData(self.aPosEditing);
								aEditData.isEditing = null;
								self.table.fnUpdate(aEditData, self.aPosEditing);
							});
							
							$("#dataTable tbody").on('click', 'input.update', function(event) {
								var tr = $(event.target).parents('tr');
								var aPos = self.table.fnGetPosition(tr[0]);
								var aData = self.table.fnGetData(aPos);
								
								var requestObject = {
									'code' : aData.code,
									'enabled' : aData.enabled,
									'boxLength' : parseInt($("#boxLength").val()),
									'boxWidth' : parseInt($("#boxWidth").val()),
									'boxHeight' : parseInt($("#boxHeight").val()),
									'boxWeight' : parseInt($("#boxWeight").val()),
									'packingCost': $("#packingCost").val()
								};
								Uniware.Ajax.postJson("/data/admin/shipping/spackagetype/edit", JSON.stringify(requestObject), function(response) {
									if (response.successful == false) {
										Uniware.Utils.showError(response.errors[0].description);
									} else {
										aData.isEditing = null;
										for(var key in response.shippingPackageTypeDTO) {
											aData[key] = response.shippingPackageTypeDTO[key];
										}
										self.table.fnUpdate(aData, aPos);
										Uniware.Utils.addNotification('Box type details has been updated');
									}
								});
							});
						} else {
						}
					});
				};
			};
			
			$(document).ready(function() {
				$("#create").click(function(){
					Uniware.LightBox.show('#newShippingPackageDiv', function() {$("#sPackageCode").focus();});
				});
				
				$("#sPackageSubmit").click(function(){
					$("#error").css('visibility', 'hidden');			
					var requestObject = {
						'code' : $("#sPackageCode").val(),
						'boxLength' : $("#sPackageLength").val(),
						'boxWidth' : $("#sPackageWidth").val(),
						'boxHeight' : $("#sPackageHeight").val(),
						'boxWeight' : $("#sPackageWeight").val(),
						'packingCost' :$("#sPackingCost").val()
					};
					Uniware.Ajax.postJson("/data/admin/shipping/spackagetype/create", JSON.stringify(requestObject), function(response) {
						if (response.successful == false) {
							$("#error").html(response.errors[0].description).css('visibility', 'visible');
						} else {
							var sPackage = response.shippingPackageTypeDTO;
							window.page.table.fnAddData(sPackage);
							Uniware.Utils.addNotification('Shipping Package Type "'+ sPackage.code + '" has been added');
							Uniware.LightBox.hide();
						}
					});
				});
				
				window.page = new Uniware.ShippingPackageTypePage();
				window.page.init();
			});
		</script>
	</tiles:putAttribute>
</tiles:insertDefinition>
