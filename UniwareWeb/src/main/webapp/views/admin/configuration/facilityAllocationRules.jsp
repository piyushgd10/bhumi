<%@ include file="/tagIncludes.jsp"%>
<tiles:insertDefinition name="admin.systemPage">
	<tiles:putAttribute name="title" value="Uniware - Facility Allocation Rules" />
	<tiles:putAttribute name="rightPane">
		<div class="greybor headlable ovrhid main-box-head">
			<h2 class="edithead head-textfields">Facility Allocation Rules</h2>
		</div>
		<div class="ovrhid pad-15">
			<div class="lfloat"><em class="f12">Please Drag &amp; Drop to change the preference</em></div>
			<div class=" btn btn-small btn-primary rfloat" id="create">+ Add Facility Allocation Rule</div>
		</div>
		<div class="greybor form-edit-table-cont round_bottom pad-15">
			<div id="allocationData"></div>
		</div>
		<div id="newAllocationRuleDiv" class="lb-over">
			<div class="lb-over-inner round_all">
				<div style="margin: 40px;">
				<form onsubmit="javascript : return false;">
					<div class="pageHeading lfloat">Facility Allocation rule</div>
					<div id="newAllocationRuleDiv_close" class="link rfloat">close</div>
					<div class="clear"></div>
					<div id="allocationFormCont">
					</div>	
					<div class="clear"></div>
				</form>
				</div>
			</div>
		</div>
	</tiles:putAttribute>
	<tiles:putAttribute name="deferredScript">
		<script type="text/html" id="allocationForm">
			<div class="searchLabel formLeft150 lfloat">Name</div>
			<div class="searchLabel formRight lfloat">
				<# if (obj.id != null) { #>
					<#=obj.name#>
				<# } else { #>
					<input id="allocationName" size="15"  autocomplete="off" maxlength="10" class="w200" value="<#=obj.name#>"/>
				<# } #>
			</div>
			<div class="clear"></div>

			<div class="searchLabel formLeft150 lfloat">Condition Expression</div>
			<div class="formRight lfloat"><textarea id="allocationConditionExpression" size="15"  autocomplete="off" class="w200"><#=Uniware.Utils.escapeHtml(obj.conditionExpressionText)#></textarea></div>
			<div class="clear"></div>

			<div class="searchLabel formLeft150 lfloat">Facility</div>
			<div class="formRight lfloat">
				<select id="selectFacility" class="w200">
					<option value="">-Select One-</option>
					<c:forEach var="facility" items="${cache.getCache('facilityCache').facilities}">
						<option value="${facility.code}" <#= (obj.facilityCode== '${facility.code}') ? 'selected' : ""#>><c:out value="${facility.displayName}" /></option>
					</c:forEach>
				</select>
			</div>
			<div class="clear"></div>
			
			<# if(obj.id != null) { #>
				<div class="searchLabel formLeft150 lfloat">Enable</div>
				<div class="formRight lfloat">
					<input id="enabled" type="checkbox" <#= obj.enabled == true ? checked="checked" : ""#>/>
				</div>
				<div class="clear"></div>
			<# } #>	
	
			<br />
			<div class="formLeft150 lfloat">&#160;</div>
			<div class="formRight lfloat">
				<input type="submit" id="<#= obj.id != null ? "allocationRuleEdit" : "allocationRuleSubmit"#>" class=" btn btn-small btn-primary lfloat" value="<#= obj.id != null ? "Update" : "Submit"#>"/>
			</div>
			<div class="clear"></div>
			<br />
			<div id="error" class="errorField lfloat" style="margin-left: 20px;"></div>
			<div class="clear"></div>
		</script>
		<script type="text/html" id="allocationDataTemplate">
			<table class="uniTable greybor" border="0" cellspacing="0" cellpadding="0" style="font-size:13px;" id="allocationDataTable">
				<tr class="bold ui-state-disabled" style="opacity:1">
					<th><strong>Name</strong></th>
					<th width="30%"><strong>Condition Expression</strong></th>
					<th><strong>Facility</strong></th>
					<th><strong>Preference </strong></th>
					<th><strong>Enabled</strong></th>
					<th><strong>Action</strong></th>
				</tr>
				<#if(obj.length == 0) { #>
					<tr>
						<td colspan="5"><div  style="margin:15px;"><em class="f15">No allocation rules</em></div></td>
					</tr>
				<# } else { #>
					<# for ( var i=0; i<obj.length; i++) { var allocationRule = obj[i]; #>
						<tr style="cursor:pointer" id="<#=allocationRule.name#>" class="<#= allocationRule.enabled == false ? "rule disabled" : "rule"#>">
							<td class="first-col"><#=allocationRule.name#></td>
							<td style="word-wrap:break-word;"><#=allocationRule.conditionExpressionText#></td>
							<td>
							<c:forEach var="facility" items="${cache.getCache('facilityCache').facilities}">
								<# if(allocationRule.facilityCode == '${facility.code}') { #>								
									${facility.displayName}
								<# } #>					
							</c:forEach>
							</td>
							<td><#=(allocationRule.preference)#></td>
							<td>
								<# if(allocationRule.enabled) { #>
									<img src="/img/icons/tick.png" />
								<# } else { #>
									<img src="/img/icons/hold.png" />	
								<# } #>
							</td>
							<td>
								<div id="<#=i#>" class="btn btn-small lfloat editAction">edit</div>
								<div id="remove-<#=i#>" class="btn btn-small btn-danger lfloat10 deleteAction">Delete</div>
							</td>
						</tr>
					<# } #>
				<# } #>
			</table>
		</script>
		<script type="text/javascript">
		
		Uniware.AllocationRules = function() {
			var self = this;
			this.facilityAllocationRules = ${facilityAllocationRules};
			this.editingPos;
		
			this.init = function() {
				self.loadAllocationTable();
				$("#create").click(function(){
					$("#allocationFormCont").html(template("allocationForm", self.facilityAllocationRules));
					Uniware.LightBox.show('#newAllocationRuleDiv');
					$("#allocationRuleSubmit").click(self.allocationFormSubmit);
				});
			}
			
			this.loadAllocationTable = function(){
				$("#allocationData").html(template("allocationDataTemplate", self.facilityAllocationRules));
				$(".editAction").click(self.allocationRulesEdit);		
				$(".deleteAction").click(self.allocationRulesDelete);
				$("#allocationDataTable").sortable({ items: "tr:not(.ui-state-disabled)", stop: self.reorderAllocationDataRows });
			}
			
			this.allocationFormSubmit = function(){
				var maxPreference = 0;
				for (i=0; i<self.facilityAllocationRules.length; i++){
					maxPreference = maxPreference < self.facilityAllocationRules[i].preference ? self.facilityAllocationRules[i].preference : maxPreference;
				}
				$("#error").css('visibility', 'hidden');			
				var requestObject = {
					name : $('#allocationName').val(),
					conditionExpressionText : $('#allocationConditionExpression').val(),
					facilityCode : $('#selectFacility').val(),
					preference : (maxPreference + 1)
				};
				Uniware.Ajax.postJson("/data/admin/configuration/facilityAllocationRule/create", JSON.stringify(requestObject), function(response) {
					if (response.successful == false) {
						$("#error").html(response.errors[0].description).css('visibility', 'visible');
					} else {
						var allocationRule = response.facilityAllocationRule;
						self.facilityAllocationRules.push(allocationRule);
						self.loadAllocationTable();
						Uniware.Utils.addNotification('Facility Allocation Rule  has been added');
						Uniware.LightBox.hide();
					}
				});
			}
			
			this.allocationRulesEdit = function(){
				self.editingPos = $(this).attr('id');
				var allocationRule = self.facilityAllocationRules[self.editingPos];
				$("#allocationFormCont").html(template("allocationForm", allocationRule));
				Uniware.LightBox.show('#newAllocationRuleDiv');
				$("#allocationRuleEdit").click(self.allocationFormEdit);
			}
			
			this.allocationFormEdit = function(){
				$("#error").css('visibility', 'hidden');			
				var requestObject = {
					'name' : self.facilityAllocationRules[self.editingPos].name,
					'conditionExpressionText' : $("#allocationConditionExpression").val(),
					'facilityCode' : $('#selectFacility').val(),
					'preference' : self.facilityAllocationRules[self.editingPos].preference,
					'enabled' : $('#enabled').is(':checked')
				};
				Uniware.Ajax.postJson("/data/admin/configuration/facilityAllocationRule/edit", JSON.stringify(requestObject), function(response) {
					if (response.successful == false) {
						$("#error").html(response.errors[0].description).css('visibility', 'visible');
					} else {
						var allocationRule = response.facilityAllocationRule;
						self.facilityAllocationRules[self.editingPos] = allocationRule;
						self.loadAllocationTable();
						Uniware.Utils.addNotification('Facility Allocation Rule "'+ allocationRule.name + '" has been updated successfully');
						Uniware.LightBox.hide();
					}
				});
			}
			
			this.allocationRulesDelete = function(){
				self.editingPos = $(this).attr('id').split('-')[1];
				var allocationRule = self.facilityAllocationRules[self.editingPos].name;
				var req = {
					'name' : allocationRule
				};
				if(confirm('Are you sure?')){
					Uniware.Ajax.postJson("/data/admin/configuration/facilityAllocationRule/delete", JSON.stringify(req), function(response) {
						if (response.successful == false) {
							$("#error").html(response.errors[0].description).css('visibility', 'visible');
						} else {
							self.facilityAllocationRules.splice(self.editingPos,1);	
							self.loadAllocationTable();
							Uniware.Utils.addNotification('Facility Allocation Rule "'+ allocationRule + '" has been removed successfully');
						}
					});
				}
			};
			
			this.reorderAllocationDataRows = function(){
				var req = {};
				req.allocationRuleNameToPreferenceMap = {};
				var sequence = 0;
				$(this).find('tr.rule').each(function(){
					req.allocationRuleNameToPreferenceMap[$(this).attr('id')] = ++sequence;
				});
				Uniware.Ajax.postJson('/data/admin/configuration/facilityAllocationRule/reorder', JSON.stringify(req), function(response) {
					if(response.successful){
						window.location.href = window.location.href;
					} else {
						Uniware.Utils.showError(response.errors[0].description);
					}
				});
			};
		};
		
		$(document).ready(function() {
			window.page = new Uniware.AllocationRules();
			window.page.init();
		});	
		</script>		
	</tiles:putAttribute>
</tiles:insertDefinition>
