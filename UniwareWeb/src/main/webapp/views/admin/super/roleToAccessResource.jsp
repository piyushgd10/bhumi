<%@ include file="/tagIncludes.jsp"%>
<tiles:insertDefinition name="admin.superPage">
	<tiles:putAttribute name="title" value="Uniware - Role To Access Resource Mapping" />
	<tiles:putAttribute name="rightPane">
		<div class="greybor headlable ovrhid main-box-head">
			<h2 class="edithead head-textfields">Role To Access Resource Mapping</h2>
		</div>
		<div class="pad-15">	
			<div class="btn btn-small btn-primary rfloat" id="showCreateRole" title="create new role">+ Add New Role</div>
			<div class="clear"></div>
		</div>
		<div id="ltCreateRoleDiv" class="lb-over">
			<div class="lb-over-inner round_all">
				<div id="tempDiv" style="margin: 40px;">
				</div>
			</div>
		</div>
		<div id="ltEditRoleDiv" class="lb-over">
			<div class="lb-over-inner round_all" style="width:1000px;">
				<div id="editRoleDiv" style="margin: 20px;height:600px;overflow-y:scroll;font-size:11px;">
				</div>
			</div>
		</div>		
		<div class="clear"></div>
		<div class="greybor form-edit-table-cont round_bottom pad-15">
			<div id="map"></div>
		</div>

	</tiles:putAttribute>
	<tiles:putAttribute name="deferredScript">
		<script type="text/html" id="rolesTemplate">
		<table class="dataTable">
			<tr class="f15 ucase"><td width="15%" valign="top">Role</td><td width="10%" valign="top">Level</td><td>Access Resource<br/><br/></td></tr>
			<# for (var code in obj.roles) { var role=obj.roles[code]; #>
			<tr>
				<td class="f15"><#=role.name#>&#160;<# if(role.code != 'VENDOR') { #><img src="/img/icons/edit.png" id="role-<#=code#>" class="roleEdit link" /><# } #></td>
				<td class="f15"><#=role.level == 'TENANT' ? 'All Facility' : 'Facility'#></td>
				<td>
				<# for (var key in role.accessResources) { var access = obj.accessResources[key]; if (access && role.accessResources[key]) { #>
					<span class="vLabel round_all"><#=access.name#></span>
				<# }} #>
				</td>
			</tr>
			<# } #>
		</table>
		</script>
		<script type="text/html" id="editRoleTemplate">
			<div class="pageHeading lfloat">Edit <#=obj.name#> Role</div>
			<div id="ltEditRoleDiv_close" class="link rfloat">close</div>
			<div class="clear"></div>

			<# for (var group in obj.accessResourcesGroup) { var groupResourceIds = obj.accessResourcesGroup[group]; #>
				<div class="heading"><#=group#></div>
				<# for (var i=0;i<groupResourceIds.length;i++) { var accessResource = obj.accessResourcesMap[groupResourceIds[i]];#>
					<div class="lfloat" style="margin:5px;">
					<input style="margin-top:0px;vertical-align: text-top;" type="checkbox" id="check-<#=accessResource.name#>" class="checkBoxRoleEdit" <#= obj.accessResources[accessResource.name] && obj.accessResources[accessResource.name] === true ? 'checked="checked"' : ''#>/>&#160;<#=accessResource.name#>
					</div>
				<# } #>
				<div class="clear"></div>
			<# } #>
			<br/><div class=""><input type="submit" id="roleEditSubmit" class=" btn btn-small btn-primary lfloat" value="submit"/></div>
			<div class="clear"></div><br/>

			<div id="errorEditRole" class="errorField" style="margin-left: 20px;"></div><br />
		</script>		
		<script type="text/html" id="createRoleTemplate">
			<div class="pageHeading lfloat">Create new Role</div>
			<div id="ltCreateRoleDiv_close" class="link rfloat">close</div>
			<div class="clear"></div>
			<br />

			<div class="formLeft150 lfloat" style="margin-top:10px;">Name</div>
			<div class="formRight lfloat"><input id="roleName" size="15" class="ucase" autocomplete="off" /></div>
			<div class="clear"></div>

			<c:if test="${cache.getCache('facilityCache').facilities.size() > 1}">
			<div class="formLeft150 lfloat" style="margin-top:10px;">Level</div>
			<div class="formRight lfloat">
				<select id="roleLevel">
					<option value="FACILITY" selected="selected">Facility</option>
					<option value="TENANT">All Facility</option>
				</select>
			</div>
			<div class="clear"></div>
			</c:if>

			<div class="formLeft150 lfloat hidden">Parent Role</div>
			<div class="formRight lfloat hidden">
				<select id="parentRole" autocomplete="off">
				<# for (var i=0;i<obj.length;i++) { var role = obj[i]; #>
					<# if(role.code == 'ADMIN') { #>
						<option value="<#=role.id#>"><#=role.name#></option>
					<# } #>
				<# } #>
				</select>
			</div>
			<div class="clear"></div>

			<br /> 
			<div class="formLeft150 lfloat">&#160;</div>
			<div class="formRight lfloat"><input type="submit" id="roleSubmit" class="btn btn-small btn-primary lfloat" value="submit"/></div>
			<div class="clear"></div>
			
			<div id="errorRole" class="errorField" style="margin-left: 20px;"></div>
			<br />
		</script>		
		<script type="text/javascript">
			Uniware.RoleToAccessResourceMapping = function() {
				var self = this;
				this.currentRole;
				this.roles = ${roles};
				this.accessResources = ${accessResources};
				this.accessResourcesMap = {};
				this.data = {};
				
				this.init = function() {
					var obj = {};
					obj.roles = self.roles;
					obj.accessResources = {};
					for (var i=0;i<self.accessResources.length;i++) {
						var accessResource = self.accessResources[i];
						obj.accessResources[accessResource.name] = accessResource;
					}
					self.accessResourcesMap = obj.accessResources;
					$('#map').html(template("rolesTemplate", obj));
					$('.roleEdit').click(self.showEditRole);
					$("#showCreateRole").click(function(){
						$("#tempDiv").html(template("createRoleTemplate", self.roles));
						Uniware.LightBox.show('#ltCreateRoleDiv', function() {$("#roleCode").focus();});
						
						$("#roleSubmit").click(function(){
							$("#errorRole").css('visibility', 'hidden');
							var req = {
								'roleName' : $("#roleName").val(),
								'level' : $("#roleLevel").length > 0 ? $("#roleLevel").val() : 'FACILITY'
							};
							Uniware.Ajax.postJson("/data/admin/super/role/create", JSON.stringify(req), function(response) {
								if (response.successful == false) {
									$("#errorRole").html(response.errors[0].description).css('visibility', 'visible');
								} else {
									Uniware.LightBox.hide();
									window.location.href = window.location.href.split('?')[0] + '?code=' + response.roleCode + '&legacy=1';
								}
							});
						});
					});
				};
				
				this.showEditRole = function() {
					var code = $(this).attr('id').substring($(this).attr('id').indexOf('-') + 1);
					var obj = self.roles[code];
					self.currentRole = obj;
					obj.accessResourcesGroup = {};
					obj.accessResourcesMap = {};
					for (var key in self.accessResourcesMap) {
						var accessResource = self.accessResourcesMap[key];
						if (accessResource.level == 'BOTH' || (accessResource.level == obj.level)) {
							obj.accessResourcesMap[key] = accessResource;
							if (accessResource.accessGroupName) {
								if (!obj.accessResourcesGroup[accessResource.accessGroupName]) {
									obj.accessResourcesGroup[accessResource.accessGroupName] = [];
								}
								obj.accessResourcesGroup[accessResource.accessGroupName].push(accessResource.name);
							} else {
								if (!obj.accessResourcesGroup['NONE']) {
									obj.accessResourcesGroup['NONE'] = [];
								}
								obj.accessResourcesGroup['NONE'].push(accessResource.name);
							}
						}
					}
					$("#editRoleDiv").html(template("editRoleTemplate", obj));
					$("#roleEditSubmit").click(self.updateRole);
					Uniware.LightBox.show('#ltEditRoleDiv');
				};
				
				this.updateRole = function() {
					var req = {
						roleId : self.currentRole.id,
						newAccessResourceNames : [],
						updateAccessResourceNames : {}
					};
					$("input.checkBoxRoleEdit:checked").each(function(){
						var resourceId = $(this).attr('id').substring($(this).attr('id').indexOf('-') + 1);
						if (self.currentRole.accessResources[resourceId] == null) {
							req.newAccessResourceNames.push(resourceId);
						}
					});
					for (var accessResourceId in self.currentRole.accessResources) {
						var enabled = self.currentRole.accessResources[accessResourceId];
						if ($('#check-'+accessResourceId).is(':checked') != enabled) {
							req.updateAccessResourceNames[accessResourceId] = !enabled;
						}
					};
					Uniware.Ajax.postJson("/data/admin/super/role/edit", JSON.stringify(req), function(response) {
						if (response.successful == false) {
							$("#errorEditRole").html(response.errors[0].description).css('visibility', 'visible');
						} else {
							Uniware.LightBox.hide();
							window.location.href = window.location.href.split('?')[0] + '?legacy=1';
						}
					}, true);
				};
			};
			
			$(document).ready(function() {
				window.page = new Uniware.RoleToAccessResourceMapping();
				window.page.init();	
				var role = '${param['code']}';
				if(role != '') {
					$('#role-' + role).trigger('click');
				}		
			});
		</script>
	</tiles:putAttribute>	
</tiles:insertDefinition>
