<%@ include file="/tagIncludes.jsp"%>
<tiles:insertDefinition name="admin.systemPage">
	<tiles:putAttribute name="title" value="Uniware - Script Configuration" />
	<tiles:putAttribute name="rightPane">
		<div class="greybor headlable ovrhid main-box-head">
			<h2 class="edithead head-textfields">Script Configuration</h2>
		</div>
		<div id="selectScriptDiv"class="round_bottom"></div>
	</tiles:putAttribute>
	<tiles:putAttribute name="deferredScript">
		<script type="text/javascript" src="${path.js('codemirror/codemirror.all.js')}"></script>
		<link href="${path.css('codemirror/ui.codemirror.all.css')}" rel="stylesheet" type="text/css" />
		<script type="text/html" id="selectScriptTemplate">
			<div style="margin: 15px 15px 5px 20px;" class="lfloat">
				<input type="text" id="scriptName" style="width:350px"/>
			</div>
        
        <div class="clear"></div>
			<div id="scriptDiv" >
			</div>
		</script>
		<script type="text/html" id="scriptConfigTemplate">
				<div class="formLabel main-boform-cont ovrhid">
					<div class="clear"></div>
					
					<div class="lfloat">	
						<input type="text" readonly="readonly" value="<#=obj.version#>" />
						<br/>
					</div>
					
					<div class="lfloat">	
						<br/>
						<textarea id="scriptConfig"></textarea>
						<br/>
					</div>
					
					<div class="clear"></div>
					<br/>
				</div>
		</script>
		<script type="text/javascript">
			Uniware.ScriptPage = function(){
				var self = this;
				this.exportJobType = null;
				this.init = function() {
					$('#selectScriptDiv').html(template('selectScriptTemplate'));
					//$('#scriptName').combobox();
					$("#scriptName").autocomplete(self.getAutoCompleteObj());
					//$('#scriptName').change(self.getScript);
				};
				
				this.getAutoCompleteObj = function() {
                    return {
                        minLength: 2,   
                        mustMatch : true,
                        autoFocus: true,
                        source: function( request, response ) {
                            Uniware.Ajax.getJson("/data/lookup/scripts?keyword=" + request.term, function(data) {
                                response( $.map( data, function( script ) {
                                    return {
                                        label: script,
                                        value: script,
                                        script: script
                                    }
                                }));
                            });
                        },
                        select : function(event, ui) {
                        	$('#scriptName').val(ui.item.script);
                        	self.getScript();
                        }
                    };
                };
                
				this.getScript = function() {
					var scriptName = $('#scriptName').val();
					if(scriptName != ""){
						Uniware.Ajax.postJson("/data/admin/super/script/get?name=" + scriptName, {}, function(response) {
							self.loadScript(response);
						});	
					} else {
						$('#scriptDiv').html("");
					}
				};
				
				this.loadScript = function(data){
					self.script = data;
					$('#scriptDiv').html('');
					$('#scriptDiv').html(template("scriptConfigTemplate", data));
					CodeMirror.commands.autocomplete = function(cm) {
				        CodeMirror.showHint(cm, CodeMirror.scraperHint);
				    };
				    CodeMirror.commands.indentAutoFull = function(cm) {
				        for (var i = 0; i <= cm.lastLine(); i++){
				          cm.indentLine(i);
				        }
				    };
				    CodeMirror.commands.blockComment = function(cm) {
				        cm.blockComment(cm.getCursor(true), cm.getCursor(false));
				    };

				    CodeMirror.commands.unComment = function(cm) {
				        cm.uncomment(cm.getCursor(true), cm.getCursor(false));
				    };
					var myCodeMirror = CodeMirror.fromTextArea(document.getElementById("scriptConfig"), {
						rtlMoveVisually: true,
						indentUnit: 4,
						lineNumbers: true,
						indentWithTabs: true,
				        autofocus: true,
				        theme: "eclipse",
				        extraKeys: {"Ctrl-Space": "autocomplete", "Shift-Ctrl-F" : "indentAutoFull", "Ctrl-/" : "blockComment", "Ctrl-\\": "unComment"},
				        autoCloseTags: true,
				        viewportMargin: Infinity
				    });
					myCodeMirror.setValue(data.script);
					
					myCodeMirror.setSize($(window).width() - 230,$(window).height() - 185);
					/*myCodeMirror.on("change", function(){
						myCodeMirror.save();
					});
					$('.CodeMirror').resizable({
						resize: function() {
							myCodeMirror.setSize($(this).width(), $(this).height());
						}
					});*/
					/* $('#update').click(function() {
						myCodeMirror.save();
						self.scriptValue = $('#scriptConfig').val();
						self.updateScript();
					}); */
				};
				
				this.updateScript = function(){
					var req = {
						name:self.script.name,
						script: self.scriptValue
					}
					Uniware.Ajax.postJson("/data/admin/super/script/edit", JSON.stringify(req) , function(response) {
						if(response.successful == true){
							Uniware.Utils.addNotification("Script has been successfully updated");
						}else{
							Uniware.Utils.showError(response.errors[0].description);
						}
					});
				};
			};

			$(document).ready(function() {
				window.page = new Uniware.ScriptPage();
				window.page.init();
				var configName = '${param['name']}';
				if(configName != '') {
					$('#scriptName').val(configName).trigger('change');
				}
			});
		</script>
	</tiles:putAttribute>
</tiles:insertDefinition>	
