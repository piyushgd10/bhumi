<%@ include file="/tagIncludes.jsp"%>
<tiles:insertDefinition name="admin.systemPage">
	<tiles:putAttribute name="title" value="Uniware -View Custom Field" />
	<tiles:putAttribute name="rightPane">
		<div id="mainDiv"></div>
	</tiles:putAttribute>
	<tiles:putAttribute name="deferredScript">
		<script type="text/javascript" src="${path.js('jquery/jquery.dataTables.min.js')}"></script>
		<script type="text/html" id="createCustomFieldTemplate">
			<form onsubmit="javascript : return false;">
				<div class="greybor headlable ovrhid main-box-head">
					<h2 class="edithead head-textfields">Create Custom Field</h2></div>
				<div class="formLabel greybor round_bottom main-boform-cont pad-15-top ovrhid">
					<table width="100%" border="0" cellspacing="5" cellpadding="5">
  						<tr>
    						<td width="20%">Entity</td>
    						<td width="30%">
    							<select id="entity" class="w200">
									<option value="">-- select an entity --</option>
									<# for(var i=0; i<window.page.entities.length; i++) { #>
										<option><#=window.page.entities[i]#></option>
									<# } #>
								</select>
							</td>
  						</tr>
 						<tr>
							<td width="20%">Name</td>
							<td><input id="name" type="text" class="w200"/></td>
						</tr>
						<tr>
							<td valign="top">Length</td>
							<td valign="top"><input id="length" type="text" class="w200"/></td>
						</tr>
						<tr>
							<td>Display Name</td>
							<td><input id="displayName"  type="text" class="w200"/></td>
						</tr>
						<tr>
							<td>Value Type</td>
							<td>
								<select id="valueType" class="w200">
									<option value="">-- select a value type --</option>
									<# for(var i=0; i<window.page.valueTypes.length; i++) { #>
										<option><#=window.page.valueTypes[i]#></option>
									<# } #>
								</select>    	
							</td>
 						</tr>
						<tr>
							<td valign="top">Possible Values</td>
							<td ><textarea id="possibleValues" class="w200"/></td>
						</tr>
						<tr>
							<td>Default Value</td>
							<td><input id="defaultValue"  type="text" class="w200"/></td>
						<tr>
						</tr>
							<td>Required</td>
							<td><input id="required" type="checkbox" /></td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>
								<input type="submit" class=" btn btn-success" id="add" value="Create"/>&nbsp;
								<input type="button" class=" btn btn-info" id="cancel" value="Cancel"/>
							</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
					</table>
				</div>
			</form>
		</script>
		<script type="text/html" id="viewCustomFieldsTemplate">
			<form onsubmit="javascript : return false;">
				<div class="greybor headlable ovrhid main-box-head">
					<h2 class="edithead head-textfields">View Custom Fields</h2>
				</div>
				<div class="main-boform-cont ovrhid">
					<div class="clear"></div>
					<div class="btn btn-small btn-info rfloat10" id="create">+ add CustomField</div>
				</div>
			</form>
			<# if (obj.length > 0) { #>
				<div class="greybor round_bottom">
					<table class="table table-striped">
						<tr class="ucase" align="left">
							<th>Entity</th>
							<th>Display Name</th>
							<th>Name</th>
							<th>Required</th>
							<th>Possible Values</th>
							<th>Default Value</th>
							<th>Type</th>
							<th>Enabled</th>
							<th>Action</th>
						</tr>
						<# for (var i = 0; i < obj.length; i++) {  #>
							<tr>
								<td><#= obj[i].entity.split('.')[4]#></td>
								<td><#= obj[i].displayName #></td>
								<td><#= obj[i].name#></td>
								<td>
									<# if(obj[i].required) { #>
										yes
									<# } else { #>
										No
									<# } #>
								</td>
								<td>
									<# if (obj[i].isEditing && obj[i].type == 'select') { #>
										<input type="text" value="<#=obj[i].possibleValues#>" class="edit possibleValues" id="posVal-<#=i#>"></input>
									<# } else if(obj[i].type == 'select') { #>
										<span class="round_all table-editable" id="rowNo-<#=i#>">
											<#= obj[i].possibleValues#>
										</span>
									<# } else {#>
										<#= obj[i].possibleValues#>
									<# } #>
								</td>
								<td><#= obj[i].defaultValue#></td>
								<td><#= obj[i].type#></td>
								<td>
									<# if(obj[i].enabled) { #>
										yes
									<# } else { #>
										No
									<# } #>
								</td>
								<td>
									<# if (obj[i].isEditing) { #>
										<input type="submit" class="btn btn-small update" value="update" id="update-<#=i#>"></input>
										<span class="link cancel" id="cancel-<#=i#>">cancel</span>
									<# } #>
								</td>
							</tr>			
						<# } #>
					</table>
				</div>
			<# } else { #>
				<em class="f15">No custom field exist for this entity</em>
			<# } #>
		</script>
		<script type="text/javascript">
			Uniware.CustomFieldPage = function() {
				var self = this;
				this.entities = ${entities};
				this.valueTypes = ${valueTypes};
				this.customFields = {};
				this.isRequired = false;
				this.init = function() {
					Uniware.Ajax.getJson("/data/admin/super/customField/list", function(response){
						self.customFields = response.customFields;
						self.viewCustomField();
					});
				};
				
				
				this.viewCustomField = function() {
					$('#mainDiv').html(template("viewCustomFieldsTemplate", self.customFields));
					$("#create").click(self.createCustomField);
					$('.table-editable').click(self.editRow);
					$('.cancel').click(self.removeEditable);
					$('.update').click(self.updateCustomField);
				};
				
				this.createCustomField = function() {
					$('#mainDiv').html(template("createCustomFieldTemplate"));
					$("#cancel").click(self.viewCustomField);
					$("#add").click(self.addCustomField);
				};
				
				this.addCustomField = function(){
					var requestObject = {
						    'entity' : $('#entity').val(),
						    'name' : $('#name').val(),
						    'displayName' : $('#displayName').val(),
						    'valueType' : $('#valueType').val(),
						    'length' : $('#length').val(),
						    'defaultValue' : $('#defaultValue').val(),
						    'possibleValues' : $('#possibleValues').val(),
						    'validatorPattern' : $('#validatorPattern').val(),
						    'required' : $('#required').is(':checked')
					}

					Uniware.Ajax.postJson("/data/admin/super/customField/create", JSON.stringify(requestObject), function(response) {
						if(response.successful == true){
							Uniware.Utils.addNotification('Custom field has been created');
							self.init();
						}else{
							Uniware.Utils.showError(response.errors[0].description);												
						}
					}, true);
				};
				
				this.editRow = function() {
					var rowIndex = $(this).attr('id').split('-')[1];
					self.customFields[rowIndex].isEditing = true;
					self.viewCustomField();
				};
				
				this.removeEditable = function(){
					var rowIndex = $(this).attr('id').split('-')[1];
					self.customFields[rowIndex].isEditing = false;
					self.viewCustomField();
				};
				
				this.updateCustomField = function() {
					var rowIndex = $(this).attr('id').split('-')[1];
					var customField = self.customFields[rowIndex];
					var possibleValueId = '#posVal-' + rowIndex;
					var requestObject = {
						    'entity' : customField.entity,
						    'name' : customField.name,
						    'possibleValues' : $(possibleValueId).val()
					}
					
					Uniware.Ajax.postJson("/data/admin/super/customField/edit", JSON.stringify(requestObject), function(response) {
						if(response.successful == true){
							Uniware.Utils.addNotification('Custom field has been edited');
							self.init();
						}else{
							Uniware.Utils.showError(response.errors[0].description);												
						}
					}, true);
				};
			};
			$('document').ready(function() {
				window.page = new Uniware.CustomFieldPage();
				window.page.init();
			});
		</script>
	</tiles:putAttribute>
</tiles:insertDefinition>
