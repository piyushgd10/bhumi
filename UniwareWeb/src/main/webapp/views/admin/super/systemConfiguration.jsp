<%@ include file="/tagIncludes.jsp"%>
<tiles:insertDefinition name="admin.systemPage">
	<tiles:putAttribute name="title" value="Uniware -SystemConfiguration" />
	<tiles:putAttribute name="rightPane">
		<div class="greybor headlable ovrhid main-box-head">
			<h2 class="edithead head-textfields">System Configuration</h2>
		</div>
		<div class="greybor round_bottom">
			<div id="systemConfig"></div>
		</div>
	</tiles:putAttribute>
	<tiles:putAttribute name="deferredScript">
		<script type="text/javascript" src="${path.js('jquery/jquery.dataTables.min.js')}"></script>
		<script type="text/javascript">
			Uniware.SystemConfigurationPage = function() {
				var self = this;
				this.table = null;
				this.systemConfiguration = ${systemConfiguration};

				this.init = function() {
					self.loadTable();
				};

				this.loadTable = function() {
					$("#systemConfig").html(template("systemConfigTemplate", self.systemConfiguration));
					$("#accordion").accordion({
						autoHeight : false,
					});
					
					$("#uniTable tbody").on('click', 'span.table-editable', function(event) {
						var tr = $(event.target).parents('tr');
						var aPos = $(tr[0]).attr('id');
						var aData = self.systemConfiguration[aPos];
						if (aData.isEditing) {

						} else {
							if (self.aPosEditing != null && self.aPosEditing != aPos) {
								var aEditData = self.systemConfiguration[self.aPosEditing];
								aEditData.isEditing = null;
								$('tr#' + self.aPosEditing).html(template("systemConfigRow", aEditData));
							}
							self.aPosEditing = aPos;
							aData.isEditing = true;
							$(tr[0]).html(template("systemConfigRow", aData));
						}
					});
					
					$('#uniTable tbody').on('click', 'input.update', function(event) {
						var tr =$(event.target).parents('tr');
						var aPos = $(tr[0]).attr('id');
						var aData = self.systemConfiguration[aPos];
						var value = null;
						var type = aData.type.split(':')[0];
						if(aData.type == "checkbox") {
							value = $(tr[0]).find('input.checkbox').is(":checked");
						} else if(type == "select") {
							value = $(tr[0]).find('select').val();
						} else if(aData.type == 'text'){
							value = $(tr[0]).find('textarea').val();
						}
						var reqObject = {
								'name' : aData.name,
								'value' : value
						};
						
						Uniware.Ajax.postJson("/data/admin/super/systemConfiguration/edit",JSON.stringify(reqObject),function(response){
							if(response.successful == false) {
								Uniware.Utils.showError(response.errors[0].description);
							}else{
								aData.isEditing = null;
								self.aPosEditing = aPos;
								aData.value = value;	
								$('tr#' +self.aPosEditing).html(template("systemConfigRow",aData));
								Uniware.Utils.addNotification("System Configuration is updated");
							}
						});
					});
					
					$('#uniTable tbody').on('click' , 'span.cancel',function(event) {
						var tr = $(event.target).parents('tr');
						var index = $(tr[0]).attr('id');
						self.systemConfiguration[index].isEditing = null;
						$('tr#' +index).html(template("systemConfigRow",self.systemConfiguration[index]));
					});				
				};
			}

			$('document').ready(function() {
				window.page = new Uniware.SystemConfigurationPage();
				window.page.init();
				$('.accordion .head').click(function() {
					$(this).next().toggle('slow');
					return false;
				}).next().hide();
			});
		</script>
		<script type="text/html" id="systemConfigTemplate">
				<div id="accordion">
				<# var groupName=null; #>
				<# for (var i=0 ; i < obj.length;) { #>
					<# if (obj[i].groupName.toLowerCase() != groupName) { #>
							<h3><a href="#"><#=obj[i].groupName.toUpperCase()#></a></h3>
							<# groupName = obj[i].groupName.toLowerCase(); #>
					<# } #>
					<div>
							<table id="uniTable" class="uniTable" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<th>Name</th>
								<th>Value</th>
								<th>Operations</th>
							</tr>
							<# while (obj[i] && obj[i].groupName.toLowerCase() == groupName) { #>
								<tr id="<#=i#>">
									<#=template("systemConfigRow", obj[i])#>
								</tr>
								<# i = i + 1; #>
							<# } #>
							</table>
					</div>
				<# } #>
				</div>
       </script>
		<script type="text/html" id="systemConfigRow">
			<# var isGoLiveModifiable = false;#>
			<sec:authorize ifAnyGranted="UNI_TECH_SUPPORT">
                  <# var isGoLiveModifiable = true;#>
            </sec:authorize>
			<# if(obj.isEditing == true && isGoLiveModifiable) { #>
				<td id="displayName"><#=obj.displayName#></td>
				<td>
					<# var switchOn = obj.type.split(":");#>
					<# switch(switchOn[0]) { 
						case 'checkbox' :	#>
						<input class="checkbox" type="checkbox" id="box + i" <#= eval(obj.value) ? checked="checked" : ""#> />
						<# break; case 'text' : #>
						<textarea id="text + i" ><#=Uniware.Utils.escapeHtml(obj.value) #> </textarea> 
						<#break; case 'select' : #>
							<select id="select + i">
								<# var selectValues = switchOn[1].split(",");#>
								<# for (var k=0;k<selectValues.length;k++) {  #>
								<option <#= obj.value == selectValues[k] ? selected="selected" : "" #>><#=selectValues[k]#></option>
								<# } #>
							</select>
						<# break; } #>
				</td>
				<td>
					<input type="submit" class="btn btn-small update" value="update"></input>
					<span class="link cancel" style="margin-top:5px;">cancel</span>
				</td>
			<# } else { #>
				<td><#=obj.displayName#></td>
				<td class="value">
				<# var switchOn = obj.type.split(":");#>
				<# switch(switchOn[0]) { 
					case 'checkbox' :	#>
					<span class="table-editable round_all"><input class="checkbox" type="checkbox" id="box + i"  <#=eval(obj.value) ? checked="checked" : ""#>  disabled="disabled"/></span>
					<# break; case 'text' : #>
					<span class="table-editable round_all"><#=obj.value #></span> 
					<#break; case 'select' : #>
						<span class="table-editable round_all">
							<#=obj.value#>
						</select></span>
					<# break; } #>
				</td>
				<td></td>
			<# } #>
       </script>
	</tiles:putAttribute>
</tiles:insertDefinition>
