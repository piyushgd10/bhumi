<%@ include file="/tagIncludes.jsp"%>
<tiles:insertDefinition name=".procurePage">
	<tiles:putAttribute name="title" value="Uniware - Create Purchase Order" />
	<tiles:putAttribute name="rightPane">
		<div id="purchaseOrderDiv"></div>
	</tiles:putAttribute>

	<tiles:putAttribute name="deferredScript">
	<script id="purchaseOrderTemplate" type="text/html">
		<div class="greybor headlable ovrhid main-box-head">
			<h2 class="edithead head-textfields">Purchase Order Items</h2>
		</div>
		<div class="greybor round_bottom main-boform-cont pad-15-top ovrhid mar-15-bot">
		<table width="100%" border="0" cellspacing="1" cellpadding="3" class="fields-table">
			<# if (obj.amendedPurchaseOrderCode) { #>
			<tr>
				<td colspan="4" class="f12 ucase">
					<a href="/procure/poItems?legacy=1&poCode=<#=obj.amendedPurchaseOrderCode#>">Amendment for Purchase Order <#=obj.amendedPurchaseOrderCode#></a>
				</td>
			</tr>
			<# } #>
			<tr> 
				<td>Purchase Order Code</td>
		  		<td><#=obj.code#></td>
        		<td>Vendor Name</td>
        		<td><#=obj.vendorName#></td>    
			</tr>
			<tr>
		  		<td>Type</td>
		  		<td><#=obj.type#></td>
		  		<td>Status</td>
		  		<td>
					<#=obj.statusCode#>
					<# if (obj.amendedPurchaseOrderCode) { #>
						- <a href="/procure/poItems?legacy=1&poCode=<#=obj.amendedPurchaseOrderCode#>"><#=obj.amendedPurchaseOrderCode#></a>
					<# } #>
				</td>
  			</tr>
			<tr>
		 		<td>From Party</td>
		  		<td><#=obj.fromParty#></td>
				<td>Agreement</td>
				<td><#=obj.vendorAgreementName#></td
  			</tr>
			<tr>
				<td>Created On</td>
	        	<td><#=(new Date(obj.created)).toDateTime()#></td>
		 		<td>Expiry Date</td>
		  		<td>
					<# if (obj.isAmending != true && (obj.statusCode == 'CREATED' || obj.statusCode == 'WAITING_FOR_APPROVAL')) { #>
						<input id="expiryDate" type="text" class="datefield w150 poUpdateItem" value="<#=obj.expiryDate ? new Date(obj.expiryDate).toPaddedDate() : ''#>"/>
					<# } else { #>
						<#= obj.expiryDate ? new Date(obj.expiryDate).toPaddedDate() : '' #>
					<# } #>
				</td>
			</tr>
			<tr>
				<td>Delivery Date</td>
		  		<td colspan="3">
					<# if (obj.isAmending != true && (obj.statusCode == 'CREATED' || obj.statusCode == 'WAITING_FOR_APPROVAL')) { #>
						<input id="deliveryDate" type="text" class="datefield w150 poUpdateItem" value="<#=obj.deliveryDate ? new Date(obj.deliveryDate).toPaddedDate() : ''#>"/>
					<# } else { #>
						<#= obj.deliveryDate ? new Date(obj.deliveryDate).toPaddedDate() : '' #>
					<# } #>
				</td>
  			</tr>
			<# if (obj.customFieldValues && obj.customFieldValues.length > 0) { #>
				<tr>
				<# for(var i=0;i<obj.customFieldValues.length;i++) { var customField = obj.customFieldValues[i]; #>
		 			<td><#=customField.displayName#></td>
					<td>
					<# if (obj.isAmending != true && (obj.statusCode == 'CREATED' || obj.statusCode == 'WAITING_FOR_APPROVAL')) { #>
						<# if (customField.valueType == 'text') { #>
							<input id="<#=customField.fieldName#>" type="text" class="w150 custom poUpdateItem" value="<#=customField.fieldValue#>"/>
						<# } else if (customField.valueType == 'date') { #>
							<input id="<#=customField.fieldName#>" type="text" class="w150 custom poUpdateItem datefield" value="<#= customField.fieldValue ? new Date(customField.fieldValue).toPaddedDate() : '' #>"/>
						<# } else if (customField.valueType == 'select') { #>
							<select id="<#=customField.fieldName#>" class="w150 custom poUpdateItem">
							<# for (var j=0;j<customField.possibleValues.length;j++) { #>
								<option value="<#=customField.possibleValues[j]#>" <#= customField.possibleValues[j] == customField.fieldValue ? selected="selected" : '' #> ><#=customField.possibleValues[j]#></option>
							<# } #>
							</select>
						<# } else if (customField.valueType == 'checkbox') { #>
							<input id="<#=customField.fieldName#>" type="checkbox" class="custom poUpdateItem" <#=customField.fieldValue ? checked="checked" : '' #>/>
						<# } #>
					<# } else { #>
						<# if (customField.valueType == 'date') { #>
							<#= customField.fieldValue ? new Date(customField.fieldValue).toPaddedDate() : '' #>
						<# } else if (customField.valueType == 'checkbox') { #>
							<input type="checkbox" disabled="disabled" <#=customField.fieldValue ? checked="checked" : '' #>/>
						<# } else { #>
							<#=customField.fieldValue#>
						<# } #>
					<# } #>
					</td>
					<# if (i == obj.customFieldValues.length - 1 && i % 2 == 0) { #>
						<td></td><td></td>
					<# } else if (i != 0 && i % 2 != 0) { #>
						</tr><tr>
					<# } #>
				<# } #>
				</tr>
            <# } #>
			<tr>
		 		<td>&#160;</td>
		  		<td colspan="3">
					<div id="poUpdateDiv" class="hidden">
						<input type="submit" class="btn btn-small btn-primary  lfloat" value="update" id="updatePO"></input>
						<div class="link lfloat10" id="cancelUpdate" style="margin-top:4px">cancel</span>
						<div class="clear"/>
					</div>
				</td>
  			</tr>
		</table>  
		<br/><br/>
		
		<div>
			<ul style="margin:0px;">
				<li id="purchaseOrderItemsMenu" class="subMenuItem subMenuSelected round_top"><span>Purchase Order Items</span></li>
			</ul>
		</div>

		<div id="purchaseOrderItems">
			<#=template("purchaseOrderItemsTemplate", obj)#>
		</div>
		<div id="inflowReceipts" class="hidden">
		</div>
		
		</div>
	</script>
	<script id="purchaseOrderItemsTemplate" type="text/html">
		<# if(obj.statusCode == 'APPROVED') { #>
			<uc:security accessResource="PO_APPROVE">
			<div id="div_<#=obj.code#>" class="btn btn-small rfloat10 email">resend email</div>
			<# if (obj.inflowReceiptsCount == 0) { #>
				<div class="btn btn-small rfloat10" id="prepareAmendment">amend</div>
			<# } #>
			<div class="btn btn-small rfloat10" id="closePO">cancel</div>
			</uc:security>
		<# } #>
		<# if(obj.statusCode != 'CREATED') { #>
		<a href="/vendor/po/show?legacy=1&code=<#=obj.code#>" target="_blank">
			<div class="btn btn-small rfloat10">print</div>
		</a>
		<# } #>
		<div class="clear"></div>
		<table id="poItemsTable" class="uniTable greybor" border="0" cellpadding="0" cellspacing="0">	
			<tr>
				<th>Item Type</th>
				<th>Item SKU</th>
				<th>Vendor SKU Code</th>
				<th width="60">Quantity</th>
				<# if(obj.statusCode != 'CREATED') { #>
					<th width="50">Pending<br/>Quantity</th>
					<th width="50">Rejected<br/>Quantity</th>
				<# } #>
				<th width="60">Unit<br/>Price</th>
				<th width="60">Discount<br/>per unit</th>
				<th width="60">Discount %</th>
				<th>Color</th>
				<th>Brand</th>
				<th>Size</th>
				<th width="60">Total Price</th>
				<# if(obj.statusCode == 'CREATED') { #>
				<th width="60">Operations</th>
				<# } #>
			</tr>
			<# var totalQty = 0;totalAmount = 0; #>
			<# for(var i=0; i<obj.purchaseOrderItems.length; i++) { var poItem = obj.purchaseOrderItems[i]; #>
				<# if(poItem.itemSKU) { #>
					<tr id="tr-<#=i#>">
						<# if(obj.statusCode == 'CREATED') { #>
							<# if(poItem.itemSKU) { #>
								<#=template("editPORow", poItem)#>
							<# } else { #>
								<#self.initRow#>
								<#=template("createPORow", poItem)#>
							<# } #>
						<# } else { #>
							<#=template("PORow", poItem)#>
						<# } #>
					</tr>
				<# } #>
			<# totalQty += poItem.quantity; totalAmount += poItem.total; } #>
			<# if (obj.statusCode != 'CREATED') { #>
			<tr class="bold" border="1">
				<td colspan="2"></td>
				<td>Total</td>
				<td><#=totalQty#></td>
				<td></td>
				<# if(obj.statusCode != 'CREATED') { #>
					<td></td>
					<td></td>
				<# } #>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td><#=totalAmount#></td>
			</tr>
			<# } #>
		</table>
		<div class="clear"></div>
		<br/>
	</script>
	<script id="PORow" type="text/html">
		<td>
			<#=obj.itemTypeName#>
			<#=Uniware.Utils.getItemDetail(obj.itemTypeImageUrl,obj.itemTypePageUrl)#>
		</td>
		<td><#=obj.itemSKU#></td>
		<td><#=obj.vendorSkuCode#></td>
		<td><#=obj.quantity#></td>
		<td><#=obj.pendingQuantity#></td>
		<td><#=obj.rejectedQuantity#></td>
		<td><#=obj.unitPrice#></td>
		<td><#=obj.discount#></td>
		<td><#=obj.discountPercentage#></td>
		<td><#=obj.color#></td>
		<td><#=obj.brand#></td>
		<td><#=obj.size#></td>
		<td><#=obj.total#></td>
	</script>
	<script type="text/javascript">
		<uc:security accessResource="PRICE_EDIT_AT_PO">
			Uniware.Utils.editPrice = true;
		</uc:security>
		
		Uniware.poItemsPage = function() {
			var self = this;
			this.aPosEditing;
			this.purchaseOrderCode = '${param['poCode']}';
			this.purchaseOrderData = null;
			this.oldPurchaseOrderData = null;
			this.facilityCode = '${cache.getCache('facilityCache').getCurrentFacility().getCode()}';
			this.init = function() {
				self.load();
			};

			this.load = function() {
				var req = {
					'purchaseOrderCode' : self.purchaseOrderCode
				}
				Uniware.Ajax.postJson("/data/vendor/po/fetch", JSON.stringify(req), function(response) {
					if (response.successful == true) {
						self.purchaseOrderData = response;
						self.render();
						Uniware.Utils.applyHover();
					} else {
						Uniware.Utils.showError(response.errors[0].description);
					}
				});
			};

			this.render = function() {
				$('#purchaseOrderDiv').html(template("purchaseOrderTemplate", self.purchaseOrderData));
				Uniware.Utils.renderComments("#purchaseOrderComments", 'PO-' + (self.isCloning ? self.oldPurchaseOrderData.code : self.purchaseOrderData.code) + '-' + self.facilityCode);
			};

		};

		$(document).ready(function() {
			window.page = new Uniware.poItemsPage();
			window.page.init();
		});
	</script>
	</tiles:putAttribute>
</tiles:insertDefinition>
