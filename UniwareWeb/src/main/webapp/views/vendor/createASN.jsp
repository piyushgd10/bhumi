<%@ include file="/tagIncludes.jsp"%>
<tiles:insertDefinition name=".vendorPage">
	<tiles:putAttribute name="title" value="Uniware - Create Advance Shipping Notice" />
	<tiles:putAttribute name="rightPane">
		<div>
			<form onsubmit="javascript : return false;">
				<div class="greybor headlable ovrhid main-box-head">
					<h2 class="edithead head-textfields">Scan Purchase Order</h2>
					<div class="lfloat">
						<input id="poCode" type="text" size="20" autocomplete="off" value="${param['poCode'] }">
					</div>
					<div id="searching" class="lfloat10 hidden" style="margin-top:5px;">
						<img src="/img/icons/refresh-animated.gif"/>
					</div>
				</div>
			</form>
		</div>
		<div id="purchaseOrderDiv"></div>
		<div id="poItemsDiv"></div>
	</tiles:putAttribute>
	
	<tiles:putAttribute name="deferredScript">
	<script id="purchaseOrder" type="text/html">
	<div class="greybor round_bottom main-boform-cont pad-15-top overhid">
		<table width="100%" border="0" cellspacing="1" cellpadding="3" class="fields-table">
		<tr> 
        	<td width="20%">Purchase Order</td>
	        <td width="20%"><#=obj.code#></td>
    	    <td width="20%">Purchase Order Code</td>
        	<td><#=obj.code#></td>    
		</tr>
		<tr> 
        	<td>Vendor Name</td>
	        <td><#=obj.vendorName#></td>
    	    <td>Purchase Order Status</td>
        	<td><#=obj.statusCode#></td>    
		</tr>
		<# if (obj.customFieldValues && obj.customFieldValues.length > 0) { #>
						<tr>
							<# for(var i=0;i<obj.customFieldValues.length;i++) { var customField = obj.customFieldValues[i]; #>
	 							<td><#=customField.displayName#></td>
								<td>
									<# if (customField.valueType == 'text') { #>
										<input id="<#=customField.fieldName#>" type="text" class="w150 custom" value="<#=customField.fieldValue#>" style="width:70%;"/>
									<# } else if (customField.valueType == 'date') { #>
										<input id="<#=customField.fieldName#>" type="text" class="w150 custom datefield" value="<#= customField.fieldValue ? new Date(customField.fieldValue).toPaddedDate() : '' #>" style="width:70%;"/>
									<# } else if (customField.valueType == 'select') { #>
										<select id="<#=customField.fieldName#>" class="w150 custom" style="width:70%;">
											<# for (var j=0;j<customField.possibleValues.length;j++) { #>
												<option value="<#=customField.possibleValues[j]#>" <#= customField.possibleValues[j] == customField.fieldValue ? selected="selected" : '' #> ><#=customField.possibleValues[j]#></option>
											<# } #>
										</select>
									<# } else if (customField.valueType == 'checkbox') { #>
										<input id="<#=customField.fieldName#>" type="checkbox" class="custom" <#=customField.fieldValue ? checked="checked" : '' #> style="width:70%;"/>
									<# } #>
								</td>
								<# if (i == obj.length- 1 && i % 2 == 0) { #>
									<td></td><td></td>
								<# } else if (i != 0 && i % 2 != 0) { #>
									</tr><tr>
								<# } #>
							<# } #>
						</tr>	
		     	   <# } #>
		</table>
	</div>
	</script>
	
	<script id="poItemsTemplate" type="text/html">
		<div class="greybor headlable round_top ovrhid mar-15-top">
			<h4 class="edithead">Purchase Order Items</h4></div>
			<table class="uniTable po greybor" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<th>Item Type</th>
					<th>Product Code</th>
					<th>Vendor SKU Code</th>
					<th>Pending Quantity</th>
					<th>Quantity</th>
				</tr>

			<# for(var i=0; i<obj.purchaseOrderItems.length; i++){ var poItem = obj.purchaseOrderItems[i]; #>
				<# if (poItem.pendingQuantity > 0) { #>
				<tr>
					<td><#=poItem.itemTypeName#></td>
					<td><#=poItem.itemSKU#></td>
					<td><#=poItem.vendorSkuCode#></td>
					<td><#=poItem.pendingQuantity#></td>
					<td>
						<# if(obj.fullfillAll) { #> 
							<input type="text" class="quantity fit-width" id="index-<#=poItem.itemSKU#>" value="<#=poItem.pendingQuantity#>"/>
 						<# } else { #>
							<input type="text" class="quantity fit-width" id="index-<#=poItem.itemSKU#>"/>
						<# } #>					
					</td>
				</tr>
				<# } #>
			<# } #>
			</table>
		<div class="clear"></div>
		<br />
		<div class="formLeft150 lfloat">Fullfill All Pending</div>
		<div class="lfloat20">
			<input type="checkbox" id="fullfillAll" <#= obj.fullfillAll == true ? checked="checked" : ""#>/>
		</div>
		<div class="clear" />
		<div class="formLeft150 lfloat">Expected Delivery Date</div>
		<div class="lfloat20">
			<input type="text" id="expectedDeliveryDate" autocomplete="off" class="datefield">
		</div>		
		<div class="lfloat20">
			<div class="btn btn-primary lfloat20" id="createASN">Create ASN</div>
		</div>
		<div class="clear"></div>
		</div><br/><br/>
	</script>	
	<script type="text/javascript">
		Uniware.CreateASNPage = function() {
			var self = this;
			this.purchaseOrder = null;
			this.fullfillAll = false;
			this.customFields = ${customFieldsJson};
			this.init = function() {
				$('#poCode').keyup(self.load);
			};
			
			this.load = function(event){
				if (event.which == 13) {
					if ($(this).val() == '') {
						Uniware.Utils.showError('Please enter a order number');
						return;
					}
					$("#searching").removeClass('hidden');
					var asnCode = '${param['asnCode']}';
					Uniware.Ajax.postJson("/data/vendor/po/fetch", JSON.stringify({'purchaseOrderCode': $('#poCode').val()}), function(response) {
						if(response.successful == true) {
							self.purchaseOrder = response;
							self.loadOrderDetails();
						} else {
							Uniware.Utils.showError(response.errors[0].description);
						}
						$("#searching").addClass('hidden');	
					});
				}
			};
			
			this.loadOrderDetails = function(){
				var obj = self.purchaseOrder;
				obj.customFieldValues = self.customFields;
				obj.fullfillAll = self.fullfillAll;
				$('#purchaseOrderDiv').html(template("purchaseOrder", obj));
				self.loadPOItems();
			};
			
			this.loadPOItems = function() {
				var obj = self.purchaseOrder;
				obj.fullfillAll = self.fullfillAll;
				$('#poItemsDiv').html(template("poItemsTemplate", obj));
				$('#createASN').click(self.createASN);
				$('.datefield').datepicker({
					dateFormat : 'dd/mm/yy'
				});
				$('#fullfillAll').click(self.fullfillPendingQuantity);
			};
			
			this.fullfillPendingQuantity = function(){
				if($(this).is(':checked')){
					self.fullfillAll = true;
				} else {
					self.fullfillAll = false;
				}
				self.loadPOItems();
			};
			
			this.createASN = function(){
				var req = {};
				var date = $("#expectedDeliveryDate").val();
				if(date != ""){
					req.expectedDeliveryDate = Date.fromPaddedDate(date).getTime();	
				}
				req.purchaseOrderCode = self.purchaseOrder.code;
				req.asnItems = [];
				Uniware.Utils.addCustomFieldsToRequest(req, $('.custom'));
				$('.quantity').each(function(index){
					if ($(this).val() != '') {
						var asnItem = {
							quantity: $(this).val()
						}
						asnItem.skuCode = $(this).attr('id').split('-')[1];
						req.asnItems.push(asnItem);
					}
				});
				
				Uniware.Ajax.postJson("/data/vendor/asn/create", JSON.stringify(req), function(response){
					if(response.successful){
						Uniware.Utils.addNotification("ASN has been created");
						document.location.href = "/vendor/viewASN?legacy=1&asnCode=" + response.asnCode;
					}else{
						Uniware.Utils.showError(response.errors[0].description);
					}
				}, true);
					
			};
			
		}
		
		$(document).ready(function() {
			window.page = new Uniware.CreateASNPage();
			window.page.init();
			var poCode = $('#poCode').val();
			if(poCode != ""){
				$('#poCode').trigger(Uniware.Event.ENTER_KEYUP);	
			}
			
		});
	</script>
	</tiles:putAttribute>
</tiles:insertDefinition>
