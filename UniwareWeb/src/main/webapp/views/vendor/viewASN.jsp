<%@ include file="/tagIncludes.jsp"%>
<tiles:insertDefinition name=".vendorPage">
	<tiles:putAttribute name="title" value="Uniware - View Advance Shipping Notice" />
	<tiles:putAttribute name="rightPane">
        <div id="ltDiv" class="lb-over">
            <div class="lb-over-inner round_all">
                <div id="tempDiv" style="margin: 40px;line-height: 32px;">
                </div>
            </div>
        </div>
		<div id="asnDiv"></div>
		<div id="asnItemsDiv"></div>
	</tiles:putAttribute>
	
	<tiles:putAttribute name="deferredScript">
        <script type="text/html" id="asnEditTemplate">
            <# var customs = obj.customFieldValues; #>
                <# for(var i=0;customs&&i<customs.length;i++) { var customField = customs[i]; #>
                <div class="formLeft150 lfloat"><#=customField.displayName#></div>
                <div class="formRight lfloat customFieldValueName" >
                    <# if (customField.valueType == 'text') { #>
                        <input id="<#=customField.fieldName#>" type="text" size="15" class="custom" value="<#=customField.fieldValue#>"/>
                        <# } else if (customField.valueType == 'date') { #>
                            <input id="<#=customField.fieldName#>" type="text" size="15" class="custom datefield" value="<#= customField.fieldValue ? new Date(customField.fieldValue).toPaddedDate() : '' #>"/>
                            <# } else if (customField.valueType == 'select') { #>
                                <select id="<#=customField.fieldName#>" class="w150 custom">
                                    <# for (var j=0;j<customField.possibleValues.length;j++) { #>
                                    <option value="<#=customField.possibleValues[j]#>" <#= customField.possibleValues[j] == customField.fieldValue ? selected="selected" : '' #> ><#=customField.possibleValues[j]#></option>
                                    <# } #>
                                </select>
                                <# } else if (customField.valueType == 'checkbox') { #>
                                    <input id="<#=customField.fieldName#>" type="checkbox" class="custom" <#=customField.fieldValue ? checked="checked" : '' #>/>
                                    <# } #>
                </div>
                <div class="clear"></div>
                <# } #>
                    <br /> <br />

                    <div class="formLeft150 lfloat">&#160;</div>
                    <div class="formRight lfloat">
                        <div id="editASNdata" class=" btn btn-small btn-primary lfloat">edit asn</div>
                    </div>
                    <div class="clear"/>
                    <div id="error" class="errorField lfloat20 invisible"></div>
                    <div class="clear"></div>
        </script>
		<script id="asn" type="text/html">
			<div class="greybor headlable round_top ovrhid mar-15-top">
			<h4 class="edithead">ASN</h4></div>
			<div class="greybor round_bottom main-boform-cont pad-15-top overhid">
				<table width="100%" border="0" cellspacing="1" cellpadding="3" class="fields-table">
                <tr>
                    <td colspan="4">
                        <div id="editASN" class="btn btn-small rfloat20"  style="margin-right: 10px;margin-top: 8px;">edit <img src="/img/icons/edit.png"/></div>
                    </td>
                </tr>
				<tr> 
		        	<td width="20%">ASN No.</td>
			        <td width="20%"><#=obj.code#></td>
		    	    <td width="20%">Purchase Order</td>
		        	<td><#=obj.purchaseOrderCode#></td>    
				</tr>
				<tr> 
		        	<td>Expected Delivery Date</td>
					<td><#=(new Date(obj.expectedDeliveryDate)).toDateTime()#></td>
		    	    <td>Created On</td>
		        	<td><#=(new Date(obj.created)).toDateTime()#></td>    
				</tr>
				<tr> 
        			<td>Attachment:</td>
    	   			<td><span id="asnDocuments"></span></td>
	   				<td></td>
		   			<td></td>     
				</tr>
				<# if (obj.customFieldValues && obj.customFieldValues.length > 0) { #>
						<tr>
							<# for(var i=0;i<obj.customFieldValues.length;i++) { var customField = obj.customFieldValues[i]; #>
	 							<td><#=customField.displayName#></td>
								<td>
									<# if (customField.valueType == 'date') { #>
										<#= customField.fieldValue ? new Date(customField.fieldValue).toPaddedDate() : '' #>
									<# } else if (customField.valueType == 'checkbox') { #>
										<input type="checkbox" disabled="disabled" <#=customField.fieldValue ? checked="checked" : '' #>/>
									<# } else { #>
										<#=customField.fieldValue#>
									<# } #>
								</td>
								<# if (i == obj.length- 1 && i % 2 == 0) { #>
									<td></td><td></td>
								<# } else if (i != 0 && i % 2 != 0) { #>
									</tr><tr>
								<# } #>
							<# } #>
						</tr>	
		     	   <# } #>
				</table>
			</div>
		</script>
	
	<script id="asnItems" type="text/html">
		<div class="greybor headlable round_top ovrhid mar-15-top">
		<h4 class="edithead">ASN Items</h4></div>
		<table class="uniTable po greybor" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<th>Item Type</th>
				<th>Product Code</th>
				<th>Vendor SKU Code</th>
				<th>Quantity</th>
			</tr>

			<# for(var i=0; i<obj.asnItems.length; i++){ var asnItem = obj.asnItems[i]; #>
				<tr>
					<td><#=asnItem.itemTypeName#></td>
					<td><#=asnItem.itemSKU#></td>
					<td><#=asnItem.vendorItemSKU#></td>
					<td><#=asnItem.quantity#></td>
				</tr>
			<# } #>
		</table>
	</script>	
	<script type="text/javascript">
		<sec:authentication property="principal" var="user" />
		Uniware.ViewASNPage = function() {
			var self = this;
			this.asnCode;
			this.init = function() {
				Uniware.Ajax.getJson("/data/vendor/asn/view?asnCode=" + self.asnCode, function(response) {
					self.asn = response;
					$('#asnDiv').html(template("asn", response));
					Uniware.Documents("#asnDocuments", 'ASN-' + self.asnCode, '${user.username}', 2);
					$('#asnItemsDiv').html(template("asnItems", response));
                    $('#editASN').click(self.editASN);
				});
			}

            this.editASN = function() {
                $("#tempDiv").html(template("asnEditTemplate", self.asn));
                $(".datefield").datepicker({
                    dateFormat : 'dd/mm/yy'
                });
                $('#editASNdata').click(function(){
                    self.editASNdata();
                });
                Uniware.LightBox.show('#ltDiv');
            };

            this.editASNdata = function() {
                $("#error").addClass('hidden');
                var req = {
                    'advanceShippingNoticeCode' : self.asn.code
                };
                Uniware.Utils.addCustomFieldsToRequest(req, $('.custom'));
                Uniware.Ajax.postJson("/data/vendor/asn/metadata/edit", JSON.stringify(req),function(response){
                    if (response.successful) {
                        Uniware.LightBox.hide();
                        self.init();
                    } else {
                        $('#error').removeClass('hidden').html(response.errors[0].description.split('|')[0]);
                    }
                });
            };

		};
		
		$(document).ready(function() {
			window.page = new Uniware.ViewASNPage();
			var asnCode = '${param['asnCode']}';
			window.page.asnCode = asnCode;
			window.page.init();
		});
	</script>
	</tiles:putAttribute>
</tiles:insertDefinition>
