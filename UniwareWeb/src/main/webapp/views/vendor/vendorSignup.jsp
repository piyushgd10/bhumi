<%@ include file="/tagIncludes.jsp"%>
<tiles:insertDefinition name=".loginPage">
	<tiles:putAttribute name="title" value="Vendor Signup - Warehouse Management" />
	<tiles:putAttribute name="body">
		<div id="userForm"></div>
	</tiles:putAttribute>
	<tiles:putAttribute name="deferredScript">
		<script type="text/javascript">
			Uniware.SignupVendor = function() {
				var self = this;
				this.vendorId = "${param['vendorId']}";
				this.registered = "${registered}";
				this.init = function() {
					if(self.registered == "true"){
						$("#userForm").html("<div style='margin:100px;' class='alert alert-success'>Already registered. Please <a href='${path.http}'>click here</a> to login.</div>");
					}else{
						$("#userForm").html(template("userTemplate", {}));
						$("#create").click(self.createUser);
					}
				};
				
				this.getRequest = function() {
					var req = {};
					req.name = $("#inputEmail").val();
					req.email = $("#inputEmail").val();
					
					req.username = $("#inputEmail").val();
					req.password = $("#inputPassword").val();
					req.confirmPassword = $("#inputConfirmPassword").val();
					req.vendorId = "${param['vendorId']}";
					return req;
				};
				
				this.createUser = function() {
					$('#error').html("");
					var req = self.getRequest();
					Uniware.Ajax.postJson('/data/signup/vendorSignup', JSON.stringify(req), function(response) {
						if (response.successful == false) {
							Uniware.Utils.showError(response.errors[0].description);
						} else {
							$("#userForm").html("<div style='margin:100px;' class='alert alert-success'>Registration successful. Please <a href='${path.http}'>click here</a> to login.</div>");
						}
					});
					return false;
				};
			};

			$(document).ready(function() {
				window.page = new Uniware.SignupVendor();
				window.page.init();
			});
		</script>
		<script type="text/html" id="userTemplate">
		<div style="margin:100px;">
		<h2 style="padding-left:175px;">Signup for : <span style="color:#205081">${vendor.name} - ${vendor.code}</span></h2><br/>
		<form class="bs-docs-example form-horizontal" onsubmit="javascript : return false;">
			<div class="control-group">
			  <label class="control-label" for="inputEmail">Email</label>
			  <div class="controls">
					<div class="input-prepend">
             			 <span class="add-on"><i class="icon-envelope" style="font-size:18px;"></i></span>
             			 <input class="span2" id="inputEmail" type="text" placeholder="Email" style="width:300px;">
           			 </div>
			  </div>
			</div>
			<div class="control-group">
			  <label class="control-label" for="inputPassword">Password</label>
			  <div class="controls">
					<div class="input-prepend">
             			 <span class="add-on"><i class="icon-key" style="font-size:18px;"></i></span>
             			 <input class="span2" id="inputPassword" type="password" placeholder="Password" style="width:300px;">
           			 </div>
			  </div>
			</div>
			<div class="control-group">
			  <label class="control-label" for="inputConfirmPassword">Confirm Password</label>
			  <div class="controls">
					<div class="input-prepend">
             			 <span class="add-on"><i class="icon-key" style="font-size:18px;"></i></span>
             			 <input class="span2" id="inputConfirmPassword" type="password" placeholder="Confirm Password" style="width:300px;">
           			 </div>
			  </div>
			</div>
			<br/>
			<div class="control-group">
			  <div class="controls">
			    <button id="create" type="submit" class="btn btn-primary bold" style="padding:6px 40px;font-size:18px;">Sign up</button>
			  </div>
			</div>
	     </form>
		 </div>
		</script>
	</tiles:putAttribute>
</tiles:insertDefinition>
