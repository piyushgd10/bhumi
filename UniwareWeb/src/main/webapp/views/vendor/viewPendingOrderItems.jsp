<%@ include file="/tagIncludes.jsp"%>
<tiles:insertDefinition name=".vendorPage">
	<tiles:putAttribute name="title" value="Uniware - Pending Items" />
	<tiles:putAttribute name="rightPane">
		<div id="pageBar">
			<div class="pageHeading"><span class="mainHeading">Pending Purchase Order Items</span></div>
			<div class="pageControls"></div>
			<div class="clear"></div> 
		</div>
		<div id="dataTable">
			<table id="flexme1" style="display: none"></table>
		</div>
	</tiles:putAttribute>
	<tiles:putAttribute name="deferredScript">
	<script type="text/javascript" src="${path.js('jquery/jquery.dataTables.min.js')}"></script>
	<script type="text/javascript">
		Uniware.ViewPendingItemsPage = function() {
			var self = this;
			this.name = 'DATATABLE VENDOR PENDING ITEMS';
			this.pageConfig = null;
			this.table = null;
			this.init = function(){
				this.pageConfig = new Uniware.PageConfig(this.name,'#flexme1');
				Uniware.Utils.applyHover('#dataTable');
			};
		};
						
		$(document).ready(function() {
			window.page = new Uniware.ViewPendingItemsPage();
			window.page.init();
		});
	</script>
	</tiles:putAttribute>
</tiles:insertDefinition>
