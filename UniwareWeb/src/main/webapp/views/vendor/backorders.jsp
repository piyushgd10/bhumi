<%@ include file="/tagIncludes.jsp"%>
<tiles:insertDefinition name=".procurePage">
	<tiles:putAttribute name="title" value="Uniware - Back Orders - Waiting for Inventory" />
	<tiles:putAttribute name="rightPane">
		<form onsubmit="javascript : return false;">
			<div class="greybor headlable ovrhid main-box-head">
				<h2 class="edithead head-textfields">Back Orders - Waiting for Inventory</h2>
			</div>
			<div class="round_bottom ovrhid pad-15">
				<div class="lfloat20">
					<div class="searchLabel">Item Type Name/Code Contains</div>
					<input type="text" id="itemTypeName" placeHolder = "Enter item name/SKU Code" size="30" autocomplete="off" />
				</div>
				<div class="lfloat20">
					<div class="searchLabel">Category</div>
					<select id="category" style="width:150px;">
					<option value="">--ALL--</option>
					<c:forEach items="${cache.getCache('categoryCache').categories}" var="category">
						<option value="${category.code }">${category.name}</option>
					</c:forEach>
					</select>
				</div>
				<div class="lfloat20" style="margin-top:20px;">	
					<input id="searchBackOrders" value="search" type="submit" class=" btn btn-primary" />
				</div>
				<div id="searching" class="lfloat10 hidden" style="margin-top:25px;">
					<img src="/img/icons/refresh-animated.gif"/>
				</div>
			</div>
		</form>
		<div id="suggestionBody" class="greybor round_bottom"  style="padding:10px;">
		</div>
	</tiles:putAttribute>
	<tiles:putAttribute name="deferredScript">
	<script type="text/html" id="suggestionTemplate">
		<# if(obj.elements != null) {#>
			<table class="uniTable mar-15-top" cellpadding="0" cellspacing="0"">
				<tr>
					<th width="30%">Item Name</th>
					<th width="15%">Item SKU</th>
					<th width="10%">Vendor SKU</th>
					<th width="8%">Quantity</th>
					<th>Unit Price</th>
					<th>Color</th>
					<th>Brand</th>
					<th>Size</th>
				</tr>
				<# for (var j=0;j<obj.elements.length;j++) {var poItem = obj.elements[j];var qtyToAdd = poItem.waitingQuantity;#>
 				<tr>
					<td>
						<#=poItem.name#>
						<#=Uniware.Utils.getItemDetail(poItem.itemTypeImageUrl, poItem.itemTypePageUrl)#>
					</td>
					<td class="ovrhid" title="<#=poItem.skuCode#>"><#=poItem.skuCode#></td>
					<td class="ovrhid"><#=poItem.vendorItemTypes[0].vendorSkuCode#></td>
					<td align="center"><#=poItem.waitingQuantity#></td>
					<td><#=poItem.vendorItemTypes[0].unitPrice#></td>
					<td><#=poItem.color#></td>
					<td><#=poItem.brand#></td>
					<td><#=poItem.size#></td>
				</tr>
				<# } #>
			</table><br/>
		<# } else { #>
			<div class="f15" style="padding:25px;"><em>No Back Order Items Found to Add to Cart</em></div>
		<# } #>
	</script>	
	<script type="text/javascript">
		Uniware.BackOrderPage = function() {
			var self = this;
			this.purchaseCart = null;
			this.cartQuantity = {};
			this.backOrders = null;
			this.vendor = null;
			
			this.init = function() {
				$('#searchBackOrders').click(self.searchBackOrders);
			};
			
			this.searchBackOrders = function(){
				$("#searching").removeClass('hidden');
				var req={
						itemTypeName: $('#itemTypeName').val(),
						categoryCode: $('#category').val()					
				}
				
				Uniware.Ajax.postJson("/data/vendor/backorders/get", JSON.stringify(req), function(response) {
					if (response.elements.length > 0) {
						self.backOrders = response;
						self.backOrders.cart = self.cartQuantity;
						$("#suggestionBody").html(template("suggestionTemplate", self.backOrders));
						Uniware.Utils.applyHover();
					} else {
						$("#suggestionBody").html(template("suggestionTemplate", {}));
					}
					$("#searching").addClass('hidden');
				});
				
			};
		};
		
		$(document).ready(function() {
			window.page = new Uniware.BackOrderPage();
			window.page.init();
			$('#searchBackOrders').trigger('click');
		});
	</script>
	</tiles:putAttribute>
</tiles:insertDefinition>			
