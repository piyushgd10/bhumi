<%@ include file="/tagIncludes.jsp"%>
<tiles:insertDefinition name=".inflowPage">
	<tiles:putAttribute name="title" value="Uniware - Create Labels for Purchase Order" />
	<tiles:putAttribute name="rightPane">
		<div>
		<form onsubmit="javascript : return false;">
			<div class="greybor headlable ovrhid main-box-head">
				<h2 class="edithead head-textfields">Scan Purchase Order</h2>
				<div class="lfloat">
					<input id="poCodePrefix" type="text" size="20" autocomplete="off" value="${cache.getCache('sequenceCache').getPrefixByName('PURCHASE_ORDER') }" style="border-radius: 4px 0px 0px 4px; width: 150px; background-color: lightgrey;">
					<input id="poCode" type="text" size="20" autocomplete="off" value="" style="border-radius: 0px 4px 4px 0px; margin-left: -5px; border-left-width: 0px;">
				</div>
				<div id="searching" class="lfloat10 hidden" style="margin-top:5px;">
					<img src="/img/icons/refresh-animated.gif"/>
				</div>
			</div>
		</form>
		</div>
		<div id="poItemsDiv" class="noprint"></div>
	</tiles:putAttribute>
	<tiles:putAttribute name="deferredScript">
	<script id="poItemsTemplate" type="text/html">
		<table class="fields-table">
			<tr> 
       			<td>Purchase Order No.</td>
      			<td><#=obj.code#></td>
				<td>Purchase Order Code</td>
	  			<td><#=obj.code#></td>
			<tr>
      	 		<td>Vendor Name</td>
      			<td><#=obj.vendorName#></td>    
	  			<td>Status</td>
	  			<td><#=obj.statusCode#></td>
  			</tr>
		</table>
		<div class="greybor headlable round_top ovrhid">
		<h4 class="edithead">Purchase Order Items</h4></div>
		<table class="uniTable po greybor" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<th>Item Type</th>
				<th>Product Code</th>
				<th>Vendor SKU Code</th>
				<th width="100">Quantity</th>
				<th width="100">Pending Quantity</th>
				<th width="100">Label Quantity</th>
				<th width="100">Action</th>
			</tr>

			<# for(var i=0; i<obj.purchaseOrderItems.length; i++){ var poItem = obj.purchaseOrderItems[i]; #>
			<tr>
				<td>
					<#=poItem.itemTypeName#>
					<#=Uniware.Utils.getItemDetail(poItem.itemTypeImageUrl, poItem.itemTypePageUrl)#>
				</td>
				<td><#=poItem.itemSKU#></td>
				<td style="font-weight:bold;"><#=poItem.vendorSkuCode#></td>
				<td><#=poItem.quantity#></td>
				<td><#=poItem.pendingQuantity#></td>
				<td><input type="text" id="labelQty-<#=i#>" style="width:50%"/></td>
				<td><div class="btn btn-mini skuLabels" id="label-<#=i#>">create labels</div></td>
			</tr>
			<# } #>
		</table>	
		<div><br/><br/>
		<# if (obj.statusCode == 'APPROVED') { #>
			<div id="createLabels" class=" btn btn-primary lfloat">create labels</div>
		<# } #>
	</script>
	<script type="text/javascript">
		Uniware.createPOLabelPage = function() {
			var self = this;
			this.purchaseOrder = null;
			
			this.init = function() {
				$('#poCode').focus();
				$('#poCode, #poCodePrefix').keyup(function(event){
					if (event.which == 13) {
						var purchaseOrderCode = $('#poCodePrefix').val() + $('#poCode').val();
						self.load(purchaseOrderCode);
					}
				});
			};
			
			this.load = function(purchaseOrderCode) {
				$("#searching").removeClass('hidden');
				$('#poItemsDiv').html("");

				Uniware.Ajax.postJson("/data/vendor/po/fetch", JSON.stringify({'purchaseOrderCode': purchaseOrderCode}), function(response) {
					if(response.successful == true) {
						self.purchaseOrder = response;
						$('#poItemsDiv').html(template("poItemsTemplate", self.purchaseOrder));
						$('#poCode').val('');
						$('#createLabels').click(self.createLabels);
						$('div.skuLabels').click(self.createSkuLabels);
						Uniware.Utils.applyHover();
					} else {
						Uniware.Utils.showError(response.errors[0].description);
					}
					$("#searching").addClass('hidden');	
				});
			};
			
			this.createSkuLabels = function() {
				var poItemIndex = parseInt($(this).attr('id').split('-')[1]);
				poItem = self.purchaseOrder.purchaseOrderItems[poItemIndex];
				var req = {
                    'itemSkuCode' : poItem.itemSKU,
					'quantity' : $('#labelQty-' + poItemIndex).val(),
					'vendorSkuCode': poItem.vendorSkuCode,
					'vendorId' : self.purchaseOrder.vendorId
				};
				
				Uniware.Ajax.postJson("/data/inflow/item/createLabels", JSON.stringify(req), function(response) {
					if (response.successful) {
						Uniware.Utils.addNotification(response.itemCodes.length + ' Labels have been created');
						Uniware.Utils.printIFrame('/inflow/items/print/labels/'+ response.itemCodes.join(',') + '?legacy=1');
					} else {
						Uniware.Utils.showError(response.errors[0].description);
					}
				});
			};
			
			this.createLabels = function() {
				Uniware.Utils.printIFrame('/jabong/inflow/items/print?legacy=1&code='+ self.purchaseOrder.code);	
			};
			
		}
		
		$(document).ready(function() {
			window.page = new Uniware.createPOLabelPage();
			window.page.init();
		});
	</script>
	</tiles:putAttribute>
</tiles:insertDefinition>