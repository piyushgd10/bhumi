<%@ include file="/tagIncludes.jsp"%>
<tiles:insertDefinition name=".materialPage">
	<tiles:putAttribute name="title" value="Uniware - Returns Debit Notes" />
	<tiles:putAttribute name="rightPane">
		<div class="headlable ovrhid main-box-head">
			<h2 class="edithead head-textfields">Returns Debit Notes</h2>
		</div>
		<form onsubmit="javascript: return false;">
			<div class="main-boform-cont ovrhid">
				<div class="lfloat">
					<div class="searchLabel">Status</div>
					<select id="status" class="w100">
						<option value="" selected="selected">-- ALL --</option>
						<c:set var="statuses" value="${cache.getCache('statuscache').gatePassStatuses}"></c:set>
						<c:forEach items="${statuses}" var="status">
							<option value="${status}">
								<c:out value="${status}" />
							</option>
						</c:forEach>
					</select>
				</div>	
				<div class="lfloat20">
					<div class="searchLabel">Created on</div>
					<input type="text" id="createdOn"  size="20" autocomplete="off" class="datefield pull-right dateRange">
				</div>
				<div class="lfloat20">
					<div class="searchLabel">&#160;</div>
					<input type="submit" class=" btn btn-primary lfloat" id="search" value="search gatepass"/>
				</div>
				<div class="clear"></div>
				<table id="dataTable" class="dataTable"></table>
			</div>
		</form>
	</tiles:putAttribute>
	<tiles:putAttribute name="deferredScript">
	<script type="text/javascript" src="${path.js('jquery/jquery.dataTables.min.js')}"></script>
	<script type="text/html" id="searchGatepassRow">
		<td><a href="/vendor/createGatepass?legacy=1&gatePassCode=<#=obj.code#>"><#=obj.code#></a></td>
		<td><#=obj.username#></td>
		<td><#=obj.statusCode#></td>
		<td><#=obj.type#></td>
		<td><#=obj.toParty#></td>
		<td><#=obj.reference#></td>
		<td><#=new Date(obj.created).toDateTime()#></td>
	</script>
	<script type="text/javascript">
		Uniware.SearchGatepassPage = function() {
			var self = this;
			this.table = null;
			
			this.init = function() {
				$('.dateRange').daterangepicker();
				$("#search").click(function(event) {
					var pipeline = new Uniware.pipelineData();

					var requestObject = {
						statusCode : $('#status').val(),
						usernameContains : $('#createdBy').val(),
						toParty : $('#toPartyName').val()
					}
					var dateRange = $("#createdOn").val();
					if (dateRange != '') {
						requestObject.fromDate = Date.fromPaddedDate(dateRange.substring(0, 10)).getTime();
						if (dateRange.length > 10) {
							requestObject.toDate = Date.fromPaddedDate(dateRange.substring(13));
						} else {
							requestObject.toDate = Date.fromPaddedDate(dateRange.substring(0, 10));
						}
						requestObject.toDate = requestObject.toDate.setDate(requestObject.toDate.getDate()+1);
					}
					pipeline.requestObject = requestObject;

					$("#searching").removeClass('hidden');
					var dtEL = $('#dataTable');
					if (Uniware.Utils.isDataTable(dtEL)) {
						var dtTable = dtEL.dataTable();
						dtTable.fnDestroy();
	                    dtTable = undefined;
					}
					
					self.table = dtEL.dataTable({
						"bServerSide": true,
						"sAjaxSource": "/data/vendor/gatepass/search",
						"bAutoWidth" : false,
						"bSort" : false,
						"bFilter": false,
						"sPaginationType": "full_numbers",
						"aoColumns" : [ {
							"sTitle" : "Code",
							"mDataProp" : "code"
						}, {
							"sTitle" : "Username",
							"mDataProp" : "username"
						}, {
							"sTitle" : "Status",
							"mDataProp" : "statusCode"
						}, {
							"sTitle" : "Type",
							"mDataProp" : "type"
						}, {
							"sTitle" : "To Party",
							"mDataProp" : "toParty"
						}, {
							"sTitle" : "Reference",
							"mDataProp" : "reference"
						}, {
							"sTitle" : "Created at",
							"mDataProp": function (aData) {
								return '';
							}
						}],
						"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull) {
							$(nRow).html(template("searchGatepassRow", aData));
							return nRow;
						},
						"fnServerData": pipeline.fnDataTablesPipeline
					});
					
					$("#dataTable tbody").on('mouseover mouseout', 'tr', function(event) {
						$(this).find('div.action').toggleClass('hidden');
					});
					$("#searching").addClass('hidden');
					Uniware.Utils.applyHover('#dataTable');
				});
			}
		};
						
		$(document).ready(function() {
			window.page = new Uniware.SearchGatepassPage();
			window.page.init();
			$('#search').trigger('click');
		});
	</script>
	</tiles:putAttribute>
</tiles:insertDefinition>
