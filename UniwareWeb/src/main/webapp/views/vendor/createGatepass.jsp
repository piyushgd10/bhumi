<%@ include file="/tagIncludes.jsp"%>
<tiles:insertDefinition name=".materialPage">
	<tiles:putAttribute name="title" value="Uniware - GatePass Create/View" />
	<tiles:putAttribute name="rightPane">
		<div class="greybor headlable ovrhid main-box-head">
			<h2 class="edithead head-textfields">View GatePass</h2>
		</div>
		<div class="clear"></div>
		<div id="gatePassDiv" class="greybor round_bottom main-boform-cont ovrhid"  style="padding:20px;"></div>
		
	</tiles:putAttribute>
	
	<tiles:putAttribute name="deferredScript">
	<script id="gatePassItemsTemplate" type="text/html">
		<div class="greybor round_all overhid">	
		<table width="100%" border="0" cellspacing="1" cellpadding="3" class="fields-table">
		<# if (obj.statusCode == 'CREATED') { #>
			<tr>
    			<td colspan="4">
					<div id="editItem" class="btn btn-small rfloat20">edit <img src="/img/icons/edit.png"/></div>
				</td>
  			</tr>
		<# } #>
		<tr> 
        	<td>GatePass Number</td>
	        <td class="bold f15"><div class="barcode"><#=obj.code#></div></td>
    	    <td>Created by</td>
        	<td class="bold f15"><#=obj.username#></td>
		</tr>
		<tr> 
        	<td>Status</td>
	        <td><#=obj.statusCode#></td>
    	    <td>Created at</td>
        	<td><#=new Date(obj.created).toDateTime()#></td>    
		</tr>
		<tr>	 
        	<td>Reference No.</td>
	        <td><#=obj.reference#></td>
    	    <td>To Party</td>
        	<td><#=obj.toPartyName#></td>    
		</tr>
		<tr>	 
        	<td>Purpose</td>
	        <td colspan="3"><#=obj.purpose#></td>
		</tr>
		<# var totalValue = 0;var quantity = 0; #>
		<# for(var i=0; i<obj.gatePassItemDTOs.length; i++){ var gatePassItemDTO = obj.gatePassItemDTOs[i]; #>
			<# totalValue = totalValue + gatePassItemDTO.total; #>
			<# quantity = quantity + gatePassItemDTO.quantity; #>
		<# } #>
		<tr>	 
        	<td>Quantity</td>
	        <td><#=quantity#></td>
    	    <td>Type</td>
        	<td><#=obj.type#></td>    
		</tr>
		<# if (Uniware.Utils.traceabilityLevel == 'ITEM') { #>
		<tr>	 
        	<td>Total Value</td>
	        <td><#=totalValue#></td>
    	    <td></td>
        	<td></td>    
		</tr>
		<# } #>
			<# if (obj.customFieldValues && obj.customFieldValues.length > 0) { #>
			<tr>
			<# for(var i=0;i<obj.customFieldValues.length;i++) { var customField = obj.customFieldValues[i]; #>
	 			<td><#=customField.displayName#></td>
				<td>
					<# if (customField.valueType == 'date') { #>
						<#= customField.fieldValue ? new Date(customField.fieldValue).toPaddedDate() : '' #>
					<# } else if (customField.valueType == 'checkbox') { #>
						<input type="checkbox" disabled="disabled" <#=customField.fieldValue ? checked="checked" : '' #>/>
					<# } else { #>
						<#=customField.fieldValue#>
					<# } #>
				</td>
				<# if (i == obj.customFieldValues.length- 1 && i % 2 == 0) { #>
					<td></td><td></td>
				<# } else if (i != 0 && i % 2 != 0) { #>
					</tr><tr>
				<# } #>
			<# } #>
			</tr>				
     	   <# } #>
		</table>	
		</div>
		<# if (obj.statusCode == 'CREATED' && Uniware.Utils.traceabilityLevel == 'ITEM') { #>
		<br/>
		<div class="pageLabel">Scan Item <input type="text" id="itemCode"/>
		<div id="items"></div>
		<br/>
		<# } #>
		<div class="greybor headlable round_top ovrhid">
			<h4 class="edithead">GatePass Items</h4>
		</div>
		<div class="greybor round_bottom form-edit-table-cont">
			<table class="uniTable" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<th width="80">SL. No.</th>
					<# if (Uniware.Utils.traceabilityLevel == 'ITEM') { #>
						<th>Item Code</th>
						<th>GRN No.</th>
						<th>Total</th>
					<# } #>
					<th>Item SKU</th>
					<th>Item Type</th>
					<th>Status</th>
					<th>Quantity</th>
					<# if (Uniware.Utils.traceabilityLevel != 'ITEM') { #>
					<th>Action</th>
					<# } #>
				</tr>
				<# for(var i=0; i<obj.gatePassItemDTOs.length; i++){ var gatePassItemDTO = obj.gatePassItemDTOs[i]; #>
					<tr id='tr-<#=i#>'>
						<td><#=(i+1)#></td>
						<# if (Uniware.Utils.traceabilityLevel == 'ITEM') { #>
							<td><#=gatePassItemDTO.itemCode#></td>
							<td><#=gatePassItemDTO.inflowReceiptCode#></td>
							<td><#=gatePassItemDTO.total#></td>
						<# } #>
						<td><#=gatePassItemDTO.itemTypeSKU#></td>
						<td><#=gatePassItemDTO.itemTypeName#></td>
						<td><#=gatePassItemDTO.gatePassItemStatus#></td>
						<td><#=gatePassItemDTO.quantity#></td>
						<# if (Uniware.Utils.traceabilityLevel != 'ITEM') { #>
						<td></td>
						<# } #>
					</tr>
				<# } #>
				<# if (obj.statusCode == 'CREATED' && Uniware.Utils.traceabilityLevel != 'ITEM') { #>
					<tr id='tr-<#=i#>'>
						<td><#=(i+1)#></td>
						<td><input id="itemType-<#=i#>" type="text" value="" class="itemType field-width"></input></td>
						<td id="itemTypeName-<#=i#>" class="itemTypeName"></td>
						<td></td>
						<td><input id="qty-<#=i#>" type="text" class="quantity field-width" style="width:80%"></input></td>
						<td>
							<div id="save-<#=i#>" class="btn  lfloat save">save</div>
						</td>
					</tr>
				<# } #>
			</table>
			<a href="/vendor/gatepass/print/<#=obj.code#>" target="_blank">
				<div id="gatepass-<#=obj.code#>" class="rfloat btn btn-small printgp" style="margin-right: 10px;margin-top: 8px;">print</div>	
			</a>
		</div>
		<# if (obj.gatePassItemDTOs.length > 0 && obj.statusCode == 'CREATED') { #>
			<br/>
			<div id="completeGatePass" class=" btn btn-primary lfloat">Complete GatePass</div>
			<div class="clear"></div>
		<# } #>
	</script>
	<script type="text/javascript">
		<sec:authentication property="principal" var="user" />
		Uniware.Utils.traceabilityLevel = "${configuration.getConfiguration('systemConfiguration').traceabilityLevel}";
		Uniware.createGatePassPage = function() {
			var self = this;
			this.facilities = [];
			<c:forEach var="facility" items="${cache.getCache('facilityCache').facilities}">
				<c:if test="${facility.id != user.currentFacilityId}">
					this.facilities.push({
						'displayName' : '${facility.displayName}',
						'code' : '${facility.code}'
					});
				</c:if>
			</c:forEach>
			this.gatePassCode;
			this.currentItem;
			this.partyCode;
			this.gatePassCustomFieldsMetadata =  ${configuration.getConfiguration('customFieldsConfiguration').getCustomFieldsByEntityJson('com.uniware.core.entity.OutboundGatePass')};
			this.init = function() {
				$('#gatePassCode').keyup(self.load);
			};
			
			
			this.load = function(event) {
				if (event.which == 13 && $('#gatePassCode').val() != '') {
					self.fetchGatePass($('#gatePassCode').val());
				}
			};
			
			this.fetchGatePass = function(code) {
				$('#gatePassItems').html("");
				var req = {
					'code' : code	
				};
				Uniware.Ajax.postJson("/data/vendor/gatepass/get", JSON.stringify(req), function(response) {
					if(response.successful == true) {
						self.gatePassCode = response.gatePassDTO.code;
						self.gatePass = response.gatePassDTO;
						$('#gatePassDiv').html(template("gatePassItemsTemplate", response.gatePassDTO));
						$(".datefield").datepicker({
							dateFormat : 'dd/mm/yy'
						});
						Uniware.Utils.barcode();
						$("input.itemType").focus().autocomplete({
					    	minLength: 4,	
					    	mustMatch : true,
							autoFocus: true,
							source: function( request, response ) {
								Uniware.Ajax.getJson("/data/lookup/itemTypes?keyword=" + request.term, function(data) {
									response( $.map( data, function( item ) {
										return {
											label: item.name + " - " + item.itemSKU,
											value: item.itemSKU,
											itemType: item
										}
									}));
								});
							},
							select: function( event, ui ) {
								var tr = $(event.target).parents('tr');
								var itemType = ui.item.itemType;
								$(tr[0]).find('.itemTypeName').html(itemType.name);
								$(tr[0]).find('.itemType').html(itemType.name);
							}
					    }).keyup(function(event) {
							if (event.which == 13) {
								var tr = $(event.target).parents('tr');
								$(tr[0]).find('input.quantity').select();
							}
						});;
					} else {
						Uniware.Utils.showError(response.errors[0].description);
					}
				}); 
			};
			
			
		};
					
		$(document).ready(function() {
			window.page = new Uniware.createGatePassPage();
			window.page.init();
			var gatePassCode = '${param['gatePassCode']}';
			if (gatePassCode != '') {
				window.page.fetchGatePass(gatePassCode);
			}
		});
	</script>
	</tiles:putAttribute>
</tiles:insertDefinition>
