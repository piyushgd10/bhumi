<%@ include file="/tagIncludes.jsp"%>
<tiles:insertDefinition name=".vendorPage">
	<tiles:putAttribute name="title" value="Uniware - Search Purchase Order" />
	<tiles:putAttribute name="rightPane">
		<div>
			<form onsubmit="javascript : return false;">
				<div class="greybor headlable ovrhid main-box-head">
					<h2 class="edithead head-textfields">Search Purchase Order</h2>
				</div>
				<div class="round_bottom ovrhid pad-15">
					<div class="lfloat20">
						<div class="searchLabel">Purchase Order Code</div>
						<input type="text" id="purchaseOrderCode" size="20" autocomplete="off" />
					</div>
					<div class="lfloat20">
						<div class="searchLabel">Item Type Name/Code Contains</div>
						<input type="text" id="itemTypeName" size="30" autocomplete="off" />
					</div>
					
					<div class="clear"></div>
					<div class="lfloat20">
						<div class="searchLabel">Category</div>
						<select id="categoryId">
						<option value="">--ALL--</option>
						<c:forEach items="${cache.getCache('categoryCache').categories}" var="category">
							<option value="${category.id}">${category.name}</option>
						</c:forEach>
						</select>
					</div>
					
					<div class="lfloat20">
						<div class="searchLabel">Created Date</div>
						<input type="text" id="dateRange" size="20" class="w200 datefield" autocomplete="off" />
					</div>	
					<div class="lfloat20">
						<div class="searchLabel">Approval Date</div>
						<input type="text" id="approvedDateRange" size="20" class="w200 datefield" autocomplete="off" />
					</div>

					<div class="lfloat20">
						<div class="searchLabel">Status</div>
						<select id="poStatus" style="width:150px;">
						<option value="">--ALL--</option>
						<c:forEach items="${cache.getCache('statuscache').purchaseOrderStatuses}" var="status">
							<option value="${status}" <c:if test="${status == 'APPROVED' }">selected</c:if>>${status}</option>
							
						</c:forEach>
						</select>
					</div>
						
					<div class="lfloat20" style="margin-top:20px;">	
						<input id="search" value="search" type="submit" class="btn btn-primary" />
					</div>
				<div id="searching" class="lfloat10 hidden" style="margin-top:25px;">
					<img src="/img/icons/refresh-animated.gif"/>
				</div>
				</div>
			</form>
		</div>
		<form onsubmit="javascript: return false;">
			<table id="dataTable" class="dataTable"></table>
		</form>
	</tiles:putAttribute>
	<tiles:putAttribute name="deferredScript">
	<script type="text/javascript" src="${path.js('jquery/jquery.dataTables.min.js')}"></script>
	<script id="purchaseOrderRow" type="text/html">
			<td><a href="/vendor/poItems?legacy=1&poCode=<#=obj.code#>"</a><#=obj.code#></td>
			<td><#=obj.statusCode#></td>
			<td><#=obj.vendorAgreement#></td>
			<td><#= (new Date(obj.created)).toDateTime()#></td>
			<td><#= obj.approved ? (new Date(obj.approved)).toDateTime() : ''#></td>
	</script>
	<script type="text/javascript">
		Uniware.SearchPOPage = function() {
			
			var self = this;
			this.table = null;
			this.purchaseOrders = null;
			
			this.init = function() {
				$('#dateRange').daterangepicker();
				$('#approvedDateRange').daterangepicker();
				$('#search').click(self.search);
			};
							
			this.search = function() {	
				$("#searching").removeClass('hidden');
				var requestObject={
					itemTypeName: $('#itemTypeName').val(),
					categoryId: $('#categoryId').val(),
					status: $('#poStatus').val(),
					purchaseOrderCode: $('#purchaseOrderCode').val()
				}
				
				var dateRange = $("#dateRange").val();
				if (dateRange != '') {
					requestObject.fromDate = Date.fromPaddedDate(dateRange.substring(0, 10)).getTime();
					if (dateRange.length > 10) {
						requestObject.toDate = Date.fromPaddedDate(dateRange.substring(13));
					} else {
						requestObject.toDate = Date.fromPaddedDate(dateRange.substring(0, 10));
					}
					requestObject.toDate = requestObject.toDate.setDate(requestObject.toDate.getDate()+1);
				}
				
				var approvedDateRange = $("#approvedDateRange").val();
				if (approvedDateRange != '') {
					requestObject.approvedFromDate = Date.fromPaddedDate(approvedDateRange.substring(0, 10)).getTime();
					if (approvedDateRange.length > 10) {
						requestObject.approvedToDate = Date.fromPaddedDate(approvedDateRange.substring(13));
					} else {
						requestObject.approvedToDate = Date.fromPaddedDate(approvedDateRange.substring(0, 10));
					}
					requestObject.approvedToDate = requestObject.approvedToDate.setDate(requestObject.approvedToDate.getDate()+1);
				}
				
				var pipeline = new Uniware.pipelineData();
				pipeline.requestObject = requestObject;
				
				var dtEL = $('#dataTable');
				if (Uniware.Utils.isDataTable(dtEL)) {
					var dtTable = dtEL.dataTable();
					dtTable.fnDestroy();
	                dtTable = undefined;
				}
				
				self.table = dtEL.dataTable({
					"bServerSide": true,
					"sAjaxSource": "/data/vendor/po/search",
					"bAutoWidth" : false,
					"bSort" : false,
					"bFilter": false,
					"sPaginationType": "full_numbers",
					"aoColumns" : [ {
						"sTitle" : "Purchase Order Code",
						"mDataProp" : 'code',
						"sWidth" : "300px"
					},{ 
						"sTitle" : "Status",
						"mDataProp" : "statusCode",
						"sWidth" : "100px"
					},{
						"sTitle" : "Agreement",
						"mDataProp" : "vendorAgreement"
					},{
						"sTitle" : "Created On",
						"mDataProp": 'created'
					},{
						"sTitle" : "Approved On",
						"mDataProp": 'approved'
					}],
					"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
						$(nRow).html(template("purchaseOrderRow", aData));
						return nRow;
					},
					"fnServerData": pipeline.fnDataTablesPipeline
				}, true);
				
				$("#searching").addClass('hidden');
			};
		};
						
		$(document).ready(function() {
			window.page = new Uniware.SearchPOPage();
			window.page.init();
			$("#search").trigger('click');
		});
	</script>
	</tiles:putAttribute>
</tiles:insertDefinition>
