<%@ include file="/tagIncludes.jsp"%>
<tiles:insertDefinition name=".shippingPage">
	<tiles:putAttribute name="title" value="Uniware - Search Inventory" />
	<tiles:putAttribute name="rightPane">
		 <div id="pageBar">
			<div class="pageHeading"><span class="mainHeading">Inventory<span class="pipe">/</span></span><span class="subHeading"></span></div>
			<div class="pageControls"></div>
			<div class="clear"></div> 
		</div>
		<table id="flexme1" style="display: none"></table>
	</tiles:putAttribute>
	<tiles:putAttribute name="deferredScript">
    <sec:authentication property="principal" var="user" />
	<script type="text/javascript" src="${path.js('jquery/jquery.dataTables.min.js')}"></script>
	<script type="text/javascript">
		Uniware.SearchInventoryPage = function() {
			var self = this;
			this.name = 'DATATABLE VENDOR SEARCH INVENTORY';
			this.pageConfig = null;
			this.table = null;
			this.datatableViews = ${user.getDatatableViewsJson("DATATABLE VENDOR SEARCH INVENTORY")};
			this.init = function(){
				self.pageConfig = new Uniware.PageConfig(this.name,'#flexme1',this.datatableViews[0],{
					showCheckbox : true,
					showSerialNumber: true
				});
				if(this.datatableViews.length){
					this.headConfig = new Uniware.HeadConfig(this.datatableViews,function(datatableObject){
						self.pageConfig.setView(datatableObject);
					},self.pageConfig);
				}
			};
		};
						
		$(document).ready(function() {
			window.page = new Uniware.SearchInventoryPage();
			window.page.init();
		});
	</script>
	</tiles:putAttribute>
</tiles:insertDefinition>
