<%@ include file="/tagIncludes.jsp"%>
<tiles:insertDefinition name="admin.catalogPage">
	<tiles:putAttribute name="title" value="Uniware - Categories" />
	<tiles:putAttribute name="rightPane">
		<div id="pageBar">
			<div class="pageHeading" id="createCategoryLink"><span class="mainHeading">Categories<span class="pipe">/</span></span><span class="subHeading"></span></div>
			<div class="pageControls"></div>
            <div id="create" class="btn btn-small btn-primary rfloat" style="margin: 10px 5px;">add category</div>
			<div class="clear"></div> 
		</div>
		<div id="createCategoryDiv" class="lb-over">
			<div class="lb-over-inner round_all">
				<div style="margin: 40px;line-height: 30px;">
					<div class="pageHeading lfloat">Edit Category</div>
					<div id="createCategoryDiv_close" class="link rfloat">close</div>
					<div class="clear"></div> 	
					<div id="createCategoryTemplateDiv" style="margin:20px 0;">
					</div>
				</div>
			</div>
		</div>
		<div id="tableContainer">
			<table id="flexme1" style="display: none"></table>
		</div>
		<div id="createCategoryTemplate">
		</div>
	</tiles:putAttribute>
	<tiles:putAttribute name="deferredScript">
    <sec:authentication property="principal" var="user" />
	<script type="text/javascript" src="${path.js('jquery/jquery.dataTables.min.js')}"></script>
	<script id="categoryTemplate" type="text/html">
			<form onsubmit="javascript : return false;">
				<div class="formLabel greybor round_bottom main-boform-cont pad-15-top ovrhid">
					<div class="formLeft150 lfloat">Name</div>
					<div class="formRight lfloat">
						<input type="text" id="name" value="<#=obj.name#>" size="30">
					</div>
					
					<div class="clear"></div>
					<div class="formLeft150 lfloat">Code</div>
					<div class="formRight lfloat">	
						<input type="text" id="code" value="<#=obj.code #>" <#=obj.code!=null ? "disabled" :"" #> size="30"/>
					</div>
					<div class="clear"></div>
				
					<div class="formLeft150 lfloat">Tax Type Code</div>
					<div class="formRight lfloat w150">
						<select id="taxTypeCode">
							<# if(obj.taxTypeCode != null) {#>
								<c:forEach items="${taxTypes}" var="taxType">
									<option value="${taxType.code}" <#= (obj.taxTypeCode== '${taxType.code}') ? 'selected' : ""#>><c:out value="${taxType.name }-${taxType.code}" /></option>
								</c:forEach>
							<# } else { #>
								<c:forEach items="${taxTypes}" var="taxType">
									<option value="${taxType.code}" <#= ('DEFAULT'== '${taxType.code}') ? 'selected' : ""#>><c:out value="${taxType.name }-${taxType.code}" /></option>
								</c:forEach>
							<# } #>
	  					</select>
					</div>
					<div class="clear"></div>

					<div class="formLeft150 lfloat">Item Detail Fields</div>
					<div class="formRight lfloat">	
						<input type="text" id="itemDetailFields" value="<#=obj.itemDetailFields #>" />
					</div>
					<div class="clear"></div>

					<div class="formLeft150 lfloat">&#160;</div>
					<div class="formRight lfloat">
						<# if(obj.code == null) { #>
							<input type="submit" class=" btn btn-success" id="addCategory" value="Create" size="30"/>
						<# } else { #>
							<input type="submit" class=" btn btn-success" id="updateCategory" value="Update" size="30"/>
						<# } #>
					</div>

					<div class="clear"></div>

				</div>
			</form>
		</script>
		<script id="categoryLink" type="text/html">
        <a href="/catalog/categories?legacy=1" style="text-decoration:none">
            <span class="mainHeading subHeaded">Categories<span class="pipe">/</span></span>
        </a> 
        <span class="subHeading"><#=obj.reqType#> Category</span>
    </script>
	<script type="text/javascript">
		Uniware.CategoriesPage = function() {
			var self = this;
			this.name = 'DATATABLE CATEGORIES';
			this.pageConfig = null;
			this.table = null;
			this.mode;
			this.datatableViews = ${user.getDatatableViewsJson("DATATABLE CATEGORIES")};
			this.init = function(){
				if (this.datatableViews.length) {
					this.headConfig = new Uniware.HeadConfig(this.datatableViews,function(datatableObject){
						self.pageConfig.setView(datatableObject);
					});
				}
				$("#create").click(self.create);
				this.pageConfig = new Uniware.PageConfig(this.name,'#flexme1',this.datatableViews[0]);
				$('#tableContainer').on('click', '.editCategory', self.editCategory);
			};
		
			this.create = function() {
				var obj = {
						'reqType' : 'Create' 	
				};
				$("#createCategoryLink").html(template("categoryLink",obj));
				$("#tableContainer").hide();
				$("#createCategoryTemplate").html(template("categoryTemplate",null));
				$("#addCategory").click(self.addCategory);
				self.mode = 'create';
			};
		
			this.addCategory = function () {
				var requestObject = {
					category : {
						'name' : $('#name').val(),
						'code' : $('#code').val(),
						'taxTypeCode' : $('#taxTypeCode').val(),
						'itemDetailFieldsText' : $("#itemDetailFields").val()
					}
				}
				
				var req =  {
						'name' : $('#name').val(),
						'code' : $('#code').val(),
						'taxTypeCode' : $('#taxTypeCode').val(),
						'itemDetailFields' : $("#itemDetailFields").val()
				}
				var requestData = JSON.stringify(requestObject);
				
				var url =(self.mode != 'edit') ? "/data/catalog/category/create" : "/data/catalog/category/update";
				Uniware.Ajax.postJson(url, requestData, function(response) {
					if(response.successful == true){
						Uniware.Utils.addNotification('Category has been saved');
						if(self.mode != 'edit'){
							self.mode = 'edit';
							$("#createCategoryTemplate").html(template("categoryTemplate",req));
							$("#updateCategory").click(self.addCategory);
						}
					}else{
						Uniware.Utils.showError(response.errors[0].description);
					}
				});	
			};
			
			this.editCategory = function(event) {
				var tr = $(event.target).parents('tr');
				var rowNumber = $(tr).attr('id').split('-')[1];
				var data = self.pageConfig.getRecord(rowNumber);
				var req = {
					'name' : self.pageConfig.getColumnValue(data,'name'),
					'code' : self.pageConfig.getColumnValue(data,'code'),
					'taxTypeCode' : self.pageConfig.getColumnValue(data,'taxTypeCode'),
					'itemDetailFields' : self.pageConfig.getColumnValue(data,'itemDetailFields')
				};
				self.mode = 'edit';
				var obj = {
					'reqType' : 'Edit'	
				};
				$("#createCategoryLink").html(template("categoryLink",obj));
				$("#tableContainer").hide();
				$("#createCategoryTemplate").html(template("categoryTemplate",req));
				$("#updateCategory").click(self.addCategory);
			};
			
		};
			$(document).ready(function() {
				window.page = new Uniware.CategoriesPage();
				window.page.init();
			});
	</script>
	</tiles:putAttribute>
</tiles:insertDefinition>
