# robots.txt for http://www.unicommerce.com/ 
User-agent: *
Disallow: /http/
Disallow: /cgi-bin/
Disallow: /css/
Disallow: /https/
Disallow: /admin/
Disallow: /icons/
Disallow: /js/

Sitemap: http://www.unicommerce.com/sitemap.xml