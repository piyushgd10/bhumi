<%@ include file="/tagIncludes.jsp"%>
<tiles:insertDefinition name=".inflowPage">
	<tiles:putAttribute name="title" value="Uniware - Search Active Purchase Order" />
	<tiles:putAttribute name="rightPane">
		<div>
			<form onsubmit="javascript : return false;">
				<div class="greybor headlable ovrhid main-box-head">
					<h2 class="edithead head-textfields">Search Active Purchase Order</h2>
				</div>
				<div class="round_bottom ovrhid pad-15">
					<div class="lfloat20">
						<div class="searchLabel">Vendor Name</div>
						<input type="text" id="vendorName" size="20" autocomplete="off" />
					</div>
					<div class="lfloat20">
						<div class="searchLabel">Item Type Name/Code Contains</div>
						<input type="text" id="itemTypeName" size="30" autocomplete="off" />
					</div>
					<div class="lfloat20">
						<div class="searchLabel">Purchase Order No.</div>
						<input type="text" id="purchaseOrderCode" size="20" autocomplete="off" />
					</div>
					<div class="lfloat20">
						<div class="searchLabel">Approval Date</div>
						<input type="text" id="approvedDateRange" size="20" class="w200 datefield pull-right" autocomplete="off" />
					</div>
					<div class="lfloat20">	
						<div class="searchLabel">&#160;</div>
						<input id="search" value="search" type="submit" class="btn btn-small btn-primary" />
					</div>
				<div id="searching" class="lfloat10 hidden" style="margin-top:25px;">
					<img src="/img/icons/refresh-animated.gif"/>
				</div>
				</div>
			</form>
		</div>
		<form onsubmit="javascript: return false;">
			<table id="dataTable" class="dataTable"></table>
		</form>
	</tiles:putAttribute>
	<tiles:putAttribute name="deferredScript">
	<script type="text/javascript" src="${path.js('jquery/jquery.dataTables.min.js')}"></script>
	<script type="text/javascript">
		Uniware.SearchPOPage = function() {
			var self = this;
			this.table = null;
			
			this.init = function() {
				$("#vendorName").autocomplete({
			    	minLength: 2,
			    	mustMatch : true,
					autoFocus: true,
					source: function( request, response ) {
						Uniware.Ajax.getJson("/data/lookup/vendors?name=" + request.term, function(data) {
							response( $.map( data, function( item ) {
								return {
									label: item.name,
								}
							}));
						});
					}
			    });
				$('#search').click(self.search);
				$('#approvedDateRange').daterangepicker();
			};

			this.search = function() {
				//verify vendor
				var vendorName = $("#vendorName").val();
				if(vendorName != "") {
					Uniware.Ajax.getJson("/data/lookup/vendors?name=" + vendorName, function(data) {
						if(data.length > 0){
							if(vendorName.toLowerCase() == data[0].name.toLowerCase()){
								self.searchPurchaseOrders(data[0].id);
							}else{
								Uniware.Utils.showError("Please select a valid vendor");
							}
						}else{
							Uniware.Utils.showError("Please select a valid vendor");
						}
					});	
				} else {
					self.searchPurchaseOrders();
				}
			};
							
			this.searchPurchaseOrders = function(vendorId) {	
				var requestObject = {
					vendorId: vendorId,
					status: 'APPROVED',
					itemTypeName: $('#itemTypeName').val(),
					purchaseOrderCode: $('#purchaseOrderCode').val()
				};
				
				var approvedDateRange = $("#approvedDateRange").val();
				if (approvedDateRange != '') {
					requestObject.approvedFromDate = Date.fromPaddedDate(approvedDateRange.substring(0, 10)).getTime();
					if (approvedDateRange.length > 10) {
						requestObject.approvedToDate = Date.fromPaddedDate(approvedDateRange.substring(13));
					} else {
						requestObject.approvedToDate = Date.fromPaddedDate(approvedDateRange.substring(0, 10));
					}
					requestObject.approvedToDate = requestObject.approvedToDate.setDate(requestObject.approvedToDate.getDate()+1);
				}
				
				var pipeline = new Uniware.pipelineData();
				pipeline.requestObject = requestObject;
				
				var dtEL = $('#dataTable');
				if (Uniware.Utils.isDataTable(dtEL)) {
					var dtTable = dtEL.dataTable();
					dtTable.fnDestroy();
	                dtTable = undefined;
				}
				
				self.table = dtEL.dataTable({
					"bServerSide": true,
					"sAjaxSource": "/data/po/searchPO",
					"bAutoWidth" : false,
					"bSort" : false,
					"bFilter": false,
					"sPaginationType": "full_numbers",
					"aoColumns" : [{
						"sTitle" : "Purchase Order",
						"mDataProp" : function (aData) {
							return '<a href="/inflow/grn/create?legacy=1&po=' + aData.code + ' ">' + aData.code + '</a>';									
						},
						"sWidth" : "300px"
					},{
						"sTitle" : "Vendor Name",
						"mDataProp" : "vendorName"
					},{
						"sTitle" : "Agreement",
						"mDataProp" : "vendorAgreement"
					},{
						"sTitle" : "Created On",
						"mDataProp": function (aData) {
							return (new Date(aData.created)).toDateTime();
						}
					},{
						"sTitle" : "Approved On",
						"mDataProp": function (aData) {
							return (new Date(aData.approved)).toDateTime();
						}
					} ],
					"fnServerData": pipeline.fnDataTablesPipeline
				}, true);
			};
			
		};
						
		$(document).ready(function() {
			window.page = new Uniware.SearchPOPage();
			window.page.init();
		});
	</script>
	</tiles:putAttribute>
</tiles:insertDefinition>
