<%@ include file="/tagIncludes.jsp"%>
<tiles:insertDefinition name=".inflowPage">
	<tiles:putAttribute name="title" value="Uniware - Warehouse Capacity" />
	<tiles:putAttribute name="rightPane">
		<div>
			<form onsubmit="javascript : return false;">
				<div class="greybor headlable ovrhid main-box-head">
				<h2 class="edithead head-textfields">Search Warehouse Capacity</h2></div>
				<div class="greybor round_bottom main-boform-cont pad-15-top overhid">
				<div class="lfloat hidden">
					<div class="formLabel">shelf type</div>
					<select id="shelfType" class="selectfield" style="width: 220px;">
						<option value="0" selected="selected">-- Select a Shelf Type --</option>
						<c:forEach items="${shelfTypes}" var="shelfType">
							<option value="${shelfType.code}"><c:out value="${shelfType.code}" /></option>
						</c:forEach>
					</select>
				</div>
				<div class="lfloat">
					<div class="formLabel">section</div>
					<select id="section" class="selectfield" style="width: 220px;">
						<option value="0" selected="selected">-- Select a Section --</option>
						<c:forEach items="${sections}" var="section">
							<option value="${section.code}"><c:out value="${section.name} - ${section.code}" /></option>
						</c:forEach>
					</select>
				</div>
				<div class="lfloat20">
					<div class="formLabel">name contains</div>
					<input type="text" id="name" size="20" class="textfield" autocomplete="off" />
				</div>
				<input type="submit" class=" btn btn-success lfloat20" id="search" value="search" style="margin: 17px;"/>
				<div id="error" class="errorField lfloat" style="margin: 10px;"></div>
				<div id="searching" class="lfloat hidden" style="margin: 10px;">
					<img src="/img/icons/refresh-animated.gif" />
				</div>
				<div class="clear"></div>
			</div>
			</form>
		</div>
		<div id="shelfs" style="margin-top:20px;">
		
		</div>
		<form onsubmit="javascript : return false;">
			<table id="dataTable" class="dataTable"></table>
		</form>

	</tiles:putAttribute>
	<tiles:putAttribute name="deferredScript">
		<script type="text/javascript" src="${path.js('jquery/jquery.dataTables.min.js')}"></script>
		<script type="text/html" id="capacityRow">
			<td><#=obj.shelfCode#></td>
			<td><#=obj.sectionCode#></td>
			<td style="line-height:16px;">
				<# if (obj.totalVolume != 0) { var bgClass = obj.filledPercentage > 80 ? "bg-red" : obj.filledPercentage > 50 ? "bg-yellow" : "bg-green";#>
				<div style="text-align:center;width:100%"><#=obj.filledPercentage#>%</div>
				<div style="border:1px solid #ccc;background:#eee;width:90%;height:15px;overflow:hidden;" class="round_all">
					<div style="width:<#=obj.filledPercentage#>%;height:15px;" class="round_all <#=bgClass#>">&#160;</div>&#160;
				</div>
				<# } else { #>
				<em>NA</em>
				<# } #>
			</td>
			<td><#=obj.itemCount#></td>
			<td><#=obj.totalVolume == 0 ? "<em>NA</em>" : obj.totalVolume #></td>
		</script>
		<script type="text/javascript">
			$(document).ready(function() {
				$("#search").click(function(){
					$("#error").addClass('invisible');
					$("#searching").removeClass('hidden');
					
					var requestObject = {
						'shelfTypeCode' : $("#shelfType").val(),
						'sectionCode' : $("#section").val(),
						'codeContains' : $("#name").val()
					};
					$("#shelfs").html('');
					Uniware.Ajax.postJson("/data/admin/layout/shelfs/search", JSON.stringify(requestObject), function(response) {
						if (response.successful == false) {
							Uniware.Utils.showError(response.errors[0].description);
						} else {
							var shelfCodes = response.shelfCodes;
							if (shelfCodes && shelfCodes.length > 0) {
								var dtEL = $('#dataTable');
								if (Uniware.Utils.isDataTable(dtEL)) {
									var dtTable = dtEL.dataTable();
									dtTable.fnClearTable();
									dtTable.fnAddData(response.shelfDTOs);
									
								} else {
									dtEL.dataTable({
										"sPaginationType": "full_numbers",
										"bSort": false,
										"bAutoWidth": false,
										"aLengthMenu": [[25, 50, -1], [25, 50, "All"]],
										"iDisplayLength": 25,
										"aaData" : response.shelfDTOs,
										"aoColumns" : [ {
											"sTitle" : "Shelf Code",
											"mDataProp" : "shelfCode"
										}, {
											"sTitle" : "Section Code",
											"mDataProp" : "sectionCode"
										}, {
											"sTitle" : "Filled %",
											"mDataProp": function (aData) {
												return "";
											}
										},{
											"sTitle" : "Item Count",
											"mDataProp" : "itemCount"
										}, {
											"sTitle" : "Total Volume (cm3)",
											"mDataProp": function (aData) {
												return "";
											}
										} ],
										"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
											$(nRow).html(template("capacityRow", aData));
											return nRow;
										}
									});
								}
							} else {
								$("#shelfs").html('<div class="formLabel">NO Shelf exists</div>');
							}
						}
						$("#searching").addClass('hidden');
					});
				});
				
			});
		</script>
	</tiles:putAttribute>	
</tiles:insertDefinition>
