<%@ include file="/tagIncludes.jsp"%>
<tiles:insertDefinition name=".inflowPage">
	<tiles:putAttribute name="title" value="Uniware - Gate Pass Entry" />
	<tiles:putAttribute name="rightPane">
		<div id="pageBar">
			 <div class="pageHeading" id="gatePassAction"><span class="mainHeading">Inbound Gate Pass Entries</span></div>
			 <div class="pageControls"></div>
		     <div class="rfloat10"><input type="button" value="Create Gate Pass" id="addGatePass" class="btn btn-small btn-primary rfloat" style="margin: 10px 5px;"/></div>
		     <div class="clear"></div>
		</div>
		<div id="tableContainer">
			<table id="flexme1" style="display: none"></table>
		</div>
		<div id="gatePassTemplateDiv"></div>
	</tiles:putAttribute>
	<tiles:putAttribute name="deferredScript">
		<sec:authentication property="principal" var="user" />
		<script type="text/javascript" src="${path.js('jquery/jquery.dataTables.min.js')}"></script>
		<script type="text/javascript">
			Uniware.InboundGatePassPage = function() {
				var self = this;
				this.pageConfig = null, this.partyCode = null;
				this.inboundGatePass = {};
				this.name = 'DATATABLE SEARCH INBOUND GATEPASS'; 
				this.pageConfig = null;
				this.table = null;
				this.partyCode = null;
				this.inboundGatePass;
				this.customFields = ${customFieldsJson};
				this.datatableViews = ${user.getDatatableViewsJson("DATATABLE SEARCH INBOUND GATEPASS")};

				this.init = function() {
					if(this.datatableViews.length){
						this.headConfig = new Uniware.HeadConfig(this.datatableViews,function(datatableObject){
							self.pageConfig.setView(datatableObject);
						});
					}
					this.pageConfig = new Uniware.PageConfig(this.name, '#flexme1',this.datatableViews[0]);
					$('#tableContainer').on('click', 'a.igpCode', function() {
						var createOrEdit = 'Edit';
						$('#gatePassAction').html(template("addGatePassLink",createOrEdit));
						self.getInboundGatePass($(this).attr('id'));
					});
					$('#addGatePass').click(function() {
						$(this).hide();
						var createOrEdit = 'Create';
						$('#gatePassAction').html(template("addGatePassLink",createOrEdit));
						self.getForm();
					});
				};

				this.getInboundGatePass = function(code) {
					var req = {};
					req.code = code;
					Uniware.Ajax.postJson('/data/material/gatepass/inbound/get', JSON.stringify(req), function(response) {
						if (response.successful == false) {
							Uniware.infoModal({
								modalContent : '<div style="margin:10px;">' + response.errors[0].description + '</div>',
								messageType : 'error',
								viewOptions : {
									backdrop : true
								}
							});
						} else {
							self.attachSuccessEvents(response);
						}
					});
				}

				this.getAutoCompleteObj = function() {
					return {
						mustMatch : true,
						autoFocus : true,
						source : function(request, response) {
							self.partyCode = null;
							Uniware.Ajax.getJson("/data/lookup/party?keyword=" + request.term, function(data) {
								response($.map(data, function(item) {
									return {
										label : item.name + "-" + item.code,
										value : item.code,
										party : item
									}
								}));
							});
						},
						select : function(event, ui) {
							self.partyCode = ui.item.party.code;
						}
					};
				};

				this.submitGatePass = function() {
					var req = {};
					req.wsInboundGatePass = {};
					req.wsInboundGatePass.code = self.inboundGatePass.code;
					req.wsInboundGatePass.partyCode = self.partyCode;
					req.wsInboundGatePass.materialDescription = $('#materialDescription').val();
					req.wsInboundGatePass.noOfParcel = $('#noOfParcel').val();
					req.wsInboundGatePass.transporter = $('#transporter').val();
					req.wsInboundGatePass.trackingNumber = $('#trackingNumber').val();
					req.wsInboundGatePass.referenceNumber = $('#referenceNumber').val();
					req.wsInboundGatePass.vehicleNumber = $('#vehicleNumber').val();
					req.wsInboundGatePass.remarks = $('#remarks').val();

					Uniware.Utils.addCustomFieldsToRequest(req.wsInboundGatePass.customFields, $('.custom'));
					if (req.wsInboundGatePass.code) {
						Uniware.Ajax.postJson('/data/material/gatepass/inbound/edit', JSON.stringify(req), function(response) {
							if (response.successful == false) {
								Uniware.infoModal({
									modalContent : '<div style="margin:10px;">' + response.errors[0].description + '</div>',
									messageType : 'error',
									viewOptions : {
										backdrop : true
									}
								});
							} else {
								self.attachSuccessEvents(response);
								Uniware.Utils.addNotification('gate pass entry has been updated');
							}
						});
					} else {
						Uniware.Ajax.postJson('/data/material/gatepass/inbound/create', JSON.stringify(req), function(response) {
							if (response.successful == false) {
								Uniware.infoModal({
									modalContent : '<div style="margin:10px;">' + response.errors[0].description + '</div>',
									messageType : 'error',
									viewOptions : {
										backdrop : true
									}
								});
							} else {
								self.attachSuccessEvents(response);
								Uniware.Utils.addNotification('gate pass entry has been added');
								var createOrEdit = 'Edit';
								$('#gatePassAction').html(template("addGatePassLink",createOrEdit));
							}
						});
					}

				};
				this.attachSuccessEvents = function(response) {
					this.inboundGatePass = response.inboundGatePassDTO;
					this.getForm();
					
				};

				this.getForm = function() {
					self.inboundGatePass.customFields = self.customFields;
					$("#gatePassTemplateDiv").html(template("gatePassTemplate", self.inboundGatePass));
					$("#partyCode").autocomplete(self.getAutoCompleteObj());
					$("#submitGatePass").click(self.submitGatePass);
					$('#tableContainer').hide();
					$('#addGatePass').hide();
				};
			};

			$(document).ready(function() {
				window.page = new Uniware.InboundGatePassPage();
				window.page.init();
			});
		</script>

		<script type="text/html" id="gatePassTemplate">
		<form class="form-horizontal" onsubmit="javascript : return false;">
			<br/>
			<table cellpadding="5">
				<tr>
					<# if(obj.code){#>
						<td class="searchLabel">Code</td>
						<td><div><button type="button" class="btn" disabled><#=obj.code#></button></div></td>
					<# } else { #>
						<td class="searchLabel">Party</td>
						<td><input type="text" id="partyCode" autocomplete="off" value="<#=obj.partyCode#>"/></td>							
					<# } #>
					<td class="searchLabel">Material Description</td>
					<td><input type="text" id="materialDescription" autocomplete="off" value="<#=obj.materialDescription#>"/></td>
				</tr>
				<tr>
					<td class="searchLabel">Number Of Parcels</td>
					<td><input type="text" id="noOfParcel" autocomplete="off" value="<#=obj.noOfParcel#>"/></td>
					<td class="searchLabel">Transporter</td>
					<td><input type="text" id="transporter" autocomplete="off" value="<#=obj.transporter#>"/></td>
				</tr>
				<tr>
					<td class="searchLabel">Tracking Number</td>
					<td><input type="text" id="trackingNumber" autocomplete="off" value="<#=obj.trackingNumber#>"/></td>
					<td class="searchLabel">Reference Number</td>
					<td><input type="text" id="referenceNumber" autocomplete="off" value="<#=obj.referenceNumber#>"/></td>
				</tr>
				<tr>
					<td class="searchLabel">Vehicle Number</td>
					<td><input type="text" id="vehicleNumber" autocomplete="off" value="<#=obj.vehicleNumber#>"/></td>
					<td class="searchLabel">Remarks</td>
					<td><input type="text" id="remarks" autocomplete="off" value="<#=obj.remarks#>"/></td>
				</tr>
			</table>
			<table cellpadding="4">
				<# if (obj.customFields && obj.customFields.length > 0) { #>
					<tr>
					<# for(var i=0;i<obj.customFields.length;i++) { var customField = obj.customFields[i]; #>
		 				<td align="left" class="searchLabel"><#=customField.displayName#></td>
						<td align="left" class="searchLabel">
							<# if (customField.valueType == 'text') { #>
								<input id="<#=customField.fieldName#>" type="text" class="w150 custom poUpdateItem" value="<#=customField.fieldValue#>" style="width:70%;"/>
							<# } else if (customField.valueType == 'date') { #>
								<input id="<#=customField.fieldName#>" type="text" class="w150 custom poUpdateItem datefield" value="<#= customField.fieldValue ? new Date(customField.fieldValue).toPaddedDate() : '' #>" style="width:70%;"/>
							<# } else if (customField.valueType == 'select') { #>
								<select id="<#=customField.fieldName#>" class="w150 custom poUpdateItem" style="width:70%;">
								<# for (var j=0;j<customField.possibleValues.length;j++) { #>
									<option value="<#=customField.possibleValues[j]#>" <#= customField.possibleValues[j] == customField.fieldValue ? selected="selected" : '' #> ><#=customField.possibleValues[j]#></option>
								<# } #>
								</select>
							<# } else if (customField.valueType == 'checkbox') { #>
								<input id="<#=customField.fieldName#>" type="checkbox" class="custom poUpdateItem" <#=customField.fieldValue ? checked="checked" : '' #> style="width:70%;"/>
							<# } #>
						</td>
						<# if (i == obj.length - 1 && i % 2 == 0) { #>
							<td></td><td></td>
						<# } else if (i != 0 && i % 2 != 0) { #>
							</tr><tr>
						<# } #>
					<# } #>
					</tr>
	            <# } #>
				<tr class="mar-15-top">
					<td></td>
					<td colspan="3">
						<input type="submit" id="submitGatePass" class=" btn btn-small btn-primary" value="save"/>
						<a class="btn btn-link cancelEdit" style="display:none;">Cancel</a>
					</td>
				</tr>
		</table>
		</form>
	</script>
	<script id="addGatePassLink" type="text/html">
        <a href="/inflow/gatePassEntry?legacy=1" style="text-decoration:none">
            <span class="mainHeading subHeaded">Inbound Gate Pass Entries<span class="pipe">/</span></span>
        </a> 
        <span class="headingActiveBreadTab"><#=obj#> Gate Pass Entry</span>
    </script>
	</tiles:putAttribute>
</tiles:insertDefinition>
