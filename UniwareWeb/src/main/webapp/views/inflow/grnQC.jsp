<%@ include file="/tagIncludes.jsp"%>
<tiles:insertDefinition name=".inflowPage">
	<tiles:putAttribute name="title" value="Uniware - Quality Check" />
	<tiles:putAttribute name="rightPane">
		<div class="noprint">
			<form onsubmit="javascript : return false;">
				<div class="greybor headlable ovrhid main-box-head">
					<h2 class="edithead head-textfields">Scan GRN</h2>
					<div class="lfloat">
						<input type="text" id="receiptCode" size="20"  autocomplete="off" value="${param['inflowReceiptCode']}">
					</div>
					<div id="searching" class="lfloat10 hidden" style="margin-top:5px;">
						<img src="/img/icons/refresh-animated.gif"/>
					</div>
				</div>
			</form>
		</div>
		<div id="receiptDiv" style="margin:10px;"></div>
		<div class="clear"></div>
		<div id="itemDetail" class="lb-over">
			<div class="lb-over-inner round_all" style="width: 600px">
				<div style="margin: 40px;">
					<div class="pageHeading lfloat">Please fill detailed information</div>
					<div id="itemDetail_close" class="link rfloat">close</div>
					<div class="clear"></div><br /><br /><br />

					<div class="formLeft lfloat bold">Scan Item</div>
					<div class="formRight lfloat">
						<input type="text" id="addDetailItemCode" value=""/>
					</div>
					<div class="clear"></div>
					
					<div id="itemDetailFields" class="invisible" style="min-height:100px;"></div>
					
					<div class="clear"></div><br/>			
					<div id="errorItemField" class="invisible errorField"></div>
				</div>
			</div>
		</div>	
	</tiles:putAttribute>
	<tiles:putAttribute name="deferredScript">
	<script id="itemDetailsTemplate" type="text/html">
	<# if(obj.length >0) { #>
		<# for(var i=0; i<obj.length; i++){ var itemDetailField = obj[i]; #>
			<div class="formLeft lfloat bold"><#=itemDetailField.displayName#></div>
			<div class="formRight lfloat">
				<input type="text" class="itemDetailField" name="<#=itemDetailField.name#>" value=""/>
			</div>
			<div class="clear"></div>
		<# } #>
	<# }else { #>
		<br/><br/><div  style="margin:15px;" class="bold"><em class="f15">No Custom Fields for this item.</em></div>
	<# } #>
	</script>	
	<script id="inflowReceipt" type="text/html">
	<div class="greybor round_bottom main-boform-cont pad-15-top overhid">
		<table width="100%" border="0" cellspacing="1" cellpadding="3" class="fields-table">
		<tr> 
        	<td width="20%">Purchase Order</td>
	        <td width="20%"><#=obj.purchaseOrder.code#></td>
    	    <td width="20%"></td>
        	<td></td>    
		</tr>
		<tr> 
        	<td>Vendor Name</td>
	        <td><#=obj.purchaseOrder.vendorName#></td>
    	    <td></td>
        	<td></td>    
		</tr>
		<tr> 
	       	<td>Vendor Invoice Number</td>
    	   	<td><#=obj.vendorInvoiceNumber#></td>
		    <td>Vendor Invoice Date</td>
        	<td><#=new Date(obj.vendorInvoiceDate).toPaddedDate()#></td>    
		</tr>
		<tr> 
	       	<td class="f15">GRN Number</td>
    	   	<td class="f15"><div class="barcode" barWidth="1" barHeight="15"><#=obj.code#></div></td>
    	   	<td>Created at</td>
        	<td><#=(new Date(obj.created)).toDateTime()#></td>    
		</tr>
		<tr> 
	       	<td>GRN Status</td>
    	   	<td><#=obj.statusCode#></td>
    	   	<td></td>
        	<td></td>    
		</tr>
		<tr> 
		 	<td>Total Quantity</td>
    	   	<td class="bold"><#=obj.totalQuantity#></td>
	   		<td>Total Rejected Quantity</td>
		   	<td class="bold"><#=obj.totalRejectedQuantity#></td>    
		</tr>
		<tr> 
		 	<td>Attachment:</td>
    	   	<td><span id="grnQcDocuments"></span></td>
	   		<td></td>
		   	<td></td>    
		</tr>
		</table>	
	</div>
	</script>

	<script id="inflowReceiptItems" type="text/html">
		<#=template("inflowReceipt",obj)#>
		<div class="greybor headlable round_top ovrhid mar-15-top">
			<h4 class="edithead">GRN Items</h4>
		</div>
		<div class="greybor round_bottom form-edit-table-cont">
			<table class="uniTable" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<th width="30"><input type="checkbox" checked="checked" id="selectAll"/></th>
					<th>Item Type</th>
					<th>Product<br/>Code</th>
					<th>Vendor<br/>SKU Code</th>
					<th>Batch Code</th>
					<th width="70">Received<br/>Quantity</th>
					<th width="70">Detailed<br/>Quantity</th>
					<th width="70">Rejected<br/>Quantity</th>
					<th>Rejection<br/>Comments</th>
					<th>Expiry</th>
					<th>operation</th>
				</tr>
				<# if (obj.rejectionByItem.length > 0) { #>
                <#if (obj.statusCode == 'QC_PENDING') { #>
				<tr style="background:#FAFAFA;">
					<td colspan="11">
						<div class="rfloat20" id="scanRejectedDiv">
							<span style="font-size:14px;">Scan Rejected Item</span>
							&#160;&#160;<input id="scanRejectedItem" class="w200" type="text"/>
						</div>		
					</td>
				</tr>
                <# } #>
				<# for(var i=0;i<obj.rejectionByItem.length;i++) { var irItem = obj.items[obj.rejectionByItem[i]]; #>
					<tr id='tr-<#=irItem.id#>' class="<#=irItem.status == 'QC_PENDING' ? '' : 'inactive italic'#>">
						<#=template("inflowReceiptItemRow", irItem)#>
					</tr>
				<# }} #>
				<# if (obj.rejectionByQty.length > 0) { #>
				<tr style="background:#FAFAFA;">
					<td colspan="11">
						<div class="rfloat20"><span style="font-size:14px;">Edit Items</span></div>
					</td>
				</tr>
				<# for(var i=0;i<obj.rejectionByQty.length;i++) { var irItem = obj.items[obj.rejectionByQty[i]]; #>
					<tr id='tr-<#=irItem.id#>' class="<#=irItem.status == 'QC_PENDING' ? '' : 'inactive italic'#>">
						<#=template("inflowReceiptItemRow", irItem)#>
					</tr>
				<# }} #>
			</table>	
		</div><br/><br/>

		<# if (obj.statusCode == 'QC_PENDING') { #>
			<div id="qcComplete" class=" btn btn-primary lfloat">quality check complete</div>
			<# if (obj.detailedQtyPending >  0 && Uniware.Utils.itemDetailingAtQC) { #>
				<div id="detailedQtyPending" class=" btn btn-primary lfloat20"><#=obj.detailedQtyPending#> Items Pending</div>
			<# } #>
		<# } #>
		<# if(obj.totalRejectedQuantity > 0 && (obj.statusCode == 'QC_COMPLETE' || obj.statusCode == 'COMPLETE')) { #>
			<a href="/inflow/receipt/rejectionReport/download/<#=obj.code#>?legacy=1" target="_blank">
				<div id="qcRejectionReport" class="btn btn-small btn-info rfloat20">Download QC Rejection  Report</div>
			</a>
		<# } #>
		<br/>
		<div class="clear"></div>
		<div id="qcComments" class="round_all"></div>
	</script>
	
	<script id="inflowReceiptItemRow" type="text/html">
		<td>
			<# if (obj.status == 'QC_PENDING' && (!Uniware.Utils.itemDetailingAtQC || obj.detailedQuantity == null || obj.detailedQuantity >= (obj.quantity - obj.rejectedQuantity))) { #>
			<input id="checkbox-<#=obj.id#>"  type="checkbox" class="selectGrnItem" checked="checked"/>
			<# } #>
		</td>
		<td>
			<#=obj.itemTypeName#>
			<#=Uniware.Utils.getItemDetail(obj.itemTypeImageUrl, obj.itemTypePageUrl)#>
		</td>
		<td class="breakWord"><#=obj.itemSKU#></td>
		<td class="breakWord"><#=obj.vendorSkuCode#></td>
		<td class="breakWord"><#=obj.batchCode#></td>
		<td><#=obj.quantity#></td>
		<td><#=obj.detailedQuantity != null ? obj.detailedQuantity : '<em>NA</em>' #></td>

		<# if (obj.status == 'QC_COMPLETE') { #>
			<td><#=obj.rejectedQuantity#></td>
			<td><#=obj.rejectionComments ? obj.rejectionComments : '&#160;' #></td>
			<td><#=obj.expiry ? new Date(obj.expiry).toPaddedDate() : '&#160;'#></td>
			<td> <#if (Uniware.Utils.traceabilityLevel == 'ITEM' && !Uniware.Utils.barcodingBeforeQC && (obj.quantity - obj.rejectedQuantity > 0)) { #>
                    <# if (obj.itemsLabelled) { #>
                        <div class="lfloat10 btn btn-small printItem" id="print-<#=obj.id#>">reprint <#=obj.quantity - obj.rejectedQuantity#> labels</div>
                    <# } else { #>
                        <div class="lfloat10 btn btn-small btn-primary printItem" id="print-<#=obj.id#>">print <#=obj.quantity - obj.rejectedQuantity#> labels</div>
                    <# }} #>
				<# if (Uniware.Utils.traceabilityLevel != 'ITEM') { #>
						<div class="lfloat10 btn btn-small printItem" id="print-<#=obj.id#>">reprint <#=obj.quantity - obj.rejectedQuantity#> labels</div>
				<# } #>
			</td>
        <# } else if (obj.status != 'QC_PENDING') { #>
            <td><#=obj.rejectedQuantity#></td>
            <td><#=obj.rejectionComments ? obj.rejectionComments : '&#160;' #></td>
            <td><#=obj.expiry ? new Date(obj.expiry).toPaddedDate() : '&#160;'#></td>
            <td></td>
		<# } else if(obj.isEditing) { #>
			<td>
				<# if(obj.isQtyEditable) { #>
					<input id="qty-<#=obj.id#>" type="text" class="rejectedQuantity edit field-width" value="<#=obj.rejectedQuantity#>"></input></td>
				<# } else { #>
					<#=obj.rejectedQuantity#>
				<# } #>
			<td>
				<# if (obj.rejectedQuantity > 0) { #>
				<textarea id="comments-<#=obj.id#>" class="field-width"><#=Uniware.Utils.escapeHtml(obj.rejectionComments)#></textarea>
				<# } #>
			</td>
			<td><input id="expiry-<#=obj.id#>" type="text" class="field-width datefield" value="<#=obj.expiry ? new Date(obj.expiry).toPaddedDate() : ''#>"/></td>
			<td>
				<input type="submit" class="btn btn-small update" value="update" id="update-<#=obj.id#>"></input>
				<span class="link cancel" id="cancel-<#=obj.id#>" style="margin-top:3px;">cancel</span>
			</td>
		<# } else { #>
			<td>
				<# if(obj.isQtyEditable) { #>
					<span class="table-editable"><#=obj.rejectedQuantity#></span>
				<# } else { #>
					<#=obj.rejectedQuantity#>
				<# } #>				
			</td>
			<td><span class=<#=obj.rejectedQuantity > 0 ? "table-editable" : "" #>><#=obj.rejectionComments ? obj.rejectionComments : '&#160;' #></span></td>
			<td><span class="table-editable datefield"><#=obj.expiry ? new Date(obj.expiry).toPaddedDate() : '&#160;'#></span></td>
			<td></td>					
		<# } #>
	</script>
	
	<script type="text/javascript">
		<sec:authentication property="principal" var="user" />
		Uniware.Utils.barcodingBeforeQC = ${configuration.getConfiguration('systemConfiguration').barcodingBeforeQualityCheck};
	    Uniware.Utils.traceabilityLevel = "${configuration.getConfiguration('systemConfiguration').traceabilityLevel}";
	    Uniware.Utils.itemDetailingAtQC = ${configuration.getConfiguration('systemConfiguration').itemDetailingMandatoryBeforePutaway};
		Uniware.qcPage = function() {
			var self = this;
			this.inflowReceipt = null;
			this.inflowReceiptCode = null;
			this.currentSku;
			this.itemEditing;
			this.facilityCode = '${cache.getCache('facilityCache').getCurrentFacility().getCode()}';
			
			this.init = function() {
				$('#receiptCode').keyup(self.load);
			};
			
			this.load = function(event) {
				if (event.which == 13 && $('#receiptCode').val() != '') {
					$('#receiptDiv').html("");
					self.fetchReceipt($('#receiptCode').val());
				}
			};
			
			this.fetchReceipt = function(inflowReceiptCode) {
				var req={
					'inflowReceiptCode': inflowReceiptCode ? inflowReceiptCode : self.inflowReceiptCode
				};
				Uniware.Ajax.postJson("/data/inflow/receipt/fetch", JSON.stringify(req), function(response) {
					if(response.successful == true) {
						self.currentSku = null;
						self.itemEditing = null;
						self.inflowReceipt = response.inflowReceipt;
						self.inflowReceiptCode = response.inflowReceipt.code;
						self.inflowReceipt.rejectionByItem = [];
						self.inflowReceipt.rejectionByQty = [];
						self.inflowReceipt.items = {};
						self.inflowReceipt.detailedQtyPending = 0;
						
						for(var i=0; i<self.inflowReceipt.inflowReceiptItems.length; i++) {
							var irItem = self.inflowReceipt.inflowReceiptItems[i];
							if (Uniware.Utils.barcodingBeforeQC && Uniware.Utils.traceabilityLevel == 'ITEM') {
								self.inflowReceipt.rejectionByItem.push(irItem.id); 
							} else {
								self.inflowReceipt.rejectionByQty.push(irItem.id);
							}
							if (Uniware.Utils.itemDetailingAtQC && irItem.detailedQuantity != null) {
								self.inflowReceipt.detailedQtyPending += (irItem.quantity - irItem.rejectedQuantity - irItem.detailedQuantity);
							}
							irItem.isQtyEditable = !(Uniware.Utils.barcodingBeforeQC && Uniware.Utils.traceabilityLevel == 'ITEM');
							self.inflowReceipt.items[irItem.id] = irItem;
						}
						self.inflowReceipt.inflowReceiptItems = null;
						self.renderReceiptItems();
						$('#receiptCode').val('');
					} else {
						Uniware.Utils.showError(response.errors[0].description);
					}
				}); 
			};
			
			this.renderReceiptItems = function() {
				$('#receiptDiv').html(template("inflowReceiptItems", self.inflowReceipt));
				Uniware.Utils.barcode();
				Uniware.Utils.applyHover("#receiptDiv");
				Uniware.Utils.renderComments("#qcComments", 'PO-' + self.inflowReceipt.purchaseOrder.code + '-' + self.facilityCode);
				Uniware.Documents("#grnQcDocuments", 'IR-' + self.inflowReceipt.code, '${user.username}', 2);
				$('#scanRejectedItem').keyup(self.rejectItem);
				$("#qcComplete").click(self.qcComplete);
				$('#detailedQtyPending').click(self.showAddItemDetail);
				$(".update").click(self.updateInflowReceiptItem);
				$(".cancel").click(self.cancel);
				$("span.table-editable").click(self.editReceiptItem);
				$('.printItem').click(self.printLabels);
				
				$("input.datefield").datepicker({
					dateFormat : 'dd/mm/yy'
				});
				
				$("input#selectAll").click(function() {
					var state = $("input#selectAll").is(":checked");
					$("input.selectGrnItem").attr('checked', state);
				});
			};
			
			this.showAddItemDetail = function() {
				
				$('#errorItemField').addClass('invisible');
				$('#itemDetailFields').addClass('invisible');
				Uniware.LightBox.show("#itemDetail");

				$('#addDetailItemCode').val('').focus().keyup(function(event){
					if (event.which == 13) {
						var req = {
							'itemCode' : $(this).val(),
							'inflowReceiptCode' : self.inflowReceipt.code
						};
						Uniware.Ajax.postJson("/data/inflow/item/detail/get", JSON.stringify(req),  function(response) {
							if (response.successful) {
								$('#itemDetailFields').html(template('itemDetailsTemplate', response.itemDetailFieldDTOs)).removeClass('invisible');
								$('.itemDetailField').each(function(index){
									$(this).keyup(function(event){
										if(event.which == 13){
											var field = $('.itemDetailField:eq(' + (index+1) + ')');
											if(field.length > 0){
												field.focus();	
											}else{
												self.addItemDetails();
											}
										}
									});
								});
								$('.itemDetailField:first').focus();
							    $('#addItemDetails').click(self.addItemDetails);
								if (response.itemDetailFieldDTOs.length == 0) {
									$('#addDetailItemCode').val('');
								}
							} else {
								$("#errorItemField").removeClass("invisible").html(response.errors[0].description.split('|')[0] + " \"" + $('#addDetailItemCode').val() + "\"");
								$('#addDetailItemCode').val('');
							}
						});
					}
				});
			};
			
			this.addItemDetails = function() {
				var req = {
					'itemCode' : $('#addDetailItemCode').val(),
					'itemDetails' : []
				}
				$('.itemDetailField').each(function(index){
					var itemDetail = {}; 
					itemDetail.name = $(this).attr("name");
					itemDetail.value = $(this).val();
					req.itemDetails.push(itemDetail);
				});
				$("#errorItemField").addClass("invisible");
				Uniware.Ajax.postJson("/data/inflow/item/detail/add", JSON.stringify(req),  function(response) {
					if (response.successful) {
						self.inflowReceipt.detailedQtyPending -= 1;
						if (self.inflowReceipt.detailedQtyPending == 0) {
							Uniware.LightBox.hide();
							self.fetchReceipt();
						} else {
							self.renderReceiptItems();
							$('#detailedQtyPending').trigger('click');	
						}
					} else {
						$("#errorItemField").removeClass("invisible").html(response.errors[0].description.split('|')[0]);
					}
				});
			};
			
			this.rejectItem = function(event) {
				if (event.which == 13) {
					var req = {
						itemCode: $('#scanRejectedItem').val(),
						inflowReceiptCode: self.inflowReceipt.code
					};
					Uniware.Ajax.postJson("/data/inflow/item/reject", JSON.stringify(req), function(response) {
						if(response.successful == true) {
							Uniware.Utils.addNotification("Item has been rejected");
							var item = self.inflowReceipt.items[response.inflowReceiptItemDTO.id];
							item.rejectedQuantity = response.inflowReceiptItemDTO.rejectedQuantity;
							self.inflowReceipt.totalRejectedQuantity += 1;
							
							if (self.itemEditing != null && self.itemEditing != item.itemSKU) {
								var orgItem = self.inflowReceipt.items[self.itemEditing];
								orgItem.isEditing = null;
							}
							item.isEditing = true;
							self.itemEditing = item.id;
							self.renderReceiptItems();
							
						} else {
							Uniware.Utils.showError(response.errors[0].description  + " \"" + $('#scanRejectedItem').val() + "\"");
						}
						$('#scanRejectedItem').val('');
					}); 
				}
			};
			
			this.updateInflowReceiptItem = function(event) {
				var itemKey = $(this).attr('id').substring($(this).attr('id').indexOf('-') + 1);
				var item = self.inflowReceipt.items[itemKey];
				var req = {
					inflowReceiptItemId: item.id,
					comments: $("#comments-" + itemKey).val(),
				}
				if ($("#expiry-" + itemKey).val() != '') {
					req.expiry = Date.fromPaddedDate($("#expiry-" + itemKey).val()).getTime(); 
				}
				
				if (item.isQtyEditable) {
					req.rejectedQuantity = ($("#qty-" + itemKey).val() != '' ? parseInt($("#qty-" + itemKey).val()) : 0);
					var url = "/data/inflow/receipt/item/nontraceable/edit";
				} else {
					var url = "/data/inflow/receipt/item/traceable/edit";
				}
			
				Uniware.Ajax.postJson(url, JSON.stringify(req), function(response) {
					if(response.successful == true) {
						Uniware.Utils.addNotification("Inflow receipt item updated successfully");
						var orgItem = self.inflowReceipt.items[itemKey];
						
						self.inflowReceipt.totalRejectedQuantity -= orgItem.rejectedQuantity;
						orgItem.rejectedQuantity = response.inflowReceiptItemDTO.rejectedQuantity;
						self.inflowReceipt.totalRejectedQuantity += orgItem.rejectedQuantity;
						
						orgItem.rejectionComments = response.inflowReceiptItemDTO.rejectionComments;
						orgItem.expiry = response.inflowReceiptItemDTO.expiry;
						orgItem.isEditing = null;
						self.renderReceiptItems();
					} else {
						Uniware.Utils.showError(response.errors[0].description);
					}
				}, true);
			};
			
			this.cancel = function(event) {
				var item = self.inflowReceipt.items[$(this).attr('id').substring($(this).attr('id').indexOf('-') + 1)]; 
				item.isEditing = null;
				self.renderReceiptItems();
			};
			
			this.qcComplete = function() {
				var req= {
					inflowReceiptCode: self.inflowReceipt.code,
					inflowReceiptItemIds:[]
				};
				var selectedGRNItems = $("input.selectGrnItem:checked");
				if (selectedGRNItems.length == 0) {
					alert('Please select an GRN item.');
					return;
				}
				selectedGRNItems.each(function() {
					req.inflowReceiptItemIds.push($(this).attr('id').substring($(this).attr('id').indexOf('-') + 1));
				});
				
				Uniware.Ajax.postJson("/data/inflow/qc/complete", JSON.stringify(req), function(response) {
					if(response.successful == true){
						Uniware.Utils.addNotification("Quality Check Completed Successfully")
						self.fetchReceipt();
					}else{
						Uniware.Utils.showError(response.errors[0].description);
					}
				}, true);
			};
			
			this.editReceiptItem = function(event) {
 				var tr = $(event.target).parents('tr');
				var itemKey = $(tr[0]).attr('id').substring($(tr[0]).attr('id').indexOf('-') + 1);
				var item = self.inflowReceipt.items[itemKey];
				
				if (item.isEditing) {
					
				} else {
					if (self.itemEditing != null && self.itemEditing != itemKey) {
						var orgItem = self.inflowReceipt.items[self.itemEditing];
						orgItem.isEditing = null;
					}
					self.itemEditing = itemKey;
					item.isEditing = true;
					self.renderReceiptItems();
				}
			};
			
			this.printLabels = function() {
			    var itemSku = $(this).attr('id').substring($(this).attr('id').indexOf('-') + 1);
			    var receiptItem = self.inflowReceipt.items[itemSku];
			    if (receiptItem.itemsLabelled || Uniware.Utils.traceabilityLevel != 'ITEM') {
			        Uniware.Utils.printIFrame('/inflow/items/print/'+ receiptItem.id);
			    } else {
			        var req = {
			            inflowReceiptItemId : receiptItem.id
			        };
			        Uniware.Ajax.postJson("/data/inflow/po/receive/createItems", JSON.stringify(req), function(response) {
			            if(response.successful == true) {
			                Uniware.Utils.printIFrame('/inflow/items/print/'+ receiptItem.id);
			                self.fetchReceipt();
			            } else {
			                // should not happen
			            }
			        }, true);
			    }
			};
		};
					
		$(document).ready(function() {
			window.page = new Uniware.qcPage();
			window.page.init();
			if ($('#receiptCode').val() != '') {
				$('#receiptCode').trigger(Uniware.Event.ENTER_KEYUP);
			}
		});
	</script>
	</tiles:putAttribute>
</tiles:insertDefinition>
