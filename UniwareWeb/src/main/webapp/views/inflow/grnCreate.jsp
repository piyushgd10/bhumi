<%@ include file="/tagIncludes.jsp"%>
<tiles:insertDefinition name=".inflowPage">
	<tiles:putAttribute name="title" value="Uniware - Receive Purchase Order" />
	<tiles:putAttribute name="rightPane">
		<div id="grnCreateDiv" class="lb-over">
			<div class="lb-over-inner round_all">
				<div style="margin: 40px;line-height: 30px;">
					<div class="pageHeading lfloat">GRN Create</div>
					<div id="grnCreateDiv_close" class="link rfloat">close</div>
					<div class="clear"></div>
					<div id="grnCreateTemplateDiv" style="margin:20px 0;">
					</div>
				</div>
			</div>
		</div>	
		<div>
			<form onsubmit="javascript : return false;">
				<div class="greybor headlable ovrhid main-box-head">
					<h2 class="edithead head-textfields">Scan Purchase Order</h2>
					<div class="lfloat">
						<input type="text" id="poCodePrefix"  style="background-color:#EDF7F1;width:100px;" value="${cache.getCache('sequenceCache').getPrefixByName('PURCHASE_ORDER')}">
						<input type="text" id="poCode" size="20" autocomplete="off" value="" style="margin-left:-10px;">
					</div>
					<div id="searching" class="lfloat10 hidden" style="margin-top:5px;">
						<img src="/img/icons/refresh-animated.gif"/>
					</div>
				</div>
			</form>
		</div>
		<div id="createGrnDiv"></div>
		<div id="inflowReceiptsDiv" class="noprint"></div>
		<div id="poItemsDiv" class="noprint"></div>
		<div class="clear"></div>
		<div id="grnCreateComments" class="round_all"></div>
	</tiles:putAttribute>
	
	<tiles:putAttribute name="deferredScript">
	<script type="text/html" id="grnCreateTemplate">
		<# if(obj.inflowReceipts && obj.inflowReceipts.length > 0) { #>
			<div class="formLeft150 lfloat">Copy from GRN</div>
			<div class="formRight lfloat">
				<select id="copyFromGRN">
					<option value="">select a grn</option>
					<# for(var i=0;i<obj.inflowReceipts.length;i++) { var grn = obj.inflowReceipts[i]; #>
						<option value="<#=i#>"><#=grn.code#></option>
					<# } #>
				</select>
			</div>
			<div class="clear"></div>		
		<# } #> 

		<div class="formLeft150 lfloat">Vendor Invoice Number</div>
		<div class="formRight lfloat">
			<input type="text" id="vendorInvoiceNumber" size="15" autocomplete="off" value="<#=obj.vendorInvoiceNumber#>" />
		</div>
		<div class="clear"></div>

		<div class="formLeft150 lfloat">Vendor Invoice Date</div>
		<div class="formRight lfloat">
        	<input type="text" id="vendorInvoiceDate" size="15" autocomplete="off" class="datefield" value="<#=obj.vendorInvoiceDate ? new Date(obj.vendorInvoiceDate).toPaddedDate() : '' #>" />
		</div>
		<div class="clear"></div>
			<# var customs = (obj.customFieldValues && obj.customFieldValues.length > 0) ? obj.customFieldValues : obj.grnCustomFieldsMetadata; #> 
			<# for(var i=0;customs&&i<customs.length;i++) { var customField = customs[i]; #>
				<div class="formLeft150 lfloat"><#=customField.displayName#></div>
				<div class="formRight lfloat customFieldValueName" >
						<# if (customField.valueType == 'text') { #>
							<input id="<#=customField.fieldName#>" type="text" size="15" class="custom" value="<#=customField.fieldValue#>"/>
						<# } else if (customField.valueType == 'date') { #>
							<input id="<#=customField.fieldName#>" type="text" size="15" class="custom datefield" value="<#= customField.fieldValue ? new Date(customField.fieldValue).toPaddedDate() : '' #>"/>
						<# } else if (customField.valueType == 'select') { #>
							<select id="<#=customField.fieldName#>" class="w150 custom">
							<# for (var j=0;j<customField.possibleValues.length;j++) { #>
								<option value="<#=customField.possibleValues[j]#>" <#= customField.possibleValues[j] == customField.fieldValue ? selected="selected" : '' #> ><#=customField.possibleValues[j]#></option>
							<# } #>
							</select>
						<# } else if (customField.valueType == 'checkbox') { #>
							<input id="<#=customField.fieldName#>" type="checkbox" class="custom" <#=customField.fieldValue ? checked="checked" : '' #>/>
						<# } #>
				</div>
				<div class="clear"></div>
			<# } #>
		<br /> <br />

		<div class="formLeft150 lfloat">&#160;</div>
		<div class="formRight lfloat">
			<# if(obj.customFieldValues) { #>
				<div id="editGRN" class="btn btn-small btn-primary lfloat"> Submit
                </div>			
			<# } else { #> 
				<div id="generateReceipt" class="btn btn-small btn-success lfloat"> create grn
				</div>
			<# } #>
		</div>
		<div class="clear"/>
		<div id="error" class="errorField lfloat20 invisible"></div>
		<div class="clear"></div>
	</script>	
	<script id="purchaseOrder" type="text/html">
	<div class="greybor round_bottom main-boform-cont pad-15-top overhid">
		<table width="100%" border="0" cellspacing="1" cellpadding="3" class="fields-table">
		<# if (obj.currentGRN) { #>
			<tr>
    			<td>&nbsp;</td>
    			<td colspan="3">
					<div id="editItem" class="btn btn-small rfloat20">edit <img src="/img/icons/edit.png"/></div>
				</td>
  			</tr>
		<# } #>
		<tr> 
        	<td width="20%">Purchase Order</td>
	        <td width="20%"><#=obj.code#></td>
    	    <td width="20%"></td>
        	<td></td>    
		</tr>
		<tr> 
        	<td>Vendor Name</td>
	        <td><#=obj.vendorName#></td>
    	    <td>Purchase Order Status</td>
        	<td><#=obj.statusCode#></td>    
		</tr>
		
		<# if (!obj.currentGRN) { #>
			</table><br/>
		<# } else { #>
			<tr> 
	        	<td>Vendor Invoice Number</td>
    	    	<td><#=obj.currentGRN.vendorInvoiceNumber#></td>
    	    	<td>Vendor Invoice Date</td>
        		<td><#=new Date(obj.currentGRN.vendorInvoiceDate).toPaddedDate()#></td>    
			</tr>
			<tr> 
	        	<td class="f15">GRN Number</td>
    	    	<td class="f15"><div class="barcode" barWidth="1" barHeight="15"><#=obj.currentGRN.code#></div></td>
    	    	<td>Created at</td>
        		<td><#=(new Date(obj.currentGRN.created)).toDateTime()#></td>    
			</tr>
			<tr> 
		       	<td>GRN Status</td>
    		   	<td><#=obj.currentGRN.statusCode#></td>
    	   		<td></td>
        		<td></td>    
			</tr>
			<# if (obj.currentGRN.customFieldValues && obj.currentGRN.customFieldValues.length > 0) { #>
			<tr>
			<# for(var i=0;i<obj.currentGRN.customFieldValues.length;i++) { var customField = obj.currentGRN.customFieldValues[i]; #>
	 			<td><#=customField.displayName#></td>
				<td>
					<# if (customField.valueType == 'date') { #>
						<#= customField.fieldValue ? new Date(customField.fieldValue).toPaddedDate() : '' #>
					<# } else if (customField.valueType == 'checkbox') { #>
						<input type="checkbox" disabled="disabled" <#=customField.fieldValue ? checked="checked" : '' #>/>
					<# } else { #>
						<#=customField.fieldValue#>
					<# } #>
				</td>
				<# if (i == obj.currentGRN.customFieldValues.length- 1 && i % 2 == 0) { #>
					<td></td><td></td>
				<# } else if (i != 0 && i % 2 != 0) { #>
					</tr><tr>
				<# } #>
			<# } #>
			</tr>				
     	   <# } #>
			<tr> 
		       	<td>Total Quantity</td>
    		   	<td class="bold"><#=obj.currentGRN.totalQuantity#></td>
    	   		<td>Total Rejected Quantity</td>
    		   	<td class="bold"><#=obj.currentGRN.totalRejectedQuantity#></td>    
			</tr>
			<tr> 
		 		<td>Total Received Amount</td>
    	   		<td class="bold"><#=obj.currentGRN.totalReceivedAmount#></td>
	   			<td>Total Rejected Amount</td>
		   		<td class="bold"><#=obj.currentGRN.totalRejectedAmount#></td>    
			</tr>
			<tr> 
		 		<td>Attachment:</td>
    	   		<td><span id="grnCreateDocuments"></span></td>
	   			<td></td>
		   		<td></td>    
			</tr>
		</table><br/>
		<div class="greybor headlable round_top ovrhid">
			<h4 class="edithead">GRN Items</h4>
		</div>
		<table width="100%" border="0" cellspacing="1" cellpadding="3" class="uniTable greybor">
			<tr>
				<th>Item Type</th>
				<th>Product Code</th>
				<th>Vendor SKU Code</th>
				<th width="80">Unit<br/>Price</th>
				<th width="80">Pending<br/>Quantity</th>
				<# if (Uniware.Utils.editAdditionalCost) { #>
		 			<th width="80">Additional Cost</th>
				<# } #>
				<# if (Uniware.Utils.editBatchCode) { #>
					<th width="80">Batch Code</th>
				<# } #>
				<th width="80">Quantity</th>
				<# if (Uniware.Utils.editMRP) { #>
					<th width="80">MRP</th>
				<# } #>
				<# if (Uniware.Utils.editDiscount) { #>
					<th width="80">Discount<br/>per unit</th>
					<th width="80">Discount %</th>
				<# } #>
				
				<th width="150" class="noprint">Action</th>
			</tr>
			<# if (Uniware.Utils.barcodingBeforeGRN) { #>
			<# for (var i=0;i<obj.currentGRN.savedItems.length;i++) { var irItem = obj.currentGRN.itemsMap[obj.currentGRN.savedItems[i]]; #>
			<tr id="tr-<#=irItem.id#>">
				<td>
					<#=irItem.itemTypeName#>
					<#=Uniware.Utils.getItemDetail(irItem.itemTypeImageUrl, irItem.itemTypePageUrl)#>
				</td>
				<td class="breakWord"><#=irItem.itemSKU#></td>
				<td class="bold breakWord"><#=irItem.vendorSkuCode#></td>
				<td><#=irItem.unitPrice#></td>
				<td><#=irItem.pendingQuantity#></td>
				<# if (Uniware.Utils.editAdditionalCost) { #>
					<td>
						<# if (irItem.isEditing) { #>
							<input id="addCost-<#=irItem.id#>" type="text" class="field-width" value="<#=irItem.additionalCost#>"/>
						<# } else if (obj.currentGRN.statusCode != 'CREATED') { #>
							&#160;<#=irItem.additionalCost#>
						<# } else { #>
							<span class="table-editable"><#=irItem.additionalCost#></span>
						<# } #>
					</td>
				<# } #>
				<# if (Uniware.Utils.editBatchCode) { #>
					<td>
						<# if (irItem.isEditing) { #>
							<input id="batch-<#=irItem.id#>" type="text" class="field-width" value="<#=irItem.batchCode#>" />
						<# } else if (obj.currentGRN.statusCode != 'CREATED') { #>
							&#160;<#=irItem.batchCode#>
						<# } else { #>
							<span class="table-editable"><#=irItem.batchCode#></span>
						<# } #>
					</td>
				<# } #>
				<td id="qty-<#=irItem.id#>"><#=irItem.quantity#></td>
				<# if (Uniware.Utils.editMRP) { #>
					<td width="80"><#=irItem.maxRetailPrice#></th>
				<# } #>
				<# if (Uniware.Utils.editDiscount) { #>
					<td width="80"><#=irItem.discount#></th>
					<td width="80"><#=irItem.discountPercentage#></th>
				<# } #>
				<td class="noprint">
					<# if (irItem.isEditing) { #>
						<input type="submit" class="btn btn-small update lfloat10" value="update" id="update-<#=irItem.id#>"></input>
						<span class="link cancel" id="cancel-<#=irItem.id#>" style="margin-top:3px;">cancel</span>
					<# } #>
				</td>				
			</tr>
			<# } #>
			<# } else { #>
 			<# for (var i=0;i<obj.currentGRN.savedItems.length;i++) { var irItem = obj.currentGRN.itemsMap[obj.currentGRN.savedItems[i]]; var skuDetail = obj.skuDetails[irItem.itemSKU]; #>
			<tr id="tr-<#=irItem.id#>">
				<td>
					<#=irItem.itemTypeName#>
					<#=Uniware.Utils.getItemDetail(irItem.itemTypeImageUrl, irItem.itemTypePageUrl)#>
				</td>
				<td>
                    <#=irItem.itemSKU#>
                    <# if (skuDetail && skuDetail.color) { #>
                        <br/>Color : <#=skuDetail.color#>
                    <# } #>
                    <# if (skuDetail && skuDetail.size) { #>
                        <br/>Size : <#=skuDetail.size#>
                    <# } #>
                    <# if (skuDetail && skuDetail.brand) { #>
                        <br/>Brand : <#=skuDetail.brand#>
                    <# } #>
                </td>
				<td class="bold"><#=irItem.vendorSkuCode#></td>
				<# if (Uniware.Utils.editUnitPrice) { #>
				<td>
					<# if (irItem.isEditing) { #>
						<input id="unitPrice-<#=irItem.id#>" type="text" class="field-width unitPrice" value="<#=irItem.unitPrice#>"/>
					<# } else if (obj.currentGRN.statusCode != 'CREATED') { #>
						&#160;<#=irItem.unitPrice#>
					<# } else { #>
						<span class="table-editable"><#=irItem.unitPrice#></span>
					<# } #>
				</td>
				<# } else { #>
					<td><#=irItem.unitPrice#></td>
				<# } #>
				<td><#=irItem.pendingQuantity#></td>
				<# if (Uniware.Utils.editAdditionalCost) { #>
					<td>
						<# if (irItem.isEditing) { #>
							<input id="addCost-<#=irItem.id#>" type="text" class="field-width" value="<#=irItem.additionalCost#>"/>
						<# } else if (obj.currentGRN.statusCode != 'CREATED') { #>
							&#160;<#=irItem.additionalCost#>
						<# } else { #>
							<span class="table-editable"><#=irItem.additionalCost#></span>
						<# } #>
					</td>
				<# } else { #>
					<td><#=irItem.additionalCost#></td>
				<# } #>
				<# if (Uniware.Utils.editBatchCode) { #>
					<td>
						<# if (irItem.isEditing) { #>
							<input id="batch-<#=irItem.id#>" type="text" class="field-width" value="<#=irItem.batchCode#>" />
						<# } else if (obj.currentGRN.statusCode != 'CREATED') { #>
							&#160;<#=irItem.batchCode#>
						<# } else { #>
							<span class="table-editable"><#=irItem.batchCode#></span>
						<# } #>
					</td>
				<# } else { #>
					<td><#=irItem.batchCode#></td>
				<# } #>
				<td>
					<# if (irItem.isEditing) { #>
						<input id="qty-<#=irItem.id#>" type="text" class="field-width" value="<#=irItem.quantity#>"/>
					<# } else if (obj.currentGRN.statusCode != 'CREATED' || irItem.itemsLabelled) { #>
						&#160;<#=irItem.quantity#>
					<# } else { #>
						<span class="table-editable"><#=irItem.quantity#></span>
					<# } #>
				</td>
				<# if (Uniware.Utils.editMRP) { #>
				<td>
					<# if (irItem.isEditing) { #>
						<input id="mrp-<#=irItem.id#>" type="text" class="field-width" value="<#=irItem.maxRetailPrice#>"/>
					<# } else if (obj.currentGRN.statusCode != 'CREATED') { #>
						&#160;<#=irItem.maxRetailPrice#>
					<# } else { #>
						<span class="table-editable"><#=irItem.maxRetailPrice#></span>
					<# } #>
				</td>
				<# } #>
				<# if (Uniware.Utils.editDiscount) { #>
				<td>
					<# if (irItem.isEditing) { #>
						<input id="discount-<#=irItem.id#>" type="text" class="discount field-width" value="<#=irItem.discount#>"/>
					<# } else if (obj.currentGRN.statusCode != 'CREATED') { #>
						&#160;<#=irItem.discount#>
					<# } else { #>
						<span class="table-editable"><#=irItem.discount#></span>
					<# } #>
				</td>
				<td>
					<# if (irItem.isEditing) { #>
						<input id="discountPer-<#=irItem.id#>" type="text" class="discountPer field-width" value="<#=irItem.discountPercentage#>"/>
					<# } else if (obj.currentGRN.statusCode != 'CREATED') { #>
						&#160;<#=irItem.discountPercentage#>
					<# } else { #>
						<span class="table-editable"><#=irItem.discountPercentage#></span>
					<# } #>
				</td>
				<# } #>
				<td class="noprint">
					<# if (irItem.isEditing) { #>
						<input type="submit" class="btn btn-small update lfloat10" value="update" id="update-<#=irItem.id#>"></input>
						<span class="link cancel" id="cancel-<#=irItem.id#>" style="margin-top:3px;">cancel</span>
					<# } else if (Uniware.Utils.traceabilityLevel == "ITEM" && Uniware.Utils.barcodingBeforeQC && irItem.quantity > 0) { #>
					<# if (irItem.itemsLabelled) { #>
						<div class="lfloat10 btn btn-small printItem" id="print-<#=irItem.id#>">reprint <#=irItem.quantity#> labels</div>
					<# } else { #>
						<div class="lfloat10 btn btn-small btn-success printItem" id="print-<#=irItem.id#>">print <#=irItem.quantity#> labels</div>
					<# }} #>
					<# if (Uniware.Utils.traceabilityLevel == 'ITEM_SKU' || Uniware.Utils.traceabilityLevel == 'NONE') { #>
						<div class="lfloat10 btn btn-small printItem" id="print-<#=irItem.id#>">print <#=irItem.quantity#> labels</div>
					<# } #>
				</td>				
			</tr>
			<# } #>

			<# for (var i=0;i<obj.currentGRN.currentItems.length;i++) { var poItem = obj.currentGRN.itemsMap['poi' + obj.currentGRN.currentItems[i]]; var skuDetail = obj.skuDetails[poItem.itemSKU]; #>
			<tr id="tr-<#=poItem.id#>">
				<td>
					<#=poItem.itemTypeName#>
					<#=Uniware.Utils.getItemDetail(poItem.itemTypeImageUrl, poItem.itemTypePageUrl)#>
				</td>
				<td>
                    <#=poItem.itemSKU#>
                    <# if (skuDetail.color) { #>
                        <br/>Color : <#=skuDetail.color#>
                    <# } #>
                    <# if (skuDetail.size) { #>
                        <br/>Size : <#=skuDetail.size#>
                    <# } #>
                    <# if (skuDetail.brand) { #>
                        <br/>Brand : <#=skuDetail.brand#>
                    <# } #>
                </td>
				<td style="font-weight:bold;"><#=poItem.vendorSkuCode#></td>
				<# if (Uniware.Utils.editMRP) { #>
					<td><input id="unitPrice-<#=poItem.id#>" type="text" class="field-width unitPrice" value="<#=poItem.unitPrice#>"/></td>
				<# } else { #>
					<td><#=poItem.unitPrice#></td>
				<# } #>
				<td><#=poItem.pendingQuantity#></td>
				<# if (Uniware.Utils.editAdditionalCost) { #>
					<td>
						<input id="addCost-<#=poItem.id#>" type="text" class="field-width" />
					</td>
				<# } #>
				<# if (Uniware.Utils.editBatchCode) { #>
					<td>
						<input id="batch-<#=poItem.id#>" type="text" class="field-width" />
					</td>
				<# } #>
				<td><input id="qty-<#=poItem.id#>" type="text" class="field-width"/></td>
				<# if (Uniware.Utils.editMRP) { #>
					<td><input id="mrp-<#=poItem.id#>" type="text" class="field-width" value="<#=poItem.maxRetailPrice#>"/></td>
				<# } #>
				<# if (Uniware.Utils.editDiscount) { #>
				<td>
					<input id="discount-<#=poItem.id#>" type="text" class="discount field-width" value="<#=poItem.discount#>"/>
				</td>
				<td>
					<input id="discountPer-<#=poItem.id#>" type="text" class="discountPer field-width" value="<#=poItem.discountPercentage#>"/>
				</td>
				<# } #>
				<td>
					<div class=" lfloat10 btn btn-small saveItem" id="save-<#=poItem.id#>">save</div>
					<div class="lfloat10 remove link" id="remove-<#=poItem.id#>" style="margin-top:4px;">cancel</div>
				</td>
			</tr>
			<# } #>

			<# if (obj.currentGRN.statusCode == 'CREATED' && obj.pendingPOIs.length > 0) { #>
			<tr>	
				<td>
				<select id="itemTypeNameCode" class="field-width">
					<option value="0"></option>
					<# for (var key in obj.skuNames) { #>
					<option value="<#=key#>"><#=obj.skuNames[key]#></option>
					<# } #>
				</select>
				</td>
				<td>
				<select id="productCode" class="field-width">
					<option value="0"></option>
					<# for (var key in obj.skuCodes) { #>
					<option value="<#=key#>"><#=obj.skuCodes[key]#></option>
					<# } #>
				</select>
				</td>
				<td>
				<select id="vendorCode" class="field-width">
					<option value="0"></option>
					<# for (var key in obj.vendorSkuCodes) { #>
					<option value="<#=key#>"><#=obj.vendorSkuCodes[key]#></option>
					<# } #>
				</select>
				</td>
				<# if (Uniware.Utils.editAdditionalCost) { #>
					<td></td>
				<# } #>
				<# if (Uniware.Utils.editBatchCode) { #>
					<td></td>
				<# } #>
				<td></td>
				<# if (Uniware.Utils.editMRP) { #>
					<td></td>
				<# } #>
				<# if (Uniware.Utils.editDiscount) { #>
					<td></td>
					<td></td>
				<# } #>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<# } #>
			<# } #>
		</table><br/><br/>
		<# if (obj.currentGRN.statusCode == 'CREATED' && Uniware.Utils.barcodingBeforeGRN) { #>
			<# if (Uniware.Utils.barcodePreGenerated) { #>
                <div id="skuSelectedDetail"></div>
                <div class="clear"/>
				<div class="lfloat">
				Item SKU : 
				<select id="preGeneratedBarcodeSKU"  class="field-width">
					<option value="">select a product type</option>
					<# for (var key in obj.skuCodes) { #>
					<option value="<#=obj.skuCodes[key]#>" <#= obj.skuCodes[key] == window.page.skuSelected ? selected="selected" : '' #>><#=obj.skuCodes[key]#> - <#=obj.skuNames[key]#></option>
					<# } #>
				</select>
				</div>
			<# } #>
			<div class="lfloat10" style="margin-top:20px;">
                <# if (obj.lastScannedBarcodes.length > 0) { #>
                    Last Scanned Barcodes
                <# for (var i=0;(i<obj.lastScannedBarcodes.length && i < 3);i++) { #>
                    <span style="font-weight:bold;"><#=obj.lastScannedBarcodes[i]#></span> ,
                <# } #>
                    <br/>
                <# } #>
				SCAN ITEM TO ADD <input id="scanItemToAdd" type="text" value=""/>
			</div>
			<div class="clear"/>
			<br/><br/>
		<# } #>

		<# if (obj.currentGRN.statusCode == 'CREATED' && obj.currentGRN.savedItems.length > 0) { #>
			<div id="closeGRN" class=" btn btn-primary lfloat">save & close GRN</div>
			<div class="clear"></div>
			<br/>
		<# } #>
		<# } #>
	</div>
	</script>
    <script id="skuSelectedDetailTemplate" type="text/html">
        <table style="width:100%;">
            <tr>
                <td>Item Sku</td>
                <td><#=obj.itemSKU#></td>
                <td colspan="5">
                    <# if (obj.itemTypeImageUrl != null) { #>
                    <img src="<#=obj.itemTypeImageUrl#>"/>
                    <# } #>
                </td>
            </tr>
            <tr>
                <td>Item Name</td>
                <td><#=obj.itemTypeName#></td>
            </tr>
            <tr>
                <td>Brand</td>
                <td><#=obj.brand#></td>
            </tr>
            <tr>
                <td>Color</td>
                <td><#=obj.color#></td>
            </tr>
            <tr>
                <td>Size</td>
                <td><#=obj.size#></td>
            </tr>
        </table>
    </script>
	<script id="poItemsTemplate" type="text/html">
		<div class="greybor headlable round_top ovrhid mar-15-top">
		<h4 class="edithead">Purchase Order Ordered/Pending Items</h4></div>
		<table class="uniTable po greybor" border="0" cellspacing="0" cellpadding="0">
			<tr style="background:#FAFAFA;">
				<th colspan="3">
					<div class="lfloat">
						<input type="checkbox" id="completedItems"/>
					</div>
					<div class="lfloat">
						<div style="margin-top:2px;" class="f12">show completed purchase order items</div>
					<div>
					<div class="clear"/>
				</th>
				<th width="100"></th>
				<th width="100"></th>
			</tr>
			<tr>
				<th>Item Type</th>
				<th>Product Code</th>
				<th>Vendor SKU Code</th>
				<th>Quantity</th>
				<th>Pending Quantity</th>
			</tr>

			<# for(var i=0; i<obj.purchaseOrderItems.length; i++){ var poItem = obj.purchaseOrderItems[i]; #>
			<tr class="<#=poItem.pendingQuantity > 0 ? '' : 'hidden compItems'#>">
				<td class="first-col">
					<#=poItem.itemTypeName#>
					<#=Uniware.Utils.getItemDetail(poItem.itemTypeImageUrl, poItem.itemTypePageUrl)#>
				</td>
				<td><#=poItem.itemSKU#></td>
				<td style="font-weight:bold;"><#=poItem.vendorSkuCode#></td>
				<td><#=poItem.quantity#></td>
				<td><#=poItem.pendingQuantity#></td>
			</tr>
			<# } #>
		</table>	
		<div>
	</script>	
	<script id="inflowReceipts" type="text/html">
	<div class="greybor headlable round_top ovrhid mar-15-top">
		<h4 class="edithead">Existing GRNs</h4>
		<# if (obj.poStatus == 'APPROVED') { #>
		<div id="showGenerateReceipt" class=" btn btn-small btn-primary rfloat20">+ add new GRN</div>
		<div class="clear"></div>
		<# } #>
	</div>
	<div class="greybor round_bottom form-edit-table-cont">
		<table class="uniTable" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<th>GRN No.</th>
					<th>Status</th>
					<th>Created at</th>
					<th>Action</th>
				</tr>
				<# for(var i=0; i<obj.inflowReceipts.length; i++){ var ir = obj.inflowReceipts[i]; #>
				<tr>
					<td><#=ir.code#></td>
					<td><#=ir.statusCode#></td>
					<td><#=(new Date(ir.created)).toDateTime()#></td>
					<td>
						<# if (Uniware.Utils.editAfterQCComplete || ir.statusCode == 'CREATED' || ir.statusCode == 'QC_PENDING' ) { #>
						<img id="grn-<#=ir.code#>" class="grnLoad" src="/img/icons/up.png" class="lfloat10" title="load this GRN"/>
						<# } #>
					</td>
				</tr>
				<# } #>
		</table>
	</div>
	</script>	
	
	<script type="text/javascript">
		<uc:security accessResource="PO_APPROVE">
			Uniware.Utils.editAfterQCComplete = true;
		</uc:security>
		<uc:security accessResource="UNIT_PRICE_EDIT_AT_GRN">
			Uniware.Utils.editUnitPrice = true;
		</uc:security>
		<uc:security accessResource="MRP_EDIT_AT_GRN">
			Uniware.Utils.editMRP = true;
		</uc:security>
		<uc:security accessResource="DISCOUNT_EDIT_AT_GRN">
			Uniware.Utils.editDiscount = true;
		</uc:security>
		<uc:security accessResource="ADDITIONAL_COST_EDIT_AT_GRN">
			Uniware.Utils.editAdditionalCost = true;
		</uc:security>
		<uc:security accessResource="BATCH_CODE_EDIT_AT_GRN">
			Uniware.Utils.editBatchCode = true;
		</uc:security>
		<sec:authentication property="principal" var="user" />
		Uniware.Utils.barcodePreGenerated = ${configuration.getConfiguration('systemConfiguration').barcodePreGenerated};
		Uniware.Utils.barcodingBeforeQC = ${configuration.getConfiguration('systemConfiguration').barcodingBeforeQualityCheck};
		Uniware.Utils.barcodingBeforeGRN = ${configuration.getConfiguration('systemConfiguration').barcodingBeforeGRN};
		Uniware.Utils.traceabilityLevel = "${configuration.getConfiguration('systemConfiguration').traceabilityLevel}";
		Uniware.Utils.receiveMultipleSkus = ${configuration.getConfiguration('systemConfiguration').allowSingleSkuMultiBatchAtGRN};
		
		Uniware.receivePOPage = function() {
			var self = this;
			this.purchaseOrder = null;
            this.lastScannedBarcodes = [];
			this.itemEditing = null;
			this.pendingPOIs = [];
			this.skuCodes = {};
			this.skuSelected = null;
			this.vendorSkuCodes = {};
			this.skuNames = {};
			this.inflowReceipts = [];
			this.pendingItems = {};
			this.allPOItems = {};
            this.skuDetails = {};
			this.pendingQuantity = {};
			this.facilityCode = '${cache.getCache('facilityCache').getCurrentFacility().getCode()}';
			this.inflowReceiptItemCustomFields = ${configuration.getConfiguration('customFieldsConfiguration').getCustomFieldsByEntityJson('com.uniware.core.entity.InflowReceiptItem')};
			this.grnCustomFieldsMetadata =  ${configuration.getConfiguration('customFieldsConfiguration').getCustomFieldsByEntityJson('com.uniware.core.entity.InflowReceipt')};
			
			this.init = function() {
				$('#poCode, #poCodePrefix').keyup(function(event){
					if (event.which == 13) {
						var purchaseOrderCode = $('#poCodePrefix').val() + $('#poCode').val();
						self.load(purchaseOrderCode);
					}
				});
			};
			
			this.load = function(purchaseOrderCode){
				$("#searching").removeClass('hidden');
				$('#createGrnDiv').html("");
				$('#inflowReceiptDiv').html("");

				var req = {};
				req.purchaseOrderCode = purchaseOrderCode;
				Uniware.Ajax.postJson("/data/inflow/po/fetch", JSON.stringify(req), function(response) {
					if(response.successful == true) {
						self.purchaseOrder = response;
						self.pendingItems = {};
						self.allPOItems = {};
                        self.skuDetails = {};
						for (var i=0;i<response.purchaseOrderItems.length;i++) {
							var item = response.purchaseOrderItems[i];
							if (item.pendingQuantity > 0) {
								self.pendingItems[item.id] = response.purchaseOrderItems[i];
							}
							self.allPOItems[item.id] = item;
                            self.skuDetails[item.itemSKU] = item;
						}
						$('#createGrnDiv').html(template("purchaseOrder", self.purchaseOrder));
						$('#poItemsDiv').removeClass('hidden').html(template("poItemsTemplate", self.purchaseOrder));
						self.loadInflowReceipts();
						$('#poCode').val('');
						Uniware.Utils.applyHover();
						Uniware.Utils.renderComments("#grnCreateComments", 'PO-' + self.purchaseOrder.code + '-' + self.facilityCode);
						$("#poCode").focus();
						$("input#completedItems").click(function() {
							$("tr.compItems").toggleClass('hidden');
						});
					} else {
						Uniware.Utils.showError(response.errors[0].description);
					}
					$("#searching").addClass('hidden');	
				}); 	
			};
			
			this.generateReceipt = function() {
				$("#error").addClass('invisible');
				var req = {
					purchaseOrderCode: self.purchaseOrder.code						
				};
				var wsGRN= { 
					vendorInvoiceNumber : $("#vendorInvoiceNumber").val()
				};
				Uniware.Utils.addCustomFieldsToRequest(wsGRN, $('.custom'));
				if ($("#vendorInvoiceDate").val() != '') {
					wsGRN.vendorInvoiceDate = Date.fromPaddedDate($("#vendorInvoiceDate").val()).getTime(); 
				}
				req.wsGRN = wsGRN;
				Uniware.Ajax.postJson("/data/inflow/po/receive", JSON.stringify(req),function(response){
					if(response.successful ==true){
						Uniware.LightBox.hide();
						self.fetchGRN(response.inflowReceiptCode);
					} else {
						$('#error').html(response.errors[0].description.split('|')[0]).removeClass('invisible');
					}
				});
			};
			
			this.editGRN = function(){
				$("#error").addClass('invisible');
				var req = {
					'inflowReceiptCode' : self.purchaseOrder.currentGRN.code
				};
				var wsGRN= { 
					vendorInvoiceNumber : $("#vendorInvoiceNumber").val()
				};
				Uniware.Utils.addCustomFieldsToRequest(wsGRN, $('.custom'));
				if ($("#vendorInvoiceDate").val() != '') {
					wsGRN.vendorInvoiceDate = Date.fromPaddedDate($("#vendorInvoiceDate").val()).getTime(); 
				}
				req.wsGRN = wsGRN;
				Uniware.Ajax.postJson("/data/inflow/edit/grn", JSON.stringify(req),function(response){
					if(response.successful ==true){
						Uniware.LightBox.hide();
						self.fetchGRN(self.purchaseOrder.currentGRN.code);
					} else {			
						$('#error').html(response.errors[0].description.split('|')[0]).removeClass('invisible');
					}
				});
			};
			
			
			this.renderGRN = function() {

                self.purchaseOrder.skuDetails = self.skuDetails;
				self.purchaseOrder.pendingPOIs = self.pendingPOIs;
				self.purchaseOrder.skuCodes = self.skuCodes;
				self.purchaseOrder.vendorSkuCodes = self.vendorSkuCodes;
				self.purchaseOrder.skuNames = self.skuNames;
				self.purchaseOrder.inflowReceiptItemCustomFields = self.inflowReceiptItemCustomFields;
                self.purchaseOrder.lastScannedBarcodes = self.lastScannedBarcodes;

				for (var key in self.purchaseOrder.currentGRN.itemsMap) {
					var item = self.purchaseOrder.currentGRN.itemsMap[key];
					if (item && item.pendingQuantity && item.itemSKU && self.pendingQuantity[item.itemSKU]) {
						item.pendingQuantity = self.pendingQuantity[item.itemSKU];
					}
				}
				$('#createGrnDiv').html(template("purchaseOrder", self.purchaseOrder));
				$('#editItem').click(self.editItem);
				Uniware.Utils.barcode();
				$('#preGeneratedBarcodeSKU').combobox().change(function(){
                    if ($(this).val() != '') {
                        self.preBarcodeProductSelected($(this).val());
                    }
                });
				$('#itemTypeNameCode,#vendorCode,#productCode').combobox().change(function(){
					if ($(this).val() != 0) {
						self.productSelected($(this).val());
						$(this).val(0);
					}
				});
				$('.saveItem').click(self.saveItem);
				$(".discount,.discountPer,.unitPrice").keyup(self.editPricingInfo);
				$('.remove').click(self.removeItem);
				$('div#closeGRN').click(self.closeGRN);
				$('.printItem').click(self.printLabels);
				
				$(".update").click(self.updateInflowReceiptItem);
				
				$(".cancel").click(self.cancel);
				$("span.table-editable").click(self.editReceiptItem);
				if (Uniware.Utils.barcodingBeforeGRN) {
					$('#scanItemToAdd').focus().keyup(self.scanItemToAdd);
				}
				Uniware.Documents("#grnCreateDocuments", 'IR-' + self.purchaseOrder.currentGRN.code, '${user.username}', 2);			
				Uniware.Utils.applyHover();
			};
			
			this.scanItemToAdd = function(event) {
				if (event.which == 13 && $(this).val() != '') {
					var url = '/data/inflow/receipt/item/add';
                    var itemCode = $(this).val().replace(/[^a-zA-Z0-9_-]/g,'');
					var req = {
						'inflowReceiptCode' : self.purchaseOrder.currentGRN.code,
						'itemCode' : itemCode
					};
					if (Uniware.Utils.barcodePreGenerated) {
						url = '/data/inflow/wap/item/add';
						req.itemSKU = $('#preGeneratedBarcodeSKU').val();
						self.skuSelected = req.itemSKU;
					}
					
					Uniware.Ajax.postJson(url, JSON.stringify(req), function(response) {
						if(response.successful == true) {
							Uniware.Utils.addNotification("Item Added successfully");
							self.fetchGRN(self.purchaseOrder.currentGRN.code);
                            self.lastScannedBarcodes.unshift(itemCode);
							
						} else {
                            Uniware.Utils.showError(response.errors[0].description);
						}
						$('#scanItemToAdd').val('').focus();
					}, true);
				}
			};
			
			this.printLabels = function() {
				var itemSku = $(this).attr('id').substring($(this).attr('id').indexOf('-') + 1);
				var receiptItem = self.purchaseOrder.currentGRN.itemsMap['iri' + itemSku];
				if (Uniware.Utils.traceabilityLevel == 'ITEM') {
                    if (receiptItem.itemsLabelled) {
                        Uniware.Utils.printIFrame('/inflow/items/print/'+ receiptItem.id);
                    } else {
                        var req = {
                            inflowReceiptItemId : receiptItem.id
                        };
                        Uniware.Ajax.postJson("/data/inflow/po/receive/createItems", JSON.stringify(req), function(response) {
                            if(response.successful == true) {
                                Uniware.Utils.printIFrame('/inflow/items/print/'+ receiptItem.id);
                                self.fetchGRN(self.purchaseOrder.currentGRN.code);
                            } else {
                                // should not happen
                            }
                        }, true);
                    }
                } else {
                    $("#printIframeDiv").html('<iframe src="' + Uniware.Path.http('/data/itemType/printBarcodes?'+ receiptItem.itemSKU + "="+receiptItem.quantity) + '" style="display:block;height:1px;width:1px;" frameborder="no"></iframe>');
                }
			};
			
			this.editPricingInfo = function(event) {
				var value = 0;
				var tr = $(event.target).parents('tr');
				var itemCode = $(tr[0]).attr('id').substring($(tr[0]).attr('id').indexOf('-') + 1);
				
				var item = self.allPOItems[itemCode];
				var unitPrice = $(tr[0]).find(".unitPrice").val();
				var unitPrice = unitPrice ? unitPrice : item.unitPrice;
				var discount = $(tr[0]).find(".discount");
				var discountPercentage = $(tr[0]).find(".discountPer");
				if ($(this).hasClass('discountPer')) {
					value = Math.getPercentageAmount(unitPrice,discountPercentage.val());
					discount.val(isNaN(value) ? 0 : value);
				} else if ($(this).hasClass('discount') || $(this).hasClass('unitPrice')) {
					value = Math.getPercentageRate(unitPrice, discount.val());
					discountPercentage.val(isNaN(value) ? 0 : value);
				}
			};
			
			this.editReceiptItem = function(event) {
 				var tr = $(event.target).parents('tr');
				var itemKey = $(tr[0]).attr('id').substring($(tr[0]).attr('id').indexOf('-') + 1);
				var item = self.purchaseOrder.currentGRN.itemsMap['iri' + itemKey];
				if (item.isEditing) {
					
				} else {
					if (self.itemEditing != null && self.itemEditing != itemKey) {
						var orgItem = self.purchaseOrder.currentGRN.itemsMap['iri' + self.itemEditing];
						orgItem.isEditing = null;
					}
					self.itemEditing = itemKey;
					item.isEditing = true;
					self.renderGRN();
				}
			};
			
			this.cancel = function(event) {
				var item = self.purchaseOrder.currentGRN.itemsMap['iri' + $(this).attr('id').substring($(this).attr('id').indexOf('-') + 1)]; 
				item.isEditing = null;
				self.renderGRN();
			};
			
			this.updateInflowReceiptItem = function(event) {
				var iriId = $(this).attr('id').substring($(this).attr('id').indexOf('-') + 1);
				var iri = self.purchaseOrder.currentGRN.itemsMap['iri' + iriId];
				
				var req = {};
				req.purchaseOrderItemId = iri.purchaseOrderItemId;
				req.inflowReceiptItemId = iri.id;
				req.inflowReceiptCode = self.purchaseOrder.currentGRN.code;
				req.inflowReceiptItem = {};
				req.inflowReceiptItem.quantity = parseInt($("#qty-"+iriId).val(), 10) !==  undefined ? parseInt($("#qty-"+iriId).val(), 10) : iri.quantity;
				if ($("#unitPrice-"+iriId).length > 0) {
					req.inflowReceiptItem.unitPrice = $("#unitPrice-"+iriId).val();
				}
				if ($("#mrp-"+iriId).length > 0) {
					req.inflowReceiptItem.maxRetailPrice = $("#mrp-"+iriId).val();
				}
				if ($("#discount-"+iriId).length > 0) {
					req.inflowReceiptItem.discount = $("#discount-"+iriId).val();
				}
				if ($("#addCost-"+iriId).length>0) {
					req.inflowReceiptItem.additionalCost = $("#addCost-"+iriId).val();
				}
				if ($("#batch-"+iriId).length>0) {
					req.inflowReceiptItem.batchCode = $("#batch-"+iriId).val();
				}
				Uniware.Ajax.postJson("/data/inflow/po/receive/item", JSON.stringify(req), function(response) {
					if(response.successful == true) {
						Uniware.Utils.addNotification("Inflow receipt item updated successfully");
						var oldReceiptItem = self.purchaseOrder.currentGRN.itemsMap['iri' + response.inflowReceiptItemDTO.id];
						self.purchaseOrder.currentGRN.totalQuantity -= oldReceiptItem.quantity;
						
						self.purchaseOrder.currentGRN.itemsMap['iri' + response.inflowReceiptItemDTO.id] = response.inflowReceiptItemDTO;
						self.purchaseOrder.currentGRN.totalQuantity += response.inflowReceiptItemDTO.quantity;
						self.pendingQuantity[response.inflowReceiptItemDTO.itemSKU] = response.inflowReceiptItemDTO.pendingQuantity;
						
						self.renderGRN();
						
					} else {
						Uniware.Utils.showError(response.errors[0].description);
					}
				}, true);
			};
			
			this.closeGRN = function() {
				var action = confirm('Are you sure?');
				if (action == false)
					return;
				
				var req = {
					inflowReceiptCode : self.purchaseOrder.currentGRN.code
				};
				
				Uniware.Ajax.postJson("/data/inflow/po/receive/sendForQC", JSON.stringify(req), function(response) {
					if(response.successful == true) {
						self.fetchGRN(self.purchaseOrder.currentGRN.code);				
					} else {
						Uniware.Utils.showError(response.errors[0].description);
					}
				}, true);
			};
			
			this.removeItem = function() {
				var itemCode = $(this).attr('id').substring($(this).attr('id').indexOf('-') + 1);
				self.purchaseOrder.currentGRN.currentItems.remove(itemCode);
				self.purchaseOrder.currentGRN.itemsMap['poi' + itemCode] = null;
				
				self.pendingPOIs.push(itemCode);
				self.skuCodes[itemCode] = self.allPOItems[itemCode].itemSKU;
				self.vendorSkuCodes[itemCode] = self.allPOItems[itemCode].vendorSkuCode;
				self.skuNames[itemCode] = self.allPOItems[itemCode].itemTypeName;
				self.renderGRN();
			};
			
			this.saveItem = function() {
				var poItemId = $(this).attr('id').substring($(this).attr('id').indexOf('-') + 1);
				var poItem = self.pendingItems[poItemId];
				
				var req = {};
				req.purchaseOrderItemId = poItem.id;
				req.inflowReceiptCode = self.purchaseOrder.currentGRN.code;
				req.inflowReceiptItem = {};
				req.inflowReceiptItem.quantity = parseInt($("#qty-"+poItemId).val());
				if ($("#unitPrice-"+poItemId).length > 0) {
					req.inflowReceiptItem.unitPrice = $("#unitPrice-"+poItemId).val();
				}
				if ($("#addCost-"+poItemId).length>0) {
					req.inflowReceiptItem.additionalCost = $("#addCost-"+poItemId).val();
				}
				if ($("#batch-"+poItemId).length>0) {
					req.inflowReceiptItem.batchCode = $("#batch-"+poItemId).val();
				}
				if ($("#mrp-"+poItemId).length > 0) {
					req.inflowReceiptItem.maxRetailPrice = $("#mrp-"+poItemId).val();
				} else {
					req.inflowReceiptItem.maxRetailPrice = poItem.maxRetailPrice;
				}
				if ($("#discount-"+poItemId).length > 0) {
					req.inflowReceiptItem.discount = $("#discount-"+poItemId).val();
				}
				Uniware.Ajax.postJson("/data/inflow/po/receive/item", JSON.stringify(req), function(response) {
					if(response.successful == true) {
						self.purchaseOrder.currentGRN.currentItems.remove(poItemId);
						self.purchaseOrder.currentGRN.savedItems.push('iri' + response.inflowReceiptItemDTO.id);
						self.purchaseOrder.currentGRN.itemsMap['iri' + response.inflowReceiptItemDTO.id] = response.inflowReceiptItemDTO;
						
						self.purchaseOrder.currentGRN.totalQuantity += response.inflowReceiptItemDTO.quantity;
						self.pendingQuantity[response.inflowReceiptItemDTO.itemSKU] = response.inflowReceiptItemDTO.pendingQuantity;
                        self.fetchGRN(self.purchaseOrder.currentGRN.code);
					} else {
						Uniware.Utils.showError(response.errors[0].description);
					}
				}, true);
				
			};
			
			this.productSelected = function(purchaseOrderItemId) {
				//converting to string as remove method makes the type check
				purchaseOrderItemId = "" + purchaseOrderItemId;
				self.pendingPOIs.remove(purchaseOrderItemId);
				delete self.skuCodes[purchaseOrderItemId];
				delete self.vendorSkuCodes[purchaseOrderItemId];
				delete self.skuNames[purchaseOrderItemId];

				self.purchaseOrder.currentGRN.itemsMap['poi' + purchaseOrderItemId] = self.pendingItems[purchaseOrderItemId];
				self.purchaseOrder.currentGRN.currentItems.push(purchaseOrderItemId);
				self.renderGRN();
			};

            this.preBarcodeProductSelected = function(productCode) {
                $('#skuSelectedDetail').html(template("skuSelectedDetailTemplate", self.skuDetails[productCode]));
                $('#scanItemToAdd').focus();
            };

			this.loadGRN = function() {
				self.fetchGRN($(this).attr('id').substring($(this).attr('id').indexOf('-') + 1));
			};
			
			this.fetchGRN = function(inflowReceiptCode) {
				var req = {
					'inflowReceiptCode': inflowReceiptCode
				};
				Uniware.Ajax.postJson("/data/inflow/receipt/fetch", JSON.stringify(req), function(response) {
					if(response.successful == true) {
						
						self.purchaseOrder.currentGRN = response.inflowReceipt;
						self.purchaseOrder.currentGRN.itemsMap = {};
						self.purchaseOrder.currentGRN.currentItems = [];
						self.purchaseOrder.currentGRN.savedItems = [];
						self.pendingPOIs = [];
						self.skuCodes = {};
						self.vendorSkuCodes = {};
						self.skuNames = {};
						
						for (var key in self.pendingItems) {
							var item = self.pendingItems[key];
							self.pendingPOIs.push(key);
							self.skuCodes[key] = item.itemSKU;
							self.vendorSkuCodes[key] = item.vendorSkuCode;
							self.skuNames[key] = item.itemTypeName;
						}
						for (var i=0;i<response.inflowReceipt.inflowReceiptItems.length;i++) {
							var iri = response.inflowReceipt.inflowReceiptItems[i];
							
							self.purchaseOrder.currentGRN.itemsMap['iri' + iri.id] = iri;
							self.purchaseOrder.currentGRN.savedItems.push('iri' + iri.id);
							if (Uniware.Utils.receiveMultipleSkus != true) {
								var purchaseOrderItemId = "" + iri.purchaseOrderItemId;
								self.pendingPOIs.remove(purchaseOrderItemId);
								delete self.skuCodes[purchaseOrderItemId];
								delete self.vendorSkuCodes[purchaseOrderItemId];
								delete self.skuNames[purchaseOrderItemId];
							}
						}
						self.purchaseOrder.currentGRN.inflowReceiptItems = null;
						self.renderGRN();
						$('#inflowReceiptsDiv').addClass('hidden');
						$('#poItemsDiv').addClass('hidden');
						
					} else {
						Uniware.Utils.showError(response.errors[0].description);
					}
				}, true);
			};
			
			this.editItem = function() {
				var obj = self.purchaseOrder.currentGRN;
				obj.grnCustomFieldsMetadata = self.grnCustomFieldsMetadata;
				$("#grnCreateTemplateDiv").html(template("grnCreateTemplate", obj));
				$(".datefield").datepicker({
					dateFormat : 'dd/mm/yy'
				});
				$('#editGRN').click(function(){
					self.editGRN();
				});
				Uniware.LightBox.show('#grnCreateDiv', function(){
					$("#vendorInvoiceNumber").focus();
				});
			};
			
			this.loadInflowReceipts = function() {
				var req = {
					'purchaseOrderCode': self.purchaseOrder.code
				};
				
				Uniware.Ajax.postJson("/data/inflow/po/inflowReceipts/fetch", JSON.stringify(req), function(response) {
					if(response.successful == true) {
						response.poStatus = self.purchaseOrder.statusCode;
						self.inflowReceipts = response.inflowReceipts;
						$('#inflowReceiptsDiv').removeClass('hidden').html(template("inflowReceipts", response));
						$('.grnLoad').click(self.loadGRN);
						$("#showGenerateReceipt").click(function(){
							$("#grnCreateTemplateDiv").html(template("grnCreateTemplate", {
								'inflowReceipts' : response.inflowReceipts,
								'grnCustomFieldsMetadata' : self.grnCustomFieldsMetadata
							}));
							$(".datefield").datepicker({
								dateFormat : 'dd/mm/yy'
							});
							$('#copyFromGRN').change(function(){
								self.copyGRNValues();
							});
							$("#generateReceipt").click(function(){
								self.generateReceipt();
							});
							Uniware.LightBox.show('#grnCreateDiv', function(){
								$("#vendorInvoiceNumber").focus();
							});
						});
					} else {
						Uniware.Utils.showError(response.errors[0].description);
					}
				});
			};
			
			this.copyGRNValues = function() {
				var value = $('#copyFromGRN').val();
				if (value != "") {
					var grn = self.inflowReceipts[value];
					$('#vendorInvoiceNumber').val(grn.vendorInvoiceNumber);
					$('#vendorInvoiceDate').val(new Date(grn.vendorInvoiceDate).toPaddedDate());
					for (var i=0;i<grn.customFieldValues.length;i++) {
						var customField = grn.customFieldValues[i];
						if(customField.valueType == 'date') {
							$('#' + customField.fieldName).val(new Date(customField.fieldValue).toPaddedDate());
						} else if (customField.valueType == 'checkbox') {
							$('#' + customField.fieldName).attr('checked', customField.fieldValue);
						} else {
							$('#' + customField.fieldName).val(customField.fieldValue);	
						}
					}
				} else {
					$('#showGenerateReceipt').trigger('click');
				}
			};
			
		};
					
		$(document).ready(function() {
			window.page = new Uniware.receivePOPage();
			window.page.init();
			var poCode = Uniware.Utils.getParameterByName('po');
			if (poCode && poCode != '') {
				window.page.load(poCode);
			}
			$('#poCode').focus();
		});
	</script>
	</tiles:putAttribute>
</tiles:insertDefinition>