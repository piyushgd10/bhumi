<%@ include file="/tagIncludes.jsp"%>
<tiles:insertDefinition name=".inflowPage">
	<tiles:putAttribute name="title" value="Uniware - Help Pages" />
	<tiles:putAttribute name="rightPane">
	<div id="helpDiv" style="width:100%;height:100%;">
	</div>
	</tiles:putAttribute>
	<tiles:putAttribute name="deferredScript">
		<script type="text/javascript">
			$(document).ready(function() {
				var helpUrl = window.location.pathname.split('/')[3];
				var frame="<iframe src='https://partner.jabong.com/" + helpUrl + "' style='display:block;margin-top:3px;width:100%;height:100%;' frameborder='no' scrolling='auto'></iframe>";
				$("#helpDiv").html(frame);
			});
		</script>
	</tiles:putAttribute>
</tiles:insertDefinition>