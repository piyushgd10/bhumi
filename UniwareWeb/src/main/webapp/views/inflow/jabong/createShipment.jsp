<%@ include file="/tagIncludes.jsp"%>
<tiles:insertDefinition name=".inflowPage">
	<tiles:putAttribute name="title" value="Uniware - Create Shipment" />
	<tiles:putAttribute name="rightPane">
		<div class="noprint">
			<form onsubmit="javascript : return false;">
				<div class="greybor headlable ovrhid main-box-head">
					<h2 class="edithead head-textfields">Scan Item</h2>
					<div class="lfloat">
						<input id="itemCode" type="text" size="20"  autocomplete="off">
					</div>
					<div id="searching" class="lfloat10 hidden" style="margin-top:5px;">
						<img src="/img/icons/refresh-animated.gif"/>
					</div>
				</div>
				<div class="clear"></div>
				<div id="itemDiv"></div>
				<div id="itemComments" class="round_all"></div>
			</form>
		</div>
	</tiles:putAttribute>
	<tiles:putAttribute name="deferredScript">
	<script id="orderTemplate" type="text/html">
		<div class="greybor round_bottom main-boform-cont pad-15-top overhid">
		<# for (var i=0;i<obj.customFieldValues.length;i++) { var customFieldValue = obj.customFieldValues[i]; #>
			<# if (customFieldValue.fieldName == 'campaign') { #>
				<# var campaign = customFieldValue.fieldValue; #>
			<# } #>
		<# } #>
		<table width="100%" border="0" cellspacing="1" cellpadding="3" class="fields-table">
		<tr> 
        	<td>Order Number</td>
	        <td><#=obj.code#></td>
    	    <td>Created at</td>
        	<td><#=new Date(obj.created).toDateTime()#></td>    
		</tr>
		<tr> 
        	<td>Campaign</td>
	        <td><#=campaign#></td>
    	    <td></td>
        	<td></td>    
		</tr>
		<tr> 
        	<td colspan="4">Order Items</td>  
		</tr>
		</table>
		<table>
			<tr>
				<td width="5%"></td>
				<td>Item Code</td>
				<td>Order Item</td>
				<td>SurfaceMode</td>
				<td>Bundle</td>
				<td>Expected Dispatch Date</td>
				<td>Status</td>
				<td>Shipment</td>
			</tr>

			<# for (var i=0;i<obj.saleOrderItems.length;i++) { var saleOrderItem = obj.saleOrderItems[i]; if(window.page.itemCodes[saleOrderItem.item]) { #>
				<# for (var j=0;j<saleOrderItem.customFieldValues.length;j++) { var customFieldValue = saleOrderItem.customFieldValues[j]; #>
					<# if (customFieldValue.fieldName == 'is_surface') { #>
						<# var surface = customFieldValue.fieldValue; #>
					<# } #>
					<# if (customFieldValue.fieldName == 'Sku_Bundle') { #>
						<# var bundle = customFieldValue.fieldValue; #>
					<# } #>
					<# if (customFieldValue.fieldName == 'expected_dispatch_date') { #>
						<# var expectedDispatchDate = customFieldValue.fieldValue ? new Date(customFieldValue.fieldValue).toDate() : ''; #>
					<# } #>
				<# } #>
			<tr>
				<td><input type="checkbox" class="saleOrderItem" id="saleOrderItem-<#=i#>" <#=(saleOrderItem.statusCode != 'FULFILLABLE' && saleOrderItem.statusCode != 'CANCELLED') || saleOrderItem.shippingPackageCode != null ? 'disabled="disabled"' : 'checked="checked"' #> /></td>
				<td><#=saleOrderItem.item ==  null ? ' - ' : saleOrderItem.item#></td>
				<td><#=saleOrderItem.code#></td>
				<td><#=surface ? 'Surface' : 'Air'#></td>
				<td><#=bundle#></td>
				<td><#=expectedDispatchDate#></td>
				<td><#=saleOrderItem.statusCode#></td>
				<td><a href="/shipping/scanShipment?legacy=1&shipmentCode=<#=saleOrderItem.shippingPackageCode#>"><#=saleOrderItem.shippingPackageCode#></a></td>
			</tr>
			<# }} #>
		</table>
		</div><br/>
		<div class="btn btn-success lfloat20" id="createShipment">Create Shipment</div>
	</script>
	<script id="itemTemplate" type="text/html">
		<div class="greybor round_bottom main-boform-cont pad-15-top overhid">
		<table width="100%" border="0" cellspacing="1" cellpadding="3" class="fields-table">
		<tr> 
        	<td colspan="4">
       			<div class=" btn rfloat" id="reprintLabel">reprint label</div>
			</td>
		</tr>
		<tr> 
        	<td>Item Number</td>
	        <td><#=obj.code#></td>
    	    <td>Created at</td>
        	<td><#=new Date(obj.created).toDateTime()#></td>    
		</tr>
		<tr> 
        	<td>Status</td>
	        <td class="bold"><#=obj.status#></td>
    	    <td></td>
        	<td></td>    	    
		</tr>
		<tr> 
			<td>Item Name</td>
        	<td>
				<#=obj.itemTypeName#>
				<#=Uniware.Utils.getItemDetail(obj.itemTypeImageUrl, obj.itemTypePageUrl)#>
			</td>    
        	<td>SKU Code</td>
	        <td><#=obj.itemSKU#></td>
		</tr>
		<tr> 
        	<td>Color</td>
	        <td><#=obj.color#></td>
    	    <td>Size</td>
        	<td><#=obj.size#></td>    	    
		</tr>
		<tr> 
        	<td>MRP</td>
	        <td><#=obj.maxRetailPrice#></td>
    	    <td>Brand</td>
        	<td><#=obj.brand#></td>    	    
		</tr>
		<tr> 
        	<td>GRN Number</td>
	        <td class="bold"><#=obj.inflowReceiptCode#></td>
    	    <td>Vendor SKU Code</td>
        	<td><#=obj.vendorSkuCode#></td>    
		</tr>
		<tr> 
        	<td>Putaway Number</td>
	        <td><#=obj.lastPutawayCode#></td>
    	    <td>Attachment:</td>
        	<td><span id="grnDocuments"></span></td>    
		</tr>
		<# if (obj.itemTypeCustomFieldValues && obj.itemTypeCustomFieldValues.length > 0) { #>
			<tr>
			<# for(var i=0;i<obj.itemTypeCustomFieldValues.length;i++) { var customField = obj.itemTypeCustomFieldValues[i]; #>
	 			<td><#=customField.displayName#></td>
				<td>
					<# if (customField.valueType == 'date') { #>
						<#= customField.fieldValue ? new Date(customField.fieldValue).toPaddedDate() : '' #>
					<# } else if (customField.valueType == 'checkbox') { #>
						<input type="checkbox" disabled="disabled" <#=customField.fieldValue ? checked="checked" : '' #>/>
					<# } else { #>
						<#=customField.fieldValue#>
					<# } #>
				</td>
				<# if (i == obj.itemTypeCustomFieldValues.length - 1 && i % 2 == 0) { #>
					<td></td><td></td>
				<# } else if (i != 0 && i % 2 != 0) { #>
					</tr><tr>
				<# } #>
			<# } #>
			</tr>				
        <# } #>
		<# if (obj.saleOrderCustomFieldValues && obj.saleOrderCustomFieldValues.length > 0) { #>
			<tr>
			<# for(var i=0;i<obj.saleOrderCustomFieldValues.length;i++) { var customField = obj.saleOrderCustomFieldValues[i]; #>
	 			<td><#=customField.displayName#></td>
				<td>
					<# if (customField.valueType == 'date') { #>
						<#= customField.fieldValue ? new Date(customField.fieldValue).toPaddedDate() : '' #>
					<# } else if (customField.valueType == 'checkbox') { #>
						<input type="checkbox" disabled="disabled" <#=customField.fieldValue ? checked="checked" : '' #>/>
					<# } else { #>
						<# if (customField.fieldValue == 'Exchange') { #>
							<span style="font-size:24px;color:red;text-transform:uppercase;"><#=customField.fieldValue#></span>
						<# } #>
					<# } #>
				</td>
				<# if (i == obj.saleOrderCustomFieldValues.length - 1 && i % 2 == 0) { #>
					<td></td><td></td>
				<# } else if (i != 0 && i % 2 != 0) { #>
					</tr><tr>
				<# } #>
			<# } #>
			</tr>				
        <# } #>
		</table>
		<# if (obj.saleOrderItemDTOs.length > 0) { #>
			<div class="greybor headlable round_top ovrhid">
				<h4 class="edithead">Item Related to Sale Order Item</h4>
			</div>
			<div class="greybor round_bottom form-edit-table-cont">
				<table class="uniTable" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td>Sale Order</td>
					<td>Sale Order Item Code</td>
					<td>Sale Order Item Status</td>
					<td>Shipping Package</td>
					<td>Shipping Package Status</td>
					<td>Picklist Number</td>
				</tr>
				<# for (var i=0;i < obj.saleOrderItemDTOs.length;i++) { var saleOrderItem = obj.saleOrderItemDTOs[i]; #>
				<tr class="f15">
					<td><a href="/reports/editSaleOrder?legacy=1&orderNum=<#=saleOrderItem.saleOrderCode#>"><#=saleOrderItem.saleOrderCode#></a></td>
					<td><#=saleOrderItem.saleOrderItemCode#></td>
					<td><#=saleOrderItem.saleOrderItemStatus#></td>
					<td><a href="/shipping/scanShipment?legacy=1&shipmentCode=<#=saleOrderItem.shippingPackageCode#>"><#=saleOrderItem.shippingPackageCode#></a></td>
					<td><#=saleOrderItem.shippingPackageStatus#></td>
					<td><#=saleOrderItem.picklistNumber#></td>
				</tr>
				<# } #>
				</table>
			</div>
		<# } #>
		</div>
	</script>
	<script type="text/javascript">
		<sec:authentication property="principal" var="user" />
        Uniware.Utils.isBundleRestriction = true;
        <uc:security accessResource="JABONG_NO_BUNDLE_RESTRICT">
            Uniware.Utils.isBundleRestriction = false;
        </uc:security>
		Uniware.Utils.rejectionComments = {};
		Uniware.Utils.commentCount = 0;
		<c:set var="comments" value="${cache.getCache('uiCustomizations').getUICustomListOptions('poLabelQC:rejectionComment')}"></c:set>
		<c:if test="${comments ne null}">
			<c:forEach items="${comments}" var="comment">
				Uniware.Utils.rejectionComments['${comment.name}'] = '${comment.value}';
				Uniware.Utils.commentCount++;
			</c:forEach>
		</c:if>
		Uniware.SearchItemPage = function() {
			var self = this;	
			this.itemCode;
			this.itemCodes = {};
			this.bundleCount;
			this.bundleCancelledCount;
			this.orderNum;
			
			this.init = function() {
				$('#itemCode').keyup(self.load);
			};
			
			this.load = function(event){
				if (event.which == 13 && $('#itemCode').val() != '') {
					$('#itemDiv').html("");
					self.searchItem($('#itemCode').val());
				}
			};
			
			this.searchItem = function(itemCode) {
				self.orderNum = null;
				self.bundleCount = {};
				self.bundleCancelledCount = {};
				var req = {
					'itemCode': itemCode
				};
				Uniware.Ajax.postJson("/data/item/fetch", JSON.stringify(req), function(response) {
					if(response.successful == true) {
						
						for (var i=0;i<response.itemDTO.saleOrderItemDTOs.length;i++) {
							var saleOrderItem = response.itemDTO.saleOrderItemDTOs[i];
							if (saleOrderItem.saleOrderItemStatus == 'FULFILLABLE' || saleOrderItem.saleOrderItemStatus == 'CANCELLED') {
								self.orderNum = saleOrderItem.saleOrderCode;
								for (var item in self.itemCodes) {
									if (saleOrderItem.saleOrderCode != self.itemCodes[item]) {
										Uniware.Utils.showError('This items belongs to a different order.');
										self.itemCodes = {};
										return;
									}
									
								}
								self.itemCode = response.itemDTO.code;
								self.itemCodes[response.itemDTO.code] = saleOrderItem.saleOrderCode;
								break;
							}
						}
						Uniware.Ajax.postJson("/data/oms/saleorder/fetch", JSON.stringify({'code':self.orderNum}), self.onOrderFetch);
						
					} else {
						Uniware.Utils.showError(response.errors[0].description);
					}
					$('#itemCode').val('').focus();
				}); 	
			};
			
			this.onOrderFetch = function(data) {
				var saleOrderDTO = data.saleOrderDTO;
				if (data.successful && saleOrderDTO) {
					self.orderDTO = saleOrderDTO;
					for (var i=0;i < saleOrderDTO.saleOrderItems.length;i++) { 
						var saleOrderItem = saleOrderDTO.saleOrderItems[i];
						if (saleOrderItem.shippingPackageStatus == 'CREATED') {
							saleOrderItem.shippingPackageCode = null;
						}
						for (var j=0;j<saleOrderItem.customFieldValues.length;j++) { 
							var customFieldValue = saleOrderItem.customFieldValues[j];
							if(customFieldValue.fieldName == 'Sku_Bundle' && customFieldValue.fieldValue != null && customFieldValue.fieldValue != '') {
								if (saleOrderItem.item == null && saleOrderItem.statusCode == 'CANCELLED') {
									// DONT ADD TO BUNDLING COUNT
								} else {
									if (self.bundleCount[customFieldValue.fieldValue]) {
										self.bundleCount[customFieldValue.fieldValue] = self.bundleCount[customFieldValue.fieldValue] + 1;
									} else {
										self.bundleCount[customFieldValue.fieldValue] = 1
									}
								}
							}
						}
					}
					$('#itemDiv').html(template("orderTemplate", saleOrderDTO));
					$('#createShipment').click(self.createShipment);
				} else {
					Uniware.Utils.showError('No Order Found, Please enter a valid Order Code');
				}
			};
			
			this.createShipment = function() {
				var req = {
					'saleOrderCode' : self.orderNum,
					'saleOrderItemCodes' : [],
                    'doNotCreateInvoice' : window.location.href.indexOf('myntra') != -1
				}
				var selectedItems = $("input.saleOrderItem:checked");
				if (selectedItems.length == 0) {
					alert('No Items selected');
					return;
				}
				var surfaceMode;
				var allbundles = {};
				var cancelledItems = 0;
				var dontProcess = false;
				selectedItems.each(function(){
					var saleOrderItem = self.orderDTO.saleOrderItems[$(this).attr('id').split('-')[1]];
					if (saleOrderItem.statusCode == 'CANCELLED') {
						cancelledItems++;
					}
					for (var j=0;j<saleOrderItem.customFieldValues.length;j++) { 
						var customFieldValue = saleOrderItem.customFieldValues[j];
						if(customFieldValue.fieldName == 'is_surface') {
							surfaceMode = surfaceMode != null ? surfaceMode : customFieldValue.fieldValue;
							if (surfaceMode != customFieldValue.fieldValue) {
								dontProcess = true;
								alert("Air/Surface Items can't be shipped together");
								return;
							}
						}
						if(customFieldValue.fieldName == 'Sku_Bundle' && customFieldValue.fieldValue != null && customFieldValue.fieldValue != '') {
							if (allbundles[customFieldValue.fieldValue]) {
								allbundles[customFieldValue.fieldValue] = allbundles[customFieldValue.fieldValue] + 1;
							} else {
								allbundles[customFieldValue.fieldValue] = 1
							}
						}
					}
					req.saleOrderItemCodes.push(saleOrderItem.code);
				});
				if (dontProcess) {
					return;
				}
                if (Uniware.Utils.isBundleRestriction) {
                    for (var m in allbundles) {
                        var count = allbundles[m];
                        if (count < self.bundleCount[m]) {
                            alert("All items of bundle " + m + " are not present.");
                            return;
                        }
                    }
                }

				
				if(cancelledItems > 0 && cancelledItems != selectedItems.length) {
					alert('Different status items cant be packed together');
					return;
				}
				if (cancelledItems == 0) {
					alert('Packet will be created successfully. Pack the items');
				}
				if (cancelledItems > 0) {
					alert('Packet will be created successfully. Do not Pack the items');
				}
				Uniware.Ajax.postJson("/data/jabong/shipment/create", JSON.stringify(req), function(response) {
					if (response.successful == false) {
						Uniware.Utils.showError(response.errors[0].description);
					} else {
						self.itemCodes = {};
						Uniware.Utils.addNotification('Shipment ' + response.shipmentCode + ' has been created.');
						Uniware.Utils.printIFrame("/oms/shipment/show/" + response.shipmentCode + '?legacy=1');
						Uniware.Ajax.postJson("/data/oms/saleorder/fetch", JSON.stringify({'code':self.orderNum}), self.onOrderFetch);
					}
				}, true);
				
			}
		}
		
		
		$(document).ready(function() {
			window.page = new Uniware.SearchItemPage();
			window.page.init();
			if ($("#itemCode").val() != '') {
				$('#itemCode').trigger(Uniware.Event.ENTER_KEYUP);
			}
		});
	</script>
	</tiles:putAttribute>

</tiles:insertDefinition>
