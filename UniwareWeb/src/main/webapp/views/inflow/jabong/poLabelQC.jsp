<%@ include file="/tagIncludes.jsp"%>
<tiles:insertDefinition name=".inflowPage">
	<tiles:putAttribute name="title" value="Uniware - Quanlit Check Purchase Order Label" />
	<tiles:putAttribute name="rightPane">
		<div class="noprint">
			<form onsubmit="javascript : return false;">
				<div class="greybor headlable ovrhid main-box-head">
					<h2 class="edithead head-textfields">Scan Item</h2>
					<div class="lfloat">
						<input id="itemCode" type="text" size="20"  autocomplete="off">
					</div>
					<div id="searching" class="lfloat10 hidden" style="margin-top:5px;">
						<img src="/img/icons/refresh-animated.gif"/>
					</div>
				</div>
				<div class="clear"></div>
				<div id="itemDiv"></div>
				<div id="itemComments" class="round_all"></div>
			</form>
		</div>
	</tiles:putAttribute>
	<tiles:putAttribute name="deferredScript">
	<script id="itemTemplate" type="text/html">
		<div class="greybor round_bottom main-boform-cont pad-15-top overhid">
		<table width="100%" border="0" cellspacing="1" cellpadding="3" class="fields-table">
		<tr> 
        	<td colspan="4">
       			<div class=" btn rfloat hidden" id="reprintLabel">reprint label</div>
			</td>
		</tr>
		<tr> 
        	<td>Item Number</td>
	        <td><#=obj.code#></td>
    	    <td>Created at</td>
        	<td><#=new Date(obj.created).toDateTime()#></td>    
		</tr>
		<tr> 
        	<td>Status</td>
	        <td class="bold"><#=obj.status#></td>
    	    <td></td>
        	<td></td>    	    
		</tr>
		<tr> 
			<td>Item Name</td>
        	<td>
				<#=obj.itemTypeName#>
				<#=Uniware.Utils.getItemDetail(obj.itemTypeImageUrl, obj.itemTypePageUrl)#>
			</td>    
        	<td>SKU Code</td>
	        <td><#=obj.itemSKU#></td>
		</tr>
		<tr> 
        	<td>Color</td>
	        <td><#=obj.color#></td>
    	    <td>Size</td>
        	<td><#=obj.size#></td>    	    
		</tr>
		<tr> 
        	<td>MRP</td>
	        <td><#=obj.maxRetailPrice#></td>
    	    <td>Brand</td>
        	<td><#=obj.brand#></td>    	    
		</tr>
		<tr> 
        	<td>GRN Number</td>
	        <td class="bold"><#=obj.inflowReceiptCode#></td>
    	    <td>Vendor SKU Code</td>
        	<td><#=obj.vendorSkuCode#></td>    
		</tr>
		<tr> 
        	<td>Putaway Number</td>
	        <td><#=obj.lastPutawayCode#></td>
    	    <td>Attachment:</td>
        	<td><span id="grnDocuments"></span></td>    
		</tr>
		<# if (obj.itemTypeCustomFieldValues && obj.itemTypeCustomFieldValues.length > 0) { #>
			<tr>
			<# for(var i=0;i<obj.itemTypeCustomFieldValues.length;i++) { var customField = obj.itemTypeCustomFieldValues[i]; #>
	 			<td><#=customField.displayName#></td>
				<td>
					<# if (customField.valueType == 'date') { #>
						<#= customField.fieldValue ? new Date(customField.fieldValue).toPaddedDate() : '' #>
					<# } else if (customField.valueType == 'checkbox') { #>
						<input type="checkbox" disabled="disabled" <#=customField.fieldValue ? checked="checked" : '' #>/>
					<# } else { #>
						<#=customField.fieldValue#>
					<# } #>
				</td>
				<# if (i == obj.itemTypeCustomFieldValues.length - 1 && i % 2 == 0) { #>
					<td></td><td></td>
				<# } else if (i != 0 && i % 2 != 0) { #>
					</tr><tr>
				<# } #>
			<# } #>
			</tr>				
        <# } #>
		<# if (obj.saleOrderCustomFieldValues && obj.saleOrderCustomFieldValues.length > 0) { #>
			<tr>
			<# for(var i=0;i<obj.saleOrderCustomFieldValues.length;i++) { var customField = obj.saleOrderCustomFieldValues[i]; #>
	 			<td><#=customField.displayName#></td>
				<td>
					<# if (customField.valueType == 'date') { #>
						<#= customField.fieldValue ? new Date(customField.fieldValue).toPaddedDate() : '' #>
					<# } else if (customField.valueType == 'checkbox') { #>
						<input type="checkbox" disabled="disabled" <#=customField.fieldValue ? checked="checked" : '' #>/>
					<# } else if (customField.displayName == "Cut off time") { #>
						<span style="font-size:16px;"><#=customField.fieldValue#></span>
					<# }else { #>
						<# if (customField.fieldValue == 'Exchange') { #>
							<span style="font-size:24px;color:red;text-transform:uppercase;"><#=customField.fieldValue#></span>
						<# } #>
					<# } #>
				</td>
				<# if (i == obj.saleOrderCustomFieldValues.length - 1 && i % 2 == 0) { #>
					<td></td><td></td>
				<# } else if (i != 0 && i % 2 != 0) { #>
					</tr><tr>
				<# } #>
			<# } #>
			</tr>				
        <# } #>
		</table>
		<# if (obj.saleOrderItemDTOs.length > 0) { #>
			<div class="greybor headlable round_top ovrhid">
				<h4 class="edithead">Item Related to Sale Order Item</h4>
			</div>
			<div class="greybor round_bottom form-edit-table-cont">
				<table class="uniTable" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td>Sale Order</td>
					<td>Sale Order Item Code</td>
					<td>Sale Order Item Status</td>
					<td>Shipping Package</td>
					<td>Shipping Package Status</td>
					<td>Picklist Number</td>
				</tr>
				<# for (var i=0;i < obj.saleOrderItemDTOs.length;i++) { var saleOrderItem = obj.saleOrderItemDTOs[i]; #>
				<tr class="f15">
					<td><a href="/reports/editSaleOrder?legacy=1&orderNum=<#=saleOrderItem.saleOrderCode#>"><#=saleOrderItem.saleOrderCode#></a></td>
					<td><#=saleOrderItem.saleOrderItemCode#></td>
					<td><#=saleOrderItem.saleOrderItemStatus#></td>
					<td><a href="/shipping/scanShipment?legacy=1&shipmentCode=<#=saleOrderItem.shippingPackageCode#>"><#=saleOrderItem.shippingPackageCode#></a></td>
					<td><#=saleOrderItem.shippingPackageStatus#></td>
					<td><#=saleOrderItem.picklistNumber#></td>
				</tr>
				<# } #>
				</table>
			</div>
		<# } #>
		</div><br/><br/>
		<# if (obj.status == 'QC_PENDING' || obj.status == 'GOOD_INVENTORY' ) { #>
            <div class="lfloat20">
                <span style="font-weight:bold;">50-50</span>&#160;&#160;&#160;
                <input id="qcPass50" type="checkbox" style="margin-top:-2px;"/><br/><br/>
                <div class="btn btn-success lfloat" id="acceptItem">Accept Item</div>
            </div>

			<div class="rfloat20">		
				<div class="rfloat">
					Rejection Reason<br/>
					<# if(Uniware.Utils.commentCount > 0) { #>
						<select id="rejectionComment" class="view">
							<# for(var comment in Uniware.Utils.rejectionComments) { #>
								<option value="<#=Uniware.Utils.rejectionComments[comment]#>"><#=comment#></option>
							<# } #>
						</select>
					<# } else { #>
						<textarea id="rejectionComment" type="text" style="height: 40px; width: 300px;"/>
					<# } #>	
				</div>
				<div class="clear"/><br/>
				<div class="btn btn-danger lfloat" id="rejectItem">Reject Item</div>
				<div class="clear"/>
			</div>
			<div class="clear"/>
		<# } #>
	</script>
	<script type="text/javascript">
        <uc:security accessResource="PO_QCLABEL_REPRINT">
            Uniware.Utils.reprintLabel = true;
        </uc:security>
		<sec:authentication property="principal" var="user" />
		Uniware.Utils.rejectionComments = {};
		Uniware.Utils.commentCount = 0;
        Uniware.Utils.isMyntra = window.location.href.indexOf('myntra') != -1;
		<c:set var="comments" value="${cache.getCache('uiCustomizations').getUICustomListOptions('poLabelQC:rejectionComment')}"></c:set>
		<c:if test="${comments ne null}">
			<c:forEach items="${comments}" var="comment">
				Uniware.Utils.rejectionComments['${comment.name}'] = '${comment.value}';
				Uniware.Utils.commentCount++;
			</c:forEach>
		</c:if>
		Uniware.SearchItemPage = function() {
			var self = this;
			this.facilityCode = '${cache.getCache('facilityCache').getCurrentFacility().getCode()}';
			this.itemCode;
			
			this.init = function() {
				$('#itemCode').keyup(self.load);
			};
			
			this.load = function(event){
				if (event.which == 13 && $('#itemCode').val() != '') {
					$('#itemDiv').html("");
					self.searchItem($('#itemCode').val());
				}
			};
			
			this.searchItem = function(itemCode) {
				var req = {
					'itemCode': itemCode
				};
				Uniware.Ajax.postJson("/data/item/fetch", JSON.stringify(req), function(response) {
					if(response.successful == true) {
						self.itemCode = response.itemDTO.code;
						$('#itemDiv').html(template("itemTemplate", response.itemDTO));
						Uniware.Utils.barcode();
						Uniware.Utils.applyHover();
						$('#rejectItem').click(self.rejectItem);
						$('#acceptItem').click(self.acceptItem);
                        if (Uniware.Utils.reprintLabel) {
                            $('#reprintLabel').removeClass('hidden');
                        }
						$('#reprintLabel').click(function(){
							Uniware.Utils.printIFrame('/jabong/inflow/items/print/labels/'+ self.itemCode + '?legacy=1');
						});
						Uniware.Utils.renderComments("#itemComments", 'ITEM-' + itemCode + '-' + self.facilityCode);
						if(response.itemDTO.inflowReceiptCode) {
							Uniware.Documents("#grnDocuments", 'IR-' + response.itemDTO.inflowReceiptCode , '${user.username}', 2);
						} else {
							$("#grnDocuments").html('');
						}
                        if (Uniware.Utils.isMyntra) {
                            $(document).on("keyup", function(e) {
                                var charCode = e.which || e.keyCode;
                                if (charCode == 13 && e.target.value !== "") {
                                    $('#acceptItem').click();
                                }
                            });
                        }
					} else {
						Uniware.Utils.showError(response.errors[0].description);
					}
					$('#itemCode').val('').focus();
				}); 	
			};
			
			this.acceptItem = function() {
				var req = {
					itemCode: self.itemCode
 				}
                if (!Uniware.Utils.isMyntra) {
                    req.acceptReason = '50-50 - ' + $('#qcPass50').is(':checked');
                }
				Uniware.Ajax.postJson("/data/jabong/item/accept", JSON.stringify(req), function(response) {
					if(response.successful == true){
						Uniware.Utils.addNotification("Item has been accepted");
						if(response.warnings!=null && response.warnings.length > 0){
							var msg=response.warnings[0].message;
						}
						else{
							var msg='';
							
						}
						if (response.numberOfItems == 1) {
							var req = {
								'saleOrderCode' : response.saleOrderCode,
								'saleOrderItemCodes' : [response.saleOrderItemCode],
                                'doNotCreateInvoice' : true
							}
							Uniware.Ajax.postJson("/data/jabong/shipment/create", JSON.stringify(req), function(response) {
								if (response.successful == false) {
									Uniware.Utils.showError(response.errors[0].description);
								} else {
									self.itemCodes = {};
									Uniware.Utils.addNotification('Shipment has been created.');
									Uniware.Utils.printIFrame("/oms/shipment/show/" + response.shipmentCode + '?legacy=1');
								}
							}, true);
							
						} else {
							if (Uniware.Utils.isMyntra && msg=='No Sale Order pending for this item please putaway to default Shelf - Standard') {
									var req = {
											itemCode: self.itemCode,
											rejectionReason : ''
						 				}
										Uniware.Ajax.postJson("/data/admin/system/goodInventoryItemCode/damage", JSON.stringify(req), function(response) {
											if(response.successful == true){
												Uniware.Utils.addNotification("Item has been marked as Bad Inventory");
											}else{
												Uniware.Utils.showError(response.errors[0].description);
											}
										}, true);
							}
							
							Uniware.Utils.printIFrame('/jabong/inflow/items/print/labels/'+ self.itemCode + '?legacy=1');
							self.searchItem(self.itemCode);
						}
						
					}else{
						Uniware.Utils.showError(response.errors[0].description);
					}
				}, true);
			
			};
			
			this.rejectItem = function() {
				var req = {
					itemCode: self.itemCode,
					comments: $('#rejectionComment').val()
 				}
				Uniware.Ajax.postJson("/data/jabong/item/reject", JSON.stringify(req), function(response) {
					if(response.successful == true){
						Uniware.Utils.addNotification("Item has been discarded");
						self.searchItem(self.itemCode);
					}else{
						Uniware.Utils.showError(response.errors[0].description);
					}
				}, true);
			
			};
		}
		
		$(document).ready(function() {
			window.page = new Uniware.SearchItemPage();
			window.page.init();
			if ($("#itemCode").val() != '') {
				$('#itemCode').trigger(Uniware.Event.ENTER_KEYUP);
			}
		});
	</script>
	</tiles:putAttribute>

</tiles:insertDefinition>
