<%@ include file="/tagIncludes.jsp"%>
<tiles:insertDefinition name=".inflowPage">
	<tiles:putAttribute name="title" value="Uniware - Create Labels for Item Type" />
	<tiles:putAttribute name="rightPane">
		<div class="noprint greybor round_all">
			<form onsubmit="javascript : return false;">
				<div class="greybor headlable ovrhid main-box-head">
					<h2 class="edithead head-textfields">Search Item Type</h2>
					<div class="lfloat">
						<input type="text" id="itemType" size="20"  autocomplete="off" style="width:400px">
					</div>
					<div id="searching" class="lfloat10 hidden" style="margin-top:5px;">
						<img src="/img/icons/refresh-animated.gif"/>
					</div>
				</div>
				<div class="clear"></div>
				<div id="itemTypeDiv" style="margin:20px;"></div>
			</form>
		</div>
		
	</tiles:putAttribute>
	<tiles:putAttribute name="deferredScript">
		<script id="itemTypeDivTemplate" type="text/html">
		<table width="100%" border="0" cellspacing="1" cellpadding="3" class="fields-table f15">
			<tr> 
        		<td>Item SKUCode</td>
        		<td width="70%"><#=obj.skuCode#></td>
			</tr>
			<tr> 
        		<td>ItemType Name</td>
        		<td><#=obj.name#>&#160;&#160;<#=Uniware.Utils.getItemDetail(obj.itemTypeImageUrl, obj.itemTypePageUrl)#></td>   
			</tr>
			<tr> 
        		<td>Category</td>
        		<td><#=obj.categoryName#></td>
			</tr>
			<tr> 
        		<td>Vendor</td>
        		<td>
					<select id="vendorSkuCode">
					<# for(var i=0; i<obj.vendorItemTypes.length;i++){ var vendorItemType = obj.vendorItemTypes[i]; #>
						<option value="vendor-<#=i#>"><#=vendorItemType.vendorSkuCode #> # <#=vendorItemType.vendorName#></option>
					<# } #>
				</select>
				</td>
			</tr>
			<# if (Uniware.Utils.barcodePreGenerated) { #>
			<tr> 
        		<td>Scan</td>
        		<td><input id="scanItem" type="text" value=""/></td>
			</tr>
			<tr> 
        		<td></td>
        		<td><div id="addLabels" class=" btn btn-primary lfloat">Add Labels</div></td>
			</tr>
				<# if (obj.addedItems.length > 0) { #>
				<tr> 
        		<td>Added Labels</td>
        		<td>
					<# for (var i=0;i<obj.addedItems.length;i++) { #>
					<#=obj.addedItems[i]#><br/>
					<# } #>
				</td>
				</tr>
				<# } #>
			<# } else { #>
			<tr> 
        		<td>Quantity</td>
        		<td><input id="quantity" type="text" value=""/></td>
			</tr>
			<# if (obj.expirable) { #>
			<tr>
				<td>Manufacturing Date</td>
				<datepicker type="grid" value="2007-03-26"/>
				<td><input id="manufacturingDate" type="date" value=""/></td>
			</tr>
			<# } #>
			<tr> 
        		<td></td>
        		<td><div id="createLabels" class=" btn btn-primary lfloat">Create Labels</div></td>
			</tr>
			<# } #>
		</table>
		</script>
		<script type="text/javascript">
			Uniware.Utils.barcodePreGenerated = ${configuration.getConfiguration('systemConfiguration').barcodePreGenerated};
			Uniware.CreateLabelItemTypePage = function() {
				var self = this;	
				this.itemTypeSkuCode;
				this.itemTypeId;
				this.itemTypeDTO = null;
				
				this.init = function() {
					$("#itemType").autocomplete({
				    	minLength: 4,	
				    	mustMatch : true,
						autoFocus: true,
						source: function( request, response ) {
							$('#itemTypeDiv').html('');
							Uniware.Ajax.getJson("/data/lookup/itemTypes?keyword=" + request.term, function(data) {
								response( $.map( data, function( item ) {
									return {
										label: item.name + " - " + item.itemSKU,
										value: item.name,
										itemType: item
									}
								}));
							});
						},
						select: function( event, ui ) {
							self.itemTypeSkuCode = ui.item.itemType.itemSKU;
							self.itemTypeId = ui.item.itemType.itemTypeId;
							self.getItemTypeDetails();
						}
				    });
				};
				
				this.getItemTypeDetails = function() {
					var req = {
						'itemSKU' : self.itemTypeSkuCode	
					};
					Uniware.Ajax.postJson("/data/reports/search/itemType/fetch", JSON.stringify(req), self.onItemTypeFetch);
				};
				
				this.onItemTypeFetch = function(response) {
					if (response && response.successful) {
						self.itemTypeDTO = response.itemTypeDTO;
						self.itemTypeDTO.addedItems = [];
						$("#itemType").val('');
						$('#itemTypeDiv').html(template("itemTypeDivTemplate", self.itemTypeDTO));
						$('#quantity').focus();
						$('#createLabels').click(self.createLabels);
						$('#scanItem').focus().keyup(self.scanItemToAdd);
					} else {
						Uniware.Utils.showError(response.errors[0].description);
					}
				};
				
				this.scanItemToAdd = function(event) {
					if (event.which == 13 && $(this).val() != '') {
						var newItems = $(this).val().split(',');
						for (var i=0;i<newItems.length;i++) {
							if ($.inArray(newItems[i], self.itemTypeDTO.addedItems) != -1) {
								alert('Item Already Added ' + newItems[i]);
								$(this).val('');
								return;
							}
						}
						self.itemTypeDTO.addedItems = self.itemTypeDTO.addedItems.concat(newItems);
						$('#itemTypeDiv').html(template("itemTypeDivTemplate", self.itemTypeDTO));
						$('#scanItem').focus().keyup(self.scanItemToAdd);
						$('#addLabels').click(function() {
							var req = {
								'itemSkuCode' : self.itemTypeDTO.skuCode,
								'itemCodes' : self.itemTypeDTO.addedItems
							};
							Uniware.Ajax.postJson("/data/inflow/item/addLabels", JSON.stringify(req), function(response) {
								if (response.successful) {
									$('#itemTypeDiv').html('');
									$("#itemType").focus();
									Uniware.Utils.addNotification(self.itemTypeDTO.addedItems.length + ' Labels have been added');
									self.itemTypeDTO = null;
								} else {
									Uniware.Utils.showError(response.errors[0].description);
								}
							});
						});
					}
				};
				
				this.createLabels = function() {
					var req = {
						'itemSkuCode' : self.itemTypeDTO.skuCode,
						'quantity' : $('#quantity').val(),
						'vendorSkuCode': $('#vendorSkuCode').val(),
						'manufacturingDate' : $('#manufacturingDate').val()

                };
					if($('#vendorSkuCode').val() != null) {
						var index = parseInt($('#vendorSkuCode').val().split('-')[1]);
						var vendorItemType = self.itemTypeDTO.vendorItemTypes[index];
						req.vendorSkuCode = vendorItemType.vendorSkuCode;
						req.vendorId = vendorItemType.vendorId;
					}
					Uniware.Ajax.postJson("/data/inflow/item/createLabels", JSON.stringify(req), function(response) {
						if (response.successful) {
							$('#itemTypeDiv').html('');
							$("#itemType").focus();
							Uniware.Utils.addNotification(response.itemCodes.length + ' Labels have been created');
							Uniware.Utils.printIFrame('/inflow/items/print/labels/'+ response.itemCodes.join(','));
						} else {
							Uniware.Utils.showError(response.errors[0].description);
						}
					});
				};
				
			}
			
			$(document).ready(function() {
				window.page = new Uniware.CreateLabelItemTypePage();
				window.page.init();
			});
		</script>
		</tiles:putAttribute>
</tiles:insertDefinition>
