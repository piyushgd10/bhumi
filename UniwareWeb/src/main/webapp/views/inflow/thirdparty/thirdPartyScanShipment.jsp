<%@ include file="/tagIncludes.jsp"%>
<tiles:insertDefinition name=".shippingPage">
<tiles:putAttribute name="title" value="Uniware - Shipment Details" />
<tiles:putAttribute name="rightPane">
    <div id="traceableItems" class="lb-over">
        <div class="lb-over-inner round_all" style="width: 600px">
            <div style="margin: 40px;">
                <div class="pageHeading lfloat">Scan Barcodes</div>
                <div id="traceableItems_close" class="link rfloat">close</div>
                <br />
                <div class="clear"></div>
                <div id="traceableItemList"></div>
                <div class="clear"></div>
                <br /> <input type="text" class="textfield" id="traceableInput" />
                <div id="error" class="invisible errorField"></div>
            </div>
        </div>
    </div>
    <div id="qcDamageItems" class="lb-over">
        <div class="lb-over-inner round_all" style="width: 600px">
            <div style="margin: 40px;">
                <div class="pageHeading lfloat">Items</div>
                <div id="qcDamageItems_close" class="link rfloat">close</div>
                <br />
                <div class="clear"></div>
                <div id="qcDamageItemList"></div>
                <div class="clear"></div>
                <div id="error" class="invisible errorField"></div>
            </div>
        </div>
    </div>
    <div>
        <form onsubmit="javascript : return false;">
            <div class="greybor headlable ovrhid main-box-head">
                <h2 class="edithead head-textfields">Shipment</h2>
                <div class="lfloat">
                    <input type="text" id="shipmentCode" name="shipmentCode" size="30" autocomplete="off" placeholder="scan/type shipment code" value="${param['shipmentCode']}">
                </div>
                <div id="searching" class="lfloat hidden" style="margin:5px 10px 0;">
                    <img src="/img/icons/refresh-animated.gif" />
                </div>
                <h2 class="edithead head-textfields">Scan Item</h2>
                <div class="lfloat">
                    <input type="text" id="scanItemBarcode" size="30" autocomplete="off" placeholder="scan/type item code" value="">
                </div>
                <h2 class="edithead head-textfields">Scan Order</h2>
                <div class="lfloat">
                    <input type="text" id="scanGlobalOrderSearch" size="30" autocomplete="off" placeholder="scan/type item code" value="">
                </div>
            </div>
        </form>
    </div>
    <div id="allPackages" class="hidden" style="margin-top:10px;"></div>
    <div id="messageDiv" style="margin:10px;font-size:18px;"></div>
    <form onsubmit="javascript:return false;">
        <div id="spInfoDiv"></div>
    </form>
    <div id="orderItems" class="hidden" style="margin-top:10px;">
    </div>

    <div id="shipmentComments" class="round_all"></div>
    <div id="statusUpdateDiv" class="lb-over">
        <div class="lb-over-inner round_all">
            <div style="margin: 40px;line-height: 30px;">
                <div id="statusUpdateTemplateDiv"></div>
            </div>
        </div>
    </div>
</tiles:putAttribute>
<tiles:putAttribute name="deferredScript">
<script type="text/html" id="qcHoldItemListTemplate">
    <table style="width:100%;font-size:14px;line-height:24px" cellpadding="5">
        <tr class="bold">
            <td width="30%">Item Code</td>
            <td id="qcDamageItemCodeSubmit"><#=obj.itemCode#></td>
        </tr>
        <tr>
            <td width="30%">Hold Reason</td>
            <td>
                <select id="qcRejectionReason">
					<option>CUT LENS-BROKEN WHILE FITTING</option>
<option>CUT LENS-BUBBLES FITTED</option>
<option>CUT LENS-CHIPPED WHILE FITTING</option>
<option>CUT LENS-LENS BROKEN</option>
<option>CUT LENS-LENS LOST WHILE FITTING</option>
<option>CUT LENS-LENS SCRATCHED</option>
<option>CUT LENS-POWER INTERCHANGED WHILE FITTING</option>
<option>CUT LENS-REMOVED STAMPING WHILE FITTING</option>
<option>CUT LENS-SMALL SIZE CUT WHILE FITTING</option>
<option>CUT LENS-VIBRATION FITTED</option>
<option>CUT LENS-WITHOUT FOG MARK FITTED</option>
<option>CUT LENS-WRONG AXIS FITTED</option>
<option>CUT LENS-WRONG BRAND FITTED</option>
<option>CUT LENS-WRONG BRAND RECEIVED BUT FITTED</option>
<option>CUT LENS-WRONG INDEX FITTED</option>
<option>CUT LENS-WRONG INDEX RECEIVED BUT FITTED</option>
<option>CUT LENS-WRONG P D FITTED</option>
<option>CUT LENS-WRONG POWER FITTED</option>
<option>CUT LENS-WRONG SHAPE CUT WHILE FITTING</option>
<option>PFU-UNDER FOLLOW UP FOR FRAME</option>
<option>PFU-UNDER FOLLOW UP FOR HEIGHT</option>
<option>PFU-UNDER FOLLOW UP FOR POWER</option>
<option>PFU-UNDER FOLLOW UP FOR WIDTH</option>
<option>Polorized lens color issue</option>
<option>UNCUT LENS-BUBBLES RECEIVED</option>
<option>UNCUT LENS-CRACKED LENS RECEIVED</option>
<option>UNCUT LENS-LENS NOT RECEIVED</option>
<option>UNCUT LENS-POWER INTERCHANGED RECEIVED</option>
<option>UNCUT LENS-SCRATCHED RECEIVED</option>
<option>UNCUT LENS-SMALL DIAMETER RECEIVED</option>
<option>UNCUT LENS-VIBRATION RECEIVED</option>
<option>UNCUT LENS-WITHOUT FOG MARK RECEIVED</option>
<option>UNCUT LENS-WITHOUT STAMPING RECEIVED</option>
<option>UNCUT LENS-WRONG AXIS RECEIVED</option>
<option>UNCUT LENS-WRONG FOG MARK RECEIVED</option>
<option>UNCUT LENS-WRONG POWER RECEIVED</option>
<option>Power Updated</option>
<option>CUT LENS FRAME-FITTED WITHOUT NOSE PADS</option>
<option>CUT LENS FRAME-FRAME LOST WHILE FITTING</option>
<option>CUT LENS FRAME-WRONG FRAME FITTED</option>
<option>CUT LENS Tracing wrong</option>
<option>CUT LENS WRONG POWER FITTED</option>
<option>CUT LENS Loose Fitting</option>
<option>CUT LENS FRAME-WRONG FRAME RECEIVED</option>
<option>UNCUT LENS LENS FRAME-WRONG FRAME RECEIVED</option>
<option>CUT LENS COATING-COATING NOT MATCHING</option>
<option>UNCUT LENS LENS COATING-COATING NOT MATCHING</option>
<option>CUT LENS COATING-RECEIVED WITHOUT COATING</option>
<option>UNCUT LENS LENS COATING-RECEIVED WITHOUT COATING</option>
<option>CUT LENS FRAME-BROKEN FRAME RECEIVED</option>
<option>UNCUT LENS LENS FRAME-BROKEN FRAME RECEIVED</option>
<option>CUT LENS FRAME-DAMAGED FRAME RECEIVED</option>
<option>UNCUT LENS LENS FRAME-DAMAGED FRAME RECEIVED</option>
<option>Expiry Date</option>
<option>Coating Damage</option>
<option>Wrong Drilling</option>

					
                </select>
            </td>
        </tr>
        <tr>
            <td width="30%">Shelf Code</td>
            <td>
                <select id="qcShelfCode">
                    <option>D-201</option>
                    <option>D-202</option>
                    <option>D-203</option>
                    <option>D-204</option>
                    <option>D-205</option>
                    <option>D-206</option>
                    <option>D-207</option>
                    <option>D-208</option>
                    <option>D-209</option>
                    <option>D-210</option>
                    <option>D-211</option>
                    <option>D-212</option>
                    <option>F-301- uncut lens qc fail</option>
                    <option>F-302- cut qc fail</option>
                    <option>F-303-frame defective</option>
                    <option>F-304-onc/pdr</option>
                    <option>F-305 defective frame for refurb</option>
                    <option>R1039A</option>
                    <option>R1039B</option>
                    <option>R1039C</option>
                    <option>R1039D</option>
                    <option>R1039E</option>
                    <option>R1039F</option>
                    <option>R1142A</option>
                    <option>R1142B</option>
                    <option>R1142C</option>
                    <option>R1142D</option>
                    <option>R1142E</option>
                    <option>R1142F</option>
                    <option>E-101 - Unfulfilable QC Fail</option>
                    <option>E-102 - On Follow up sheet</option>
                    <option>E-103 - Other Reason/Tech Issue</option>
                    <option>E-104</option>
                </select>
            </td>
        </tr>
        <tr>
            <td width="30%"</td>
            <td><div id="qcHoldSubmit" class="btn btn-small btn-grey">Submit</div></td>
        </tr>

    </table>
</script>
<script type="text/html" id="qcDamageItemListTemplate">
    <table style="width:100%;font-size:14px;line-height:24px" cellpadding="5">
        <tr class="bold">
            <td width="30%">Item Code</td>
            <td id="qcDamageItemCodeSubmit"><#=obj.itemCode#></td>
        </tr>
        <tr>
            <td width="30%">Sale Order Item Code</td>
            <td id="qcDamageSaleOrderItemCodeSubmit"><#=obj.saleOrderItemCode#></td>
        </tr>
        <tr>
            <td width="30%">Rejection Reason</td>
            <td>
                <select id="qcRejectionReason">
                    <option>CUT LENS-BROKEN WHILE FITTING</option>
<option>CUT LENS-BUBBLES FITTED</option>
<option>CUT LENS-CHIPPED WHILE FITTING</option>
<option>CUT LENS-LENS BROKEN</option>
<option>CUT LENS-LENS LOST WHILE FITTING</option>
<option>CUT LENS-LENS SCRATCHED</option>
<option>CUT LENS-POWER INTERCHANGED WHILE FITTING</option>
<option>CUT LENS-REMOVED STAMPING WHILE FITTING</option>
<option>CUT LENS-SMALL SIZE CUT WHILE FITTING</option>
<option>CUT LENS-VIBRATION FITTED</option>
<option>CUT LENS-WITHOUT FOG MARK FITTED</option>
<option>CUT LENS-WRONG AXIS FITTED</option>
<option>CUT LENS-WRONG BRAND FITTED</option>
<option>CUT LENS-WRONG BRAND RECEIVED BUT FITTED</option>
<option>CUT LENS-WRONG INDEX FITTED</option>
<option>CUT LENS-WRONG INDEX RECEIVED BUT FITTED</option>
<option>CUT LENS-WRONG P D FITTED</option>
<option>CUT LENS-WRONG POWER FITTED</option>
<option>CUT LENS-WRONG SHAPE CUT WHILE FITTING</option>
<option>PFU-UNDER FOLLOW UP FOR FRAME</option>
<option>PFU-UNDER FOLLOW UP FOR HEIGHT</option>
<option>PFU-UNDER FOLLOW UP FOR POWER</option>
<option>PFU-UNDER FOLLOW UP FOR WIDTH</option>
<option>Polorized lens color issue</option>
<option>UNCUT LENS-BUBBLES RECEIVED</option>
<option>UNCUT LENS-CRACKED LENS RECEIVED</option>
<option>UNCUT LENS-LENS NOT RECEIVED</option>
<option>UNCUT LENS-POWER INTERCHANGED RECEIVED</option>
<option>UNCUT LENS-SCRATCHED RECEIVED</option>
<option>UNCUT LENS-SMALL DIAMETER RECEIVED</option>
<option>UNCUT LENS-VIBRATION RECEIVED</option>
<option>UNCUT LENS-WITHOUT FOG MARK RECEIVED</option>
<option>UNCUT LENS-WITHOUT STAMPING RECEIVED</option>
<option>UNCUT LENS-WRONG AXIS RECEIVED</option>
<option>UNCUT LENS-WRONG FOG MARK RECEIVED</option>
<option>UNCUT LENS-WRONG POWER RECEIVED</option>
<option>Power Updated</option>
<option>CUT LENS FRAME-FITTED WITHOUT NOSE PADS</option>
<option>CUT LENS FRAME-FRAME LOST WHILE FITTING</option>
<option>CUT LENS FRAME-WRONG FRAME FITTED</option>
<option>CUT LENS Tracing wrong</option>
<option>CUT LENS WRONG POWER FITTED</option>
<option>CUT LENS Loose Fitting</option>
<option>CUT LENS FRAME-WRONG FRAME RECEIVED</option>
<option>UNCUT LENS LENS FRAME-WRONG FRAME RECEIVED</option>
<option>CUT LENS COATING-COATING NOT MATCHING</option>
<option>UNCUT LENS LENS COATING-COATING NOT MATCHING</option>
<option>CUT LENS COATING-RECEIVED WITHOUT COATING</option>
<option>UNCUT LENS LENS COATING-RECEIVED WITHOUT COATING</option>
<option>CUT LENS FRAME-BROKEN FRAME RECEIVED</option>
<option>UNCUT LENS LENS FRAME-BROKEN FRAME RECEIVED</option>
<option>CUT LENS FRAME-DAMAGED FRAME RECEIVED</option>
<option>UNCUT LENS LENS FRAME-DAMAGED FRAME RECEIVED</option>
<option>Expiry Date</option>
<option>Coating Damage</option>
<option>Wrong Drilling</option>



                </select>
            </td>
        </tr>
        <tr>
            <td width="30%">Shelf Code</td>
            <td>
                <select id="qcShelfCode">
                    <option>D-201</option>
                    <option>D-202</option>
                    <option>D-203</option>
                    <option>D-204</option>
                    <option>D-205</option>
                    <option>D-206</option>
                    <option>D-207</option>
                    <option>D-208</option>
                    <option>D-209</option>
                    <option>D-210</option>
                    <option>D-211</option>
                    <option>D-212</option>
                    <option>F-301- uncut lens qc fail</option>
                    <option>F-302- cut qc fail</option>
                    <option>F-303-frame defective</option>
                    <option>F-304-onc/pdr</option>
                    <option>F-305 defective frame for refurb</option>
                    <option>R1039A</option>
                    <option>R1039B</option>
                    <option>R1039C</option>
                    <option>R1039D</option>
                    <option>R1039E</option>
                    <option>R1039F</option>
                    <option>R1142A</option>
                    <option>R1142B</option>
                    <option>R1142C</option>
                    <option>R1142D</option>
                    <option>R1142E</option>
                    <option>R1142F</option>
                    <option>E-101 - Unfulfilable QC Fail</option>
                    <option>E-102 - On Follow up sheet</option>
                    <option>E-103 - Other Reason/Tech Issue</option>
                    <option>E-104</option>
                </select>
            </td>
        </tr>
        <tr>
            <td width="30%"</td>
            <td><div id="qcDamageSubmit" class="btn btn-small btn-grey">Submit</div></td>
        </tr>

    </table>
</script>
<script type="text/html" id="traceableItemsTemplate">
    <table style="width:100%;font-size:12px;line-height:24px">
        <tr class="bold"><td width="30%">SKU</td><td>Name</td><td>Item Barcode</td></tr>
        <# for(var j = 0; j < obj.shippingPackageItems.length; j++) { var traceableItem = obj.shippingPackageItems[j];#>
        <tr>
            <td><#=traceableItem.itemSku#></td>
            <td><#=traceableItem.itemName#></td>
            <td><#=traceableItem.itemCode#></td>
        </tr>
        <# } #>
    </table>
    <div id="scannedItems"><#= obj.itemCodes ? 'Items scanned : ' + obj.itemCodes.join(',') : '' #></div>
</script>
<script id="lenskartWorkOrderTemplate" type="text/html">
    <div class="greybor round_bottom main-boform-cont pad-15-top">
        <table width="100%" border="0" cellspacing="1" cellpadding="3" class="fields-table">
            <tr>
                <td width="20%">Package Code</td>
                <td id="headerTd" colspan="3">
                    <div id="shippingPackageCode" class="barcode lfloat" barWidth="1" barHeight="15"><#=obj.code#></div>
                </td>
            </tr>
            <# if (obj.orderNumber.indexOf('-') != -1) { var index = parseInt(obj.orderNumber.split('-')[1]); if (index > 2) { #>
                <tr>
                    <td colspan="4"><span style="font-size:30px;font-weight:bold;color:red;">Check the Multiple Orders first on this <a href="http://vsm.lenskart.com/orderCheck" target="_blank">VSM LINK</a></span></td>
                </tr>
                <# }} #>
                    <tr>
                        <td>Order</td>
                        <td>
                            <a target="_top" href="/order/orderitems?orderCode=<#=obj.orderNumber#>"><#=obj.orderNumber#></a> -
			<span style="font-weight:bold;">
				<#=obj.orderStatus#>
			</span>
                        </td>
                        <td width="20%">Created On</td>
                        <td><#=(new Date(obj.created)).toDateTime()#></td>
                    </tr>
                    <tr>
                        <td>Status</td>
                        <td><#=obj.statusCode#></td>
                        <td>Number of Items</td>
                        <td><#= obj.noOfItems #></td>
                    </tr>
                    <tr>
                        <td>All Picklists</td>
                        <td id="otherPicklists"></td>
                        <td>Picklist Number</td>
                        <td>
                            <# if (obj.picklistNumber) { #>
                                <a target="_top" href="/picklists/view?picklistCode=<#=obj.picklistNumber#>"><#=obj.picklistNumber#></a>
                                <# } #>
                        </td>
                    </tr>
                    <tr>
                        <td>Unfulfillable Add Inventory</td>
                        <td id="addInventory"></td>
                        <td>Mark Critical</td>
                        <td><a href="http://vsm.lenskart.com/priorityUnicomOrder/<#=obj.orderNumber#>" target="_blank">Mark Critical</a></td>
                    </tr>
                    <tr>
                        <td>Unfulfillable Start Retire</td>
                        <td id="startRetire"></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <# if (obj.onHold) { #>
                        <tr>
                            <td colspan="4">
                                <span style="font-size:30px;color:red;">This order is currently on Hold. Please wait.</span>
                            </td>
                        </tr>
                        <# } #>
                            <tr><td>Customer Comments</td><td id="customerComments"></td><td>Customer Service Comments</td><td id="customerServiceComments"></td></tr>
                            <tr><td>Special Order</td><td id="specialOrder"></td><td>Gift Wrap</td><td id="isGiftWrap"></td></tr>
                            <tr><td>Gift Message</td><td id="giftMessage"></td><td>Personalization Info</td><td id="personalizationInfo"></td></tr>

                            <# if (Uniware.Utils.isLenskartFitting && obj.requiresCustomization && obj.customFieldValues && obj.customFieldValues.length > 0) { #>
                                <tr>
                                    <# count = 0; #>
                                        <# for(var i=0;i<obj.customFieldValues.length;i++) { var customField = obj.customFieldValues[i]; if (customField.fieldName.indexOf('fitting') != -1) { #>
                                        <td><#=customField.displayName#></td>
                                        <td>
                                            <# if (customField.valueType == 'date') { #>
                                                <input id="<#=customField.fieldName#>" type="text" size="15" class="custom datefield" value="<#= customField.fieldValue ? new Date(customField.fieldValue).toPaddedDate() : '' #>"/>
                                                <# } else if (customField.valueType == 'checkbox') { #>
                                                    <input id="<#=customField.fieldName#>" type="checkbox" class="custom" <#=customField.fieldValue ? checked="checked" : '' #>/>
                                                    <# } else if (customField.valueType == 'text') { #>
                                                        <input id="<#=customField.fieldName#>" type="text" size="15" class="custom" value="<#=customField.fieldValue#>"/>
                                                        <# } else if (customField.valueType == 'select') { #>
                                                            <select id="<#=customField.fieldName#>" class="w150 custom">
                                                                <# for (var j=0;j<customField.possibleValues.length;j++) { #>
                                                                <option value="<#=customField.possibleValues[j]#>" <#= customField.possibleValues[j] == customField.fieldValue ? selected="selected" : '' #> ><#=customField.possibleValues[j]#></option>
                                                                <# } #>
                                                            </select>
                                                            <# } #>
                                        </td>
                                        <# if (i == obj.customFieldValues.length- 1 && count % 2 == 0) { #>
                                            <td></td><td></td>
                                            <# } else if (count != 0 && count % 2 != 0) { #>
                                </tr><tr>
                                    <# } count++; #>
                                        <# }} #>
                                </tr>
                                <# } #>
                                    <# if(Uniware.Utils.isLenskartQc && (obj.statusCode == 'PENDING_CUSTOMIZATION' || obj.statusCode == 'CUSTOMIZATION_COMPLETE' || obj.statusCode == 'PICKED')) { #>
                                        <# if (obj.customFieldValues && obj.customFieldValues.length > 0) { #>
                                            <tr>
                                                <# count = 0; #>
                                                    <# for(var i=0;i<obj.customFieldValues.length;i++) { var customField = obj.customFieldValues[i]; if (customField.fieldName.indexOf('fitting') == -1) { #>
                                                    <td><#=customField.displayName#></td>
                                                    <td>
                                                        <# if (customField.valueType == 'date') { #>
                                                            <input id="<#=customField.fieldName#>" type="text" size="15" class="custom datefield" value="<#= customField.fieldValue ? new Date(customField.fieldValue).toPaddedDate() : '' #>"/>
                                                            <# } else if (customField.valueType == 'checkbox') { #>
                                                                <input id="<#=customField.fieldName#>" type="checkbox" class="custom" <#=customField.fieldValue ? checked="checked" : '' #>/>
                                                                <# } else if (customField.valueType == 'text') { #>
                                                                    <input id="<#=customField.fieldName#>" type="text" size="15" class="custom" value="<#=customField.fieldValue#>"/>
                                                                    <# } else if (customField.valueType == 'select') { #>
                                                                        <select id="<#=customField.fieldName#>" class="w150 custom">
                                                                            <# for (var j=0;j<customField.possibleValues.length;j++) { #>
                                                                            <option value="<#=customField.possibleValues[j]#>" <#= customField.possibleValues[j] == customField.fieldValue ? selected="selected" : '' #> ><#=customField.possibleValues[j]#></option>
                                                                            <# } #>
                                                                        </select>
                                                                        <# } #>
                                                    </td>
                                                    <# if (i == obj.customFieldValues.length- 1 && count % 2 == 0) { #>
                                                        <td></td><td></td>
                                                        <# } else if (count != 0 && count % 2 != 0) { #>
                                            </tr><tr>
                                                <# } count++; #>
                                                    <# }} #>
                                            </tr>
                                            <# } #>
                                                <# } #>
                                                    <tr>
                                                        <td id="shippingAddress"></td>
                                                        <td id="shippingAddressValue" colspan="3"></td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td colspan="3">
                                                            <br/>
                                                            <div id="lenskartWorkorderFieldsEdit" class="btn btn-small btn-success lfloat">Save</div>
                                                            <div id="markPendingCustomization" class="btn btn-small btn-primary lfloat20 hidden">Mark Pending Customization</div>
                                                            <div id="markCompleteCustomization" class="btn btn-small btn-primary lfloat20 hidden">Mark Workorder Complete</div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td id="shippingAddress"></td>
                                                        <td id="shippingAddressValue" colspan="3"></td>
                                                    </tr>
        </table>
    </div>
</script>
<script id="spInfoTemplate" type="text/html">
<div class="greybor round_bottom main-boform-cont pad-15-top">
<table width="100%" border="0" cellspacing="1" cellpadding="3" class="fields-table">
<tr>
    <td width="20%">Package Code</td>
    <td id="headerTd" colspan="3">
        <div id="shippingPackageCode" class="barcode lfloat" barWidth="1" barHeight="15"><#=obj.code#></div>
        <# if (Uniware.Utils.isLenskartReship && obj.statusCode == 'RETURN_ACKNOWLEDGED') { #>
            <div id="redispatchShipment" class="btn btn-small btn-primary rfloat10">Redispatch Shipment</div>
            <# } #>
                <# if (obj.shippingProviderCode) { #>
                    <uc:security accessResource="SHIPPING_PROVIDER_ALLOCATE">
                        <# var labelHref = "/oms/shipment/show/" + obj.code + '?legacy=1'; #>
                            <a href="<#=obj.shippingLabelLink ? obj.shippingLabelLink + '?legacy=1': labelHref#>" target="_blank">
                                <div class="btn btn-small rfloat10">reprint shipment</div>
                            </a>
                    </uc:security>
                    <# } else if (obj.statusCode == 'PACKED') { #>
                        <div id="printLabel" class="btn btn-small btn-primary rfloat10">print shipment</div>
                        <# } #>

                            <# if(obj.statusCode == 'CUSTOMIZATION_COMPLETE') { #>
                                <div id="printInvoice" class="btn btn-small btn-primary rfloat10">Print Invoice</div>
                                <# } #>

                                    <# if (obj.invoiceCode && !window.page.singleActionInvoiceAndLabel) { #>
                                        <uc:security accessResource="CUSTOMER_INVOICE">
                                            <a href="/oms/invoice/show?invoiceCodes=<#=obj.invoiceCode#>&legacy=1" target="_blank">
                                                <div id="reprintInvoice" class="btn btn-small rfloat20">reprint invoice</div>
                                            </a>
                                        </uc:security>
                                        <# } #>
                                            <# if (!obj.invoiceCode && Uniware.Utils.traceabilityLevel != 'ITEM' && obj.statusCode != 'PENDING_CUSTOMIZATION' && obj.statusCode != 'CUSTOMIZATION_COMPLETE') { #>
                                                <div id="printInvoice" class="btn btn-small btn-primary rfloat20">print invoice</div>
                                                <# } #>
                                                    <# if ((obj.statusCode == "PACKED" || obj.statusCode == "READY_TO_SHIP") && !obj.isEditing ) { #>
                                                        <div id="updateInvoice" class="btn btn-small rfloat20">update invoice</div>
                                                        <div id="editShipment" class="btn btn-small rfloat20">edit <img src="/img/icons/edit.png"/></div>
                                                        <# } #>
                                                            <# if (obj.statusCode == "CREATED" && !obj.isEditing ) { #>
                                                                <div id="editPackageWeight" class="btn btn-small rfloat20">edit <img src="/img/icons/edit.png"/></div>
                                                                <# } #>
                                                                    <# if (!obj.isEditing && !(obj.statusCode == "PACKED" || obj.statusCode == "READY_TO_SHIP" || obj.statusCode == "CREATED") && obj.customFieldValues && obj.customFieldValues.length > 0 && obj.shippingManifestCode != null) { #>
                                                                        <div id="editCustomFieldValues" class="btn btn-small rfloat20">edit <img src="/img/icons/edit.png"/></div>
                                                                        <# } #>
    </td>
</tr>
<tr>
    <td width="20%">AWB Number</td>
    <# if((obj.statusCode == 'DISPATCHED' || obj.statusCode == 'SHIPPED' || obj.statusCode == 'RETURN_EXPECTED') && !obj.isAwbNumberEditing) { #>
        <td width="20%" id="awbNumber"><#=obj.trackingNumber#>
            <# if(Uniware.Utils.isAwbEditable) {#><i class="icon-pencil editAwbNumber" /><# } #></td>
        <# } else if((obj.statusCode == 'DISPATCHED' || obj.statusCode == 'SHIPPED' || obj.statusCode == 'RETURN_EXPECTED') && obj.isAwbNumberEditing){#>
            <td width="20%"><input type="text" id="awbNumber" value="<#=obj.trackingNumber#>"/></td>
            <# } else if(obj.statusCode == 'PACKED' && !obj.isAwbNumberEditingForPackedPackage && obj.shippingProviderCode != null) { #>
                <td width="20%" id="awbNumber"><i class="icon-pencil editPackedPackageAwbNumber" /><#=obj.trackingNumber#></td>
                <# } else if(obj.statusCode == 'PACKED' && obj.isAwbNumberEditingForPackedPackage && obj.shippingProviderCode != null){ #>
                    <td width="20%"><input type="text" id="awbNumber" value="<#=obj.trackingNumber#>"/></td>
                    <# } else { #>
                        <td width="20%" id="awbNumber"><#=obj.trackingNumber#></td>
                        <# } #>

                            <td width="20%">Created On</td>
                            <td><#=(new Date(obj.created)).toDateTime()#></td>
</tr>
<tr>
    <td>Shipping Provider</td>
    <td>
        <# if (obj.providerEditable && obj.isEditing) { #>
            <select id="shippingProvider">
                <option value="">select a provider</option>
                <# for(var providerCode in obj.shippingProviders) {#>
                    <option value="<#=providerCode#>" <#= providerCode==obj.shippingProviderCode ? selected='selected' :''#>><#=obj.shippingProviders[providerCode]#></option>
                    <# } #>
            </select>
            <# }else { #>
                <#=(obj.shippingProviders && obj.shippingProviders[obj.shippingProviderCode]) ? obj.shippingProviders[obj.shippingProviderCode] : obj.shippingProviderCode#>
                    <#  } #>
    </td>
    <td>Order</td>
    <td><a target="_top" href="/order/orderitems?orderCode=<#=obj.orderNumber#>"><#=obj.orderNumber#></a> - <#=obj.orderStatus#></td>
</tr>
<tr>
    <td>Shipping Method</td>
    <td><#=obj.shippingMethods[obj.shippingMethodCode]#></td>
    <td>Reshipment Order</td>
    <td>
        <# if(obj.reshipmentSaleOrderCode != null) { #>
            <a target="_top" href="/order/orderitems?orderCode=<#=obj.reshipmentSaleOrderCode#>"><#=obj.reshipmentSaleOrderCode#></a> - <#=obj.reshipmentSaleOrderStatus#>
            <# } #>
    </td>
</tr>
<tr>
    <td>Shipping Package Type</td>
    <td>
        <# if (obj.isEditing || obj.isWeightEditing) { #>
            <select id="shippingPackageType">
                <# for(var packageTypeCode in obj.shippingPackageTypes) { var packageType = obj.shippingPackageTypes[packageTypeCode]; #>
                    <# if (packageType.enabled) { #>
                        <option value="<#=packageType.code#>" <#= packageType.code==obj.shippingPackageType ? selected='selected' :''#>><#=packageType.code#></option>
                        <# } #>
                            <# } #>
            </select>
            <# } else { #>
                <#=obj.shippingPackageType#>
                    <# } #>
    </td>
    <td>Parent Package</td>
    <td>
        <# if (obj.parentPackageCode) { #>
            <a target="_top" href="/order/orderitems?orderCode=<#=obj.orderNumber#>&shipmentCode=<#=obj.parentPackageCode#>"><#=obj.parentPackageCode#></a>
            <# } #>
    </td>
</tr>
<tr>
    <td>Shipping Package Dimensions(mm)</td>
    <td>
        <# if (obj.isEditing || obj.isWeightEditing) { #>
            <input id="length" type="text" value="<#=obj.length#>" style="width:30px;" />
            x
            <input id="width" type="text" value="<#=obj.width#>" style="width:30px;" />
            x
            <input id="height" type="text" value="<#=obj.height#>" style="width:30px;" /> mm
            <# } else { #>
                <#=obj.length#> x <#=obj.width#> x <#=obj.height#> mm
                    <# } #>
    </td>
    <td>Picklist Number</td>
    <td><#=obj.picklistNumber#></td>
</tr>
<tr>
    <td>Status</td>
    <td><#=obj.statusCode#></td>
    <td>Estimated Weight (kg)</td>
    <td><#=obj.estimatedWeight#></td>
</tr>
<tr>
    <td>Tracking Status</td>
    <td><#=obj.trackingStatus#>
        <# if(obj.statusCode == 'DISPATCHED' || obj.statusCode == 'SHIPPED' || obj.statusCode == 'RETURN_EXPECTED') { #>
            <i id="updateStatus" class="icon-pencil" />
            <# } #>
    </td>
    <td>Actual Weight (kg)</td>
    <td>
        <# if (obj.isEditing || obj.isWeightEditing) { #>
            <input id="actualWeight" type="text" value="<#=obj.actualWeight#>" size="11" autocomplete = "off"/>
            <# } else { #>
                <#=obj.actualWeight#>
                    <# } #>
    </td>
</tr>
<tr>
    <td>Courier Status</td>
    <td><#=obj.courierStatus#></td>
    <td>Reshipment Code</td>
    <td><#=obj.reshipmentCode#></td>
</tr>
<# if (obj.invoiceCode) { #>
    <tr>
        <td>Invoice</td>
        <td><#=obj.invoiceCode#> at <#=new Date(obj.invoiceDate).toDateTime()#></td>
        <td>Invoice Generated by</td>
        <td><#=obj.invoicedByUser#></td>
    </tr>
    <# } #>
        <tr>
            <td>Zone</td>
            <td><#=obj.zone#></td>
            <td>Number of Items</td>
            <td><#= obj.noOfItems #></td>
        </tr>
        <tr>
            <td>Delivery Time</td>
            <td><#=obj.deliveryTime ? new Date(obj.deliveryTime).toDateTime() : '' #></td>
            <td>Dispatch Time</td>
            <td><#=obj.dispatchTime ? new Date(obj.dispatchTime).toDateTime() : '' #></td>
        </tr>
        <tr>
            <td>Shipping Manifest</td>
            <td><a target="_top" href="/manifests/edit?code=<#=obj.shippingManifestCode#>"><#=obj.shippingManifestCode#></a></td>
            <td>Return Manifest</td>
            <td><a target="_top" href="/returns/editReturnManifest?code=<#=obj.returnManifestCode#>"><#=obj.returnManifestCode#></a></td>
        </tr>
        <tr>
            <td>Number of Boxes</td>
            <td>
                <# if (obj.isEditing || obj.isWeightEditing) { #>
                    <input id="noOfBoxes" type="text" value="<#=obj.noOfBoxes#>" size="11" autocomplete = "off"/>
                    <# } else { #>
                        <#=obj.noOfBoxes#>
                            <# } #>
            </td>
            <td>Channel Name</td>
            <td><#=obj.channelName#></td>
        </tr>
        <# if(obj.isEditing || obj.isCustomFieldEditing || obj.isWeightEditing) { #>
            <# if (obj.customFieldValues && obj.customFieldValues.length > 0) { #>
                <tr>
                    <# for(var i=0;i<obj.customFieldValues.length;i++) { var customField = obj.customFieldValues[i]; #>
                    <td><#=customField.displayName#></td>
                    <td>
                        <# if (customField.valueType == 'date') { #>
                            <input id="<#=customField.fieldName#>" type="text" size="15" class="custom datefield" value="<#= customField.fieldValue ? new Date(customField.fieldValue).toPaddedDate() : '' #>"/>
                            <# } else if (customField.valueType == 'checkbox') { #>
                                <input id="<#=customField.fieldName#>" type="checkbox" class="custom" <#=customField.fieldValue ? checked="checked" : '' #>/>
                                <# } else if (customField.valueType == 'text') { #>
                                    <input id="<#=customField.fieldName#>" type="text" size="15" class="custom" value="<#=customField.fieldValue#>"/>
                                    <# } else if (customField.valueType == 'select') { #>
                                        <select id="<#=customField.fieldName#>" class="w150 custom">
                                            <# for (var j=0;j<customField.possibleValues.length;j++) { #>
                                            <option value="<#=customField.possibleValues[j]#>" <#= customField.possibleValues[j] == customField.fieldValue ? selected="selected" : '' #> ><#=customField.possibleValues[j]#></option>
                                            <# } #>
                                        </select>
                                        <# } #>
                    </td>
                    <# if (i == obj.customFieldValues.length- 1 && i % 2 == 0) { #>
                        <td></td><td></td>
                        <# } else if (i != 0 && i % 2 != 0) { #>
                </tr><tr>
                    <# } #>
                        <# } #>
                </tr>
                <# } #>
                    <#} else {#>
                        <# if (obj.customFieldValues && obj.customFieldValues.length > 0) { #>
                            <tr>
                                <# for(var i=0;i<obj.customFieldValues.length;i++) { var customField = obj.customFieldValues[i]; #>
                                <td><#=customField.displayName#></td>
                                <td>
                                    <# if (customField.valueType == 'date') { #>
                                        <#= customField.fieldValue ? new Date(customField.fieldValue).toPaddedDate() : '' #>
                                            <# } else if (customField.valueType == 'checkbox') { #>
                                                <input type="checkbox" disabled="disabled" <#=customField.fieldValue ? checked="checked" : '' #>/>
                                                <# } else { #>
                                                    <#=customField.fieldValue#>
                                                        <# } #>
                                </td>
                                <# if (i == obj.customFieldValues.length- 1 && i % 2 == 0) { #>
                                    <td></td><td></td>
                                    <# } else if (i != 0 && i % 2 != 0) { #>
                            </tr><tr>
                                <# } #>
                                    <# } #>
                            </tr>
                            <# } #>
                                <# } #>
                                    <#if(obj.stateRegulatoryForms.length > 0) { #>
                                        <tr>
                                            <td colspan="4">
                                                <div id="infoDiv" class="alert alert-info lfloat">
                                                    <i class="icon-info-sign lfloat" style="margin-top:1px;"></i><span class="lfloat10">Please click on the applicable form to print: &nbsp</span>
                                                    <# for(var i=0; i < obj.stateRegulatoryForms.length; i++) {#>
						<span class="lfloat10">
							<a href="/oms/shipment/stateRegulatoryForm/print/<#=obj.stateRegulatoryForms[i].code#>/<#=obj.code#>?legacy=1" target="_blank">
                                <#=obj.stateRegulatoryForms[i].name#>
                            </a>
						</span>
                                                    <# } #>
                                                </div>
                                            </td>
                                        </tr>
                                        <# } #>
                                            <# if(obj.statusCode == 'PENDING_CUSTOMIZATION') { #>
                                                <tr>
                                                    <td colspan="4"><br/><div id="markCompleteCustomization" class="btn btn-small btn-primary lfloat">Mark Pending Customization Complete</div></td>
                                                </tr>
                                                <# } #>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td colspan="3">
                                                            <input type="submit" id="updateSP" class=" btn btn-small btn-primary lfloat invisible" value="Update" />
                                                            <div id="cancelUpdate" class="lfloat10 link hidden">Discard Changes</div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td colspan="3">
                                                            <input type="submit" id="updateCustomField" class=" btn btn-small btn-primary lfloat invisible" value="Update" />
                                                            <div id="cancelUpdateFields" class="lfloat10 link hidden">Discard Changes</div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td colspan="3">
                                                            <input type="submit" id="updateAwb" class=" btn btn-small btn-primary lfloat invisible" value="Update AWB" />
                                                            <div id="cancelAwbUpdate" class="lfloat10 link hidden">Discard Changes</div>
                                                        </td>
                                                    </tr>
</table>
</div>
</script>
<script id="lenskartItemsTemplate" type="text/html">
    <div class="greybor headlable round_top ovrhid">
        <h4 class="edithead">Items</h4>
    </div>
    <div class="greybor form-edit-table-cont round_bottom">
        <table class="uniTable" border="0" cellspacing="0" cellpadding="0" style="font-size:13px;">
            <tr>
                <th>Name</th>
                <# if (Uniware.Utils.isLenskartQc && ((obj.statusCode == 'PICKED' && !obj.requiresCustomization) || obj.statusCode == 'CUSTOMIZATION_COMPLETE')) { #>
                    <th>QC Pass</th>
                    <# } #>
                        <th>SKU</th>
                        <th>Image</th>
                        <th>Action</th>
            </tr>
            <tr id="noResultTR" class="<# if(obj.shippingPackageItems.length > 0) { #> hidden <# } #>">
                <td colspan="3"><em>No items in this package</em></td>
            </tr>
            <# for ( var i = 0; i < obj.shippingPackageItems.length; i++) { var shippingPackageItem = obj.shippingPackageItems[i]; #>
            <tr style=<#=shippingPackageItem.statusCode == "CANCELLED" ? "background:red;color:#FFF;" : "" #>>
            <td>
                <# if (shippingPackageItem.itemTypePageUrl) { #>
                    <a href="<#=shippingPackageItem.itemTypePageUrl#>" target="_blank"><#=shippingPackageItem.itemName#></a>
                    <# } else { #>
                        <#=shippingPackageItem.itemName#>
                    <# } #><br/>
                    <span id="lenstype-<#=shippingPackageItem.code#>" style="font-size:14px;font-weight:bold;"></span><br/>
                    <span id="lensHeight-<#=shippingPackageItem.code#>" style="font-size:14px;color:#ff0000;"></span><br/>
                    <span id="lensWidth-<#=shippingPackageItem.code#>" style="font-size:14px;color:#ff0000;"></span><br/>
                    <span id="patientComments-<#=shippingPackageItem.code#>" style="font-size:14px;color:#ff0000;"></span><br/>
            </td>
            <# if (Uniware.Utils.isLenskartQc  && ((obj.statusCode == 'PICKED' && !obj.requiresCustomization) || obj.statusCode == 'CUSTOMIZATION_COMPLETE')) { #>
                <td>
                    <# if (!obj.qcHold && shippingPackageItem.itemCode) { #>
                        <div class="btn btn-small btn-success lfloat10 markQCPass" id="qcPass-<#=shippingPackageItem.itemCode#>">
                            QC Pass
                        </div>
                        <# } #>
                </td>
                <# } #>
                    <td>
                        <#=shippingPackageItem.itemSku#>
                            <# if (shippingPackageItem.itemCode) { #>
                                - <span class="bold f15"><#=shippingPackageItem.itemCode#></span>
                                <# } #>
                    </td>
                    <td><img src="<#=shippingPackageItem.itemTypeImageUrl#>"/></td>
                    <td>
                        <# if ((Uniware.Utils.isLenskartQcInspector || Uniware.Utils.isLenskartFitting) && shippingPackageItem.itemCode && obj.orderStatus != 'CANCELLED') { #>
                            <div class="btn btn-small btn-danger lfloat10 markQCDamage" id="qcDamage-<#=shippingPackageItem.itemCode#>" data-saleOrderItem="<#=shippingPackageItem.code#>">
                                QC Damage
                            </div>
                            <# } #>
                                <# if (!obj.qcHold && (Uniware.Utils.isLenskartQc || Uniware.Utils.isLenskartFitting)) { #>
                                    <div class="btn btn-small btn-warning lfloat10 markQCHold" id="qcHold-<#=shippingPackageItem.itemCode#>">
                                        QC Hold
                                    </div>
                                    <# } #>
                                        <# if (Uniware.Utils.isVsmPicker && !shippingPackageItem.itemCode) { #>
                                            <div class="clear"/>
                                            <div class="vsmLinks">
                                                <a href="http://vsm.lenskart.com/priorityUnicomOrder/<#=obj.orderNumber#>" target="_blank">Mark Critical</a><br/>
                                                <a href="http://vsm.lenskart.com/viewAddInventory/<#=shippingPackageItem.itemSku#>" target="_blank">Add Inventory</a><br/>
                                                <a href="http://vsm.lenskart.com/viewStartRetire/<#=shippingPackageItem.itemSku#>" target="_blank">Start Retire</a><br/>
                                                <# if (obj.picklistNumber) { #>
                                                    <a target="_top" href="/picklists/view?picklistCode=<#=obj.picklistNumber#>" target="_blank">Mark Not Found (<#=obj.picklistNumber#>)</a><br/>
                                                    <# } #>
                                            </div>
                                            <# } #>
                    </td>
                    </tr>
                    <# } #>
        </table>
    </div><br/>
    <div class="clear"/>
</script>
<script id="itemsTemplate" type="text/html">
    <div class="greybor headlable round_top ovrhid">
        <h4 class="edithead">Items</h4>
    </div>
    <div class="greybor form-edit-table-cont round_bottom">
        <table class="uniTable" border="0" cellspacing="0" cellpadding="0" style="font-size:13px;">
            <tr>
                <th>Name</th>
                <th>SKU</th>
                <th>Shelf Code</th>
                <th>Status</th>
                <th>On hold</th>
            </tr>
            <tr id="noResultTR" class="<# if(obj.shippingPackageItems.length > 0) { #> hidden <# } #>">
                <td colspan="3"><em>No items in this package</em></td>
            </tr>
            <# for ( var i = 0; i < obj.shippingPackageItems.length; i++) { var shippingPackageItem = obj.shippingPackageItems[i]; #>
            <tr style=<#=shippingPackageItem.statusCode == "CANCELLED" ? "background:red;color:#FFF;" : "" #>>
            <td><#=shippingPackageItem.itemName#></td>
            <td>
                <#=shippingPackageItem.itemSku#>
                    <# if (shippingPackageItem.itemCode) { #>
                        - <span class="bold f15"><#=shippingPackageItem.itemCode#></span>
                        <# } #>
            </td>
            <td><#=shippingPackageItem.shelfCode#></td>
            <td><#=shippingPackageItem.statusCode#></td>
            <td>
                <# if (shippingPackageItem.onHold) { #>
                    <div class="lfloat"><img src="/img/icons/hold.png" style="width:18px;"/></div>
                    <div class="alert lfloat10">ON HOLD</div>
                    <div class="clear"/>
                    <# } #>
            </td>
            </tr>
            <# } #>
        </table>
    </div><br/>
    <div class="clear"/>
</script>
<script id="splitPackage" type="text/html">
    <div class="greybor headlable round_top ovrhid">
        <h4 class="edithead">Items</h4>
    </div>
    <div class="greybor form-edit-table-cont round_bottom">
        <table class="uniTable" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <th>Name</th>
                <th>Sku</th>
                <th>Code</th>
                <th>Status</th>
                <th>On hold</th>
                <th>Package</th>
            </tr>
            <tr id="noResultTR" class="<# if(obj.shippingPackageItems.length > 0) { #> hidden <# } #>">
                <td colspan="3"><em>No order items</em></td>
            </tr>
            <# for ( var i = 0; i < obj.shippingPackageItems.length; i++) { var shippingPackageItem = obj.shippingPackageItems[i]; #>
            <tr>
                <td><#=shippingPackageItem.itemName#></td>
                <td>
                    <#=shippingPackageItem.itemSku#>
                        <# if (shippingPackageItem.itemCode) { #>
                            - <span class="bold f15"><#=shippingPackageItem.itemCode#></span>
                            <# } #>
                </td>
                <td><#=shippingPackageItem.saleOrderItemCode#></td>
                <td><#=shippingPackageItem.statusCode#></td>
                <td>
                    <# if (shippingPackageItem.onHold) { #>
                        <div class="lfloat"><img src="/img/icons/hold.png" style="width:18px;"/></div>
                        <div class="alert lfloat10">ON HOLD</div>
                        <div class="clear"/>
                        <# } #>
                </td>
                <td>
                    <select class="splitItem">
                        <# for ( var j = 1; j <= obj.shippingPackageItems.length; j++) { #>
                        <option value=<#=j#>><#=j#></option>
                        <#}#>
                    </select>
                </td>
            </tr>
            <# } #>
        </table>
    </div>
    <div class="clear"></div><br/>
    <div id="save" class="btn btn-primary lfloat20" >Save</div>
    <div id="discard" class="link lfloat20" style="margin-top: 5px;">Discard Changes</div>
    <div class="clear">
</script>
<script id="packageSplits" type="text/html">
    <div class="clear"/>
    <div class="pageHeading" style="margin:20px 5px 5px;">This package has been splitted into following packages:</div>
    <table class="uniTable" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <th>Package Code</th>
            <th>Package Status</th>
            <th>Order Number</th>
            <th>Courier</th>
            <th>Tracking Number</th>
        </tr>
        <# for ( var i = 0; i < obj.shippingPackageChildren.length; i++) { var childShippingPackage = obj.shippingPackageChildren[i]; #>
        <tr>
            <td><a target="_top" href="/order/orderitems?orderCode=<#=childShippingPackage.orderNumber#>&shipmentCode=<#=childShippingPackage.code#>"><#=childShippingPackage.code#></a></td>
            <td><#=childShippingPackage.statusCode#></td>
            <td><#=childShippingPackage.orderNumber#></td>
            <td><#=obj.shippingProviders[childShippingPackage.shippingProviderCode]#></td>
            <td><#=childShippingPackage.trackingNumber#></td>
        </tr>
        <# } #>
    </table>
    </div>
</script>
<script id="allPackagesTemplate" type="text/html">
    <div class="greybor headlable round_top ovrhid">
        <h4 class="edithead">All Packages for Sale Order: <#=obj.saleOrderCode#></h4>
    </div>
    <div class="greybor form-edit-table-cont round_bottom">
        <table class="uniTable" border="0" cellspacing="0" cellpadding="0" style="font-size:13px;">
            <tr>
                <th width="30"><input type="checkbox" id="selectAll"/></th>
                <th>Package Code</th>
                <th>Status</th>
                <th>No of Items</th>
            </tr>
            <# var mergeablePackageCount = 0; #>
                <# for ( var i = 0; i < obj.elements.length; i++) { var shippingPackage = obj.elements[i]; #>
                <# if (shippingPackage.status != 'CANCELLED') { #>
                    <tr>
                        <td>
                            <# if (shippingPackage.status == 'PENDING_CUSTOMIZATION') { #>
                                <input type="checkbox" class="packageCode" id="<#=shippingPackage.code#>"/>
                                <#mergeablePackageCount++; #>
                                    <# } #>
                        </td>
                        <td><#=shippingPackage.code#></td>
                        <td><#=shippingPackage.status#></td>
                        <td><#=shippingPackage.noOfItems#></td>
                    </tr>
                    <# }} #>
        </table>
    </div><br/>
    <div id="mergePackages" class="btn btn-primary hidden lfloat20">Merge Packages</div>
    <div class="clear"/>
    <br/><br/>
    <div class="clear"/>
</script>
<script type="text/html" id="statusUpdateTemplate">
    <div class="pageHeading lfloat">Tracking Status Update</div>
    <div id="statusUpdateDiv_close" class="link rfloat">close</div>
    <div class="clear"></div>
    <br />

    <div class="formLeft150 lfloat">Status</div>
    <div class="formRight lfloat">
        <select id="trackingStatus">
            <option value="">---Select----</option>
            <# for(var trackingCode in obj) {#>
                <option id ="<#=trackingCode#>" value="<#=trackingCode#>"><#=obj[trackingCode] #> </option>
                <# } #>
        </select>
    </div>
    <div class="clear"></div>
    <div class="formLeft150 lfloat">Date</div>
    <div class="formRight lfloat">
        <input id="updatedDate" class="datefield" type="text" />
    </div>
    <div class="clear"></div>
    <div class="formLeft150 lfloat"></div>
    <div id="updateTrackingStatus" class=" btn btn-success">update</div>
    <div class="clear"></div>
    <div id="error" class="invisible errorField lfloat"></div>
    <div class="clear"></div>
    </div>
</script>
<script type="text/javascript">
<uc:security accessResource="MODIFY_TRACKING_NUMBER_POST_DISPATCH">
        Uniware.Utils.isAwbEditable = true;
</uc:security>
<uc:security accessResource="LENSKART_FITTING_HANDOVER">
        Uniware.Utils.isLenskartFitting = true;
</uc:security>
<uc:security accessResource="LENSKART_POS_INVOICE">
        Uniware.Utils.isLenskartPos = true;
</uc:security>
<uc:security accessResource="REPICK_ITEMS">
        Uniware.Utils.isRepickItems = true;
</uc:security>
<uc:security accessResource="LENSKART_QC_ACCESS">
        Uniware.Utils.isLenskartQc = true;
</uc:security>
<uc:security accessResource="JABONG_AUTO_INVOICE_PRINT">
        Uniware.Utils.isJabongAutoInvoicePrint = true;
</uc:security>
<uc:security accessResource="JABONG_SHIPPING_AUTOMATION">
        Uniware.Utils.isJabongShippingAutomation = true;
</uc:security>
<uc:security accessResource="LENSKART_RTO_RESHIP">
        Uniware.Utils.isLenskartReship = true;
</uc:security>
<uc:security accessResource="LENSKART_QC_INSPECTOR">
        Uniware.Utils.isLenskartQc = true;
Uniware.Utils.isLenskartQcInspector = true;
</uc:security>
<uc:security accessResource="LENSKART_VSM_PICKER">
        Uniware.Utils.isVsmPicker = true;
</uc:security>
Uniware.Utils.traceabilityLevel = "${configuration.getConfiguration('systemConfiguration').traceabilityLevel}";
Uniware.scanShipmentPage = function() {
    var self = this;
    this.table = null;
    this.spackage = null;
    this.methodToProviders = ${configuration.getConfiguration('shippingFacilityLevelConfiguration').methodCodeToProviderCodeToShippingProvidersJson};
    this.shippingMethods = ${configuration.getConfiguration('shippingConfiguration').shippingMethodsJson};
    this.methodTypeMap = ${configuration.getConfiguration('shippingFacilityLevelConfiguration').providerToMethodToShippingProviderMethodTypeJson};
    this.shippingPackageTypes = ${configuration.getConfiguration('shippingFacilityLevelConfiguration').shippingPackageTypesJson};
    this.singleActionInvoiceAndLabel = ${configuration.getConfiguration('systemConfiguration').singleActionInvoiceAndLabel};
    this.shipmentTrackingStatusJson = ${configuration.getConfiguration('shippingConfiguration').shipmentTrackingStatusJson};
    this.init = function() {
        $("#shipmentCode").keyup(self.scanShipment);
        $("#scanItemBarcode").keyup(self.scanItemBarcode);
        $("#scanGlobalOrderSearch").keyup(self.scanGlobalOrderSearch);
    }

    this.scanShipment = function(event) {
        if (event.which == 13) {
            if ($(this).val() == '') {
                Uniware.Utils.showError('Please enter a shipment id');
                return;
            }
            $("#searching").removeClass('hidden');
            self.searchShipment($('#shipmentCode').val());
        };
    };

    this.scanItemBarcode = function(event) {
        if (event.which == 13) {
            if ($(this).val() == '') {
                Uniware.Utils.showError('Please enter a item barcode');
                return;
            }
            $("#searching").removeClass('hidden');
            self.searchItemBarcode($('#scanItemBarcode').val());
        };
    };

    this.scanGlobalOrderSearch = function(event) {
        if (event.which == 13) {
            if ($(this).val() == '') {
                Uniware.Utils.showError('Please enter a order number');
                return;
            }
            self.searchGlobalOrderSearch($('#scanGlobalOrderSearch').val());
        };
    };

    this.searchGlobalOrderSearch = function(keyword) {
        var req = {
            'keyword' : keyword,
            'type' : "SALE_ORDER"
        };
        Uniware.Ajax.postJson("/data/search/globalSearch", JSON.stringify(req), function(response){
            if (response.successful && response.searchDTOs.length > 0) {
                self.searchOrder(response.searchDTOs[0].searchResults[response.searchDTOs[0].searchResults.length - 1].value, true);
            } else {
                Uniware.Utils.showError('No Orders Found. Please enter a valid order code');
            }
            $("#searching").addClass('hidden');
        });
    };

    this.searchItemBarcode = function(itemCode) {
        var req = {
            'itemCode': itemCode
        };
        Uniware.Ajax.postJson("/data/item/fetch", JSON.stringify(req), function(response) {
            if(response.successful == true) {
                $('#itemCode').val('')
                self.itemCode = response.itemDTO.code;
                var shipmentCode = '';
                for (var i=0;i<response.itemDTO.saleOrderItemDTOs.length;i++) {
                    var saleOrderItemDTO = response.itemDTO.saleOrderItemDTOs[i];
                    if (saleOrderItemDTO.shippingPackageCode) {
                        shipmentCode = saleOrderItemDTO.shippingPackageCode;
                        break;
                    }
                }
                if (shipmentCode != '') {
                    self.searchShipment(shipmentCode);
                } else {
                    Uniware.Utils.showError('No Shipment found for this item');
                }

            } else {
                Uniware.Utils.showError(response.errors[0].description);
            }
            $('#itemCode').val('').focus();
            $("#searching").addClass('hidden');
        });
    };

    this.searchShipment = function(shippingPackageCode) {
        var req = {
            'shipmentCode' : shippingPackageCode
        };
        Uniware.Ajax.postJson("/data/shipping/search", JSON.stringify(req), function(response){
            if (response.successful == true) {
                $('#shipmentCode').val('');
                self.spackage = response.shippingPackageFullDTO;
                self.showResults(self.spackage);
                Uniware.Utils.barcode();
                Uniware.Utils.renderComments("#shipmentComments", 'SO-' + self.spackage.orderNumber);
                $("#shipmentCode").val('').focus();
                if (self.spackage.invoiceCode && Uniware.Utils.isJabongAutoInvoicePrint) {
                    Uniware.Utils.printIFrame("/oms/invoice/show?invoiceCodes=" + self.spackage.invoiceCode + "&legacy=1");
                } else if (Uniware.Utils.isJabongShippingAutomation && self.spackage.actualWeight == self.spackage.estimatedWeight) {
                    $('#editShipment').click();
                }
                self.searchOrder(self.spackage.orderNumber);
            } else {
                Uniware.Utils.showError('No Items Found. Please enter a valid shipment code');
            }
            $("#searching").addClass('hidden');
        });
    };

    this.searchOrder = function(orderNumber, searchShipment) {
        Uniware.Ajax.postJson("/data/oms/saleorder/fetch", JSON.stringify({'code':orderNumber}), function(data) {
            if (searchShipment) {
                var shipmentCode = '';
                for (var i = 0;i<data.saleOrderDTO.shippingPackages.length;i++) {
                    var shippingPackage = data.saleOrderDTO.shippingPackages[i];
                    if (shippingPackage.status != 'CANCELLED') {
                        shipmentCode = shippingPackage.code;
                    }
                    if (shipmentCode != '') {
                        self.searchShipment(shipmentCode);
                    } else {
                        Uniware.Utils.showError('No Shipment found for this item');
                    }

                }
            } else {
                self.onOrderFetch(data);
            }
        });
    };

    this.onOrderFetch = function(data) {
        self.noOrderItems = data.saleOrderDTO.saleOrderItems.length;
        var address = data.saleOrderDTO.addresses[0];
        $('#shippingAddress').html('Shipping Address');
        var str = address.name + ' - ' + address.addressLine1;
        if (address.addressLine2) {
            str += ', ' + address.addressLine2;
        }
        str += '<br/>' + address.city;
        str += ', ' + address.state + ' - ' + address.pincode;
        $('#shippingAddressValue').html(str);
        var str = '';
        var strStockRetire = '';
        for (var i = 0;i<data.saleOrderDTO.saleOrderItems.length;i++) {
            var item = data.saleOrderDTO.saleOrderItems[i];
            for (var j = 0;j < item.customFieldValues.length;j++) {
                var customField = item.customFieldValues[j];
                if (customField.fieldName == 'right_lens') {
                    if (customField.fieldValue == 0) {
                        $('#lenstype-' + item.code).html('Left Lens');
                    } else if (customField.fieldValue == 1) {
                        $('#lenstype-' + item.code).html('Right Lens');
                    }
                }
                if (customField.fieldName == 'lens_height' && customField.fieldValue) {
                    $('#lensHeight-' + item.code).html('Lens Height = ' + customField.fieldValue);
                }
                if (customField.fieldName == 'lens_width' && customField.fieldValue) {
                    $('#lensWidth-' + item.code).html('Lens Width = ' + customField.fieldValue);
                }
                if (customField.fieldName == 'patientComments' && customField.fieldValue) {
                    $('#patientComments-' + item.code).html('Patient Comments = ' + customField.fieldValue);
                }
            }
            if (item.statusCode == 'UNFULFILLABLE') {
                str += '<a href="http://vsm.lenskart.com/viewAddInventory/' + item.itemSku + '" target="_blank">' + item.code + ' - ' + item.itemName + ' - ' + item.itemSku + '</a><br/>';
                strStockRetire += '<a href="http://vsm.lenskart.com/viewStartRetire/' + item.itemSku + '" target="_blank">' + item.code + ' - ' + item.itemName + ' - ' + item.itemSku + '</a><br/>';
            }
        }
        $('#addInventory').html(str);
        $('#startRetire').html(strStockRetire);
        self.getOtherPackages();
    };

    this.showResults = function(shippingPackage) {
        self.initSpInfoTemplate(shippingPackage);
        $("#orderItems").html(template(shippingPackage.statusCode == 'SPLITTED' ? "packageSplits" : "itemsTemplate", shippingPackage)).removeClass('hidden');
        if(shippingPackage.splittable == true) {
            self.split();
        }
        self.specificToClient(shippingPackage);
    };

    this.initSpInfoTemplate = function(shippingPackage) {
        shippingPackage.shippingProviders = self.methodToProviders[shippingPackage.shippingMethodCode];
        shippingPackage.shippingMethods = self.shippingMethods;
        shippingPackage.shippingPackageTypes = self.shippingPackageTypes;
        for(var i=0;i<shippingPackage.shippingPackageItems.length;i++) {
            if (shippingPackage.shippingPackageItems[i].onHold) {
                shippingPackage.onHold |= shippingPackage.shippingPackageItems[i].onHold;
            }
        }
        if ((Uniware.Utils.isLenskartFitting || Uniware.Utils.isLenskartQc || Uniware.Utils.isVsmPicker) && (shippingPackage.statusCode == 'PENDING_CUSTOMIZATION' || shippingPackage.statusCode == 'PICKING'
                || shippingPackage.statusCode == 'PICKED' || shippingPackage.statusCode == 'CREATED' || shippingPackage.statusCode == 'CUSTOMIZATION_COMPLETE')) {

            $("#spInfoDiv").html(template("lenskartWorkOrderTemplate", shippingPackage)).removeClass('hidden');

        } else {
            $("#spInfoDiv").html(template("spInfoTemplate", shippingPackage)).removeClass('hidden');

        }

        $("#markPendingCustomization").click(function(){
            var req = {
                'shippingPackageCode' : self.spackage.code
            }
            Uniware.Ajax.postJson("/data/oms/packer/workorder", JSON.stringify(req), function(response) {
                if(response && response.successful) {
                    self.searchShipment(self.spackage.code);
                    Uniware.Utils.addNotification("Shipping Package Status Marked as Pending Customization");
                } else {
                    Uniware.Utils.showError(response.errors[0].description);
                }
            });
        });

        $("#markCompleteCustomization").click(function(){
            var req = {
                'shippingPackageCode' : self.spackage.code
            }
            Uniware.Ajax.postJson("/data/oms/shipment/markCustomizationComplete",JSON.stringify(req),function(response){
                if(response.successful == true){
                    self.searchShipment(self.spackage.code);
                    Uniware.Utils.addNotification("Shipping Package Status Marked as CUSTOMIZATION_COMPLETE");
                    if (Uniware.Utils.isLenskartFitting) {
                        var req = {
                            comment : 'Order=' +  self.spackage.displayOrderNumber + ',action=WorkorderComplete,Reason=',
                            referenceIdentifier : 'SO-' + self.spackage.orderNumber
                        }
                        Uniware.Ajax.postJson("/data/comment/add", JSON.stringify(req), function(response) {
                            if (response.successful == true) {
                                //Uniware.Utils.addNotification("Comment has been added");
                            } else {
                                Uniware.Utils.showError(response.errors[0].description);
                            }
                        }, true);
                    }
                    $('#printInvoice').click(self.printInvoiceDialog(self.spackage.invoiceCode));
                } else {
                    Uniware.Utils.showError(response.errors[0].description);
                }
            });
        });
        $(".datefield").datepicker({
            dateFormat : 'dd/mm/yy'
        });
        $("#updateStatus").click(self.updateStatus);
        Uniware.Utils.barcode();
        $('#editShipment').click(function(event) {
            self.editShipment();
        });
        $('#editPackageWeight').click(function(event) {
            self.editShipmentDetail();
        });
        $('#redispatchShipment').click(self.redispatchShipment);
        $('#editCustomFieldValues').click(function(event) {
            self.editShipmentCustomFieldDetail();
        });
        $('.editAwbNumber').click(function(){
            self.editAwbNumberValue();
        });
        $('.editPackedPackageAwbNumber').click(function(){
            self.editPackedPackageAwbNumberValue();
        });
        $('#workorderFieldsEdit').click(self.updateCustomFieldDetail);
        $('#updateInvoice').click(self.updateInvoice);
        $('#printLabel').click(function(){
            var req = {
                shippingPackageCode : self.spackage.code
            };
            Uniware.Ajax.postJson("/data/oms/shipment/provider/allocate", JSON.stringify(req), function(data) {
                if (data && data.successful && data.shippingProviderCode) {
                    self.searchShipment(self.spackage.code);
                    Uniware.Utils.printIFrame(data.shippingLabelLink ? data.shippingLabelLink + '?legacy=1': "/oms/shipment/show/" + self.spackage.code + '?legacy=1');
                } else {
                    Uniware.Utils.showError(data.errors[0].description);
                }
            }, true);

        });

        $('#printInvoice').click(self.printInvoice);
        $("#shipmentCode").val('').focus();

    };

    this.printInvoice = function() {
        var req = {
            shippingPackageCode : self.spackage.code
        };
        if (!self.singleActionInvoiceAndLabel) {
            Uniware.Ajax.postJson("/data/oms/invoice/create", JSON.stringify(req), function(data) {
                if (data && data.successful && data.invoiceCode) {
                    Uniware.Utils.printIFrame("/oms/invoice/show?invoiceCodes=" + self.spackage.invoiceCode + '&legacy=1');
                    self.searchShipment(self.spackage.code);
                } else {
                    Uniware.Utils.showError(data.errors[0].description);
                }
            }, true);
        } else {
            Uniware.Ajax.postJson("/data/oms/shipment/invoice/provider/allocate", JSON.stringify(req), function(data) {
                if (data && data.successful && data.shippingProviderCode) {
                    Uniware.Utils.printIFrame(data.shippingLabelLink ? data.shippingLabelLink : "/oms/shipment/show/" + self.spackage.code + '?legacy=1');
                    self.searchShipment(self.spackage.code);
                } else {
                    Uniware.Utils.showError(data.errors[0].description);
                }
            }, true);
        }
    };

    this.redispatchShipment = function () {
        var req = {
            'shippingPackageCode' : self.spackage.code,
            'saleOrderCode' : self.spackage.orderNumber
        };
        Uniware.Ajax.postJson("/data/oms/returns/shipment/redispatch", JSON.stringify(req), function(data) {
            if (data && data.successful) {
                Uniware.Utils.addNotification('Reshipment create successfully');
                self.searchShipment(self.spackage.code);
            } else {
                Uniware.Utils.showError(data.errors[0].description);
            }
        }, true);
    };

    this.updateStatus = function(){
        Uniware.LightBox.show("#statusUpdateDiv");
        $("#statusUpdateTemplateDiv").html(template("statusUpdateTemplate",self.shipmentTrackingStatusJson));
        $(".datefield").datepicker({
            dateFormat : 'dd/mm/yy'
        });
        $("#updateTrackingStatus").click(self.updateTrackingStatus);
    };

    this.updateTrackingStatus = function(){
        var req = {};
        req.shippingPackageCode = self.spackage.code;

        if($('#updatedDate').val() != "") {
            req.statusDate = Date.fromPaddedDate($('#updatedDate').val()).getTime();
        }
        if($('#trackingStatus').val() != ""){
            req.shipmentTrackingStatus = $('#trackingStatus').val();
        }
        req.manualStatusUpdate = true;
        Uniware.Ajax.postJson("/data/oms/shipment/shipmentStatusUpdate", JSON.stringify(req), function(response) {
            if(response.successful == true) {
                self.searchShipment(self.spackage.code);
                Uniware.Utils.addNotification('Tracking Status Updated Successfully');
                Uniware.LightBox.hide();
            } else {
                $('#error').html(response.errors[0].description);
                $('#error').removeClass('invisible');
            }
        });
    };

    this.printInvoiceDialog = function(invoiceCode) {
        Uniware.Utils.printIFrame("/oms/invoice/show?invoiceCodes=" + invoiceCode + '&legacy=1');
        $("#masterScan").focus();
    };

    this.updateInvoice = function() {
        var req = {
            shippingPackageCode: self.spackage.code
        }
        Uniware.Ajax.postJson("/data/oms/invoice/update", JSON.stringify(req), function(response) {
            if(response.successful == true) {
                Uniware.Utils.addNotification('Shipping Package Invoice Updated Successfully');
            } else {
                Uniware.Utils.showError(response.errors[0].description);
            }
        });
    };

    this.editShipment = function(event) {
        self.spackage.isEditing = true;
        $("#spInfoDiv").html(template("spInfoTemplate", self.spackage));
        $(".datefield").datepicker({
            dateFormat : 'dd/mm/yy'
        });
        $("#shippingPackageType").change(function() {
            var packageType = self.shippingPackageTypes[$(this).val()];
            $('#length').val(packageType.boxLength);
            $('#width').val(packageType.boxWidth);
            $('#height').val(packageType.boxHeight);
        });
        Uniware.Utils.barcode();
        $("#shippingProvider").change(function(){
            var shippingProvider = $("#shippingProvider").val();
            var shippingProviderMethodType = self.methodTypeMap[shippingProvider.toLowerCase()][self.spackage.shippingMethodCode];
            if(shippingProviderMethodType == "MANUAL"){
                $("#awbNumber").html("<input type='text' id='trackingNumber' />");
            } else {
                $("#awbNumber").html(self.spackage.trackingNumber);
            }
        });
        $('#updateSP').removeClass("invisible");
        $('#cancelUpdate').removeClass("hidden");
        $("#updateSP").click(self.updateSP);
        $('#cancelUpdate').click(self.cancelUpdate);
        if (Uniware.Utils.isJabongShippingAutomation) {
            $('#actualWeight').val('').focus();
        }
    };

    this.editShipmentDetail = function() {
        self.spackage.isWeightEditing = true;
        $("#spInfoDiv").html(template("spInfoTemplate", self.spackage));
        $(".datefield").datepicker({
            dateFormat : 'dd/mm/yy'
        });
        $("#shippingPackageType").change(function() {
            var packageType = self.shippingPackageTypes[$(this).val()];
            $('#length').val(packageType.boxLength);
            $('#width').val(packageType.boxWidth);
            $('#height').val(packageType.boxHeight);
        });
        Uniware.Utils.barcode();
        $('#updateSP').removeClass("invisible");
        $('#cancelUpdate').removeClass("hidden");
        $("#updateSP").click(self.updateSPDetail);
        $('#cancelUpdate').click(self.cancelUpdate);
    };

    this.editShipmentCustomFieldDetail = function() {
        self.spackage.isCustomFieldEditing = true;
        $("#spInfoDiv").html(template("spInfoTemplate", self.spackage));
        $(".datefield").datepicker({
            dateFormat : 'dd/mm/yy'
        });
        Uniware.Utils.barcode();
        $('#updateCustomField').removeClass("invisible");
        $('#cancelUpdateFields').removeClass("hidden");
        $("#updateCustomField").click(self.updateCustomFieldDetail);
        $('#cancelUpdateFields').click(self.cancelUpdateFields);
    };

    this.editAwbNumberValue = function() {
        self.spackage.isAwbNumberEditing = true;
        $("#spInfoDiv").html(template("spInfoTemplate", self.spackage));
        Uniware.Utils.barcode();
        $('#updateAwb').removeClass("invisible");
        $('#cancelAwbUpdate').removeClass("hidden");
        $("#updateAwb").click(self.updateAwbNumber);
        $('#cancelAwbUpdate').click(self.cancelAwbUpdate);
    };

    this.editPackedPackageAwbNumberValue = function() {
        self.spackage.isAwbNumberEditingForPackedPackage = true;
        $("#spInfoDiv").html(template("spInfoTemplate", self.spackage));
        Uniware.Utils.barcode();
        $('#updateAwb').removeClass("invisible");
        $('#cancelAwbUpdate').removeClass("hidden");
        $("#updateAwb").click(self.updateAwbNumber);
        $('#cancelAwbUpdate').click(self.cancelAwbUpdate);
    };

    this.updateSP = function() {
        var req = {
            trackingNumber : $("#trackingNumber").val(),
            shippingProviderCode: self.spackage.providerEditable ? $('#shippingProvider').val() : self.spackage.shippingProviderCode,
            shippingPackageCode: self.spackage.code,
            shippingPackageTypeCode: $('#shippingPackageType').val(),
            actualWeight: $('#actualWeight').val(),
            noOfBoxes : $('#noOfBoxes').val(),

            shippingBox: {
                length : $('#length').val(),
                width : $('#width').val(),
                height : $('#height').val()
            }
        }
        Uniware.Utils.addCustomFieldsToRequest(req, $('.custom'));
        Uniware.Ajax.postJson("/data/shipping/package/update", JSON.stringify(req), function(response) {
            if(response.successful == true) {
                self.spackage = response.shippingPackageFullDTO;
                self.initSpInfoTemplate(self.spackage);
                if (Uniware.Utils.isJabongShippingAutomation) {
                    var req = {
                        comment : 'Shipment updated for - ' + self.spackage.code,
                        referenceIdentifier : 'SO-' + self.spackage.orderNumber
                    }
                    Uniware.Ajax.postJson("/data/comment/add", JSON.stringify(req), function(response) {
                        if (response.successful == true) {
                            Uniware.Utils.addNotification("Comment has been added");
                        } else {
                            Uniware.Utils.showError(response.errors[0].description);
                        }
                    }, true);
                    Uniware.Utils.printIFrame("/oms/shipment/show/" + self.spackage.code + '?legacy=1');
                }
                Uniware.Utils.addNotification('Shipping Package Updated Successfully');

            } else {
                Uniware.Utils.showError(response.errors[0].description);
            }
        },true);
    };

    this.updateSPDetail = function() {
        var req = {
            shippingPackageCode: self.spackage.code,
            shippingPackageTypeCode: $('#shippingPackageType').val(),
            actualWeight: $('#actualWeight').val(),
            noOfBoxes : $('#noOfBoxes').val(),

            shippingBox: {
                length : $('#length').val(),
                width : $('#width').val(),
                height : $('#height').val()
            }
        }
        Uniware.Utils.addCustomFieldsToRequest(req, $('.custom'));
        Uniware.Ajax.postJson("/data/shipping/package/detail/update", JSON.stringify(req), function(response) {
            if(response.successful == true) {
                self.spackage = response.shippingPackageFullDTO;
                self.initSpInfoTemplate(self.spackage);
                if (Uniware.Utils.isJabongShippingAutomation) {
                    var req = {
                        comment : 'Shipment updated for - ' + self.spackage.code,
                        referenceIdentifier : 'SO-' + self.spackage.orderNumber
                    }
                    Uniware.Ajax.postJson("/data/comment/add", JSON.stringify(req), function(response) {
                        if (response.successful == true) {
                            Uniware.Utils.addNotification("Comment has been added");
                        } else {
                            Uniware.Utils.showError(response.errors[0].description);
                        }
                    }, true);
                    Uniware.Utils.printIFrame("/oms/shipment/show/" + self.spackage.code + '?legacy=1');
                }
                Uniware.Utils.addNotification('Shipping Package Updated Successfully');
            } else {
                Uniware.Utils.showError(response.errors[0].description);
            }
        });
    };

    this.updateCustomFieldDetail = function() {
        if(!confirm("Are you sure?")){
            return false;
        }else{
            var req = {
                shippingPackageCode: self.spackage.code,
                noOfBoxes : 1
            }
            Uniware.Utils.addCustomFieldsToRequest(req, $('.custom'));
            Uniware.Ajax.postJson("/data/shipping/package/detail/update/customFields", JSON.stringify(req), function(response) {
                if(response.successful == true) {
                    self.searchShipment(self.spackage.code);
                    Uniware.Utils.addNotification('Shipping Package Updated Successfully');
                } else {
                    Uniware.Utils.showError(response.errors[0].description);
                }
            }, true);
        }
    };

    this.updateAwbNumber = function() {
        if(!confirm("Are you sure?")){
            return false;
        }else{
            var req = {
                shippingPackageCode: self.spackage.code,
                trackingNumber: $('#awbNumber').val()
            }
            var url = null;
            if(self.spackage.statusCode == 'PACKED'){
                url = "/data/oms/shipment/awb/assign";
            } else {
                url = "/data/shipping/package/detail/update/awbNumber";
            }
            Uniware.Ajax.postJson(url, JSON.stringify(req), function(response) {
                if(response.successful == true) {
                    if(self.spackage.statusCode == 'PACKED'){
                        window.location.href = '/shipping/scanShipment?legacy=1&shipmentCode=' + self.spackage.code;
                    } else {
                        self.spackage = response.shippingPackageFullDTO;
                        self.initSpInfoTemplate(self.spackage);
                        Uniware.Utils.renderComments("#shipmentComments", 'SO-' + self.spackage.orderNumber);
                        Uniware.Utils.addNotification('Shipping Package Tracking Number Updated Successfully');
                    }
                } else {
                    Uniware.Utils.showError(response.errors[0].description);
                }
            });
        }
    };

    this.cancelUpdate = function(){
        self.spackage.isEditing = null;
        self.spackage.isWeightEditing = null;
        self.initSpInfoTemplate(self.spackage);
    };

    this.cancelUpdateFields = function(){
        self.spackage.isCustomFieldEditing = null;
        self.initSpInfoTemplate(self.spackage);
    };

    this.cancelAwbUpdate = function() {
        self.spackage.isAwbNumberEditing = null;
        self.spackage.isAwbNumberEditingForPackedPackage = null;
        self.initSpInfoTemplate(self.spackage);
    };

    this.split = function(){
        $('#split').click(function(){
            $("#orderItems").html(template("splitPackage", self.spackage));
            self.splitPackage();
            self.discard();
        });
    };

    this.splitPackage = function(){
        $('#save').click(function(){
            var requestObject = {
                'shippingPackageCode' : self.spackage.code,
                'splitItems' : []
            };

            var areAllSplitNumberSame = true;
            var splitNumber = null;

            $('.splitItem').each(function(){
                var splitItem = {};
                splitItem.saleOrderItemCode = self.spackage.shippingPackageItems[requestObject.splitItems.length].saleOrderItemCode;
                splitItem.splitNumber = $(this).val();
                if(splitNumber !=null && splitItem.splitNumber != splitNumber){
                    areAllSplitNumberSame = false;
                }
                splitNumber = splitItem.splitNumber;
                requestObject.splitItems[requestObject.splitItems.length] = splitItem;
            });

            if(areAllSplitNumberSame == true){
                Uniware.Utils.showError('To split, Please enter different package numbers.');
                return false;
            }

            Uniware.Ajax.postJson("/data/shipping/splitPackage", JSON.stringify(requestObject), function(response) {
                if(response.successful == true){
                    self.spackage = response.parentShippingPackage;
                    self.showResults(response.parentShippingPackage);
                    Uniware.Utils.addNotification("Shipping Package has been successfully splitted");
                }
            });
        });
    };

    this.discard = function(){
        $('#discard').click(function(){
            self.showResults(self.spackage);
        });
    };

    this.getOtherPackages = function(){
        var requestObject = {
            saleOrderCode : self.spackage.orderNumber
        };
        Uniware.Ajax.postJson("/data/oms/shipment/allShipments/get", JSON.stringify(requestObject), function(response) {
            response.saleOrderCode = self.spackage.orderNumber;
            if(response.elements.length > 1) {
                $("#allPackages").html(template("allPackagesTemplate", response)).removeClass('hidden');

                $("input#selectAll").change(function () {
                    var state = $("input#selectAll").is(":checked");
                    $("input.packageCode").attr('checked', state).trigger('change');
                });
            }
            var itemsCount = 0;
            var packages = 0;
            var str = '';
            for (var i = 0;i < response.elements.length;i++) {
                if (response.elements[i].status == 'PENDING_CUSTOMIZATION') {
                    itemsCount += response.elements[i].noOfItems;
                    packages += 1;
                }
                if (response.elements[i].status != 'CANCELLED') {
                    str += '<a href="/shipping/thirdPartyShipment?shipmentCode=' + response.elements[i].code + '&legacy=1">' + response.elements[i].code + '</a>' + ' - ' + response.elements[i].status + '<br/>';
                }
            }
            $('#otherPicklists').html(str);
            if (itemsCount == self.noOrderItems && packages > 1) {
                $('#mergePackages').removeClass('hidden').click(self.mergePackages);
                $('#messageDiv').html('<div style="padding:10px;font-weight:bold;">All Items have been Picked. Click on Merge shipments</div>');
            } else if (self.noOrderItems != self.spackage.noOfItems) {
                $('#messageDiv').html('<div style="background:red;font-color:white;padding:10px;">All Items have been Not been Picked. Please wait for other items</div>');
            }
            if (self.noOrderItems != self.spackage.noOfItems) {
                $('#lenskartWorkorderFieldsEdit').addClass('hidden');
                $('div.markQCPass').addClass('hidden');
            } else {
                $('#lenskartWorkorderFieldsEdit').removeClass('hidden');
            }
            if (Uniware.Utils.isLenskartQc && !Uniware.Utils.isLenskartQcInspector) {
                $('#lenskartWorkorderFieldsEdit').addClass('hidden');
            }

        });
    };

    this.mergePackages = function() {
        var selectedPackages = $("input.packageCode:checked");
        if (selectedPackages.length < 2) {
            alert('Please select at least 2 Packages');
            return;
        }
        var req = {
            'saleOrderCode' : self.spackage.orderNumber,
            'shippingPackageCodes' : []
        };
        selectedPackages.each(function(){
            req.shippingPackageCodes[req.shippingPackageCodes.length] =  $(this).attr('id');
        });

        Uniware.Ajax.postJson("/data/shipping/packages/merge", JSON.stringify(req), function(response) {
            if(response.successful){
                self.searchShipment(response.shippingPackageCode);
                Uniware.Utils.addNotification("Packages have been merged successfully");
            }else{
                if(response.message != null){
                    Uniware.Utils.showError(response.message);
                } else {
                    Uniware.Utils.showError(response.errors[0].description);
                }
            }
        });
    };

    this.specificToClient = function(shippingPackage) {

        if ((Uniware.Utils.isLenskartFitting || Uniware.Utils.isLenskartQc || Uniware.Utils.isRepickItems || Uniware.Utils.isVsmPicker) && (shippingPackage.statusCode == 'PENDING_CUSTOMIZATION' || shippingPackage.statusCode == 'PICKING'
                || shippingPackage.statusCode == 'PICKED' || shippingPackage.statusCode == 'CREATED' || shippingPackage.statusCode == 'CUSTOMIZATION_COMPLETE')) {
            self.spackage.itemCodes = [];
            for (var i=0;i<shippingPackage.shippingPackageItems.length;i++) {
                if (shippingPackage.shippingPackageItems[i].itemCode) {
                    self.spackage.itemCodes.push(shippingPackage.shippingPackageItems[i].itemCode);
                }
            }

            for (var i=0;i<shippingPackage.saleOrderCustomFieldValues.length;i++) {
                var customField = shippingPackage.saleOrderCustomFieldValues[i];
                if (customField.fieldName == 'customer_Comments' && customField.fieldValue) {
                    $('#customerComments').html(customField.fieldValue.replace(/\\n/g,'<br/>'));
                }
                if (customField.fieldName == 'splOrderFlag') {
                    $('#specialOrder').html('<span style="font-weight:bold;font-size:24px;">' + customField.fieldValue + '</span>');
                }
                if (customField.fieldName == 'customerServiceComments' && customField.fieldValue) {
                    $('#customerServiceComments').html(customField.fieldValue.replace(/\\n/g,'<br/>'));
                }

            }
            var info = '';
            var allItemsScanned = true;
            for (var i=0;i<shippingPackage.shippingPackageItems.length;i++) {
                var shippingPackageItem = shippingPackage.shippingPackageItems[i];
                $('#isGiftWrap').html('' + shippingPackageItem.giftWrap);
                $('#giftMessage').html(shippingPackageItem.giftMessage);
                for (var j=0;j<shippingPackageItem.customFieldValues.length;j++) {
                    var customField = shippingPackageItem.customFieldValues[j];
                    if (customField.fieldName == 'personalization_info' && customField.fieldValue && customField.fieldValue != '') {
                        info += shippingPackageItem.code + ' - ' +  customField.fieldValue;
                    }
                }
                allItemsScanned = allItemsScanned && (shippingPackageItem.itemCode != null && shippingPackageItem.itemCode != '');
            }
            if (allItemsScanned && shippingPackage.statusCode == 'PICKED' && shippingPackage.requiresCustomization) {
                $('#markPendingCustomization').removeClass('hidden');
            }
            $('#personalizationInfo').html(info);
            for (var i=0;i<shippingPackage.customFieldValues.length;i++) {
                var customField = shippingPackage.customFieldValues[i];
                if (customField.fieldName == 'qcHold') {
                    self.spackage.qcHold = customField.fieldValue;
                    if (self.spackage.qcHold) {
                        $('#qcHold').html('<div id="unholdQC" class="btn btn-small btn-warning lfloat50">Unhold QC</div>');
                    }
                }
            }

            if (Uniware.Utils.isRepickItems && (self.spackage.itemCodes.length != shippingPackage.shippingPackageItems.length)) {
                $('#headerTd').append('<div id="scanItems" class="btn btn-small btn-primary rfloat20">pick Items</div>');
            }
            $('#scanItems').click(self.itemScanAndPrintInvoice);

            $("#orderItems").html(template("lenskartItemsTemplate", shippingPackage));

            if (shippingPackage.statusCode != 'PENDING_CUSTOMIZATION' && shippingPackage.statusCode != 'CUSTOMIZATION_COMPLETE' && $('#fitting_assigned_to').val() != '') {
                $('#fitting_assigned_to').html('<option value="' + $('#fitting_assigned_to').val() + '">' + $('#fitting_assigned_to').val() + '</option>');
            }
            var fitting = $('#fittingStatus');
            if (fitting.val() == '') {
                fitting.html('<option value="fitting assigned">fitting assigned</option>');
                Uniware.Utils.addFittingAssignedComment = true;

            } else if (fitting.val() == 'fitting assigned') {
                $('#fittingStatus').html('<option value="' + $('#fittingStatus').val() + '">' + $('#fittingStatus').val() + '</option>');
                if (shippingPackage.statusCode == 'PENDING_CUSTOMIZATION') {
                    $('#markCompleteCustomization').removeClass('hidden');
                }
            }

            $('div.markQCDamage').click(self.markQCDamage);
            $('div.markQCHold').click(self.markQCHold);
            $('div.markQCPass').click(self.markQCPass);
            $('#lenskartWorkorderFieldsEdit').click(self.lenskartWorkorderFieldsEdit);
        }

        if (Uniware.Utils.isLenskartPos && (shippingPackage.statusCode == 'CREATED' || shippingPackage.statusCode == 'PICKED')) {
            $('#editPackageWeight').addClass('hidden');
            $('#headerTd').append('<div id="printPosInvoice" class="btn btn-small btn-primary rfloat20">print invoice</div>');
            if (shippingPackage.statusCode == "PICKED") {
                $('#printPosInvoice').click(self.printInvoice);
            } else {
                $('#printPosInvoice').click(self.itemScanAndPrintInvoice);
            }
        }
    };

    this.lenskartWorkorderFieldsEdit = function() {
        if (!confirm("Are you sure?")) {
            return false;
        } else {
            var req = {
                shippingPackageCode: self.spackage.code,
                noOfBoxes : 1
            }
            if ($('#fittingStatus').val() == 'fitting assigned' && $('#fitting_assigned_to').val() == '') {
                alert('Fitting Assigned To field cannot be empty');
                return;
            }
            if ($('#fittingStatus').val() == 'fitting assigned' && self.spackage.onHold) {
                alert('This order is on Hold. Please wait.');
                return;
            }
            Uniware.Utils.addCustomFieldsToRequest(req, $('.custom'));
            var fittingAssigned = $('#fitting_assigned_to').val();
            var oldFittingAssigned = '';
            for (var i=0;i<self.spackage.customFieldValues.length;i++) {
                if (self.spackage.customFieldValues[i].fieldName == 'fitting_assigned_to') {
                    oldFittingAssigned = self.spackage.customFieldValues[i].fieldValue;
                }
            }
            if (oldFittingAssigned != fittingAssigned) {
                Uniware.Utils.addFittingAssignedComment = true;
            }
            Uniware.Ajax.postJson("/data/shipping/package/detail/update/customFields", JSON.stringify(req), function(response) {
                if(response.successful == true) {
                    self.searchShipment(self.spackage.code);
                    Uniware.Utils.addNotification('Shipping Package Updated Successfully');
                    if (Uniware.Utils.addFittingAssignedComment && Uniware.Utils.isLenskartFitting) {
                        var req = {
                            comment : 'Order=' +  self.spackage.displayOrderNumber + ',action=FittingAssigned:' + fittingAssigned + ',Reason=',
                            referenceIdentifier : 'SO-' + self.spackage.orderNumber
                        }
                        Uniware.Ajax.postJson("/data/comment/add", JSON.stringify(req), function(response) {
                            if (response.successful == true) {
                                //Uniware.Utils.addNotification("Comment has been added");
                            } else {
                                Uniware.Utils.showError(response.errors[0].description);
                            }
                        }, true);
                    }
                } else {
                    Uniware.Utils.showError(response.errors[0].description);
                }
            }, true);
        }
    };

    this.markQCHoldSubmit = function() {
        var req = {
            shippingPackageCode: self.spackage.code,
            noOfBoxes : 1
        }
        req.customFieldValues = [];
        req.customFieldValues.push({
            'name' : 'qcHold',
            'value' : 'true'
        });
        req.customFieldValues.push({
            'name' : 'shelfCode',
            'value' : $('#qcShelfCode').val()
        });
        req.customFieldValues.push({
            'name' : 'qcHoldRemarks',
            'value' : self.spackage.itemCode + ' - ' + $('#qcRejectionReason').val()
        });
        Uniware.Ajax.postJson("/data/shipping/package/detail/update/customFields", JSON.stringify(req), function(response) {
            if(response.successful == true) {
                var req = {
                    comment : 'Order=' +  self.spackage.displayOrderNumber + ',action=QcHold,Reason=' + $('#qcDamageItemCodeSubmit').html() + '-' + $('#qcRejectionReason').val(),
                    referenceIdentifier : 'SO-' + self.spackage.orderNumber
                }
                Uniware.Ajax.postJson("/data/comment/add", JSON.stringify(req), function(response) {
                    if (response.successful == true) {
                        Uniware.Utils.addNotification("Comment has been added");
                    } else {
                        Uniware.Utils.showError(response.errors[0].description);
                    }
                }, true);
                self.searchShipment(self.spackage.code);
                Uniware.Utils.addNotification('Shipping Package Updated Successfully');
            } else {
                Uniware.Utils.showError(response.errors[0].description);
            }
        }, true);
    };

    this.markQCHold = function() {
        var items = $(this).attr('id').split('-');
        var itemCode = items.splice(1,items.length).join('-');
        self.spackage.itemCode = itemCode;
        $("#qcDamageItemList").html(template("qcHoldItemListTemplate", self.spackage));

        Uniware.LightBox.show("#qcDamageItems", function() {
            $("#qcHoldSubmit").click(self.markQCHoldSubmit);
        });
    };

    this.markQCPass = function() {
        var items = $(this).attr('id').split('-');
        var itemCode = items.splice(1,items.length).join('-');
        itemCode = itemCode.replace(/( )/g, "\\\\$1" );
        $('#qcDamage-' + itemCode).addClass('hidden');
        $('#qcHold-' + itemCode).addClass('hidden');
        $('#qcPass-' + itemCode).removeClass('markQCPass').html('QC Done').css('opacity','0.8');

        if ($('div.markQCPass').length == 0) {
            self.printInvoice();
        }
    };

    this.qcDamageSubmit = function () {
        var req = {
            shippingPackageCode : self.spackage.code,
            saleOrderCode : self.spackage.orderNumber,
            itemCode : $('#qcDamageItemCodeSubmit').html(),
            fixedPacket : !self.spackage.requiresCustomization,
            rejectionReason : $('#qcRejectionReason').val()
        };
        req.shippingPackageCustomFieldValues = [];
        req.saleOrderCustomFields = [];
        req.shippingPackageCustomFieldValues.push({
            'name' : 'fittingStatus',
            'value' : '-'
        });
        req.shippingPackageCustomFieldValues.push({
            'name' : 'fitting_assigned_to',
            'value' : '-'
        });
        req.shippingPackageCustomFieldValues.push({
            'name' : 'shelfCode',
            'value' : $('#qcShelfCode').val()
        });
        req.saleOrderCustomFields.push({
            'name' : 'saleOrderShelf',
            'value' : $('#qcShelfCode').val()
        });

        var url = 'http://vsm.lenskart.com/updateQcFail.php?uwItemId=';
        url += $('#qcDamageSaleOrderItemCodeSubmit').html() + '&qcFailReason=' + $('#qcRejectionReason').val() + '&qcFailShelf=' + $('#qcShelfCode').val();
        Uniware.Ajax.getJson(url, function(response) {
            if (response[0] == 'Success') {
                Uniware.Ajax.postJson("/data/oms/packer/outboundqc/damage", JSON.stringify(req), function(data) {
                    if (data && data.successful) {
                        var req = {
                            comment : 'Order=' +  self.spackage.displayOrderNumber + ',action=QcReject,Reason=' + $('#qcDamageItemCodeSubmit').html() + '-' + $('#qcRejectionReason').val(),
                            referenceIdentifier : 'SO-' + self.spackage.orderNumber
                        }
                        Uniware.Ajax.postJson("/data/comment/add", JSON.stringify(req), function(response) {
                            if (response.successful == true) {
                                Uniware.Utils.addNotification("Comment has been added");
                            } else {
                                Uniware.Utils.showError(response.errors[0].description);
                            }
                        }, true);



                        self.searchShipment(self.spackage.code);
                    } else {
                        Uniware.Utils.showError(data.errors[0].description);
                    }
                }, true);
            } else {
                Uniware.Utils.showError('Error from VSM hit - ' + response[0]);
            }
        });


    };

    this.markQCDamage = function() {
        var items = $(this).attr('id').split('-');
        var itemCode = items.splice(1,items.length).join('-');
        self.spackage.itemCode = itemCode;
        self.spackage.saleOrderItemCode = $(this).attr('data-saleOrderItem');
        $("#qcDamageItemList").html(template("qcDamageItemListTemplate", self.spackage));

        Uniware.LightBox.show("#qcDamageItems", function() {
            $("#qcDamageSubmit").click(self.qcDamageSubmit);
        });
    };

    this.itemScanAndPrintInvoice = function() {
        if (self.spackage.itemCodes && self.spackage.itemCodes.length == 0) {
            self.spackage.itemCodes = [];
            for (var i=0;i<self.spackage.shippingPackageItems.length;i++) {
                if (self.spackage.shippingPackageItems[i].itemCode) {
                    self.spackage.itemCodes.push(self.spackage.shippingPackageItems[i].itemCode);
                }
            }
        }

        $("#traceableItemList").html(template("traceableItemsTemplate", self.spackage));
        Uniware.LightBox.show("#traceableItems", function(){
            $("#traceableInput").val('').focus();
            $("#traceableInput").keyup(self.onItemSubmit);
        }, function() {
            self.spackage.itemCodes = [];
        });
    }

    this.onItemSubmit = function(event) {
        if (event.which == 13 && $(this).val() != '') {
            $('#error').addClass('invisible');
            if (!self.spackage.itemCodes) {
                self.spackage.itemCodes = [];
            }
            var itemCode = $(this).val().replace(/[^a-zA-Z0-9_-]/g,'');
            if ($.inArray(itemCode, self.spackage.itemCodes) == -1) {
                self.spackage.itemCodes.push(itemCode);
                $('#scannedItems').html("Items scanned : " + self.spackage.itemCodes.join(','));
                $(this).val('');
            } else {
                $('#error').removeClass('invisible').html('Item already scanned');
            }

            if (self.spackage.shippingPackageItems.length == self.spackage.itemCodes.length) {
                // all items scanned print invoice
                var req = {
                    'saleOrderCode' : self.spackage.orderNumber,
                    'shippingPackageCode' : self.spackage.code,
                    'itemCodes' : self.spackage.itemCodes
                }
                $('#error').addClass('invisible');
                Uniware.Ajax.postJson("/data/oms/packer/invoice/print", JSON.stringify(req), function(response) {
                    if (response.successful) {
                        Uniware.LightBox.hide("#traceableItems");
                        if (!Uniware.Utils.isRepickItems) {
                            self.printInvoice();
                        } else {
                            var newItems = self.spackage.itemCodes;
                            for (var j=0;j<self.spackage.shippingPackageItems.length;j++) {
                                var shippingPackageItem = self.spackage.shippingPackageItems[j];
                                if (shippingPackageItem.itemCode) {
                                    newItems.remove(shippingPackageItem.itemCode);
                                }
                            }

                            for (var i=0;i<newItems.length;i++) {
                                var req = {
                                    comment : 'Order=' +  self.spackage.displayOrderNumber + ',action=Repick,Reason=' + newItems[i],
                                    referenceIdentifier : 'SO-' + self.spackage.orderNumber
                                }
                                Uniware.Ajax.postJson("/data/comment/add", JSON.stringify(req), function(response) {
                                    if (response.successful == true) {
                                        //Uniware.Utils.addNotification("Comment has been added");
                                    } else {
                                        Uniware.Utils.showError(response.errors[0].description);
                                    }
                                }, true);
                            }
                        }
                        self.searchShipment(self.spackage.code);

                    } else {
                        self.spackage.itemCodes = [];
                        $('#scannedItems').html('');
                        $('#error').removeClass('invisible').html(response.errors[0].description + ', rescan items.');
                    }
                });

            } else {
                $('#error').addClass('invisible');
                self.itemScanAndPrintInvoice();
            }

        }
    };
};

$(document).ready(function() {
    window.page = new Uniware.scanShipmentPage();
    window.page.init();
    if ($("#shipmentCode").val() != '') {
        var e = jQuery.Event( 'keyup', { which: 13 } );
        $('#shipmentCode').trigger(e);
    }
});
</script>
</tiles:putAttribute>
</tiles:insertDefinition>
