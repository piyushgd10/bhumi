<%@ include file="/tagIncludes.jsp"%>
<tiles:insertDefinition name=".inflowPage">
	<tiles:putAttribute name="title" value="Uniware - View Item Details" />
	<tiles:putAttribute name="rightPane">
		<div class="noprint">
			<form onsubmit="javascript : return false;">
				<div class="greybor headlable ovrhid main-box-head">
					<h2 class="edithead head-textfields">Scan Item</h2>
					<div class="lfloat">
						<input type="text" id="itemCode" size="20"  autocomplete="off">
					</div>
					<div id="searching" class="lfloat10 hidden" style="margin-top:5px;">
						<img src="/img/icons/refresh-animated.gif"/>
					</div>
				</div>
				<div class="clear"></div>
				<div id="itemDiv"></div>
			</form>
		</div>
		
	</tiles:putAttribute>
	
	<tiles:putAttribute name="deferredScript">
	<script id="itemTemplate" type="text/html">
		<div class="greybor round_bottom main-boform-cont pad-15-top overhid">
		<table width="100%" border="0" cellspacing="1" cellpadding="3" class="fields-table">
		<tr> 
        	<td>Item Number</td>
	        <td><#=obj.code#></td>
    	    <td>Product Code</td>
        	<td><#=obj.itemSKU#></td>    
		</tr>
		<tr> 
        	<td>Status</td>
	        <td class="bold"><#=obj.status#></td>
    	    <td>Item Name</td>
        	<td>
				<#=obj.itemTypeName#>
				<#=Uniware.Utils.getItemDetail(obj.itemTypeImageUrl, obj.itemTypePageUrl)#>
			</td>
		</tr>
		<tr> 
        	<td>GRN Number</td>
	        <td class="bold"><#=obj.inflowReceiptCode#></td>
    	    <td>Vendor SKU Code</td>
        	<td><#=obj.vendorSkuCode#></td>    
		</tr>
		</table>
		<# if (obj.status == 'CREATED') { #>
			<br/>
			<div id="discardItem" class=" btn btn-primary lfloat">Discard Barcode</div>
			<div class="clear"></div>
		<# } #>
		</div>
	</script>	
	<script type="text/javascript">
		Uniware.DiscardBarcodePage = function() {
			var self = this;	
			this.itemCode;
			
			this.init = function() {
				$('#itemCode').keyup(self.load);
			};
			
			this.load = function(event){
				if (event.which == 13 && $('#itemCode').val() != '') {
					$('#itemDiv').html("");
					self.searchItem($('#itemCode').val());
				}
			};
			
			this.searchItem = function(itemCode) {
				var req = {
					'itemCode': itemCode
				};
				Uniware.Ajax.postJson("/data/item/fetch", JSON.stringify(req), function(response) {
					if(response.successful == true) {
						self.itemCode = response.itemDTO.code;
						$('#itemDiv').html(template("itemTemplate", response.itemDTO));
						$('#itemCode').val('');
						$('#discardItem').click(self.discardItem);
						Uniware.Utils.barcode();
					} else {
						Uniware.Utils.showError(response.errors[0].description);
					}
				}); 	
			};
			
			this.discardItem = function() {
				var req = {
					'itemCode': self.itemCode
				};
				Uniware.Ajax.postJson("/data/inflow/discardBarcode", JSON.stringify(req), function(response) {
					if(response.successful == true) {
						self.searchItem(self.itemCode);
						Uniware.Utils.addNotification('Barcode has been discarded.');
						$('#itemCode').focus();
						
					} else {
						Uniware.Utils.showError(response.errors[0].description);
					}
				});
			};
			
		}
		
		$(document).ready(function() {
			window.page = new Uniware.DiscardBarcodePage();
			window.page.init();
		});
	</script>
	</tiles:putAttribute>
</tiles:insertDefinition>
