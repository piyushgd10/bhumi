<%@ include file="/tagIncludes.jsp"%>
<tiles:insertDefinition name=".inflowPage">
	<tiles:putAttribute name="title" value="Uniware - View GRN" />
	<tiles:putAttribute name="rightPane">
		<div class="noprint">
			<form onsubmit="javascript : return false;">
				<div class="greybor headlable ovrhid main-box-head">
					<h2 class="edithead head-textfields">Scan GRN</h2>
					<div class="lfloat">
						<input type="text" id="receiptCode" size="20"  autocomplete="off">
					</div>
					<div id="searching" class="lfloat10 hidden" style="margin-top:5px;">
						<img src="/img/icons/refresh-animated.gif"/>
					</div>
				</div>
			</form>
		</div>
		<div id="receiptDiv" style="margin: 0 10px;"></div>
		<div class="clear"></div>
	</tiles:putAttribute>
	
	<tiles:putAttribute name="deferredScript">
	
	<script id="inflowReceipt" type="text/html">
	<div class="greybor round_bottom main-boform-cont pad-15-top overhid">
		<table width="100%" border="0" cellspacing="1" cellpadding="3" class="fields-table">
		<tr> 
        	<td width="20%">Purchase Order</td>
	        <td width="20%"><#=obj.purchaseOrder.code#></td>
    	    <td width="20%"></td>
        	<td></td>    
		</tr>
		<tr> 
        	<td>Vendor Name</td>
	        <td><#=obj.purchaseOrder.vendorName#></td>
    	    <td></td>
        	<td></td>    
		</tr>
		<tr> 
	       	<td>Vendor Invoice Number</td>
    	   	<td><#=obj.vendorInvoiceNumber#></td>
		    <td>Vendor Invoice Date</td>
        	<td><#=new Date(obj.vendorInvoiceDate).toPaddedDate()#></td>    
		</tr>
		<tr> 
	       	<td class="f15">GRN Number</td>
    	   	<td class="f15"><div class="barcode" barWidth="1" barHeight="15"><#=obj.code#></div></td>
    	   	<td>Created at</td>
        	<td><#=(new Date(obj.created)).toDateTime()#></td>    
		</tr>
			<# if (obj.customFieldValues && obj.customFieldValues.length > 0) { #>
				<tr>
				<# for(var i=0;i<obj.customFieldValues.length;i++) { var customField = obj.customFieldValues[i]; #>
		 			<td><#=customField.displayName#></td>
					<td>
					<# if (obj.isAmending != true && (obj.statusCode == 'CREATED' || obj.statusCode == 'WAITING_FOR_APPROVAL')) { #>
						<# if (customField.valueType == 'text') { #>
							<input id="<#=customField.fieldName#>" type="text" class="w150 custom" value="<#=customField.fieldValue#>"/>
						<# } else if (customField.valueType == 'date') { #>
							<input id="<#=customField.fieldName#>" type="text" class="w150 custom datefield" value="<#= customField.fieldValue ? new Date(customField.fieldValue).toPaddedDate() : '' #>"/>
						<# } else if (customField.valueType == 'select') { #>
							<select id="<#=customField.fieldName#>" class="w150 custom">
							<# for (var j=0;j<customField.possibleValues.length;j++) { #>
								<option value="<#=customField.possibleValues[j]#>" <#= customField.possibleValues[j] == customField.fieldValue ? selected="selected" : '' #> ><#=customField.possibleValues[j]#></option>
							<# } #>
							</select>
						<# } else if (customField.valueType == 'checkbox') { #>
							<input id="<#=customField.fieldName#>" type="checkbox" class="custom" <#=customField.fieldValue ? checked="checked" : '' #>/>
						<# } #>
					<# } else { #>
						<# if (customField.valueType == 'date') { #>
							<#= customField.fieldValue ? new Date(customField.fieldValue).toPaddedDate() : '' #>
						<# } else if (customField.valueType == 'checkbox') { #>
							<input type="checkbox" disabled="disabled" <#=customField.fieldValue ? checked="checked" : '' #>/>
						<# } else { #>
							<#=customField.fieldValue#>
						<# } #>
					<# } #>
					</td>
					<# if (i == obj.customFieldValues.length - 1 && i % 2 == 0) { #>
						<td></td><td></td>
					<# } else if (i != 0 && i % 2 != 0) { #>
						</tr><tr>
					<# } #>
				<# } #>
				</tr>
            <# } #>
		<tr> 
	       	<td>GRN Status</td>
    	   	<td class="bold"><#=obj.statusCode#></td>
    	   	<td>Created By</td>
        	<td><#=obj.createdBy#></td>    
		</tr>
		<tr> 
		 	<td>Total Quantity</td>
    	   	<td class="bold"><#=obj.totalQuantity#></td>
	   		<td>Total Rejected Quantity</td>
		   	<td class="bold"><#=obj.totalRejectedQuantity#></td>    
		</tr>
		<tr> 
		 	<td>Total Received Amount</td>
    	   	<td class="bold"><#=obj.totalReceivedAmount#></td>
	   		<td>Total Rejected Amount</td>
		   	<td class="bold"><#=obj.totalRejectedAmount#></td>    
		</tr>
		<tr> 
		 	<td>Attachment:</td>
    	   	<td><span id="grnViewDocuments"></span></td>
	   		<td></td>
		   	<td></td>    
		</tr>
		<tr>
			<td colspan="4">
			<br/>
			<# if (obj.statusCode == 'CREATED' && obj.inflowReceiptItems.length == 0) { #>
				<div id="discardGRN" class=" btn btn-mini btn-danger lfloat discard">discard</div>
				<div class="clear"></div>
			<# } #>
			</td>
		</tr>
		</table>	
	</div>
	</script>

	<script id="inflowReceiptItems" type="text/html">
		<#=template("inflowReceipt",obj)#>
		<div class="greybor headlable round_top ovrhid mar-15-top">
			<h4 class="edithead">Items</h4>
		</div>
		<div class="greybor round_bottom form-edit-table-cont">
			<table class="uniTable" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<th>Item Type</th>
					<th>Product Code</th>
					<th>Batch Code</th>
					<th>Vendor SKU Code</th>
					<th>Unit Price</th>
					<th width="100">Received<br/>Quantity</th>
					<th width="100">Rejected<br/>Quantity</th>
					<th>Rejection<br/>Comments</th>
					<th width="100">Status</th>
				</tr>
				<# for(var i=0; i<obj.inflowReceiptItems.length; i++){ var irItem = obj.inflowReceiptItems[i]; #>
					<tr id='tr-<#=i#>'>
						<td>
							<#=irItem.itemTypeName#>
							<#=Uniware.Utils.getItemDetail(irItem.itemTypeImageUrl, irItem.itemTypePageUrl)#>
						</td>
						<td class="breakWord"><#=irItem.itemSKU#></td>
						<td class="breakWord"><#=irItem.batchCode#></td>
						<td class="breakWord"><#=irItem.vendorSkuCode#></td>
						<td><#=irItem.unitPrice#></td>
						<td><#=irItem.quantity#></td>
						<td><#=irItem.rejectedQuantity#></td>
						<td><#=irItem.rejectionComments#></td>
						<td><#=irItem.status#></td>
					</tr>
				<# } #>
			</table>	
		</div>
		<div id="grnViewComments" class="round_all"></div>
	</script>
	
	<script type="text/javascript">
		<sec:authentication property="principal" var="user" />
		Uniware.receivePOPage = function() {
			var self = this;	
			this.inflowReceiptCode = null;
			this.facilityCode = '${cache.getCache('facilityCache').getCurrentFacility().getCode()}';
			
			this.init = function() {
				$('#receiptCode').keyup(self.load);
				var inflowReceiptCode = '${param['inflowReceiptCode']}';
				self.inflowReceiptCode = inflowReceiptCode;
				if(inflowReceiptCode != "") {
					self.searchReceipt(inflowReceiptCode);
				}
			};
			
			this.load = function(event){
				if (event.which == 13) {
					$('#receiptDiv').html("");
					self.searchReceipt($('#receiptCode').val());
				}
			};
			
			this.searchReceipt = function(receiptCode){
				var req={
					inflowReceiptCode: receiptCode
				};
				Uniware.Ajax.postJson("/data/inflow/receipt/fetch", JSON.stringify(req), function(response) {
					if(response.successful == true){
						self.inflowReceiptCode = receiptCode;
						$('#receiptDiv').html(template("inflowReceiptItems", response.inflowReceipt));
						$('div#discardGRN').click(self.discardGRN);
						$('#receiptCode').val('');
						Uniware.Utils.barcode();
						Uniware.Utils.applyHover();
						Uniware.Utils.renderComments("#grnViewComments", 'PO-' + response.inflowReceipt.purchaseOrder.code + '-' + self.facilityCode);
						Uniware.Documents("#grnViewDocuments", 'IR-' + response.inflowReceipt.code, '${user.username}', 2);
					}else{
						Uniware.Utils.showError(response.errors[0].description);
					}
				}); 	
			};
			
			this.discardGRN = function() {
				var requestObject = {
					'inflowReceiptCode' : self.inflowReceiptCode
				};
				Uniware.Ajax.postJson("/data/inflow/receipt/discard", JSON.stringify(requestObject), function(response) {
					if (response.successful == false) {
						Uniware.Utils.showError(response.errors[0].description);
					} else {
						Uniware.Utils.addNotification('GRN "'+ self.inflowReceiptCode + '" has been discarded');
						window.location.reload();
					}
				}, true);
			};
		}

					
		$(document).ready(function() {
			window.page = new Uniware.receivePOPage();
			window.page.init();
		});
	</script>
	</tiles:putAttribute>
</tiles:insertDefinition>
