<%@ include file="/tagIncludes.jsp"%>
<tiles:insertDefinition name=".inflowPage">
	<tiles:putAttribute name="title" value="Uniware - Search Receipt" />
	<tiles:putAttribute name="rightPane">
		<div>
			<form onsubmit="javascript : return false;">
				<div class="greybor headlable ovrhid main-box-head">
					<h2 class="edithead head-textfields">Search GRN</h2>
				</div>
				<div class="round_bottom ovrhid pad-15">
					<div class="lfloat20">
						<div class="searchLabel">Vendor Name Contains</div>
						<input type="text" id="vendorName"  autocomplete="off" />
					</div>
					<div class="lfloat20">
						<div class="searchLabel">Item Type Name/Code Contains</div>
						<input type="text" id="itemTypeName" size="30" autocomplete="off" />
					</div>
					<div class="lfloat20">
						<div class="searchLabel">GRN No.</div>
						<input type="text" id="inflowReceiptCode"  autocomplete="off" />
					</div>
					<div class="lfloat20">
						<div class="searchLabel">Vendor Invoice</div>
						<input type="text" id="vendorInvoiceNumber"  autocomplete="off" />
					</div>
					<div class="clear"></div>
					<div class="lfloat20">
						<div class="searchLabel">Purchase Order No.</div>
						<input type="text" id="purchaseOrderCode" size="20"  autocomplete="off" />
					</div>
					
					
					<div class="lfloat20">
						<div class="searchLabel">Created Date</div>
						<input type="text" id="dateRange" size="20" class="w200 datefield pull-right" autocomplete="off" />
					</div>
					<div class="lfloat20">
						<div class="searchLabel">Status</div>
						<select id="receiptStatus" style="width:150px;">
						<option value="">--ALL--</option>
						<c:forEach items="${cache.getCache('statuscache').inflowReceiptStatuses}" var="status">
							<option value="${status}">${status}</option>
						</c:forEach>
						</select>
					</div>
					<div class="lfloat20" style="margin-top:20px;">	
						<input id="search" value="search" type="submit" class=" btn btn-primary" />
					</div>
					<div id="searching" class="lfloat10 hidden" style="margin-top:25px;">
						<img src="/img/icons/refresh-animated.gif"/>
					</div>
				</div>
			</form>
		</div>
		<table id="dataTable" class="dataTable"></table>
	</tiles:putAttribute>
	<tiles:putAttribute name="deferredScript">
	<script type="text/javascript" src="${path.js('jquery/jquery.dataTables.min.js')}"></script>
	<script type="text/javascript">
		Uniware.SearchReceiptPage = function() {
			var self = this;
			this.table = null;
			
			this.init = function() {
				$('#dateRange').daterangepicker();
				$("#search").click(self.search);
			};
							
			this.search = function() {
				var pipeline = new Uniware.pipelineData();
				$("#searching").removeClass('hidden');
				var requestObject = {
					vendorName : $('#vendorName').val(),
					inflowReceiptCode : $('#inflowReceiptCode').val(),
					invoiceNumber : $('#vendorInvoiceNumber').val(),
					purchaseOrderCode: $('#purchaseOrderCode').val(),
					itemTypeName: $('#itemTypeName').val(),
					statusCode: $('#receiptStatus').val(),
				}
				
				var dateRange = $("#dateRange").val();
				if (dateRange != '') {
					requestObject.fromDate = Date.fromPaddedDate(dateRange.substring(0, 10)).getTime();
					if (dateRange.length > 10) {
						requestObject.toDate = Date.fromPaddedDate(dateRange.substring(13));
					} else {
						requestObject.toDate = Date.fromPaddedDate(dateRange.substring(0, 10));
					}
					requestObject.toDate = requestObject.toDate.setDate(requestObject.toDate.getDate()+1);
				}
				
				pipeline.requestObject = requestObject;
				
				var dtEL = $('#dataTable');
				if (Uniware.Utils.isDataTable(dtEL)) {
					var dtTable = dtEL.dataTable();
					dtTable.fnDestroy();
	                dtTable = undefined;
				}
				
				self.table = dtEL.dataTable({
					"bServerSide": true,
					"sAjaxSource": "/data/inflow/receipt/search",
					"bAutoWidth" : false,
					"bSort" : false,
					"bFilter": false,
					"aoColumns" : [ {
						"sTitle" : "GRN Number",
						"mDataProp": function (aData) {
							return '<a class="link action" href="/inflow/grn/view?legacy=1&inflowReceiptCode='+ aData.code +'">' + aData.code + '</a>';
						}
					},{
						"sTitle" : "Vendor Invoice",
						"mDataProp": function (aData) {
							return aData.vendorInvoiceNumber;
						}
					}, {
						"sTitle" : "Purchase Order",
						"mDataProp": function (aData) {
							return '<a class="link action" href="/inflow/grn/create?legacy=1&po='+ aData.purchaseOrderCode +'">' + aData.purchaseOrderCode + '</a>';
						}
					}, {
						"sTitle" : "Vendor Name",
						"mDataProp": function (aData) {
							return aData.vendorName;
						}
					}, {
						"sTitle" : "Status Code",
						"mDataProp" : "statusCode"
					}, {
						"sTitle" : "Created At",
						"mDataProp": function (aData) {
							return (new Date(aData.created)).toDateTime();
						}
					}],
					
					"fnServerData": pipeline.fnDataTablesPipeline
				});
				$("#searching").addClass('hidden');
			};
		};
						
		$(document).ready(function() {
			window.page = new Uniware.SearchReceiptPage();
			window.page.init();
		});
	</script>
	</tiles:putAttribute>
</tiles:insertDefinition>
