<%@ include file="/tagIncludes.jsp"%>
<tiles:insertDefinition name=".inflowPage">
	<tiles:putAttribute name="title" value="Uniware - Adjust Inventory" />
	<tiles:putAttribute name="rightPane">
		<div class="greybor headlable ovrhid main-box-head">
			<h2 class="edithead head-textfields">Adjust Inventory</h2>
		</div>
		<div class="greybor round_bottom main-boform-cont pad-15-top ovrhid">
			<form onsubmit="javascript:return false;">
				<select id="action">
					<option value="">Select an Action</option>
					<option value="TRANSFER">Transfer</option>
				</select>
				<div class="clear"></div>
				<br/>
				<div id="itemTypeInventoryDiv">
				<script id="itemTypeInventory" type="text/html">
				<div class="searchLabel">Item Type</div>
				<input id="itemType" skuCode="" class="textfield" type="text">
				
				<div class="clear"></div>
				<br/>
				<div class="searchLabel">Quantity</div>
				<input id="quantity" class="textfield" type="text">
				
				<div class="clear"></div>
				<br/>
				<div class="searchLabel">Shelf Code</div>
				<input id="shelfCode" class="textfield" type="text">
				<# if (obj.type == "TRANSFER") { #>
					<div class="clear"></div>
					<br/>
					<div class="searchLabel">To Shelf Code</div>
					<input id="toShelfCode" class="textfield" type="text">
				<# } #>	
				<div class="clear"></div>
				<br/><br/>
				<div class="formRight lfloat">
					<input type="submit" class=" btn btn-success" id="submit" value="submit"/>
				</div>
			</script>
				</div>
			</form>
		</div>	

	<tiles:putAttribute name="deferredScript">
		<script type="text/javascript">
		Uniware.AdjustInventory = function(){
			var self = this;
			this.init = function(){
				$('#action').change(function(){
					var action = $(this).val();
					if(action != ""){
						$("#itemTypeInventoryDiv").html(template("itemTypeInventory", {type: action}));
						self.searchItemType();
						self.submit();
					}else{
						$("#itemTypeInventoryDiv").html("");
					}
				})
			}
			
			this.searchItemType = function(){
			    $("#itemType").autocomplete({
			    	minLength: 2,	
			    	mustMatch : true,
					autoFocus: true,
					source: function( request, response ) {
						var requestObject = {
							keyword : $('#itemType').val()
						}
						Uniware.Ajax.postJson("/data/procure/catalog/search", JSON.stringify(requestObject), function(data) {
							response( $.map( data.elements, function( item ) {
								return {
									label: item.name + " - " + item.skuCode,
									value: item.name,
									code: item.skuCode
								}
							}));
						});
					},
					select: function( event, ui ) {
						$('#itemType').attr("skuCode",ui.item.code);
					}
			    });
			}
			
			this.submit = function(){
				$('#submit').click(function(){
					var requestObject = {
							itemSKU : $('#itemType').attr("skuCode"),
							quantity: $('#quantity').val(),
							shelfCode: $('#shelfCode').val(),
							transferToShelfCode: $('#toShelfCode').val(),
							adjustmentType: $("#action").val()
					}
					
					Uniware.Ajax.postJson('/data/inflow/inventory/adjust', JSON.stringify(requestObject), function(response) {
						if (response.successful == false) {
							Uniware.Utils.showError(response.errors[0].description);
						} else {
							Uniware.Utils.addNotification('Inventory has been adjusted');
							$('#action').change();
							self.init();
						}
					});	
				});
			}
		}
		
		$(document).ready(function() {
			window.page = new Uniware.AdjustInventory();
			window.page.init();
		});
		</script>
	</tiles:putAttribute>		
	</tiles:putAttribute>
</tiles:insertDefinition>
