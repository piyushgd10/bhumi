<%@ include file="/tagIncludes.jsp"%>
<tiles:insertDefinition name=".wapPage">
	<tiles:putAttribute name="title" value="Uniware - Create GRN" />
	<tiles:putAttribute name="body">
		<br />
		<div class="greybor headlable ovrhid main-box-head">
			<h2 class="edithead head-textfields">Inbound > Scan GRN</h2>
		</div>
		<div id="grnDiv" class="greybor round_bottom main-boform-cont ovrhid" style="padding: 10px;"></div>
	</tiles:putAttribute>
	<tiles:putAttribute name="deferredScript">
	<script id="grnTemplate" type="text/html">
	<div id="error" class="hidden errorField"></div>
	<table>
		<tr>
			<td width="60">GRN No: </td>
			<td class="ucase"><#= obj.inflowReceiptCode ? obj.inflowReceiptCode : '<input type="text" id="inflowReceiptCode" class="w150"/>' #></td>
		</tr>
		<# if (obj.inflowReceiptCode) { #>
		<tr>
			<td>Item : </td>
			<td><input type="text" id="itemCode" class="w150"/></td>
		</tr>
		<tr>
			<td></td>
			<td><div id="rejectItem" class=" btn lfloat" >Reject Item</div></td>
		</tr>
		<# } #>
	</table>
	</script>
		<script type="text/javascript">
		Uniware.WapCreateGRN = function() {
			var self = this;
			this.inflowReceiptCode;
			
			this.init = function() {
				self.render();
				$('#inflowReceiptCode').focus().keyup(self.load);
			};
			
			this.load = function(event) {
				if (event.which == 13 && $('#inflowReceiptCode').val() != '') {
					self.inflowReceiptCode = $('#inflowReceiptCode').val();
					self.render();
					$('#itemCode').focus();
				}
			};

			this.rejectItem = function(event) {
				$('#error').addClass('hidden');
				var itemCode = $('#itemCode').val();
				var req = {
					'inflowReceiptCode' : self.inflowReceiptCode,
					'itemCode' : itemCode,
				}
				Uniware.Ajax.postJson('/data/inflow/item/reject', JSON.stringify(req), function(response){
					if (response.successful) {
						$('#itemCode').val('');
					} else {
						$('#error').html(response.errors[0].description).removeClass('hidden');
						$('#itemCode').val('');
					}
				});
			};
			
			this.render = function() {
				$('#grnDiv').html(template('grnTemplate', self));
				$('#rejectItem').click(self.rejectItem);
			};
			
		}
		
		$(document).ready(function() {
			window.page = new Uniware.WapCreateGRN();
			window.page.init();
		});
	</script>
	</tiles:putAttribute>
</tiles:insertDefinition>
