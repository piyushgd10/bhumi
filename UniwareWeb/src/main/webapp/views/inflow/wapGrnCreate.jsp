<%@ include file="/tagIncludes.jsp"%>
<tiles:insertDefinition name=".wapPage">
	<tiles:putAttribute name="title" value="Uniware - Create GRN" />
	<tiles:putAttribute name="body">
		<br />
		<div class="greybor headlable ovrhid main-box-head">
			<h2 class="edithead head-textfields">Inbound > Scan GRN</h2>
		</div>
		<div id="grnDiv" class="greybor round_bottom main-boform-cont ovrhid" style="padding: 10px;"></div>
	</tiles:putAttribute>
	<tiles:putAttribute name="deferredScript">
	<script id="grnTemplate" type="text/html">
	<div id="error" class="hidden errorField"></div>
	<table>
		<tr>
			<td width="80">GRN No: </td>
			<td><#= obj.code ? obj.code : '<input type="text" id="inflowReceiptCode" class="w150 ucase"/>' #></td>
		</tr>
		<# if (obj.code) { #>
		<tr>
			<td>Product : </td>
			<td>
				<select id="itemSKU">
					<option value="">-- select item type --</option>
					<#for(var key in obj.itemTypes) { #>
						<option <#= key==obj.selectedItemSKU ? selected='selected' : ''#> value="<#=key#>"><#=key#> - <#=obj.itemTypes[key].itemName#></option>
					<# } #>
				</select>
			</td>
		</tr>
		<# if(obj.selectedItemSKU) { #>
		<tr>
			<td>Item Name: </td>
			<td><#=obj.itemTypes[obj.selectedItemSKU].itemName#></td>
		</tr>
		<tr>
			<td>Vendor SKU: </td>
			<td><#=obj.itemTypes[obj.selectedItemSKU].vendorSKU#></td>
		</tr>
		<tr>
			<td>Color: </td>
			<td><#=obj.itemTypes[obj.selectedItemSKU].color#></td>
		</tr>
		<tr>
			<td>Size: </td>
			<td><#=obj.itemTypes[obj.selectedItemSKU].size#></td>
		</tr>
		<tr>
			<td>Brand: </td>
			<td><#=obj.itemTypes[obj.selectedItemSKU].brand#></td>
		</tr>
		<# for(var key in obj.itemTypeCustomFields) {#>
			<tr>
				<td><#=key#>: </td>
				<td><#=obj.itemTypeCustomFields[key]#></td>
			</tr>		
		<# } #>

		<tr>
			<td>Item : </td>
			<td><input type="text" id="itemCode" class="w150"/></td>
		</tr>
		<# } #>

		<# } #>
	</table>
	<# if (obj.code && obj.selectedItemSKU) { #>
		</br>
		<div id="addItem" class=" btn lfloat" >Add Item</div>
	<# } #>
	</script>
		<script type="text/javascript">
		Uniware.WapCreateGRN = function() {
			var self = this;
			this.skuCodes = [];
			this.itemTypes = {};
			this.itemTypeCustomFields = {};
			this.inflowReceipt;
			
			this.init = function() {
				self.render();
				$('#inflowReceiptCode').focus().keyup(self.load);
			};
			
			this.load = function(event) {
				if (event.which == 13 && $('#inflowReceiptCode').val() != '') {
					self.fetchGRN($('#inflowReceiptCode').val());
				}
			};
			
			this.fetchGRN = function(code) {
				var req = {
					'inflowReceiptCode': code
				};
				Uniware.Ajax.postJson("/data/inflow/receipt/fetch", JSON.stringify(req), function(response) {
					if(response.successful == true) {
						self.inflowReceipt = response.inflowReceipt;
						self.fetchPO(response.inflowReceipt.purchaseOrder.code);
					} else {
						$('#error').html(response.errors[0].description).removeClass('hidden');
						$('#inflowReceiptCode').val('');
					}
				});
			};
			
			this.fetchPO = function(poCode){
				Uniware.Ajax.postJson("/data/inflow/po/fetch", JSON.stringify({'purchaseOrderCode': poCode}), function(response) {
					self.skuCodes = [];
					for (var i=0;i<response.purchaseOrderItems.length;i++) {
						var item = response.purchaseOrderItems[i];
						if (item.pendingQuantity > 0) {
							var poItem = response.purchaseOrderItems[i];
							self.skuCodes.push(poItem.itemSKU);
							self.itemTypes[poItem.itemSKU] = {};
							self.itemTypes[poItem.itemSKU].itemName = poItem.itemTypeName;
							self.itemTypes[poItem.itemSKU].vendorSKU = poItem.vendorSkuCode;
							self.itemTypes[poItem.itemSKU].color = poItem.color;
							self.itemTypes[poItem.itemSKU].size = poItem.size;
							self.itemTypes[poItem.itemSKU].brand = poItem.brand;
							self.itemTypeCustomFields[poItem.itemSKU] = poItem.itemTypeCustomFields; 
						}
					}
					self.inflowReceipt.itemTypes = self.itemTypes;
					self.render();
				});
			};
			
			this.loadCustomFields = function(){
				var itemSKU = $(this).val();
				if(itemSKU != ""){
					self.inflowReceipt.itemTypeCustomFields = self.itemTypeCustomFields[itemSKU];
				}else{
					self.inflowReceipt.itemTypeCustomFields = {};
				}
				self.inflowReceipt.selectedItemSKU = itemSKU;
				self.render();
			};

			this.addItem = function(event) {
				$('#error').addClass('hidden');
				var itemCode = $('#itemCode').val();
				var itemSKU = $('#itemSKU').val();
				var req = {
					'inflowReceiptCode' : self.inflowReceipt.code,
					'itemCode' : itemCode,
					'itemSKU' : itemSKU
				}
				Uniware.Ajax.postJson('/data/inflow/wap/item/add', JSON.stringify(req), function(response){
					if (response.successful) {
						$('#error').html("Item has been added successfully.").removeClass('hidden');
						$('#itemCode').val('');
					} else {
						$('#error').html(response.errors[0].description).removeClass('hidden');
						$('#itemCode').val('');
					}
				});
			};
			
			this.render = function() {
				$('#grnDiv').html(template('grnTemplate', self.inflowReceipt));
				$('#itemSKU').change(self.loadCustomFields);
				$('#addItem').click(self.addItem);
			};
			
		}
		
		$(document).ready(function() {
			window.page = new Uniware.WapCreateGRN();
			window.page.init();
		});
	</script>
	</tiles:putAttribute>
</tiles:insertDefinition>
