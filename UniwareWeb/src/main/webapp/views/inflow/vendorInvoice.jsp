<%@ include file="/tagIncludes.jsp"%>
<tiles:insertDefinition name=".procurePage">
	<tiles:putAttribute name="title" value="Uniware - VendorInvoices" />
	<tiles:putAttribute name="rightPane">
		<div id="pageBar">
			<div class="pageHeading" id="vendorInvoiceCreateLink"><span class="mainHeading">Vendor Invoices<span class="pipe">/</span></span><span class="subHeading"></span></div>
			<div class="pageControls" id="pageViews"></div>
				<div id="createDebitNote" class="btn btn-small btn-primary rfloat" style="margin: 10px 5px;">create debit note</div>
				<div id="createVendorInvoice" class="btn btn-small btn-primary rfloat" style="margin: 10px 5px;">add vendor invoice</div>
			<div class="clear"></div> 
		</div>
		<div id="tableContainer">
			<table id="flexme1" style="display: none"></table>
		</div>
		<div id="createVendorInvoiceDiv"></div>
		
		<div id="vendorInvoiceDetails" style="margin:10px;">
		<script type="text/html" id="vendorInvoiceTemplate">
		<table width="100%" border="0" cellspacing="1" cellpadding="3" class="fields-table">
		<tr> 
        	<td colspan="4">
				<a href="/inflow/vendorInvoice/print/<#=obj.code#>?legacy=1" target="_blank">
					<div id="vendorInvoice-<#=obj.code#>" class="rfloat btn btn-small printgp" style="margin-right: 10px;margin-top: 8px;">print</div>	
				</a>
				<#if (Uniware.Utils.editVendorInvoiceMetadata) { #>
				    <div id="editVendorInvoiceCustomFields" class="btn btn-small rfloat20" style="margin-top: 8px;">edit <img src="/img/icons/edit.png"/></div>
				<# } #>
			</td>
		</tr>
		<tr> 
        	<td>Vendor Invoice</td>
	        <td class="bold f15"><div class="barcode"><#=obj.code#></div></td>
    	    <td>Created by</td>
        	<td class="bold f15"><#=obj.createdBy#></td>
		</tr>
		<tr> 
        	<td>Status</td>
	        <td><#=obj.statusCode#></td>
    	    <td>Created at</td>
        	<td><#=new Date(obj.created).toDateTime()#></td>    
		</tr>
		<tr> 
        	<td>Type</td>
	        <td class="bold f15"><#=obj.type#></td>
    	    <td>Purchase Order</td>
        	<td><a href="/procure/poItems?legacy=1&poCode=<#=obj.purchaseOrderCode#>"><#=obj.purchaseOrderCode#></a></td>
		</tr>
		<tr>
			<# if(window.page.isCustomFieldEditing) { #>
				<td>Vendor Invoice no.</td>
	        	<td class="bold f15"><input id="vendorInvNumber" type="text" size="15" value="<#=obj.vendorInvoiceNumber#>"></td>
    	    	<td>Vendor Invoice Date</td>
        		<td><input id="vendorInvDate" type="text" size="15" class="datefield customDate" value="<#=obj.vendorInvoiceDate#>"/></td>
			<# } else { #>
				<td>Vendor Invoice no.</td>
	        	<td class="bold f15"><input id="vendorInvNumber" type="text" size="15" value="<#=obj.vendorInvoiceNumber#>" readonly="true" /></td>
    	    	<td>Vendr Invoice Date</td>
        		<td><input id="vendorInvDate" type="text" size="15" value="<#=obj.vendorInvoiceDate#>" readonly="true"/></td>
			<# } #>   
		</tr>
		<tr> 
        	<td>Vendor Code</td>
	        <td><#=obj.vendorCode#></td>
    	    <td>Vendor Name</td>
        	<td><#=obj.vendorName#></td>    
		</tr>
        <# if (obj.type == 'CREDIT') { #>
            <tr>
                <td>Debit Invoice No</td>
                <td class="bold f15">
                    <# for(var i=0; i < obj.debitInvoiceCodes.length; i++) { #>
                        <#=obj.debitInvoiceCodes[i]#>
                    <# } #>
                </td>
                <td>GRNs Added</td>
                <td class="bold f15">
                    <# for(var i=0; i < obj.inflowReceipts.length; i++){ #>
                        <#=obj.inflowReceipts[i]#>
                    <# } #>
                </td>
            </tr>
        <# } #>
        <# if (obj.type == 'DEBIT') { #>
            <tr>
                <td>Credit Invoice No</td>
                <td class="bold f15"><#=obj.creditInvoiceCode#></td>
                <td></td>
                <td></td>
            </tr>
        <# } #>
		<tr>
		<# var totalPrice = 0; #>
		<# var totalQuantity = 0; #>
		<# for(var i=0; i<obj.vendorInvoiceItems.length; i++){ var vendorInvoiceItem = vendorInvoiceItems[i]; #>
			<# totalPrice = totalPrice + vendorInvoiceItem.total; #>
			<# totalQuantity = totalQuantity + vendorInvoiceItem.quantity; #>
		<# } #> 
        	<td>Total Quantity</td>
	        <td><#=totalQuantity#></td>
    	    <td>Total Price</td>
        	<td><#=totalPrice#></td>    
		</tr>
		<# if(window.page.isCustomFieldEditing) { #>
           <# if (obj.customFieldValues && obj.customFieldValues.length > 0) { #>
			<tr>
			<# for(var i=0;i<obj.customFieldValues.length;i++) { var customField = obj.customFieldValues[i]; #>
	 			<td><#=customField.displayName#></td>
				<td>
					<# if (customField.valueType == 'date') { #>
						<input id="<#=customField.fieldName#>" type="text" size="15" class="custom datefield customFieldDate" value="<#= customField.fieldValue ? new Date(customField.fieldValue).toPaddedDate() : '' #>"/>
					<# } else if (customField.valueType == 'checkbox') { #>
						<input id="<#=customField.fieldName#>" type="checkbox" class="custom" <#=customField.fieldValue ? checked="checked" : '' #>/>
					<# } else if (customField.valueType == 'text') { #>
						<input id="<#=customField.fieldName#>" type="text" size="15" class="custom" value="<#=customField.fieldValue#>"/>
					<# } else if (customField.valueType == 'select') { #>
						<select id="<#=customField.fieldName#>" class="w150 custom">
							<# for (var j=0;j<customField.possibleValues.length;j++) { #>
								<option value="<#=customField.possibleValues[j]#>" <#= customField.possibleValues[j] == customField.fieldValue ? selected="selected" : '' #> ><#=customField.possibleValues[j]#></option>
							<# } #>
						</select>
					<# } #>
				</td>
				<# if (i == obj.customFieldValues.length- 1 && i % 2 == 0) { #>
					<td></td><td></td>
				<# } else if (i != 0 && i % 2 != 0) { #>
					</tr><tr>
				<# } #>
			<# } #>
			</tr>				
     	   <# } #>
			<tr>
				<td colspan="4">
					<div id="saveDetails" class="btn btn-primary lfloat20" >Save</div>
		   			<div id="discardDetails" class="link lfloat20" style="margin-top: 5px;">Discard Changes</div>			
				</td>
			</tr>
		<#} else {#>
			<# if (obj.customFieldValues && obj.customFieldValues.length > 0) { #>
			<tr>
			<# for(var i=0;i<obj.customFieldValues.length;i++) { var customField = obj.customFieldValues[i]; #>
	 			<td><#=customField.displayName#></td>
				<td>
					<# if (customField.valueType == 'date') { #>
						<#= customField.fieldValue ? new Date(customField.fieldValue).toPaddedDate() : '' #>
					<# } else if (customField.valueType == 'checkbox') { #>
						<input type="checkbox" disabled="disabled" <#=customField.fieldValue ? checked="checked" : '' #>/>
					<# } else { #>
						<#=customField.fieldValue#>
					<# } #>
				</td>
				<# if (i == obj.customFieldValues.length- 1 && i % 2 == 0) { #>
					<td></td><td></td>
				<# } else if (i != 0 && i % 2 != 0) { #>
					</tr><tr>
				<# } #>
			<# } #>
			</tr>				
     	   <# } #>
		<# } #>
		</table>
		<# if (obj.statusCode == 'CREATED' && obj.type == 'CREDIT') { #>
			<br/>
			<div class="pageLabel">
				<div class="lfloat">Add GRN <input type="text" id="grnCode"/></div>
				<div class="clear" />
				<br/>
			</div>
		<# } #>
		<# if (obj.statusCode == 'CREATED' && obj.type == 'DEBIT') { #>
			<br/>
			<div class="pageLabel">
				<div class="lfloat">Add Gatepass <input type="text" id="gatePass"/></div>
				<div class="clear" />
				<br/>
			</div>
		<# } #>

		<div class="greybor headlable round_top ovrhid">
			<h4 class="edithead">Vendor Invoice Items</h4>
		</div>
		<div class="greybor round_bottom form-edit-table-cont">
			<table class="uniTable" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<th width="80">SL. No.</th>
					<th>Item SKU</th>
					<th>Item Name</th>
					<th>Description</th>
					<th width="7%">Quantity</th>
					<th width="7%">
                        Unit Price<br/>
                        <# if (!Uniware.Utils.priceExclusive) { #>
                            (With Taxes)
                        <# } #>
                    </th>
					<th width="7%">Discount</th>
					<th width="7%">CST/Vat Percentage</th>
					<th width="7%">Central Gst Percentage</th>
					<th width="7%">State Gst Percentage</th>
					<th width="7%">UT Gst Percentage</th>
					<th width="7%">Integrated Gst Percentage</th>
					<th width="7%">Compensation Cess Percentage</th>
					<th width="7%">Subtotal</th>
					<th width="7%">Total</th>
					<th>Action</th>
				</tr>
				<# for(var i=0; i<obj.vendorInvoiceItems.length; i++){ var vendorInvoiceItem = vendorInvoiceItems[i]; #>
					<tr id='tr-<#=i#>'>
						<td><#=(i+1)#></td>
						<td><#=vendorInvoiceItem.itemTypeSkuCode#></td>
						<td><#=vendorInvoiceItem.itemTypeName#></td>
						<td>
							<# if (vendorInvoiceItem.isEditing && vendorInvoiceItem.itemTypeSkuCode == null) { #>
								<input id="description-<#=i#>" type="text" class="field-width" value="<#=vendorInvoiceItem.description#>"/>
							<# } else if(obj.statusCode == 'CREATED' && vendorInvoiceItem.itemTypeSkuCode == null){ #>
								<span class="table-editable"><#=vendorInvoiceItem.description#></span>
							<# } else { #>
								<#=vendorInvoiceItem.description#>
							<# } #>
						</td>
						<td>
							<# if (vendorInvoiceItem.isEditing) { #>
								<input id="qty-<#=i#>" type="text" class="field-width" value="<#=vendorInvoiceItem.quantity#>"/>
							<# } else if(obj.statusCode == 'CREATED'){ #>
								<span class="table-editable"><#=vendorInvoiceItem.quantity#></span>
							<# } else { #>
								<#=vendorInvoiceItem.quantity#>
							<# } #>
						</td>
						<td>
							<# if (vendorInvoiceItem.isEditing) { #>
								<input id="unitPrice-<#=i#>" type="text" class="field-width" value="<#=vendorInvoiceItem.unitPrice#>"/>
							<# } else if(obj.statusCode == 'CREATED'){ #>
								<span class="table-editable"><#=vendorInvoiceItem.unitPrice#></span>
							<# } else { #>
								<#=vendorInvoiceItem.unitPrice#>
							<# } #>
						</td>
						<td>
							<# if (vendorInvoiceItem.isEditing) { #>
								<input id="discount-<#=i#>" type="text" class="field-width" value="<#=vendorInvoiceItem.discount#>"/>
							<# } else if(obj.statusCode == 'CREATED'){ #>
								<span class="table-editable"><#=vendorInvoiceItem.discount#></span>
							<# } else { #>
								<#=vendorInvoiceItem.discount#>
							<# } #>
						</td>
						<td>
							<# if (vendorInvoiceItem.isEditing) { #>
								<input id="taxPercentage-<#=i#>" type="text" class="field-width" value="<#=vendorInvoiceItem.taxPercentage#>"/>
							<# } else if(obj.statusCode == 'CREATED'){ #>
								<span class="table-editable"><#=vendorInvoiceItem.taxPercentage#></span>
							<# } else { #>
								<#=vendorInvoiceItem.taxPercentage#>
							<# } #>
						</td>
						<td>
							<# if (vendorInvoiceItem.isEditing) { #>
								<input id="centralGstPercentage-<#=i#>" type="text" class="field-width" value="<#=vendorInvoiceItem.centralGstPercentage#>"/>
							<# } else if(obj.statusCode == 'CREATED'){ #>
								<span class="table-editable"><#=vendorInvoiceItem.centralGstPercentage#></span>
							<# } else { #>
								<#=vendorInvoiceItem.centralGstPercentage#>
							<# } #>
						</td>
						<td>
							<# if (vendorInvoiceItem.isEditing) { #>
								<input id="stateGstPercentage-<#=i#>" type="text" class="field-width" value="<#=vendorInvoiceItem.stateGstPercentage#>"/>
							<# } else if(obj.statusCode == 'CREATED'){ #>
								<span class="table-editable"><#=vendorInvoiceItem.stateGstPercentage#></span>
							<# } else { #>
								<#=vendorInvoiceItem.stateGstPercentage#>
							<# } #>
						</td>
						<td>
							<# if (vendorInvoiceItem.isEditing) { #>
								<input id="unionTerritoryGstPercentage-<#=i#>" type="text" class="field-width"
									   value="<#=vendorInvoiceItem.unionTerritoryGstPercentage#>"/>
								<# } else if(obj.statusCode == 'CREATED'){ #>
									<span class="table-editable"><#=vendorInvoiceItem.unionTerritoryGstPercentage#></span>
									<# } else { #>
										<#=vendorInvoiceItem.unionTerritoryGstPercentage#>
											<# } #>
						</td>
						<td>
							<# if (vendorInvoiceItem.isEditing) { #>
								<input id="integratedGstPercentage-<#=i#>" type="text" class="field-width"
									   value="<#=vendorInvoiceItem.integratedGstPercentage#>"/>
								<# } else if(obj.statusCode == 'CREATED'){ #>
									<span class="table-editable"><#=vendorInvoiceItem.integratedGstPercentage#></span>
									<# } else { #>
										<#=vendorInvoiceItem.integratedGstPercentage#>
											<# } #>
						</td>
						<td>
							<# if (vendorInvoiceItem.isEditing) { #>
								<input id="compensationCessPercentage-<#=i#>" type="text" class="field-width"
									   value="<#=vendorInvoiceItem.compensationCessPercentage#>"/>
								<# } else if(obj.statusCode == 'CREATED'){ #>
									<span class="table-editable"><#=vendorInvoiceItem.compensationCessPercentage#></span>
									<# } else { #>
										<#=vendorInvoiceItem.compensationCessPercentage#>
											<# } #>
						</td>
						<td><#=vendorInvoiceItem.subtotal#></td>
						<td><#=vendorInvoiceItem.total#></td>
						<td>
							<# if (vendorInvoiceItem.isEditing) { #>
								<input type="submit" class="btn btn-small update lfloat10" value="update" id="update-<#=i#>"></input>
								<span class="link cancel" id="cancel-<#=i#>" style="margin-top:3px;">cancel</span>
							<# } else if (obj.statusCode == 'CREATED') { #>
								<div id="remove-<#=i#>" class="btn btn-small btn-danger removeItem">delete</div>
							<# } #>
						</td>
					</tr>
				<# } #>
				<# if (obj.statusCode == 'CREATED') { #>
					<tr>
						<td><#=(i+1)#></td>
						<td id="newSku"></td>
						<td></td>
						<td><input id="newDescription" type="text" value="" class="itemType field-width"></input></td>
						<td><input id="newQty" type="text" value="" class="itemType field-width"></input></td>
						<td><input id="newUnitPrice" type="text" value="" class="itemType field-width"></input></td>
						<td><input id="newDiscount" type="text" value="0" class="itemType field-width"></input></td>
						<td><input id="newTaxPercentage" type="text" value="0" class="itemType field-width"></input></td>
						<td><input id="newCGSTPercentage" type="text" value="0" class="itemType field-width"></input>
						</td>
						<td><input id="newSGSTPercentage" type="text" value="0" class="itemType field-width"></input>
						</td>
						<td><input id="newUTGSTPercentage" type="text" value="0" class="itemType field-width"></input>
						</td>
						<td><input id="newIGSTPercentage" type="text" value="0" class="itemType field-width"></input>
						</td>
						<td><input id="newCompensationCessPercentage" type="text" value="0"
								   class="itemType field-width"></input></td>
						<td></td>
						<td></td>
						<td>
							<div id="saveNewInvoiceItem" class="btn btn-small lfloat save">save</div>
						</td>
					</tr>
				<# } #>
			</table>
		</div><br/>
		<# if (obj.statusCode == 'CREATED') { #>
			<br/>
			<# if (obj.vendorInvoiceItems.length > 0) { #>
				<div id="markComplete" class="btn btn-small btn-success lfloat20">complete</div>
			<# } #>
            <div id="discardVendorInvoice" class="btn btn-small btn-danger lfloat20">discard</div>
            <div class="clear"></div>
		<# } #>
		<# if (obj.statusCode == 'COMPLETE' && obj.type == 'CREDIT' && obj.debitInvoiceCodes.length == 0) { #>
			<br/>
            <div id="createAutoDebitInvoice" class="btn btn-small btn-warning lfloat20">Create Auto Debit Invoice</div>
            <div class="clear"></div>
		<# } #>
		</script>
		</div>
	</tiles:putAttribute>
	<tiles:putAttribute name="deferredScript">
    <sec:authentication property="principal" var="user" />
	<script type="text/javascript" src="${path.js('jquery/jquery.dataTables.min.js')}"></script>
	<script id="createDebitInvoiceTemplate" type="text/html">
		<input type="hidden" id="vendorInvoiceId" value="${vendorInvoice.id}">
					<table cellpadding="4">
						<tr>
							<td class="searchLabel">Party</td>
							<td><input type="text" id="partyCode"></td>
							<td class="searchLabel" ></td>
							<td></td>
						</tr>
					</table>
		<table cellpadding="4">
		<# if (obj && obj.length > 0) { #>
				<tr>
				<# for(var i=0;i<obj.length;i++) { var customField = obj[i]; #>
		 			<td align="left" class="searchLabel"><#=customField.displayName#></td>
					<td align="left" class="searchLabel">
						<# if (customField.valueType == 'text') { #>
							<input id="<#=customField.fieldName#>" type="text" class="w150 custom poUpdateItem" value="<#=customField.fieldValue#>" style="width:70%;"/>
						<# } else if (customField.valueType == 'date') { #>
							<input id="<#=customField.fieldName#>" type="text" class="w150 custom poUpdateItem datefield" value="<#= customField.fieldValue ? new Date(customField.fieldValue).toPaddedDate() : '' #>" style="width:70%;"/>
						<# } else if (customField.valueType == 'select') { #>
							<select id="<#=customField.fieldName#>" class="w150 custom poUpdateItem" style="width:70%;">
							<# for (var j=0;j<customField.possibleValues.length;j++) { #>
								<option value="<#=customField.possibleValues[j]#>" <#= customField.possibleValues[j] == customField.fieldValue ? selected="selected" : '' #> ><#=customField.possibleValues[j]#></option>
							<# } #>
							</select>
						<# } else if (customField.valueType == 'checkbox') { #>
							<input id="<#=customField.fieldName#>" type="checkbox" class="custom poUpdateItem" <#=customField.fieldValue ? checked="checked" : '' #> style="width:70%;"/>
						<# } #>
					</td>
					<# if (i == obj.length - 1 && i % 2 == 0) { #>
						<td></td><td></td>
					<# } else if (i != 0 && i % 2 != 0) { #>
						</tr><tr>
					<# } #>
				<# } #>
				</tr>
            <# } #>
			<tr class="mar-15-top">
				<td></td>
				<td colspan="3">
					<input type="submit" class="btn btn-primary" id="addVendorInvoice" value="Create" style="margin-top:10px;"/>
				</td>
		 	</tr>
		</table>
	</script>
	<script id="createVendorInvoiceTemplate" type="text/html">
		<input type="hidden" id="vendorInvoiceId" value="${vendorInvoice.id}">
					<table cellpadding="4">
						<tr>
							<td class="searchLabel" >Purchase Order</td>
							<td><input type="text" id="poCode"></td>
							<td class="searchLabel" >Vendor Invoice Number</td>
							<td><input type="text" id="vendorInvoiceNumber"></td>
						</tr>
						<tr>
							<td class="searchLabel">Vendor Invoice Date</td>
							<td><input type="text" id="vendorInvoiceDate" class="date datefield"></td>
							<td class="searchLabel" ></td>
							<td></td>
						</tr>
					</table>
		<table cellpadding="4">
		<# if (obj && obj.length > 0) { #>
				<tr>
				<# for(var i=0;i<obj.length;i++) { var customField = obj[i]; #>
		 			<td align="left" class="searchLabel"><#=customField.displayName#></td>
					<td align="left" class="searchLabel">
						<# if (customField.valueType == 'text') { #>
							<input id="<#=customField.fieldName#>" type="text" class="w150 custom poUpdateItem" value="<#=customField.fieldValue#>" style="width:70%;"/>
						<# } else if (customField.valueType == 'date') { #>
							<input id="<#=customField.fieldName#>" type="text" class="w150 custom poUpdateItem datefield" value="<#= customField.fieldValue ? new Date(customField.fieldValue).toPaddedDate() : '' #>" style="width:70%;"/>
						<# } else if (customField.valueType == 'select') { #>
							<select id="<#=customField.fieldName#>" class="w150 custom poUpdateItem" style="width:70%;">
							<# for (var j=0;j<customField.possibleValues.length;j++) { #>
								<option value="<#=customField.possibleValues[j]#>" <#= customField.possibleValues[j] == customField.fieldValue ? selected="selected" : '' #> ><#=customField.possibleValues[j]#></option>
							<# } #>
							</select>
						<# } else if (customField.valueType == 'checkbox') { #>
							<input id="<#=customField.fieldName#>" type="checkbox" class="custom poUpdateItem" <#=customField.fieldValue ? checked="checked" : '' #> style="width:70%;"/>
						<# } #>
					</td>
					<# if (i == obj.length - 1 && i % 2 == 0) { #>
						<td></td><td></td>
					<# } else if (i != 0 && i % 2 != 0) { #>
						</tr><tr>
					<# } #>
				<# } #>
				</tr>
            <# } #>
			<tr class="mar-15-top">
				<td></td>
				<td colspan="3">
					<input type="submit" class="btn btn-primary" id="addVendorInvoice" value="Create" style="margin-top:10px;"/>
				</td>
		 	</tr>
		</table>
	</script>
	<script id="vendorInvoiceLink" type="text/html">
        <a href="/procure/searchVendorInvoice?legacy=1" style="text-decoration:none">
            <span class="mainHeading subHeaded">VendorInvoices<span class="pipe">/</span></span>
        </a> 
        <span class="subHeading"><#=obj#></span>
    </script>
	<script type="text/javascript">
		<uc:security accessResource="EDIT_VENDOR_INVOICE_METADATA">
			Uniware.Utils.editVendorInvoiceMetadata = true;
		</uc:security>
		Uniware.Utils.vendorInvoiceUpdateAccess = true;
        Uniware.Utils.priceExclusive = ${configuration.getConfiguration('systemConfiguration').isVendorPricesTaxExclusive()};
		Uniware.SearchVendorInvoiceCatalogPage = function() {
			Uniware.Utils.countries = ${cache.getCache("locationCache").getCountriesJson()};
		    Uniware.Utils.currentFacilityCountry = '${cache.getCache("facilityCache").getCurrentFacility().getPartyAddressByType('SHIPPING').getCountryCode()}';
			var self = this;
			var url = window.location.href;
			this.name = 'DATATABLE SEARCH VENDOR INVOICES';
			this.pageConfig = null;
			this.table = null;
			this.vendorInvoiceCode = null;
			this.editRow = null;
			this.vendorInvoice = null;
			this.partyCode;
			this.linkType = null;
			this.isCustomFieldEditing = false;
			this.customFields = ${customFieldsJson};
			this.datatableViews = ${user.getDatatableViewsJson("DATATABLE SEARCH VENDOR INVOICES")};
			this.init = function(){
				var vendorInvoiceCode = Uniware.Utils.getParameterByName('vendorInvoiceCode');
				if(vendorInvoiceCode){
					self.vendorInvoiceCode = vendorInvoiceCode;
					self.getVendorInvoiceDetails();
				}else{
					if(this.datatableViews.length){
						this.headConfig = new Uniware.HeadConfig(this.datatableViews,function(datatableObject){
							self.pageConfig.setView(datatableObject);
						});
					}
					$("#createVendorInvoice").click(self.createVendorInvoice);
					$("#createDebitNote").click(self.createDebitNote);
					this.pageConfig = new Uniware.PageConfig(this.name,'#flexme1',this.datatableViews[0]);
					$("#tableContainer").on('click', '.editVendorInvoice' ,self.editVendorInvoice);
				}
			};
			
			this.editVendorInvoiceCustomFields = function() {
				self.isCustomFieldEditing = true;
				self.loadVendorInvoiceDetails();
			};
			
			this.discardDetails = function () {
				self.isCustomFieldEditing = false;
				self.loadVendorInvoiceDetails();
			};
			
			this.saveDetails = function () {
				var req = {
					'vendorCode' : self.vendorInvoice.vendorCode,
					'vendorInvoiceCode' : self.vendorInvoiceCode,
					'vendorInvoiceNumber' : $("#vendorInvNumber").val(),
					'vendorInvoiceDate' : $("#vendorInvDate").val()
				};
				Uniware.Utils.addCustomFieldsToRequest(req, $('.custom'));
				Uniware.Ajax.postJson("/data/inflow/vendorInvoice/detail/edit", JSON.stringify(req), function(response) {
					if(response.successful == true) {
						self.getVendorInvoiceDetails();
					} else {
						Uniware.Utils.showError(response.errors[0].description);
					}
				});
					
			};
			
			this.editVendorInvoice = function(event) {
				var tr = $(event.target).parents('tr');
				var rowNumber = $(tr).attr('id').split('-')[1];
				var data = self.pageConfig.getRecord(rowNumber);
				self.vendorInvoiceCode = self.pageConfig.getColumnValue(data,'vendorInvoiceCode');
				self.getVendorInvoiceDetails();
				self.linkType = 'Edit';
				$("#vendorInvoiceCreateLink").html(template("vendorInvoiceLink",self.linkType));
				$("#createVendorInvoiceDiv").html("");
				$("#tableContainer").hide();
				$("#createVendorInvoice").hide();
			};
			
			this.createVendorInvoice = function() {
				self.linkType = 'Create';
				$("#vendorInvoiceCreateLink").html(template("vendorInvoiceLink",self.linkType));
				$("#tableContainer").hide();
				$("#createVendorInvoice,#createDebitNote").hide();
				$("#createVendorInvoiceDiv").html(template("createVendorInvoiceTemplate",self.customFields));
				$("#addVendorInvoice").click(self.addCreditVendorInvoice);
				$(".datefield").datepicker({
					dateFormat : 'dd/mm/yy'
				});
			};
			
			this.createDebitNote = function() {
				self.linkType = 'Create';
				$("#vendorInvoiceCreateLink").html(template("vendorInvoiceLink",self.linkType));
				$("#tableContainer").hide();
				$("#createVendorInvoice,#createDebitNote").hide();
				$("#createVendorInvoiceDiv").html(template("createDebitInvoiceTemplate",self.customFields));
				$('#partyCode').autocomplete(self.getAutoCompleteObj());
				$("#addVendorInvoice").click(self.addDebitVendorInvoice);
			};
			
			this.addDebitVendorInvoice = function() {
				var requestObject =  { 'vendorInvoice' :{
					'partyCode' : self.partyCode
				}};
				Uniware.Utils.addCustomFieldsToRequest(requestObject.vendorInvoice, $('.custom'));
				var requestData = JSON.stringify(requestObject);
				
				var url = "/data/inflow/vendorInvoice/debit/create";
				
				Uniware.Ajax.postJson(url, requestData, function(response) {
					if(response.successful == true){
						self.vendorInvoiceCode = response.vendorInvoiceCode;
						Uniware.Utils.addNotification('Vendor Invoice has been created');
						self.getVendorInvoiceDetails();
					}else{
						Uniware.Utils.showError(response.errors[0].description);													
					}
				});
			};
			
			this.getAutoCompleteObj = function(){
				return {
			    	mustMatch : true,
					source: function( request, response ) {
						self.partyCode = null;
						Uniware.Ajax.getJson("/data/lookup/party?keyword=" + request.term,function(data) {
							response( $.map( data, function( party ) {
								return {
									label: party.name + "-" + party.code ,
										value: party.name + "-" + party.code,
										party: party
									}
							}));
						});
			
					},
					select: function(event, ui){
						self.partyCode = ui.item.party.code;
					}
				 };
			};
			
			this.addCreditVendorInvoice = function () {
				var requestObject =  { 'vendorInvoice' :{
					'vendorInvoiceNumber' : $('#vendorInvoiceNumber').val(),
					'purchaseOrderCode' : $('#poCode').val()
				}};
				if ($("#vendorInvoiceDate").val() != '') {
					requestObject.vendorInvoice.vendorInvoiceDate = Date.fromPaddedDate($("#vendorInvoiceDate").val()).getTime(); 
				}
				Uniware.Utils.addCustomFieldsToRequest(requestObject.vendorInvoice, $('.custom'));
				var requestData = JSON.stringify(requestObject);
				
				var url = "/data/inflow/vendorInvoice/credit/create";
				
				Uniware.Ajax.postJson(url, requestData, function(response) {
					if(response.successful == true){
						self.vendorInvoiceCode = response.vendorInvoiceCode;
						Uniware.Utils.addNotification('Vendor Invoice has been created');
						self.getVendorInvoiceDetails();
					}else{
						Uniware.Utils.showError(response.errors[0].description);													
					}
				});
			};
			
			this.cancel = function(event){
				var item = self.vendorInvoice.vendorInvoiceItems[$(this).attr('id').substring($(this).attr('id').indexOf('-') + 1)];
				item.isEditing = false;
				self.loadVendorInvoiceDetails();
			};
			
			this.editVendorInvoiceItem = function(event){
				var index = $(this).attr('id').split('-')[1];
				var vendorInvoiceItem = self.vendorInvoice.vendorInvoiceItems[index];
				var req = {
					'vendorInvoiceCode' : self.vendorInvoiceCode,
					'vendorInvoiceItems' : [{
						'quantity' : $("#qty-" + index).val(),
					    'unitPrice' : $("#unitPrice-" + index).val(),
					    'discount' : $("#discount-" + index).val(),
					    'taxPercentage' : $("#taxPercentage-" + index).val(),
                        'centralGstPercentage': $("#centralGstPercentage-" + index).val(),
                        'stateGstPercentage': $("#stateGstPercentage-" + index).val(),
                        'unionTerritoryGstPercentage': $("#unionTerritoryGstPercentage-" + index).val(),
                        'integratedGstPercentage': $("#integratedGstPercentage-" + index).val(),
                        'compensationCessPercentage': $("#compensationCessPercentage-" + index).val()
					}]
				};
				if (vendorInvoiceItem.itemTypeSkuCode != null) {
					req.vendorInvoiceItems[0].itemSkuCode = vendorInvoiceItem.itemTypeSkuCode;
					req.vendorInvoiceItems[0].description = vendorInvoiceItem.description;
				} else {
					req.vendorInvoiceItems[0].description = $('#description-' + index).val();
				}
				
				Uniware.Ajax.postJson("/data/inflow/vendorInvoice/addOrEditItem", JSON.stringify(req), function(response) {
					if(response.successful == true) {
						self.getVendorInvoiceDetails();
						Uniware.Utils.addNotification("Vendor Invoice Item has been updated.");
					} else {
						Uniware.Utils.showError(response.errors[0].description);
					}
				});
			}
			
			this.getVendorInvoiceDetails = function() {
				self.isCustomFieldEditing = false;
				var vendorInvoiceCode = self.vendorInvoiceCode;
				var requestObject = {
					'vendorInvoiceCode' : vendorInvoiceCode 
				}
				Uniware.Ajax.postJson('/data/inflow/vendorInvoice/detail/get', JSON.stringify(requestObject), function(data) {
					if (data.successful == true) {
						self.vendorInvoice = data.vendorInvoice;
						self.loadVendorInvoiceDetails(data);
					} else {
						Uniware.Utils.showError(data.errors[0].description);													
					}
				});
			};
			
			this.loadVendorInvoiceDetails = function() {
				$("#createVendorInvoiceDiv").html('');
				$("#vendorInvoiceDetails").html(template("vendorInvoiceTemplate", self.vendorInvoice));
				$('#grnCode').focus().keyup(self.addGRN);
				$('#gatePass').focus().keyup(self.addGatePass);
				$('#discardVendorInvoice').click(self.discardVendorInvoice);
				$('#markComplete').click(self.markComplete);
				$('#createAutoDebitInvoice').click(self.createAutoDebitInvoice);
				$('#saveNewInvoiceItem').click(self.saveNewInvoiceItem);
				$("span.table-editable").click(self.editableVendorInvoiceItem);
				$(".update").click(self.editVendorInvoiceItem);
				$(".cancel").click(self.cancel);
				$(".removeItem").click(self.removeItem);
				$("#editVendorInvoiceCustomFields").click(self.editVendorInvoiceCustomFields);
				$("#discardDetails").click(self.discardDetails);
				$("#saveDetails").click(self.saveDetails);
                $("input#newDescription").autocomplete({
                    minLength: 4,
                    mustMatch : true,
                    autoFocus: true,
                    source: function( request, response ) {
                        Uniware.Ajax.getJson("/data/lookup/itemTypes?keyword=" + request.term, function(data) {
                            response( $.map( data, function( item ) {
                                return {
                                    label: item.name + " - " + item.itemSKU,
                                    value: item.name + " - " + item.itemSKU,
                                    itemType: item
                                }
                            }));
                        });
                    },
                    select: function( event, ui ) {
                        var tr = $(event.target).parents('tr');
                        var itemType = ui.item.itemType;
                        $(tr[0]).find('.itemTypeName').html(itemType.name);
                        $(tr[0]).find('.itemType').html(itemType.name);
                        $('td#newSku').html(itemType.itemSKU);
                    }
                }).keyup(function(event) {
                    if (event.which == 13) {
                        var tr = $(event.target).parents('tr');
                        $(tr[0]).find('input#newQty').select();
                    }
                });
                $(".customDate").datepicker({
					dateFormat : 'yy-mm-dd'
				});
                $(".customFieldDate").datepicker({
					dateFormat : 'dd/mm/yy'
				});
			};
			
			this.removeItem = function() {
				var vendorInvoiceItem = self.vendorInvoice.vendorInvoiceItems[$(this).attr('id').substring($(this).attr('id').indexOf('-') + 1)];
				var req = {
					'vendorInvoiceCode' : self.vendorInvoiceCode,
					'description' : vendorInvoiceItem.description
				};
				if (vendorInvoiceItem.itemTypeSkuCode != null) {
					req.itemSkuCode = vendorInvoiceItem.itemTypeSkuCode;
				}
				Uniware.Ajax.postJson('/data/inflow/vendorInvoice/removeItem', JSON.stringify(req), function(response) {
					if (response.successful) {
						self.getVendorInvoiceDetails();
						Uniware.Utils.addNotification('Vendor Invoice Item has been removed successfully');
					} else {
						Uniware.Utils.showError(response.errors[0].description);							
					}
				}, true);
			};
			
			this.editableVendorInvoiceItem = function(event) {
 				var tr = $(event.target).parents('tr');
				var itemKey = $(tr[0]).attr('id').substring($(tr[0]).attr('id').indexOf('-') + 1);
				var item = self.vendorInvoice.vendorInvoiceItems[itemKey];
				
				if (item.isEditing) {
					
				} else {
					if (self.itemEditing != null && self.itemEditing != itemKey) {
						var orgItem = self.vendorInvoice.vendorInvoiceItems[self.itemEditing];
						orgItem.isEditing = null;
					}
					self.itemEditing = itemKey;
					item.isEditing = true;
					self.loadVendorInvoiceDetails(self.gatePass);
				}
			};
			
			this.saveNewInvoiceItem = function() {
				var req = {
					'vendorInvoiceCode' : self.vendorInvoiceCode,
					'vendorInvoiceItems' : [{
						'description' : $('#newDescription').val(),
						'unitPrice' : $('#newUnitPrice').val(),
					    'discount' : $('#newDiscount').val(),
					    'quantity' : $('#newQty').val(),
                        'taxPercentage': $('#newTaxPercentage').val(),
                        'centralGstPercentage': $('#newCGSTPercentage').val(),
                        'stateGstPercentage': $('#newSGSTPercentage').val(),
                        'unionTerritoryGstPercentage': $('#newUTGSTPercentage').val(),
                        'integratedGstPercentage': $('#newIGSTPercentage').val(),
                        'compensationCessPercentage': $('#newCompensationCessPercentage').val()
					}]
				};
                if ($('td#newSKU').html() != '') {
                    req.vendorInvoiceItems[0]['itemSkuCode'] = $('td#newSKU').html();
                }
				Uniware.Ajax.postJson('/data/inflow/vendorInvoice/addOrEditItem', JSON.stringify(req), function(response) {
					if (response.successful) {
						self.getVendorInvoiceDetails();
						Uniware.Utils.addNotification('Vendor Invoice Item has been added successfully');
					} else {
						Uniware.Utils.showError(response.errors[0].description);							
					}
				}, true);
			};
			
			this.markComplete = function() {
                var totalQty = 0;
                var totalPrice = 0;
                for (var i=0;i<self.vendorInvoice.vendorInvoiceItems.length;i++) {
                    totalQty += self.vendorInvoice.vendorInvoiceItems[i].quantity;
                    totalPrice += self.vendorInvoice.vendorInvoiceItems[i].total;
                }
                var str = 'Please check the following things.\n\n';
                str += 'Total Invoice Value = ' + totalPrice + '\n';
                str += 'Total Quantity = ' + totalQty + '\n\n';
                str += 'No changes will be allowed in Vendor Invoice after completion. Are you sure?';
				var action = confirm(str);
				if (action == false)
					return;
				var req = {
					'vendorInvoiceCode' : self.vendorInvoiceCode
				};
				Uniware.Ajax.postJson('/data/inflow/vendorInvoice/complete', JSON.stringify(req), function(response) {
					if (response.successful) {
						self.getVendorInvoiceDetails();
						Uniware.Utils.addNotification('Vendor Invoice "'+ self.vendorInvoiceCode + '" has been complete');
					} else {
						Uniware.Utils.showError(response.errors[0].description);							
					}
				}, true);
			};
			
			this.createAutoDebitInvoice = function() {
				var req = {
					'creditVendorInvoiceCode' : self.vendorInvoiceCode,
                    'addQCRejectedItems' : true
				};
				Uniware.Ajax.postJson('/data/inflow/vendorInvoice/debit/autoCreate', JSON.stringify(req), function(response) {
					if (response.successful) {
						self.vendorInvoiceCode = response.vendorInvoiceCode;
						self.getVendorInvoiceDetails();
						Uniware.Utils.addNotification('Vendor Debit Invoice "'+ response.vendorInvoiceCode + '" has been created.');
					} else {
						Uniware.Utils.showError(response.errors[0].description);							
					}
				}, true);
				
			};
			
			this.discardVendorInvoice = function() {
				var req = {
					'vendorInvoiceCode' : self.vendorInvoiceCode
				};
				Uniware.Ajax.postJson('/data/inflow/vendorInvoice/discard', JSON.stringify(req), function(response) {
					if (response.successful) {
						self.getVendorInvoiceDetails();
						Uniware.Utils.addNotification('Vendor Invoice "'+ self.vendorInvoiceCode + '" has been discarded');
					} else {
						Uniware.Utils.showError(response.errors[0].description);							
					}
				}, true);
			};
			
			this.addGRN = function(event) {
				if (event.which == 13) {
					var req = {
						'vendorInvoiceCode' : self.vendorInvoice.code,
						'purchaseOrderCode' : self.vendorInvoice.purchaseOrderCode,
						'inflowReceiptCode' : $('#grnCode').val()
					};
					Uniware.Ajax.postJson('/data/inflow/vendorInvoice/inflowReceipt/add', JSON.stringify(req), function(response) {
						if (response.successful == true) {
							self.getVendorInvoiceDetails();
						} else {
							Uniware.Utils.showError(response.errors[0].description);													
						}
					});
				}
			};
			
			this.addGatePass = function(event) {
				if (event.which == 13) {
					var req = {
						'code' : $('#gatePass').val()	
					};
					Uniware.Ajax.postJson("/data/material/gatepass/get", JSON.stringify(req), function(response) {
						if(response.successful == true) {
							var req = {
								'vendorInvoiceCode' : self.vendorInvoiceCode,
								'vendorInvoiceItems' : []
							};
							var skuToVendorInvoiceItems = {};
							for (var i=0;i<response.gatePassDTO.gatePassItemDTOs.length;i++) {
								var gatePassItem = response.gatePassDTO.gatePassItemDTOs[i];
								if (skuToVendorInvoiceItems[gatePassItem.itemTypeSKU]) {
									var item = skuToVendorInvoiceItems[gatePassItem.itemTypeSKU];
									item.quantity = item.quantity + gatePassItem.quantity;
								} else {
									var item = {
										'itemSkuCode' : gatePassItem.itemTypeSKU,
										'description' : gatePassItem.itemTypeName,
										'quantity' : gatePassItem.quantity,
										'unitPrice' : gatePassItem.unitPrice == 0 ? 1 : gatePassItem.unitPrice,
										'taxPercentage' : gatePassItem.taxPercentage,
                                        'centralGstPercentage': gatePassItem.centralGstPercentage,
                                        'stateGstPercentage': gatePassItem.stateGstPercentage,
                                        'unionTerritoryGstPercentage': gatePassItem.unionTerritoryGstPercentage,
                                        'integratedGstPercentage': gatePassItem.integratedGstPercentage,
                                        'compensationCessPercentage': gatePassItem.compensationCessPercentage,
										'discount' : 0
									};
									skuToVendorInvoiceItems[gatePassItem.itemTypeSKU] = item;
								}
							}
							for (var sellerSkuCode in skuToVendorInvoiceItems) {
								req.vendorInvoiceItems.push(skuToVendorInvoiceItems[sellerSkuCode]);
							}
							Uniware.Ajax.postJson('/data/inflow/vendorInvoice/addOrEditItem', JSON.stringify(req), function(response) {
								if (response.successful) {
									self.getVendorInvoiceDetails();
									Uniware.Utils.addNotification('Vendor Invoice Item has been added successfully');
								} else {
									Uniware.Utils.showError(response.errors[0].description);							
								}
							}, true);
						} else {
							Uniware.Utils.showError(response.errors[0].description);
						}
					}, true); 
				}
			};
		};
		
		$(document).ready(function() {
			window.page = new Uniware.SearchVendorInvoiceCatalogPage();
			window.page.init();
		});
	</script>
	</tiles:putAttribute>
</tiles:insertDefinition>
