<%@ include file="/tagIncludes.jsp"%>
<tiles:insertDefinition name=".reportsPage">
    <tiles:putAttribute name="title" value="Uniware - View Item Details" />
    <tiles:putAttribute name="rightPane">
        <div class="noprint">
            <div class="greybor headlable ovrhid main-box-head">
                <h2 class="edithead head-textfields">Scan Item</h2>
                <div class="lfloat">
                    <input id="itemCode" type="text" size="20"  autocomplete="off" value="${param['itemCode']}">
                </div>
                <div id="searching" class="lfloat10 hidden" style="margin-top:5px;">
                    <img src="/img/icons/refresh-animated.gif"/>
                </div>
            </div>
            <div class="clear"></div>
            <div id="itemDiv"></div>
        </div>
    </tiles:putAttribute>
    
    <tiles:putAttribute name="deferredScript">
    <script id="itemTemplate" type="text/html">
        <div class="greybor round_bottom main-boform-cont pad-15-top overhid">
        <table width="100%" border="0" cellspacing="1" cellpadding="3" class="fields-table">
            <tr> 
            <td colspan="4">
                <# if(Uniware.Utils.currentFacility != '') { #>
                      <div class="btn btn-small rfloat10" id="reprintLabel">reprint label</div>
                <# } #>
                <# if(obj.itemCustomFields){ #>
                    <uc:security accessResource="EDIT_ITEM_CUSTOM_FIELDS">
                        <div id="editItem" class="btn btn-small rfloat20">edit <img src="/img/icons/edit.png"/></div>
                    </uc:security>
                <# } #>        
                &#160;
            </td>
        </tr>
        <tr> 
            <td>Item Number</td>
            <td><#=obj.code#></td>
            <td>Created at</td>
            <td><#=new Date(obj.created).toDateTime()#></td>    
        </tr>
        <tr> 
            <td>Status</td>
            <td class="bold"><#=obj.status#></td>
            <td>Inventory Type</td>
            <td><#=obj.inventoryType#></td>            
        </tr>
        <tr> 
            <td>Item Name</td>
            <td>
                <#=obj.itemTypeName#>
                <#=Uniware.Utils.getItemDetail(obj.itemTypeImageUrl, obj.itemTypePageUrl)#>
            </td>    
            <td>SKU Code</td>
            <td><#=obj.itemSKU#></td>
        </tr>
        <tr> 
            <td>Vendor Name</td>
            <td class="bold"><#=obj.vendor#></td>
            <td>Vendor Code</td>
            <td><#=obj.vendorCode#></td>            
        </tr>
        <tr> 
            <td>Color</td>
            <td><#=obj.color#></td>
            <td>Size</td>
            <td><#=obj.size#></td>            
        </tr>
        <tr> 
            <td>MRP</td>
            <td><#=obj.maxRetailPrice#></td>
            <td>Brand</td>
            <td><#=obj.brand#></td>            
        </tr>
        <tr> 
            <td>GRN Number</td>
            <td class="bold"><#=obj.inflowReceiptCode#></td>
            <td>Vendor SKU Code</td>
            <td><#=obj.vendorSkuCode#></td>    
        </tr>
        <tr> 
            <td>Putaway Number</td>
            <td><#=obj.lastPutawayCode#></td>
            <# if(obj.status == 'BAD_INVENTORY') { #>
            <td>Rejection Reason</td>
            <td><#=obj.rejectionReason#></td>   
            <# } else { #>
            <# } #> 
        </tr>
        <# if(obj.itemCustomFields != null){ #>
            <# var j = 0;#>
            <tr class="ucase">
            <# for(var key in obj.itemCustomFields) {#>
                <td id="name"><#=key#></td>
                <td id="value"><# if (obj.isEditing) { #>
                    <input id="customField + j" type="text" value="<#=obj.itemCustomFields[key]#>" class="customField" name="<#=key#>"/>
                    <# } else { #>
                    <#=obj.itemCustomFields[key]#>
                    <# } #>
                </td>
            
                <# if(j % 2 != 0) { #>
                    </tr><tr>
                <# } #>
                <# var j = j + 1; #>
            <# } #>

            <# if(j%2 == 1) { #>
                <td></td><td></td>
            <# } #>
            </tr>
        <# } #>
        <# if (obj.itemTypeCustomFieldValues && obj.itemTypeCustomFieldValues.length > 0) { #>
            <tr>
            <# for(var i=0;i<obj.itemTypeCustomFieldValues.length;i++) { var customField = obj.itemTypeCustomFieldValues[i]; #>
                 <td><#=customField.displayName#></td>
                <td>
                    <# if (customField.valueType == 'date') { #>
                        <#= customField.fieldValue ? new Date(customField.fieldValue).toPaddedDate() : '' #>
                    <# } else if (customField.valueType == 'checkbox') { #>
                        <input type="checkbox" disabled="disabled" <#=customField.fieldValue ? checked="checked" : '' #>/>
                    <# } else { #>
                        <#=customField.fieldValue#>
                    <# } #>
                </td>
                <# if (i == obj.itemTypeCustomFieldValues.length - 1 && i % 2 == 0) { #>
                    <td></td><td></td>
                <# } else if (i != 0 && i % 2 != 0) { #>
                    </tr><tr>
                <# } #>
            <# } #>
            </tr>                
        <# } #>
        <# if (obj.isEditing) { #>
        <tr>
            <td>&nbsp;</td>
            <td colspan="3">
                <input type="submit" id="updateItem" class="btn btn-small btn-primary lfloat" value="Update" />
                <div id="cancelUpdate" class="lfloat10 link">Discard Changes</div>
            </td>
          </tr>
        <# } #>        
        </table>
        <# if (obj.saleOrderItemDTOs.length > 0) { #>
            <div class="greybor headlable round_top ovrhid">
                <h4 class="edithead">Item Related to Sale Order Item</h4>
            </div>
            <div class="greybor round_bottom form-edit-table-cont">
                <table class="uniTable" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td>Sale Order</td>
                    <td>Sale Order Item Code</td>
                    <td>Sale Order Item Status</td>
                    <td>Shipping Package</td>
                    <td>Shipping Package Status</td>
                    <td>Picklist Number</td>
                    <# if(Uniware.Utils.currentFacility == '') { #>
                        <td>Facility</td>
                    <# } #>
                </tr>
                <# for (var i=0;i < obj.saleOrderItemDTOs.length;i++) { var saleOrderItem = obj.saleOrderItemDTOs[i]; #>
                <tr class="f15">
                    <td><a target="_top" href="/order/orderitems?orderCode=<#=saleOrderItem.saleOrderCode#>" ><#=saleOrderItem.saleOrderCode#></a></td>
                    <td><#=saleOrderItem.saleOrderItemCode#></td>
                    <td><#=saleOrderItem.saleOrderItemStatus#></td>
                    <td><a target="_top" href="/order/shipments?orderCode=<#=saleOrderItem.saleOrderCode#>&shipmentCode=<#=saleOrderItem.shippingPackageCode#>"><#=saleOrderItem.shippingPackageCode#></a></td>
                    <td><#=saleOrderItem.shippingPackageStatus#></td>
                    <td><#=saleOrderItem.picklistNumber#></td>
                    <# if(Uniware.Utils.currentFacility == '') { #>
                        <td><#=saleOrderItem.facilityName#></td>
                    <# } #>
                </tr>
                <# } #>
                </table>
            </div>
        <# } #>
        <# if (obj.gatePassDTOs.length > 0) { #>
            <div class="greybor headlable round_top ovrhid">
                <h4 class="edithead">Gate Passes</h4>
            </div>
            <div class="greybor round_bottom form-edit-table-cont">
                <table class="uniTable" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td>Code</td>
                    <td>Status Code</td>
                    <td>Type</td>
                    <td>Reference</td>
                    <td>Purpose</td>
                    <# if(Uniware.Utils.currentFacility == '') { #>
                        <td>Facility</td>
                    <# } #>
                </tr>
                <# for (var i=0;i < obj.gatePassDTOs.length;i++) { var gatePass = obj.gatePassDTOs[i]; #>
                <tr class="f15">
                    <td><#=gatePass.code#></td>
                    <td><#=gatePass.statusCode#></td>
                    <td><#=gatePass.type#></td>
                    <td><#=gatePass.reference#></td>
                    <td><#=gatePass.purpose#></td>
                    <# if(Uniware.Utils.currentFacility == '') { #>
                        <td><#=gatePass.facilityName#></td>
                    <# } #>
                </tr>
                <# } #>
                </table>
            </div>
        <# } #>
        <# if(obj.addReason) { #>
            <h5 class="edithead">Reason</h5>
            <textarea id="reason"></textarea>
            <br/>
        <# } #>
        <uc:security accessResource="DISCARD_GOOD_INVENTORY">
            <br/>
            <# if (obj.status == 'GOOD_INVENTORY' && !obj.addReason) { #>
                <div class="btn btn-warning lfloat10" id="badInventory">Mark As Bad Inventory</div>
            <# } #>
            <# if (obj.status == 'GOOD_INVENTORY' && obj.addReason) { #>
                <div class="btn btn-warning lfloat10" id="markBadInventory">Mark As Bad Inventory</div>
            <# } #>
            <div class="clear"></div>
        </uc:security>
        </div>
        
    </script>
    <script type="text/javascript">
        Uniware.Utils.reprintItemLabel = ${configuration.getConfiguration('systemConfiguration').reprintItemLabel};
        Uniware.Utils.currentFacility = '${cache.getCache('facilityCache').currentFacility.code}';
        Uniware.SearchItemPage = function() {
            var self = this;    
            this.itemCode;
            this.itemDTOContents;
            
            this.init = function() {
                $('#itemCode').keyup(self.load);
            };
            
            this.load = function(event){
                if (event.which == 13 && $('#itemCode').val() != '') {
                    $('#itemDiv').html("");
                    self.searchItem($('#itemCode').val());
                }
            };
            
            this.searchItem = function(itemCode) {
                var req = {
                    'itemCode': itemCode
                };
                if(Uniware.Utils.currentFacility != "") {
                    Uniware.Ajax.postJson("/data/item/fetch", JSON.stringify(req), function(response) {
                        if(response.successful == true) {
                            self.itemCode = response.itemDTO.code;
                            self.itemDTOContents = response.itemDTO;
                            self.render();
                        } else {
                            Uniware.Utils.showError(response.errors[0].description);
                        }
                        $('#itemCode').val('').focus();
                    }); 
                } else {
                    Uniware.Ajax.postJson("/data/item/allFacility/fetch", JSON.stringify(req), function(response) {
                        if(response.successful == true) {
                            self.itemCode = response.itemDTO.code;
                            self.itemDTOContents = response.itemDTO;
                            self.render();
                        } else {
                            Uniware.Utils.showError(response.errors[0].description);
                        }
                        $('#itemCode').val('').focus();
                    }); 
                }
            };
            
            this.render = function(){
                $('#itemDiv').html(template("itemTemplate", self.itemDTOContents));
                $('#reprintLabel').click(self.reprintLabel);
                Uniware.Utils.barcode();
                Uniware.Utils.applyHover();
                $('#badInventory').click(self.makeBadInventory);
                $('#editItem').click(self.editItem);
            };
            
            this.reprintLabel = function(){
                Uniware.Utils.printIFrame('/inflow/items/print/labels/'+ self.itemCode);
            };
            
            this.editItem = function() {
                self.itemDTOContents.isEditing = true;
                self.render();
                $('#updateItem').click(self.updateItemCustomFieldValues);
                $('#cancelUpdate').click(self.cancelUpdate);
            };
            
            this.makeBadInventory = function() {
                self.itemDTOContents.addReason = true;
                $('#itemDiv').html(template("itemTemplate", self.itemDTOContents));
                $("#markBadInventory").click(self.markInventoryBad);
            };
            
            this.markInventoryBad = function() {
                var req = {
                        itemCode: self.itemCode,
                        rejectionReason : $("#reason").val()
                     }
                    Uniware.Ajax.postJson("/data/admin/system/goodInventoryItemCode/damage", JSON.stringify(req), function(response) {
                        if(response.successful == true){
                            Uniware.Utils.addNotification("Item has been marked as Bad Inventory");
                            self.searchItem(self.itemCode);
                        }else{
                            Uniware.Utils.showError(response.errors[0].description);
                        }
                    }, true);
            };
            
            this.cancelUpdate = function(){
                self.itemDTOContents.isEditing = false;
                self.render();
            };
            
            this.updateItemCustomFieldValues = function() {
                var req = {
                    'itemCode' : self.itemCode,
                    'itemDetails' : []
                };
                $('.customField').each(function(index) {
                    var itemDetail = {};
                    itemDetail.name = $(this).attr('name');
                    itemDetail.value = $(this).val();
                    req.itemDetails.push(itemDetail); 
                });
                Uniware.Ajax.postJson("/data/reports/lookups/itemCustomField/edit", JSON.stringify(req), function(response) {
                    if(response.successful) {
                        self.searchItem(self.itemCode);
                        Uniware.Utils.addNotification("Item Details have been updated.");
                    } else {
                        Uniware.Utils.showError(response.errors[0].description);
                    }
                }, true);
            };
        }
        
        $(document).ready(function() {
            window.page = new Uniware.SearchItemPage();
            window.page.init();
            if ($("#itemCode").val() != '') {
                $('#itemCode').trigger(Uniware.Event.ENTER_KEYUP);
            }
        });
    </script>
    </tiles:putAttribute>
</tiles:insertDefinition>
