<%@ include file="/tagIncludes.jsp"%>
<tiles:insertDefinition name=".reportsPage">
	<tiles:putAttribute name="title" value="Uniware - Search Item Type History" />
	<tiles:putAttribute name="rightPane">
		<div class="noprint greybor">
			<form onsubmit="javascript : return false;">
				<div class="greybor headlable ovrhid main-box-head">
					<h2 class="edithead head-textfields">Search Item Type</h2>
					<div class="lfloat">
						<input id="itemType" type="text" size="20"  autocomplete="off" style="width:400px" value="${param['itemType']}">
					</div>
					<div id="searching" class="lfloat10 hidden" style="margin-top:5px;">
						<img src="/img/icons/refresh-animated.gif"/>
					</div>
				</div>
				<div class="clear"></div>
				<div id="itemTypeDiv" style="margin:20px;"></div>
			</form>
		</div>
	</tiles:putAttribute>
	<tiles:putAttribute name="deferredScript">
		<script id="itemTypeDivTemplate" type="text/html">
		<div class="greybor headlable round_top">
			<h4 class="edithead">
				<#=obj.name#> - <#=obj.skuCode#>
				&#160;&#160;<#=Uniware.Utils.getItemDetail(obj.itemTypeImageUrl, obj.itemTypePageUrl)#>
			</h4>
			<div class="clear"></div>
		</div>
		<div class="greybor round_bottom main-boform-cont pad-15-top overhid">
		<table width="100%" border="0" cellspacing="1" cellpadding="3" class="fields-table f15">
			<tr> 
        		<td>Inventory Count</td>
        		<td>
					<div class="lfloat"><#=obj.inventory#></div>
					<# if(obj.inventory > 0) { #>
						<div class="lfloat20 link markNotFoundQuantity">show details</div>
					<# } #>
					<div class="clear"/>
				</td>   
        		<td>Open Sale Count</td>
        		<td><#=obj.openSale#></td>   
			</tr>
			<tr> 
        		<td>In Picking</td>
        		<td><#=obj.inPicking#></td>   
        		<td>Open Purchase Count</td>
        		<td><a href="/procure/searchPO?legacy=1&itemType=<#=obj.skuCode#>"><#=obj.openPurchase#></a></td>   
			</tr>
			<tr> 
        		<td>Putaway Pending</td>
        		<td><#=obj.putawayPending#></td>   
        		<td>Pending in GRN</td>
        		<td><#=obj.pendingGRN#></td>   
			</tr>
			<tr> 
        		<td>Quantity Not Found</td>
        		<td>
					<div class="lfloat"><#=obj.quantityNotFound#></div>
					<# if(obj.quantityNotFound > 0) { #>
						<div class="lfloat20 link markQuantity">show details</div>
						
					<# } #>
					<div class="clear"/>	
				</td>   
        		<td>Quantity Damaged</td>
        		<td><#=obj.quantityDamaged#></td>   
			</tr>
			<tr>
				<td colspan="4"><div id="itemTypeQuantityNotFoundTemplateDiv" /></td>
			</tr>
			<tr>
				<td colspan="4"><br/><br/></td>
			</tr>
			<tr>
				<td colspan="4"><div id="itemTypeInventoryTemplateDiv" /></td>
			</tr>
		</table>
		</div>
		<div class="clear"/><br/>
		<# if (Uniware.Utils.canViewBarcodes) { #>
			<div id="viewAvailableBarcodes" class=" lfloat btn btn-small btn-primary">View Available Barcodes</div>
			<# if(Uniware.Utils.reprintItemLabel) {#>
				<div id="printBarcodes" class=" lfloat10 btn btn-small btn-primary">Print Barcodes</div>
			<# } #>
		<# } #>
		<div class="clear"/><br/>
			<div id="barcodes"/>
		</script>
		<script type="text/html" id="itemTypeQuantityNotFoundTemplate">
			<table width="100%" border="0" cellspacing="1" cellpadding="3" class="uniTable greybor">
				<tr><th colspan="6" class="edithead">Mark Quantity Found</th></tr>
				<tr>
					<th align="left">S.NO</th>
					<th align="left">Item Sku</th>
					<th align="left">Shelf</th>
					<th align="left">Not Found Quantity</th>
					<# if(Uniware.Utils.canUpdateFoundQuantity) { #>
						<th align="left">Found Quantity</th>
						<th align="left">Operations</th>
					<# } #>
				</tr>
				<# for(var i=0; i<obj.itemQuantityNotFoundDTO.length ; i++) { var itemDetails = obj.itemQuantityNotFoundDTO[i];#>
					<# if(itemDetails.type == 'GOOD_INVENTORY') { #>
						<# if(itemDetails.notFoundQuantity > 0) {#>
							<# if(!itemDetails.isEditing) { #>
								<# var index = i + 1;#>
								<tr id="<#=i#>">
									<td><#=index#></td>
									<td><#=itemDetails.itemSku#></td>
									<td><#=itemDetails.shelfCode#></td>
									<td><#=itemDetails.notFoundQuantity#></td>
									<# if(Uniware.Utils.canUpdateFoundQuantity) { #>
										<td><span class="table-editable">0</span></td>
										<td></td>
									<# } #>
								</tr>
							<# } else { #>
								<tr id="<#=i#>">
								<td><#=i#></td>
								<td><#=itemDetails.itemSku#></td>
								<td><#=itemDetails.shelfCode#></td>
								<td><#=itemDetails.notFoundQuantity#></td>
								<# if(Uniware.Utils.canUpdateFoundQuantity) { #>
									<td>
										<input id="foundQuantity-<#=i#>" type="text" class="field-width" value="0"/>
									</td>
									<td>
										<input type="submit" class="btn btn-small update lfloat10" value="Mark Found" id="<#=i#>"></input>
										<span class="link cancel" id="cancel-<#=i#>" style="margin-top:3px;">cancel</span>
									</td>
								<# } #>
								</tr>
							<# } #>
						<# } #>
					<# } #>
				<# } #>
			</table>
		</script>
		<script type="text/html" id="itemTypeInventoryTemplate">
			<table width="100%" border="0" cellspacing="1" cellpadding="3" class="uniTable greybor">
				<tr><th colspan="6" class="edithead">Mark Not Found</th></tr>
				<tr>
					<th align="left">S.NO</th>
					<th align="left">Item Sku</th>
					<th align="left">Shelf</th>
					<th align="left">Quantity</th>
					<# if(Uniware.Utils.canUpdateFoundQuantity) { #>
						<th align="left">Not Found Quantity</th>
						<th align="left">Operations</th>
					<# } #>
				</tr>
				<# for(var i=0; i<obj.inventoryDTOs.length ; i++) { var itemDetails = obj.inventoryDTOs[i];#>
					<# if(itemDetails.type == 'GOOD_INVENTORY') { #>
						<# if(itemDetails.quantity > 0) {#>
							<# if(!itemDetails.isEditing) { #>
								<# var index = i + 1;#>
								<tr id="<#=i#>">
									<td><#=index#></td>
									<td><#=itemDetails.itemSKU#></td>
									<td><#=itemDetails.shelf#></td>
									<td><#=itemDetails.quantity#></td>
									<# if(Uniware.Utils.canUpdateFoundQuantity) { #>
										<td><span class="table-editable editInventory">0</span></td>
										<td></td>
									<# } #>
								</tr>
							<# } else { #>
								<tr id="<#=i#>">
								<td><#=i#></td>
								<td><#=itemDetails.itemSku#></td>
								<td><#=itemDetails.shelfCode#></td>
								<td><#=itemDetails.quantity#></td>
								<# if(Uniware.Utils.canUpdateFoundQuantity) { #>
									<td>
										<input id="notFoundQuantity-<#=i#>" type="text" class="field-width" value="0"/>
									</td>
									<td>
										<input type="submit" class="btn btn-small updateNotFound lfloat10" value="Not Found" id="<#=i#>"></input>
										<span class="link cancelNotFound" id="cancel-<#=i#>" style="margin-top:3px;">cancel</span>
									</td>
								<# } #>
								</tr>
							<# } #>
						<# } #>
					<# } #>
				<# } #>
			</table>
		</script>
		<script type="text/javascript">
			<uc:security accessResource="GOOD_INVENTORY_BARCODE_VIEW">
				Uniware.Utils.canViewBarcodes = true;
			</uc:security>
			<uc:security accessResource="MARK_QUANTITY_FOUND">
				Uniware.Utils.canUpdateFoundQuantity = true;
			</uc:security>
			Uniware.Utils.reprintItemLabel = ${configuration.getConfiguration('systemConfiguration').reprintItemLabel};
			Uniware.SearchItemTypePage = function() {
				var self = this;	
				this.itemTypeSkuCode;
				this.itemTypeId;
				this.itemDetails;
				this.itemInventoryDetails;
				this.showDetails = false;
				this.showInventoryDetails = false;
				
				
				this.init = function() {
					$("#itemTypeDiv").on('click','.markQuantity',self.markQuantityFound);
					$("#itemTypeDiv").on('click','.markNotFoundQuantity',self.markNotFoundQuantity);
					$("#itemType").autocomplete({
				    	minLength: 4,	
				    	mustMatch : true,
						autoFocus: true,
						source: function( request, response ) {
							$('#itemTypeDiv').html('');
							Uniware.Ajax.getJson("/data/lookup/itemTypes?keyword=" + request.term, function(data) {
								response( $.map( data, function( item ) {
									return {
										label: item.name + " - " + item.itemSKU + " - " + item.scanIdentifier,
										value: item.name,
										itemType: item
									}
								}));
							});
						},
						select: function( event, ui ) {
							self.itemTypeSkuCode = ui.item.itemType.itemSKU;
							self.itemTypeId = ui.item.itemType.itemTypeId;
							self.getItemTypeDetails();
						}
				    });
				};
				
				this.getItemTypeDetails = function() {
					var req = {
						'itemSKU' : self.itemTypeSkuCode	
					};
					Uniware.Ajax.postJson("/data/reports/search/itemType/fetch", JSON.stringify(req), self.onItemTypeFetch);
				};
				
				this.onItemTypeFetch = function(response) {
					if (response && response.successful) {
						$('#itemTypeDiv').html(template("itemTypeDivTemplate", response.itemTypeDTO));
						$("#itemType").val('');
						Uniware.Utils.applyHover();
						$("#viewAvailableBarcodes").click(self.getAvailableBarcodes);
						$("#printBarcodes").click(self.printAvailableBarcodes);
					} else {
						Uniware.Utils.showError(response.errors[0].description);
					}
				};
				
				this.getAvailableBarcodes = function() {
					var req = {
						'itemSku' : self.itemTypeSkuCode		
					};
					Uniware.Ajax.postJson("/data/reports/search/itemType/barcodes/view", JSON.stringify(req), function(response){
						$('#barcodes').html('');
						if (response.length > 0) {
							for (var i=0;i<response.length;i++) {
								$('#barcodes').append('<div class="lfloat" style="margin:10px;">' + response[i] + '</div>');
							}
							$('#barcodes').append('<div class="clear"/>');
						} else {
							$('#barcodes').html("No Barcodes Available for this Product.");
						}
					});
				};
				
				this.printAvailableBarcodes = function() {
					var req = {
						'itemSku' : self.itemTypeSkuCode		
					};
					Uniware.Ajax.postJson("/data/reports/search/itemType/barcodes/view", JSON.stringify(req), function(response){
						if (response.length > 0) {
							self.printBarcodes(response);
							self.getItemTypeDetails();
						} else {
							Uniware.Utils.showError("No Barcodes Available for this Product.");
						}
					});
				};
				
				this.printBarcodes = function(data) {
					var form = document.createElement("form");
					form.setAttribute("method", "post");
					form.setAttribute("action", "/inflow/items/printing/labels");
					form.setAttribute("name", "request");
					form.setAttribute("id", "formRequest");
					
					var hiddenField = document.createElement("input");
					hiddenField.setAttribute("name", "itemCodes");
					hiddenField.setAttribute("type", "hidden");
					hiddenField.setAttribute("value", data.join(','));
					form.appendChild(hiddenField);
					
					form.setAttribute("target", "formresult");
					document.body.appendChild(form);

					window.open("", 'formresult');
					form.submit();
					$('form#formRequest').remove();
				};
				
				this.markQuantityFound = function(){
					Uniware.Ajax.getJson("/data/reports/showItemTypeQuantityNotFoundDetails?itemSkuCode=" + self.itemTypeSkuCode,function(data){
						if(self.showDetails) {
							$(".markQuantity").html("show details");
							$("#itemTypeQuantityNotFoundTemplateDiv").html("");
							self.showDetails = false;
						} else {
							$(".markQuantity").html("hide details");
							self.itemDetails = data;
							self.renderItemQuantityDetailTemplate(self.itemDetails);
							self.showDetails = true;
						}
					},true);
				};
				
				this.markNotFoundQuantity = function () {
					var req = {
						'skuCode' : self.itemTypeSkuCode		
					};
					Uniware.Ajax.postJson("/data/reports/searchInventory", JSON.stringify(req), function(data){
						if(self.showInventoryDetails) {
							$(".markNotFoundQuantity").html("show details");
							$("#itemTypeInventoryTemplateDiv").html("");
							self.showInventoryDetails = false;
						} else {
							$(".markNotFoundQuantity").html("hide details");
							self.itemInventoryDetails = data;
							self.renderInventoryDetailTemplate(self.itemInventoryDetails);
							self.showInventoryDetails = true;
						}
					},true);
				};
				
				this.editItemQuantityNotFound = function(event){
					var index = $(event.target).parents('tr').attr('id');
					self.itemDetails.itemQuantityNotFoundDTO[index].isEditing = true;
					self.renderItemQuantityDetailTemplate(self.itemDetails);
				};
				
				this.editInventoryNotFound = function (event) {
					var index = $(event.target).parents('tr').attr('id');
					self.itemInventoryDetails.inventoryDTOs[index].isEditing = true;
					self.renderInventoryDetailTemplate(self.itemInventoryDetails);
				}
				
				this.cancel = function(event) {
					var index = $(event.target).parents('tr').attr('id');
					self.itemDetails.itemQuantityNotFoundDTO[index].isEditing = null; 
					self.renderItemQuantityDetailTemplate(self.itemDetails);
				};
				
				this.cancelNotFound = function (event) {
					var index = $(event.target).parents('tr').attr('id');
					self.itemInventoryDetails.inventoryDTOs[index].isEditing = null; 
					self.renderInventoryDetailTemplate(self.itemInventoryDetails);
				};
				
				this.updateItemTypeQuantityNotFound = function(event) {
					var index = $(event.target).parents('tr').attr('id');
					self.itemDetails.itemQuantityNotFoundDTO[index].isEditing = null;
					var req = {};
					req.itemSku = self.itemTypeSkuCode;
					req.quantityFound = $("#foundQuantity-"+index).val();
					req.shelfCode = self.itemDetails.itemQuantityNotFoundDTO[index].shelfCode;
					Uniware.Ajax.postJson("/data/reports/markQuantityFound", JSON.stringify(req), function(response) {
						if(response.successful) {
							Uniware.Utils.addNotification("Quantity Found Updated for shelf " + response.itemQuantityNotFoundDTO[index].shelfCode);
							self.getItemTypeDetails();
						} else {
							Uniware.Utils.showError(response.errors[0].description);
							self.renderItemQuantityDetailTemplate(self.itemDetails);
						}
					}, true);
				};
				
				this.updateInventoryNotFound = function(event) {
					var index = $(event.target).parents('tr').attr('id');
					self.itemInventoryDetails.inventoryDTOs[index].isEditing = null; 
					var req = {};
					req.itemSku = self.itemTypeSkuCode;
					req.quantityNotFound = $("#notFoundQuantity-"+index).val();
					req.shelfCode = self.itemInventoryDetails.inventoryDTOs[index].shelf;
					Uniware.Ajax.postJson("/data/reports/markQuantityNotFound", JSON.stringify(req), function(response) {
						if(response.successful) {
							Uniware.Utils.addNotification("Quantity Not Found Updated for shelf " + self.itemInventoryDetails.inventoryDTOs[index].shelfCode);
							self.getItemTypeDetails();
						} else {
							Uniware.Utils.showError(response.errors[0].description);
							self.renderInventoryDetailTemplate(self.itemInventoryDetails);
						}
					}, true);
				};
				
				this.renderItemQuantityDetailTemplate = function(data) {
					$('#itemTypeQuantityNotFoundTemplateDiv').html(template("itemTypeQuantityNotFoundTemplate",self.itemDetails));
					$(".update").click(self.updateItemTypeQuantityNotFound);
					$(".cancel").click(self.cancel);
					$("span.table-editable").click(self.editItemQuantityNotFound);
				};
				
				this.renderInventoryDetailTemplate = function(data) {
					$('#itemTypeInventoryTemplateDiv').html(template("itemTypeInventoryTemplate",self.itemInventoryDetails));
					$(".updateNotFound").click(self.updateInventoryNotFound);
					$(".cancelNotFound").click(self.cancelNotFound);
					$("span.editInventory").click(self.editInventoryNotFound);
				};
				
				this.searchItemType = function(itemSku) {
					Uniware.Ajax.getJson("/data/lookup/itemTypes?keyword=" + itemSku, function(data) {
						if(data.length == 1) {
							self.itemTypeId = data[0].itemTypeId;
							self.itemTypeSkuCode = data[0].itemSKU;
							self.getItemTypeDetails();
						}
					});
				};
			}
			
			$(document).ready(function() {
				window.page = new Uniware.SearchItemTypePage();
				window.page.init();
				if ($("#itemType").val() != '') {
					window.page.searchItemType($("#itemType").val());
				}
			});
		</script>
		</tiles:putAttribute>
</tiles:insertDefinition>
