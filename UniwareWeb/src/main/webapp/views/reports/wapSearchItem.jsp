<%@ include file="/tagIncludes.jsp"%>
<tiles:insertDefinition name=".wapPage">
	<tiles:putAttribute name="title" value="Uniware - View Item Details" />
	<tiles:putAttribute name="body">
		<br />
		<div class="greybor headlable ovrhid main-box-head">
			<h2 class="edithead head-textfields">Lookup > Scan Item</h2>
		</div>
		<div id="itemDiv" class="greybor round_bottom main-boform-cont ovrhid" style="padding: 10px;"></div>
	</tiles:putAttribute>
	<tiles:putAttribute name="deferredScript">
	<script id="itemTemplate" type="text/html">
	<table>
		<tr>
			<td width="60">Item: </td>
			<td><#= obj.item ? obj.item.code : '<input type="text" id="itemCode" class="w150 ucase"/>' #></td>
		</tr>
		<# if (obj.item) { #>
		<tr>
			<td>Item SkuCode: </td>
			<td><#=obj.item.itemSKU#></td>
		</tr>
		<tr>
			<td>Item Name: </td>
			<td><#=obj.item.itemTypeName#></td>
		</tr>
		<tr>
			<td>Status: </td>
			<td><#=obj.item.status#></td>
		</tr>
		<tr>
			<td>GRN Number: </td>
			<td><#=obj.item.inflowReceiptCode#></td>
		</tr>
		<tr>
			<td>Putaway Number: </td>
			<td><#=obj.item.lastPutawayCode#></td>
		</tr>
		<# if(obj.item.saleOrderItemDTOs !=null && obj.item.saleOrderItemDTOs.length > 0) { #>
			<tr>
				<td>Picklist Number: </td>
				<td><#=obj.item.saleOrderItemDTOs[0].picklistNumber#></td>
			</tr>
		<# } #>
		<# if (obj.item.status == 'CREATED') { #>
		<tr>
			<td></td>
			<td><div id="discardItem" class=" btn lfloat">Discard Item</div></td>
		</tr>
		<# }} #>
	</table>
	<div id="error" class="hidden errorField"></div>
	</script>
	<script type="text/javascript">
		Uniware.WapSearchItem = function() {
			var self = this;
			this.item;
			
			this.init = function() {
				self.render();
				$('#itemCode').focus().keyup(self.load);
			};
			
			this.load = function(event) {
				if (event.which == 13 && $('#itemCode').val() != '') {
					var req = {
						'itemCode': $('#itemCode').val()
					};
					Uniware.Ajax.postJson("/data/item/fetch", JSON.stringify(req), function(response) {
						if(response.successful == true) {
							self.item = response.itemDTO;
							self.render();
						} else {
							Uniware.Utils.showError(response.errors[0].description);
						}
						$('#itemCode').val('').focus();
					});
				}
			};

			this.discardItem = function(event) {
				$('#error').addClass('hidden');
				var req = {
					'itemCode' : self.item.code
				}
				Uniware.Ajax.postJson('/data/inflow/discardBarcode', JSON.stringify(req), function(response){
					if (response.successful) {
						self.item.status = 'DISCARDED';
						self.render();
					} else {
						$('#error').html(response.errors[0].description).removeClass('hidden');
					}
				});
			};
			
			this.render = function() {
				$('#itemDiv').html(template('itemTemplate', self));
				$('#discardItem').click(self.discardItem);
			};
			
		}
		
		$(document).ready(function() {
			window.page = new Uniware.WapSearchItem();
			window.page.init();
		});
	</script>
	</tiles:putAttribute>
</tiles:insertDefinition>
