<%@ include file="/tagIncludes.jsp"%>
<tiles:insertDefinition name=".tasksPage">
	<tiles:putAttribute name="title" value="Uniware - Notifications" />
	<tiles:putAttribute name="rightPane">
		<div id="pageBar">
			<div class="pageHeading"><span class="mainHeading">Notifications<span class="pipe">/</span></span><span class="subHeading"></span></div>
			<div class="pageControls"></div>
			<div id="retry" class="btn btn-small btn-primary rfloat" style="margin: 10px 5px;">retry</div>
            <div id="skip" class="btn btn-small btn-primary rfloat" style="margin: 10px 5px;">skip</div>
			<div class="clear"></div> 
		</div>
		<table id="flexme1" style="display: none"></table>
	</tiles:putAttribute>
	<tiles:putAttribute name="deferredScript">
    <sec:authentication property="principal" var="user" />
	<script type="text/javascript" src="${path.js('jquery/jquery.dataTables.min.js')}"></script>
	<script type="text/javascript">
		Uniware.NotificationsPage = function() {
			var self = this;
			this.name = 'DATATABLE NOTIFICATIONS';
			this.pageConfig = null;
			this.table = null;
			this.datatableViews = ${user.getDatatableViewsJson("DATATABLE NOTIFICATIONS")};
			this.init = function(){
				this.pageConfig = new Uniware.PageConfig(this.name,'#flexme1',this.datatableViews[0],{
					showCheckbox : true,
					showSerialNumber: false
				});
				if (this.datatableViews.length) {
					this.headConfig = new Uniware.HeadConfig(this.datatableViews,function(datatableObject){
						self.pageConfig.setView(datatableObject);
					},this.pageConfig);
				}
				$("#retry").click(self.retry);
				$("#skip").click(self.skip);
			};
			
			this.retry = function() {
				var selectedRows = self.pageConfig.getSelectedRecords();
				if (selectedRows.length == 0) {
					alert("Please select some notifications");
					return;
				}
				
				var req = {
						notificationIds : [],
						status : 'NEW'
				};
				for (var i=0;i<selectedRows.length;i++) {
					var notificationId = self.pageConfig.getColumnValue(selectedRows[i], 'notificationId');
					var notificationStatus = self.pageConfig.getColumnValue(selectedRows[i], 'status');
					if(notificationStatus == 'FAILED'){
						req.notificationIds.push(notificationId);						
					}
				}
				Uniware.Ajax.postJson("/data/tasks/notifications/retryOrSkip", JSON.stringify(req), function(response) {
					if (response.successful) {
						self.pageConfig.reload();
						Uniware.Utils.addNotification("Notifications status updated");
					} else {
						Uniware.Utils.showError("Notifications couldnt be updated");
					}
				}, true);
			};
			
			this.skip = function() {
				var selectedRows = self.pageConfig.getSelectedRecords();
				if (selectedRows.length == 0) {
					alert("Please select some notifications");
					return;
				}
				if (confirm("Are You Sure?")) {
					var req = {
						notificationIds : [],
						status : 'SKIPPED'
					};
					for (var i=0;i<selectedRows.length;i++) {
						var notificationId = self.pageConfig.getColumnValue(selectedRows[i], 'notificationId');
						var notificationStatus = self.pageConfig.getColumnValue(selectedRows[i], 'status');
						if(notificationStatus == 'FAILED'){
							req.notificationIds.push(notificationId);						
						}
					}
					Uniware.Ajax.postJson("/data/tasks/notifications/retryOrSkip", JSON.stringify(req), function(response) {
						if (response.successful) {
							self.pageConfig.reload();
							Uniware.Utils.addNotification("Notifications status updated");
						} else {
							Uniware.Utils.showError("Notifications couldnt be updated");
						}
					}, true);
				}
			};
			
		};
		$(document).ready(function() {
			window.page = new Uniware.NotificationsPage();
			window.page.init();
		});
	</script>
	</tiles:putAttribute>
</tiles:insertDefinition>
