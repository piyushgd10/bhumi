<%@ include file="/tagIncludes.jsp"%>
<tiles:insertDefinition name=".tasksPage">
	<tiles:putAttribute name="title" value="Uniware - All Alerts" />
	<tiles:putAttribute name="rightPane">
		<div>
			<div class="greybor headlable ovrhid main-box-head">
				<h2 class="edithead head-textfields">Subscribe to Alerts</h2>
			</div>
		</div>
		<div style="margin-top:5px; overflow: hidden; background:#f0eee8; padding:10px;">
			<div id="dataDiv" class="round_all" style="padding:20px; background:#f9f7f1">	</div>
		</div>

	</tiles:putAttribute>
	<tiles:putAttribute name="deferredScript">
		<script type="text/html" id="alertsTableTemplate">
		<table id="dataTable" width="100%" border="0" cellspacing="0" cellpadding="0" class="alert-table">
		<tr>
			<th width="35">SMS</th>
			<th width="40">EMAIL</th>
			<th></th>
			<th width="100">Frequency</th>
			<th width="100">Status</th>
			<th width="100">Action</th>
		</tr>
		<# for (var i = 0;i<obj.alertDTOs.length;i++) { var alertDTO = obj.alertDTOs[i]; #>
		<tr>
			<td><input class="alert-check" type="checkbox" /></td>
			<td><input class="alert-check subscribed" type="checkbox" <#=alertDTO.subscribed ? checked="checked" : ""#> /></td>
			<td>
				<div class="alert-head"><#=alertDTO.name#></div>
				<span class="alert-small"><#=alertDTO.description#></span>
			</td>
			<td><#=alertDTO.frequency#></td>
			<td>
			<# if(alertDTO.severity > 100) { #>
				<img src="/img/icons/alert-danger.png" />
			<# }else if(alertDTO.severity <= 100 && alertDTO.severity > 10){ #>	
				<img src="/img/icons/alert-info.png" />	
			<# }else{ #>
				<img src="/img/icons/alert-done.png" />	
			<# } #>		
			</td>
			<td>
				<a href="<#=alertDTO.actionPageUrl#>" class="btn btn-small action lfloat" style="text-decoration:none">Take Action</a>
			</td>
		</tr>			
		<# } #>
		</table>
		
		<div id="actionRow" class="hidden mar-15-top">
			<div id="update" class=" btn btn-primary lfloat">update</div>
			<div id="cancel" class="round_all link lfloat20" style="margin-top:5px;">cancel</div>
		</div>
		</script>
		<script type="text/javascript">
			Uniware.Alerts = function() {
				var self = this;
				this.alerts = ${alerts};

				this.init = function() {
					$("#dataDiv").html(template("alertsTableTemplate", self.alerts));
					$("input.alert-check").click(function(){
						$("#actionRow").removeClass('hidden');
					});
					$("#cancel").click(self.init);
					$("#update").click(self.update);
				};
				
				this.update = function() {
					var req = {
						alertSubscriptions : []
					}
					
					var length = null;
					$('.subscribed').each(function(index){
						length = req.alertSubscriptions.length;
						req.alertSubscriptions[length] = {};
						req.alertSubscriptions[length].alertTypeId = self.alerts.alertDTOs[index].alertId;
						req.alertSubscriptions[length].subscribed = $(this).is(":checked");
					});
					
					Uniware.Ajax.postJson("/data/tasks/alerts/subscription/update", JSON.stringify(req), function(response) {
						if(response.successful){
							$("#actionRow").addClass('hidden');
							Uniware.Utils.addNotification("Subscriptions has been updated successfully");
						}else{
							Uniware.Utils.showError(response.errors[0].description);
						}
					});
				};
				
			};
			
			$(document).ready(function() {
				window.page = new Uniware.Alerts();
				window.page.init();
			});
		</script>
	</tiles:putAttribute>
</tiles:insertDefinition>
