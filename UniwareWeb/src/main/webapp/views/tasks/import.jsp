<%@ include file="/tagIncludes.jsp"%>
<tiles:insertDefinition name=".tasksPage">
	<tiles:putAttribute name="title" value="Uniware - Import Configurations" />
	<tiles:putAttribute name="rightPane">
		<div class="greybor headlable ovrhid main-box-head">
			<h2 class="edithead head-textfields">Import Configurations</h2>
		</div>
		<div class="greybor round_bottom main-boform-cont ovrhid"  style="padding:20px;">
			<div class="pageLabel lfloat w200" style="margin-top:4px;">Select Import Type</div>
			<div class="lfloat">
				<select id="importJobType">
				<option value="">--select a type to import--</option>
				<c:forEach items="${configuration.getConfiguration('importJobConfiguration').importJobTypes }" var="jobType">
					<uc:security accessResource="${jobType.accessResourceName}">
					<option value="${jobType.name }">${jobType.name }</option>
					</uc:security>
				</c:forEach>
			</select>
			</div>
			<div class="clear"></div><br>
			<div id="importOptions"></div>
			<div class="clear"></div><br>
			<div id="columns"></div>
		</div>	

	</tiles:putAttribute>
	
	<tiles:putAttribute name="deferredScript">
	<script id="importOptionsTemplate" type="text/html">
		<div class="pageLabel lfloat w200">Import Options</div>
		<div class="lfloat">
			<select id="importOption">
				<option value="">-- select an option --</option>
				<# for (var i=0; i<obj.importOptions.length; i++) { #>
					<option <#=obj.importOptions.length==1?selected='selected':''#>><#=obj.importOptions[i]#></option>
				<# } #>
			</select>
		</div>
	</script>
	
	<script id="importJobTypeTemplate" type="text/html">
		<# if(obj.selectedOption != null) { #>
			<div class="clear"></div>
			<br/><br/>
	
			<div class="pageLabel lfloat w200">File</div>
			<div class="lfloat">
				<input type="file" name="file" id="importFile" class="lfloat mar-15-bot" />
			</div>
			<div class="clear"></div>
			<br/>
	
			<div class="pageLabel lfloat w200"></div>
			<div class=" btn btn-primary lfloat mar-15-bot" id="uploadFile">upload file</div>
			<div class="clear"></div><br/>
	
			<div class="greybor headlable round_top ovrhid">
				<h4 class="edithead">Format CSV File</h4>
				<div id="downloadCSV" class="link rfloat20">Download CSV format</div>
			</div>
			<div class="greybor round_bottom form-edit-table-cont">
				<table class="uniTable" border="0" cellspacing="0" cellpadding="0" style="font-size:12px">
					<tr>
						<th>Column Name</th>
						<th>Description</th>
						<th width="150">Required</th>
					</tr>
					<# for (var i = 0; i < obj.sourceColumns.length; i++) { var column = obj.sourceColumns[i]; #>
						<tr>
							<td><#=column.source#></td>
							<td><#=column.description#></td>
							<# if(obj.selectedOption == 'UPDATE_EXISTING') { #>
								<td align="center"><#=column.requiredInUpdate? '<img src="/img/icons/tick.png"/>':''#></td>
							<# } else { #>
								<td align="center"><#=column.required? '<img src="/img/icons/tick.png"/>':''#></td>
							<# } #>
						</tr>
					<# } #>
				</table>
			</div>
		<# } #>
	</script>
	<script type="text/javascript" src="${path.js('jquery/ajaxfileupload.js')}"></script>
	<script type="text/javascript">
		Uniware.ImportPage = function() {
			var self = this;
			this.jobTypeName;
			this.selectedOption;
			this.data;
			this.init = function() {
				$('#importJobType').change(self.getImportOptions);
			};
			
			this.upload = function(event){
				var importFile = $('#importFile').val();
				if(importFile == null || importFile == ""){
					Uniware.Utils.addNotification("Please enter a file path");
					event.stopPropagation();
				}else{
					var url = '';
					if ($('#importJobType').val().indexOf('Lazada') != -1) {
						var importType = $('#importJobType').val();
						url = "/lazada/import/catalog?importType=" + importType;
					} else {
						url = "/data/import/job/create?name=" + $('#importJobType').val() + "&importOption=" + self.data.selectedOption;
					}
					$.ajaxFileUpload({
						url : url,
						secureuri : true,
						fileElementId : 'importFile',
						dataType : 'json',
						success : function(data, status) {
							if(data.successful == true){
								Uniware.Utils.addNotification("Import Job created successfully. Import Job Id:" + data.importJobId + " ,Task Id:" + data.taskId);
								$('#importJobType').val("").trigger('change');
							}else{
								Uniware.Utils.showError(data.errors[0].description);	
							}
						}
					});	
				}
			};
			
			this.getImportOptions = function(){
				$('#importOptions').html('');
				$('#columns').html('');
				if ($(this).val() != "") {
					self.jobTypeName = $(this).val(); 
					Uniware.Ajax.getJson("/data/tasks/import/jobtype/fetch?jobTypeName=" + encodeURIComponent(self.jobTypeName) , function(response) {
						self.data = response;
						$('#importOptions').html(template("importOptionsTemplate", self.data));
						$('#importOption').change(self.listColumns);
						if(self.data.importOptions.length == 1){
							$('#importOption').trigger('change');	
						}
					});	
				}
			};
			
			this.listColumns = function(event) {
				self.data.selectedOption = null;
				$('#columns').html('');
				if ($(this).val() != "") {
					self.data.selectedOption = $(this).val(); 
					$('#columns').html(template("importJobTypeTemplate", self.data));
                    $("#uploadFile").click(function() {
                        self.upload();
                        document.getElementById("uploadFile").setAttribute('disabled', 'disabled');
                        setTimeout(function() {
                            	document.getElementById("uploadFile").removeAttribute('disabled');
                        	}, 3000 );
                    });
                    $('#downloadCSV').click(self.downloadCSV);
				}
			};
			
			this.downloadCSV = function(){
				 window.location.href = '/import/jobType/sampleCSV?legacy=1&jobTypeName=' + encodeURIComponent(self.jobTypeName) + "&importOption=" + self.data.selectedOption;
			}
		};
			
		$(document).ready(function() {
			window.page = new Uniware.ImportPage();
			window.page.init();
		});
	</script>
	</tiles:putAttribute>	
</tiles:insertDefinition>
