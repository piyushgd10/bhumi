<%@ include file="/tagIncludes.jsp"%>
<tiles:insertDefinition name=".tasksPage">
	<tiles:putAttribute name="title" value="Uniware - Datatable View Configurations" />
	<tiles:putAttribute name="rightPane">
		<sec:authentication property="principal" var="user" />
		<div class="greybor headlable ovrhid main-box-head">
			<h2 class="edithead head-textfields">Datatable View Configuration</h2>
		</div>
		<div style="padding:20px;">
			<div class="pageLabel lfloat" style="width:200px;margin-top:4px;">Datatable Type</div>
			<div class="lfloat">
				<select id="configName">
					<c:set var="exportConfigs" value="${configuration.getConfiguration('exportJobConfiguration').datatableConfigs}"></c:set>
					<option value="0"> -- Select Datatable --</option>
					<c:forEach items="${exportConfigs}" var="exportConfig">
					<uc:security accessResource="${exportConfig.accessResourceName}">
						<option value="${exportConfig.name}"><c:out value="${exportConfig.displayName}" /></option>
					</uc:security>
					</c:forEach>
				</select>
			</div>
			<div class="clearfix"></div>
		</div>	
		<div id="configDiv"></div>
	</tiles:putAttribute>
	<tiles:putAttribute name="deferredScript">
		<script type="text/html" id="configTemplate">
		<div class="headlable round_top ovrhid">
				<h2 class="edithead head-textfields">View</h2>
			</div>
			<div style="padding:20px;">
				<table cellpadding="10">
					<tr>
						<td class="pageLabel formLeft150" style="width:150px;margin-top:4px;">View Name</td>
						<td class="formRight lfloat">
							<input id="reportName" type="text"/>
						</td>
						<td class="pageLabel formLeft150" style="width:150px;margin-top:4px;">Default View</td>
						<td>
							<input id="defaultView" type="checkbox" style="margin-top:-1px;"/> 
						</td>
					</tr>
					<tr>
						<td class="pageLabel formLeft150" style="width:150px;margin-top:4px;">Results Per Page</td>
						<td class="formRight lfloat">
							<select id="rowsPerPage" >
								<option value="25" selected>25</option>
								<option value="50">50</option>
								<option value="100">100</option>
							</select>
						</td>
						<td class="pageLabel formLeft150" style="width:150px;margin-top:4px;">Sort Column</td>
						<td>
							<select id="sortColumn" >
								<option value="">---Select---</option>
								<# for (var i=0;i<obj.exportColumns.length;i++) { var exportColumn = obj.exportColumns[i]; #>
										<option value="<#=exportColumn.id#>"><#=exportColumn.name#></option>
								<# } #>
							</select> 
						</td>
						<td>
							<input id="descending" type="checkbox" style="margin-top:-1px;"/>Descending
						</td>
					</tr>
			</table>
			<div id="error" class="errorField invisible"></div>
		</div>
		<div class="headlable round_top ovrhid">
			<h2 class="edithead head-textfields">Columns</h2>
		</div>
		<div style="padding:20px;">
			<div class="lfloat"><em class="f12">Please Drag &amp; Drop to change the preference</em></div>
			<table class="center-fields-table" cellpadding="10" width="700" id="columnsDatatable">
			<tr class="invisible"><td width="50"></td><td width="200"></td><td></td></tr>
			<tr><th><input type="checkbox" id="all" style="margin-top:-1px;"></input> All
				</th><th>Name</th><th>Width</th></tr>
			<# for (var i=0;i<obj.exportColumns.length;i++) { var exportColumn = obj.exportColumns[i]; #>
				<tr>
				<div class="lfloat" style="font-size:12px;">
					<div class="lfloat">
						<td><input type="checkbox" id="column-<#=i#>" class="column"></input></td>
						<td style="margin:10px 10px 10px 0;font-size:14px;">
							<input type="text" id="name-<#=exportColumn.id#>" value="<#=exportColumn.name#>"/>
						</td>
						<td style="font-size:14px;">
							<input type="text" id="width-<#=exportColumn.id#>" value="<#=exportColumn.width#>"/>
						</td>
					</div>
				</div>
				</tr>
			<# } #>
			</table>
			<div class="clear"/>
		</div><br/>
		<div class="clear"/>
		<div class="headlable round_top ovrhid">
			<h2 class="edithead head-textfields">Filters</h2>
		</div>
		<div style="padding:20px;">
			<table class="fields-table" cellpadding="10">
			<tr class="invisible"><td width="50"></td><td width="200"></td><td></td></tr>
			<# for (var i=0;i<obj.exportFilters.length;i++) { var exportFilter = obj.exportFilters[i]; #>
				<tr>
					<td><input type="checkbox" class="filter" id="filter-<#=i#>"/></td>
					<td><#=exportFilter.name#></td>
					<# switch(exportFilter.type) { 
						case 'TEXT' : #>
							<td><input type="text"  id="filter-<#=exportFilter.id#>"/></td>

					<# break; case 'BOOLEAN' : #>
							<td><input type="checkbox" id="filter-<#=exportFilter.id#>"  /></td>
					<# break; case 'MULTISELECT' : #>
							<td><select id="filter-<#=exportFilter.id#>"  size="<#=exportFilter.selectItems.length > 5 ? 5 : exportFilter.selectItems.length#>" multiple="multiple">
								<# for(var j=0;j<exportFilter.selectItems.length;j++) { var selectItem = exportFilter.selectItems[j]; #>
								<option value="<#=selectItem.value#>"><#=selectItem.name#></option>
								<# } #>
							</select></td>
					<# break; case 'SELECT' : #>
							<td><select id="filter-<#=exportFilter.id#>" >
								<# for(var j=0;j<exportFilter.selectItems.length;j++) { var selectItem = exportFilter.selectItems[j]; #>
								<option value="<#=selectItem.value#>"><#=selectItem.name#></option>
								<# } #>
							</select></td>
					<# break; case 'DATERANGE' : #>
							<# if(obj.scheduling) { #>
								<td>
									<select id="filter-<#=exportFilter.id#>" >
										<option value="">---Select----</option>
										<option value="TODAY">TODAY</option>
										<option value="YESTERDAY">YESTERDAY</option>
										<option value="LAST_WEEK">LAST_WEEK</option>
										<option value="LAST_MONTH">LAST_MONTH</option>
										<option value="THIS_MONTH">THIS_MONTH</option>
									</select>
								</td>
							<# } else { #>
								<td><input type="text" class="dateRange datefield" id="filter-<#=exportFilter.id#>"/></td>
							<# } #>
					<# break; case 'DATETIME' : #>
							<td><input type="text" class="date datefield" id="filter-<#=exportFilter.id#>"/></td>
					<# } #>
				</tr>
			<# } #>
			</table>
		</div>
		<div class="clear"/>
		<div class="lfloat20">
			<br/><br/>
			<div id="createJob" class="btn btn-primary lfloat ">Create Datatable View</div>
		</div>
		<div class="clear"/>
		</script>	
		<script type="text/javascript">
			Uniware.ExportConfigPage = function() {
				var self = this;
				this.config = null;
				
				this.init = function() {
					$('#configName').change(function() {
						$('#configDiv').addClass('hidden');
						if ($(this).val() != 0) {
							Uniware.Ajax.getJson("/data/tasks/export/config/get?exportConfigName=" + $(this).val(), self.onConfigFetch);
						}
					});
					
				};
				
				
				this.onConfigFetch = function(config) {
					self.config = config;
					$("#configDiv").html(template("configTemplate", self.config)).removeClass('hidden');
					$("#columnsDatatable").sortable({ items: "tr:not(.ui-state-disabled)"});
					$("#all").click(function(){
						$("input.column").attr('checked',$(this).is(':checked'));
					});
					
					$('.dateRange').each(function(){
						$(this).daterangepicker();
					});
					
					$('.date').datepicker({
							dateFormat : 'dd/mm/yy'
					});
										
					$("#createJob").click(function(){
						var index, filter, selectedFilters, selectedColumns, req, datatableView;
						selectedColumns = $("input.column:checked");
						var sortColumn = {
								'column' : $("#sortColumn").val(),
								'descending' : $("#descending").is(':checked'),
						}
						datatableView = {
								'resultsPerPage' : $("#rowsPerPage").val(),
								'columns' : [],
								'filters' : [],
								'sortColumns' : [],
						};
						
						datatableView.sortColumns.push(sortColumn);
						
						selectedColumns.each(function() {
							var index = parseInt($(this).attr('id').substring($(this).attr('id').indexOf('-') + 1));
							var column = self.config.exportColumns[index];
							var exportColumn = {name : column.id};
							
							exportColumn.displayName = $('#name-'+column.id).val();
							exportColumn.width = $('#width-'+column.id).val();
							
							datatableView.columns.push(exportColumn);
						});
						
						selectedFilters = $("input.filter:checked");
						selectedFilters.each(function() {
							var index = parseInt($(this).attr('id').substring($(this).attr('id').indexOf('-') + 1));
							var filter = self.config.exportFilters[index];
							
							var exportFilters = {
									id : self.config.exportFilters[index].id
							};
						  
							switch (filter.type) {
							case 'TEXT' : 
								exportFilters.text = $('#filter-' + exportFilters.id).val();
								break;
							case 'BOOLEAN' :
								exportFilters.checked = $('#filter-' + exportFilters.id).is(':checked');
								break;
							case 'DATETIME' :
								var dateTime = $('#filter-' + exportFilters.id).val();
								exportFilters.dateTime = Date.fromPaddedDate(dateTime).getTime();
								break;
							case 'DATERANGE' :
								var dateRange = $('#filter-' + exportFilters.id).val();
								exportFilters.dateRange = {};
								if (self.scheduling) {
									exportFilters.dateRange.textRange = $('#filter-' + exportFilters.id).val();
								} else if (dateRange != '') {
									exportFilters.dateRange.start = Date.fromPaddedDate(dateRange.substring(0, 10)).getTime();
									if (dateRange.length > 10) {
										exportFilters.dateRange.end = Date.fromPaddedDate(dateRange.substring(13));
									} else {
										exportFilters.dateRange.end = Date.fromPaddedDate(dateRange.substring(0, 10));
									}
									exportFilters.dateRange.end = exportFilters.dateRange.end.setDate(exportFilters.dateRange.end.getDate()+1);
								}
								break;
							case 'SELECT' :
								exportFilters.selectedValue = $('#filter-' + exportFilters.id).val();
								break;
							case 'MULTISELECT' :
								exportFilters.selectedValues = $('#filter-' + exportFilters.id).val();
								break;
							}
							datatableView.filters.push(exportFilters);
						});

						req = {
								'datatableName' : $("#configName").val(),
								'dataTableView' : datatableView,
								'name' : $('#reportName').val(),
								'isDefaultView' : $('#defaultView').is(':checked'),
						};
						
						Uniware.Ajax.postJson("/data/user/datatableView/create", JSON.stringify(req), function(response) {
							if (response.successful == false) {
								Uniware.Utils.showError(response.errors[0].description);
							} else {
								Uniware.Utils.addNotification('Datatable view has been created.');
								$('#configName').val('0').trigger('change');
							}
						}, true);
						
					});
				};
				
				
			};
			
			$(document).ready(function() {
				window.page = new Uniware.ExportConfigPage();
				window.page.init();
				var configName = '${param['name']}';
				if (configName != '') {
					$('#configName').val(configName).trigger('change');
				}				
			});
		</script>
	</tiles:putAttribute>
</tiles:insertDefinition>
