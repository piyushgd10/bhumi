<%@ include file="/tagIncludes.jsp"%>
<tiles:insertDefinition name=".tasksPage">
	<tiles:putAttribute name="title" value="Uniware - Export Subscription" />
	<tiles:putAttribute name="rightPane">
		<div id="subscribeUsersDiv" class="lb-over">
			<div class="lb-over-inner round_all">
				<div style="margin: 40px; line-height: 30px;">
					<div class="pageHeading lfloat">Add Users</div>
					<div id="subscribeUsersDiv_close" class="link rfloat20">close</div>
					<div class="clear"></div>
					<div id="selectedUsersDiv"></div>
					<div class="clear"></div>
					<div><input type="text" id="usersExportSubscribe" placeHolder="user"/></div>
					<div class="clear"></div>
					<div><br/><input type="button" class="btn btn-primary" id="subscribeUserToExports" value="submit"/></div>
				</div>
			</div>
		</div>
		<div>
			<div class="greybor headlable ovrhid main-box-head">
				<h2 class="edithead head-textfields">Subscribe to Exports</h2>
			</div>
		</div>
		<div id="dataDiv" style="margin-top:5px; overflow: hidden; padding:20px;">
		</div>
	</tiles:putAttribute>
	<tiles:putAttribute name="deferredScript">
        <script type="text/javascript" src="${path.js('unifier/later.min.js')}"></script>
        <script type="text/javascript" src="${path.js('unifier/moment.min.js')}"></script>
        <script type="text/javascript" src="${path.js('unifier/prettycron.js')}"></script>
		<script type="text/html" id="exportsTableTemplate">
		<table style="text-align:center;" id="dataTable" width="100%" border="0" cellspacing="1" class="table table-striped table-bordered table-hover" cellpadding="10">
		<tr>
			<th>Export Type</th>
			<th>Report Name</th>
			<th>Created By</th>
			<th>Frequency</th>
			<th>Scheduled Time</th>
			<th>Subscribed</th>
			<uc:security accessResource="SUBSCRIBE_EXPORT">
		      <th>Subscribe Users</th>
			  <th>Delete Export</th>
			</uc:security>
		</tr>
		<# for (var i = 0;i<obj.exportJobs.length;i++) { var exportJob = obj.exportJobs[i]; #>
		<tr id="tr-<#=i#>">
			<td class="breakWord"><#=exportJob.exportJobTypeName#></td>
			<td class="breakWord"><#=exportJob.reportName#></td>
			<td class="breakWord"><#=exportJob.userName#></td>
			<td><#=prettyCron.toString(exportJob.frequency)#></td>
			<td><#=exportJob.scheduledTime#></td>
			<td><input class="export-check subscribed" type="checkbox" <#=exportJob.subscribed ? checked="checked" : ""#> /></td>
			<uc:security accessResource="SUBSCRIBE_EXPORT">
		      <td><input type="button" class="subscribeUsers btn btn-primary btn-small" value="Subscribe Users" /></td>
			  <td>
				<div id="deleteRow-<#=i#>">
					<div id="deleteExport-<#=i#>" class=" btn btn-danger btn-mini export-delete">Delete</div>
				</div>	
			  </td>
			</uc:security>
		</tr>			
		<# } #>
		</table>
			<div id="actionRow" class="hidden">
				<div id="update" class=" btn btn-success">update</div>
			    <div id="cancel" class="btn btn-danger">cancel</div>
			</div>
		</script>
		<script type="text/javascript">
			Uniware.Exports = function() {
				var self = this;
				Uniware.Ajax.getJson("/data/tasks/export/getExportsForCurrentUser", function(response){
					self.exports=response;
				});
				this.selectedUsers = {};
				this.exportJobId;
				this.divId;
				this.data;

				this.init = function() {
					Uniware.Ajax.getJson("/data/tasks/export/getExportsForCurrentUser", function(response){
						self.exports=response;
						$("#dataDiv").html(template("exportsTableTemplate", self.exports));
						$(".subscribeUsers").click(self.subscribeUser);
						$("input.export-check").click(function(){
							$("#actionRow").removeClass('hidden');
						});
						$("div.export-delete").click(self.deleteExport);
						$("#cancel").click(self.init);
						$("#update").click(self.update);
						$('#subscribeUserToExports').click(self.subscribeUserToExports);
					});
				};
				
				this.subscribeUser = function(event){
					var tr = $(event.target).parents('tr');
					var index = $(tr[0]).attr('id').split('-')[1];
					var exportJobRow = self.exports.exportJobs[index];
					self.exportJobId = exportJobRow.exportJobId;
					$('#selectedUsersDiv').html('');
					Uniware.Ajax.getJson("/data/tasks/export/subscribedUsers?exportJobId=" + self.exportJobId, function(response){
						for(var i=0;i<response.length;i++) {
							self.selectedUsers[response[i].id] = response[i].userName;
							$('#selectedUsersDiv').append("<div id='div-"+response[i].id+"' style='float:left;'>"+ response[i].userName +"  <a id='"+response[i].id+"'><i class='icon-remove' /></a>,</div>");
							$('#'+response[i].id+'').click(self.removeDiv);
						}
						Uniware.LightBox.show("#subscribeUsersDiv",null,null,self.flushSelectedUSers);
						$('#usersExportSubscribe').autocomplete({
							minLength :2,
							mustMatch :true,
							autoFocus :true,
							source : function(request,response){
								Uniware.Ajax.getJson("/data/lookup/users?name="+request.term,function(data){
									response($.map(data,function(item){
										return {
											label: item.userName,
											value: item.value,
											itemDetail : item
										}	
									}));
								});
							},
							select: function( event, ui ) {
								self.selectedUsers[ui.item.itemDetail.id] = ui.item.value;
								$('#selectedUsersDiv').append("<div id='div-"+ui.item.itemDetail.id+"' style='float:left;'>"+ ui.item.value +"  <a id='"+ui.item.itemDetail.id+"'><i class='icon-remove'/></a>,</div>" );
								$('#usersExportSubscribe').val('');
								$('#'+ui.item.itemDetail.id+'').click(self.removeDiv);
								return false;
							}
						});
						
					});
				};
				
				this.flushSelectedUSers = function() {
					self.selectedUsers = {};
				}
				
				this.removeDiv = function(){
					$('#div-' + $(this).attr('id')).remove();
					var id = $(this).attr('id');
					delete self.selectedUsers[$(this).attr('id')];
				};
			
				this.subscribeUserToExports = function(){
						$('#selectedUsersDiv').html('');
						var usernames = [];
						for(var userId in self.selectedUsers) {
							usernames.push(self.selectedUsers[userId]);
						}
						self.selectedUsers = {};
						var req = {
							'subscribingUsers' : usernames,
							'exportJobId' : self.exportJobId
						};
						Uniware.Ajax.postJson("/data/tasks/exports/config/subscribeUsers", JSON.stringify(req), function(response){
							if (response.successful == false) {
								Uniware.Utils.showError(response.errors[0].description);
							} else {
								window.location.href = "/tasks/exportSubscribe?legacy=1";
								Uniware.Utils.addNotification("Selected users subscribed for exports");
							}
						},true);
				};
				
				this.deleteExport = function(event) {
					var tr = $(event.target).parents('tr');
					var index = tr[0].id.split('-')[1];
					var exportJobRow = self.exports.exportJobs[index];
					self.exportJobId = exportJobRow.exportJobId;
					var req = {
						exportJobId : self.exportJobId
					}
					if(confirm("Are you Sure?"))
					Uniware.Ajax.postJson("/data/tasks/exports/subscription/delete", JSON.stringify(req), function(response) {
						if(response.successful){
							$("#deleteRow").addClass('hidden');
							window.location.reload();
							Uniware.Utils.addNotification("Export has been deleted successfully");
						} else{
							Uniware.Utils.showError(response.errors[0].description);
						}
					});
				};
				
				this.editTask = function(event){
					var tr = $(event.target).parents('tr');	
					var index = $('.action').index(this);
					var enabled = self.exports.exportJobs[index].enabled;
					var req = {
							exportJobId : self.exports.exportJobs[index].exportJobId,
							enabled : !enabled
					}
					Uniware.Ajax.postJson("/data/tasks/export/recurrent/edit", JSON.stringify(req), function(response) {
							if(response.successful){
								Uniware.Utils.addNotification("Task has been updated successfully");
								self.exports.exportJobs[index].enabled= !enabled;
								self.init();
							}
					});
				};
				
				this.update = function() {
					var req = {
						exportSubscriptions : []
					}
					var length = null;
					$('.subscribed').each(function(index){
						length = req.exportSubscriptions.length;
						req.exportSubscriptions[length] = {};
						req.exportSubscriptions[length].exportJobId = self.exports.exportJobs[index].exportJobId;
						req.exportSubscriptions[length].subscribed = $(this).is(":checked");
					});
					Uniware.Ajax.postJson("/data/tasks/exports/subscription/update", JSON.stringify(req), function(response) {
						if(response.successful){
							$("#actionRow").addClass('hidden');
							Uniware.Utils.addNotification("Subscriptions has been updated successfully");
						}else{
							Uniware.Utils.showError(response.errors[0].description);
						}
					});
				};
				
			};
			
			$(document).ready(function() {
				window.page = new Uniware.Exports();
				window.page.init();
			});
		</script>
	</tiles:putAttribute>
</tiles:insertDefinition>
