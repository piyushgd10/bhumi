<%@ include file="/tagIncludes.jsp"%>
<tiles:insertDefinition name=".tasksPage">
	<tiles:putAttribute name="title" value="Uniware - Recurrent Tasks" />
	<tiles:putAttribute name="rightPane">
		<div id="ltTaskResultDiv" class="lb-over">
			<div class="lb-over-inner round_all">
				<div id="taskResultDiv" style="margin: 40px;">
				</div>
			</div>
		</div>
		<div id="ltEditParameterDiv" class="lb-over">
			<div class="lb-over-inner round_all">
				<div id="editParameterDiv" style="margin: 40px;">
				</div>
			</div>
		</div>
		<div>
			<div class="greybor headlable round_top ovrhid main-box-head">
				<h2 class="edithead head-textfields">Recurrent Tasks</h2>
			</div>
		</div>
		<div class="clear"></div>
		<div style="margin-top:5px; overflow: hidden; background:#f0eee8; padding:10px;">
			<div id="dataDiv" class="round_all" style="padding:20px; background:#f9f7f1">
				<form onsubmit="javascript : return false;">
					<table id="dataTable" class="dataTable alert-table"></table>
				</form>
			</div>
		</div>
	</tiles:putAttribute>
	<tiles:putAttribute name="deferredScript">
		<script type="text/javascript" src="${path.js('jquery/jquery.dataTables.min.js')}"></script>
		<script type="text/html" id="tasks">
			<table class="uniTable greybor" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<th width="250px">Task Name</th>
					<th width="100px">Cron Expression</th>
					<th>Last Execution<br/> Time</th>
					<th>Task Result</th>
					<th>Current Status</th>
					<th>Percentage Completed</th>
					<th>Operations</th>
				</tr>
				<# if(obj.recurrentTasks != null) { #>
					<tr bgcolor="#F5F5F5" class="alert-head"><td colspan="7">RECURRENT TASKS</td></tr>
					<# } #>

						<# for(var taskId in obj.recurrentTasks){ var task = obj.recurrentTasks[taskId];#>
							<tr id="<#=task.type#>-<#=task.id#>"><#=template("taskRow", task)#></tr>
							<# } #>

								<# if(obj.runtimeTasks != null) { #>
									<tr bgcolor="#F5F5F5" class="alert-head"><td colspan="7">RUNTIME TASKS</td></tr>
									<# } #>

										<# for(var taskId in obj.runtimeTasks){ var task = obj.runtimeTasks[taskId];#>
											<tr id="<#=task.type#>-<#=task.id#>"><#=template("taskRow", task)#></tr>
											<# } #>
			</table>
		</script>
		<script type="text/html" id="taskRow">
			<td>
				<#=obj.name#>
					<# if(Uniware.Utils.runTimeTask) { #>
						<span class="editTaskParams editable" title="Edit Task Parameters" style="margin-left:5px;"></span>
						<# } #>
			</td>
			<td><#=obj.delay#></td>
			<td><#=new Date(obj.lastExecTime).toDateTime()#></td>
			<#if(obj.taskResult != null) { #>
				<td style="line-height:12px;">
					<#=obj.taskResult.message#>
						<#if(obj.type == 'RECURRENT') { #>
							<div class="recurrentDetailedMessage" style="cursor:pointer; text-decoration:underline;">Detailed Message</div>
							<# } else { #>
								<div class="runtimeDetailedMessage" style="cursor:pointer; text-decoration:underline;">Detailed Message</div>
								<# } #>
				</td>
				<# } else { #>
					<td></td>
					<# } #>
						<td  style="line-height:12px;"><#=obj.currentStatus#></td>
						<td>
							<div style="text-align:center;width:100%"><#=obj.percentageCompleted#>%</div>
							<div style="border:1px solid #ccc;background:#eee;width:90%;height:15px;overflow:hidden;" class="round_all">
								<#if(obj.taskResult && obj.taskResult.failed) { #>
									<div style="width:<#=obj.percentageCompleted#>%;height:15px;" class="round_all bg-red">&#160;</div>&#160;
									<# } else { #>
										<div style="width:<#=obj.percentageCompleted#>%;height:15px;" class="round_all bg-green">&#160;</div>&#160;
										<# } #>
							</div>
						</td>
						<#if(obj.type!="RUNTIME" && obj.enabled == true) { #>
							<td class="section-operations">
								<# if(Uniware.Utils.runTimeTask) { #>
									<div class="btn btn-small lfloat runtask">Run it Now</div>
									<# } else { #>
										<div class="runtask"></div>
										<# } #>
							</td>
							<# } else { #>
								<td></td>
								<# } #>
		</script>

		<script type="text/html" id="taskResultTemplate">
			<div class="pageHeading lfloat" style=>Task Result</div>
			<div class="clear"></div>
			<br />

			<div class="lfloat" style="font-size:12px;">
				<#var flag = false; #>
					<# for(var key in obj) {#>
						<# flag= "true"; #>
							<div class="lfloat"><#= key #>:</div>
							<div class="formRight lfloat"><#= JSON.stringify(obj[key]) #></div>
							<div class="clear"></div>
							<# } #>
								<# if(flag == false) { #>
									Task Result Not Available
									<# } #>
			</div>
			<div class="clear"></div>

			<br />
			<div class="formLeft150 lfloat">&#160;</div>
			<div class="formRight lfloat"><input type="submit" id="ltTaskResultDiv_close" class=" btn btn-small btn-primary lfloat" value="OK"/></div>
			<div class="clear"></div>
		</script>
		<script type="text/html" id="editParameterTemplate">
			<div class="pageHeading lfloat">Edit Task Parameter</div>
			<div id="ltEditParameterDiv_close" class="link rfloat">close</div>

			<div class="clear"></div><br/>
			<div class="lfloat formLeft150  bold">Task Name</div>
			<div class="lfloat10"><input id="taskName" type="text" value="<#= obj.name #>" readOnly="readOnly" /></div>

			<div class="clear"></div><br/>
			<div class="lfloat formLeft150  bold">Cron Expression</div>
			<div class="lfloat10">
				<input id="delay" type="text" value="<#=obj.delay #>" />
			</div>

			<div class="clear"></div><br/>
			<div class="lfloat formLeft150 bold">Enabled</div>
			<div class="lfloat10">
				<input id="enabled"  type="checkbox"  <#=obj.enabled ? checked="checked" : '' #>/>
			</div>

			<# if(obj.taskParameters.length > 0) { #>
				<# for(var i=0; i < obj.taskParameters.length; i++){ #>
				<div id="<#= obj.taskParameters[i].name#>"><#=template("parameterRow", obj.taskParameters[i])#>
					<# } #>
						<# } else { #>
							<div class="clear"></div><br/>
							<div class="formleft150 lfloat">No Parameters exist for this task</div>
							<# } #>

								<div class="clear"></div><br/>
								<div class="formLeft150 lfloat">&#160;</div>
								<div class="lfloat10"><input type="submit" id="update" class=" btn btn-small btn-primary lfloat" value="update"/></div>
								<div class="clear"></div>

								<div id="errorTaskParameter" class="errorField" style="margin-left: 20px;"></div>
								<br />
		</script>
		<script type="text/html" id="parameterRow">
			<div class="clear"></div><br />
			<div class="formLeft100 lfloat"><#=obj.name#></div>
			<div class="clear"></div>
			<div class="lfloat">
				<textarea id="name-<#=obj.name#>" style="height: 40px; width: 420px;"><#=Uniware.Utils.escapeHtml(obj.value)#></textarea>
			</div>
			<div class="clear"></div>
		</script>
		<script type="text/javascript">
			Uniware.RecurrentTask = function() {
				<uc:security accessResource="RUN_RECURRENT_TASK">
				Uniware.Utils.runTimeTask = true;
				</uc:security>

				var self = this;
				this.tasks = null;
				this.pendingRuntimeTasks = {};
				this.pendingRecurrentTasks = {};
				this.init = function() {
					self.listTasks();
				};

				this.listTasks = function(){
					Uniware.Ajax.getJson("/data/tasks/recurrentTasks/fetch", function(response) {
						self.tasks = response;
						for(var taskId in  response.runtimeTasks){
							if(response.runtimeTasks[taskId].percentageCompleted <100){
								self.pendingRuntimeTasks[response.runtimeTasks[taskId].id] = response.runtimeTasks[taskId];
							}
						}
						for(var taskId in  response.recurrentTasks){
							if(response.recurrentTasks[taskId].percentageCompleted <100){
								self.pendingRecurrentTasks[response.recurrentTasks[taskId].id] = response.recurrentTasks[taskId];
							}
						}
						self.initTable();
					});
				};

				this.initTable = function(){
					$('#dataTable').html(template("tasks", self.tasks));

					$("#dataTable tbody").on('click', 'div.runtask', function(event) {
						self.runTask(event);
					});
					$("#dataTable tbody").on('click', 'div.recurrentDetailedMessage', function(event) {
						self.showRecurrentTaskResult(event);
					});
					$("#dataTable tbody").on('click', 'div.runtimeDetailedMessage', function(event) {
						self.showRuntimeTaskResult(event);
					});
					$("#dataTable tbody").on('click', 'span.editTaskParams', function(event) {
						self.showEditTask(event);
					});
				};

				this.showEditTask = function(event) {
					var tr = $(event.target).parents('tr');
					var task = tr[0].id.split("-");
					if(task[0] == 'RECURRENT') {
						var taskName = self.tasks.recurrentTasks[task[1]].name;
					} else {
						var taskName = self.tasks.runtimeTasks[task[1]].name;
					}
					var requestObject = {
						'taskName' : taskName
					};
					Uniware.Ajax.postJson("/data/admin/system/task/get", JSON.stringify(requestObject), function(response){
						if(response.successful == false) {
							Uniware.Utils.showError(response.errors[0].description);
						} else {
							self.task = response.task;
							self.taskParams = self.task.taskParameters;
							$("#editParameterDiv").html(template("editParameterTemplate",self.task)).show();
							Uniware.LightBox.show('#ltEditParameterDiv');
							$('#update').click(self.update);
						}
					});
				};

				this.update = function() {
					var requestObject = {};
					requestObject = {
						'taskName': self.task.name,
						'enabled': $('#enabled').is(":checked"),
						'delay': $('#delay').val() * 60
					};
					requestObject.parameters = {};
					for(var i=0; i < self.taskParams.length; i++){
						requestObject.parameters[self.taskParams[i].name] = $('#name-' + self.taskParams[i].name).val();
					}

					Uniware.Ajax.postJson("/data/admin/system/task/update", JSON.stringify(requestObject), function(response){
						if(response.successful == false) {
							$("#errorTaskParameter").html(response.errors[0].description.split('|')[0]).css('visibility', 'visible');
						} else {
							Uniware.LightBox.hide();
							Uniware.Utils.addNotification("'" + response.task+ "'" + " Task has been updated successfully");
							self.initTable();
						}
					});
				};

				this.showRecurrentTaskResult = function(event) {
					var tr = $(event.target).parents('tr');
					var taskId = tr[0].id.split("-")[1];
					Uniware.Ajax.getJson("/data/tasks/recurrentTask/fetchResult?taskId=" + taskId, function(response) {
						$("#taskResultDiv").html(template("taskResultTemplate",response)).show();
						Uniware.LightBox.show('#ltTaskResultDiv');
					});

				};

				this.showRuntimeTaskResult = function(event) {
					var tr = $(event.target).parents('tr');
					var taskId = tr[0].id.split("-")[1];
					Uniware.Ajax.getJson("/data/tasks/runtimeTask/fetchResult?taskId=" + taskId, function(response) {
						$("#taskResultDiv").html(template("taskResultTemplate",response)).show();
						Uniware.LightBox.show('#ltTaskResultDiv');
					});

				};

				this.runTask = function(event) {
					var tr = $(event.target).parents('tr');
					var trId = tr[0].id;
					var taskId = trId.substring(trId.indexOf("-")+1);
					Uniware.Ajax.getJson("/data/tasks/recurrentTask/run?taskId=" + taskId, function(response) {
						self.tasks.runtimeTasks = response;
						for(var id in response){
							if(response[id].percentageCompleted <100){
								self.pendingRuntimeTasks[id] = response[id];
							}
						}
						clearTimeout(self.timer);
						self.initTable();
						Uniware.Utils.addNotification(self.tasks.recurrentTasks[taskId].name + " task has been run successfully");
					});
				};
			};

			$(document).ready(function() {
				window.page = new Uniware.RecurrentTask();
				window.page.init();
			});
		</script>
	</tiles:putAttribute>
</tiles:insertDefinition>
