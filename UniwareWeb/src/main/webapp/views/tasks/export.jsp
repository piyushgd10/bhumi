<%@ include file="/tagIncludes.jsp"%>
<tiles:insertDefinition name=".tasksPage">
	<tiles:putAttribute name="title" value="Uniware - Export Configurations" />
	<tiles:putAttribute name="rightPane">
		<sec:authentication property="principal" var="user" />
		<div id="scheduleExportsDiv" class="lb-over">
			<div class="lb-over-inner round_all">
				<div style="margin: 40px; line-height: 30px;">
					<div class="pageHeading lfloat">Schedule Exports</div>
					<div id="scheduleExportsDiv_close" class="link rfloat20">close</div>
					<div class="clear"></div>
					<div id="selectedUsers"></div>
					<div class="clear"></div>
					<div id="scheduleExportsTemplateDiv" style="margin: 20px 0;"></div>
				</div>
			</div>
		</div>
		<div class="greybor headlable ovrhid main-box-head">
			<h2 class="edithead head-textfields">Export Configurations</h2>
		</div>
		<div style="padding:20px;">
			<div class="pageLabel lfloat" style="width:200px;margin-top:4px;">Export Type</div>
			<div class="lfloat">
				<select id="configName">
					<c:set var="exportConfigs" value="${configuration.getConfiguration('exportJobConfiguration').exportConfigs}"></c:set>
					<option value="0"> -- select a type to export --</option>
					<c:forEach items="${exportConfigs}" var="exportConfig">
					<uc:security accessResource="${exportConfig.accessResourceName}">
						<option value="${exportConfig.name}"><c:out value="${exportConfig.name}" /></option>
					</uc:security>
					</c:forEach>
				</select>
			</div>
			<uc:security accessResource="SUBSCRIBE_EXPORT">
				<div id="scheduleExports" class="btn btn-primary btn-small rfloat20 invisible">create scheduled report</div>
				<div class="clear"></div><br/>
			</uc:security>
			<div class="clearfix"></div>
		</div>	
		<div id="scheduleConfigDiv"></div>
		<div id="configDiv"></div>
	</tiles:putAttribute>
	<tiles:putAttribute name="deferredScript">
		<script type="text/html" id="configTemplate">
		<# if(obj.scheduling) { #>
		<div class="headlable round_top ovrhid">
				<h2 class="edithead head-textfields">Schedule</h2>
			</div>
			<div style="padding:20px;">
				<table cellpadding="10">
					<tr>
						<td class="pageLabel formLeft150" style="width:150px;margin-top:4px;">Report Name</td>
						<td class="formRight lfloat">
							<input id="reportName" type="text"/>
						</td>
						<td class="pageLabel formLeft150" style="width:150px;margin-top:4px;">Frequency</td>
                        <td colspan="2">
                            <input id="cron-val" type="hidden"/>
                            <div id="selector">
                        </td>
					</tr>
			</table>
				<div id="error" class="errorField invisible"></div>
		</div>
		<# } #>
		<div class="headlable round_top ovrhid">
			<h2 class="edithead head-textfields">Columns</h2>
		</div>
		<div style="padding:20px;">
			<div class="lfloat" style="margin:10px 10px 10px 0;font-size:12px;">
				<div class="lfloat"><input type="checkbox" id="all" style="margin-top:-1px;"></input> All</div>
			</div>
			<# for (var i=0;i<obj.exportColumns.length;i++) { var exportColumn = obj.exportColumns[i]; #>
				<# if(exportColumn.exportable) { #>
					<div class="lfloat" style="margin:10px 10px 10px 0;font-size:12px;">
						<div class="lfloat">
							<input type="checkbox" id="column-<#=exportColumn.id#>" class="column" style="margin-top:-1px;"></input>
							 <#=exportColumn.name#>
						</div>
					</div>
				<# } #>
			<# } #>
			<div class="clear"/>
		</div><br/>
		<div class="clear"/>
		<div class="headlable round_top ovrhid">
			<h2 class="edithead head-textfields">Filters</h2>
		</div>
		<div style="padding:20px;">
			<table class="fields-table" cellpadding="10">
			<tr class="invisible"><td width="50"></td><td width="200"></td><td></td></tr>
			<# for (var i=0;i<obj.exportFilters.length;i++) { var exportFilter = obj.exportFilters[i]; #>
				<tr>
					<td><input type="checkbox" class="filter" id="filter-<#=i#>"/></td>
					<td><#=exportFilter.name#></td>
					<# switch(exportFilter.type) { 
						case 'TEXT' : #>
							<td><input type="text"  id="filter-<#=exportFilter.id#>"/></td>

					<# break; case 'BOOLEAN' : #>
							<td><input type="checkbox" id="filter-<#=exportFilter.id#>"  /></td>
					<# break; case 'MULTISELECT' : #>
							<td><select id="filter-<#=exportFilter.id#>"  size="<#=exportFilter.selectItems.length > 5 ? 5 : exportFilter.selectItems.length#>" multiple="multiple">
								<# for(var j=0;j<exportFilter.selectItems.length;j++) { var selectItem = exportFilter.selectItems[j]; #>
								<option value="<#=selectItem.value#>"><#=selectItem.name#></option>
								<# } #>
							</select></td>
					<# break; case 'SELECT' : #>
							<td><select id="filter-<#=exportFilter.id#>" >
								<# for(var j=0;j<exportFilter.selectItems.length;j++) { var selectItem = exportFilter.selectItems[j]; #>
								<option value="<#=selectItem.value#>"><#=selectItem.name#></option>
								<# } #>
							</select></td>
					<# break; case 'DATERANGE' : #>
							<# if(obj.scheduling) { #>
								<td>
									<select id="filter-<#=exportFilter.id#>" >
										<option value="">---Select----</option>
										<option value="TODAY">TODAY</option>
										<option value="YESTERDAY">YESTERDAY</option>
										<option value="LAST_7_DAYS">LAST_7_DAYS</option>
										<option value="LAST_WEEK">LAST_WEEK</option>
										<option value="LAST_MONTH">LAST_MONTH</option>
										<option value="THIS_MONTH">THIS_MONTH</option>
									</select>
								</td>
							<# } else { #>
								<td><input type="text" class="dateRange datefield" id="filter-<#=exportFilter.id#>"/></td>
							<# } #>
					<# break; case 'DATETIME' : #>
							<td><input type="text" class="date datefield" id="filter-<#=exportFilter.id#>"/></td>
					<# } #>
				</tr>
			<# } #>
			</table>
		</div>
		<div class="clear"/>
		<div class="lfloat20">
			<# if (obj.email && obj.email !== '' && !obj.scheduling ) { #>
				<input id="sendEmail" type="checkbox" /> send an email on job completion
				<input id="email" type="text"  value="<#=obj.email#>"  size="30"/>
			<# } else if (!obj.scheduling) { #>
				<a href="/myaccount/myInfo?legacy=1">Configure you email to receive job status</a>
			<# } #>
			<br/><br/>
			<div id="createJob" class="btn btn-primary lfloat "><# if(!obj.scheduling) { #>Create Export Job<# } else { #>create scheduled export<# } #></div>
			<# if(obj.scheduling) { #>
				<div id="cancelReportCreation" class="btn btn-danger lfloat20 ">cancel subscribe report</div>
			<# } #>
		</div>
		<div class="clear"/>
		</script>	
		<script type="text/javascript">
			Uniware.ExportConfigPage = function() {
				var self = this;
				this.email = '${user.user.email}';
				this.config = null;
				this.scheduling = false;
				//this.frequencies = ${frequencies};
				this.selectedUsers = [];
				
				this.init = function() {
					$('#configName').change(function() {
						$('#configDiv').addClass('hidden');
						if ($(this).val() != 0) {
							self.scheduling = false;
							Uniware.Ajax.getJson("/data/tasks/export/config/get?exportConfigName=" + $(this).val(), self.onConfigFetch);
							$('#scheduleExports').removeClass('invisible');
						} else {
							$('#scheduleExports').addClass('invisible');
						}
					});
					
				};
				
				
				$('#scheduleExports').click(function(){
					self.scheduling = true;
					self.onConfigFetch(self.config);
					$('#cancelReportCreation').click(self.cancelReport);
				});
				
				this.cancelReport = function(){
					self.scheduling = false;
					self.onConfigFetch(self.config);
				}				
				this.onConfigFetch = function(config) {
					self.config = config;
					self.config.email = self.email;
					//self.config.frequencies = self.frequencies;
					self.config.scheduling = self.scheduling;
					$("#configDiv").html(template("configTemplate", self.config)).removeClass('hidden');
					if (self.scheduling) {
						$('#selector').cron({
	                        initial: "0 8 * * *",
	                        useGentleSelect: true,
	                        effectOpts: {
	                            openEffect: "fade",
	                            openSpeed: "slow"
	                        },
	                        onChange: function() {
	                            $('#cron-val').val($(this).cron("value"));
	                        }
	                    });
					}
					$("#all").click(function(){
						$("input.column").attr('checked',$(this).is(':checked'));
					});
					
					$('.dateRange').each(function(){
						$(this).daterangepicker();
					});
					
					$('.date').datepicker({
							dateFormat : 'dd/mm/yy'
					});
										
					$("#createJob").click(function(){
						var index, filter, selectedFilters, selectedColumns, req;
						selectedColumns = $("input.column:checked");
						req = {
							'exportJobTypeName' : $("#configName").val(),
							'exportFilters' : [],
							'exportColums' : [],
							'reportName' : $('#reportName').val(),
						};
						if ($("#sendEmail:checked").length > 0) {
							req.notificationEmail = $('#email').val().trim();
						}
						
						var cronValue = $('#cron-val').val();						
						if (cronValue != undefined){
							req.cronExpression = '0 ' + cronValue;
							req.frequency = 'RECURRENT';
						} else {
							req.frequency = 'ONETIME';
						}
						
						selectedColumns.each(function() {
							req.exportColums.push($(this).attr('id').substring($(this).attr('id').indexOf('-') + 1));
						});
						
						selectedFilters = $("input.filter:checked");
						selectedFilters.each(function() {
							var index = parseInt($(this).attr('id').substring($(this).attr('id').indexOf('-') + 1));
							var filter = self.config.exportFilters[index];
							
							var exportFilters = {
									id : self.config.exportFilters[index].id
							};
						  
							switch (filter.type) {
							case 'TEXT' : 
								exportFilters.text = $('#filter-' + exportFilters.id).val();
								break;
							case 'BOOLEAN' :
								exportFilters.checked = $('#filter-' + exportFilters.id).is(':checked');
								break;
							case 'DATETIME' :
								var dateTime = $('#filter-' + exportFilters.id).val();
								exportFilters.dateTime = Date.fromPaddedDate(dateTime).getTime();
								break;
							case 'DATERANGE' :
								var dateRange = $('#filter-' + exportFilters.id).val();
								exportFilters.dateRange = {};
								if (self.scheduling) {
									exportFilters.dateRange.textRange = $('#filter-' + exportFilters.id).val();
								} else if (dateRange != '') {
									exportFilters.dateRange.start = Date.fromPaddedDate(dateRange.substring(0, 10)).getTime();
									if (dateRange.length > 10) {
										exportFilters.dateRange.end = Date.fromPaddedDate(dateRange.substring(13));
									} else {
										exportFilters.dateRange.end = Date.fromPaddedDate(dateRange.substring(0, 10));
									}
									exportFilters.dateRange.end = exportFilters.dateRange.end.setDate(exportFilters.dateRange.end.getDate()+1);
								}
								break;
							case 'SELECT' :
								exportFilters.selectedValue = $('#filter-' + exportFilters.id).val();
								break;
							case 'MULTISELECT' :
								exportFilters.selectedValues = $('#filter-' + exportFilters.id).val();
								break;
							}
							req.exportFilters.push(exportFilters);
						});
						
						Uniware.Ajax.postJson("/data/tasks/export/job/create", JSON.stringify(req), function(response) {
							if (response.successful == false) {
								Uniware.Utils.showError(response.errors[0].description);
							} else {
								Uniware.Utils.addNotification('An Export Job has been created, Please check the Running Jobs.');
								if (self.scheduling) {
									var req = {
										exportSubscriptions : []
									};
									length = req.exportSubscriptions.length;
									req.exportSubscriptions[length] = {};
									req.exportSubscriptions[length].exportJobId = response.exportJobId;
									req.exportSubscriptions[length].subscribed = true;
									Uniware.Ajax.postJson("/data/tasks/exports/subscription/update", JSON.stringify(req), function(response) {
										if(response.successful){
											$("#actionRow").addClass('hidden');
											Uniware.Utils.addNotification('An Export Job has been created and subscribed, Please check the Running Jobs.');
										}else{
											Uniware.Utils.showError(response.errors[0].description);
										}
									});
								}
								$('#configName').val('0').trigger('change');
								self.scheduling = false;
							}
						}, true);
						
					});
				};
				
				
			};
			
			$(document).ready(function() {
				window.page = new Uniware.ExportConfigPage();
				window.page.init();
				var configName = '${param['name']}';
				if (configName != '') {
					$('#configName').val(configName).trigger('change');
				}				
			});
		</script>
	</tiles:putAttribute>
</tiles:insertDefinition>
