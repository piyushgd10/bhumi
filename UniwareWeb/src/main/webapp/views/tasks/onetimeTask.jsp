<%@ include file="/tagIncludes.jsp"%>
<tiles:insertDefinition name=".tasksPage">
	<tiles:putAttribute name="title" value="Uniware - Onetime Tasks" />
	<tiles:putAttribute name="rightPane">
		<div>
			<div class="greybor headlable ovrhid main-box-head">
				<h2 class="edithead head-textfields">Onetime Tasks</h2></div>
		</div>
		<div class="clear"></div>
		<div style="margin-top:5px; overflow: hidden;">
		<form onsubmit="javascript : return false;">
			<table id="dataTable" class="dataTable"></table>
		</form>
		</div>

	</tiles:putAttribute>
	<tiles:putAttribute name="deferredScript">
		<script type="text/javascript" src="${path.js('jquery/jquery.dataTables.min.js')}"></script>
		<script type="text/html" id="taskRow">
			<#if(obj.taskResult != null) { #>
				<#if(obj.taskResult.failed == false) { #>
				<td><img src="/img/icons/tick.png"/></td>
				<# } else { #>
				<td><img src="/img/icons/hold.png"/></td>
				<# } #>
			<# } else { #>
				<td></td>
			<# } #>
			<td><#=obj.name#></td>
			<td><#=obj.type#></td>
			<td><#=obj.statusCode#></td>
			<td><#=new Date(obj.scheduledTime).toDateTime()#></td>
			<#if(obj.taskResult != null) { #>
				<td>
					<#=obj.taskResult.message#><br/>
					<div style="font-size:12px;line-height:14px;">
                    <#if (obj.type == 'IMPORT') {#>
                             Successful Imports: <#=obj.taskResult.resultItems['successfulImportCount']#><br/>
                             <# if (obj.taskResult.resultItems['failedImportCount'] > 0) {#>
                                Failed Imports: <#=obj.taskResult.resultItems['failedImportCount']#><br/><a href="<#=obj.taskResult.resultItems['importLogFilePath']#>" target="_blank">Download Failed Log</a>
							<# } #>
                    <#} else if (obj.type == 'EXPORT') {#>
                            <# if (obj.taskResult.resultItems['exportCount'] != -1) {#>
                                Exported Records: <#=obj.taskResult.resultItems['exportCount']#><br/><a href="<#=obj.taskResult.resultItems['exportFilePath']#>" target="_blank">Download</a>
                            <# } #>
                    <# } #>
					</div>
				</td>
			<# } else { #>
				<td></td>
			<# } #>

		</script>
		<script type="text/javascript">
			Uniware.OnetimeTask = function() {
				var self = this;
				
				this.init = function() {
					self.listOnetimeTasks();
				};
				
				this.runTask = function(event) {
					var aPos = self.table.fnGetPosition($(event.target).parent().get(0));
					var aData = self.table.fnGetData(aPos[0]);
					Uniware.Ajax.getJson("/data/tasks/onetimeTask/run?taskId=" + aData.id, function(response) {
						if(response.successful == true){
							Uniware.Utils.addNotification("Task " + aData.name + " has been run successfully");
						}else{
							Uniware.Utils.showError(response.errors[0].description);
						}
						aData.runnable = !aData.runnable;
						self.table.fnUpdate(aData, aPos[0]);
					});
				};
				
				this.listOnetimeTasks = function(){
					Uniware.Ajax.getJson("/data/tasks/onetimeTasks/fetch", function(response) {
						var dtEL = $('#dataTable');
						if (Uniware.Utils.isDataTable(dtEL)) {
							var dtTable = dtEL.dataTable();
							dtTable.fnClearTable();
							dtTable.fnAddData(response.elements);
							
						} else {
							self.table = dtEL.dataTable({
								"sPaginationType": "full_numbers",
								"bFilter" : false,
								"bPaginate": false,
								"bInfo":false,
								"aaData" : response.elements,
								"aoColumns" : [ {
									"sTitle" : "",
									"mDataProp": function (aData) {
										return "";
									},
									"sWidth" : "50"
								},{
									"sTitle" : "Name",
									"mDataProp" : "name",
									"sWidth" : "300"
								},
								{
									"sTitle" : "Type",
									"mDataProp" : "type",
									"sWidth" : "100"
								},
								{
									"sTitle" : "Status Code",
									"mDataProp" : "statusCode",
									"sWidth" : "100"
								},
								{
									"sTitle" : "Scheduled Time",
									"mDataProp" : "scheduledTime",
									"sWidth" : "120"
								},
								{
									"sTitle" : "Result",
									"mDataProp": function (aData) {
										return "";
									}
								}],
								"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
									$(nRow).html(template("taskRow", aData));
									return nRow;
								}
							});
							
							$("#dataTable tbody").on('mouseover mouseout', 'tr', function(event) {
								$(this).find('div.action').toggleClass('hidden');
							});
							$("#dataTable tbody").on('click', 'div.runtask', function(event) {
								self.runTask(event);
							});
							$("#dataTable tbody").on('mouseover', '#detailedMessage', function(event) {
								self.fetchTask(event);
							});
						}
					});
				};
				
				this.fetchTask = function(event){
					var aPos = self.table.fnGetPosition($(event.target).parent().get(0));
					var aData = self.table.fnGetData(aPos[0]);
					Uniware.Ajax.getJson("/data/tasks/onetimeTask/fetch?taskId=" + aData.id, function(response) {
						
					});
				};
				
			};
			
			$(document).ready(function() {
				window.page = new Uniware.OnetimeTask();
				window.page.init();
			});
		</script>
	</tiles:putAttribute>
</tiles:insertDefinition>
