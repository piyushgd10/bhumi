<%@ include file="/tagIncludes.jsp"%>
<tiles:insertDefinition name=".wapPage">
	<tiles:putAttribute name="title" value="Uniware - Add Items To Gatepass" />
	<tiles:putAttribute name="body">
		<br />
		<div class="greybor headlable ovrhid main-box-head">
			<h2 class="edithead head-textfields">GatePass > Scan Items</h2>
		</div>
		<div class="greybor round_bottom main-boform-cont ovrhid" style="padding: 10px;">
			<div id="gatePassDiv" ></div>
			<div id="message" class="hidden errorField"></div>
		</div>
	</tiles:putAttribute>
	<tiles:putAttribute name="deferredScript">
	<script id="gatePassTemplate" type="text/html">
	<table>
		<tr>
			<td width="100">GatePass No. : </td>
			<td><#= obj.code ? obj.code : '<input type="text" id="code" class="w150 ucase"/>' #></td>
		</tr>
		<# if (obj.code) { #>
		<tr>
			<td>Type : </td>
			<td><#=obj.type#></td>
		</tr>
		<tr>
			<td>To Party : </td>
			<td><#=obj.toPartyName#></td>
		</tr>
		<tr>
			<td>Item : </td>
			<td><input type="text" id="itemCode" class="w150"/></td>
		</tr>
		<tr>
			<td></td>
			<td><div id="addItem" class=" btn lfloat" >Add Item</div></td>
		</tr>		
		<# } #>
	</table>
	</script>
		<script type="text/javascript">
		Uniware.WapGatepassPage = function() {
			var self = this;
			this.wapGatePass = {};
			
			this.init = function() {
				self.render();
				$('#code').focus().keyup(self.load);
			};
			
			this.load = function(event) {
				if (event.which == 13 && $('#code').val() != '') {
					self.fetchGatepass($('#code').val());
				}
			};
			
			this.fetchGatepass = function(code) {
				$('#message').addClass('hidden');
				var req = {
					'code' : code	
				};
				Uniware.Ajax.postJson("/data/material/gatepass/get", JSON.stringify(req), function(response) {
					if(response.successful == true) {
						self.wapGatePass = response.gatePassDTO;
						self.render();
						$('#addItem').click(self.addItem);
					} else {
						$('#message').html(response.errors[0].description).removeClass('hidden');
						$('#code').val('');
					}
				});
			};

			
			this.addItem = function(event) {
				$('#message').addClass('hidden');
				var itemCode = $('#itemCode').val();
				var req = {
					'gatePassCode': self.wapGatePass.code,
					'itemCode': itemCode
				}
				Uniware.Ajax.postJson('/data/material/gatepass/add/item', JSON.stringify(req), function(response){
					if (response.successful) {
						$('#message').html("Item has been added successfully.").removeClass('hidden');
						$('#itemCode').val('');
					} else {
						$('#message').html(response.errors[0].description).removeClass('hidden');
						$('#itemCode').val('');
					}
				});
			};
			
			this.render = function() {
				$('#gatePassDiv').html(template('gatePassTemplate', self.wapGatePass));
			};
			
		}
		
		$(document).ready(function() {
			window.page = new Uniware.WapGatepassPage();
			window.page.init();
		});
	</script>
	</tiles:putAttribute>
</tiles:insertDefinition>
