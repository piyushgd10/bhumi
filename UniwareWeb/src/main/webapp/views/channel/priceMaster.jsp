<%@ include file="/tagIncludes.jsp"%>
<tiles:insertDefinition name=".reportsPage">
	<tiles:putAttribute name="title" value="Uniware - Price Master" />
	<tiles:putAttribute name="rightPane">
		<div id="pageBar">
			<div class="pageHeading" id="pricingInfo"><span class="mainHeading">Price Master</span></div>
			<div class="pageControls"></div>
			<div class="clear"></div> 
		</div>
		<div id="editMarginDiv" class="lb-over">
			<div class="lb-over-inner round_all">
				<div style="margin: 40px; line-height: 30px;">
					<div class="pageHeading lfloat">Edit Price Master Item Type</div>
					<div id="editMarginDiv_close" class="link rfloat">close</div>
					<div class="clear"></div>
					<div id="editMarginDivTemplateDiv" style="margin: 20px 0;"></div>
				</div>
			</div>
		</div>
		<div class="ovrhid">
			<div id="redirectDiv" style="display: none;"></div>
			<div id="tableContainer">
				<table id="flexme1" style="display: block;"></table>
			</div>
		</div>
	</tiles:putAttribute>
	<tiles:putAttribute name="deferredScript">
		<sec:authentication property="principal" var="user" />
		<script type="text/javascript" src="${path.js('jquery/jquery.dataTables.min.js')}"></script>
		<script type="text/html" id="editPriceMasterItemTypeTemplate">
            <div class="formLeft150 lfloat">Price Master</div>
            <div class="formRight lfloat"><#=window.page.pageConfig.getColumnValue(obj, 'priceMaster') #></div>
            <div class="clear"></div>
			
            <div class="formLeft150 lfloat">Item Sku</div>
			<div class="formRight lfloat"><#=window.page.pageConfig.getColumnValue(obj, 'productCode') #></div>
			<div class="clear"></div>

			<div class="formLeft150 lfloat">Item Name</div>
			<div class="formRight lfloat"><#=window.page.pageConfig.getColumnValue(obj, 'productName') #></div>
			<div class="clear"></div>			

			<div class="formLeft150 lfloat">Transfer Price</div>
			<div class="formRight lfloat"><input type="text" id="transferPrice" size="30" value="<#=window.page.pageConfig.getColumnValue(obj, 'transferPrice') #>"/></div>
			<div class="clear"></div>

            <div class="formLeft150 lfloat">Selling Price</div>
            <div class="formRight lfloat"><input type="text" id="sellingPrice" size="30" value="<#=window.page.pageConfig.getColumnValue(obj, 'sellingPrice') #>"/></div>
            <div class="clear"></div>
			<br />
			
			<div class="formLeft150 lfloat"></div>
			<div class="formRight lfloat">
				<div class="formRight lfloat">
					<input type="submit" id="editPriceMasterItemTypeValue" class=" btn btn-small btn-primary lfloat" value="submit"/>
					<div id="error" class="errorField lfloat20"></div>
				</div>
			</div>
			<div class="clear"></div>
			
	</script>
		<script type="text/javascript">
		Uniware.PackageReconciliationPage = function() {
			var self = this;
			self.name = 'DATATABLE PRICE MASTER';
            self.pageConfig = null;
            self.table = null;
            this.datatableViews = ${user.getDatatableViewsJson("DATATABLE PRICE MASTER")};
			this.init = function() {
				this.pageConfig = new Uniware.PageConfig(this.name,'#flexme1',this.datatableViews[0]);
				if(this.datatableViews.length){
					this.headConfig = new Uniware.HeadConfig(this.datatableViews,function(datatableObject){
						self.pageConfig.setView(datatableObject);
					},this.pageConfig);
				}
                $('#tableContainer').on('click', '.editPriceMasterItemType', self.editPriceMasterItemType);
            };
            
            this.editPriceMasterItemType = function(event) {
            	var tr = $(event.target).parents('tr');
				var rowNumber = $(tr).attr('id').split('-')[1];
				var data = self.pageConfig.getRecord(rowNumber);
				self.editRow = data;
				$("#editMarginDivTemplateDiv").html(template("editPriceMasterItemTypeTemplate",data));
				$("#editPriceMasterItemTypeValue").click(self.editPriceMasterItemTypeValue);
				Uniware.LightBox.show("#editMarginDiv");
            };
            
            this.editPriceMasterItemTypeValue = function(data){
            	var data = self.editRow;
            	var req = {};
            	var priceMasterItemType = {};
            	priceMasterItemType.skuCode = self.pageConfig.getColumnValue(data, 'productCode');
            	priceMasterItemType.transferPrice = $("#transferPrice").val();
            	priceMasterItemType.sellingPrice = $("#sellingPrice").val();
            	req.priceMasterName = self.pageConfig.getColumnValue(data, 'priceMaster');
            	req.priceMasterItemTypes = [];
            	req.priceMasterItemTypes.push(priceMasterItemType);
            	Uniware.Ajax.postJson("/data/pricing/priceMaster/addItemTypes", JSON.stringify(req), function(response) {
					if (response.successful == false) {
						Uniware.Utils.showError(response.errors[0].description);
						Uniware.LightBox.hide();
					} else {
						Uniware.LightBox.hide();
						Uniware.Utils.addNotification('Price Master Item Type updated successfully');
						self.pageConfig.reload();
					}
				});
				
            };
			
	};
	
	$(document).ready(function() {
		window.page = new Uniware.PackageReconciliationPage();
		window.page.init();
	});
	</script>
	</tiles:putAttribute>
</tiles:insertDefinition>

