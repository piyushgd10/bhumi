<%@ include file="/tagIncludes.jsp"%>
<tiles:insertTemplate template="/views/template/homeBase.jsp">
	<tiles:putAttribute name="title" value="Uniware - Warehouse Management - Home Page" />

	<tiles:putAttribute name="head">
	</tiles:putAttribute>
	<tiles:putAttribute name="body">
		<div>
			<div id="uploader" align="center" style="margin-top: 50px">
				<input type="file" name="file" id="file" class="textfield" style="width: 190px" />
				<div class=" btn btn-primary rfloat" id="uploadFile">upload file</div>
			</div>
		</div>
	</tiles:putAttribute>
	<tiles:putAttribute name="deferredScript">
		<script type="text/javascript" src="${path.js('jquery/ajaxfileupload.js')}"></script>
		<script>
			$(document).ready(function() {
				$("#uploadFile").click(function() {
					$.ajaxFileUpload({
						url : Uniware.Path.http("/files/upload"),
						secureuri : true,
						fileElementId : 'file',
						dataType : 'json',
						success : function(data, status) {
							if (data.binaryObjectId) {
								alert("uploaded to object id: " + data.binaryObjectId);
							}
						}
					});
				});
			});
		</script>
	</tiles:putAttribute>
</tiles:insertTemplate>



