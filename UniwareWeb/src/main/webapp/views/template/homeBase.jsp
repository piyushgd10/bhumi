<%@ include file="/tagIncludes.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><tiles:getAsString name="title"/></title>
<link href="${path.css('unifier/ui.home.css')}" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="${path.https}/img/icons/favicon.ico?v=2" type="image/x-icon" />
<link rel=icon type=image/ico href="${path.https}/img/icons/favicon.ico?v=2" />
<script>
	if (typeof Uniware == 'undefined') {
		Uniware = {};
	};
	Uniware.Path = {};
	Uniware.ADMIN = ${administration eq true};
	Uniware.Path.http = function(path) {
		return '${path.http}' + path;
	}
	Uniware.Path.resources = function(path) {
		return '${path.resources("")}' + path;
	}
</script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<tiles:insertAttribute name="head" defaultValue="" />
</head>
<body>
	<div id="lightBox"></div>
	<div id="calWidth" class="hidden"></div>
	<div id="actionInfo" class="round_all hidden"></div>
	<div id="processing" class="round_all hidden">
		<img src='${path.resources("img/icons/processing.gif")}'></img>
		<div class="rfloat">processing ...</div>
	</div>	
	<div id="printIframeHelper" class="hidden"></div>
		
	<div id="wrapper">
	<div id="body-container">
		<tiles:insertAttribute name="header" defaultValue="/views/template/header.jsp" />
		<div id="body-main">
			<tiles:insertAttribute name="body"/>
		</div>
		<tiles:insertAttribute name="footer" defaultValue="/views/template/footer.jsp" />
	</div>
	</div>
	<script type="text/javascript" src="${path.js('jquery/jquery.all.min.js')}"></script>
	<script type="text/javascript" src="${path.js('unifier/unifier.all.js')}"></script>
	<tiles:insertAttribute name="deferredScript" defaultValue="" />
</body>
</html>