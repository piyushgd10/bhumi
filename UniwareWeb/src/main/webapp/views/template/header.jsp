<%@ include file="/tagIncludes.jsp"%>
<header id="header">
    <div class="wrapper">
        <a class="logo" href="${administration eq true ? '/admin' : '/'}"></a>
        <sec:authentication property="principal" var="user" />
        <div class="navbar lfloat20" style="color: #fff; margin-top: 10px">
	        <c:choose>
	        	<c:when test="${user.primaryUser ne null}">
	        		Welcome ${user.primaryUser.username} (<strong>You are logged in as ${user.username} <span class="link" id="logoutAs" style="color:#6DBCFF">logout</span></strong>)
	        	</c:when>
	        	<c:otherwise>Welcome ${user.username}</c:otherwise>
	        </c:choose>
        </div>
        <c:set var="requestURI" value="${pageContext.request.getAttribute('javax.servlet.forward.request_uri')}" />
        <div class="navbar rfloat">
            <ul class="nav">
                <li class="dropdown accountInfo"><a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="header-user icon-user lfloat"></i>
                        <div class="lfloat">
                            <div class="headerNotification hidden round_all_b">5</div>
                            <i class="icon-caret-down mar-15-top" style="font-size: 17px;display:inline-block;"></i>
                        </div>
                        <div class="clrstyle"></div>
                </a>
                    <ul class="dropdown-menu pull-right" role="menu" aria-labelledby="dLabel">
                        <li><a tabindex="-1" href="/myaccount/myInfo">Account Settings</a></li>
                        <li><a tabindex="-1" href="/myaccount/changePassword">Change Password</a></li>
                        <uc:security accessResource="LOGIN_AS_USER">
                            <c:if test="${user.primaryUser eq null}">
                                <li><a tabindex="-1" id="loginAs">Login as</a></li>
                            </c:if>
                        </uc:security>
                        <li class="divider"></li>
                        <li><a tabindex="-1" href="/logout">Log Out</a></li>
                    </ul></li>
            </ul>
        </div>
         <div class="rfloat" style="position: relative">
        <div class="exportImport" title="View Exports" data-toggle="dropdown">
            <div id="showExports">
            </div>
        </div>
            <div class="toolTipOuter dropdown-menu" id="exportsData"></div>
        </div>
        <div class="rfloat" style="position: relative">
		<div class="exportImport dropdown-toggle" title="View Imports"	data-toggle="dropdown">
			<div id="showImports">
				<div id="importDataLoader"></div>
			</div>
		</div>
            <div class="toolTipOuter dropdown-menu" id="importsData"></div>
        </div>
       
        <div class="rfloat" style="position: relative" id="headerSearchFieldOuter">
            <div>
                <input type="text" name="search" id="headerSearchField" placeholder="Search" />
            </div>
            <div id="searchDropdown" class="pad-10"></div>
        </div>
    </div>
    <div class="clear"></div>
</header>
<div id="loginAsUserDiv" class="lb-over">
    <div class="lb-over-inner round_all">
        <div style="margin: 40px; line-height: 30px;">
            <div class="pageHeading lfloat">Login As</div>
            <div id="loginAsUserDiv_close" class="link rfloat20">close</div>
            <div class="clear"></div>
            <div class="searchLabel">Users</div>
            <div class="clear"></div>
            <div class="lfloat">
                <div>
                    <input type="text" id="users" placeHolder="user" />
                </div>
            </div>
            <div class="clear"></div>
            <div>
                <br /> <input type="button" class="btn btn-primary" id="userLogin" value="submit" />
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>
<div id="403Div" class="lb-over">
    <div class="lb-over-inner round_all" style="padding: 20px;">
        <div class="pageHeading lfloat"></div>
        <div id="403Div_close" class="link rfloat">close</div>
        <div class="clear"></div>
        <div class="round_all greybor" style="padding: 20px 40px;">
            <h1 style="color: #666;">Access Denied</h1>
            <br /> <span class="f15"> <span class="errorField">Oops!</span> You are not authorized to perform this action.<br /> Please contact your system administrator for access.
            </span>
        </div>
    </div>
</div>
<div id="401Div" class="lb-over">
    <div class="lb-over-inner round_all" style="padding: 20px;">
        <div class="round_all greybor" style="padding: 20px 40px;">
            <br /> <span class="f15"> <span class="errorField">Oops!</span> You have been logged out.<br /> Please <a href="">relogin</a>.
            </span>
        </div>
    </div>
</div>
<script id="exportJobsTemplate" type="text/html">
	<div class="toolTipArrow"></div>
	<div class="toolTipContWrapper round_all">
		<div>Exports</div>
		<div class="toolTipCont">
			<# for(var i=0; i<obj.exportJobs.length; i++){ var exportJob = obj.exportJobs[i];#>
				<#=template("exportJobTemplate", exportJob)#>
			<# } #>
		<# if (obj.exportJobs.length==0) { #>
			<div class="jobDataWrapper">
				<em>No result found</em>
			</div>
		<# } #>
		</div>
	</div>
</script>
<script id="importJobsTemplate" type="text/html">
	<div class="toolTipArrow"></div>
	<div class="toolTipContWrapper round_all">
		<div>Imports</div>
		<div class="toolTipCont">
		<# for(var i=0; i<obj.importJobs.length; i++){ var importJob = obj.importJobs[i];#>
			<#=template("importJobTemplate", importJob)#>
		<# } #>
		<# if (obj.importJobs.length==0) { #>
			<div class="jobDataWrapper">
				<em>No result found</em>
			</div>
		<# } #>
		</div>
	</div>
</script>
<script id="importJobTemplate" type="text/html">
		<div class="jobDataWrapper">
			<div class="jobData">
				<# if(obj.statusCode=="COMPLETE"){#>
					<# if(obj.taskResult.resultItems.failedImportCount > 0){#>
						<div class="lfloat infoIco"></div>
					<# }else{#>
						<div class="lfloat tickIco"></div>
					<# }#>
				<# }#>
				<div class="lfloat10 w200">
					<div><#=obj.name#></div>
					<# if(obj.statusCode!="COMPLETE"){#>
						<div class="jobLoader round_all_s">
							<div class="jobLoaderStatus round_all_s" style="width:<#=obj.percentageComplete#>px"></div>
						</div>
					<# }#>
					<div class="lfloat smallText"><#=new Date(obj.scheduledTime).toDateTime()#></div>
					<# if(obj.taskResult.resultItems.failedImportCount > 0){#>
						<div class="rfloat btn-mini btn-warning"><a style="color:#fff; text-decoration:none" href="<#=obj.taskResult.resultItems.importLogFilePath#>">Error <#=obj.taskResult.resultItems.failedImportCount#>/<#=obj.taskResult.resultItems.failedImportCount + obj.taskResult.resultItems.successfulImportCount#></a></div>
					<# }else{#>
						<div class="rfloat smallText"></div>
					<# }#>					
					<div class="clear"></div>
				</div>
				<div class="clear"></div>
			</div>
		</div>
</script>
<script id="exportJobTemplate" type="text/html">
		<div class="jobDataWrapper">
			<div class="jobData">
				<# if(obj.statusCode=="COMPLETE"){#>
					<div class="lfloat tickIco"></div>
				<# }#>
				<div class="lfloat10 w200">
					<div><#=obj.name#></div>
					<# if(obj.statusCode!="COMPLETE"){#>
						<div class="jobLoader round_all_s">
							<div class="jobLoaderStatus round_all_s" style="width:<#=obj.percentageComplete#>px"></div>
						</div>
					<# }#>
					<div class="lfloat smallText"><#=new Date(obj.scheduledTime).toDateTime()#></div>
					<div class="rfloat smallText">
						<div class="jobRecords"><#=obj.taskResult.resultItems.exportCount && obj.taskResult.resultItems.exportCount != -1 ? obj.taskResult.resultItems.exportCount + ' records':''#></div>
						<div class="jobDownlaod btn-mini btn-primary"><a href="<#=obj.taskResult.resultItems.exportFilePath#>">Download</a></div>
						</div>
					<div class="clear"></div>
				</div>
				<div class="clear"></div>
			</div>
		</div>
</script>
<script id="searchTemplate" type="text/html">
	<ul>
		<# var countResult = 0; #>
		<# for(var i=0; i<obj.searchDTOs.length; i++) { #>
           <# for(var j=0; j<obj.searchDTOs[i].searchResults.length; j++) { countResult++; #>	
			    <li><#=obj.searchDTOs[i].type#>: <a href="<#=obj.searchDTOs[i].searchResults[j].url#>"><#=obj.searchDTOs[i].searchResults[j].value#></a></li>
		    <# } #>
        <# } #>		
		<# if (countResult==0) { #>
			<li><em>No result found</em></li>
		<# } #>
	</ul>
</script>
<script id="leftNavTemplate" type="text/html">
<# var leftCount = -1; #>
<ol class="main-nav unstyled">
	<# for(var i=0; i<obj.length; i++) { #>	
		<# for(var j=0; j<obj[i].tabs.length; j++) { var tab = obj[i].tabs[j];#>
			<# if (!tab.hidden) { #>
			<# var leftCount = leftCount + 1; #>
				<# if (tab.sideTabGroups.length > 0) { #>
					<li class="leftNavTab handCrsr <#=(tab.name == Uniware.layout.currentTab) ? 'selected' :''#>" name="<#=i#>-<#=j#>-<#=tab.name#>" leftCount="<#=leftCount#>">
						<a>
							<div class="lfloat leftNavText"><#=tab.name#></div>
								<i data-placement="right" data-toggle="tooltip" data-original-title="<#=tab.name#>" class="leftIcoTooltip lfloat iconu-<#=tab.icon#>"></i>
							<div class="clrstyle"></div>
						</a>
					</li>
				<# }else{#>
					<li class="<#=(tab.name == Uniware.layout.currentTab) ? 'selected' :''#>" name="<#=i#>-<#=j#>-<#=tab.name#>"> 
						<a href="<#=tab.href#>">
							<div class="lfloat leftNavText"><#=tab.name#></div>
							<i data-placement="right" data-toggle="tooltip" data-original-title="<#=tab.name#>" class="leftIcoTooltip lfloat iconu-<#=tab.icon#>"></i>
							<div class="clrstyle"></div>
						</a>
					</li>
				<# } #>
			<# } #>
		<# } #>
	<# } #>
</ol>
<div id="activeState" style="display:none"></div>
</script>

<script id="leftNavTabTemplate" type="text/html">
	<ol class="main-nav unstyled">
		<# for(var i=0; i<obj.length; i++) { #>	
			<# for(var j=0; j<obj[i].sideTabs.length; j++) { var sideTab = obj[i].sideTabs[j];#>	
				<# if (!sideTab.hidden) { #>
					<li class="<#=(sideTab.href == Uniware.layout.requestURI) ? 'selected' :''#>">
						<a href="<#=sideTab.href#>" class="<#=(sideTab.href == Uniware.layout.requestURI) ? 'selected' :''#>">
							<#=sideTab.name#>
						</a>
					</li>
				<# } #>
			<# } #>
		<# } #>
	</ol>
</script>