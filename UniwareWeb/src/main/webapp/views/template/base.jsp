<%@ include file="/tagIncludes.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><tiles:getAsString name="title" /></title>
<link href="${path.css('unifier/ui.home.css')}" rel="stylesheet" type="text/css" />
<link href="${path.css('unifier/print.css')}" rel="stylesheet" type="text/css" media="print" />
<link rel="shortcut icon" href="${path.https}/img/icons/favicon.ico?v=2" type="image/x-icon" />
<link rel="icon" type="image/ico" href="${path.https}/img/icons/favicon.ico?v=2" />

<sec:authentication property="principal" var="user" />
<c:set var="requestURI" value="${pageContext.request.getAttribute('javax.servlet.forward.request_uri')}" />
<script>
	var myutil = {
		removeParamFromUrl: function(oldurlWithHash, param) {
		  var oldurl = oldurlWithHash.split("#")[0];
	      var patharry = oldurl.split("?");
	      var url = "";
	      if (patharry.length === 1) {
	        url = oldurl;
	      } else {
	        var allparames = patharry[1].split("&");
	        var index = allparames.indexOf(param);
	        if (index > -1) {
	          allparames.splice(index, 1);
	        }else{
	        	return {
	        		update:false
	        	};
	        }
	        if (allparames.length === 0) {
	          url = patharry[0];
	        } else {
	          url = patharry[0] + "?" + allparames.join("&");
	        }
	      }
	      
	      if (oldurlWithHash.split("#").length > 1) {
	        url = url + "#" + oldurlWithHash.split("#")[1];
	      }
	      
	      return {
	        	update:true,
	        	url:url
	      };
	    }
	};
	if(window.top !== window.self){
		if(typeof window.top.postMessage === "function"){
			window.top.postMessage({type:"urlChanged",val:window.location.pathname + window.location.search}, '*');
		}
	}else{
		//If there is a legacy=1 present on top URL then we must redirect to main url
		var X =  myutil.removeParamFromUrl(window.location.href, "legacy=1");
		if(X.update){
			window.location = X.url; 
		}
	}

	if (typeof Uniware == 'undefined') {
		Uniware = {};
	};
	Uniware.Path = {};
	Uniware.ADMIN = ${administration eq true};
	Uniware.Path.http = function(path) {
		return '${path.http}' + path;
	}

	Uniware.Path.resources = function(path) {
		return '${path.resources("")}' + path;
	}
</script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<tiles:insertAttribute name="head" defaultValue="" />
</head>
<body>
	<div id="lightBox"></div>
	<div id="calWidth" class="hidden"></div>
	<div id="imageDiv" class="hidden round_all" style="position: absolute; z-index: 20000; border: 1px solid;"></div>
	<div id="actionInfo" class="alert alert-info bold hidden"></div>
	<div id="processing" class="round_all hidden">
		<img src='${path.resources("img/icons/processing.gif")}'></img>
		<div class="rfloat">processing ...</div>
	</div>
	<div id="printIframeDiv" class="" style="top: -10px; position: absolute; width: 1px; height: 1px;"></div>
	<div class="modal hide fade" id="btsLightBox" style="width: 800px; margin-left: -400px">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<div class="f15" id="modalHeadText"></div>
		</div>
		<div class="modal-body" id="modalBodyCont"></div>
		<div class="modal-footer" id="modalFooter" style="display: none"></div>
	</div>
	<div id="body" class="clearfix">
		<div id="content">
			<div id="reloadBar" class="alert alert-info ${sessionScope['reloadTypes'] != null && fn:length(sessionScope['reloadTypes']) > 0 ? '' : 'hidden'}">
				<div class="info">Administrative changes will be reflected in next reload cycle.</div>
				&nbsp;&nbsp;&nbsp; <a class="action btn btn-small btn-info">Reload changes Now</a>
			</div>
			<tiles:insertAttribute name="rightPane" />
		</div>
	</div>
	<div id="infoModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"></div>
	<script type="text/javascript" src="${path.js('jquery/jquery.all.min.js')}"></script>
	<script type="text/javascript">
	if(window.top !== window.self){
		$(document).on("click", "html", function(e) {
	       window.top.postMessage({type:"bodyClickEvent",val:""}, '*');
	    });
	    window.top.postMessage({type:"title",val:document.title}, '*');
	    
	}
	$(document).keyup(function(e) {
            if (e.which == 122) {
              window.top.postMessage({type:"F11Pressed",val:""}, '*');
            }
        });
	</script>
	<script type="text/javascript" src="${path.js('unifier/unifier.all.js')}"></script>
	<tiles:insertAttribute name="deferredScript" defaultValue="" />

	<div onclick="window.print();" style="right: 20px; bottom: 20px; color: white; z-index: 200000; position: fixed; padding: 2px 9px; background: none repeat scroll 0px 0px rgb(0, 159, 203);">
		<i class="icon-print"></i>&nbsp;Print
	</div>
</body>
</html>

