<%@ include file="/tagIncludes.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title><tiles:getAsString name="title" /></title>
	<script>
		if (typeof Uniware == 'undefined') {
			Uniware = {};
		};
		Uniware.Path = {};
		Uniware.Path.http = function(path) {
			return '${path.http}' + path;
		}
	
		Uniware.Path.resources = function(path) {
			return '${path.resources("")}' + path;
		}
	</script>
	<link href="${path.css('unifier/ui.home.css')}" rel="stylesheet" type="text/css" />
	<link rel="shortcut icon" href="${path.https}/img/icons/favicon.ico?v=2" type="image/x-icon" />
	<link rel="icon" type="image/ico" href="${path.https}/img/icons/favicon.ico?v=2" />
</head>
<body>
	<div id="body-container">
		<div>
			<tiles:insertAttribute name="body" />
		</div>
	</div>
	<script type="text/javascript" src="${path.js('jquery/jquery.all.min.js')}"></script>
	<script type="text/javascript" src="${path.js('unifier/common.js')}"></script>
	<tiles:insertAttribute name="deferredScript" defaultValue="" />
</body>
</html>