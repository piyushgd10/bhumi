<!DOCTYPE HTML>
<html>
<head>
<title>Uniware</title>
<link rel="shortcut icon" id="favicon" href="/img/favicon.ico" type="image/png">
	
	<link rel="stylesheet" href="${path.css('unifier/bootstrap.min.css')}"/>
	<style>
		
		body{
			background: #000;
		}
		.progessBlock{
			position: fixed;
			top: 50%;
			left: 50%;
			margin-left: -280px;
			margin-top: -80px;
			width: 560px;
			height: 160px;
			color: #fff;
			font-size: 24px;
			line-height: 30px;
			text-align: center;
			font-family: "ProximaNovaRegular","Helvetica Neue",Helvetica,Arial,sans-serif;
		}
		/* .sr-only {
		    border: 0 none;
		    clip: rect(0px, 0px, 0px, 0px);
		    height: 1px;
		    margin: -1px;
		    overflow: hidden;
		    padding: 0;
		    position: absolute;
		    width: 1px;
		} */
	</style>
	<script src="${path.js('jquery/jquery.all.min.js')}"></script>
</head>
<body>
<div class="progessBlock">
	<div id="message">Your instance is being created...</div>
	<div class="progress progress-striped active">
	  <div class="bar" id="status" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100">
	    <!-- <span class="sr-only">45% Complete</span> -->
	  </div>
	</div>
</div>
	
	<!--[if lt IE 11]>
		<div style='border: 1px solid #F7941D; background: #FEEFDA; text-align: center; clear: both; height: 75px; position: relative;'>
	    <div style='position: absolute; right: 3px; top: 3px; font-family: courier new; font-weight: bold;'></div>
	    <div style='width: 640px; margin: 0 auto; text-align: left; padding: 0; overflow: hidden; color: black;'>
	      <div style='width: 75px; float: left;'><img src='/img/ie6nomore-warning.jpg' alt='Warning!'/></div>
	      <div style='width: 275px; float: left; font-family: Arial, sans-serif;'>
		<div style='font-size: 14px; font-weight: bold; margin-top: 12px;'>You are using an outdated browser</div>
		<div style='font-size: 12px; margin-top: 6px; line-height: 12px;'>For a better experience using this site, please upgrade to a modern web browser.</div>
	      </div>
	      <div style='width: 75px; float: left;'><a href='http://www.firefox.com' target='_blank'><img src='/img/ie6nomore-firefox.jpg' style='border: none;' alt='Get Firefox'/></a></div>
	      <div style='width: 73px; float: left;'><a href='http://www.apple.com/safari/download/' target='_blank'><img src='/img/ie6nomore-safari.jpg' style='border: none;' alt='Get Safari'/></a></div>
	      <div style='float: left;'><a href='http://www.google.com/chrome' target='_blank'><img src='/img/ie6nomore-chrome.jpg' style='border: none;' alt='Get Google Chrome'/></a></div>
	    </div>
	  </div>
	<![endif]-->
</body>
</html>
<script>
(function(){
	var userName = "${userName}";
	var password = "${password}";
	var tenantCode = "${tenantCode}";
	var lastStatus = "";
	var totalStates = ${totalStates};
	function checkStatus (code){
		$.ajax({
			url:"/data/user/tenant/new/status" ,
			type:"POST",
			data:JSON.stringify({
				"tenantCode":tenantCode
			}),
			dataType:"JSON",
			contentType:"application/json"
		}).done(function(data){
			if(data.tenantState.code=="DONE"){
				$.ajax({
					url:"/login_security_check",
					type:"POST",
					data:{
						"j_username":userName,
						"j_password":password
					},
					dataType:"JSON"
				}).done(function(data){
					if(data["successful"]==true){
						window.location.href="/dashboard";
					}
				});
			}else if(data.createTenantResponse && !data.createTenantResponse.successful){
				var message = data.createTenantResponse.errors[0].message;
				var description = data.createTenantResponse.errors[0].description;
				$("#message").text(message + " : " + description);
			}else{
				if(lastStatus!=data.tenantState.code){
					$("#status").width(100*(data.tenantState.id/totalStates)+"%");
					lastStatus=data.tenantState.code;
				}
				setTimeout(function(){
					checkStatus(tenantCode);
				},1000);
			}
			console.log(data);
		});
	}
	checkStatus(tenantCode);
})();
</script>
