<script type="text/javascript">
	if(window.top !== window.self){
		window.top.postMessage({type:"ErrorUserisNotLoggedIn",val:""}, '*');
	}
</script>
<%@ include file="/tagIncludes.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title><tiles:getAsString name="title" /></title>
	<link href="${path.css('unifier/ui.login.css')}" rel="stylesheet" type="text/css" />
	<script>
		if (typeof Uniware == 'undefined') {
			Uniware = {};
		};
		Uniware.Path = {};
		Uniware.Path.http = function(path) {
			return '${path.http}' + path;
		}
	
		Uniware.Path.resources = function(path) {
			return '${path.resources("")}' + path;
		}
	</script>
	<link rel="shortcut icon" href="${path.https}/img/icons/favicon.ico?v=2" type="image/x-icon" />
	<link rel="icon" type="image/ico" href="${path.https}/img/icons/favicon.ico?v=2" />
	<style>    
	.permission-wrapper {
	    background-color:#EEEEEE;
	    margin-top: 70px;
	    box-shadow: 0 0 15px #BBB;
	}
	</style>
</head>
<body id="body-login">
	<div id="calWidth" class="hidden"></div>
	<div id="actionInfo" style="margin:2px auto;padding: 5px 15px;text-align: center;top:0;left:50%;position: fixed;z-index: 1000;" class="alert alert-info bold hidden" ></div>
	<div id="body-header">
		<div class="row"><a id="logo-link" ></a></div>
	</div>
	<div id="body-main">
		<div class="row"><tiles:insertAttribute name="body" /></div>
	</div>
	<div class="loginFooter" align="right">
		&#169; 2015 Unicommerce eSolutions Pvt. Ltd. India
	</div>
	<script type="text/javascript" src="${path.js('jquery/jquery.all.min.js')}"></script>
	<script type="text/javascript" src="${path.js('unifier/common.js')}"></script>
	<tiles:insertAttribute name="deferredScript" defaultValue="" />
</body>
</html>
