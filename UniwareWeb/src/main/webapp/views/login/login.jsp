<script type="text/javascript">
	if(window.top !== window.self){
		window.top.postMessage({type:"ErrorUserisNotLoggedIn",val:""}, '*');
	}
</script>

<%@ include file="/tagIncludes.jsp"%>

<tiles:insertDefinition name=".loginPage">
	<tiles:putAttribute name="title" value="Login - Warehouse Management" />
	<tiles:putAttribute name="body">
	    <div class="content-wrapper">
	    	<div class="loginPageHead">Smartest way to fulfill your orders!</div>
	    	<div class="formBox">
	    		<div class="formProdInfoWrapper">
	    			<div class="formProdWrapper">
	    				<div class="formProdIco saas"></div>
	    				<div  class="formProdText">
	    					SaaS Platform
	    					<span>No capital investments. Free updates</span>
	    				</div>
	    			</div>
	    			<div class="formProdWrapper">
	    				<div class="formProdIco multiChannel"></div>
	    				<div  class="formProdText">
	    					Multi Channel Fulfilment
	    					<span>Calm the chaos of order management</span>
	    				</div>
	    			</div>
	    			<div class="formProdWrapper">
	    				<div class="formProdIco dashboard"></div>
	    				<div  class="formProdText">
	    					Dashboard and Alerts
	    					<span>Make more informed strategy</span>
	    				</div>
	    			</div>
	    			<div class="formProdWrapper" style="margin-bottom:0px">
	    				<div class="formProdIco intShipping"></div>
	    				<div  class="formProdText">
	    					Integrated Shipping
	    					<span>30+ Couriers integrated</span>
	    				</div>
	    			</div>
	    		</div>
	    		<div class="rfloat" style="width:70%">
			    	<div class="form-wrapper">
				        <div class="form-heading">Login to Uniware</div>
						<form action="${path.http}/login_security_check" method="post" class="login-form radius_five">
							<div id="form-login">
								<c:if test="${param['error'] eq true}">
									<p class="text-error">Invalid username or password</p>
								</c:if>
								<input type="text" name="j_username" size="40" class="textfield" placeholder="Email/Username">
								<input name="j_password" type="password" size="40" class="textfield" placeholder="Password">
								<input type="submit" class="btn loginBtn btn-primary lfloat" value="Log In"/>
								<label class="checkbox lfloat">
									<input name="_spring_security_remember_me" type="checkbox"> Keep me logged in
								</label>
								<div class="clearfix"></div>
							</div>
						</form>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		<script>
			Uniware.LoginPage = true;
		</script>
	</tiles:putAttribute>
</tiles:insertDefinition>