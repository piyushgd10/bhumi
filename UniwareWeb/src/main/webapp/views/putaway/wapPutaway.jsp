<%@ include file="/tagIncludes.jsp"%>
<tiles:insertDefinition name=".wapPage">
	<tiles:putAttribute name="title" value="Uniware - Putaway Complete" />
	<tiles:putAttribute name="body">
		<br />
		<div class="greybor headlable ovrhid main-box-head">
			<h2 class="edithead head-textfields">Putaway > Scan Items</h2>
		</div>
		<div id="putawayDiv"
			class="greybor round_bottom main-boform-cont ovrhid"
			style="padding: 10px;"></div>
	</tiles:putAttribute>
	<tiles:putAttribute name="deferredScript">
		<script id="putawayTemplate" type="text/html">
	<div id="error" class="hidden errorField"></div>
	<table>
		<tr>
			<td width="80">PUTAWAY : </td>
			<td><#= obj.putawayCode ? obj.putawayCode : '<input type="text" id="putawayCode" class="w150 ucase"/>' #></td>
		</tr>
		<# if (obj.putawayCode) { #>
		<tr>
			<td>SHELF : </td>
			<td><#= obj.shelfCode ? obj.shelfCode : '<input type="text" id="shelfCode" class="w150 ucase"/>' #></td>
		</tr>
		<tr>
			<td>Count : </td>
			<td><#= obj.itemCodes ? obj.itemCodes.length : 0 #></td>
		</tr>
		<# if (obj.shelfCode) { #>
		<tr>
			<td>Item/Shelf : </td>
			<td><input type="text" id="itemCode" class="w150 ucase"/></td>
		</tr>
		<# if (obj.itemCodes && obj.itemCodes.length > 0) { #>
		<tr>
			<td valign="top">Already Scanned Items: </td>
			<td class="ucase">
			<# for (var i=obj.itemCodes.length-1;i>=0;i--) { #>
				<span class="<#= i == obj.itemCodes.length-1 ? 'bold' : ''#>"><#=obj.itemCodes[i]#><br/></span>
			<# } #>
			</td>
		</tr>
		<# } #>
		<# }} #>
	</table>
	</script>
		<script type="text/javascript">
		Uniware.WapPutawayPage = function() {
			var self = this;
			this.wapPutaway = {};
			
			this.init = function() {
				self.render();
				$('#putawayCode').focus().keyup(self.load);
			};
			
			this.load = function(event) {
				if (event.which == 13 && $('#putawayCode').val() != '') {
					self.fetchPutaway($('#putawayCode').val());
				}
			};
			
			this.fetchPutaway = function(code) {
				$('#error').addClass('hidden');
				var req = {
					'code' : code	
				};
				Uniware.Ajax.postJson("/data/putaway/get", JSON.stringify(req), function(response) {
					if(response.successful == true) {
						self.wapPutaway.putawayCode = response.putawayDTO.code;
						self.render();
						$('#shelfCode').focus().keyup(self.fetchShelf);
					} else {
						$('#error').html(response.errors[0].description).removeClass('hidden');
						$('#putawayCode').val('');
					}
				});
			};

			this.fetchShelf = function(event) {
				if (event.which == 13 && $(this).val() != '') {
					$('#error').addClass('hidden');
					var req = {
						'code' : $(this).val()	
					};
					Uniware.Ajax.postJson("/data/putaway/shelf/get", JSON.stringify(req), function(response) {
						if(response.successful == true) {
							self.wapPutaway.shelfCode = response.shelfCode;
							self.wapPutaway.itemCodes = [];
							self.render();
							$('#itemCode').focus().keyup(self.scanItem);
						} else {
							$('#error').html(response.errors[0].description).removeClass('hidden');
							$('#shelfCode').val('');
						}
					});
				}
			};
			
			this.scanItem = function(event) {
				if (event.which == 13 && $(this).val() != '') {
					$('#error').addClass('hidden');
					if ($(this).val() == self.wapPutaway.shelfCode) {
						self.putawayDone();
					} else {
						var itemCode = $(this).val();
						if($.inArray(itemCode, self.wapPutaway.itemCodes)==-1){
							var req = {};
							req.putawayCode = self.wapPutaway.putawayCode;
							req.itemCode = $(this).val();
							Uniware.Ajax.postJson('/data/putaway/item/check', JSON.stringify(req), function(response){
								if (response.successful) {
									if($.inArray(req.itemCode, self.wapPutaway.itemCodes)==-1) {
										self.wapPutaway.itemCodes.push(req.itemCode);
									}
									self.render();
									$('#itemCode').focus().keyup(self.scanItem);
								} else {
									$('#error').html(response.errors[0].description).removeClass('hidden');
									$('#itemCode').val('').blur();
								}
							});
						}else{
							$('#error').html('Item already scanned in this putaway').removeClass('hidden');
							$('#itemCode').val('').blur();
						}
					}
				}
			};
			
			this.putawayDone = function() {
				var req = $.extend(true, {}, self.wapPutaway);
				Uniware.Ajax.postJson('/data/putaway/items/complete', JSON.stringify(req), function(response){
					if (response.successful == true) {
						self.wapPutaway = {};
						self.render();
						$('#putawayDiv').prepend('<span class="f15" style="color:green;">Putaway Done Successfully</span>');
					} else {
						$('#error').html(response.errors[0].description).removeClass('hidden');
					}
				});
			};
			
			this.render = function() {
				$('#putawayDiv').html(template('putawayTemplate', self.wapPutaway));
			};
			
		}
		
		$(document).ready(function() {
			window.page = new Uniware.WapPutawayPage();
			window.page.init();
		});
	</script>
	</tiles:putAttribute>
</tiles:insertDefinition>
