<%@ include file="/tagIncludes.jsp"%>
<tiles:insertDefinition name=".homePage">
	<tiles:putAttribute name="title" value="Uniware - Warehouse Management - Home Page" />
	<tiles:putAttribute name="rightPane">
		<div style="padding:15px 0;margin:0 10px;font-size:25px;border-bottom:1px solid #E6E6E6;">
			<i class="icon-home"></i>&#160;Dashboard
		</div>
		<div id="charts"></div>
		<div id="chartWidgetOverlay"></div>
		<div class="lb-over" id="chartMaximiseOverlay">
			<div class="chartZoomClose" ><span id="chartMaximiseOverlay_close"></span></div>
			<div class="lb-over-inner round_all" id="chartZoom"></div>	
		</div>
		<div id="leftWidgetPreview">
			<div class="chartZoomClose"><span id="leftWidgetPreview_close"></span></div>
			<div class="lb-over-inner round_all" id="chartPreview"></div>
			<div class="clear"></div>
			<div id="widget-preview-add" class="btn-grey round_all lfloat">+ Add to dashboard</div>
			<div class="clear"></div>
		</div>
		<div id="widget-preview-arrow"></div>	
		<div class="widgetBox-outer">
			<div class="widgetAction">
				<div class="widgetActionIcon"></div>
				<div class="widgetActionHoverText">Add More Widgets</div>
			</div>
			<div class="clear"></div>
			<div class="widgetsBox">
				<div class="widgets-bg" id="widgetIcons"></div>
			</div>
		</div>
	</tiles:putAttribute> 
	<tiles:putAttribute name="deferredScript">
		<script type="text/javascript" src="${path.js('unifier/FusionCharts.js')}"></script>
		<script type="text/javascript" src="${path.js('unifier/FusionCharts.HC.js')}"></script>
		<script type="text/javascript" src="${path.js('unifier/FusionCharts.HC.Charts.js')}"></script>
		<script type="text/javascript">
			Uniware.AreaChartData = {
			    "chart": {
			        "bgcolor": "E9E9E9",
			        "outcnvbasefontcolor": "666666",
			        "caption": "",
			        "xaxisname": "",
			        "yaxisname": "",
			        "numberprefix": "",
			        "showlabels": "1",
			        "showvalues": "0",
			        "plotfillalpha": "90",
			        "numvdivlines": "10",
			        "showalternatevgridcolor": "1",
			        "alternatevgridcolor": "e1f5ff",
			        "divlinecolor": "e1f5ff",
			        "vdivlinecolor": "e1f5ff",
			        "basefontcolor": "666666",
			        "canvasborderthickness": "0",
			        "showplotborder": "0",
			        "showborder" : "0",
			        "plotborderthickness": "0",
			        "canvaspadding": "8"
			    },
			    "categories": [
			        {
			            "category": []
			        }
			    ],
			    "dataset": [
			        {
			            "seriesname": "",
			            "color": "D22024",
			            "plotbordercolor": "D22024",
			            "data": []
			        },
			        {
			            "seriesname": "",
			            "color": "7EA214",
			            "plotbordercolor": "7EA214",
			            "data": []
			        }
			    ]
			};
			Uniware.ColumnChartData = {
			    "chart": {
			    	"xaxisname": "",
			        "yaxisname": "",
			        "caption": "",
			        "numberprefix": "",
			        "useroundedges": "1",
			        "showborder": "0"
			    },
			    "data": []
			};
			Uniware.PieChartData = {
			    "chart": {
			        "caption": "",
			        "palette": "2",
			        "animation": "1",
			        "subcaption": "",
			        "yaxisname": "",
			        "showvalues": "1",
			        "numberprefix": "",
			        "formatnumberscale": "0",
			        "showpercentintooltip": "1",
			        "canvasborderthickness": "0",
			        "showplotborder": "0",
			        "plotborderthickness": "0",
			        "canvasborderalpha":"0",
			        "showlabels": "1",
			        "showborder": "0",
			        "showlegend": "0"
			    },
			    "data": [],
			    "styles": {
			        "definition": [
			            {
			                "type": "font",
			                "name": "CaptionFont",
			                "color": "666666",
			                "size": "15"
			            },
			            {
			                "type": "font",
			                "name": "SubCaptionFont",
			                "bold": "0"
			            }
			        ],
			        "application": [
			            {
			                "toobject": "caption",
			                "styles": "CaptionFont"
			            },
			            {
			                "toobject": "SubCaption",
			                "styles": "SubCaptionFont"
			            }
			        ]
			    }
			}
			
			Uniware.DashboardPage = function() {
				var self = this;
				this.chartGroups = {};
				this.allChartWidgets = {};
				this.leftChartWidgets = {};
				this.chartData = {};
				this.init = function() {
					<c:set var="dashboardWidgets" value="${configuration.getConfiguration('dashboardWidgetConfiguration').dashboardWidgets}"></c:set>
					<c:forEach items="${dashboardWidgets}" var="dashboardWidget">
					<uc:security accessResource="${dashboardWidget.accessResourceName}">
						var chartWidget = {};
						chartWidget.name = '${dashboardWidget.name}';
						chartWidget.name = chartWidget.name.replace(/_/g,' ');
						chartWidget.code = '${dashboardWidget.code}';
						chartWidget.widgetGroup = '${dashboardWidget.widgetGroup}';
						self.allChartWidgets[chartWidget.code] = chartWidget;
						self.leftChartWidgets[chartWidget.code] = chartWidget;
					</uc:security>
					</c:forEach>
					for (var code in self.allChartWidgets) {
						chartWidget = self.allChartWidgets[code];
						if (!self.chartGroups[chartWidget.widgetGroup]) {
							self.chartGroups[chartWidget.widgetGroup] = [];
						}
						self.chartGroups[chartWidget.widgetGroup].push(chartWidget.code);
					}
					Uniware.Ajax.getJson('/data/admin/widgets/user/get', self.loadUserWidgets);
					$( "#charts" ).sortable({
						   stop: self.reorderWidgets
					}).draggable();
				};
				
				this.reorderWidgets = function() {
					var req = {
						'widgets': []
					};
					var sequence = 0;
					$(".chart").each(function() {
						req.widgets.push({'code' : $(this).attr('id').split('-')[1], 'sequence' : ++sequence});
					});
					Uniware.Ajax.postJson('/data/admin/widget/reorder', JSON.stringify(req), function(response) {
						if(response.successful){
							
						} else {
							Uniware.Utils.showError(response.errors[0].description);
						}
					});
				};
				
				this.loadUserWidgets = function(response) {
					for (var i=0;i<response.length;i++) {
						var res = response[i];
						var chartWidget = self.allChartWidgets[response[i].code];
						if (chartWidget) {
    						delete self.leftChartWidgets[response[i].code];
    						$('div#charts').append(template("dashBoardCharts", {'code' : res.code}));
    						$('#zoom-' + res.code).click(self.chartZoom);
    						$('#refresh-' + res.code).click(self.chartRefresh);
    						$('#remove-' + res.code).click(self.removeChart);
    						(function(res) {
    							Uniware.Ajax.getJson('/data/admin/widget/get/' + chartWidget.code, function(data) {
    								self.chartData[res.code] = data;
    								self.renderChart(res.code, data, {
    									width : 350,
    									height: 210
    								});
    							});
    						})(res);
						}
					}
					$('.widgetsBox').on('click', '.previewWidget', self.previewWidget);
					
					
					$('.widgetAction, #lightBox').click(self.toggleWidgetPanel);
					
					for (var code in self.leftChartWidgets) {
						$('.widgetBox-outer').css("visibility","visible");
					}
				};
				
				this.toggleWidgetPanel = function(){
					$('.widgetActionIcon').toggleClass('widget-close');
					$('.widgetActionHoverText').toggleClass('widgetActionActive');
					$('.widgetsBox').slideToggle('slow', function() {
						if($('.widgetsBox').css('display') == "block"){
							$(".widgets-bg").animate({"left": "0px"}, "slow");
						} 
						else{
							var left = 0;
							for (var obj in self.leftChartWidgets) {
								left -= 120;
							}
							$(".widgets-bg").animate({"left": left + "px"}, "slow");
						}
					  });
					if ($('.widgetActionIcon').hasClass('widget-close')) {
						$('#lightBox').show();
						$(window).bind("keypress", function(e) {
							if (e.keyCode == 27) {
								self.toggleWidgetPanel();
							}
						});
						for(var code in self.leftChartWidgets){
							$('div#widgetIcons').append(template("leftChart", {'leftChart' : self.leftChartWidgets[code]}));
							$(".widgets-bg").css( "left","-=120" );
						}
					} else {
						$('#lightBox').hide();
						$(window).unbind("keypress");
						$('div#widgetIcons').html("");
						$('#leftWidgetPreview').hide();
						$('#widget-preview-arrow').hide();
					};
				};
				
				this.chartZoom = function() {
					var data = self.chartData[$(this).attr('id').split('-')[1]];
					Uniware.LightBox.show('#chartMaximiseOverlay', function(){$('.widgetAction').hide()}, null, function(){$('.widgetAction').show()});
					self.renderChart('Zoom', data, {
						width : 700,
						height: 420
					});
					
				};
				
				this.chartRefresh = function() {
					var code = $(this).attr('id').split('-')[1];
					Uniware.Ajax.getJson('/data/admin/widget/get/' + code, function(data) {
						self.chartData[code] = data;
						self.renderChart(code, data, {
							width : 350,
							height: 210
						});
					});
				};
				
				this.removeChart = function() {
					var code = $(this).attr('id').split('-')[1];
					var req = {'code' : code}
					
					Uniware.Ajax.postJson('/data/admin/widget/remove', JSON.stringify(req), function(response) {
						if(response.successful){
							$('#chartBox-' + code).effect('transfer', {to: ".widgetActionIcon", className: "ui-effects-transfer"}, 2000).remove();
							self.leftChartWidgets[code] = self.allChartWidgets[code];
						}else{
							Uniware.Utils.showError(response.errors[0].description);
						}
					});
					$('.widgetBox-outer').css("visibility","visible");
				};
				
				this.previewWidget = function() {
					var code = $(this).attr('id').split('-')[1];
					self.selectedWidget = code;
					var viewportWidth = Uniware.Utils.getViewportDimensions().w - 720;
					var data = self.chartData[code];
					$('#leftWidgetPreview').show();
					$('#widget-preview-arrow').show();
					var widgetPrvArrow = $('#widgetIcon-' +self.selectedWidget).position().left;
					var widgetPrvBox = $('#widgetIcon-' +code).position().left;
					if (widgetPrvBox < 45){
						var widgetPrvBox = 45;
					};	
					$('#widget-preview-arrow').animate({"left": widgetPrvArrow + "px"}, "slow");
					if (widgetPrvBox > viewportWidth){
						var widgetPrvBox = viewportWidth;
						$('#leftWidgetPreview').animate({"left": widgetPrvBox + "px"}, "slow");
					}else{
						$('#leftWidgetPreview').animate({"left": widgetPrvBox + "px"}, "slow");
					};
					
					$('#widget-preview-add').unbind('click').click(self.addWidget);
					if (data==null){
						Uniware.Ajax.getJson('/data/admin/widget/get/' + code, function(data) {
							self.chartData[code] = data;
							self.renderChart('Preview', data, {
								width : 700,
								height: 420
							});
						});
					}else{
						self.renderChart('Preview', data, {
							width : 700,
							height: 420
						});
					}
				};
				
				this.addWidget = function(){
					var req = {
						'code' : self.selectedWidget
					}
					Uniware.Ajax.postJson('/data/admin/widget/add', JSON.stringify(req), function(response) {
						if(response.successful){
							$('#widgetIcon-' + self.selectedWidget).remove();
							$('#leftWidgetPreview').hide();
							$('#widget-preview-arrow').hide();
							$('#chartPreview').html('');
							delete self.leftChartWidgets[self.selectedWidget];
							var leftChartCount = 0;
							for (var code in self.leftChartWidgets) {
								leftChartCount++;
								break;
							};
							if (leftChartCount == 0){
								$('.widgetBox-outer').css("visibility","hidden");
								$('#chartWidgetOverlay').toggleClass('chartWidgetOverlay-block');
								$('.widgetsBox').hide();
								$('.widgetActionHoverText').removeClass('widgetActionActive');
								$('.widgetActionIcon').removeClass('widget-close');
								$('#lightBox').hide();
							};
							$('div#charts').append(template("dashBoardCharts", {'code' : self.selectedWidget}));
							$('#zoom-' + self.selectedWidget).click(self.chartZoom);
							$('#refresh-' + self.selectedWidget).click(self.chartRefresh);
							$('#remove-' + self.selectedWidget).click(self.removeChart);
							
							self.renderChart(self.selectedWidget, self.chartData[self.selectedWidget], {
								width : 350,
								height: 210
							});
						} else {
							Uniware.Utils.showError(response.errors[0].description);
						}
					});
				};
				
				this.renderChart = function(id, data, options) {
					if(!$.isArray(data.dataset)){
						data.dataset = $.makeArray(data.dataset);	
					}
					if (data.type == 'PIE') {
						var chartData = $.extend(true, {}, Uniware.PieChartData);
						chartData.chart.caption = data.title;
						for(var i=0;i<data.dataset.length;i++){
							chartData.data.push({'label':data.dataset[i].name, 'value': parseInt(data.dataset[i].value)});
						}
						FusionCharts.setCurrentRenderer("javascript");
				        chartObj = new FusionCharts({
				        	type:'Pie2D',
				           width: options.width, height: options.height,
				           id: 'chartFS' + id,
				           renderAt: 'chart' + id
				        });
				        chartObj.setJSONData(chartData);
				        chartObj.render();
						
					} else if (data.type == 'BAR') {
						var chartData = $.extend(true, {}, Uniware.ColumnChartData);
						chartData.chart.caption = data.title;
						for(var i=0;i<data.dataset.length;i++){
							chartData.data.push({'label':data.dataset[i].name, 'value': parseInt(data.dataset[i].value)});
						}
						chartData.chart.yaxisname = data.yTitle;
						chartData.chart.xaxisname = data.xTitle;
						FusionCharts.setCurrentRenderer("javascript");
				        chartObj = new FusionCharts({
				        	type:'Column2D',
				        	width: options.width, height: options.height,
				           id: 'chartFS' + id,
				           renderAt: 'chart' + id
				        });
				        chartObj.setJSONData(chartData);
				        chartObj.render();
					} else if (data.type == 'AREA') {
						var chartData = $.extend(true, {}, Uniware.AreaChartData);
						chartData.chart.caption = data.title;
						chartData.chart.yaxisname = data.yTitle;
						for(var i=0;i<data.labels.length;i++) {
							chartData.categories[0].category.push({'label':data.labels[i]});
						}
						for (var j=0;j<data.dataset.length;j++) {
							for (var k = 0;k<data.dataset[j].values.length;k++) {
								chartData.dataset[j].data.push({'value':parseInt(data.dataset[j].values[k])});
							}
							chartData.dataset[j].seriesname = data.dataset[j].name;
						}
						
						FusionCharts.setCurrentRenderer("javascript");
				        chartObj = new FusionCharts({
				        	type:'MSArea',
				        	width: options.width, height: options.height,
							id: 'chartFS' + id,
							renderAt: 'chart' + id
				        });
				        chartObj.setJSONData(chartData);
				        chartObj.render();
					}
					
				};
			};
			
			$(document).ready(function() {
				window.page = new Uniware.DashboardPage();
				window.page.init();
				$('#leftWidgetPreview_close').click(function(){
					$('#leftWidgetPreview').hide();
					$('#widget-preview-arrow').hide();
				});
			});
		</script>
		<script id="dashBoardCharts" type="text/html">
			<div style="width:350px; height:226px;" class="round_all chart" id="chartBox-<#=obj.code#>">
				<div class="chartZoom" id="chartButtons-<#=obj.code#>">
					<i id="remove-<#=obj.code#>" class="icon-remove-sign rfloat5"></i>
					<i id="zoom-<#=obj.code#>" class="icon-zoom-in rfloat5"></i>
					<i id="refresh-<#=obj.code#>" class="icon-refresh rfloat5"></i>
				</div>
				<div id="chart<#=obj.code#>"/>
			</div>
		</script>
		<script id="leftChart" type="text/html">
			<div class="previewWidget widget-icon-box" align="center" title="click to preview" id="widgetIcon-<#=obj.leftChart.code#>">
				<img src="/img/charts/<#=obj.leftChart.code#>.png"/>
				<div class="widget-icon-text"><#=obj.leftChart.name#></div>
			</div>
		</script>
	</tiles:putAttribute>
</tiles:insertDefinition>
