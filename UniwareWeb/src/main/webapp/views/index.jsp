<%@ include file="/tagIncludes.jsp"%>
<!DOCTYPE HTML>
<html>
<head>
<title>Uniware</title>
<script>
	if(window.top !== window.self){
		if(typeof window.top.postMessage === "function"){
			window.top.postMessage({type:"ErrorUserisNotLoggedIn",val:""}, '*');
		}
	}
</script>
<link rel="shortcut icon" id="favicon" href="${cache.getCache('environmentCache').staticContentServerUrl}/img/images/favicon.ico" type="image/png">
	<style>
		.loadingPage > div{
			background: url(${cache.getCache('environmentCache').staticContentServerUrl}/img/images/loading_help_black.gif?v=1) no-repeat center center;
			position: fixed;
			height: 100%;
			width: 100%;
		}
		.loadingPage {
			background: url(${cache.getCache('environmentCache').staticContentServerUrl}/img/images/bodyBgNew.png) repeat, #363838;
		}
	</style>
</head>
<body class="loadingPage">
	<div id="hello"></div>
	<!--[if lt IE 11]>
		<div style='border: 1px solid #F7941D; background: #FEEFDA; text-align: center; clear: both; height: 75px; position: relative;'>
	    <div style='position: absolute; right: 3px; top: 3px; font-family: courier new; font-weight: bold;'></div>
	    <div style='width: 640px; margin: 0 auto; text-align: left; padding: 0; overflow: hidden; color: black;'>
	      <div style='width: 75px; float: left;'><img src='/img/ie6nomore-warning.jpg' alt='Warning!'/></div>
	      <div style='width: 275px; float: left; font-family: Arial, sans-serif;'>
		<div style='font-size: 14px; font-weight: bold; margin-top: 12px;'>You are using an outdated browser</div>
		<div style='font-size: 12px; margin-top: 6px; line-height: 12px;'>For a better experience using this site, please upgrade to a modern web browser.</div>
	      </div>
	      <div style='width: 75px; float: left;'><a href='http://www.firefox.com' target='_blank'><img src='/img/ie6nomore-firefox.jpg' style='border: none;' alt='Get Firefox'/></a></div>
	      <div style='width: 73px; float: left;'><a href='http://www.apple.com/safari/download/' target='_blank'><img src='/img/ie6nomore-safari.jpg' style='border: none;' alt='Get Safari'/></a></div>
	      <div style='float: left;'><a href='http://www.google.com/chrome' target='_blank'><img src='/img/ie6nomore-chrome.jpg' style='border: none;' alt='Get Google Chrome'/></a></div>
	    </div>
	  </div>
	<![endif]-->
</body>
</html>
<script>
	var UniWeb = {};
	UniWeb.DataAPIServer = '';
	UniWeb.remotejsServer = '${cache.getCache('environmentCache').staticContentServerUrl}';
	UniWeb.googleAnalyticsCode = '${cache.getCache('environmentCache').googleAnalyticsCode}';
    UniWeb.authServerUrl = '${cache.getCache('environmentCache').authServerUrl}';
	UniWeb.webSocketEnabled = 'true';
</script>
<script src="${cache.getCache('environmentCache').staticContentServerUrl}/loader/initialPayload.min.js?_time=${time}"></script>
<c:if test="${cache.getCache('tenantCache').getCurrentTenant().product.code == 'STANDARD'}">
<!--  Webengage Start 
<script id="_webengage_script_tag" type="text/javascript">
  var _weq = _weq || {};
  _weq['webengage.licenseCode'] = '82616cb6';
  _weq['webengage.widgetVersion'] = "4.0";
  
  (function(d){
    var _we = d.createElement('script');
    _we.type = 'text/javascript';
    _we.async = true;
    _we.src = (d.location.protocol == 'https:' ? "https://ssl.widgets.webengage.com" : "http://cdn.widgets.webengage.com") + "/js/widget/webengage-min-v-4.0.js";
    var _sNode = d.getElementById('_webengage_script_tag');
    _sNode.parentNode.insertBefore(_we, _sNode);
  })(document);
</script>
Webengage End -->
</c:if>
