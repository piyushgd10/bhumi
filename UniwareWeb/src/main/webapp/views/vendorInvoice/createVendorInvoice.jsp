<%@ include file="/tagIncludes.jsp"%>
<tiles:insertDefinition name=".plainPage">
	<tiles:putAttribute name="title" value="Uniware - Warehouse Management - Create Vendor Invoice" />
	<tiles:putAttribute name="body">
		<div id="dataTable"></div>
	</tiles:putAttribute>
	<tiles:putAttribute name="deferredScript">
		<link href="${path.css('handsontable/jquery.handsontable.full.css')}" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="${path.js('jquery/ajaxfileupload.js')}"></script>
		<script type="text/javascript" src="${path.js('handsontable/jquery.handsontable.js')}"></script>
		<script type="text/javascript">
			Uniware.CreateVendorInvoicePage = function() {
				var self = this;
				this.spreadSheet = null;
				this.init = function() {
					var req = {
						'purchaseOrderCode' : 'NAES/12-13/3860'
					};
					Uniware.Ajax.postJson("/data/po/fetch", JSON.stringify(req), function(response) {
						if (response.successful == true) {
							self.purchaseOrderData = response;
							self.load();
						} else {
							Uniware.Utils.showError(response.errors[0].description);
						}
					});
				};

				this.load = function() {
					var items;
					var itemsMap;
					var poItems = self.purchaseOrderData.purchaseOrderItems;
					var columns = [ {
						type : 'autocomplete',
						options : {
							items : 10,
							minLength : 10
						}, //`options` overrides `defaults` defined in bootstrap typeahead
						source : function(request, process) {
							items = [];
							itemsMap = {};
							Uniware.Ajax.getJson("/data/lookup/vendorItemTypes?vendorCode=" + 's00000' + "&keyword=" + request, function(data) {
								$.each(data.elements, function(i, item) {
									itemsMap[item.name] = item;
									items.push(item.name);
								});
								process(items);
							});
						},
						strict : true
					}, {
						data : "itemSKU"
					}, {
						data : "vendorSkuCode"
					}, {
						data : "quantity"
					}, {
						data : "pendingQuantity"
					}, {
						data : "rejectedQuantity"
					}, {
						data : "unitPrice"
					}, {
						data : "discount"
					}, {
						data : "discountPercentage"
					}, {
						data : "total"
					} ];
					var colHeaders = [ "Product Name", "Product Code", "Vendor SKU Code", "Quantity", "Pending Quantity", "Rejected Quantity", "Unit Price", "Discount", "Discount Percentage",
							"Total Price" ];

					self.spreadSheet = new Uniware.SpreadSheet('#dataTable', self.purchaseOrderData.purchaseOrderItems, colHeaders, columns, self.changeColumn);
					self.spreadSheet.init();
				};

				this.changeColumn = function(data, source) {
					if (data != null) {
						self.spreadSheet.changeColumn(data, source, 0, 1, data[0][3] * 2);
					}
				}
			};

			$(document).ready(function() {
				window.page = new Uniware.CreateVendorInvoicePage();
				window.page.init();
			});
		</script>
	</tiles:putAttribute>
</tiles:insertDefinition>
