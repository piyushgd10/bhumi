<%@ include file="/tagIncludes.jsp"%>
<tiles:insertTemplate template="/views/template/homeBase.jsp">
	<tiles:putAttribute name="title" value="Uniware - Page Not Found" />
	<tiles:putAttribute name="header">
		<div id="body-header">
			<div class="lfloat">
				<a id="logo-link" href="/" target="_top"></a>
			</div>
		</div>
		<div class="clear"></div>
	</tiles:putAttribute>	
	<tiles:putAttribute name="body">
		<h1>Error Code 404 - Page Not Found</h1>
	</tiles:putAttribute> 
</tiles:insertTemplate>