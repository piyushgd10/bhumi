<script type="text/javascript">
	if(window.top !== window.self){
		window.top.postMessage({type:"Error403",val:""}, '*');
	}
</script>
<%@ include file="/tagIncludes.jsp"%>
<tiles:insertTemplate template="/views/template/homeBase.jsp">
	<tiles:putAttribute name="title" value="Uniware - Access Denied" />
	<tiles:putAttribute name="header">
		<div id="body-header">
			<div class="lfloat">
				<a id="logo-link" href="/" target="_top"></a>
			</div>
		</div>
		<div class="clear"></div>		
	</tiles:putAttribute>	
	<tiles:putAttribute name="body">
		<div class="round_all greybor" style="padding:20px 40px 80px;">
			<h1 style="color:#666;">Access Denied</h1>
			<br/>
			<span class="f15">
				<span class="errorField">Oops!</span> You are not authorized to view this page.<br/>
				Please contact your system administrator for access.
			</span>	
		</div>
	</tiles:putAttribute> 
</tiles:insertTemplate>