<%@ include file="/tagIncludes.jsp"%>
<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="${path.css('loginframe/login.css')}" media="screen">
    <script src="https://code.jquery.com/jquery-1.11.2.min.js"></script>
</head>
<body>
<box class="frame">
    <right class="login">
        <form id="ajaxform">
            <header>Login<br/><to></to>  <change id="change">(change)</change></header>
            <inputs>
                <input type="text" name="j_username" tabindex="1" size="40"  class="textfield " value="" required>
                <label class="email">EMAIL/MOBILE</label>
                <input type="password" name="j_password" tabindex="2" size="40"  class="textfield " value="" required>
                <label class="password">PASSWORD</label>
            </inputs>
            <tools>
                <forgot>Forgot Password?</forgot>
                <remember>
                    <label for="remember"> <input type="checkbox" id="remember" name="_spring_security_remember_me" tabindex="3"> Remember me</label>
                </remember>
                <input type="button" class="login-btn" value="Log In" tabindex="4" />
            </tools>
        </form>
        <%--<login-with data-title="-or-">--%>
        <%--<google  tabindex="5">Login with Google+</google>--%>
        <%--</login-with>--%>
    </right>

    <right class="forgot" style="display: none">
        <form id="forgotFrom">
            <header><to>Forgot Password?</to></header>
            <des>Enter your Email Address or your mobile number to reset your password.</des>
            <inputs>
                <input type="text" id="forgetEmail" name="email" tabindex="1" class="textfield " value="" required>
                <label class="email" data-error="Invalid email/mobile">EMAIL/MOBILE</label>
            </inputs>
            <tools>
                <back>Back to log in</back>
                <input type="button" tabindex="4" class="login-btn" value="Reset Password" />
            </tools>
        </form>

    </right>
    <right class="sent" style="display: none">
        <form>
            <header><to>We sent you an email</to></header>
            <des>Follow the instructions to create a new password.</des>
            <tools>
                <input type="button" tabindex="4" class="login-btn" value="Back to log in" />
            </tools>
        </form>

    </right>
    <right class="sentOTP" style="display: none">
        <form>
            <header><to>One Time Password Sent</to></header>
            <des>We have sent your One Time Password to your mobile number.</des>
            <tools>
                <input type="button" tabindex="4" class="login-btn" value="Back to log in" />
                <back><br><br>Re-send OTP</back>
            </tools>
        </form>

    </right>
</box>

<script type="text/javascript">

    var loginForm = document.getElementsByClassName('login'),
            forgotForm = document.getElementsByClassName('forgot'),
            emailSent = document.getElementsByClassName('sent'),
            otpSent = document.getElementsByClassName('sentOTP'),
            curUrl = window.location.host;

    if(window.top !== window.self){
        if(typeof window.top.postMessage === "function"){
            document.getElementById("change").addEventListener("click", function () {
                window.top.postMessage({type:"ChangeNeededInClientName",val:""}, '*');
            }, false);
        }
    }else{
        //window.location = "/login";
    }


    loginForm[0].getElementsByTagName('to')[0].innerHTML = curUrl;
    loginForm[0].getElementsByTagName('forgot')[0].onclick = function(){
        loginForm[0].style.display = "none";
        forgotForm[0].style.display = "block";
    };
    forgotForm[0].getElementsByTagName('back')[0].onclick = function(){
        loginForm[0].style.display = "block";
        forgotForm[0].style.display = "none";
    };
    emailSent[0].getElementsByClassName('login-btn')[0].onclick = function(){
        loginForm[0].style.display = "block";
        emailSent[0].style.display = "none";
    };
    otpSent[0].getElementsByClassName('login-btn')[0].onclick = function(){
        loginForm[0].style.display = "block";
        otpSent[0].style.display = "none";
    };

    $("#ajaxform .login-btn").click(function(e) {
        var postData = $(this).serializeArray();
        $.ajax({
            url: "/login_security_check",
            type: "POST",
            dataType: "json", // expected format for response
            data: postData,
            xhrFields: {
                withCredentials: true
            },
            crossDomain: true,
            success: function(data) {
                if (data.successful === true) {
                    if (data.targetUrl) {
                    } else {
                        data.targetUrl = "/";
                    }

                    if(window.top !== window.self){
                        if(typeof window.top.postMessage === "function"){
                            window.top.postMessage({type:"loginSuccess",val: "https://"+ window.location.host + data.targetUrl}, '*');
                        }
                    }else{
                        //window.location = "/login";
                    }
                } else {
                    window.top.postMessage({type:"loginFailure",val: "https://" + window.location.host + "/login"}, '*');
                    localStorage.setItem("loginstatus", "loginFailure");
                }


            }
        });
        return false;
    });
    var iVal;
    var sendOTP = function(){
        $.ajax({
            url: "/data/myaccount/forgotPassword",
            type: "POST",
            dataType: "json", // expected format for response
            contentType: "application/json",
            data: JSON.stringify({
                emailOrMobile: iVal
            }),
            crossDomain: true,
            success: function(data) {
                if (data.successful === true) {
                    loginForm[0].style.display = "none";
                    forgotForm[0].style.display = "none";
                    if(isNaN(iVal)){
                        emailSent[0].style.display = "block";
                    } else{
                        otpSent[0].style.display = "block";
                        $('back',otpSent[0]).removeClass('disable');
                    }

                } else {
                    loginForm[0].style.display = "none";
                    forgotForm[0].style.display = "block";
                    emailSent[0].style.display = "none";
                    otpSent[0].style.display = "none";
                    $("#forgetEmail").addClass('error').val("");
                }
            }
        });
        return false;
    };

    $('#forgotFrom .login-btn').click(function(e){
        iVal = $("#forgetEmail").val();
        return sendOTP();
    });
    $('back',otpSent[0]).click(function(){
        $(this).addClass('disable');
        return sendOTP();
    })
    $(document).ready(function(){
        window.top.postMessage({type:"iframeLoaded",val:""}, '*');
    });
</script>
</body>
</html>