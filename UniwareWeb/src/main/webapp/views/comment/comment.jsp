<%@ include file="/tagIncludes.jsp"%>
<tiles:insertDefinition name=".plainPage">
	<tiles:putAttribute name="title" value="Uniware - Warehouse Management - Comments" />
	<sec:authentication property="principal" var="user" />
	<tiles:putAttribute name="body">
		<div id="comments" class="round_all_b" style="padding-top: 5px;">
			<ul id="commentsList"></ul>
		</div>
	</tiles:putAttribute>
	<tiles:putAttribute name="deferredScript">
		<script id="commentsTemplate" type="text/html">
			<div class="commentsBox">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
  					<tr>
    					<td><textarea rows="2" id="commentBox" placeholder="Write a comment..."></textarea></td>
    					<td width="150" align="center">
							<div id="postComment" class="btn btn-large f18">
								<i class="icon-comments"/> Comment
							</div>
						</td>
  					</tr>
				</table>
			</div>
			<# for(var i=0; i<obj.length; i++){ var com = obj[i];#>
				<li>
					<# if(com.username == '${user.username}' && !com.systemGenerated) { #>
						<div class="deletecomment" id="<#=com.commentId#>-<#=i#>" title="remove this comment"></div>
					<# } #>
					<div class="commentsUserName"><strong><#=com.username#></strong> <#=com.comment#></div> 
					<div class="commentsDate"><#= (new Date(com.created)).toDateTime()#></div>
				</li>			
			<# } #>
		</script>
		<script type="text/javascript">
			Uniware.CommentsPage = function() {
				var self = this;
				this.comments = ${comments};
				this.referenceIdentifier = "${param['referenceIdentifier']}";

				this.init = function() {
					$('#commentsList').html(template("commentsTemplate", self.comments));
					$('#postComment').click(self.addComment);
					$('.deletecomment').click(self.deleteComment);
				};

				this.addComment = function(event) {
					var req = {
							comment : $('#commentBox').val(),
							referenceIdentifier : self.referenceIdentifier
					}
					Uniware.Ajax.postJson("/data/comment/add", JSON.stringify(req), function(response) {
						if(response.successful == true){
							Uniware.Utils.addNotification("Comment has been added");
							self.comments.splice(0,0,response.userCommentDTO);
							self.init();
						}else{
							Uniware.Utils.showError(response.errors[0].description);
						}
					}, true);
				};
				
				this.deleteComment = function(){
					var id = $(this).attr('id');
					var commentId = id.split("-")[0];
					var index = id.split("-")[1];
					var req = {
							'commentId' : commentId
					}
					Uniware.Ajax.postJson("/data/comment/delete", JSON.stringify(req), function(response) {
						if(response.successful == true){
							Uniware.Utils.addNotification("Comment has been deleted");
							self.comments.splice(index, 1);
							self.init();
						}else{
							Uniware.Utils.showError(response.errors[0].description);
						}
					}, true);
				};
			};

			$(document).ready(function() {
				window.page = new Uniware.CommentsPage();
				window.page.init();
			});
		</script>
	</tiles:putAttribute>
</tiles:insertDefinition>
