<%@ include file="/tagIncludes.jsp"%>
<tiles:insertDefinition name=".wapPage">
	<tiles:putAttribute name="title" value="Uniware - WAP Login Page" />
	<tiles:putAttribute name="body">
	<div style="font-size:11px;">
		<div class="form-heading">Login to Uniware</div>
		<form action="${path.http}/login_security_check" method="post" class="login-form radius_five">
			<div id="form-login">
				<c:if test="${param['error'] eq true}">
					<p class="text-error">Invalid username or password</p>
				</c:if>
				<br/>Username : <br/>
				<input type="text" name="j_username" size="40" class="textfield">
				<br/><br/>Password : <br/>
				<input name="j_password" type="password" size="40" class="textfield">
				<br/><br/>
				<input type="submit" class="btn loginBtn btn-small lfloat" value="Log In"/>
				<input name="_spring_security_remember_me" type="checkbox" checked="checked" style="visibility:hidden;">
				<div class="clearfix"></div>
			</div>
		</form>
	</div>
	</tiles:putAttribute>
</tiles:insertDefinition>