<%@ include file="/tagIncludes.jsp"%>
<tiles:insertDefinition name=".reportsPage">
  <tiles:putAttribute name="title" value="Uniware - Shipment Payment Reconciliation" />
  <tiles:putAttribute name="rightPane">
    <div id="pageBar">
    <div class="pageHeading">
      <span class="mainHeading">Shipments<span class="pipe">/</span></span><span class="subHeading"></span>
    </div>
    <div class="pageControls"></div>
    <div class="btn btn-success rfloat20" id="markReconciled" style="margin: 10px 5px;">Mark Reconciled</div>
    <div class="clear"></div>
    <div id="packageDiv" class="lb-over">
      <div class="lb-over-inner round_all">
        <div style="margin: 40px;line-height: 30px;">
          <div class="pageHeading lfloat">Package Info</div>
          <div id="packageDiv_close" class="link rfloat">close</div>
          <div class="clear"></div>
          <div id="packageTemplateDiv" style="margin:20px 0;">
          </div>
        </div>
      </div>
    </div>
    <div id="tableContainer">
      <table id="flexme1" style="display: block;"></table>
    </div>
  </tiles:putAttribute>
  <tiles:putAttribute name="deferredScript">
    <sec:authentication property="principal" var="user" />
    <script type="text/javascript" src="${path.js('jquery/jquery.dataTables.min.js')}"></script>
    <script type="text/javascript">
      Uniware.PackageReconciliationPage = function() {
        var self = this;
        this.name = 'DATATABLE COD RECONCILIATION';
        this.pageConfig = null;
        this.table = null;
        this.vendorCode = null;
        this.editRowData = null;
        this.datatableViews = ${user.getDatatableViewsJson("DATATABLE COD RECONCILIATION")};

        this.init = function() {
          self.pageConfig = new Uniware.PageConfig(this.name,'#flexme1',this.datatableViews[0],{
            showCheckbox : true,
            showSerialNumber: true
          });
          if (this.datatableViews.length) {
            this.headConfig = new Uniware.HeadConfig(this.datatableViews,function(datatableObject){
              self.pageConfig.setView(datatableObject);
            },this.pageConfig);
          }
          $("#markReconciled").click(self.markReconciled);
          $('#tableContainer').on('click', '.editPackage', self.editPackage);
        };

        this.editPackage = function(event) {
          var tr = $(event.target).parents('tr');
          var rowNumber = $(tr).attr('id').split('-')[1];
          var data = self.pageConfig.getRecord(rowNumber);
          self.editRowData = data;
          $("#packageTemplateDiv").html(template("packageTemplate",self.editRowData.values));
          Uniware.LightBox.show("#packageDiv");
          $("#updatePackage").click(self.updatePackage);
        };


        this.updatePackage = function() {
          $("#errorContact").addClass('invisible');
          var req = {
            shippingPackageCode : self.editRowData.values[0],
            collectedAmount : $('#collectedAmount').val()
          };

          Uniware.Ajax.postJson('/data/oms/shipment/collectedAmount/edit', JSON.stringify(req), function(response) {
            if (response.successful == false) {
              $("#errorContact").html(response.errors[0].description.split('|')[0]).removeClass('invisible');
            } else {
              Uniware.Utils.addNotification('collected amount has been updated');
              Uniware.LightBox.hide();
              self.pageConfig.reload();
            }
          });
        };

        this.markReconciled = function(){
          var selectedPackages = $('.dtCheckbox-checked');
          if (selectedPackages.length == 0) {
            alert('No Package selected');
            return;
          }
          var req = {
            shippingPackageCodes:[]
          };
          var selectedRows = self.pageConfig.getSelectedRecords();
          for(var i =0 ; i < selectedRows.length; i++) {
            req.shippingPackageCodes[req.shippingPackageCodes.length] =  selectedRows[i].values[0];
          }

          Uniware.Ajax.postJson("/data/oms/shipment/markReconciled", JSON.stringify(req), function(response){
            if (response.successful == false) {
              Uniware.Utils.showError(response.errors[0].description);
            } else {
              Uniware.Utils.addNotification('Reconcilition Successful');
              self.pageConfig.reload();
            }
          }, true);
        };
      };

      $(document).ready(function() {
        window.page = new Uniware.PackageReconciliationPage();
        window.page.init();
      });
    </script>
    <script type="text/html" id="packageTemplate">
      <div class="formLeft150 lfloat">Shipping Package Code</div>
      <div class="formRight lfloat"><#=obj[0]#></div>
      <div class="clear"></div>

      <div class="formLeft150 lfloat">Shipping Provider</div>
      <div class="formRight lfloat"><#=obj[3]#></div>
      <div class="clear"></div>

      <div class="formLeft150 lfloat">AWB</div>
      <div class="formRight lfloat"><#=obj[1]#></div>
      <div class="clear"></div>

      <div class="formLeft150 lfloat">Collectable Amount</div>
      <div class="formRight lfloat"><#=obj[4]#></div>
      <div class="clear"></div>

      <div class="formLeft150 lfloat">Collected Amount</div>
      <div class="formRight lfloat"><input type="text" id="collectedAmount" size="15" autocomplete="off" value="<#=obj.collectedAmount#>"  /></div>
      <div class="clear"></div>

      <br /> <br />
      <div class="formLeft150 lfloat">&#160;</div>
      <div class="formRight lfloat">
        <div id="updatePackage" class=" btn btn-small btn-primary lfloat">submit</div>
      </div>
      <div class="clear"/>
      <div id="errorContact" class="errorField lfloat20 invisible"></div>
      <div class="clear"></div>
    </script>
  </tiles:putAttribute>
</tiles:insertDefinition>

