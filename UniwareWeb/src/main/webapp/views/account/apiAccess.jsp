<%@ include file="/tagIncludes.jsp"%>

<tiles:insertDefinition name=".loginPage">
    <tiles:putAttribute name="title" value="Uniware API Access" />
    <tiles:putAttribute name="body">
        <div class="content-wrapper">
            <div class="permission-wrapper round_all_b" style="padding: 35px; max-height: 500px">
                <div style="background-color: #666666; top: 140px; height: 71px;">
                    <a style="float: right" href="/" id="logo-link"></a>
                </div>
                <br />
                <div style="background-color: #999; height: 2px"></div>
                <br />
                <div style="background-color: white; padding: 15px 35px; line-height: 1.2">
                    <div class="Txt_Allow">
                        <p class="lastNode" style="color: #666; font-size: 22pt;">Ready to use Vendor Product?</p>
                    </div>
                    <br />
                    <div style="color: #666; font-size: 13pt;">
                        <p class="lastNode">Great! We need your consent to share your ${tenant} data. Don't worry, we won't share your password. You can change this any time by editing your account preferences.</p>

                        <p>Please take the time to read Vendor Product's terms of service and privacy policy, because these policies will apply when you use this service. Remember, we don't manage policies set by
                            other companies. By clicking on the "I Agree" button, you're allowing us to link your ${tenant} account with Vendor Product.</p>
                    </div>
                </div>
                <br />
                <div id="actionRow" class="formRight lfloat">
                    <input type="submit" value="I Agree" class="btn btn-primary lfloat" style="text-transform: capitalize;" id="grantAccess">
                    <div style="margin-top: 7px" id="cancel" class="lfloat10 link">cancel</div>
                </div>
                <div class="clear"></div>

                <br />
                <div style="background-color: #999; height: 1.5px"></div>
                <br />
                <div id="body-footer" style="float: right">� 2013 Unicommerce eSolutions Pvt. Ltd. India</div>
            </div>
        </div>
        <script>
            Uniware.LoginPage = true;
        </script>
    </tiles:putAttribute>
    <tiles:putAttribute name="deferredScript">
        <script type="text/javascript">
            Uniware.ApiAccess = function(){
                var self = this;
                this.channel = "${param['channel']}";
                this.redirectUrl = "${param['redirectUrl']}";
                this.ruParams = "${param['ruparams']}";
                this.init = function() {
                    $('#grantAccess').click(self.grantAccess);
                    $('#cancel').click(self.cancel);
                };
                this.cancel = function(){
                    window.location = self.redirectUrl;
                };
                
                this.grantAccess = function(){
                    var req = {
                        channel: self.channel,
                        ruParams: self.ruParams,
                        redirectUrl: self.redirectUrl, 
                    }
                    Uniware.Ajax.postJson("/data/api/access/grantAccess", JSON.stringify(req), function(response) {
                        if(response.successful == true){
                            window.location = response.redirectUrl;
                        }else{
                            Uniware.Utils.showError(response.errors[0].description);
                        }
                    }, true);
                };
            };
                
            $(document).ready(function() {
                window.page = new Uniware.ApiAccess();
                window.page.init();
            });
        </script>
    </tiles:putAttribute>
</tiles:insertDefinition>
