<%@ include file="/tagIncludes.jsp"%>
<tiles:insertDefinition name=".wapPage">
	<tiles:putAttribute name="title" value="Uniware - Pick Items" />
	<tiles:putAttribute name="body">
		<br />
		<div class="greybor headlable ovrhid main-box-head">
			<h2 class="edithead head-textfields">Picking > Scan Picklist</h2>
		</div>
		<div class="greybor round_bottom main-boform-cont ovrhid" style="padding: 10px;">
			<div id="picklistDiv"></div>
			<div id="message" class="hidden errorField"></div>
		</div>
	</tiles:putAttribute>
	<tiles:putAttribute name="deferredScript">
	<script id="picklistTemplate" type="text/html">
	<table>
		<tr>
			<td width="100">Picklist Code: </td>
			<td><#= obj.picklistCode ? obj.picklistCode : '<input type="text" id="picklistCode" class="w150 ucase"/>' #></td>
		</tr>
		<# if (obj.picklistCode) { #>
		<tr>
			<td>Shelf : </td>
			<td><#=obj.shippingPackageItem.shelfCode#></td>
		</tr>
		<tr>
			<td>Product Code : </td>
			<td><#=obj.shippingPackageItem.itemSku#></td>
		</tr>
		<tr>
			<td>Item Name : </td>
			<td><#=obj.shippingPackageItem.itemName#></td>
		</tr>
		<tr>
			<td>Color : </td>
			<td><#=obj.shippingPackageItem.color#></td>
		</tr>
		<tr>
			<td>Size : </td>
			<td><#=obj.shippingPackageItem.size#></td>
		</tr>
		<tr>
			<td>Brand : </td>
			<td><#=obj.shippingPackageItem.brand#></td>
		</tr>
		<# for(var key in obj.shippingPackageItem.itemTypeCustomFields) {#>
			<tr>
				<td><#=key#></td>
				<td><#=obj.shippingPackageItem.itemTypeCustomFields[key]#></td>
			</tr>		
		<# } #>

		<tr>
			<td>Item : </td>
			<td><input type="text" id="itemCode" class="w150"/></td>
		</tr>
		<tr>
			<td></td>
			<td><div id="pickItem" class=" btn lfloat" >Pick Item</div></td>
		</tr>
		<# } #>
	</table>
	</script>
		<script type="text/javascript">
		Uniware.WapPicking = function() {
			var self = this;
			this.packlistItem = {};
			this.itemCodes = {};
			
			this.init = function() {
				self.render();
				$('#picklistCode').focus().keyup(self.load);
			};
			
			this.load = function(event) {
				if (event.which == 13 && $('#picklistCode').val() != '') {
					self.scanPicklist($('#picklistCode').val());
				}
			};
			
			this.scanPicklist = function(picklistCode) {
				Uniware.Ajax.postJson("/data/oms/packer/packlist/fetch", JSON.stringify({'picklistCode' : picklistCode}), function(response){
					if(response.successful == true) {
						self.packlist = response.packlist;
						self.selectItemToPick();						
					} else {
						$('#message').html(response.errors[0].description).removeClass('hidden');
						$('#inflowReceiptCode').val('');
					}					
				});
			};
			
			this.selectItemToPick = function(){
				self.packlistItem.picklistCode = self.packlist.code;
				for(var i=0; i<self.packlist.packlistItems.length; i++){
					for(var j=0; j<self.packlist.packlistItems[i].shippingPackage.shippingPackageItems.length; j++){
						var shippingPackageItem = self.packlist.packlistItems[i].shippingPackage.shippingPackageItems[j];
						if(self.packlistItem.shippingPackageItem == null && shippingPackageItem.itemCode == null){
							self.packlistItem.shippingPackage = self.packlist.packlistItems[i].shippingPackage;
							self.packlistItem.shippingPackageItem = shippingPackageItem;
						}else if(shippingPackageItem.itemCode != null){
							self.itemCodes[shippingPackageItem.itemCode] = shippingPackageItem.itemCode; 
						}
					}
				}
				
				if(self.packlistItem.shippingPackage == null){
					$('#picklistDiv').html('');
					$('#message').html("No Item To Pick").removeClass('hidden');
					$('#inflowReceiptCode').val('');
				}else{
					self.render();
					$('#pickItem').click(self.pickItem);	
				}
			};
			
			this.pickItem = function(event) {
				$('#message').addClass('hidden');
				var itemCode = $('#itemCode').val();
				var itemSKU = $('#itemSKU').val();
				
				if(self.itemCodes[itemCode] != null){
					$('#message').html("This item belongs to your picklist. Please pick it.").removeClass('hidden');
					$('#itemCode').val('');
				}else{
					var req = {
						'shippingPackageCode': self.packlistItem.shippingPackage.code,
						'itemCode': itemCode
					}
					Uniware.Ajax.postJson('/data/oms/packer/allocate', JSON.stringify(req), function(response){
						if (response.successful) {
							$('#message').html("Item has been allocated successfully. Pick it.").removeClass('hidden');
							self.packlistItem.shippingPackageItem.itemCode = itemCode;
							self.packlistItem = {};
							self.selectItemToPick();
						} else {
							$('#message').html(response.errors[0].description).removeClass('hidden');
							$('#itemCode').val('');
						}
						$('#itemCode').focus();
					});
				}
			};
			
			this.render = function() {
				$('#picklistDiv').html(template('picklistTemplate', self.packlistItem));
				$('#itemCode').focus();
			};
			
		}
		
		$(document).ready(function() {
			window.page = new Uniware.WapPicking();
			window.page.init();
		});
	</script>
	</tiles:putAttribute>
</tiles:insertDefinition>
