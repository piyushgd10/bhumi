!(function($) {
    $.addFlex = function(t, p) {
        if (t.grid) return false;
        p = $.extend({
        	requestObject : {},
			height: 200,
			width: 'auto',
			novstripe: false,
			striped: true,
			minwidth: 30,
			minheight: 80,
			resizable: true,
			url: '',
			method: 'POST',
			dataType: 'json',
			errormsg: 'Connection Error',
			usepager: true,
			nowrap: true,
			page: 1,
			total: 1,
			useRp: true,
			rp: 25,
			fetchRP : 25,
			rpOptions: [25,50,100],
			title: false,
			pagestat: 'Displaying {from} to {to} of {total} items',
			pagetext: 'Page',
			outof: 'of',
			findtext: 'Find',
			procmsg: 'Processing, please wait ...',
			query: '',
			qtype: '',
			nomsg: 'No items',
			minColToggle: 1,
			showToggleBtn: true,
			hideOnSubmit: true,
			autoload: true,
			blockOpacity: 0.5,
			preProcess: false,
			onDragCol: false,
			onToggleCol: false,
			onChangeSort: false,
			onSuccess: false,
			onError: false,
			onSubmit: false,
			dataSourceOptions: null,
			sortorder : true,
			activeFilters : [],
			clickedAway : false,
	        isVisible : true,
	        resizeTimer : null,
	        showCheckbox: false,
	        showSerialNumber: false,
	        selectionGroup: false,
	        dropdownToggle: false,
			}, p);
        $(t).show().attr({cellPadding: 0,cellSpacing: 0,border: 0}).removeAttr('width');
        var g = {
        		dataObj : null,
    			createDataSet : function(){
    				this.dataObj = new Uniware.DataMap({
						rowsPerIndex: p.rp,
						requestMultiplier: 4
				   	});
    				this.dataSourceObj = new Uniware.DataResource({
    					url: p.dataSourceOptions.url,
    					options : p.dataSourceOptions.requestObject
    				});
    			},
        		hset: {},
        		renderFilters: function(){
        			$('span.filterButton',g.fDiv).each(function(){
    	        	    var filterKey = $(this).attr('id');
    	        	    var filterObj = p.filterObject[filterKey];
    	        	    $(this).popover({
    	        	    	  animation : false,
    	        			  placement: 'bottom',
    	         			  html: true,
    	         			  content : g.getDefaultHTML(filterKey),
    	         			  title : filterObj.name,
    	         			  template: '<div class="popover popoverMenu"><div class="arrow"></div><div class="popover-inner"><h3 class="popover-title"></h3><div class="popover-content"><p></p></div></div></div>'
    	        	    }).click(function(e){
    	        	    	p.clickedAway = false;
    	        	        p.isVisible = true;
    	        	        e.preventDefault();
    	        	        $('.popover').bind('click',function() {
    	                        p.clickedAway = false
    	                    });
    	        	        
    	        	        if($(this).hasClass('activeFilter') && !$(this).hasClass('btn-activeFilter')){
    	        	        	$(this).removeClass('activeFilter');
    	        	        }else{
    	        	        	$(this).addClass('activeFilter');
    	        	        }
    	        	        $(this).parent('div').siblings().children('span.filterButton').each(function(){
    	        	        	$(this).popover('hide');
    	        	        	if(!$(this).hasClass('btn-activeFilter')){
    	        	        		$(this).removeClass('activeFilter');
    	        	        	}
    	        	        });
    	        	        if($('.popoverMenu').length){
    	        	        var popOverRightOffset = ($(window).width() - ($('.popoverMenu').offset().left +$('.popoverMenu').outerWidth()));
	    	        	        if(popOverRightOffset<0){
	    	        	        	var left = $('.popoverMenu').outerWidth() - 22;
	    	        	        	$('.popoverMenu').css('left','-'+left+'px');
	    	        	        	$('.popoverMenu').addClass('pullRight');
	    	        	        }else{
	    	        	        	$('.popoverMenu').removeClass('pullRight');
	    	        	        }
    	        	        }
    	                    if(filterObj.type=="DATERANGE"){
    	        				$('#'+filterObj.attachToColumn).daterangepicker();
    	        				$('.daterangepicker').bind('click',function() {
        	                        p.clickedAway = false
        	                    });
    	        			}
    	        	    	if(filterObj.active){
    	        	    		g.updateFilterHTML(filterKey);
    	        	    	}
  	        	    		g.activateFilterButtons(filterKey);
    	        	    });
        	        });
        		},
        		activateFilterButtons : function(filterKey){
        			var filterObj = p.filterObject[filterKey];
        			$('#'+filterObj.attachToColumn).keydown(function(e) {
                        if (e.keyCode == 13) {
                            $('button[buttontype="apply"]').trigger('click');
                        }
                    });
        			$('button','.popover').bind('click',function(e){
        				e.preventDefault();
        				var buttonType= $(this).attr('buttonType');
        				for(var i=0;i<p.activeFilters.length;i++){
        					var filter = p.activeFilters[i];
        					if(filter.id == filterObj.id){
        						p.activeFilters.splice(i, 1);
        						i--;
        					}
        				}
        				if(buttonType=="apply"){
        					var value = $('#'+filterObj.attachToColumn).val();
        					filterObj.active = true;
            				filterObj.value = value;
            				var obj = {};
            				obj.id = filterObj.id;
            				var filterValue;
            				switch(filterObj.type){
            				case "TEXT":
            					obj.text = value;
            					filterValue = value;
            					break;
            				case "MULTISELECT":
            					obj.selectedValues = value;
            	                var valueToNames = {};
            	                for (i = 0; i < filterObj.selectItems.length; i++) {
            	                  valueToNames[filterObj.selectItems[i].value] = filterObj.selectItems[i].name;
            	                }
            	                filterValue = "";
            	                for (i = 0; i < value.length; i++) {
            	                  filterValue += valueToNames[value[i]] + ',';
            	                }
            	                filterValue = filterValue.substring(0, filterValue.length - 1);
            	                break;
            				case "BOOLEAN":
            					obj.checked = $('#'+filterObj.attachToColumn).prop('checked');
            					filterObj.value = obj.checked;
            					filterValue = obj.checked ? "Yes" : "No";
            					break;
            				case "DATERANGE":
            					obj.dateRange = {};
            					obj.dateRange.start = Date.fromPaddedDate(value.substring(0, 10)).getTime();
            					var endDate = Date.fromPaddedDate(value.substring(13));
            					endDate.setDate(endDate.getDate()+1);
            					obj.dateRange.end = endDate.getTime();
            					filterValue = value;
            					break;
            				};
            				p.activeFilters.push(obj);
                			$('th[abbr='+ filterObj.attachToColumn +'] span.filterValues').html(filterValue).addClass("filterApplied");
            				$('#'+filterKey).addClass('btn-activeFilter');
        				}else if(buttonType=="clear"){
        					p.filterObject[filterKey].active= false;
            				delete(p.filterObject[filterKey].value);
            				$('th[abbr='+ p.filterObject[filterKey].attachToColumn +'] span.filterValues').html("").removeClass("filterApplied");
            				$('#'+filterKey).removeClass('btn-activeFilter').removeClass('activeFilter'); 
        				}
        				$('#'+filterKey).popover('hide');
        			    p.newp = 1;//change page number to 1
                        g.dataObj = null;//refill the dataObj
                        g.populate();
        			});
        		},
        		updateFilterHTML: function(filterKey){
        			var filterObj = p.filterObject[filterKey];
        			if(filterObj.type=="TEXT"){
        				$('#'+filterObj.attachToColumn).val(filterObj.value).focus();
        			}
        			if (filterObj.type=="DATERANGE") {
        				if (filterObj.value && filterObj.value.start) {
        					var str = new Date(filterObj.value.start).toPaddedDate();
        					str  += ' - ' + new Date(filterObj.value.end).toPaddedDate();
           					$('#'+filterObj.attachToColumn).val(str).focus();
        				} else {
        					$('#'+filterObj.attachToColumn).val(filterObj.value).focus();
        				}
        			}
        			if(filterObj.type=="BOOLEAN"){
        				$('#'+filterObj.attachToColumn).prop('checked',filterObj.value);
        			}
        			if(filterObj.type=="MULTISELECT"){
        				for(var i=0;i<filterObj.value.length;i++){
        					$('#'+filterObj.attachToColumn+' option[value="'+filterObj.value[i]+'"]').attr('selected','selected');
        				}
        			}
        			$('.popover').find('button[buttonType="clear"]').show();
        		},
        		getDefaultHTML: function(filterKey){
        			var filterObj = p.filterObject[filterKey];
        			var html = '';
	        	    switch(filterObj.type){
	        	    case "TEXT":
	        	    	html = html+'<input id="'+filterObj.attachToColumn+'" type="text"/>';
	        	    	break;
	        	    case "MULTISELECT":
	        	    	html = html+'<select id="'+filterObj.attachToColumn+'" multiple="multiple">';
	        	    	var dropDownArray = filterObj.selectItems;
	        	    	for(var i=0;i<dropDownArray.length;i++){
	        	    		html = html+'<option value="'+dropDownArray[i].value+'">'+dropDownArray[i].name+'</option>';
	        	    	}
	        	    	html = html+'</select>';
	        	    	break;
	        	    case "DATERANGE":
	        	    	html = html+'<input id="'+filterObj.attachToColumn+'" type="text"/>';
	        	    	break;
	        	    case "BOOLEAN":
	        	    	html = html + '<label class="checkbox"><input id="'+filterObj.attachToColumn+'" type="checkbox"> '+filterObj.name+'</label>';
	        	    	break;
	        	    }
	        	    return html+'<div style="margin-top:10px;"><button buttonType="apply" class="btn btn-mini btn-success">Apply</button class="btn">&nbsp;&nbsp;<button style="display:none;" buttonType="clear" class="btn btn-mini">Clear</button></div>';
        		},
        		rePosDrag: function() {
                var cdleft = 0 - this.hDiv.scrollLeft;
                if (this.hDiv.scrollLeft > 0)
                    cdleft -= Math.floor(p.cgwidth / 2);
                $(g.cDrag).css({top: g.hDiv.offsetTop + 1});
                $(g.fDiv).css({top: g.hDiv.offsetTop + 1});
                var cdpad = this.cdpad;
                $('div', g.cDrag).hide();
                $('div.fCol', g.fDiv).hide();
                $('thead tr:first th:visible', this.hDiv).each(function() {
                    var n = $('thead tr:first th:visible', g.hDiv).index(this);
                    var cdpos = parseInt($('div', this).width());
                    if (cdleft == 0)
                        cdleft -= Math.floor(p.cgwidth / 2);
                    cdpos = cdpos + cdleft + cdpad;
                    if (isNaN(cdpos)) {
                        cdpos = 0
                    }
                    if(n==0){
                    	cdpos++;
                    }
                    $('div:eq(' + n + ')', g.cDrag).css({'left': cdpos + 'px'}).show();
                    $('div.fCol:eq(' + n + ')', g.fDiv).css({'left': (cdpos-16) + 'px'}).show();
                    cdleft = cdpos
                });
                $('.fakeScroll',g.bDiv).css({width:$('.hDivBox').css('width'),height:'1px'});
            },
            fixHeight: function(newH) {
                newH = false;
                if (!newH)
                    newH = $(g.bDiv)[0].clientHeight;
                var hdHeight = $(this.hDiv).height();
                $('div', this.cDrag).each(function() {
                    $(this).height(hdHeight)
                });
                $(g.nDiv).height('auto').width('auto');
                $(g.block).css({height: newH,marginBottom: (newH * -1)});
            },dragStart: function(dragtype, e, obj) {
                if (dragtype == 'colresize') {
                	var height = $(g.bDiv).height()+$(g.hDiv).height();
                	$(g.nDiv).hide();
                    var n = $('div', this.cDrag).index(obj);
                    var ow = $('th:visible div:eq(' + n + ')', this.hDiv).width();
                    $(obj).addClass('dragging').css({'height':height}).siblings().hide();
                    $(obj).prev().addClass('dragging').css({'height':height}).show();
                    this.colresize = {startX: e.pageX,ol: parseInt(obj.style.left),ow: ow,n: n};
                    $('body').css('cursor', 'col-resize')
                } else if (dragtype == 'colMove') {
                    $(g.nDiv).hide();
                    this.hset = $(this.hDiv).offset();
                    this.hset.right = this.hset.left + $('table', this.hDiv).width();
                    this.hset.bottom = this.hset.top + $('table', this.hDiv).height();
                    this.dcol = obj;
                    this.dcoln = $('th', this.hDiv).index(obj);
                    this.colCopy = document.createElement("div");
                    this.colCopy.className = "colCopy";
                    this.colCopy.innerHTML = obj.innerHTML;
                    //$('div.fCol:eq('+this.dcoln+')',g.fDiv).find('a').css('visibility','hidden');
                    $(this.colCopy).prepend('<span class="indicator icon-ban-circle"></span>');
                    if ($.browser.msie) {
                        this.colCopy.className = "colCopy ie"
                    }
                    $(this.colCopy).css({position: 'absolute',float: 'left',display: 'none',textAlign: obj.align});
                    $('body').append(this.colCopy);
                    $(this.colCopy).find('i').remove();
                    $(this.cDrag).hide();
                }
                $('body').noSelect()
            },dragMove: function(e) {
                if (this.colresize) {
                    var n = this.colresize.n;
                    var diff = e.pageX - this.colresize.startX;
                    var nleft = this.colresize.ol + diff;
                    var nw = this.colresize.ow + diff;
                    if (nw > p.minwidth) {
                        $('div:eq(' + n + ')', this.cDrag).css('left', nleft);
                        this.colresize.nw = nw
                    }
                } else if (this.colCopy) {
                    $(this.dcol).addClass('thMove').removeClass('thOver');
                    if (e.pageX > this.hset.right || e.pageX < this.hset.left || e.pageY > this.hset.bottom || e.pageY < this.hset.top) {
                        $('body').css('cursor', 'move')
                    } else {
                        $('body').css('cursor', 'pointer')
                    }
                    $(this.colCopy).css({top: e.pageY + 5,left: e.pageX + 5,display: 'block'})
                }
            },dragEnd: function() {
                if (this.colresize) {
                    var n = this.colresize.n;
                    var nw = this.colresize.nw;
                    $('th:visible div:eq(' + n + ')', this.hDiv).css('width', nw);
                    $('tr', this.bDiv).each(function() {
                        $('td:visible div:eq(' + n + ')', this).css('width', nw)
                    });
                    this.hDiv.scrollLeft = this.bDiv.scrollLeft;
                    $('div:eq(' + n + ')', this.cDrag).siblings().show();
                    $('.dragging', this.cDrag).removeClass('dragging');
                    this.rePosDrag();
                    this.fixHeight();
                    this.colresize = false
                } else if (this.colCopy) {
                    $(this.colCopy).remove();
                    if (this.dcolt != null) {
                        if (this.dcoln > this.dcolt)
                            $('th:eq(' + this.dcolt + ')', this.hDiv).before(this.dcol);
                        else
                            $('th:eq(' + this.dcolt + ')', this.hDiv).after(this.dcol);
                        
                        this.switchCol(this.dcoln, this.dcolt);
                        $(this.cdropleft).remove();
                        this.rePosDrag();
                        if (p.onDragCol) {
                            p.onDragCol(this.dcoln, this.dcolt)
                        }
                    }
                    this.dcol = null;
                    this.hset = null;
                    this.dcoln = null;
                    this.dcolt = null;
                    this.colCopy = null;
                    $('.thMove', this.hDiv).removeClass('thMove');
                    $(this.cDrag).show()
                }
                $('body').css('cursor', 'default');
                $('body').noSelect(false)
            },toggleCol: function(cid, visible) {
                var ncol = $("th[axis='col" + cid + "']", this.hDiv)[0];
                var n = $('thead th', g.hDiv).index(ncol);
                var cb = $('input[value=' + cid + ']', g.nDiv)[0];
                if (visible == null) {
                    visible = ncol.hidden
                }
                if ($('input:checked', g.nDiv).length < p.minColToggle && !visible) {
                    return false
                }
                if (visible) {
                    ncol.hidden = false;
                    $(ncol).show();
                    cb.checked = true
                } else {
                    ncol.hidden = true;
                    $(ncol).hide();
                    cb.checked = false
                }
                $('tbody tr', t).each(function() {
                    if (visible) {
                        $('td:eq(' + n + ')', this).show()
                    } else {
                        $('td:eq(' + n + ')', this).hide()
                    }
                });
                this.rePosDrag();
                if (p.onToggleCol) {
                    p.onToggleCol(cid, visible)
                }
                return visible
            },switchCol: function(cdrag, cdrop) {
                $('tbody tr', t).each(function() {
                    if (cdrag > cdrop)
                        $('td:eq(' + cdrop + ')', this).before($('td:eq(' + cdrag + ')', this));
                    else
                        $('td:eq(' + cdrop + ')', this).after($('td:eq(' + cdrag + ')', this))
                });
                if (cdrag > cdrop) {
                    $('tr:eq(' + cdrop + ')', this.nDiv).before($('tr:eq(' + cdrag + ')', this.nDiv));
                    $('div.fCol:eq(' + cdrop + ')', this.fDiv).before($('div.fCol:eq(' + cdrag + ')', this.fDiv));
                } else {
                    $('tr:eq(' + cdrop + ')', this.nDiv).after($('tr:eq(' + cdrag + ')', this.nDiv));
                    //var ele = 
                    $('div.fCol:eq(' + cdrop + ')', this.fDiv).after($('div.fCol:eq(' + cdrag + ')', this.fDiv));
                }
                if ($.browser.msie && $.browser.version < 7.0) {
                    $('tr:eq(' + cdrop + ') input', this.nDiv)[0].checked = true
                }
                this.hDiv.scrollLeft = this.bDiv.scrollLeft
            },scroll: function() {
                this.hDiv.scrollLeft = this.bDiv.scrollLeft;
                this.rePosDrag();
            },addData: function(data) {
            	$(g.bDiv).scrollTop(0);
                if (p.dataType == 'json') {
                    data = $.extend({rows: [],page: 0,total: 0}, data)
                }
                if (p.preProcess) {
                    data = p.preProcess(data)
                }
                $('.pReload', this.pDiv).removeClass('loading');
                this.loading = false;
                if (!data) {
                    $('.pPageStat', this.pDiv).html(p.errormsg);
                    return false
                }
                if (p.dataType == 'xml') {
                    p.total = +$('rows total', data).text()
                } else {
                    p.total = data.total
                }
                if (p.total == 0) {
                    $('tr, a, td, div', t).unbind();
                    $(t).empty().html('<div style="padding-top:30px; width:'+$(g.hTable).width()+'px"><em style="padding-left:30px; font-size:21px">No data available</em></div>');;
                    p.pages = 1;
                    p.page = 1;
                    this.buildpager();
                    $('.pPageStat', this.pDiv).html(p.nomsg);
                    return false
                }
                p.pages = Math.ceil(p.total / p.rp);
                if (p.dataType == 'xml') {
                    p.page = +$('rows page', data).text()
                } else {
                    p.page = data.page
                }
                this.buildpager();
                var tbody = document.createElement('tbody');
                if (p.dataType == 'json') {
                    $.each(data.rows, function(i, row) {
                        var tr = document.createElement('tr');
                        $(tr).attr('id','dataRow-'+(i + p.rp*(p.page-1) + 1));
                        if (i % 2 && p.striped) tr.className = 'erow';
                        var rowObj = {};
                        $('thead tr:first th', g.hDiv).each(function() {
                        	var idx = $(this).attr('axis').substr(3);
                        	var colConfig = p.colModel[idx];
                        	rowObj[colConfig.id] = row.values[idx];
                        });
                        for(var j=0;p.hiddenColumns && j<p.hiddenColumns.length;j++){
                        	rowObj[p.hiddenColumns[j].id] = row.values[j + p.colModel.length];
                    	}
                        $(tr).attr('status',rowObj['selectionGroup']);
                        $('thead tr:first th', g.hDiv).each(function(index) {
                            var td = document.createElement('td');
                            var idx = $(this).attr('axis').substr(3);
                        	var colConfig = p.colModel[idx];
                            td.align = colConfig.align;
                            var innnerValue;
                            if(colConfig.type == 'date' && row.values[idx] != null){
                        		var date = new Date(row.values[idx]);
                        		row.values[idx] = date.toDateTime();
                        	}
                            if (colConfig.clientTemplate != null) {
                            	innerValue = template(colConfig.clientTemplate.replace(/href="([^"]*)"/g, function($0, $1){return 'href="' + ($1.indexOf('legacy') != -1 ? $1 :($1.indexOf('?') != -1 ? ($1 + '&legacy=1'):($1 + '?legacy=1'))) + '"'}) ,rowObj);
                            } else {
                            	innerValue = row.values[idx];
                            }
                            
                            td.innerHTML = innerValue;
                            $(td).attr('abbr', colConfig.id);
                            $(tr).append(td);
                            td = null
                        });
                        if ($('thead', this.gDiv).length < 1) {
                            for (idx = 0; idx < cell.length; idx++) {
                                var td = document.createElement('td');
                                if (typeof row.cell[idx] != "undefined") {
                                    td.innerHTML = (row.cell[idx] != null) ? row.cell[idx] : ''
                                } else {
                                    td.innerHTML = row.cell[p.colModel[idx].name]
                                }
                                $(tr).append(td);
                                td = null
                            }
                        }
                        $(tbody).append(tr);
                        tr = null
                    })
                }
                $('tr', t).unbind();
                $(t).empty();
                $(t).append(tbody);
                this.addCellProp();
                this.addRowProp();
                this.rePosDrag();
                tbody = null;
                data = null;
                i = null;
                if (p.onSuccess) {
                    p.onSuccess(this)
                }
                if (p.hideOnSubmit) {
                    $(g.block).remove()
                }
                this.hDiv.scrollLeft = this.bDiv.scrollLeft;
                if ($.browser.opera) {
                    $(t).css('visibility', 'visible')
                }
                
                if(p.selectionGroup){
                	var rowStatus =[];
                	var html='';
                	$('tr[status]').each(function(){
                		rowStatus.push($(this).attr('status'));
                	});
                	var rowStatusData = $.unique(rowStatus);
                	html+='<li style="border-top:0px;" id="flexigridSelectAll">All</li>';
                	html+='<li id="flexigridSelectNone">None</li>';
                	for (i=0; i<rowStatusData.length; i++){
                		html+='<li class="showGroupOption">'+rowStatusData[i]+'</li>';
                	}
                	$('.flexigridShowGroupOptions').html(html);
                }
                
                $('#flexigridSelectAll').click(function(){
                	$('.inputSelect-head').addClass('dtCheckboxHead-checked');
                	$('span.inputSelect').addClass('dtCheckbox-checked');
                });
                
                $('#flexigridSelectNone').click(function(){
                	$('.inputSelect-head').removeClass('dtCheckboxHead-checked');
                	$('span.inputSelect').removeClass('dtCheckbox-checked');
                });
                
                $('.showGroupOption').click(function(){
                	$('tr[status='+$(this).html()+']').children().children('span.inputSelect').addClass('dtCheckbox-checked');
                });
                
            },changeSort: function(th) {
                if (this.loading) {
                    return true
                }
                $(g.nDiv).hide();
                if (p.sortname == $(th).attr('abbr')) {
                    if (p.sortorder == true) {
                        p.sortorder = false
                    } else {
                        p.sortorder = true
                    }
                }else{
                	p.sortorder = true
                }
                p.newp = 1;//change page number to 1
                g.dataObj = null;//refill the dataObj
                $(th).addClass('sorted').siblings().removeClass('sorted');
                $('.desctrue', this.hDiv).removeClass('desctrue');
                $('.descfalse', this.hDiv).removeClass('descfalse');
                $('div', th).addClass('desc' + p.sortorder);
                p.sortname = $(th).attr('abbr');
                if (p.onChangeSort) {
                    p.onChangeSort(p.sortname, p.sortorder)
                } else {
                    this.populate()
                }
            },buildpager: function() {
                $('.pcontrol input', this.pDiv).val(p.page);
                $('.pcontrol span', this.pDiv).html(p.pages);
                var r1 = (p.page - 1) * p.rp + 1;
                var r2 = r1 + p.rp - 1;
                if (p.total < r2) {
                    r2 = p.total
                }
                var stat = p.pagestat;
                stat = stat.replace(/{from}/, r1);
                stat = stat.replace(/{to}/, r2);
                stat = stat.replace(/{total}/, p.total);
                $('.pPageStat', this.pDiv).html(stat)
            },populate: function() {
                if (this.loading) {
                    return true
                }
                if (p.onSubmit) {
                    var gh = p.onSubmit();
                    if (!gh) {
                        return false
                    }
                }
                this.loading = true;
                $('.pPageStat', this.pDiv).html(p.procmsg);
                $('.pReload', this.pDiv).addClass('loading');
                $(g.block).css({top: g.bDiv.offsetTop});
                $('.dtCheckboxHead').removeClass('dtCheckboxHead-checked');
                if (p.hideOnSubmit) {
                    $(this.gDiv).prepend(g.block)
                }
                if ($.browser.opera) {
                    $(t).css('visibility', 'hidden')
                }
                if (!p.newp) {
                    p.newp = 1
                }
                if (p.page > p.pages) {
                    p.page = p.pages
                }
                if(this.dataObj == null){
					this.createDataSet();
				}
                if (this.dataObj.hasDataForPage(p.newp,p.rp)) {
                	var rows = this.dataObj.getRecords(p.newp,p.rp);
        			var data = {
    					rows  : rows,
    					total : p.total,
    					page : p.newp
        			};
        			g.addData(data);
                } else {
                	var searchOptions = g.getSearchOptions();
                	this.dataSourceObj.getData(searchOptions,function(response) {
                    	g.dataObj.pushDataToMap(response.rows,p.newp,p.rp,response.resultCount);
            			var data = {
            					rows  : g.dataObj.getRecords(p.newp,p.rp),
            					total : response.resultCount,
            					page : p.newp
            			};
            			g.addData(data);
                    });
                }
            },
            updateFilters : function(){
            	for(var i=0;i<p.activeFilters.length;i++){
            		var filter = p.activeFilters[i];
            		var filterObj = p.filterObject[filter.id];
            		$('#'+filter.id,g.fDiv).addClass('').addClass('activeFilter');
            		var filterValue;
            		switch(filterObj.type){
    				case "TEXT":
    					filterValue = filter.text;
    					break;
    				case "MULTISELECT":
    					filterValue = "";
    					for (var i=0;i<filter.selectedValues.length;i++) {
    						filterValue += filter.selectedValues[i] + ','; 
    					}
    					filterValue = filterValue.substring(0, filterValue.length - 1);
    					break;
    				case "BOOLEAN":
    					filterValue = filter.checked ? "Yes" : "No";
    					break;
    				case "DATERANGE":
    					filterValue = filter.dateRange.start.toPaddedDate() + "-" + filter.dateRange.end.toPaddedDate();
    					break;
    				};
            		
        			$('th[abbr='+ filterObj.attachToColumn +'] span.filterValues').html(filterValue).attr('title',filterValue).addClass('filterApplied');
    				$('#'+filterObj.id).addClass('btn-activeFilter');
            		filterObj.active = true; 
            		for(var key in filter){
            			if(key != 'id'){
            				filterObj.value = filter[key];
            			}
            		}	
            	}
            },
            getSearchOptions: function(){
            	var columnsId = [];
            	for(var i=0;i<p.colModel.length;i++){
            		columnsId.push(p.colModel[i].id);
            	}
            	for (var j=0;p.hiddenColumns && j< p.hiddenColumns.length;j++) {
            		columnsId.push(p.hiddenColumns[j].id);
            	}
            	var searchParams = {};
            	var pagingParams = {
            		 "noOfResults": p.fetchRP*4,
            		 "start": (p.newp-1)*p.rp,
            		 "columns": columnsId,
            		 "fetchResultCount": true
            	};
            	$.extend(searchParams,pagingParams);
            	if(p.activeFilters.length>0){
            		var filterParams = {
                    		"filters": p.activeFilters	
                    };
            		$.extend(searchParams,filterParams);
            	}
            	if(typeof p.sortname != 'undefined'){
            		var sortingParams = {
            				"sortColumns": [{"column":p.sortname,"descending":p.sortorder}]
            		};
            		$.extend(searchParams,sortingParams);
            	}
            	return searchParams;
            },
            doSearch: function() {
                p.query = $('input[name=q]', g.sDiv).val();
                p.qtype = $('select[name=qtype]', g.sDiv).val();
                p.newp = 1;
                this.populate()
            },changePage: function(ctype) {
                if (this.loading) {
                    return true
                }
                switch (ctype) {
                    case 'first':
                        p.newp = 1;
                        break;
                    case 'prev':
                        if (p.page > 1) {
                            p.newp = parseInt(p.page) - 1
                        }
                        break;
                    case 'next':
                        if (p.page < p.pages) {
                            p.newp = parseInt(p.page) + 1
                        }
                        break;
                    case 'last':
                        p.newp = p.pages;
                        break;
                    case 'input':
                        var nv = parseInt($('.pcontrol input', this.pDiv).val());
                        if (isNaN(nv)) {
                            nv = 1
                        }
                        if (nv < 1) {
                            nv = 1
                        } else if (nv > p.pages) {
                            nv = p.pages
                        }
                        $('.pcontrol input', this.pDiv).val(nv);
                        p.newp = nv;
                        break
                }
                if (p.newp == p.page) {
                    return false
                }
                if (p.onChangePage) {
                    p.onChangePage(p.newp)
                } else {
                    this.populate()
                }
            },addCellProp: function() {
                $('tbody tr td', g.bDiv).each(function() {
                    var tdDiv = document.createElement('div');
                    var n = $('td', $(this).parent()).index(this);
                    var pth = $('th:eq(' + n + ')', g.hDiv).get(0);
                    if (pth != null) {
                        $(tdDiv).css({textAlign: pth.align,width: $('div:first', pth)[0].style.width});
                        if (pth.hidden) {
                            $(this).css('display', 'none')
                        }
                    }
                    if (p.nowrap == false) {
                        $(tdDiv).css('white-space', 'normal')
                    }
                    if (this.innerHTML == '') {
                        this.innerHTML = '&nbsp;'
                    }
                    tdDiv.innerHTML = this.innerHTML;
                    var prnt = $(this).parent()[0];
                    var pid = false;
                    if (prnt.id) {
                        pid = prnt.id.substr(3)
                    }
                    if (pth != null) {
                        if (pth.process)
                            pth.process(tdDiv, pid)
                    }
                    $(this).empty().html((n == 0 && (p.showCheckbox || p.showSerialNumber)) ? '<span id="rowCheckbox-'+$(this).parent('tr').attr('id').split('-')[1]+'" class="'+((p.showCheckbox)? 'inputSelect':'')+' dtCheckbox">'+((p.showSerialNumber) ? $(this).parent('tr').attr('id').split('-')[1]:'')+'</span>' : '').append(tdDiv).removeAttr('width')
                })
            },getCellDim: function(obj) {
                var ht = parseInt($(obj).height());
                var pht = parseInt($(obj).parent().height());
                var wt = parseInt(obj.style.width);
                var pwt = parseInt($(obj).parent().width());
                var top = obj.offsetParent.offsetTop;
                var left = obj.offsetParent.offsetLeft;
                var pdl = parseInt($(obj).css('paddingLeft'));
                var pdt = parseInt($(obj).css('paddingTop'));
                return {ht: ht,wt: wt,top: top,left: left,pdl: pdl,pdt: pdt,pht: pht,pwt: pwt}
            },addRowProp: function() {
                $('tbody tr', g.bDiv).each(function() {
                    if ($.browser.msie && $.browser.version < 7.0) {
                        $(this).hover(function() {
                            $(this).addClass('trOver')
                        }, function() {
                            $(this).removeClass('trOver')
                        })
                    }
                });
                $('.inputSelect').click(function(){
                	$(this).toggleClass('dtCheckbox-checked');
                });
            },
            setTableHeight : function(){
                var h = $(window).height()-$(g.hDiv).outerHeight()-$(g.gDiv).offset().top-$(g.pDiv).outerHeight()-1;
                $(g.bDiv).css({'height': h+'px'});
            },
            pager: 0};
			
        g.bDiv = document.createElement('div');
        g.cDrag = document.createElement('div');
        g.fDiv = document.createElement('div');
        g.block = document.createElement('div');
        g.nDiv = document.createElement('div');
        g.nBtn = document.createElement('div');
        g.tDiv = document.createElement('div');
        g.sDiv = document.createElement('div');
        
		
		g.gDiv = document.createElement('div'); //creating global div
        g.gDiv.className = 'flexigrid';
        if (p.width != 'auto') {
            g.gDiv.style.width = p.width + 'px'
        }
        if ($.browser.msie) {
            $(g.gDiv).addClass('ie')
        }
        if (p.novstripe) {
            $(g.gDiv).addClass('novstripe')
        }
        $(t).before(g.gDiv);
        $(g.gDiv).append(t);
		//creating table header
		thead = document.createElement('thead');
		var tr = document.createElement('tr');
		var td = document.createElement('td');
		for (var i = 0; i < p.colModel.length; i++) {
			var cm = p.colModel[i];
			var th = document.createElement('th');
			th.innerHTML = cm.name;
			if (cm.id && cm.sortable) {
				$(th).attr('abbr', cm.id)
			}
			$(th).attr('axis', 'col' + i);
			if (cm.align) {
				th.align = cm.align
			}
			if (cm.width) {
				$(th).attr('width', cm.width)
			}
			if (cm.hidden) {
				th.hidden = true
			}
			if (cm.process) {
				th.process = cm.process
			}
			$(tr).append(th);
		}
		for (var i=0; i < p.hiddenColumns.length; i++){
			if(p.hiddenColumns[i].name == 'selectionGroup'){
				p.selectionGroup = true;
			}
		}
		$(thead).append(tr).children().append(td);
		$(t).prepend(thead);
		//creating header div
		g.hDiv = document.createElement('div');
        g.hDiv.className = 'hDiv';
        $(t).before(g.hDiv);
        g.hTable = document.createElement('table');
        g.hTable.cellPadding = 0;
        g.hTable.cellSpacing = 0;
        $(g.hDiv).append('<div class="hDivBox"></div>');
        $('div', g.hDiv).append(g.hTable);
        var thead = $("thead:first", t).get(0);
        if (thead)
            $(g.hTable).append(thead);
        thead = null;
        if (!p.colmodel)
            var ci = 0;
        $('thead tr:first th', g.hDiv).each(function(index) {
            var thdiv = document.createElement('div');
            if ($(this).attr('abbr')) {
                $(this).click(function(e) {
                	if (!$(this).hasClass('thOver'))
                        return false;
                    var obj = (e.target || e.srcElement);
                    if($(obj).hasClass('inputSelect-head')){
                    	if (!$('.inputSelect-head').hasClass('dtCheckboxHead-checked')){
                        	$('span.inputSelect').addClass('dtCheckbox-checked');
                    	}else{
                        	$('span.inputSelect').removeClass('dtCheckbox-checked');
                    	}
                    	$('.inputSelect-head').toggleClass('dtCheckboxHead-checked');
                    	return true;
                	}
                    if($(obj).attr('id')=='flexigridSno'){
                    	return false;
                    }
                    if($(obj).attr('id')=='flexigridShowGroup' || $(obj).hasClass('icon-caret-down')){
                    	$('.flexigridShowGroupOptions').toggle();
                    	p.dropdownToggle = true;
                    	return false;
                    }
                    
                    if (obj.href || obj.type)
                        return true;
                    g.changeSort(this)
                });
                if ($(this).attr('abbr') == p.sortname) {
                    this.className = 'sorted';
                    thdiv.className = 'desc' + p.sortorder
                }
            }
            if (this.hidden) {
                $(this).hide()
            }
            if (!p.colmodel) {
                $(this).attr('axis', 'col' + ci++)
            }
            $(thdiv).css({textAlign: this.align,width:((index == 0 && (p.showCheckbox || p.showSerialNumber))? parseInt(this.width,10)+ 50: this.width)+ 'px'});
            var thCheckboxHTML = '';
            if(index == 0 && (p.showCheckbox || p.showSerialNumber)){
            	if(p.showCheckbox){
            		if(p.selectionGroup){
            			thCheckboxHTML='<span class="dtCheckboxHead" id="flexigridShowGroup">'+
            			'<span class="inputSelect-head lfloat checkBox-group"></span><span class="icon-caret-down rfloat"></span></span>';
            		}else{
            			thCheckboxHTML='<span class="dtCheckboxHead inputSelect-head"></span>';
            		}
            	}else{
            		thCheckboxHTML='<span class="dtCheckboxHead" id="flexigridSno">S No</span>';
            	}
            }
            thdiv.innerHTML = thCheckboxHTML+'<span class="filterValues"></span>'+ this.innerHTML;
            $(thdiv).append('&nbsp;&nbsp;<i class="icon-arrow-up"></i><i class="icon-arrow-down"></i>');
            $(this).empty().append(thdiv).removeAttr('width').mousedown(function(e) {
               if(index != 0){
            	   g.dragStart('colMove', e, this)  
               }
            }).hover(function() {
                if (!g.colresize && !$(this).hasClass('thMove') && !g.colCopy) {
                    $(this).addClass('thOver')
                }
                if (g.colCopy) {
                    var n = $('th', g.hDiv).index(this);
                    if (n == g.dcoln) {
                        return false
                    }
                    $(this).append(g.cdropleft)
                    g.dcolt = n;
                    $(".indicator").removeClass('icon-ban-circle').addClass('icon-ok-sign');
                }
            }, function() {
                $(this).removeClass('thOver');
                if (g.colCopy) {
                    $(g.cdropleft).remove();
                    g.dcolt = null;
                    $(".indicator").removeClass('icon-ok-sign').addClass('icon-ban-circle');
                }
            })
        });
       
        g.bDiv.className = 'bDiv';
        $(t).before(g.bDiv);
        $(g.bDiv).scroll(function(e) {
            g.scroll()
        }).append('<div class="fakeScroll"></div>').append(t);
        g.addCellProp();
        g.addRowProp();
        var cdcol = $('thead tr:first th:first', g.hDiv).get(0);
        if (cdcol != null) {
            g.cDrag.className = 'cDrag';
            g.cdpad = 0;
            g.cdpad += (isNaN(parseInt($('div', cdcol).css('borderLeftWidth'))) ? 0 : parseInt($('div', cdcol).css('borderLeftWidth')));
            g.cdpad += (isNaN(parseInt($('div', cdcol).css('borderRightWidth'))) ? 0 : parseInt($('div', cdcol).css('borderRightWidth')));
            g.cdpad += (isNaN(parseInt($('div', cdcol).css('paddingLeft'))) ? 0 : parseInt($('div', cdcol).css('paddingLeft')));
            g.cdpad += (isNaN(parseInt($('div', cdcol).css('paddingRight'))) ? 0 : parseInt($('div', cdcol).css('paddingRight')));
            //g.cdpad += (isNaN(parseInt($(cdcol).css('borderLeftWidth'))) ? 0 : parseInt($(cdcol).css('borderLeftWidth')));
            g.cdpad += (isNaN(parseInt($(cdcol).css('borderRightWidth'))) ? 0 : parseInt($(cdcol).css('borderRightWidth')));
            g.cdpad += (isNaN(parseInt($(cdcol).css('paddingLeft'))) ? 0 : parseInt($(cdcol).css('paddingLeft')));
            g.cdpad += (isNaN(parseInt($(cdcol).css('paddingRight'))) ? 0 : parseInt($(cdcol).css('paddingRight')));
            $(g.bDiv).before(g.cDrag);
            var cdheight = $(g.bDiv).height();
            var hdheight = $(g.hDiv).height();
            $(g.cDrag).css({top: -hdheight + 'px'});
            $('thead tr:first th', g.hDiv).each(function() {
                var cgDiv = document.createElement('div');
                $(g.cDrag).append(cgDiv);
                if (!p.cgwidth) {
                    p.cgwidth = $(cgDiv).width()
                }
                $(cgDiv).mousedown(function(e) {
                    g.dragStart('colresize', e, this)
                });
                if ($.browser.msie && $.browser.version < 7.0) {
                    g.fixHeight($(g.gDiv).height());
                    $(cgDiv).hover(function() {
                        g.fixHeight();
                        $(this).addClass('dragging')
                    }, function() {
                        if (!g.colresize)
                            $(this).removeClass('dragging')
                    })
                }
            })
            
            g.fDiv.className = 'fDiv';
            $(g.bDiv).before(g.fDiv);
            p.filterObject = {};
            $('thead tr:first th', g.hDiv).each(function(index) {
                var fBox = document.createElement('div');
                fBox.className = 'fCol';
                if(p.selectionGroup && index==0){
                	$(g.fDiv).append('<ul class="dropdown-menu flexigridShowGroupOptions"></ul>');
                }
                $(g.fDiv).append(fBox);
                var columnId = $(this).attr('abbr');
                for(var i=0;i<p.filterOptions.length;i++){
                	var filter = p.filterOptions[i];
                	if(filter.attachToColumn == columnId){
                		p.filterObject[filter.id] = filter;
                		$(fBox).html('<span class="filterButton" id="'+filter.id+'"><i class="icon-filter"></i></span>');
                		break;
                	}
                	
                }
            });
            
        }
		
		//Creating pager div
        if (p.usepager) {
			g.pDiv = document.createElement('div');
            g.pDiv.className = 'pDiv';
            g.pDiv.innerHTML = '<div class="pDiv2"></div>';
            $(g.bDiv).after(g.pDiv);
            var html = ' <div class="pGroup"> <div class="pFirst pButton"><span class="icon-fast-backward"></span></div><div class="pPrev pButton"><span class="icon-step-backward"></span></div> </div> <div class="pGroup pagebox"><span class="pcontrol">' + p.pagetext + ' <input type="text" class="input-mini" size="4" value="1" /> ' + p.outof + ' <span> 1 </span></span></div> <div class="pGroup"> <div class="pNext pButton"><span class="icon-step-forward"></span></div><div class="pLast pButton"><span class="icon-fast-forward"></span></div> </div> <div class="pGroup"> <div class="pReload pButton"><span class=" icon-refresh"></span></div> </div> <div class="pGroup"><span class="pPageStat"></span></div>';
            $('div', g.pDiv).html(html);
            $('.pReload', g.pDiv).click(function() {
            	g.dataObj.clear();
                g.populate()
            });
            $('.pFirst', g.pDiv).click(function() {
                g.changePage('first')
            });
            $('.pPrev', g.pDiv).click(function() {
                g.changePage('prev')
            });
            $('.pNext', g.pDiv).click(function() {
                g.changePage('next')
            });
            $('.pLast', g.pDiv).click(function() {
                g.changePage('last')
            });
            $('.pcontrol input', g.pDiv).keydown(function(e) {
                if (e.keyCode == 13)
                    g.changePage('input')
            });
            if ($.browser.msie && $.browser.version < 7)
                $('.pButton', g.pDiv).hover(function() {
                    $(this).addClass('pBtnOver')
                }, function() {
                    $(this).removeClass('pBtnOver')
                });
            if (p.useRp) {
                var opt = '', sel = '';
                for (var nx = 0; nx < p.rpOptions.length; nx++) {
                    if (p.rp == p.rpOptions[nx])
                        sel = 'selected="selected"';
                    else
                        sel = '';
                    opt += "<option value='" + p.rpOptions[nx] + "' " + sel + " >" + p.rpOptions[nx] + "&nbsp;&nbsp;</option>"
                }
                $('.pDiv2', g.pDiv).prepend("<div class='pGroup'><select class='span1 resultPerPage' name='rp'>" + opt + "</select></div>");
                $('select', g.pDiv).change(function() {
                    if (p.onRpChange) {
                        p.onRpChange(+this.value)
                    } else {
                        p.newp = 1;
                        p.rp = +this.value;
                        g.populate()
                    }
                })
            }
            
            
            
            if (p.searchitems) {
                $('.pDiv2', g.pDiv).prepend("<div class='pGroup'> <div class='pSearch pButton'><span></span></div> </div>");
                $('.pSearch', g.pDiv).click(function() {
                    $(g.sDiv).slideToggle('fast', function() {
                        $('.sDiv:visible input:first', g.gDiv).trigger('focus')
                    })
                });
                g.sDiv.className = 'sDiv';
                var sitems = p.searchitems;
                var sopt = '', sel = '';
                for (var s = 0; s < sitems.length; s++) {
                    if (p.qtype == '' && sitems[s].isdefault == true) {
                        p.qtype = sitems[s].name;
                        sel = 'selected="selected"'
                    } else {
                        sel = ''
                    }
                    sopt += "<option value='" + sitems[s].name + "' " + sel + " >" + sitems[s].display + "&nbsp;&nbsp;</option>"
                }
                if (p.qtype == '') {
                    p.qtype = sitems[0].name
                }
                $(g.sDiv).append("<div class='sDiv2'>" + p.findtext + " <input type='text' value='" + p.query + "' size='30' name='q' class='qsbox' /> " + " <select name='qtype'>" + sopt + "</select></div>");
                $('input[name=q]', g.sDiv).keydown(function(e) {
                    if (e.keyCode == 13) {
                        g.doSearch()
                    }
                });
                $('select[name=qtype]', g.sDiv).keydown(function(e) {
                    if (e.keyCode == 13) {
                        g.doSearch()
                    }
                });
                $('input[value=Clear]', g.sDiv).click(function() {
                    $('input[name=q]', g.sDiv).val('');
                    p.query = '';
                    g.doSearch()
                });
                $(g.bDiv).after(g.sDiv)
            }
        }
        $(g.pDiv, g.sDiv).append("<div style='clear:both'></div>");
		
        g.mDiv = document.createElement('div');
		g.mDiv.className = 'mDiv';
		g.mDiv.innerHTML = '<div class="ftitle"></div>';
		$(g.gDiv).prepend(g.mDiv);
		
        g.cdropleft = document.createElement('span');
        g.cdropleft.className = 'cdropleft';
        g.block.className = 'gBlock';
        var gh = $(g.bDiv)[0].clientHeight;
        var gtop = g.bDiv.offsetTop;
        $(g.block).css({width: g.bDiv.style.width,height: gh,background: 'white',position: 'relative',marginBottom: (gh * -1),zIndex: 1,top: gtop,left: '0px'});
        $(g.block).fadeTo(0, p.blockOpacity);
		if ($('th', g.hDiv).length) {
            g.nDiv.className = 'nDiv';
            g.nDiv.innerHTML = "<table cellpadding='0' cellspacing='0'><tbody></tbody></table>";
            
            var cn = 0;
            $('th div', g.hDiv).each(function() {
                var kcol = $("th[axis='col" + cn + "']", g.hDiv)[0];
                var chk = 'checked="checked"';
                if (kcol.style.display == 'none') {
                    chk = ''
                }
                $('tbody', g.nDiv).append('<tr><td class="ndcol1"><input type="checkbox" ' + chk + ' class="togCol" value="' + cn + '" /></td><td class="ndcol2">' + this.innerHTML + '</td></tr>');
                cn++
            });
            $('td.ndcol2',g.nDiv).find('i').remove();
            if ($.browser.msie && $.browser.version < 7.0)
                $('tr', g.nDiv).hover(function() {
                    $(this).addClass('ndcolover')
                }, function() {
                    $(this).removeClass('ndcolover')
                });
            $('td.ndcol2', g.nDiv).click(function() {
                if ($('input:checked', g.nDiv).length <= p.minColToggle && $(this).prev().find('input')[0].checked)
                    return false;
                return g.toggleCol($(this).prev().find('input').val())
            });
            $('input.togCol', g.nDiv).click(function() {
                if ($('input:checked', g.nDiv).length < p.minColToggle && this.checked == false)
                    return false;
                $(this).parent().next().trigger('click')
            });
            $(g.gDiv).prepend(g.nDiv);
            $(g.nBtn).addClass('nBtn').html('<span></span>').attr('title', 'Hide/Show Columns').click(function() {
				$(this).toggleClass('active');
				$(g.nDiv).css({width:$(g.nDiv).width()});
                var gleft = g.nBtn.offsetLeft - $(g.nDiv).width() + $(g.nBtn).width() + 1;
    			$(g.nDiv).css({left: gleft}).noSelect();
                $(g.nDiv).toggle();
            });
            $(g.mDiv).append(g.nBtn);
        }
		
		
		//setFilter
        
        
       
        //filter end here
		
		
        $(g.bDiv).hover(function() {
            $(g.nDiv).hide();
			$(g.nBtn).removeClass('active');
        }, function() {
            if (g.multisel) {
                g.multisel = false
            }
        });
        $(g.gDiv).hover(function() {
        }, function() {
            $(g.nDiv).hide();
			$(g.nBtn).removeClass('active');
        });
        $(document).mousemove(function(e) {
            g.dragMove(e)
        }).mouseup(function(e) {
            g.dragEnd()
        }).hover(function() {
        }, function() {
            g.dragEnd()
        });
        if ($.browser.msie && $.browser.version < 7.0) {
            $('.hDiv,.bDiv,.mDiv,.pDiv,.vGrip,.tDiv, .sDiv', g.gDiv).css({width: '100%'});
            $(g.gDiv).addClass('ie6');
            if (p.width != 'auto') {
                $(g.gDiv).addClass('ie6fullwidthbug')
            }
        }
        $(document).click(function(e) {
        	if(p.isVisible & p.clickedAway){
        	    $('span.filterButton',g.fDiv).each(function(){
        	    	$(this).popover('hide');
        	    	if(!$(this).hasClass('btn-activeFilter')){
        	    		$(this).removeClass('activeFilter');
        	    	}
        	    });
        	    p.isVisible = p.clickedAway = false
        	}else{
        	    p.clickedAway = true
        	}
        	
        	if($('.flexigridShowGroupOptions').is(':visible') && p.dropdownToggle){
        		$('.flexigridShowGroupOptions').hide();
        	}
        	
        });
        g.rePosDrag();
        g.renderFilters();
        g.fixHeight();
        if(p.activeFilters.length){
        	g.updateFilters();
        }
        g.setTableHeight();
        $(window).resize(function() {
            clearTimeout(p.resizeTimer);
            p.resizeTimer = setTimeout(function(){g.setTableHeight();}, 100);
        });
        t.p = p;
        t.grid = g;
        g.populate();
        return t;
    };
    var docloaded = false;
    $(document).ready(function() {
        docloaded = true
    });
    $.fn.datatable = function(p) {
        return this.each(function() {
            if (!docloaded) {
                $(this).hide();
                var t = this;
                $(document).ready(function() {
                    $.addFlex(t, p)
                })
            } else {
                $.addFlex(this, p)
            }
        })
    };
    $.fn.flexReload = function(p) {
        return this.each(function() {
            if (this.grid && this.p.dataSourceOptions.url) {
            	this.grid.dataObj = null;
            	this.grid.populate();
            }
        })
    };
    
    $.fn.getSelectedRecords = function(p) {
    	var grid = $(this)[0].grid;
    	var records = [];
        $('.dtCheckbox-checked').each(function() {
        	var rowIndex = parseInt($(this).attr('id').split('-')[1], 10);
        	records.push(grid.dataObj.getRecord(rowIndex));
        });
        return records;
    };
    
    $.fn.getRecord = function(rowIndex) {
    	var grid = $(this)[0].grid;
    	return grid.dataObj.getRecord(rowIndex);
    };
    
    $.fn.flexOptions = function(p) {
        return this.each(function() {
            if (this.grid)
                $.extend(this.p, p)
        })
    };
    $.fn.flexToggleCol = function(cid, visible) {
        return this.each(function() {
            if (this.grid)
                this.grid.toggleCol(cid, visible)
        })
    };
    $.fn.flexAddData = function(data) {
        return this.each(function() {
            if (this.grid)
                this.grid.addData(data)
        })
    };
    $.fn.noSelect = function(p) {
        var prevent = (p == null) ? true : p;
        if (prevent) {
            return this.each(function() {
                if ($.browser.msie || $.browser.safari)
                    $(this).bind('selectstart', function() {
                        return false
                    });
                else if ($.browser.mozilla) {
                    $(this).css('MozUserSelect', 'none');
                    $('body').trigger('focus')
                } else if ($.browser.opera)
                    $(this).bind('mousedown', function() {
                        return false
                    });
                else
                    $(this).attr('unselectable', 'on')
            })
        } else {
            return this.each(function() {
                if ($.browser.msie || $.browser.safari)
                    $(this).unbind('selectstart');
                else if ($.browser.mozilla)
                    $(this).css('MozUserSelect', 'inherit');
                else if ($.browser.opera)
                    $(this).unbind('mousedown');
                else
                    $(this).removeAttr('unselectable', 'on')
            })
        }
    }
})(jQuery);
