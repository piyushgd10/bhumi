(function() {
	var templates = {
		"name":"scraper",
		"context":"scraper",
		"templates" : [
			{
				"name":"<scraper>",
				"tagClass" : "all",
				"description":"scraper with attribute \"name\"",
				"template":"<scraper name=\"${name}\">${  }</scraper>",
				"closeType" : "notSelfClosed"
			},
			{
				"name":"<query>",
				"tagClass" : "all",
				"description":"query with attributes \"name\" and \"query\"",
				"template":"<query name=\"${name}\" query=\"${query}\">${  }</query>",
				"closeType" : "notSelfClosed"
			},
			{
				"name":"<foreach>",
				"tagClass" : "all",
				"description":"iterate using <foreach> pattern in",
				"template":"<foreach pattern=\"${pattern}\" in=\"${expression}\">${  }</foreach>",
				"closeType" : "notSelfClosed"
			},
			{
				"name":"<foreach>",
				"tagClass" : "all",
				"description":"iterate using <foreach> var collection",
				"template":"<foreach var=\"${var}\" collection=\"${collection}\">${  }</foreach>",
				"closeType" : "notSelfClosed"
			},
			{
				"name":"<param>",
				"tagClass" : "queryParam",
				"description":"param with attributes \"name\" and \"value\"",
				"template":"<param name=\"${name}\" value=\"${value}\" />${  }",
				"closeType" : "selfClosed"
			},{
				"name":"<if>",
				"tagClass" : "all",
				"description":"if with attribute \"condition\" ",
				"template":"<if condition=\"${condition}\">${  }</if>",
				"closeType" : "notSelfClosed"
			},
			{
				"name":"<if>",
				"tagClass" : "all",
				"description":"if with attributes \"pattern\" and \"in\"",
				"template":"<if pattern=\"${pattern}\" in=\"${in}\">${  }</if>",
				"closeType" : "notSelfClosed"
			},
			{
				"name":"<else>",
				"tagClass" : "else",
				"description":"else ",
				"template":"<else>${  }</else>",
				"closeType" : "notSelfClosed"
			},
			{
				"name":"<while>",
				"tagClass" : "all",
				"description":"while with attribute \"condition\" ",
				"template":"<while condition=\"${condition}\">${  }</while>",
				"closeType" : "notSelfClosed"
			},
			{
				"name":"<log>",
				"tagClass" : "all",
				"description":"log with attributes \"level\" and \"value\"",
				"template":"<log level=\"${level}\" value=\"${value}\" />${  }",
				"closeType" : "selfClosed"
			},
			{
				"name":"<startTag>",
				"tagClass" : "all",
				"description":"startTag with attribute \"name\" ",
				"template":"<startTag name=\"${name}\"/>${  }",
				"closeType" : "selfClosed"
			},
			{
				"name":"<attribute>",
				"tagClass" : "attribute",
				"description":"attribute Tag with attributes \"name\" and \"value\"",
				"template":"<attribute name=\"${name}\" value=\"${value}\"/>${  }",
				"closeType" : "selfClosed"
			},
			{
				"name":"<endTag>",
				"tagClass" : "all",
				"description":"endTag with attribute \"name\" ",
				"template":"<endTag name=\"${name}\" />${  }",
				"closeType" : "selfClosed"
			},
			{
				"name":"<valueTag>",
				"tagClass" : "all",
				"description":"valueTag with attributes \"name\" and \"value\"",
				"template":"<valueTag name=\"${name}\" value=\"${value}\" />${  }",
				"closeType" : "selfClosed"
			},
			{
				"name":"<var>",
				"tagClass" : "all",
				"description":"var with attributes \"name\" and \"value\"",
				"template":"<var name=\"${name}\" value=\"${value}\" />${  }",
				"closeType" : "selfClosed"
			},
			{
				"name":"<scriptError>",
				"tagClass" : "all",
				"description":"scriptError with attribute \"message\" ",
				"template":"<scriptError message=\"${message}\" />${  }",
				"closeType" : "selfClosed"
			},
			{
				"name":"<break>",
				"tagClass" : "break",
				"description":"break tag ",
				"template":"<break />${  }",
				"closeType" : "selfClosed"
			},
			{
				"name":"<tagexp>",
				"tagClass" : "all",
				"description":"tagexp with attribute \"name\" ",
				"template":"<tagexp name=\"${name}\"><![CDATA[${  }]]></tagexp>",
				"closeType" : "notSelfClosed"
			},
			{
				"name":"<regexp>",
				"tagClass" : "all",
				"description":"regexp with attribute \"name\" ",
				"template":"<regexp name=\"${name}\"><![CDATA[${  }]]></regexp>",
				"closeType" : "notSelfClosed"
			},
			{
				"name":"<http>",
				"tagClass" : "all",
				"description":"http with attributes \"var\", \"timeout\", \"url\" and \"method\" ",
				"template":"<http var=\"${var}\" timeout=\"${timeout}\" url=\"${url}\" method=\"${method}\">${  }</http>",
				"closeType" : "notSelfClosed"
			},
			{
				"name":"<header>",
				"tagClass" : "httpHeader",
				"description":"header with attributes \"name\" and \"value\"",
				"template":"<header name=\"${name}\" value=\"${value}\" />${  }",
				"closeType" : "selfClosed"
			},
			{
				"name":"<param>",
				"tagClass" : "httpParam",
				"description":"param with attributes \"name\" and \"value\"",
				"template":"<param name=\"${name}\" value=\"${value}\" />${  }",
				"closeType" : "selfClosed"
			},
			{
				"name":"<params>",
				"tagClass" : "httpParamsMap",
				"description":"params with attribute \"map\" ",
				"template":"<params map=\"${map}\"/>${  }",
				"closeType" : "selfClosed"
			},
			{
				"name":"<file>",
				"tagClass" : "httpFile",
				"description":"file with attributes \"name\" and \"value\"",
				"template":"<file name=\"${name}\" value=\"${value}\"/>${  }",
				"closeType" : "selfClosed"
			},
			{
				"name":"<body>",
				"tagClass" : "httpBody",
				"description":"body of http",
				"template":"<body>${  }</body>",
				"closeType" : "notSelfClosed"
			},
			{
				"name":"<form>",
				"tagClass" : "all",
				"description":"form with attributes \"name\", \"method\" and \"url\"",
				"template":"<form name=\"${name}\" method=\"${method}\" url=\"${url}\">${  }</form>",
				"closeType" : "notSelfClosed"
			},
			{
				"name":"<javascript>",
				"tagClass" : "all",
				"description":"javascript with attribute \"script\" ",
				"template":"<javascript script=\"${script}\">${  }</javascript>",
				"closeType" : "notSelfClosed"
			},
			{
				"name":"<text>",
				"tagClass" : "all",
				"description":"text with attribute \"value\" ",
				"template":"<text value=\"${value}\"/>${  }",
				"closeType" : "selfClosed"
			},
			{
				"name":"<method>",
				"tagClass" : "method",
				"description":"method with attribute \"name\" ",
				"closeType" : "notSelfClosed",
				"template":"<method name=\"${name}\">${  }</method>"
			},
			{
				"name":"<invoke>",
				"tagClass" : "all",
				"description":"invoke with attribute \"method\" ",
				"template":"<invoke method=\"${method}\" />${  }",
				"closeType" : "selfClosed"
			},
			{
				"name":"<cdata>",
				"tagClass" : "cdata",
				"description":"CDATA ",
				"template":"<![CDATA[${  }]]>",
				"closeType" : "metaTag"
			}

		]
	};
	CodeMirror.templatesHint.addTemplates(templates);
})();