(function () {
  function htmlHint(editor, htmlStructure, getToken) {
    var cur = editor.getCursor();
    var token = getToken(editor, cur);
    var keywords = [];
    var i = 0;
    var j = 0;
    var k = 0;
    var from = {line: cur.line, ch: cur.ch};
    var to = {line: cur.line, ch: cur.ch};
    var flagClean = true;

    var text = editor.getRange({line: 0, ch: 0}, cur);
    
    var open = text.lastIndexOf('<');
    var close = text.lastIndexOf('>');
    
    var typeFlag = 0;       // Zero Means no flag right now. 1-tag, 2-attribute, 3-string ... this way.
    var keywords_r = [];    // Kept for required field. // Not using Now. templates in used.
    var attr_req = "";

    var tokenString = token.string.replace("<","");
    var startForEclipse = token.string;  // string to be passed for template hints.

    // console.log(token.type);

    // To find the previous tag name.
    var prevpos = editor.getCursor(), prevtok = editor.getTokenAt(prevpos);
    var previnner = CodeMirror.innerMode(editor.getMode(), prevtok.state), state = previnner.state;

    var prevTagName = state.context && state.context.tagName;
    var prevTagNameSubTag = "";
    if(prevTagName){ // finding the Sub tag Class.
        for(i = 0; i < htmlStructure.length; i++) { 
            if(htmlStructure[i].tag == prevTagName){
                prevTagNameSubTag = htmlStructure[i].subtag;
                break;
            }
        }
    }
   
    if(open > close) {
      var last = editor.getRange({line: cur.line, ch: cur.ch - 1}, cur);  // reading the last character.
      if(last == "<") {  // if only "<" present. show all tags.
        typeFlag = 1;   //it would be a tag. 
        if(prevTagName){
            for(i = 1; i < htmlStructure.length; i++){
                for(pp=0; pp < prevTagNameSubTag.length ; pp++){
                    attr_req = "";
                    if(htmlStructure[i].tagClass == prevTagNameSubTag[pp].tagClass){
                        keywords.push(htmlStructure[i].tag);
                        for (var x= 0; x < htmlStructure[i].attr.length; x++) {
                            if (htmlStructure[i].attr[x].usage == "required") {
                                attr_req +=  htmlStructure[i].attr[x].key + "=\"\" ";
                            }
                        }
                        keywords_r.push(attr_req);
                    }
                }
            }
        }else{ // Only Scrapper Case.
            keywords.push(htmlStructure[0].tag);
            attr_req = "";
            for (var x= 0; x < htmlStructure[0].attr.length; x++) {
                if (htmlStructure[0].attr[x].usage == "required") {
                    attr_req +=  htmlStructure[0].attr[x].key + "=\"\" ";
                }
            }
            keywords_r.push(attr_req);
        }
        from.ch = token.start + 1;
      } else {
        var counter = 0;
        var found = function(token, type, position) {
          counter++;
          if(counter > 50) return null; // assumming that there would not be more than 49 attributes.
          if(token.type == type) {
            return token;
          } else {
            position.ch = token.start;
            var newToken = editor.getTokenAt(position);
            return found(newToken, type, position);
          }
        };

        var in_array_tagClass = function(value, array){
            for (var i = 0; i < array.length; i++) {
                if(array[i].tagClass == value) return true;
            };
            return false;
        };

        var nodeToken = found(token, "tag", {line: cur.line, ch: cur.ch});
        if(nodeToken != null){ 
          var node = nodeToken.string.substring(1); // getting the token name. src in <src  or scraper in <scraper name
        }

        if(token.type === null && token.string.trim() === "") {  // no need to edit.
            typeFlag = 2; // For Attribute.
            for(i = 0; i < htmlStructure.length; i++) {
                if(htmlStructure[i].tag == node) {
                    if(prevTagName){
                        if( in_array_tagClass(htmlStructure[i].tagClass,prevTagNameSubTag)){
                            for(j = 0; j < htmlStructure[i].attr.length; j++) {
                                keywords.push(htmlStructure[i].attr[j].key + "=\"\" ");
                            }
                        }
                    }else{
                        for(j = 0; j < htmlStructure[0].attr.length; j++) {
                            keywords.push(htmlStructure[0].attr[j].key + "=\"\" ");
                        }
                    }
                }
            }   
        } else if(token.type == "string") { // Added Subtag Filter.
            typeFlag = 3; //String
            tokenString = tokenString.substring(1, tokenString.length - 1);
            var attributeToken = found(token, "attribute", {line: cur.line, ch: cur.ch});
            var attribute = attributeToken.string;

            for(i = 0; i < htmlStructure.length; i++) {
                if(htmlStructure[i].tag == node) {
                    if(prevTagName){
                        if( in_array_tagClass(htmlStructure[i].tagClass,prevTagNameSubTag)){
                          for(j = 0; j < htmlStructure[i].attr.length; j++) {
                            if(htmlStructure[i].attr[j].key == attribute) {
                              for(k = 0; k < htmlStructure[i].attr[j].values.length; k++) {
                                keywords.push(htmlStructure[i].attr[j].values[k]);
                              }
                            }
                          }
                        }
                    }else{
                        for(j = 0; j < htmlStructure[0].attr.length; j++) {
                            if(htmlStructure[0].attr[j].key == attribute) {
                              for(k = 0; k < htmlStructure[0].attr[j].values.length; k++) {
                                keywords.push(htmlStructure[0].attr[j].values[k]);
                              }
                            }
                        }
                    }
                }
            }
            from.ch = token.start + 1;
        } else if(token.type == "attribute") { // this is imp. Added Subtag Filter.
            typeFlag = 2; //atrribute
            for(i = 0; i < htmlStructure.length; i++) {
                if(htmlStructure[i].tag == node) {
                    if(prevTagName){
                        if( in_array_tagClass(htmlStructure[i].tagClass,prevTagNameSubTag)){
                            for(j = 0; j < htmlStructure[i].attr.length; j++) {
                                keywords.push(htmlStructure[i].attr[j].key + "=\"\" ");
                            }
                        }
                    }else{
                        for(j = 0; j < htmlStructure[0].attr.length; j++) {
                            keywords.push(htmlStructure[0].attr[j].key + "=\"\" ");
                        }
                    }
                }
            }
            from.ch = token.start;
        } else if(token.type == "tag") {  //Done.
            typeFlag = 1;
            if(prevTagName){
                for(i = 1; i < htmlStructure.length; i++){
                    for(pp=0; pp < prevTagNameSubTag.length ; pp++){
                        attr_req = "";
                        if(htmlStructure[i].tagClass == prevTagNameSubTag[pp].tagClass){
                            keywords.push(htmlStructure[i].tag);
                            for (var x= 0; x < htmlStructure[i].attr.length; x++) {
                                if (htmlStructure[i].attr[x].usage == "required") {
                                    attr_req +=  htmlStructure[i].attr[x].key + "=\"\" ";
                                }
                            }
                            keywords_r.push(attr_req);
                        }
                    }
                }
            }else{
                keywords.push(htmlStructure[0].tag);
                attr_req = "";
                for (var x= 0; x < htmlStructure[0].attr.length; x++) {
                    if (htmlStructure[0].attr[x].usage == "required") {
                        attr_req +=  htmlStructure[0].attr[x].key + "=\"\" ";
                    }
                }
                keywords_r.push(attr_req);
            }
            from.ch = token.start + 1;
        }
      }
    } else {
        // when open <= close. It means all tags closed or both open and close tags dont exist.
        typeFlag = 1 ;
        if(prevTagName){
            for(i = 1; i < htmlStructure.length; i++){
                for(pp=0; pp < prevTagNameSubTag.length ; pp++){
                    if(htmlStructure[i].tagClass == prevTagNameSubTag[pp].tagClass){
                        keywords.push("<" + htmlStructure[i].tag);
                        for (var x= 0; x < htmlStructure[i].attr.length; x++) {
                            if (htmlStructure[i].attr[x].usage == "required") {
                                attr_req +=  htmlStructure[i].attr[x].key + "=\"\" ";
                            }
                        }
                        keywords_r.push(attr_req);
                    }
                }
            }
        }else{
            // console.log("Only scraper 2");
            keywords.push("<"+htmlStructure[0].tag);
            attr_req = "";
            for (var x= 0; x < htmlStructure[0].attr.length; x++) {
                if (htmlStructure[0].attr[x].usage == "required") {
                    attr_req +=  htmlStructure[0].attr[x].key + "=\"\" ";
                }
            }
            keywords_r.push(attr_req);
        }

      tokenString = ("<" + tokenString).trim();
      startForEclipse = ("<" + startForEclipse).trim();
      from.ch = token.start;
    }

    if(flagClean === true && tokenString.trim() === "") {
      flagClean = false;
    }

    if(flagClean) {
      // keywords = cleanResults(tokenString, keywords, keywords_r);
      result = cleanResults(tokenString, keywords, keywords_r);
      keywords = result.keywords;
      keywords_r = result.keywords_r;
    }
    var scraperFlag = (from.ch == token.start);
     // templates
    keywords_templates = [];
    if (CodeMirror.templatesHint && typeFlag == 1) {  // templates for tags only.
      CodeMirror.templatesHint.getCompletions(editor, keywords_templates, startForEclipse,scraperFlag,prevTagName,prevTagNameSubTag);
    }

    for (var i = 0; i < keywords.length; i++) {
        keywords_templates.push(keywords[i]);
    };

    var dataToReturn = {list: keywords_templates, from: from, to: to, list_r: keywords_r, typeFlag: typeFlag};
    if (CodeMirror.attachContextInfo) {
      // if context info is available, attach it
      CodeMirror.attachContextInfo(dataToReturn);
    }
    return dataToReturn;
  }


  var cleanResults = function(text, keywords,keywords_r) {
    var results = [];
    var results_r = [];
    var i = 0;

    for(i = 0; i < keywords.length; i++) {
      if(keywords[i].substring(0, text.length) == text) {
        results.push(keywords[i]);
        results_r.push(keywords_r[i]);
      }
    }
    
    return {keywords: results, keywords_r: results_r};
    // return results;
  };

var htmlStructure = [   
    {   tag: 'scraper',
        tagClass: 'all',
        attr: [
            {key: "name", values: [] , usage:"required"},
            {key: "version", values: [], usage:"optional"},
            {key: "cachehttpclient", values: [], usage:"optional"}
        ],
        subtag: [ {tagClass:'all'}, {tagClass:'method'}],
        textFlag: 0
        
    },
    {   tag: 'foreach',
        tagClass: 'all', 
        attr: [
            {key: "pattern", values: [], usage:"optional"},
            {key: "in", values: [], usage:"optional"},
            {key: "var", values: [], usage:"optional"},
            {key: "varstatus", values: [], usage:"optional"},
            {key: "restmatch", values: [], usage:"optional"},
            {key: "collection", values: [], usage:"optional"},
            {key: "query", values: [], usage:"optional"}
      
        ],
        subtag: [ {tagClass:'all'},{tagClass:'break'}],
        textFlag: 0 
    },
    {   tag: 'query',
        tagClass: 'all',
        attr: [
            {key: "name", values: [] , usage:"required"},
            {key: "query", values: [], usage:"required"}
            
        ],
        subtag: [ {tagClass:'queryParam'}],
        textFlag: 0
        
    },
    {   tag: 'param',
        tagClass: 'queryParam',
        attr: [
            {key: "name", values: [] , usage:"required"},
            {key: "value", values: [], usage:"required"}
            
        ],
        subtag: [],
        textFlag: 0
        
    },
    {   tag: 'if',
        tagClass: 'all',
        attr: [
            {key: "pattern", values: [] , usage:"optional"},
            {key: "in", values: [], usage:"optional"},
            {key: "condition", values: [], usage:"optional"}
        ],
        subtag: [ {tagClass:'all'}, {tagClass:'else'}],
        textFlag: 0
        
    },
    {   tag: 'else',
        tagClass: 'else',
        attr: [],
        subtag: [ {tagClass:'all'}, {tagClass:'else'}],
        textFlag: 0
        
    },

    {   tag: 'while',
        tagClass: 'all',
        attr: [
            {key: "condition", values: [] , usage:"required"}
        ],
        subtag: [ {tagClass:'all'},{tagClass:'break'}],
        textFlag: 0
        
    },
    {   tag: 'log',
        tagClass: 'all',
        attr: [
            {key: "value", values: [] , usage:"optional"},
            {key: "level", values: ["info","debug"] , usage:"required"}
            
        ],
        subtag: [{tagClass: "cdata"}],
        textFlag: 1
        
    },
    {   tag: 'startTag',
        tagClass: 'all',
        attr: [
            {key: "name", values: [] , usage:"required"},
            {key: "appender", values: [], usage:"optional"}
            
        ],
        subtag: [ {tagClass:'attribute'}],
        textFlag: 0
        
    },
    {   tag: 'attribute',
        tagClass: 'attribute',
        attr: [
            {key: "name", values: [] , usage:"required"},
            {key: "value", values: [], usage:"required"}
            
        ],
        subtag: [],
        textFlag: 0
        
    },
    {   tag: 'endTag',
        tagClass: 'all',
        attr: [
            {key: "name", values: [] , usage:"required"},
            {key: "appender", values: [], usage:"optional"}
            
        ],
        subtag: [],
        textFlag: 0
        
    },
    {   tag: 'valueTag',
        tagClass: 'all',
        attr: [
            {key: "name", values: [] , usage:"required"},
            {key: "value", values: [], usage:"required"},
            {key: "cdata", values: [], usage:"optional"},
            {key: "appender", values: [], usage:"optional"}
            
        ],
        subtag: [ {tagClass:'attribute'}],
        textFlag: 0
        
    },
    {   tag: 'var',
        tagClass: 'all',
        attr: [
            {key: "name", values: [] , usage:"optional"},
            {key: "value", values: [], usage:"optional"}
            
        ],
        subtag: [{tagClass: "cdata"}],
        textFlag: 1
        
    },
    {   tag: 'scriptError',
        tagClass: 'all',
        attr: [
            {key: "message", values: [] , usage:"optional"}
            
        ],
        subtag: [],
        textFlag: 0
        
    },
    {   tag: 'break',
        tagClass: 'break',
        attr: [],
        subtag: [],
        textFlag: 0
        
    },
    {   tag: 'tagexp',
        tagClass: 'all',
        attr: [
            {key: "name", values: [] , usage:"required"}
            
        ],
        subtag: [{tagClass: "cdata"}],
        textFlag: 1
        
    },
    {   tag: 'regexp',
        tagClass: 'all',
        attr: [
            {key: "name", values: [] , usage:"required"}
            
        ],
        subtag: [{tagClass: "cdata"}],
        textFlag: 1
        
    },
    {   tag: 'http',
        tagClass: 'all',
        attr: [
            {key: "id", values: [] , usage:"optional"},
            {key: "method", values: ["get","post"] , usage:"required"},
            {key: "url", values: [] , usage:"required"},
            {key: "var", values: [] , usage:"optional"},
            {key: "timeout", values: [] , usage:"optional"},
            {key: "maxpagesize", values: [] , usage:"optional"},
            {key: "basicAuthUsername", values: [] , usage:"optional"},
            {key: "basicAuthPassword", values: [] , usage:"optional"},
            {key: "ntAuthUsername", values: [] , usage:"optional"},
            {key: "ntAuthPassword", values: [] , usage:"optional"},
            {key: "ntAuthDomain", values: [] , usage:"optional"},
            {key: "cacheClient", values: [] , usage:"optional"},
            {key: "parseAsXml", values: [] , usage:"optional"},
            {key: "failOnError", values: [] , usage:"optional"},
            {key: "downloadToFile", values: [] , usage:"optional"},
            {key: "multipart", values: [] , usage:"optional"},
            {key: "fetchCookies", values: [] , usage:"optional"}
            
        ],
        subtag: [
            {tagClass:'httpHeader'},
            {tagClass:'httpParam'},
            {tagClass:'httpParamsMap'},
            {tagClass:'httpFile'},
            {tagClass:'httpBody'}
        ],
        textFlag: 0
        
    },
    {   tag: 'header',
        tagClass: 'httpHeader',
        attr: [
            {key: "name", values: [] , usage:"required"},
            {key: "value", values: [] , usage:"required"},
            {key: "if", values: [] , usage:"optional"}
        ],
        subtag: [],
        textFlag: 0
        
    },
    {   tag: 'param',
        tagClass: 'httpParam',
        attr: [
            {key: "name", values: [] , usage:"required"},
            {key: "value", values: [] , usage:"required"},
            {key: "if", values: [] , usage:"optional"}
        ],
        subtag: [],
        textFlag: 0
        
    },
    {   tag: 'params',
        tagClass: 'httpParamsMap',
        attr: [
            {key: "map", values: [] , usage:"required"}
        ],
        subtag: [],
        textFlag: 0
        
    },
    {   tag: 'file',
        tagClass: 'httpFile',
        attr: [
            {key: "name", values: [] , usage:"required"},
            {key: "value", values: [] , usage:"required"},
            {key: "if", values: [] , usage:"optional"}
        ],
        subtag: [],
        textFlag: 0
        
    },
    {   tag: 'body',
        tagClass: 'httpBody',
        attr: [],
        subtag: [{tagClass: "cdata"}],
        textFlag: 1
        
    },
    {   tag: 'form',
        tagClass: 'all',
        attr: [
            {key: "name", values: [] , usage:"required"},
            {key: "method", values: [] , usage:"optional"},
            {key: "url", values: [] , usage:"optional"},
            {key: "ref", values: [] , usage:"optional"}
        ],
        subtag: [{tagClass: "httpParam"}],
        textFlag: 0
        
    },
    {   tag: 'javascript',
        tagClass: 'all',
        attr: [
            {key: "script", values: [] , usage:"required"},
            {key: "outvars", values: [] , usage:"optional"}
        ],
        subtag: [{tagClass: "cdata"}],
        textFlag: 0
        
    },
    {   tag: 'text',
        tagClass: 'all',
        attr: [
            {key: "value", values: [] , usage:"required"},
            {key: "appender", values: [] , usage:"optional"}
        ],
        subtag: [{tagClass: "cdata"}],
        textFlag: 0
        
    },
    {   tag: 'method',
        tagClass: 'method',
        attr: [
            {key: "name", values: [] , usage:"required"}
        ],
        subtag: [{tagClass:'all'}],
        textFlag: 0
        
    },
    {   tag: 'invoke',
        tagClass: 'all',
        attr: [
            {key: "method", values: [] , usage:"required"}
        ],
        subtag: [],
        textFlag: 0
        
    }
    
    
];

  var globalAttributes = [];

  CodeMirror.scraperHint = function(editor) {
    if(String.prototype.trim == undefined) {
      String.prototype.trim=function(){return this.replace(/^\s+|\s+$/g, '');};
    }
    return htmlHint(editor, htmlStructure, function (e, cur) { return e.getTokenAt(cur); });
  };
})();
