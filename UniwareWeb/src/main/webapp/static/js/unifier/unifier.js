$(document).ready(function() {
	$('#headerSearchField').blur();
	if ($('#helpPane').length > 0) {
		$("#barIcons").removeClass('hidden');
		$("#helpResource").click(function(){
			if ($('#helpContent').html() == '') {
				Uniware.Ajax.get($(this).attr('path'), 'html', function(data){
					$('#helpContent').html(data);
					$('#helpClose').click(function(){
						$('#helpPane,#barIcons').toggleClass('hidden');
					});
				});
			}
			$('#helpPane,#barIcons').toggleClass('hidden');
		});
	}

	$(".facilityChange").click(function(){
		var req = {
			'facilityCode' : $(this).attr('id').substring($(this).attr('id').indexOf('-') + 1),
			'currentUrl' : window.location.pathname
		};
		Uniware.Ajax.postJson('/user/switchfacility', JSON.stringify(req),function(response) {
			if (response.successful) {
				if (response.currentUrlAccessible) {
					window.location.reload();
				} else {
					window.location.href = '/';
				}
			} else {
				Uniware.Utils.showError(response.errors[0].description);
			}
		}, true);
	});
	
	$('#loginAs').click(function(){
		Uniware.LightBox.show('#loginAsUserDiv', function() {
			$('#users').focus();
		});
		$('#users').autocomplete({
			minLength :2,
			mustMatch :true,
			autoFocus :true,
			source : function(request,response){
				Uniware.Ajax.getJson("/lookup/users?name="+request.term,function(data){
					response($.map(data,function(item){
					return {
						label: item.userName,
						value: item.value,
						itemDetail : item
					}	
					}));
				});
			}
		});
		
	});
	$('#logoutAs').click(function(){
		Uniware.Ajax.getJson("/login/logoutAsUser", function(){
			window.location.href = '/';
		});
	});
	
	$('#userLogin').click(function(){
		var req = {};
		req.userName = $('#users').val();
		Uniware.Ajax.postJson("/login/loginAsUser",JSON.stringify(req),function(response){
			if(response.successful) {
				window.location.href = '/';
			};
		});
	});
	
	$("#reloadBar").click(function(){
		Uniware.Ajax.getJson('/admin/configuration/reload');
		$(this).addClass('hidden');
		Uniware.Utils.addNotification('Configurations are being reloaded now');
	});
	
	$('#headerSearchField').click(function(event){
		event.stopPropagation();
		$(this).animate({'width':'220px'});
	});
	
	$('#headerSearchField').focus(function(event){
		event.stopPropagation();
		$(this).animate({'width':'220px'});
	});
	
	$('html').click(function(event){
		$('#headerSearchField').animate({'width':'90px'});
		$('#headerSearchFieldOuter').removeClass('searchOpen');
		$('#headerSearchField').val('');
		$('#searchDropdown').hide();
	});
	
	$('#searchDropdown').click(function(event){
		event.stopPropagation();
		$('#headerSearchField').animate({'width':'220px'});
	});
	
	$('#headerSearchField').keyup(function(event) {
		if (event.which == 13 && $(this).val() != '') {
			Uniware.Ajax.postJson('/search/globalSearch', JSON.stringify({'keyword' : $(this).val()}),function(data){
				$('#searchDropdown').show();
				$('#searchDropdown').html(template("searchTemplate", data));
				$('#headerSearchFieldOuter').addClass('searchOpen');
			});
		}
	});
	
	if (Uniware.layout) {
		loadImportJob();
		loadExportJob();
		$('#showExports').click(loadExportJob);
		$('#showImports').click(loadImportJob);
		window.page = new Uniware.LeftNavigation();
		window.page.init();
	}
	
	if($('.pageHeadBar').length > 0){
		var theLoc = $('.pageHeadBar').position().top;
		$('.pageHeadBar').width($('.pageHeadBar').width()-17);
		$(window).scroll(function() {
			if(theLoc >= $(window).scrollTop()) {
				if($('.pageHeadBar').hasClass('fixed')) {
				$('.pageHeadBar').removeClass('fixed');
			}
		} else { 
			if(!$('.pageHeadBar').hasClass('fixed')) {
				$('.pageHeadBar').addClass('fixed');
				}
			}
		});
	}
	
});

function loadExportJob(){
	var reloadExportJob = false;
	Uniware.Ajax.getJson("/user/exportJobs",function(response){
		if(response.successful) {
			$('#exportsData').html(template("exportJobsTemplate", response));
			$.each(response.exportJobs, function(index,val){
				if(val.percentageComplete!=100){
					reloadExportJob = true;
				};
			});
			
			if(reloadExportJob==true){
				setInterval(function() {loadExportJob()}, 3000);
			}
		}
	});
}

function loadImportJob(){
	var reloadExportJob = false;
	Uniware.Ajax.getJson("/user/importJobs",function(response){
		if(response.successful) {
			$('#importsData').html(template("importJobsTemplate", response));
			$.each(response.importJobs, function(index,val){
				if(val.percentageComplete!=100){
					reloadExportJob = true;
					$('#importDataLoader').show();
				};
			});
			
			if(reloadExportJob==true){
				setInterval(function() {loadExportJob()}, 3000);
			}
		}
	});
}

Uniware.LeftNavigation = function(){
	var self = this;
	this.init = function() {
		$('#mainMenu').html(template("leftNavTemplate", Uniware.layout.tabGroups));
		$('.leftNavTab').click(self.leftNavTab);
		if ($('.main-nav > li.selected').length > 0) {
			$('#activeState').css('top',parseInt($('.main-nav > li.selected').attr('leftCount'))*39+'px');
			$('.main-nav > li.selected').trigger('click');
		}
	}
	
	this.leftNavTab = function(){
		$('.leftIcoTooltip').tooltip();
		$('.main-nav > li').removeClass('selected');
		$(this).addClass('selected');
		var leftNavTabParentIndex = $(this).attr('name').split('-')[0]; 
		var leftNavTabIndex = $(this).attr('name').split('-')[1]; 
		$('#mainMenu').addClass('navAdminInner');
		$('#tabMenu').html(template("leftNavTabTemplate", Uniware.layout.tabGroups[leftNavTabParentIndex].tabs[leftNavTabIndex].sideTabGroups)).show();
		$('#activeState').animate({'top':parseInt($('.main-nav > li.selected').attr('leftCount'))*39+'px'});
	}
}