
$(document).ready(function() {
	var inputs = $("input[type='text']");
	if (inputs.length > 0) {
		$(inputs[0]).focus();
	} else {
		inputs = $("input:not([type])");
		if (inputs.length > 0) {
			$(inputs[0]).focus();
		}
	}
	Uniware.Utils.barcode();
	$(".print").click(function() {
		Uniware.Utils.print();
	});
});
Uniware.HeadConfig = function(datatableViews,changeViewCallBack,pageConfig){
	var self = this;
	this.datatableViews = datatableViews;
	this.changeViewCallBack = changeViewCallBack;
	this.pageConfig = pageConfig;
	
	this.init = function(){
		$('.mainHeading','#pageBar .pageHeading').addClass('subHeaded');
		this.changeSubHeader(this.datatableViews[0].name);
		this.renderViewsMenu();
	};
	
	this.renderViewsMenu = function(){
		var html = [];
		html.push('<div class="dropdown">');
		html.push('<a class="dropdown-toggle btn btn-small" data-toggle="dropdown" href="#"><i class="icon-eye-open"></i>&nbsp;<i class="icon-caret-down"></i></a>');
		html.push('<ul class="dropdown-menu pull-right" role="menu" aria-labelledby="dLabel">');
		for(var i=0;i<this.datatableViews.length;i++){
			html.push('<li><a class="view" name="'+this.datatableViews[i].name+'" tabindex="-1" href="#">'+this.datatableViews[i].name+'</a></li>');
		}
		html.push('</ul>');
		html.push('</div>');
		if (this.pageConfig) {
			html.push('<div class="exportDataView icon-download-alt lfloat" style="font-size:26px;margin:0 5px 0 0;"></div>');
		}
		html = html.join("");
		$('.pageControls','#pageBar').append(html);
		this.attachMenuEvents();
	};
	
	this.attachMenuEvents = function(){
		$('a.view','#pageBar .pageControls .dropdown .dropdown-menu').click(function(){
			var nameOfView = $(this).html();
			self.changeSubHeader(nameOfView);
			for(var i=0;i<self.datatableViews.length;i++){
				if(self.datatableViews[i].name == nameOfView){
					self.changeViewCallBack(self.datatableViews[i]);
					break;
				}
			}
		});
		$('.exportDataView').click(function(){
			self.pageConfig.exportCurrentDataView();
		});
	};
	
	this.changeSubHeader = function(string){
		$('.subHeading','#pageBar .pageHeading').html(string);
	}
	this.init();
};
Uniware.PageConfig = function(pageName,tableId,datatableViewsObj,options){
	
	var self = this;
	this.pageName = pageName;
	this.datatableViewsObj = datatableViewsObj;
	this.options = options;
	this.tableObject;
	this.tableId = tableId;
	this.datatable;
	this.url = '/data/tasks/export/config/get?exportConfigName=';
	this.columnNameToIndex = {};
	
	
	this.init = function(){
		var self = this;
		Uniware.Ajax.getJson(this.url+this.pageName, function(response){
			var exportColumns = response.exportColumns;
			for (var i=0;i<exportColumns.length;i++) {
				self.columnNameToIndex[exportColumns[i].id] = i;
			}
			for (var i=0;i<response.hiddenColumns.length;i++) {
				self.columnNameToIndex[response.hiddenColumns[i].id] = exportColumns.length + i;
			}
			self.tableObject = {
				colModel : response.exportColumns,
				originalColumns : response.exportColumns,
				hiddenColumns : response.hiddenColumns,
				height: response.height,
				dataSourceOptions : {
					url:"/data/tasks/export/data",
					requestObject : {
						name: self.pageName
					}
				},
			    filterOptions : response.exportFilters
			}
			self.createTable();
		}, false);
	};
	
	this.createTable = function(){
		this.currentTableObject = {};
		if(typeof this.datatableViewsObj != 'undefined'){
			if(typeof this.datatableViewsObj.configJson != 'object'){
				this.datatableViewsObj.configJson = jQuery.parseJSON(this.datatableViewsObj.configJson);
			}
			this.tableObject.sortname = this.datatableViewsObj.configJson.sortColumns[0].column;
			this.tableObject.sortorder = this.datatableViewsObj.configJson.sortColumns[0].descending;
			this.tableObject.activeFilters = this.datatableViewsObj.configJson.filters;
			this.tableObject.fetchRP = this.datatableViewsObj.configJson.resultsPerPage;
			this.tableObject.rp = this.datatableViewsObj.configJson.resultsPerPage;
			var exportColumns = [];
			var isSortableIncluded = false;
			if(this.datatableViewsObj.configJson.columns != 'undefined' 
				&& this.datatableViewsObj.configJson.columns.length>0) {
				for(var i=0;i<this.datatableViewsObj.configJson.columns.length;i++) {
					var customColumn = this.datatableViewsObj.configJson.columns[i];
					for(var j=0;j<this.tableObject.originalColumns.length;j++) {
						var exportColumn = this.tableObject.originalColumns[j];
						if((exportColumn.id == customColumn.name)) {
							if(typeof customColumn.displayName != 'undefined') {
								exportColumn.name = customColumn.displayName;
							}
							if(typeof customColumn.width != 'undefined') {
								exportColumn.width = customColumn.width;
							}
							if(exportColumn.id == this.tableObject.sortname) {
								isSortableIncluded = true;
							}
							exportColumns.push(exportColumn);
						}
					}
				}
			} else {
				exportColumns = this.tableObject.originalColumns;
				isSortableIncluded = true;
			}
			
			if(!isSortableIncluded) {
				for(var i=0;i<this.tableObject.originalColumns.length;i++) {
					var exportColumn = this.tableObject.originalColumns[i];
					if(exportColumn.id == this.tableObject.sortname) {
						exportColumns.push(exportColumn);
					}
				}
			}
			this.tableObject.colModel = exportColumns;
		}
		this.currentTableObject = $.extend(true, this.currentTableObject,this.tableObject,this.options);
		self.datatable = $(this.tableId).datatable(this.currentTableObject);
	};
	
	this.reload = function() {
		self.datatable.flexReload();
	};
	
	this.getSelectedRecords = function() {
		return self.datatable.getSelectedRecords();
	};
	
	this.getRecord = function(rowIndex) {
		return self.datatable.getRecord(rowIndex);
	};
	
	this.getColumnValue = function(dataRecord, columnName) {
		return dataRecord.values[self.columnNameToIndex[columnName]];
	};
	
	this.exportCurrentDataView = function() {
		var req = {
				'exportJobTypeName' : this.pageName,
				'exportColums' : []
			};
		req.exportFilters = this.currentTableObject.activeFilters;
		for (var i=0;i<this.currentTableObject.colModel.length;i++) {
			req.exportColums.push(this.currentTableObject.colModel[i].id);
		}
		Uniware.Ajax.postJson("/data/tasks/export/job/create", JSON.stringify(req), function(response) {
			if (response.successful == false) {
				Uniware.Utils.showError(response.errors[0].description);
			} else {
				Uniware.Utils.addNotification('An Export Job has been created, Please check the Running Jobs.<br/>An Email will be sent on job completion.');
			}
		}, true);
	};
	
	this.setView = function(datatableViewsObj) {
		this.datatableViewsObj = datatableViewsObj;
		$(this.tableId).parents('.flexigrid').before('<table class="newTable"></table>').remove();
		$('.newTable').attr('id','flexme1').removeClass('newTable');
		this.createTable();
		$('.subHeading','#pageBar .pageHeading').html(datatableViewsObj.name);
	};
	
	this.init();
};

Uniware.DataMap = function(params){

	this.params = params;
	this.map;
	this.rowsPerIndex;
	this.requestMultiplier;
	this.lastIndex;
	
	this.init = function(){
		this.rowsPerIndex = this.params.rowsPerIndex;
		this.requestMultiplier = this.params.requestMultiplier;
		this.map = {};
	};
	
	this.pushDataToMap = function(data,pageNum,resultsPerPage,totalRecords){
		if(this.lastIndex == null){
			this.lastIndex = Math.ceil(totalRecords/this.rowsPerIndex);
		}
		var dataArray = [];
		var firstIndex = (resultsPerPage/this.rowsPerIndex)*(pageNum-1)+1;
		for(var i=0;i<data.length;i++){
			dataArray.push(data[i]);
			if((i+1)%this.rowsPerIndex == 0 || i == (data.length-1)){
				var page = firstIndex+parseInt(i/this.rowsPerIndex);
				this.map[page] = dataArray;
				dataArray = [];				
			}
		}
	};
	

	this.clear = function() {
		this.map = {};
	};
	
	this.getRecords = function(key,rp){
		if(typeof rp != 'undefined' & rp!=this.rowsPerIndex){
			var numberOfPages = rp/this.rowsPerIndex;
			var dataToSend = [];
			var fromPage = (key-1)*numberOfPages+1;
			var toPage = key*numberOfPages;
			for(var pageNumber=fromPage;pageNumber<=toPage && pageNumber<=this.lastIndex;pageNumber++){
				var array = this.map[pageNumber];
				dataToSend = dataToSend.concat(array);
			}
			return dataToSend;
		}
		else{
			return this.map[key];
		}
	};
	
	this.getRecord = function(index) {
		return this.map[Math.floor((index - 1) / this.rowsPerIndex)+ 1][(index - 1) % this.rowsPerIndex];
	}
	
	this.hasDataForPage = function(key,rp){
		if(typeof rp != 'undefined' & rp!=this.rowsPerIndex){
			var numberOfPages = rp/this.rowsPerIndex;
			var fromPage = (key-1)*numberOfPages+1;
			var toPage = key*numberOfPages;
			var hasData = true;
			for(var pageNumber=fromPage;pageNumber<=toPage && pageNumber<=this.lastIndex;pageNumber++){
				if(typeof this.map[pageNumber] == 'undefined'){
					hasData = false;
					break;
				}
			}
			return hasData;
		}
		else{
			if(typeof this.map[key] == 'undefined'){
				return false;
			}
			return true;
		}
	};
	
	this.init();
}

Uniware.DataResource = function(params){
	
	this.params = params;
	
	this.url = null;
	this.requestObject = null;
	
	this.init = function() {
		this.url = this.params.url;
		this.requestObject = this.params.options;
	};
	
	this.getData = function(searchOptions,callBack) {
		$.extend(searchOptions,this.requestObject);
		Uniware.Ajax.postJson(this.url,JSON.stringify(searchOptions),function(response) {
			callBack(response);
        });
	};
	
	this.init();
}
// Date functions begin here
Date.fromPaddedDate = function(a_str) {
	var result = null, regex = /^([0123]?\d)\/([01]?\d)\/([12]\d\d\d)$/, arr = regex.exec(a_str), d, m, y;

	if (arr && arr.length > 3) {
		d = parseInt(arr[1], 10);
		m = parseInt(arr[2], 10) - 1;
		y = parseInt(arr[3], 10);
		result = new Date(y, m, d);

		d = ((d > 9) ? '' : '0') + d;
		m = m + 1;
		m = ((m > 9) ? '' : '0') + m;
		if (result && result.toPaddedDate() !== (d + '/' + m + '/' + y)) {
			result = null;
		}
	}
	return result;
};

Date.fromFlatDate = function(a_str) {
	var result = null, regex = /^(\d\d)(\d\d)([2]\d\d\d)$/, arr = regex.exec(a_str), d, m, y;

	if (arr && arr.length > 3) {
		d = parseInt(arr[1], 10);
		m = parseInt(arr[2], 10) - 1;
		y = parseInt(arr[3], 10);
		result = new Date(y, m, d);
	}
	return result;
};

Date.prototype.toPaddedDate = function() {
	var d = this.getDate(), m = this.getMonth() + 1, y = this.getFullYear();
	return ((d > 9) ? '' : '0') + d + "/" + ((m > 9) ? '' : '0') + m + "/" + y;
};

Date.prototype.getSmallMonthName = function() {
	var m_names = [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ];
	return m_names[this.getMonth()];
};

Date.prototype.toDateTime = function() {
	var d = this.getDate(), hh = this.getHours(), mm = this.getMinutes();
	return ((d > 9) ? '' : '0') + d + ' ' + this.getSmallMonthName() + ', ' + ((hh > 9) ? '' : '0') + hh + ':' + ((mm > 9) ? '' : '0') + mm;
};

Date.prototype.toDate = function() {
	var d = this.getDate(), y = this.getFullYear();
	return ((d > 9) ? '' : '0') + d + ' ' + this.getSmallMonthName() + ' ' + y;
};

Date.prototype.timeFromNow = function(format) {
    var seconds = Math.round((new Date() - this)/1000);
    var d = Math.round(seconds / 86400), 
    th = Math.round(seconds / 3600), h = th % 24,
    tm = Math.round(seconds / 60), m = tm % 60,
    ts = seconds, s = ts % 60;
    tokens = {d: d, h: h,H:th,M:tm,m:m,S:ts,s: s};
    
    if (typeof format === 'function') {
        return format(tokens);
    } else {
        var output = format;
        output = output.replace(/\(([^%]*)%(d|H|h|M|m|S|s)([^\)]*)\)/g, function($0, $1, $2, $3) {
            var val = tokens[$2];
            return val > 0 ? $1 + val + $3 : "";
        });
        output = output.replace(/%(d|H|h|M|m|S|s)/g, function($0, $1) {
            return tokens[$1];
        });
        return output;
    }
};

//String extensions
String.compare = function(s1, s2) {
    if (s1 == s2) {return 0;}
    else if (s1 > s2) {return 1;}
    return -1;
};

String.compareIgnoreCase = function(s1, s2) {
	if (s1) {
		s1 = s1.toLowerCase();
	}
	if (s2) {
		s2 = s2.toLowerCase();
	}
	return String.compare(s1, s2);
};

String.jEscape = function(str) {
	return str.replace(/([;&,\.\+\*\~':"\!\^#$%@\[\]\(\)=>\|])/g, '\\$1');
};

String.encodeHTML = function () {
    return this.replace(/&/g, '&amp;')
       .replace(/</g, '&lt;')
       .replace(/>/g, '&gt;')
       .replace(/"/g, '&quot;');
};

String.decodeHTML = function () {
    return this.replace(/&quot;/g, '"')
       .replace(/&gt;/g, '>')
       .replace(/&lt;/g, '<')
       .replace(/&amp;/g, '&');
};

String.prototype.trim = function() {return(this.replace(/^\s+/,'').replace(/\s+$/,'')); };

Array.prototype.remove= function(){
    var what, a= arguments, L= a.length, ax;
    while(L && this.length){
        what= a[--L];
        while((ax= this.indexOf(what))!= -1){
            this.splice(ax, 1);
        }
    }
    return this;
};

Array.prototype.removeOnce= function(){
    var what, a= arguments, L= a.length, ax;
    while(L && this.length){
        what= a[--L];
        while((ax= this.indexOf(what))!= -1){
            this.splice(ax, 1);
            break;
        }
    }
    return this;
};

Math.multiply = function(a, b) {
	return Math.round(a * b * 100) / 100;
}

Math.getPercentageAmount = function(total, percentage) {
	return Math.round(total * percentage) / 100;
}

Math.getPercentageRate = function(total, amount) {
	return Math.round((amount / total) * 10000) / 100;
}

Uniware.Documents = function(documentsDiv, identifier, username, maxFileSize, mode) {
	var self = this;
	this.documentsDiv = documentsDiv;
	this.identifier = identifier;
	this.username = username;
	this.maxFileSize = maxFileSize;
	this.mode = mode;
	
	this.init = function(){
		window.addEventListener("message", function(e){
            // in case the whatfix extension is on
            if(e.data === "$#@extension:on"){
                return;
            }
			var str = '<iframe src="' + self.authResponse.url + '/documents?identifier=' + self.authResponse.identifier + '&token=' + self.authResponse.token + '&checksum='
			+ self.authResponse.checksum + '&username=' + self.username + '&maxFileSize=' + self.maxFileSize + '&mode=' + self.mode +'" style="display:block;width:100%;height:100%;" frameborder="no" scrolling="auto"></iframe>';
			$("#modalBodyCont").html(str);
			$("#modalHeadText").html('Attachments');
			$("#modalFooter").html('');
			$('#btsLightBox').modal('show');
		}, false);
		self.renderDocuments();
	};
	
	this.renderDocuments = function() {
		var req = {
				identifier : self.identifier
		}
		Uniware.Ajax.postJson("/data/document/auth/details/get", JSON.stringify(req), function(response) {
			if (response.successful) {
				self.authResponse = response; 
				var str = '<iframe src="' + self.authResponse.url + '/documents?displayType=mini&identifier=' + self.authResponse.identifier + '&token=' + self.authResponse.token + '&checksum=' 
				+ self.authResponse.checksum + '&username=' + self.username + '&maxFileSize=' + self.maxFileSize + '&mode=' + self.mode +'" style="display:block; margin-top:3px" height="22" frameborder="no" scrolling="auto"></iframe>';
				$(documentsDiv).html(str);
			} else {
				Uniware.Utils.addNotification('Unable to load documents');
			}
		});
	};
	self.init();
	
};

Uniware.Utilities = function() {
};

Uniware.Utilities.prototype = {
	getHash: function(key) {
		if (location.hash.length > 0) {
			var params = location.hash.substring(1).split('&');
			for (var i=0;i<params.length;i++) {
				if (key == params[i].split('=')[0]) {
					return params[i].split('=')[1];
				}
			}
		}
		return null;
	},
	isValidEmail : function(e) {
		var regExp = /^([a-zA-Z0-9_\.\-])+(\+[a-zA-Z0-9]+)*\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		return regExp.test(e);
	},
	print : function() {
		window.print();
	},
	printIFrame : function(url) {
		if (url.indexOf('http') == 0) {
			window.open(url);
		} else {
			if (url.indexOf('legacy') == -1) {
                if (url.indexOf('?') == -1) {
                        url += '?legacy=1';
                } else {
                        url += '&legacy=1';
                }
			}
			$("#printIframeDiv").html('<iframe src="' + Uniware.Path.http(url) + '" style="display:block;height:1px;width:1px;" frameborder="no"></iframe>');
		}
	},
	getScript : function(src, callback) {
		var scriptNode = document.createElement('script');
		scriptNode.type = "text/javascript";
		scriptNode.async = true;
		scriptNode.src = src;
		if (typeof callback === "function") {
			scriptNode.onload = scriptNode.onreadystatechange = function() {
				if (!this.readyState || this.readyState === "loaded" || this.readyState === "complete") {
					callback.call();
				}
			}
		}
		$("head")[0].appendChild(scriptNode);
	},
	getViewportPosition : function() {
		var c = {
			x : 0,
			y : 0
		}, de = document.documentElement, db = document.body;

		if (de && (de.scrollTop || de.scrollLeft)) {
			c.x = de.scrollLeft;
			c.y = de.scrollTop;
		} else if (db) {
			c.x = db.scrollLeft;
			c.y = db.scrollTop;
		}
		return c;
	},
	getViewportDimensions : function() {
		var d = {
			w : 0,
			h : 0
		}, w = window, b = document.body;

		if (w.innerWidth) {
			d.w = w.innerWidth;
			d.h = w.innerHeight;
		} else if (b && b.parentElement && b.parentElement.clientWidth) {
			d.w = b.parentElement.clientWidth;
			d.h = b.parentElement.clientHeight;
		} else if (b && b.clientWidth) {
			d.w = b.clientWidth;
			d.h = b.clientHeight;
		}
		return d;
	},
	copyProperties : function(fromObj, toObj) {
		for (var key in fromObj) {
			toObj[key] = fromObj[key];
		}
	},
	isDataTable : function(dataTable) {
		var settings = $.fn.dataTableSettings;
		for ( var i = 0, iLen = settings.length; i < iLen; i++) {
			if (settings[i].nTable == dataTable[0]) {
				return true;
			}
		}
		return false;
	},
	barcode : function() {
		$(".barcode").each(function() {
			var width = $(this).attr("barWidth");
			width = width ? parseInt(width, 10) : 1;
			var height = $(this).attr("barHeight");
			height = height ? parseInt(height, 10) : 30;
			$(this).barcode($(this).html(), "code128", {
				"barWidth" : width,
				"barHeight" : height
			});
			$(this).removeClass("barcode");
		});
	},
	formatPrice : function(input) {
		return numeral(input).format('0,0.00');
	},
	potentialWidth: function(str) {
		return $("#calWidth").html(str).width();
	},
	addNotification : function(str) {
		var w = Uniware.Utils.potentialWidth(str);
		var l = (Uniware.Utils.getViewportDimensions().w - w - 50) / 2;
		$("#actionInfo").removeClass('alert-error alert-info').addClass('alert-info hidden').width(w + 50).html(str).css({'left' : l + 'px'}).removeClass('hidden');
		$(window).one('click', function(){
			$("#actionInfo").addClass('hidden');
		});
	},
	showError : function(str) {
		var msg = str.split('|')[0];
		var w = Uniware.Utils.potentialWidth(msg);
		var l = (Uniware.Utils.getViewportDimensions().w - w - 50) / 2;
		$("#actionInfo").removeClass('alert-error alert-info').addClass('alert-error hidden').width(w + 50).html(msg).css({'left' : l + 'px'}).removeClass('hidden');
		$(window).one('click', function(){
			$("#actionInfo").addClass('hidden');
		});
	},
	// generic html structure in getItemDetail Method
	applyHover : function(onSelector) {
		if (onSelector) {
			$(onSelector).on('mouseover', '.imgHover', function() {
				var imgSrc = $(this).attr('imageSrc');
				$('#imageDiv').html('<img src="' + imgSrc + '" style="height:350px; width:300px;" class="round_all">');
				var left = $(this).offset().left + $(this).width() + 5;
				var elementTop = $(this).offset().top - $(window).scrollTop();  
				var top = elementTop > 100 ? $(this).offset().top - 100 : $(window).scrollTop() + 1;
				var viewPortTop = $(window).height() + $(window).scrollTop(); 
				if(top > (viewPortTop - 370)) {
					top = viewPortTop - 370;
				}
				$('#imageDiv').css('left',left).css('top',top);
				$('#imageDiv').removeClass('hidden')
			});
			$(onSelector).on('mouseout', '.imgHover', function() {
				$('#imageDiv').addClass('hidden');
			});
			
		} else {
			$(".imgHover").hover(function() {
				var imgSrc = $(this).attr('imageSrc');
				$('#imageDiv').html('<img src="' + imgSrc + '" style="height:350px; width:300px;" class="round_all">');
				var left = $(this).offset().left + $(this).width(); + 5;
				var elementTop = $(this).offset().top - $(window).scrollTop();  
				var top = elementTop > 100 ? $(this).offset().top - 100 : $(window).scrollTop() + 1;
				var viewPortTop = $(window).height() + $(window).scrollTop(); 
				if(top > (viewPortTop - 370)) {
					top = viewPortTop - 370;
				}
				$('#imageDiv').css('left',left).css('top',top);
				$('#imageDiv').removeClass('hidden');
	        },
	        function() {
	        	$('#imageDiv').addClass('hidden');
	        });
		}
	},
	getItemDetail : function(imageUrl, pageUrl) {
		var str = '';
		if (pageUrl) {
			str += '<div class="rfloat f15" style="padding:3px;"><a href="' + pageUrl + '" target="_blank" class="nodecoration" style="color:#333;">';
			str += '<i class="icon-external-link"></i>';
			str += '</a></div>';
		}
		if (imageUrl) {
			str += '<div class="imgHover rfloat f15" style="padding:3px;" imageSrc="' + imageUrl + '">';
			str += '<i class="icon-camera"></i>';
			str += '</div>';
		}
		str +='<div class="clear" style="padding: 0px;"/>';
		return str;
	},
	showCustomFields : function(customField) {
		var str = '';
		if(customField) {
			if(typeof customField.fieldValue != 'undefined' && customField.fieldValue != null && customField.fieldValue != '' ) {
				if (customField.valueType == 'text' || customField.valueType == 'decimal' || customField.valueType == 'integer') {
					str +='<input id="' + customField.fieldName + '" type="text" size="15" class="custom ' + (customField.className ? customField.className : '') + '"  value="' + customField.fieldValue + '" />';
				} else if(customField.valueType == 'date') {
					str +='<input id="' + customField.fieldName + '" type="text" size="15" class="custom datefield ' + (customField.className ? customField.className : '') + '" value="' + (customField.fieldValue ? new Date(customField.fieldValue).toPaddedDate() : '') + '"/>';
				} else if(customField.valueType  == 'select') {
					str +='<select id="' + customField.fieldName + '" class="w150 custom ' + (customField.className ? customField.className : '') + '">';
					for (var i=0;i< customField.possibleValues.length ;i++) {
						str +='<option value="' + customField.possibleValues[i] + '" ' + (customField.possibleValues[i] == customField.fieldValue ? ' selected="selected" ' : '') + '>' + customField.possibleValues[i] + '</option>';
					}
					str +='</select>';
				} else if(customField.valueType  == 'checkbox') {
					str +='<input id="' + customField.fieldName + '" type="checkbox" class="custom ' + (customField.className ? customField.className : '') + '" ' + (customField.fieldValue ? 'checked="checked"' : '') + ' />';
				}
			} else {
				if (customField.valueType == 'text' || customField.valueType == 'decimal' || customField.valueType == 'integer') {
					str +='<input id="' + customField.fieldName + '" type="text" size="15" class="custom ' + (customField.className ? customField.className : '') + '" />';
				} else if(customField.valueType == 'date') {
					str +='<input id="' + customField.fieldName + '" type="text" size="15" class="custom datefield ' + (customField.className ? customField.className : '') + '" />';
				} else if(customField.valueType  == 'select') {
					str +='<select id="' + customField.fieldName + '" class="w150 custom ' + (customField.className ? customField.className : '') + '" >';
					for (var i=0;i< customField.possibleValues.length ;i++) {
						str +='<option value="' + customField.possibleValues[i] + '" >' + customField.possibleValues[i] + '</option>';
					}
					str +='</select>';
				} else if(customField.valueType  == 'checkbox') {
					str +='<input id="' + customField.fieldName + '" type="checkbox" class="custom ' + (customField.className ? customField.className : '') + '" />';
				}
			}
		}
		return str;
	},
	renderComments : function(commentDiv, referenceId) {
		var str = '<iframe src="/comment?legacy=1&referenceIdentifier=' + referenceId + '" style="display:block;width:100%;height:100%;" frameborder="no" scrolling="auto"></iframe>';
		$(commentDiv).html(str);		
	},
	addCustomFieldsToRequest : function(request, customFields) {
		customFields.each(function() {
			if (!request.customFieldValues) {
				request.customFieldValues = [];
			}
			if ($(this).hasClass('datefield') && $(this).val() != '') {
				request.customFieldValues.push({'name' : $(this).attr('id'), 'value' : Date.fromPaddedDate($(this).val()).getTime()});
			} else if ($(this).is(':checkbox')) {
				request.customFieldValues.push({'name' : $(this).attr('id'), 'value' : $(this).is(':checked')});
			} else {
				request.customFieldValues.push({'name' : $(this).attr('id'), 'value' : $(this).val()});
			}
		});
	},
	escapeHtml : function(input) {
		if (input) {
			return input.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/"/g, "&quot;");	
		}
		return '';
	},
	getParameterByName: function(name) {
	    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
	    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
	        results = regex.exec(location.search);
	    return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
	}
};
Uniware.Utils = new Uniware.Utilities();

Uniware.Event = {};
Uniware.Event.ENTER_KEYUP = $.Event('keyup', {which: 13});

/***Uniware Bootstap Modal Box***/
Uniware.infoModal = function(config){
	var config = config;
	var modalContainer = $('#infoModal');
	modalContainer.find('.alert').remove();
	modalContainer.unbind();
	modalContainer.append('<div class="alert"><button type="button" class="close">&times;</button>'+config.modalContent+'</div>');
	if(typeof config.messageType != 'undefined'){
		modalContainer.find('.alert').addClass('alert-'+config.messageType);
	}
	config.defaultOptions = {
			backdrop : true,
			keyboard : true,
			show : true
	}
	if(typeof config.viewOptions != 'undefined'){
		$.extend(config.defaultOptions,config.viewOptions);
	}
	modalContainer.find('.close').click(function(){modalContainer.modal('hide');});
	modalContainer.modal(config.defaultOptions);
	if(typeof config.callBack != 'undefined'){
		for(var key in config.callBack){
			modalContainer.on(key,config.callBack[key]);
		}
	}
};

Uniware.actionModal = function(config){
	this.config = config;
	this.modalContainer = $('#actionModal');
	this.show = function(){
		var self = this;
		var config = self.config;
		self.modalContainer.unbind();
		self.modalContainer.find('.modal-header').empty().append('<button type="button" class="close" aria-hidden="true">&times;</button>').append(config.header);
		self.modalContainer.find('.modal-body').empty().append(config.body);
		self.modalContainer.find('.modal-footer').empty().append(config.footer);
		self.modalContainer.modal({
			backdrop : true,
			keyboard : true,
			show : true
		});
		if(typeof config.callBack != 'undefined'){
			for(var key in config.callBack){
				self.modalContainer.on(key,config.callBack[key]);
			}
		}
		this.modalContainer.find('.close,.dismis').click(function(){
			self.hide();
		});
		$(window).bind("keypress", function(e) {
			if (e.keyCode == 27) {
				self.hide();
			}
		});
	};
	this.hide = function(){
		var self = this;
		self.modalContainer.modal('hide');
	};
	this.show();
};

/* Uniware.LightBox START */
Uniware.LightBox = function() {
	// Close Lightbox if user clicks on fade-out screen.
	$("#lightBox").click(function() {
		Uniware.LightBox.hide();
	});

	return {
		show : function(curr, callbackOnShow, isWhite, callbackOnHide) {
			if (!curr) {
				return;
			}

			var self = Uniware.LightBox;
			if (self.current) {
				$(self.current).hide();
			}
			self.current = curr;
			
			$("#lightBox").removeClass('processing').css({
				'width' : $(document).width(),
				'height' : $(document).height()
			});
			if (isWhite && isWhite === true) {
				$("#lightBox").addClass('processing');
			}
			$("#lightBox").show();
			var position = Uniware.Utils.getViewportPosition();
			var topMargin = ($(window).height() - $(self.current).height()) / 2;
			topMargin = (topMargin < 0 ? 0 : topMargin) + position.y;
			$(self.current).css({top : topMargin});
			$(self.current).fadeIn(100, function() {
				$(self.current + "_close").click(function() {
					Uniware.LightBox.hide(callbackOnHide);
				});
				if (callbackOnShow) {
					callbackOnShow.call(self);
				}
			});

			$(window).bind("keypress", function(e) {
				if (e.keyCode == 27) {
					Uniware.LightBox.hide(callbackOnHide);
				}
			});
			if(window.top !== window.self){
				window.top.postMessage({type:"ShowMask",val:"true"}, '*');
			}
		},
		hide : function(callbackOnHide) {
			var self = Uniware.LightBox;
			if (self.current) {
				$(self.current).fadeOut(100, function() {
					if (typeof callbackOnHide === "function") {
						callbackOnHide.call(self);
					}
					$("#lightBox").hide();
				});
			} else {
				return;
			}
			$(window).unbind("keypress");
			if(window.top !== window.self){
				window.top.postMessage({type:"ShowMask",val:"false"}, '*');
			}
		}
	};
}();
/* Uniware.LightBox END */

Uniware.Service = function() {};
/* @start Uniware.Ajax */
Uniware.Service.Ajax = function() {};
Uniware.Service.Ajax.prototype = {
	get: function(url, dataType, onSuccess, bProcess, onError) {
		if (($('#401Div').length != 0 &&  !$('#401Div').is(':hidden')) || ($('#403Div').length !=0 && !$('#403Div').is(':hidden'))) {
			return;
		}
		if (bProcess === true) {
			Uniware.LightBox.show("#processing", null, true);	
		}
		var xhr = $.ajax({type: "GET", url: url, dataType: (dataType || "json"),
			success: function(data, textStatus, jqXHR) {
				if (bProcess === true) {
					Uniware.LightBox.hide();
				}
				if (typeof onSuccess === "function") {
					onSuccess.call(this, data);
				}
			},
			error: function(jqXHR, textStatus, errorThrown) {
				Uniware.LightBox.hide();
				if (jqXHR.status == 403) {
		    		Uniware.LightBox.show('#403Div');
		    		return;
		    	}
		    	var contentType = jqXHR.getResponseHeader("Content-Type");
			    if (dataType == "json" && jqXHR.status === 200 && contentType.toLowerCase().indexOf("text/html") >= 0) {
			        Uniware.LightBox.show('#401Div');
		    		return;
			    }
				if (typeof onError === "function") {
					onError.call(this, jqXHR, textStatus, errorThrown);
				}
			},
			beforeSend: function(xhr) {
				if(xhr) {xhr.setRequestHeader("Accept", "application/json");}
			}
		});
		return xhr;
	},
	getJson: function(url, onSuccess, bProcess, onError) { this.get(url, "json", onSuccess, bProcess, onError); },
	postJson: function(url, requestData, onSuccess, bProcess, onError) {
		if (($('#401Div').length != 0 &&  !$('#401Div').is(':hidden')) || ($('#403Div').length !=0 && !$('#403Div').is(':hidden'))) {
			return;
		}
		if (bProcess === true) {
			Uniware.LightBox.show("#processing", null, true);	
		}
		var xhr = $.ajax({
		    type: "POST",
		    contentType: "application/json; charset=utf-8",
		    url: url,
		    data: requestData,
		    dataType: "json",
		    success: function(data, textStatus, jqXHR) {
		    	if (bProcess === true) {
					Uniware.LightBox.hide();
				}
				if (typeof onSuccess === "function") {
					onSuccess.call(this, data);
				}
			},
		    error: function(jqXHR, textStatus, errorThrown) {
		    	Uniware.LightBox.hide();
		    	if (jqXHR.status == 403) {
		    		Uniware.LightBox.show('#403Div');
		    		return;
		    	}
		    	var contentType = jqXHR.getResponseHeader("Content-Type");
			    if (jqXHR.status === 200 && contentType.toLowerCase().indexOf("text/html") >= 0) {
			        Uniware.LightBox.show('#401Div');
		    		return;
			    }
				if (typeof onError === "function") {
					onError.call(this, jqXHR, textStatus, errorThrown);
				}
			},
		    beforeSend: function(xhr) {
				if(xhr) {xhr.setRequestHeader("Accept", "application/json");}
			}
		});
		return xhr;
	}
};
Uniware.Ajax = new Uniware.Service.Ajax();

Uniware.pipelineData = function() {
	var self = this;
	this.requestObject = null;
	
	var oCache = {
		iCacheLower: -1
	};
		
	var totalRecords = -1;
    
	this.fnSetKey = function(aoData, sKey, mValue) {
		for ( var i=0, iLen=aoData.length ; i<iLen ; i++){
			if(aoData[i].name == sKey){
				aoData[i].value = mValue;
		    }
		}
	};
		 
	this.fnGetKey = function(aoData, sKey){
		for (var i=0, iLen=aoData.length ; i<iLen ; i++){
		    if(aoData[i].name == sKey){
		    	return aoData[i].value;
		    }
		}
	    return null;
	};
		 
	this.fnDataTablesPipeline = function(sSource, aoData, fnCallback) {
	    var iPipe = 2; /* Adjust the pipe size */
	    
	    var bNeedServer = false;
	    var sEcho = self.fnGetKey(aoData, "sEcho");
	    var iRequestStart = self.fnGetKey(aoData, "iDisplayStart");
	    var iRequestLength = self.fnGetKey(aoData, "iDisplayLength");
	    var iRequestEnd = iRequestStart + iRequestLength;
	    oCache.iDisplayStart = iRequestStart;
	     
	    /* outside pipeline? */
	    if(oCache.iCacheLower < 0 || iRequestStart < oCache.iCacheLower || iRequestEnd > oCache.iCacheUpper){
	        bNeedServer = true;
	    }
	     
	    /* sorting etc changed? */
	    if(oCache.lastRequest && !bNeedServer){
	        for( var i=0, iLen=aoData.length ; i<iLen ; i++){
	            if(aoData[i].name != "iDisplayStart" && aoData[i].name != "iDisplayLength" && aoData[i].name != "sEcho"){
	                if(aoData[i].value != oCache.lastRequest[i].value){
	                    bNeedServer = true;
	                    break;
	                }
	            }
	        }
	    }
	     
	    /* Store the request for checking next time around */
	    oCache.lastRequest = aoData.slice();
	    if(bNeedServer)
	    {
	        if(iRequestStart < oCache.iCacheLower)
	        {
	            iRequestStart = iRequestStart - (iRequestLength*(iPipe-1));
	            if(iRequestStart < 0)
	            {
	                iRequestStart = 0;
	            }
	        }
	         
	        oCache.iCacheLower = iRequestStart;
	        oCache.iCacheUpper = iRequestStart + (iRequestLength * iPipe);
	        oCache.iDisplayLength = self.fnGetKey( aoData, "iDisplayLength");
	        self.fnSetKey( aoData, "iDisplayStart", iRequestStart);
	        self.fnSetKey( aoData, "iDisplayLength", iRequestLength*iPipe);
	         
	        var map = {};
			$.each(aoData, function(index, obj){
				map[obj.name] = obj.value;
			});
			var searchOptions = {
				'searchKey' : map['sSearch'],
				'displayLength' : map['iDisplayLength'],
				'displayStart' : map['iDisplayStart'],
				'columns' : map['iColumns'],
				'sortingCols' : map['iSortingCols'],
				'sortColumnIndex' : map['iSortCol_0'],
				'sortDirection' : map['sSortDir_0'],
				'columnNames' : map['sColumns'],
				'getCount' : totalRecords == -1
			}
			
			self.requestObject.searchOptions = searchOptions;
			
			
			Uniware.Ajax.postJson(sSource, JSON.stringify(self.requestObject), function(response) {
				 var json = {};
				 json.sEcho = map['sEcho'];

				 //if total no of records are cached
				 if(response.totalRecords ==  null){
					 response.totalRecords = totalRecords;
				 }else{
					 totalRecords = response.totalRecords;
				 }
				 
				 json.iTotalRecords = response.totalRecords;
				 json.iTotalDisplayRecords = response.totalRecords;
				 
				 
				 json.aaData = response.elements;
				 /* Callback processing */
				 oCache.lastJson = jQuery.extend(true, {}, json);
	             if(oCache.iCacheLower != oCache.iDisplayStart){
	                 json.aaData.splice( 0, oCache.iDisplayStart-oCache.iCacheLower);
	             }
	             json.aaData.splice( oCache.iDisplayLength, json.aaData.length);
	             fnCallback(json);  
			 }, true);
	    }
	    else
	    {
	        json = jQuery.extend(true, {}, oCache.lastJson);
	        json.sEcho = sEcho; /* Update the echo for each response */
	        json.aaData.splice( 0, iRequestStart-oCache.iCacheLower);
	        json.aaData.splice( iRequestLength, json.aaData.length);
	        fnCallback(json);
	        return;
	    }
	}
	
	this.update = function(aaData, pos){
		oCache.lastJson.aaData[pos] = aaData;
	}
};

Uniware.SpreadSheet = function(settings) {
	var self = this;
	this.colHeaders = [];
	this.changeEvents = {};
	this.columnsChanged = [];
	this.settings = settings;
	
	this.init = function() {
		if(settings.columns){
			for(var i=0;i<settings.columns.length;i++){
				if(settings.colHeaders == null){
					self.colHeaders.push(settings.columns[i].label);	
				}else{
					self.colHeaders = false;
				}
				if(!settings.columns[i].type){
					settings.columns[i].type = {renderer: Uniware.SpreadSheet.myCustomRenderer};	
				}
			 }	
		}else{
			self.colHeaders = false;
		}
		 
		this.tableSettings = {
		 	data: settings.data,
            undo:true,
            enterMoves: {row: 0, col: 1},
            manualColumnResize:true,
            fillHandle:false,
            colHeaders:self.colHeaders,
            minSpareRows:settings.minSpareRows != null ? settings.minSpareRows : 1,
            onChange: self.changeColumn,
            onBeforeChange: self.beforeChangeColumn,
            columns: settings.columns,
            autoScroll:true,
            cells: settings.cellCallBack,
            stretchH: settings.stretchH ? settings.stretchH : 'none',
            scrollbarModelH: 'native',
            onCreateRow:self.onCreateRow,
            colWidths: settings.colWidths,
            rowHeaders:settings.rowHeaders,
            
		};
		this.tableInstance  =  $(settings.tableId).handsontable(this.tableSettings);
	};
	
	this.onCreateRow = function(){
		
	}
	
	this.addFooter = function(){
		self.tableInstance.handsontable('alter', 'insert_row', 1, 1);       
	};
	
	this.addChangeEvent = function(column, callback) {
		self.changeEvents[column] = callback;
	}
	
	this.changeColumn = function(data,source){
		if(source == "loadData"){
			return;
		}
		for(var i=0; i<data.length;i++){
			if($.inArray(data[i][1],self.columnsChanged) == -1){
				var callback = self.changeEvents[data[i][1]];
				if (callback) {
					callback.call(this,data[i][0],data[i][1],data[i][2],data[i][3]);
				}	
			}
		}
		self.columnsChanged = [];
	};
	
	this.beforeChangeColumn = function(changes){
	};
	
	this.getDataAtRowProp = function(column, rowIndex) {
		return self.tableInstance.handsontable('getDataAtRowProp', rowIndex, column);
	};
	
	this.getDataAtCell = function(row, col) {
		return self.tableInstance.handsontable('getDataAtCell', row, col);
	};

	this.setDataAtRowProp = function(rowIndex, column, newValue) {
		self.columnsChanged.push(column);
		return self.tableInstance.handsontable('setDataAtRowProp', rowIndex, column, newValue);
	};
	
	this.setDataAtCell = function(row, col, newValue) {
		return self.tableInstance.handsontable('setDataAtCell', row, col, newValue);
	};
	
	this.updateSettings = function(options){
		self.tableInstance.handsontable("updateSettings", options);
	};
	
	this.getData = function(){
		return self.tableInstance.data('handsontable').getData();
	};
	
	this.isRowEmpty = function(rowIndex){
		var state = true;
		for(var i=0; i<self.colHeaders.length; i++){
			var cellData = self.tableInstance.handsontable('getDataAtCell', rowIndex, i);
			if(cellData != null && cellData != ""){
				state = state & false;	
			}
		}
		return state;
	};
	
	this.rowCount = function(){
		return self.tableInstance.handsontable('getDataAtCell');
	};
	
	this.getSelectedCell = function(){
		return self.tableInstance.handsontable('getSelected');
	}
	
	this.addRow = function(index, amount){
		return self.tableInstance.handsontable('alter', 'insert_row', index, amount);
	}
};

function isEmptyRow(instance, row) {
	  var rowData = instance.getData()[row];
	  for (var i = 0, ilen = rowData.length; i < ilen; i++) {
	    if (rowData[i] !== null) {
	      return false;
	    }
	  }
	  return true;
};

Uniware.SpreadSheet.myCustomRenderer = function(instance, td, row, col, prop, value, cellProperties) {
	var column = instance.getSettings().columns[col];
	if(column.readOnly){
		td.style.color = '#777777';
		td.style.fontStyle = 'italic';
	}
	
	if(column.style){
		for(var x in column.style){
			td.style[x] = column.style[x];
		}
	}
	if(column.align){
		td.align = column.align;
	}
	Handsontable.TextCell.renderer.apply(this, arguments);
};