/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Apr 12, 2012
 *  @author praveeng
 */
package com.uniware.web.print;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.Map;

import org.jboss.resteasy.core.Dispatcher;
import org.jboss.resteasy.mock.MockDispatcherFactory;
import org.jboss.resteasy.mock.MockHttpRequest;
import org.jboss.resteasy.mock.MockHttpResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.unifier.core.cache.CacheManager;
import com.unifier.core.template.Template;
import com.unifier.services.pdf.IPdfDocumentService;
import com.unifier.services.pdf.impl.PdfDocumentServiceImpl.PrintOptions;
import com.unifier.services.printing.IPrintConfigService;
import com.unifier.web.controller.admin.PrintConfigResource;
import com.uniware.core.vo.PrintTemplateVO;
import com.uniware.core.vo.SamplePrintTemplateVO;
import com.uniware.services.cache.PrintTemplateCache;
import com.uniware.services.cache.SamplePrintTemplateCache;
import com.uniware.services.invoice.IInvoiceService;
import com.uniware.services.saleorder.ISaleOrderService;
import com.uniware.services.shipping.IShippingProviderService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:applicationContext-test.xml")
public class TestPrintConfigResource {

    @Autowired
    PrintConfigResource      printConfigResource;

    @Autowired
    IInvoiceService          invoiceService;

    @Autowired
    ISaleOrderService        saleOrderService;

    @Autowired
    IPdfDocumentService      pdfDocumentService;

    @Autowired
    ApplicationContext       context;

    @Autowired
    IPrintConfigService      printConfigService;

    @Autowired
    IShippingProviderService shippingProviderService;

    @Before
    public void loadCache() {

    }

    @SuppressWarnings("unchecked")
    @Test
    public void testDeserializeInvoiceParams() throws FileNotFoundException, IOException, ClassNotFoundException {
        ObjectInputStream object = new ObjectInputStream(new BufferedInputStream(new FileInputStream(
                "/home/praveeng/git/Uniware/UniwareWeb/src/main/webapp/static/files/templates/invoice.obj")));
        Map<String, Object> params = (Map<String, Object>) object.readObject();
        object.close();

        Template template = CacheManager.getInstance().getCache(PrintTemplateCache.class).getTemplateByType(PrintTemplateVO.Type.INVOICE.name());
        PrintTemplateVO invoiceTemplate = CacheManager.getInstance().getCache(PrintTemplateCache.class).getPrintTemplateByType(PrintTemplateVO.Type.INVOICE);
        SamplePrintTemplateVO samplePrintTemplate = CacheManager.getInstance().getCache(SamplePrintTemplateCache.class).getSamplePrintTemplate(
                invoiceTemplate.getSamplePrintTemplateCode());
        String invoiceHtml = template.evaluate(params);
        pdfDocumentService.writeHtmlToPdf(new FileOutputStream("/home/praveeng/git/Uniware/UniwareWeb/src/main/webapp/static/files/templates/invoice.pdf"), invoiceHtml,
                new PrintOptions(samplePrintTemplate.getPrintDialog(), samplePrintTemplate.getNumberOfCopies()));
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testDeserializeShipmentLabelParams() throws FileNotFoundException, IOException, ClassNotFoundException {
        ObjectInputStream object = new ObjectInputStream(new BufferedInputStream(new FileInputStream(
                "/home/praveeng/git/Uniware/UniwareWeb/src/main/webapp/static/files/templates/shipmentLabel.obj")));
        Map<String, Object> params = (Map<String, Object>) object.readObject();
        object.close();

        Template template = CacheManager.getInstance().getCache(PrintTemplateCache.class).getTemplateByType(PrintTemplateVO.Type.SHIPMENT_LABEL.name());
        PrintTemplateVO shipmentLabelTemplate = CacheManager.getInstance().getCache(PrintTemplateCache.class).getPrintTemplateByType(PrintTemplateVO.Type.SHIPMENT_LABEL);
        SamplePrintTemplateVO samplePrintTemplate = CacheManager.getInstance().getCache(SamplePrintTemplateCache.class).getSamplePrintTemplate(
                shipmentLabelTemplate.getSamplePrintTemplateCode());
        String shipmentLabelHtml = template.evaluate(params);
        pdfDocumentService.writeHtmlToPdf(new FileOutputStream("/home/praveeng/git/Uniware/UniwareWeb/src/main/webapp/static/files/templates/shipmentLabel.pdf"),
                shipmentLabelHtml, new PrintOptions(samplePrintTemplate.getPrintDialog(), samplePrintTemplate.getNumberOfCopies()));
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testDeserializeShippingManifestParams() throws FileNotFoundException, IOException, ClassNotFoundException {
        ObjectInputStream object = new ObjectInputStream(new BufferedInputStream(new FileInputStream(
                "/home/praveeng/git/Uniware/UniwareWeb/src/main/webapp/static/files/templates/shippingManifest.obj")));
        Map<String, Object> params = (Map<String, Object>) object.readObject();
        object.close();

        Template template = CacheManager.getInstance().getCache(PrintTemplateCache.class).getTemplateByType(PrintTemplateVO.Type.SHIPPING_MANIFEST.name());
        PrintTemplateVO shippingManifestTemplate = CacheManager.getInstance().getCache(PrintTemplateCache.class).getPrintTemplateByType(PrintTemplateVO.Type.SHIPPING_MANIFEST);
        SamplePrintTemplateVO samplePrintTemplate = CacheManager.getInstance().getCache(SamplePrintTemplateCache.class).getSamplePrintTemplate(
                shippingManifestTemplate.getSamplePrintTemplateCode());
        String shippingManifestHtml = template.evaluate(params);

        pdfDocumentService.writeHtmlToPdf(new FileOutputStream("/home/praveeng/git/Uniware/UniwareWeb/src/main/webapp/static/files/templates/shippingManifest.pdf"),
                shippingManifestHtml, new PrintOptions(samplePrintTemplate.getPrintDialog(), samplePrintTemplate.getNumberOfCopies()));
    }

    @Test
    public void testGetGlobalTemplate() throws Exception {
        Dispatcher dispatcher = MockDispatcherFactory.createDispatcher();
        dispatcher.getRegistry().addSingletonResource(printConfigResource);
        MockHttpRequest request = MockHttpRequest.get("/admin/printing/globalTemplate/get");
        request.getUri().getQueryParameters().add("name", PrintTemplateVO.Type.INVOICE.name());
        MockHttpResponse response = new MockHttpResponse();
        dispatcher.invoke(request, response);
        ByteArrayOutputStream stream = (ByteArrayOutputStream) response.getOutputStream();
        System.out.println(stream.toString());
    }
}
