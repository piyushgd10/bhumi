package com.uniware.web;

import com.unifier.core.utils.FileUtils;

import java.io.File;
import java.util.StringTokenizer;

public class ListURLs {

    /**
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        File folder = new File("src/main/java/com/uniware/web/controller");
        printFileNames(folder);

    }

    private static void printFileNames(File folder) throws Exception {
        String[] fileNames = folder.list();
        for (String fileName : fileNames) {
            File newFile = new File(folder, fileName);
            if (newFile.isDirectory()) {
                printFileNames(newFile);
            } else {
                String file = FileUtils.getFileAsString(newFile.getAbsolutePath());
                StringTokenizer st = new StringTokenizer(file, "\n");
                String appender = "";
                while (st.hasMoreTokens()) {
                    String token = st.nextToken();

                    if (token.contains("@Path(")) {
                        if (token.startsWith("@")) {
                            appender = token.substring(token.indexOf('"') + 1, token.lastIndexOf('"'));
                            continue;
                        }
                        //System.out.println(token + " - " + appender + token.substring(token.indexOf('"') + 1, token.lastIndexOf('"')));
                        StringTokenizer tmpTokens = new StringTokenizer(token.substring(token.indexOf('"') + 1, token.lastIndexOf('"')), "\" ,");
                        while (tmpTokens.hasMoreTokens()) {
                            System.out.println((appender + tmpTokens.nextToken()).replaceAll("//", "/") + "\t" + newFile.getName());
                        }
                    }
                }
            }

        }
    }

}
