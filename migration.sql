create temporary table role_temp (old_resource varchar(100),access_pattern varchar(500), new_resource varchar(100), access_group varchar(100), level varchar(45));

insert into role_temp values(null, '/admin/layout/searchShelf', 'CONFIGURE_SHELVES', null, "FACILITY");

update role_temp rt, access_pattern ap, access_resource ar set old_resource = ar.name where ap.access_resource_id = ar.id and rt.access_pattern = ap.url_pattern;
delete from role_temp where old_resource is null;

update role_temp set access_group = null where access_group = '';
insert ignore into access_resource_group select null, access_group, now(), now() from role_temp;
insert ignore into access_resource select null, new_resource, (select id from access_resource_group where name=access_group), level, now(), now() from role_temp;
update access_pattern ap, access_resource old, access_resource new, role_temp rt set ap.access_resource_id = new.id where ap.access_resource_id = old.id and rt.old_resource = old.name and rt.new_resource = new.name and ap.url_pattern = rt.access_pattern;
insert ignore into role_access_resource select null, rar.role_id, new.id,1, now(), now() from role_access_resource rar,role_temp rt, access_resource ar, access_resource new where rar.access_resource_id = ar.id and ar.name = rt.old_resource and new.name = rt.new_resource;
insert ignore into product_access_resource select null, par.product_code, rt.new_resource, par.enabled, now(), now() from product_access_resource par, role_temp rt where par.access_resource_name = rt.old_resource;