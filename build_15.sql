-- Amit | July 21, 2017 | Tax on shipping charges, cod charges etc.
CREATE TABLE `invoice_item_tax` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `invoice_item_id` int(10) unsigned NOT NULL,
  `cost_head` varchar(50) NOT NULL,
  `tax_type_code` varchar(45) NOT NULL,
  `tax_percentage` decimal(6,3) NOT NULL,
  `vat` decimal(12,2) unsigned NOT NULL DEFAULT '0.00',
  `cst` decimal(12,2) unsigned NOT NULL DEFAULT '0.00',
  `service_tax` decimal(12,2) unsigned NOT NULL DEFAULT '0.00',
  `additional_tax_percentage` decimal(6,3) NOT NULL,
  `additional_tax` decimal(12,2) NOT NULL DEFAULT '0.00',
  `integrated_gst` decimal(12,2) unsigned NOT NULL DEFAULT '0.00',
  `union_territory_gst` decimal(12,2) unsigned NOT NULL DEFAULT '0.00',
  `state_gst` decimal(12,2) unsigned NOT NULL DEFAULT '0.00',
  `central_gst` decimal(12,2) unsigned NOT NULL DEFAULT '0.00',
  `compensation_cess` decimal(12,2) unsigned NOT NULL DEFAULT '0.00',
  `central_gst_percentage` decimal(6,3) NOT NULL,
  `state_gst_percentage` decimal(6,3) NOT NULL,
  `union_territory_gst_percentage` decimal(6,3) NOT NULL,
  `integrated_gst_percentage` decimal(6,3) NOT NULL,
  `compensation_cess_percentage` decimal(6,3) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_invoice_item_cost_head_unique` (`invoice_item_id`,`cost_head`),
  KEY `fk_invoice_item_taxes_invoice_item` (`invoice_item_id`),
  KEY `key_invoice_item_taxes_cost_head` (`cost_head`),
  CONSTRAINT `fk_invoice_item_taxes_invoice_item` FOREIGN KEY (`invoice_item_id`) REFERENCES `invoice_item` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

insert into invoice_item_tax select null, ii.id, "SELLING_PRICE", ii.tax_type_code, ii.tax_percentage, ii.vat, ii.cst, ii.service_tax, ii.additional_tax_percentage, ii.additional_tax, ii.integrated_gst, ii.union_territory_gst, ii.state_gst, ii.central_gst, ii.compensation_cess, ii.central_gst_percentage, ii.state_gst_percentage, ii.union_territory_gst_percentage, ii.integrated_gst_percentage, ii.compensation_cess_percentage from invoice_item ii;

-- done
-- aditya
update export_job_type set export_job_config = '<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<table-config name=\"DATATABLE GATEPASS ITEM\" height=\"1400\">\n    <queryString>\n        <![CDATA[from outbound_gate_pass ogp join invoice i on (ogp.invoice_id = i.id or ogp.return_invoice_id = i.id) join invoice_item ii on ii.invoice_id = i.id left join invoice_item_tax iit on iit.invoice_item_id = ii.id join item_type it on it.id = ii.item_type_id left join party p on p.id = ogp.to_party_id where p.tenant_id = :tenantId and iit.cost_head = \'SELLING_PRICE\' and (:facilityId is null or (ogp.facility_id = :facilityId or ogp.to_party_id = :facilityId))]]>\n    </queryString>\n    <columns>\n        <column id=\"gatepassCode\" name=\"Gatepass Code\" value=\"ogp.code\" width=\"100\" sortable=\"true\" align=\"left\" hidden=\"true\"/>\n        <column id=\"invoiceCode\" name=\"Invoice Code\" value=\"i.code\" width=\"100\" sortable=\"true\" align=\"left\">\n            \n        </column>\n        <column id=\"sku\" name=\"Sku\" value=\"it.sku_code\" width=\"100\" sortable=\"true\" align=\"left\" />\n        <column id=\"itemName\" name=\"Item Name\" value=\"it.name\" align=\"left\" width=\"150\" sortable=\"true\" />\n        <column id=\"quantity\" name=\"Quantity\" value=\"ii.quantity\" width=\"120\" sortable=\"true\" align=\"right\" />\n        <column id=\"unitPrice\" name=\"Unit Price\" value=\"ii.unit_price\" width=\"120\" sortable=\"true\" align=\"right\" />\n        <column id=\"subtotal\" name=\"Subtotal\" value=\"ii.subtotal\" width=\"120\" sortable=\"true\" align=\"right\" />\n        <column id=\"discount\" name=\"Discount\" value=\"ii.discount\" width=\"120\" sortable=\"true\" align=\"right\" />\n        <column id=\"vat\" name=\"vat\" value=\"iit.vat\" width=\"120\" sortable=\"true\" align=\"right\" />\n        <column id=\"cst\" name=\"cst\" value=\"iit.cst\" width=\"120\" sortable=\"true\" align=\"right\" />\n        <column id=\"centralGst\" name=\"Central Gst\" value=\"iit.central_gst\" width=\"120\" sortable=\"true\" align=\"right\" />\n        <column id=\"stateGst\" name=\"State Gst\" value=\"iit.state_gst\" width=\"120\" sortable=\"true\" align=\"right\" />\n        <column id=\"unionTerritoryGst\" name=\"Union Territory Gst\" value=\"iit.union_territory_gst\" width=\"120\" sortable=\"true\" align=\"right\" />\n        <column id=\"compensationCess\" name=\"Compensation Cess\" value=\"iit.compensation_cess\" width=\"120\" sortable=\"true\" align=\"right\" />\n        <column id=\"integratedGst\" name=\"Integrated Gst\" value=\"iit.integrated_gst\" width=\"120\" sortable=\"true\" align=\"right\" />\n        <column id=\"shippingCharges\" name=\"Shipping Charges\" value=\"ii.shipping_charges\" width=\"120\" sortable=\"true\" align=\"right\" />\n    </columns>\n    <filters>\n        <filter id=\"gatepassCodeFilter\" name=\"Gatepass Code is\" condition=\"ogp.code = :gatepassCodeFilter\" type=\"text\" attachToColumn=\"gatepassCode\" />\n        <filter id=\"skuFilter\" name=\"Sku contains\" condition=\"it.sku_code like :skuFilter\" type=\"text\" valueExpression=\"#{\'%\' + #skuFilter + \'%\'}\" attachToColumn=\"sku\" />\n        <filter id=\"invoiceCodeFilter\" name=\"Invoice Code is\" condition=\"i.code = :invoiceCodeFilter\" type=\"text\" attachToColumn=\"invoiceCode\" />\n    </filters>\n</table-config>\n' where name = "DATATABLE GATEPASS ITEM";
-- done

-- Amit | cost price in exports | Aug 03, 2017
update export_job_type set export_job_config = '<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<export-config>\n    <queryString>\n        <![CDATA[FROM item_type_inventory_snapshot itis join item_type it on itis.item_type_id = it.id join category c on it.category_id = c.id join facility f on (itis.facility_id = f.id) where it.tenant_id = :tenantId and (:facilityId is null or itis.facility_id = :facilityId) and it.type=\'SIMPLE\']]>\n    </queryString>\n    <columns>\n        <column id=\"facility\" name=\"Facility\" value=\"f.display_name\" />\n        <column id=\"itemTypeName\" name=\"Item Type Name\" value=\"it.name\" />\n        <column id=\"itemtypeSku\" name=\"Item SkuCode\" value=\"it.sku_code\" />\n        <column id=\"ean\" name=\"EAN\" value=\"it.ean\" />\n        <column id=\"upc\" name=\"UPC\" value=\"it.upc\" />\n        <column id=\"isbn\" name=\"ISBN\" value=\"it.isbn\" />\n        <column id=\"color\" name=\"Color\" value=\"it.color\" />\n        <column id=\"size\" name=\"Size\" value=\"it.size\" />\n        <column id=\"brand\" name=\"Brand\" value=\"it.brand\" />\n        <column id=\"categoryName\" name=\"Category Name\" value=\"c.name\" />\n        <column id=\"MRP\" name=\"MRP\" value=\"it.max_retail_price\" />\n        <column id=\"openSale\" name=\"Open Sale\" value=\"itis.open_sale\" />\n        <column id=\"inventory\" name=\"Inventory\" value=\"itis.inventory\" />\n        <column id=\"inventoryBlocked\" name=\"Inventory Blocked\" value=\"itis.inventory_blocked\" />\n        <column id=\"badInventory\" name=\"Bad Inventory\" value=\"itis.bad_inventory\" />\n        <column id=\"putawayPending\" name=\"Putaway Pending\" value=\"itis.putaway_pending\" />\n        <column id=\"pendingInventoryAssessment\" name=\"Pending Inventory Assessment\" value=\"itis.pending_inventory_assessment\" />\n        <column id=\"returnAwaited\" name=\"Return Awaited\" value=\"itis.pending_stock_transfer\" />\n        <column id=\"openPurchase\" name=\"Open Purchase\" value=\"itis.open_purchase\" />\n        <column id=\"enabled\" name=\"Enabled\" value=\"it.enabled\" />\n        <column id=\"updated\" name=\"Updated\" value=\"itis.updated\" />\n        <column id=\"costPrice\" name=\"Cost Price\" value=\"it.cost_price\" />\n    </columns>\n    <custom-fields>\n        <custom-field id=\"itemType\" entity=\"com.uniware.core.entity.ItemType\" alias=\"it\" />\n    </custom-fields>\n    <filters>\n        <filter id=\"categoryIds\" name=\"In Category\" condition=\"c.id in (:categoryIds)\" type=\"multiselect\" values=\"query:select id, name from category where tenant_id = :tenantId\" />\n        <filter id=\"brandContains\" name=\"Brand Contains\" condition=\"it.brand like :brandContains\" type=\"text\" valueExpression=\"%#{#brandContains}%\" />\n        <filter id=\"enabledFilter\" name=\"Enabled?\" condition=\"it.enabled = :enabledFilter\" type=\"boolean\" />\n        <filter id=\"updatedIn\" name=\"Updated in Date Range\" condition=\"itis.updated &gt; :updatedInStart and itis.updated &lt; :updatedInEnd\" type=\"daterange\" />\n        <filter id=\"updatedSince\" name=\"Updated Since (hours)\" condition=\"itis.updated > DATE_SUB(now(), INTERVAL :updatedSince HOUR)\" type=\"text\" />\n    </filters>\n</export-config>\n' where name = "Inventory Snapshot";
update export_job_type set export_job_config = '<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<table-config name=\"DATATABLE SEARCH PUTAWAY\" height=\"1400\">\n    <queryString>\n        <![CDATA[from putaway p join user u on u.id = p.user_id left join putaway_item pi on pi.putaway_id = p.id left join item_type it on pi.item_type_id = it.id where p.facility_id = :facilityId]]>\n    </queryString>\n    <columns>\n        <column id=\"code\" name=\"Putaway Number\" value=\"p.code\" width=\"140\" sortable=\"true\" align=\"left\">\n            <clientTemplate><![CDATA[<a class=\"link\" href=\"/putaway/createPutaway?code=<#=obj.code#>\"><#=obj.code#></a>]]></clientTemplate>\n        </column>\n        <column id=\"type\" name=\"Type\" value=\"p.type\" width=\"220\" sortable=\"true\" align=\"left\" />\n        <column id=\"username\" name=\"Generated By\" value=\"u.username\" width=\"180\" sortable=\"true\" align=\"left\" />\n        <column id=\"skuCode\" name=\"Sku Code\" value=\"it.sku_code\" width=\"130\" sortable=\"true\" align=\"left\" />\n        <column id=\"quantity\" name=\"Qty\" value=\"SUM(pi.quantity)\" width=\"80\" sortable=\"true\" align=\"left\" />\n        <column id=\"statusCode\" name=\"Status\" value=\"p.status_code\" width=\"180\" sortable=\"true\" align=\"left\" />\n        <column id=\"created\" name=\"Created At\" value=\"p.created\" width=\"150\" sortable=\"true\" align=\"left\" type=\"date\" />\n        <column id=\"updated\" name=\"Updated At\" value=\"p.updated\" width=\"150\" sortable=\"true\" align=\"left\" type=\"date\" />\n    </columns>\n    <filters>\n        <filter id=\"codeFilter\" name=\"Code Contains\" condition=\"p.code like :codeFilter\" type=\"text\" valueExpression=\"#{\'%\' + #codeFilter + \'%\'}\" attachToColumn=\"code\" />\n        <filter id=\"itemSkuFilter\" name=\"Item sellerSkuCode Code/Name contains\" condition=\"(it.sku_code like :itemSkuFilter or it.name like :itemSkuFilter)\" type=\"text\" valueExpression=\"#{\'%\' + #itemSkuFilter + \'%\'}\" attachToColumn=\"quantity\" />\n        <filter id=\"usernameFilter\" name=\"Username Contains\" condition=\"u.username like :usernameFilter\" type=\"text\" valueExpression=\"#{\'%\' + #usernameFilter + \'%\'}\" attachToColumn=\"username\" />\n        <filter id=\"typeFilter\" name=\"Type\" condition=\"p.type in (:typeFilter)\" type=\"multiselect\" values=\"enum:Cancelled|PUTAWAY_CANCELLED_ITEM,GRN|PUTAWAY_GRN_ITEM,Returned|PUTAWAY_COURIER_RETURNED_ITEM,Reverse Pickiup|PUTAWAY_REVERSE_PICKUP_ITEM,Gatepass|PUTAWAY_GATEPASS_ITEM,Inspected Not Bad|PUTAWAY_INSPECTED_NOT_BAD_ITEM,Transfer|PUTAWAY_SHELF_TRANSFER,Direct Returns|PUTAWAY_RECEIVED_RETURNS\" attachToColumn=\"type\" />\n        <filter id=\"statusFilter\" name=\"Status\" condition=\"p.status_code in (:statusFilter)\" type=\"multiselect\" values=\"query:select code,description from putaway_status\" attachToColumn=\"statusCode\" />\n        <filter id=\"dateRangeFilter\" name=\"Created in Date Range\" condition=\"p.created &gt; :dateRangeFilterStart and p.created &lt; :dateRangeFilterEnd\" type=\"daterange\" attachToColumn=\"created\" />\n        <filter id=\"productCodeFilter\" name=\"Sku Code Contains\" condition=\"it.sku_code like :productCodeFilter\" type=\"text\" valueExpression=\"#{\'%\' + #productCodeFilter + \'%\'}\" attachToColumn=\"skuCode\" />\n    </filters>\n    <groupBy>\n        group by p.id\n    </groupBy>\n</table-config>\n' where name = "DATATABLE SEARCH PUTAWAY";
update export_job_type set export_job_config = '<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<table-config name=\"DATATABLE CATEGORIES\" height=\"1400\">\n    <queryString>\n        <![CDATA[from category c left join tax_type tt on tt.id = c.tax_type_id left join tax_type gtt on gtt.id = c.gst_tax_type_id where c.tenant_id = :tenantId]]>\n    </queryString>\n    <columns>\n        <column id=\"name\" name=\"Name\" value=\"c.name\" sortable=\"true\" width=\"160\" align=\"left\">\n            <clientTemplate><![CDATA[<a class=\"link clickable\"><#=obj.name#></a>]]></clientTemplate>\n        </column>\n        <column id=\"code\" name=\"Code\" value=\"c.code\" width=\"160\" sortable=\"true\" align=\"left\" />\n        <column id=\"taxTypeCode\" name=\"Tax Type Code\" value=\"tt.code\" sortable=\"true\" width=\"160\" align=\"left\" />\n        <column id=\"gstTaxTypeCode\" name=\"GST tax Code\" value=\"gtt.code\" sortable=\"true\" width=\"160\" align=\"left\" />\n        <column id=\"itemDetailFields\" name=\"Item Details\" value=\"c.item_detail_fields\" width=\"180\" sortable=\"true\" align=\"left\" hidden=\"true\" />\n        <column id=\"created\" name=\"Created At\" value=\"c.created\" width=\"180\" sortable=\"true\" align=\"left\" type=\"date\" />\n        <column id=\"updated\" name=\"Updated At\" value=\"c.updated\" width=\"180\" sortable=\"true\" align=\"left\" type=\"date\" />\n    </columns>\n    <defaultSortBy>\n        c.updated desc\n    </defaultSortBy>\n    <filters>\n        <filter id=\"nameFilter\" name=\"Name Contains\" condition=\"c.name like :nameFilter\" type=\"text\" valueExpression=\"#{\'%\' + #nameFilter + \'%\'}\" attachToColumn=\"name\" />\n        <filter id=\"codeFilter\" name=\"Category Code Contains\" condition=\"c.code like :codeFilter\" type=\"text\" valueExpression=\"#{\'%\' + #codeFilter + \'%\'}\" attachToColumn=\"code\" />\n        <filter id=\"taxTypeCodeFilter\" name=\"Tax Type Code Contains\" condition=\"tt.code like :taxTypeCodeFilter\" type=\"text\" valueExpression=\"#{\'%\' + #taxTypeCodeFilter + \'%\'}\" attachToColumn=\"taxTypeCode\" />\n    </filters>\n</table-config>\n\n\n' where name = "DATATABLE CATEGORIES";

-- done

-- Amit | Aug 03, 2017 | default service tax
insert into tax_type select null, t.id, "GST_SERVICE_CHARGE", "GST_SERVICE_CHARGE", now(), now(), null, 1 from tenant t;
insert into tax_type_configuration select null, tt.id, null, now(), now() from tax_type tt where tt.code = "GST_SERVICE_CHARGE";
insert into tax_type_configuration_range select null, ttc.id, 0,0,0,0,0,999999999.99,now(),now(),-1,-1,-1,-1,-1 from tax_type_configuration ttc, tax_type tt where ttc.tax_type_id = tt.id and tt.code = "GST_SERVICE_CHARGE";

ALTER TABLE invoice_item modify column `tax_type_code` varchar(45) DEFAULT NULL,
 modify column  `tax_percentage` decimal(6,3) DEFAULT NULL,
 modify column  `vat` decimal(12,2) unsigned DEFAULT NULL ,
 modify column  `cst` decimal(12,2) unsigned DEFAULT NULL ,
 modify column  `service_tax` decimal(12,2) unsigned DEFAULT NULL ,
 modify column  `additional_tax_percentage` decimal(6,3) DEFAULT NULL,
 modify column  `additional_tax` decimal(12,2) DEFAULT NULL ,
 modify column  `integrated_gst` decimal(12,2) unsigned DEFAULT NULL ,
 modify column  `union_territory_gst` decimal(12,2) unsigned DEFAULT NULL ,
 modify column  `state_gst` decimal(12,2) unsigned DEFAULT NULL ,
 modify column  `central_gst` decimal(12,2) unsigned DEFAULT NULL ,
 modify column  `compensation_cess` decimal(12,2) unsigned DEFAULT NULL ,
 modify column  `central_gst_percentage` decimal(6,3) DEFAULT NULL,
 modify column  `state_gst_percentage` decimal(6,3) DEFAULT NULL,
 modify column  `union_territory_gst_percentage` decimal(6,3) DEFAULT NULL,
 modify column  `integrated_gst_percentage` decimal(6,3) DEFAULT NULL,
 modify column  `compensation_cess_percentage` decimal(6,3) DEFAULT NULL;
-- done

-- Amit | Aug 10, 2017 | Channel Created in invoice datatable
update export_job_type set export_job_config = '<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<table-config name=\"DATATABLE SEARCH INVOICES\" height=\"1400\">\n    <queryString>\n  <![CDATA[from shipping_package sp join invoice i on sp.invoice_id = i.id join user u on (i.user_id=u.id) left join invoice_item invi on invi.invoice_id = i.id left join sale_order so on sp.sale_order_id = so.id left join facility f on sp.facility_id = f.id left join channel c on so.channel_id = c.id where so.tenant_id = :tenantId and (:facilityId is null or sp.facility_id = :facilityId)]]>\n    </queryString>\n    <columns>\n        <column id=\"hiddenSaleOrderCode\" name=\"Sale Order Code\" value=\"so.code\" width=\"140\" sortable=\"true\" align=\"left\" hidden=\"true\"/>\n        <column id=\"warehouseName\" name=\"Warehouse\" value=\"f.display_name\" width=\"140\" sortable=\"true\" align=\"left\" />\n        <column id=\"invoiceCode\" name=\"Invoice #\" value=\"i.code\" width=\"140\" sortable=\"true\" align=\"left\" />\n        <column id=\"invoiceGeneratedBy\" name=\"Generated By\" value=\"u.username\" width=\"100\" sortable=\"true\" align=\"left\" />\n        <column id=\"created\" name=\"Created At\" value=\"i.created\" width=\"110\" sortable=\"true\" align=\"left\" type=\"date\" />\n        <column id=\"channelCreated\" name=\"Created On Channel At\" value=\"i.channel_created\" width=\"110\" sortable=\"true\" align=\"left\" type=\"date\" />\n        <column id=\"shippingPackageCode\" name=\"Shipping Package #\" value=\"sp.code\" width=\"170\" sortable=\"true\" align=\"left\">\n            <clientTemplate><![CDATA[<a class=\"link\" href=\"/order/shipments?orderCode=<#=obj.hiddenSaleOrderCode#>&shipmentCode=<#=obj.shippingPackageCode#>\"><#=obj.shippingPackageCode#></a>]]></clientTemplate>\n        </column>\n        <column id=\"channelColorCode\" name=\"ChannelColorCode\" value=\"c.color_code\" width=\"80\" sortable=\"true\" align=\"left\" hidden=\"true\" exportable=\"false\"/>\n\t\t<column id=\"channelShortName\" name=\"ChannelShortName\" value=\"c.short_name\" width=\"80\" sortable=\"true\" align=\"left\" hidden=\"true\" exportable=\"false\"/>\n\t\t<column id=\"channelName\" name=\"channelName\" value=\"c.name\" width=\"80\" sortable=\"true\" align=\"left\" hidden=\"true\" exportable=\"false\"/>\n\t\t<column id=\"sourceCode\" name=\"SourceCode\" value=\"#{T(com.unifier.core.cache.CacheManager).getInstance().getCache(\'channelCache\').getChannelByCode(#channel).source.code}\" width=\"130\" sortable=\"true\" align=\"left\" hidden=\"true\" calculated=\"true\" exportable=\"false\"/>\n\t\t<column id=\"channel\" name=\"Channel\" value=\"c.code\" width=\"80\" sortable=\"true\" align=\"left\" ui-config-params=\"{\'overflowAllowed\':\'true\'}\">\n\t\t\t<clientTemplate><![CDATA[<div style=\"position:relative\">\n\t\t\t\t\t\t\t\t<uni-short-logo colorcode=\"<#=channelColorCode#>\" shortcode=\"<#=channelShortName#>\" sourcecode=\"<#=sourceCode#>\" channelname=\"<#=channelName#>\"></uni-short-logo></div>]]>\n\t\t\t</clientTemplate>\n\t\t</column>\n        <column id=\"saleOrderCode\" name=\"Sale Order Code\" value=\"so.code\" width=\"140\" sortable=\"true\" align=\"left\">\n            <clientTemplate><![CDATA[<a class=\"link\" href=\"/order/orderitems?orderCode=<#=obj.saleOrderCode#>\"><#=obj.saleOrderCode#></a>]]></clientTemplate>\n        </column>\n        <column id=\"customerName\" name=\"Customer Name\" value=\"so.customer_name\" width=\"140\" sortable=\"true\" align=\"left\" />\n        <column id=\"quantity\" name=\"Quantity\" value=\"SUM(invi.quantity)\" width=\"80\" sortable=\"true\" align=\"right\" />\n        <column id=\"subTotal\" name=\"Sub Total\" value=\"SUM(invi.subtotal)\" width=\"80\" sortable=\"true\" align=\"right\" hidden=\"true\">\n            <clientTemplate><![CDATA[<#=window.numeral(obj.subTotal).format(\'0,0.00\') #>]]></clientTemplate>\n        </column>\n        <column id=\"taxes\" name=\"Taxes\" value=\"SUM(invi.vat + invi.cst)\" width=\"80\" sortable=\"true\" align=\"right\" hidden=\"true\">\n            <clientTemplate><![CDATA[<#=window.numeral(obj.taxes).format(\'0,0.00\')#>]]></clientTemplate>\n        </column>\n        <column id=\"additionalTax\" name=\"Additional Tax\" value=\"SUM(invi.additional_tax)\" width=\"110\" sortable=\"true\" align=\"right\" hidden=\"true\"/>\n        <column id=\"shippingCharges\" name=\"Shipping Charges\" value=\"SUM(invi.shipping_charges)\" width=\"110\" sortable=\"true\" align=\"right\" hidden=\"true\"/>\n        <column id=\"total\" name=\"Total\" value=\"SUM(invi.total)\" width=\"180\" sortable=\"true\" align=\"right\" hidden=\"true\">\n            <clientTemplate><![CDATA[<#=window.numeral(obj.total).format(\'0,0.00\')#>]]></clientTemplate>\n        </column>\n        <column id=\"amountDetails\" name=\"Amount Details\" value=\"so.customer_name\" width=\"180\" sortable=\"true\" align=\"right\">\n            <clientTemplate><![CDATA[Sub Total : <#=window.numeral(obj.subTotal).format(\'0,0.00\') #>  <br/> Taxes : <#=window.numeral(obj.taxes).format(\'0,0.00\')#> <br/> Additional Tax : <#=obj.additionalTax#> <br/> Shipping Charges : <#=obj.shippingCharges#> <br/> Total : <#=window.numeral(obj.total).format(\'0,0.00\') #> <br/>]]></clientTemplate>\n        </column>\n    </columns>\n    <defaultSortBy>\n        i.created desc\n    </defaultSortBy>\n    <filters>\n        <filter id=\"channelFilter\" name=\"Channel Contains\" condition=\"so.channel_id in (:channelFilter)\" type=\"multiselect\" values=\"query:select id,code from channel where tenant_id = :tenantId\" attachToColumn=\"channel\" />\n        <filter id=\"invoiceCodeFilter\" name=\"Invoice Code equals\" condition=\"i.code = :invoiceCodeFilter\" type=\"text\" attachToColumn=\"invoiceCode\" />\n        <filter id=\"shippingPackageCodeFilter\" name=\"Shipping Package Code equals\" condition=\"sp.code = :shippingPackageCodeFilter\" type=\"text\" attachToColumn=\"shippingPackageCode\" />\n        <filter id=\"saleOrderCodeFilter\" name=\"Sale Order Code equals\" condition=\"(so.code = :saleOrderCodeFilter or so.display_order_code = :saleOrderCodeFilter)\" type=\"text\" attachToColumn=\"saleOrderCode\" />\n        <filter id=\"invoiceGeneratedByFilter\" name=\"Generated By equals\" condition=\"u.username = :invoiceGeneratedByFilter\" type=\"text\" attachToColumn=\"invoiceGeneratedBy\" />\n        <filter id=\"customerNameFilter\" name=\"Customer Name Contains\" condition=\"so.customer_name like :customerNameFilter\" type=\"text\" valueExpression=\"#{\'%\' + #customerNameFilter + \'%\'}\" attachToColumn=\"customerName\" />\n        <filter id=\"dateRangeFilter\" name=\"Created in Date Range\" condition=\"i.created &gt; :dateRangeFilterStart and i.created &lt; :dateRangeFilterEnd\" type=\"daterange\" attachToColumn=\"created\" />\n    </filters>\n    <groupBy>\n        group by i.id\n    </groupBy>\n</table-config>\n\n\n' where name = "DATATABLE SEARCH INVOICES";
update export_job_type set export_job_config = '<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<table-config name=\"DATATABLE SEARCH PUTAWAY\" height=\"1400\">\n    <queryString>\n        <![CDATA[from putaway p join user u on u.id = p.user_id left join putaway_item pi on pi.putaway_id = p.id left join item_type it on pi.item_type_id = it.id where p.facility_id = :facilityId]]>\n    </queryString>\n    <columns>\n        <column id=\"code\" name=\"Putaway Number\" value=\"p.code\" width=\"140\" sortable=\"true\" align=\"left\">\n            <clientTemplate><![CDATA[<a class=\"link\" href=\"/putaway/createPutaway?code=<#=obj.code#>\"><#=obj.code#></a>]]></clientTemplate>\n        </column>\n        <column id=\"type\" name=\"Type\" value=\"p.type\" width=\"220\" sortable=\"true\" align=\"left\" />\n        <column id=\"username\" name=\"Generated By\" value=\"u.username\" width=\"180\" sortable=\"true\" align=\"left\" />\n        <column id=\"skuCode\" name=\"Sku Code\" value=\"it.sku_code\" width=\"130\" sortable=\"true\" align=\"left\" />\n        <column id=\"quantity\" name=\"Qty\" value=\"SUM(pi.quantity)\" width=\"80\" sortable=\"true\" align=\"left\" />\n        <column id=\"statusCode\" name=\"Status\" value=\"p.status_code\" width=\"180\" sortable=\"true\" align=\"left\" />\n        <column id=\"created\" name=\"Created At\" value=\"p.created\" width=\"150\" sortable=\"true\" align=\"left\" type=\"date\" />\n        <column id=\"updated\" name=\"Updated At\" value=\"p.updated\" width=\"150\" sortable=\"true\" align=\"left\" type=\"date\" />\n    </columns>\n    <filters>\n        <filter id=\"codeFilter\" name=\"Code Contains\" condition=\"p.code like :codeFilter\" type=\"text\" valueExpression=\"#{\'%\' + #codeFilter + \'%\'}\" attachToColumn=\"code\" />\n        <filter id=\"itemSkuFilter\" name=\"Item sellerSkuCode Code/Name contains\" condition=\"(it.sku_code like :itemSkuFilter or it.name like :itemSkuFilter)\" type=\"text\" valueExpression=\"#{\'%\' + #itemSkuFilter + \'%\'}\" attachToColumn=\"quantity\" />\n        <filter id=\"usernameFilter\" name=\"Username Contains\" condition=\"u.username like :usernameFilter\" type=\"text\" valueExpression=\"#{\'%\' + #usernameFilter + \'%\'}\" attachToColumn=\"username\" />\n        <filter id=\"typeFilter\" name=\"Type\" condition=\"p.type in (:typeFilter)\" type=\"multiselect\" values=\"enum:Cancelled|PUTAWAY_CANCELLED_ITEM,GRN|PUTAWAY_GRN_ITEM,Returned|PUTAWAY_COURIER_RETURNED_ITEM,Reverse Pickiup|PUTAWAY_REVERSE_PICKUP_ITEM,Gatepass|PUTAWAY_GATEPASS_ITEM,Inspected Not Bad|PUTAWAY_INSPECTED_NOT_BAD_ITEM,Transfer|PUTAWAY_SHELF_TRANSFER,Direct Returns|PUTAWAY_RECEIVED_RETURNS\" attachToColumn=\"type\" />\n        <filter id=\"statusFilter\" name=\"Status\" condition=\"p.status_code in (:statusFilter)\" type=\"multiselect\" values=\"query:select code,description from putaway_status\" attachToColumn=\"statusCode\" />\n        <filter id=\"dateRangeFilter\" name=\"Created in Date Range\" condition=\"p.created &gt; :dateRangeFilterStart and p.created &lt; :dateRangeFilterEnd\" type=\"daterange\" attachToColumn=\"created\" />\n        <filter id=\"productCodeFilter\" name=\"Sku Code Contains\" condition=\"it.sku_code like :productCodeFilter\" type=\"text\" valueExpression=\"#{\'%\' + #productCodeFilter + \'%\'}\" attachToColumn=\"skuCode\" />\n    </filters>\n    <groupBy>\n        group by p.id\n    </groupBy>\n</table-config>\n' where name = "DATATABLE SEARCH PUTAWAY";

-- Digvijay | Aug 30, 2017 | Fulfillment Tat in Sale Orders Import
update import_job_type set import_job_config = '<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<import-config>\n    <columns>\n        <column name=\"Sales Order Code\" source=\"Sales Order Code\" description=\"Sales order code (only alphanumerics, underscore and forward slash are allowed)\" groupBy=\"true\" required=\"true\" />\n        <column name=\"Display Sales Order Code\" source=\"Display Sales Order Code\" description=\"Display code on invoice and labels\" />\n        <column name=\"Notification Email\" source=\"Notification Email\" description=\"Email for sending notifications\" />\n        <column name=\"Notification Mobile\" source=\"Notification Mobile\" description=\"Mobile number for sending notifications\" />\n        <column name=\"COD\" source=\"COD\" description=\"1 if order is COD else 0\" required=\"true\" />\n        <column name=\"Channel\" source=\"Channel\" description=\"Order Channel\" />\n        <column name=\"Shipping Address Id\" source=\"Shipping Address Id\" description=\"Shipping address reference id\" />\n        <column name=\"Shipping Address Name\" source=\"Shipping Address Name\" description=\"Ship to - customer name\" />\n        <column name=\"Shipping Address Line 1\" source=\"Shipping Address Line 1\" description=\"Shipping address line 1\" />\n        <column name=\"Shipping Address Line 2\" source=\"Shipping Address Line 2\" description=\"Shipping address line 2\" />\n        <column name=\"Shipping Address City\" source=\"Shipping Address City\" description=\"Shipping address city\" />\n        <column name=\"Shipping Address State\" source=\"Shipping Address State\" description=\"Shipping address state\" />\n        <column name=\"Shipping Address Country\" source=\"Shipping Address Country\" description=\"Shipping address country\" />\n        <column name=\"Shipping Address Pincode\" source=\"Shipping Address Pincode\" description=\"Shipping address pincode\" />\n        <column name=\"Shipping Address Phone\" source=\"Shipping Address Phone\" description=\"Ship to - customer phone\" />\n        <column name=\"Billing Address Id\" source=\"Billing Address Id\" description=\"billing address reference id\" />\n        <column name=\"Billing Address Name\" source=\"Billing Address Name\" description=\"billing - customer name\" />\n        <column name=\"Billing Address Line 1\" source=\"Billing Address Line 1\" description=\"billing address line 1\" />\n        <column name=\"Billing Address Line 2\" source=\"Billing Address Line 2\" description=\"billing address line 2\" />\n        <column name=\"Billing Address City\" source=\"Billing Address City\" description=\"billing address city\" />\n        <column name=\"Billing Address State\" source=\"Billing Address State\" description=\"billing address state\" />\n        <column name=\"Billing Address Country\" source=\"Billing Address Country\" description=\"billing address country\" />\n        <column name=\"Billing Address Pincode\" source=\"Billing Address Pincode\" description=\"billing address pincode\" />\n        <column name=\"Billing Address Phone\" source=\"Billing Address Phone\" description=\"billing - customer phone\" />\n        <column name=\"Combination Identifier\" source=\"Combination Identifier\" description=\"combination identifier\" />\n        <column name=\"Combination Description\" source=\"Combination Description\" description=\"combination description\" />\n        <column name=\"Sale Order Item Code\" source=\"Sale Order Item Code\" description=\"Sale order item id/code\" required=\"true\" />\n        <column name=\"Shipping Method\" source=\"Shipping Method\" description=\"Shipping method for the item\" required=\"true\" />\n        <column name=\"Item SKU Code\" source=\"Item SKU Code\" description=\"SKU code of the item\" required=\"true\" />\n        <column name=\"Channel Product Id\" source=\"Channel Product Id\" description=\"Channel Product Id of the item\" />\n        <column name=\"Item Name\" source=\"Item Name\" description=\"Name of the item\" />\n        <column name=\"Requires Customization\" source=\"Requires Customization\" description=\"Requires Customization\" />\n        <column name=\"Gift Wrap\" source=\"Gift Wrap\" description=\"Is the order item to be gift wrapped? 1 if yes, 0 if no\" />\n        <column name=\"Gift Message\" source=\"Gift Message\" description=\"Message of the gift, if to be gift wrapped\" />\n        <column name=\"Selling Price\" source=\"Selling Price\" description=\"Selling price of the item\" required=\"true\" />\n        <column name=\"Discount\" source=\"Discount\" description=\"Discount of the item\" />\n        <column name=\"Shipping Charges\" source=\"Shipping Charges\" description=\"Shipping charges on item born by customer\" />\n        <column name=\"Shipping Method Charges\" source=\"Shipping Method Charges\" description=\"Fast shipping charges to be born by customer\" />\n        <column name=\"COD Service Charges\" source=\"COD Service Charges\" description=\"COD service charges on customer\" />\n        <column name=\"Gift Wrap Charges\" source=\"Gift Wrap Charges\" description=\"Gift wrap charges on customer\" />\n        <column name=\"Voucher Code\" source=\"Voucher Code\" description=\"Voucher Code\" />\n        <column name=\"Voucher Value\" source=\"Voucher Value\" description=\"Voucher Value\" />\n        <column name=\"Store Credit\" source=\"Store Credit\" description=\"Store Credit used by customer\" />\n        <column name=\"Packet Number\" source=\"Packet Number\" description=\"All items with same packet number in an order shipped together\" />\n        <column name=\"On Hold\" source=\"On Hold\" description=\"Whether to put item initially on hold\" />\n        <column name=\"Order Date as dd/mm/yyyy hh:MM:ss\" source=\"Order Date as dd/mm/yyyy hh:MM:ss\" description=\"Display sale order date time\" />\n        <column name=\"Shipping Provider\" source=\"Shipping Provider\" description=\"Pre-allocated shipping provider\" />\n        <column name=\"Tracking Number\" source=\"Tracking Number\" description=\"Pre-allocated shipment tracking number\" />\n        <column name=\"Quantity\" source=\"Quantity\" description=\"Number of Similar Items\" />\n        <column name=\"QuantityInCombo\" source=\"Quantity In Combo\" description=\"Quantity of an item in combo\" />\n        <column name=\"Prepaid Amount\" source=\"Prepaid Amount\" description=\"Prepaid Amount\" />\n        <column name=\"Customer Code\" source=\"Customer Code\" description=\"Customer Code\" />\n        <column name=\"Currency Code\" source=\"Currency Code\" description=\"Currency Code\" />\n        <column name=\"Order Total Shipping Charges\" source=\"Order Total Shipping Charges\" description=\"Use this in place of Shipping Charges. Value will be divided in all items equally\" />\n\t\t<column name=\"Fulfillment Tat\" source=\"Fulfillment Tat\" description=\"Fulfillment Tat (dd-MM-yyyy HH:mm:ss:SSS)\" />\n        <custom-fields entity=\"com.uniware.core.entity.SaleOrder\" />\n        <custom-fields entity=\"com.uniware.core.entity.SaleOrderItem\" />\n    </columns>\n</import-config>\n' where name = "Sale Orders";

-- done
-- Aditya
-- create entities for cycle count
create table cycle_count (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(45) NOT NULL,
  `facility_id` int(10) unsigned NOT NULL,
  `status_code` varchar(45) NOT NULL,
  `target_completion` datetime NOT NULL,
  `completed` datetime ,
  `created` datetime NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `completed_shelves` int DEFAULT NULL,
  `total_shelves` INT DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `CODE_FACILITY_unique` (`code`, `facility_id`),
  CONSTRAINT `fk_cycle_count_facility` FOREIGN KEY (`facility_id`) REFERENCES `facility` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

create table sub_cycle_count (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(45) NOT NULL,
  `cycle_count_id` int(10) unsigned NOT NULL,
  `status_code` varchar(45) NOT NULL,
  `target_completion` datetime NOT NULL,
  `completed` datetime ,
  `created` datetime NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `CODE_unique` (`code`),
  CONSTRAINT `fk_sub_cycle_count` FOREIGN KEY (`cycle_count_id`) REFERENCES `cycle_count` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

create table cycle_count_shelf_summary (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sub_cycle_count_id` int(10) unsigned NOT NULL,
  `shelf_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `non_barcoded_item_count` int(11),
  `item_codes` text(20),
  `created` datetime NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_sub_cycle_count_shelf_summary` FOREIGN KEY (`sub_cycle_count_id`) REFERENCES `sub_cycle_count` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_shelf_summary` FOREIGN KEY (`shelf_id`) REFERENCES `shelf` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_user_shelf_summary` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


create table cycle_count_item (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `shelf_id` int(10) unsigned NOT NULL,
  `sku_code` varchar(45) NOT NULL,
  `item_type_name` varchar(200) NOT NULL,
  `cycle_count_shelf_summary_id` int(10) unsigned NOT NULL,
  `inventory_type` enum('GOOD_INVENTORY','BAD_INVENTORY','QC_REJECTED', 'VIRTUAL_INVENTORY') NOT NULL,
  `expected_inventory` int(11) NOT NULL,
  `ageing_start_date` DATE,
  `inventory_difference` int(11) NOT NULL,
  `pending_reconciliation` int(11) NOT NULL,
  `discarded` tinyint(1) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_sub_cycle_count_item` FOREIGN KEY (`cycle_count_shelf_summary_id`) REFERENCES `cycle_count_shelf_summary` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_cycle_count_item_shelf` FOREIGN KEY (`shelf_id`) REFERENCES `shelf` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
-- done
-- search shelf in cycle count

create table cycle_count_error_item(
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sub_cycle_count_id` int(10) unsigned NOT NULL,
  `shelf_id` int(10) unsigned NOT NULL,
  `item_code` varchar(100),
  `item_status_code` varchar(45) NOT NULL,
  `comment` varchar(64),
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_sub_cycle_count_error_item` FOREIGN KEY (`sub_cycle_count_id`) REFERENCES `sub_cycle_count` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_cycle_count_bad_item_shelf` FOREIGN KEY (`shelf_id`) REFERENCES `shelf` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

alter table item ADD COLUMN last_cycle_count_id INT(11) NOT NULL;
-- ALTER TABLE item ADD CONSTRAINT `fk_item_cycle_count` FOREIGN KEY (`last_cycle_count_id`) REFERENCES `cycle_count` (`id`) ON DELETE CASCADE;
-- access resource changes for cycle count
insert ignore into access_resource_group (name, created, updated) VALUES ('CYCLE_COUNT', now(), now());

insert ignore into access_resource (name, access_resource_group_id, level, created, updated) SELECT 'CYCLE_COUNT_CREATE', arg.id, 'FACILITY', now(), now() from access_resource_group arg where arg.name = 'CYCLE_COUNT';
insert ignore into access_resource (name, access_resource_group_id, level, created, updated) SELECT 'CYCLE_COUNT_VIEW', arg.id, 'FACILITY', now(), now() from access_resource_group arg where arg.name = 'CYCLE_COUNT';
insert ignore into access_resource (name, access_resource_group_id, level, created, updated) SELECT 'COUNT_SHELF', arg.id, 'FACILITY', now(), now() from access_resource_group arg where arg.name = 'CYCLE_COUNT';
insert ignore into access_resource (name, access_resource_group_id, level, created, updated) SELECT 'CYCLE_COUNT_CLOSE', arg.id, 'FACILITY', now(), now() from access_resource_group arg where arg.name = 'CYCLE_COUNT';

insert ignore into access_pattern (access_resource_id, url_pattern, created, updated) SELECT ar.id, '/data/cyclecount/create', now(), now() from access_resource ar where name = 'CYCLE_COUNT_CREATE';
insert ignore into access_pattern (access_resource_id, url_pattern, created, updated) SELECT ar.id, '/data/cyclecount/subcyclecount/create', now(), now() from access_resource ar where name = 'CYCLE_COUNT_CREATE';
insert ignore into access_pattern (access_resource_id, url_pattern, created, updated) SELECT ar.id, '/data/cyclecount/zones/get', now(), now() from access_resource ar where name = 'CYCLE_COUNT_CREATE';
insert ignore into access_pattern (access_resource_id, url_pattern, created, updated) SELECT ar.id, '/data/cyclecount/shelf/get', now(), now() from access_resource ar where name = 'CYCLE_COUNT_CREATE';
insert ignore into access_pattern (access_resource_id, url_pattern, created, updated) SELECT ar.id, '/data/cyclecount/subcyclecount/shelf/add', now(), now() from access_resource ar where name = 'CYCLE_COUNT_CREATE';
insert ignore into access_pattern (access_resource_id, url_pattern, created, updated) SELECT ar.id, '/data/cyclecount/shelf/remove', now(), now() from access_resource ar where name = 'CYCLE_COUNT_CREATE';
insert ignore into access_pattern (access_resource_id, url_pattern, created, updated) SELECT ar.id, '/data/cyclecount/subcyclecount/manual/create', now(), now() from access_resource ar where name = 'CYCLE_COUNT_CREATE';
insert ignore into access_pattern (access_resource_id, url_pattern, created, updated) SELECT ar.id, '/data/cyclecount/subcyclecount/semiauto/create', now(), now() from access_resource ar where name = 'CYCLE_COUNT_CREATE';
insert ignore into access_pattern (access_resource_id, url_pattern, created, updated) SELECT ar.id, '/data/cyclecount/edit', now(), now() from access_resource ar where name = 'CYCLE_COUNT_CREATE';
insert ignore into access_pattern (access_resource_id, url_pattern, created, updated) SELECT ar.id, '/data/cyclecount/get', now(), now() from access_resource ar where name = 'CYCLE_COUNT_VIEW';
insert ignore into access_pattern (access_resource_id, url_pattern, created, updated) SELECT ar.id, '/cyclecounts', now(), now() from access_resource ar where name = 'CYCLE_COUNT_VIEW';
insert ignore into access_pattern (access_resource_id, url_pattern, created, updated) SELECT ar.id, '/subcyclecounts', now(), now() from access_resource ar where name = 'CYCLE_COUNT_VIEW';
insert ignore into access_pattern (access_resource_id, url_pattern, created, updated) SELECT ar.id, '/data/cyclecount/active/get', now(), now() from access_resource ar where name = 'CYCLE_COUNT_VIEW';
insert ignore into access_pattern (access_resource_id, url_pattern, created, updated) SELECT ar.id, '/data/cyclecount/shelf/search', now(), now() from access_resource ar where name = 'CYCLE_COUNT_VIEW';
insert ignore into access_pattern (access_resource_id, url_pattern, created, updated) SELECT ar.id, '/data/cyclecount/subcyclecount/get', now(), now() from access_resource ar where name = 'CYCLE_COUNT_VIEW';
insert ignore into access_pattern (access_resource_id, url_pattern, created, updated) SELECT ar.id, '/data/cyclecount/erroritems/get', now(), now() from access_resource ar where name = 'CYCLE_COUNT_VIEW';
insert ignore into access_pattern (access_resource_id, url_pattern, created, updated) SELECT ar.id, '/data/cyclecount/shelf/erroritems/get', now(), now() from access_resource ar where name = 'CYCLE_COUNT_VIEW';
insert ignore into access_pattern (access_resource_id, url_pattern, created, updated) SELECT ar.id, '/subcyclecount/shelves', now(), now() from access_resource ar where name = 'CYCLE_COUNT_VIEW';
insert ignore into access_pattern (access_resource_id, url_pattern, created, updated) SELECT ar.id, '/cyclecount/shelf/results', now(), now() from access_resource ar where name = 'CYCLE_COUNT_VIEW';
insert ignore into access_pattern (access_resource_id, url_pattern, created, updated) SELECT ar.id, '/cyclecount/shelf/picklists', now(), now() from access_resource ar where name = 'CYCLE_COUNT_VIEW';
insert ignore into access_pattern (access_resource_id, url_pattern, created, updated) SELECT ar.id, '/cyclecount/results', now(), now() from access_resource ar where name = 'CYCLE_COUNT_VIEW';
insert ignore into access_pattern (access_resource_id, url_pattern, created, updated) SELECT ar.id, '/cyclecount/performance', now(), now() from access_resource ar where name = 'CYCLE_COUNT_VIEW';
insert ignore into access_pattern (access_resource_id, url_pattern, created, updated) SELECT ar.id, '/data/cyclecount/shelf/submit', now(), now() from access_resource ar where name = 'COUNT_SHELF';
insert ignore into access_pattern (access_resource_id, url_pattern, created, updated) SELECT ar.id, '/data/cyclecount/shelf/start', now(), now() from access_resource ar where name = 'COUNT_SHELF';
insert ignore into access_pattern (access_resource_id, url_pattern, created, updated) SELECT ar.id, '/data/cyclecount/shelf/block', now(), now() from access_resource ar where name = 'COUNT_SHELF';
insert ignore into access_pattern (access_resource_id, url_pattern, created, updated) SELECT ar.id, '/data/cyclecount/shelf/unblock', now(), now() from access_resource ar where name = 'COUNT_SHELF';
insert ignore into access_pattern (access_resource_id, url_pattern, created, updated) SELECT ar.id, '/data/cyclecount/shelf/recount', now(), now() from access_resource ar where name = 'COUNT_SHELF';
insert ignore into access_pattern (access_resource_id, url_pattern, created, updated) SELECT ar.id, '/data/cyclecount/item/get', now(), now() from access_resource ar where name = 'COUNT_SHELF';
insert ignore into access_pattern (access_resource_id, url_pattern, created, updated) SELECT ar.id, '/data/cyclecount/close', now(), now() from access_resource ar where name = 'CYCLE_COUNT_CLOSE';

insert ignore into role_access_resource (role_id, access_resource_name, created) SELECT r.id, 'CYCLE_COUNT_CREATE', now() from role r where r.code = 'ADMIN';
insert ignore into role_access_resource (role_id, access_resource_name, created) SELECT r.id, 'CYCLE_COUNT_VIEW', now() from role r where r.code = 'ADMIN';
insert ignore into role_access_resource (role_id, access_resource_name, created) SELECT r.id, 'COUNT_SHELF', now() from role r where r.code = 'ADMIN';
insert ignore into role_access_resource (role_id, access_resource_name, created) SELECT r.id, 'CYCLE_COUNT_CLOSE', now() from role r where r.code = 'ADMIN';

insert ignore into facility_access_resource (facility_type, access_resource_name, created, enabled) VALUES ('WAREHOUSE', 'CYCLE_COUNT_CREATE', now(), 1);
insert ignore into facility_access_resource (facility_type, access_resource_name, created, enabled) VALUES ('WAREHOUSE', 'CYCLE_COUNT_VIEW', now(), 1);
insert ignore into facility_access_resource (facility_type, access_resource_name, created, enabled) VALUES ('WAREHOUSE', 'COUNT_SHELF', now(), 1);
insert ignore into facility_access_resource (facility_type, access_resource_name, created, enabled) VALUES ('WAREHOUSE', 'CYCLE_COUNT_CLOSE', now(), 1);

insert ignore into product_access_resource (product_code, access_resource_name, enabled, created, updated) VALUES ('ENTERPRISE', 'CYCLE_COUNT_CREATE', 1, now(), now());
insert ignore into product_access_resource (product_code, access_resource_name, enabled, created, updated) VALUES ('ENTERPRISE', 'CYCLE_COUNT_VIEW', 1, now(), now());
insert ignore into product_access_resource (product_code, access_resource_name, enabled, created, updated) VALUES ('ENTERPRISE', 'COUNT_SHELF', 1, now(), now());
insert ignore into product_access_resource (product_code, access_resource_name, enabled, created, updated) VALUES ('ENTERPRISE', 'CYCLE_COUNT_CLOSE', 1, now(), now());
-- done
-- sequence changes
insert ignore into sequence (tenant_id, facility_id, name, level, prefix, next_year_prefix, prefix_expression, reset_counter_next_year, created) SELECT t.id, f.id, 'CYCLE_COUNT', 'FACILITY', 'CC', NULL, NULL, 0, now() from tenant t, facility f;
insert ignore into sequence (tenant_id, facility_id, name, level, prefix, next_year_prefix, prefix_expression, reset_counter_next_year, created) SELECT t.id, f.id, 'SUB_CYCLE_COUNT', 'FACILITY', 'CC', NULL, NULL, 0, now() from tenant t, facility f;
-- done
-- todo: change in pickset too, but don't add type (it is always set to DEFAULT)
-- add shelf status code for cycle count
ALTER TABLE shelf ADD COLUMN status_code VARCHAR(45) DEFAULT NULL AFTER code;
UPDATE shelf SET status_code = 'ACTIVE';
-- done
-- environment properties
insert ignore into environment_property (name, value) VALUES ('automatic.sub.cycle.count.allowed', 'true');
insert ignore into environment_property (name, value) VALUES ('cycle.count.shelves.from.zones.in.parallel', 'false');
insert ignore into environment_property (name, value) VALUES ('sub.cycle.count.max.batch.size', '200');
insert ignore into environment_property (name, value) VALUES ('sub.cycle.count.min.batch.size', '50');
insert ignore into environment_property (name, value) VALUES ('cycle.count.allowed.start.hour', '0');
insert ignore into environment_property (name, value) VALUES ('cycle.count.allowed.end.hour', '0');
insert ignore into environment_property (name, value) VALUES ('max.shelf.blocked.hours.for.cycle.count', '2');
insert ignore into system_configuration (facility_id, tenant_id, name, display_name, value, type, access_resource_name, group_name, sequence, created)
  SELECT f.id, p.tenant_id, 'automatic.sub.cycle.count.allowed', 'Automatic sub cycle count creation?', 'true', 'checkbox', 'CYCLE_COUNT_VIEW', 'CYCLE COUNT', 10, now() from facility f join party p on f.id = p.id;
insert ignore into system_configuration (facility_id, tenant_id, name, display_name, value, type, access_resource_name, group_name, sequence, created)
  SELECT f.id, p.tenant_id, 'cycle.count.shelves.from.zones.in.parallel', 'Add shelves in parallel from zones(For automatic)', 'false', 'checkbox', 'CYCLE_COUNT_VIEW', 'CYCLE COUNT', 20, now() from facility f join party p on f.id = p.id;
insert ignore into system_configuration (facility_id, tenant_id, name, display_name, value, type, access_resource_name, group_name, sequence, created)
  SELECT f.id, p.tenant_id, 'sub.cycle.count.max.batch.size', 'Maximum batch size of sub cycle counts', '200', 'text', 'CYCLE_COUNT_VIEW', 'CYCLE COUNT', 30, now() from facility f join party p on f.id = p.id;
insert ignore into system_configuration (facility_id, tenant_id, name, display_name, value, type, access_resource_name, group_name, sequence, created)
  SELECT f.id, p.tenant_id, 'sub.cycle.count.min.batch.size', 'Minimum batch size of Sub Cycle Counts', '50', 'text', 'CYCLE_COUNT_VIEW', 'CYCLE COUNT', 40, now() from facility f join party p on f.id = p.id;
insert ignore into system_configuration (facility_id, tenant_id, name, display_name, value, type, access_resource_name, group_name, sequence, created)
  SELECT f.id, p.tenant_id, 'cycle.count.allowed.start.hour', 'Allowed start hour of cycle count', '0', 'text', 'CYCLE_COUNT_VIEW', 'CYCLE COUNT', 50, now() from facility f join party p on f.id = p.id;
insert ignore into system_configuration (facility_id, tenant_id, name, display_name, value, type, access_resource_name, group_name, sequence, created)
  SELECT f.id, p.tenant_id, 'automatic.sub.cycle.count.allowed', 'Automatic sub cycle count allowed', 'true', 'checkbox', 'CYCLE_COUNT_VIEW', 'CYCLE COUNT', 60, now() from facility f join party p on f.id = p.id;
insert ignore into system_configuration (facility_id, tenant_id, name, display_name, value, type, access_resource_name, group_name, sequence, created)
  SELECT f.id, p.tenant_id, 'cycle.count.allowed.end.hour', 'Allowed end hour of cycle count', '23', 'text', 'CYCLE_COUNT_VIEW', 'CYCLE COUNT', 70, now() from facility f join party p on f.id = p.id;
insert ignore into system_configuration (facility_id, tenant_id, name, display_name, value, type, access_resource_name, group_name, sequence, created)
  SELECT f.id, p.tenant_id, 'max.shelf.blocked.hours.for.cycle.count', 'Maximum hours for which a shelf can be blocked for cycle count', '2', 'text', 'CYCLE_COUNT_VIEW', 'CYCLE COUNT', 80, now() from facility f join party p on f.id = p.id;

-- datatables
-- cycle count
insert ignore INTO export_job_type (tenant_id, name, display_name, view_name, export_job_config, enabled, no_sql, access_resource_name, post_processor_script_name, created, updated, type)
SELECT t.id, 'DATATABLE_CYCLE_COUNT', 'Cycle Count', null, '<?xml version="1.0" encoding="UTF-8"?>
<table-config name="DATATABLE_CYCLE_COUNT" height="1400">
    <queryString>
        <![CDATA[from cycle_count cc left join sub_cycle_count scc on cc.id=scc.cycle_count_id left join cycle_count_shelf_summary ccss on ccss.sub_cycle_count_id=scc.id left join shelf s on ccss.shelf_id=s.id left join section ss on s.section_id=ss.id left join pick_set ps on ps.id = ss.pick_set_id where cc.facility_id= :facilityId ]]>
    </queryString>
    <columns>
        <column id="cycleCountCode" name="Cycle Count Code" value="cc.code" width="150" sortable="true" align="left">
            <clientTemplate><![CDATA[<a class="link" href="/subcyclecounts?cyclecountcode=<#=cycleCountCode#>"><#=cycleCountCode#></a>]]></clientTemplate>
        </column>
        <column id="startDate" name="Start Date" value="cc.created" width="110" sortable="true" align="left" type="date" />
        <column id="targetDate" name="Target Date" value="cc.target_completion" width="110" align="left" type="date"/>
        <column id="actualCommpletion" name="Actual Completion Date" value="cc.completed" width="150" align="left" type="date"/>
        <column id="totalShelfCount" name="Total Shelf Count" value="count(distinct s.id)" width="110" sortable="true" align="left"/>
        <column id="completedShelfCount" name="Completed Shelf Count" value="count(distinct s.id) - count(if(ccss.active =true , ccss.shelf_id,null))" width="140" align="left"/>
        <column id="percentageCompleted" name="Percentage Completed" value="(1- count(if(ccss.active =true , ccss.shelf_id,null))/count(distinct s.id))*100" width="130" align="left"/>
        <column id="nonBarcodedItemCount" name="Non Barcoded Items" value="sum(ccss.non_barcoded_item_count)" width="120" align="left"/>
        <column id="statusCode" name="Status" value="cc.status_code" width="110" sortable="true" align="left"/>
        <column id="result" name="Result" value="cc.code" width="80" sortable="true" align="left">
            <clientTemplate><![CDATA[<a target="_top" class="btn btn-mini" href="/cyclecount/results?cyclecountcode=<#=obj.cycleCountCode#>">Results</a>]]></clientTemplate>
        </column>
        <column id="performance" name="Performance" value="cc.code" width="110" sortable="true" align="left">
            <clientTemplate><![CDATA[<a target="_top" class="btn btn-mini" href="/cyclecount/performance?cyclecountcode=<#=obj.cycleCountCode#>">Performance</a>]]></clientTemplate>
        </column>
        <column id="edit" name="Edit" value="cc.code" width="110" sortable="true" align="left">
            <clientTemplate><![CDATA[<a target="_top" class="btn btn-mini edit-cycle-count" href="/cyclecount/edit?cyclecountcode=<#=obj.cycleCountCode#>">Edit</a>]]></clientTemplate>
        </column>
    </columns>
    <groupBy>
        group by cc.id
    </groupBy>
    <defaultSortBy>
        cc.created desc
    </defaultSortBy>
    <filters>
        <filter id="codeContains" name="Cycle Count Code Contains" condition="(cc.code like :codeContains)" type="text" valueExpression="#{\'%\' + #codeContains + \'%\'}" attachToColumn="cycleCountCode" />
    </filters>
</table-config>', 1, 0, 'CYCLE_COUNT_VIEW', null, now(), now(), 'TABLE' FROM tenant t;
-- sub cycle count
insert ignore INTO export_job_type (tenant_id, name, display_name, view_name, export_job_config, enabled, no_sql, access_resource_name, post_processor_script_name, created, updated, type)
SELECt t.id, 'DATATABLE_SUB_CYCLE_COUNT', 'Sub Cycle Count', null, '<?xml version="1.0" encoding="UTF-8"?>
<table-config name="DATATABLE_SUB_CYCLE_COUNT" height="1400">
    <queryString>
        <![CDATA[from sub_cycle_count scc join cycle_count cc on cc.id=scc.cycle_count_id left join cycle_count_shelf_summary ccss on ccss.sub_cycle_count_id=scc.id left join shelf s on ccss.shelf_id=s.id where cc.facility_id= :facilityId]]>
    </queryString>
    <columns>
        <column id="subCycleCountCode" name="Sub Cycle Count Code" value="scc.code" width="150" sortable="true" align="left">
            <clientTemplate><![CDATA[<a class="link" href="/subcyclecount/shelves?cyclecountcode=<#=cycleCountCode#>&amp;subcyclecountcode=<#=subCycleCountCode#>"><#=subCycleCountCode#></a>]]></clientTemplate>
        </column>
        <column id="startDate" name="Start Date" value="scc.created" width="110" sortable="true" align="left" type="date" />
        <column id="targetDate" name="Target Date" value="scc.target_completion" width="110" align="left" type="date"/>
        <column id="actualCommpletion" name="Actual Completion Date" value="scc.completed" width="150" align="left" type="date"/>
        <column id="totalShelfCount" name="Total Shelf Count" value="count(distinct s.id)" width="110" sortable="true" align="left"/>
        <column id="completedShelfCount" name="Completed Shelf Count" value="count(distinct s.id) - count(if(ccss.active =true , ccss.shelf_id,null))" width="140" align="left" sortable="true"/>
        <column id="percentageCompleted" name="Percentage Completed" value="(1- count(if(ccss.active =true , ccss.shelf_id,null))/count(distinct s.id))*100" width="130" align="left" sortable="true"/>
        <column id="statusCode" name="Status" value="scc.status_code" width="200" sortable="true" align="left"/>
        <column id="cycleCountCode" name="Cycle Count Code" value="cc.code" width="130" hidden="true"/>
    </columns>
    <groupBy>
        group by scc.id
    </groupBy>
    <defaultSortBy>
        cc.created desc
    </defaultSortBy>
    <filters>
        <filter id="codeContains" name="Code Contains" condition="(scc.code like :codeContains)" type="text" valueExpression="#{\'%\' + #codeContains + \'%\'}" attachToColumn="subCycleCountCode" />
        <filter id="cycleCountCodeFilter" name="Cycle Count Code Equals" condition="cc.code = :cycleCountCodeFilter" type="text" attachToColumn="cycleCountCode" />
    </filters>
</table-config>
', 1, 0, 'CYCLE_COUNT_VIEW', null, now(), now(), 'TABLE' FROM tenant t;
-- cycle count shelf
insert ignore INTO export_job_type (tenant_id, name, display_name, view_name, export_job_config, enabled, no_sql, access_resource_name, post_processor_script_name, created, updated, type)
SELECT t.id, 'DATATABLE_CYCLE_COUNT_SHELF_RESULTS', 'Shelf', null, '<?xml version="1.0" encoding="UTF-8"?>
<table-config name="DATATABLE_CYCLE_COUNT_SHELF_RESULTS" height="1400">
    <queryString>
        <![CDATA[from cycle_count_item cci join cycle_count_shelf_summary ccss on cci.cycle_count_shelf_summary_id=ccss.id join sub_cycle_count scc on ccss.sub_cycle_count_id=scc.id join cycle_count cc on scc.cycle_count_id=cc.id join shelf s on cci.shelf_id=s.id join vendor_item_type vit on cci.sku_code=vit.sku_code join party p on vit.vendor_id=p.id where cc.facility_id= :facilityId]]>
    </queryString>
    <columns>
        <column id="itemTypeName" name="Product Name" value="cci.item_type_name" width="150" sortable="true" align="left"/>
        <column id="sellerName" name="Seller" value="p.name" width="150" sortable="true" align="left" hidden="true"/>
        <column id="skuCode" name="SUPC" value="cci.sku_code" width="150" sortable="true" align="left"/>
        <column id="inventoryType" name="Inventory Type" value="cci.inventory_type" width="120" align="left"/>
        <column id="expectedInventory" name="Expected Inventory" value="cci.expected_inventory" width="100" sortable="true" align="left"/>
        <column id="inventoryFound" name="Inventory Found" value="cci.expected_inventory + cci.inventory_difference" width="100" sortable="true" align="left"/>
        <column id="extra" name="Extra" value="if(cci.inventory_difference&gt;0,inventory_difference,0)" width="100" sortable="true" align="left"/>
        <column id="reconciledExtra" name="Extra Reconciled" value="if(cci.inventory_difference&gt;0,inventory_difference - pending_reconciliation,0)" width="100" sortable="true" align="left"/>
        <column id="netExtra" name="Net Extra" value="if(cci.inventory_difference&gt;0,pending_reconciliation,0)" width="100" sortable="true" align="left"/>
        <column id="missing" name="Missing" value="if(cci.inventory_difference&lt;0,abs(cci.inventory_difference),0)" width="100" sortable="true" align="left"/>
        <column id="reconciledMissing" name="Missing Reconciled" value="if(cci.inventory_difference&lt;0,abs(cci.inventory_difference) - pending_reconciliation,0)" width="100" sortable="true" align="left"/>
        <column id="netMissing" name="Net Missing" value="if(cci.inventory_difference&lt;0,pending_reconciliation,0)" width="100" sortable="true" align="left"/>
        <column id="created" name="Created" value="cc.created" type="date" width="100" sortable="true" align="left"/>
        <column id="shelfCode" name="Shelf Code" value="s.code" width="100" align="left" hidden="true"/>
        <column id="subCycleCountCode" name="Sub Cycle Count Code" value="scc.code" width="100" align="left" hidden="true"/>
    </columns>
    <defaultSortBy>
        cci.created desc
    </defaultSortBy>
    <filters>
        <filter id="itemTypeNameFilter" name="Item Type Name like" condition="(cci.item_type_name like :itemTypeNameFilter)" type="text" valueExpression="#{\'%\' + #itemTypeNameFilter + \'%\'}" attachToColumn="itemTypeName" />
        <filter id="skuCodeFilter" name="SUPC equals" condition="(cci.sku_code = :skuCodeFilter)" type="text" attachToColumn="skuCode"/>
        <filter id="shelfCodeFilter" name="Shelf Code equals" condition="(s.code = :shelfCodeFilter)" type="text" attachToColumn="shelfCode"/>
        <filter id="subCycleCountCodeFilter" name="Sub Cycle Count Code equals" condition="(scc.code = :subCycleCountCodeFilter)" type="text" attachToColumn="subCycleCountCode"/>
    </filters>
</table-config>
', 1, 0, 'CYCLE_COUNT_VIEW', null, now(), now(), 'TABLE' FROM tenant t;
-- cycle count results
insert ignore INTO export_job_type (tenant_id, name, display_name, view_name, export_job_config, enabled, no_sql, access_resource_name, post_processor_script_name, created, updated, type)
  SELECT t.id, 'DATATABLE_CYCLE_COUNT_RESULTS', 'Cycle Count Results', null, '<?xml version="1.0" encoding="UTF-8"?>
<table-config name="DATATABLE_CYCLE_COUNT_RESULTS" height="1400" resultCountNotSupported="true">
    <queryString>
        <![CDATA[from cycle_count_item cci join cycle_count_shelf_summary ccss on cci.cycle_count_shelf_summary_id=ccss.id join user u on ccss.user_id=u.id join sub_cycle_count scc on ccss.sub_cycle_count_id=scc.id join cycle_count cc on scc.cycle_count_id=cc.id join shelf s on ccss.shelf_id=s.id join item_type it on it.sku_code=cci.sku_code join vendor_item_type vit on it.id=vit.item_type_id join party p on vit.vendor_id=p.id join vendor v on (p.id=v.id and it.seller_code=p.code) where vit.facility_id=cc.facility_id and  cc.facility_id= :facilityId]]>
    </queryString>
    <columns>
        <column id="discarded" name="Discarded" value="cci.discarded" width="100" align="left" type="boolean" hidden="true" exportable="true"/>
        <column id="shelfCode" name="Shelf Code" value="s.code" width="100" align="left"/>
        <column id="itemTypeName" name="Product Name" value="cci.item_type_name" width="150" sortable="true" align="left"/>
        <column id="sellerName" name="Seller" value="p.name" width="120" sortable="true" align="left" hidden="true"/>
        <column id="skuCode" name="Sku Code" value="cci.sku_code" width="150" sortable="true" align="left"/>
        <column id="inventoryType" name="Inventory Type" value="cci.inventory_type" width="120" align="left"/>
        <column id="expectedInventory" name="Expected Inventory" value="cci.expected_inventory" width="100" sortable="true" align="left"/>
        <column id="inventoryFound" name="Inventory Found" value="cci.expected_inventory + cci.inventory_difference" width="100" sortable="true" align="left"/>
        <column id="extra" name="Extra" value="if(cci.inventory_difference&gt;0,inventory_difference,0)" width="50" sortable="true" align="left"/>
        <column id="reconciledExtra" name="Extra Reconciled" value="if(cci.inventory_difference&gt;0,inventory_difference - pending_reconciliation,0)" width="100" sortable="true" align="left"/>
        <column id="netExtra" name="Net Extra" value="if(cci.inventory_difference&gt;0,pending_reconciliation,0)" width="60" sortable="true" align="left"/>
        <column id="missing" name="Missing" value="if(cci.inventory_difference&lt;0,abs(cci.inventory_difference),0)" width="60" sortable="true" align="left"/>
        <column id="reconciledMissing" name="Missing Reconciled" value="if(cci.inventory_difference&lt;0,abs(cci.inventory_difference) - pending_reconciliation,0)" width="100" sortable="true" align="left"/>
        <column id="netMissing" name="Net Missing" value="if(cci.inventory_difference&lt;0,pending_reconciliation,0)" width="80" sortable="true" align="left"/>
        <column id="username" name="Username" value="u.username" width="150" sortable="true" align="left"/>
        <column id="created" name="Created" value="cci.created" type="date" width="100" sortable="true" align="left"/>
        <column id="cycleCountCode" name="Cycle Count Code" hidden="true" value="cc.code" width="150" align="left"/>
        <column id="inventory_type" name="Inventory Type" value="cci.inventory_type" width="100" sortable="true" align="left"/>
    </columns>
    <defaultSortBy>
        cci.created desc
    </defaultSortBy>
    <filters>
        <filter id="itemTypeNameFilter" name="Item Type Name like" condition="(cci.item_type_name like :itemTypeNameFilter)" type="text" valueExpression="#{\'%\' + #itemTypeNameFilter + \'%\'}" attachToColumn="itemTypeName" />
        <filter id="skuCodeFilter" name="SUPC equals" condition="(cci.sku_code = :skuCodeFilter)" type="text" attachToColumn="skuCode"/>
        <filter id="shelfCodeFilter" name="Shelf Code equals" condition="(s.code = :shelfCodeFilter)" type="text" attachToColumn="shelfCode"/>
        <filter id="cycleCountCodeFilter" name="Cycle Count Code equals" condition="(cc.code = :cycleCountCodeFilter)" type="text" attachToColumn="cycleCountCode"/>
        <filter id="missingFilter" name="Missing Filter" condition="cci.inventory_difference&lt;0" type="boolean" attachToColumn="difference"/>
        <filter id="extraFilter" name="Extra Filter" condition="cci.inventory_difference&gt;0" type="boolean" attachToColumn="difference"/>
    </filters>
</table-config>
', 1, 0, 'CYCLE_COUNT_VIEW', null, now(), now(), 'TABLE' FROM tenant t;
-- cycle count error items
insert ignore INTO export_job_type (tenant_id, name, display_name, view_name, export_job_config, enabled, no_sql, access_resource_name, post_processor_script_name, created, updated, type)
  SELECT t.id, 'DATATABLE_CYCLE_COUNT_ERROR_ITEMS', 'Cycle Count Error Items', null, '<?xml version="1.0" encoding="UTF-8"?>
<table-config name="DATATABLE_CYCLE_COUNT_ERROR_ITEMS" height="1400" resultCountNotSupported="true" >
    <queryString>
        <![CDATA[from cycle_count_error_item ccei join shelf s on ccei.shelf_id=s.id join item i on ccei.item_code=i.code join sub_cycle_count scc on ccei.sub_cycle_count_id=scc.id join cycle_count cc on scc.cycle_count_id = cc.id where i.facility_id = cc.facility_id and cc.facility_id = :facilityId ]]>
    </queryString>
    <columns>
        <column id="shelfCode" name="Shelf Code" value="s.code" width="150" sortable="true" align="left"/>
        <column id="itemCode" name="Item Code" value="ccei.item_code" width="150" sortable="true" align="left"/>
        <column id="itemTypeName" name="Product Name" value="i.item_type_name" width="200" sortable="true" align="left" />
        <column id="skuCode" name="Sku Code" value="i.sku_code" sortable="true" width="110" align="left"/>
        <column id="reasonCode" name="Reason Code" value="ccei.reason_code" sortable="true" width="130" align="left"/>
        <column id="comment" name="Comment" value="ccei.comment" sortable="true" width="130" align="left"/>
        <column id="created" name="Scanned Time" value="ccei.created" sortable="true" width="110" align="left" type="date"/>
        <column id="cycleCountCode" name="Cycle Count Code" value="cc.code" width="150" hidden="true" align="left"/>
    </columns>
    <defaultSortBy>
        ccei.created desc
    </defaultSortBy>
    <filters>
        <filter id="cycleCountCodeFilter" name="Cycle Count Code Equals" condition="(cc.code = :cycleCountCodeFilter)" type="text" attachToColumn="cycleCountCode" />
    	<filter id="skuCodeFilter" name="SUPC equals" condition="(i.sku_code = :skuCodeFilter)" type="text" attachToColumn="skuCode"/>
        <filter id="itemCodeFilter" name="SUPC equals" condition="(ccei.item_code = :itemCodeFilter)" type="text" attachToColumn="itemCode"/>
    </filters>
</table-config>
', 1, 0, 'CYCLE_COUNT_VIEW', null, now(), now(), 'TABLE' FROM tenant t;
-- cycle count missing items
insert ignore INTO export_job_type (tenant_id, name, display_name, view_name, export_job_config, enabled, no_sql, access_resource_name, post_processor_script_name, created, updated, type)
  SELECT t.id, 'DATATABLE_CYCLE_COUNT_MISSING_ITEMS', 'Cycle Count Missing Items', null, '<?xml version="1.0" encoding="UTF-8"?>
<table-config name="DATATABLE_CYCLE_COUNT_MISSING_ITEMS" height="1400" resultCountNotSupported="true" >
    <queryString>
        <![CDATA[from item i join party p on i.vendor_id=p.id where i.shelf_id is null and i.status_code in (\'GOOD_INVENTORY\',\'BAD_INVENTORY\',\'QC_REJECTED\') and i.facility_id = :facilityId and p.facility_id= :facilityId]]>
    </queryString>
    <columns>
        <column id="itemCode" name="Item Code" value="i.code" width="150" sortable="true" align="left"/>
        <column id="inventoryType" name="Inventory Type" value="i.inventory_type" sortable="true" width="120" align="left"/>
        <column id="skuCode" name="Sku Code" value="i.sku_code" sortable="true" width="200" align="left"/>
        <column id="itemTypeName" name="Product Name" value="i.item_type_name" width="350" sortable="true" align="left" />
        <column id="sellerName" name="Seller" value="p.name" width="250" sortable="true" align="left"/>
        <column id="created" name="Created" value="i.created" hidden="true" sortable="true" width="110" align="left" type="date"/>
    </columns>
    <defaultSortBy>
        i.created desc
    </defaultSortBy>
    <filters>
        <filter id="skuCodeFilter" name="SUPC equals" condition="(i.sku_code = :skuCodeFilter)" type="text" attachToColumn="skuCode"/>
    </filters>
</table-config>
', 1, 0, 'CYCLE_COUNT_VIEW', null, now(), now(), 'TABLE' FROM tenant t;
-- sub cycle count shelves
insert ignore INTO export_job_type (tenant_id, name, display_name, view_name, export_job_config, enabled, no_sql, access_resource_name, post_processor_script_name, created, updated, type)
  SELECT t.id, 'DATATABLE_SUB_CYCLE_COUNT_SHELVES', 'Sub Cycle Count Shelves', null, '<?xml version="1.0" encoding="UTF-8"?>
<table-config name="DATATABLE_SUB_CYCLE_COUNT_SHELVES" data-source="SECONDARY" height="1400"  >
    <queryString>
        <![CDATA[from cycle_count_shelf_summary ccss join user u on ccss.user_id = u.id left join cycle_count_item cci on cci.cycle_count_shelf_summary_id=ccss.id join sub_cycle_count scc on ccss.sub_cycle_count_id=scc.id join cycle_count cc on cc.id=scc.cycle_count_id join shelf s on ccss.shelf_id=s.id join section ss on s.section_id=ss.id join pick_set ps on ss.pick_set_id=ps.id where cc.facility_id= :facilityId]]>
    </queryString>
    <columns>
        <column id="shelfCode" name="Shelf Code" value="s.code" width="150" sortable="true" align="left">
            <clientTemplate><![CDATA[<a class="link" href="/cyclecount/shelf/results?shelfCode=<#=shelfCode#>"><#=shelfCode#></a>]]></clientTemplate>
        </column>
        <column id="shelfId" name="ShelfId" value="s.id" width="150" hidden="true"/>
        <column id="pickSetName" name="Zone Name" value="ps.name" width="150" sortable="true" align="left"/>
        <column id="putawayPendingCount" name="Putaway Pending Count" calculated="true" value="query:select count(distinct p.id) from putaway p join putaway_item pi on pi.putaway_id=p.id where pi.shelf_id= :shelfId and p.status_code not in (\'DISCARDED\',\'COMPLETE\')" width="140" align="left">
            <clientTemplate><![CDATA[<a data-shelfcode="<#=shelfCode#>" class="link putawayPendingCountShelf"><#=putawayPendingCount#></a>]]></clientTemplate>
        </column>

        <column id="pickupPending" name="Pickup Pending" calculated="true" value="query:select count(pl.id) from picklist pl join pick_bucket_slot pbs on pbs.picklist_id = pl.id join pick_bucket_slot_item pbsi on pbsi.pick_bucket_slot_id = pbs.id join sale_order_item soi on soi.id = pbsi.sale_order_item_id join item_type_inventory iti on soi.item_type_inventory_id = iti.id join shelf s on iti.shelf_id = s.id join shipping_package sp on (sp.code = pbs.shipping_package_id and sp.facility_id = pl.facility_id) where s.id = :shelfId and (sp.status_code in (\'PICKING\', \'PICKED\') OR pbsi.status_code = \'PUTBACK_PENDING\' AND pl.status_code!=\'CLOSED\')"  width="200" align=" LEFT ">
          <clientTemplate><![CDATA[<a data-shelfcode="<#=shelfCode#>" class="link picklistPendingCountShelf"><#=pickupPending#></a>]]></clientTemplate>
        </column>
        <column id="expectedInventory" name="Expected Inventory" value="ifnull(sum(cci.expected_inventory),0)" width="140" align="left" />
        <column id="totalFound" name="Total Found" value="ifnull(sum(cci.expected_inventory) + sum(cci.inventory_difference),0)" width="150" sortable="true" align="left"/>
        <column id="totalExtra" name="Total Extra Inventory" value="sum(if(cci.inventory_difference &gt; 0,cci.inventory_difference,0))" width="150" sortable="true" align="left"/>
        <column id="totalMissing" name="Total Missing Inventory" value="abs(sum(if(cci.inventory_difference &lt; 0,cci.inventory_difference,0)))" width="150" sortable="true" align="left"/>
        <column id="foundBarcodes" name="Barcodes" value="ccss.item_codes" width="250" sortable="true" align="left"/>
        <column id="woBarcode" name="W/O Barcode" value="ccss.non_barcoded_item_count" width="150" hidden="true"/>
        <column id="updated" name="Updated" value="ccss.updated" width="110" sortable="true" align="left" type="date" />
        <column id="username" name="Username" value="u.username" width="150" sortable="true" align="left"/>
        <column id="statusCode" name="Status" value="s.status_code" width="200" sortable="true" align="left"/>
        <column id="statusCodeForFilter" name="Status" value="s.status_code" width="200" sortable="true" align="left" hidden="true"/>
        <column id="subCycleCountCode" name="Sub Cycle Count Code" value="scc.code" width="130" hidden="true"/>
        <column id="active" name="Count Active" value="ccss.active" width="100" hidden="true"/>
    </columns>
    <groupBy>
        group by ccss.id
    </groupBy>
    <defaultSortBy>
        cc.created desc
    </defaultSortBy>
    <filters>
        <filter id="codeContains" name="Shelf Code Equals" condition="(cc.code like :codeContains)" type="text" valueExpression="#{\'%\' + #codeContains + \'%\'}" attachToColumn="subCycleCountCode" />
        <filter id="subCycleCountCodeFilter" name="Sub Cycle Count Code Equals" condition="scc.code = :subCycleCountCodeFilter" type="text" attachToColumn="subCycleCountCode" />
        <filter id="statusCodeFilter" name="Shelf Status in" condition="s.status_code in (:statusCodeFilter)" type="multiselect" values="enum:UNBLOCKED|QUEUED_FOR_COUNT,BLOCKED|COUNT_REQUESTED,READY|READY_FOR_COUNT,IN PROGRESS|COUNT_IN_PROGRESS" attachToColumn="statusCodeForFilter" />
        <filter id="completedFilter" name="Shelf Completed" condition="ccss.active != :completedFilter" type="boolean" attachToColumn="active" />
    </filters>
</table-config>
', 1, 0, 'CYCLE_COUNT_VIEW', null, now(), now(), 'TABLE' FROM tenant t;
-- export cycle count report
insert ignore INTO export_job_type (tenant_id, name, display_name, view_name, export_job_config, enabled, no_sql, access_resource_name, post_processor_script_name, created, updated, type)
  SELECT t.id, 'CYCLE_COUNT_REPORT', 'Cycle Count Report', null, '<?xml version="1.0" encoding="UTF-8"?>
<export-config>
    <queryString>
        <![CDATA[from cycle_count c left join sub_cycle_count scc on scc.cycle_count_id = c.id left join cycle_count_shelf_summary ccss on ccss.sub_cycle_count_id = scc.id left join shelf s on s.id = ccss.shelf_id left join user u on ccss.user_id = u.id where c.facility_id = :facilityId and c.status_code = \'CREATED\']]>
    </queryString>
    <columns>
        <column id="cyclecountcode" name="Cycle Count Code" value="c.code" />
        <column id="subcyclecountcode" name="Sub Cycle Count Code" value="scc.code" />
        <column id="shelfcode" name="Shelf Code" value="s.code" />
        <column id="shelfstatus" name="Shelf Code Status" value="s.status_code" />
        <column id="itemcodes" name="Item Codes" value="ccss.item_codes" />
        <column id="updated" name="Updated" value="c.updated" />
        <column id="Subcyleupdated" name="Shelf updated" value="case when ccss.active = 0 then ccss.updated else \'\' end " />
        <column id="user" name="User" value="u.username" />
    </columns>
    <filters>
        <filter id="subcycle" name="Sub Cycle Count Code" condition="scc.code= :subcycle" type="text" />
    </filters>
</export-config>
', 1, 0, 'EXPORT_INVENTORY', null, now(), now(), 'EXPORT' FROM tenant t;
-- Cycle Count Overall Data
insert ignore INTO export_job_type (tenant_id, name, display_name, view_name, export_job_config, enabled, no_sql, access_resource_name, post_processor_script_name, created, updated, type)
  SELECT t.id, 'Cycle Count Overall Data', 'Cycle Count Overall Data', null, '<?xml version="1.0" encoding="UTF-8"?>
<export-config resultCountNotSupported="true">
    <queryString>
        <![CDATA[from cycle_count_item cci join cycle_count_shelf_summary ccss on cci.cycle_count_shelf_summary_id=ccss.id join user u on ccss.user_id=u.id join sub_cycle_count scc on ccss.sub_cycle_count_id=scc.id join cycle_count cc on scc.cycle_count_id=cc.id join shelf s on ccss.shelf_id=s.id join item_type it on it.sku_code=cci.sku_code join vendor_item_type vit on it.id=vit.item_type_id join party p on vit.vendor_id=p.id join vendor v on (p.id=v.id and it.seller_code=p.code) where (cci.expected_inventory >0 or cci.inventory_difference >0) and (vit.facility_id=cc.facility_id and cc.facility_id= :facilityId)]]>
    </queryString>
    <columns>
        <column id="discarded" name="Discarded" value="cci.discarded" />
        <column id="shelfCode" name="Shelf Code" value="s.code" />
        <column id="itemTypeName" name="Product Name" value="cci.item_type_name" />
        <column id="sellerName" name="Seller" value="p.name" />
        <column id="skuCode" name="Sku Code" value="cci.sku_code" />
        <column id="expectedInventory" name="Expected Inventory" value="cci.expected_inventory" />
        <column id="inventoryFound" name="Inventory Found" value="cci.expected_inventory + cci.inventory_difference" />
        <column id="extra" name="Extra" value="if(cci.inventory_difference&gt;0,inventory_difference,0)" />
        <column id="reconciledExtra" name="Extra Reconciled" value="if(cci.inventory_difference&gt;0,inventory_difference - pending_reconciliation,0)" />
        <column id="netExtra" name="Net Extra" value="if(cci.inventory_difference&gt;0,pending_reconciliation,0)" />
        <column id="missing" name="Missing" value="if(cci.inventory_difference&lt;0,abs(cci.inventory_difference),0)" />
        <column id="reconciledMissing" name="Missing Reconciled" value="if(cci.inventory_difference&lt;0,abs(cci.inventory_difference) - pending_reconciliation,0)" />
        <column id="netMissing" name="Net Missing" value="if(cci.inventory_difference&lt;0,pending_reconciliation,0)" />
        <column id="username" name="Username" value="u.username" />
        <column id="created" name="Created" value="cci.created" />
    </columns>
    <filters>
        <filter id="itemTypeNameFilter" name="Item Type Name like" condition="(cci.item_type_name like :itemTypeNameFilter)" type="text" valueExpression="#{''%'' + #itemTypeNameFilter + ''%''}"  />
        <filter id="skuCodeFilter" name="SUPC equals" condition="(cci.sku_code = :skuCodeFilter)" type="text" />
        <filter id="shelfCodeFilter" name="Shelf Code equals" condition="(s.code = :shelfCodeFilter)" type="text" />
        <filter id="cycleCountCodeFilter" name="Cycle Count Code equals" condition="(cc.code = :cycleCountCodeFilter)" type="text" required = "true"/>
        <filter id="missingFilter" name="Missing Filter" condition="cci.inventory_difference&lt;0" type="boolean" />
        <filter id="extraFilter" name="Extra Filter" condition="cci.inventory_difference&gt;0" type="boolean" />
     </filters>
</export-config>', 1, 0, 'EXPORT_INVENTORY', null, now(), now(), 'EXPORT' FROM tenant t;
-- user_datatable_view entries
insert IGNORE into user_datatable_view (tenant_id, user_id, datatable_type, name, code, config_json, display_order, system_view, created, updated, access_resource_name)
  SELECT t.id, NULL, "DATATABLE_CYCLE_COUNT", "ALL", "ALL", '{"resultsPerPage":25,"columns":[],"filters":[],"sortColumns":[{"column":"startDate","descending":true}]}', 5, 0, now(), now(), 'CYCLE_COUNT_VIEW' from tenant t;
insert IGNORE into user_datatable_view (tenant_id, user_id, datatable_type, name, code, config_json, display_order, system_view, created, updated, access_resource_name)
  SELECT t.id, NULL, "DATATABLE_SUB_CYCLE_COUNT", "ALL", "ALL", '{"resultsPerPage":25,"columns":[],"filters":[],"sortColumns":[{"column":"startDate","descending":true}]}', 5, 0, now(), now(), 'CYCLE_COUNT_VIEW' from tenant t;
insert IGNORE into user_datatable_view (tenant_id, user_id, datatable_type, name, code, config_json, display_order, system_view, created, updated, access_resource_name)
  SELECT t.id, NULL, "DATATABLE_CYCLE_COUNT_SHELF_RESULTS", "ALL", "ALL", '{"resultsPerPage":25,"columns":[],"filters":[],"sortColumns":[{"column":"startDate","descending":true}]}', 5, 0, now(), now(), 'CYCLE_COUNT_VIEW' from tenant t;
insert IGNORE into user_datatable_view (tenant_id, user_id, datatable_type, name, code, config_json, display_order, system_view, created, updated, access_resource_name)
  SELECT t.id, NULL, "DATATABLE_CYCLE_COUNT_RESULTS", "ALL", "ALL", '{"resultsPerPage":25,"columns":[],"filters":[],"sortColumns":[{"column":"startDate","descending":true}]}', 5, 0, now(), now(), 'CYCLE_COUNT_VIEW' from tenant t;
insert IGNORE into user_datatable_view (tenant_id, user_id, datatable_type, name, code, config_json, display_order, system_view, created, updated, access_resource_name)
  SELECT t.id, NULL, "DATATABLE_CYCLE_COUNT_ERROR_ITEMS", "Error Items", "ERROR_ITEMS", '{"resultsPerPage":25,"columns":[],"filters":[],"sortColumns":[{"column":"created","descending":true}]}', 5, 0, now(), now(), 'CYCLE_COUNT_VIEW' from tenant t;
insert IGNORE into user_datatable_view (tenant_id, user_id, datatable_type, name, code, config_json, display_order, system_view, created, updated, access_resource_name)
SELECT t.id, NULL, "DATATABLE_CYCLE_COUNT_MISSING_ITEMS", "ALL", "ALL", '{"resultsPerPage":25,"columns":[],"filters":[],"sortColumns":[{"column":"created","descending":true}]}', 5, 0, now(), now(), 'CYCLE_COUNT_VIEW' from tenant t;
insert IGNORE into user_datatable_view (tenant_id, user_id, datatable_type, name, code, config_json, display_order, system_view, created, updated, access_resource_name)
  SELECT t.id, NULL, "DATATABLE_SUB_CYCLE_COUNT_SHELVES", "ALL", "ALL", '{"resultsPerPage":25,"columns":[],"filters":[],"sortColumns":[{"column":"startDate","descending":true}]}', 5, 0, now(), now(), 'CYCLE_COUNT_VIEW' from tenant t;
insert IGNORE into user_datatable_view (tenant_id, user_id, datatable_type, name, code, config_json, display_order, system_view, created, updated, access_resource_name)
  SELECT t.id, NULL, "CYCLE_COUNT_REPORT", "ALL", "ALL", '{"resultsPerPage":25,"columns":[],"filters":[],"sortColumns":[{"column":"startDate","descending":true}]}', 5, 0, now(), now(), 'CYCLE_COUNT_VIEW' from tenant t;


-- adding ageing to item, item_type and manufacturing
ALTER TABLE item ADD COLUMN manufacturing_date DATE DEFAULT NULL after item_details, ADD COLUMN ageing_start_date DATE DEFAULT NULL AFTER manufacturing_date, ADD COLUMN bad_inventory_marked_time DATETIME DEFAULT NULL AFTER ageing_start_date;
UPDATE item SET ageing_start_date = manufacturing_date, updated = updated WHERE manufacturing_date IS NOT NULL;
UPDATE item i, inflow_receipt_item iri SET i.ageing_start_date = DATE(iri.created), i.updated = i.updated WHERE i.inflow_receipt_item_id = iri.id AND i.ageing_start_date IS NULL;
UPDATE item i, putaway p SET i.ageing_start_date = DATE(p.created), i.updated = i.updated WHERE i.putaway_id = p.id AND i.status_code = "PUTAWAY_PENDING";
-- done
-- changes in inflow_receipt_item
ALTER TABLE inflow_receipt_item ADD COLUMN manufacturing_date DATE DEFAULT NULL AFTER batch_code;
-- done
-- column additions in category
ALTER TABLE category ADD COLUMN expirable tinyint(1) DEFAULT 0, ADD COLUMN shelf_life int(10) unsigned DEFAULT 0, ADD COLUMN grn_expiry_tolerance int(10) NOT NULL DEFAULT 75, ADD COLUMN dispatch_expiry_tolerance int(10) NOT NULL DEFAULT 30, ADD COLUMN return_expiry_tolerance int(10) NOT NULL DEFAULT 30;
-- done

-- column additions in item type
ALTER TABLE item_type ADD COLUMN expirable tinyint(1) DEFAULT NULL, ADD COLUMN shelf_life int(10) unsigned default 0 AFTER expirable;
--  -- drop sku_code and tenant_id constraint, so that we can add a new constraint with ageing included
-- select a.constraint_name from (SELECT constraint_name from information_schema.table_constraints where constraint_schema = 'uniware' and table_name = 'item_type' and constraint_type='UNIQUE') a where a.constraint_name rlike "sku_code";
-- set @runQuery = concat('ALTER TABLE item_type_inventory DROP INDEX ', @constraintName);
-- PREPARE myquery from @runQuery;
-- EXECUTE myquery;
--   -- done
-- -- done
-- modifications to putaway_item
ALTER TABLE putaway_item ADD COLUMN ageing_start_date DATE NOT NULL AFTER `type`;
UPDATE putaway_item pi, putaway p SET pi.ageing_start_date = DATE(pi.updated), pi.updated = pi.updated WHERE pi.putaway_id = p.id;
-- done
-- changes in item_type_inventory
ALTER TABLE item_type_inventory ADD COLUMN ageing_start_date DATE NOT NULL AFTER `type`;
-- drop facility_id constraint on item_type_inventory, so that we can add a new constraint with ageing included
SELECT constraint_name from information_schema.table_constraints where constraint_schema = 'uniware' and table_name = 'item_type_inventory' and constraint_type='UNIQUE' INTO @constraintName;
set @runQuery = concat('ALTER TABLE item_type_inventory DROP INDEX ', @constraintName);
PREPARE myquery from @runQuery;
EXECUTE myquery;
-- done

update item_type_inventory iti join system_configuration sc on (iti.facility_id = sc.facility_id and sc.name = "traceability.level") set iti.ageing_start_date = "1970-01-01", iti.updated = iti.updated where sc.value in ("ITEM_SKU", "NONE");
UPDATE item_type_inventory SET ageing_start_date = DATE(created), updated = updated where ageing_start_date is null;
-- done
-- shelf changes
ALTER TABLE shelf ADD COLUMN facility_id INT(10) NOT NULL AFTER section_id;
update shelf s, shelf_type st set s.facility_id = st.facility_id where s.shelf_type_id = st.id;
ALTER TABLE shelf ADD CONSTRAINT `facility_code_unique` UNIQUE (facility_id, code);
-- done

-- additional data table view
update user_datatable_view set system_view = 1 where datatable_type = "DATATABLE_SUB_CYCLE_COUNT_SHELVES" and code = "ALL";
update user_datatable_view set system_view = 1 where datatable_type = "DATATABLE_SUB_CYCLE_COUNT";
DELETE from user_datatable_view WHERE datatable_type = "DATATABLE_SUB_CYCLE_COUNT_SHELVES" AND code = "ALL";
insert IGNORE into user_datatable_view (tenant_id, user_id, datatable_type, name, code, config_json, display_order, system_view, created, updated, access_resource_name)
  SELECT t.id, NULL, "DATATABLE_SUB_CYCLE_COUNT_SHELVES", "Unblocked", "UNBLOCKED", '{"resultsPerPage":25,"columns":[],"filters":[{"id":"completedFilter","checked":false},{"id":"statusCodeFilter","selectedValues":["QUEUED_FOR_COUNT"]}],"sortColumns":[{"column":"updated","descending":true}]}', 10, 1, now(), now(), 'CYCLE_COUNT_VIEW' from tenant t;
insert IGNORE into user_datatable_view (tenant_id, user_id, datatable_type, name, code, config_json, display_order, system_view, created, updated, access_resource_name)
  SELECT t.id, NULL, "DATATABLE_SUB_CYCLE_COUNT_SHELVES", "Blocked", "BLOCKED", '{"resultsPerPage":25,"columns":[],"filters":[{"id":"completedFilter","checked":false},{"id":"statusCodeFilter","selectedValues":["COUNT_REQUESTED"]}],"sortColumns":[{"column":"updated","descending":true}]}', 20, 1, now(), now(), 'CYCLE_COUNT_VIEW' from tenant t;
insert IGNORE into user_datatable_view (tenant_id, user_id, datatable_type, name, code, config_json, display_order, system_view, created, updated, access_resource_name)
  SELECT t.id, NULL, "DATATABLE_SUB_CYCLE_COUNT_SHELVES", "Ready", "READY", '{"resultsPerPage":25,"columns":[],"filters":[{"id":"completedFilter","checked":false},{"id":"statusCodeFilter","selectedValues":["READY_FOR_COUNT"]}],"sortColumns":[{"column":"updated","descending":true}]}', 30, 1, now(), now(), 'CYCLE_COUNT_VIEW' from tenant t;
insert IGNORE into user_datatable_view (tenant_id, user_id, datatable_type, name, code, config_json, display_order, system_view, created, updated, access_resource_name)
  SELECT t.id, NULL, "DATATABLE_SUB_CYCLE_COUNT_SHELVES", "In Progress", "IN_PROGRESS", '{"resultsPerPage":25,"columns":[],"filters":[{"id":"completedFilter","checked":false},{"id":"statusCodeFilter","selectedValues":["COUNT_IN_PROGRESS"]}],"sortColumns":[{"column":"updated","descending":true}]}', 40, 1, now(), now(), 'CYCLE_COUNT_VIEW' from tenant t;
insert IGNORE into user_datatable_view (tenant_id, user_id, datatable_type, name, code, config_json, display_order, system_view, created, updated, access_resource_name)
  SELECT t.id, NULL, "DATATABLE_SUB_CYCLE_COUNT_SHELVES", "Completed", "COMPLETED", '{"resultsPerPage":25,"columns":[],"filters":[{"id":"completedFilter","checked":true}],"sortColumns":[{"column":"updated","descending":true}]}', 50, 1, now(), now(), 'CYCLE_COUNT_VIEW' from tenant t;
-- alter table cycle_count_error_item, add reason_code
ALTER TABLE cycle_count_error_item CHANGE item_status_code reason_code varchar(45);
-- done
-- alter table inflow_receipt_item, don't know how this was not written earlier.
alter table inflow_receipt_item add column `integrated_gst` decimal(12,2) unsigned NOT NULL DEFAULT '0.00', add column `union_territory_gst` decimal(12,2) unsigned NOT NULL DEFAULT '0.00', add column `state_gst` decimal(12,2) unsigned NOT NULL DEFAULT '0.00', add column `central_gst` decimal(12,2) unsigned NOT NULL DEFAULT '0.00', add column `compensation_cess` decimal(12,2) unsigned NOT NULL DEFAULT '0.00';
-- done

--
ALTER TABLE putaway_item add completed_by int(10) unsigned default NULL after status_code, ADD CONSTRAINT fk_putaway_item_completed_by1 FOREIGN KEY(completed_by) REFERENCES user(id) ON DELETE CASCADE ON UPDATE CASCADE;
-- done
--
-- alter table item MODIFY last_cycle_count_id int(11) NULL;
update  user_datatable_view set system_view=1 where user_datatable_view.datatable_type ="DATATABLE_CYCLE_COUNT" and name = "ALL";
update  user_datatable_view set system_view=1 where user_datatable_view.datatable_type ="DATATABLE_CYCLE_COUNT_SHELF_RESULTS" and name = "ALL";
update  user_datatable_view set system_view=1 where user_datatable_view.datatable_type ="DATATABLE_CYCLE_COUNT_RESULTS" and name = "ALL";
update  user_datatable_view set system_view=1 where user_datatable_view.datatable_type ="DATATABLE_CYCLE_COUNT_MISSING_ITEMS" and name = "ALL";
delete from sequence where name in ("CYCLE_COUNT", "SUB_CYCLE_COUNT");
insert ignore into sequence (tenant_id, facility_id, name, level, prefix, next_year_prefix, prefix_expression, reset_counter_next_year, created) SELECT t.id, f.id, 'CYCLE_COUNT', 'FACILITY', 'CC', NULL, NULL, 0, now() from tenant t, facility f, party p where t.id=p.tenant_id and p.id=f.id;
insert ignore into sequence (tenant_id, facility_id, name, level, prefix, next_year_prefix, prefix_expression, reset_counter_next_year, created) SELECT t.id, f.id, 'SUB_CYCLE_COUNT', 'FACILITY', 'SCC', NULL, NULL, 0, now() from tenant t, facility f, party p where t.id=p.tenant_id and p.id=f.id;

-- Aditya
update export_job_type set export_job_config = '<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<table-config name=\"DATATABLE SEARCH PUTAWAY\" height=\"1400\">\n    <queryString>\n        <![CDATA[from putaway p join user u on u.id = p.user_id left join putaway_item pi on pi.putaway_id = p.id left join shelf s on s.id = pi.shelf_id left join item_type it on pi.item_type_id = it.id where p.facility_id = :facilityId]]>\n    </queryString>\n    <columns>\n        <column id=\"code\" name=\"Putaway Number\" value=\"p.code\" width=\"140\" sortable=\"true\" align=\"left\">\n            <clientTemplate><![CDATA[<a class=\"link\" href=\"/putaway/createPutaway?code=<#=obj.code#>\"><#=obj.code#></a>]]></clientTemplate>\n        </column>\n        <column id=\"type\" name=\"Type\" value=\"p.type\" width=\"220\" sortable=\"true\" align=\"left\" />\n        <column id=\"username\" name=\"Generated By\" value=\"u.username\" width=\"180\" sortable=\"true\" align=\"left\" />\n        <column id=\"shelfCode\" name=\"Shelf Code\" value=\"s.code\" width=\"180\" sortable=\"true\" align=\"left\" hidden=\"true\"/>\n        <column id=\"skuCode\" name=\"Sku Code\" value=\"it.sku_code\" width=\"130\" sortable=\"true\" align=\"left\" />\n        <column id=\"pendingQuantity\" name=\"Pending Qty\" value=\"SUM(pi.quantity - pi.putaway_quantity)\" width=\"80\" sortable=\"true\" align=\"left\"/>\n        <column id=\"quantity\" name=\"Qty\" value=\"SUM(pi.quantity)\" width=\"80\" sortable=\"true\" align=\"left\" />\n        <column id=\"statusCode\" name=\"Status\" value=\"p.status_code\" width=\"180\" sortable=\"true\" align=\"left\" />\n        <column id=\"created\" name=\"Created At\" value=\"p.created\" width=\"150\" sortable=\"true\" align=\"left\" type=\"date\" />\n        <column id=\"updated\" name=\"Updated At\" value=\"p.updated\" width=\"150\" sortable=\"true\" align=\"left\" type=\"date\" />\n    </columns>\n    <filters>\n        <filter id=\"codeFilter\" name=\"Code Contains\" condition=\"p.code like :codeFilter\" type=\"text\" valueExpression=\"#{\'%\' + #codeFilter + \'%\'}\" attachToColumn=\"code\" />\n        <filter id=\"itemSkuFilter\" name=\"Item sellerSkuCode Code/Name contains\" condition=\"(it.sku_code like :itemSkuFilter or it.name like :itemSkuFilter)\" type=\"text\" valueExpression=\"#{\'%\' + #itemSkuFilter + \'%\'}\" attachToColumn=\"quantity\" />\n        <filter id=\"usernameFilter\" name=\"Username Contains\" condition=\"u.username like :usernameFilter\" type=\"text\" valueExpression=\"#{\'%\' + #usernameFilter + \'%\'}\" attachToColumn=\"username\" />\n        <filter id=\"typeFilter\" name=\"Type\" condition=\"p.type in (:typeFilter)\" type=\"multiselect\" values=\"enum:Cancelled|PUTAWAY_CANCELLED_ITEM,GRN|PUTAWAY_GRN_ITEM,Returned|PUTAWAY_COURIER_RETURNED_ITEM,Reverse Pickiup|PUTAWAY_REVERSE_PICKUP_ITEM,Gatepass|PUTAWAY_GATEPASS_ITEM,Inspected Not Bad|PUTAWAY_INSPECTED_NOT_BAD_ITEM,Transfer|PUTAWAY_SHELF_TRANSFER,Direct Returns|PUTAWAY_RECEIVED_RETURNS\" attachToColumn=\"type\" />\n        <filter id=\"statusFilter\" name=\"Status\" condition=\"p.status_code in (:statusFilter)\" type=\"multiselect\" values=\"query:select code,description from putaway_status\" attachToColumn=\"statusCode\" />\n        <filter id=\"dateRangeFilter\" name=\"Created in Date Range\" condition=\"p.created &gt; :dateRangeFilterStart and p.created &lt; :dateRangeFilterEnd\" type=\"daterange\" attachToColumn=\"created\" />\n        <filter id=\"productCodeFilter\" name=\"Sku Code Contains\" condition=\"it.sku_code like :productCodeFilter\" type=\"text\" valueExpression=\"#{\'%\' + #productCodeFilter + \'%\'}\" attachToColumn=\"skuCode\" />\n    </filters>\n    <groupBy>\n        group by p.id\n    </groupBy>\n</table-config>\n' where name = "DATATABLE SEARCH PUTAWAY";
-- done
-- Aditya
INSERT IGNORE INTO system_configuration (facility_id, tenant_id, name, display_name, value, type, access_resource_name, group_name, sequence, created)
  SELECT
    f.id,
    p.tenant_id,
    'allow.multiple.ageing.date.on.shelf',
    'Allow multiple ageing dates on a single shelf',
    'true',
    'checkbox',
    'MINIMAL',
    'SYSTEM',
    90,
    now()
  FROM facility f
    JOIN party p ON f.id = p.id;
-- done
-- Aditya
insert ignore INTO export_job_type (tenant_id, name, display_name, view_name, export_job_config, enabled, no_sql, access_resource_name, post_processor_script_name, created, updated, type)
  SELECT t.id, 'DATATABLE_SEARCH_SHELF_PICKLIST', 'Shelf Picklist', null, '<?xml version="1.0" encoding="UTF-8"?>
<table-config name="DATATABLE_SEARCH_SHELF_PICKLIST" height="1400">
    <!--Needed only cycle count to find pending picklists on a shelf -->
    <queryString>
        <![CDATA[from picklist p join user u on p.user_id = u.id join pick_bucket_slot pbs on pbs.picklist_id=p.id join shipping_package sp on pbs.shipping_package_id=sp.id join sale_order_item soi on soi.shipping_package_id=sp.id join item_type_inventory iti on soi.item_type_inventory_id=iti.id join shelf s on iti.shelf_id=s.id where (sp.status_code in (\'PICKING\', \'PICKED\')) and p.facility_id = :facilityId]]>
    </queryString>
    <columns>
        <column id="picklistCode" name="Picklist Code" value="p.code" width="200" sortable="true" align="left" >
            <clientTemplate><![CDATA[<a href="/picklists/view?picklistCode=<#=obj.picklistCode#>"><#=obj.picklistCode#></a>]]></clientTemplate>
        </column>
        <column id="generatedBy" name="Generated By" value="u.username" width="220" sortable="true" align="left" />
        <column id="shelfCode" name="Shelf Code" value="s.code" width="200" sortable="true" align="left"/>
        <column id="status" name="Status" value="p.status_code" width="200" sortable="true" align="left"/>
        <column id="created" name="Created" value="p.created" width="150" sortable="true" align="left" type="date"/>
        <column id="operations" name="Operations" value="p.id" width="180" sortable="true" align="left" >
            <clientTemplate><![CDATA[<a class="uni-btn-link pleaseOpenInNewTab icon icon_print print" title="Print" id="printPicklist-<#=obj.picklistCode#>" href="/oms/picker/picklist/show/<#=obj.picklistCode#>?legacy=1" target="_blank"></a>]]></clientTemplate>
        </column>
    </columns>
    <defaultSortBy>
        p.created desc
    </defaultSortBy>
    <groupBy>
        group by p.id
    </groupBy>
    <filters>
        <filter id="picklistCodeFilter" name="Picklist Code Contains" condition="p.code like :picklistCodeFilter" type="text" valueExpression="#{\'\'%\'\' + #picklistCodeFilter + \'\'%\'\'}" attachToColumn="picklistCode"/>
        <filter id="statusFilter" name="Status" condition="p.status_code in (:statusFilter)" type="multiselect" values="enum:Created|CREATED,Received|RECEIVED" attachToColumn="status"/>
        <filter id="dateRangeFilter" name="Created in Date Range" condition="p.created &gt; :dateRangeFilterStart and p.created &lt; :dateRangeFilterEnd" type="daterange" attachToColumn="created"/>
        <filter id="shelfCodeFilter" name="Shelf Code Equals" condition="s.code = :shelfCodeFilter" type="text" attachToColumn="shelfCode"/>
    </filters>
</table-config>
', 1, 0, 'CYCLE_COUNT_VIEW', null, now(), now(), 'EXPORT' FROM tenant t;
insert IGNORE into user_datatable_view (tenant_id, user_id, datatable_type, name, code, config_json, display_order, system_view, created, updated, access_resource_name)
  SELECT t.id, NULL, "DATATABLE_SEARCH_SHELF_PICKLIST", "All", "ALL", '{"resultsPerPage":25,"columns":[],"filters":[],"sortColumns":[{"column":"updated","descending":true}]}', 10, 1, now(), now(), 'CYCLE_COUNT_VIEW' from tenant t;
update export_job_type set export_job_config = '<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<table-config name=\"DATATABLE SEARCH PUTAWAY\" height=\"1400\">\n    <queryString>\n        <![CDATA[from putaway p join user u on u.id = p.user_id left join putaway_item pi on pi.putaway_id = p.id left join item_type it on pi.item_type_id = it.id join shelf s on s.id = pi.shelf_id where p.facility_id = :facilityId]]>\n    </queryString>\n    <columns>\n        <column id=\"code\" name=\"Putaway Number\" value=\"p.code\" width=\"140\" sortable=\"true\" align=\"left\">\n            <clientTemplate><![CDATA[<a class=\"link\" href=\"/putaway/createPutaway?code=<#=obj.code#>\"><#=obj.code#></a>]]></clientTemplate>\n        </column>\n        <column id=\"type\" name=\"Type\" value=\"p.type\" width=\"220\" sortable=\"true\" align=\"left\" />\n        <column id=\"username\" name=\"Generated By\" value=\"u.username\" width=\"180\" sortable=\"true\" align=\"left\" />\n        <column id=\"skuCode\" name=\"Sku Code\" value=\"it.sku_code\" width=\"130\" sortable=\"true\" align=\"left\" />\n\t\t<column id=\"shelfCode\" name=\"Shelf Code\" value=\"s.code\" width=\"180\" sortable=\"true\" align=\"left\" hidden=\"true\"/>        <column id=\"quantity\" name=\"Qty\" value=\"SUM(pi.quantity)\" width=\"80\" sortable=\"true\" align=\"left\" />\n        <column id=\"statusCode\" name=\"Status\" value=\"p.status_code\" width=\"180\" sortable=\"true\" align=\"left\" />\n        <column id=\"created\" name=\"Created At\" value=\"p.created\" width=\"150\" sortable=\"true\" align=\"left\" type=\"date\" />\n        <column id=\"updated\" name=\"Updated At\" value=\"p.updated\" width=\"150\" sortable=\"true\" align=\"left\" type=\"date\" />\n    </columns>\n    <filters>\n        <filter id=\"codeFilter\" name=\"Code Contains\" condition=\"p.code like :codeFilter\" type=\"text\" valueExpression=\"#{\'%\' + #codeFilter + \'%\'}\" attachToColumn=\"code\" />\n        <filter id=\"itemSkuFilter\" name=\"Item sellerSkuCode Code/Name contains\" condition=\"(it.sku_code like :itemSkuFilter or it.name like :itemSkuFilter)\" type=\"text\" valueExpression=\"#{\'%\' + #itemSkuFilter + \'%\'}\" attachToColumn=\"quantity\" />\n        <filter id=\"usernameFilter\" name=\"Username Contains\" condition=\"u.username like :usernameFilter\" type=\"text\" valueExpression=\"#{\'%\' + #usernameFilter + \'%\'}\" attachToColumn=\"username\" />\n        <filter id=\"typeFilter\" name=\"Type\" condition=\"p.type in (:typeFilter)\" type=\"multiselect\" values=\"enum:Cancelled|PUTAWAY_CANCELLED_ITEM,GRN|PUTAWAY_GRN_ITEM,Returned|PUTAWAY_COURIER_RETURNED_ITEM,Reverse Pickiup|PUTAWAY_REVERSE_PICKUP_ITEM,Gatepass|PUTAWAY_GATEPASS_ITEM,Inspected Not Bad|PUTAWAY_INSPECTED_NOT_BAD_ITEM,Transfer|PUTAWAY_SHELF_TRANSFER,Direct Returns|PUTAWAY_RECEIVED_RETURNS\" attachToColumn=\"type\" />\n        <filter id=\"statusFilter\" name=\"Status\" condition=\"p.status_code in (:statusFilter)\" type=\"multiselect\" values=\"query:select code,description from putaway_status\" attachToColumn=\"statusCode\" />\n        <filter id=\"dateRangeFilter\" name=\"Created in Date Range\" condition=\"p.created &gt; :dateRangeFilterStart and p.created &lt; :dateRangeFilterEnd\" type=\"daterange\" attachToColumn=\"created\" />\n        <filter id=\"productCodeFilter\" name=\"Sku Code Contains\" condition=\"it.sku_code like :productCodeFilter\" type=\"text\" valueExpression=\"#{\'%\' + #productCodeFilter + \'%\'}\" attachToColumn=\"skuCode\" />\n    \t<filter id=\"shelfCodeFilter\" name=\"Status\" condition=\"s.code = :shelfCodeFilter\" type=\"text\" attachToColumn=\"shelfCode\"/>\n    </filters>\n    <groupBy>\n        group by p.id\n    </groupBy>\n</table-config>\n' where name = "DATATABLE SEARCH PUTAWAY";-- done
update export_job_type set export_job_config = '<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<table-config name=\"DATATABLE_SUB_CYCLE_COUNT_SHELVES\" data-source=\"SECONDARY\" height=\"1400\"  >\n    <queryString>\n        <![CDATA[from cycle_count_shelf_summary ccss join user u on ccss.user_id = u.id left join cycle_count_item cci on cci.cycle_count_shelf_summary_id=ccss.id join sub_cycle_count scc on ccss.sub_cycle_count_id=scc.id join cycle_count cc on cc.id=scc.cycle_count_id join shelf s on ccss.shelf_id=s.id join section ss on s.section_id=ss.id join pick_set ps on ss.pick_set_id=ps.id where cc.facility_id= :facilityId]]>\n    </queryString>\n    <columns>\n        <column id=\"shelfCode\" name=\"Shelf Code\" value=\"s.code\" width=\"150\" sortable=\"true\" align=\"left\">\n            <clientTemplate><![CDATA[<a class=\"link\" href=\"/cyclecount/shelf/results?shelfCode=<#=shelfCode#>\"><#=shelfCode#></a>]]></clientTemplate>\n        </column>\n        <column id=\"shelfId\" name=\"ShelfId\" value=\"s.id\" width=\"150\" hidden=\"true\"/>\n        <column id=\"pickSetName\" name=\"Zone Name\" value=\"ps.name\" width=\"150\" sortable=\"true\" align=\"left\"/>\n        <column id=\"putawayPendingCount\" name=\"Putaway Pending Count\" calculated=\"true\" value=\"query:select count(distinct p.id) from putaway p join putaway_item pi on pi.putaway_id=p.id where pi.shelf_id= :shelfId and p.status_code not in (\'DISCARDED\',\'COMPLETE\')\" width=\"140\" align=\"left\">\n            <clientTemplate><![CDATA[<a data-shelfcode=\"<#=shelfCode#>\" class=\"link putawayPendingCountShelf\"><#=putawayPendingCount#></a>]]></clientTemplate>\n        </column>\n        <column id=\"pickupPending\" name=\"Pickup Pending\" calculated=\"true\" value=\"query:select count(pl.id) from picklist pl join pick_bucket_slot pbs on pbs.picklist_id = pl.id join pick_bucket_slot_item pbsi on pbsi.pick_bucket_slot_id = pbs.id join sale_order_item soi on soi.id = pbsi.sale_order_item_id join item_type_inventory iti on soi.item_type_inventory_id = iti.id join shelf s on iti.shelf_id = s.id join shipping_package sp on (sp.id = pbs.shipping_package_id and sp.facility_id = pl.facility_id) where s.id = :shelfId and (sp.status_code in (\'PICKING\', \'PICKED\') or pbsi.status_code = \'PUTBACK_PENDING\') and pl.status_code!=\'CLOSED\'\"  width=\"200\" align=\" LEFT \">\n          <clientTemplate><![CDATA[<a data-shelfcode=\"<#=shelfCode#>\" class=\"link picklistPendingCountShelf\"><#=pickupPending#></a>]]></clientTemplate>\n        </column>\n        <column id=\"expectedInventory\" name=\"Expected Inventory\" value=\"ifnull(sum(cci.expected_inventory),0)\" width=\"140\" align=\"left\" />\n        <column id=\"totalFound\" name=\"Total Found\" value=\"ifnull(sum(cci.expected_inventory) + sum(cci.inventory_difference),0)\" width=\"150\" sortable=\"true\" align=\"left\"/>\n        <column id=\"totalExtra\" name=\"Total Extra Inventory\" value=\"sum(if(cci.inventory_difference &gt; 0,cci.inventory_difference,0))\" width=\"150\" sortable=\"true\" align=\"left\"/>\n        <column id=\"totalMissing\" name=\"Total Missing Inventory\" value=\"abs(sum(if(cci.inventory_difference &lt; 0,cci.inventory_difference,0)))\" width=\"150\" sortable=\"true\" align=\"left\"/>\n        <column id=\"foundBarcodes\" name=\"Barcodes\" value=\"ccss.item_codes\" width=\"250\" sortable=\"true\" align=\"left\"/>\n        <column id=\"woBarcode\" name=\"W/O Barcode\" value=\"ccss.non_barcoded_item_count\" width=\"150\" hidden=\"true\"/>\n        <column id=\"updated\" name=\"Updated\" value=\"ccss.updated\" width=\"110\" sortable=\"true\" align=\"left\" type=\"date\" />\n        <column id=\"username\" name=\"Username\" value=\"u.username\" width=\"150\" sortable=\"true\" align=\"left\"/>\n        <column id=\"statusCode\" name=\"Status\" value=\"s.status_code\" width=\"200\" sortable=\"true\" align=\"left\"/>\n        <column id=\"statusCodeForFilter\" name=\"Status\" value=\"s.status_code\" width=\"200\" sortable=\"true\" align=\"left\" hidden=\"true\"/>\n        <column id=\"subCycleCountCode\" name=\"Sub Cycle Count Code\" value=\"scc.code\" width=\"130\" hidden=\"true\"/>\n        <column id=\"active\" name=\"Count Active\" value=\"ccss.active\" width=\"100\" hidden=\"true\"/>\n    </columns>\n    <groupBy>\n        group by ccss.id\n    </groupBy>\n    <defaultSortBy>\n        cc.created desc\n    </defaultSortBy>\n    <filters>\n        <filter id=\"codeContains\" name=\"Shelf Code Equals\" condition=\"(cc.code like :codeContains)\" type=\"text\" valueExpression=\"#{\'%\' + #codeContains + \'%\'}\" attachToColumn=\"subCycleCountCode\" />\n        <filter id=\"subCycleCountCodeFilter\" name=\"Sub Cycle Count Code Equals\" condition=\"scc.code = :subCycleCountCodeFilter\" type=\"text\" attachToColumn=\"subCycleCountCode\" />\n        <filter id=\"statusCodeFilter\" name=\"Shelf Status in\" condition=\"s.status_code in (:statusCodeFilter)\" type=\"multiselect\" values=\"enum:UNBLOCKED|QUEUED_FOR_COUNT,BLOCKED|COUNT_REQUESTED,READY|READY_FOR_COUNT,IN PROGRESS|COUNT_IN_PROGRESS\" attachToColumn=\"statusCodeForFilter\" />\n        <filter id=\"completedFilter\" name=\"Shelf Completed\" condition=\"ccss.active != :completedFilter\" type=\"boolean\" attachToColumn=\"active\" />\n    </filters>\n</table-config>' where name = "DATATABLE_SUB_CYCLE_COUNT_SHELVES";
update export_job_type set export_job_config = '<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<table-config name=\"DATATABLE_CYCLE_COUNT_RESULTS\" height=\"1400\" resultCountNotSupported=\"true\">\n    <queryString>\n        <![CDATA[from cycle_count_item cci join cycle_count_shelf_summary ccss on cci.cycle_count_shelf_summary_id=ccss.id join user u on ccss.user_id=u.id join sub_cycle_count scc on ccss.sub_cycle_count_id=scc.id join cycle_count cc on scc.cycle_count_id=cc.id join shelf s on ccss.shelf_id=s.id join item_type it on it.sku_code=cci.sku_code join vendor_item_type vit on it.id=vit.item_type_id join party p on vit.vendor_id=p.id join vendor v on p.id=v.id where vit.facility_id=cc.facility_id and  cc.facility_id= :facilityId]]>\n    </queryString>\n    <columns>\n        <column id=\"discarded\" name=\"Discarded\" value=\"cci.discarded\" width=\"100\" align=\"left\" type=\"boolean\" hidden=\"true\" exportable=\"true\"/>\n        <column id=\"shelfCode\" name=\"Shelf Code\" value=\"s.code\" width=\"100\" align=\"left\"/>\n        <column id=\"itemTypeName\" name=\"Product Name\" value=\"cci.item_type_name\" width=\"150\" sortable=\"true\" align=\"left\"/>\n        <column id=\"sellerName\" name=\"Seller\" value=\"p.name\" width=\"120\" sortable=\"true\" align=\"left\" hidden=\"true\"/>\n        <column id=\"skuCode\" name=\"Sku Code\" value=\"cci.sku_code\" width=\"150\" sortable=\"true\" align=\"left\"/>\n        <column id=\"inventoryType\" name=\"Inventory Type\" value=\"cci.inventory_type\" width=\"120\" align=\"left\"/>\n        <column id=\"expectedInventory\" name=\"Expected Inventory\" value=\"cci.expected_inventory\" width=\"100\" sortable=\"true\" align=\"left\"/>\n        <column id=\"inventoryFound\" name=\"Inventory Found\" value=\"cci.expected_inventory + cci.inventory_difference\" width=\"100\" sortable=\"true\" align=\"left\"/>\n        <column id=\"extra\" name=\"Extra\" value=\"if(cci.inventory_difference&gt;0,inventory_difference,0)\" width=\"50\" sortable=\"true\" align=\"left\"/>\n        <column id=\"reconciledExtra\" name=\"Extra Reconciled\" value=\"if(cci.inventory_difference&gt;0,inventory_difference - pending_reconciliation,0)\" width=\"100\" sortable=\"true\" align=\"left\"/>\n        <column id=\"netExtra\" name=\"Net Extra\" value=\"if(cci.inventory_difference&gt;0,pending_reconciliation,0)\" width=\"60\" sortable=\"true\" align=\"left\"/>\n        <column id=\"missing\" name=\"Missing\" value=\"if(cci.inventory_difference&lt;0,abs(cci.inventory_difference),0)\" width=\"60\" sortable=\"true\" align=\"left\"/>\n        <column id=\"reconciledMissing\" name=\"Missing Reconciled\" value=\"if(cci.inventory_difference&lt;0,abs(cci.inventory_difference) - pending_reconciliation,0)\" width=\"100\" sortable=\"true\" align=\"left\"/>\n        <column id=\"netMissing\" name=\"Net Missing\" value=\"if(cci.inventory_difference&lt;0,pending_reconciliation,0)\" width=\"80\" sortable=\"true\" align=\"left\"/>\n        <column id=\"username\" name=\"Username\" value=\"u.username\" width=\"150\" sortable=\"true\" align=\"left\"/>\n        <column id=\"created\" name=\"Created\" value=\"cci.created\" type=\"date\" width=\"100\" sortable=\"true\" align=\"left\"/>\n        <column id=\"cycleCountCode\" name=\"Cycle Count Code\" hidden=\"true\" value=\"cc.code\" width=\"150\" align=\"left\"/>\n        <column id=\"inventory_type\" name=\"Inventory Type\" value=\"cci.inventory_type\" width=\"100\" sortable=\"true\" align=\"left\"/>\n    </columns>\n    <defaultSortBy>\n        cci.created desc\n    </defaultSortBy>\n    <filters>\n        <filter id=\"itemTypeNameFilter\" name=\"Item Type Name like\" condition=\"(cci.item_type_name like :itemTypeNameFilter)\" type=\"text\" valueExpression=\"#{\'%\' + #itemTypeNameFilter + \'%\'}\" attachToColumn=\"itemTypeName\" />\n        <filter id=\"skuCodeFilter\" name=\"SUPC equals\" condition=\"(cci.sku_code = :skuCodeFilter)\" type=\"text\" attachToColumn=\"skuCode\"/>\n        <filter id=\"shelfCodeFilter\" name=\"Shelf Code equals\" condition=\"(s.code = :shelfCodeFilter)\" type=\"text\" attachToColumn=\"shelfCode\"/>\n        <filter id=\"cycleCountCodeFilter\" name=\"Cycle Count Code equals\" condition=\"(cc.code = :cycleCountCodeFilter)\" type=\"text\" attachToColumn=\"cycleCountCode\"/>\n        <filter id=\"missingFilter\" name=\"Missing Filter\" condition=\"cci.inventory_difference&lt;0\" type=\"boolean\" attachToColumn=\"difference\"/>\n        <filter id=\"extraFilter\" name=\"Extra Filter\" condition=\"cci.inventory_difference&gt;0\" type=\"boolean\" attachToColumn=\"difference\"/>\n    </filters>\n</table-config>' where name = "DATATABLE_CYCLE_COUNT_RESULTS";
update export_job_type set export_job_config = '<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<export-config resultCountNotSupported=\"true\">\n    <queryString>\n        <![CDATA[from cycle_count_item cci join cycle_count_shelf_summary ccss on cci.cycle_count_shelf_summary_id=ccss.id join user u on ccss.user_id=u.id join sub_cycle_count scc on ccss.sub_cycle_count_id=scc.id join cycle_count cc on scc.cycle_count_id=cc.id join shelf s on ccss.shelf_id=s.id join item_type it on it.sku_code=cci.sku_code join vendor_item_type vit on it.id=vit.item_type_id join party p on vit.vendor_id=p.id join vendor v on p.id=v.id where (cci.expected_inventory >0 or cci.inventory_difference >0) and (vit.facility_id=cc.facility_id and cc.facility_id= :facilityId)]]>\n    </queryString>\n    <columns>\n        <column id=\"discarded\" name=\"Discarded\" value=\"cci.discarded\" />\n        <column id=\"shelfCode\" name=\"Shelf Code\" value=\"s.code\" />\n        <column id=\"itemTypeName\" name=\"Product Name\" value=\"cci.item_type_name\" />\n        <column id=\"sellerName\" name=\"Seller\" value=\"p.name\" />\n        <column id=\"skuCode\" name=\"Sku Code\" value=\"cci.sku_code\" />\n        <column id=\"expectedInventory\" name=\"Expected Inventory\" value=\"cci.expected_inventory\" />\n        <column id=\"inventoryFound\" name=\"Inventory Found\" value=\"cci.expected_inventory + cci.inventory_difference\" />\n        <column id=\"extra\" name=\"Extra\" value=\"if(cci.inventory_difference&gt;0,inventory_difference,0)\" />\n        <column id=\"reconciledExtra\" name=\"Extra Reconciled\" value=\"if(cci.inventory_difference&gt;0,inventory_difference - pending_reconciliation,0)\" />\n        <column id=\"netExtra\" name=\"Net Extra\" value=\"if(cci.inventory_difference&gt;0,pending_reconciliation,0)\" />\n        <column id=\"missing\" name=\"Missing\" value=\"if(cci.inventory_difference&lt;0,abs(cci.inventory_difference),0)\" />\n        <column id=\"reconciledMissing\" name=\"Missing Reconciled\" value=\"if(cci.inventory_difference&lt;0,abs(cci.inventory_difference) - pending_reconciliation,0)\" />\n        <column id=\"netMissing\" name=\"Net Missing\" value=\"if(cci.inventory_difference&lt;0,pending_reconciliation,0)\" />\n        <column id=\"username\" name=\"Username\" value=\"u.username\" />\n        <column id=\"created\" name=\"Created\" value=\"cci.created\" />\n    </columns>\n    <filters>\n        <filter id=\"itemTypeNameFilter\" name=\"Item Type Name like\" condition=\"(cci.item_type_name like :itemTypeNameFilter)\" type=\"text\" valueExpression=\"#{\'%\' + #itemTypeNameFilter + \'%\'}\"  />\n        <filter id=\"skuCodeFilter\" name=\"SUPC equals\" condition=\"(cci.sku_code = :skuCodeFilter)\" type=\"text\" />\n        <filter id=\"shelfCodeFilter\" name=\"Shelf Code equals\" condition=\"(s.code = :shelfCodeFilter)\" type=\"text\" />\n        <filter id=\"cycleCountCodeFilter\" name=\"Cycle Count Code equals\" condition=\"(cc.code = :cycleCountCodeFilter)\" type=\"text\" required = \"true\"/>\n        <filter id=\"missingFilter\" name=\"Missing Filter\" condition=\"cci.inventory_difference&lt;0\" type=\"boolean\" />\n        <filter id=\"extraFilter\" name=\"Extra Filter\" condition=\"cci.inventory_difference&gt;0\" type=\"boolean\" />\n     </filters>\n</export-config>' where name = "Cycle Count Overall Data";
ALTER TABLE item_type_inventory ADD CONSTRAINT `facility_id` UNIQUE (`facility_id`,`item_type_id`,`shelf_id`,`type`, `ageing_start_date`);
insert ignore into access_pattern select null,id,'/data/admin/layout/shelves/active/count',now(),now() from access_resource where name='MINIMAL';
-- Aditya | added views for cycle count result
insert IGNORE into user_datatable_view (tenant_id, user_id, datatable_type, name, code, config_json, display_order, system_view, created, updated, access_resource_name)
  SELECT t.id, NULL, "DATATABLE_CYCLE_COUNT_RESULTS", "Extra", "EXTRA", '{"resultsPerPage":25,"columns":[{"name":"shelfCode"},{"name":"itemTypeName"},{"name":"sellerName"},{"name":"skuCode"},{"name":"expectedInventory"},{"name":"inventoryFound"},{"name":"extra"},{"name":"reconciledExtra"},{"name":"netExtra"},{"name":"created"},{"name":"cycleCountCode"},{"name":"difference"}],"filters":[{"id":"extraFilter","checked":true}],"sortColumns":[{"column":"created","descending":true}]}', 20, 1, now(), now(), 'CYCLE_COUNT_VIEW' from tenant t;
insert IGNORE into user_datatable_view (tenant_id, user_id, datatable_type, name, code, config_json, display_order, system_view, created, updated, access_resource_name)
  SELECT t.id, NULL, "DATATABLE_CYCLE_COUNT_RESULTS", "Missing Inventory", "MISSING", 'Missing Inventory'',''MISSING'',''{"resultsPerPage":25,"columns":[{"name":"shelfCode"},{"name":"itemTypeName"},{"name":"sellerName"},{"name":"skuCode"},{"name":"expectedInventory"},{"name":"inventoryFound"},{"name":"missing"},{"name":"reconciledMissing"},{"name":"netMissing"},{"name":"created"},{"name":"cycleCountCode"},{"name":"difference"}],"filters":[{"id":"missingFilter","checked":true}],"sortColumns":[{"column":"created","descending":true}]}', 30, 1, now(), now(), 'CYCLE_COUNT_VIEW' from tenant t;
insert IGNORE into user_datatable_view (tenant_id, user_id, datatable_type, name, code, config_json, display_order, system_view, created, updated, access_resource_name)
  SELECT t.id, NULL, "DATATABLE_CYCLE_COUNT_ERROR_ITEMS", "Error Items", "ERROR_ITEMS", 'sortColumns":[{"column":"created","descending":true}]}', 10, 1, now(), now(), 'CYCLE_COUNT_VIEW' from tenant t;
insert IGNORE into user_datatable_view (tenant_id, user_id, datatable_type, name, code, config_json, display_order, system_view, created, updated, access_resource_name)
  SELECT t.id, NULL, "DATATABLE_CYCLE_COUNT_MISSING_ITEMS", "Missing Items", "MISSING_ITEMS", '{"resultsPerPage":25,"columns":[],"filters":[],"sortColumns":[]}', 10, 1, now(), now(), 'CYCLE_COUNT_VIEW' from tenant t;
update export_job_type set export_job_config = '<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<table-config name=\"DATATABLE_CYCLE_COUNT_ERROR_ITEMS\" height=\"1400\" resultCountNotSupported=\"true\" >\n    <queryString>\n        <![CDATA[from cycle_count_error_item ccei join shelf s on ccei.shelf_id=s.id join item i on ccei.item_code=i.code join item_type it on i.item_type_id = it.id join sub_cycle_count scc on ccei.sub_cycle_count_id=scc.id join cycle_count cc on scc.cycle_count_id = cc.id where i.facility_id = cc.facility_id and cc.facility_id = :facilityId ]]>\n    </queryString>\n    <columns>\n        <column id=\"shelfCode\" name=\"Shelf Code\" value=\"s.code\" width=\"150\" sortable=\"true\" align=\"left\"/>\n        <column id=\"itemCode\" name=\"Item Code\" value=\"ccei.item_code\" width=\"150\" sortable=\"true\" align=\"left\"/>\n        <column id=\"itemTypeName\" name=\"Product Name\" value=\"it.name\" width=\"200\" sortable=\"true\" align=\"left\" />\n        <column id=\"skuCode\" name=\"Sku Code\" value=\"it.sku_code\" sortable=\"true\" width=\"110\" align=\"left\"/>\n        <column id=\"reasonCode\" name=\"Reason Code\" value=\"ccei.reason_code\" sortable=\"true\" width=\"130\" align=\"left\"/>\n        <column id=\"comment\" name=\"Comment\" value=\"ccei.comment\" sortable=\"true\" width=\"130\" align=\"left\"/>\n        <column id=\"created\" name=\"Scanned Time\" value=\"ccei.created\" sortable=\"true\" width=\"110\" align=\"left\" type=\"date\"/>\n        <column id=\"cycleCountCode\" name=\"Cycle Count Code\" value=\"cc.code\" width=\"150\" hidden=\"true\" align=\"left\"/>\n    </columns>\n    <defaultSortBy>\n        ccei.created desc\n    </defaultSortBy>\n    <filters>\n        <filter id=\"cycleCountCodeFilter\" name=\"Cycle Count Code Equals\" condition=\"(cc.code = :cycleCountCodeFilter)\" type=\"text\" attachToColumn=\"cycleCountCode\" />\n    \t<filter id=\"skuCodeFilter\" name=\"SUPC equals\" condition=\"(i.sku_code = :skuCodeFilter)\" type=\"text\" attachToColumn=\"skuCode\"/>\n        <filter id=\"itemCodeFilter\" name=\"SUPC equals\" condition=\"(ccei.item_code = :itemCodeFilter)\" type=\"text\" attachToColumn=\"itemCode\"/>\n    </filters>\n</table-config>\n' where name = "DATATABLE_CYCLE_COUNT_ERROR_ITEMS";
update export_job_type set export_job_config = '<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<table-config name=\"DATATABLE_CYCLE_COUNT_MISSING_ITEMS\" height=\"1400\" resultCountNotSupported=\"true\" >\n    <queryString>\n        <![CDATA[from item i join party p on i.vendor_id=p.id join item_type it on it.id = i.item_type_id where i.shelf_id is null and i.status_code in (\'GOOD_INVENTORY\',\'BAD_INVENTORY\',\'QC_REJECTED\') and i.facility_id = :facilityId and p.facility_id= :facilityId]]>\n    </queryString>\n    <columns>\n        <column id=\"itemCode\" name=\"Item Code\" value=\"i.code\" width=\"150\" sortable=\"true\" align=\"left\"/>\n        <column id=\"inventoryType\" name=\"Inventory Type\" value=\"i.inventory_type\" sortable=\"true\" width=\"120\" align=\"left\"/>\n        <column id=\"skuCode\" name=\"Sku Code\" value=\"it.sku_code\" sortable=\"true\" width=\"200\" align=\"left\"/>\n        <column id=\"itemTypeName\" name=\"Product Name\" value=\"it.name\" width=\"350\" sortable=\"true\" align=\"left\" />\n        <column id=\"sellerName\" name=\"Seller\" value=\"p.name\" width=\"250\" sortable=\"true\" align=\"left\"/>\n        <column id=\"created\" name=\"Created\" value=\"i.created\" hidden=\"true\" sortable=\"true\" width=\"110\" align=\"left\" type=\"date\"/>\n    </columns>\n    <defaultSortBy>\n        i.created desc\n    </defaultSortBy>\n    <filters>\n        <filter id=\"skuCodeFilter\" name=\"SUPC equals\" condition=\"(it.sku_code = :skuCodeFilter)\" type=\"text\" attachToColumn=\"skuCode\"/>\n    </filters>\n</table-config>' where name = "DATATABLE_CYCLE_COUNT_MISSING_ITEMS";
ALTER TABLE cycle_count_shelf_summary MODIFY COLUMN item_codes TEXT;
-- done
-- Aditya | drop bad constraint on sub cycle count and add new one
delete from sequence where name in ("SUB_CYCLE_COUNT");
insert ignore into sequence (tenant_id, facility_id, name, level, prefix, next_year_prefix, prefix_expression, reset_counter_next_year, created) SELECT t.id, NULL , 'SUB_CYCLE_COUNT', 'TENANT', 'SCC', NULL, NULL, 0, now() from tenant t;
-- done
-- Aditya | drop "All" user datetable view for "DATATABLE_CYCLE_COUNT_MISSING_ITEMS"
delete from user_datatable_view where name = 'ALL' and datatable_type = 'DATATABLE_CYCLE_COUNT_MISSING_ITEMS';
update user_datatable_view set name = 'Overall' where code = 'ALL' and datatable_type = 'DATATABLE_CYCLE_COUNT_RESULTS';
update user_datatable_view set config_json = '{"resultsPerPage":25,"columns":[{"name":"shelfCode"},{"name":"itemTypeName"},{"name":"sellerName"},{"name":"skuCode"},{"name":"expectedInventory"},{"name":"inventoryFound"},{"name":"missing"},{"name":"reconciledMissing"},{"name":"netMissing"},{"name":"created"},{"name":"cycleCountCode"},{"name":"difference"}],"filters":[{"id":"missingFilter","checked":true}],"sortColumns":[{"column":"created","descending":true}]}' where code = 'MISSING' and datatable_type = 'DATATABLE_CYCLE_COUNT_RESULTS';
update user_datatable_view set config_json = '{"resultsPerPage":25,"columns":[],"filters":[],"sortColumns":[{"column":"created","descending":true}]}' where code = 'MISSING_ITEMS' and datatable_type = 'DATATABLE_CYCLE_COUNT_MISSING_ITEMS';
-- done
-- Aditya | added imports
insert ignore into access_resource (name, access_resource_group_id, level, created, updated)
  SELECT 'MODIFY_EXPIRABLE_STATUS', arg.id, 'TENANT', now(), now() from access_resource_group arg where name = 'IMPORT';
insert ignore into facility_access_resource (facility_type, access_resource_name, enabled, created, updated) values ('WAREHOUSE', 'MODIFY_EXPIRABLE_STATUS', 1, now(), now());
insert ignore into role_access_resource (role_id, access_resource_name, created) SELECT r.id, 'MODIFY_EXPIRABLE_STATUS', now() from role r where r.code = 'ADMIN';
insert ignore into product_access_resource (product_code, access_resource_name, enabled, created, updated) VALUES ('ENTERPRISE', 'MODIFY_EXPIRABLE_STATUS', 1, now(), now());

insert into import_job_type (tenant_id, name, handler_class, import_job_config, enabled, access_resource_name, group_by_sort, import_options, log_level, created, updated)
  SELECT t.id, 'Modify Expirable Status', 'com.uniware.services.imports.ModifyExpirableStatusJobHandler', '<?xml version="1.0" encoding="ISO-8859-1"?>
<import-config>
    <columns>
        <column name="Sku Code" source="Sku Code" description="Item Sku Code" required="true" requiredInUpdate="true"/>
        <column name="Expirable" source="Expirable" description="Expirable or not" required="true" requiredInUpdate="true"/>
        <column name="Shelf Life" source="Shelf Life" description="Expiry" required="true" requiredInUpdate="false"/>
    </columns>
</import-config>', 1,'MODIFY_EXPIRABLE_STATUS', 0,'UPDATE_EXISTING', 0, now(), now() from tenant t;

insert into access_resource (name, access_resource_group_id, level, created, updated) SELECT 'IMPORT_UPDATE_ITEM', arg.id, 'TENANT', now(), now() from access_resource_group arg where name = 'IMPORT';
insert ignore into facility_access_resource (facility_type, access_resource_name, enabled, created, updated) values ('WAREHOUSE', 'IMPORT_UPDATE_ITEM', 1, now(), now());
insert ignore into role_access_resource (role_id, access_resource_name, created) SELECT r.id, 'IMPORT_UPDATE_ITEM', now() from role r where r.code = 'ADMIN';
insert ignore into product_access_resource (product_code, access_resource_name, enabled, created, updated) VALUES ('ENTERPRISE', 'IMPORT_UPDATE_ITEM', 1, now(), now());

insert into import_job_type (tenant_id, name, handler_class, import_job_config, enabled, access_resource_name, group_by_sort, import_options, log_level, created, updated)
  select t.id, 'update item', 'com.uniware.services.imports.UpdateItemImportJobHandler', '<?xml version="1.0" encoding="UTF-8"?>
<import-config>
    <columns>
        <column name="Item Code" source="Item Code" description="item code" required="true" requiredInUpdate = "true"/>
        <column name="Facility Code" source="Facility Code" description="facility code" required="true" requiredInUpdate = "true"/>
        <column name="Manufacturing Date" source="Manufacturing Date" description="manufacturing date as dd/MM/yyyy" requiredInUpdate="true" />
    </columns>
</import-config>', 1, 'IMPORT_UPDATE_ITEM', 0,  'UPDATE_EXISTING', 0, now(), now() from tenant t;

delete from import_job_type where name = 'Modify Expirable Status';
delete from access_resource where name = 'MODIFY_EXPIRABLE_STATUS';
delete from role_access_resource where access_resource_name = 'MODIFY_EXPIRABLE_STATUS';

-- Aditya | added update shelf import
insert ignore into access_resource (name, access_resource_group_id, level, created, updated) SELECT 'CONFIGURE_SHELVES', arg.id, 'TENANT', now(), now() from access_resource_group arg where name = 'IMPORT';
insert ignore into facility_access_resource (facility_type, access_resource_name, enabled, created, updated) values ('WAREHOUSE', 'CONFIGURE_SHELVES', 1, now(), now());
insert ignore into role_access_resource (role_id, access_resource_name, created) SELECT r.id, 'CONFIGURE_SHELVES', now() from role r where r.code = 'ADMIN';
insert ignore into product_access_resource (product_code, access_resource_name, enabled, created, updated) VALUES ('ENTERPRISE', 'CONFIGURE_SHELVES', 1, now(), now());

insert into import_job_type (tenant_id, name, handler_class, import_job_config, enabled, access_resource_name, group_by_sort, import_options, log_level, created, updated)
  select t.id, 'Update shelf', 'com.uniware.services.imports.UpdateShelfImportJobHandler', '', 1, 'CONFIGURE_SHELVES', 0,  'CREATE_NEW,UPDATE_EXISTING,CREATE_NEW_AND_UPDATE_EXISTING', 0, now(), now() from tenant t;
update import_job_type set import_job_config = '<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<import-config>\n    <columns>\n        <column name=\"Shelf Type Code\" source=\"Shelf Type Code\" description=\"shelf type code\" required=\"true\" />\n        <column name=\"Shelf Code\" source=\"Shelf Code\" description=\"shelf code\" required=\"true\" />\n        <column name=\"Total Volume\" source=\"Total Volume\" description=\"total volume\" />\n        <column name=\"Filled Volume\" source=\"Filled Volume\" description=\"filled volume\" />\n        <column name=\"Enabled\" source=\"Enabled\" description=\"enabled\" />\n        <column name=\"Item Count\" source=\"Item Count\" description=\"item count\" />\n    </columns>\n</import-config>\n' where name = 'Update shelf';
-- done

-- Aditya | cganges in  constraint;
-- ALTER TABLE item MODIFY last_cycle_count_id int(10) NULL;
-- ALTER TABLE item ADD CONSTRAINT `fk_item_cycle_count` FOREIGN KEY (`last_cycle_count_id`) REFERENCES `cycle_count` (`id`) ON DELETE CASCADE;
update import_job_type set import_job_config = '<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<import-config>\n    <columns>\n        <column name=\"Shelf Type Code\" source=\"Shelf Type Code\" description=\"shelf type code\" required=\"true\" requiredInUpdate = \"true\"/>\n        <column name=\"Section Code\" source=\"Section Code\" description=\"Section Code\" required=\"true\" requiredInUpdate = \"false\"/>\n        <column name=\"Shelf Code\" source=\"Shelf Code\" description=\"shelf code\" required=\"true\" requiredInUpdate = \"true\"/>\n        <column name=\"Enabled\" source=\"Enabled\" description=\"enabled\" />\n    </columns>\n</import-config>' where name = 'Update shelf';
-- done
update export_job_type set type='TABLE' where name = 'DATATABLE_SEARCH_SHELF_PICKLIST';

-- Aditya | When last_cycle_count_item_id was first created, it was set to not null, so mysql entered 0 as its default value. This caused the addition of constraint to fail

-- UPDATE item set last_cycle_count_id = NULL WHERE last_cycle_count_id = 0;
-- ALTER TABLE item ADD CONSTRAINT `fk_item_cycle_count` FOREIGN KEY (`last_cycle_count_id`) REFERENCES `cycle_count` (`id`) ON DELETE CASCADE;
-- done
-- Aditya | cycle count id is unsigned, so the previous query was failing
ALTER TABLE item MODIFY last_cycle_count_id int(10) UNSIGNED NULL;
UPDATE item set last_cycle_count_id = NULL WHERE last_cycle_count_id = 0;
ALTER TABLE item ADD CONSTRAINT `fk_item_cycle_count` FOREIGN KEY (`last_cycle_count_id`) REFERENCES `cycle_count` (`id`) ON DELETE CASCADE;
-- done
-- Aditya | added (and updated) access_patterns for rest and mobile end points
INSERT IGNORE INTO access_pattern (access_resource_id, url_pattern, created, updated) SELECT ar.id, '/services/rest/mobile/v1/*/*/*/*', now(), now() from access_resource ar where ar.name = 'MINIMAL';
INSERT IGNORE INTO access_pattern (access_resource_id, url_pattern, created, updated) SELECT ar.id, '/services/rest/v1/*/*/*/*', now(), now() from access_resource ar where ar.name = 'MINIMAL';
UPDATE access_pattern set url_pattern='/services/rest/v1/*' where url_pattern = '/services/rest/*';
UPDATE access_pattern set url_pattern='/services/rest/v1/*/*' where url_pattern = '/services/rest/*/*';
UPDATE access_pattern set url_pattern='/services/rest/v1/*/*/*' where url_pattern = '/services/rest/*/*/*';

---- done
-- done
-- Aditya | re-enter access resources for this import
insert ignore into access_resource (name, access_resource_group_id, level, created, updated)
  SELECT 'MODIFY_EXPIRABLE_STATUS', arg.id, 'TENANT', now(), now() from access_resource_group arg where name = 'IMPORT';
insert ignore into facility_access_resource (facility_type, access_resource_name, enabled, created, updated) values ('WAREHOUSE', 'MODIFY_EXPIRABLE_STATUS', 1, now(), now());
insert ignore into role_access_resource (role_id, access_resource_name, created) SELECT r.id, 'MODIFY_EXPIRABLE_STATUS', now() from role r where r.code = 'ADMIN';
insert ignore into product_access_resource (product_code, access_resource_name, enabled, created, updated) VALUES ('ENTERPRISE', 'MODIFY_EXPIRABLE_STATUS', 1, now(), now());
insert ignore into import_job_type (tenant_id, name, handler_class, import_job_config, enabled, access_resource_name, group_by_sort, import_options, log_level, created, updated)
  select t.id, 'update item', 'com.uniware.services.imports.UpdateItemImportJobHandler', '<?xml version="1.0" encoding="UTF-8"?>
<import-config>
    <columns>
        <column name="Item Code" source="Item Code" description="item code" required="true" requiredInUpdate = "true"/>
        <column name="Facility Code" source="Facility Code" description="facility code" required="true" requiredInUpdate = "true"/>
        <column name="Manufacturing Date" source="Manufacturing Date" description="manufacturing date as dd/MM/yyyy" requiredInUpdate="true" />
    </columns>
</import-config>', 1, 'IMPORT_UPDATE_ITEM', 0,  'UPDATE_EXISTING', 0, now(), now() from tenant t;
-- done

-- Aditya | export job type for categories
update export_job_type set export_job_config = '<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<table-config name=\"DATATABLE CATEGORIES\" height=\"1400\">\n    <queryString>\n        <![CDATA[from category c left join tax_type tt on tt.id = c.tax_type_id left join tax_type gtt on gtt.id = c.gst_tax_type_id where c.tenant_id = :tenantId]]>\n    </queryString>\n    <columns>\n        <column id=\"name\" name=\"Name\" value=\"c.name\" sortable=\"true\" width=\"160\" align=\"left\">\n            <clientTemplate><![CDATA[<a class=\"link clickable\"><#=obj.name#></a>]]></clientTemplate>\n        </column>\n        <column id=\"code\" name=\"Code\" value=\"c.code\" width=\"160\" sortable=\"true\" align=\"left\" />\n        <column id=\"taxTypeCode\" name=\"Tax Type Code\" value=\"tt.code\" sortable=\"true\" width=\"160\" align=\"left\" />\n        <column id=\"gstTaxTypeCode\" name=\"GST tax Code\" value=\"gtt.code\" sortable=\"true\" width=\"100\" align=\"left\" />\n        <column id=\"itemDetailFields\" name=\"Item Details\" value=\"c.item_detail_fields\" width=\"180\" sortable=\"true\" align=\"left\" hidden=\"true\" />\n        <column id=\"dispatchExpiryTolerance\" name=\"Dispatch Expiry Tolerance\" value=\"c.dispatch_expiry_tolerance\" width=\"100\" align=\"left\"/>\n        <column id=\"grnExpiryTolerance\" name=\"Grn Expiry Tolerance\" value=\"c.grn_expiry_tolerance\" width=\"100\" align=\"left\"/>\n        <column id=\"returnExpiryTolerance\" name=\"Return Expiry Tolerance\" value=\"c.return_expiry_tolerance\" width=\"100\" align=\"left\"/>\n        <column id=\"expirable\" name=\"Expirable\" value=\"c.expirable\" width=\"80\" align=\"left\"/>\n        <column id=\"shelfLife\" name=\"Shelf Life\" value=\"c.shelf_life\" width=\"80\" sortable=\"true\" align=\"left\"/>\n        <column id=\"created\" name=\"Created At\" value=\"c.created\" width=\"180\" sortable=\"true\" align=\"left\" type=\"date\" />\n        <column id=\"updated\" name=\"Updated At\" value=\"c.updated\" width=\"180\" sortable=\"true\" align=\"left\" type=\"date\" />      \n    </columns>\n    <defaultSortBy>\n        c.updated desc\n    </defaultSortBy>\n    <filters>\n        <filter id=\"nameFilter\" name=\"Name Contains\" condition=\"c.name like :nameFilter\" type=\"text\" valueExpression=\"#{\'%\' + #nameFilter + \'%\'}\" attachToColumn=\"name\" />\n        <filter id=\"codeFilter\" name=\"Category Code Contains\" condition=\"c.code like :codeFilter\" type=\"text\" valueExpression=\"#{\'%\' + #codeFilter + \'%\'}\" attachToColumn=\"code\" />\n        <filter id=\"taxTypeCodeFilter\" name=\"Tax Type Code Contains\" condition=\"tt.code like :taxTypeCodeFilter\" type=\"text\" valueExpression=\"#{\'%\' + #taxTypeCodeFilter + \'%\'}\" attachToColumn=\"taxTypeCode\" />\n    </filters>\n</table-config>\n\n\n' where name = "DATATABLE CATEGORIES";
-- Done

-- Aditya | export job type for categories
update export_job_type set export_job_config = '<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<table-config name=\"DATATABLE CATEGORIES\" height=\"1400\">\n    <queryString>\n        <![CDATA[from category c left join tax_type tt on tt.id = c.tax_type_id left join tax_type gtt on gtt.id = c.gst_tax_type_id where c.tenant_id = :tenantId]]>\n    </queryString>\n    <columns>\n        <column id=\"name\" name=\"Name\" value=\"c.name\" sortable=\"true\" width=\"160\" align=\"left\">\n            <clientTemplate><![CDATA[<a class=\"link clickable\"><#=obj.name#></a>]]></clientTemplate>\n        </column>\n        <column id=\"code\" name=\"Code\" value=\"c.code\" width=\"160\" sortable=\"true\" align=\"left\" />\n        <column id=\"taxTypeCode\" name=\"Tax Type Code\" value=\"tt.code\" sortable=\"true\" width=\"160\" align=\"left\" />\n        <column id=\"gstTaxTypeCode\" name=\"GST tax Code\" value=\"gtt.code\" sortable=\"true\" width=\"100\" align=\"left\" />\n        <column id=\"itemDetailFields\" name=\"Item Details\" value=\"c.item_detail_fields\" width=\"180\" sortable=\"true\" align=\"left\" hidden=\"true\" />\n        <column id=\"dispatchExpiryTolerance\" name=\"Dispatch Expiry Tolerance\" value=\"c.dispatch_expiry_tolerance\" width=\"100\" align=\"left\"/>\n        <column id=\"grnExpiryTolerance\" name=\"Grn Expiry Tolerance\" value=\"c.grn_expiry_tolerance\" width=\"100\" align=\"left\"/>\n        <column id=\"returnExpiryTolerance\" name=\"Return Expiry Tolerance\" value=\"c.return_expiry_tolerance\" width=\"100\" align=\"left\"/>\n        <column id=\"expirable\" name=\"Expirable\" value=\"c.expirable\" width=\"80\" align=\"left\"/>\n        <column id=\"shelfLife\" name=\"Shelf Life\" value=\"c.shelf_life\" width=\"80\" sortable=\"true\" align=\"left\"/>\n        <column id=\"created\" name=\"Created At\" value=\"c.created\" width=\"180\" sortable=\"true\" align=\"left\" type=\"date\" />\n        <column id=\"updated\" name=\"Updated At\" value=\"c.updated\" width=\"180\" sortable=\"true\" align=\"left\" type=\"date\" />      \n    </columns>\n    <defaultSortBy>\n        c.updated desc\n    </defaultSortBy>\n    <filters>\n        <filter id=\"nameFilter\" name=\"Name Contains\" condition=\"c.name like :nameFilter\" type=\"text\" valueExpression=\"#{\'%\' + #nameFilter + \'%\'}\" attachToColumn=\"name\" />\n        <filter id=\"codeFilter\" name=\"Category Code Contains\" condition=\"c.code like :codeFilter\" type=\"text\" valueExpression=\"#{\'%\' + #codeFilter + \'%\'}\" attachToColumn=\"code\" />\n        <filter id=\"taxTypeCodeFilter\" name=\"Tax Type Code Contains\" condition=\"tt.code like :taxTypeCodeFilter\" type=\"text\" valueExpression=\"#{\'%\' + #taxTypeCodeFilter + \'%\'}\" attachToColumn=\"taxTypeCode\" />\n    </filters>\n</table-config>\n\n\n' where name = "DATATABLE CATEGORIES";
-- Done

-- Aditya | added access resource for viewing additional fields of categories
INSERT IGNORE INTO access_resource (name, level, created, updated) SELECT 'PROFESSIONAL_CATEGORY_VIEW', 'TENANT', now(), now();
INSERT IGNORE INTO facility_access_resource (facility_type, access_resource_name, enabled, created, updated) values ('WAREHOUSE', 'PROFESSIONAL_CATEGORY_VIEW', 1, now(), now());
INSERT IGNORE INTO role_access_resource (role_id, access_resource_name, created) SELECT r.id, 'PROFESSIONAL_CATEGORY_VIEW', now() from role r where r.code = 'ADMIN';
INSERT IGNORE INTO product_access_resource (product_code, access_resource_name, enabled, created, updated) VALUES ('ENTERPRISE', 'PROFESSIONAL_CATEGORY_VIEW', 1, now(), now());
-- update previous user_datatable_view to show restricted view and add a new one for all columns
UPDATE user_datatable_view SET config_json = '{"resultsPerPage":25,"columns":[{"name":"code"},{"name":"taxTypeCode"},{"name":"gstTaxTypeCode"},{"name":"itemDetailFields"},{"name":"created"},{"name":"updated"}],"filters":[],"sortColumns":[{"column":"updated","descending":true}]}' WHERE name = 'DATATABLE CATEGORIES' AND code = 'ALL' AND access_resource_name = 'MINIMAL';
insert into user_datatable_view (tenant_id, user_id, datatable_type, name, code, config_json, display_order, system_view, created, updated, access_resource_name)
     SELECT t.id, NULL , 'DATATABLE CATEGORIES', 'Professional', 'PROFESSIONAL', '{"resultsPerPage":25,"columns":[],"filters":[],"sortColumns":[{"column":"updated","descending":true}]}', 10, 1, now(), now(), 'PROFESSIONAL_CATEGORY_VIEW' from tenant t;
-- done

-- Amit | Oct 04, 2017 | Inventory ageing fix. These will be removed manually from build server.
insert ignore into item_type_inventory select null, it1.facility_id, it1.item_type_id, it1.shelf_id, it1.type, "1970-01-01", 0,0,0,0,0,it1.priority, it1.sla, it1.created, it1.updated from item_type_inventory it1 where it1.ageing_start_date <> "1970-01-01" and it1.facility_id in (select distinct(facility_id) from system_configuration where name = "traceability.level" and value in ("ITEM_SKU", "NONE"));
create table iti_to_change as select it1.id as bad_id, it2.id as good_id from item_type_inventory it1, item_type_inventory it2 where it1.facility_id = it2.facility_id and it1.item_type_id = it2.item_type_id and it1.shelf_id = it2.shelf_id and it1.type = it2.type and it2.ageing_start_date = '1970-01-01' and it1.ageing_start_date <> "1970-01-01" and it1.facility_id in (select distinct(facility_id) from system_configuration where name = "traceability.level" and value in ("ITEM_SKU", "NONE")) ;
update item_type_inventory it2, item_type_inventory it1, iti_to_change ic set it2.quantity = it2.quantity + it1.quantity, it2.quantity_blocked = it2.quantity_blocked + it1.quantity_blocked, it2.quantity_not_found = it2.quantity_not_found + it1.quantity_not_found, it2.quantity_damaged = it2.quantity_damaged + it1.quantity_damaged where it1.id = ic.bad_id and it2.id = ic.good_id;
update item_type_inventory it1, iti_to_change ic set it1.quantity = 0, it1.quantity_blocked = 0, it1.quantity_not_found = 0, it1.quantity_damaged = 0 where it1.id = ic.bad_id;
update sale_order_item soi, iti_to_change ic set soi.item_type_inventory_id = ic.good_id where soi.item_type_inventory_id = ic.bad_id;
delete iti.* from item_type_inventory iti, iti_to_change ic where iti.id = ic.bad_id;
-- done
-- aditya | updated export job type
update export_job_type set export_job_config = '<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<table-config name=\"DATATABLE_CYCLE_COUNT_SHELF_RESULTS\" height=\"1400\">\n    <queryString>\n        <![CDATA[from cycle_count_item cci join cycle_count_shelf_summary ccss on cci.cycle_count_shelf_summary_id=ccss.id join sub_cycle_count scc on ccss.sub_cycle_count_id=scc.id join cycle_count cc on scc.cycle_count_id=cc.id join shelf s on cci.shelf_id=s.id join item_type it on cci.sku_code = it.sku_code join vendor_item_type vit on it.id=vit.item_type_id join party p on vit.vendor_id=p.id where cc.facility_id= :facilityId]]>\n    </queryString>\n    <columns>\n        <column id=\"itemTypeName\" name=\"Product Name\" value=\"cci.item_type_name\" width=\"150\" sortable=\"true\" align=\"left\"/>\n        <column id=\"sellerName\" name=\"Seller\" value=\"p.name\" width=\"150\" sortable=\"true\" align=\"left\" hidden=\"true\"/>\n        <column id=\"skuCode\" name=\"SUPC\" value=\"cci.sku_code\" width=\"150\" sortable=\"true\" align=\"left\"/>\n        <column id=\"inventoryType\" name=\"Inventory Type\" value=\"cci.inventory_type\" width=\"120\" align=\"left\"/>\n        <column id=\"expectedInventory\" name=\"Expected Inventory\" value=\"cci.expected_inventory\" width=\"100\" sortable=\"true\" align=\"left\"/>\n        <column id=\"inventoryFound\" name=\"Inventory Found\" value=\"cci.expected_inventory + cci.inventory_difference\" width=\"100\" sortable=\"true\" align=\"left\"/>\n        <column id=\"extra\" name=\"Extra\" value=\"if(cci.inventory_difference&gt;0,inventory_difference,0)\" width=\"100\" sortable=\"true\" align=\"left\"/>\n        <column id=\"reconciledExtra\" name=\"Extra Reconciled\" value=\"if(cci.inventory_difference&gt;0,inventory_difference - pending_reconciliation,0)\" width=\"100\" sortable=\"true\" align=\"left\"/>\n        <column id=\"netExtra\" name=\"Net Extra\" value=\"if(cci.inventory_difference&gt;0,pending_reconciliation,0)\" width=\"100\" sortable=\"true\" align=\"left\"/>\n        <column id=\"missing\" name=\"Missing\" value=\"if(cci.inventory_difference&lt;0,abs(cci.inventory_difference),0)\" width=\"100\" sortable=\"true\" align=\"left\"/>\n        <column id=\"reconciledMissing\" name=\"Missing Reconciled\" value=\"if(cci.inventory_difference&lt;0,abs(cci.inventory_difference) - pending_reconciliation,0)\" width=\"100\" sortable=\"true\" align=\"left\"/>\n        <column id=\"netMissing\" name=\"Net Missing\" value=\"if(cci.inventory_difference&lt;0,pending_reconciliation,0)\" width=\"100\" sortable=\"true\" align=\"left\"/>\n        <column id=\"created\" name=\"Created\" value=\"cc.created\" type=\"date\" width=\"100\" sortable=\"true\" align=\"left\"/>\n        <column id=\"shelfCode\" name=\"Shelf Code\" value=\"s.code\" width=\"100\" align=\"left\" hidden=\"true\"/>\n        <column id=\"subCycleCountCode\" name=\"Sub Cycle Count Code\" value=\"scc.code\" width=\"100\" align=\"left\" hidden=\"true\"/>\n    </columns>\n    <defaultSortBy>\n        cci.created desc\n    </defaultSortBy>\n    <filters>\n        <filter id=\"itemTypeNameFilter\" name=\"Item Type Name like\" condition=\"(cci.item_type_name like :itemTypeNameFilter)\" type=\"text\" valueExpression=\"#{\'%\' + #itemTypeNameFilter + \'%\'}\" attachToColumn=\"itemTypeName\" />\n        <filter id=\"skuCodeFilter\" name=\"SUPC equals\" condition=\"(cci.sku_code = :skuCodeFilter)\" type=\"text\" attachToColumn=\"skuCode\"/>\n        <filter id=\"shelfCodeFilter\" name=\"Shelf Code equals\" condition=\"(s.code = :shelfCodeFilter)\" type=\"text\" attachToColumn=\"shelfCode\"/>\n        <filter id=\"subCycleCountCodeFilter\" name=\"Sub Cycle Count Code equals\" condition=\"(scc.code = :subCycleCountCodeFilter)\" type=\"text\" attachToColumn=\"subCycleCountCode\"/>\n    </filters>\n</table-config>' where name = "DATATABLE_CYCLE_COUNT_SHELF_RESULTS";
-- done

-- aditya |
update export_job_type set export_job_config = '<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<table-config name=\"DATATABLE SEARCH PUTAWAY\" height=\"1400\">\n    <queryString>\n        <![CDATA[from putaway p join user u on u.id = p.user_id left join putaway_item pi on pi.putaway_id = p.id join shelf s on s.id = pi.shelf_id left join item_type it on pi.item_type_id = it.id where p.facility_id = :facilityId]]>\n    </queryString>\n    <columns>\n        <column id=\"code\" name=\"Putaway Number\" value=\"p.code\" width=\"140\" sortable=\"true\" align=\"left\">\n            <clientTemplate><![CDATA[<a class=\"link\" href=\"/putaway/createPutaway?code=<#=obj.code#>\"><#=obj.code#></a>]]></clientTemplate>\n        </column>\n        <column id=\"type\" name=\"Type\" value=\"p.type\" width=\"220\" sortable=\"true\" align=\"left\" />\n        <column id=\"username\" name=\"Generated By\" value=\"u.username\" width=\"180\" sortable=\"true\" align=\"left\" />\n        <column id=\"skuCode\" name=\"Sku Code\" value=\"it.sku_code\" width=\"130\" sortable=\"true\" align=\"left\" />\n        <column id=\"quantity\" name=\"Qty\" value=\"SUM(pi.quantity)\" width=\"80\" sortable=\"true\" align=\"left\" />\n        <column id=\"statusCode\" name=\"Status\" value=\"p.status_code\" width=\"180\" sortable=\"true\" align=\"left\" />\n        <column id=\"created\" name=\"Created At\" value=\"p.created\" width=\"150\" sortable=\"true\" align=\"left\" type=\"date\" />\n        <column id=\"updated\" name=\"Updated At\" value=\"p.updated\" width=\"150\" sortable=\"true\" align=\"left\" type=\"date\" />\n    </columns>\n    <filters>\n        <filter id=\"codeFilter\" name=\"Code Contains\" condition=\"p.code like :codeFilter\" type=\"text\" valueExpression=\"#{\'%\' + #codeFilter + \'%\'}\" attachToColumn=\"code\" />\n        <filter id=\"itemSkuFilter\" name=\"Item sellerSkuCode Code/Name contains\" condition=\"(it.sku_code like :itemSkuFilter or it.name like :itemSkuFilter)\" type=\"text\" valueExpression=\"#{\'%\' + #itemSkuFilter + \'%\'}\" attachToColumn=\"quantity\" />\n        <filter id=\"usernameFilter\" name=\"Username Contains\" condition=\"u.username like :usernameFilter\" type=\"text\" valueExpression=\"#{\'%\' + #usernameFilter + \'%\'}\" attachToColumn=\"username\" />\n        <filter id=\"typeFilter\" name=\"Type\" condition=\"p.type in (:typeFilter)\" type=\"multiselect\" values=\"enum:Cancelled|PUTAWAY_CANCELLED_ITEM,GRN|PUTAWAY_GRN_ITEM,Returned|PUTAWAY_COURIER_RETURNED_ITEM,Reverse Pickiup|PUTAWAY_REVERSE_PICKUP_ITEM,Gatepass|PUTAWAY_GATEPASS_ITEM,Inspected Not Bad|PUTAWAY_INSPECTED_NOT_BAD_ITEM,Transfer|PUTAWAY_SHELF_TRANSFER,Direct Returns|PUTAWAY_RECEIVED_RETURNS\" attachToColumn=\"type\" />\n        <filter id=\"statusFilter\" name=\"Status\" condition=\"p.status_code in (:statusFilter)\" type=\"multiselect\" values=\"query:select code,description from putaway_status\" attachToColumn=\"statusCode\" />\n        <filter id=\"dateRangeFilter\" name=\"Created in Date Range\" condition=\"p.created &gt; :dateRangeFilterStart and p.created &lt; :dateRangeFilterEnd\" type=\"daterange\" attachToColumn=\"created\" />\n        <filter id=\"productCodeFilter\" name=\"Sku Code Contains\" condition=\"it.sku_code like :productCodeFilter\" type=\"text\" valueExpression=\"#{\'%\' + #productCodeFilter + \'%\'}\" attachToColumn=\"skuCode\" />\n        <filter id=\"shelfCodeFilter\" name=\"Status\" condition=\"s.code = :shelfCodeFilter\" type=\"text\" attachToColumn=\"shelfCode\"/>\n\n    </filters>\n    <groupBy>\n        group by p.id\n    </groupBy>\n</table-config>\n' where name = "DATATABLE SEARCH PUTAWAY";

-- Piyush
insert ignore into access_pattern select null, ar.id, '/data/myaccount/usageHistory/monthly/get', now(), now() from access_resource ar where ar.name = 'MINIMAL';

-- aditya | adding cycle count widgets
INSERT IGNORE INTO dashboard_widget (name, code, widget_group, access_resource_name, seconds_to_cache, preloaded, hql, enabled, created, updated)
VALUES ('CYCLE COUNT SUMMARY', 'CYCLE_COUNT_SUMMARY', 'OVERVIEW_CYCLE_COUNT', 'CYCLE_COUNT_VIEW', '#{0}', 0, 0, 1, now(), now());
INSERT IGNORE INTO dashboard_widget (name, code, widget_group, access_resource_name, seconds_to_cache, preloaded, hql, enabled, created, updated)
VALUES ('CYCLE COUNT CUM SUMMARY', 'CYCLE_COUNT_CUM_SUMMARY', 'OVERVIEW_CYCLE_COUNT', 'CYCLE_COUNT_VIEW', '#{0}', 0, 0, 1, now(), now());
INSERT IGNORE INTO dashboard_widget (name, code, widget_group, access_resource_name, seconds_to_cache, preloaded, hql, enabled, created, updated)
VALUES ('ZONE PERFORMANCE', 'ZONE_PERFORMANCE', 'OVERVIEW_CYCLE_COUNT', 'CYCLE_COUNT_VIEW', '#{0}', 0, 0, 1, now(), now());
-- done
-- ankit
alter table channel_item_type add index `tenant_sellerSku_status` (tenant_id,seller_sku_code,status_code);

-- Piyush
insert ignore into access_pattern select null, ar.id, '/data/myaccount/usageHistory/monthly/get', now(), now() from access_resource ar where ar.name = 'MINIMAL';
-- Aditya | Making ageing on item as nullable
ALTER TABLE item MODIFY ageing_start_date date DEFAULT NULL;
-- done

insert ignore into system_configuration select null, concat(t.id,'-allow.tax.on.international.order'), null, t.id, 'allow.tax.on.international.order', 'Allow tax on international order', 'false' , 'checkbox',NULL,'MINIMAL','SYSTEM', 10, now(), now() from tenant t;
-- done
-- ankit
update channel set inventory_sync_status = 'OFF' where order_sync_status = 'OFF';
-- done

-- aditya | adding system config for checking if strict fifo
insert into system_configuration (id_unique, facility_id, tenant_id, name, display_name, value, type, group_name, sequence, created)
  SELECT concat(t.id, '-is.ageing.followed.strictly'), NULL, t.id, 'is.ageing.followed.strictly', 'Is ageing followed strictly', 'false', 'checkbox', null, 1, now() from tenant t;
-- done
-- Samdeesh | index alters
ALTER TABLE sale_order_item ADD INDEX `idx_facility_status_code` (`facility_id`,`status_code`);

ALTER TABLE notification ADD INDEX `idx_tenant_id_status_code` (`tenant_id`,`status_code`);

ALTER TABLE notification DROP INDEX `index_status_code`;

ALTER TABLE channel_item_type ADD INDEX `idx_tenant_id_status_code_listing_status_disabled` (`tenant_id`,`status_code`,`listing_status`,`disabled`);

ALTER TABLE channel_item_type DROP INDEX  `index_overview_channel_item_type`;

ALTER TABLE channel_item_type ADD INDEX `idx_tenant_id_status_code_disable_due_to_errors` (`tenant_id`,`status_code`,`disabled_due_to_errors`);

ALTER TABLE sale_order ADD INDEX `idx_tenant_id_channel_id_created` (`tenant_id`,`channel_id`,`created`);
-- done

insert ignore into system_configuration select null, concat(t.id,'-allow.tax.on.international.order'), null, t.id, 'allow.tax.on.international.order', 'Allow tax on international order', 'false' , 'checkbox',NULL,'MINIMAL','SYSTEM', 10, now(), now() from tenant t;
-- Aditya | Making ageing on item as nullable
ALTER TABLE item MODIFY ageing_start_date date;
-- done


insert ignore into import_job_type select null, t.id, 'Re-enable CIT Inventory', 'com.uniware.services.imports.ReEnableCITInventoryImportJobHandler', '', 0, 'CHANNELS_VIEW', null, 0, 'UPDATE_EXISTING', null,  0, null, now(), now() from tenant t;

update import_job_type set import_job_config = '<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<import-config>\n    <columns>\n        <column name=\"Channel Code\" source=\"Channel Code\" description=\"channel code\" required=\"true\" />\n        <column name=\"Channel Product Id\" source=\"Channel Product Id\" description=\"Channel Product Id\" required=\"true\"/>\n        </columns>\n</import-config>' where name = "Re-enable CIT Inventory";

-- Piyush | selective facility inventory sync
CREATE TABLE `channel_associated_facility` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `channel_id` int(10) unsigned NOT NULL,
  `facility_id` int(10) unsigned NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `channel_facility_id` (`channel_id`,`facility_id`),
  CONSTRAINT `fk_caf_channel_id` FOREIGN KEY (`channel_id`) REFERENCES `channel` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_caf_facility_id` FOREIGN KEY (`facility_id`) REFERENCES `facility` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
);
insert ignore into system_configuration (facility_id, tenant_id, name, display_name, value, type, access_resource_name, group_name, sequence, created) SELECT null, t.id, 'selective.facility.inventory.sync.enabled', 'Selective Facility For Inventory Sync?', 'false', 'checkbox', 'CHANNELS_ADMIN', 'SYSTEM', 10, now() from tenant t;
insert ignore into access_pattern select null,id,'/data/channel/editassociatedfacility',now(),now() from access_resource where name='CHANNELS_ADMIN';
-- Done

-- Piyush
insert ignore into system_configuration (id_unique, facility_id, tenant_id, name, display_name, value, type, access_resource_name, group_name, sequence, created) SELECT concat(t.id, 'selective.facility.inventory.sync.enabled'), null, t.id, 'selective.facility.inventory.sync.enabled', 'Selective Facility For Inventory Sync?', 'false', 'checkbox', 'CHANNELS_ADMIN', 'SYSTEM', 10, now() from tenant t;
insert ignore into system_configuration (id_unique, facility_id, tenant_id, name, display_name, value, type, access_resource_name, group_name, sequence, created) SELECT concat(t.id, 'return.invoice.other.charges.applicable'), null, t.id, 'return.invoice.other.charges.applicable', 'Return Invoice Other Charges Applicable?', 'false', 'checkbox', 'MINIMAL', 'SYSTEM', 10, now() from tenant t;

-- Piyush | Add Gatepass Items Import
insert ignore into import_job_type select null, t.id, 'Add Gatepass Items', 'com.uniware.services.imports.AddGatepassItemsImportJobHandler', '', 1, 'MATERIAL_MANAGEMENT', null, 0, 'CREATE_NEW', null,  0, null, now(), now() from tenant t;
update import_job_type set import_job_config = '<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<import-config>\n    <columns>\n        <column name=\"Gatepass Code\" source=\"Gatepass Code\" description=\"gatepass code\" required=\"true\" />\n        <column name=\"Item Or Sku Code\" source=\"Item Or Sku Code\" description=\"item or sku code\" required=\"true\"/>\n        <column name=\"Qty\" source=\"Qty\" description=\"quantity\" />\n        <column name=\"Inventory Type\" source=\"Inventory Type\" description=\"inventory type:GOOD_INVENTORY/BAD_INVENTORY/QC_REJECTED\" />\n    </columns>\n</import-config>' where name = "Add Gatepass Items";
-- Done
update export_job_type set export_job_config = '<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<table-config name=\"DATATABLE_CYCLE_COUNT\" height=\"1400\">\n    <queryString>\n        <![CDATA[from cycle_count cc left join sub_cycle_count scc on cc.id=scc.cycle_count_id left join cycle_count_shelf_summary ccss on ccss.sub_cycle_count_id=scc.id left join shelf s on ccss.shelf_id=s.id left join section ss on s.section_id=ss.id left join pick_set ps on ps.id = ss.pick_set_id where cc.facility_id= :facilityId ]]>\n    </queryString>\n    <columns>\n        <column id=\"cycleCountCode\" name=\"Cycle Count Code\" value=\"cc.code\" width=\"150\" sortable=\"true\" align=\"left\">\n            <clientTemplate><![CDATA[<a class=\"link\" href=\"/subcyclecounts?cyclecountcode=<#=cycleCountCode#>&statusCode=<#=statusCode#>\"><#=cycleCountCode#></a>]]></clientTemplate>\n        </column>\n        <column id=\"facilityId\" name=\"Facility Id\" value=\"cc.facility_id\" width=\"80\" hidden=\"true\"/>\n        <column id=\"startDate\" name=\"Start Date\" value=\"cc.created\" width=\"110\" sortable=\"true\" align=\"left\" type=\"date\" />\n        <column id=\"targetDate\" name=\"Target Date\" value=\"cc.target_completion\" width=\"110\" align=\"left\" type=\"date\"/>\n        <column id=\"actualCommpletion\" name=\"Actual Completion Date\" value=\"cc.completed\" width=\"150\" align=\"left\" type=\"date\"/>\n        <column id=\"totalShelfCount\" name=\"Total Shelf Count\" value=\"count(distinct s.id)\" width=\"110\" sortable=\"true\" align=\"left\"/>\n        <column id=\"completedShelfCount\" name=\"Completed Shelf Count\" value=\"count(distinct s.id) - count(if(ccss.active =true , ccss.shelf_id,null))\" width=\"140\" align=\"left\"/>\n        <column id=\"percentageCompleted\" name=\"Percentage Completed\" value=\"(1- count(if(ccss.active =true , ccss.shelf_id,null))/count(distinct s.id))*100\" width=\"130\" align=\"left\"/>\n        <column id=\"nonBarcodedItemCount\" name=\"Non Barcoded Items\" value=\"sum(ccss.non_barcoded_item_count)\" width=\"120\" align=\"left\"/>\n        <column id=\"statusCode\" name=\"Status\" value=\"cc.status_code\" width=\"110\" sortable=\"true\" align=\"left\"/>\n        <column id=\"result\" name=\"Result\" value=\"cc.code\" width=\"80\" sortable=\"true\" align=\"left\">\n            <clientTemplate><![CDATA[<a target=\"_top\" class=\"btn btn-mini\" href=\"/cyclecount/results?cyclecountcode=<#=obj.cycleCountCode#>\">Results</a>]]></clientTemplate>\n        </column>\n        <column id=\"performance\" name=\"Performance\" value=\"cc.code\" width=\"110\" sortable=\"true\" align=\"left\">\n            <clientTemplate><![CDATA[<a target=\"_top\" class=\"btn btn-mini\" href=\"/cyclecount/performance?cyclecountcode=<#=obj.cycleCountCode#>&startDate=<#=obj.startDate#>&facilityId=<#=facilityId#>\">Performance</a>]]></clientTemplate>\n        </column>\n        <column id=\"edit\" name=\"Edit\" value=\"cc.code\" width=\"110\" sortable=\"true\" align=\"left\">\n            <clientTemplate><![CDATA[<a target=\"_top\" class=\"btn btn-mini edit-cycle-count\" href=\"/cyclecount/edit?cyclecountcode=<#=obj.cycleCountCode#>\">Edit</a>]]></clientTemplate>\n        </column>\n    </columns>\n    <groupBy>\n        group by cc.id\n    </groupBy>\n    <defaultSortBy>\n        cc.created desc\n    </defaultSortBy>\n    <filters>\n        <filter id=\"codeContains\" name=\"Cycle Count Code Contains\" condition=\"(cc.code like :codeContains)\" type=\"text\" valueExpression=\"#{\'%\' + #codeContains + \'%\'}\" attachToColumn=\"cycleCountCode\" />\n    </filters>\n</table-config>' where name = "DATATABLE_CYCLE_COUNT";

-- Piyush | Gatepass Items Import to have shelf and unit price
update import_job_type set import_job_config = '<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<import-config>\n    <columns>\n        <column name=\"Gatepass Code\" source=\"Gatepass Code\" description=\"gatepass code\" required=\"true\" />\n        <column name=\"Item Or Sku Code\" source=\"Item Or Sku Code\" description=\"item or sku code\" required=\"true\"/>\n        <column name=\"Qty\" source=\"Qty\" description=\"quantity\" />\n        <column name=\"Inventory Type\" source=\"Inventory Type\" description=\"inventory type:GOOD_INVENTORY/BAD_INVENTORY/QC_REJECTED\" />\n        <column name=\"Shelf Code\" source=\"Shelf Code\" description=\"shelf code\" />\n        <column name=\"Unit Price\" source=\"Unit Price\" description=\"unit price\" />\n    </columns>\n</import-config>' where name = "Add Gatepass Items";

-- aditya | adding system config for checking if strict fifo
insert into system_configuration (id_unique, facility_id, tenant_id, name, display_name, value, type, group_name, sequence, created)
  SELECT concat(t.id, '-is.ageing.followed.strictly'), NULL, t.id, 'is.ageing.followed.strictly', 'Is ageing followed strictly', 'false', 'checkbox', null, 1, now() from tenant t;
-- done

-- Gurpreet | Picklist Mark Not Found feature
insert into access_pattern select null, id, '/data/oms/picker/picklist/markinventorynotfound', now(), now() from access_resource where name = 'PICKLIST_RECEIVE';

insert ignore INTO export_job_type (tenant_id, name, display_name, view_name, export_job_config, enabled, no_sql, access_resource_name, post_processor_script_name, created, updated, type)
SELECT t.id, 'DATATABLE_MARK_NOT_FOUND_PICKLIST', 'Mark Not Found Picklist', null, '<?xml version="1.0" encoding="UTF-8"?>
<table-config name="DATATABLE_MARK_NOT_FOUND_PICKLIST" height="400">
    <queryString>
        <![CDATA[from picklist p join pick_bucket_slot pbs on pbs.picklist_id = p.id
        join pick_bucket_slot_item pbsi on pbsi.pick_bucket_slot_id = pbs.id
        join shipping_package sp on sp.id = pbs.shipping_package_id
        join sale_order_item soi on soi.id = pbsi.sale_order_item_id
        left join item_type_inventory iti on soi.item_type_inventory_id = iti.id
        left join item_type it on it.id = iti.item_type_id
        left join shelf s on iti.shelf_id = s.id
        where p.facility_id = :facilityId and pbsi.status_code in ("CREATED","PUTBACK_PENDING")]]>
    </queryString>
    <columns>
        <column id="picklistCode" name="Picklist Code" width="110"  value="p.code" />
        <column id="skuCode" name="Sku Code" width="110"  value="it.sku_code" />
        <column id="shelfCode" name="Shelf Code" width="90"  value="s.code" />
        <column id="quantityToPick" name="Quantity" width="60"  value="count(*)" />
        <column id="notFoundCount" name="Not Found" width="70"  value="1" >
            <clientTemplate><![CDATA[<input class="notFoundQuantity" type="text" style="padding:7px;margin:-6px 0px"/>]]>
            </clientTemplate>
        </column>
        <column id="notFoundButtonColumn" name="Action" width="80" value="1">
            <clientTemplate><![CDATA[<div class="notFoundButton uni-btn-primary btn uni-btn-small" style="margin:-10px 0px">Not Found</div>]]>
            </clientTemplate>
        </column>
    </columns>
    <groupBy>
        group by it.sku_code, s.id
    </groupBy>
    <filters>
        <filter id="skuCode" name="Sku Code" condition="it.sku_code= :skuCode" attachToColumn="skuCode" type="text" />
        <filter id="picklistCode" name="Picklist Code" condition="p.code= :picklistCode" attachToColumn="picklistCode" type="text" />
    </filters>
</table-config>', 1, 0, 'PICKLIST_RECEIVE', null, now(), now(), 'TABLE' FROM tenant t;

insert IGNORE into user_datatable_view (tenant_id, user_id, datatable_type, name, code, config_json, display_order, system_view, created, updated, access_resource_name)
  SELECT t.id, NULL, "DATATABLE_MARK_NOT_FOUND_PICKLIST", "ALL", "ALL", '{"resultsPerPage":25,"columns":[],"filters":[],"sortColumns":[]}', 5, 1, now(), now(), 'PICKLIST_RECEIVE' from tenant t;

-- sagar | account billing api
insert into access_pattern select null, ar.id, "/data/admin/system/billingParty/accounts/get", now(), now() from access_resource ar where ar.name = "MINIMAL";

-- Piyush | Auto GRN import
insert ignore into import_job_type select null, t.id, 'Auto GRN Items', 'com.uniware.services.imports.AutoGrnItemsImportJobHandler', '', 1, 'PROCUREMENT', null, 0, 'CREATE_NEW', null,  0, null, now(), now() from tenant t;
update import_job_type set import_job_config = '<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<import-config>\n    <columns>\n        <column name=\"Vendor Code\" source=\"Vendor Code\" description=\"vendor code\" required=\"true\" />\n        <column name=\"Vendor Invoice Number\" source=\"Vendor Invoice Number\" description=\"vendor invoice number\" groupBy=\"true\" required=\"true\" />\n        <column name=\"Vendor Invoice Date\" source=\"Vendor Invoice Date\" description=\"vendor invoice date in format dd-MM-yyyy\" required=\"true\"/>\n        <column name=\"Sku Code\" source=\"Sku Code\" description=\"sku code\" required=\"true\" />\n        <column name=\"Qty\" source=\"Qty\" description=\"quantity\" />\n        <column name=\"Item Code\" source=\"Item Code\" description=\"item code\" />\n        <column name=\"Unit Price\" source=\"Unit Price\" description=\"unit price\" />\n        <column name=\"Manufacturing Date\" source=\"Manufacturing Date\" description=\"manufacturing date\" />\n        <column name=\"Item Details\" source=\"Item Details\" description=\"item details\" />\n    </columns>\n</import-config>' where name = "Auto GRN Items";
-- done
-- Aditya | Inventory sanity
-- Item type inventory
SET autocommit = 0;

CREATE TEMPORARY TABLE faulty_facility_ids SELECT DISTINCT (sc.facility_id) FROM item_type_inventory iti JOIN system_configuration sc ON sc.facility_id = iti.facility_id WHERE iti.ageing_start_date <> "1970-01-01" AND sc.name RLIKE 'trac' AND value != 'ITEM';

INSERT IGNORE INTO item_type_inventory SELECT NULL, it1.facility_id, it1.item_type_id, it1.shelf_id, it1.type, "1970-01-01", 0,0,0,0,0,it1.priority, it1.sla, it1.created, it1.updated FROM item_type_inventory it1 WHERE it1.ageing_start_date <> "1970-01-01" AND it1.facility_id IN (select * from faulty_facility_ids);

CREATE TEMPORARY TABLE iti_to_change AS SELECT it1.id AS bad_id, it2.id AS good_id FROM item_type_inventory it1, item_type_inventory it2 WHERE it1.item_type_id = it2.item_type_id AND it1.shelf_id = it2.shelf_id AND it1.type = it2.type AND it2.ageing_start_date = '1970-01-01' AND it1.ageing_start_date <> "1970-01-01" AND it1.facility_id = it2.facility_id AND it1.facility_id IN (select * from faulty_facility_ids);

ALTER TABLE iti_to_change ADD INDEX (bad_id, good_id);

DROP PROCEDURE IF EXISTS update_inventory;
DELIMITER $$
CREATE PROCEDURE update_inventory()
  BEGIN
    DECLARE i INT DEFAULT 0;
    DECLARE size INT DEFAULT 0;

    DECLARE id_bad INT;
    DECLARE id_good INT;
    DECLARE cursor_name CURSOR FOR select * from iti_to_change;

    OPEN cursor_name;
    read_loop: LOOP
      SELECT COUNT(*) FROM iti_to_change INTO size;
      IF i >= size THEN
         LEAVE read_loop;
      END IF;
      SET i = i + 1;
      FETCH cursor_name INTO id_bad, id_good;
     UPDATE item_type_inventory it1, item_type_inventory it2, iti_to_change ic SET it1.quantity = it2.quantity + it1.quantity, it1.quantity_blocked = it2.quantity_blocked + it1.quantity_blocked, it1.quantity_not_found = it2.quantity_not_found + it1.quantity_not_found, it1.quantity_damaged = it2.quantity_damaged + it1.quantity_damaged
      WHERE it1.id = id_good AND it2.id = id_bad;

    END LOOP;
    CLOSE cursor_name;
  END $$
DELIMITER ;
CALL update_inventory();

UPDATE item_type_inventory it1, iti_to_change ic SET it1.quantity = 0, it1.quantity_blocked = 0, it1.quantity_not_found = 0, it1.quantity_damaged = 0 WHERE it1.id = ic.bad_id;

UPDATE sale_order_item soi, iti_to_change ic SET soi.item_type_inventory_id = ic.good_id, soi.updated = soi.updated WHERE soi.item_type_inventory_id = ic.bad_id;

DELETE iti.* FROM item_type_inventory iti, iti_to_change ic WHERE iti.id = ic.bad_id;
COMMIT;

-- Pending putaway items
set autocommit = 0;
create temporary table putaway_ids select distinct(pi.putaway_id) from putaway_item pi join putaway p on p.id = pi.putaway_id join system_configuration sc on sc.facility_id = p.facility_id where pi.ageing_start_date <> '1970-01-01' and pi.status_code != 'COMPLETE' and sc.name = 'traceability.level' and sc.value != 'ITEM';

CREATE temporary TABLE pi_correction LIKE putaway_item;

alter table pi_correction add column skuAgeingShelfPutawayTypeConstraint varchar(200);

alter table pi_correction add constraint unique (`skuAgeingShelfPutawayTypeConstraint`);


insert into pi_correction select pi.id, pi.putaway_id, pi.item_type_id, pi.shelf_id, pi.type, '1970-01-01', pi.quantity, pi.putaway_quantity, pi.status_code, pi.completed_by, pi.created, pi.updated, concat(pi.item_type_id, '-','1970-01-01','-',ifnull(pi.shelf_id, 'NULL'),'-',pi.putaway_id,'-',pi.type) from putaway_item pi join putaway_ids pid on pid.putaway_id = pi.putaway_id where pi.status_code != 'COMPLETE' on duplicate key update quantity = pi.quantity + values(quantity), putaway_quantity = pi.putaway_quantity + values(putaway_quantity);

alter table pi_correction drop column `skuAgeingShelfPutawayTypeConstraint`;

delete pi.* from putaway_item pi, pi_correction pic where pi.putaway_id = pic.putaway_id and pi.status_code != 'COMPLETE';

insert into putaway_item select * from pi_correction pi;
commit;
-- done
