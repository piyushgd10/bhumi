-- Sunny | bulk print regulatory forms
insert ignore into access_pattern select null, ap.access_resource_id, '/oms/shipment/stateRegulatoryForms/print/*', now(), now() from access_pattern ap where url_pattern = '/oms/shipment/stateRegulatoryForm/print/*/*';
-- done

--  Sunny | payment instrument in sale order
alter table sale_order add column payment_instrument enum('CASH', 'CREDIT_CARD', 'DEBIT_CARD', 'NET_BANKING', 'WALLET') after payment_method_code;
-- done

-- Piyush | Sequence table Alter
alter table sequence add column next_year_prefix varchar(45) DEFAULT NULL after prefix, add column reset_counter_next_year tinyint(1) DEFAULT '1' after prefix_expression;
-- done | sunny
