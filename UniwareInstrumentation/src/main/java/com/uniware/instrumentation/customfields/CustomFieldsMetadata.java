/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jun 23, 2012
 *  @author singla
 */
package com.uniware.instrumentation.customfields;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author singla
 */
public class CustomFieldsMetadata {
    private final Map<String, Set<CustomFieldVO>> entityToCustomFields = new HashMap<String, Set<CustomFieldVO>>();

    /**
     * @param entity
     * @param name
     * @param javaType
     * @param sqlType
     * @throws ClassNotFoundException
     */
    public void addCustomField(String entity, String name, String javaType, String sqlType, String defaultValue, boolean serializable) throws ClassNotFoundException {
        Set<CustomFieldVO> customFields = entityToCustomFields.get(entity);
        if (customFields == null) {
            customFields = new HashSet<CustomFieldsMetadata.CustomFieldVO>();
            entityToCustomFields.put(entity, customFields);
        }
        customFields.add(new CustomFieldVO(entity, name, javaType, sqlType, defaultValue, serializable));
    }

    public boolean hasAnyCustomField() {
        return entityToCustomFields.size() > 0;
    }

    public Set<CustomFieldVO> getCustomFields(String entity) {
        return entityToCustomFields.get(entity);
    }

    public static class CustomFieldVO {

        private String   entity;
        private String   name;
        private Class<?> javaType;
        private String   sqlType;
        private String   defaultValue;
        private boolean  serializable;

        /**
         * @param entity
         * @param name
         * @param javaType
         * @param sqlType
         * @throws ClassNotFoundException
         */
        public CustomFieldVO(String entity, String name, String javaType, String sqlType, String defaultValue) throws ClassNotFoundException {
            this.entity = entity;
            this.name = name;
            this.javaType = Class.forName(javaType);
            this.sqlType = sqlType;
            this.defaultValue = defaultValue;
        }

        public CustomFieldVO(String entity, String name, String javaType, String sqlType, String defaultValue, boolean serializable) throws ClassNotFoundException {
            this.entity = entity;
            this.name = name;
            this.javaType = Class.forName(javaType);
            this.sqlType = sqlType;
            this.defaultValue = defaultValue;
            this.serializable = serializable;
        }

        /**
         * @return the entity
         */
        public String getEntity() {
            return entity;
        }

        /**
         * @param entity the entity to set
         */
        public void setEntity(String entity) {
            this.entity = entity;
        }

        /**
         * @return the name
         */
        public String getName() {
            return name;
        }

        /**
         * @param name the name to set
         */
        public void setName(String name) {
            this.name = name;
        }

        /**
         * @return the javaType
         */
        public Class<?> getJavaType() {
            return javaType;
        }

        /**
         * @param javaType the javaType to set
         */
        public void setJavaType(Class<?> javaType) {
            this.javaType = javaType;
        }

        /**
         * @return the sqlType
         */
        public String getSqlType() {
            return sqlType;
        }

        /**
         * @param sqlType the sqlType to set
         */
        public void setSqlType(String sqlType) {
            this.sqlType = sqlType;
        }

        /**
         * @return the defaultValue
         */
        public String getDefaultValue() {
            return defaultValue;
        }

        /**
         * @param defaultValue the defaultValue to set
         */
        public void setDefaultValue(String defaultValue) {
            this.defaultValue = defaultValue;
        }

        public boolean isSerializable() {
            return serializable;
        }

        public void setSerializable(boolean serializable) {
            this.serializable = serializable;
        }

        /* (non-Javadoc)
         * @see java.lang.Object#hashCode()
         */
        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + ((entity == null) ? 0 : entity.hashCode());
            result = prime * result + ((name == null) ? 0 : name.hashCode());
            return result;
        }

        /* (non-Javadoc)
         * @see java.lang.Object#equals(java.lang.Object)
         */
        @Override
        public boolean equals(Object obj) {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            CustomFieldVO other = (CustomFieldVO) obj;
            if (entity == null) {
                if (other.entity != null)
                    return false;
            } else if (!entity.equals(other.entity))
                return false;
            if (name == null) {
                if (other.name != null)
                    return false;
            } else if (!name.equals(other.name))
                return false;
            return true;
        }
    }
}
