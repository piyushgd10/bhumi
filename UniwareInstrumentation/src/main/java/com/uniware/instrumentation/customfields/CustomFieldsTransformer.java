/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jun 22, 2012
 *  @author singla
 */
package com.uniware.instrumentation.customfields;

import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.IllegalClassFormatException;
import java.lang.instrument.Instrumentation;
import java.net.UnknownHostException;
import java.security.ProtectionDomain;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Set;

import javax.naming.NamingException;

import org.apache.tomcat.dbcp.dbcp2.BasicDataSource;

import com.mongodb.*;
import com.uniware.instrumentation.customfields.CustomFieldsMetadata.CustomFieldVO;

import javassist.*;
import javassist.bytecode.AnnotationsAttribute;
import javassist.bytecode.ConstPool;
import javassist.bytecode.annotation.*;

/**
 * @author singla
 */
public class CustomFieldsTransformer implements ClassFileTransformer {

    private static BasicDataSource        dataSource;
    private final Instrumentation         instrumentation;

    private volatile CustomFieldsMetadata customFieldsMetadata;

    /**
     * @param instrumentation
     */
    public CustomFieldsTransformer(Instrumentation instrumentation) {
        this.instrumentation = instrumentation;
        this.instrumentation.addTransformer(this);
    }

    /* (non-Javadoc)
     * @see java.lang.instrument.ClassFileTransformer#transform(java.lang.ClassLoader, java.lang.String, java.lang.Class, java.security.ProtectionDomain, byte[])
     */
    @Override
    public byte[] transform(ClassLoader clazzLoader, String className, Class<?> clazz, ProtectionDomain arg3, byte[] rawBytes) throws IllegalClassFormatException {
        if (className.startsWith("com/uniware/core/entity") && className.indexOf('$') == -1) {
            String packageQualifiedName = className.replaceAll("/", ".");
            CtClass cl = null;
            try {
                ClassPool pool = ClassPool.getDefault();
                boolean customizableEntity = false;
                cl = pool.makeClass(new java.io.ByteArrayInputStream(rawBytes));
                for (Object o : cl.getAnnotations()) {
                    if (o.toString().contains("Customizable")) {
                        customizableEntity = true;
                        break;
                    }
                }
                if (!customizableEntity) {
                    return rawBytes;
                }

                //
                // Instrument customFieldValue field in all the entities marked Customizable to 
                /// add support of adding a non serializable custom field at runtime.
                //
                if (isNonSerializableFieldsSupported()) {
                    instrumentFieldCustomFieldValue(cl, className, packageQualifiedName, rawBytes);
                }

                if (customFieldsMetadata == null) {
                    loadCustomFieldsMetadata();
                }

                // Now load any serializable fields if present.
                Set<CustomFieldVO> customFields = customFieldsMetadata.getCustomFields(packageQualifiedName);
                if (customFields != null && customFields.size() > 0) {
                    System.out.println("Instrumenting class : " + className);
                    for (CustomFieldVO customField : customFields) {
                        if (customField.isSerializable()) {
                            String fieldInfo = getFieldInfo(customField);
                            System.out.println("Adding field: " + fieldInfo);
                            cl.addField(CtField.make(fieldInfo, cl));
                            String setterInfo = getSetterInfo(customField);
                            System.out.println("Adding setter: " + setterInfo);
                            CtMethod setter = CtMethod.make(setterInfo, cl);
                            cl.addMethod(setter);
                            String getterInfo = getGetterInfo(customField);
                            System.out.println("Adding getter: " + getterInfo);
                            CtMethod getter = CtMethod.make(getterInfo, cl);
                            cl.addMethod(getter);
                        }
                    }
                    System.out.println("Done instrumentating class : " + className);
                    return cl.toBytecode();
                }
                return cl.toBytecode();
            } catch (Throwable e) {
                //LOG.error("Could not instrument class: " + className, e);
            } finally {
                if (cl != null) {
                    cl.detach();
                }
            }
        }
        return rawBytes;
    }

    private void instrumentFieldCustomFieldValue(CtClass cl, String className, String packageQualifiedName, byte[] rawBytes) throws ClassNotFoundException, CannotCompileException {
        System.out.println("Instrumenting field customFieldValue to class : " + className);
        CustomFieldVO customFieldValue = new CustomFieldVO(className, "customFieldValue", "com.unifier.core.entity.CustomFieldValue", "varchar(128)", null);
        String fieldInfo = getFieldInfo(customFieldValue);
        System.out.println("Adding field: " + fieldInfo);
        cl.addField(CtField.make(fieldInfo, cl));

        String setterInfo = getSetterInfo(customFieldValue);
        System.out.println("Adding setter: " + setterInfo);
        CtMethod setter = CtMethod.make(setterInfo, cl);
        cl.addMethod(setter);

        String getterInfo = getGetterInfo(customFieldValue);
        System.out.println("Adding getter: " + getterInfo);
        CtMethod getter = CtMethod.make(getterInfo, cl);

        //
        // This attribute has to be fetched in entity with a join, so an annotation 
        // has to be added to the getter method. 
        //
        // The annotations to be added are: 
        //
        // @OneToOne(fetch = FetchType.LAZY)
        // @JoinColumnsOrFormulas(value = {
        //     @JoinColumnOrFormula(column = @JoinColumn(name = "id", referencedColumnName = "identifier", insertable = false, updatable = false)),
        //     @JoinColumnOrFormula(formula = @JoinFormula(referencedColumnName = "entity", value = "'EntityName'")) })
        //
        ConstPool constPool = cl.getClassFile().getConstPool();
        AnnotationsAttribute getterAttr = new AnnotationsAttribute(constPool, AnnotationsAttribute.visibleTag);

        // @OneToOne(fetch = FetchType.LAZY)
        Annotation oneToOneAnnot = new Annotation("javax.persistence.OneToOne", constPool);
        EnumMemberValue e = new EnumMemberValue(constPool);
        e.setType("javax.persistence.FetchType");
        e.setValue("LAZY");
        oneToOneAnnot.addMemberValue("fetch", e);
        getterAttr.addAnnotation(oneToOneAnnot);

        // @JoinColumnsOrFormulas
        Annotation joinColumnsOrFormulasAnnot = new Annotation("org.hibernate.annotations.JoinColumnsOrFormulas", constPool);
        ArrayMemberValue members = new ArrayMemberValue(constPool);
        AnnotationMemberValue[] elements = new AnnotationMemberValue[2];

        // @JoinColumnOrFormula(column = @JoinColumn(name = "id", referencedColumnName = "identifier", insertable = false, updatable = false))
        Annotation joinColumnFormula1 = new Annotation("org.hibernate.annotations.JoinColumnOrFormula", constPool);
        Annotation joinColumn = new Annotation("javax.persistence.JoinColumn", constPool);
        joinColumn.addMemberValue("name", new StringMemberValue("id", constPool));
        joinColumn.addMemberValue("referencedColumnName", new StringMemberValue("identifier", constPool));
        joinColumn.addMemberValue("insertable", new BooleanMemberValue(false, constPool));
        joinColumn.addMemberValue("updatable", new BooleanMemberValue(false, constPool));
        joinColumnFormula1.addMemberValue("column", new AnnotationMemberValue(joinColumn, constPool));
        elements[0] = new AnnotationMemberValue(joinColumnFormula1, constPool);

        // @JoinColumnOrFormula(formula = @JoinFormula(referencedColumnName = "entity", value = "'EntityName'"))
        Annotation joinColumnFormula2 = new Annotation("org.hibernate.annotations.JoinColumnOrFormula", constPool);
        Annotation joinFormula = new Annotation("org.hibernate.annotations.JoinFormula", constPool);
        joinFormula.addMemberValue("referencedColumnName", new StringMemberValue("entity", constPool));
        joinFormula.addMemberValue("value", new StringMemberValue("'" + packageQualifiedName + "'", constPool));
        joinColumnFormula2.addMemberValue("formula", new AnnotationMemberValue(joinFormula, constPool));
        elements[1] = new AnnotationMemberValue(joinColumnFormula2, constPool);

        members.setValue(elements);
        joinColumnsOrFormulasAnnot.addMemberValue("value", members);
        getterAttr.addAnnotation(joinColumnsOrFormulasAnnot);
        getter.getMethodInfo().addAttribute(getterAttr);
        cl.addMethod(getter);
        System.out.println("Done instrumenting field customFieldValue to class : " + className);
    }

    private String getFieldInfo(CustomFieldVO customField) {
        return new StringBuilder("private ").append(customField.getJavaType().getName()).append(' ').append(customField.getName()).append(";").toString();
    }

    private String getGetterInfo(CustomFieldVO customField) {
        return new StringBuilder("public ").append(customField.getJavaType().getName()).append(" get").append(Character.toUpperCase(customField.getName().charAt(0))).append(
                customField.getName().substring(1)).append("(){return this.").append(customField.getName()).append(";}").toString();
    }

    private String getSetterInfo(CustomFieldVO customField) {
        return new StringBuilder("public void set").append(Character.toUpperCase(customField.getName().charAt(0))).append(customField.getName().substring(1)).append("(").append(
                customField.getJavaType().getName()).append(' ').append(customField.getName()).append("){ this.").append(customField.getName()).append(" = ").append(
                        customField.getName()).append(";}").toString();
    }

    private synchronized void loadCustomFieldsMetadata() {
        if (customFieldsMetadata == null) {
            System.out.println("Loading custom fields metadata...");
            customFieldsMetadata = new CustomFieldsMetadata();
            Connection connection = null;
            Statement statement = null;
            ResultSet resultSet = null;
            try {
                connection = getDatabaseConnection();
                statement = connection.createStatement();
                resultSet = statement.executeQuery("select entity, name, java_type, sql_type, default_value, serializable from custom_field_metadata");
                while (resultSet.next()) {
                    customFieldsMetadata.addCustomField(resultSet.getString("entity"), resultSet.getString("name"), resultSet.getString("java_type"),
                            resultSet.getString("sql_type"), resultSet.getString("default_value"), resultSet.getBoolean("serializable"));
                }
            } catch (Exception e) {
                System.out.println("unable to load custom fields metadata");
                System.out.println(e);
            } finally {
                safelyCloseResultSet(resultSet);
                safelyCloseStatement(statement);
                safelyCloseConnection(connection);
            }
            System.out.println("Loaded custom fields metadata... SUCCESSFULLY");
        }
    }

    private boolean isNonSerializableFieldsSupported() {
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            connection = getDatabaseConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery("select value from environment_property where name = 'allow.serializable.custom.fields'");
            if (resultSet.next()) {
                return !resultSet.getBoolean("value");
            }
        } catch (Exception e) {
            System.out.println("unable to load environment property");
            System.out.println(e);
        } finally {
            safelyCloseResultSet(resultSet);
            safelyCloseStatement(statement);
            safelyCloseConnection(connection);
        }
        return true;
    }

    /**
     * @param connection
     */
    private void safelyCloseConnection(Connection connection) {
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
            }
        }
    }

    /**
     * @param statement
     */
    private void safelyCloseStatement(Statement statement) {
        if (statement != null) {
            try {
                statement.close();
            } catch (SQLException e) {
            }
        }
    }

    /**
     * @param resultSet
     */
    private void safelyCloseResultSet(ResultSet resultSet) {
        if (resultSet != null) {
            try {
                resultSet.close();
            } catch (SQLException e) {
            }
        }
    }

    private static Connection getDatabaseConnection() throws NamingException, SQLException, UnknownHostException, ClassNotFoundException {
        if (dataSource == null) {
            System.out.println("Creating datasource");
            MongoClient mongoClient = new MongoClient(new MongoClientURI("mongodb://" + System.getProperty("commonMongoHosts")));
            DB db = mongoClient.getDB("uniwareConfig");
            DBCollection coll = db.getCollection("dataSourceConfiguration");
            BasicDBObject fields = new BasicDBObject();
            fields.put("serverName", System.getProperty("serverName"));
            fields.put("type", "MAIN");
            DBObject dataSourceVO = coll.findOne(fields);
            if (dataSourceVO == null) {
                throw new RuntimeException("Unable to load datasource");
            }
            dataSource = new BasicDataSource();
            dataSource.setUrl((String) dataSourceVO.get("url"));
            dataSource.setUsername((String) dataSourceVO.get("username"));
            dataSource.setPassword((String) dataSourceVO.get("password"));
            dataSource.setDriverClassName((String) dataSourceVO.get("driverClassName"));
        }
        return dataSource.getConnection();
    }

//    public static void main(String[] args) throws ClassNotFoundException, SQLException, UnknownHostException, NamingException {
//        System.out.println("hello");
//        System.setProperty("commonMongoHosts", "localhost:27017");
//        System.setProperty("serverName", "Sunny");
//        Connection connection = getDatabaseConnection();
//        Statement statement = connection.createStatement();
//        ResultSet resultSet = statement.executeQuery("select value from environment_property where name = 'static.content.server.url'");
//        if (resultSet.next()) {
//            System.out.println(resultSet.getString("value"));
//        }
//    }
}
