/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *  @version     1.0, Jun 23, 2012
 *  @author singla
 */
package com.uniware.instrumentation.agent;

import java.lang.instrument.Instrumentation;

import com.uniware.instrumentation.customfields.CustomFieldsTransformer;

/**
 * @author singla
 */
public class InstrumentationAgent {

    public static void premain(String agentArgs, Instrumentation inst) {
        new CustomFieldsTransformer(inst);
    }
}
