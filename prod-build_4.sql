-- Akshay lite view order
update user_datatable_view set display_order = 10 where code = 'ALL_LISTINGS';
update user_datatable_view set display_order = 5 where code = 'SYNC_DISABLED_LITE';
update user_datatable_view set display_order = 1 where code = 'SYNC_ENABLED_LITE';
-- done

-- Akshay vendor invoice
insert ignore into access_pattern select null,ar.id,'/data/inflow/vendorInvoices/get',now(),now() from access_resource ar where ar.name = 'VENDOR_INVOICE';
-- done