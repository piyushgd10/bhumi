package com.uniware.core.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * ShippingProviderCutoff generated by hbm2java
 */
@Entity
@Table(name = "shipping_provider_cutoff", catalog = "uniware")
public class ShippingProviderCutoff implements java.io.Serializable {

    private Integer          id;
    private ShippingProvider shippingProvider;
    private Date             cutoffTime;

    public ShippingProviderCutoff() {
    }

    public ShippingProviderCutoff(ShippingProvider shippingProvider, Date cutoffTime) {
        this.shippingProvider = shippingProvider;
        this.cutoffTime = cutoffTime;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "shipping_provider_id", nullable = false)
    public ShippingProvider getShippingProvider() {
        return this.shippingProvider;
    }

    public void setShippingProvider(ShippingProvider shippingProvider) {
        this.shippingProvider = shippingProvider;
    }

    @Temporal(TemporalType.TIME)
    @Column(name = "cutoff_time", nullable = false, length = 8)
    public Date getCutoffTime() {
        return this.cutoffTime;
    }

    public void setCutoffTime(Date cutoffTime) {
        this.cutoffTime = cutoffTime;
    }

}
