package com.unifier.core.entity;


import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

import com.uniware.core.entity.Facility;
import com.uniware.core.entity.ItemType;

/**
 * ReorderConfig generated by hbm2java
 */
@Entity
@Table(name = "reorder_config", catalog = "uniware", uniqueConstraints = @UniqueConstraint(columnNames = "item_type_id"))
public class ReorderConfig implements java.io.Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 7623762868686687688L;
    
    private Integer  id;
    private Facility facility;
    private ItemType itemType;
    private int      reorderQuantity;
    private int      reorderThreshold;
    private Date     created;
    private Date     updated;

    public ReorderConfig() {
    }

    public ReorderConfig(Facility facility, ItemType itemType, int reorderQuantity, int reorderThreshold, Date created, Date updated) {
        this.facility = facility;
        this.itemType = itemType;
        this.reorderQuantity = reorderQuantity;
        this.reorderThreshold = reorderThreshold;
        this.created = created;
        this.updated = updated;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "facility_id", nullable = false)
    public Facility getFacility() {
        return this.facility;
    }

    public void setFacility(Facility facility) {
        this.facility = facility;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "item_type_id", unique = true, nullable = false)
    public ItemType getItemType() {
        return this.itemType;
    }

    public void setItemType(ItemType itemType) {
        this.itemType = itemType;
    }

    @Column(name = "reorder_quantity", nullable = false)
    public int getReorderQuantity() {
        return this.reorderQuantity;
    }

    public void setReorderQuantity(int reorderQuantity) {
        this.reorderQuantity = reorderQuantity;
    }

    @Column(name = "reorder_threshold", nullable = false)
    public int getReorderThreshold() {
        return this.reorderThreshold;
    }

    public void setReorderThreshold(int reorderThreshold) {
        this.reorderThreshold = reorderThreshold;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created", nullable = false, length = 19)
    public Date getCreated() {
        return this.created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated", nullable = false, length = 19)
    public Date getUpdated() {
        return this.updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

}
