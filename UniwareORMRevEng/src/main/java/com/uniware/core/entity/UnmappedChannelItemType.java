package com.uniware.core.entity;

// Generated Jun 28, 2013 4:58:41 PM by Hibernate Tools 3.4.0.CR1

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

/**
 * UnmappedChannelItemType generated by hbm2java
 */
@Entity
@Table(name = "unmapped_channel_item_type", catalog = "uniware", uniqueConstraints = @UniqueConstraint(columnNames = { "tenant_id", "channel_id", "channel_sku_code" }))
public class UnmappedChannelItemType implements java.io.Serializable {

    private Integer id;
    private Tenant  tenant;
    private Channel channel;
    private String  channelSkuCode;
    private String  channelItemTypeIdentifier;
    private String  channelItemTypeName;
    private String  channelItemTypeUrl;
    private Date    created;
    private Date    updated;

    public UnmappedChannelItemType() {
    }

    public UnmappedChannelItemType(Tenant tenant, Channel channel, String channelSkuCode, Date created, Date updated) {
        this.tenant = tenant;
        this.channel = channel;
        this.channelSkuCode = channelSkuCode;
        this.created = created;
        this.updated = updated;
    }

    public UnmappedChannelItemType(Tenant tenant, Channel channel, String channelSkuCode, String channelItemTypeIdentifier, String channelItemTypeName, String channelItemTypeUrl,
            Date created, Date updated) {
        this.tenant = tenant;
        this.channel = channel;
        this.channelSkuCode = channelSkuCode;
        this.channelItemTypeIdentifier = channelItemTypeIdentifier;
        this.channelItemTypeName = channelItemTypeName;
        this.channelItemTypeUrl = channelItemTypeUrl;
        this.created = created;
        this.updated = updated;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "tenant_id", nullable = false)
    public Tenant getTenant() {
        return this.tenant;
    }

    public void setTenant(Tenant tenant) {
        this.tenant = tenant;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "channel_id", nullable = false)
    public Channel getChannel() {
        return this.channel;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }

    @Column(name = "channel_sku_code", nullable = false, length = 128)
    public String getChannelSkuCode() {
        return this.channelSkuCode;
    }

    public void setChannelSkuCode(String channelSkuCode) {
        this.channelSkuCode = channelSkuCode;
    }

    @Column(name = "channel_item_type_identifier", length = 256)
    public String getChannelItemTypeIdentifier() {
        return this.channelItemTypeIdentifier;
    }

    public void setChannelItemTypeIdentifier(String channelItemTypeIdentifier) {
        this.channelItemTypeIdentifier = channelItemTypeIdentifier;
    }

    @Column(name = "channel_item_type_name", length = 256)
    public String getChannelItemTypeName() {
        return this.channelItemTypeName;
    }

    public void setChannelItemTypeName(String channelItemTypeName) {
        this.channelItemTypeName = channelItemTypeName;
    }

    @Column(name = "channel_item_type_url", length = 256)
    public String getChannelItemTypeUrl() {
        return this.channelItemTypeUrl;
    }

    public void setChannelItemTypeUrl(String channelItemTypeUrl) {
        this.channelItemTypeUrl = channelItemTypeUrl;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created", nullable = false, length = 19)
    public Date getCreated() {
        return this.created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated", nullable = false, length = 19)
    public Date getUpdated() {
        return this.updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

}
