package com.uniware.core.entity;

// Generated Sep 30, 2013 4:58:45 PM by Hibernate Tools 3.4.0.CR1

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * ChannelPriceMaster generated by hbm2java
 */
@Entity
@Table(name = "channel_price_master", catalog = "uniware")
public class ChannelPriceMaster implements java.io.Serializable {

    private Integer            id;
    private Tenant             tenant;
    private ChannelPriceMaster channelPriceMaster;
    private String             name;
    private String             description;
    private Date               validFrom;
    private Date               validUpto;
    private String             transferPriceFormula;
    private Date               created;
    private Date               updated;
    private Set                channelPriceMasterItemTypes = new HashSet(0);
    private Set                channelPriceMasters         = new HashSet(0);

    public ChannelPriceMaster() {
    }

    public ChannelPriceMaster(Tenant tenant, String name, Date validFrom, Date validUpto, String transferPriceFormula, Date created, Date updated) {
        this.tenant = tenant;
        this.name = name;
        this.validFrom = validFrom;
        this.validUpto = validUpto;
        this.transferPriceFormula = transferPriceFormula;
        this.created = created;
        this.updated = updated;
    }

    public ChannelPriceMaster(Tenant tenant, ChannelPriceMaster channelPriceMaster, String name, String description, Date validFrom, Date validUpto, String transferPriceFormula,
            Date created, Date updated, Set channelPriceMasterItemTypes, Set channelPriceMasters) {
        this.tenant = tenant;
        this.channelPriceMaster = channelPriceMaster;
        this.name = name;
        this.description = description;
        this.validFrom = validFrom;
        this.validUpto = validUpto;
        this.transferPriceFormula = transferPriceFormula;
        this.created = created;
        this.updated = updated;
        this.channelPriceMasterItemTypes = channelPriceMasterItemTypes;
        this.channelPriceMasters = channelPriceMasters;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "tenant_id", nullable = false)
    public Tenant getTenant() {
        return this.tenant;
    }

    public void setTenant(Tenant tenant) {
        this.tenant = tenant;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "parent_price_master_id")
    public ChannelPriceMaster getChannelPriceMaster() {
        return this.channelPriceMaster;
    }

    public void setChannelPriceMaster(ChannelPriceMaster channelPriceMaster) {
        this.channelPriceMaster = channelPriceMaster;
    }

    @Column(name = "name", nullable = false, length = 256)
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "description", length = 65535)
    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "valid_from", nullable = false, length = 19)
    public Date getValidFrom() {
        return this.validFrom;
    }

    public void setValidFrom(Date validFrom) {
        this.validFrom = validFrom;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "valid_upto", nullable = false, length = 19)
    public Date getValidUpto() {
        return this.validUpto;
    }

    public void setValidUpto(Date validUpto) {
        this.validUpto = validUpto;
    }

    @Column(name = "transfer_price_formula", nullable = false, length = 256)
    public String getTransferPriceFormula() {
        return this.transferPriceFormula;
    }

    public void setTransferPriceFormula(String transferPriceFormula) {
        this.transferPriceFormula = transferPriceFormula;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created", nullable = false, length = 19)
    public Date getCreated() {
        return this.created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated", nullable = false, length = 19)
    public Date getUpdated() {
        return this.updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "channelPriceMaster")
    public Set getChannelPriceMasterItemTypes() {
        return this.channelPriceMasterItemTypes;
    }

    public void setChannelPriceMasterItemTypes(Set channelPriceMasterItemTypes) {
        this.channelPriceMasterItemTypes = channelPriceMasterItemTypes;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "channelPriceMaster")
    public Set getChannelPriceMasters() {
        return this.channelPriceMasters;
    }

    public void setChannelPriceMasters(Set channelPriceMasters) {
        this.channelPriceMasters = channelPriceMasters;
    }

}
