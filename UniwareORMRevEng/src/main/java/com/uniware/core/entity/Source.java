package com.uniware.core.entity;

// Generated 14 May, 2013 3:55:42 PM by Hibernate Tools 3.4.0.CR1

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

/**
 * Source generated by hbm2java
 */
@Entity
@Table(name = "source", catalog = "uniware", uniqueConstraints = @UniqueConstraint(columnNames = "code"))
public class Source implements java.io.Serializable {

    private Integer                     id;
    private String                      code;
    private String                      name;
    private String                      saleOrderListScriptName;
    private String                      saleOrderDetailsScriptName;
    private String                      notificationScriptName;
    private String                      preConfigurationScriptName;
    private String                      postConfigurationScriptName;
    private String                      userVerificationScriptName;
    private String                      shippingProviderAllocationScriptName;
    private String                      dispatchVerificationScriptName;
    private boolean                     orderSyncConfigured;
    private boolean                     enabled;
    private boolean                     thirdPartyConfigurationRequired;
    private byte                        priority;
    private String                      instructions;
    private Date                        created;
    private Date                        updated;
    private Set<SourceShippingProvider> sourceShippingProviders = new HashSet<SourceShippingProvider>(0);
    private Set<ShippingManifest>       shippingManifests       = new HashSet<ShippingManifest>(0);
    private Set<UnmappedSourceItemType> unmappedSourceItemTypes = new HashSet<UnmappedSourceItemType>(0);
    private Set<SourceItemType>         sourceItemTypes         = new HashSet<SourceItemType>(0);
    private Set<SourceParameter>        sourceParameters        = new HashSet<SourceParameter>(0);
    private Set<Channel>                channels                = new HashSet<Channel>(0);
    private Set<SaleOrder>              saleOrders              = new HashSet<SaleOrder>(0);

    public Source() {
    }

    public Source(String code, String name, boolean orderSyncConfigured, boolean enabled, boolean thirdPartyConfigurationRequired, byte priority, Date created, Date updated) {
        this.code = code;
        this.name = name;
        this.orderSyncConfigured = orderSyncConfigured;
        this.enabled = enabled;
        this.thirdPartyConfigurationRequired = thirdPartyConfigurationRequired;
        this.priority = priority;
        this.created = created;
        this.updated = updated;
    }

    public Source(String code, String name, String saleOrderListScriptName, String saleOrderDetailsScriptName, String notificationScriptName, String preConfigurationScriptName,
            String postConfigurationScriptName, String userVerificationScriptName, String shippingProviderAllocationScriptName, String dispatchVerificationScriptName,
            boolean orderSyncConfigured, boolean enabled, boolean thirdPartyConfigurationRequired, byte priority, String instructions, Date created, Date updated,
            Set<SourceShippingProvider> sourceShippingProviders, Set<ShippingManifest> shippingManifests, Set<UnmappedSourceItemType> unmappedSourceItemTypes,
            Set<SourceItemType> sourceItemTypes, Set<SourceParameter> sourceParameters, Set<Channel> channels, Set<SaleOrder> saleOrders) {
        this.code = code;
        this.name = name;
        this.saleOrderListScriptName = saleOrderListScriptName;
        this.saleOrderDetailsScriptName = saleOrderDetailsScriptName;
        this.notificationScriptName = notificationScriptName;
        this.preConfigurationScriptName = preConfigurationScriptName;
        this.postConfigurationScriptName = postConfigurationScriptName;
        this.userVerificationScriptName = userVerificationScriptName;
        this.shippingProviderAllocationScriptName = shippingProviderAllocationScriptName;
        this.dispatchVerificationScriptName = dispatchVerificationScriptName;
        this.orderSyncConfigured = orderSyncConfigured;
        this.enabled = enabled;
        this.thirdPartyConfigurationRequired = thirdPartyConfigurationRequired;
        this.priority = priority;
        this.instructions = instructions;
        this.created = created;
        this.updated = updated;
        this.sourceShippingProviders = sourceShippingProviders;
        this.shippingManifests = shippingManifests;
        this.unmappedSourceItemTypes = unmappedSourceItemTypes;
        this.sourceItemTypes = sourceItemTypes;
        this.sourceParameters = sourceParameters;
        this.channels = channels;
        this.saleOrders = saleOrders;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "code", unique = true, nullable = false, length = 45)
    public String getCode() {
        return this.code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Column(name = "name", nullable = false, length = 100)
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "sale_order_list_script_name", length = 256)
    public String getSaleOrderListScriptName() {
        return this.saleOrderListScriptName;
    }

    public void setSaleOrderListScriptName(String saleOrderListScriptName) {
        this.saleOrderListScriptName = saleOrderListScriptName;
    }

    @Column(name = "sale_order_details_script_name", length = 256)
    public String getSaleOrderDetailsScriptName() {
        return this.saleOrderDetailsScriptName;
    }

    public void setSaleOrderDetailsScriptName(String saleOrderDetailsScriptName) {
        this.saleOrderDetailsScriptName = saleOrderDetailsScriptName;
    }

    @Column(name = "notification_script_name", length = 256)
    public String getNotificationScriptName() {
        return this.notificationScriptName;
    }

    public void setNotificationScriptName(String notificationScriptName) {
        this.notificationScriptName = notificationScriptName;
    }

    @Column(name = "pre_configuration_script_name", length = 256)
    public String getPreConfigurationScriptName() {
        return this.preConfigurationScriptName;
    }

    public void setPreConfigurationScriptName(String preConfigurationScriptName) {
        this.preConfigurationScriptName = preConfigurationScriptName;
    }

    @Column(name = "post_configuration_script_name", length = 256)
    public String getPostConfigurationScriptName() {
        return this.postConfigurationScriptName;
    }

    public void setPostConfigurationScriptName(String postConfigurationScriptName) {
        this.postConfigurationScriptName = postConfigurationScriptName;
    }

    @Column(name = "user_verification_script_name", length = 256)
    public String getUserVerificationScriptName() {
        return this.userVerificationScriptName;
    }

    public void setUserVerificationScriptName(String userVerificationScriptName) {
        this.userVerificationScriptName = userVerificationScriptName;
    }

    @Column(name = "shipping_provider_allocation_script_name", length = 256)
    public String getShippingProviderAllocationScriptName() {
        return this.shippingProviderAllocationScriptName;
    }

    public void setShippingProviderAllocationScriptName(String shippingProviderAllocationScriptName) {
        this.shippingProviderAllocationScriptName = shippingProviderAllocationScriptName;
    }

    @Column(name = "dispatch_verification_script_name", length = 256)
    public String getDispatchVerificationScriptName() {
        return this.dispatchVerificationScriptName;
    }

    public void setDispatchVerificationScriptName(String dispatchVerificationScriptName) {
        this.dispatchVerificationScriptName = dispatchVerificationScriptName;
    }

    @Column(name = "order_sync_configured", nullable = false)
    public boolean isOrderSyncConfigured() {
        return this.orderSyncConfigured;
    }

    public void setOrderSyncConfigured(boolean orderSyncConfigured) {
        this.orderSyncConfigured = orderSyncConfigured;
    }

    @Column(name = "enabled", nullable = false)
    public boolean isEnabled() {
        return this.enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Column(name = "third_party_configuration_required", nullable = false)
    public boolean isThirdPartyConfigurationRequired() {
        return this.thirdPartyConfigurationRequired;
    }

    public void setThirdPartyConfigurationRequired(boolean thirdPartyConfigurationRequired) {
        this.thirdPartyConfigurationRequired = thirdPartyConfigurationRequired;
    }

    @Column(name = "priority", nullable = false)
    public byte getPriority() {
        return this.priority;
    }

    public void setPriority(byte priority) {
        this.priority = priority;
    }

    @Column(name = "instructions", length = 65535)
    public String getInstructions() {
        return this.instructions;
    }

    public void setInstructions(String instructions) {
        this.instructions = instructions;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created", nullable = false, length = 19)
    public Date getCreated() {
        return this.created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated", nullable = false, length = 19)
    public Date getUpdated() {
        return this.updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "source")
    public Set<SourceShippingProvider> getSourceShippingProviders() {
        return this.sourceShippingProviders;
    }

    public void setSourceShippingProviders(Set<SourceShippingProvider> sourceShippingProviders) {
        this.sourceShippingProviders = sourceShippingProviders;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "source")
    public Set<ShippingManifest> getShippingManifests() {
        return this.shippingManifests;
    }

    public void setShippingManifests(Set<ShippingManifest> shippingManifests) {
        this.shippingManifests = shippingManifests;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "source")
    public Set<UnmappedSourceItemType> getUnmappedSourceItemTypes() {
        return this.unmappedSourceItemTypes;
    }

    public void setUnmappedSourceItemTypes(Set<UnmappedSourceItemType> unmappedSourceItemTypes) {
        this.unmappedSourceItemTypes = unmappedSourceItemTypes;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "source")
    public Set<SourceItemType> getSourceItemTypes() {
        return this.sourceItemTypes;
    }

    public void setSourceItemTypes(Set<SourceItemType> sourceItemTypes) {
        this.sourceItemTypes = sourceItemTypes;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "source")
    public Set<SourceParameter> getSourceParameters() {
        return this.sourceParameters;
    }

    public void setSourceParameters(Set<SourceParameter> sourceParameters) {
        this.sourceParameters = sourceParameters;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "source")
    public Set<Channel> getChannels() {
        return this.channels;
    }

    public void setChannels(Set<Channel> channels) {
        this.channels = channels;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "source")
    public Set<SaleOrder> getSaleOrders() {
        return this.saleOrders;
    }

    public void setSaleOrders(Set<SaleOrder> saleOrders) {
        this.saleOrders = saleOrders;
    }

}
