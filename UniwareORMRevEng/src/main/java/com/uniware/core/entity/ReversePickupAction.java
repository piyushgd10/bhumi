package com.uniware.core.entity;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * ReversePickupAction generated by hbm2java
 */
@Entity
@Table(name = "reverse_pickup_action", catalog = "uniware", uniqueConstraints = @UniqueConstraint(columnNames = "code"))
public class ReversePickupAction implements java.io.Serializable {

    private Integer            id;
    private String             code;
    private String             description;
    private Set<ReversePickup> reversePickups = new HashSet<ReversePickup>(0);

    public ReversePickupAction() {
    }

    public ReversePickupAction(String code) {
        this.code = code;
    }

    public ReversePickupAction(String code, String description, Set<ReversePickup> reversePickups) {
        this.code = code;
        this.description = description;
        this.reversePickups = reversePickups;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "code", unique = true, nullable = false, length = 45)
    public String getCode() {
        return this.code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Column(name = "description", length = 45)
    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "reversePickupAction")
    public Set<ReversePickup> getReversePickups() {
        return this.reversePickups;
    }

    public void setReversePickups(Set<ReversePickup> reversePickups) {
        this.reversePickups = reversePickups;
    }

}
