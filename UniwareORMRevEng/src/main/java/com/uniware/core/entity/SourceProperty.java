package com.uniware.core.entity;

// Generated Aug 27, 2013 5:28:44 PM by Hibernate Tools 3.4.0.CR1

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * SourceProperty generated by hbm2java
 */
@Entity
@Table(name = "source_property", catalog = "uniware", uniqueConstraints = @UniqueConstraint(columnNames = { "source_code", "name" }))
public class SourceProperty implements java.io.Serializable {

    private Integer id;
    private Source  source;
    private String  name;
    private String  value;

    public SourceProperty() {
    }

    public SourceProperty(Source source, String name, String value) {
        this.source = source;
        this.name = name;
        this.value = value;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "source_code", nullable = false)
    public Source getSource() {
        return this.source;
    }

    public void setSource(Source source) {
        this.source = source;
    }

    @Column(name = "name", nullable = false, length = 45)
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "value", nullable = false, length = 256)
    public String getValue() {
        return this.value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
