package com.uniware.core.entity;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

/**
 * ItemTypeInventory generated by hbm2java
 */
@Entity
@Table(name = "item_type_inventory", catalog = "uniware", uniqueConstraints = @UniqueConstraint(columnNames = { "facility_id", "item_type_id", "shelf_id" }))
public class ItemTypeInventory implements java.io.Serializable {

    private Integer            id;
    private Shelf              shelf;
    private Facility           facility;
    private ItemType           itemType;
    private int                quantity;
    private int                quantityNotFound;
    private int                quantityDamaged;
    private int                quantityLost;
    private int                priority;
    private Date               created;
    private Date               updated;
    private Set<SaleOrderItem> saleOrderItems = new HashSet<SaleOrderItem>(0);

    public ItemTypeInventory() {
    }

    public ItemTypeInventory(Shelf shelf, Facility facility, ItemType itemType, int quantity, int quantityNotFound, int quantityDamaged, int quantityLost, int priority,
            Date created, Date updated) {
        this.shelf = shelf;
        this.facility = facility;
        this.itemType = itemType;
        this.quantity = quantity;
        this.quantityNotFound = quantityNotFound;
        this.quantityDamaged = quantityDamaged;
        this.quantityLost = quantityLost;
        this.priority = priority;
        this.created = created;
        this.updated = updated;
    }

    public ItemTypeInventory(Shelf shelf, Facility facility, ItemType itemType, int quantity, int quantityNotFound, int quantityDamaged, int quantityLost, int priority,
            Date created, Date updated, Set<SaleOrderItem> saleOrderItems) {
        this.shelf = shelf;
        this.facility = facility;
        this.itemType = itemType;
        this.quantity = quantity;
        this.quantityNotFound = quantityNotFound;
        this.quantityDamaged = quantityDamaged;
        this.quantityLost = quantityLost;
        this.priority = priority;
        this.created = created;
        this.updated = updated;
        this.saleOrderItems = saleOrderItems;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "shelf_id", nullable = false)
    public Shelf getShelf() {
        return this.shelf;
    }

    public void setShelf(Shelf shelf) {
        this.shelf = shelf;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "facility_id", nullable = false)
    public Facility getFacility() {
        return this.facility;
    }

    public void setFacility(Facility facility) {
        this.facility = facility;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "item_type_id", nullable = false)
    public ItemType getItemType() {
        return this.itemType;
    }

    public void setItemType(ItemType itemType) {
        this.itemType = itemType;
    }

    @Column(name = "quantity", nullable = false)
    public int getQuantity() {
        return this.quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Column(name = "quantity_not_found", nullable = false)
    public int getQuantityNotFound() {
        return this.quantityNotFound;
    }

    public void setQuantityNotFound(int quantityNotFound) {
        this.quantityNotFound = quantityNotFound;
    }

    @Column(name = "quantity_damaged", nullable = false)
    public int getQuantityDamaged() {
        return this.quantityDamaged;
    }

    public void setQuantityDamaged(int quantityDamaged) {
        this.quantityDamaged = quantityDamaged;
    }

    @Column(name = "quantity_lost", nullable = false)
    public int getQuantityLost() {
        return this.quantityLost;
    }

    public void setQuantityLost(int quantityLost) {
        this.quantityLost = quantityLost;
    }

    @Column(name = "priority", nullable = false)
    public int getPriority() {
        return this.priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created", nullable = false, length = 19)
    public Date getCreated() {
        return this.created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated", nullable = false, length = 19)
    public Date getUpdated() {
        return this.updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "itemTypeInventory")
    public Set<SaleOrderItem> getSaleOrderItems() {
        return this.saleOrderItems;
    }

    public void setSaleOrderItems(Set<SaleOrderItem> saleOrderItems) {
        this.saleOrderItems = saleOrderItems;
    }

}
