package com.uniware.core.entity;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

/**
 * ShippingProviderMethod generated by hbm2java
 */
@Entity
@Table(name = "shipping_provider_method", catalog = "uniware", uniqueConstraints = @UniqueConstraint(columnNames = { "facility_id", "shipping_method_id", "shipping_provider_id" }))
public class ShippingProviderMethod implements java.io.Serializable {

    private Integer                             id;
    private ShippingMethod                      shippingMethod;
    private Facility                            facility;
    private ShippingProvider                    shippingProvider;
    private String                              trackingNumberGeneration;
    private int                                 trackingNumberThreshold;
    private boolean                             enabled;
    private Date                                created;
    private Date                                updated;
    private Set<ShippingProviderTrackingNumber> shippingProviderTrackingNumbers = new HashSet<ShippingProviderTrackingNumber>(0);

    public ShippingProviderMethod() {
    }

    public ShippingProviderMethod(ShippingMethod shippingMethod, Facility facility, ShippingProvider shippingProvider, String trackingNumberGeneration,
            int trackingNumberThreshold, boolean enabled, Date created, Date updated) {
        this.shippingMethod = shippingMethod;
        this.facility = facility;
        this.shippingProvider = shippingProvider;
        this.trackingNumberGeneration = trackingNumberGeneration;
        this.trackingNumberThreshold = trackingNumberThreshold;
        this.enabled = enabled;
        this.created = created;
        this.updated = updated;
    }

    public ShippingProviderMethod(ShippingMethod shippingMethod, Facility facility, ShippingProvider shippingProvider, String trackingNumberGeneration,
            int trackingNumberThreshold, boolean enabled, Date created, Date updated, Set<ShippingProviderTrackingNumber> shippingProviderTrackingNumbers) {
        this.shippingMethod = shippingMethod;
        this.facility = facility;
        this.shippingProvider = shippingProvider;
        this.trackingNumberGeneration = trackingNumberGeneration;
        this.trackingNumberThreshold = trackingNumberThreshold;
        this.enabled = enabled;
        this.created = created;
        this.updated = updated;
        this.shippingProviderTrackingNumbers = shippingProviderTrackingNumbers;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "shipping_method_id", nullable = false)
    public ShippingMethod getShippingMethod() {
        return this.shippingMethod;
    }

    public void setShippingMethod(ShippingMethod shippingMethod) {
        this.shippingMethod = shippingMethod;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "facility_id", nullable = false)
    public Facility getFacility() {
        return this.facility;
    }

    public void setFacility(Facility facility) {
        this.facility = facility;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "shipping_provider_id", nullable = false)
    public ShippingProvider getShippingProvider() {
        return this.shippingProvider;
    }

    public void setShippingProvider(ShippingProvider shippingProvider) {
        this.shippingProvider = shippingProvider;
    }

    @Column(name = "tracking_number_generation", nullable = false, length = 6)
    public String getTrackingNumberGeneration() {
        return this.trackingNumberGeneration;
    }

    public void setTrackingNumberGeneration(String trackingNumberGeneration) {
        this.trackingNumberGeneration = trackingNumberGeneration;
    }

    @Column(name = "tracking_number_threshold", nullable = false)
    public int getTrackingNumberThreshold() {
        return this.trackingNumberThreshold;
    }

    public void setTrackingNumberThreshold(int trackingNumberThreshold) {
        this.trackingNumberThreshold = trackingNumberThreshold;
    }

    @Column(name = "enabled", nullable = false)
    public boolean isEnabled() {
        return this.enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created", nullable = false, length = 19)
    public Date getCreated() {
        return this.created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated", nullable = false, length = 19)
    public Date getUpdated() {
        return this.updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "shippingProviderMethod")
    public Set<ShippingProviderTrackingNumber> getShippingProviderTrackingNumbers() {
        return this.shippingProviderTrackingNumbers;
    }

    public void setShippingProviderTrackingNumbers(Set<ShippingProviderTrackingNumber> shippingProviderTrackingNumbers) {
        this.shippingProviderTrackingNumbers = shippingProviderTrackingNumbers;
    }

}
