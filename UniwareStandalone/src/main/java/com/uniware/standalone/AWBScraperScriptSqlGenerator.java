/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Sep 1, 2012
 *  @author praveeng
 */
package com.uniware.standalone;

import com.unifier.core.utils.StringUtils;

import java.io.File;
import java.io.FileFilter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;

public class AWBScraperScriptSqlGenerator {

    public static final String NAME_FILTER = ".*bluedart.xml";

    public static void main(String[] args) throws IOException {
        File exportFolder = new File("../UniwareResources/scripts/scraper-awb");
        List<File> filesToProcess = new ArrayList<File>();
        addFilesToProcess(exportFolder, filesToProcess);
        for (File file : filesToProcess) {
            String scraperScript = FileUtils.readFileToString(file);
            Pattern pattern = Pattern.compile("<scraper name=\"(.*?)\">");
            Matcher matcher = pattern.matcher(scraperScript);
            if (matcher.find()) {
                String scriptName = matcher.group(1);
                StringBuilder builder = new StringBuilder();
                builder.append("update shipping_provider set scraper_script = ").append(StringUtils.escapeSql(scraperScript)).append(" where code in (");
                for (String name : StringUtils.split(scriptName, ",")) {
                    builder.append("'" + name.trim() + "',");
                }
                builder.deleteCharAt(builder.length() - 1);
                builder.append(");");
                System.out.println(builder);
            }

        }
    }

    private static void addFilesToProcess(File exportFolder, List<File> filesToProcess) {
        if (!exportFolder.isDirectory()) {
            throw new IllegalArgumentException(exportFolder.getAbsolutePath() + " is not a directory");
        }
        filesToProcess.addAll(Arrays.asList(exportFolder.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File arg0, String arg1) {
                return arg1.startsWith("scraper-") && arg1.matches(NAME_FILTER);
            }
        })));
        for (File childFolder : exportFolder.listFiles(new FileFilter() {
            @Override
            public boolean accept(File arg0) {
                return arg0.isDirectory();
            }
        })) {
            addFilesToProcess(childFolder, filesToProcess);
        }
    }
}
