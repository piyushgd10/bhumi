/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Aug 9, 2012
 *  @author singla
 */
package com.uniware.standalone;

import com.unifier.core.utils.FileUtils;
import com.unifier.scraper.sl.parser.ScraperScriptNode;
import com.unifier.scraper.sl.runtime.ScraperScript;
import com.unifier.scraper.sl.runtime.ScriptExecutionContext;

import java.util.Map;

public class Main {

    private static final String USAGE  = "[-h] -i <script file>";
    private static final String HEADER = "Program to execute a script";
    private static final String FOOTER = "";

    @SuppressWarnings("static-access")
    public static void main(String[] args) throws Exception {
        // Create a Parser
//        CommandLineParser parser = new BasicParser();
//        Options options = new Options();
//        options.addOption("h", "help", false, "Print this usage information");
//        options.addOption(OptionBuilder.isRequired(true).hasArg(true).withArgName("input file").withLongOpt("input").create('i'));
//        options.addOption(OptionBuilder.isRequired(false).hasArg(true).withArgName("parameters").withLongOpt("params").create('p'));
//        // Parse the program arguments
//        CommandLine commandLine = null;
//        try {
//            commandLine = parser.parse(options, args);
//            if (commandLine.hasOption('h')) {
//                printUsage(options);
//                System.exit(0);
//            }
//        } catch (Exception e) {
//            System.out.println("You provided bad program arguments!");
//            printUsage(options);
//            System.exit(1);
//        }
//        if (commandLine != null) {
//            String inputFilePath = commandLine.getOptionValue("i");
//            Map<String, String> params = null;
//            if (StringUtils.isNotBlank(commandLine.getOptionValue("p"))) {
//                params = JsonUtils.jsonToMap(commandLine.getOptionValue("p"));
//            }
//            executeScript(inputFilePath, params);
//        }
    }

    private static void executeScript(String inputFilePath, Map<String, String> params) throws Exception {
        try {
            ScraperScriptNode scriptNode = ScraperScriptNode.parse(FileUtils.getFileAsString(inputFilePath));
            scriptNode.validate();
            ScraperScript scraperScript = scriptNode.compile();
            if (params != null) {
                ScriptExecutionContext.current().getScriptVariables().putAll(params);
            }
            scraperScript.execute();
        } finally {
            ScriptExecutionContext.destroy();
        }
    }

//    private static void printUsage(Options options) {
//        HelpFormatter helpFormatter = new HelpFormatter();
//        helpFormatter.setWidth(200);
//        helpFormatter.printHelp(USAGE, HEADER, options, FOOTER);
//    }
}
