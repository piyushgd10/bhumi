/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Sep 1, 2012
 *  @author singla
 */
package com.uniware.standalone;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.unifier.core.utils.FileUtils;

public class ScriptQueryGenerator {

    public static void main(String[] args) throws IOException {
        File dir = new File("../../Uniware/UniwareResources/scripts/scraper");
        FileFilter filter = new FileFilter() {

            @Override
            public boolean accept(File pathname) {
                String[] channelsArray = { "snapdeal", "flipkart", "tradus", "shopclues", "naaptol", "paytm", "ebay", "amazon", "homeshop18", "rediff", "kartrocket" };
                Set<String> channels = new HashSet<>();
                channels.addAll(Arrays.asList(channelsArray));
                System.out.println(pathname);
                return channels.contains(pathname.getName().substring(0, pathname.getName().indexOf('-')));
            }
        };
        for (File file : dir.listFiles(filter)) {
            String fileContents = FileUtils.getFileAsString(file.getAbsolutePath());
            String fileName = findScriptName(fileContents);
            String decodedScriptVar = "decoded_" + fileName;
            System.out.println(new StringBuilder().append("var ").append(decodedScriptVar).append(" = db.eval('decodeHex (\\\"").append(toHex(fileContents)).append(
                    "\\\")'); \ndb.script.update({name: \"").append(fileName).append("\"}, {$set: {script:").append(decodedScriptVar).append(" }}, {multi : true});"));
        }
    }

    private static String findScriptName(String file) {
        Pattern p = Pattern.compile("<scraper\\s+name\\s*=\\s*\"([^\"]*)\"");
        Matcher m = p.matcher(file);
        if (m.find()) {
            return m.group(1);
        }
        return "";
    }

    public static String toHex(String arg) {
        try {
            return String.format("%040x", new BigInteger(1, arg.getBytes("UTF-8")));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

}
