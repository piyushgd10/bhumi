package com.uniware.standalone;

import java.beans.IntrospectionException;
import java.lang.reflect.InvocationTargetException;

import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateUtils {
    private static SessionFactory sessionFactory;

    @SuppressWarnings("deprecation")
    private static SessionFactory configureSessionFactory() throws HibernateException {
        sessionFactory = new Configuration().configure().buildSessionFactory();
        // final EventListenerRegistry registry = ((SessionFactoryImpl) sessionFactory).getServiceRegistry().getService(EventListenerRegistry.class);
        // registry.getEventListenerGroup(EventType.POST_LOAD).appendListener((PostLoadEventListener) listener);
        return sessionFactory;
    }

    public static SessionFactory getSessionFactory() {
        return configureSessionFactory();
    }

    public static void main(String[] args) throws ClassNotFoundException, HibernateException, NoSuchMethodException, SecurityException, IllegalAccessException,
            IllegalArgumentException, InvocationTargetException, IntrospectionException {
        //        Session session = getSessionFactory().openSession();
        //        Transaction tx = session.beginTransaction();
        //        Query q = session.createQuery("from Country where id = 6");
        //        Country country = (Country) q.uniqueResult();
        //        //Country country = new Country("YY", "YYtestCountry", true, DateUtils.getCurrentTime(), DateUtils.getCurrentTime());
        //        System.out.println(country.getClass().getMethod("getCustomFieldValue").invoke(country));
        //        System.out.println(country.getClass().getMethod("lookupCustomValue", String.class).invoke(country, "test"));
        //
        //        System.out.println(country.getClass().getMethod("setCustomValue", String.class, String.class).invoke(country, new Object[] { "test", "country123" }));
        //        System.out.println(country.getClass().getMethod("lookupCustomValue", String.class).invoke(country, "test"));
        //        country.setPostcodePattern("dasdsadd");
        //        //session.persist(country);
        //        tx.commit();
        //        session.flush();
    }
}
