package com.uniware.standalone;

import com.itextpdf.text.DocumentException;
import com.unifier.core.utils.CollectionUtils;
import com.unifier.core.utils.StringUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFPicture;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.xmlbeans.XmlException;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTPPr;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTRPr;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by karunsingla on 23/12/14.
 */
public class TestPaperGenerator {

    private static int questionN = 0;

    public static void main(String[] args) throws IOException, DocumentException, XmlException {
        XWPFDocument document = new XWPFDocument(new FileInputStream("/Users/sunny/Work/Unicommerce/Uniware/UniwareStandalone/Test Paper Master.docx"));
        Iterator<XWPFParagraph> it = document.getParagraphsIterator();

        Map<String, List<XWPFParagraph>> paragraphMap = new HashMap<>();
        Map<String, String> questionKeys = new HashMap<>();
        Map<Integer, List<Integer>> questionToOptions = new HashMap<>();
        List<XWPFParagraph> paragraphs = null;
        Integer question = null, option = null;
        String questionKey = null;
        while (it.hasNext()) {
            XWPFParagraph paragraph = it.next();
            if (paragraph.getText().contains("--Q")) {
                if (question != null) {
                    paragraphMap.put(question + "-" + option, paragraphs);
                }

                paragraphs = new ArrayList<>();

                String[] params = paragraph.getText().substring(3).split(":");
                question = Integer.parseInt(params[0]);
                option = Integer.parseInt(params[1]);
                questionKey = params[2];
                questionKeys.put(question + "-" + option, questionKey);
                List<Integer> options = questionToOptions.get(question);
                if (options == null) {
                    options = new ArrayList<>();
                    questionToOptions.put(question, options);
                }
                options.add(option);
            } else {
                paragraphs.add(paragraph);
            }
        }
        if (paragraphs.size() > 0) {
            paragraphMap.put(question + "-" + option, paragraphs);
        }

        File file = new File(System.getProperty("user.home") + "/Downloads/TestPapers-" + System.currentTimeMillis());
        if (!file.mkdirs()) {
            System.out.println("Unable to create directories");
            System.exit(0);
        }

        for (int i = 0; i < 400; i++) {
            questionN = 0;
            String paperId = getRandomPaperId(i);
            StringBuilder answerKey = new StringBuilder();
            XWPFDocument output = new XWPFDocument(new FileInputStream("/Users/sunny/Work/Unicommerce/Uniware/UniwareStandalone/Template.docx"));
            for (XWPFParagraph paragraph : output.getParagraphs()) {
                boolean found = false;
                for (XWPFRun xwpfRun : paragraph.getRuns()) {
                    if (xwpfRun.getText(xwpfRun.getTextPosition()).contains("Code:")) {
                        found = true;

                    }
                    if (found) {
                        xwpfRun.setText(paperId);
                        found = false;
                    }
                }
            }
            //            headerD.getParagraphs().get(0).createRun().setText(paperId);
            for (int cQuestion : randomize(CollectionUtils.asList(questionToOptions.keySet()))) {
                int cOption = questionToOptions.get(cQuestion).get((int) (Math.random() * questionToOptions.get(cQuestion).size()));
                answerKey.append(questionKeys.get(cQuestion + "-" + cOption));
                for (XWPFParagraph paragraph : paragraphMap.get(cQuestion + "-" + cOption)) {
                    XWPFParagraph newParagraph = output.createParagraph();
                    cloneParagraph(newParagraph, paragraph);
                }
            }

            output.getHeaderFooterPolicy().getDefaultFooter().getParagraphs().get(1).getRuns().get(0).setText(paperId);
            output.write(new FileOutputStream(file.getPath() + "/Paper - " + paperId + ".docx"));
            System.out.println(paperId + "-" + answerKey.toString());
        }
    }

    private static String getRandomPaperId(int i) {
        return (6000 + i) + "0" + StringUtils.getRandomNumeric(2);
    }

    private static List<Integer> randomize(List<Integer> givenList) {
        List<Integer> randomList = new ArrayList<>();
        while (givenList.size() > 0) {
            int i = (int) (Math.random() * givenList.size());
            randomList.add(givenList.remove(i));
        }
        return randomList;
    }

    public static void cloneParagraph(XWPFParagraph clone, XWPFParagraph source) {
        CTPPr pPr = clone.getCTP().isSetPPr() ? clone.getCTP().getPPr() : clone.getCTP().addNewPPr();
        pPr.set(source.getCTP().getPPr());
        for (XWPFRun r : source.getRuns()) {
            XWPFRun nr = clone.createRun();
            cloneRun(nr, r);
        }
    }

    public static void cloneRun(XWPFRun clone, XWPFRun source) {
        CTRPr rPr = clone.getCTR().isSetRPr() ? clone.getCTR().getRPr() : clone.getCTR().addNewRPr();
        rPr.set(source.getCTR().getRPr());

        for (XWPFPicture picture : source.getEmbeddedPictures()) {
            try {
                clone.addPicture(new ByteArrayInputStream(picture.getPictureData().getData()), picture.getPictureData().getPictureType(), picture.getDescription(),
                        (int) picture.getCTPicture().getSpPr().getXfrm().getExt().getCx(), (int) picture.getCTPicture().getSpPr().getXfrm().getExt().getCy());
            } catch (InvalidFormatException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (source.getText(0) != null && source.getText(0).contains("@N")) {
            questionN++;
            clone.setText(source.getText(0).replaceAll("@N", String.valueOf(questionN)));
        } else {
            clone.setText(source.getText(0));
        }
    }

}
