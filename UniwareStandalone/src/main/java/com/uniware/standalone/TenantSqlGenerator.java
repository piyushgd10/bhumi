/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Aug 9, 2012
 *  @author singla
 */
package com.uniware.standalone;

import com.unifier.core.utils.EncryptionUtils;
import com.unifier.core.utils.FileUtils;
import com.unifier.core.utils.StringUtils;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public class TenantSqlGenerator {

    private static final String                          TOP_LEVEL_TABLE           = "tenant";
    private static Connection                            sourceConnection;
    private static Connection                            destinationConnection;
    private static final String                          SOURCE_HOST               = "localhost";
    private static final String                          SOURCE_USERNAME           = "root";
    private static final String                          SOURCE_PASSWORD           = "uniware";
    private static final String                          SOURCE_DATABASE           = "muniware";
    private static final String                          DESTINATION_HOST          = "localhost";
    private static final String                          DESTINATION_USERNAME      = "root";
    private static final String                          DESTINATION_PASSWORD      = "uniware";
    private static final String                          DESTINATION_DATABASE      = "uniware";
    private static final Integer                         SOURCE_TENANT_ID          = 1;
    private static final Integer                         DESTINATION_TENANT_ID     = 2;
    private static Map<String, Table>                    tableMetadata             = new ConcurrentHashMap<String, TenantSqlGenerator.Table>();
    private static Writer                                fileWriter;
    private static final Map<String, String>             forcedPrecedence          = new HashMap<String, String>();
    static {
        forcedPrecedence.put("party", "facility");
    }

    private static final Map<String, ReferenceProcessor> dynamicReferenceProcessor = new HashMap<String, ReferenceProcessor>();
    static {
        dynamicReferenceProcessor.put("notification.identifier", new ReferenceProcessor() {

            @Override
            public String getReferencedTable(ResultSet resultSet) throws SQLException {
                String entityName = resultSet.getString("entity");
                if ("SaleOrder".equalsIgnoreCase(entityName)) {
                    return "sale_order";
                } else if ("SaleOrderItem".equalsIgnoreCase(entityName)) {
                    return "sale_order_item";
                } else if ("PurchaseOrder".equalsIgnoreCase(entityName)) {
                    return "purchase_order";
                } else if ("ShippingPackage".equalsIgnoreCase(entityName)) {
                    return "shipping_package";
                } else if ("Reshipment".equalsIgnoreCase(entityName)) {
                    return "reshipment";
                } else if ("ShippingManifest".equalsIgnoreCase(entityName)) {
                    return "shipping_manifest";
                } else if ("InflowReceipt".equalsIgnoreCase(entityName)) {
                    return "inflow_receipt";
                } else if ("ReturnManifest".equalsIgnoreCase(entityName)) {
                    return "return_manifest";
                }
                throw new IllegalStateException("unknown entity type:" + entityName);
            }

        });
    }
    private static Set<String>                           tablesToExclude           = new HashSet<String>();
    static {
        //        tablesToExclude.add("sale_order");
        //        tablesToExclude.add("inflow_receipt");
        //        tablesToExclude.add("purchase_order");
        //        tablesToExclude.add("outbound_gate_pass");
        //        tablesToExclude.add("item");
        //        tablesToExclude.add("putaway");
        //        tablesToExclude.add("shipping_manifest");
        //        tablesToExclude.add("address_detail");
        //        tablesToExclude.add("invoice");
        //        tablesToExclude.add("shipment_tracking");
        //        tablesToExclude.add("picklist");
        //        tablesToExclude.add("notification");
        //        tablesToExclude.add("sale_order_flow_summary");
        //        tablesToExclude.add("import_job");
        //        tablesToExclude.add("onetime_task");
        //        tablesToExclude.add("export_job");
        //        tablesToExclude.add("item_type_inventory");
        //        tablesToExclude.add("return_manifest");
        //        tablesToExclude.add("shipping_provider_tracking_number");
        //        tablesToExclude.add("vendor");
        //        tablesToExclude.add("sale_order_item_alternate");
        //        tablesToExclude.add("item_type_conversion");
        //        tablesToExclude.add("user_comment");
        //        tablesToExclude.add("shipping_provider_location");
    }

    public static void main(String[] args) throws Exception {
        fileWriter = new OutputStreamWriter(new FileOutputStream("/Users/karunsingla/Downloads/migrateTenant.sql"), "UTF-8");
        addDumpStartStatements();
        List<String> triggers = addTriggerDropStatements();
        loadTableMetadata();
        loadTableReferences();
        loadTableUniqueKeys();
        loadTableColumns();
        removeExcludedTables();
        List<Table> tablesToProcess = getTablesToProcess();
        Set<String> commonTablesToProcess = new HashSet<String>();
        for (Table table : tableMetadata.values()) {
            if (table.getIrrelevantReferences().size() > 0) {
                for (Map.Entry<String, String> entry : table.getIrrelevantReferences().entrySet()) {
                    Table irrelevantTable = tableMetadata.get(entry.getValue());
                    if (irrelevantTable.getUniqueColumns().size() == 0) {
                        table.getIrrelevantReferences().remove(entry.getKey());
                        table.getHangingChilds().put(entry.getKey(), entry.getValue());
                    } else {
                        commonTablesToProcess.add(irrelevantTable.getName());
                    }
                }
            }
        }

        //        System.exit(0);
        loadDestinationAutoIncrementValues();
        for (String tableName : commonTablesToProcess) {
            Table table = tableMetadata.get(tableName);
            cacheIdMappingWithUniques(table);
        }
        Table tenantTable = tablesToProcess.remove(0);
        tenantTable.idMappings.put(SOURCE_TENANT_ID, DESTINATION_TENANT_ID);
        for (Table table : tablesToProcess) {
            fileWriter.append(" -- Table:" + table.getName()).append("\n");
            serializeRows(table, null);
            fileWriter.flush();
        }

        fileWriter.append("DELIMITER $$\n");
        for (String trigger : triggers) {
            addTriggerRecreateStatements(trigger);
        }
        fileWriter.append("DELIMITER ;\n");
        addDumpEndStatements();
        fileWriter.close();
    }

    private static List<String> addTriggerDropStatements() {
        List<String> triggers = new ArrayList<String>();
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            connection = getDestinationDatabaseConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery("select trigger_name from information_schema.triggers where trigger_schema = '" + DESTINATION_DATABASE + "'");
            while (resultSet.next()) {
                String trigger = resultSet.getString("trigger_name");
                fileWriter.append("drop trigger " + DESTINATION_DATABASE + "." + trigger + ";\n");
                triggers.add(trigger);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            safelyCloseResultSet(resultSet);
            safelyCloseStatement(statement);
        }
        return triggers;
    }

    private static void addTriggerRecreateStatements(String trigger) {
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            connection = getDestinationDatabaseConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery("show create trigger " + DESTINATION_DATABASE + "." + trigger);
            while (resultSet.next()) {
                fileWriter.append(resultSet.getString("SQL Original Statement")).append("$$\n");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            safelyCloseResultSet(resultSet);
            safelyCloseStatement(statement);
        }
    }

    private static void addDumpEndStatements() throws IOException {
        //        fileWriter.append("SET @TRIGGER_CHECKS = 1");
    }

    private static void addDumpStartStatements() throws IOException {
        //        fileWriter.append("SET @TRIGGER_CHECKS = 0");

    }

    private static void cacheIdMappingWithUniques(Table table) {
        try {
            Map<String, Integer> sourceUniqueKeyToIds = getUniqueKeyToIds(getSourceDatabaseConnection(), SOURCE_DATABASE, table);
            Map<String, Integer> destinationUniqueKeyToIds = getUniqueKeyToIds(getDestinationDatabaseConnection(), DESTINATION_DATABASE, table);
            for (Map.Entry<String, Integer> entry : sourceUniqueKeyToIds.entrySet()) {
                if (destinationUniqueKeyToIds.containsKey(entry.getKey())) {
                    table.idMappings.put(entry.getValue(), destinationUniqueKeyToIds.get(entry.getKey()));
                }
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

    private static Map<String, Integer> getUniqueKeyToIds(Connection connection, String database, Table table) {
        Map<String, Integer> uniqueKeyToIds = new HashMap<String, Integer>();
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(createQuery(table, null, database));
            while (resultSet.next()) {
                StringBuilder builder = new StringBuilder();
                for (String column : table.getUniqueColumns()) {
                    builder.append(resultSet.getString(column));
                }
                uniqueKeyToIds.put(builder.toString(), resultSet.getInt("id"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            safelyCloseResultSet(resultSet);
            safelyCloseStatement(statement);
        }
        return uniqueKeyToIds;
    }

    private static void loadTableUniqueKeys() {
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            connection = getSourceDatabaseConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery("select tc.TABLE_NAME TABLE_NAME, group_concat(distinct kcu.COLUMN_NAME) UNIQUE_COLUMNS from information_schema.TABLE_CONSTRAINTS tc, information_schema.KEY_COLUMN_USAGE kcu where tc.CONSTRAINT_NAME = kcu.CONSTRAINT_NAME and tc.CONSTRAINT_TYPE = 'UNIQUE' and tc.TABLE_NAME = kcu.TABLE_NAME and tc.TABLE_SCHEMA = '"
                    + SOURCE_DATABASE + "' group by tc.CONSTRAINT_NAME, tc.TABLE_NAME");
            while (resultSet.next()) {
                String tableName = resultSet.getString("TABLE_NAME");
                String uniqueColumns = resultSet.getString("UNIQUE_COLUMNS");
                tableMetadata.get(tableName).getUniqueColumns().addAll(StringUtils.split(uniqueColumns));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            safelyCloseResultSet(resultSet);
            safelyCloseStatement(statement);
        }
    }

    private static void removeExcludedTables() {
        for (String table : tablesToExclude) {
            tableMetadata.remove(table);
        }
        boolean processingDone = false;
        while (!processingDone) {
            processingDone = true;
            for (Table table : tableMetadata.values()) {
                if (table.references.values().removeAll(tablesToExclude)) {
                    System.out.println("excluded dependent table:" + table.getName());
                    tableMetadata.remove(table.getName());
                    tablesToExclude.add(table.getName());
                    processingDone = false;
                }
            }
        }
    }

    private static void serializeRows(Table table, Integer filteredId) {
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            Connection connection = getSourceDatabaseConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(createQuery(table, filteredId, SOURCE_DATABASE));
            while (resultSet.next()) {
                int previousIdValue = resultSet.getInt("id");
                if (!table.idMappings.containsKey(previousIdValue)) {
                    StringBuilder builder = new StringBuilder();
                    builder.append("INSERT INTO ").append(DESTINATION_DATABASE).append(".").append(table.getName()).append(" (");
                    for (String column : table.getColumns()) {
                        builder.append(column).append(",");
                    }
                    builder.deleteCharAt(builder.length() - 1);
                    builder.append(")").append(" VALUES(");
                    int columnIndex = 0;
                    for (String column : table.getColumns()) {
                        columnIndex++;
                        String referencedTable = null;
                        if (dynamicReferenceProcessor.containsKey(table.getName() + "." + column)) {
                            referencedTable = dynamicReferenceProcessor.get(table.getName() + "." + column).getReferencedTable(resultSet);
                        }
                        if (referencedTable == null) {
                            referencedTable = table.getReferences().get(column);
                        }
                        if (referencedTable == null) {
                            referencedTable = table.getHangingChilds().get(column);
                        }
                        if (referencedTable == null) {
                            referencedTable = table.getIrrelevantReferences().get(column);
                        }
                        if (referencedTable != null) {
                            int previousReferenceId = resultSet.getInt(column);
                            if (previousReferenceId != 0) {
                                Integer nextReferenceId = tableMetadata.get(referencedTable).idMappings.get(previousReferenceId);
                                if (nextReferenceId == null) {
                                    serializeRows(tableMetadata.get(referencedTable), previousReferenceId);
                                    nextReferenceId = tableMetadata.get(referencedTable).idMappings.get(previousReferenceId);
                                }
                                builder.append(nextReferenceId).append(",");
                                if (column.equalsIgnoreCase("id")) {
                                    table.idMappings.put(previousReferenceId, nextReferenceId);
                                }
                            } else {
                                builder.append("NULL,");
                            }
                        } else if (column.equalsIgnoreCase("id")) {
                            int nextAutoIncrement = table.getNextAutoIncrement();
                            builder.append(nextAutoIncrement).append(",");
                            table.idMappings.put(previousIdValue, nextAutoIncrement);
                        } else {
                            if (Types.LONGVARBINARY == resultSet.getMetaData().getColumnType(columnIndex)) {
                                builder.append("UNHEX(\"").append(EncryptionUtils.hexEncode(FileUtils.toByteArray(resultSet.getBlob(column).getBinaryStream()))).append("\"),");
                            } else {
                                builder.append(StringUtils.escapeSql(resultSet.getString(column))).append(",");
                            }
                        }
                    }
                    builder.deleteCharAt(builder.length() - 1).append(");");
                    fileWriter.append(builder.toString()).append("\n");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            safelyCloseResultSet(resultSet);
            safelyCloseStatement(statement);
        }

    }

    private static String createQuery(Table table, Integer filteredId, String database) {
        StringBuilder builder = new StringBuilder();
        builder.append("SELECT ");
        for (String column : table.getColumns()) {
            builder.append(column).append(",");
        }
        builder.deleteCharAt(builder.length() - 1).append(" from ").append(database).append(".").append(table.getName());
        if (filteredId != null) {
            builder.append(" where id = ").append(filteredId);
        } else if (table.getReferences().size() > 0) {
            StringBuilder whereBuilder = new StringBuilder();
            for (Map.Entry<String, String> entry : table.getReferences().entrySet()) {
                if (!table.getReferences().get(entry.getKey()).equalsIgnoreCase(table.getName())
                        && tableMetadata.get(table.getReferences().get(entry.getKey())).idMappings.size() > 0) {
                    whereBuilder.append("(").append(entry.getKey()).append(" is null or ").append(entry.getKey()).append(" in (").append(
                            join(tableMetadata.get(table.getReferences().get(entry.getKey())).idMappings.keySet())).append("))").append(" and ");
                }
            }
            if (whereBuilder.length() > 0) {
                whereBuilder.delete(whereBuilder.length() - 5, whereBuilder.length());
                builder.append(" where ").append(whereBuilder);
            }
        }
        Set<String> selfReferencingKeys = new HashSet<String>();
        for (Map.Entry<String, String> entry : table.getReferences().entrySet()) {
            if (table.getName().equalsIgnoreCase(entry.getValue())) {
                selfReferencingKeys.add(entry.getKey());
            }
        }
        if (selfReferencingKeys.size() > 0) {
            builder.append(" order by ").append(StringUtils.join(',', selfReferencingKeys));
        }
        return builder.toString();
    }

    public static String join(Set<Integer> ids) {
        StringBuilder builder = new StringBuilder();
        for (Integer id : ids) {
            builder.append(id).append(',');
        }
        return builder.deleteCharAt(builder.length() - 1).toString();
    }

    private static void loadDestinationAutoIncrementValues() {
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            connection = getDestinationDatabaseConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery("select TABLE_NAME, AUTO_INCREMENT from information_schema.TABLES where TABLE_SCHEMA = '" + DESTINATION_DATABASE + "'");
            while (resultSet.next()) {
                String tableName = resultSet.getString("TABLE_NAME");
                if (tableMetadata.containsKey(tableName)) {
                    tableMetadata.get(tableName).setAutoIncrement(resultSet.getInt("AUTO_INCREMENT"));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            safelyCloseResultSet(resultSet);
            safelyCloseStatement(statement);
        }
    }

    private static List<Table> getTablesToProcess() {
        Map<String, Table> tableMetadata = new HashMap<String, TenantSqlGenerator.Table>();
        tableMetadata.putAll(TenantSqlGenerator.tableMetadata);
        Set<String> irrelevantReferences = getIrrelevantReferences(tableMetadata);
        while (irrelevantReferences.size() > 0) {
            for (Table table : tableMetadata.values()) {
                for (Map.Entry<String, String> entry : table.getReferences().entrySet()) {
                    if (irrelevantReferences.contains(entry.getValue())) {
                        table.getReferences().remove(entry.getKey());
                        table.getIrrelevantReferences().put(entry.getKey(), entry.getValue());
                    }
                }
            }
            for (String irrelevantReference : irrelevantReferences) {
                tableMetadata.remove(irrelevantReference);
            }
            irrelevantReferences = getIrrelevantReferences(tableMetadata);
        }
        for (Table table : tableMetadata.values()) {
            table.getReferencedTables().addAll(table.getReferences().values());
            table.getReferencedTables().remove(table.getName());
        }
        Map<String, Table> pendingTables = new ConcurrentHashMap<String, TenantSqlGenerator.Table>();
        Set<String> referencesToBeRemoved = new HashSet<String>();
        referencesToBeRemoved.add(TOP_LEVEL_TABLE);
        for (Map.Entry<String, String> entry : forcedPrecedence.entrySet()) {
            tableMetadata.get(entry.getKey()).getReferencedTables().remove(entry.getValue());
        }
        pendingTables.putAll(tableMetadata);
        pendingTables.remove(TOP_LEVEL_TABLE);
        int level = 1;
        boolean circularDependencies = true;
        while (pendingTables.size() > 0) {
            circularDependencies = true;
            for (Table table : pendingTables.values()) {
                table.getReferencedTables().removeAll(referencesToBeRemoved);
                if (table.getReferencedTables().size() == 0) {
                    circularDependencies = false;
                    referencesToBeRemoved.add(table.getName());
                    table.setLevel(level++);
                    pendingTables.remove(table.getName());
                }
            }
            if (circularDependencies) {
                System.out.println("Circular dependencies:" + pendingTables);
            }
        }
        List<Table> tablesToProcess = new ArrayList<TenantSqlGenerator.Table>();
        tablesToProcess.addAll(tableMetadata.values());
        Collections.sort(tablesToProcess, new Comparator<Table>() {

            @Override
            public int compare(Table o1, Table o2) {
                return o1.getLevel() > o2.getLevel() ? 1 : -1;
            }
        });
        return tablesToProcess;
    }

    private static Set<String> getIrrelevantReferences(Map<String, Table> tableMetadata) {
        Set<String> irrelevantReferences = new HashSet<String>();
        for (Table table : tableMetadata.values()) {
            if (table.getReferences().size() == 0 || (table.getReferences().size() == 1 && table.getReferences().values().iterator().next().equals(table.getName()))) {
                irrelevantReferences.add(table.getName());
            }
        }
        irrelevantReferences.remove(TOP_LEVEL_TABLE);
        return irrelevantReferences;
    }

    public static void loadTableMetadata() throws Exception {
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            connection = getSourceDatabaseConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery("select TABLE_NAME, AUTO_INCREMENT from information_schema.TABLES where TABLE_SCHEMA = '" + SOURCE_DATABASE + "'");
            while (resultSet.next()) {
                tableMetadata.put(resultSet.getString("TABLE_NAME"), new Table(resultSet.getString("TABLE_NAME"), resultSet.getInt("AUTO_INCREMENT")));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            safelyCloseResultSet(resultSet);
            safelyCloseStatement(statement);
        }
    }

    public static void loadTableReferences() throws Exception {
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            connection = getSourceDatabaseConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery("select TABLE_NAME, COLUMN_NAME, REFERENCED_TABLE_NAME from information_schema.KEY_COLUMN_USAGE where REFERENCED_COLUMN_NAME = 'id' and TABLE_SCHEMA = '"
                    + SOURCE_DATABASE + "'");
            while (resultSet.next()) {
                String parentTableName = resultSet.getString("REFERENCED_TABLE_NAME");
                String childTableName = resultSet.getString("TABLE_NAME");
                String columnName = resultSet.getString("COLUMN_NAME");
                tableMetadata.get(childTableName).getReferences().put(columnName, parentTableName);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            safelyCloseResultSet(resultSet);
            safelyCloseStatement(statement);
        }
    }

    public static void loadTableColumns() throws Exception {
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            connection = getSourceDatabaseConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery("select TABLE_NAME, COLUMN_NAME from information_schema.COLUMNS where TABLE_SCHEMA = '" + SOURCE_DATABASE
                    + "' order by ORDINAL_POSITION");
            while (resultSet.next()) {
                String tableName = resultSet.getString("TABLE_NAME");
                String columnName = resultSet.getString("COLUMN_NAME");
                tableMetadata.get(tableName).getColumns().add(columnName);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            safelyCloseResultSet(resultSet);
            safelyCloseStatement(statement);
        }
    }

    private static Connection getSourceDatabaseConnection() throws Exception {
        if (sourceConnection == null) {
            Class.forName("com.mysql.jdbc.Driver");
            sourceConnection = DriverManager.getConnection("jdbc:mysql://" + SOURCE_HOST + ":3306/", SOURCE_USERNAME, SOURCE_PASSWORD);
        }
        return sourceConnection;
    }

    private static Connection getDestinationDatabaseConnection() throws Exception {
        if (destinationConnection == null) {
            Class.forName("com.mysql.jdbc.Driver");
            destinationConnection = DriverManager.getConnection("jdbc:mysql://" + DESTINATION_HOST + ":3306/", DESTINATION_USERNAME, DESTINATION_PASSWORD);
        }
        return destinationConnection;
    }

    /**
     * @param connection
     */
    public static void safelyCloseConnection(Connection connection) {
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
            }
        }
    }

    /**
     * @param statement
     */
    public static void safelyCloseStatement(Statement statement) {
        if (statement != null) {
            try {
                statement.close();
            } catch (SQLException e) {
            }
        }
    }

    /**
     * @param resultSet
     */
    public static void safelyCloseResultSet(ResultSet resultSet) {
        if (resultSet != null) {
            try {
                resultSet.close();
            } catch (SQLException e) {
            }
        }
    }

    public static class Table {
        private String                      name;
        private int                         autoIncrement;

        private final List<String>          columns              = new ArrayList<String>();
        private final Map<String, String>   references           = new ConcurrentHashMap<String, String>();
        private final Set<String>           referencedTables     = new HashSet<String>();
        private final Map<String, String>   irrelevantReferences = new ConcurrentHashMap<String, String>();
        private final Map<String, String>   hangingChilds        = new HashMap<String, String>();
        private final List<String>          uniqueColumns        = new ArrayList<String>(0);
        private int                         level                = 0;

        private final Map<Integer, Integer> idMappings           = new HashMap<Integer, Integer>();

        public Table(String name, int autoIncrement) {
            this.name = name;
            this.autoIncrement = autoIncrement;
        }

        /**
         * @return the name
         */
        public String getName() {
            return name;
        }

        /**
         * @param name the name to set
         */
        public void setName(String name) {
            this.name = name;
        }

        /**
         * @return the autoIncrement
         */
        public int getAutoIncrement() {
            return autoIncrement;
        }

        public int getNextAutoIncrement() {
            return autoIncrement++;
        }

        /**
         * @param autoIncrement the autoIncrement to set
         */
        public void setAutoIncrement(int autoIncrement) {
            this.autoIncrement = autoIncrement;
        }

        /* (non-Javadoc)
         * @see java.lang.Object#hashCode()
         */
        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + ((name == null) ? 0 : name.hashCode());
            return result;
        }

        /* (non-Javadoc)
         * @see java.lang.Object#equals(java.lang.Object)
         */
        @Override
        public boolean equals(Object obj) {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            Table other = (Table) obj;
            if (name == null) {
                if (other.name != null)
                    return false;
            } else if (!name.equals(other.name))
                return false;
            return true;
        }

        /**
         * @return the references
         */
        public Map<String, String> getReferences() {
            return references;
        }

        /**
         * @return the references
         */
        public Map<String, String> getIrrelevantReferences() {
            return irrelevantReferences;
        }

        /**
         * @return the level
         */
        public int getLevel() {
            return level;
        }

        /**
         * @param level the level to set
         */
        public void setLevel(int level) {
            this.level = level;
        }

        /* (non-Javadoc)
         * @see java.lang.Object#toString()
         */
        @Override
        public String toString() {
            return "Table [name=" + name + ", references=" + references + ", level=" + level + "]";
        }

        /**
         * @return the referencedTables
         */
        public Set<String> getReferencedTables() {
            return referencedTables;
        }

        /**
         * @return the idMappings
         */
        public Map<Integer, Integer> getIdMappings() {
            return idMappings;
        }

        /**
         * @return the columns
         */
        public List<String> getColumns() {
            return columns;
        }

        /**
         * @return the hangingChilds
         */
        public Map<String, String> getHangingChilds() {
            return hangingChilds;
        }

        /**
         * @return the uniqueColumns
         */
        public List<String> getUniqueColumns() {
            return uniqueColumns;
        }

    }

    interface ReferenceProcessor {
        String getReferencedTable(ResultSet resultSet) throws SQLException;
    }
}
