package com.uniware.standalone;

import java.net.URLDecoder;
import java.util.List;

import com.unifier.core.utils.StringUtils;
import com.unifier.core.utils.XMLParser;
import com.unifier.core.utils.XMLParser.Element;

public class UniwareRequestGenerator {

    @SuppressWarnings("deprecation")
    public static void main(String[] args) {
        XMLParser xmlParser = new XMLParser("/tmp/triveniget.xml");
        Element tamperElement = xmlParser.parse();
        String httpRequestMethod = tamperElement.text("tdRequestMethod");
        StringBuilder postStringBuilder = new StringBuilder();
        String url = null;
        if (httpRequestMethod.equalsIgnoreCase("POST")) {
            url = tamperElement.elementAt("tdRequest").attribute("uri").toString();
            String httpTagElement = "<http url =\"" + URLDecoder.decode(url) + "\"" + "var=\"response\"  method=\"POST\"  timeout=80>";
            postStringBuilder.append(httpTagElement);
        } else {
            url = URLDecoder.decode(tamperElement.elementAt("tdRequest").attribute("uri").toString()).split("\\?")[0];
            String httpTagElement = "<http url =\"" + url + "\"" + "var=\"response\"  method=\"GET\"  timeout=80>";
            postStringBuilder.append(httpTagElement);
        }
        postStringBuilder.append(StringUtils.CHAR_NEW_LINE);
        List<Element> headerList = tamperElement.list("tdRequestHeader");
        for (Element headerElement : headerList) {
            postStringBuilder.append("<header name=\"" + StringUtils.escapeSql(URLDecoder.decode(headerElement.attribute("name").toString())) + "\"" + "value=\""
                    + StringUtils.escapeSql(URLDecoder.decode(headerElement.text().toString())) + "\"/>");
            postStringBuilder.append(StringUtils.CHAR_NEW_LINE);
        }
        if (httpRequestMethod.equalsIgnoreCase("POST")) {
            List<Element> postParamsList = tamperElement.list("tdPostElement");
            for (Element postElement : postParamsList) {
                postStringBuilder.append("<param name=\"" + StringUtils.escapeSql(URLDecoder.decode(postElement.attribute("name").toString())) + "\"" + "value=\""
                        + StringUtils.escapeSql(URLDecoder.decode(postElement.text().toString())) + "\"/>");
                postStringBuilder.append(StringUtils.CHAR_NEW_LINE);
            }
        }
        if (httpRequestMethod.equalsIgnoreCase("GET")) {
            String getUrlParamStringArray[] = URLDecoder.decode(tamperElement.elementAt("tdRequest").attribute("uri").toString()).split("\\?");
            if (getUrlParamStringArray.length == 2) {
                String getUrlParamString = getUrlParamStringArray[1];
                if (getUrlParamString.contains("&")) {
                    String getUrlParams[] = getUrlParamString.split("&");
                    for (String getUrlParam : getUrlParams) {
                        String nameValuePair[] = getUrlParam.split("\\=");
                        if (nameValuePair.length == 2) {
                            postStringBuilder.append("<param name=\"" + StringUtils.escapeSql(URLDecoder.decode(nameValuePair[0])) + "\"" + "value=\""
                                    + StringUtils.escapeSql(URLDecoder.decode(nameValuePair[1])) + "\"/>");
                        } else {
                            postStringBuilder.append("<param name=\"" + StringUtils.escapeSql(URLDecoder.decode(nameValuePair[0])) + "\"" + "value=\"" + "\"/>");
                        }
                        postStringBuilder.append(StringUtils.CHAR_NEW_LINE);
                    }
                }
            }
        }
        postStringBuilder.append(StringUtils.CHAR_NEW_LINE);
        postStringBuilder.append("</http>");
        System.out.println(StringUtils.escapeSql(postStringBuilder.toString()));
    }
}
