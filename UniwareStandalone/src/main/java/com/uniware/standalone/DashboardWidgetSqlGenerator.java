/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Sep 1, 2012
 *  @author singla
 */
package com.uniware.standalone;

import java.io.File;
import java.io.FileFilter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;

import com.unifier.core.utils.StringUtils;

public class DashboardWidgetSqlGenerator {

    public static final String NAME_FILTER = "sale-order-detective.xml";

    public static void main(String[] args) throws IOException {
        File exportFolder = new File("../UniwareResources/scripts/scraper");
        List<File> filesToProcess = new ArrayList<File>();
        addFilesToProcess(exportFolder, filesToProcess);
        for (File file : filesToProcess) {
            String exportSql = FileUtils.readFileToString(file);
            Pattern pattern = Pattern.compile("<scraper name=\"(.*?)\">");
            Matcher matcher = pattern.matcher(exportSql);
            if (matcher.find()) {
                String widgetName = matcher.group(1);
                System.out.println(new StringBuilder().append("update script_config set script = ").append(StringUtils.escapeSql(exportSql)).append(" where name = \"").append(
                        widgetName).append("Script").append("\";"));
            }

        }
    }

    private static void addFilesToProcess(File exportFolder, List<File> filesToProcess) {
        if (!exportFolder.isDirectory()) {
            throw new IllegalArgumentException(exportFolder.getAbsolutePath() + " is not a directory");
        }
        filesToProcess.addAll(Arrays.asList(exportFolder.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File arg0, String arg1) {
                return arg1.matches(NAME_FILTER);
            }
        })));
        for (File childFolder : exportFolder.listFiles(new FileFilter() {
            @Override
            public boolean accept(File arg0) {
                return arg0.isDirectory();
            }
        })) {
            addFilesToProcess(childFolder, filesToProcess);
        }
    }
}
