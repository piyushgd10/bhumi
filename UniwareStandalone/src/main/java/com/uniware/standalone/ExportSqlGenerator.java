/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Sep 1, 2012
 *  @author singla
 */
package com.uniware.standalone;

import java.io.File;
import java.io.FileFilter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.unifier.core.utils.FileUtils;
import com.unifier.core.utils.StringUtils;

public class ExportSqlGenerator {

    public static final String NAME_FILTER = "datatable-channel-item-type.xml";

    public static void main(String[] args) throws IOException {
        String fileContents = FileUtils.getFileAsString("/Users/amit/Desktop/a.txt");
        if (fileContents.contains("table-config")) {
            System.out.println(new StringBuilder().append("update export_job_type set export_job_config = ").append(StringUtils.escapeSql(fileContents)).append(" where name = \"").append(
                    findDatatableName(fileContents)).append("\";"));
        } else if (fileContents.contains("export-config")) {
            String exportName = new File(args[0]).getName().substring("export-".length()).replaceAll("-", " ").replaceAll("\\.xml", "");
            System.out.println(new StringBuilder().append("update export_job_type set export_job_config = ").append(StringUtils.escapeSql(fileContents)).append(" where name = \"").append(
                    exportName).append("\";"));
        } else if (fileContents.contains("import-config")) {
            String fileName = new File(args[0]).getName();
            String importName = fileName.substring(0, fileName.length() - "-import.xml".length()).replaceAll("-", " ");
            System.out.println(new StringBuilder().append("update import_job_type set import_job_config = ").append(StringUtils.escapeSql(fileContents)).append(" where name = \"").append(
                    importName).append("\";"));
        } else if (fileContents.contains("<scraper")) {
            String fileName = findScriptName(fileContents);
            String decodedScriptVar = "decoded_" + fileName;
            String encodedScript = toHex(fileContents);
            StringBuilder queryBuilder = new StringBuilder().append("Change DEFAULT version value in update query \nvar ").append(decodedScriptVar).append(" = db.eval('decodeHex (\\\"").append(encodedScript).append(
                    "\\\")'); \ndb.uniwareScript.update({name: \"").append(fileName).append("\",version : \"DEFAULT\"}, {$set: {script:").append(decodedScriptVar).append(" }});");

            System.out.println(queryBuilder.toString());
        } else if (fileContents.contains("table")) {
            String fileName = findScriptName(fileContents);
            String decodedScriptVar = "decoded_" + fileName;
            String encodedScript = toHex(fileContents);
            StringBuilder queryBuilder = new StringBuilder().append("var ").append(decodedScriptVar).append(" = db.eval('decodeHex (\\\"").append(encodedScript).append(
                    "\\\")'); \ndb.samplePrintTemplate.update({code: \"").append(fileName).append("\"}, {$set: {template:").append(decodedScriptVar).append(" }}, {multi : true});");

            queryBuilder.append("\ndb.commonToTenantSpecificMigrationStatus.find().forEach(function(doc){db.getSiblingDB(doc.tenantCode)['template'].update({code: \"").append(
                    fileName).append("\"}, {$set: {template:").append(decodedScriptVar).append("}}, {multi : true});})");
            System.out.println(queryBuilder.toString());
        } else {
            System.out.println(toHex(fileContents));
            System.out.println(StringUtils.escapeSql(fileContents));
        }

        String fileName = findScriptName(fileContents);
        String decodedScriptVar = "decoded_" + fileName;
        String encodedScript = toHex(fileContents);
        StringBuilder queryBuilder = new StringBuilder().append("var ").append(decodedScriptVar).append(" = db.eval('decodeHex (\\\"").append(encodedScript).append(
                "\\\")'); \ndb.samplePrintTemplate.update({code: \"").append(fileName).append("\"}, {$set: {template:").append(decodedScriptVar).append(" }}, {multi : true});");

        queryBuilder.append("\ndb.commonToTenantSpecificMigrationStatus.find().forEach(function(doc){db.getSiblingDB(doc.tenantCode)['template'].update({code: \"").append(
                fileName).append("\"}, {$set: {template:").append(decodedScriptVar).append("}}, {multi : true});})");
        System.out.println(queryBuilder.toString());
    }

    public static void generateExportConfigSqls() throws IOException {
        File exportFolder = new File("../UniwareResources/exportJobTypes");
        List<File> filesToProcess = new ArrayList<File>();
        addFilesToProcess(exportFolder, filesToProcess);
        for (File file : filesToProcess) {
            String exportSql = FileUtils.getFileAsString(file.getAbsolutePath());
            String name = file.getName().substring("export-".length()).replaceAll("-", " ").replaceAll("\\.xml", "");
            if (file.getName().startsWith("datatable")) {
                name = findDatatableName(exportSql);
            }
            System.out.println(new StringBuilder().append("update export_job_type set export_job_config = ").append(StringUtils.escapeSql(exportSql)).append(" where name = \"").append(
                    name).append("\";"));
        }
    }

    private static String findDatatableName(String file) {
        Pattern p = Pattern.compile("<table-config\\s+name\\s*=\\s*\"([^\"]*)\"");
        Matcher m = p.matcher(file);
        if (m.find()) {
            return m.group(1);
        }
        return "";
    }

    private static String findScriptName(String file) {
        Pattern p = Pattern.compile("<scraper\\s+name\\s*=\\s*\"([^\"]*)\"");
        Matcher m = p.matcher(file);
        if (m.find()) {
            return m.group(1);
        }
        return "temp";
    }

    public static String toHex(String arg) {
        try {
            return String.format("%040x", new BigInteger(1, arg.getBytes("UTF-8")));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static void addFilesToProcess(File exportFolder, List<File> filesToProcess) {
        if (!exportFolder.isDirectory()) {
            throw new IllegalArgumentException(exportFolder.getAbsolutePath() + " is not a directory");
        }
        filesToProcess.addAll(Arrays.asList(exportFolder.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File arg0, String arg1) {
                return arg1.matches(NAME_FILTER);
            }
        })));
        for (File childFolder : exportFolder.listFiles(new FileFilter() {
            @Override
            public boolean accept(File arg0) {
                return arg0.isDirectory();
            }
        })) {
            addFilesToProcess(childFolder, filesToProcess);
        }
    }
}
