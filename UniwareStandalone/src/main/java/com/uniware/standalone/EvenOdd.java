/*
 *  Copyright 2013 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Aug 27, 2013
 *  @author sunny
 */
package com.uniware.standalone;

public class EvenOdd {

    private static Object  o     = new Object();
    private static Integer count = 1;

    public static void main(String[] args) {
        new Thread(new Test(0), "first").start();
        new Thread(new Test(1), "second").start();
    }

    static class Test implements Runnable {
        private int x;

        public Test(int x) {
            super();
            this.x = x;
        }

        public int getX() {
            return x;
        }

        public void setX(int x) {
            this.x = x;
        }

        public void run() {
            while (count < 10) {
                synchronized (o) {
                    if (count % 2 == x) {
                        System.out.println(Thread.currentThread().getName() + ": " + count);
                        count++;
                        o.notify();
                    } else {
                        try {
                            o.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
    }
}
