/*
 *  Copyright 2013 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 06-May-2013
 *  @author Sunny Agarwal
 */
package com.uniware.standalone;

import java.util.Calendar;

//import org.junit.Test;

public class FixedPeriodCronTest {

    Calendar[] referenceDates = new Calendar[1];

    //@Test
    public void testCron() {
        FixedPeriodCron cron = new FixedPeriodCron("30 */2 15 * *");
        System.out.println(Calendar.getInstance().getTime());
        Calendar c = cron.getClosestDateBeforeOrSame(Calendar.getInstance());
        System.out.println(c.getTime());
    }
}
