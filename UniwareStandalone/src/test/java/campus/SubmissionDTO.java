/*
 * Copyright 2014 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  @version     1.0, 12/5/14 9:35 AM
 *  @author amdalal
 */

package campus;

import java.util.Date;
import java.util.List;

public class SubmissionDTO {

    private Long            from;

    private String          questionPaperCode;

    private List<Character> submittedAnswersList;

    private Date            sentTime;

    private int             score = 0;

    public Long getFrom() {
        return from;
    }

    public void setFrom(Long from) {
        this.from = from;
    }

    public String getQuestionPaperCode() {
        return questionPaperCode;
    }

    public void setQuestionPaperCode(String questionPaperCode) {
        this.questionPaperCode = questionPaperCode;
    }

    public List<Character> getSubmittedAnswersList() {
        return submittedAnswersList;
    }

    public void setSubmittedAnswersList(List<Character> submittedAnswersList) {
        this.submittedAnswersList = submittedAnswersList;
    }

    public Date getSentTime() {
        return sentTime;
    }

    public void setSentTime(Date sentTime) {
        this.sentTime = sentTime;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }
}
