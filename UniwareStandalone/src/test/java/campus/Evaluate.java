/*
 * Copyright 2014 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  @version     1.0, 12/5/14 9:35 AM
 *  @author amdalal
 */

package campus;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.unifier.core.fileparser.DelimitedFileParser;
import com.unifier.core.fileparser.Row;
import com.unifier.core.utils.FileUtils;
import com.unifier.core.utils.StringUtils;
import com.unifier.core.utils.XMLParser;

public class Evaluate {

    private static final String          SOLUTIONS_PATH         = "/Users/amdalal/Desktop/solutions.csv";
    private static final String          SMS_DATA_PATH          = "/Users/amdalal/Desktop/sms_data.xml";
    private static final String          RESULT_PATH            = "/Users/amdalal/Desktop/result.csv";
    private static final String          ERROR_PATH             = "/Users/amdalal/Desktop/error.csv";

    private static final int             CORRECT_ANSWER_MARKS   = 2;
    private static final int             INCORRECT_ANSWER_MARKS = -1;

    private static final List<Character> VALID_OPTIONS          = Arrays.asList(new Character[] { 'A', 'B', 'C', 'D' });

    public static void main(String[] args) {
        Map<String, List<Character>> solutionsMap = cacheSolutions();
        SmsDataDTO smsDataDTO = parseSmsData(solutionsMap);
        evaluate(solutionsMap, smsDataDTO.getSubmissionDTOList());
        // write failures in a file
        FileUtils.deleteFileOrDirectory(new File(ERROR_PATH));
        for (Map.Entry<String, String> e : smsDataDTO.getErrorEntries().entrySet()) {
            try {
                String errorSubmission = new StringBuilder(e.getKey()).append(",").append(e.getValue()).append("\n").toString();
                FileUtils.appendToFile(ERROR_PATH, errorSubmission);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        // send sms
        /*
        HttpSender sender = new HttpSender(true);
        String rsp="";
        Map<String, String> params = new HashMap<>();
        try {
            params.put(URLEncoder.encode("user", "UTF-8"), URLEncoder.encode("amdalal", "UTF-8"));
            params.put(URLEncoder.encode("password", "UTF-8"), URLEncoder.encode("790266", "UTF-8"));
            params.put(URLEncoder.encode("msisdn", "UTF-8"), URLEncoder.encode("8527234999", "UTF-8"));
            params.put(URLEncoder.encode("msg", "UTF-8"), URLEncoder.encode("hello", "UTF-8"));
            params.put(URLEncoder.encode("sid", "UTF-8"), URLEncoder.encode("8527234999", "UTF-8"));
            params.put(URLEncoder.encode("fl", "UTF-8"), URLEncoder.encode("0", "UTF-8"));
            System.out.println(sender.executePost("http://smslane.com/vendorsms/pushsms.aspx", params));
            System.out.println("log");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        */
    }

    private static void evaluate(Map<String, List<Character>> solutionsMap, List<SubmissionDTO> submissionDTOList) {
        FileUtils.deleteFileOrDirectory(new File(RESULT_PATH));
        Map<String, SubmissionDTO> questionPaperCodeToSolMap = new HashMap<>(500);
        for (SubmissionDTO sbDTO : submissionDTOList) {
            List<Character> correctSolutionList = solutionsMap.get(sbDTO.getQuestionPaperCode());
            int score = 0;
            if (correctSolutionList == null) {
                System.out.println("[FATAL] Question paper code: " + sbDTO.getQuestionPaperCode() + " not found in solutions cache");
                System.exit(0);
            } else {
                int index = 0;
                for (Character chosenOption : sbDTO.getSubmittedAnswersList()) {
                    if (VALID_OPTIONS.contains(chosenOption)) {
                        if (chosenOption.equals(correctSolutionList.get(index))) {
                            score += CORRECT_ANSWER_MARKS;
                        } else {
                            score += INCORRECT_ANSWER_MARKS;
                        }
                    }
                    index++;
                }
            }
            if (questionPaperCodeToSolMap.get(sbDTO.getQuestionPaperCode()) == null || questionPaperCodeToSolMap.get(sbDTO.getQuestionPaperCode()).getScore() <= score) {
                questionPaperCodeToSolMap.put(sbDTO.getQuestionPaperCode(), sbDTO);
                sbDTO.setScore(score);
            }
        }
        for (Map.Entry<String, SubmissionDTO> e : questionPaperCodeToSolMap.entrySet()) {
            try {
                StringBuilder entry = new StringBuilder(e.getValue().getFrom().toString()).append(",").append(e.getValue().getQuestionPaperCode()).append(",");
                String entryStr = entry.append(e.getValue().getScore()).append(",").append(e.getValue().getSentTime()).append("\n").toString();
                FileUtils.appendToFile(RESULT_PATH, entryStr);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    private static Map<String, List<Character>> cacheSolutions() {
        Map<String, List<Character>> solutionsMap = new HashMap<>(1500);
        DelimitedFileParser dfp = new DelimitedFileParser(SOLUTIONS_PATH, ',', '"', false);
        Iterator<Row> rowIterator = dfp.parse();
        while (rowIterator.hasNext()) {
            Row r = rowIterator.next();
            List<Character> keyList = new ArrayList<>(10);
            for (char c : r.getColumnValue(1).toCharArray()) {
                keyList.add(Character.toUpperCase(c));
            }
            solutionsMap.put(r.getColumnValue(0), keyList);
        }
        return solutionsMap;
    }

    private static SmsDataDTO parseSmsData(Map<String, List<Character>> solutionsMap) {
        SmsDataDTO smsDataDTO = new SmsDataDTO();
        XMLParser.Element element = new XMLParser(SMS_DATA_PATH).parse();
        smsDataDTO.setTotalMessages(Integer.parseInt(element.attribute("count")));

        for (XMLParser.Element e : element.list("sms")) {
            System.out.println("-------------------------------------");
            String mobNum = e.attribute("address").trim();
            System.out.println("[INFO] Reading sms from: " + mobNum);
            String normalizedMobNum = normalizeMobileNumber(mobNum);
            String timestamp = e.attribute("date");
            String sms = e.attribute("body").trim().toUpperCase();
            System.out.println("[INFO] Body: " + sms);
            List<String> splittedSmsList = StringUtils.split(sms, "\\s+");
            if (splittedSmsList.size() != 2) {
                System.out.println("[ERROR] Invalid sms format");
                smsDataDTO.getErrorEntries().put(mobNum, "Please send response in valid format. [QUES_PAPER_CODE 10_CHAR_ANSWER_KEY]");
                continue;
            }
            if (solutionsMap.get(splittedSmsList.get(0)) == null) {
                System.out.println("[ERROR] Invalid question paper code: " + splittedSmsList.get(0));
                smsDataDTO.getErrorEntries().put(mobNum, "Invalid question paper code");
                continue;
            }

            SubmissionDTO submissionDTO = new SubmissionDTO();
            submissionDTO.setFrom(Long.parseLong(normalizedMobNum));
            submissionDTO.setSentTime(new Date(Long.parseLong(timestamp)));
            submissionDTO.setQuestionPaperCode(splittedSmsList.get(0));
            submissionDTO.setSubmittedAnswersList(new ArrayList<Character>(10));
            char[] submissionArr = splittedSmsList.get(1).toCharArray();
            if (submissionArr.length != 10) {
                System.out.println("[ERROR] All answers not submitted. Number of answers: " + submissionArr.length);
                smsDataDTO.getErrorEntries().put(mobNum, "All 10 questions must be answered. Please use hyphen for not attempted questions");
                continue;
            }
            for (char c : submissionArr) {
                submissionDTO.getSubmittedAnswersList().add(Character.valueOf(c));
            }
            smsDataDTO.getSubmissionDTOList().add(submissionDTO);
            System.out.println("[INFO] Processed SUCCESSFULLY!");
        }
        System.out.println("================================================");
        System.out.println("[INFO] Total messages received           : " + smsDataDTO.getTotalMessages());
        System.out.println("[INFO] Total messages parsed successfully: " + smsDataDTO.getSubmissionDTOList().size());
        System.out.println("[INFO] Total invalid messages            : " + smsDataDTO.getErrorEntries().size());
        System.out.println("================================================");
        return smsDataDTO;
    }

    private static String normalizeMobileNumber(String mobNum) {
        mobNum = mobNum.replaceAll("\\s+", "");
        // remove +91
        if (mobNum.startsWith("+")) {
            mobNum = mobNum.substring(3);
        }
        // remove first 0
        if (mobNum.startsWith("0")) {
            mobNum = mobNum.substring(1);
        }
        return mobNum;
    }
}
