/*
 * Copyright 2014 Unicommerce eSolutions (P) Limited All Rights Reserved. 
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  @version     1.0, 12/5/14 9:35 AM
 *  @author amdalal
 */

package campus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by amdalal on 11/28/14.
 */
public class SmsDataDTO {

    private int                 totalMessages;

    private List<SubmissionDTO> submissionDTOList = new ArrayList<>();

    private Map<String, String> errorEntries      = new HashMap<>();

    public int getTotalMessages() {
        return totalMessages;
    }

    public void setTotalMessages(int totalMessages) {
        this.totalMessages = totalMessages;
    }

    public List<SubmissionDTO> getSubmissionDTOList() {
        return submissionDTOList;
    }

    public void setSubmissionDTOList(List<SubmissionDTO> submissionDTOList) {
        this.submissionDTOList = submissionDTOList;
    }

    public Map<String, String> getErrorEntries() {
        return errorEntries;
    }

    public void setErrorEntries(Map<String, String> errorEntries) {
        this.errorEntries = errorEntries;
    }
}
