package com.uniware.database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.PostConstruct;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.apache.tomcat.dbcp.dbcp2.BasicDataSource;
import org.apache.tomcat.jdbc.pool.PoolProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

import com.uniware.core.entity.Product;
import com.uniware.core.entity.Tenant;
import com.uniware.core.utils.UserContext;
import com.uniware.core.vo.DataSourceConfigurationVO;

/**
 * Created by sunny on 04/03/15.
 */
public abstract class UniwareDataSource extends BasicDataSource {

    private static final Logger          LOG                    = LoggerFactory.getLogger(UniwareDataSource.class);

    private String                       serverNames;
    private BasicDataSource              globalDataSource;
    private Set<BasicDataSource>         allDataSources         = new HashSet<>();
    private Map<String, Tenant>          tenantCodeToTenant     = new ConcurrentHashMap<>();
    private Map<String, BasicDataSource> tenantCodeToDataSource = new ConcurrentHashMap<>();

    @PostConstruct
    public void init() {
        Assert.notNull(serverNames, "Missing system property: " + serverNames);
        try {
            loadDataSources();
        } catch (NamingException e) {
            e.printStackTrace();
        }
        Assert.notEmpty(allDataSources, "No dataSource found");
        globalDataSource = allDataSources.iterator().next();
    }

    private void loadDataSources() throws NamingException {
        List<String> servers = Arrays.asList(serverNames.split(","));
        for (DataSourceConfigurationVO dataSourceConfiguration : getDataSourceConfigurations(servers)) {
            BasicDataSource dataSource = createDataSource(dataSourceConfiguration);
            allDataSources.add(dataSource);
            List<String> tenants = loadTenants(dataSource);
            LOG.info("Datasource: {}, tenants: {}", dataSourceConfiguration.getUrl(), tenants);
        }
    }

    public abstract List<DataSourceConfigurationVO> getDataSourceConfigurations(List<String> servers);

    private List<String> loadTenants(BasicDataSource dataSource) {
        List<String> tenants = new ArrayList<>();
        LOG.info("Loading tenants...");
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            connection = dataSource.getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery("select * from tenant");
            while (resultSet.next()) {
                Tenant tenant = prepareTenant(resultSet);
                tenantCodeToTenant.put(tenant.getCode(), tenant);
                tenantCodeToDataSource.put(tenant.getCode(), dataSource);
                tenants.add(tenant.getCode());
                LOG.info("Done loading tenant: {}", tenant.getCode());
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            safelyCloseResultSet(resultSet);
            safelyCloseStatement(statement);
            safelyCloseConnection(connection);
        }
        return tenants;
    }

    private Tenant prepareTenant(ResultSet resultSet) throws Exception {
        Tenant tenant = new Tenant();
        tenant.setId(resultSet.getInt("id"));
        tenant.setCode(resultSet.getString("code"));
        tenant.setName(resultSet.getString("name"));
        tenant.setAccessUrl(resultSet.getString("access_url"));
        tenant.setAccountCode(resultSet.getString("account_code"));
        tenant.setStatusCode(Tenant.StatusCode.valueOf(resultSet.getString("status_code")));
        tenant.setMode(Tenant.Mode.valueOf(resultSet.getString("mode")));
        if (resultSet.getInt("base_tenant_id") != 0) {
            tenant.setBaseTenant(new Tenant(resultSet.getInt("base_tenant_id")));
        }
        tenant.setAccessIpWhitelistString(resultSet.getString("access_ip_whitelist"));
        tenant.setLogoUrl(resultSet.getString("logo_url"));
        tenant.setCompanyName(resultSet.getString("company_name"));
        tenant.setProduct(new Product(resultSet.getString("product_code")));
        tenant.setCreated(resultSet.getDate("created"));
        tenant.setUpdated(resultSet.getDate("updated"));
        tenant.setLastReactivationDate(resultSet.getDate("last_reactivation_date"));
        tenant.setGoLiveDate(resultSet.getDate("go_live_date"));
        tenant.setSetupState(resultSet.getString("setup_state"));
        return tenant;
    }

    private BasicDataSource createDataSource(DataSourceConfigurationVO dsc) {
        LOG.info("Creating datasource {} with configuration: {}", dsc.getServerName(), dsc);
        BasicDataSource datasource = new BasicDataSource();
        datasource.setUsername(dsc.getUsername());
        datasource.setPassword(dsc.getPassword());
        datasource.setUrl(dsc.getUrl());
        datasource.setInitialSize(dsc.getInitialSize());
        datasource.setMaxConnLifetimeMillis(dsc.getMaxConnLifetimeMillis());
        datasource.setMaxIdle(dsc.getMaxIdle());
        datasource.setMinIdle(dsc.getMinIdle());
        datasource.setMaxTotal(dsc.getMaxTotal());
        datasource.setMaxWaitMillis(dsc.getMaxWaitMillis());
        datasource.setDriverClassName(dsc.getDriverClassName());
        datasource.setEvictionPolicyClassName(dsc.getEvictionPolicyClassName());

        datasource.setAbandonedUsageTracking(dsc.isAbandonedUsageTracking());
        datasource.setRemoveAbandonedOnBorrow(dsc.isRemoveAbandonedOnBorrow());
        datasource.setRemoveAbandonedOnMaintenance(dsc.isRemoveAbandonedOnMaintenance());
        datasource.setPoolPreparedStatements(dsc.isPoolPreparedStatements());
        datasource.setMaxOpenPreparedStatements(dsc.getMaxOpenPreparedStatements());
        datasource.setLogAbandoned(dsc.isLogAbandoned());
        datasource.setLifo(dsc.isLifo());
        datasource.setAbandonedUsageTracking(dsc.isAbandonedUsageTracking());
        datasource.setCacheState(dsc.isCacheState());
        datasource.setEnableAutoCommitOnReturn(dsc.isEnableAutoCommitOnReturn());
        datasource.setDefaultCatalog(dsc.getDefaultCatalog());
        datasource.setNumTestsPerEvictionRun(dsc.getNumTestsPerEvictionRun());
        datasource.setConnectionProperties(dsc.getConnectionProperties());
        datasource.setDefaultReadOnly(dsc.isDefaultReadOnly());
        datasource.setAccessToUnderlyingConnectionAllowed(dsc.isAccessToUnderlyingConnectionAllowed());
        datasource.setValidationQueryTimeout(dsc.getValidationQueryTimeout());
        datasource.setValidationQuery(dsc.getValidationQuery());
        datasource.setTimeBetweenEvictionRunsMillis(dsc.getTimeBetweenEvictionRunsMillis());
        datasource.setTestWhileIdle(dsc.isTestWhileIdle());
        datasource.setTestOnReturn(dsc.isTestOnReturn());
        datasource.setTestOnCreate(dsc.isTestOnCreate());
        datasource.setTestOnBorrow(dsc.isTestOnBorrow());
        datasource.setSoftMinEvictableIdleTimeMillis(dsc.getTimeBetweenEvictionRunsMillis());
        datasource.setRollbackOnReturn(dsc.isRollbackOnReturn());
        datasource.setRemoveAbandonedTimeout(dsc.getRemoveAbandonedTimeout());
        datasource.setSoftMinEvictableIdleTimeMillis(dsc.getSoftMinEvictableIdleTimeMillis());

        return datasource;
    }

    @Override
    public Connection getConnection() throws SQLException {
        BasicDataSource dataSource = null;
        Tenant tenant = UserContext.current().getTenant();
        if (tenant == null) {
            return globalDataSource.getConnection();
        } else {
            String tenantCode = tenant.getCode();
            dataSource = tenantCodeToDataSource.get(tenantCode);
            if (dataSource != null) {
                if (LOG.isDebugEnabled()) {
                    LOG.debug("tenantCode: {}, datasource: {}", tenant.getCode(), dataSource.getUrl());
                }
                return tenantCodeToDataSource.get(tenantCode).getConnection();
            }
        }
        return null;
    }

    @Override
    public Connection getConnection(String username, String password) throws SQLException {
        throw new IllegalArgumentException("not supported");
    }

    public List<Tenant> getAllTenants() {
        for (BasicDataSource dataSource : allDataSources) {
            loadTenants(dataSource);
        }
        return Collections.unmodifiableList(new ArrayList<>(tenantCodeToTenant.values()));
    }

    public String getServerNames() {
        return serverNames;
    }

    public void setServerNames(String serverNames) {
        this.serverNames = serverNames;
    }

    /**
     * @param connection
     */
    private void safelyCloseConnection(Connection connection) {
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
            }
        }
    }

    /**
     * @param statement
     */
    private void safelyCloseStatement(Statement statement) {
        if (statement != null) {
            try {
                statement.close();
            } catch (SQLException e) {
            }
        }
    }

    /**
     * @param resultSet
     */
    private void safelyCloseResultSet(ResultSet resultSet) {
        if (resultSet != null) {
            try {
                resultSet.close();
            } catch (SQLException e) {
            }
        }
    }
}
