package com.uniware.database;

import com.uniware.core.entity.Tenant;
import com.uniware.core.utils.UserContext;
import com.uniware.core.vo.DataSourceConfigurationVO;
import com.uniware.dao.datasource.IDataSourceMao;
import org.apache.tomcat.jdbc.pool.PoolProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.AbstractDataSource;
import org.springframework.util.Assert;

import javax.annotation.PostConstruct;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by sunny on 04/03/15.
 */
public class UniwareReplicationDataSource extends UniwareDataSource {

    @Autowired
    private IDataSourceMao dataSourceMao;

    public List<DataSourceConfigurationVO> getDataSourceConfigurations(List<String> servers) {
        return dataSourceMao.getDataSourcesByServerNames(servers, DataSourceConfigurationVO.Type.REPLICATION);
    }
}
