/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 *  @version     1.0, Dec 18, 2011
 *  @author singla
 */
package com.uniware.services.shipping.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.itextpdf.text.DocumentException;
import com.unifier.core.annotation.Level;
import com.unifier.core.annotation.audit.LogActivity;
import com.unifier.core.api.comments.AddCommentRequest;
import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.template.Template;
import com.unifier.core.transport.http.HttpSender;
import com.unifier.core.transport.http.HttpTransportException;
import com.unifier.core.utils.CollectionUtils;
import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.EncryptionUtils;
import com.unifier.core.utils.FileUtils;
import com.unifier.core.utils.JsonUtils;
import com.unifier.core.utils.PdfUtils;
import com.unifier.core.utils.StringUtils;
import com.unifier.core.utils.ValidatorUtils;
import com.unifier.scraper.sl.runtime.ScraperScript;
import com.unifier.scraper.sl.runtime.ScriptExecutionContext;
import com.unifier.services.aspect.RollbackOnFailure;
import com.unifier.services.comments.ICommentsService;
import com.unifier.services.comments.impl.CommentsServiceImpl;
import com.unifier.services.pdf.IPdfDocumentService;
import com.unifier.services.pdf.impl.PdfDocumentServiceImpl;
import com.unifier.services.utils.CustomFieldUtils;
import com.unifier.services.utils.ExceptionUtils;
import com.uniware.core.api.catalog.AddOrEditShippingProviderLocationRequest;
import com.uniware.core.api.catalog.AddOrEditShippingProviderLocationResponse;
import com.uniware.core.api.catalog.CreateShippingProviderLocationRequest;
import com.uniware.core.api.catalog.CreateShippingProviderLocationResponse;
import com.uniware.core.api.catalog.EditShippingProviderLocationRequest;
import com.uniware.core.api.catalog.EditShippingProviderLocationResponse;
import com.uniware.core.api.catalog.SearchShippingProviderLocationResponse.ShippingProviderLocationDTO;
import com.uniware.core.api.invoice.CreateShippingPackageInvoiceRequest;
import com.uniware.core.api.invoice.CreateShippingPackageInvoiceResponse;
import com.uniware.core.api.invoice.WsTaxInformation;
import com.uniware.core.api.item.ItemDetailFieldDTO;
import com.uniware.core.api.model.WsAddressDetail;
import com.uniware.core.api.packer.RegulatoryFormDTO;
import com.uniware.core.api.packer.SaleOrderItemDTO;
import com.uniware.core.api.packer.ShippingPackageFullDTO;
import com.uniware.core.api.saleorder.CreateShippingPackageRequest;
import com.uniware.core.api.saleorder.CreateShippingPackageResponse;
import com.uniware.core.api.saleorder.PageRequest;
import com.uniware.core.api.saleorder.ShippingPackageDTO;
import com.uniware.core.api.shipping.AddShipmentsToBatchRequest;
import com.uniware.core.api.shipping.AddShipmentsToBatchResponse;
import com.uniware.core.api.shipping.AddSignatureToShippingManifestRequest;
import com.uniware.core.api.shipping.AddSignatureToShippingManifestResponse;
import com.uniware.core.api.shipping.CompleteCustomizationForShippingPackageRequest;
import com.uniware.core.api.shipping.CompleteCustomizationForShippingPackageResponse;
import com.uniware.core.api.shipping.CreateBatchRequest;
import com.uniware.core.api.shipping.CreateBatchResponse;
import com.uniware.core.api.shipping.EditShippingPackageRequest;
import com.uniware.core.api.shipping.EditShippingPackageResponse;
import com.uniware.core.api.shipping.EditShippingPackageTrackingNumberRequest;
import com.uniware.core.api.shipping.EditShippingPackageTrackingNumberResponse;
import com.uniware.core.api.shipping.FetchShippingManifestPdfRequest;
import com.uniware.core.api.shipping.FetchShippingManifestPdfResponse;
import com.uniware.core.api.shipping.GeneratePDFShippingLabelRequest;
import com.uniware.core.api.shipping.GeneratePDFShippingLabelResponse;
import com.uniware.core.api.shipping.GenerateShippingLabelsRequest;
import com.uniware.core.api.shipping.GenerateShippingLabelsResponse;
import com.uniware.core.api.shipping.GetApplicableRegulatoryFormsRequest;
import com.uniware.core.api.shipping.GetItemsForDetailingRequest;
import com.uniware.core.api.shipping.GetItemsForDetailingResponse;
import com.uniware.core.api.shipping.GetItemsForDetailingResponse.SaleOrderItemForDetalingDTO;
import com.uniware.core.api.shipping.GetPendingShipmentBatchRequest;
import com.uniware.core.api.shipping.GetPendingShipmentBatchResponse;
import com.uniware.core.api.shipping.GetSalesOrderPackagesRequest;
import com.uniware.core.api.shipping.GetSalesOrderPackagesResponse;
import com.uniware.core.api.shipping.GetShippingPackageDetailRequest;
import com.uniware.core.api.shipping.GetShippingPackageDetailResponse;
import com.uniware.core.api.shipping.GetShippingPackageTypesRequest;
import com.uniware.core.api.shipping.GetShippingPackageTypesResponse;
import com.uniware.core.api.shipping.GetShippingPackagesRequest;
import com.uniware.core.api.shipping.GetShippingPackagesResponse;
import com.uniware.core.api.shipping.MergeShippingPackagesRequest;
import com.uniware.core.api.shipping.MergeShippingPackagesResponse;
import com.uniware.core.api.shipping.ReassessShippingPackageRequest;
import com.uniware.core.api.shipping.ReassessShippingPackageResponse;
import com.uniware.core.api.shipping.ReassessShippingPackageServiceabilityRequest;
import com.uniware.core.api.shipping.ReassessShippingPackageServiceabilityResponse;
import com.uniware.core.api.shipping.SaleOrderDetailDTO;
import com.uniware.core.api.shipping.SearchShippingPackageRequest;
import com.uniware.core.api.shipping.SearchShippingPackageResponse;
import com.uniware.core.api.shipping.SendShippingPackageForCustomizationRequest;
import com.uniware.core.api.shipping.SendShippingPackageForCustomizationResponse;
import com.uniware.core.api.shipping.ShippingPackageDetailDTO;
import com.uniware.core.api.shipping.SplitShippingPackageRequest;
import com.uniware.core.api.shipping.SplitShippingPackageRequest.SplitItem;
import com.uniware.core.api.shipping.SplitShippingPackageResponse;
import com.uniware.core.api.shipping.UpdateShipmentTrackingResponse;
import com.uniware.core.api.shipping.UpdateShipmentTrackingStatusRequest;
import com.uniware.core.api.shipping.UpdateShipmentTrackingStatusResponse;
import com.uniware.core.api.shipping.UpdateShippingPackageTrackingStatusRequest;
import com.uniware.core.api.shipping.UpdateShippingPackageTrackingStatusResponse;
import com.uniware.core.api.shipping.UpdateTrackingStatusRequest;
import com.uniware.core.api.shipping.UpdateTrackingStatusResponse;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.api.warehouse.SearchShipmentRequest;
import com.uniware.core.api.warehouse.SearchShipmentResponse;
import com.uniware.core.api.warehouse.ShippingPackageTypeDTO;
import com.uniware.core.cache.FacilityCache;
import com.uniware.core.entity.Channel;
import com.uniware.core.entity.Facility;
import com.uniware.core.entity.Item;
import com.uniware.core.entity.ItemType;
import com.uniware.core.entity.ItemTypeInventory;
import com.uniware.core.entity.PaymentMethod;
import com.uniware.core.entity.Picklist;
import com.uniware.core.entity.ReturnManifestItem;
import com.uniware.core.entity.SaleOrder;
import com.uniware.core.entity.SaleOrderItem;
import com.uniware.core.entity.Section;
import com.uniware.core.entity.Shelf;
import com.uniware.core.entity.ShipmentBatch;
import com.uniware.core.entity.ShipmentTracking;
import com.uniware.core.entity.ShipmentTrackingStatus;
import com.uniware.core.entity.ShippingManifest;
import com.uniware.core.entity.ShippingMethod;
import com.uniware.core.entity.ShippingPackage;
import com.uniware.core.entity.ShippingPackage.ShippingManager;
import com.uniware.core.entity.ShippingPackageStatus;
import com.uniware.core.entity.ShippingPackageType;
import com.uniware.core.entity.ShippingProvider;
import com.uniware.core.entity.ShippingProviderLocation;
import com.uniware.core.entity.ShippingProviderMethod;
import com.uniware.core.entity.ShippingProviderMethod.TrackingNumberGeneration;
import com.uniware.core.entity.ShippingProviderSource;
import com.uniware.core.entity.Source;
import com.uniware.core.entity.Source.ShipmentLabelFormat;
import com.uniware.core.locking.Namespace;
import com.uniware.core.locking.annotation.Lock;
import com.uniware.core.locking.annotation.Locks;
import com.uniware.core.utils.ActivityContext;
import com.uniware.core.utils.EntityPersistenceContext;
import com.uniware.core.utils.EnvironmentProperties;
import com.uniware.core.utils.UserContext;
import com.uniware.core.vo.PrintTemplateVO;
import com.uniware.core.vo.SamplePrintTemplateVO;
import com.uniware.dao.saleorder.ISaleOrderDao;
import com.uniware.dao.shipping.IShippingDao;
import com.uniware.dao.warehouse.IFacilityDao;
import com.uniware.services.audit.impl.ActivityEntityEnum;
import com.uniware.services.audit.impl.ActivityTypeEnum;
import com.uniware.services.audit.impl.ActivityUtils;
import com.uniware.services.cache.ChannelCache;
import com.uniware.services.cache.ItemTypeDetailCache;
import com.uniware.services.cache.PrintTemplateCache;
import com.uniware.services.cache.SamplePrintTemplateCache;
import com.uniware.services.cache.ScriptVersionedCache;
import com.uniware.services.catalog.ICatalogService;
import com.uniware.services.configuration.PickConfiguration;
import com.uniware.services.configuration.ShippingConfiguration;
import com.uniware.services.configuration.ShippingFacilityLevelConfiguration;
import com.uniware.services.configuration.ShippingSourceConfiguration;
import com.uniware.services.configuration.SourceConfiguration;
import com.uniware.services.configuration.SystemConfiguration;
import com.uniware.services.configuration.SystemConfiguration.TraceabilityLevel;
import com.uniware.services.configuration.data.manager.ProductConfiguration;
import com.uniware.services.document.IDocumentService;
import com.uniware.services.exception.NoAvailableShippingProviderException;
import com.uniware.services.exception.NoAvailableTrackingNumberException;
import com.uniware.services.inventory.IInventoryService;
import com.uniware.services.invoice.IInvoiceService;
import com.uniware.services.picker.IPickerService;
import com.uniware.services.saleorder.ISaleOrderService;
import com.uniware.services.shipping.IDispatchService;
import com.uniware.services.shipping.IRegulatoryService;
import com.uniware.services.shipping.IShipmentTrackingService;
import com.uniware.services.shipping.IShippingInvoiceService;
import com.uniware.services.shipping.IShippingProviderService;
import com.uniware.services.shipping.IShippingService;
import com.uniware.services.shipping.ProviderShipmentStatus;
import com.uniware.services.tenant.ITenantService;

/**
 * @author singla
 */
@Service("shippingService")
public class ShippingServiceImpl implements IShippingService {

    private static final Logger      LOG        = LoggerFactory.getLogger(ShippingServiceImpl.class);
    public static final String       PAGE_BREAK = "<div style=\"page-break-before: always;height:1px;\"></div>";

    @Autowired
    private IShippingDao             shippingDao;

    @Autowired
    private ISaleOrderDao            saleOrderDao;

    @Autowired
    private IFacilityDao             facilityDao;

    @Autowired
    private ICatalogService          catalogService;

    @Autowired
    private IShippingProviderService shippingProviderService;

    @Autowired
    private IInventoryService        inventoryService;

    @Autowired
    private IInvoiceService          invoiceService;

    @Autowired
    private IPickerService           pickerService;

    @Autowired
    private IShipmentTrackingService shipmentTrackingService;

    @Autowired
    private ICommentsService         commentsService;

    @Autowired
    private IRegulatoryService       regulatoryService;

    @Autowired
    private ISaleOrderService        saleOrderService;

    @Autowired
    private ITenantService           tenantService;

    @Autowired
    private IPdfDocumentService      pdfDocumentService;

    @Autowired
    private IDispatchService         dispatchService;

    @Autowired
    private IDocumentService         documentService;

    @Autowired
    private IShippingInvoiceService  iShippingInvoiceService;

    @Override
    public ShippingMethod getShippingMethodByCode(String shippingMethodCode, boolean cashOnDelivery) {
        return ConfigurationManager.getInstance().getConfiguration(ShippingConfiguration.class).getShippingMethod(
                ShippingMethod.Code.valueOf(shippingMethodCode).getName() + "-" + (cashOnDelivery ? PaymentMethod.Code.COD : PaymentMethod.Code.PREPAID));
    }

    @Override
    public ShippingMethod getShippingMethodById(int shippingMethodId) {
        ShippingMethod shippingMethod = ConfigurationManager.getInstance().getConfiguration(ShippingConfiguration.class).getShippingMethodById(shippingMethodId);
        if (shippingMethod != null) {
            return shippingMethod;
        } else {
            return getShippingMethodByIdInternal(shippingMethodId);
        }
    }

    /**
     * @param shippingMethodId
     * @return
     */
    @Transactional(readOnly = true)
    public ShippingMethod getShippingMethodByIdInternal(int shippingMethodId) {
        return shippingDao.getShippingMethodById(shippingMethodId);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.uniware.services.shipping.IShippingService#
     * updateShippingPackageDimensions(com.uniware.core.entity.ShippingPackage)
     */
    @Transactional(readOnly = true)
    private void updateShippingPackageDimensions(ShippingPackage shippingPackage) {
        ShippingPackageType shippingPackageType = ConfigurationManager.getInstance().getConfiguration(ShippingFacilityLevelConfiguration.class).getShippingPackageTypeByCode(
                "DEFAULT");
        shippingPackage.setShippingPackageType(shippingPackageType);
        if (shippingPackage.getSaleOrderItems().size() == 1) {
            ItemType itemType = catalogService.getNonBundledItemTypeById(shippingPackage.getSaleOrderItems().iterator().next().getItemType().getId());
            shippingPackage.setBoxLength(itemType.getLength());
            shippingPackage.setBoxWidth(itemType.getWidth());
            shippingPackage.setBoxHeight(itemType.getHeight());
        } else {
            shippingPackage.setBoxLength(shippingPackageType.getBoxLength());
            shippingPackage.setBoxWidth(shippingPackageType.getBoxWidth());
            shippingPackage.setBoxHeight(shippingPackageType.getBoxHeight());
        }
        //        List<ShippingPackageType> listPackageType = ConfigurationManager.getInstance().getConfiguration(ShippingFacilityLevelConfiguration.class).getShippingPackageTypes();
        //        if (listPackageType.size() == 1) {
        //            return listPackageType.get(0);
        //        }
        //        List<Cuboid> items = new ArrayList<Cuboid>();
        //        for (SaleOrderItem saleOrderItem : shippingPackage.getSaleOrderItems()) {
        //            ItemType itemType = catalogService.getNonBundledItemTypeById(saleOrderItem.getItemType().getId());
        //            //changing item dimensions to CM
        //            Cuboid item = new Cuboid((int) Math.ceil(itemType.getLength() / 10), (int) Math.ceil(itemType.getWidth() / 10), (int) Math.ceil(itemType.getHeight() / 10));
        //            items.add(item);
        //        }
        //
        //        List<PackageBox> packageBoxes = new ArrayList<PackagingUtils.PackageBox>();
        //        for (ShippingPackageType shippingPackageType : listPackageType) {
        //            packageBoxes.add(new PackageBox(shippingPackageType.getCode(), (int) Math.floor(shippingPackageType.getBoxLength() / 10),
        //                    (int) Math.floor(shippingPackageType.getBoxWidth() / 10), (int) Math.floor(shippingPackageType.getBoxHeight()) / 10));
        //        }
        //        PackageBox box = PackagingUtils.getBestFitPackage(items, packageBoxes);
        //        if (box == null) {
        //            return ConfigurationManager.getInstance().getConfiguration(ShippingFacilityLevelConfiguration.class).getShippingPackageTypeByCode("DEFAULT");
        //        } else {
        //            return ConfigurationManager.getInstance().getConfiguration(ShippingFacilityLevelConfiguration.class).getShippingPackageTypeByCode(box.getCode());
        //        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.uniware.services.shipping.IShippingService#addShippingPackage(com
     * .uniware.core.entity.ShippingPackage)
     */
    @Override
    @Transactional
    public ShippingPackage addShippingPackage(ShippingPackage shippingPackage) {
        return shippingDao.addShippingPackage(shippingPackage);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.uniware.services.shipping.IShippingService#updateShippingPackage(
     * com.uniware.core.entity.ShippingPackage)
     */
    @Override
    @Transactional
    public ShippingPackage updateShippingPackage(ShippingPackage shippingPackage) {
        return shippingDao.updateShippingPackage(shippingPackage);
    }

    /* (non-Javadoc)
     * @see com.uniware.services.shipping.IShippingService#splitShippingPackage(com.uniware.core.api.shipping.SplitShippingPackageRequest)
     */
    @Override
    @LogActivity
    public SplitShippingPackageResponse splitShippingPackage(SplitShippingPackageRequest request) {
        SplitShippingPackageResponse response = new SplitShippingPackageResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            try {
                ShippingPackage shippingPackage = getShippingPackageByCode(request.getShippingPackageCode());
                Channel channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelById(shippingPackage.getSaleOrder().getChannel().getId());
                Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(channel.getSourceCode());
                if (source.isDisallowPackageSplit()) {
                    context.addError(WsResponseCode.SPLIT_PACKAGE_NOT_ALLOWED, "Package split is not allowed for " + source.getCode());
                } else {
                    splitShippingPackageInternal(request, response, context, shippingPackage);
                }
            } catch (NoAvailableTrackingNumberException te) {
                context.addError(WsResponseCode.NO_TRACKING_NUMBER_AVAILABLE_FOR_GIVEN_PROVIDER, "No available tracking number");
            } catch (NoAvailableShippingProviderException se) {
                context.addError(WsResponseCode.SHIPPING_PROVIDER_NOT_YET_ALLOCATED, "No available shipping provider");
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Locks({ @Lock(ns = Namespace.SALE_ORDER, key = "#{#args[3].saleOrder.code}") })
    @Transactional
    @RollbackOnFailure
    private SplitShippingPackageResponse splitShippingPackageInternal(SplitShippingPackageRequest request, SplitShippingPackageResponse response, ValidationContext context,
            ShippingPackage shippingPackage) throws NoAvailableTrackingNumberException, NoAvailableShippingProviderException {
        shippingPackage = shippingDao.getShippingPackageByCode(shippingPackage.getCode());
        Map<Integer, List<SaleOrderItem>> saleOrderItemsMap = new HashMap<Integer, List<SaleOrderItem>>();
        Map<String, BigDecimal> channelProductIdToTax = null;
        if (ShippingPackage.StatusCode.PACKED.name().equals(shippingPackage.getStatusCode())
                || ShippingPackage.StatusCode.READY_TO_SHIP.name().equals(shippingPackage.getStatusCode())) {
            int itemCount = 0;
            // TODO - No Need To Calculate. Not Available For Lite
            //            if (ShippingManager.NONE.equals(shippingPackage.getShippingManager())) {
            //                channelProductIdToTax = new HashMap<>();
            //                for (InvoiceItem invoiceItem : shippingPackage.getInvoice().getInvoiceItems()) {
            //                    channelProductIdToTax.put(invoiceItem.getChannelProductId(), invoiceItem.getVat().add(invoiceItem.getCst()));
            //                }
            //            }
            for (SplitItem splitItem : request.getSplitItems()) {
                List<SaleOrderItem> saleOrderItems;
                if (saleOrderItemsMap.containsKey(splitItem.getSplitNumber())) {
                    saleOrderItems = saleOrderItemsMap.get(splitItem.getSplitNumber());
                } else {
                    saleOrderItems = new ArrayList<SaleOrderItem>();
                    saleOrderItemsMap.put(splitItem.getSplitNumber(), saleOrderItems);
                }
                SaleOrderItem saleOrderItem = getSaleOrderItemFromPackage(shippingPackage, splitItem.getSaleOrderItemCode());
                if (saleOrderItem == null) {
                    context.addError(WsResponseCode.INVALID_SALE_ORDER_ITEM_CODE, "invalid saleOrderItem code at request.splitItem[" + itemCount++ + "].saleOrderItemCode");
                } else {
                    saleOrderItems.add(saleOrderItem);
                }
            }
        } else {
            context.addError(WsResponseCode.INVALID_PACKAGE_STATE, "package can only be splitted in PACKED/READY_TO_SHIP states");
        }

        if (!context.hasErrors()) {
            for (Map.Entry<Integer, List<SaleOrderItem>> entry : saleOrderItemsMap.entrySet()) {
                ShippingPackage splitPackage = createSplittedShippingPackage(shippingPackage, entry.getValue());
                splitPackage.setStatusCode(
                        splitPackage.isRequiresCustomization() ? ShippingPackage.StatusCode.CUSTOMIZATION_COMPLETE.name() : ShippingPackage.StatusCode.PICKED.name());

                CreateShippingPackageInvoiceRequest createShippingPackageInvoiceRequest = new CreateShippingPackageInvoiceRequest();
                createShippingPackageInvoiceRequest.setUserId(request.getUserId());
                createShippingPackageInvoiceRequest.setShippingPackageCode(splitPackage.getCode());
                CreateShippingPackageInvoiceResponse createShippingPackageInvoiceResponse = iShippingInvoiceService.createShippingPackageInvoice(
                        createShippingPackageInvoiceRequest);
                if (!createShippingPackageInvoiceResponse.isSuccessful()) {
                    context.getErrors().addAll(createShippingPackageInvoiceResponse.getErrors());
                    break;
                }
                response.getShippingPackages().add(prepareShippingPackageFullDTO(splitPackage));
            }
            if (!context.hasErrors()) {
                shippingPackage.setTrackingNumber(null);
                shippingPackage.setShippingProviderCode(null);
                shippingPackage.setShippingProviderName(null);
                shippingPackage.setPutawayPending(false);
                shippingPackage.setStatusCode(ShippingPackage.StatusCode.SPLITTED.name());
                updateShippingPackage(shippingPackage);

                ShippingPackageFullDTO dto = prepareShippingPackageFullDTO(shippingPackage);
                dto.setShippingPackageChildren(response.getShippingPackages());
                response.setParentShippingPackage(dto);
                response.setSuccessful(true);
                if (ActivityContext.current().isEnable()) {
                    ActivityUtils.appendActivity(shippingPackage.getCode(), ActivityEntityEnum.SHIPPING_PACKAGE.getName(), shippingPackage.getSaleOrder().getCode(),
                            Arrays.asList(new String[] {
                                    "Shipment {" + ActivityEntityEnum.SHIPPING_PACKAGE + ":" + shippingPackage.getCode() + "} has been moved to "
                                            + shippingPackage.getStatusCode() }),
                            ActivityTypeEnum.EDIT.name());
                }
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        } else {
            response.setSuccessful(true);
        }
        return response;
    }

    @Override
    @Transactional
    public ShippingPackageFullDTO prepareShippingPackageFullDTO(ShippingPackage shippingPackage) {
        ShippingPackageFullDTO spDTO = new ShippingPackageFullDTO(shippingPackage);
        spDTO.setCustomFieldValues(CustomFieldUtils.getCustomFieldValuesDTO(shippingPackage));
        spDTO.setSaleOrderCustomFieldValues(CustomFieldUtils.getCustomFieldValuesDTO(shippingPackage.getSaleOrder()));
        spDTO.setOrderNumber(shippingPackage.getSaleOrder().getCode());
        spDTO.setDisplayOrderNumber(shippingPackage.getSaleOrder().getDisplayOrderCode());
        spDTO.setOrderStatus(shippingPackage.getSaleOrder().getStatusCode());
        spDTO.setChannelName(shippingPackage.getSaleOrder().getChannel().getName());
        SaleOrderItemDTO saleOrderItemDTO;
        for (SaleOrderItem saleOrderItem : shippingPackage.getSaleOrderItems()) {
            saleOrderItemDTO = new SaleOrderItemDTO(saleOrderItem);
            saleOrderItemDTO.setCustomFieldValues(CustomFieldUtils.getCustomFieldValuesDTO(saleOrderItem));
            if (saleOrderItem.getItem() != null) {
                saleOrderItemDTO.setItemCode(saleOrderItem.getItem().getCode());
            }
            if (saleOrderItem.getItemTypeInventory() != null) {
                ItemTypeInventory inventory = inventoryService.getItemTypeInventoryById(saleOrderItem.getItemTypeInventory().getId());
                saleOrderItemDTO.setShelfCode(inventory.getShelf().getCode());
            }
            Item item = saleOrderItem.getItem();
            if (item != null) {
                saleOrderItemDTO.setItemCode(item.getCode());
            }
            spDTO.addSaleOrderItemDTO(saleOrderItemDTO);
            if (StringUtils.equalsAny(shippingPackage.getStatusCode(), ShippingPackage.StatusCode.READY_TO_SHIP.name(), ShippingPackage.StatusCode.PACKED.name())
                    && SaleOrderItem.StatusCode.CANCELLED.name().equals(saleOrderItem.getStatusCode())) {
                spDTO.setRepackageable(true);
            }
            if (StringUtils.equalsAny(shippingPackage.getStatusCode(), ShippingPackage.StatusCode.READY_TO_SHIP.name(), ShippingPackage.StatusCode.PACKED.name())
                    && StringUtils.isBlank(saleOrderItem.getTrackingNumber()) && !shippingPackage.isThirdPartyShipping()) {
                spDTO.setProviderEditable(true);
            }

        }
        ShippingManifest shippingManifest = shippingDao.getShippingManifestForShippingPackage(shippingPackage);
        if (shippingManifest != null) {
            spDTO.setShippingManifestCode(shippingManifest.getCode());
        }
        ReturnManifestItem returnManifestItem = shippingDao.getReturnManifestForShippingPackage(shippingPackage);
        if (returnManifestItem != null) {
            spDTO.setReturnManifestCode(returnManifestItem.getReturnManifest().getCode());
        }
        Picklist picklist = pickerService.getPicklistForShippingPackage(shippingPackage.getCode());
        if (picklist != null) {
            spDTO.setPicklistNumber(picklist.getCode());
            spDTO.setZone(picklist.getPickSetName());
        }
        spDTO.setStateRegulatoryForms(regulatoryService.getApplicableRegulatoryForms(new GetApplicableRegulatoryFormsRequest(shippingPackage.getCode())).getStateRegulatoryForms());
        return spDTO;
    }

    @Override
    @Transactional
    public ReturnManifestItem getReturnManifestItemForShippingPackage(ShippingPackage shippingPackage) {
        return shippingDao.getReturnManifestForShippingPackage(shippingPackage);
    }

    private SaleOrderItem getSaleOrderItemFromPackage(ShippingPackage shippingPackage, String saleOrderItemCode) {
        for (SaleOrderItem saleOrderItem : shippingPackage.getSaleOrderItems()) {
            if (saleOrderItem.getCode().equals(saleOrderItemCode)) {
                return saleOrderItem;
            }
        }
        return null;
    }

    /* (non-Javadoc)
     * @see com.uniware.services.shipping.IShippingService#getShippingPackageById(int)
     */
    @Override
    @Transactional(readOnly = true)
    public ShippingPackage getShippingPackageById(int shippingPackageId) {
        return shippingDao.getShippingPackageById(shippingPackageId);
    }

    @Override
    @Transactional
    public ShippingPackage createSplittedShippingPackage(ShippingPackage parentPackage, List<SaleOrderItem> saleOrderItems)
            throws NoAvailableTrackingNumberException, NoAvailableShippingProviderException {
        ShippingPackage shippingPackage = new ShippingPackage();
        shippingPackage.getSaleOrderItems().addAll(saleOrderItems);
        shippingPackage.setStatusCode(ShippingPackage.StatusCode.PICKED.name());
        shippingPackage.setParentShippingPackage(parentPackage);
        shippingPackage.setSaleOrder(parentPackage.getSaleOrder());
        shippingPackage.setShippingManager(parentPackage.getShippingManager());
        shippingPackage.setShipmentLabelFormat(parentPackage.getShipmentLabelFormat());
        shippingPackage = prepareShippingPackage(shippingPackage, parentPackage.isSplittable());
        addShippingPackage(shippingPackage);
        List<String> saleOrderItemCodes = new ArrayList<>();
        for (SaleOrderItem saleOrderItem : saleOrderItems) {
            shippingPackage.getSaleOrderItems().add(saleOrderItem);
            saleOrderItem.setShippingPackage(shippingPackage);
            if (SaleOrderItem.StatusCode.CANCELLED.name().equals(saleOrderItem.getStatusCode())) {
                shippingPackage.setPutawayPending(true);
            }
            saleOrderDao.updateSaleOrderItem(saleOrderItem);
            if (ActivityContext.current().isEnable()) {
                saleOrderItemCodes.add(saleOrderItem.getCode());
            }
        }
        if (ActivityContext.current().isEnable()) {
            ActivityUtils.appendActivity(shippingPackage.getCode(), ActivityEntityEnum.SHIPPING_PACKAGE.getName(), shippingPackage.getSaleOrder().getCode(),
                    Arrays.asList(new String[] {
                            "Created split shipping package {" + ActivityEntityEnum.SHIPPING_PACKAGE + ":" + shippingPackage.getCode() + "} in PICKED status with items {"
                                    + ActivityEntityEnum.SALE_ORDER_ITEM + ":" + saleOrderItemCodes + "}" }),
                    ActivityTypeEnum.EDIT.name());
        }
        return shippingPackage;
    }

    @Override
    @Transactional
    public ReassessShippingPackageResponse reassessShippingPackage(ReassessShippingPackageRequest request) {
        ReassessShippingPackageResponse response = new ReassessShippingPackageResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            ShippingPackage shippingPackage = getShippingPackageByCode(request.getShippingPackageCode());
            if (shippingPackage == null) {
                context.addError(WsResponseCode.INVALID_SHIPPING_PACKAGE_CODE, "No shipping package exists for provided code");
            } else {
                response.setShippingPackage(reassessShippingPackage(shippingPackage));
            }
        }
        response.setSuccessful(!context.hasErrors());
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    @LogActivity
    public ShippingPackage reassessShippingPackage(ShippingPackage shippingPackage) {
        if (shippingPackage.getSaleOrderItems().size() > 0) {
            shippingPackage = prepareShippingPackage(shippingPackage, shippingPackage.isSplittable());
            if (ActivityContext.current().isEnable()) {
                ActivityUtils.appendActivity(shippingPackage.getCode(), ActivityEntityEnum.SHIPPING_PACKAGE.getName(), shippingPackage.getSaleOrder().getCode(),
                        Arrays.asList(new String[] {
                                "Shipping package {" + ActivityEntityEnum.SHIPPING_PACKAGE + ":" + shippingPackage.getCode() + "} reassessed.["
                                        + shippingPackage.getSaleOrderItems().size() + "] Sale Order Items" }),
                        ActivityTypeEnum.PROCESSING.name());
            }
        } else {
            shippingPackage.setNoOfItems(0);
            shippingPackage.setPutawayPending(false);
            shippingPackage.setStatusCode(ShippingPackage.StatusCode.CANCELLED.name());
            shippingPackage.setCancellationTime(DateUtils.getCurrentTime());
            if (ActivityContext.current().isEnable()) {
                ActivityUtils.appendActivity(shippingPackage.getCode(), ActivityEntityEnum.SHIPPING_PACKAGE.getName(), shippingPackage.getSaleOrder().getCode(),
                        Arrays.asList(new String[] {
                                "Shipping package {" + ActivityEntityEnum.SHIPPING_PACKAGE + ":" + shippingPackage.getCode() + "} moved to " + shippingPackage.getStatusCode() }),
                        ActivityTypeEnum.CANCEL.name());
            }
        }
        return updateShippingPackage(shippingPackage);
    }

    @Override
    @Locks({ @Lock(ns = Namespace.SALE_ORDER, key = "#{#args[0].saleOrderCode}") })
    @Transactional
    public CreateShippingPackageResponse createShippingPackageForSelectedSaleOrderItems(CreateShippingPackageRequest request) {
        CreateShippingPackageResponse response = new CreateShippingPackageResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            SaleOrder saleOrder = saleOrderService.getSaleOrderForUpdate(request.getSaleOrderCode());
            if (saleOrder == null) {
                context.addError(WsResponseCode.INVALID_SALE_ORDER_CODE);
            } else {
                List<SaleOrderItem> saleOrderItems = new ArrayList<>();
                for (SaleOrderItem saleOrderItem : saleOrder.getSaleOrderItems()) {
                    if (request.getSaleOrderItemCodes().contains(saleOrderItem.getCode())) {
                        if (!SaleOrderItem.StatusCode.FULFILLABLE.name().equals(saleOrderItem.getStatusCode())) {
                            context.addError(WsResponseCode.INVALID_SALE_ORDER_STATE, "SaleOrderItem " + saleOrderItem.getCode() + " not in FULFILLABLE state");
                            break;
                        } else if (saleOrderItem.getShippingPackage() != null) {
                            context.addError(WsResponseCode.INVALID_SALE_ORDER_STATE, "Package already created for saleOrderItem: " + saleOrderItem.getCode());
                            break;
                        } else {
                            saleOrderItems.add(saleOrderItem);
                        }
                    }
                }
                if (!context.hasErrors()) {
                    createShippingPackage(saleOrderItems, true);
                    response.setSuccessful(true);
                }
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    @Override
    @Transactional
    public ShippingPackage createShippingPackage(List<SaleOrderItem> saleOrderItems, boolean splittable, String shippingPackageCode) {
        List<String> saleOrderItemsCodes = new ArrayList<>();
        ShippingPackage shippingPackage = new ShippingPackage();
        shippingPackage.setCode(shippingPackageCode);
        shippingPackage.getSaleOrderItems().addAll(saleOrderItems);
        shippingPackage.setSaleOrder(saleOrderItems.get(0).getSaleOrder());
        shippingPackage.setShippingManager(saleOrderItems.get(0).getSaleOrder().isThirdPartyShipping() ? ShippingManager.CHANNEL
                : ConfigurationManager.getInstance().getConfiguration(ProductConfiguration.class).isCourierManagementSwitchedOff() ? ShippingManager.NONE
                        : ShippingManager.UNIWARE);
        shippingPackage.setShipmentLabelFormat(ShipmentLabelFormat.valueOf(saleOrderItems.get(0).getSaleOrder().getChannel().getShipmentLabelFormat()));
        shippingPackage = prepareShippingPackage(shippingPackage, splittable);
        prepareLocationServiceabilityDetails(shippingPackage);
        LOG.info("Creating Shipping Package {}", shippingPackage.getCode());
        addShippingPackage(shippingPackage);
        for (SaleOrderItem saleOrderItem : saleOrderItems) {
            saleOrderItem.setShippingPackage(shippingPackage);
            saleOrderDao.updateSaleOrderItem(saleOrderItem);
            if (ActivityContext.current().isEnable()) {
                saleOrderItemsCodes.add(saleOrderItem.getCode());
            }
        }
        if (ActivityContext.current().isEnable()) {
            ActivityUtils.appendActivity(shippingPackage.getCode(), ActivityEntityEnum.SHIPPING_PACKAGE.getName(), shippingPackage.getSaleOrder().getCode(),
                    Arrays.asList(new String[] {
                            "{" + ActivityEntityEnum.SALE_ORDER_ITEM + ":" + saleOrderItemsCodes + "} have been added to shipping package {" + ActivityEntityEnum.SHIPPING_PACKAGE
                                    + ":" + shippingPackage.getCode() + "} as " + (splittable ? SaleOrderItem.PackageType.FLEXIBLE : SaleOrderItem.PackageType.FIXED) }),
                    ActivityTypeEnum.EDIT.name());
        }
        EntityPersistenceContext<ShippingPackage> persistenceContext = EntityPersistenceContext.current();
        persistenceContext.setEntity(shippingPackage);
        return shippingPackage;
    }

    @Override
    @Transactional
    public ShippingPackage createShippingPackage(List<SaleOrderItem> saleOrderItems, boolean splittable) {
        return createShippingPackage(saleOrderItems, splittable, null);
    }

    /**
     * @param request
     */
    @Override
    @Transactional
    @LogActivity
    public ReassessShippingPackageServiceabilityResponse reassessShippingPackageLocationServiceability(ReassessShippingPackageServiceabilityRequest request) {
        ReassessShippingPackageServiceabilityResponse response = new ReassessShippingPackageServiceabilityResponse();
        ShippingPackage shippingPackage = shippingDao.getShippingPackageById(request.getShippingPackageId(), true);
        String oldStatus = shippingPackage.getStatusCode();
        prepareLocationServiceabilityDetails(shippingPackage);
        response.setShippingPackage(shippingDao.updateShippingPackage(shippingPackage));
        if (ActivityContext.current().isEnable() && !oldStatus.equalsIgnoreCase(shippingPackage.getStatusCode())) {
            ActivityUtils.appendActivity(shippingPackage.getCode(), ActivityEntityEnum.SHIPPING_PACKAGE.getName(), shippingPackage.getSaleOrder().getCode(),
                    Arrays.asList(new String[] {
                            "Channel provides shipping:" + shippingPackage.isThirdPartyShipping() + " Shipping package {" + ActivityEntityEnum.SHIPPING_PACKAGE + ":"
                                    + shippingPackage.getCode() + "} moved to " + shippingPackage.getStatusCode() }),
                    ActivityTypeEnum.PROCESSING.name());
        }
        return response;
    }

    /**
     * @param shippingPackage
     */
    private void prepareLocationServiceabilityDetails(ShippingPackage shippingPackage) {
        if (shippingPackage.isThirdPartyShipping() || shippingPackage.getSaleOrderItems().iterator().next().getShippingProvider() == null) {
            shippingPackage.setStatusCode(ShippingPackage.StatusCode.CREATED.name());
        } else {
            try {
                shippingProviderService.getApplicableShippingProvider(shippingPackage);
                shippingPackage.setStatusCode(ShippingPackage.StatusCode.CREATED.name());
            } catch (NoAvailableShippingProviderException e) {
                shippingPackage.setStatusCode(ShippingPackage.StatusCode.LOCATION_NOT_SERVICEABLE.name());
            }
        }
    }

    /**
     * @param shippingPackage
     */
    private ShippingPackage prepareShippingPackage(ShippingPackage shippingPackage, boolean splittable) {
        updateShippingPackageDimensions(shippingPackage);
        shippingPackage.setCreated(DateUtils.getCurrentTime());
        shippingPackage.setUpdated(DateUtils.getCurrentTime());
        shippingPackage.setSplittable(splittable);
        shippingPackage.setShippingAddress(shippingPackage.getSaleOrderItems().iterator().next().getShippingAddress());
        BigDecimal shippingCharges = BigDecimal.ZERO, shippingMethodCharges = BigDecimal.ZERO, cashOnDeliveryCharges = BigDecimal.ZERO, giftWrapCharges = BigDecimal.ZERO,
                sellingPrice = BigDecimal.ZERO, totalPrice = BigDecimal.ZERO, discount = BigDecimal.ZERO, prepaidAmount = BigDecimal.ZERO;
        int estimatedWeight = 0;
        int priority = 0;
        boolean requiresCustomization = false;
        for (SaleOrderItem saleOrderItem : shippingPackage.getSaleOrderItems()) {
            discount = discount.add(saleOrderItem.getDiscount());
            shippingCharges = shippingCharges.add(saleOrderItem.getShippingCharges());
            shippingMethodCharges = shippingMethodCharges.add(saleOrderItem.getShippingMethodCharges());
            cashOnDeliveryCharges = cashOnDeliveryCharges.add(saleOrderItem.getCashOnDeliveryCharges());
            giftWrapCharges = giftWrapCharges.add(saleOrderItem.getGiftWrapCharges());
            sellingPrice = sellingPrice.add(saleOrderItem.getSellingPrice());
            totalPrice = totalPrice.add(saleOrderItem.getTotalPrice());
            prepaidAmount = prepaidAmount.add(saleOrderItem.getPrepaidAmount());
            ItemType itemType = catalogService.getNonBundledItemTypeById(saleOrderItem.getItemType().getId());
            if (saleOrderItem.getItemTypeInventory() != null) {
                ItemTypeInventory itemTypeInventory = inventoryService.getItemTypeInventoryById(saleOrderItem.getItemTypeInventory().getId());
                priority = Math.max(itemTypeInventory.getPriority(), priority);
            }
            estimatedWeight += itemType.getWeight();
            requiresCustomization = requiresCustomization || saleOrderItem.isRequiresCustomization() || saleOrderItem.getItemType().isRequiresCustomization();
        }

        if (priority == 0) {
            shippingPackage.setPriority(shippingPackage.getSaleOrderItems().iterator().next().getSaleOrder().getPriority());
        }
        if (Facility.Type.WAREHOUSE.name().equals(CacheManager.getInstance().getCache(FacilityCache.class).getCurrentFacility().getType())) {
            addPickDetails(shippingPackage);
        }
        shippingPackage.setRequiresCustomization(requiresCustomization);
        shippingPackage.setNoOfItems(shippingPackage.getSaleOrderItems().size());
        shippingPackage.setPriority(priority);
        estimatedWeight += shippingPackage.getShippingPackageType().getBoxWeight();
        shippingPackage.setEstimatedWeight(estimatedWeight);
        shippingPackage.setActualWeight(estimatedWeight);
        shippingPackage.setDiscount(discount);
        shippingPackage.setShippingCharges(shippingCharges);
        shippingPackage.setShippingMethodCharges(shippingMethodCharges);
        shippingPackage.setCashOnDeliveryCharges(cashOnDeliveryCharges);
        shippingPackage.setGiftWrapCharges(giftWrapCharges);
        shippingPackage.setSellingPrice(sellingPrice);
        shippingPackage.setTotalPrice(totalPrice);
        shippingPackage.setCollectableAmount(shippingPackage.getTotalPrice().subtract(prepaidAmount));
        if (shippingPackage.getSaleOrder().isCashOnDelivery() && shippingPackage.getCollectableAmount().doubleValue() == 0) {
            shippingPackage.setShippingMethod(getShippingMethodByCode(shippingPackage.getSaleOrderItems().iterator().next().getShippingMethod().getCode(), false));
        } else {
            shippingPackage.setShippingMethod(shippingPackage.getSaleOrderItems().iterator().next().getShippingMethod());
        }
        Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(shippingPackage.getSaleOrder().getChannel().getSourceCode());
        if (source.isPODRequired() && !ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).isRestrictOTPGeneration()) {
            String fetchOTPFromThirdPartyScriptName = ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).getOTPFromThirdPartyScriptName();
            String OTP = StringUtils.getRandomNumeric(6);
            if (StringUtils.isNotBlank(fetchOTPFromThirdPartyScriptName)) {
                ScraperScript fetchOTPFromThirdPartyScript = CacheManager.getInstance().getCache(ScriptVersionedCache.class).getScriptByName(fetchOTPFromThirdPartyScriptName);
                if (fetchOTPFromThirdPartyScriptName == null) {
                    throw new RuntimeException("Script not found for fetching OTP");
                } else {
                    try {
                        ScriptExecutionContext seContext = ScriptExecutionContext.current();
                        seContext.addVariable("shippingPackage", shippingPackage);
                        seContext.setTraceLoggingEnabled(UserContext.current().isTraceLoggingEnabled());
                        fetchOTPFromThirdPartyScript.execute();
                        OTP = seContext.getScriptOutput();

                    } catch (Exception e) {
                        LOG.error("Failed to fetch OTP from third party", e);
                        throw new RuntimeException("Script not found for fetching OTP");
                    } finally {
                        ScriptExecutionContext.destroy();
                    }
                }
            }
            shippingPackage.setPodCode(OTP);
        } else {
            shippingPackage.setPodVerified(true);
        }
        shippingPackage.setRegulatoryFormCodes(getRegulatoryFormString(regulatoryService.getApplicableRegulatoryForms(shippingPackage)));
        return shippingPackage;
    }

    private String getRegulatoryFormString(List<RegulatoryFormDTO> stateRegulatoryForms) {
        if (stateRegulatoryForms.size() > 0) {
            StringBuilder applicableForms = new StringBuilder();
            for (RegulatoryFormDTO regulatoryFormDTO : stateRegulatoryForms) {
                applicableForms.append(regulatoryFormDTO.getCode()).append(':').append(regulatoryFormDTO.getName()).append(',');
            }
            applicableForms.deleteCharAt(applicableForms.length() - 1);
            return applicableForms.toString();
        } else {
            return null;
        }
    }

    private void addPickDetails(ShippingPackage shippingPackage) {
        Section defaultSection = ConfigurationManager.getInstance().getConfiguration(PickConfiguration.class).getDefaultPickSet().getSections().iterator().next();
        StringBuilder pickSectionsBuilder = new StringBuilder();
        StringBuilder pickItemsBuilder = new StringBuilder();
        for (SaleOrderItem saleOrderItem : shippingPackage.getSaleOrderItems()) {
            if (saleOrderItem.getItemTypeInventory() != null) {
                Section section = saleOrderItem.getItemTypeInventory().getShelf().getSection();
                shippingPackage.setPickSet(section.getPickSet());
                ItemType itemType = catalogService.getNonBundledItemTypeById(saleOrderItem.getItemType().getId());
                saleOrderItem.setSection(section);

                if (pickSectionsBuilder.indexOf(section.getCode()) == -1) {
                    pickSectionsBuilder.append(section.getCode()).append('|');
                }
                if (pickItemsBuilder.indexOf(itemType.getSkuCode()) == -1) {
                    pickItemsBuilder.append(itemType.getSkuCode()).append('|');
                }
            } else {
                saleOrderItem.setSection(defaultSection);
            }
        }

        if (pickSectionsBuilder.length() > 0) {
            shippingPackage.setPickSections(pickSectionsBuilder.deleteCharAt(pickSectionsBuilder.length() - 1).toString());
            shippingPackage.setPickItems(pickItemsBuilder.deleteCharAt(pickItemsBuilder.length() - 1).toString());
        } else {
            shippingPackage.setPickSet(ConfigurationManager.getInstance().getConfiguration(PickConfiguration.class).getDefaultPickSet());
        }
    }

    @Override
    @Transactional(readOnly = true)
    public String prepareShippingLabelText(String packageCode, Template template) {
        ShippingPackage shippingPackage = shippingDao.getShippingPackageByCode(packageCode);
        Map<String, Object> params = new HashMap<>();
        if (shippingPackage != null) {
            ShippingProvider shippingProvider = ConfigurationManager.getInstance().getConfiguration(ShippingConfiguration.class).getShippingProviderByCode(
                    shippingPackage.getShippingProviderCode());
            params.put("shippingPackage", shippingPackage);
            params.put("warehouse", shippingPackage.getFacility());
            params.put("shippingLocation", shippingProvider != null ? shippingProviderService.getShippingProviderLocation(shippingPackage.getShippingMethod().getId(),
                    shippingProvider.getId(), shippingPackage.getShippingAddress().getPincode()) : null);
            params.put("shippingProvider", shippingProvider);
            params.put("shippingPackageType", shippingPackage.getShippingPackageType());
            params.put("saleOrder", shippingPackage.getSaleOrder());
            params.put("invoice", shippingPackage.getInvoice());
            return template.evaluate(params);
        } else {
            return null;
        }
    }

    @Override
    @Transactional(readOnly = true)
    public SearchShipmentResponse searchShipment(SearchShipmentRequest request) {
        SearchShipmentResponse response = new SearchShipmentResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            ShippingPackage shippingPackage = shippingDao.searchShipmentByCode(request.getShipmentCode());
            if (shippingPackage != null) {
                response.setShippingPackageFullDTO(prepareShippingPackageFullDTO(shippingPackage));
                if (ShippingPackage.StatusCode.SPLITTED.name().equals(shippingPackage.getStatusCode())) {
                    List<ShippingPackage> shippingPackages = shippingDao.getShippingPackageChildren(shippingPackage.getId());
                    for (ShippingPackage childPackage : shippingPackages) {
                        response.getShippingPackageFullDTO().getShippingPackageChildren().add(prepareShippingPackageFullDTO(childPackage));
                    }
                }
                response.setSuccessful(true);
            } else {
                context.addError(WsResponseCode.INVALID_SHIPPING_PACKAGE_CODE, "Shipment code is invalid");
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional(readOnly = true)
    public List<ShippingPackage> getShippingPackageChildren(Integer shipmentId) {
        return shippingDao.getShippingPackageChildren(shipmentId);
    }

    /* (non-Javadoc)
     * @see com.uniware.services.shipping.IShippingService#getShippingPackageById(int, boolean)
     */
    @Override
    @Transactional
    public ShippingPackage getShippingPackageById(int shippingPackageId, boolean refresh) {
        return shippingDao.getShippingPackageById(shippingPackageId, refresh);
    }

    @Override
    @LogActivity
    public EditShippingPackageResponse editShippingPackage(EditShippingPackageRequest request) {
        EditShippingPackageResponse response = new EditShippingPackageResponse();
        ValidationContext context = request.validate();
        try {
            if (!context.hasErrors()) {
                ShippingPackage shippingPackage = getShippingPackageByCode(request.getShippingPackageCode());
                if (shippingPackage == null) {
                    context.addError(WsResponseCode.INVALID_SHIPPING_PACKAGE_CODE, "Invalid Shipping Package Code");
                } else {
                    if (shippingPackage.getShippingProviderCode() != null && !shippingPackage.getShippingProviderCode().equals(request.getShippingProviderCode())
                            && shippingPackage.getSaleOrderItems().iterator().next().getShippingProvider() != null) {
                        context.addError(WsResponseCode.ACTION_NOT_APPLICABLE_FOR_PRE_ALLOCATED_SHIPPING_PROVIDER, "Action not applicable for pre-allocated shipping provider");
                    } else if (shippingPackage.isThirdPartyShipping() && StringUtils.isNotBlank(request.getShippingProviderCode())
                            && (shippingPackage.getShippingProviderCode() == null || !shippingPackage.getShippingProviderCode().equals(request.getShippingProviderCode()))) {
                        context.addError(WsResponseCode.ACTION_NOT_APPLICABLE_FOR_THIRD_PARTY_SHIPPING, "Action not applicable for third party shipping");
                    } else {
                        editShippingPackageInternal(request, response, context, shippingPackage);
                        if (!context.hasErrors()) {
                            response.setSuccessful(true);
                        }
                    }
                }
            }
        } catch (DataIntegrityViolationException e) {
            if (ExceptionUtils.isDuplicateKeyException(e)) {
                context.addError(WsResponseCode.DUPLICATE_TRACKING_NUMBER, "Tracking number already allocated");
            } else {
                context.addError(WsResponseCode.UNKNOWN_ERROR, "Unknown error while updating tracking number");
            }
        } catch (NoAvailableShippingProviderException e) {
            context.addError(WsResponseCode.LOCATION_NOT_SERVICEABLE_WITH_GIVEN_PROVIDER_METHOD, "Location not serviceable with given provider method");
        } catch (NoAvailableTrackingNumberException e) {
            context.addError(WsResponseCode.LOCATION_NOT_SERVICEABLE_WITH_GIVEN_PROVIDER_METHOD, "No tracking number available for given provider");
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    public EditShippingPackageResponse editShippingPackageDetail(EditShippingPackageRequest request) {
        EditShippingPackageResponse response = new EditShippingPackageResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            ShippingPackage shippingPackage = getShippingPackageByCode(request.getShippingPackageCode());
            if (shippingPackage == null) {
                context.addError(WsResponseCode.INVALID_SHIPPING_PACKAGE_CODE, "Invalid Shipping Package Code");
            } else {
                editShippingPackageDetailInternal(request, response, context, shippingPackage);
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
            response.setMessage(context.getErrors().iterator().next().getDescription());
        }
        return response;
    }

    @Locks({ @Lock(ns = Namespace.SALE_ORDER, key = "#{#args[3].saleOrder.code}") })
    @Transactional
    private void editShippingPackageDetailInternal(EditShippingPackageRequest request, EditShippingPackageResponse response, ValidationContext context,
            ShippingPackage shippingPackage) {
        ShippingPackageType shippingPackageType = null;
        shippingPackage = getShippingPackageById(shippingPackage.getId());
        if (StringUtils.equalsAny(shippingPackage.getStatusCode(), ShippingPackage.StatusCode.CREATED.name(), ShippingPackage.StatusCode.PICKED.name(),
                ShippingPackage.StatusCode.PICKING.name(), ShippingPackage.StatusCode.PACKED.name())) {
            if (!context.hasErrors()) {
                if (request.getActualWeight() != null) {
                    shippingPackage.setActualWeight(request.getActualWeight().multiply(new BigDecimal(1000)).intValue());
                }
                if (request.getNoOfBoxes() != null) {
                    shippingPackage.setNoOfBoxes(request.getNoOfBoxes());
                }
                if (StringUtils.isNotBlank(request.getShippingPackageTypeCode())) {
                    shippingPackageType = ConfigurationManager.getInstance().getConfiguration(ShippingFacilityLevelConfiguration.class).getShippingPackageTypeByCode(
                            request.getShippingPackageTypeCode());
                    if (shippingPackageType == null) {
                        context.addError(WsResponseCode.INVALID_REQUEST, "Shipping package type code [" + request.getShippingPackageTypeCode() + "] not recognized.");
                    }
                }
                if (request.getShippingPackageTypeCode() == null && request.getShippingBox() == null) {
                    context.addError(WsResponseCode.INVALID_REQUEST, "Either shipping package type code or shipping box is required.");
                }
            }
            if (!context.hasErrors()) {
                if (shippingPackageType != null) {
                    shippingPackage.setShippingPackageType(shippingPackageType);
                    shippingPackage.setBoxLength(shippingPackageType.getBoxLength());
                    shippingPackage.setBoxWidth(shippingPackageType.getBoxWidth());
                    shippingPackage.setBoxHeight(shippingPackageType.getBoxHeight());
                }
                // Override box dimensions in case both shipping package type and shipping box is provided in request.
                if (request.getShippingBox() != null) {
                    shippingPackage.setBoxLength(request.getShippingBox().getLength());
                    shippingPackage.setBoxWidth(request.getShippingBox().getWidth());
                    shippingPackage.setBoxHeight(request.getShippingBox().getHeight());
                }
                if (request.getCustomFieldValues() != null) {
                    CustomFieldUtils.setCustomFieldValues(shippingPackage,
                            CustomFieldUtils.getCustomFieldValues(context, ShippingPackage.class.getName(), request.getCustomFieldValues()), false);
                }
                shippingPackage = updateShippingPackage(shippingPackage);
                response.setShippingPackageFullDTO(prepareShippingPackageFullDTO(shippingPackage));
                response.setSuccessful(true);
            }
        } else {
            context.addError(WsResponseCode.INVALID_PACKAGE_STATE, "Invalid Shipping Package State. Not editable.");
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
    }

    /* (non-Javadoc)
     * @see com.uniware.services.shipping.IShippingService#updateShippingPackage(com.uniware.core.api.shipping.EditShippingPackageRequest)
     */
    @Locks({ @Lock(ns = Namespace.SALE_ORDER, key = "#{#args[3].saleOrder.code}") })
    @Transactional(rollbackFor = { NoAvailableTrackingNumberException.class, NoAvailableShippingProviderException.class })
    public void editShippingPackageInternal(EditShippingPackageRequest request, EditShippingPackageResponse response, ValidationContext context, ShippingPackage shippingPackage)
            throws NoAvailableShippingProviderException, NoAvailableTrackingNumberException {
        boolean activityEnabled = ActivityContext.current().isEnable();
        List<String> activityLog = new ArrayList<>();
        shippingPackage = shippingDao.getShippingPackageByCode(request.getShippingPackageCode());
        if (StringUtils.equalsAny(shippingPackage.getStatusCode(), ShippingPackage.StatusCode.READY_TO_SHIP.name(), ShippingPackage.StatusCode.PACKED.name())) {
            ShippingConfiguration configuration = ConfigurationManager.getInstance().getConfiguration(ShippingConfiguration.class);
            ShippingProvider provider = null;
            if (StringUtils.isNotBlank(request.getShippingProviderCode())) {
                provider = configuration.getShippingProviderByCode(request.getShippingProviderCode());
                if (provider == null) {
                    context.addError(WsResponseCode.INVALID_SHIPPING_PROVIDER_CODE, "Invalid Shipping provider");
                }
            }

            //ShippingMethod method = ConfigurationManager.getInstance().getConfiguration(ShippingConfiguration.class).getShippingMethodById(request.getShippingMethodId());
            ShippingMethod method = shippingPackage.getShippingMethod();
            if (method == null) {
                context.addError(WsResponseCode.INVALID_SHIPPING_METHOD, "Invalid Shipping Method");
            }

            if (!context.hasErrors()) {
                if (request.getActualWeight() != null) {
                    shippingPackage.setActualWeight(request.getActualWeight().multiply(new BigDecimal(1000)).intValue());
                    if (activityEnabled) {
                        activityLog.add("Weight changed to " + shippingPackage.getActualWeight());
                    }
                }
                if (request.getNoOfBoxes() != null) {
                    shippingPackage.setNoOfBoxes(request.getNoOfBoxes());
                    if (activityEnabled) {
                        activityLog.add(" No of boxes changed to " + request.getNoOfBoxes());
                    }
                }
                if (request.getCustomFieldValues() != null) {
                    CustomFieldUtils.setCustomFieldValues(shippingPackage,
                            CustomFieldUtils.getCustomFieldValues(context, ShippingPackage.class.getName(), request.getCustomFieldValues()), false);
                }
                if (request.getShippingPackageTypeCode() != null) {
                    ShippingPackageType shippingPackageType = ConfigurationManager.getInstance().getConfiguration(
                            ShippingFacilityLevelConfiguration.class).getShippingPackageTypeByCode(request.getShippingPackageTypeCode());
                    if (shippingPackageType != null) {
                        shippingPackage.setShippingPackageType(shippingPackageType);
                        if (request.getShippingBox() != null) {
                            shippingPackage.setBoxLength(request.getShippingBox().getLength());
                            shippingPackage.setBoxWidth(request.getShippingBox().getWidth());
                            shippingPackage.setBoxHeight(request.getShippingBox().getHeight());
                        } else {
                            shippingPackage.setBoxLength(shippingPackageType.getBoxLength());
                            shippingPackage.setBoxWidth(shippingPackageType.getBoxWidth());
                            shippingPackage.setBoxHeight(shippingPackageType.getBoxHeight());
                        }
                        if (activityEnabled) {
                            activityLog.add("Type :" + shippingPackage.getShippingPackageType() + " Length:" + shippingPackage.getBoxLength() + " Width:"
                                    + shippingPackage.getBoxWidth() + " Height:" + shippingPackage.getBoxHeight());
                        }
                    } else {
                        context.addError(WsResponseCode.INVALID_SHIPPING_PACKAGE_TYPE_ID, "Invalid Shipping Package Type");
                    }
                }
            }
            if (!context.hasErrors()) {
                if (provider != null) {
                    boolean providerApplicable = false;
                    boolean courierManagementSwitchedOff = ConfigurationManager.getInstance().getConfiguration(ProductConfiguration.class).isCourierManagementSwitchedOff();
                    if (shippingPackage.getShippingProviderCode() == null || !shippingPackage.getShippingProviderCode().equals(provider.getCode())) {
                        if (courierManagementSwitchedOff) {
                            providerApplicable = true;
                        } else {
                            List<ShippingProvider> availableProviders = shippingProviderService.getAvailableShippingProviders(method.getId(),
                                    shippingPackage.getShippingAddress().getPincode(), shippingPackage);
                            for (ShippingProvider shippingProvider : availableProviders) {
                                if (shippingProvider.getId().equals(provider.getId())) {
                                    providerApplicable = true;
                                    break;
                                }
                            }
                        }
                        if (providerApplicable) {
                            shippingPackage.setShippingLabelLink(null);
                            shippingPackage.setShippingMethod(method);
                            shippingPackage.setShipmentLabelFormat(ShipmentLabelFormat.PDF);
                            shippingPackage.setShippingProviderCode(provider.getCode());
                            shippingPackage.setShippingProviderName(provider.getName());
                        } else {
                            context.addError(WsResponseCode.LOCATION_NOT_SERVICEABLE_WITH_GIVEN_PROVIDER_METHOD, "Location not serviceable with given provider method");
                        }
                    }
                    if (providerApplicable || (provider.getCode().equals(shippingPackage.getShippingProviderCode())
                            && (StringUtils.isNotBlank(request.getTrackingNumber()) && !request.getTrackingNumber().equals(shippingPackage.getTrackingNumber())))) {
                        if (courierManagementSwitchedOff) {
                            if (StringUtils.isNotBlank(request.getTrackingNumber())) {
                                shippingPackage.setStatusCode(ShippingPackage.StatusCode.READY_TO_SHIP.name());
                                shippingPackage.setTrackingNumber(request.getTrackingNumber().trim());
                            }
                        } else {
                            ShippingProviderMethod shippingProviderMethod = ConfigurationManager.getInstance().getConfiguration(
                                    ShippingFacilityLevelConfiguration.class).getShippingProviderMethod(provider.getCode(), method.getCode(), method.isCashOnDelivery());
                            if (TrackingNumberGeneration.MANUAL.name().equals(shippingProviderMethod.getTrackingNumberGeneration())) {
                                if (StringUtils.isNotBlank(request.getTrackingNumber())) {
                                    shippingPackage.setStatusCode(ShippingPackage.StatusCode.READY_TO_SHIP.name());
                                    shippingPackage.setTrackingNumber(request.getTrackingNumber().trim());
                                    activityLog.add("AWB No. " + request.getTrackingNumber() + "manually allocated to package {" + ActivityEntityEnum.SHIPPING_PACKAGE + ":"
                                            + shippingPackage.getCode() + "}");
                                } else {
                                    shippingPackage.setStatusCode(ShippingPackage.StatusCode.PACKED.name());
                                    shippingPackage.setTrackingNumber(null);
                                }
                            } else {
                                shippingPackage.setStatusCode(ShippingPackage.StatusCode.READY_TO_SHIP.name());
                                shippingPackage.setTrackingNumber(shippingProviderService.generateTrackingNumber(shippingPackage));
                            }
                        }
                        if (activityEnabled) {
                            activityLog.add("Shipping package {" + ActivityEntityEnum.SHIPPING_PACKAGE + ":" + shippingPackage.getCode() + "} moved to "
                                    + shippingPackage.getStatusCode() + " status"
                                    + (StringUtils.isNotBlank(shippingPackage.getShippingProviderCode())
                                            ? ". Shipping Provider " + shippingPackage.getShippingProviderCode() + " with AWB " + shippingPackage.getTrackingNumber() + ""
                                            : " with invoice code {" + ActivityEntityEnum.INVOICE + ":" + shippingPackage.getInvoice().getCode() + "}"));
                        }
                        shippingPackage = updateShippingPackage(shippingPackage);
                    }
                }
                response.setShippingPackageFullDTO(prepareShippingPackageFullDTO(shippingPackage));
                if (activityEnabled) {
                    ActivityUtils.appendActivity(shippingPackage.getCode(), ActivityEntityEnum.SHIPPING_PACKAGE.getName(), shippingPackage.getSaleOrder().getCode(), activityLog,
                            ActivityTypeEnum.PROCESSING.name());
                }
            }

        } else {
            context.addError(WsResponseCode.INVALID_PACKAGE_STATE, "Invalid Shipping Package State");
        }
    }

    @Override
    public AddOrEditShippingProviderLocationResponse addOrEditShippingProviderLocation(AddOrEditShippingProviderLocationRequest request) {
        AddOrEditShippingProviderLocationResponse response = new AddOrEditShippingProviderLocationResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            String pincode = request.getPincode();
            String shippingProviderCode = request.getShippingProviderCode();
            String shippingMethodName = request.getShippingMethodName();
            ShippingProviderLocation location = getShippingProviderLocation(shippingMethodName, shippingProviderCode, pincode);
            if (location == null) {
                response.setResponse(createShippingProviderLocation(new CreateShippingProviderLocationRequest(request)));
            } else {
                response.setResponse(editShippingProviderLocation(new EditShippingProviderLocationRequest(request)));
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    @Override
    @Transactional
    public CreateShippingProviderLocationResponse createShippingProviderLocation(CreateShippingProviderLocationRequest request) {
        CreateShippingProviderLocationResponse response = new CreateShippingProviderLocationResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            ShippingMethod shippingMethod = ConfigurationManager.getInstance().getConfiguration(ShippingConfiguration.class).getShippingMethod(request.getShippingMethodName());
            if (shippingMethod == null) {
                context.addError(WsResponseCode.INVALID_SHIPPING_METHOD, "Invalid Shipping Method");
            } else {
                ShippingProvider shippingProvider = ConfigurationManager.getInstance().getConfiguration(ShippingConfiguration.class).getShippingProviderByCode(
                        request.getShippingProviderCode());
                if (shippingProvider == null) {
                    context.addError(WsResponseCode.INVALID_SHIPPING_PROVIDER_CODE, "Invalid Shipping Provider");
                } else {
                    ShippingProviderLocation location = null;
                    if (StringUtils.isNotBlank(request.getFacilityCode())) {
                        location = getShippingProviderLocation(request.getShippingMethodName(), request.getShippingProviderCode(), request.getPincode(), request.getFacilityCode());
                    } else {
                        location = getShippingProviderLocation(request.getShippingMethodName(), request.getShippingProviderCode(), request.getPincode());
                    }
                    if (location != null) {
                        context.addError(WsResponseCode.DUPLICATE_SHIPPING_PROVIDER_LOCATION, "Shipping Provider Location already exists");
                    } else {
                        location = new ShippingProviderLocation();
                        location.setEnabled(true);
                        location.setName(request.getName());
                        location.setPincode(request.getPincode());
                        location.setPriority(request.getPriority());
                        location.setRoutingCode(request.getRoutingCode());
                        location.setShippingProvider(shippingProvider);
                        location.setShippingMethod(shippingMethod);
                        location.setFilterExpressionText(request.getFilterExpression());
                        if (request.getCustomFieldValues() != null) {
                            Map<String, Object> customFieldValues = CustomFieldUtils.getCustomFieldValues(context, ShippingProviderLocation.class.getName(),
                                    request.getCustomFieldValues());
                            CustomFieldUtils.setCustomFieldValues(location, customFieldValues, false);
                        }
                        if (StringUtils.isNotBlank(request.getFacilityCode())) {
                            location.setFacility(facilityDao.getFacilityByCode(request.getFacilityCode()));
                        }
                        location.setCreated(DateUtils.getCurrentTime());
                        shippingDao.createShippingProviderLocation(location);
                        response.setSuccessful(true);
                    }
                }
            }
        }

        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    public ShippingProviderLocation updateShippingProviderLocation(ShippingProviderLocation location) {
        return shippingDao.updateShippingProviderLocation(location);
    }

    @Override
    @Transactional
    public EditShippingProviderLocationResponse editShippingProviderLocation(EditShippingProviderLocationRequest request) {
        EditShippingProviderLocationResponse response = new EditShippingProviderLocationResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            ShippingMethod shippingMethod = ConfigurationManager.getInstance().getConfiguration(ShippingConfiguration.class).getShippingMethod(request.getShippingMethodName());
            if (shippingMethod == null) {
                context.addError(WsResponseCode.INVALID_SHIPPING_METHOD, "Invalid Shipping Method");
            } else {
                ShippingProvider shippingProvider = ConfigurationManager.getInstance().getConfiguration(ShippingConfiguration.class).getShippingProviderByCode(
                        request.getShippingProviderCode());
                if (shippingProvider == null) {
                    context.addError(WsResponseCode.INVALID_SHIPPING_PROVIDER_CODE, "Invalid Shipping Provider");
                }
                ShippingProviderLocation location = shippingDao.getShippingProviderLocation(request.getShippingMethodName(), request.getShippingProviderCode(),
                        request.getPincode());

                if (location == null) {
                    context.addError(WsResponseCode.INVALID_SHIPPING_PROVIDER_LOCATION, "Invalid Shipping Provider Location");
                } else {
                    location.setName(request.getName());
                    location.setPincode(request.getPincode());
                    location.setPriority(request.getPriority());
                    location.setRoutingCode(ValidatorUtils.getOrDefaultValue(request.getRoutingCode(), location.getRoutingCode()));
                    location.setShippingProvider(shippingProvider);
                    location.setShippingMethod(shippingMethod);
                    location.setEnabled(ValidatorUtils.getOrDefaultValue(request.isEnabled(), location.isEnabled()));
                    location.setFilterExpressionText(request.getFilterExpression());
                    if (request.getCustomFieldValues() != null) {
                        Map<String, Object> customFieldValues = CustomFieldUtils.getCustomFieldValues(context, ShippingProviderLocation.class.getName(),
                                request.getCustomFieldValues());
                        CustomFieldUtils.setCustomFieldValues(location, customFieldValues, false);
                    }
                    shippingDao.updateShippingProviderLocation(location);
                    ShippingProviderLocationDTO providerLocationDTO = new ShippingProviderLocationDTO();
                    providerLocationDTO.setEnabled(location.isEnabled());
                    providerLocationDTO.setId(location.getId());
                    providerLocationDTO.setPincode(location.getPincode());
                    providerLocationDTO.setPriority(location.getPriority());
                    providerLocationDTO.setRoutingCode(location.getRoutingCode());
                    providerLocationDTO.setShippingProvider(location.getShippingProvider().getName());
                    providerLocationDTO.setShippingProviderCode(location.getShippingProvider().getCode());
                    providerLocationDTO.setShippingMethod(location.getShippingMethod().getName());
                    providerLocationDTO.setShippingMethodCode(location.getShippingMethod().getCode());
                    providerLocationDTO.setCod(location.getShippingMethod().isCashOnDelivery());
                    response.setShippingProviderLocation(providerLocationDTO);
                    response.setSuccessful(true);
                }
            }

        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional(readOnly = true)
    public SearchShippingPackageResponse searchShippingPackages(SearchShippingPackageRequest request) {
        SearchShippingPackageResponse response = new SearchShippingPackageResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {

            List<Object> ids = shippingDao.searchShippingPackages(request);

            if (request.getSearchOptions() != null && request.getSearchOptions().isGetCount()) {
                Long count = shippingDao.getShippingPackageCount(request);
                response.setTotalRecords(count);
            }
            if (ids.size() > 0) {
                List<ShippingPackage> shippingPackages = shippingDao.getShippingPackagesByIds(ids);
                for (ShippingPackage shippingPackage : shippingPackages) {
                    ShippingPackageDTO shippingPackageDTO = new ShippingPackageDTO(shippingPackage);
                    response.getElements().add(shippingPackageDTO);
                }
            }
            response.setSuccessful(true);

        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    public ShippingProviderLocation getShippingProviderLocation(String shippingMethodName, String shippingProviderCode, String pincode) {
        return shippingDao.getShippingProviderLocation(shippingMethodName, shippingProviderCode, pincode);
    }

    @Override
    @Transactional
    public ShippingProviderLocation getShippingProviderLocation(String shippingMethodName, String shippingProviderCode, String pincode, String facilityCode) {
        return shippingDao.getShippingProviderLocation(shippingMethodName, shippingProviderCode, pincode, facilityCode);
    }

    @Override
    @Transactional
    public List<ShippingProviderLocation> getShippingProviderLocationsByFacilityIdAndUpdatedTime(Integer facilityId, Date updatedAfter) {
        return shippingDao.getShippingProviderLocationsByFacilityCodeAndUpdatedTime(facilityId, updatedAfter);
    }

    /* (non-Javadoc)
     * @see com.uniware.services.shipping.IShippingService#getShippingPackageDetailedById(java.lang.Integer)
     */
    @Override
    @Transactional(readOnly = true)
    public ShippingPackage getShippingPackageDetailedById(Integer shippingPackageId) {
        ShippingPackage shippingPackage = getShippingPackageById(shippingPackageId);
        Hibernate.initialize(shippingPackage.getShippingAddress());
        Hibernate.initialize(shippingPackage.getShippingMethod());
        Hibernate.initialize(shippingPackage.getShippingPackageType());
        Hibernate.initialize(shippingPackage.getSaleOrder());
        if (shippingPackage.getInvoice() != null) {
            Hibernate.initialize(shippingPackage.getInvoice());
            Hibernate.initialize(shippingPackage.getInvoice().getInvoiceItems());
        }
        for (SaleOrderItem saleOrderItem : shippingPackage.getSaleOrderItems()) {
            Hibernate.initialize(saleOrderItem.getItemType());
            Hibernate.initialize(saleOrderItem.getItemType().getCategory());
            if (saleOrderItem.getItem() != null) {
                Hibernate.initialize(saleOrderItem.getItem());
            }
        }
        return shippingPackage;
    }

    /* (non-Javadoc)
     * @see com.uniware.services.shipping.IShippingService#getShippingPackageDetailedByCode(java.lang.String)
     */
    @Override
    @Transactional(readOnly = true)
    public ShippingPackage getShippingPackageDetailedByCode(String shippingPackageCode) {
        ShippingPackage shippingPackage = getShippingPackageByCode(shippingPackageCode);
        return getShippingPackageDetailedById(shippingPackage.getId());
    }

    @Override
    @Transactional(readOnly = true)
    public List<ShippingPackage> getShippingPackagesByStatusCode(String statusCode) {
        return shippingDao.getShippingPackagesByStatusCode(statusCode);
    }

    @Override
    @Transactional(readOnly = true)
    public List<ShippingPackage> getPaginatedShippingPackagesByStatusCode(String statusCode, PageRequest request) {
        return shippingDao.getPaginatedShippingPackagesByStatusCode(statusCode, request);
    }

    @Override
    @Transactional(readOnly = true)
    public ShippingPackage getShippingPackageByInvoiceId(Integer invoiceId) {
        ShippingPackage shippingPackage = shippingDao.getShippingPackageByInvoiceId(invoiceId);
        Hibernate.initialize(shippingPackage.getShippingAddress());
        Hibernate.initialize(shippingPackage.getShippingMethod());
        Hibernate.initialize(shippingPackage.getSaleOrder());
        for (SaleOrderItem saleOrderItem : shippingPackage.getSaleOrderItems()) {
            Hibernate.initialize(saleOrderItem.getItemType());
        }
        return shippingPackage;
    }

    @Override
    @Transactional(readOnly = true)
    public ShippingPackage getShippingPackageByReturnInvoiceId(Integer returnInvoiceId) {
        ShippingPackage shippingPackage = shippingDao.getShippingPackageByReturnInvoiceId(returnInvoiceId);
        if (shippingPackage != null) {
            Hibernate.initialize(shippingPackage.getShippingAddress());
            Hibernate.initialize(shippingPackage.getShippingMethod());
            Hibernate.initialize(shippingPackage.getSaleOrder());
            for (SaleOrderItem saleOrderItem : shippingPackage.getSaleOrderItems()) {
                Hibernate.initialize(saleOrderItem.getItemType());
            }
        }
        return shippingPackage;
    }

    @Override
    @Transactional(readOnly = true)
    public List<ShippingPackage> getShippingPackagesByStatusCode(String statusCode, Date updatedBefore, Date createdBefore) {
        return shippingDao.getShippingPackagesByStatusCode(statusCode, updatedBefore, createdBefore);
    }

    @Override
    @Transactional
    public ShippingPackage getShippingPackageWithDetailsByCode(String shipmentCode) {
        ShippingPackage shippingPackage = getShippingPackageByCode(shipmentCode);
        if (shippingPackage != null) {
            Hibernate.initialize(shippingPackage.getSaleOrder());
            Hibernate.initialize(shippingPackage.getShippingManifest());
        }
        return shippingPackage;
    }

    @Override
    @Transactional
    public ShippingPackage getShippingPackageByCode(String shipmentCode) {
        return shippingDao.getShippingPackageByCode(shipmentCode);
    }

    @Override
    @Transactional
    public ShippingPackage getShippingPackageWithSaleOrderByCode(String shipmentCode) {
        return shippingDao.getShippingPackageWithSaleOrderByCode(shipmentCode);
    }

    /* (non-Javadoc)
     * @see com.uniware.services.shipping.IShippingService#getShippingPackageByTrackingNumber(java.lang.String, java.lang.Integer)
     */
    @Override
    @Transactional(readOnly = true)
    public ShippingPackage getShippingPackageWithDetailsByTrackingNumber(String trackingNumber, String shippingProviderCode) {
        ShippingPackage shippingPackage = shippingDao.getShippingPackageByTrackingNumberOrShippingProvider(trackingNumber, shippingProviderCode);
        if (shippingPackage != null) {
            Hibernate.initialize(shippingPackage.getSaleOrder());
            Hibernate.initialize(shippingPackage.getShippingManifest());
        }
        return shippingPackage;
    }

    /* (non-Javadoc)
     * @see com.uniware.services.shipping.IShippingService#getServiceablePincodesByShippingProviderMethod(com.uniware.core.entity.ShippingProviderMethod)
     */
    @Override
    @Transactional(readOnly = true)
    public Set<String> getServiceablePincodesByShippingProviderMethod(ShippingProvider shippingProvider, ShippingMethod shippingMethod) {
        List<String> pincodes = shippingDao.getServiceablePincodesByShippingProviderMethod(shippingProvider.getId(), shippingMethod.getId());
        return CollectionUtils.asSet(pincodes);
    }

    @Override
    @Transactional(readOnly = true)
    public Set<String> getServiceablePincodesByShippingProviderMethod(ShippingProvider shippingProvider, ShippingMethod shippingMethod, String facilityCode) {
        List<String> pincodes = shippingDao.getServiceablePincodesByShippingProviderMethod(shippingProvider.getId(), shippingMethod.getId(), facilityCode);
        return CollectionUtils.asSet(pincodes);
    }

    @Override
    @Transactional
    public void updateNonServiceablePincodes(ShippingProvider shippingProvider, ShippingMethod shippingMethod, Set<String> pincodes) {
        shippingDao.updateNonServiceablePincodes(shippingProvider.getId(), shippingMethod.getId(), pincodes);
    }

    @Override
    @Transactional
    public void updateNonServiceablePincodes(ShippingProvider shippingProvider, ShippingMethod shippingMethod, Set<String> pincodes, int facilityId) {
        shippingDao.updateNonServiceablePincodes(shippingProvider.getId(), shippingMethod.getId(), pincodes, facilityId);
    }

    @Override
    @Transactional
    public ShippingProvider getShippingProviderByCode(String shippingProviderCode) {
        return shippingDao.getShippingProviderByCode(shippingProviderCode);
    }

    @Override
    @Locks({ @Lock(ns = Namespace.SALE_ORDER, key = "#{#args[0].saleOrderCode}") })
    @Transactional
    @RollbackOnFailure
    @LogActivity
    public MergeShippingPackagesResponse mergeShippingPackages(MergeShippingPackagesRequest request) {
        MergeShippingPackagesResponse response = new MergeShippingPackagesResponse();
        ValidationContext context = request.validate();
        Map<String, WsTaxInformation.ProductTax> skuToTaxPercentage = new HashMap<>();
        if (!context.hasErrors()) {
            SaleOrder saleOrder = saleOrderDao.getSaleOrderForUpdate(request.getSaleOrderCode());
            if (saleOrder == null) {
                context.addError(WsResponseCode.INVALID_SALE_ORDER_CODE, "Invalid sale order code");
            } else {
                List<ShippingPackage> packagesToMerge = new ArrayList<ShippingPackage>();
                List<SaleOrderItem> saleOrderItems = new ArrayList<SaleOrderItem>();
                String previousPackageStatus = null;
                for (String packageCode : request.getShippingPackageCodes()) {
                    ShippingPackage sPackage = shippingDao.getShippingPackageByCode(packageCode);
                    if (!sPackage.getSaleOrder().getCode().equals(saleOrder.getCode())) {
                        context.addError(WsResponseCode.INVALID_REQUEST, "all selected packages must belong to same sale order");
                    } else if (!StringUtils.equalsAny(sPackage.getStatusCode(), ShippingPackage.StatusCode.PENDING_CUSTOMIZATION.name(),
                            ShippingPackage.StatusCode.CUSTOMIZATION_COMPLETE.name(), ShippingPackage.StatusCode.PACKED.name(), ShippingPackage.StatusCode.READY_TO_SHIP.name())) {
                        context.addError(WsResponseCode.INVALID_SHIPPING_PACKAGE_CODE, "Packages can only be merged in 'PACKED' or 'READY_TO_SHIP' state");
                    } else if (previousPackageStatus != null && !previousPackageStatus.equals(sPackage.getStatusCode())) {
                        context.addError(WsResponseCode.INVALID_REQUEST, "all selected packages should be in same state");
                    } else {
                        packagesToMerge.add(sPackage);
                        saleOrderItems.addAll(sPackage.getSaleOrderItems());
                    }
                    previousPackageStatus = sPackage.getStatusCode();
                }
                if (!context.hasErrors()) {
                    boolean splittable = true;
                    boolean putawayPending = false;
                    for (ShippingPackage shippingPackage : packagesToMerge) {
                        splittable &= shippingPackage.isSplittable();
                        putawayPending |= shippingPackage.isPutawayPending();
                        shippingPackage.setTrackingNumber(null);
                        shippingPackage.setShippingProviderCode(null);
                        shippingPackage.setShippingProviderName(null);
                        shippingPackage.setPutawayPending(false);
                        shippingPackage.setStatusCode(ShippingPackage.StatusCode.MERGED.name());
                        // TODO - No Need To Calculate. Not Available For Lite
                        //            if (ShippingManager.NONE.equals(shippingPackage.getShippingManager())) {
                        //                channelProductIdToTax = new HashMap<>();
                        //                for (InvoiceItem invoiceItem : shippingPackage.getInvoice().getInvoiceItems()) {
                        //                    channelProductIdToTax.put(invoiceItem.getChannelProductId(), invoiceItem.getVat().add(invoiceItem.getCst()));
                        //                }
                        //            }
                        if (ActivityContext.current().isEnable()) {
                            ActivityUtils.appendActivity(shippingPackage.getCode(), ActivityEntityEnum.SHIPPING_PACKAGE.getName(), shippingPackage.getSaleOrder().getCode(),
                                    Arrays.asList(new String[] {
                                            "Shipping package {" + ActivityEntityEnum.SHIPPING_PACKAGE + ":" + shippingPackage.getCode() + "} moved to "
                                                    + shippingPackage.getStatusCode() }),
                                    ActivityTypeEnum.EDIT.name());
                        }
                    }
                    ShippingPackage mergedShippingPackage = createShippingPackage(saleOrderItems, splittable);
                    mergedShippingPackage.setPutawayPending(putawayPending);
                    if (StringUtils.equalsAny(previousPackageStatus, ShippingPackage.StatusCode.PENDING_CUSTOMIZATION.name(),
                            ShippingPackage.StatusCode.CUSTOMIZATION_COMPLETE.name())) {
                        mergedShippingPackage.setStatusCode(previousPackageStatus);
                        response.setShippingPackageCode(mergedShippingPackage.getCode());
                        response.setSuccessful(true);
                    } else {
                        mergedShippingPackage.setStatusCode(mergedShippingPackage.isRequiresCustomization() ? ShippingPackage.StatusCode.CUSTOMIZATION_COMPLETE.name()
                                : ShippingPackage.StatusCode.PICKED.name());

                        CreateShippingPackageInvoiceRequest createShippingPackageInvoiceRequest = new CreateShippingPackageInvoiceRequest();
                        createShippingPackageInvoiceRequest.setUserId(request.getUserId());
                        createShippingPackageInvoiceRequest.setShippingPackageCode(mergedShippingPackage.getCode());
                        CreateShippingPackageInvoiceResponse createShippingPackageInvoiceResponse = iShippingInvoiceService.createShippingPackageInvoice(
                                createShippingPackageInvoiceRequest);
                        if (createShippingPackageInvoiceResponse.hasErrors()) {
                            context.getErrors().addAll(createShippingPackageInvoiceResponse.getErrors());
                        } else {
                            response.setSuccessful(true);
                            response.setShippingPackageCode(mergedShippingPackage.getCode());
                        }
                    }
                    if (ActivityContext.current().isEnable()) {
                        ActivityUtils.appendActivity(mergedShippingPackage.getCode(), ActivityEntityEnum.SHIPPING_PACKAGE.getName(), mergedShippingPackage.getSaleOrder().getCode(),
                                Arrays.asList(new String[] {
                                        "Shipping package {" + ActivityEntityEnum.SHIPPING_PACKAGE + ":" + mergedShippingPackage.getCode() + "} moved to "
                                                + mergedShippingPackage.getStatusCode() }),
                                ActivityTypeEnum.EDIT.name());
                    }
                }
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    public GetSalesOrderPackagesResponse getShippingPackages(GetSalesOrderPackagesRequest request) {
        GetSalesOrderPackagesResponse response = new GetSalesOrderPackagesResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            List<ShippingPackage> shippingPackages = shippingDao.getShippingPackages(request.getSaleOrderCode());
            for (ShippingPackage shippingPackage : shippingPackages) {
                response.getElements().add(new ShippingPackageDTO(shippingPackage));
            }
            response.setSuccessful(true);
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    public SendShippingPackageForCustomizationResponse sendShippingPackageForCustomization(SendShippingPackageForCustomizationRequest request) {
        ShippingPackage shippingPackage = getShippingPackageByCode(request.getShippingPackageCode());
        return doSendShippingPackageForCustomization(shippingPackage.getSaleOrder().getCode(), request);
    }

    @Locks({ @Lock(ns = Namespace.SALE_ORDER, key = "#{#args[0]}") })
    @Transactional
    private SendShippingPackageForCustomizationResponse doSendShippingPackageForCustomization(String saleOrderCode, SendShippingPackageForCustomizationRequest request) {
        SendShippingPackageForCustomizationResponse response = new SendShippingPackageForCustomizationResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            ShippingPackage shippingPackage = getShippingPackageByCode(request.getShippingPackageCode());
            saleOrderService.getSaleOrderForUpdate(shippingPackage.getSaleOrder().getCode());
            if (shippingPackage == null) {
                context.addError(WsResponseCode.INVALID_SHIPPING_PACKAGE_CODE);
            } else if (!shippingPackage.isRequiresCustomization()) {
                context.addError(WsResponseCode.INVALID_PACKAGE_STATE, "customization not needed for this package");
            } else if (!ShippingPackage.StatusCode.PICKED.name().equals(shippingPackage.getStatusCode())) {
                context.addError(WsResponseCode.INVALID_PACKAGE_STATE, "Only PICKED packages can be sent for customization");
            } else {
                if (ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).getTraceabilityLevel() == TraceabilityLevel.ITEM) {
                    for (SaleOrderItem saleOrderItem : shippingPackage.getSaleOrderItems()) {
                        if (saleOrderItem.getItem() == null) {
                            context.addError(WsResponseCode.INVALID_PACKAGE_STATE, "all items should be allocated before customization");
                        }
                    }
                }
                if (!context.hasErrors()) {
                    shippingPackage.setStatusCode(ShippingPackage.StatusCode.PENDING_CUSTOMIZATION.name());
                    response.setShippingPackageCode(request.getShippingPackageCode());
                    response.setSuccessful(true);
                }
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    public CompleteCustomizationForShippingPackageResponse completeCustomizationForShippingPackage(CompleteCustomizationForShippingPackageRequest request) {
        ShippingPackage shippingPackage = getShippingPackageByCode(request.getShippingPackageCode());
        return doCompleteCustomizationForShippingPackage(shippingPackage.getSaleOrder().getCode(), request);
    }

    @Locks({ @Lock(ns = Namespace.SALE_ORDER, key = "#{#args[0]}") })
    @Transactional
    private CompleteCustomizationForShippingPackageResponse doCompleteCustomizationForShippingPackage(String saleOrderCode,
            CompleteCustomizationForShippingPackageRequest request) {
        CompleteCustomizationForShippingPackageResponse response = new CompleteCustomizationForShippingPackageResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            ShippingPackage shippingPackage = getShippingPackageByCode(request.getShippingPackageCode());
            if (shippingPackage == null) {
                context.addError(WsResponseCode.INVALID_SHIPPING_PACKAGE_CODE);
            } else if (!ShippingPackage.StatusCode.PENDING_CUSTOMIZATION.name().equals(shippingPackage.getStatusCode())) {
                context.addError(WsResponseCode.INVALID_PACKAGE_STATE, "Only PENDING_CUSTOMIZATION packages can be marked as customization complete");
            } else {
                shippingPackage.setStatusCode(ShippingPackage.StatusCode.CUSTOMIZATION_COMPLETE.name());
                response.setSuccessful(true);
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    public UpdateTrackingStatusResponse updateTrackingStatus(UpdateTrackingStatusRequest request) {
        UpdateTrackingStatusResponse response = new UpdateTrackingStatusResponse();
        ValidationContext context = request.validate();
        ShippingConfiguration shippingConfiguration = ConfigurationManager.getInstance().getConfiguration(ShippingConfiguration.class);
        ShippingProvider shippingProvider = getShippingProviderByCode(request.getProviderCode());
        if (shippingProvider == null) {
            context.addError(WsResponseCode.INVALID_CODE, "Invalid value for shipping provider code");
        } else {
            ShippingProviderSource providerSource = ConfigurationManager.getInstance().getConfiguration(ShippingSourceConfiguration.class).getShippingSourceByCode(
                    shippingProvider.getShippingProviderSourceCode());
            ShipmentTrackingStatus shipmentTrackingStatus = shippingConfiguration.getMappedShipmentTrackingStatus(providerSource.getId(), request.getTrackingStatus());
            if (shipmentTrackingStatus == null) {
                context.addError(WsResponseCode.INVALID_TRACKING_STATUS, "Invalid tracking status");
            } else {
                ShippingPackage shippingPackage = getShippingPackageByShippingProviderAndTrackingNumber(request.getProviderCode(), request.getTrackingNumber());
                if (shippingPackage == null) {
                    context.addError(WsResponseCode.INVALID_TRACKING_NUMBER, "Invalid tracking number");
                } else {
                    UpdateShipmentTrackingStatusRequest updateShipmentTrackingStatusRequest = new UpdateShipmentTrackingStatusRequest();
                    updateShipmentTrackingStatusRequest.setShippingPackageCode(shippingPackage.getCode());
                    updateShipmentTrackingStatusRequest.setShipmentTrackingStatus(shipmentTrackingStatus.getCode());
                    updateShipmentTrackingStatusRequest.setStatusDate(request.getStatusDate());
                    response.setResponse(updateShipmentTrackingStatus(updateShipmentTrackingStatusRequest));
                }
            }
        }
        response.addErrors(context.getErrors());
        return response;
    }

    @Override
    @Locks({ @Lock(ns = Namespace.SALE_ORDER, key = "#{#args[0].saleOrder.code}") })
    @Transactional
    public boolean updateShippingPackageTrackingStatus(ShippingPackage shippingPackage, String statusCode, String shipmentTrackingStatusCode, String providerStatusCode,
            Date statusDate, boolean disableStatusUpdate) {
        if (StringUtils.equalsAny(shippingPackage.getStatusCode(), ShippingPackage.StatusCode.DISPATCHED.name(), ShippingPackage.StatusCode.SHIPPED.name(),
                ShippingPackage.StatusCode.RETURN_EXPECTED.name())) {
            shippingPackage.setStatusCode(statusCode);
            shippingPackage.setShipmentTrackingStatusCode(shipmentTrackingStatusCode);
            shippingPackage.setProviderStatusCode(providerStatusCode);
            shippingPackage.setDisableStatusUpdate(disableStatusUpdate);
            if (ShippingPackage.StatusCode.DELIVERED.name().equals(statusCode)) {
                shippingPackage.setDeliveryTime(statusDate);
                for (SaleOrderItem saleOrderItem : shippingPackage.getSaleOrderItems()) {
                    if (SaleOrderItem.StatusCode.DISPATCHED.name().equals(saleOrderItem.getStatusCode())) {
                        saleOrderItem.setStatusCode(SaleOrderItem.StatusCode.DELIVERED.name());
                        saleOrderDao.updateSaleOrderItem(saleOrderItem);
                    }
                }
            }
            shippingDao.updateShippingPackage(shippingPackage);
            return true;
        }
        return false;
    }

    @Transactional
    @Override
    public ShippingPackage getShippingPackage(String shippingPackageCode, String shippingProviderCode, String trackingNumber) {
        return shippingDao.getShippingPackage(shippingPackageCode, shippingProviderCode, trackingNumber);
    }

    @Override
    public UpdateShippingPackageTrackingStatusResponse updateShippingPackageTrackingStatus(UpdateShippingPackageTrackingStatusRequest request) {
        UpdateShippingPackageTrackingStatusResponse response = new UpdateShippingPackageTrackingStatusResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            ShippingPackage shippingPackage = getShippingPackage(request.getShippingPackageCode(), request.getShippingProviderCode(), request.getTrackingNumber());
            if (shippingPackage == null) {
                context.addError(WsResponseCode.INVALID_SHIPPING_PACKAGE_CODE);
            } else {
                if (updateShippingPackageTrackingStatus(shippingPackage, request.getStatusCode(), request.getShipmentTrackingStatusCode(), request.getProviderStatusCode(),
                        request.getStatusDate(), request.isDisableStatusUpdate())) {
                    response.setSuccessful(true);
                }
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    @Override
    @Transactional
    public UpdateShipmentTrackingStatusResponse updateShipmentTrackingStatus(UpdateShipmentTrackingStatusRequest request) {
        UpdateShipmentTrackingStatusResponse response = new UpdateShipmentTrackingStatusResponse();
        ValidationContext context = request.validate();
        ShippingPackage shippingPackage = null;
        ShippingProvider shippingProvider = null;
        ShippingConfiguration shippingConfiguration = ConfigurationManager.getInstance().getConfiguration(ShippingConfiguration.class);
        ShipmentTrackingStatus trackingStatus = shippingConfiguration.getShipmentTrackingStatusByCode(request.getShipmentTrackingStatus());
        if (!context.hasErrors()) {
            if (request.getStatusDate().after(DateUtils.getCurrentTime())) {
                context.addError(WsResponseCode.INVALID_DATE, "Selected date cannot be after the current date");
            } else if (trackingStatus == null) {
                context.addError(WsResponseCode.INVALID_TRACKING_STATUS);
            } else {
                shippingPackage = getShippingPackageByCode(request.getShippingPackageCode());
                if (shippingPackage == null) {
                    context.addError(WsResponseCode.INVALID_SHIPPING_PACKAGE_CODE);
                } else {
                    if (ShippingManager.UNIWARE.equals(shippingPackage.getShippingManager())) {
                        shippingProvider = ConfigurationManager.getInstance().getConfiguration(ShippingConfiguration.class).getShippingProviderByCode(
                                shippingPackage.getShippingProviderCode());
                        if (shippingProvider == null) {
                            context.addError(WsResponseCode.INVALID_SHIPPING_PROVIDER_CODE);
                        } else {
                            ProviderShipmentStatus providerShipmentStatus = new ProviderShipmentStatus();
                            providerShipmentStatus.setStatus(request.getShipmentTrackingStatus());
                            providerShipmentStatus.setStatusDate(request.getStatusDate());
                            providerShipmentStatus.setTrackingNumber(shippingPackage.getTrackingNumber());
                            ShipmentTracking shipmentTracking = shipmentTrackingService.getShipmentTracking(shippingPackage.getTrackingNumber(), shippingProvider.getId());
                            if (shipmentTracking != null) {
                                UpdateShipmentTrackingResponse updateResponse = shipmentTrackingService.updateShipmentTracking(shippingPackage.getShippingProviderCode(),
                                        shipmentTracking, providerShipmentStatus, request.isManualStatusUpdate());
                                response.setResponse(updateResponse);
                            }
                        }
                    } else {
                        UpdateShippingPackageTrackingStatusRequest updateShippingPackageTrackingRequest = new UpdateShippingPackageTrackingStatusRequest();
                        updateShippingPackageTrackingRequest.setShippingPackageCode(shippingPackage.getCode());
                        updateShippingPackageTrackingRequest.setShippingProviderCode(shippingPackage.getShippingProviderCode());
                        updateShippingPackageTrackingRequest.setTrackingNumber(shippingPackage.getTrackingNumber());
                        updateShippingPackageTrackingRequest.setShipmentTrackingStatusCode(request.getShipmentTrackingStatus());
                        updateShippingPackageTrackingRequest.setStatusCode(trackingStatus.getShippingPackageStatus().getCode());
                        updateShippingPackageTrackingRequest.setProviderStatusCode(null);
                        updateShippingPackageTrackingRequest.setStatusDate(request.getStatusDate());
                        updateShippingPackageTrackingRequest.setDisableStatusUpdate(true);
                        response.setResponse(updateShippingPackageTrackingStatus(updateShippingPackageTrackingRequest));
                    }
                }
            }
        }
        response.addErrors(context.getErrors());
        return response;
    }

    @Override
    public EditShippingPackageResponse editShippingPackageCustomFields(EditShippingPackageRequest request) {
        EditShippingPackageResponse response = new EditShippingPackageResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            ShippingPackage shippingPackage = getShippingPackageByCode(request.getShippingPackageCode());
            if (shippingPackage == null) {
                context.addError(WsResponseCode.INVALID_SHIPPING_PACKAGE_CODE, "Invalid Shipping Package Code");
            } else {
                editShippingPackageCustomFieldsInternal(request, response, context, shippingPackage);
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Locks({ @Lock(ns = Namespace.SALE_ORDER, key = "#{#args[3].saleOrder.code}") })
    @Transactional
    public void editShippingPackageCustomFieldsInternal(EditShippingPackageRequest request, EditShippingPackageResponse response, ValidationContext context,
            ShippingPackage shippingPackage) {
        shippingPackage = shippingDao.getShippingPackageByCode(request.getShippingPackageCode());
        if (!context.hasErrors()) {
            if (request.getCustomFieldValues() != null) {
                CustomFieldUtils.setCustomFieldValues(shippingPackage,
                        CustomFieldUtils.getCustomFieldValues(context, ShippingPackage.class.getName(), request.getCustomFieldValues()), false);
            }
            shippingPackage = updateShippingPackage(shippingPackage);
            response.setShippingPackageFullDTO(prepareShippingPackageFullDTO(shippingPackage));
            response.setSuccessful(true);
        }
    }

    @Override
    public EditShippingPackageTrackingNumberResponse editShippingPackageTrackingNumber(EditShippingPackageTrackingNumberRequest request) {
        EditShippingPackageTrackingNumberResponse response = new EditShippingPackageTrackingNumberResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            ShippingPackage shippingPackage = getShippingPackageByCode(request.getShippingPackageCode());
            if (shippingPackage == null) {
                context.addError(WsResponseCode.INVALID_SHIPPING_PACKAGE_CODE, "Invalid Shipping Package Code");
            } else {
                try {
                    editShippingPackageTrackingInternal(request, response, context, shippingPackage);
                    response.setSuccessful(true);
                } catch (DataIntegrityViolationException e) {
                    if (ExceptionUtils.isDuplicateKeyException(e)) {
                        context.addError(WsResponseCode.DUPLICATE_TRACKING_NUMBER, "Tracking number already allocated");
                    } else {
                        context.addError(WsResponseCode.UNKNOWN_ERROR, "Unknown error while updating tracking number");
                    }
                }
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Locks({ @Lock(ns = Namespace.SALE_ORDER, key = "#{#args[3].saleOrder.code}") })
    @Transactional
    private void editShippingPackageTrackingInternal(EditShippingPackageTrackingNumberRequest request, EditShippingPackageTrackingNumberResponse response,
            ValidationContext context, ShippingPackage shippingPackage) {
        shippingPackage = shippingDao.getShippingPackageByCode(request.getShippingPackageCode());
        if (!StringUtils.equalsAny(shippingPackage.getStatusCode(), ShippingPackage.StatusCode.DISPATCHED.name(), ShippingPackage.StatusCode.SHIPPED.name(),
                ShippingPackage.StatusCode.RETURN_EXPECTED.name())) {
            context.addError(WsResponseCode.INVALID_PACKAGE_STATE, "Invalid shipping package state");
        } else {
            if (!context.hasErrors()) {
                AddCommentRequest commentRequest = new AddCommentRequest();
                commentRequest.setComment(new StringBuilder("Tracking Number updated from ").append(shippingPackage.getTrackingNumber()).append(" to ").append(
                        request.getTrackingNumber()).toString());
                commentRequest.setReferenceIdentifier(
                        new StringBuilder(CommentsServiceImpl.Identifier.SaleOrder.prefix()).append(shippingPackage.getSaleOrder().getCode()).toString());
                commentRequest.setUserId(request.getUserId());
                commentRequest.setSystemGenerated(true);
                commentsService.addComment(commentRequest);
                ShippingProvider shippingProvider = ConfigurationManager.getInstance().getConfiguration(ShippingConfiguration.class).getShippingProviderByCode(
                        shippingPackage.getShippingProviderCode());
                if (shippingProvider != null) {
                    ShipmentTracking shipmentTracking = shipmentTrackingService.getShipmentTracking(shippingPackage.getTrackingNumber(), shippingProvider.getId());
                    if (shipmentTracking != null) {
                        shipmentTracking.setTrackingNumber(request.getTrackingNumber());
                    }
                }
                shippingPackage.setTrackingNumber(request.getTrackingNumber());
                response.setShippingPackageFullDTO(prepareShippingPackageFullDTO(shippingPackage));
            }
        }
    }

    @Override
    @Transactional
    public GetShippingPackagesResponse getShippingPackages(GetShippingPackagesRequest request) {
        GetShippingPackagesResponse response = new GetShippingPackagesResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            List<ShippingPackage> shippingPackages = shippingDao.getShippingPackages(request);
            for (ShippingPackage shippingPackage : shippingPackages) {
                response.addShippingPackage(shippingPackage.getCode());
            }
            response.setSuccessful(true);
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    public GetShippingPackageDetailResponse getShippingPackageDetails(GetShippingPackageDetailRequest request) {
        GetShippingPackageDetailResponse response = new GetShippingPackageDetailResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            ShippingPackage shippingPackage = shippingDao.getShippingPackageByCode(request.getShippingPackageCode());
            if (shippingPackage == null) {
                context.addError(WsResponseCode.INVALID_SHIPPING_PACKAGE_CODE, "Shipping Package code or facility are invalid");
            }
            if (!context.hasErrors()) {
                response.setShippingPackageDetailDTO(prepareShippingPackageDetailDTO(shippingPackage));
                response.setSuccessful(true);
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    public ShippingPackageDetailDTO prepareShippingPackageDetailDTO(ShippingPackage shippingPackage) {
        ShippingPackageDetailDTO shippingDetailDTO = new ShippingPackageDetailDTO();
        shippingDetailDTO.setCode(shippingPackage.getCode());
        shippingDetailDTO.setSaleOrderCode(shippingPackage.getSaleOrder().getCode());
        shippingDetailDTO.setStatusCode(shippingPackage.getStatusCode());
        if (shippingPackage.getShippingManifest() != null) {
            shippingDetailDTO.setShippingManifestCode(shippingPackage.getShippingManifest().getCode());
        }
        shippingDetailDTO.setSaleOrderDetails(prepareSaleOrderDetailDTO(shippingPackage, shippingPackage.getSaleOrder(), shippingDetailDTO));
        return shippingDetailDTO;
    }

    ;

    public SaleOrderDetailDTO prepareSaleOrderDetailDTO(ShippingPackage shippingPackage, SaleOrder saleOrder, ShippingPackageDetailDTO shippingDetailDTO) {
        SaleOrderDetailDTO saleOrderDTO = new SaleOrderDetailDTO();
        Map<Integer, WsAddressDetail> addresses = new HashMap<Integer, WsAddressDetail>();
        WsAddressDetail billingAddress = new WsAddressDetail(saleOrder.getBillingAddress());
        saleOrderDTO.setBillingAddress(billingAddress);
        saleOrderDTO.getAddresses().add(billingAddress);
        addresses.put(saleOrder.getBillingAddress().getId(), billingAddress);
        if (!addresses.containsKey(shippingPackage.getShippingAddress().getId())) {
            WsAddressDetail wsAddressDetail = new WsAddressDetail(shippingPackage.getShippingAddress());
            saleOrderDTO.getAddresses().add(wsAddressDetail);
            addresses.put(shippingPackage.getShippingAddress().getId(), wsAddressDetail);
        }
        saleOrderDTO.setCod(saleOrder.isCashOnDelivery());
        saleOrderDTO.setCode(saleOrder.getCode());
        saleOrderDTO.setChannel(saleOrder.getChannel().getCode());
        saleOrderDTO.setCurrencyCode(saleOrder.getCurrencyCode());
        saleOrderDTO.setCustomerCode(saleOrder.getCustomer() != null ? saleOrder.getCustomer().getCode() : null);
        saleOrderDTO.setDisplayOrderCode(saleOrder.getDisplayOrderCode());
        saleOrderDTO.setDisplayOrderDateTime(saleOrder.getDisplayOrderDateTime());
        saleOrderDTO.setNotificationEmail(saleOrder.getNotificationEmail());
        saleOrderDTO.setNotificationMobile(saleOrder.getNotificationMobile());
        saleOrderDTO.setPriority(saleOrder.getPriority());
        saleOrderDTO.setStatus(saleOrder.getStatusCode());
        saleOrderDTO.setCreated(saleOrder.getCreated());
        saleOrderDTO.setUpdated(saleOrder.getUpdated());
        for (SaleOrderItem saleOrderItem : shippingPackage.getSaleOrderItems()) {
            com.uniware.core.api.saleorder.SaleOrderItemDTO saleOrderItemDTO = new com.uniware.core.api.saleorder.SaleOrderItemDTO(saleOrderItem);
            saleOrderItemDTO.setCode(saleOrderItem.getCode());
            saleOrderItemDTO.setShippingMethodCode(saleOrderItem.getShippingMethod().getCode());
            saleOrderItemDTO.setSellingPrice(saleOrderItem.getSellingPrice());
            saleOrderItemDTO.setShippingCharges(saleOrderItem.getShippingCharges());
            saleOrderItemDTO.setTotalPrice(saleOrderItem.getTotalPrice());
            saleOrderItemDTO.setDiscount(saleOrderItem.getDiscount());
            ItemType itemType = catalogService.getNonBundledItemTypeById(saleOrderItem.getItemType().getId());
            saleOrderItemDTO.setItemSku(itemType.getSkuCode());
            saleOrderItemDTO.setItemName(itemType.getName());
            saleOrderItemDTO.setColor(itemType.getColor());
            saleOrderItemDTO.setBrand(itemType.getBrand());
            saleOrderItemDTO.setSize(itemType.getSize());
            shippingDetailDTO.getSaleOrderItems().add(saleOrderItemDTO);
        }
        return saleOrderDTO;
    }

    @Override
    @Transactional
    public List<ShippingMethod> getShippingMethods() {
        return shippingDao.getShippingMethods();
    }

    @Override
    @Transactional
    public List<ShippingPackageType> getShippingPackageTypes() {
        return shippingDao.getShippingPackageTypes();
    }

    @Override
    public GetShippingPackageTypesResponse getShippingPackageTypes(GetShippingPackageTypesRequest request) {
        GetShippingPackageTypesResponse response = new GetShippingPackageTypesResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            List<ShippingPackageTypeDTO> shippingPackageTypes = new ArrayList<>();
            for (ShippingPackageType shippingPackageType : ConfigurationManager.getInstance().getConfiguration(
                    ShippingFacilityLevelConfiguration.class).getShippingPackageTypes()) {
                shippingPackageTypes.add(new ShippingPackageTypeDTO(shippingPackageType));
            }
            response.setShippingPackageTypes(shippingPackageTypes);
            response.setSuccessful(true);
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    public List<ShippingPackageStatus> getShippingPackageStatuses() {
        return shippingDao.getShippingPackageStatuses();
    }

    @Override
    @Transactional
    public List<ShippingPackage> getShippingPackagesByTrackingNumber(String trackingNumber) {
        return shippingDao.getShippingPackagesByTrackingNumber(trackingNumber);
    }

    @Override
    @Transactional
    public ShippingPackage getShippingPackageByShippingProviderAndTrackingNumber(String shippingProviderCode, String trackingNumber) {
        return shippingDao.getShippingPackageByShippingProviderAndTrackingNumber(shippingProviderCode, trackingNumber);
    }

    @Override
    @Transactional
    public AddShipmentsToBatchResponse addShipmentsToBatch(AddShipmentsToBatchRequest request) {
        AddShipmentsToBatchResponse response = new AddShipmentsToBatchResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            ShipmentBatch shipmentBatch;
            if (StringUtils.isNotBlank(request.getShipmentBatchCode())) {
                shipmentBatch = shippingDao.getShipmentBatchByCode(request.getShipmentBatchCode(), true);
                if (shipmentBatch == null) {
                    context.addError(WsResponseCode.INVALID_SHIPMENT_BATCH_CODE, "Invalid shipment batch code");
                }
            } else {
                shipmentBatch = new ShipmentBatch(ShipmentBatch.StatusCode.PENDING.name(), DateUtils.getCurrentTime());
                shipmentBatch = shippingDao.addShipmentBatch(shipmentBatch);
            }
            if (!context.hasErrors()) {
                int packagesUpdated = shippingDao.updateShipmentBatch(request.getShippingPackageCodes(), shipmentBatch.getCode());
                if (packagesUpdated != request.getShippingPackageCodes().size()) {
                    context.addWarning(WsResponseCode.INVALID_SHIPPING_PACKAGE_CODE, "Some of the packages were invalid and hence not added to batch");
                }
                response.setShipmentBatchCode(shipmentBatch.getCode());
                response.setSuccessful(true);
            }

        }
        response.setErrors(context.getErrors());
        response.setWarnings(context.getWarnings());
        return response;
    }

    @Override
    @Transactional(readOnly = true)
    public GetPendingShipmentBatchResponse getPendingShipmentBatch(GetPendingShipmentBatchRequest request) {
        GetPendingShipmentBatchResponse response = new GetPendingShipmentBatchResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            List<ShipmentBatch> shipmentBatches = shippingDao.getShipmentBatchesByCode(ShipmentBatch.StatusCode.PENDING.name());
            List<String> shipmentBatchCodes = new ArrayList<String>(shipmentBatches.size());
            for (ShipmentBatch shipmentBatch : shipmentBatches) {
                shipmentBatchCodes.add(shipmentBatch.getCode());
            }
            response.setSuccessful(true);
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional(readOnly = true)
    public GetItemsForDetailingResponse getItemsForDetailing(GetItemsForDetailingRequest request) {
        GetItemsForDetailingResponse response = new GetItemsForDetailingResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            List<SaleOrderItem> saleOrderItems = shippingDao.getSaleOrderItemsForDetaling(request.getShippingPackageCodes());
            Set<String> itemDetailFieldNames = new HashSet<String>();
            List<ItemDetailFieldDTO> itemDetailFieldDTOs = new ArrayList<ItemDetailFieldDTO>();
            List<SaleOrderItemForDetalingDTO> saleOrderItemsForDetailing = new ArrayList<SaleOrderItemForDetalingDTO>();
            for (SaleOrderItem saleOrderItem : saleOrderItems) {
                Map<String, String> itemDetailFieldValues = null;
                if (StringUtils.isNotBlank(saleOrderItem.getItemDetails())) {
                    itemDetailFieldValues = JsonUtils.jsonToMap(saleOrderItem.getItemDetails());
                }
                SaleOrderItemForDetalingDTO saleOrderItemForDetalingDTO = new SaleOrderItemForDetalingDTO();
                saleOrderItemForDetalingDTO.setItemTypeName(saleOrderItem.getItemType().getName());
                saleOrderItemForDetalingDTO.setItemTypeSkuCode(saleOrderItem.getItemType().getSkuCode());
                saleOrderItemForDetalingDTO.setSaleOrderCode(saleOrderItem.getSaleOrder().getCode());
                saleOrderItemForDetalingDTO.setSaleOrderItemCode(saleOrderItem.getCode());
                saleOrderItemForDetalingDTO.setShippingPackageCode(saleOrderItem.getShippingPackage().getCode());
                List<String> itemDetailFields = StringUtils.split(saleOrderItem.getItemDetailFields());
                saleOrderItemForDetalingDTO.setItemDetailFieldNames(itemDetailFields);
                saleOrderItemForDetalingDTO.setItemDetailFieldValues(itemDetailFieldValues);
                saleOrderItemsForDetailing.add(saleOrderItemForDetalingDTO);
                itemDetailFieldNames.addAll(itemDetailFields);
            }
            ItemTypeDetailCache cache = CacheManager.getInstance().getCache(ItemTypeDetailCache.class);
            for (String itemDetailFieldName : itemDetailFieldNames) {
                itemDetailFieldDTOs.add(cache.getItemDetailFieldByName(itemDetailFieldName));
            }
            response.setItemDetailFields(itemDetailFieldDTOs);
            response.setSaleOrderItemsForDetailing(saleOrderItemsForDetailing);
            response.setSuccessful(true);
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    public CreateBatchResponse createBatch(CreateBatchRequest request) {
        CreateBatchResponse response = new CreateBatchResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            ShipmentBatch shipmentBatch = shippingDao.getShipmentBatchByCode(request.getShipmentBatchCode());
            if (shipmentBatch != null) {
                context.addError(WsResponseCode.INVALID_SHIPMENT_BATCH_CODE, "Batch already exists with code " + request.getShipmentBatchCode());
            } else {
                shipmentBatch = new ShipmentBatch(request.getShipmentBatchCode(), ShipmentBatch.StatusCode.PENDING.name(), DateUtils.getCurrentTime());
                shippingDao.createShipmentBatch(shipmentBatch);
            }
        }
        if (!context.hasErrors()) {
            response.setSuccessful(true);
        }
        response.setErrors(context.getErrors());
        return response;

    }

    @Override
    public ShippingPackage prepareShippingPackageForCompleteOrder(ShippingPackage shippingPackage, boolean splittable) {
        return prepareShippingPackage(shippingPackage, splittable);
    }

    @Override
    @Transactional(readOnly = true)
    public List<ShippingPackage> getShippingPackagesBySaleOrder(String saleOrderCode) {
        return shippingDao.getShippingPackagesBySaleOrder(saleOrderCode);
    }

    @Override
    @Transactional(readOnly = true)
    public List<ShippingPackage> getAllShippingPackagesBySaleOrder(String saleOrderCode) {
        return shippingDao.getAllShippingPackagesBySaleOrder(saleOrderCode);
    }

    @Override
    @Transactional(readOnly = true)
    public int getReturnsCountBySaleOrder(String saleOrderCode) {
        return shippingDao.getReturnsCountBySaleOrder(saleOrderCode);
    }

    @Override
    @Transactional(readOnly = true)
    public int getInvoiceCountBySaleOrder(String saleOrderCode) {
        return shippingDao.getInvoiceCountBySaleOrder(saleOrderCode);
    }

    public int getShipmentCountBySaleOrder(String saleOrderCode) {
        return shippingDao.getShipmentCountBySaleOrder(saleOrderCode);
    }

    @Override
    @Transactional
    public ShippingPackage getValidShippingPackage(String saleOrderCode, List<String> saleOrderItemCodes) {
        SaleOrder saleOrder = saleOrderService.getSaleOrderByCode(saleOrderCode);
        if (saleOrder != null) {
            List<ShippingPackage> shippingPackages = getAllShippingPackagesBySaleOrder(saleOrderCode);
            if (shippingPackages.size() == 1 && shippingPackages.get(0).getSaleOrderItems().size() == 1) {
                return shippingPackages.get(0);
            } else {
                for (ShippingPackage shippingPackage : getAllShippingPackagesBySaleOrder(saleOrderCode)) {
                    if (!shippingPackage.getSaleOrderItems().isEmpty() && shippingPackage.getSaleOrderItems().size() == saleOrderItemCodes.size()) {
                        Set<String> packageSaleOrderItems = new HashSet<>(saleOrderItemCodes.size());
                        for (SaleOrderItem saleOrderItem : shippingPackage.getSaleOrderItems()) {
                            packageSaleOrderItems.add(saleOrderItem.getCode());
                        }
                        for (String saleOrderItemCode : saleOrderItemCodes) {
                            packageSaleOrderItems.remove(saleOrderItemCode);
                        }
                        if (packageSaleOrderItems.isEmpty()) {
                            return shippingPackage;
                        }
                    }
                }
            }
        }
        return null;
    }

    @Override
    @Transactional(readOnly = true)
    public List<SaleOrderItem> getSaleOrderItemsWithPendingDetailingStatus(List<String> shippingPackageCodes) {
        return shippingDao.getSaleOrderItemsForDetaling(shippingPackageCodes);
    }

    @Override
    public GeneratePDFShippingLabelResponse generatePDFShippingLabel(GeneratePDFShippingLabelRequest request) throws Exception {
        ValidationContext context = request.validate();
        GeneratePDFShippingLabelResponse response = new GeneratePDFShippingLabelResponse();
        if (!context.hasErrors()) {
            GenerateShippingLabelsRequest generateShippingLabelsRequest = new GenerateShippingLabelsRequest();
            generateShippingLabelsRequest.setShippingPackageCodes(Arrays.asList(request.getShippingPackageCode()));
            LOG.info("Generating shipping label for package code: {}", request.getShippingPackageCode());
            GenerateShippingLabelsResponse generateShippingLabelsResponse = generateShippingLabels(generateShippingLabelsRequest);
            LOG.info("Generate shipping label response: {}", generateShippingLabelsResponse);
            if (generateShippingLabelsResponse.isSuccessful()) {
                File generatedFile = generateShippingLabelsResponse.getShippingLabels().get(0);
                if (ShipmentLabelFormat.PDF.equals(generateShippingLabelsResponse.getShipmentLabelFormat())) {
                    response.setShippingLabel(generatedFile);
                } else if (ShipmentLabelFormat.CSV.equals(generateShippingLabelsResponse.getShipmentLabelFormat())) {
                    throw new IllegalArgumentException("Cannot convert CSV to PDF");
                } else if (ShipmentLabelFormat.HTML.equals(generateShippingLabelsResponse.getShipmentLabelFormat())) {
                    PrintTemplateVO invoiceTemplate = CacheManager.getInstance().getCache(PrintTemplateCache.class).getPrintTemplateByType(PrintTemplateVO.Type.SHIPMENT_LABEL);
                    SamplePrintTemplateVO samplePrintTemplate = CacheManager.getInstance().getCache(SamplePrintTemplateCache.class).getSamplePrintTemplate(
                            invoiceTemplate.getSamplePrintTemplateCode());
                    String destPdfFilePath = "/tmp/" + EncryptionUtils.md5Encode(request.getShippingPackageCode() + UserContext.current().getTenant().getCode()) + "label.pdf";
                    if (samplePrintTemplate.isXml()) {
                        PdfUtils.convertXMLToPDF(generatedFile.getAbsolutePath(), destPdfFilePath);
                    } else {
                        PdfUtils.convertHtmlToPdf(generatedFile.getAbsolutePath(), destPdfFilePath);
                    }
                    response.setShippingLabel(new File(destPdfFilePath));
                }
                response.setSuccessful(true);
            } else {
                LOG.error("Unable to generate PDF label, error: {}", generateShippingLabelsResponse.getErrors().toString());
                response.setResponse(generateShippingLabelsResponse);
            }
        } else {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    @Override
    public GenerateShippingLabelsResponse generateShippingLabels(GenerateShippingLabelsRequest request)
            throws IOException, URISyntaxException, DocumentException, HttpTransportException {
        GenerateShippingLabelsResponse response = new GenerateShippingLabelsResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            List<ShippingPackage> shippingPackages = new ArrayList<>();
            Set<ShipmentLabelFormat> shipmentLabelFormats = new HashSet<>();
            for (String shippingPackageCode : request.getShippingPackageCodes()) {
                ShippingPackage shippingPackage = getShippingPackageDetailedByCode(shippingPackageCode);
                shippingPackages.add(shippingPackage);
                if (StringUtils.isNotBlank(shippingPackage.getShippingLabelLink())) {
                    switch (shippingPackage.getShipmentLabelFormat()) {
                        case CSV:
                            shipmentLabelFormats.add(ShipmentLabelFormat.CSV);
                            break;
                        case HTML:
                            shipmentLabelFormats.add(ShipmentLabelFormat.HTML);
                            break;
                        case PNG:
                            shipmentLabelFormats.add(ShipmentLabelFormat.PNG);
                            break;
                        default:
                            shipmentLabelFormats.add(ShipmentLabelFormat.PDF);
                            break;
                    }
                } else {
                    shipmentLabelFormats.add(ShipmentLabelFormat.PDF);
                }
            }
            if (shipmentLabelFormats.size() > 1) {
                throw new IllegalArgumentException("Cannot print incompatible type labels " + shipmentLabelFormats.toString() + " together");
            }

            ShipmentLabelFormat shipmentLabelFormat = shipmentLabelFormats.iterator().next();
            List<File> shippingLabels = new ArrayList<>();
            String outFileName;
            switch (shipmentLabelFormat) {
                case CSV:
                    HttpSender sender = new HttpSender(true);
                    List<String> csvs = new ArrayList<>();
                    Template template = CacheManager.getInstance().getCache(PrintTemplateCache.class).getTemplateByType(PrintTemplateVO.Type.SHIPMENT_LABEL_CSV.name());
                    String downloadFileName = tenantService.getDownloadFileName("shipmentLabels", String.valueOf(shippingPackages.get(0).getInvoice().getInvoiceItems().size()));
                    response.setDownloadFileName(downloadFileName);
                    int channelIdOfFirstPackage = shippingPackages.get(0).getSaleOrder().getChannel().getId();
                    for (ShippingPackage shippingPackage : shippingPackages) {
                        if (shippingPackage.getSaleOrder().getChannel().getId() != channelIdOfFirstPackage) {
                            throw new RuntimeException("CSV labels of only one channel can be printed at a time.");
                        } else {
                            if (shippingPackage.getShippingLabelLink() != null) {
                                sender.downloadToFile(shippingPackage.getShippingLabelLink(), "/tmp/" + downloadFileName);
                                csvs.add(FileUtils.getFileAsString("/tmp/" + downloadFileName));
                            } else {
                                csvs.add(prepareShippingLabelText(shippingPackage.getCode(), template));
                            }
                        }
                    }
                    outFileName = "/tmp/" + EncryptionUtils.md5Encode(request.getShippingPackageCodes().toString()) + ".csv";
                    String output = FileUtils.mergeCSVs(csvs, true);
                    FileUtils.writeToFile(outFileName, output);
                    shippingLabels.add(new File(outFileName));
                    response.setShipmentLabelFormat(ShipmentLabelFormat.CSV);
                    break;
                case HTML:
                    outFileName = "/tmp/" + EncryptionUtils.md5Encode(request.getShippingPackageCodes().toString()) + ".html";
                    List<String> inputFiles = new ArrayList<String>(shippingPackages.size());
                    for (ShippingPackage shippingPackage : shippingPackages) {
                        String md5Code = EncryptionUtils.md5Encode(shippingPackage.getCode() + UserContext.current().getTenant().getCode());
                        String htmlFilePath = "/tmp/" + md5Code + ".html";
                        FileUtils.downloadFile(shippingPackage.getShippingLabelLink(), htmlFilePath);
                        inputFiles.add(htmlFilePath);
                    }
                    FileUtils.concatFiles(outFileName, inputFiles, PAGE_BREAK);
                    shippingLabels.add(new File(outFileName));
                    response.setShipmentLabelFormat(ShipmentLabelFormat.HTML);
                    break;
                case PNG:
                    HttpSender httpSender = new HttpSender();
                    for (ShippingPackage shippingPackage : shippingPackages) {
                        String destPngFilePath = "/tmp/" + EncryptionUtils.md5Encode(shippingPackage.getCode() + UserContext.current().getTenant().getCode()) + ".png";
                        String destPdfFilePath = "/tmp/" + EncryptionUtils.md5Encode(shippingPackage.getCode() + UserContext.current().getTenant().getCode()) + ".pdf";
                        httpSender.downloadToFile(shippingPackage.getShippingLabelLink(), destPngFilePath);
                        PdfUtils.imageToPdf(destPngFilePath, destPdfFilePath);
                        shippingLabels.add(new File(destPdfFilePath));
                    }
                    response.setShipmentLabelFormat(ShipmentLabelFormat.PDF);
                    break;
                default:
                    Map<Integer, List<String>> channelIdToShippingLabels = new HashMap<Integer, List<String>>();
                    PrintTemplateVO shippingLabelTemplate = CacheManager.getInstance().getCache(PrintTemplateCache.class).getPrintTemplateByType(
                            PrintTemplateVO.Type.SHIPMENT_LABEL);
                    httpSender = new HttpSender();
                    for (ShippingPackage shippingPackage : shippingPackages) {
                        if (shippingPackage.getShippingLabelLink() != null) {
                            Integer channelId = shippingPackage.getSaleOrder().getChannel().getId();
                            String destFilePath = "/tmp/" + EncryptionUtils.md5Encode(shippingPackage.getCode() + UserContext.current().getTenant().getCode()) + ".pdf";
                            httpSender.downloadToFile(shippingPackage.getShippingLabelLink(), destFilePath);
                            List<String> labels = channelIdToShippingLabels.get(channelId);
                            if (labels == null) {
                                labels = new ArrayList<>();
                                channelIdToShippingLabels.put(channelId, labels);
                            }
                            labels.add(destFilePath);
                        } else {
                            template = CacheManager.getInstance().getCache(PrintTemplateCache.class).getTemplateByType(PrintTemplateVO.Type.SHIPMENT_LABEL.name());
                            String shippingLabelHtml = prepareShippingLabelText(shippingPackage.getCode(), template);
                            if (StringUtils.isNotBlank(shippingLabelHtml)) {
                                File shippingLabel = new File(
                                        "/tmp/" + EncryptionUtils.md5Encode(shippingPackage.getCode() + UserContext.current().getTenant().getCode()) + ".pdf");
                                SamplePrintTemplateVO samplePrintTemplate = CacheManager.getInstance().getCache(SamplePrintTemplateCache.class).getSamplePrintTemplate(
                                        shippingLabelTemplate.getSamplePrintTemplateCode());
                                try (FileOutputStream fos = new FileOutputStream(shippingLabel)) {
                                    pdfDocumentService.writeHtmlToPdf(fos, shippingLabelHtml, new PdfDocumentServiceImpl.PrintOptions(samplePrintTemplate));
                                }
                                shippingLabels.add(shippingLabel);
                            }
                        }
                    }
                    for (Map.Entry<Integer, List<String>> e : channelIdToShippingLabels.entrySet()) {
                        Channel channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelById(e.getKey());
                        Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(channel.getSourceCode());
                        if (!Source.ShipmentLabelAggregationFormat.NONE.equals(channel.getShippingLabelAggregationFormat())) {
                            String destFilePath = "/tmp/" + EncryptionUtils.md5Encode(e.getValue() + UserContext.current().getTenant().getCode()) + ".pdf";
                            PdfUtils.aggregatePdfsToA4(e.getValue(), channel.getShippingLabelAggregationFormat().isLandscape(),
                                    channel.getShippingLabelAggregationFormat().getFromSize(), source.getShippingLabelXOffset(), source.getShippingLabelYOffset(), destFilePath);
                            shippingLabels.add(new File(destFilePath));
                        } else {
                            for (String label : e.getValue()) {
                                shippingLabels.add(new File(label));
                            }
                        }
                    }
                    response.setShipmentLabelFormat(ShipmentLabelFormat.PDF);
                    break;
            }
            response.setShippingLabels(shippingLabels);
            response.setSuccessful(true);
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    @Override
    public FetchShippingManifestPdfResponse fetchShippingManifestPdf(FetchShippingManifestPdfRequest request) throws IOException {
        FetchShippingManifestPdfResponse response = new FetchShippingManifestPdfResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            ShippingManifest shippingManifest = dispatchService.getShippingManifestByCode(request.getShippingManifestCode());
            if (shippingManifest == null) {
                context.addError(WsResponseCode.INVALID_SHIPPING_MANIFEST_CODE, "No shipping manifest exists with code : " + request.getShippingManifestCode());
            }

            if (!context.hasErrors()) {
                Template template = CacheManager.getInstance().getCache(PrintTemplateCache.class).getTemplateByType(PrintTemplateVO.Type.SHIPPING_MANIFEST.name());
                String manifestHtml = dispatchService.getManifestHtml(request.getShippingManifestCode(), template);
                if (StringUtils.isBlank(manifestHtml)) {
                    context.addError(WsResponseCode.INVALID_SHIPPING_MANIFEST_CODE);
                } else {
                    String directory = EnvironmentProperties.getTemporaryFilePath();
                    directory = directory.endsWith("/") ? directory : directory + "/";
                    String filePath = directory + EncryptionUtils.md5Encode(shippingManifest.getCode() + UserContext.current().getTenant().getCode()) + ".pdf";
                    PrintTemplateVO manifestTemplate = CacheManager.getInstance().getCache(PrintTemplateCache.class).getPrintTemplateByType(PrintTemplateVO.Type.SHIPPING_MANIFEST);
                    SamplePrintTemplateVO samplePrintTemplate = CacheManager.getInstance().getCache(SamplePrintTemplateCache.class).getSamplePrintTemplate(
                            manifestTemplate.getSamplePrintTemplateCode());
                    pdfDocumentService.writeHtmlToPdf(new FileOutputStream(filePath), manifestHtml, new PdfDocumentServiceImpl.PrintOptions(samplePrintTemplate));
                    File file = new File(filePath);
                    String manifestUrl = null;
                    try {
                        manifestUrl = documentService.uploadFile(file, "unicommerce-manifest");
                        shippingManifest.setManifestLink(manifestUrl);
                        dispatchService.updateShippingManifest(shippingManifest);
                    } catch (RuntimeException ex) {
                        context.addError(WsResponseCode.FILE_UPLOAD_ERROR, ex.toString());
                    }
                    if (!context.hasErrors()) {
                        response.setManifestUrl(manifestUrl);
                    }
                }
            }
        }
        if (!context.hasErrors()) {
            response.setSuccessful(true);
        } else {
            response.addErrors(context.getErrors());
        }
        return response;
    }

    @Override
    @Locks({ @Lock(ns = Namespace.MANIFEST, key = "#{#args[0].shippingManifestCode}", level = Level.FACILITY) })
    @Transactional
    public AddSignatureToShippingManifestResponse addSignatureToShippingManifest(AddSignatureToShippingManifestRequest request) {
        AddSignatureToShippingManifestResponse response = new AddSignatureToShippingManifestResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            ShippingManifest shippingManifest = dispatchService.getShippingManifestByCode(request.getShippingManifestCode());
            if (shippingManifest == null) {
                context.addError(WsResponseCode.INVALID_SHIPPING_MANIFEST_CODE);
            } else {
                String filePath = FileUtils.normalizeFilePath(EnvironmentProperties.getTemporaryFilePath(),
                        EncryptionUtils.md5Encode(UserContext.current().getTenant().getCode() + UserContext.current().getFacility().getCode() + "-" + shippingManifest.getCode())
                                + ".png");
                EncryptionUtils.base64DecodeToFile(request.getSignatureImage(), filePath);
                File file = new File(filePath);
                String signatureLink = null;
                try {
                    signatureLink = documentService.uploadFile(file, "unicommerce-manifest-signature");
                } catch (RuntimeException ex) {
                    context.addError(WsResponseCode.FILE_UPLOAD_ERROR, ex.toString());
                }
                if (signatureLink != null) {
                    shippingManifest.setSignatureLink(signatureLink);
                    shippingManifest.setSigneeName(request.getSigneeName());
                    shippingManifest.setSigneeMobile(request.getSigneeMobile());
                    dispatchService.updateShippingManifest(shippingManifest);
                }
            }
        }
        if (!context.hasErrors()) {
            response.setSuccessful(true);
            response.setMessage("Signature added to shipping manifest successfully");
        } else {
            response.addErrors(context.getErrors());
        }
        return response;
    }

    @Override
    @Transactional
    public ShippingPackage getShippingPackageWithSaleOrderItemsForPickSet(String packageCode, int pickSetId) {
        return shippingDao.getShippingPackageWithSaleOrderItemsForPickSet(packageCode, pickSetId);
    }

    @Override
    @Transactional(readOnly = true)
    public Long getPicklistPendingForShelfCount(Shelf shelf) {
        String shelfCode = shelf.getCode();
        return shippingDao.getPicklistPendingForShelfCount(shelfCode);
    }

}
