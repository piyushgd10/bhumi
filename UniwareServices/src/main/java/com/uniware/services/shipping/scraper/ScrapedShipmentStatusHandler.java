/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 15, 2012
 *  @author singla
 */
package com.uniware.services.shipping.scraper;

import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.StringUtils;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.uniware.services.shipping.ProviderShipmentStatus;

/**
 * @author singla
 */
public class ScrapedShipmentStatusHandler extends DefaultHandler {

    public static final String                  TAG_SHIPMENT        = "Shipment";
    public static final String                  TAG_TRACKING_NUMBER = "TrackingNumber";
    public static final String                  TAG_STATUS          = "Status";
    public static final String                  TAG_STATUS_DATE     = "StatusDate";
    public static final String                  TAG_DATE_FORMAT     = "DateFormat";
    public static final String                  DEFAULT_DATE_FORMAT = "dd-MMM-yyyy HH:mm:ss";

    private Map<String, ProviderShipmentStatus> shipmentStatuses;
    private ProviderShipmentStatus              currentShipmentStatus;
    private boolean                             insideElement;
    private String                              elementContent      = "";
    private String                              dateFormat          = DEFAULT_DATE_FORMAT;
    private String                              lastElementRead;

    /* (non-Javadoc)
     * @see org.xml.sax.helpers.DefaultHandler#characters(char[], int, int)
     */
    @Override
    public void characters(char[] buff, int offset, int length) throws SAXException {
        String chars = new String(buff, offset, length).trim();
        if (!StringUtils.isEmpty(chars) && insideElement) {
            elementContent += chars;
        }
    }

    /* (non-Javadoc)
     * @see org.xml.sax.helpers.DefaultHandler#endElement(java.lang.String, java.lang.String, java.lang.String)
     */
    @Override
    public void endElement(String namespaceURI, String sName, String qName) throws SAXException {
        insideElement = false;
        elementContent = elementContent.trim();
        if (!StringUtils.isEmpty(elementContent)) {
            if (TAG_TRACKING_NUMBER.equals(lastElementRead)) {
                currentShipmentStatus.setTrackingNumber(elementContent);
            } else if (TAG_STATUS_DATE.equals(lastElementRead)) {
                Date statusDate = DateUtils.stringToDate(elementContent, dateFormat);
                if (statusDate == null) {
                    statusDate = DateUtils.stringToDate(elementContent, "dd-MMM-yyyy");
                }
                currentShipmentStatus.setStatusDate(statusDate);
            } else if (TAG_DATE_FORMAT.equals(lastElementRead)) {
                dateFormat = elementContent;
            } else if (TAG_STATUS.equals(lastElementRead)) {
                currentShipmentStatus.setStatus(elementContent);
            }
        }
        if (TAG_SHIPMENT.equals(qName)) {
            validate(currentShipmentStatus);
            shipmentStatuses.put(currentShipmentStatus.getTrackingNumber(), currentShipmentStatus);
            currentShipmentStatus = null;
        }
        elementContent = "";
    }

    /**
     * @param currentShipmentStatus2
     */
    private void validate(ProviderShipmentStatus shipmentStatus) throws SAXException {
        if (StringUtils.isEmpty(shipmentStatus.getTrackingNumber())) {
            throw new SAXException("TrackingNumber is mandatory in Shipment");
        } else if (shipmentStatus.getStatusDate() == null) {
            throw new SAXException("StatusDate is mandatory in Shipment");
        } else if (StringUtils.isEmpty(shipmentStatus.getStatus())) {
            throw new SAXException("Status is mandatory in Shipment");
        }
    }

    /* (non-Javadoc)
     * @see org.xml.sax.helpers.DefaultHandler#startDocument()
     */
    @Override
    public void startDocument() throws SAXException {
        shipmentStatuses = new HashMap<String, ProviderShipmentStatus>();
    }

    /* (non-Javadoc)
     * @see org.xml.sax.helpers.DefaultHandler#startElement(java.lang.String, java.lang.String, java.lang.String, org.xml.sax.Attributes)
     */
    @Override
    public void startElement(String namespaceURI, String sName, String qName, Attributes attrs) throws SAXException {
        insideElement = true;
        if (qName.equals(TAG_SHIPMENT)) {
            currentShipmentStatus = new ProviderShipmentStatus();
        }
        lastElementRead = qName;
    }

    /**
     * @param xml
     * @return
     * @throws IOException
     * @throws SAXException
     * @throws ParserConfigurationException
     */
    public Map<String, ProviderShipmentStatus> parse(String xml) throws IOException, SAXException, ParserConfigurationException {
        try {
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser saxParser = factory.newSAXParser();
            saxParser.parse(new ByteArrayInputStream(xml.getBytes("UTF-8")), this);
            return shipmentStatuses;
        } catch (Exception e) {
            throw new RuntimeException("Unable to parse Shipment status XML:" + xml, e);
        }
    }
}
