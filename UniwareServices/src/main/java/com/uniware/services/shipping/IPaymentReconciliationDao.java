/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 12, 2012
 *  @author praveeng
 */
package com.uniware.services.shipping;

import java.math.BigDecimal;
import java.util.List;

import com.uniware.core.entity.PaymentMethod;
import com.uniware.core.entity.PaymentReconciliation;

public interface IPaymentReconciliationDao {

    /**
     * @return
     */
    List<PaymentReconciliation> getPaymentReconciliations();

    /**
     * @param paymentMethodCode
     * @return
     */
    PaymentReconciliation getPaymentReconciliationByPaymentMethodCode(String paymentMethodCode);

    /**
     * @param paymentMethodCode
     * @param tolerance
     */
    void markPaymentReconciled(String paymentMethodCode, BigDecimal tolerance);

    /**
     * @param paymentMethodCode
     * @return
     */
    PaymentMethod getPaymentMethodByCode(String paymentMethodCode);

    void markPackagesReconciled(List<String> shippingPackageCodes);

}
