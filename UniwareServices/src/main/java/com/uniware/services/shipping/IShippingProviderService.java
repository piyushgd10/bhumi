/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jan 24, 2012
 *  @author singla
 */
package com.uniware.services.shipping;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.unifier.core.api.validation.ValidationContext;
import com.uniware.core.api.admin.shipping.ShippingProviderDTO;
import com.uniware.core.api.catalog.GetServiceabilityRequest;
import com.uniware.core.api.catalog.GetServiceabilityResponse;
import com.uniware.core.api.catalog.SearchShippingProviderLocationRequest;
import com.uniware.core.api.catalog.SearchShippingProviderLocationResponse;
import com.uniware.core.api.invoice.WsShippingProviderInfo;
import com.uniware.core.api.shipping.*;
import com.uniware.core.entity.ShippingMethod;
import com.uniware.core.entity.ShippingPackage;
import com.uniware.core.entity.ShippingProvider;
import com.uniware.core.entity.ShippingProviderAllocationRule;
import com.uniware.core.entity.ShippingProviderLocation;
import com.uniware.core.entity.ShippingProviderMethod;
import com.uniware.core.vo.ShippingZoneVO;
import com.uniware.services.exception.NoAvailableShippingProviderException;
import com.uniware.services.exception.NoAvailableTrackingNumberException;

/**
 * @author singla
 */
public interface IShippingProviderService {

    /**
     * @param shippingPackage
     * @return
     * @throws NoAvailableTrackingNumberException
     */
    String generateTrackingNumber(ShippingPackage shippingPackage) throws NoAvailableTrackingNumberException;

    /**
     * @param request
     * @return
     * @throws NoAvailableTrackingNumberException
     * @throws NoAvailableShippingProviderException
     */
    AllocateShippingProviderResponse allocateShippingProvider(AllocateShippingProviderRequest request);

    AssignManualShippingProviderResponse assignManualShippingProvider(AssignManualShippingProviderRequest request);

    /**
     * @param shippingPackage
     * @return
     * @throws NoAvailableShippingProviderException
     */
    ShippingProvider findSuitableShippingProvider(ShippingPackage shippingPackage) throws NoAvailableShippingProviderException;

    /**
     * @param shippingProvider
     * @return
     */
    AbstractShipmentHandler constructShipmentHandler(ShippingProvider shippingProvider);

    /**
     * @param provider
     * @param shippingMethod
     * @return
     * @throws NoAvailableTrackingNumberException
     */
    String generateTrackingNumberFromList(ShippingProvider provider, ShippingMethod shippingMethod) throws NoAvailableTrackingNumberException;

    /**
     * @param shippingMethodId
     * @param shippingProviderId
     * @param pincode
     * @return
     */
    ShippingProviderLocation getShippingProviderLocation(Integer shippingMethodId, Integer shippingProviderId, String pincode);

    /**
     * @param shippingProvider
     * @param shipments
     * @return
     */
    Map<String, ProviderShipmentStatus> scrapeShipmentStatuses(ShippingProvider shippingProvider, List<String> shipments);

    /**
     * @return
     */
    List<ShippingProvider> getConfiguredShippingProviders();

    /**
     * @return
     */
    Integer getNoOfAvailableTrackingNumber(ShippingProviderMethod shippingProviderMethod);

    MarkShippingPackageReadyForPickupResponse markShippingPackageReadyForPickup(MarkShippingPackageReadyForPickupRequest request);

    /**
     * @param createInvoiceAndAllocateShippingProviderRequest
     * @return
     * @throws NoAvailableShippingProviderException
     * @throws NoAvailableTrackingNumberException
     */
    CreateInvoiceAndAllocateShippingProviderResponse createInvoiceAndAllocateShippingProvider(
            CreateInvoiceAndAllocateShippingProviderRequest createInvoiceAndAllocateShippingProviderRequest);

    /**
     * @param shippingPackage
     * @return
     * @throws NoAvailableShippingProviderException
     */
    ShippingProvider getApplicableShippingProvider(ShippingPackage shippingPackage) throws NoAvailableShippingProviderException;

    boolean assignChannelShippingProvider(ShippingPackage shippingPackage, WsShippingProviderInfo shippingProviderInfo, ValidationContext context, boolean moveToReadyToShip);

    void updateShippingLabelLink(ShippingPackage shippingPackage, String shippingLabelLink);

    /**
     * @param request
     * @return
     */
    AssignManualTrackingNumberResponse assignManualTrackingNumber(AssignManualTrackingNumberRequest request);

    /**
     * @param request
     * @return
     */
    SearchShippingProviderLocationResponse searchShippingProviderLocation(SearchShippingProviderLocationRequest request);

    List<ShippingProviderMethod> getShippingProviderMethods(int shippingProviderId);

    Set<Integer> getServiceableByFacilities(Integer shippingMethodId, String pincode);

    BulkAllocateShippingProviderResponse bulkAllocateShippingProvider(BulkAllocateShippingProviderRequest request);

    BulkCreateInvoiceAndAllocateShippingProviderResponse bulkCreateInvoiceAndAllocateShippingProvider(BulkCreateInvoiceAndAllocateShippingProviderRequest request);

    //    List<ShippingProvider> getShippingProviders();

    List<ShippingProviderAllocationRule> getShippingProviderAllocationRules();

    List<ShippingProviderMethod> getShippingProviderMethods();

    //    List<ShippingProviderRegion> getShippingProviderRegions();

    List<ShippingProvider> getAvailableShippingProviders(Integer shippingMethodId, String pincode, ShippingPackage shippingPackage) throws NoAvailableShippingProviderException;

    List<ShippingProviderDTO> lookupProviders(String keyword);

    CreateInvoiceAndGenerateLabelResponse createInvoiceAndGenerateLabel(CreateInvoiceAndGenerateLabelRequest request);

    GetServiceabilityResponse getServiceability(GetServiceabilityRequest request);

    ReAllocateShippingProviderResponse reallocateShippingProvider(ReAllocateShippingProviderRequest request);

    //    GetShippingProviderParametersResponse getShippingProviderParameters(GetShippingProviderParametersRequest request);

    RefreshShippingLabelResponse refreshShippingLabel(RefreshShippingLabelRequest request);

    ShippingZoneVO getShippingInfobyPincode(String pincode);

    List<ShippingZoneVO> getAllShippingZones();

    List<ShippingProviderMethod> getShippingProviderMethods(int shippingProviderId, int facilityId);

    GetShippingPackageShipmentDetailsResponse getShippingPackageShipmentDetails(GetShippingPackageShipmentDetailsRequest request);

    //    List<ShippingProviderSource> getAllShippingSources();

}
