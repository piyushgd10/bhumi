/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 16, 2011
 *  @author singla
 */
package com.uniware.services.shipping;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.springframework.transaction.annotation.Transactional;

import com.itextpdf.text.DocumentException;
import com.unifier.core.annotation.audit.LogActivity;
import com.unifier.core.template.Template;
import com.unifier.core.transport.http.HttpTransportException;
import com.uniware.core.api.catalog.AddOrEditShippingProviderLocationRequest;
import com.uniware.core.api.catalog.AddOrEditShippingProviderLocationResponse;
import com.uniware.core.api.catalog.CreateShippingProviderLocationRequest;
import com.uniware.core.api.catalog.CreateShippingProviderLocationResponse;
import com.uniware.core.api.catalog.EditShippingProviderLocationRequest;
import com.uniware.core.api.catalog.EditShippingProviderLocationResponse;
import com.uniware.core.api.packer.ShippingPackageFullDTO;
import com.uniware.core.api.saleorder.CreateShippingPackageRequest;
import com.uniware.core.api.saleorder.CreateShippingPackageResponse;
import com.uniware.core.api.saleorder.PageRequest;
import com.uniware.core.api.shipping.AddShipmentsToBatchRequest;
import com.uniware.core.api.shipping.AddShipmentsToBatchResponse;
import com.uniware.core.api.shipping.AddSignatureToShippingManifestRequest;
import com.uniware.core.api.shipping.AddSignatureToShippingManifestResponse;
import com.uniware.core.api.shipping.CompleteCustomizationForShippingPackageRequest;
import com.uniware.core.api.shipping.CompleteCustomizationForShippingPackageResponse;
import com.uniware.core.api.shipping.CreateBatchRequest;
import com.uniware.core.api.shipping.CreateBatchResponse;
import com.uniware.core.api.shipping.EditShippingPackageRequest;
import com.uniware.core.api.shipping.EditShippingPackageResponse;
import com.uniware.core.api.shipping.EditShippingPackageTrackingNumberRequest;
import com.uniware.core.api.shipping.EditShippingPackageTrackingNumberResponse;
import com.uniware.core.api.shipping.FetchShippingManifestPdfRequest;
import com.uniware.core.api.shipping.FetchShippingManifestPdfResponse;
import com.uniware.core.api.shipping.GeneratePDFShippingLabelRequest;
import com.uniware.core.api.shipping.GeneratePDFShippingLabelResponse;
import com.uniware.core.api.shipping.GenerateShippingLabelsRequest;
import com.uniware.core.api.shipping.GenerateShippingLabelsResponse;
import com.uniware.core.api.shipping.GetItemsForDetailingRequest;
import com.uniware.core.api.shipping.GetItemsForDetailingResponse;
import com.uniware.core.api.shipping.GetPendingShipmentBatchRequest;
import com.uniware.core.api.shipping.GetPendingShipmentBatchResponse;
import com.uniware.core.api.shipping.GetSalesOrderPackagesRequest;
import com.uniware.core.api.shipping.GetSalesOrderPackagesResponse;
import com.uniware.core.api.shipping.GetShippingPackageDetailRequest;
import com.uniware.core.api.shipping.GetShippingPackageDetailResponse;
import com.uniware.core.api.shipping.GetShippingPackageTypesRequest;
import com.uniware.core.api.shipping.GetShippingPackageTypesResponse;
import com.uniware.core.api.shipping.GetShippingPackagesRequest;
import com.uniware.core.api.shipping.GetShippingPackagesResponse;
import com.uniware.core.api.shipping.MergeShippingPackagesRequest;
import com.uniware.core.api.shipping.MergeShippingPackagesResponse;
import com.uniware.core.api.shipping.ReassessShippingPackageRequest;
import com.uniware.core.api.shipping.ReassessShippingPackageResponse;
import com.uniware.core.api.shipping.ReassessShippingPackageServiceabilityRequest;
import com.uniware.core.api.shipping.ReassessShippingPackageServiceabilityResponse;
import com.uniware.core.api.shipping.SearchShippingPackageRequest;
import com.uniware.core.api.shipping.SearchShippingPackageResponse;
import com.uniware.core.api.shipping.SendShippingPackageForCustomizationRequest;
import com.uniware.core.api.shipping.SendShippingPackageForCustomizationResponse;
import com.uniware.core.api.shipping.SplitShippingPackageRequest;
import com.uniware.core.api.shipping.SplitShippingPackageResponse;
import com.uniware.core.api.shipping.UpdateShipmentTrackingStatusRequest;
import com.uniware.core.api.shipping.UpdateShipmentTrackingStatusResponse;
import com.uniware.core.api.shipping.UpdateShippingPackageTrackingStatusRequest;
import com.uniware.core.api.shipping.UpdateShippingPackageTrackingStatusResponse;
import com.uniware.core.api.shipping.UpdateTrackingStatusRequest;
import com.uniware.core.api.shipping.UpdateTrackingStatusResponse;
import com.uniware.core.api.warehouse.SearchShipmentRequest;
import com.uniware.core.api.warehouse.SearchShipmentResponse;
import com.uniware.core.entity.ReturnManifestItem;
import com.uniware.core.entity.SaleOrderItem;
import com.uniware.core.entity.Shelf;
import com.uniware.core.entity.ShippingMethod;
import com.uniware.core.entity.ShippingPackage;
import com.uniware.core.entity.ShippingPackageStatus;
import com.uniware.core.entity.ShippingPackageType;
import com.uniware.core.entity.ShippingProvider;
import com.uniware.core.entity.ShippingProviderLocation;
import com.uniware.services.exception.NoAvailableShippingProviderException;
import com.uniware.services.exception.NoAvailableTrackingNumberException;

/**
 * @author singla
 */

public interface IShippingService {
    ShippingMethod getShippingMethodByCode(String shippingMethodCode, boolean cashOnDelivery);

    /**
     * @param shippingPackage
     * @return
     */
    ShippingPackage addShippingPackage(ShippingPackage shippingPackage);

    /**
     * @param shippingPackage
     */
    ShippingPackage updateShippingPackage(ShippingPackage shippingPackage);

    /**
     * @param request
     * @return
     * @throws NoAvailableTrackingNumberException
     * @throws NoAvailableShippingProviderException
     */
    SplitShippingPackageResponse splitShippingPackage(SplitShippingPackageRequest request);

    /**
     * @param shippingPackageId
     * @return
     */
    ShippingPackage getShippingPackageById(int shippingPackageId);

    ShippingPackage getShippingPackageById(int shippingPackageId, boolean refresh);

    /**
     * @param saleOrderItems
     * @param splittable
     * @param shippingPackageCode
     */
    ShippingPackage createShippingPackage(List<SaleOrderItem> saleOrderItems, boolean splittable, String shippingPackageCode);

    /**
     * @param parentPackage
     * @param saleOrderItems
     * @return
     * @throws NoAvailableTrackingNumberException
     * @throws NoAvailableShippingProviderException
     */
    ShippingPackage createSplittedShippingPackage(ShippingPackage parentPackage, List<SaleOrderItem> saleOrderItems)
            throws NoAvailableTrackingNumberException, NoAvailableShippingProviderException;

    /**
     * @param shippingMethodId
     * @return
     */
    ShippingMethod getShippingMethodById(int shippingMethodId);

    /**
     * @param request
     * @return
     */
    SearchShipmentResponse searchShipment(SearchShipmentRequest request);

    /**
     * @param shipmentId
     * @return
     */
    List<ShippingPackage> getShippingPackageChildren(Integer shipmentId);

    /**
     * @param shippingPackage
     * @return
     */
    ReassessShippingPackageResponse reassessShippingPackage(ReassessShippingPackageRequest request);

    /**
     * @param request
     * @return
     */
    EditShippingPackageResponse editShippingPackage(EditShippingPackageRequest request);

    /**
     * @param shippingPackageId
     * @param code
     */

    ShippingPackageFullDTO prepareShippingPackageFullDTO(ShippingPackage shippingPackage);

    AddOrEditShippingProviderLocationResponse addOrEditShippingProviderLocation(AddOrEditShippingProviderLocationRequest request);

    CreateShippingProviderLocationResponse createShippingProviderLocation(CreateShippingProviderLocationRequest locationRequest);

    EditShippingProviderLocationResponse editShippingProviderLocation(EditShippingProviderLocationRequest request);

    //    ShippingProviderRegion addShippingProviderRegion(ShippingProviderRegion region);

    SearchShippingPackageResponse searchShippingPackages(SearchShippingPackageRequest request);

    ShippingProviderLocation getShippingProviderLocation(String shippingMethodName, String shippingProviderCode, String pincode);

    List<ShippingProviderLocation> getShippingProviderLocationsByFacilityIdAndUpdatedTime(Integer facilityId, Date updatedAfter);

    /**
     * @param shippingPackageId
     * @return
     */
    ShippingPackage getShippingPackageDetailedById(Integer shippingPackageId);

    /**
     * @param request
     * @return
     */
    ReassessShippingPackageServiceabilityResponse reassessShippingPackageLocationServiceability(ReassessShippingPackageServiceabilityRequest request);

    /**
     * @param statusCode
     * @return
     */
    List<ShippingPackage> getShippingPackagesByStatusCode(String statusCode);

    List<ShippingPackage> getPaginatedShippingPackagesByStatusCode(String statusCode, PageRequest request);

    ShippingPackage getShippingPackageByInvoiceId(Integer invoiceId);

    /**
     * @param shipmentCode
     * @return
     */
    ShippingPackage getShippingPackageByCode(String shipmentCode);

    /**
     * @param shippingPackageCode
     * @return
     */
    ShippingPackage getShippingPackageDetailedByCode(String shippingPackageCode);

    /**
     * @param shippingProvider
     * @param shippingMethod
     * @return
     */
    Set<String> getServiceablePincodesByShippingProviderMethod(ShippingProvider shippingProvider, ShippingMethod shippingMethod);

    /**
     * @param shippingProvider
     * @param shippingMethod
     * @param pincodes
     */
    void updateNonServiceablePincodes(ShippingProvider shippingProvider, ShippingMethod shippingMethod, Set<String> pincodes);

    /**
     * @param shippingProviderCode
     * @return
     */
    ShippingProvider getShippingProviderByCode(String shippingProviderCode);

    /**
     * @param request
     * @return
     */
    MergeShippingPackagesResponse mergeShippingPackages(MergeShippingPackagesRequest request);

    /**
     * @param request
     * @return
     */
    GetSalesOrderPackagesResponse getShippingPackages(GetSalesOrderPackagesRequest request);

    SendShippingPackageForCustomizationResponse sendShippingPackageForCustomization(SendShippingPackageForCustomizationRequest request);

    CompleteCustomizationForShippingPackageResponse completeCustomizationForShippingPackage(CompleteCustomizationForShippingPackageRequest request);

    ShippingPackage createShippingPackage(List<SaleOrderItem> saleOrderItems, boolean splittable);

    EditShippingPackageResponse editShippingPackageDetail(EditShippingPackageRequest request);

    UpdateShipmentTrackingStatusResponse updateShipmentTrackingStatus(UpdateShipmentTrackingStatusRequest request);

    EditShippingPackageResponse editShippingPackageCustomFields(EditShippingPackageRequest request);

    EditShippingPackageTrackingNumberResponse editShippingPackageTrackingNumber(EditShippingPackageTrackingNumberRequest request);

    List<ShippingMethod> getShippingMethods();

    List<ShippingPackageType> getShippingPackageTypes();

    GetShippingPackageTypesResponse getShippingPackageTypes(GetShippingPackageTypesRequest request);

    List<ShippingPackageStatus> getShippingPackageStatuses();

    @Transactional(readOnly = true) ShippingPackage getShippingPackageByReturnInvoiceId(Integer returnInvoiceId);

    @Transactional(readOnly = true)
    List<ShippingPackage> getShippingPackagesByStatusCode(String statusCode, Date updatedBefore, Date createdBefore);

    ShippingPackage getShippingPackageWithDetailsByCode(String shipmentCode);

    String prepareShippingLabelText(String code, Template template);

    UpdateTrackingStatusResponse updateTrackingStatus(UpdateTrackingStatusRequest request);

    GetShippingPackagesResponse getShippingPackages(GetShippingPackagesRequest request);

    GetShippingPackageDetailResponse getShippingPackageDetails(GetShippingPackageDetailRequest request);

    List<ShippingPackage> getShippingPackagesByTrackingNumber(String trackingNumber);

    AddShipmentsToBatchResponse addShipmentsToBatch(AddShipmentsToBatchRequest request);

    GetPendingShipmentBatchResponse getPendingShipmentBatch(GetPendingShipmentBatchRequest request);

    GetItemsForDetailingResponse getItemsForDetailing(GetItemsForDetailingRequest request);

    CreateBatchResponse createBatch(CreateBatchRequest request);

    ReturnManifestItem getReturnManifestItemForShippingPackage(ShippingPackage shippingPackage);

    ShippingPackage prepareShippingPackageForCompleteOrder(ShippingPackage shippingPackage, boolean splittable);

    List<ShippingPackage> getShippingPackagesBySaleOrder(String saleOrderCode);

    int getReturnsCountBySaleOrder(String saleOrderCode);

    int getInvoiceCountBySaleOrder(String saleOrderCode);

    int getShipmentCountBySaleOrder(String saleOrderCode);

    ShippingPackage getShippingPackageByShippingProviderAndTrackingNumber(String shippingProviderCode, String trackingNumber);

    UpdateShippingPackageTrackingStatusResponse updateShippingPackageTrackingStatus(UpdateShippingPackageTrackingStatusRequest request);

    ShippingPackage getShippingPackageWithSaleOrderByCode(String shipmentCode);

    ShippingPackage getShippingPackageWithDetailsByTrackingNumber(String trackingNumber, String shippingProviderCode);

    @Transactional
    @LogActivity
    ShippingPackage reassessShippingPackage(ShippingPackage shippingPackage);

    CreateShippingPackageResponse createShippingPackageForSelectedSaleOrderItems(CreateShippingPackageRequest request);

    boolean updateShippingPackageTrackingStatus(ShippingPackage shippingPackage, String statusCode, String shipmentTrackingStatusCode, String providerStatusCode, Date statusDate,
            boolean disableStatusUpdate);

    ShippingPackage getShippingPackage(String shippingPackageCode, String shippingProviderCode, String trackingNumber);

    Set<String> getServiceablePincodesByShippingProviderMethod(ShippingProvider shippingProvider, ShippingMethod shippingMethod, String facilityCode);

    void updateNonServiceablePincodes(ShippingProvider shippingProvider, ShippingMethod shippingMethod, Set<String> pincodes, int facilityId);

    ShippingProviderLocation getShippingProviderLocation(String shippingMethodName, String shippingProviderCode, String pincode, String facilityCode);

    List<ShippingPackage> getAllShippingPackagesBySaleOrder(String saleOrderCode);

    ShippingProviderLocation updateShippingProviderLocation(ShippingProviderLocation location);

    ShippingPackage getValidShippingPackage(String saleOrderCode, List<String> saleOrderItemCodes);

    List<SaleOrderItem> getSaleOrderItemsWithPendingDetailingStatus(List<String> shippingPackageCodes);

    GeneratePDFShippingLabelResponse generatePDFShippingLabel(GeneratePDFShippingLabelRequest request) throws Exception;

    GenerateShippingLabelsResponse generateShippingLabels(GenerateShippingLabelsRequest request) throws IOException, URISyntaxException, DocumentException, HttpTransportException;

    FetchShippingManifestPdfResponse fetchShippingManifestPdf(FetchShippingManifestPdfRequest request) throws IOException;

    AddSignatureToShippingManifestResponse addSignatureToShippingManifest(AddSignatureToShippingManifestRequest request);

    ShippingPackage getShippingPackageWithSaleOrderItemsForPickSet(String packageCode, int pickSetId);

    Long getPicklistPendingForShelfCount(Shelf shelf);
}
