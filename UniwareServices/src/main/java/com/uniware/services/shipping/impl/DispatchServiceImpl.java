/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 *  @version     1.0, Dec 18, 2011
 *  @author singla
 */
package com.uniware.services.shipping.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.locks.ReadWriteLock;

import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.unifier.core.annotation.Level;
import com.unifier.core.annotation.audit.LogActivity;
import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.api.validation.WsError;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.email.EmailMessage;
import com.unifier.core.entity.User;
import com.unifier.core.expressions.Expression;
import com.unifier.core.sms.SmsMessage;
import com.unifier.core.template.Template;
import com.unifier.core.transport.http.HttpSender;
import com.unifier.core.transport.http.HttpTransportException;
import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.DateUtils.DateRange;
import com.unifier.core.utils.EncryptionUtils;
import com.unifier.core.utils.FileUtils;
import com.unifier.core.utils.HibernateUtils;
import com.unifier.core.utils.JsonUtils;
import com.unifier.core.utils.StringUtils;
import com.unifier.core.utils.ValidatorUtils;
import com.unifier.scraper.sl.exception.ScriptExecutionException;
import com.unifier.scraper.sl.runtime.IScriptProvider;
import com.unifier.scraper.sl.runtime.ScraperScript;
import com.unifier.scraper.sl.runtime.ScriptExecutionContext;
import com.unifier.services.aspect.RollbackOnFailure;
import com.unifier.services.email.IEmailService;
import com.unifier.services.pdf.IPdfDocumentService;
import com.unifier.services.pdf.impl.PdfDocumentServiceImpl.PrintOptions;
import com.unifier.services.sms.ISmsService;
import com.unifier.services.utils.CustomFieldUtils;
import com.uniware.core.api.admin.shipping.ShippingMethodDTO;
import com.uniware.core.api.admin.shipping.ShippingProviderSearchDTO;
import com.uniware.core.api.admin.shipping.ShippingProviderSearchDTO.PacketType;
import com.uniware.core.api.invoice.CreateShippingPackageInvoiceRequest;
import com.uniware.core.api.invoice.CreateShippingPackageInvoiceResponse;
import com.uniware.core.api.invoice.WsTaxInformation;
import com.uniware.core.api.saleorder.AddressDTO;
import com.uniware.core.api.saleorder.CancelSaleOrderRequest;
import com.uniware.core.api.shipping.AddShipmentsToBatchRequest;
import com.uniware.core.api.shipping.AddShipmentsToBatchResponse;
import com.uniware.core.api.shipping.AddShippingPackageToManifestRequest;
import com.uniware.core.api.shipping.AddShippingPackageToManifestResponse;
import com.uniware.core.api.shipping.AddShippingPackagesToManifestRequest;
import com.uniware.core.api.shipping.AddShippingPackagesToManifestResponse;
import com.uniware.core.api.shipping.AutoDispatchShippingPackageRequest;
import com.uniware.core.api.shipping.AutoDispatchShippingPackageResponse;
import com.uniware.core.api.shipping.CheckIfShippingManifestClosingRequest;
import com.uniware.core.api.shipping.CheckIfShippingManifestClosingResponse;
import com.uniware.core.api.shipping.CloseShippingManifestRequest;
import com.uniware.core.api.shipping.CloseShippingManifestResponse;
import com.uniware.core.api.shipping.CreateShippingManifestRequest;
import com.uniware.core.api.shipping.CreateShippingManifestResponse;
import com.uniware.core.api.shipping.DiscardShippingManifestRequest;
import com.uniware.core.api.shipping.DiscardShippingManifestResponse;
import com.uniware.core.api.shipping.DispatchSaleOrderItemsRequest;
import com.uniware.core.api.shipping.DispatchSaleOrderItemsResponse;
import com.uniware.core.api.shipping.DispatchShippingPackageRequest;
import com.uniware.core.api.shipping.DispatchShippingPackageResponse;
import com.uniware.core.api.shipping.DoAddShippingPackageToManifestResponse;
import com.uniware.core.api.shipping.EditShippingManifestMetadataRequest;
import com.uniware.core.api.shipping.EditShippingManifestMetadataResponse;
import com.uniware.core.api.shipping.ForceDispatchShippingPackageRequest;
import com.uniware.core.api.shipping.ForceDispatchShippingPackageResponse;
import com.uniware.core.api.shipping.GetApplicableRegulatoryFormsRequest;
import com.uniware.core.api.shipping.GetChannelsForManifestRequest;
import com.uniware.core.api.shipping.GetChannelsForManifestResponse;
import com.uniware.core.api.shipping.GetChannelsForManifestResponse.ChannelDTO;
import com.uniware.core.api.shipping.GetCurrentChannelShippingManifestRequest;
import com.uniware.core.api.shipping.GetCurrentChannelShippingManifestResponse;
import com.uniware.core.api.shipping.GetEligibleShippingProvidersForManifestRequest;
import com.uniware.core.api.shipping.GetEligibleShippingProvidersForManifestResponse;
import com.uniware.core.api.shipping.GetManifestShippingProvidersSummaryRequest;
import com.uniware.core.api.shipping.GetManifestShippingProvidersSummaryResponse;
import com.uniware.core.api.shipping.GetManifestShippingProvidersSummaryResponse.ManifestProviderSummaryDTO;
import com.uniware.core.api.shipping.GetManifestShippingProvidersSummaryResponse.ManifestProviderSummaryDTO.ManifestProviderDTO;
import com.uniware.core.api.shipping.GetManifestSummaryRequest;
import com.uniware.core.api.shipping.GetManifestSummaryResponse;
import com.uniware.core.api.shipping.GetShippingManifestItemDetailsRequest;
import com.uniware.core.api.shipping.GetShippingManifestItemDetailsResponse;
import com.uniware.core.api.shipping.GetShippingManifestItemDetailsResponse.ShippingManifestItemDetailDTO;
import com.uniware.core.api.shipping.GetShippingManifestItemDetailsResponse.ShippingManifestItemDetailDTO.ShippingPackageLineItem;
import com.uniware.core.api.shipping.GetShippingManifestRequest;
import com.uniware.core.api.shipping.GetShippingManifestResponse;
import com.uniware.core.api.shipping.ManifestDTO;
import com.uniware.core.api.shipping.ManifestDetailDTO;
import com.uniware.core.api.shipping.ManifestItemDTO;
import com.uniware.core.api.shipping.ManifestItemDTO.ManifestLineItem;
import com.uniware.core.api.shipping.ManifestItemDetailDTO;
import com.uniware.core.api.shipping.ManifestSummaryDTO;
import com.uniware.core.api.shipping.MarkSaleOrderItemsDeliveredRequest;
import com.uniware.core.api.shipping.MarkSaleOrderItemsDeliveredResponse;
import com.uniware.core.api.shipping.MarkShippingPackageDeliveredRequest;
import com.uniware.core.api.shipping.MarkShippingPackageDeliveredResponse;
import com.uniware.core.api.shipping.RemoveManifestItemRequest;
import com.uniware.core.api.shipping.RemoveManifestItemResponse;
import com.uniware.core.api.shipping.ResendPODCodeRequest;
import com.uniware.core.api.shipping.ResendPODCodeResponse;
import com.uniware.core.api.shipping.SearchManifestRequest;
import com.uniware.core.api.shipping.SearchManifestResponse;
import com.uniware.core.api.shipping.SearchManifestResponse.ShippingManifestDTO;
import com.uniware.core.api.shipping.ShippingManifestStatusDTO;
import com.uniware.core.api.shipping.ShippingManifestStatusDTO.FailedShippingPackageDTO;
import com.uniware.core.api.shipping.VerifySaleOrderItemsPODCodeRequest;
import com.uniware.core.api.shipping.VerifySaleOrderItemsPODCodeResponse;
import com.uniware.core.api.shipping.VerifyShippingPackagePODCodeRequest;
import com.uniware.core.api.shipping.VerifyShippingPackagePODCodeResponse;
import com.uniware.core.api.systemnotification.sandbox.SandboxParams;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.api.warehouse.ShippingPackageTypeDTO;
import com.uniware.core.cache.EnvironmentPropertiesCache;
import com.uniware.core.cache.FacilityCache;
import com.uniware.core.concurrent.ContextAwareExecutor;
import com.uniware.core.concurrent.ContextAwareExecutorFactory;
import com.uniware.core.entity.Channel;
import com.uniware.core.entity.ChannelConfigurationParameter;
import com.uniware.core.entity.ChannelConnector;
import com.uniware.core.entity.ChannelConnectorParameter;
import com.uniware.core.entity.Facility;
import com.uniware.core.entity.FacilityProfile;
import com.uniware.core.entity.InventoryLedger;
import com.uniware.core.entity.InventoryLedger.ChangeType;
import com.uniware.core.entity.Invoice;
import com.uniware.core.entity.InvoiceItem;
import com.uniware.core.entity.ItemType;
import com.uniware.core.entity.SaleOrder;
import com.uniware.core.entity.SaleOrderItem;
import com.uniware.core.entity.ShipmentTracking;
import com.uniware.core.entity.ShippingManifest;
import com.uniware.core.entity.ShippingManifestItem;
import com.uniware.core.entity.ShippingManifestStatus;
import com.uniware.core.entity.ShippingMethod;
import com.uniware.core.entity.ShippingPackage;
import com.uniware.core.entity.ShippingPackage.ShippingManager;
import com.uniware.core.entity.ShippingPackage.StatusCode;
import com.uniware.core.entity.ShippingPackageType;
import com.uniware.core.entity.ShippingProvider;
import com.uniware.core.entity.ShippingProviderMethod;
import com.uniware.core.entity.SmsTemplate;
import com.uniware.core.entity.Source;
import com.uniware.core.entity.SourceConnector;
import com.uniware.core.locking.ILockingService;
import com.uniware.core.locking.Namespace;
import com.uniware.core.locking.annotation.Lock;
import com.uniware.core.locking.annotation.Locks;
import com.uniware.core.script.error.SaleOrderScriptError;
import com.uniware.core.utils.ActivityContext;
import com.uniware.core.utils.Constants;
import com.uniware.core.utils.Constants.EmailTemplateType;
import com.uniware.core.utils.EnvironmentProperties;
import com.uniware.core.utils.UserContext;
import com.uniware.core.vo.PrintTemplateVO;
import com.uniware.core.vo.SamplePrintTemplateVO;
import com.uniware.dao.saleorder.ISaleOrderDao;
import com.uniware.dao.shipping.IShippingDao;
import com.uniware.services.audit.impl.ActivityEntityEnum;
import com.uniware.services.audit.impl.ActivityTypeEnum;
import com.uniware.services.audit.impl.ActivityUtils;
import com.uniware.services.cache.ChannelCache;
import com.uniware.services.cache.PrintTemplateCache;
import com.uniware.services.cache.SamplePrintTemplateCache;
import com.uniware.services.cache.ScriptVersionedCache;
import com.uniware.services.catalog.ICatalogService;
import com.uniware.services.configuration.EmailConfiguration;
import com.uniware.services.configuration.EmailConfiguration.EmailTemplateVO;
import com.uniware.services.configuration.ShippingConfiguration;
import com.uniware.services.configuration.ShippingFacilityLevelConfiguration;
import com.uniware.services.configuration.ShippingSourceConfiguration;
import com.uniware.services.configuration.SmsConfiguration;
import com.uniware.services.configuration.SmsConfiguration.SmsTemplateVO;
import com.uniware.services.configuration.SourceConfiguration;
import com.uniware.services.configuration.SystemConfiguration;
import com.uniware.services.configuration.data.manager.ProductConfiguration;
import com.uniware.services.document.IDocumentService;
import com.uniware.services.invoice.IInvoiceService;
import com.uniware.services.ledger.IInventoryLedgerService;
import com.uniware.services.publisher.IMessageBroadcaster;
import com.uniware.services.publisher.Message;
import com.uniware.services.saleorder.ISaleOrderService;
import com.uniware.services.shipping.IDispatchService;
import com.uniware.services.shipping.IRegulatoryService;
import com.uniware.services.shipping.IShippingInvoiceService;
import com.uniware.services.shipping.IShippingService;

/**
 * @author singla
 */
@Service("dispatchService")
public class DispatchServiceImpl implements IDispatchService {

    private static final Logger         LOG          = LoggerFactory.getLogger(DispatchServiceImpl.class);

    private static final String         SERVICE_NAME = "dispatchservice";
    @Autowired
    ISaleOrderService                   saleOrderService;
    @Autowired
    IInventoryLedgerService             inventoryLedgerService;
    private HttpSender                  httpSender   = new HttpSender();
    @Autowired
    private ApplicationContext          applicationContext;
    @Autowired
    private ILockingService             lockingService;
    @Autowired
    private IShippingDao                shippingDao;
    @Autowired
    private ISaleOrderDao               saleOrderDao;
    @Autowired
    private ICatalogService             catalogService;
    @Autowired
    private IInvoiceService             invoiceService;
    @Autowired
    private IEmailService               emailService;
    @Autowired
    private ISmsService                 smsService;
    @Autowired
    private IPdfDocumentService         pdfDocumentService;
    @Autowired
    private IRegulatoryService          regulatoryService;
    @Autowired
    private IShippingService            shippingService;
    @Autowired
    private ContextAwareExecutorFactory executorFactory;
    @Autowired
    private IMessageBroadcaster         messageBroadcaster;
    @Autowired
    private IDocumentService            documentService;
    @Autowired
    private IShippingInvoiceService     iShippingInvoiceService;

    @Override
    @Transactional
    public ShippingManifest addShippingManifest(ShippingManifest shippingManifest) {
        return shippingDao.addShippingManifest(shippingManifest);
    }

    @Override
    @Transactional
    public GetChannelsForManifestResponse getChannelsForManifest(GetChannelsForManifestRequest request) {
        GetChannelsForManifestResponse response = new GetChannelsForManifestResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            List<Channel> channels = shippingDao.getChannelsForManifest();
            List<ChannelDTO> manifestChannels = new ArrayList<>(channels.size());
            for (Channel channel : channels) {
                Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(channel.getSourceCode());
                manifestChannels.add(new ChannelDTO(channel, source));
            }
            response.setChannels(manifestChannels);
            response.setSuccessful(true);
        }
        return response;
    }

    @Override
    @Transactional(readOnly = true)
    public GetEligibleShippingProvidersForManifestResponse getShippingProvidersForManifest(GetEligibleShippingProvidersForManifestRequest request) {
        GetEligibleShippingProvidersForManifestResponse response = new GetEligibleShippingProvidersForManifestResponse();
        ValidationContext context = request.validate();
        Channel channel;
        if (!context.hasErrors()) {
            channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelByCode(request.getChannelCode());
            if (channel == null) {
                context.addError(WsResponseCode.INVALID_CHANNEL_CODE, "Invalid channel");
            } else {
                Map<String, ShippingProviderSearchDTO> codeToShippingProvider = new HashMap<>();
                List<Object[]> eligibleShippingProviders = shippingDao.getEligibleShippingProvidersForManifest(request.getChannelCode());
                prepareShippingProviders(codeToShippingProvider, eligibleShippingProviders);
                List<ShippingProviderSearchDTO> shippingProviders = new ArrayList<>(codeToShippingProvider.values().size());
                shippingProviders.addAll(codeToShippingProvider.values());
                response.setShippingProviders(shippingProviders);
                response.setSuccessful(true);
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    private void prepareShippingProviders(Map<String, ShippingProviderSearchDTO> codeToShippingProvider, List<Object[]> eligibleShippingProviders) {
        ShippingConfiguration shippingConfiguration = ConfigurationManager.getInstance().getConfiguration(ShippingConfiguration.class);
        for (Object[] shippingProviderAndMethod : eligibleShippingProviders) {
            String shippingProviderCode = (String) shippingProviderAndMethod[0];
            String shippingProviderName = (String) shippingProviderAndMethod[1];
            ShippingMethod shippingMethod = shippingConfiguration.getShippingMethodById((Integer) shippingProviderAndMethod[2]);
            if (!shippingMethod.getCode().equals(ShippingMethod.Code.PKP.name())) {
                ShippingManager shippingManager = (ShippingManager) shippingProviderAndMethod[3];
                ShippingProviderSearchDTO shippingProvider = codeToShippingProvider.get(shippingProviderCode);
                if (shippingProvider == null) {
                    shippingProvider = new ShippingProviderSearchDTO(shippingProviderCode, shippingProviderName);
                    codeToShippingProvider.put(shippingProviderCode, shippingProvider);
                }
                shippingProvider.getPacketTypes().add(ShippingManager.CHANNEL.equals(shippingManager) ? PacketType.CHANNEL : PacketType.VENDOR_SELF);
                shippingProvider.getShippingMethods().add(new ShippingMethodDTO(shippingMethod.getName(), shippingMethod.getName()));
            }
        }
    }

    @Override
    public CreateShippingManifestResponse createShippingManifest(CreateShippingManifestRequest request) {
        CreateShippingManifestResponse response = new CreateShippingManifestResponse();
        ValidationContext context = request.validate();
        Channel channel = null;
        ShippingMethod shippingMethod = null;
        if (!context.hasErrors()) {
            channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelByCode(request.getChannel());
            if (channel == null) {
                context.addError(WsResponseCode.INVALID_CHANNEL_CODE, "Invalid channel");
            } else {
                Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(channel.getSourceCode());
                if (StringUtils.isBlank(request.getShippingProviderCode()) && !source.isAllowCombinedManifest()) {
                    context.addError(WsResponseCode.INVALID_SHIPPING_PROVIDER_CODE, "The shipping provider code is invalid");
                } else {
                    ShippingConfiguration configuration = ConfigurationManager.getInstance().getConfiguration(ShippingConfiguration.class);
                    if (StringUtils.isBlank(request.getShippingMethodCode()) && !source.isAllowAnyShippingMethod()) {
                        context.addError(WsResponseCode.INVALID_SHIPPING_METHOD, "Any shipping method is not allowed for channel " + channel.getName());
                    } else if (StringUtils.isNotBlank(request.getShippingMethodCode()) && source.isManifestAllReadyToShipOrdersTogether()) {
                        context.addError(WsResponseCode.INVALID_SHIPPING_METHOD, "You have to create a single manifest for all ready to ship orders of this channel");
                    } else if (StringUtils.isNotBlank(request.getShippingMethodCode())) {
                        shippingMethod = configuration.getShippingMethodByCode(request.getShippingMethodCode());
                        if (shippingMethod == null) {
                            context.addError(WsResponseCode.INVALID_SHIPPING_METHOD, "Invalid shipping method");
                        } else if (shippingMethod.getCode().equals(ShippingMethod.Code.PKP.name())) {
                            context.addError(WsResponseCode.INVALID_SHIPPING_METHOD, "Manifest cannot be created for shipping method " + shippingMethod.getName());
                        }
                    }
                }
            }
        }
        if (!context.hasErrors()) {
            User user = new User();
            user.setId(request.getUserId());
            ShippingManifest shippingManifest = new ShippingManifest();
            shippingManifest.setUser(user);
            shippingManifest.setStatusCode(ShippingManifest.StatusCode.CREATED.name());
            if (StringUtils.isNotBlank(request.getShippingProviderCode())) {
                shippingManifest.setShippingProviderCode(request.getShippingProviderCode());
                shippingManifest.setShippingProviderName(request.getShippingProviderName());
            }
            shippingManifest.setCreated(DateUtils.getCurrentTime());
            shippingManifest.setComments(request.getComments());
            shippingManifest.setShippingMethod(shippingMethod);
            shippingManifest.setChannel(channel);
            shippingManifest.setShippingManager(request.getThirdPartyShipping() ? ShippingManager.CHANNEL
                    : ConfigurationManager.getInstance().getConfiguration(ProductConfiguration.class).isCourierManagementSwitchedOff() ? ShippingManager.NONE
                            : ShippingManager.UNIWARE);
            Map<String, Object> customFieldValues = CustomFieldUtils.getCustomFieldValues(context, ShippingManifest.class.getName(), request.getCustomFieldValues());
            CustomFieldUtils.setCustomFieldValues(shippingManifest, customFieldValues);
            addShippingManifest(shippingManifest);
            response.setSuccessful(true);
            response.setShippingManifestId(shippingManifest.getId());
            response.setShippingManifestCode(shippingManifest.getCode());
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    // TODO: 04/09/17 @ADITYA ERROR occuring here
    public AddShippingPackagesToManifestResponse addShippingPackagesToManifest(AddShippingPackagesToManifestRequest request) {
        AddShippingPackagesToManifestResponse response = new AddShippingPackagesToManifestResponse();
        ValidationContext context = request.validate();
        List<ManifestItemDTO> manifestItems = new ArrayList<>(request.getShippingPackageCodes().size());
        if (!context.hasErrors()) {
            for (String shippingPackageCode : request.getShippingPackageCodes()) {
                AddShippingPackageToManifestResponse addPShippingPackageResponse = addShippingPackageToManifest(
                        new AddShippingPackageToManifestRequest(request.getShippingManifestCode(), shippingPackageCode));
                if (addPShippingPackageResponse.isSuccessful()) {
                    manifestItems.add(addPShippingPackageResponse.getManifestItem());
                } else {
                    context.addErrors(addPShippingPackageResponse.getErrors());
                }
            }
        }
        if (!context.hasErrors()) {
            response.setSuccessful(true);
            response.setManifestItems(manifestItems);
        } else {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    @Override
    @LogActivity
    public AddShippingPackageToManifestResponse addShippingPackageToManifest(AddShippingPackageToManifestRequest request) {
        AddShippingPackageToManifestResponse response = new AddShippingPackageToManifestResponse();
        ValidationContext context = request.validate();
        ShippingManifest shippingManifest = null;
        ShippingPackage shippingPackage = null;
        if (!context.hasErrors()) {
            ReadWriteLock readWriteLock = lockingService.getReadWriteLock(Namespace.MANIFEST, request.getShippingManifestCode(), Level.FACILITY);
            try {
                Long timestamp = System.currentTimeMillis();
                readWriteLock.readLock().lock();
                LOG.info("[AddShippingPackageToManifest] Lock obtained in on namespace: {} and key: {} in {} ms", new Object[] {
                        Namespace.MANIFEST.name() + UserContext.current().getFacilityId(),
                        request.getShippingManifestCode(),
                        (System.currentTimeMillis() - timestamp) });
                Integer maxAllowedPackagesInManifest = CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class).getMaxNumberOfItemsInManifest();
                if (!context.hasErrors()) {
                    shippingManifest = getShippingManifestDetailedByCode(request.getShippingManifestCode());
                    if (shippingManifest == null || !shippingManifest.getStatusCode().equals(ShippingManifest.StatusCode.CREATED.name())) {
                        context.addError(WsResponseCode.INVALID_SHIPPING_MANIFEST_CODE, "Shipping Manifest CODE is invalid or manifest is closed");
                    } else if (shippingManifest.getShippingManifestItems().size() >= maxAllowedPackagesInManifest) {
                        context.addError(WsResponseCode.SHIPPING_MANIFEST_ITEM_LIMIT_REACHED, "Max package limit in manifest exceeded, limit: " + maxAllowedPackagesInManifest);
                    } else {
                        shippingPackage = validateAndGetPackageAddedToManifest(context, shippingManifest, request.getAwbOrShipmentCode());
                    }
                }
                if (!context.hasErrors()) {
                    doAddShippingPackageToManifest(context, shippingPackage, shippingManifest, request.getShippingPackageTypeCode());
                }
                if (!context.hasErrors()) {
                    response.setSuccessful(true);
                    response.setTotalManifestItemCount(shippingManifest.getShippingManifestItems().size());
                } else {
                    response.addErrors(context.getErrors());
                }
                if (shippingPackage != null) {
                    response.setManifestItem(prepareManifestItemDTO(shippingPackage));
                }
            } finally {
                readWriteLock.readLock().unlock();
                LOG.info("[AddShippingPackageToManifest] Lock released on namespace: {} and key: {} in {} ms", Namespace.MANIFEST.name(), request.getShippingManifestCode());
            }
        }
        return response;
    }

    @Transactional
    private ShippingPackage validateAndGetPackageAddedToManifest(ValidationContext context, ShippingManifest shippingManifest, String packageCodeOrAwb) {
        ShippingPackage shippingPackage = shippingService.getShippingPackageWithDetailsByCode(packageCodeOrAwb);
        boolean courierManagementSwitchedOff = ConfigurationManager.getInstance().getConfiguration(ProductConfiguration.class).isCourierManagementSwitchedOff();
        if (shippingPackage == null) {
            shippingPackage = shippingService.getShippingPackageWithDetailsByTrackingNumber(packageCodeOrAwb, shippingManifest.getShippingProviderCode());
        }
        if (shippingPackage == null) {
            context.addError(WsResponseCode.INVALID_SHIPPING_PACKAGE_CODE, "Invalid Package Code/AWB Number entered, try rescanning the package");
        } else if (!shippingPackage.getSaleOrder().getChannel().getId().equals(shippingManifest.getChannel().getId())) {
            context.addError(WsResponseCode.SHIPPING_PROVIDER_MISMATCH_PACKAGE_TO_MANIFEST,
                    "Only " + shippingManifest.getChannel().getName() + " channel package can be added to manifest");
        } else if (shippingPackage.isThirdPartyShipping() != shippingManifest.isThirdPartyShipping()) {
            context.addError(WsResponseCode.SHIPPING_PROVIDER_MISMATCH_PACKAGE_TO_MANIFEST,
                    "Only " + (shippingManifest.isThirdPartyShipping() ? "channel provided shipping" : "self shipping") + " packages can be added to this manifest");
        } else if (!ShippingPackage.StatusCode.READY_TO_SHIP.name().equals(shippingPackage.getStatusCode())) {
            context.addError(WsResponseCode.INVALID_PACKAGE_STATE, "[" + shippingPackage.getCode() + "] Only Ready To Ship packages can be added to manifest");
        } else if (!courierManagementSwitchedOff && ShippingManager.UNIWARE.equals(shippingPackage.getShippingManager())
                && ConfigurationManager.getInstance().getConfiguration(ShippingFacilityLevelConfiguration.class).getShippingProviderMethod(
                        shippingPackage.getShippingProviderCode(), shippingPackage.getShippingMethod().getCode(), shippingPackage.getShippingMethod().isCashOnDelivery()) == null) {
            context.addError(WsResponseCode.INVALID_SHIPPING_PROVIDER_CODE,
                    "Either shipping provider or shipping provider method is disabled for " + shippingPackage.getShippingProviderCode());
        } else if (!courierManagementSwitchedOff && ShippingPackage.StatusCode.PACKED.name().equals(shippingPackage.getStatusCode())
                && shippingPackage.getShippingProviderCode() != null
                && ShippingProviderMethod.TrackingNumberGeneration.MANUAL.name().equals(
                        ConfigurationManager.getInstance().getConfiguration(ShippingFacilityLevelConfiguration.class).getShippingProviderMethod(
                                shippingPackage.getShippingProviderCode(), shippingPackage.getShippingMethod().getCode(),
                                shippingPackage.getShippingMethod().isCashOnDelivery()).getTrackingNumberGeneration())) {
            context.addError(WsResponseCode.MANUAL_TRACKING_NUMBER_NOT_ASSIGNED, "manual tracking number not assigned");
        } else if (shippingPackage.getShippingManifest() != null) {
            context.addError(WsResponseCode.INVALID_SHIPPING_PACKAGE_CODE,
                    "Package has been already added to Shipping Manifest: " + shippingPackage.getShippingManifest().getCode());
        } else if (shippingManifest.getShippingMethod() != null && !shippingPackage.getShippingMethod().getId().equals(shippingManifest.getShippingMethod().getId())) {
            context.addError(WsResponseCode.SHIPPING_PROVIDER_MISMATCH_PACKAGE_TO_MANIFEST,
                    "Only " + shippingManifest.getShippingMethod().getName() + " package can be added to this manifest");
        } else if (shippingManifest.getShippingProviderCode() != null && shippingPackage.getShippingProviderCode() != null
                && !shippingManifest.getShippingProviderCode().equalsIgnoreCase(shippingPackage.getShippingProviderCode())) {
            context.addError(WsResponseCode.SHIPPING_PROVIDER_MISMATCH_PACKAGE_TO_MANIFEST,
                    "Only " + shippingManifest.getShippingProviderName() + " package can be added to this manifest");
        } else if (shippingPackage.getShippingMethod().getCode().equals(ShippingMethod.Code.PKP.name())) {
            context.addError(WsResponseCode.INVALID_SHIPPING_PACKAGE_CODE,
                    "Package with shipping method " + shippingPackage.getShippingMethod().getName() + " cannot be added to manifest");
        }
        return shippingPackage;
    }

    @Override
    @Transactional
    public GetShippingManifestResponse getShippingManifest(GetShippingManifestRequest request) {
        GetShippingManifestResponse response = new GetShippingManifestResponse();
        Long start = System.currentTimeMillis();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            Long timestamp = System.currentTimeMillis();
            ShippingManifest shippingManifest = shippingDao.getShippingManifestDetailedByCode(request.getShippingManifestCode());
            LOG.info("DB query ran in " + (System.currentTimeMillis() - timestamp) + " ms");
            if (shippingManifest == null) {
                context.addError(WsResponseCode.INVALID_SHIPPING_MANIFEST_ID, "Invalid shipping manifest");
            } else {
                response.setSuccessful(true);
                response.setShippingManifest(getManifestDTO(shippingManifest));
            }
        }
        response.setErrors(context.getErrors());
        LOG.info("get manifest ran in " + (System.currentTimeMillis() - start) + " ms");
        return response;
    }

    @Transactional
    @Override
    public ShippingManifest getShippingManifestByCode(String shippingManifestCode) {
        return shippingDao.getShippingManifestByCode(shippingManifestCode);
    }

    @Transactional
    @Override
    public ShippingManifest getShippingManifestDetailedByCode(String shippingManifestCode) {
        ShippingManifest shippingManifest = shippingDao.getShippingManifestDetailedByCode(shippingManifestCode);
        if (shippingManifest != null) {
            Hibernate.initialize(shippingManifest.getChannel());
            Hibernate.initialize(shippingManifest.getShippingMethod());
            Hibernate.initialize(shippingManifest.getUser());
            for (ShippingManifestItem smi : shippingManifest.getShippingManifestItems()) {
                Hibernate.initialize(smi.getShippingPackage().getSaleOrder().getCode());
            }
        }
        return shippingManifest;
    }

    @Locks({ @Lock(ns = Namespace.SALE_ORDER, key = "#{#args[1].saleOrder.code}") })
    @Transactional
    @RollbackOnFailure
    private DoAddShippingPackageToManifestResponse doAddShippingPackageToManifest(ValidationContext context, ShippingPackage shippingPackage, ShippingManifest shippingManifest,
            String shippingPackageTypeCode) {
        DoAddShippingPackageToManifestResponse response = new DoAddShippingPackageToManifestResponse();
        boolean logActivity = ActivityContext.current().isEnable();
        List<String> activityLogs = new ArrayList<>();
        shippingManifest = shippingDao.getShippingManifestByCode(shippingManifest.getCode());
        if (!ShippingManifest.StatusCode.CREATED.name().equals(shippingManifest.getStatusCode())) {
            context.addError(WsResponseCode.INVALID_SHIPPING_MANIFEST_STATE, "Shipping Manifest not in CREATED state");
        }

        if (!context.hasErrors()) {
            shippingPackage = shippingDao.getShippingPackageById(shippingPackage.getId(), true);
            validateShippingPackageStateForManifest(context, shippingPackage);
        }

        ShippingManifestItem shippingManifestItem = null;
        if (!context.hasErrors()) {
            if (ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).isSelectPackageTypeBeforeDispatch() && shippingPackageTypeCode != null) {
                ShippingPackageType shippingPackageType = ConfigurationManager.getInstance().getConfiguration(
                        ShippingFacilityLevelConfiguration.class).getShippingPackageTypeByCode(shippingPackageTypeCode);
                if (shippingPackageType != null) {
                    shippingPackage.setShippingPackageType(shippingPackageType);
                    shippingPackage.setBoxLength(shippingPackageType.getBoxLength());
                    shippingPackage.setBoxWidth(shippingPackageType.getBoxWidth());
                    shippingPackage.setBoxHeight(shippingPackageType.getBoxHeight());
                    shippingPackage.setActualWeight(shippingPackage.getActualWeight() + shippingPackageType.getBoxWeight());
                    activityLogs.add("Type: " + shippingPackage.getShippingPackageType().getCode() + " Length:" + shippingPackage.getBoxLength() + " Width:"
                            + shippingPackage.getBoxWidth() + " Height:" + shippingPackage.getBoxHeight() + " Weight:" + shippingPackage.getActualWeight());
                }
            }
            shippingPackage.setShippingManifest(shippingManifest);
            shippingManifestItem = new ShippingManifestItem(shippingManifest, shippingPackage, shippingPackage.getTrackingNumber(), DateUtils.getCurrentTime(),
                    DateUtils.getCurrentTime());
            shippingManifestItem = shippingDao.addShippingManifestItem(shippingManifestItem);
            shippingManifest.getShippingManifestItems().add(shippingManifestItem);
            String orderItems = "";
            for (SaleOrderItem saleOrderItem : shippingPackage.getSaleOrderItems()) {
                saleOrderItem.setStatusCode(SaleOrderItem.StatusCode.MANIFESTED.name());
                saleOrderService.updateSaleOrderItem(saleOrderItem);
                orderItems = orderItems.concat(saleOrderItem.getCode() + ",");
            }
            shippingPackage.setStatusCode(ShippingPackage.StatusCode.MANIFESTED.name());
            activityLogs.add("{" + ActivityEntityEnum.SHIPPING_PACKAGE + ":" + shippingPackage.getCode() + "} with Order Items {" + ActivityEntityEnum.SALE_ORDER_ITEM + ":"
                    + orderItems.substring(0, orderItems.lastIndexOf(",")) + "} MANIFESTED. Manifest Code {" + ActivityEntityEnum.MANIFEST + ":" + shippingManifest.getCode()
                    + "}");
            if (ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).isDispatchShippingPackageOnAddingToManifest()) {
                DispatchShippingPackageResponse dispatchShippingPackageResponse = dispatchShippingPackageInternal(shippingPackage);
                if (!dispatchShippingPackageResponse.isSuccessful()) {
                    context.addErrors(dispatchShippingPackageResponse.getErrors());
                }
            }
            if (!context.hasErrors()) {
                if (logActivity) {
                    ActivityUtils.appendActivity(shippingPackage.getCode(), ActivityEntityEnum.SHIPPING_PACKAGE.getName(), shippingPackage.getSaleOrder().getCode(), activityLogs,
                            ActivityTypeEnum.PROCESSING.name());
                }
            }
        }
        if (context.hasErrors()) {
            response.addErrors(context.getErrors());
        }
        response.setSuccessful(!context.hasErrors());
        return response;
    }

    private void validateShippingPackageStateForManifest(ValidationContext context, ShippingPackage shippingPackage) {
        if (ShippingPackage.StatusCode.PACKED.name().equals(shippingPackage.getStatusCode()) && shippingPackage.getShippingProviderCode() != null
                && shippingPackage.isManualAwbAllocation()) {
            context.addError(WsResponseCode.MANUAL_TRACKING_NUMBER_NOT_ASSIGNED, "manual tracking number not assigned");
        } else if (!ShippingPackage.StatusCode.READY_TO_SHIP.name().equals(shippingPackage.getStatusCode())) {
            context.addError(WsResponseCode.INVALID_PACKAGE_STATE, "[" + shippingPackage.getCode() + "] Only Ready To Ship packages can be added to manifest");
        } else {
            checkIfPutawayPendingOrOnHold(shippingPackage, context);
        }
        if (!context.hasErrors()) {
            verifyShippingPackageOnChannelForManifest(context, shippingPackage);
        }
    }

    private void checkIfPutawayPendingOrOnHold(ShippingPackage shippingPackage, ValidationContext context) {
        for (SaleOrderItem saleOrderItem : shippingPackage.getSaleOrderItems()) {
            if (SaleOrderItem.StatusCode.CANCELLED.name().equals(saleOrderItem.getStatusCode())) {
                context.addError(WsResponseCode.SALE_ORDER_ONE_OR_MORE_ITEMS_ARE_CANCELLED,
                        "[" + shippingPackage.getCode() + "] One or more items are cancelled. Repackage before dispatch.");
                break;
            } else if (saleOrderItem.isOnHold()) {
                context.addError(WsResponseCode.SHIPPING_PACKAGE_IS_ON_HOLD, "[" + shippingPackage.getCode() + "] Shipping Package is currently on HOLD, Wait for this package");
                break;
            }
        }
    }

    private void verifyShippingPackageOnChannelForManifest(ValidationContext context, ShippingPackage shippingPackage) {
        Channel channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelById(shippingPackage.getSaleOrder().getChannel().getId());
        String manifestVerificationScript = Source.MANIFEST_VERIFICATION_SCRIPT_NAME;
        if (StringUtils.isNotEmpty(manifestVerificationScript)) {
            ScraperScript script = CacheManager.getInstance().getCache(ChannelCache.class).getScriptByName(channel.getCode(), manifestVerificationScript);
            if (script != null) {
                ScriptExecutionContext seContext = ScriptExecutionContext.current();
                seContext.addVariable("shippingPackage", shippingPackage);
                seContext.addVariable("channel", channel);
                seContext.setScriptProvider(new IScriptProvider() {

                    @Override
                    public ScraperScript getScript(String scriptName) {
                        return CacheManager.getInstance().getCache(ScriptVersionedCache.class).getScriptByName(scriptName);
                    }
                });
                for (ChannelConnector channelConnector : channel.getChannelConnectors()) {
                    for (ChannelConnectorParameter channelConnectorParameter : channelConnector.getChannelConnectorParameters()) {
                        seContext.addVariable(channelConnectorParameter.getName(), channelConnectorParameter.getValue());
                    }
                }
                for (ChannelConfigurationParameter channelConfigurationParameter : channel.getChannelConfigurationParameters()) {
                    seContext.addVariable(channelConfigurationParameter.getSourceConfigurationParameterName(), channelConfigurationParameter.getValue());
                }
                SandboxParams sandboxParameters = getSandboxParameters(channel, shippingPackage);
                try {
                    boolean traceLoggingEnabled = UserContext.current().isTraceLoggingEnabled();
                    if (traceLoggingEnabled) {
                        LOG.info("SandboxParameters for manifest verification script: {}", JsonUtils.objectToString(sandboxParameters));
                    }
                    seContext.setTraceLoggingEnabled(traceLoggingEnabled);
                    LOG.info("Executing manifest verification script for shipping package: {}", shippingPackage.getCode());
                    script.execute();
                    LOG.info("Successfuly executed check if cancelled script for shipping package: {}", shippingPackage.getCode());
                } catch (ScriptExecutionException ex) {
                    if (StringUtils.isNotEmpty(ex.getReasonCode()) && SaleOrderScriptError.getScriptError(ex.getReasonCode()).equals(SaleOrderScriptError.ORDER_CANCELLED)) {
                        LOG.info("ShippingPackage: {} is CANCELLED on channel", shippingPackage.getCode());
                    } else {
                        LOG.error("ScriptExecutionException while running check if cancelled script for shipping package: {}", shippingPackage.getCode(), ex);
                    }
                    context.addError(WsResponseCode.INVALID_SALE_ORDER_ITEM_STATE, "[" + shippingPackage.getCode() + "] " + ex.getMessage());
                } catch (Exception e) {
                    LOG.error("Unable to execute manifest verification script for shippingPackage:" + shippingPackage.getCode(), e);
                    context.addError(WsResponseCode.INVALID_SALE_ORDER_ITEM_STATE, "[" + shippingPackage.getCode() + "] " + e.getMessage());
                } finally {
                    ScriptExecutionContext.destroy();
                }
            }
        }
    }

    @Override
    public DispatchSaleOrderItemsResponse dispatchSaleOrderItems(DispatchSaleOrderItemsRequest request) {
        DispatchSaleOrderItemsResponse response = new DispatchSaleOrderItemsResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            SaleOrder saleOrder = saleOrderService.getSaleOrderByCode(request.getSaleOrderCode());
            if (saleOrder == null) {
                context.addError(WsResponseCode.INVALID_SALE_ORDER_CODE);
            } else {
                ShippingPackage shippingPackage = shippingService.getValidShippingPackage(request.getSaleOrderCode(), request.getSaleOrderItemCodes());
                if (shippingPackage == null) {
                    context.addError(WsResponseCode.INVALID_SHIPPING_PACKAGE_CODE, "Sale order item codes do not correspond to a package");
                } else {
                    DispatchShippingPackageRequest dispatchShippingPackageRequest = new DispatchShippingPackageRequest();
                    dispatchShippingPackageRequest.setShippingPackageCode(shippingPackage.getCode());
                    response.setResponse(dispatchReadyToShipShippingPackage(dispatchShippingPackageRequest));
                }
            }
        }
        return response;
    }

    @Override
    public DispatchShippingPackageResponse dispatchReadyToShipShippingPackage(DispatchShippingPackageRequest request) {
        DispatchShippingPackageResponse response = new DispatchShippingPackageResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            ShippingPackage shippingPackage = shippingService.getShippingPackageByCode(request.getShippingPackageCode());
            if (shippingPackage == null) {
                context.addError(WsResponseCode.INVALID_SHIPPING_PACKAGE_CODE);
            } else {
                response = doDispatchReadyToShipShippingPackage(response, context, shippingPackage);
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    @Locks({ @Lock(ns = Namespace.SALE_ORDER, key = "#{#args[2].saleOrder.code}") })
    @RollbackOnFailure
    @Transactional
    private DispatchShippingPackageResponse doDispatchReadyToShipShippingPackage(DispatchShippingPackageResponse response, ValidationContext context,
            ShippingPackage shippingPackage) {
        shippingPackage = shippingDao.getShippingPackageById(shippingPackage.getId());
        validateShippingPackageStateForManifest(context, shippingPackage);
        if (!context.hasErrors()) {
            for (SaleOrderItem saleOrderItem : shippingPackage.getSaleOrderItems()) {
                saleOrderItem.setStatusCode(SaleOrderItem.StatusCode.MANIFESTED.name());
                saleOrderService.updateSaleOrderItem(saleOrderItem);
            }
            shippingPackage.setStatusCode(StatusCode.MANIFESTED.name());
            response = dispatchShippingPackageInternal(shippingPackage);
        }
        return response;
    }

    @Override
    @Transactional
    public MarkShippingPackageDeliveredResponse markShippingPackageDelivered(MarkShippingPackageDeliveredRequest request) {
        MarkShippingPackageDeliveredResponse response = new MarkShippingPackageDeliveredResponse();
        ValidationContext context = request.validate();
        ShippingPackage shippingPackage;
        if (!context.hasErrors()) {
            shippingPackage = shippingService.getShippingPackageByCode(request.getShippingPackageCode());
            if (shippingPackage == null) {
                context.addError(WsResponseCode.INVALID_SHIPPING_PACKAGE_CODE);
            } else {
                ShippingMethod shippingMethod = ConfigurationManager.getInstance().getConfiguration(ShippingConfiguration.class).getShippingMethodById(
                        shippingPackage.getShippingMethod().getId());
                Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(
                        shippingPackage.getSaleOrder().getChannel().getSourceCode());
                if (ShippingMethod.Code.PKP.name().equals(shippingMethod.getCode())
                        || (source.isPODRequired() && !ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).isRestrictOTPGeneration())) {
                    if (!StringUtils.equalsAny(shippingPackage.getStatusCode(), StatusCode.READY_TO_SHIP.name(), StatusCode.DISPATCHED.name())) {
                        context.addError(WsResponseCode.INVALID_PACKAGE_STATE);
                    } else if (!shippingPackage.isPodVerified() && StringUtils.isNotBlank(shippingPackage.getPodCode())
                            && !shippingPackage.getPodCode().equals(request.getPodCode())) {
                        context.addError(WsResponseCode.INVALID_POD_CODE, "Code entered is invalid");
                    }
                } else {
                    if (!StringUtils.equalsAny(shippingPackage.getStatusCode(), StatusCode.DISPATCHED.name())) {
                        context.addError(WsResponseCode.INVALID_PACKAGE_STATE);
                    }
                }
                if (!context.hasErrors()) {
                    doMarkShippingPackageDelivered(shippingPackage, context, response);
                }
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    @Override
    @Transactional
    public ResendPODCodeResponse resendPODCode(ResendPODCodeRequest request) {
        ResendPODCodeResponse response = new ResendPODCodeResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            SaleOrder saleOrder = saleOrderService.getSaleOrderByCode(request.getSaleOrderCode());
            if (saleOrder == null) {
                context.addError(WsResponseCode.INVALID_SALE_ORDER_CODE, request.getSaleOrderCode());
            } else {
                Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(saleOrder.getChannel().getSourceCode());
                if (!source.isPODRequired() || ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).isRestrictOTPGeneration()) {
                    context.addError(WsResponseCode.INVALID_SOURCE, "Action not applicable for source");
                } else {
                    List<ShippingPackage> shippingPackages = shippingService.getAllShippingPackagesBySaleOrder(request.getSaleOrderCode());
                    if (shippingPackages == null || shippingPackages.isEmpty()) {
                        context.addError(WsResponseCode.INVALID_PACKAGE_STATE, "No packages exists for sale order " + request.getSaleOrderCode());
                    } else {
                        Map<String, String> soiCodeToPackageCode = new HashMap<>();
                        Map<String, ShippingPackage> packageCodeToShippingPackage = new HashMap<>();
                        Map<String, Set<String>> spCodeToSaleOrderItemCodes = new HashMap<>();
                        for (ShippingPackage shippingPackage : shippingPackages) {
                            packageCodeToShippingPackage.put(shippingPackage.getCode(), shippingPackage);
                            for (SaleOrderItem saleOrderItem : shippingPackage.getSaleOrderItems()) {
                                soiCodeToPackageCode.put(saleOrderItem.getCode(), shippingPackage.getCode());
                                Set<String> soiCodes = spCodeToSaleOrderItemCodes.get(shippingPackage.getCode());
                                if (soiCodes == null) {
                                    soiCodes = new HashSet<>();
                                    spCodeToSaleOrderItemCodes.put(shippingPackage.getCode(), soiCodes);
                                }
                                soiCodes.add(saleOrderItem.getCode());
                            }
                        }
                        String packageCode = soiCodeToPackageCode.get(request.getSaleOrderItemCodes().get(0));
                        List<String> requestSaleOrderItems = new ArrayList<>(request.getSaleOrderItemCodes());
                        Set<String> packageSaleOrderItems = spCodeToSaleOrderItemCodes.get(packageCode);
                        for (String saleOrderItemCode : request.getSaleOrderItemCodes()) {
                            if (packageSaleOrderItems != null && packageSaleOrderItems.contains(saleOrderItemCode)) {
                                packageSaleOrderItems.remove(saleOrderItemCode);
                                requestSaleOrderItems.remove(saleOrderItemCode);
                            }
                        }
                        if (packageSaleOrderItems == null || !packageSaleOrderItems.isEmpty() || !requestSaleOrderItems.isEmpty()) {
                            context.addError(WsResponseCode.INVALID_SALE_ORDER_ITEM_CODE, "Invalid order item combination.They do not belong to same package");
                        }
                        if (!context.hasErrors()) {
                            ShippingPackage shippingPackage = packageCodeToShippingPackage.get(packageCode);
                            SmsConfiguration.SmsTemplateVO smsTemplate = ConfigurationManager.getInstance().getConfiguration(SmsConfiguration.class).getTemplateByName(
                                    SmsTemplate.Name.PACKAGE_CREATED.name());
                            String notificationMobile = shippingPackage.getSaleOrder().getNotificationMobile();
                            LOG.info("Sending package creation sms");
                            if (smsTemplate != null && smsTemplate.isEnabled() && StringUtils.isNotBlank(notificationMobile)) {
                                try {
                                    Facility facility = CacheManager.getInstance().getCache(FacilityCache.class).getFacilityById(shippingPackage.getFacility().getId());
                                    FacilityProfile facilityProfile = CacheManager.getInstance().getCache(FacilityCache.class).getFacilityProfileByCode(facility.getCode());
                                    SmsMessage message = new SmsMessage(notificationMobile, SmsTemplate.Name.PACKAGE_CREATED.name());
                                    message.addTemplateParam("shippingPackage", shippingPackage);
                                    message.addTemplateParam("facility", facility);
                                    message.addTemplateParam("facilityProfile", facilityProfile);
                                    if (ShippingMethod.Code.STD.name().equals(shippingPackage.getShippingMethod().getCode())) {
                                        if (facilityProfile != null) {
                                            Integer SLA = facilityProfile.getDispatchSLA() + facilityProfile.getDeliverySLA();
                                            message.addTemplateParam("SLA", SLA);
                                        }
                                    }
                                    smsService.send(message, source.getCode());
                                    response.setSuccessful(true);
                                } catch (Exception e) {
                                    LOG.error("Error occured while sending package dispatched sms:", e);
                                }
                            }
                        }
                    }
                }
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    @Locks({ @Lock(ns = Namespace.SALE_ORDER, key = "#{#args[0].saleOrder.code}") })
    @Transactional
    @RollbackOnFailure
    private void doMarkShippingPackageDelivered(ShippingPackage shippingPackage, ValidationContext context, MarkShippingPackageDeliveredResponse response) {
        shippingPackage = shippingService.getShippingPackageByCode(shippingPackage.getCode());
        ShippingMethod shippingMethod = ConfigurationManager.getInstance().getConfiguration(ShippingConfiguration.class).getShippingMethodById(
                shippingPackage.getShippingMethod().getId());
        if (ShippingMethod.Code.PKP.name().equals(shippingMethod.getCode())) {
            if (!StringUtils.equalsAny(shippingPackage.getStatusCode(), StatusCode.READY_TO_SHIP.name())) {
                context.addError(WsResponseCode.INVALID_PACKAGE_STATE);
            }
            markShippingPackageDispatched(shippingPackage, false);
            DispatchShippingPackageResponse dispatchShippingPackageResponse = dispatchShippingPackageOnChannel(shippingPackage);
            if (dispatchShippingPackageResponse.hasErrors()) {
                context.addErrors(dispatchShippingPackageResponse.getErrors());
            }
        } else {
            if (!StringUtils.equalsAny(shippingPackage.getStatusCode(), StatusCode.DISPATCHED.name())) {
                context.addError(WsResponseCode.INVALID_PACKAGE_STATE);
            }
        }

        if (!context.hasErrors()) {
            LOG.info("Marking shipping package as DELIVERED");
            shippingPackage.setStatusCode(StatusCode.DELIVERED.name());
            shippingPackage.setDeliveryTime(DateUtils.getCurrentTime());
            shippingPackage.setPodVerified(true);
            for (SaleOrderItem saleOrderItem : shippingPackage.getSaleOrderItems()) {
                if (SaleOrderItem.StatusCode.DISPATCHED.name().equals(saleOrderItem.getStatusCode())) {
                    saleOrderItem.setStatusCode(SaleOrderItem.StatusCode.DELIVERED.name());
                    saleOrderDao.updateSaleOrderItem(saleOrderItem);
                }
            }
            response.setSuccessful(true);
        }

        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
    }

    @Override
    public MarkSaleOrderItemsDeliveredResponse markSaleOrderItemsDelivered(MarkSaleOrderItemsDeliveredRequest request) {
        MarkSaleOrderItemsDeliveredResponse response = new MarkSaleOrderItemsDeliveredResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            SaleOrder saleOrder = saleOrderService.getSaleOrderByCode(request.getSaleOrderCode());
            if (saleOrder == null) {
                context.addError(WsResponseCode.INVALID_SALE_ORDER_CODE);
            } else {
                ShippingPackage shippingPackage = shippingService.getValidShippingPackage(request.getSaleOrderCode(), request.getSaleOrderItemCodes());
                if (shippingPackage == null) {
                    context.addError(WsResponseCode.INVALID_SHIPPING_PACKAGE_CODE, "Sale order item codes do not correspond to a package");
                } else {
                    MarkShippingPackageDeliveredRequest markShippingPackageDeliveredRequest = new MarkShippingPackageDeliveredRequest();
                    markShippingPackageDeliveredRequest.setShippingPackageCode(shippingPackage.getCode());
                    markShippingPackageDeliveredRequest.setPodCode(request.getPodCode());
                    response.setResponse(markShippingPackageDelivered(markShippingPackageDeliveredRequest));
                }
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    @Locks({ @Lock(ns = Namespace.SALE_ORDER, key = "#{#args[0].saleOrder.code}") })
    @Transactional
    private DispatchShippingPackageResponse dispatchShippingPackageInternal(ShippingPackage shippingPackage) {
        DispatchShippingPackageResponse response = new DispatchShippingPackageResponse();
        ValidationContext context = new ValidationContext();
        shippingPackage = shippingDao.getShippingPackageById(shippingPackage.getId());
        Channel channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelById(shippingPackage.getSaleOrder().getChannel().getId());
        Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(channel.getSourceCode());
        if (!StatusCode.MANIFESTED.name().equals(shippingPackage.getStatusCode())) {
            context.addError(WsResponseCode.INVALID_PACKAGE_STATE, "[" + shippingPackage.getCode() + "] Package not in MANIFESTED state");
        } else {
            response = dispatchShippingPackageOnChannel(shippingPackage);
            if (!response.hasErrors() && !source.isDispatchPackageOnManifestClose()) {
                markShippingPackageDispatched(shippingPackage, false);
                response.setSuccessful(true);
                response.setShippingPackageCode(shippingPackage.getCode());
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    private DispatchShippingPackageResponse dispatchShippingPackageOnChannel(ShippingPackage shippingPackage) {
        DispatchShippingPackageResponse response = new DispatchShippingPackageResponse();
        ValidationContext context = new ValidationContext();
        Channel channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelById(shippingPackage.getSaleOrder().getChannel().getId());
        Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(channel.getSourceCode());
        String dispatchScriptName = CacheManager.getInstance().getCache(ChannelCache.class).getScriptName(channel.getCode(), Source.DISPATCH_VERIFICATION_SCRIPT_NAME);
        if (dispatchScriptName != null) {
            ScraperScript script = CacheManager.getInstance().getCache(ScriptVersionedCache.class).getScriptByName(dispatchScriptName);
            SaleOrderScriptError scriptError;
            if (script != null) {
                ScriptExecutionContext seContext = ScriptExecutionContext.current();
                seContext.addVariable("shippingPackage", shippingPackage);
                seContext.addVariable("channel", channel);
                seContext.addVariable("source", source);
                seContext.addVariable("applicationContext", applicationContext);
                seContext.setScriptProvider(new IScriptProvider() {

                    @Override
                    public ScraperScript getScript(String scriptName) {
                        return CacheManager.getInstance().getCache(ScriptVersionedCache.class).getScriptByName(scriptName);
                    }
                });
                for (ChannelConnector channelConnector : channel.getChannelConnectors()) {
                    SourceConnector sourceConnector = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceConnector(channel.getSourceCode(),
                            channelConnector.getSourceConnectorName());
                    if (sourceConnector.isRequiredInOrderSync()) {
                        if (ChannelConnector.Status.INVALID_CREDENTIALS.equals(channelConnector.getStatusCode())) {
                            context.addError(WsResponseCode.INVALID_SHIPPING_PACKAGE_STATE, "Connector broken");
                            response.setErrors(context.getErrors());
                            return response;
                        }
                    }
                    for (ChannelConnectorParameter channelConnectorParameter : channelConnector.getChannelConnectorParameters()) {
                        seContext.addVariable(channelConnectorParameter.getName(), channelConnectorParameter.getValue());
                    }
                }
                for (ChannelConfigurationParameter channelConfigurationParameter : channel.getChannelConfigurationParameters()) {
                    seContext.addVariable(channelConfigurationParameter.getSourceConfigurationParameterName(), channelConfigurationParameter.getValue());
                }
                SandboxParams sandboxParameters = getSandboxParameters(channel, shippingPackage);
                try {
                    boolean traceLoggingEnabled = UserContext.current().isTraceLoggingEnabled();
                    if (traceLoggingEnabled) {
                        LOG.info("SandboxParameters for dispatch verification script: {}", JsonUtils.objectToString(sandboxParameters));
                    }
                    LOG.info("Executing dispatch verification script for package code: {}", shippingPackage.getCode());
                    seContext.setTraceLoggingEnabled(traceLoggingEnabled);
                    script.execute();
                    LOG.info("Successfully executed dispatch verification script for package code: {}", shippingPackage.getCode());
                    shippingPackage.setDispatchedOnChannel(true);
                    if (ActivityContext.current().isEnable()) {
                        ActivityUtils.appendActivity(shippingPackage.getCode(), ActivityEntityEnum.SHIPPING_PACKAGE.getName(), shippingPackage.getSaleOrder().getCode(),
                                Arrays.asList(new String[] {
                                        "Shipping package {" + ActivityEntityEnum.SHIPPING_PACKAGE + ":" + shippingPackage.getCode() + "} dispatched on "
                                                + shippingPackage.getSaleOrder().getChannel().getCode() }),
                                ActivityTypeEnum.PROCESSING.name());
                    }
                } catch (ScriptExecutionException ex) {
                    scriptError = SaleOrderScriptError.getScriptError(ex.getReasonCode());
                    LOG.error("ScriptExecutionException while executing dispatchVerificationScript for shippingPackage:" + shippingPackage.getCode(), ex);
                    context.addError(WsResponseCode.INVALID_SHIPPING_PACKAGE_STATE, ex.getMessage());
                    if (SaleOrderScriptError.ORDER_CANCELLED.equals(scriptError)) {
                        response.setCancelledOnChannel(true);
                    }
                } catch (Exception e) {
                    String errorMessage = e.getMessage();
                    LOG.error("Unable to execute dispatchVerificationScript for shippingPackage:" + shippingPackage.getCode(), e);
                    context.addError(WsResponseCode.INVALID_SHIPPING_PACKAGE_STATE, e.getMessage());
                } finally {
                    ScriptExecutionContext.destroy();
                }
            }
        } else {
            shippingPackage.setDispatchedOnChannel(true);
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        } else {
            response.setSuccessful(true);
        }
        return response;
    }

    @Transactional
    private void markShippingPackageDispatched(ShippingPackage shippingPackage, boolean forceDispatch) {
        shippingPackage = shippingService.getShippingPackageByCode(shippingPackage.getCode());
        shippingPackage.setStatusCode(ShippingPackage.StatusCode.DISPATCHED.name());
        shippingPackage.setDispatchedOnChannel(true);
        shippingPackage.setDispatchTime(DateUtils.getCurrentTime());
        if (ShippingManager.UNIWARE.equals(shippingPackage.getShippingManager())) {
            ShippingProvider shippingProvider = ConfigurationManager.getInstance().getConfiguration(ShippingConfiguration.class).getShippingProviderByCode(
                    shippingPackage.getShippingProviderCode());
            ShipmentTracking shipmentTracking = new ShipmentTracking(ShipmentTracking.StatusCode.NO_INFORMATION.name(), ShipmentTracking.Type.SHIP_TO_CUSTOMER.name(),
                    shippingProvider, shippingPackage.getTrackingNumber(), DateUtils.getCurrentTime(), DateUtils.getCurrentTime());
            addShipmentTracking(shipmentTracking);
        }
        shippingService.updateShippingPackage(shippingPackage);
        String orderItems = "";
        for (SaleOrderItem saleOrderItem : shippingPackage.getSaleOrderItems()) {
            saleOrderItem.setStatusCode(SaleOrderItem.StatusCode.DISPATCHED.name());
            saleOrderService.updateSaleOrderItem(saleOrderItem);
            orderItems = orderItems.concat(saleOrderItem.getCode() + ",");
            addInventoryLedgerEntry(saleOrderItem);
        }
        Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(shippingPackage.getSaleOrder().getChannel().getSourceCode());
        if (!source.isRestrictCustomerNotificationOnDispatch()) {
            if (ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).isSendEmailOnPackageDispatch()) {
                notifyCustomerViaEmail(shippingPackage);
            }
            if (ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).isSendSmsOnPackageDispatch()) {
                notifyCustomerViaSms(shippingPackage);
            }
        }
        if (ActivityContext.current().isEnable()) {
            ActivityUtils.appendActivity(shippingPackage.getCode(), ActivityEntityEnum.SHIPPING_PACKAGE.getName(), shippingPackage.getSaleOrder().getCode(),
                    Arrays.asList(new String[] {
                            "Shipping package {" + ActivityEntityEnum.SHIPPING_PACKAGE + ":" + shippingPackage.getCode() + "} with Order Items {"
                                    + ActivityEntityEnum.SALE_ORDER_ITEM + ":" + orderItems.substring(0, orderItems.lastIndexOf(",")) + "}" + (forceDispatch ? " force" : "")
                                    + " dispatched." }),
                    ActivityTypeEnum.PROCESSING.name());
        }
        LOG.info("Shipping package [{}] successfully dispatched.", shippingPackage.getCode());
    }

    /**
     * Creates a new inventory ledger and passes it to {@link IInventoryLedgerService#addInventoryLedgerEntry} for
     * persistence in db.
     */
    private void addInventoryLedgerEntry(SaleOrderItem saleOrderItem) {
        InventoryLedger inventoryLedger = new InventoryLedger();
        inventoryLedger.setTransactionType(InventoryLedger.TransactionType.SALE_ORDER_DISPATCH);
        inventoryLedger.setAdditionalInfo(saleOrderItem.getShippingPackage().getShippingManifest() != null ? saleOrderItem.getShippingPackage().getShippingManifest().getCode(): "FORCE_DISPATCHED");
        inventoryLedger.setTransactionIdentifier(saleOrderItem.getCode());
        inventoryLedger.setSkuCode(saleOrderItem.getItemType().getSkuCode());
        inventoryLedgerService.addInventoryLedgerEntry(inventoryLedger, 1, ChangeType.DECREASE);
    }

    @Transactional
    @Override
    public ShipmentTracking addShipmentTracking(ShipmentTracking shipmentTracking) {
        return shippingDao.addShipmentTracking(shipmentTracking);
    }

    private SandboxParams getSandboxParameters(Channel channel, ShippingPackage shippingPackage) {
        SandboxParams sandboxParams = new SandboxParams();
        try {
            sandboxParams.setChannelConnectorParameters(channel.getChannelConnectorParameters());
            sandboxParams.setShippingPackage(shippingPackage);
            sandboxParams.setChannel(channel);
        } catch (Exception e) {
            LOG.error("Error creating sandbox params", e);
        }
        return sandboxParams;
    }

    private void notifyCustomerViaEmail(ShippingPackage shippingPackage) {
        EmailTemplateVO emailTemplate = ConfigurationManager.getInstance().getConfiguration(EmailConfiguration.class).getTemplateByType(
                EmailTemplateType.PACKAGE_DISPATCHED.name());
        String defaultEmail = emailTemplate.getTo() != null ? String.valueOf(emailTemplate.getTo().evaluate(new HashMap<String, Object>())) : null;
        List<String> recipients = new ArrayList<>(2);
        if (StringUtils.isNotBlank(shippingPackage.getSaleOrder().getNotificationEmail())) {
            if (ValidatorUtils.isEmailValid(shippingPackage.getSaleOrder().getNotificationEmail())) {
                recipients.add(shippingPackage.getSaleOrder().getNotificationEmail());
            } else {
                LOG.warn("Shipping Package: {} Ignoring invalid recipient {}", shippingPackage.getCode(), shippingPackage.getSaleOrder().getNotificationEmail());
            }
        } else {
            LOG.warn("Shipping Package: {}, notification email not present", shippingPackage.getCode());
        }
        if (StringUtils.isNotBlank(defaultEmail) && ValidatorUtils.isEmailValid(defaultEmail)) {
            recipients.add(defaultEmail);
        }
        if (emailTemplate.isEnabled() && !recipients.isEmpty()) {
            EmailMessage message = new EmailMessage(recipients, EmailTemplateType.PACKAGE_DISPATCHED.name());
            message.addTemplateParam("shippingPackage", shippingPackage);
            Invoice invoice = invoiceService.getInvoiceDetailedById(shippingPackage.getInvoice().getId());
            message.addTemplateParam("invoice", invoice);

            //add invoice as attachment
            boolean attachInvoice = ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).isAttachInvoicePdfToEmailOnPackageDispatch();
            if (attachInvoice) {
                String directory = EnvironmentProperties.getTemporaryFilePath();
                Expression invoicePdfNameExpression = ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).getInvoicePdfNameExpression();
                Map<String, Object> contextParams = new HashMap<>();
                contextParams.put("invoice", invoice);
                contextParams.put("shippingPackage", shippingPackage);
                String attachmentName = invoicePdfNameExpression.evaluate(contextParams, String.class);
                attachmentName = StringUtils.underscorify(attachmentName);

                File file = new File((directory.endsWith("/") ? directory : directory + "/") + attachmentName + ".pdf");
                String invoiceHtml = iShippingInvoiceService.getInvoiceHtmlByShippingPackageCode(shippingPackage.getCode());
                PrintTemplateVO invoiceTemplate = CacheManager.getInstance().getCache(PrintTemplateCache.class).getPrintTemplateByType(PrintTemplateVO.Type.INVOICE);
                SamplePrintTemplateVO samplePrintTemplate = CacheManager.getInstance().getCache(SamplePrintTemplateCache.class).getSamplePrintTemplate(
                        invoiceTemplate.getSamplePrintTemplateCode());
                PrintOptions printOptions = new PrintOptions(samplePrintTemplate);
                printOptions.setPrintDialog(PrintOptions.PrintDialog.DEFAULT.name());
                FileOutputStream fos = null;
                try {
                    fos = new FileOutputStream(file);
                    pdfDocumentService.writeHtmlToPdf(fos, invoiceHtml, printOptions);
                    message.getAttachments().add(file);
                } catch (Exception e) {
                    LOG.error("Error occured while sending package dispatched mail:", e);
                } finally {
                    FileUtils.safelyCloseOutputStream(fos);
                }
            }
            emailService.send(message);
        }
    }

    private void notifyCustomerViaSms(ShippingPackage shippingPackage) {
        SmsTemplateVO smsTemplate = ConfigurationManager.getInstance().getConfiguration(SmsConfiguration.class).getTemplateByName(SmsTemplate.Name.PACKAGE_DISPATCHED.name());
        String notificationMobile = shippingPackage.getSaleOrder().getNotificationMobile();
        if (smsTemplate != null && smsTemplate.isEnabled() && StringUtils.isNotBlank(notificationMobile)) {
            try {
                SmsMessage message = new SmsMessage(notificationMobile, SmsTemplate.Name.PACKAGE_DISPATCHED.name());
                message.addTemplateParam("shippingPackage", shippingPackage);
                Invoice invoice = invoiceService.getInvoiceDetailedById(shippingPackage.getInvoice().getId());
                message.addTemplateParam("invoice", invoice);
                smsService.send(message);
            } catch (Exception e) {
                LOG.error("Error occured while sending package dispatched sms:", e);
            }
        }
    }

    private ManifestItemDTO prepareManifestItemDTO(ShippingPackage shippingPackage) {
        return prepareManifestItemDTOByPackage(shippingPackage);
    }

    private ManifestItemDetailDTO prepareManifestItemDetailDTO(ShippingManifestItem shippingManifestItem) {
        return prepareManifestItemDetailDTOByPackage(shippingManifestItem.getShippingPackage());
    }

    private ManifestItemDetailDTO prepareManifestItemDetailDTOByPackage(ShippingPackage shippingPackage) {
        long start = System.currentTimeMillis();
        ManifestItemDetailDTO manifestItemDTO = new ManifestItemDetailDTO();
        manifestItemDTO.setShippingPackageId(shippingPackage.getId());
        manifestItemDTO.setShippingPackageCode(shippingPackage.getCode());
        manifestItemDTO.setShippingPackageCustomFieldValues(CustomFieldUtils.getCustomFieldValues(shippingPackage));

        SaleOrder saleOrder = HibernateUtils.initializeAndUnproxy(shippingPackage.getSaleOrder());
        manifestItemDTO.setSaleOrderCustomFieldValues(CustomFieldUtils.getCustomFieldValues(saleOrder));

        manifestItemDTO.setTrackingNumber(shippingPackage.getTrackingNumber());
        ShippingMethod shippingMethod = shippingService.getShippingMethodById(shippingPackage.getShippingMethod().getId());
        manifestItemDTO.setShippingMethod(shippingMethod.getName());
        manifestItemDTO.setQuantity(shippingPackage.getSaleOrderItems().size());
        manifestItemDTO.setNoOfBoxes(shippingPackage.getNoOfBoxes());
        manifestItemDTO.setShippingAddress(new AddressDTO(shippingPackage.getShippingAddress()));
        manifestItemDTO.setTotalAmount(shippingPackage.getTotalPrice());
        manifestItemDTO.setSellingAmount(shippingPackage.getSellingPrice());
        manifestItemDTO.setShippingCharges(shippingPackage.getShippingCharges());
        manifestItemDTO.setCollectableAmount(shippingPackage.getCollectableAmount());

        BigDecimal taxAmount = BigDecimal.ZERO;
        for (InvoiceItem invoiceItem : shippingPackage.getInvoice().getInvoiceItems()) {
            taxAmount = taxAmount.add(invoiceItem.getTotalTaxAmount());
        }
        manifestItemDTO.setTaxAmount(taxAmount);
        if (ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).isSellingPricesTaxExclusive()) {
            manifestItemDTO.setSellingAmount(manifestItemDTO.getSellingAmount().add(taxAmount));
            manifestItemDTO.setTotalAmount(manifestItemDTO.getTotalAmount().add(taxAmount));
        }
        ShippingPackageType shippingPackageType = HibernateUtils.initializeAndUnproxy(shippingPackage.getShippingPackageType());
        ShippingPackageTypeDTO sPackageType = new ShippingPackageTypeDTO(shippingPackageType);
        sPackageType.setBoxLength(shippingPackage.getBoxLength());
        sPackageType.setBoxWidth(shippingPackage.getBoxWidth());
        sPackageType.setBoxHeight(shippingPackage.getBoxHeight());
        manifestItemDTO.setShippingPackageType(sPackageType);
        manifestItemDTO.setWeight(shippingPackage.getActualWeight());
        manifestItemDTO.setCashOnDelivery(shippingMethod.isCashOnDelivery());
        manifestItemDTO.setDisplayOrderCode(shippingPackage.getSaleOrder().getDisplayOrderCode());
        manifestItemDTO.setOrderCode(shippingPackage.getSaleOrder().getCode());
        manifestItemDTO.setInvoiceCode(shippingPackage.getInvoice().getCode());
        manifestItemDTO.setInvoiceDisplayCode(shippingPackage.getInvoice().getDisplayCode());
        StringBuilder builder = new StringBuilder();
        StringBuilder skuBuilder = new StringBuilder();
        if (!shippingPackage.getSaleOrderItems().isEmpty()) {
            for (SaleOrderItem saleOrderItem : shippingPackage.getSaleOrderItems()) {
                if (shippingPackage.getSaleOrder().isProductManagementSwitchedOff()) {
                    builder.append(saleOrderItem.getChannelProductName()).append(" (").append(saleOrderItem.getSellerSkuCode()).append("), ");
                    skuBuilder.append(saleOrderItem.getSellerSkuCode()).append(",");
                } else {
                    ItemType itemType = catalogService.getNonBundledItemTypeById(saleOrderItem.getItemType().getId());
                    builder.append(itemType.getName()).append(" (").append(itemType.getSkuCode()).append("), ");
                    skuBuilder.append(itemType.getSkuCode()).append(",");
                }
            }
            manifestItemDTO.setProductSKUs(skuBuilder.deleteCharAt(skuBuilder.length() - 1).toString());
            manifestItemDTO.setProductDescription(builder.deleteCharAt(builder.length() - 2).toString());
        }
        manifestItemDTO.setStateRegulatoryFormDTOs(
                regulatoryService.getApplicableRegulatoryForms(new GetApplicableRegulatoryFormsRequest(shippingPackage.getCode())).getStateRegulatoryForms());
        LOG.info("prepare manifest item ran in " + (System.currentTimeMillis() - start) + " ms");
        return manifestItemDTO;
    }

    @Transactional
    private ManifestItemDTO prepareManifestItemDTOByPackage(ShippingPackage shippingPackage) {
        shippingPackage = shippingService.getShippingPackageByCode(shippingPackage.getCode());
        ManifestItemDTO manifestItemDTO = new ManifestItemDTO();
        manifestItemDTO.setShippingPackageCode(shippingPackage.getCode());
        manifestItemDTO.setShippingPackageStatusCode(shippingPackage.getStatusCode());
        manifestItemDTO.setTrackingNumber(shippingPackage.getTrackingNumber());
        manifestItemDTO.setShippingMethod(shippingPackage.getShippingMethod().getName());
        manifestItemDTO.setQuantity(shippingPackage.getSaleOrderItems().size());
        manifestItemDTO.setNoOfBoxes(shippingPackage.getNoOfBoxes());
        manifestItemDTO.setShippingAddress(new AddressDTO(shippingPackage.getShippingAddress()));
        manifestItemDTO.setTotalAmount(shippingPackage.getTotalPrice());
        manifestItemDTO.setShippingCharges(shippingPackage.getShippingCharges());
        manifestItemDTO.setCollectableAmount(shippingPackage.getCollectableAmount());

        if (ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).isSellingPricesTaxExclusive()) {
            BigDecimal taxAmount = BigDecimal.ZERO;
            for (InvoiceItem invoiceItem : shippingPackage.getInvoice().getInvoiceItems()) {
                taxAmount = taxAmount.add(invoiceItem.getTotalTaxAmount());
            }
            manifestItemDTO.setTotalAmount(manifestItemDTO.getTotalAmount().add(taxAmount));
        }
        ShippingPackageType shippingPackageType = shippingPackage.getShippingPackageType();
        ShippingPackageTypeDTO sPackageType = new ShippingPackageTypeDTO(shippingPackageType);
        sPackageType.setBoxLength(shippingPackage.getBoxLength());
        sPackageType.setBoxWidth(shippingPackage.getBoxWidth());
        sPackageType.setBoxHeight(shippingPackage.getBoxHeight());
        manifestItemDTO.setShippingPackageType(sPackageType);
        manifestItemDTO.setWeight(shippingPackage.getActualWeight());
        manifestItemDTO.setCashOnDelivery(shippingPackage.getShippingMethod().isCashOnDelivery());
        manifestItemDTO.setDisplayOrderCode(shippingPackage.getSaleOrder().getDisplayOrderCode());
        if (shippingPackage.getInvoice() != null) {
            manifestItemDTO.setInvoiceCode(shippingPackage.getInvoice().getCode());
        }
        manifestItemDTO.setShippingProviderCode(shippingPackage.getShippingProviderCode());
        manifestItemDTO.setShippingProviderName(shippingPackage.getShippingProviderName());
        Map<String, List<SaleOrderItem>> lineItemToSaleOrderItems = prepareLiteItemsDetails(shippingPackage);
        List<ManifestLineItem> manifestLineItems = new ArrayList<>(lineItemToSaleOrderItems.size());
        for (Entry<String, List<SaleOrderItem>> lineItem : lineItemToSaleOrderItems.entrySet()) {
            ManifestLineItem manifestLineItem = new ManifestLineItem();
            SaleOrderItem soi = lineItem.getValue().iterator().next();
            manifestLineItem.setLineItemIdentifier(lineItem.getKey());
            manifestLineItem.setItemName(shippingPackage.getSaleOrder().isProductManagementSwitchedOff() ? soi.getChannelProductName() : soi.getItemType().getName());
            manifestLineItem.setSellerSkuCode(soi.getChannelSkuCode());
            manifestLineItem.setQuantity(lineItem.getValue().size());
            manifestLineItems.add(manifestLineItem);
        }
        manifestItemDTO.setManifestLineItems(manifestLineItems);
        return manifestItemDTO;
    }

    private Map<String, List<SaleOrderItem>> prepareLiteItemsDetails(ShippingPackage shippingPackage) {
        Map<String, List<SaleOrderItem>> lineItemToSaleOrderItems = new HashMap<>();
        for (SaleOrderItem soi : shippingPackage.getSaleOrderItems()) {
            String lineItemIdentifier = shippingPackage.getSaleOrder().isProductManagementSwitchedOff() ? soi.getChannelProductId() : soi.getItemType().getSkuCode();
            List<SaleOrderItem> saleOrderItems = lineItemToSaleOrderItems.get(lineItemIdentifier);
            if (saleOrderItems == null) {
                saleOrderItems = new ArrayList<>();
                lineItemToSaleOrderItems.put(lineItemIdentifier, saleOrderItems);
            }
            saleOrderItems.add(soi);
        }
        return lineItemToSaleOrderItems;
    }

    private ManifestDetailDTO prepareManifestDetailDTO(ShippingManifest shippingManifest) {
        ManifestDetailDTO manifestDTO = new ManifestDetailDTO(shippingManifest);
        manifestDTO.setCustomFieldValues(CustomFieldUtils.getCustomFieldValuesDTO(shippingManifest));
        if (shippingManifest.getShippingMethod() != null) {
            ShippingMethod shippingMethodById = shippingService.getShippingMethodById(shippingManifest.getShippingMethod().getId());
            manifestDTO.setCashOnDelivery(shippingMethodById.isCashOnDelivery());
            manifestDTO.setShippingMethod(shippingMethodById.getName());
        }
        if (shippingManifest.getChannel() != null) {
            Channel channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelByCode(shippingManifest.getChannel().getCode());
            Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(channel.getSourceCode());
            manifestDTO.setChannel(channel.getCode());
            manifestDTO.setFetchCurrentChannelManifestEnabled(source.isFetchCurrentChannelManifestEnabled());
        }

        long start = System.currentTimeMillis();
        //manifestDTO.setShippingManifestStatus(shippingMao.findShippingManifestStatusByCode(shippingManifest.getCode()));
        LOG.info("mongo status fetch ran in " + (System.currentTimeMillis() - start) + " ms");
        return manifestDTO;
    }

    private ManifestDTO prepareManifestDTO(ShippingManifest shippingManifest) {
        ManifestDTO manifestDTO = new ManifestDTO(shippingManifest);
        manifestDTO.setCustomFieldValues(CustomFieldUtils.getCustomFieldValuesDTO(shippingManifest));
        if (shippingManifest.getShippingMethod() != null) {
            ShippingMethod shippingMethodById = shippingService.getShippingMethodById(shippingManifest.getShippingMethod().getId());
            manifestDTO.setCashOnDelivery(shippingMethodById.isCashOnDelivery());
            manifestDTO.setShippingMethod(shippingMethodById.getName());
        }
        if (shippingManifest.getChannel() != null) {
            Channel channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelByCode(shippingManifest.getChannel().getCode());
            Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(channel.getSourceCode());
            manifestDTO.setChannel(channel.getCode());
            manifestDTO.setFetchCurrentChannelManifestEnabled(source.isFetchCurrentChannelManifestEnabled());
        }

        long start = System.currentTimeMillis();
        //manifestDTO.setShippingManifestStatus(shippingMao.findShippingManifestStatusByCode(shippingManifest.getCode()));
        LOG.info("mongo status fetch ran in " + (System.currentTimeMillis() - start) + " ms");
        return manifestDTO;
    }

    private ManifestDetailDTO getDetailedManifestDTO(ShippingManifest shippingManifest) {
        ManifestDetailDTO manifestDTO = prepareManifestDetailDTO(shippingManifest);
        for (ShippingManifestItem shippingManifestItem : shippingManifest.getShippingManifestItems()) {
            manifestDTO.getManifestItems().add(prepareManifestItemDetailDTO(shippingManifestItem));
        }
        return manifestDTO;
    }

    private ManifestDTO getManifestDTO(ShippingManifest shippingManifest) {
        ManifestDTO manifestDTO = prepareManifestDTO(shippingManifest);
        for (ShippingManifestItem shippingManifestItem : shippingManifest.getShippingManifestItems()) {
            manifestDTO.getManifestItems().add(prepareManifestItemDTO(shippingManifestItem.getShippingPackage()));
        }
        return manifestDTO;
    }

    @Override
    @Transactional(readOnly = true)
    public String getManifestHtml(String manifestCode, Template template) {
        ShippingManifest shippingManifest = shippingDao.getShippingManifestDetailedByCode(manifestCode);
        Map<String, Object> params = new HashMap<>();
        if (shippingManifest != null) {
            params.put("manifest", shippingManifest);
            ManifestDetailDTO manifestDTO = getDetailedManifestDTO(shippingManifest);
            if (manifestDTO != null) {
                params.put("shippingManifest", manifestDTO);
            }
            return template.evaluate(params);
        } else {
            return null;
        }
    }

    @Override
    public CheckIfShippingManifestClosingResponse checkIfShippingManifestClosing(CheckIfShippingManifestClosingRequest request) {
        CheckIfShippingManifestClosingResponse response = new CheckIfShippingManifestClosingResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            String lockKey = request.getShippingManifestCode();
            ReadWriteLock readWriteLock = lockingService.getReadWriteLock(Namespace.MANIFEST, lockKey, Level.FACILITY);
            response.setClosing(true);
            if (readWriteLock.writeLock().tryLock()) {
                try {
                    response.setClosing(false);
                } finally {
                    readWriteLock.writeLock().unlock();
                }
            }
            response.setSuccessful(true);
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    @Override
    @LogActivity
    public CloseShippingManifestResponse closeShippingManifest(CloseShippingManifestRequest request) {
        String lockKey = request.getShippingManifestCode();
        ReadWriteLock readWriteLock = lockingService.getReadWriteLock(Namespace.MANIFEST, lockKey, Level.FACILITY);
        if (readWriteLock.writeLock().tryLock()) {
            try {
                LOG.info("[CloseShippingManifest] Acquired lock on namespace: {} and key: {}", Namespace.MANIFEST.name(), lockKey);
                return closeShippingManifestInternal(request);
            } finally {
                readWriteLock.writeLock().unlock();
                LOG.info("[CloseShippingManifest] Released lock on namespace: {} and key: {}", Namespace.MANIFEST.name(), lockKey);
            }
        } else {
            CloseShippingManifestResponse response = new CloseShippingManifestResponse();
            response.addError(new WsError("Manifest is in locked state"));
            return response;
        }
    }

    @LogActivity
    private CloseShippingManifestResponse closeShippingManifestInternal(CloseShippingManifestRequest request) {
        CloseShippingManifestResponse response = new CloseShippingManifestResponse();
        final ValidationContext context = request.validate();
        ShippingManifest shippingManifest = null;
        ShippingManifestStatusDTO status = new ShippingManifestStatusDTO(request.getShippingManifestCode());
        if (!context.hasErrors()) {
            shippingManifest = getShippingManifestDetailedByCode(request.getShippingManifestCode());
            if (shippingManifest == null) {
                context.addError(WsResponseCode.INVALID_SHIPPING_MANIFEST_CODE, "Shipping Manifest CODE is invalid :Code :" + request.getShippingManifestCode());
            } else if (!ShippingManifest.StatusCode.CREATED.name().equals(shippingManifest.getStatusCode())) {
                context.addError(WsResponseCode.INVALID_SHIPPING_MANIFEST_STATE,
                        "Shipping Manifest is not in CREATED state :" + "Shipping Manifest Status Code :" + shippingManifest.getStatusCode());
            } else if (shippingManifest.getShippingManifestItems().isEmpty()) {
                context.addError(WsResponseCode.INVALID_SHIPPING_MANIFEST_STATE, "No items added to shipping manifest");
            }
        }

        Source source = null;
        Integer totalShippingPackageCount = null;
        if (!context.hasErrors()) {
            totalShippingPackageCount = shippingManifest.getShippingManifestItems().size();
            source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(shippingManifest.getChannel().getSourceCode());
            if (source.isManifestAllReadyToShipOrdersTogether() && !source.isAllowCombinedManifest()) {
                List<ShippingPackage> readyToShipShippingPackages = getEligibleShippingPackagesForShippingManifest(shippingManifest);
                for (ShippingManifestItem manifestItem : shippingManifest.getShippingManifestItems()) {
                    readyToShipShippingPackages.remove(manifestItem.getShippingPackage());
                }
                if (!readyToShipShippingPackages.isEmpty()) {
                    List<String> codes = new ArrayList<>(readyToShipShippingPackages.size());
                    for (ShippingPackage shippingPackage : readyToShipShippingPackages) {
                        codes.add(shippingPackage.getCode());
                    }
                    context.addError(WsResponseCode.INVALID_SHIPPING_MANIFEST_STATE, "Packages " + codes.toString() + " must be added to manifest");
                }
            }
        }

        if (!context.hasErrors()) {
            Integer totalMilestoneCount = 0;
            Integer milestoneCount = shippingManifest.getShippingManifestItems().size();
            if (StringUtils.isNotBlank(
                    CacheManager.getInstance().getCache(ChannelCache.class).getScriptName(shippingManifest.getChannel().getCode(), Source.POST_MANIFEST_SCRIPT_NAME))) {
                totalMilestoneCount += milestoneCount;
                LOG.info("post manifest script not blank : added milestone counts:" + totalMilestoneCount);
            }
            if (!ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).isDispatchShippingPackageOnAddingToManifest()) {
                for (ShippingManifestItem shippingManifestItem : shippingManifest.getShippingManifestItems()) {
                    if (!shippingManifestItem.getShippingPackage().isDispatchedOnChannel()) {
                        totalMilestoneCount = totalMilestoneCount + 1;
                    }
                    LOG.info("adding milestone for:" + shippingManifestItem.getId());
                }
                LOG.info("isDispatchShippingPackageOnAddingToManifest returned false : added milestones : final number:" + totalMilestoneCount);
            }
            status.setMileStoneCount(totalMilestoneCount + 1);
            response.setShippingManifestStatus(status);
        }

        if (!context.hasErrors()) {
            executePreManifestScript(shippingManifest.getId(), context, status);
        }

        Map<String, FailedShippingPackageDTO> failedShippingPackages = new HashMap<>(shippingManifest == null ? 0 : shippingManifest.getShippingManifestItems().size());
        if (!context.hasErrors() && !ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).isDispatchShippingPackageOnAddingToManifest()) {
            LOG.info("Dispatching shipping packages");
            shippingManifest = dispatchShippingPackages(shippingManifest, response, failedShippingPackages);
            LOG.info("Done dispatching shipping packages");
        }

        if (shippingManifest.getShippingManifestItems().isEmpty()) {
            context.addError(WsResponseCode.THIRD_PARTY_API_RETURNED_ERROR, "Dispatch failed for all shipping packages : Error from panel");
        }

        if (!context.hasErrors()) {
            try {
                LOG.info("Executing post manifest");
                shippingManifest = executePostManifestScript(shippingManifest.getId(), response, failedShippingPackages);
                LOG.info("Done executing post manifest");
                if (failedShippingPackages.size() == totalShippingPackageCount) {
                    context.addError(WsResponseCode.THIRD_PARTY_API_RETURNED_ERROR, "Manifest creation failed for all shipping packages : Error from panel");
                }
            } catch (Exception e) {
                LOG.error("Unable to generate manifest:" + shippingManifest.getCode(), e);
                context.addError(WsResponseCode.THIRD_PARTY_API_RETURNED_ERROR, e.getMessage());
            }
        }

        if (!context.hasErrors() && source.isDispatchPackageOnManifestClose()) {
            LOG.info("Marking packages as dispatched");
            for (ShippingManifestItem shippingManifestItem : shippingManifest.getShippingManifestItems()) {
                if (!failedShippingPackages.containsKey(shippingManifestItem.getShippingPackage().getCode())) {
                    markShippingPackageDispatched(shippingManifestItem.getShippingPackage(), false);
                }
            }
            LOG.info("Done marking packages as dispatched");
        }
        if (!context.hasErrors() && !failedShippingPackages.isEmpty()) {
            LOG.info("Handling failed shipments");
            List<FailedShippingPackageDTO> failedPackages = new ArrayList<>(failedShippingPackages.size());
            failedPackages.addAll(failedShippingPackages.values());
            status.setFailedShippingPackages(failedPackages);
            shippingManifest = handleFailedShipments(response, shippingManifest, failedShippingPackages);
            LOG.info("Done handling failed shipments");
        }

        if (context.hasErrors()) {
            status.setSuccessful(false);
            status.setCurrentStatus(context.getErrors().get(0).getDescription());
            response.setErrors(context.getErrors());
        } else {
            shippingManifest.setStatusCode(ShippingManifest.StatusCode.CLOSED.name());
            shippingManifest.setClosed(DateUtils.getCurrentTime());
            updateShippingManifest(shippingManifest);
            markAllSaleOrderInManifestDirty(shippingManifest.getCode());
            if (ActivityContext.current().isEnable()) {
                logCloseManifestActivity(shippingManifest);
            }
            sendShippingManifestMail(shippingManifest);
            response.setSuccessful(true);
            response.setShippingManifestCode(shippingManifest.getCode());
            status.setSuccessful(true);
            status.setMileStone("All packages successfully dispatched");
        }

        status.setCurrentMileStone(status.getMileStoneCount());
        status.setCompleted(true);
        status.setUpdated(DateUtils.getCurrentTime());
        messageBroadcaster.broadcast(new Message("/shippingManifest-" + shippingManifest.getCode(), Message.Type.FACILITY, response.getShippingManifestStatus()));
        LOG.info("Completed manifest close");
        return response;
    }

    private void markAllSaleOrderInManifestDirty(String shippingManifestCode) {
        ShippingManifest sm = getShippingManifestDetailedByCode(shippingManifestCode);
        for (ShippingManifestItem smi : sm.getShippingManifestItems()) {
            doMarkSaleOrderDirty(smi);
        }
    }

    @Locks({ @Lock(ns = Namespace.SALE_ORDER, key = "#{#args[0].shippingPackage.saleOrder.code}") })
    @Transactional
    private void doMarkSaleOrderDirty(ShippingManifestItem smi) {
        SaleOrder so = saleOrderDao.getSaleOrderForUpdate(smi.getShippingPackage().getSaleOrder().getCode());
        saleOrderDao.markSaleOrderDirty(so);
    }

    @Transactional
    private void logCloseManifestActivity(ShippingManifest shippingManifest) {
        shippingManifest = getShippingManifestByCode(shippingManifest.getCode());
        for (ShippingManifestItem shippingManifestItem : shippingManifest.getShippingManifestItems()) {
            ActivityUtils.appendActivity(shippingManifestItem.getShippingPackage().getCode(), ActivityEntityEnum.SHIPPING_PACKAGE.getName(),
                    shippingManifestItem.getShippingPackage().getSaleOrder().getCode(),
                    Arrays.asList(new String[] { "Manifest {" + ActivityEntityEnum.MANIFEST + ":" + shippingManifest.getCode() + "} CLOSED." }), ActivityTypeEnum.COMPLETE.name());
        }
    }

    private ShippingManifest dispatchShippingPackages(ShippingManifest shippingManifest, CloseShippingManifestResponse response,
            Map<String, FailedShippingPackageDTO> failedShippingPackages) {
        Map<ShippingPackage, Future<DispatchShippingPackageResponse>> taskFutures = new LinkedHashMap<>(shippingManifest.getShippingManifestItems().size());
        ContextAwareExecutor executor = executorFactory.getExecutor(SERVICE_NAME);
        LOG.info("[ContextAwareExecutor] ActiveCount: {}, QueueSize: {}", executor.getActiveCount(), executor.getQueue().size());
        for (final ShippingManifestItem shippingManifestItem : shippingManifest.getShippingManifestItems()) {
            if (!shippingManifestItem.getShippingPackage().isDispatchedOnChannel()) {
                taskFutures.put(shippingManifestItem.getShippingPackage(), executor.submit(new Callable<DispatchShippingPackageResponse>() {
                    @Override
                    public DispatchShippingPackageResponse call() {
                        DispatchShippingPackageResponse response = new DispatchShippingPackageResponse();
                        try {
                            response = dispatchShippingPackageInternal(shippingManifestItem.getShippingPackage());
                        } catch (Throwable e) {
                            LOG.error("Unexpected error while dispatching package:" + shippingManifestItem.getShippingPackage().getCode(), e);
                            response.addError(new WsError(WsResponseCode.UNKNOWN_ERROR.code(), e.getMessage()));
                        }
                        return response;
                    }
                }));
            }
        }
        for (Entry<ShippingPackage, Future<DispatchShippingPackageResponse>> entry : taskFutures.entrySet()) {
            DispatchShippingPackageResponse dispatchShippingPackageResponse = null;
            String failureReason = null;
            try {
                dispatchShippingPackageResponse = entry.getValue().get();
            } catch (Exception e) {
                LOG.error("Errors getting future response", e);
                failureReason = e.getMessage();
            }
            if (dispatchShippingPackageResponse == null || !dispatchShippingPackageResponse.isSuccessful()) {
                String shippingPackageCode = entry.getKey().getCode();
                ShippingPackage shippingPackage = shippingService.getShippingPackageByCode(shippingPackageCode);
                removeManifestItem(new RemoveManifestItemRequest(shippingManifest.getCode(), shippingPackageCode));
                if (dispatchShippingPackageResponse == null) {
                    failedShippingPackages.put(entry.getKey().getCode(),
                            new FailedShippingPackageDTO(shippingPackageCode, shippingPackage.getSaleOrder().getCode(), failureReason, false));
                } else {
                    failedShippingPackages.put(entry.getKey().getCode(), new FailedShippingPackageDTO(shippingPackageCode, shippingPackage.getSaleOrder().getCode(),
                            dispatchShippingPackageResponse.getErrors().get(0).getDescription(), dispatchShippingPackageResponse.isCancelledOnChannel()));
                }
            }
            response.getShippingManifestStatus().setMileStone("Dispatching package " + entry.getKey().getCode() + " on channel");
            messageBroadcaster.broadcast(new Message("/shippingManifest-" + shippingManifest.getCode(), Message.Type.FACILITY, response.getShippingManifestStatus()));
        }
        return getShippingManifestByCode(shippingManifest.getCode());
    }

    private ShippingManifest handleFailedShipments(CloseShippingManifestResponse response, ShippingManifest shippingManifest,
            Map<String, FailedShippingPackageDTO> failedShippingPackages) {
        List<String> failedShippingPackageCodes = new ArrayList<>(failedShippingPackages.size());
        for (FailedShippingPackageDTO failedShippingPackage : failedShippingPackages.values()) {
            ShippingPackage shippingPackage = shippingService.getShippingPackageByCode(failedShippingPackage.getCode());
            failedShippingPackageCodes.add(shippingPackage.getCode());
            removeManifestItem(new RemoveManifestItemRequest(shippingManifest.getCode(), shippingPackage.getCode()));
            if (failedShippingPackage.isCancelled()) {
                CancelSaleOrderRequest cancelSaleOrderRequest = new CancelSaleOrderRequest();
                cancelSaleOrderRequest.setSaleOrderCode(shippingPackage.getSaleOrder().getCode());
                List<String> saleOrderItemCodes = new ArrayList<>(shippingPackage.getSaleOrderItems().size());
                for (SaleOrderItem saleOrderItem : shippingPackage.getSaleOrderItems()) {
                    saleOrderItemCodes.add(saleOrderItem.getCode());
                }
                cancelSaleOrderRequest.setSaleOrderItemCodes(saleOrderItemCodes);
                cancelSaleOrderRequest.setCancellationReason("Shipping Manifest: " + shippingManifest.getCode() + ", cancelled on channel");
                saleOrderService.cancelSaleOrder(cancelSaleOrderRequest);
            }
        }
        AddShipmentsToBatchRequest addShipmentsToBatchRequest = new AddShipmentsToBatchRequest();
        addShipmentsToBatchRequest.setShippingPackageCodes(failedShippingPackageCodes);
        AddShipmentsToBatchResponse addShipmentsToBatchResponse = shippingService.addShipmentsToBatch(addShipmentsToBatchRequest);
        response.getShippingManifestStatus().setFailedShipmentsBatchCode(addShipmentsToBatchResponse.getShipmentBatchCode());
        return getShippingManifestByCode(shippingManifest.getCode());
    }

    @Transactional
    private void executePreManifestScript(Integer shippingManifestId, ValidationContext context, ShippingManifestStatusDTO status) {
        ShippingManifest shippingManifest = shippingDao.getShippingManifestById(shippingManifestId);
        Channel channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelById(shippingManifest.getChannel().getId());
        if (!context.hasErrors() && StringUtils.isNotBlank(
                CacheManager.getInstance().getCache(ChannelCache.class).getScriptName(shippingManifest.getChannel().getCode(), Source.PRE_MANIFEST_SCRIPT_NAME))) {
            ScraperScript scraperScript = CacheManager.getInstance().getCache(ChannelCache.class).getScriptByName(channel.getCode(), Source.PRE_MANIFEST_SCRIPT_NAME);
            if (scraperScript != null) {
                LOG.info("Running pre manifest checks");
                status.setCurrentStatus("Running pre manifest checks");
                messageBroadcaster.broadcast(new Message("/shippingManifest-" + shippingManifest.getCode(), Message.Type.FACILITY, status));
                try {
                    ScriptExecutionContext scriptContext = ScriptExecutionContext.current();
                    scriptContext.addVariable("channel", channel);
                    scriptContext.addVariable("applicationContext", applicationContext);
                    scriptContext.addVariable("shippingManifest", shippingManifest);
                    for (ChannelConnector channelConnector : channel.getChannelConnectors()) {
                        for (ChannelConnectorParameter channelConnectorParameter : channelConnector.getChannelConnectorParameters()) {
                            scriptContext.addVariable(channelConnectorParameter.getName(), channelConnectorParameter.getValue());
                        }
                    }
                    for (ChannelConfigurationParameter channelConfigurationParameter : channel.getChannelConfigurationParameters()) {
                        scriptContext.addVariable(channelConfigurationParameter.getSourceConfigurationParameterName(), channelConfigurationParameter.getValue());
                    }
                    scriptContext.setScriptProvider(new IScriptProvider() {

                        @Override
                        public ScraperScript getScript(String scriptName) {
                            return CacheManager.getInstance().getCache(ScriptVersionedCache.class).getScriptByName(scriptName);
                        }
                    });
                    scriptContext.setTraceLoggingEnabled(UserContext.current().isTraceLoggingEnabled());
                    scraperScript.execute();
                    LOG.info("Done running pre manifest checks");
                } catch (Exception e) {
                    LOG.error("unable to validate manifest:" + shippingManifest.getCode(), e);
                    context.addError(WsResponseCode.INVALID_STATE, e.getMessage());
                } finally {
                    ScriptExecutionContext.destroy();
                }
            }
        }
    }

    @Transactional
    private ShippingManifest executePostManifestScript(Integer shippingManifestId, CloseShippingManifestResponse response,
            Map<String, FailedShippingPackageDTO> failedShippingPackages) {
        ShippingManifest shippingManifest = shippingDao.getShippingManifestById(shippingManifestId);
        Channel channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelById(shippingManifest.getChannel().getId());
        Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(channel.getSourceCode());
        if (StringUtils.isNotBlank(
                CacheManager.getInstance().getCache(ChannelCache.class).getScriptName(shippingManifest.getChannel().getCode(), Source.POST_MANIFEST_SCRIPT_NAME))) {
            ScraperScript scraperScript = CacheManager.getInstance().getCache(ChannelCache.class).getScriptByName(channel.getCode(), Source.POST_MANIFEST_SCRIPT_NAME);
            if (scraperScript != null) {
                try {
                    ScriptExecutionContext scriptContext = ScriptExecutionContext.current();
                    scriptContext.addVariable("channel", channel);
                    scriptContext.addVariable("source", source);
                    scriptContext.addVariable("shippingManifest", shippingManifest);
                    scriptContext.addVariable("failedShippingPackages", failedShippingPackages);
                    scriptContext.addVariable("status", response.getShippingManifestStatus());
                    scriptContext.addVariable("applicationContext", applicationContext);
                    scriptContext.addVariable("messageBroadcaster", messageBroadcaster);
                    for (ChannelConnector channelConnector : channel.getChannelConnectors()) {
                        for (ChannelConnectorParameter channelConnectorParameter : channelConnector.getChannelConnectorParameters()) {
                            scriptContext.addVariable(channelConnectorParameter.getName(), channelConnectorParameter.getValue());
                        }
                    }
                    for (ChannelConfigurationParameter channelConfigurationParameter : channel.getChannelConfigurationParameters()) {
                        scriptContext.addVariable(channelConfigurationParameter.getSourceConfigurationParameterName(), channelConfigurationParameter.getValue());
                    }
                    scriptContext.setScriptProvider(new IScriptProvider() {

                        @Override
                        public ScraperScript getScript(String scriptName) {
                            return CacheManager.getInstance().getCache(ScriptVersionedCache.class).getScriptByName(scriptName);
                        }
                    });
                    scriptContext.setTraceLoggingEnabled(UserContext.current().isTraceLoggingEnabled());
                    LOG.info("Executing post manifest script");
                    scraperScript.execute();
                    LOG.info("Successfully executed post manifest script");
                    String manifestLink = ScriptExecutionContext.current().getScriptOutput();
                    if (StringUtils.isNotBlank(manifestLink)) {
                        String fileName = manifestLink.substring(manifestLink.lastIndexOf("/") + 1);
                        String destFilePath = "/tmp/" + System.currentTimeMillis() + "-" + fileName;
                        try {
                            httpSender.downloadToFile(manifestLink, destFilePath);
                            manifestLink = documentService.uploadFile(new File(destFilePath), Constants.CHANNEL_SHIPPING_MANIFEST_BUCKET_NAME);
                            shippingManifest.setManifestSynced(true);
                            shippingManifest.setManifestLink(manifestLink);
                            response.getShippingManifestStatus().setShippingManifestLink(manifestLink);
                        } catch (HttpTransportException e) {
                            LOG.error("Unable to download manifest" + e);
                            throw new RuntimeException(e);
                        }
                    }
                } finally {
                    ScriptExecutionContext.destroy();
                }
            }
        } else if (!channel.isThirdPartyShipping() && !ConfigurationManager.getInstance().getConfiguration(ProductConfiguration.class).isCourierManagementSwitchedOff()
                && shippingManifest.getShippingProviderCode() != null) {
            ShippingProvider shippingProvider = ConfigurationManager.getInstance().getConfiguration(ShippingConfiguration.class).getShippingProviderByCode(
                    shippingManifest.getShippingProviderCode());
            if (StringUtils.isNotBlank(ConfigurationManager.getInstance().getConfiguration(ShippingSourceConfiguration.class).getShippingSourceByCode(
                    shippingProvider.getShippingProviderSourceCode()).getPostManifestScript())) {
                ScraperScript scraperScript = CacheManager.getInstance().getCache(ScriptVersionedCache.class).getScriptByName(
                        ConfigurationManager.getInstance().getConfiguration(ShippingSourceConfiguration.class).getShippingSourceByCode(
                                shippingProvider.getShippingProviderSourceCode()).getPostManifestScript());
                if (scraperScript != null) {
                    try {
                        ScriptExecutionContext scriptContext = ScriptExecutionContext.current();
                        scriptContext.addVariable("shippingManifest", shippingManifest);
                        scriptContext.addVariable("resultItems", failedShippingPackages);
                        scriptContext.setTraceLoggingEnabled(UserContext.current().isTraceLoggingEnabled());
                        scraperScript.execute();
                    } finally {
                        ScriptExecutionContext.destroy();
                    }
                }
            }
        }
        //shippingMao.save(response.getShippingManifestStatus());
        return shippingManifest;
    }

    @Override
    @Transactional
    public List<ShippingPackage> getEligibleShippingPackagesForShippingManifest(ShippingManifest shippingManifest) {
        return shippingDao.getEligibleShippingPackagesForShippingManifest(shippingManifest);
    }

    @Override
    @Transactional
    public ShippingManifest updateShippingManifest(ShippingManifest shippingManifest) {
        return shippingDao.updateShippingManifest(shippingManifest);
    }

    @Override
    @Transactional
    public String getShippingManifestOutput(String manifestCode, Template template) {
        ShippingManifest manifest = shippingDao.getShippingManifestDetailedByCode(manifestCode);
        ManifestDetailDTO shippingManifest = getDetailedManifestDTO(manifest);
        Map<String, Object> params = new HashMap<>();
        params.put("shippingManifest", shippingManifest);
        params.put("manifest", manifest);
        return template.evaluate(params);
    }

    @Override
    @Transactional
    public DiscardShippingManifestResponse discardShippingManifest(DiscardShippingManifestRequest request) {
        DiscardShippingManifestResponse response = new DiscardShippingManifestResponse();
        ValidationContext context = request.validate();
        ShippingManifest shippingManifest = null;
        if (!context.hasErrors()) {
            shippingManifest = shippingDao.getShippingManifestDetailedByCode(request.getShippingManifestCode());
            if (shippingManifest == null) {
                context.addError(WsResponseCode.INVALID_SHIPPING_MANIFEST_ID, "Shipping Manifest ID is invalid");
            }
        }

        if (!context.hasErrors()) {
            ReadWriteLock readWriteLock = lockingService.getReadWriteLock(Namespace.MANIFEST, request.getShippingManifestCode(), Level.FACILITY);
            try {
                readWriteLock.writeLock().lock();
                if (!ShippingManifest.StatusCode.CREATED.name().equals(shippingManifest.getStatusCode())) {
                    context.addError(WsResponseCode.INVALID_SHIPPING_MANIFEST_STATE, "Shipping Manifest not in CREATED state");
                } else if (shippingManifest.getShippingManifestItems().size() > 0) {
                    context.addError(WsResponseCode.INVALID_SHIPPING_MANIFEST_STATE, "Shipping Manifest contains shipping packages");
                } else if (!shippingManifest.getUser().getId().equals(request.getUserId())) {
                    context.addError(WsResponseCode.INVALID_USER_ID, "shipping manifest created by another user");
                } else {
                    shippingManifest.setStatusCode(ShippingManifest.StatusCode.DISCARDED.name());
                    response.setSuccessful(true);
                }
            } finally {
                readWriteLock.writeLock().unlock();
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Transactional
    private void sendShippingManifestMail(ShippingManifest shippingManifest) {
        Template template = CacheManager.getInstance().getCache(PrintTemplateCache.class).getTemplateByType(PrintTemplateVO.Type.SHIPPING_MANIFEST_CSV.name());
        if (template != null) {
            shippingManifest = getShippingManifestDetailedByCode(shippingManifest.getCode());
            String userEmail = shippingManifest.getUser().getEmail();
            ManifestDetailDTO manifestDTO = getDetailedManifestDTO(shippingManifest);
            Map<String, Object> params = new HashMap<>();
            params.put("shippingManifest", manifestDTO);
            params.put("manifest", shippingManifest);
            String shippingManifestText = template.evaluate(params);
            String directory = EnvironmentProperties.getShippingManifestDirectoryPath();
            File file = new File((directory.endsWith("/") ? directory : directory + "/") + shippingManifest.getCode() + ".csv");
            FileOutputStream fout = null;
            try {
                fout = new FileOutputStream(file);
                FileUtils.write(shippingManifestText.getBytes(), fout);
                List<String> recipients = new ArrayList<>();
                recipients.add(userEmail);
                EmailMessage message = new EmailMessage(recipients, EmailTemplateType.SHIPPING_MANIFEST.name());
                message.addTemplateParam("shippingManifest", shippingManifest);
                message.getAttachments().add(file);
                emailService.send(message);
            } catch (Exception e) {
                LOG.error("Error occured while sending shipping manifest mail:", e);
            } finally {
                FileUtils.safelyCloseOutputStream(fout);
            }
        } else {
            LOG.warn("Ignoring shipping manifest mail as template is missing");
        }
    }

    @Override
    @Transactional(readOnly = true)
    public ShippingManifest getShippingManifestById(Integer shippingManifestId) {
        return shippingDao.getShippingManifestById(shippingManifestId);
    }

    @Override
    @Transactional(readOnly = true)
    public SearchManifestResponse searchManifest(SearchManifestRequest request) {
        SearchManifestResponse response = new SearchManifestResponse();
        ValidationContext context = request.validate();
        if (context.hasErrors()) {
            response.setSuccessful(false);
            response.setErrors(context.getErrors());
        } else {
            List<ShippingManifest> shippingManifests = shippingDao.searchManifest(request);
            if (request.getSearchOptions() != null && request.getSearchOptions().isGetCount()) {
                Long count = shippingDao.getManifestCount(request);
                response.setTotalRecords(count);
            }
            for (ShippingManifest manifest : shippingManifests) {
                ShippingManifestDTO shippingManifestDTO = new ShippingManifestDTO();
                shippingManifestDTO.setCreated(manifest.getCreated());
                User user = manifest.getUser();
                shippingManifestDTO.setGeneratedBy(user.getEmail());
                shippingManifestDTO.setUsername(user.getUsername());
                shippingManifestDTO.setManifestCode(manifest.getCode());
                if (manifest.getShippingMethod() != null) {
                    shippingManifestDTO.setShippingMethod(manifest.getShippingMethod().getName());
                    shippingManifestDTO.setPaymentMethodCode(manifest.getShippingMethod().getPaymentMethod().getCode());
                }
                shippingManifestDTO.setShippingProvider(manifest.getShippingProviderName() != null ? manifest.getShippingProviderName() : "All");
                shippingManifestDTO.setStatusCode(manifest.getStatusCode());
                response.getElements().add(shippingManifestDTO);
            }
            response.setErrors(context.getErrors());
            response.setSuccessful(true);
        }
        return response;
    }

    @Override
    @Transactional(readOnly = true)
    public List<ShippingPackage> getShippingPackagesDispatchedInDateRange(DateRange range) {
        return shippingDao.getShippingPackagesDispatchedInDateRange(range);
    }

    @Override
    @Transactional
    public List<ShippingManifestStatus> getShippingManifestStatuses() {
        return shippingDao.getShippingManifestStatuses();
    }

    @Override
    public RemoveManifestItemResponse removeManifestItem(RemoveManifestItemRequest request) {
        String lockKey = request.getManifestCode();
        ReadWriteLock readWriteLock = lockingService.getReadWriteLock(Namespace.MANIFEST, lockKey, Level.FACILITY);
        try {
            LOG.info("[RemoveManifestItem] Acquiring lock on namespace: {} and key: {}", Namespace.MANIFEST.name(), lockKey);
            Long timestamp = System.currentTimeMillis();
            readWriteLock.writeLock().lock();
            LOG.info("[RemoveManifestItem] lock on namespace: {} and key: {} obtained successfully in: {} ms",
                    new Object[] { Namespace.MANIFEST.name(), lockKey, (System.currentTimeMillis() - timestamp) });
            return removeManifestItemInternal(request);
        } finally {
            readWriteLock.writeLock().unlock();
            LOG.info("[RemoveManifestItem] Released lock on namespace: {} and key: {}", Namespace.MANIFEST.name(), lockKey);
        }
    }

    private RemoveManifestItemResponse removeManifestItemInternal(RemoveManifestItemRequest request) {
        RemoveManifestItemResponse response = new RemoveManifestItemResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            ShippingManifest shippingManifest = getShippingManifestDetailedByCode(request.getManifestCode());
            if (shippingManifest == null) {
                context.addError(WsResponseCode.INVALID_SHIPPING_MANIFEST_CODE, "Shipping Manifest Code is invalid");
            } else {
                ShippingManifestItem shippingManifestItem = null;
                for (ShippingManifestItem item : shippingManifest.getShippingManifestItems()) {
                    if (item.getShippingPackage().getCode().equals(request.getShippingPackageCode())) {
                        shippingManifestItem = item;
                    }
                }
                if (shippingManifestItem == null) {
                    context.addError(WsResponseCode.INVALID_SHIPPING_PACKAGE_CODE, "Manifest " + request.getManifestCode() + " doesn't contain the requested item");
                } else if (!StatusCode.MANIFESTED.name().equals(shippingManifestItem.getShippingPackage().getStatusCode())) {
                    context.addError(WsResponseCode.INVALID_SHIPPING_PACKAGE_STATE, "Only manifested packages can be removed from manifest");
                } else {
                    doRemoveShippingManifestItemInternal(shippingManifestItem);
                    response.setSuccessful(true);
                    response.setTotalManifestItemCount(shippingManifest.getShippingManifestItems().size() - 1);
                }
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Locks({ @Lock(ns = Namespace.SALE_ORDER, key = "#{#args[0].shippingPackage.saleOrder.code}") })
    @Transactional
    private void doRemoveShippingManifestItemInternal(ShippingManifestItem shippingManifestItem) {
        ShippingPackage shippingPackage = shippingService.getShippingPackageByCode(shippingManifestItem.getShippingPackage().getCode());
        shippingPackage.setShippingManifest(null);
        shippingPackage.setStatusCode(StatusCode.READY_TO_SHIP.name());
        for (SaleOrderItem saleOrderItem : shippingPackage.getSaleOrderItems()) {
            saleOrderItem.setStatusCode(SaleOrderItem.StatusCode.FULFILLABLE.name());
        }
        shippingDao.removeManifestItem(shippingManifestItem);
    }

    @Override
    @LogActivity
    public ForceDispatchShippingPackageResponse forceDispatchShippingPackage(ForceDispatchShippingPackageRequest request) {
        ForceDispatchShippingPackageResponse response = new ForceDispatchShippingPackageResponse();
        ShippingPackage shippingPackage = null;
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            shippingPackage = shippingService.getShippingPackageByCode(request.getShippingPackageCode());
            if (shippingPackage == null) {
                context.addError(WsResponseCode.INVALID_SHIPPING_PACKAGE_CODE);
            } else {
                if (!StringUtils.equalsAny(shippingPackage.getStatusCode(), StatusCode.CREATED.name(), ShippingPackage.StatusCode.PACKED.name(),
                        ShippingPackage.StatusCode.READY_TO_SHIP.name(), ShippingPackage.StatusCode.MANIFESTED.name())) {
                    context.addError(WsResponseCode.INVALID_SHIPPING_PACKAGE_STATE);
                } else {
                    checkIfPutawayPendingOrOnHold(shippingPackage, context);
                }
            }
        }

        if (!context.hasErrors()) {
            String shippingProviderCode = request.getShippingProviderCode();
            String trackingNumber = request.getTrackingNumber();
            if (StringUtils.isBlank(request.getShippingProviderCode())) {
                shippingProviderCode = "SELF";
            }
            String shippingProviderName = shippingProviderCode;
            ShippingProvider shippingProvider = ConfigurationManager.getInstance().getConfiguration(ShippingConfiguration.class).getShippingProviderByCode(shippingProviderCode);
            if (shippingProvider != null) {
                shippingProviderName = shippingProvider.getName();
            }
            if (StringUtils.isBlank(trackingNumber)) {
                trackingNumber = shippingPackage.getFacility().getId() + "_" + shippingPackage.getCode();
            }
            try {
                forceDispatchShippingPackageInternal(shippingPackage.getSaleOrder().getCode(), shippingPackage.getCode(), shippingProviderCode, shippingProviderName,
                        trackingNumber, request.getUserId(), request.isSkipDetailing());
                response.setSuccessful(true);
                if (ActivityContext.current().isEnable()) {
                    LOG.info("activitycontext : forceshipping package");
                    ActivityUtils.appendActivity(shippingPackage.getCode(), ActivityEntityEnum.SHIPPING_PACKAGE.getName(), shippingPackage.getSaleOrder().getCode(),
                            Arrays.asList(new String[] {
                                    "Shipping package {" + ActivityEntityEnum.SHIPPING_PACKAGE + ":" + shippingPackage.getCode() + "} forced dispatched with shipping provider "
                                            + shippingProviderName + " tracking number " + trackingNumber }),
                            ActivityTypeEnum.COMPLETE.name());
                }
            } catch (Exception e) {
                context.addError(WsResponseCode.INVALID_PACKAGE_STATE, e.getMessage());
            }
        }

        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    @Locks({ @Lock(ns = Namespace.SALE_ORDER, key = "#{#args[0]}") })
    @Transactional
    private boolean forceDispatchShippingPackageInternal(String saleOrderCode, String shippingPackageCode, String shippingProviderCode, String shippingProviderName,
            String trackingNumber, Integer userId, boolean skipDetailing) {
        ShippingPackage shippingPackage = shippingService.getShippingPackageByCode(shippingPackageCode);
        if (!StringUtils.equalsAny(shippingPackage.getStatusCode(), StatusCode.CREATED.name(), StatusCode.PACKED.name(), StatusCode.READY_TO_SHIP.name(),
                StatusCode.MANIFESTED.name())) {
            throw new IllegalStateException("Invalid package state");
        }
        saleOrderService.getSaleOrderForUpdate(shippingPackage.getSaleOrder().getId());
        while (!StatusCode.DISPATCHED.name().equals(shippingPackage.getStatusCode())) {
            StatusCode status = StatusCode.valueOf(shippingPackage.getStatusCode());
            switch (status) {
                case READY_TO_SHIP:
                    markShippingPackageDispatched(shippingPackage, true);
                    break;
                case MANIFESTED:
                    RemoveManifestItemRequest removeManifestItemRequest = new RemoveManifestItemRequest(shippingPackage.getShippingManifest().getCode(), shippingPackage.getCode());
                    removeManifestItem(removeManifestItemRequest);
                    break;
                case PACKED:
                    shippingPackage.setShippingProviderCode(shippingProviderCode);
                    shippingPackage.setShippingProviderName(shippingProviderName);
                    shippingPackage.setTrackingNumber(trackingNumber);
                    shippingPackage.setStatusCode(StatusCode.READY_TO_SHIP.name());
                    break;
                case CREATED:
                    CreateShippingPackageInvoiceRequest createShippingPackageInvoiceRequest = new CreateShippingPackageInvoiceRequest();
                    if (shippingPackage.getSaleOrder().isProductManagementSwitchedOff()) {
                        Map<String, WsTaxInformation.ProductTax> channelProductIdToTax = new HashMap<>();
                        WsTaxInformation.ProductTax productTax = new WsTaxInformation.ProductTax();
                        productTax.setTaxPercentage(new BigDecimal(5)); // TODO: Check This
                        for (SaleOrderItem saleOrderItem : shippingPackage.getSaleOrderItems()) {
                            if (!channelProductIdToTax.containsKey(saleOrderItem.getChannelProductId())) {
                                channelProductIdToTax.put(saleOrderItem.getChannelProductId(), productTax);
                            }
                        }
                        createShippingPackageInvoiceRequest.setChannelProductIdToTax(channelProductIdToTax);
                    }
                    createShippingPackageInvoiceRequest.setUserId(userId);
                    createShippingPackageInvoiceRequest.setShippingPackageCode(shippingPackageCode);
                    createShippingPackageInvoiceRequest.setCommitBlockedInventory(true);
                    createShippingPackageInvoiceRequest.setSkipDetailing(skipDetailing);
                    createShippingPackageInvoiceRequest.setForceCreate(true);
                    CreateShippingPackageInvoiceResponse createShippingPackageInvoiceResponse = iShippingInvoiceService.createShippingPackageInvoice(
                            createShippingPackageInvoiceRequest);
                    if (createShippingPackageInvoiceResponse.isSuccessful()) {
                        shippingPackage = shippingService.getShippingPackageByCode(shippingPackageCode);
                    } else {
                        throw new IllegalStateException(createShippingPackageInvoiceResponse.getErrors().toString());
                    }
                default:
                    break;
            }
        }
        return true;
    }

    @Override
    @Transactional
    public GetCurrentChannelShippingManifestResponse getCurrentChannelShippingManifest(GetCurrentChannelShippingManifestRequest request) {
        GetCurrentChannelShippingManifestResponse response = new GetCurrentChannelShippingManifestResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            Channel channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelByCode(request.getChannelCode());
            if (channel == null) {
                context.addError(WsResponseCode.INVALID_CHANNEL_CODE);
            } else {
                ShippingManifest manifest = getShippingManifestByCode(request.getShippingManifestCode());
                if (manifest == null) {
                    context.addError(WsResponseCode.INVALID_SHIPPING_MANIFEST_CODE, request.getShippingManifestCode());
                }
                if (!context.hasErrors()) {
                    Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(channel.getSourceCode());
                    if (!source.isFetchCurrentChannelManifestEnabled()) {
                        context.addError(WsResponseCode.INVALID_CHANNEL_CODE);
                    } else {
                        ScraperScript scraperScript = CacheManager.getInstance().getCache(ChannelCache.class).getScriptByName(channel.getCode(),
                                Source.FETCH_CURRENT_CHANNEL_MANIFEST_SCRIPT_NAME);
                        if (scraperScript != null) {
                            ScriptExecutionContext seContext = ScriptExecutionContext.current();
                            seContext.addVariable("channel", channel);
                            seContext.addVariable("source", source);
                            seContext.addVariable("shippingManifest", manifest);
                            for (ChannelConnector channelConnector : channel.getChannelConnectors()) {
                                for (ChannelConnectorParameter channelConnectorParameter : channelConnector.getChannelConnectorParameters()) {
                                    seContext.addVariable(channelConnectorParameter.getName(), channelConnectorParameter.getValue());
                                }
                            }
                            for (ChannelConfigurationParameter channelConfigurationParameter : channel.getChannelConfigurationParameters()) {
                                seContext.addVariable(channelConfigurationParameter.getSourceConfigurationParameterName(), channelConfigurationParameter.getValue());
                            }
                            seContext.setScriptProvider(new IScriptProvider() {

                                @Override
                                public ScraperScript getScript(String scriptName) {
                                    return CacheManager.getInstance().getCache(ScriptVersionedCache.class).getScriptByName(scriptName);
                                }
                            });
                            try {
                                seContext.setTraceLoggingEnabled(UserContext.current().isTraceLoggingEnabled());
                                scraperScript.execute();
                                response.setChannelManifestLink(seContext.getScriptOutput());
                                response.setSuccessful(true);
                            } catch (Exception e) {
                                LOG.error("Error fetching current channel manifest for channel: " + channel.getCode(), e);
                                context.addError(WsResponseCode.BAD_CHANNEL_STATE, e.getMessage());
                            } finally {
                                ScriptExecutionContext.destroy();
                            }
                        }
                    }
                }
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    @Override
    @Transactional
    public GetManifestSummaryResponse fetchShippingManifestSummary(GetManifestSummaryRequest request) {
        GetManifestSummaryResponse response = new GetManifestSummaryResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            ShippingManifest shippingManifest = shippingDao.getShippingManifestByCode(request.getCode());
            if (shippingManifest == null) {
                context.addError(WsResponseCode.INVALID_SHIPPING_MANIFEST_CODE, "Invalid shipping manifest code");
            } else {
                response.setSummary(prepareManifestSummaryDTO(shippingManifest));
                response.setSuccessful(true);
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    private ManifestSummaryDTO prepareManifestSummaryDTO(ShippingManifest shippingManifest) {
        ManifestSummaryDTO manifestDTO = new ManifestSummaryDTO();
        manifestDTO.setCode(shippingManifest.getCode());
        manifestDTO.setCreated(shippingManifest.getCreated());
        manifestDTO.setStatusCode(shippingManifest.getStatusCode());
        manifestDTO.setUsername(shippingManifest.getUser().getUsername());
        if (shippingManifest.getShippingProviderCode() != null) {
            manifestDTO.setShippingProvider(shippingManifest.getShippingProviderName());
            manifestDTO.setShippingProviderCode(shippingManifest.getShippingProviderCode());
        } else {
            manifestDTO.setShippingProvider("ALL");
            manifestDTO.setShippingProviderCode("ALL");
        }
        manifestDTO.setShippingArrangedBy(shippingManifest.getShippingManager().name());
        manifestDTO.setShippingManfestLink(shippingManifest.getManifestLink());
        manifestDTO.setCustomFieldValues(CustomFieldUtils.getCustomFieldValuesDTO(shippingManifest));
        manifestDTO.setComments(shippingManifest.getComments());
        if (shippingManifest.getShippingMethod() != null) {
            ShippingMethod shippingMethodById = shippingService.getShippingMethodById(shippingManifest.getShippingMethod().getId());
            manifestDTO.setCashOnDelivery(shippingMethodById.isCashOnDelivery());
            manifestDTO.setShippingMethod(shippingMethodById.getName());
        }
        Channel channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelByCode(shippingManifest.getChannel().getCode());
        Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(channel.getSourceCode());
        manifestDTO.setChannel(channel.getCode());
        manifestDTO.setFetchCurrentChannelManifestEnabled(source.isFetchCurrentChannelManifestEnabled());
        manifestDTO.setClosed(shippingManifest.getClosed());
        return manifestDTO;
    }

    @Override
    @Transactional
    public GetManifestShippingProvidersSummaryResponse fetchManifestShippingProvidersSummary(GetManifestShippingProvidersSummaryRequest request) {
        GetManifestShippingProvidersSummaryResponse response = new GetManifestShippingProvidersSummaryResponse();
        ValidationContext context = request.validate();
        ShippingManifest shippingManifest = null;
        Map<String, ManifestProviderDTO> providerCodeToManifestShipment = new HashMap<>();
        if (!context.hasErrors()) {
            shippingManifest = shippingDao.getShippingManifestByCode(request.getCode());
            if (shippingManifest == null) {
                context.addError(WsResponseCode.INVALID_SHIPPING_MANIFEST_CODE, "Invalid shipping manifest code");
            } else {
                for (ShippingManifestItem manifestItem : shippingManifest.getShippingManifestItems()) {
                    ShippingPackage shippingPackage = manifestItem.getShippingPackage();
                    ManifestProviderDTO manifestShipmentProvider = providerCodeToManifestShipment.get(shippingPackage.getShippingProviderCode());
                    if (manifestShipmentProvider == null) {
                        manifestShipmentProvider = new ManifestProviderDTO(shippingPackage.getShippingProviderCode(), shippingPackage.getShippingProviderName());
                        providerCodeToManifestShipment.put(manifestShipmentProvider.getProviderCode(), manifestShipmentProvider);
                    }
                    manifestShipmentProvider.addShipment(shippingPackage);
                }
            }
        }
        if (!context.hasErrors()) {
            List<ManifestProviderDTO> manifestProvidersSumary = new ArrayList<>(providerCodeToManifestShipment.values());
            response.setManifestSummary(new ManifestProviderSummaryDTO(shippingManifest.getShippingManifestItems().size(), manifestProvidersSumary));
            response.setSuccessful(true);
        } else {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    @Override
    @Transactional
    public GetShippingManifestItemDetailsResponse getShippingManifestItemDetails(GetShippingManifestItemDetailsRequest request) {
        GetShippingManifestItemDetailsResponse response = new GetShippingManifestItemDetailsResponse();
        ValidationContext context = request.validate();
        ShippingPackage shippingPackage = null;
        if (!context.hasErrors()) {
            ShippingManifest sManifest = shippingDao.getShippingManifestByCode(request.getManifestCode());
            if (sManifest == null) {
                context.addError(WsResponseCode.INVALID_SHIPPING_MANIFEST_CODE, "Invalid shipping manifest code");
            } else {
                for (ShippingManifestItem sManifestItem : sManifest.getShippingManifestItems()) {
                    if (sManifestItem.getShippingPackage().getCode().equals(request.getPackageCode())) {
                        shippingPackage = sManifestItem.getShippingPackage();
                        ShippingManifestItemDetailDTO shippingManifestItemDetailDTO = new ShippingManifestItemDetailDTO();
                        prepareManifestPackageDetailDTO(shippingManifestItemDetailDTO, sManifestItem.getShippingPackage());
                        response.setShippingManifestItemDetail(shippingManifestItemDetailDTO);
                    }
                }
            }
            if (shippingPackage == null) {
                context.addError(WsResponseCode.INVALID_SHIPPING_PACKAGE_CODE, "Shipping package is invalid or does not belog to manifest : " + sManifest.getCode());
            }
        }
        response.setErrors(context.getErrors());
        response.setSuccessful(!context.hasErrors());
        return response;
    }

    @Override
    @Transactional
    public EditShippingManifestMetadataResponse editShipingManifestMetadata(EditShippingManifestMetadataRequest request) {
        EditShippingManifestMetadataResponse response = new EditShippingManifestMetadataResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            ShippingManifest shippingManifest = getShippingManifestByCode(request.getShippingManifestCode());
            if (shippingManifest == null) {
                context.addError(WsResponseCode.INVALID_SHIPPING_MANIFEST_CODE);
            } else {
                if (request.getCustomFieldValues() != null) {
                    CustomFieldUtils.setCustomFieldValues(shippingManifest,
                            CustomFieldUtils.getCustomFieldValues(context, ShippingManifest.class.getName(), request.getCustomFieldValues()), false);
                }
                if (!context.hasErrors()) {
                    shippingDao.updateShippingManifest(shippingManifest);
                    response.setSuccessful(true);
                }
            }
        }
        response.setWarnings(context.getWarnings());
        response.setErrors(context.getErrors());
        return response;
    }

    private void prepareManifestPackageDetailDTO(ShippingManifestItemDetailDTO shippingManifestItemDetailDTO, ShippingPackage sp) {
        shippingManifestItemDetailDTO.setCode(sp.getCode());
        shippingManifestItemDetailDTO.setShippingProviderCode(sp.getShippingProviderCode());
        shippingManifestItemDetailDTO.setShippingMethodCode(sp.getShippingMethod().getShippingMethodCode());
        ShippingMethod shippingMethod = shippingService.getShippingMethodById(sp.getShippingMethod().getId());
        shippingManifestItemDetailDTO.setCashOnDelivery(shippingMethod.isCashOnDelivery());
        shippingManifestItemDetailDTO.setShippingPackageType(sp.getShippingPackageType().getCode());
        shippingManifestItemDetailDTO.setShippingAddress(new AddressDTO(sp.getShippingAddress()));
        shippingManifestItemDetailDTO.setActualWeight(new BigDecimal(sp.getActualWeight()).divide(new BigDecimal(1000.0), 3, RoundingMode.HALF_EVEN));
        shippingManifestItemDetailDTO.setUpdated(sp.getUpdated());
        shippingManifestItemDetailDTO.setNoOfItems(sp.getNoOfItems());
        shippingManifestItemDetailDTO.setNoOfBoxes(sp.getNoOfBoxes());
        shippingManifestItemDetailDTO.setTrackingNumber(sp.getTrackingNumber());
        shippingManifestItemDetailDTO.setStatusCode(sp.getStatusCode());
        shippingManifestItemDetailDTO.setTotalAmount(sp.getTotalPrice());

        if (ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).isSellingPricesTaxExclusive()) {
            BigDecimal taxAmount = BigDecimal.ZERO;
            for (InvoiceItem invoiceItem : sp.getInvoice().getInvoiceItems()) {
                taxAmount = taxAmount.add(invoiceItem.getTotalTaxAmount());
            }
            shippingManifestItemDetailDTO.setTotalAmount(shippingManifestItemDetailDTO.getTotalAmount().add(taxAmount));
        }
        Map<String, List<SaleOrderItem>> lineItemToSaleOrderItems = prepareLiteItemsDetails(sp);

        List<ShippingPackageLineItem> packageLineItems = new ArrayList<>(lineItemToSaleOrderItems.size());
        for (Entry<String, List<SaleOrderItem>> lineItem : lineItemToSaleOrderItems.entrySet()) {
            ShippingPackageLineItem shippingPackageLineItem = new ShippingPackageLineItem();
            SaleOrderItem soi = lineItem.getValue().iterator().next();
            shippingPackageLineItem.setLineItemIdentifier(lineItem.getKey());
            shippingPackageLineItem.setItemName(sp.getSaleOrder().isProductManagementSwitchedOff() ? soi.getChannelProductName() : soi.getItemType().getName());
            shippingPackageLineItem.setSellerSkuCode(soi.getChannelSkuCode());
            shippingPackageLineItem.setQuantity(lineItem.getValue().size());
            packageLineItems.add(shippingPackageLineItem);
        }
        shippingManifestItemDetailDTO.setShippingPackageLineItems(packageLineItems);
    }

    @Override
    public AutoDispatchShippingPackageResponse autoDispatchShippingPackage(AutoDispatchShippingPackageRequest request) {
        AutoDispatchShippingPackageResponse response = new AutoDispatchShippingPackageResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            ShippingPackage shippingPackage = shippingService.getShippingPackageByCode(request.getShippingPackageCode());
            if (shippingPackage == null) {
                context.addError(WsResponseCode.INVALID_SHIPPING_PACKAGE_CODE, "Invalid shipping package code " + request.getShippingPackageCode());
            } else if (ShippingManager.CHANNEL.name().equals(shippingPackage.getShippingManager().name())) {
                context.addError(WsResponseCode.INVALID_SHIPPING_METHOD, "Package with channel shipping should be manifested");
            } else {
                CreateShippingManifestRequest createShippingManifestRequest = new CreateShippingManifestRequest();
                createShippingManifestRequest.setUserId(request.getUserId());
                Channel channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelById(shippingPackage.getSaleOrder().getChannel().getId());
                createShippingManifestRequest.setChannel(channel.getCode());
                createShippingManifestRequest.setShippingProviderCode(shippingPackage.getShippingProviderCode());
                createShippingManifestRequest.setShippingProviderName(shippingPackage.getShippingProviderName());
                CreateShippingManifestResponse createShippingManifestResponse = createShippingManifest(createShippingManifestRequest);
                if (!createShippingManifestResponse.isSuccessful()) {
                    context.addErrors(createShippingManifestResponse.getErrors());
                } else {
                    AddShippingPackageToManifestRequest addShippingPackageToManifestRequest = new AddShippingPackageToManifestRequest();
                    addShippingPackageToManifestRequest.setShippingManifestCode(createShippingManifestResponse.getShippingManifestCode());
                    addShippingPackageToManifestRequest.setAwbOrShipmentCode(shippingPackage.getCode());
                    addShippingPackageToManifestRequest.setShippingPackageTypeCode(shippingPackage.getShippingPackageType().getCode());
                    AddShippingPackageToManifestResponse addShippingPackagesToManifestResponse = addShippingPackageToManifest(addShippingPackageToManifestRequest);
                    if (!addShippingPackagesToManifestResponse.isSuccessful()) {
                        context.addErrors(addShippingPackagesToManifestResponse.getErrors());
                    } else {
                        CloseShippingManifestRequest closeShippingManifestRequest = new CloseShippingManifestRequest();
                        closeShippingManifestRequest.setShippingManifestCode(createShippingManifestResponse.getShippingManifestCode());
                        CloseShippingManifestResponse closeShippingManifestResponse = closeShippingManifest(closeShippingManifestRequest);
                        if (!closeShippingManifestResponse.isSuccessful()) {
                            context.addErrors(closeShippingManifestResponse.getErrors());
                        } else {
                            response.setSuccessful(true);
                        }
                    }
                }
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        response.setSuccessful(!context.hasErrors());
        return response;
    }

    @Override
    public VerifySaleOrderItemsPODCodeResponse verifySaleOrderItemsPODCode(VerifySaleOrderItemsPODCodeRequest request) {
        VerifySaleOrderItemsPODCodeResponse response = new VerifySaleOrderItemsPODCodeResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            SaleOrder saleOrder = saleOrderService.getSaleOrderByCode(request.getSaleOrderCode());
            if (saleOrder == null) {
                context.addError(WsResponseCode.INVALID_SALE_ORDER_CODE);
            } else {
                ShippingPackage shippingPackage = shippingService.getValidShippingPackage(request.getSaleOrderCode(), request.getSaleOrderItemCodes());
                if (shippingPackage == null) {
                    context.addError(WsResponseCode.INVALID_SHIPPING_PACKAGE_CODE, "Sale order item codes do not correspond to a package");
                } else {
                    VerifyShippingPackagePODCodeRequest verifyShippingPackagePODCodeRequest = new VerifyShippingPackagePODCodeRequest();
                    verifyShippingPackagePODCodeRequest.setShippingPackageCode(shippingPackage.getCode());
                    verifyShippingPackagePODCodeRequest.setPodCode(request.getPodCode());
                    response.setResponse(verifyShippingPackagePODCode(verifyShippingPackagePODCodeRequest));
                }
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    @Override
    @Transactional
    public VerifyShippingPackagePODCodeResponse verifyShippingPackagePODCode(VerifyShippingPackagePODCodeRequest request) {
        VerifyShippingPackagePODCodeResponse response = new VerifyShippingPackagePODCodeResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            ShippingPackage shippingPackage = shippingService.getShippingPackageByCode(request.getShippingPackageCode());
            if (shippingPackage == null) {
                context.addError(WsResponseCode.INVALID_SHIPPING_PACKAGE_CODE, request.getShippingPackageCode());
            } else {
                Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(
                        shippingPackage.getSaleOrder().getChannel().getSourceCode());
                if (!source.isPODRequired() || ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).isRestrictOTPGeneration()) {
                    context.addError(WsResponseCode.INVALID_SOURCE, "Action not applicable for source " + source.getCode());
                } else if (!shippingPackage.isPodVerified() && !shippingPackage.getPodCode().equals(request.getPodCode())) {
                    context.addError(WsResponseCode.INVALID_POD_CODE, "Invalid pod code " + request.getPodCode());
                } else {
                    shippingPackage.setPodVerified(true);
                }
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        response.setSuccessful(!context.hasErrors());
        return response;
    }

    @Override
    public String generateManifestPdfLabel(String shippingManifestCode, String manifestLabelHtml) {
        ValidationContext context = new ValidationContext();
        PrintTemplateVO shippingManifestLabelTemplate = CacheManager.getInstance().getCache(PrintTemplateCache.class).getPrintTemplateByType(
                PrintTemplateVO.Type.SHIPPING_MANIFEST);
        String labelPath = "/tmp/" + EncryptionUtils.md5Encode(shippingManifestCode) + ".pdf";
        if (StringUtils.isNotBlank(manifestLabelHtml)) {
            File shippingLabel = new File(labelPath);
            FileOutputStream fos = null;
            try {
                fos = new FileOutputStream(shippingLabel);
                SamplePrintTemplateVO samplePrintTemplate = CacheManager.getInstance().getCache(SamplePrintTemplateCache.class).getSamplePrintTemplate(
                        shippingManifestLabelTemplate.getSamplePrintTemplateCode());
                pdfDocumentService.writeHtmlToPdf(fos, manifestLabelHtml, new PrintOptions(samplePrintTemplate));
                return EncryptionUtils.base64EncodeFile(labelPath);
            } catch (FileNotFoundException e) {
                context.addError(WsResponseCode.UNABLE_TO_DOWNLOAD_LABEL, "Unable to download label for package " + shippingManifestCode);
            } finally {
                FileUtils.safelyCloseOutputStream(fos);
            }
        } else {
            context.addError(WsResponseCode.UNABLE_TO_DOWNLOAD_LABEL, "Unable to download label for package " + shippingManifestCode);
        }
        return null;
    }
}
