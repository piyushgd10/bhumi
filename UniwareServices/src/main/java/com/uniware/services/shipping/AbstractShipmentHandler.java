/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *  @version     1.0, Jan 24, 2012
 *  @author singla
 */
package com.uniware.services.shipping;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.unifier.core.transport.http.HttpSender;
import com.unifier.core.transport.http.HttpTransportException;
import com.uniware.core.utils.Constants;
import com.uniware.services.document.IDocumentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.utils.ApplicationUtils;
import com.unifier.core.utils.StringUtils;
import com.unifier.scraper.sl.exception.ScriptExecutionException;
import com.unifier.scraper.sl.runtime.ScraperScript;
import com.unifier.scraper.sl.runtime.ScriptExecutionContext;
import com.uniware.core.entity.PartyAddressType;
import com.uniware.core.entity.ShipmentTracking;
import com.uniware.core.entity.ShippingMethod;
import com.uniware.core.entity.ShippingPackage;
import com.uniware.core.entity.ShippingProvider;
import com.uniware.core.entity.ShippingProviderConnector;
import com.uniware.core.entity.ShippingProviderConnector.Status;
import com.uniware.core.entity.ShippingProviderConnectorParameter;
import com.uniware.core.entity.ShippingProviderMethod;
import com.uniware.core.entity.ShippingSourceConnector;
import com.uniware.core.entity.Source;
import com.uniware.core.utils.UserContext;
import com.uniware.services.cache.ScriptVersionedCache;
import com.uniware.services.configuration.ShippingConfiguration;
import com.uniware.services.configuration.ShippingFacilityLevelConfiguration;
import com.uniware.services.configuration.ShippingSourceConfiguration;
import com.uniware.services.exception.NoAvailableTrackingNumberException;

/**
 * @author singla
 */
public abstract class AbstractShipmentHandler implements Serializable {

    private static final Logger                LOG        = LoggerFactory.getLogger(AbstractShipmentHandler.class);

    private HttpSender                         httpSender = new HttpSender();
    private ShippingProvider                   shippingProvider;
    private int                                maxTrackingNumberPerRequest;

    @Autowired
    private transient IShippingProviderService shippingProviderService;

    @Autowired
    private transient IDocumentService         documentService;

    /**
     * @return the shippingProviderService
     */
    public IShippingProviderService getShippingProviderService() {
        return shippingProviderService;
    }

    /**
     * @param shippingProviderService the shippingProviderService to set
     */
    public void setShippingProviderService(IShippingProviderService shippingProviderService) {
        this.shippingProviderService = shippingProviderService;
    }

    /**
     * @return the shippingProvider
     */
    public ShippingProvider getShippingProvider() {
        return shippingProvider;
    }

    /**
     * @param shippingProvider the shippingProvider to initialize
     */
    public void setShippingProvider(ShippingProvider shippingProvider) {
        this.shippingProvider = shippingProvider;
    }

    public String generateTrackingNumber(ShippingPackage shippingPackage) throws NoAvailableTrackingNumberException {
        ShippingFacilityLevelConfiguration shippingConfiguration = ConfigurationManager.getInstance().getConfiguration(ShippingFacilityLevelConfiguration.class);
        ShippingMethod shippingMethod = shippingPackage.getShippingMethod();
        ShippingProvider shippingProvider = ConfigurationManager.getInstance().getConfiguration(ShippingConfiguration.class).getShippingProviderByCode(
                shippingPackage.getShippingProviderCode());
        ShippingProviderMethod shippingProviderMethod = shippingConfiguration.getShippingProviderMethod(shippingProvider.getId(), shippingMethod.getId());
        if (ShippingProviderMethod.TrackingNumberGeneration.LIST.name().equals(shippingProviderMethod.getTrackingNumberGeneration())
                || ShippingProviderMethod.TrackingNumberGeneration.GLOBAL_LIST.name().equals(shippingProviderMethod.getTrackingNumberGeneration())) {
            return shippingProviderService.generateTrackingNumberFromList(getShippingProvider(), shippingMethod);
        } else if (ShippingProviderMethod.TrackingNumberGeneration.MANUAL.name().equals(shippingProviderMethod.getTrackingNumberGeneration())) {
            return null;
        } else if (ShippingProviderMethod.TrackingNumberGeneration.CUSTOM.name().equals(shippingProviderMethod.getTrackingNumberGeneration())) {
            return scrapeTrackingNumber(shippingPackage, shippingProviderMethod);
        } else {
            throw new IllegalStateException("Illegal value for tracking number generation " + shippingProviderMethod.getTrackingNumberGeneration());
        }
    }

    private String scrapeTrackingNumber(ShippingPackage shippingPackage, ShippingProviderMethod shippingProviderMethod) {
        if (StringUtils.isBlank(ConfigurationManager.getInstance().getConfiguration(ShippingSourceConfiguration.class).getShippingSourceByCode(
                shippingProvider.getShippingProviderSourceCode()).getTrackingAllocationScript())) {
            throw new IllegalStateException("tracking number allocation script name is null:" + shippingProvider.getCode());
        }
        ScraperScript scraperScript = CacheManager.getInstance().getCache(ScriptVersionedCache.class).getScriptByName(
                ConfigurationManager.getInstance().getConfiguration(ShippingSourceConfiguration.class).getShippingSourceByCode(
                        shippingProvider.getShippingProviderSourceCode()).getTrackingAllocationScript());
        if (scraperScript == null) {
            throw new IllegalStateException("tracking number allocation script is not properly configured for shipping provider:" + shippingProvider.getCode());
        }
        try {
            ScriptExecutionContext context = ScriptExecutionContext.current();
            context.addVariable("shippingPackage", shippingPackage);
            context.addVariable("shippingProviderLocation", shippingProviderService.getShippingProviderLocation(shippingProviderMethod.getShippingMethod().getId(),
                    shippingProvider.getId(), shippingPackage.getShippingAddress().getPincode()));
            context.addVariable("warehouseShippingProviderLocation", shippingProviderService.getShippingProviderLocation(shippingProviderMethod.getShippingMethod().getId(),
                    shippingProvider.getId(), shippingPackage.getFacility().getPartyAddressByType(PartyAddressType.Code.SHIPPING.name()).getPincode()));

            Map<String, String> shippingProviderParameters = new HashMap<String, String>();
            Map<String, String> scriptResponse = new HashMap<String, String>();
            for (ShippingProviderConnector connector : shippingProvider.getShippingProviderConnectors()) {
                ShippingSourceConnector sc = ConfigurationManager.getInstance().getConfiguration(ShippingSourceConfiguration.class).getShippingSourceConnector(
                        shippingProvider.getShippingProviderSourceCode(), connector.getShippingSourceConnectorName());
                if (sc.isRequiredInAwbFetch() && !connector.getStatusCode().equals(Status.NOT_CONFIGURED)) {
                    for (ShippingProviderConnectorParameter providerParameter : connector.getShippingProviderConnectorParameters()) {
                        shippingProviderParameters.put(providerParameter.getName(), providerParameter.getValue());
                    }
                }
            }
            context.addVariable("shippingProviderParameters", shippingProviderParameters);
            context.addVariable("response", scriptResponse);
            context.setTraceLoggingEnabled(UserContext.current().isTraceLoggingEnabled());
            scraperScript.execute();
            String shippingLabelLink = scriptResponse.get("shippingLabelLink");
            if (StringUtils.isNotBlank(shippingLabelLink)) {
//                boolean uploadOnS3Enabled = Boolean.parseBoolean(scriptResponse.get("uploadOnS3Enabled"));
//                if (uploadOnS3Enabled) {
//                    shippingLabelLink = documentService.uploadFile(new File(shippingLabelLink), Constants.CHANNEL_SHIPPING_LABEL_BUCKET_NAME);
//                    shippingPackage.setLabelSynced(true);
//                } else {
//                    shippingLabelLink = ApplicationUtils.appendAppIdentifier(shippingLabelLink);
//                }
//                shippingPackage.setShippingLabelLink(shippingLabelLink);
                shippingProviderService.updateShippingLabelLink(shippingPackage, shippingLabelLink);
                String shippingLabelFormat = scriptResponse.get("shippingLabelFormat");
                if (shippingLabelFormat != null) {
                    switch (Source.ShipmentLabelFormat.valueOf(scriptResponse.get("shippingLabelFormat"))) {
                        case CSV:
                            shippingPackage.setShipmentLabelFormat(Source.ShipmentLabelFormat.CSV);
                            break;
                        case HTML:
                            shippingPackage.setShipmentLabelFormat(Source.ShipmentLabelFormat.HTML);
                            break;
                        case PDF:
                            shippingPackage.setShipmentLabelFormat(Source.ShipmentLabelFormat.PDF);
                            break;
                        case PNG:
                            shippingPackage.setShipmentLabelFormat(Source.ShipmentLabelFormat.PNG);
                            break;
                    }
                }
            }
            return context.getScriptOutput();
        } catch (ScriptExecutionException e) {
            LOG.error("unable to scrape tracking number allocation for provider:" + shippingProvider.getCode(), e);
            throw e;
        } catch (Exception e) {
            LOG.error("unable to scrape tracking number allocation for provider:" + shippingProvider.getCode(), e);
            throw new RuntimeException(e);
        } finally {
            ScriptExecutionContext.destroy();
        }
    }

    public abstract Map<String, ProviderShipmentStatus> getShipmentStatuses(List<ShipmentTracking> shipments);

    /**
     * @return the maxTrackingNumberPerRequest
     */
    public int getMaxTrackingNumberPerRequest() {
        return maxTrackingNumberPerRequest;
    }

    /**
     * @param maxTrackingNumberPerRequest the maxTrackingNumberPerRequest to set
     */
    public void setMaxTrackingNumberPerRequest(int maxTrackingNumberPerRequest) {
        this.maxTrackingNumberPerRequest = maxTrackingNumberPerRequest;
    }

    /**
     * @param shipmentTrackings
     * @return
     */
    protected List<String> getTrackingNumbers(List<ShipmentTracking> shipmentTrackings) {
        List<String> trackingNumbers = new ArrayList<String>();
        for (ShipmentTracking shipmentTracking : shipmentTrackings) {
            trackingNumbers.add(shipmentTracking.getTrackingNumber());
        }
        return trackingNumbers;
    }
}
