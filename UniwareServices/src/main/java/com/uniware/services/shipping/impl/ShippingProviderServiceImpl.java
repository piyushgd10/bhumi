/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jan 24, 2012
 *  @author singla
 */
package com.uniware.services.shipping.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.support.DefaultConversionService;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.unifier.core.annotation.Parameter;
import com.unifier.core.annotation.audit.LogActivity;
import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.api.validation.WsError;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.template.Template;
import com.unifier.core.transport.http.HttpSender;
import com.unifier.core.transport.http.HttpTransportException;
import com.unifier.core.utils.CollectionUtils;
import com.unifier.core.utils.EncryptionUtils;
import com.unifier.core.utils.FileUtils;
import com.unifier.core.utils.JibxUtils;
import com.unifier.core.utils.JsonUtils;
import com.unifier.core.utils.StringUtils;
import com.unifier.core.utils.XMLParser;
import com.unifier.core.utils.XMLParser.Element;
import com.unifier.scraper.sl.exception.ScriptExecutionException;
import com.unifier.scraper.sl.runtime.IScriptProvider;
import com.unifier.scraper.sl.runtime.ScraperScript;
import com.unifier.scraper.sl.runtime.ScriptExecutionContext;
import com.unifier.services.aspect.RollbackOnFailure;
import com.unifier.services.pdf.IPdfDocumentService;
import com.unifier.services.pdf.impl.PdfDocumentServiceImpl.PrintOptions;
import com.unifier.services.tenantprofile.service.ITenantProfileService;
import com.unifier.services.user.notification.IUserNotificationService;
import com.unifier.services.utils.ExceptionUtils;
import com.unifier.services.vo.TenantProfileVO;
import com.uniware.core.api.admin.shipping.ShippingProviderDTO;
import com.uniware.core.api.catalog.GetServiceabilityRequest;
import com.uniware.core.api.catalog.GetServiceabilityResponse;
import com.uniware.core.api.catalog.SearchShippingProviderLocationRequest;
import com.uniware.core.api.catalog.SearchShippingProviderLocationResponse;
import com.uniware.core.api.catalog.SearchShippingProviderLocationResponse.ShippingProviderLocationDTO;
import com.uniware.core.api.invoice.CreateShippingPackageInvoiceRequest;
import com.uniware.core.api.invoice.CreateShippingPackageInvoiceResponse;
import com.uniware.core.api.invoice.WsShippingProviderInfo;
import com.uniware.core.api.invoice.WsTaxInformation;
import com.uniware.core.api.shipping.AllocateShippingProviderRequest;
import com.uniware.core.api.shipping.AllocateShippingProviderResponse;
import com.uniware.core.api.shipping.AssignManualShippingProviderRequest;
import com.uniware.core.api.shipping.AssignManualShippingProviderResponse;
import com.uniware.core.api.shipping.AssignManualTrackingNumberRequest;
import com.uniware.core.api.shipping.AssignManualTrackingNumberResponse;
import com.uniware.core.api.shipping.BulkAllocateShippingProviderRequest;
import com.uniware.core.api.shipping.BulkAllocateShippingProviderResponse;
import com.uniware.core.api.shipping.BulkCreateInvoiceAndAllocateShippingProviderRequest;
import com.uniware.core.api.shipping.BulkCreateInvoiceAndAllocateShippingProviderResponse;
import com.uniware.core.api.shipping.CreateInvoiceAndAllocateShippingProviderRequest;
import com.uniware.core.api.shipping.CreateInvoiceAndAllocateShippingProviderResponse;
import com.uniware.core.api.shipping.CreateInvoiceAndGenerateLabelRequest;
import com.uniware.core.api.shipping.CreateInvoiceAndGenerateLabelResponse;
import com.uniware.core.api.shipping.GetShippingPackageShipmentDetailsRequest;
import com.uniware.core.api.shipping.GetShippingPackageShipmentDetailsResponse;
import com.uniware.core.api.shipping.MarkShippingPackageReadyForPickupRequest;
import com.uniware.core.api.shipping.MarkShippingPackageReadyForPickupResponse;
import com.uniware.core.api.shipping.ReAllocateShippingProviderRequest;
import com.uniware.core.api.shipping.ReAllocateShippingProviderResponse;
import com.uniware.core.api.shipping.RefreshShippingLabelRequest;
import com.uniware.core.api.shipping.RefreshShippingLabelResponse;
import com.uniware.core.api.shipping.ShipmentTrackingSyncStatusDTO;
import com.uniware.core.api.shipping.VerifyandSyncShippingConnectorParamsResponse;
import com.uniware.core.api.systemnotification.sandbox.SandboxParams;
import com.uniware.core.api.usernotification.AddUserNotificationRequest;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.cache.FacilityCache;
import com.uniware.core.entity.Channel;
import com.uniware.core.entity.ChannelConfigurationParameter;
import com.uniware.core.entity.ChannelConnector;
import com.uniware.core.entity.ChannelConnectorParameter;
import com.uniware.core.entity.Facility;
import com.uniware.core.entity.ShippingMethod;
import com.uniware.core.entity.ShippingPackage;
import com.uniware.core.entity.ShippingPackage.ShippingManager;
import com.uniware.core.entity.ShippingProvider;
import com.uniware.core.entity.ShippingProviderAllocationRule;
import com.uniware.core.entity.ShippingProviderConnector;
import com.uniware.core.entity.ShippingProviderConnectorParameter;
import com.uniware.core.entity.ShippingProviderLocation;
import com.uniware.core.entity.ShippingProviderMethod;
import com.uniware.core.entity.ShippingProviderMethod.TrackingNumberGeneration;
import com.uniware.core.entity.ShippingProviderSource;
import com.uniware.core.entity.ShippingProviderTrackingNumber;
import com.uniware.core.entity.ShippingSourceConnector;
import com.uniware.core.entity.Source;
import com.uniware.core.entity.Source.ShipmentLabelFormat;
import com.uniware.core.entity.SourceConnector;
import com.uniware.core.locking.Namespace;
import com.uniware.core.locking.annotation.Lock;
import com.uniware.core.locking.annotation.Locks;
import com.uniware.core.script.error.SaleOrderScriptError;
import com.uniware.core.utils.ActivityContext;
import com.uniware.core.utils.Constants;
import com.uniware.core.utils.UserContext;
import com.uniware.core.vo.PrintTemplateVO;
import com.uniware.core.vo.SamplePrintTemplateVO;
import com.uniware.core.vo.ShippingZoneVO;
import com.uniware.core.vo.UserNotificationTypeVO;
import com.uniware.dao.shipping.IShippingDao;
import com.uniware.dao.shipping.IShippingMao;
import com.uniware.services.audit.impl.ActivityEntityEnum;
import com.uniware.services.audit.impl.ActivityTypeEnum;
import com.uniware.services.audit.impl.ActivityUtils;
import com.uniware.services.cache.ChannelCache;
import com.uniware.services.cache.PrintTemplateCache;
import com.uniware.services.cache.SamplePrintTemplateCache;
import com.uniware.services.cache.ScriptVersionedCache;
import com.uniware.services.channel.IChannelService;
import com.uniware.services.configuration.ShippingConfiguration;
import com.uniware.services.configuration.ShippingFacilityLevelConfiguration;
import com.uniware.services.configuration.ShippingSourceConfiguration;
import com.uniware.services.configuration.SourceConfiguration;
import com.uniware.services.configuration.data.manager.ProductConfiguration;
import com.uniware.services.document.IDocumentService;
import com.uniware.services.exception.NoAvailableShippingProviderException;
import com.uniware.services.exception.NoAvailableTrackingNumberException;
import com.uniware.services.invoice.IInvoiceService;
import com.uniware.services.saleorder.ISaleOrderService;
import com.uniware.services.shipping.AbstractShipmentHandler;
import com.uniware.services.shipping.IShippingAdminService;
import com.uniware.services.shipping.IShippingInvoiceService;
import com.uniware.services.shipping.IShippingProviderService;
import com.uniware.services.shipping.IShippingService;
import com.uniware.services.shipping.ProviderShipmentStatus;
import com.uniware.services.shipping.scraper.ScrapedShipmentStatusHandler;

/**
 * @author singla
 */
@Service
public class ShippingProviderServiceImpl implements IShippingProviderService {

    private static final Logger      LOG               = LoggerFactory.getLogger(ShippingProviderServiceImpl.class);

    private static final Integer     MAX_FAILED_COUNT  = 6;

    private HttpSender               httpSender        = new HttpSender();

    @Autowired
    private ApplicationContext       applicationContext;

    @Autowired
    private IShippingDao             shippingDao;

    @Autowired
    private IInvoiceService          invoiceService;

    @Autowired
    private IShippingService         shippingService;

    @Autowired
    private IPdfDocumentService      pdfDocumentService;

    @Autowired
    private ITenantProfileService    tenantProfileService;

    @Autowired
    private IShippingAdminService    shippingAdminService;

    @Autowired
    private IUserNotificationService userNotificationService;

    @Autowired
    private IShippingMao             shippingMao;

    @Autowired
    private ISaleOrderService        saleOrderService;

    @Autowired
    private IChannelService          channelService;

    @Autowired
    private IDocumentService         documentService;

    @Autowired
    private IShippingInvoiceService  iShippingInvoiceService;

    private final ConversionService  conversionService = new DefaultConversionService();

    @Override
    @Transactional
    public String generateTrackingNumberFromList(ShippingProvider provider, ShippingMethod shippingMethod) throws NoAvailableTrackingNumberException {
        ShippingProviderMethod shippingProviderMethod = shippingDao.getShippingProviderMethod(provider.getCode(), shippingMethod.getName());
        boolean isGlobal = TrackingNumberGeneration.GLOBAL_LIST.name().equals(shippingProviderMethod.getTrackingNumberGeneration());
        ShippingProviderTrackingNumber trackingNumber = shippingDao.getAvailableTrackingNumber(provider.getId(), shippingMethod.getId(), isGlobal);
        if (trackingNumber != null) {
            trackingNumber.setUsed(true);
            shippingDao.updateShippingProviderTrackingNumber(trackingNumber);
            return trackingNumber.getTrackingNumber();
        } else {
            throw new NoAvailableTrackingNumberException(
                    "unable to find any valid trackingNumber for provider:" + provider.getName() + ", shippingMethod:" + shippingMethod.getName());
        }
    }

    @Override
    @Transactional
    public List<ShippingProvider> getAvailableShippingProviders(Integer shippingMethodId, String pincode, ShippingPackage shippingPackage)
            throws NoAvailableShippingProviderException {
        List<ShippingProviderLocation> shippingProviderLocations = shippingDao.getAvailableShippingProviders(shippingMethodId, pincode);
        List<ShippingProvider> globalServicingProviders = ConfigurationManager.getInstance().getConfiguration(
                ShippingFacilityLevelConfiguration.class).getGlobalServicingShippingProviders(shippingMethodId);
        if (shippingProviderLocations.size() == 0 && CollectionUtils.isEmpty(globalServicingProviders)) {
            throw new NoAvailableShippingProviderException("no shipping provider is available for given shippingMethod and pincode");
        }
        List<ShippingProvider> shippingProviders = new ArrayList<ShippingProvider>();
        for (ShippingProviderLocation shippingProviderLocation : shippingProviderLocations) {
            boolean available = true;
            if (shippingPackage != null && shippingProviderLocation.getFilterExpression() != null) {
                Map<String, Object> contextParams = new HashMap<String, Object>();
                contextParams.put("shippingPackage", shippingPackage);
                available = shippingProviderLocation.getFilterExpression().evaluate(contextParams, Boolean.class);
            }
            if (available) {
                ShippingProvider shippingProvider = shippingProviderLocation.getShippingProvider();
                shippingProviders.add(shippingProvider);
            }
        }
        if (!CollectionUtils.isEmpty(globalServicingProviders)) {
            shippingProviders.addAll(globalServicingProviders);
        }
        return shippingProviders;
    }

    /* (non-Javadoc)
     * @see com.uniware.services.shipping.IShippingProviderService#getShippingProviderLocation(java.lang.Integer, java.lang.Integer, java.lang.String)
     */
    @Override
    @Transactional
    public ShippingProviderLocation getShippingProviderLocation(Integer shippingMethodId, Integer shippingProviderId, String pincode) {
        return shippingDao.getShippingProviderLocation(shippingMethodId, shippingProviderId, pincode);
    }

    @Override
    @LogActivity
    public AllocateShippingProviderResponse allocateShippingProvider(AllocateShippingProviderRequest request) {
        AllocateShippingProviderResponse response = new AllocateShippingProviderResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            ShippingPackage shippingPackage = shippingService.getShippingPackageByCode(request.getShippingPackageCode());
            if (shippingPackage == null) {
                context.addError(WsResponseCode.INVALID_SHIPPING_PACKAGE_CODE, "Shipping package is not found");
            } else if (ShippingManager.NONE.equals(shippingPackage.getShippingManager())) {
                context.addError(WsResponseCode.COURIER_MANAGEMENT_SWITCHED_OFF, "The courier management is switched off");
            } else {
                try {
                    allocateShippingProviderInternal(response, context, shippingPackage);
                } catch (Throwable e) {
                    LOG.error("Failed to allocate shipping provider", e);
                    response = new AllocateShippingProviderResponse();
                    response.addError(new WsError(90001, e.getMessage()));
                }
            }
        }
        response.addErrors(context.getErrors());
        return response;
    }

    @Override
    @LogActivity
    public ReAllocateShippingProviderResponse reallocateShippingProvider(ReAllocateShippingProviderRequest request) {
        ReAllocateShippingProviderResponse response = new ReAllocateShippingProviderResponse();
        ValidationContext context = request.validate();
        ShippingPackage shippingPackage = null;
        if (!context.hasErrors()) {
            shippingPackage = shippingService.getShippingPackageByCode(request.getShippingPackageCode());
            if (shippingPackage == null) {
                context.addError(WsResponseCode.INVALID_SHIPPING_PACKAGE_CODE);
            } else {
                Channel channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelById(shippingPackage.getSaleOrder().getChannel().getId());
                Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(channel.getSourceCode());
                if (!source.isRefetchShippingLabelAllowed()) {
                    context.addError(WsResponseCode.REFETCH_LABEL_NOT_ALLOWED);
                }
            }
        }
        if (!context.hasErrors()) {
            try {
                response = reallocateShippingProviderInternal(response, context, shippingPackage);
            } catch (Throwable e) {
                LOG.error("Failed to reallocate shipping provider", e);
                response.addError(new WsError(90001, e.getMessage()));
            }
        }

        if (context.hasErrors()) {
            response.addErrors(context.getErrors());
        }
        return response;
    }

    @Locks({ @Lock(ns = Namespace.SALE_ORDER, key = "#{#args[2].saleOrder.code}") })
    @Transactional
    @RollbackOnFailure
    private ReAllocateShippingProviderResponse reallocateShippingProviderInternal(ReAllocateShippingProviderResponse response, ValidationContext context,
            ShippingPackage shippingPackage) {
        shippingPackage = shippingDao.getShippingPackageByCode(shippingPackage.getCode());
        if (!StringUtils.equalsAny(shippingPackage.getStatusCode(), ShippingPackage.StatusCode.PACKED.name(), ShippingPackage.StatusCode.READY_TO_SHIP.name())) {
            context.addError(WsResponseCode.INVALID_PACKAGE_STATE);
        } else if (!shippingPackage.getSaleOrder().isThirdPartyShipping()) {
            context.addError(WsResponseCode.INVALID_PACKAGE_STATE, "Third party shipping not available");
        }
        if (!context.hasErrors()) {
            shippingPackage.setShippingProviderCode(null);
            shippingPackage.setShippingProviderName(null);
            shippingPackage.setTrackingNumber(null);
            shippingPackage.setShippingLabelLink(null);
            shippingPackage.setManualAwbAllocation(false);
            shippingPackage.setStatusCode(ShippingPackage.StatusCode.PACKED.name());
            AllocateShippingProviderRequest allocateShippingProviderRequest = new AllocateShippingProviderRequest();
            allocateShippingProviderRequest.setShippingPackageCode(shippingPackage.getCode());
            AllocateShippingProviderResponse allocateShippingProviderResponse = allocateShippingProvider(allocateShippingProviderRequest);
            response.setResponse(allocateShippingProviderResponse);
            response.setShippingLabelLink(allocateShippingProviderResponse.getShippingLabelLink());
        } else {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    /* (non-Javadoc)
     * @see com.uniware.services.shipping.IShippingService#allocateShippingProvider(com.uniware.core.api.shipping.AllocateShippingProviderRequest)
     */
    @Locks({ @Lock(ns = Namespace.SALE_ORDER, key = "#{#args[2].saleOrder.code}") })
    @Transactional(rollbackFor = { NoAvailableTrackingNumberException.class, NoAvailableShippingProviderException.class })
    private void allocateShippingProviderInternal(AllocateShippingProviderResponse response, ValidationContext context, ShippingPackage shippingPackage)
            throws NoAvailableTrackingNumberException, NoAvailableShippingProviderException {
        boolean activityEnabled = ActivityContext.current().isEnable();
        List<String> activityLogs = new ArrayList<>();
        shippingPackage = shippingDao.getShippingPackageByCode(shippingPackage.getCode());
        if (!StringUtils.equalsAny(shippingPackage.getStatusCode(), ShippingPackage.StatusCode.PACKED.name())) {
            context.addError(WsResponseCode.INVALID_PACKAGE_STATE);
        } else if (shippingPackage.getShippingProviderCode() != null) {
            context.addError(WsResponseCode.SHIPPING_PROVIDER_ALREADY_ALLOCATED, "Shipping Provider is already allocated");
        } else {
            Channel channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelById(shippingPackage.getSaleOrder().getChannel().getId());
            ScraperScript shippingProviderAllocationScript = null;
            if (ShippingManager.CHANNEL.equals(shippingPackage.getShippingManager()) && shippingPackage.getSaleOrderItems().iterator().next().getShippingProvider() == null) {
                if (CacheManager.getInstance().getCache(ChannelCache.class).getScriptName(channel.getCode(), Source.SHIPPING_PROVIDER_ALLOCATION_SCRIPT_NAME) != null) {
                    shippingProviderAllocationScript = CacheManager.getInstance().getCache(ChannelCache.class).getScriptByName(channel.getCode(),
                            Source.SHIPPING_PROVIDER_ALLOCATION_SCRIPT_NAME);
                }
                if (shippingProviderAllocationScript != null) {
                    if (getShippingProviderFromThirdParty(context, shippingPackage, shippingProviderAllocationScript, channel)) {
                        if (activityEnabled && !context.hasErrors()) {
                            activityLogs.add("Channel provides shipping. Shipping provider:" + shippingPackage.getShippingProviderCode() + " allocated with AWB number:"
                                    + shippingPackage.getTrackingNumber());
                        }
                    }
                } else {
                    if (ConfigurationManager.getInstance().getConfiguration(ProductConfiguration.class).isCourierManagementSwitchedOff()) {
                        shippingPackage.setShippingManager(ShippingManager.NONE);
                    } else {
                        shippingPackage.setShippingManager(ShippingManager.UNIWARE);
                    }
                }
            }

            if (!context.hasErrors() && ShippingManager.UNIWARE.equals(shippingPackage.getShippingManager())) {
                ShippingProvider shippingProvider = findSuitableShippingProvider(shippingPackage);
                shippingPackage.setShippingProviderCode(shippingProvider.getCode());
                shippingPackage.setShippingProviderName(shippingProvider.getName());
                shippingPackage.setTrackingNumber(generateTrackingNumber(shippingPackage));
                if (activityEnabled) {
                    activityLogs.add(
                            "Self Shipping. Shipping provider:" + shippingPackage.getShippingProviderCode() + " allocated with AWB number:" + shippingPackage.getTrackingNumber());
                }
            }

            if (!context.hasErrors() && shippingPackage.getShippingLabelLink() == null
                    && CacheManager.getInstance().getCache(ChannelCache.class).getScriptName(channel.getCode(), Source.FETCH_SHIPPING_LABEL_SCRIPT_NAME) != null) {
                ScraperScript fetchShippingLabelScript = CacheManager.getInstance().getCache(ChannelCache.class).getScriptByName(channel.getCode(),
                        Source.FETCH_SHIPPING_LABEL_SCRIPT_NAME);
                fetchShippingLabelFromChannelForSelfShipping(context, channel, shippingPackage, fetchShippingLabelScript);
                if (activityEnabled) {
                    activityLogs.add("Self Shipping. Label fetched from channel:" + shippingPackage.getShippingLabelLink());
                }
            }
            if (!context.hasErrors()) {
                markPackageReadyToShip(shippingPackage);
                shippingDao.updateShippingPackage(shippingPackage);
                if (activityEnabled) {
                    ActivityUtils.appendActivity(shippingPackage.getCode(), ActivityEntityEnum.SHIPPING_PACKAGE.getName(), shippingPackage.getSaleOrder().getCode(), activityLogs,
                            ActivityTypeEnum.EDIT.name());
                }
                response.setSuccessful(true);
                response.setShippingLabelLink(shippingPackage.getShippingLabelLink());
                response.setShippingPackageCode(shippingPackage.getCode());
                response.setShippingProviderCode(shippingPackage.getShippingProviderCode());
                response.setTrackingNumber(shippingPackage.getTrackingNumber());
                response.setShipmentLabelFormat(shippingPackage.isThirdPartyShipping() ? channel.getShipmentLabelFormat() : ShipmentLabelFormat.PDF.name());
                response.setShippingManagedBy(shippingPackage.getShippingManager().name());
                response.setStatusCode(shippingPackage.getStatusCode());
            }
        }
    }

    private void markPackageReadyToShip(ShippingPackage shippingPackage) {
        boolean activityEnabled = ActivityContext.current().isEnable();
        List<String> activityLogs = new ArrayList<>();
        if (shippingPackage.getShippingProviderCode() != null) {
            if (shippingPackage.getTrackingNumber() != null) {
                shippingPackage.setStatusCode(ShippingPackage.StatusCode.READY_TO_SHIP.name());
                if (activityEnabled) {
                    activityLogs.add(
                            "Shipping package {" + ActivityEntityEnum.SHIPPING_PACKAGE + ":" + shippingPackage.getCode() + "} moved to " + shippingPackage.getStatusCode());
                }
            } else {
                shippingPackage.setManualAwbAllocation(true);
                if (activityEnabled) {
                    activityLogs.add("Manual AWB allocation");
                }
            }
            if (activityEnabled) {
                ActivityUtils.appendActivity(shippingPackage.getCode(), ActivityEntityEnum.SHIPPING_PACKAGE.getName(), shippingPackage.getSaleOrder().getCode(), activityLogs,
                        ActivityTypeEnum.EDIT.name());
            }
        }
    }

    public void fetchShippingLabelFromChannelForSelfShipping(ValidationContext context, Channel channel, ShippingPackage shippingPackage, ScraperScript fetchShippingLabelScript) {
        ScriptExecutionContext seContext = ScriptExecutionContext.current();
        for (ChannelConnector channelConnector : channel.getChannelConnectors()) {
            for (ChannelConnectorParameter channelConnectorParameter : channelConnector.getChannelConnectorParameters()) {
                seContext.addVariable(channelConnectorParameter.getName(), channelConnectorParameter.getValue());
            }
        }
        for (ChannelConfigurationParameter channelConfigurationParameter : channel.getChannelConfigurationParameters()) {
            seContext.addVariable(channelConfigurationParameter.getSourceConfigurationParameterName(), channelConfigurationParameter.getValue());
        }
        seContext.setScriptProvider(new IScriptProvider() {

            @Override
            public ScraperScript getScript(String scriptName) {
                return CacheManager.getInstance().getCache(ScriptVersionedCache.class).getScriptByName(scriptName);
            }
        });
        seContext.addVariable("shippingPackage", shippingPackage);
        seContext.addVariable("channel", channel);
        try {
            seContext.setTraceLoggingEnabled(UserContext.current().isTraceLoggingEnabled());
            fetchShippingLabelScript.execute();
            String shippingLabelLink = seContext.getScriptOutput();
            if (StringUtils.isNotBlank(shippingLabelLink)) {
                updateShippingLabelLink(shippingPackage, shippingLabelLink);
            }
        } finally {
            ScriptExecutionContext.destroy();
        }
    }

    @Override
    public BulkAllocateShippingProviderResponse bulkAllocateShippingProvider(BulkAllocateShippingProviderRequest request) {
        BulkAllocateShippingProviderResponse response = new BulkAllocateShippingProviderResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            for (String shippingPackageCode : request.getShippingPackageCodes()) {
                AllocateShippingProviderRequest allocateShippingProviderRequest = new AllocateShippingProviderRequest();
                allocateShippingProviderRequest.setShippingPackageCode(shippingPackageCode);
                AllocateShippingProviderResponse allocateShippingProviderResponse;
                try {
                    allocateShippingProviderResponse = allocateShippingProvider(allocateShippingProviderRequest);
                } catch (Throwable e) {
                    LOG.error("Bulk allocation of shipping provider failed", e);
                    allocateShippingProviderResponse = new AllocateShippingProviderResponse();
                    allocateShippingProviderResponse.addError(new WsError(90001, e.getMessage()));
                }
                if (!allocateShippingProviderResponse.isSuccessful()) {
                    response.getFailures().put(shippingPackageCode, allocateShippingProviderResponse.getErrors());
                } else {
                    response.getSuccessfulPackages().add(shippingPackageCode);
                }
            }
            if (response.getFailures().size() == 0) {
                response.setSuccessful(true);
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Transactional
    @Override
    public RefreshShippingLabelResponse refreshShippingLabel(RefreshShippingLabelRequest request) {
        RefreshShippingLabelResponse response = new RefreshShippingLabelResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            ShippingPackage shippingPackage = shippingService.getShippingPackageByCode(request.getShippingPackageCode());
            if (shippingPackage == null) {
                context.addError(WsResponseCode.INVALID_SHIPPING_PACKAGE_CODE);
            } else if (!StringUtils.equalsAny(shippingPackage.getStatusCode(), ShippingPackage.StatusCode.PACKED.name(), ShippingPackage.StatusCode.READY_TO_SHIP.name(),
                    ShippingPackage.StatusCode.MANIFESTED.name(), ShippingPackage.StatusCode.DISPATCHED.name())) {
                context.addError(WsResponseCode.INVALID_PACKAGE_STATE);
            } else {
                boolean success = false;
                Channel channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelById(shippingPackage.getSaleOrder().getChannel().getId());
                if (StringUtils.isNotBlank(CacheManager.getInstance().getCache(ChannelCache.class).getScriptName(channel.getCode(),
                        com.uniware.core.entity.Source.REFRESH_SHIPPING_LABEL_SCRIPT_NAME))) {
                    ScraperScript script = CacheManager.getInstance().getCache(ChannelCache.class).getScriptByName(channel.getCode(),
                            com.uniware.core.entity.Source.REFRESH_SHIPPING_LABEL_SCRIPT_NAME);

                    if (script != null) {
                        success = getLabelFromChannel(context, shippingPackage, script, channel);
                    }
                }
                if (!success) {
                    context.addError(WsResponseCode.BAD_CHANNEL_STATE);
                } else {
                    response.setSuccessful(true);
                }
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    private boolean getLabelFromChannel(ValidationContext context, ShippingPackage shippingPackage, ScraperScript refetchLabelScript, Channel channel) {
        SandboxParams sandboxParams = getSandboxParameters(channel, shippingPackage);
        boolean success = false;
        try {
            String allocationXml = executeScript(shippingPackage, refetchLabelScript, channel, sandboxParams);
            if (StringUtils.isNotBlank(allocationXml)) {
                Element rootElement = XMLParser.parse(allocationXml);
                String shippingLabelLink = rootElement.text("ShippingLabelLink");
                if (StringUtils.isNotBlank(shippingLabelLink)) {
                    updateShippingLabelLink(shippingPackage, shippingLabelLink);
                    success = true;
                }
            }
        } catch (Exception e) {
            LOG.error("Unable to fetch shipping provider information from third party", e);
        } finally {
            ScriptExecutionContext.destroy();
        }
        return success;
    }

    private boolean getShippingProviderFromThirdParty(ValidationContext context, ShippingPackage shippingPackage, ScraperScript shippingProviderAllocationScript, Channel channel) {
        String message = null;
        SaleOrderScriptError scriptError;
        SandboxParams sandboxParams = getSandboxParameters(channel, shippingPackage);
        try {
            String allocationXml = executeScript(shippingPackage, shippingProviderAllocationScript, channel, sandboxParams);
            if (StringUtils.isNotBlank(allocationXml)) {
                LOG.info("Shipping Provider Allocation xml : {}", allocationXml);
                WsShippingProviderInfo shippingProviderInfo = JibxUtils.unmarshall("unicommerceServicesBinding19", WsShippingProviderInfo.class, allocationXml);
                return assignChannelShippingProvider(shippingPackage, shippingProviderInfo, context, false);
            }
        } catch (ScriptExecutionException ex) {
            scriptError = SaleOrderScriptError.getScriptError(ex.getReasonCode());
            LOG.error("ScriptExecutionException while fetching shipping provider information from third party", ex);
            message = scriptError == null ? ex.getMessage() : scriptError.getMessage();
        } catch (Exception e) {
            LOG.error("Unable to fetch shipping provider information from third party", e);
            message = e.getMessage();
        } finally {
            ScriptExecutionContext.destroy();
        }
        if (message != null) {
            context.addError(WsResponseCode.UNABLE_TO_FETCH_SHIPPING_PROVIDER_INFORMATION, message);
        }
        return true;
    }

    @Override
    public boolean assignChannelShippingProvider(ShippingPackage shippingPackage, WsShippingProviderInfo shippingProviderInfo, ValidationContext context, boolean moveToReadyToShip) {
        String shippingProviderCode = null;
        String trackingNumber = null;
        String shippingLabelLink = null;
        String shipmentLabelFormat = null;
        boolean manualTrackingNumberAllocation = false;
        try {
            shippingPackage.setAdditionalInfo(shippingProviderInfo.getAdditionalInfo());
            if (shippingProviderInfo.getThirdPartyShippingNotAvailable() != null && shippingProviderInfo.getThirdPartyShippingNotAvailable()) {
                shippingPackage.setShipmentLabelFormat(ShipmentLabelFormat.PDF);
                if (ConfigurationManager.getInstance().getConfiguration(ProductConfiguration.class).isCourierManagementSwitchedOff()) {
                    shippingPackage.setShippingManager(ShippingManager.NONE);
                } else {
                    shippingPackage.setShippingManager(ShippingManager.UNIWARE);
                }
                return false;
            } else {
                manualTrackingNumberAllocation = shippingProviderInfo.getManualTrackingNumberAllocation() != null && shippingProviderInfo.getManualTrackingNumberAllocation();
                trackingNumber = shippingProviderInfo.getTrackingNumber();
                shippingProviderCode = shippingProviderInfo.getShippingProvider();
                shippingLabelLink = shippingProviderInfo.getShippingLabelLink();
                shipmentLabelFormat = shippingProviderInfo.getShipmentLabelFormat();
            }
        } catch (Exception e) {
            LOG.error("Unable to fetch shipping provider information from third party", e);
            context.addError(WsResponseCode.UNABLE_TO_FETCH_SHIPPING_PROVIDER_INFORMATION, e.getMessage());
        }
        if (StringUtils.isBlank(trackingNumber) && !manualTrackingNumberAllocation) {
            context.addError(WsResponseCode.UNABLE_TO_FETCH_SHIPPING_PROVIDER_INFORMATION, "Unable to fetch shipping provider information from third party.");
        } else if (shippingProviderCode == null) {
            context.addError(WsResponseCode.UNABLE_TO_FETCH_SHIPPING_PROVIDER_INFORMATION, "Unable to fetch shipping provider information from third party.");
        } else {
            shippingPackage.setShippingProviderCode(shippingProviderCode.trim());
            shippingPackage.setShippingProviderName(shippingProviderCode.trim());
            shippingPackage.setTrackingNumber(trackingNumber.trim());
            shippingPackage.setShippingManager(ShippingManager.CHANNEL);
            updateShippingLabelLink(shippingPackage, shippingLabelLink);
            if (StringUtils.isNotBlank(shipmentLabelFormat)) {
                shippingPackage.setShipmentLabelFormat(ShipmentLabelFormat.valueOf(shipmentLabelFormat));
            }
        }
        if(!context.hasErrors()){
            if(moveToReadyToShip){
                markPackageReadyToShip(shippingPackage);
            }
            shippingDao.updateShippingPackage(shippingPackage);
        }
        return !context.hasErrors();
    }

    @Override
    public void updateShippingLabelLink(ShippingPackage shippingPackage, String shippingLabelLink) {
        if (StringUtils.isNotBlank(shippingLabelLink)) {
            String fileName = shippingLabelLink.substring(shippingLabelLink.lastIndexOf("/") + 1);
            String destFilePath = "/tmp/" + fileName;
            try {
                httpSender.downloadToFile(shippingLabelLink, destFilePath);
                shippingLabelLink = documentService.uploadFile(new File(destFilePath), Constants.CHANNEL_SHIPPING_LABEL_BUCKET_NAME);
                shippingPackage.setLabelSynced(true);
                shippingPackage.setShippingLabelLink(shippingLabelLink);
            } catch (HttpTransportException e) {
                LOG.error("Unable to download shipping label" + e);
                throw new RuntimeException(e);
            }
        }
    }

    private String executeScript(ShippingPackage shippingPackage, ScraperScript shippingProviderAllocationScript, Channel channel, SandboxParams sandboxParams) {
        Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(channel.getSourceCode());
        ScriptExecutionContext seContext = ScriptExecutionContext.current();
        TenantProfileVO tenantProfileVO = tenantProfileService.getTenantProfileByCode(UserContext.current().getTenant().getCode());
        seContext.addVariable("shippingPackage", shippingPackage);
        seContext.addVariable("tenantProfile", tenantProfileVO);
        seContext.addVariable("applicationContext", applicationContext);
        for (ChannelConnector channelConnector : channel.getChannelConnectors()) {
            SourceConnector sourceConnector = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceConnector(channel.getSourceCode(),
                    channelConnector.getSourceConnectorName());
            if (sourceConnector.isRequiredInOrderSync()) {
                if (ChannelConnector.Status.INVALID_CREDENTIALS.equals(channelConnector.getStatusCode())) {
                    throw new ScriptExecutionException("Connector broken");
                }
            }
            for (ChannelConnectorParameter channelConnectorParameter : channelConnector.getChannelConnectorParameters()) {
                seContext.addVariable(channelConnectorParameter.getName(), channelConnectorParameter.getValue());
            }
        }
        for (ChannelConfigurationParameter channelConfigurationParameter : channel.getChannelConfigurationParameters()) {
            seContext.addVariable(channelConfigurationParameter.getSourceConfigurationParameterName(), channelConfigurationParameter.getValue());
        }
        seContext.setScriptProvider(new IScriptProvider() {

            @Override
            public ScraperScript getScript(String scriptName) {
                return CacheManager.getInstance().getCache(ScriptVersionedCache.class).getScriptByName(scriptName);
            }
        });
        seContext.addVariable("channel", channel);
        seContext.addVariable("source", source);
        String shippingProviderAllocationXml;
        try {
            boolean traceLoggingEnabled = UserContext.current().isTraceLoggingEnabled();
            if (traceLoggingEnabled) {
                LOG.info("SandboxParameters for shipping provider allocation script: {}", JsonUtils.objectToString(sandboxParams));
            }
            seContext.setTraceLoggingEnabled(traceLoggingEnabled);
            shippingProviderAllocationScript.execute();
            shippingProviderAllocationXml = seContext.getScriptOutput();
            LOG.info("Shipping Provider Xml response: {}", shippingProviderAllocationXml);
        } finally {
            ScriptExecutionContext.destroy();
        }
        return shippingProviderAllocationXml;
    }

    private SandboxParams getSandboxParameters(Channel channel, ShippingPackage shippingPackage) {
        SandboxParams sandboxParams = new SandboxParams();
        try {
            sandboxParams.setChannelConnectorParameters(channel.getChannelConnectorParameters());
            sandboxParams.setShippingPackage(shippingPackage);
            sandboxParams.setChannel(channel);
        } catch (Exception e) {
            LOG.error("Error creating sandbox params", e);
        }
        return sandboxParams;
    }

    @Override
    public AssignManualTrackingNumberResponse assignManualTrackingNumber(AssignManualTrackingNumberRequest request) {
        AssignManualTrackingNumberResponse response = new AssignManualTrackingNumberResponse();
        ValidationContext context = request.validate();
        try {
            if (!context.hasErrors()) {
                ShippingPackage shippingPackage = shippingService.getShippingPackageByCode(request.getShippingPackageCode());
                if (shippingPackage == null) {
                    context.addError(WsResponseCode.INVALID_SHIPPING_PACKAGE_CODE);
                } else {
                    assignManualTrackingNumberInternal(request, response, shippingPackage, context);
                }
            }
        } catch (DataIntegrityViolationException dke) {
            if (ExceptionUtils.isDuplicateKeyException(dke)) {
                context.addError(WsResponseCode.DUPLICATE_TRACKING_NUMBER, "Given tracking number is already assigned to other package");
            } else {
                context.addError(WsResponseCode.UNKNOWN_ERROR, "Unknown error while assigning tracking number");
            }
        }
        if (!context.hasErrors()) {
            response.setSuccessful(true);
        } else {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    @Locks({ @Lock(ns = Namespace.SALE_ORDER, key = "#{#args[2].saleOrder.code}") })
    @Transactional
    private void assignManualTrackingNumberInternal(AssignManualTrackingNumberRequest request, AssignManualTrackingNumberResponse response, ShippingPackage shippingPackage,
            ValidationContext context) {
        shippingPackage = shippingDao.getShippingPackageById(shippingPackage.getId(), true);
        if (!StringUtils.equalsAny(shippingPackage.getStatusCode(), ShippingPackage.StatusCode.PACKED.name(), ShippingPackage.StatusCode.READY_TO_SHIP.name())) {
            context.addError(WsResponseCode.INVALID_PACKAGE_STATE);
        } else if (shippingPackage.getShippingProviderCode() == null) {
            context.addError(WsResponseCode.SHIPPING_PROVIDER_NOT_YET_ALLOCATED, "shipping provider not yet allocated");
        } else if (StringUtils.isNotBlank(shippingPackage.getTrackingNumber())) {
            context.addError(WsResponseCode.ACTION_APPLICABLE_FOR_MANUAL_TRACKING_NUMBER_GENERATION, "tracking number already assigned to this shipment");
        } else {
            shippingPackage.setTrackingNumber(request.getTrackingNumber());
            shippingPackage.setStatusCode(ShippingPackage.StatusCode.READY_TO_SHIP.name());
            shippingDao.updateShippingPackage(shippingPackage);
            response.setShippingPackageCode(shippingPackage.getCode());
            response.setShippingProviderCode(shippingPackage.getShippingProviderCode());
            response.setTrackingNumber(shippingPackage.getTrackingNumber());
        }
    }

    @Override
    public AssignManualShippingProviderResponse assignManualShippingProvider(AssignManualShippingProviderRequest request) {
        AssignManualShippingProviderResponse response = new AssignManualShippingProviderResponse();
        ValidationContext context = request.validate();
        try {
            if (!context.hasErrors()) {
                ShippingPackage shippingPackage = shippingService.getShippingPackageByCode(request.getShippingPackageCode());
                if (shippingPackage == null) {
                    context.addError(WsResponseCode.INVALID_SHIPPING_PACKAGE_CODE);
                } else {
                    assignManualShippingProviderInternal(request, response, shippingPackage, context);
                }
            }
        } catch (DataIntegrityViolationException dke) {
            if (ExceptionUtils.isDuplicateKeyException(dke)) {
                context.addError(WsResponseCode.DUPLICATE_TRACKING_NUMBER, "Given tracking number is already assigned to other package");
            } else {
                context.addError(WsResponseCode.UNKNOWN_ERROR, "Unknown error while assigning tracking number");
            }
        }
        if (!context.hasErrors()) {
            response.setSuccessful(true);
        } else {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    @Locks({ @Lock(ns = Namespace.SALE_ORDER, key = "#{#args[2].saleOrder.code}") })
    @Transactional
    private void assignManualShippingProviderInternal(AssignManualShippingProviderRequest request, AssignManualShippingProviderResponse response, ShippingPackage shippingPackage,
            ValidationContext context) {
        shippingPackage = shippingDao.getShippingPackageById(shippingPackage.getId(), true);
        if (!StringUtils.equalsAny(shippingPackage.getStatusCode(), ShippingPackage.StatusCode.PACKED.name(), ShippingPackage.StatusCode.READY_TO_SHIP.name())) {
            context.addError(WsResponseCode.INVALID_PACKAGE_STATE);
        } else if (StringUtils.isNotBlank(shippingPackage.getShippingProviderCode())) {
            context.addError(WsResponseCode.SHIPPING_PROVIDER_ALREADY_ALLOCATED, "Shipping Provider is already allocated");
        } else if (StringUtils.isNotBlank(shippingPackage.getTrackingNumber())) {
            context.addError(WsResponseCode.ACTION_APPLICABLE_FOR_MANUAL_TRACKING_NUMBER_GENERATION, "tracking number already assigned to this shipment");
        } else {
            ShippingProvider shippingProvider = ConfigurationManager.getInstance().getConfiguration(ShippingConfiguration.class).getShippingProviderByCode(
                    request.getShippingProviderCode());
            if (shippingProvider == null) {
                context.addError(WsResponseCode.INVALID_WAREHOUSE_CONFIGURATION, "Shipping provider not configured : " + request.getShippingProviderCode());
            } else {
                shippingPackage.setShippingProviderCode(shippingProvider.getCode());
                shippingPackage.setShippingProviderName(shippingProvider.getName());
                shippingPackage.setTrackingNumber(request.getTrackingNumber());
                shippingPackage.setStatusCode(ShippingPackage.StatusCode.READY_TO_SHIP.name());
                shippingDao.updateShippingPackage(shippingPackage);
                response.setSuccessful(true);
            }
        }
    }

    @Override
    @Transactional
    public ShippingProvider findSuitableShippingProvider(ShippingPackage shippingPackage) throws NoAvailableShippingProviderException {
        if (shippingPackage.getSaleOrderItems().iterator().next().getShippingProvider() != null) {
            return shippingPackage.getSaleOrderItems().iterator().next().getShippingProvider();
        }
        return getApplicableShippingProvider(shippingPackage);
    }

    @Override
    @Transactional
    public ShippingProvider getApplicableShippingProvider(ShippingPackage shippingPackage) throws NoAvailableShippingProviderException {
        List<ShippingProvider> shippingProviders = getAvailableShippingProviders(shippingPackage.getShippingMethod().getId(), shippingPackage.getShippingAddress().getPincode(),
                shippingPackage);
        Set<Integer> applicableShippingProviders = new LinkedHashSet<Integer>();
        for (ShippingProvider shippingProvider : shippingProviders) {
            applicableShippingProviders.add(shippingProvider.getId());
        }
        Map<String, Object> contextParams = new HashMap<String, Object>();
        contextParams.put("shippingPackage", shippingPackage);
        Integer allocatedShippingProvider = null;
        ShippingFacilityLevelConfiguration configuration = ConfigurationManager.getInstance().getConfiguration(ShippingFacilityLevelConfiguration.class);
        ShippingConfiguration shippingConfiguration = ConfigurationManager.getInstance().getConfiguration(ShippingConfiguration.class);
        boolean forceAllocation = false;
        for (ShippingProviderAllocationRule allocationRule : configuration.getShippingProviderAllocationRules()) {
            if (allocationRule.getConditionExpression() != null
                    && (ShippingProviderAllocationRule.AllocationCriteria.FORCE_ALLOCATE.name().equals(allocationRule.getAllocationCriteria()) || !forceAllocation)
                    && allocationRule.getConditionExpression().evaluate(contextParams, Boolean.class)) {
                if (ShippingProviderAllocationRule.AllocationCriteria.FORCE_ALLOCATE.name().equals(allocationRule.getAllocationCriteria())) {
                    if (applicableShippingProviders.contains(allocationRule.getShippingProvider().getId())) {
                        allocatedShippingProvider = allocationRule.getShippingProvider().getId();
                        break;
                    }
                    forceAllocation = true;
                } else if (ShippingProviderAllocationRule.AllocationCriteria.ALLOCATE.name().equals(allocationRule.getAllocationCriteria())) {
                    if (applicableShippingProviders.contains(allocationRule.getShippingProvider().getId())) {
                        allocatedShippingProvider = allocationRule.getShippingProvider().getId();
                        break;
                    }
                } else {
                    applicableShippingProviders.remove(allocationRule.getShippingProvider().getId());
                }
            }
        }
        if (forceAllocation && allocatedShippingProvider == null) {
            throw new NoAvailableShippingProviderException("forced allocation results in no shipping provider");
        } else if (allocatedShippingProvider != null) {
            return shippingConfiguration.getShippingProviderById(allocatedShippingProvider);
        } else if (applicableShippingProviders.size() > 0) {
            return shippingConfiguration.getShippingProviderById(applicableShippingProviders.iterator().next());
        } else {
            throw new NoAvailableShippingProviderException("no shipping provider matches the allocation criterias");
        }
    }

    /* (non-Javadoc)
     * @see com.uniware.services.shipping.IShippingProviderService#constructShipmentHandler(com.uniware.core.entity.ShippingProvider)
     */
    @Override
    @SuppressWarnings("unchecked")
    @Transactional
    public AbstractShipmentHandler constructShipmentHandler(ShippingProvider shippingProvider) {
        try {
            ShippingProviderSource source = ConfigurationManager.getInstance().getConfiguration(ShippingSourceConfiguration.class).getShippingSourceByCode(
                    shippingProvider.getShippingProviderSourceCode());
            Class<? extends AbstractShipmentHandler> handlerClass = (Class<? extends AbstractShipmentHandler>) Class.forName(source.getHandlerClassName());
            AbstractShipmentHandler shipmentHandler = handlerClass.newInstance();
            Map<String, String> params = new HashMap<String, String>();
            for (ShippingProviderConnector spc : shippingProvider.getShippingProviderConnectors()) {
                for (ShippingProviderConnectorParameter parameter : spc.getShippingProviderConnectorParameters()) {
                    params.put(parameter.getName(), parameter.getValue());
                }
            }
            Field[] fields = handlerClass.getDeclaredFields();
            for (Field field : fields) {
                if (field.isAnnotationPresent(Parameter.class)) {
                    Parameter pAnnotation = field.getAnnotation(Parameter.class);
                    if (!params.containsKey(pAnnotation.value())) {
                        if (pAnnotation.required()) {
                            throw new IllegalStateException("provider parameters not configured properly:" + shippingProvider.getName());
                        }
                    } else {
                        field.setAccessible(true);
                        field.set(shipmentHandler, conversionService.convert(params.get(pAnnotation.value()), field.getType()));
                    }
                }
            }
            applicationContext.getAutowireCapableBeanFactory().autowireBeanProperties(shipmentHandler, AutowireCapableBeanFactory.AUTOWIRE_BY_TYPE, false);
            shipmentHandler.setShippingProvider(shippingProvider);
            shipmentHandler.setMaxTrackingNumberPerRequest(source.getMaxTrackingNumberPerRequest());
            return shipmentHandler;
        } catch (InstantiationException e) {
            LOG.error("handler class should have a public no-args constructor", e);
            throw new IllegalStateException(e);
        } catch (IllegalAccessException e) {
            LOG.error("handler class should have be accessible", e);
            throw new IllegalStateException(e);
        } catch (ClassNotFoundException e) {
            LOG.error("handler class is not correct", e);
            throw new IllegalStateException(e);
        }
    }

    /* (non-Javadoc)
     * @see com.uniware.services.shipping.IShippingProviderService#generateTrackingNumber(com.uniware.core.entity.ShippingPackage)
     */
    @Override
    @Transactional
    public String generateTrackingNumber(ShippingPackage shippingPackage) throws NoAvailableTrackingNumberException {
        if (shippingPackage.getShippingProviderCode() == null || shippingPackage.getSaleOrderItems().isEmpty()) {
            throw new IllegalStateException("Shipping provider is not allocated");
        } else {
            if (StringUtils.isNotEmpty(shippingPackage.getSaleOrderItems().iterator().next().getTrackingNumber())) {
                return shippingPackage.getSaleOrderItems().iterator().next().getTrackingNumber();
            } else {
                return ConfigurationManager.getInstance().getConfiguration(ShippingConfiguration.class).getShipmentHandlerByProviderCode(
                        shippingPackage.getShippingProviderCode()).generateTrackingNumber(shippingPackage);
            }
        }
    }

    @Transactional
    private boolean validateProviderConnector(String shippingProviderCode) {
        ShippingProvider provider = ConfigurationManager.getInstance().getConfiguration(ShippingConfiguration.class).getShippingProviderByCode(shippingProviderCode);
        boolean validated = true;
        for (ShippingProviderConnector spc : provider.getShippingProviderConnectors()) {
            ShippingSourceConnector sc = ConfigurationManager.getInstance().getConfiguration(ShippingSourceConfiguration.class).getShippingSourceConnector(
                    provider.getShippingProviderSourceCode(), spc.getShippingSourceConnectorName());
            if (sc.isRequiredInTracking() && !spc.getStatusCode().equals(ShippingProviderConnector.Status.NOT_CONFIGURED)) {
                boolean connectorBroken = false;
                VerifyandSyncShippingConnectorParamsResponse verifyParamsResponse = shippingAdminService.verifyAndSyncChannelConnectorParameters(provider.getCode(),
                        spc.getShippingSourceConnectorName());
                if (!verifyParamsResponse.isSuccessful()) {
                    spc.setErrorMessage(
                            WsResponseCode.INVALID_CHANNEL_CREDENTIALS + ":" + (verifyParamsResponse.getErrors() != null ? verifyParamsResponse.getErrors().get(0) : ""));
                    connectorBroken = true;
                    validated &= false;
                }
                if (connectorBroken) {
                    if (spc.getFailedCount() == MAX_FAILED_COUNT) {
                        userNotificationService.addNotification(
                                new AddUserNotificationRequest(provider.getCode() + ":" + UserNotificationTypeVO.NotificationType.CHANNEL_CONNECTOR_BROKEN,
                                        UserNotificationTypeVO.NotificationType.CHANNEL_CONNECTOR_BROKEN, null, "/admin/shipping/providers/edit?providerCode=" + provider.getCode(),
                                        provider.getName() + " connector is broken"));
                    } else {
                        spc.setFailedCount(spc.getFailedCount() + 1);

                    }
                    if (com.uniware.core.entity.ShippingProviderConnector.Status.ACTIVE.equals(spc.getStatusCode())) {
                        spc.setStatusCode(ShippingProviderConnector.Status.BROKEN);
                        shippingAdminService.updateShippingProviderConnector(spc);
                    }
                } else if (com.uniware.core.entity.ShippingProviderConnector.Status.BROKEN.equals(spc.getStatusCode())) {
                    spc.resetCount();
                    spc.setStatusCode(ShippingProviderConnector.Status.ACTIVE);
                    spc.setErrorMessage(null);
                    shippingAdminService.updateShippingProviderConnector(spc);
                }

            }
        }
        return validated;
    }

    /* (non-Javadoc)
     * @see com.uniware.services.shipping.IShippingProviderService#scrapeShipmentStatuses(com.uniware.core.entity.ShippingProvider, java.util.List)
     */
    @Override
    public Map<String, ProviderShipmentStatus> scrapeShipmentStatuses(ShippingProvider shippingProvider, List<String> shipments) {
        ScriptVersionedCache cache = CacheManager.getInstance().getCache(ScriptVersionedCache.class);
        ShipmentTrackingSyncStatusDTO statusDTO = ConfigurationManager.getInstance().getConfiguration(ShippingConfiguration.class).getShipmentTrackingSyncStatusDTOByCode(
                shippingProvider.getCode());
        statusDTO.setMessage("Verifying Connectors");
        boolean validated = validateProviderConnector(shippingProvider.getCode());
        if (!validated) {
            statusDTO.reset();
            statusDTO.setMessage("Connector is broken");
            LOG.info("Shipping provider {} connector broken", shippingProvider.getCode());
            return null;
        }
        if (StringUtils.isNotBlank(ConfigurationManager.getInstance().getConfiguration(ShippingSourceConfiguration.class).getShippingSourceByCode(
                shippingProvider.getShippingProviderSourceCode()).getScraperScript())) {
            ScraperScript scraperScript = cache.getScriptByName(ConfigurationManager.getInstance().getConfiguration(ShippingSourceConfiguration.class).getShippingSourceByCode(
                    shippingProvider.getShippingProviderSourceCode()).getScraperScript());
            if (scraperScript == null) {
                throw new IllegalStateException("scraper script is not properly configured for shipping provider:" + shippingProvider.getCode());
            }
            try {
                ScriptExecutionContext context = ScriptExecutionContext.current();
                context.addVariable("trackingNumbers", shipments);
                context.addVariable("shippingProvider", shippingProvider);
                Map<String, String> shippingProviderParameters = new HashMap<String, String>();
                for (ShippingProviderConnector spc : shippingProvider.getShippingProviderConnectors()) {
                    for (ShippingProviderConnectorParameter parameter : spc.getShippingProviderConnectorParameters()) {
                        shippingProviderParameters.put(parameter.getName(), parameter.getValue());
                    }
                }
                context.addVariable("shippingProviderParameters", shippingProviderParameters);
                context.setTraceLoggingEnabled(UserContext.current().isTraceLoggingEnabled());
                scraperScript.execute();
                String output = context.getScriptOutput();
                if (StringUtils.isNotBlank(output)) {
                    ScrapedShipmentStatusHandler handler = new ScrapedShipmentStatusHandler();
                    return handler.parse(output);
                } else {
                    return null;
                }
            } catch (Exception e) {
                LOG.error("unable to scrape shipment statuses for provider:" + shippingProvider.getCode() + ", trackingNumbers:" + StringUtils.join(',', shipments), e);
                throw new RuntimeException(
                        "unable to scrape shipment statuses for provider:" + shippingProvider.getCode() + ", trackingNumbers:" + StringUtils.join(',', shipments), e);
            } finally {
                ScriptExecutionContext.destroy();
            }
        }
        return null;
    }

    @Override
    @Transactional
    public List<ShippingProvider> getConfiguredShippingProviders() {
        return shippingDao.getConfiguredShippingProviders();
    }

    /* (non-Javadoc)
     * @see com.uniware.services.shipping.IShippingProviderService#getNoOfAvailableTrackingNumber(com.uniware.core.entity.ShippingProviderMethod)
     */
    @Override
    @Transactional
    public Integer getNoOfAvailableTrackingNumber(ShippingProviderMethod shippingProviderMethod) {
        if (ShippingProviderMethod.TrackingNumberGeneration.LIST.name().equals(shippingProviderMethod.getTrackingNumberGeneration())) {
            return shippingDao.getNoOfAvailableTrackingNumberList(shippingProviderMethod);
        } else {
            throw new UnsupportedOperationException("cannot get no of available tracking numbers for CUSTOM generation types");
        }
    }

    @Override
    @RollbackOnFailure
    @Transactional
    public CreateInvoiceAndGenerateLabelResponse createInvoiceAndGenerateLabel(CreateInvoiceAndGenerateLabelRequest request) {
        CreateInvoiceAndGenerateLabelResponse response = new CreateInvoiceAndGenerateLabelResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            CreateInvoiceAndAllocateShippingProviderRequest createInvoiceAndAllocateProviderRequest = new CreateInvoiceAndAllocateShippingProviderRequest();
            createInvoiceAndAllocateProviderRequest.setShippingPackageCode(request.getShippingPackageCode());
            createInvoiceAndAllocateProviderRequest.setUserId(request.getUserId());
            CreateInvoiceAndAllocateShippingProviderResponse allocateProviderResponse;
            allocateProviderResponse = createInvoiceAndAllocateShippingProvider(createInvoiceAndAllocateProviderRequest);
            response.setShippingLabelLink(allocateProviderResponse.getShippingLabelLink());
            if (allocateProviderResponse.isSuccessful()) {
                String shipmentLabel = generatePdfLabel(allocateProviderResponse.getShippingPackageCode(), allocateProviderResponse.getShippingLabelLink(), context);
                if (!context.hasErrors()) {
                    response.setShippingPackageCode(allocateProviderResponse.getShippingPackageCode());
                    response.setInvoiceCode(allocateProviderResponse.getInvoiceCode());
                    response.setShippingProviderCode(allocateProviderResponse.getShippingProviderCode());
                    response.setTrackingNumber(allocateProviderResponse.getTrackingNumber());
                    response.setLabel(shipmentLabel);
                    response.setSuccessful(true);
                }
            } else {
                response.setResponse(allocateProviderResponse);
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    public String generatePdfLabel(String shippingPackageCode, String labelLink, ValidationContext context) {
        if (labelLink != null) {
            String destFilePath = "/tmp/" + EncryptionUtils.md5Encode(shippingPackageCode) + ".pdf";
            try {
                httpSender.downloadToFile(labelLink, destFilePath);
                return EncryptionUtils.base64EncodeFile(destFilePath);
            } catch (HttpTransportException e) {
                context.addError(WsResponseCode.UNABLE_TO_DOWNLOAD_LABEL, "Unable to download label for package " + shippingPackageCode);
            }
        } else {
            PrintTemplateVO shippingLabelTemplate = CacheManager.getInstance().getCache(PrintTemplateCache.class).getPrintTemplateByType(PrintTemplateVO.Type.SHIPMENT_LABEL);
            Template template = CacheManager.getInstance().getCache(PrintTemplateCache.class).getTemplateByType(PrintTemplateVO.Type.SHIPMENT_LABEL.name());
            String shippingLabelHtml = shippingService.prepareShippingLabelText(shippingPackageCode, template);
            String labelPath = "/tmp/" + EncryptionUtils.md5Encode(shippingPackageCode) + ".pdf";
            if (StringUtils.isNotBlank(shippingLabelHtml)) {
                File shippingLabel = new File(labelPath);
                FileOutputStream fos = null;
                try {
                    fos = new FileOutputStream(shippingLabel);
                    SamplePrintTemplateVO samplePrintTemplate = CacheManager.getInstance().getCache(SamplePrintTemplateCache.class).getSamplePrintTemplate(
                            shippingLabelTemplate.getSamplePrintTemplateCode());
                    pdfDocumentService.writeHtmlToPdf(fos, shippingLabelHtml, new PrintOptions(samplePrintTemplate));
                    return EncryptionUtils.base64EncodeFile(labelPath);
                } catch (FileNotFoundException e) {
                    context.addError(WsResponseCode.UNABLE_TO_DOWNLOAD_LABEL, "Unable to download label for package " + shippingPackageCode);
                } finally {
                    FileUtils.safelyCloseOutputStream(fos);
                }
            } else {
                context.addError(WsResponseCode.UNABLE_TO_DOWNLOAD_LABEL, "Unable to download label for package " + shippingPackageCode);
            }
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    @Override
    @Transactional
    public GetShippingPackageShipmentDetailsResponse getShippingPackageShipmentDetails(GetShippingPackageShipmentDetailsRequest request) {
        GetShippingPackageShipmentDetailsResponse response = new GetShippingPackageShipmentDetailsResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            ShippingPackage shippingPackage = shippingDao.getShippingPackageByCode(request.getShippingPackageCode());
            if (shippingPackage == null) {
                context.addError(WsResponseCode.INVALID_SHIPPING_PACKAGE_CODE, "Invalif shipping package code " + request.getShippingPackageCode());
            } else {
                response.setStatusCode(shippingPackage.getStatusCode());
                if (StringUtils.equalsAny(shippingPackage.getStatusCode(), ShippingPackage.StatusCode.READY_TO_SHIP.name(), ShippingPackage.StatusCode.MANIFESTED.name(),
                        ShippingPackage.StatusCode.DISPATCHED.name(), ShippingPackage.StatusCode.DELIVERED.name(), ShippingPackage.StatusCode.RETURN_EXPECTED.name(),
                        ShippingPackage.StatusCode.RETURN_ACKNOWLEDGED.name(), ShippingPackage.StatusCode.RETURNED.name(), ShippingPackage.StatusCode.SHIPPED)) {
                    String label = generatePdfLabel(shippingPackage.getCode(), shippingPackage.getShippingLabelLink(), context);
                    if (!context.hasErrors()) {
                        response.setShippingProviderCode(shippingPackage.getShippingProviderCode());
                        response.setTrackingNumber(shippingPackage.getTrackingNumber());
                        response.setShippingLabelLink(shippingPackage.getShippingLabelLink());
                        response.setLabel(label);
                    }
                }
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        response.setSuccessful(!context.hasErrors());
        return response;
    }

    @Override
    public MarkShippingPackageReadyForPickupResponse markShippingPackageReadyForPickup(MarkShippingPackageReadyForPickupRequest request) {
        MarkShippingPackageReadyForPickupResponse response = new MarkShippingPackageReadyForPickupResponse();
        ValidationContext context = request.validate();
        ShippingPackage shippingPackage = null;
        if (!context.hasErrors()) {
            shippingPackage = shippingService.getShippingPackageByCode(request.getShippingPackageCode());
            if (shippingPackage == null) {
                context.addError(WsResponseCode.INVALID_SHIPPING_PACKAGE_CODE);
            } else {
                ShippingMethod shippingMethod = ConfigurationManager.getInstance().getConfiguration(ShippingConfiguration.class).getShippingMethodById(
                        shippingPackage.getShippingMethod().getId());
                if (!ShippingMethod.Code.PKP.name().equals(shippingMethod.getCode())) {
                    context.addError(WsResponseCode.INVALID_SHIPPING_PACKAGE_CODE, "Action only applicable for Pickup type packages");
                }
            }
        }
        if (!context.hasErrors()) {
            doMarkShippingPackageReadyForPickup(shippingPackage, response, context);
        } else {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    @Transactional
    @RollbackOnFailure
    @Locks({ @Lock(ns = Namespace.SALE_ORDER, key = "#{#args[0].saleOrder.code}") })
    private MarkShippingPackageReadyForPickupResponse doMarkShippingPackageReadyForPickup(ShippingPackage shippingPackage, MarkShippingPackageReadyForPickupResponse response,
            ValidationContext context) {
        shippingPackage = shippingService.getShippingPackageByCode(shippingPackage.getCode());
        if (!StringUtils.equalsAny(shippingPackage.getStatusCode(), ShippingPackage.StatusCode.PACKED.name())) {
            context.addError(WsResponseCode.INVALID_PACKAGE_STATE);
        } else if (shippingPackage.getShippingProviderCode() != null) {
            context.addError(WsResponseCode.SHIPPING_PROVIDER_ALREADY_ALLOCATED, "Shipping Provider is already allocated");
        } else {
            ShippingProvider shippingProvider = ConfigurationManager.getInstance().getConfiguration(ShippingConfiguration.class).getShippingProviderByCode(
                    ShippingProvider.Code.SELF_PICKUP.getCode());
            if (shippingProvider == null) {
                context.addError(WsResponseCode.INVALID_WAREHOUSE_CONFIGURATION, "PICKUP shipping provider not configured");
            } else {
                shippingPackage.setShippingProviderCode(shippingProvider.getCode());
                shippingPackage.setShippingProviderName(shippingProvider.getName());
                String trackingNumber = UserContext.current().getFacilityId() == null ? shippingPackage.getCode()
                        : shippingPackage.getCode() + "-" + UserContext.current().getFacilityId();
                shippingPackage.setTrackingNumber(trackingNumber);
                shippingPackage.setStatusCode(ShippingPackage.StatusCode.READY_TO_SHIP.name());
                response.setSuccessful(true);
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    /* (non-Javadoc)
     * @see com.uniware.services.shipping.IShippingProviderService#createInvoiceAndAllocateShippingProvider(com.uniware.core.api.shipping.CreateInvoiceAndAllocateShippingProviderRequest)
     */
    @Override
    public CreateInvoiceAndAllocateShippingProviderResponse createInvoiceAndAllocateShippingProvider(CreateInvoiceAndAllocateShippingProviderRequest request) {
        CreateInvoiceAndAllocateShippingProviderResponse response = new CreateInvoiceAndAllocateShippingProviderResponse();
        ValidationContext context = request.validate();
        response.setErrors(context.getErrors());
        ShippingPackage shippingPackage = shippingService.getShippingPackageByCode(request.getShippingPackageCode());
        if (!context.hasErrors()) {
            if (!shippingPackage.getStatusCode().equals(ShippingPackage.StatusCode.PACKED.name())) {
                CreateShippingPackageInvoiceRequest createShippingPackageInvoiceRequest = new CreateShippingPackageInvoiceRequest();
                createShippingPackageInvoiceRequest.setUserId(request.getUserId());
                createShippingPackageInvoiceRequest.setShippingPackageCode(request.getShippingPackageCode());
                createShippingPackageInvoiceRequest.setCommitBlockedInventory(true);
                if (request.getTaxInformation() != null) {
                    Map<String, WsTaxInformation.ProductTax> channelProductIdToTax = new HashMap<>(request.getTaxInformation().getProductTaxes().size());
                    for (WsTaxInformation.ProductTax productTax : request.getTaxInformation().getProductTaxes()) {
                        channelProductIdToTax.put(productTax.getChannelProductId(), productTax);
                    }
                    createShippingPackageInvoiceRequest.setChannelProductIdToTax(channelProductIdToTax);
                }
                CreateShippingPackageInvoiceResponse createShippingPackageInvoiceResponse = iShippingInvoiceService.createShippingPackageInvoice(
                        createShippingPackageInvoiceRequest);
                response.setErrors(createShippingPackageInvoiceResponse.getErrors());
            }
            if (!response.hasErrors()) {
                AllocateShippingProviderRequest allocateShippingProviderRequest = new AllocateShippingProviderRequest();
                allocateShippingProviderRequest.setShippingPackageCode(request.getShippingPackageCode());
                AllocateShippingProviderResponse allocateProviderResponse = allocateShippingProvider(allocateShippingProviderRequest);
                if (!allocateProviderResponse.hasErrors()) {
                    response.setSuccessful(true);
                    Integer invoiceId = shippingService.getShippingPackageByCode(request.getShippingPackageCode()).getInvoice().getId();
                    response.setInvoiceCode(invoiceService.getInvoiceDetailedById(invoiceId).getCode());
                    response.setInvoiceDisplayCode(invoiceService.getInvoiceDetailedById(invoiceId).getDisplayCode());
                    response.setShippingPackageCode(allocateProviderResponse.getShippingPackageCode());
                    response.setShippingProviderCode(allocateProviderResponse.getShippingProviderCode());
                    response.setTrackingNumber(allocateProviderResponse.getTrackingNumber());
                    response.setShippingLabelLink(allocateProviderResponse.getShippingLabelLink());
                } else {
                    response.setErrors(allocateProviderResponse.getErrors());
                }
            }
        }
        return response;
    }

    /* (non-Javadoc)
     * @see com.uniware.services.shipping.IShippingProviderService#createInvoiceAndAllocateShippingProvider(com.uniware.core.api.shipping.CreateInvoiceAndAllocateShippingProviderRequest)
     */
    @Override
    public BulkCreateInvoiceAndAllocateShippingProviderResponse bulkCreateInvoiceAndAllocateShippingProvider(BulkCreateInvoiceAndAllocateShippingProviderRequest request) {
        BulkCreateInvoiceAndAllocateShippingProviderResponse response = new BulkCreateInvoiceAndAllocateShippingProviderResponse();
        ValidationContext context = request.validate();
        response.setErrors(context.getErrors());
        if (!context.hasErrors()) {
            for (String shippingPackageCode : request.getShippingPackageCodes()) {
                CreateInvoiceAndAllocateShippingProviderRequest allocateShippingProviderRequest = new CreateInvoiceAndAllocateShippingProviderRequest();
                allocateShippingProviderRequest.setUserId(request.getUserId());
                allocateShippingProviderRequest.setShippingPackageCode(shippingPackageCode);
                CreateInvoiceAndAllocateShippingProviderResponse allocateShippingProviderResponse;
                try {
                    allocateShippingProviderResponse = createInvoiceAndAllocateShippingProvider(allocateShippingProviderRequest);
                } catch (Throwable e) {
                    LOG.error("Failed to create invoice and allocate shipping provider", e);
                    allocateShippingProviderResponse = new CreateInvoiceAndAllocateShippingProviderResponse();
                    allocateShippingProviderResponse.addError(new WsError(90001, e.getMessage()));
                }
                if (!allocateShippingProviderResponse.isSuccessful()) {
                    response.getFailures().put(shippingPackageCode, allocateShippingProviderResponse.getErrors());
                } else {
                    response.getSuccessfulPackages().add(shippingPackageCode);
                }
            }
            if (response.getFailures().size() == 0) {
                response.setSuccessful(true);
            }
        }
        return response;
    }

    @Override
    @Transactional
    public SearchShippingProviderLocationResponse searchShippingProviderLocation(SearchShippingProviderLocationRequest request) {
        SearchShippingProviderLocationResponse response = new SearchShippingProviderLocationResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            for (ShippingProviderLocation providerLocation : shippingDao.searchShippingProviderLocation(request)) {
                ShippingProviderLocationDTO providerLocationDTO = new ShippingProviderLocationDTO();
                providerLocationDTO.setEnabled(providerLocation.isEnabled());
                providerLocationDTO.setId(providerLocation.getId());
                providerLocationDTO.setPincode(providerLocation.getPincode());
                providerLocationDTO.setPriority(providerLocation.getPriority());
                providerLocationDTO.setRoutingCode(providerLocation.getRoutingCode());
                providerLocationDTO.setShippingProvider(providerLocation.getShippingProvider().getName());
                providerLocationDTO.setShippingProviderCode(providerLocation.getShippingProvider().getCode());
                providerLocationDTO.setShippingMethod(providerLocation.getShippingMethod().getName());
                providerLocationDTO.setShippingMethodCode(providerLocation.getShippingMethod().getCode());
                providerLocationDTO.setCod(providerLocation.getShippingMethod().isCashOnDelivery());
                //                providerLocationDTO.setShippingProviderRegion(providerLocation.getShippingProviderRegion().getName());
                response.getElements().add(providerLocationDTO);
            }

            if (request.getSearchOptions() != null && request.getSearchOptions().isGetCount()) {
                Long count = shippingDao.getShippingProviderLocationCount(request);
                response.setTotalRecords(count);
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    public List<ShippingProviderDTO> lookupProviders(String keyword) {
        List<ShippingProviderDTO> shippingProviders = new ArrayList<ShippingProviderDTO>();
        for (ShippingProvider shippingProvider : shippingDao.lookupShippingProviders(keyword)) {
            ShippingProviderDTO providerDTO = new ShippingProviderDTO();
            providerDTO.setCode(shippingProvider.getCode());
            providerDTO.setName(shippingProvider.getName());
            shippingProviders.add(providerDTO);
        }
        return shippingProviders;
    }

    @Override
    @Transactional
    public GetServiceabilityResponse getServiceability(GetServiceabilityRequest request) {
        GetServiceabilityResponse response = new GetServiceabilityResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            ShippingMethod shippingMethod = shippingService.getShippingMethodByCode(ShippingMethod.Code.STD.name(), request.getCashOnDelivery());
            Set<Integer> facilityIds = getServiceableByFacilities(shippingMethod.getId(), request.getPincode());
            if (!facilityIds.isEmpty()) {
                List<String> facilityCodes = new ArrayList<String>();
                for (Integer facilityId : facilityIds) {
                    Facility facility = CacheManager.getInstance().getCache(FacilityCache.class).getFacilityById(facilityId);
                    if (facility != null) {
                        facilityCodes.add(facility.getCode());
                    }
                }
                response.setFacilityCodes(facilityCodes);
                response.setSuccessful(true);
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    public List<ShippingProviderMethod> getShippingProviderMethods(int shippingProviderId) {
        return shippingDao.getShippingProviderMethods(shippingProviderId);
    }

    @Override
    @Transactional
    public List<ShippingProviderMethod> getShippingProviderMethods(int shippingProviderId, int facilityId) {
        return shippingDao.getShippingProviderMethods(shippingProviderId, facilityId);
    }

    @Override
    @Transactional(readOnly = true)
    public Set<Integer> getServiceableByFacilities(Integer shippingMethodId, String pincode) {
        return shippingDao.getServiceableByFacilities(shippingMethodId, pincode);
    }

    @Override
    @Transactional
    public List<ShippingProviderAllocationRule> getShippingProviderAllocationRules() {
        return shippingDao.getShippingProviderAllocationRules();
    }

    @Override
    @Transactional
    public List<ShippingProviderMethod> getShippingProviderMethods() {
        return shippingDao.getShippingProviderMethods();
    }

    @Override
    public ShippingZoneVO getShippingInfobyPincode(String pincode) {
        return shippingMao.getShippingZoneVO(pincode);
    }

    @Override
    public List<ShippingZoneVO> getAllShippingZones() {
        return shippingMao.getAllShippingZones();
    }

    /*@Override
    public GetShippingProviderParametersResponse getShippingProviderParameters(GetShippingProviderParametersRequest request) {
        GetShippingProviderParametersResponse response = new GetShippingProviderParametersResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            ShippingProvider shippingProvider = ConfigurationManager.getInstance().getConfiguration(ShippingConfiguration.class).getShippingProviderByCode(
                    request.getShippingProviderCode());
            if (shippingProvider == null) {
                context.addError(WsResponseCode.INVALID_SHIPPING_PROVIDER_CODE);
            } else {
                List<ShippingProviderParameterDTO> shippingProviderParameters = new ArrayList<>(shippingProvider.getShippingProviderParameters().size());
                for (ShippingProviderParameter shippingProviderParameter : shippingProvider.getShippingProviderParameters()) {
                    shippingProviderParameters.add(new ShippingProviderParameterDTO(shippingProviderParameter));
                }
                response.setShippingProviderCode(shippingProvider.getCode());
                response.setShippingProviderParameters(shippingProviderParameters);
                response.setSuccessful(true);
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        return response;
    }*/
}
