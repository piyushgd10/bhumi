package com.uniware.services.shipping;

import org.springframework.transaction.annotation.Transactional;

import com.uniware.core.api.invoice.BulkCreateInvoiceRequest;
import com.uniware.core.api.invoice.BulkCreateInvoiceResponse;
import com.uniware.core.api.invoice.ChangeInvoiceCodeRequest;
import com.uniware.core.api.invoice.ChangeInvoiceCodeResponse;
import com.uniware.core.api.invoice.CreateInvoiceWithDetailsRequest;
import com.uniware.core.api.invoice.CreateInvoiceWithDetailsResponse;
import com.uniware.core.api.invoice.CreateShippingPackageInvoiceRequest;
import com.uniware.core.api.invoice.CreateShippingPackageInvoiceResponse;
import com.uniware.core.api.invoice.CreateShippingPackageReverseInvoiceRequest;
import com.uniware.core.api.invoice.CreateShippingPackageReverseInvoiceResponse;
import com.uniware.core.api.invoice.GetAllInvoicesDetailsRequest;
import com.uniware.core.api.invoice.GetAllInvoicesDetailsResponse;
import com.uniware.core.api.invoice.GetInvoiceDetailsByCodeRequest;
import com.uniware.core.api.invoice.GetInvoiceDetailsByCodeResponse;
import com.uniware.core.api.invoice.GetInvoiceDetailsRequest;
import com.uniware.core.api.invoice.GetInvoiceDetailsResponse;
import com.uniware.core.api.invoice.GetInvoiceLabelRequest;
import com.uniware.core.api.invoice.GetInvoiceLabelResponse;
import com.uniware.core.api.invoice.UpdateInvoiceRequest;
import com.uniware.core.api.invoice.UpdateInvoiceResponse;

/**
 * Created by Sagar Sahni on 31/05/17.
 */
public interface IShippingInvoiceService {
    CreateShippingPackageInvoiceResponse createShippingPackageInvoice(CreateShippingPackageInvoiceRequest createShippingPackageInvoiceRequest);

    BulkCreateInvoiceResponse bulkCreateInvoice(BulkCreateInvoiceRequest request);

    GetInvoiceDetailsResponse getInvoiceDetails(GetInvoiceDetailsRequest request);

    @Transactional(readOnly = true) GetAllInvoicesDetailsResponse getAllShippingPackageInvoicesDetails(GetAllInvoicesDetailsRequest request);

    CreateInvoiceWithDetailsResponse createInvoiceWithDetails(CreateInvoiceWithDetailsRequest request);

    GetInvoiceDetailsByCodeResponse getInvoiceDetailsByCode(GetInvoiceDetailsByCodeRequest request);

    CreateShippingPackageReverseInvoiceResponse createShippingPackageReverseInvoice(CreateShippingPackageReverseInvoiceRequest request);

    UpdateInvoiceResponse updateInvoice(UpdateInvoiceRequest request);

    ChangeInvoiceCodeResponse changeInvoiceCode(ChangeInvoiceCodeRequest request);

    GetInvoiceLabelResponse getInvoiceLabel(GetInvoiceLabelRequest request);

    String getInvoiceHtmlByShippingPackageCode(String shippingPackageCode);

    String getInvoiceHtml(String shippingPackageCode);
}
