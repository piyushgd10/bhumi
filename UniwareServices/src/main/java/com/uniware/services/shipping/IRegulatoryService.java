/*
 *  Copyright 2013 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 28-Jun-2013
 *  @author sunny
 */
package com.uniware.services.shipping;

import com.unifier.core.template.Template;
import com.uniware.core.api.packer.RegulatoryFormDTO;
import com.uniware.core.api.shipping.GetApplicableRegulatoryFormsRequest;
import com.uniware.core.api.shipping.GetApplicableRegulatoryFormsResponse;
import com.uniware.core.entity.RegulatoryForm;
import com.uniware.core.entity.ShippingPackage;

import java.util.List;

public interface IRegulatoryService {

    List<RegulatoryFormDTO> getApplicableRegulatoryForms(ShippingPackage shippingPackage);

    String getStateRegulatoryFormHtml(String shippingPackageCode, Template template);

    List<RegulatoryForm> getAllRegulatoryForms();

    GetApplicableRegulatoryFormsResponse getApplicableRegulatoryForms(GetApplicableRegulatoryFormsRequest request);

}
