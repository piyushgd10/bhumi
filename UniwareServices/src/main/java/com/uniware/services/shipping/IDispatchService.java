/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 16, 2011
 *  @author singla
 */
package com.uniware.services.shipping;

import com.unifier.core.template.Template;
import com.unifier.core.utils.DateUtils.DateRange;
import com.uniware.core.api.shipping.AddShippingPackageToManifestRequest;
import com.uniware.core.api.shipping.AddShippingPackageToManifestResponse;
import com.uniware.core.api.shipping.AddShippingPackagesToManifestRequest;
import com.uniware.core.api.shipping.AddShippingPackagesToManifestResponse;
import com.uniware.core.api.shipping.AutoDispatchShippingPackageRequest;
import com.uniware.core.api.shipping.AutoDispatchShippingPackageResponse;
import com.uniware.core.api.shipping.CheckIfShippingManifestClosingRequest;
import com.uniware.core.api.shipping.CheckIfShippingManifestClosingResponse;
import com.uniware.core.api.shipping.CloseShippingManifestRequest;
import com.uniware.core.api.shipping.CloseShippingManifestResponse;
import com.uniware.core.api.shipping.CreateShippingManifestRequest;
import com.uniware.core.api.shipping.CreateShippingManifestResponse;
import com.uniware.core.api.shipping.DiscardShippingManifestRequest;
import com.uniware.core.api.shipping.DiscardShippingManifestResponse;
import com.uniware.core.api.shipping.DispatchSaleOrderItemsRequest;
import com.uniware.core.api.shipping.DispatchSaleOrderItemsResponse;
import com.uniware.core.api.shipping.DispatchShippingPackageRequest;
import com.uniware.core.api.shipping.DispatchShippingPackageResponse;
import com.uniware.core.api.shipping.EditShippingManifestMetadataRequest;
import com.uniware.core.api.shipping.EditShippingManifestMetadataResponse;
import com.uniware.core.api.shipping.ForceDispatchShippingPackageRequest;
import com.uniware.core.api.shipping.ForceDispatchShippingPackageResponse;
import com.uniware.core.api.shipping.GetChannelsForManifestRequest;
import com.uniware.core.api.shipping.GetChannelsForManifestResponse;
import com.uniware.core.api.shipping.GetCurrentChannelShippingManifestRequest;
import com.uniware.core.api.shipping.GetCurrentChannelShippingManifestResponse;
import com.uniware.core.api.shipping.GetEligibleShippingProvidersForManifestRequest;
import com.uniware.core.api.shipping.GetEligibleShippingProvidersForManifestResponse;
import com.uniware.core.api.shipping.GetManifestShippingProvidersSummaryRequest;
import com.uniware.core.api.shipping.GetManifestShippingProvidersSummaryResponse;
import com.uniware.core.api.shipping.GetManifestSummaryRequest;
import com.uniware.core.api.shipping.GetManifestSummaryResponse;
import com.uniware.core.api.shipping.GetShippingManifestItemDetailsRequest;
import com.uniware.core.api.shipping.GetShippingManifestItemDetailsResponse;
import com.uniware.core.api.shipping.GetShippingManifestRequest;
import com.uniware.core.api.shipping.GetShippingManifestResponse;
import com.uniware.core.api.shipping.MarkSaleOrderItemsDeliveredRequest;
import com.uniware.core.api.shipping.MarkSaleOrderItemsDeliveredResponse;
import com.uniware.core.api.shipping.MarkShippingPackageDeliveredRequest;
import com.uniware.core.api.shipping.MarkShippingPackageDeliveredResponse;
import com.uniware.core.api.shipping.RemoveManifestItemRequest;
import com.uniware.core.api.shipping.RemoveManifestItemResponse;
import com.uniware.core.api.shipping.ResendPODCodeRequest;
import com.uniware.core.api.shipping.ResendPODCodeResponse;
import com.uniware.core.api.shipping.SearchManifestRequest;
import com.uniware.core.api.shipping.SearchManifestResponse;
import com.uniware.core.api.shipping.VerifySaleOrderItemsPODCodeRequest;
import com.uniware.core.api.shipping.VerifySaleOrderItemsPODCodeResponse;
import com.uniware.core.api.shipping.VerifyShippingPackagePODCodeRequest;
import com.uniware.core.api.shipping.VerifyShippingPackagePODCodeResponse;
import com.uniware.core.entity.ShipmentTracking;
import com.uniware.core.entity.ShippingManifest;
import com.uniware.core.entity.ShippingManifestStatus;
import com.uniware.core.entity.ShippingPackage;

import java.util.List;

/**
 * @author singla
 */
public interface IDispatchService {

    CreateShippingManifestResponse createShippingManifest(CreateShippingManifestRequest request);

    ShippingManifest addShippingManifest(ShippingManifest shippingManifest);

    AddShippingPackageToManifestResponse addShippingPackageToManifest(AddShippingPackageToManifestRequest request);

    AddShippingPackagesToManifestResponse addShippingPackagesToManifest(AddShippingPackagesToManifestRequest request);

    CloseShippingManifestResponse closeShippingManifest(CloseShippingManifestRequest request);

    String getShippingManifestOutput(String manifestCode, Template template);

    ShippingManifest getShippingManifestById(Integer shippingManifestId);

    SearchManifestResponse searchManifest(SearchManifestRequest request);

    DiscardShippingManifestResponse discardShippingManifest(DiscardShippingManifestRequest request);

    List<ShippingPackage> getShippingPackagesDispatchedInDateRange(DateRange range);

    String getManifestHtml(String manifestCode, Template template);

    List<ShippingManifestStatus> getShippingManifestStatuses();

    ShippingManifest getShippingManifestDetailedByCode(String shippingManifestCode);

    RemoveManifestItemResponse removeManifestItem(RemoveManifestItemRequest request);

    ShippingManifest updateShippingManifest(ShippingManifest shippingManifest);

    MarkShippingPackageDeliveredResponse markShippingPackageDelivered(MarkShippingPackageDeliveredRequest request);

    MarkSaleOrderItemsDeliveredResponse markSaleOrderItemsDelivered(MarkSaleOrderItemsDeliveredRequest request);

    ShipmentTracking addShipmentTracking(ShipmentTracking shipmentTracking);

    ShippingManifest getShippingManifestByCode(String shippingManifestCode);

    List<ShippingPackage> getEligibleShippingPackagesForShippingManifest(ShippingManifest shippingManifest);

    DispatchShippingPackageResponse dispatchReadyToShipShippingPackage(DispatchShippingPackageRequest request);

    DispatchSaleOrderItemsResponse dispatchSaleOrderItems(DispatchSaleOrderItemsRequest request);

    GetEligibleShippingProvidersForManifestResponse getShippingProvidersForManifest(GetEligibleShippingProvidersForManifestRequest request);

    GetChannelsForManifestResponse getChannelsForManifest(GetChannelsForManifestRequest request);

    CheckIfShippingManifestClosingResponse checkIfShippingManifestClosing(CheckIfShippingManifestClosingRequest request);

    ForceDispatchShippingPackageResponse forceDispatchShippingPackage(ForceDispatchShippingPackageRequest request);

    GetCurrentChannelShippingManifestResponse getCurrentChannelShippingManifest(GetCurrentChannelShippingManifestRequest request);

    GetManifestSummaryResponse fetchShippingManifestSummary(GetManifestSummaryRequest request);

    GetManifestShippingProvidersSummaryResponse fetchManifestShippingProvidersSummary(GetManifestShippingProvidersSummaryRequest request);

    GetShippingManifestItemDetailsResponse getShippingManifestItemDetails(GetShippingManifestItemDetailsRequest request);

    GetShippingManifestResponse getShippingManifest(GetShippingManifestRequest request);

    EditShippingManifestMetadataResponse editShipingManifestMetadata(EditShippingManifestMetadataRequest request);

    ResendPODCodeResponse resendPODCode(ResendPODCodeRequest request);

    AutoDispatchShippingPackageResponse autoDispatchShippingPackage(AutoDispatchShippingPackageRequest request);

    VerifyShippingPackagePODCodeResponse verifyShippingPackagePODCode(VerifyShippingPackagePODCodeRequest request);

    VerifySaleOrderItemsPODCodeResponse verifySaleOrderItemsPODCode(VerifySaleOrderItemsPODCodeRequest request);

    String generateManifestPdfLabel(String shippingManifestCode, String manifestLabelHtml);
}
