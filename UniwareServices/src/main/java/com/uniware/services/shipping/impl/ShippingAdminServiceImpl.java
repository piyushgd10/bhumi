/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 *  @version     1.0, 01-Mar-2012
 *  @author vibhu
 */
package com.uniware.services.shipping.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.StringTokenizer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.unifier.core.api.validation.ResponseCode;
import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.api.validation.WsError;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.StringUtils;
import com.unifier.scraper.sl.runtime.IScriptProvider;
import com.unifier.scraper.sl.runtime.ScraperScript;
import com.unifier.scraper.sl.runtime.ScriptExecutionContext;
import com.unifier.services.aspect.MarkDirty;
import com.uniware.core.api.admin.shipping.AddAwbNumberRequest;
import com.uniware.core.api.admin.shipping.AddAwbNumberResponse;
import com.uniware.core.api.admin.shipping.AddShippingProviderMethodsRequest;
import com.uniware.core.api.admin.shipping.AddShippingProviderMethodsResponse;
import com.uniware.core.api.admin.shipping.CreateProviderAllocationRuleRequest;
import com.uniware.core.api.admin.shipping.CreateProviderAllocationRuleResponse;
import com.uniware.core.api.admin.shipping.DeleteAwbNumberRequest;
import com.uniware.core.api.admin.shipping.DeleteAwbNumberResponse;
import com.uniware.core.api.admin.shipping.DeleteProviderAllocationRuleRequest;
import com.uniware.core.api.admin.shipping.DeleteProviderAllocationRuleResponse;
import com.uniware.core.api.admin.shipping.EditProviderAllocationRuleRequest;
import com.uniware.core.api.admin.shipping.EditProviderAllocationRuleResponse;
import com.uniware.core.api.admin.shipping.EditShippingProviderMethodRequest;
import com.uniware.core.api.admin.shipping.EditShippingProviderMethodResponse;
import com.uniware.core.api.admin.shipping.EditShippingServiceabilityRequest;
import com.uniware.core.api.admin.shipping.EditShippingServiceabilityResposne;
import com.uniware.core.api.admin.shipping.GetShippingProviderRequest;
import com.uniware.core.api.admin.shipping.GetShippingProviderResponse;
import com.uniware.core.api.admin.shipping.GetShippingProvidersResponse;
import com.uniware.core.api.admin.shipping.MethodDTO;
import com.uniware.core.api.admin.shipping.ReorderProviderAllocationRulesRequest;
import com.uniware.core.api.admin.shipping.ReorderProviderAllocationRulesResponse;
import com.uniware.core.api.admin.shipping.SearchAwbNumberRequest;
import com.uniware.core.api.admin.shipping.SearchAwbNumberResponse;
import com.uniware.core.api.admin.shipping.SearchAwbNumberResponse.ShippingProviderMethodAwbDTO;
import com.uniware.core.api.admin.shipping.ShippingProviderAllocationRuleDTO;
import com.uniware.core.api.admin.shipping.ShippingProviderDTO;
import com.uniware.core.api.admin.shipping.ShippingProviderDetailDTO;
import com.uniware.core.api.admin.shipping.ShippingProviderMethodDTO;
import com.uniware.core.api.admin.shipping.ShippingProviderSearchDTO;
import com.uniware.core.api.admin.shipping.ViewAwbNumberRequest;
import com.uniware.core.api.admin.shipping.ViewAwbNumberResponse;
import com.uniware.core.api.admin.shipping.WsProviderAllocationRule;
import com.uniware.core.api.model.WSShippingMethod;
import com.uniware.core.api.model.WsShippingProviderConnectorParameter;
import com.uniware.core.api.shipping.AddShippingProviderConnectorRequest;
import com.uniware.core.api.shipping.AddShippingProviderConnectorResponse;
import com.uniware.core.api.shipping.AddShippingProviderRequest;
import com.uniware.core.api.shipping.AddShippingProviderResponse;
import com.uniware.core.api.shipping.EditShippingProviderRequest;
import com.uniware.core.api.shipping.EditShippingProviderResponse;
import com.uniware.core.api.shipping.GetShippingProviderParametersRequest;
import com.uniware.core.api.shipping.GetShippingProviderParametersResponse;
import com.uniware.core.api.shipping.VerifyandSyncShippingConnectorParamsResponse;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.api.warehouse.CreateShippingPackageTypeRequest;
import com.uniware.core.api.warehouse.CreateShippingPackageTypeResponse;
import com.uniware.core.api.warehouse.EditShippingPackageTypeRequest;
import com.uniware.core.api.warehouse.EditShippingPackageTypeResponse;
import com.uniware.core.api.warehouse.ShippingPackageTypeDTO;
import com.uniware.core.cache.FacilityCache;
import com.uniware.core.entity.Facility;
import com.uniware.core.entity.ShippingMethod;
import com.uniware.core.entity.ShippingPackageType;
import com.uniware.core.entity.ShippingProvider;
import com.uniware.core.entity.ShippingProvider.ShippingServiceablity;
import com.uniware.core.entity.ShippingProvider.SyncStatus;
import com.uniware.core.entity.ShippingProviderAllocationRule;
import com.uniware.core.entity.ShippingProviderAllocationRule.AllocationCriteria;
import com.uniware.core.entity.ShippingProviderConnector;
import com.uniware.core.entity.ShippingProviderConnector.Status;
import com.uniware.core.entity.ShippingProviderConnectorParameter;
import com.uniware.core.entity.ShippingProviderMethod;
import com.uniware.core.entity.ShippingProviderMethod.TrackingNumberGeneration;
import com.uniware.core.entity.ShippingProviderSource;
import com.uniware.core.entity.ShippingProviderTrackingNumber;
import com.uniware.core.entity.ShippingSourceColor;
import com.uniware.core.entity.ShippingSourceConnector;
import com.uniware.core.entity.ShippingSourceConnectorParameter;
import com.uniware.core.entity.SourceConnectorParameter;
import com.uniware.core.utils.UserContext;
import com.uniware.dao.shipping.IShippingAdminDao;
import com.uniware.dao.shipping.IShippingAdminMao;
import com.uniware.dao.shipping.IShippingDao;
import com.uniware.dao.warehouse.IFacilityDao;
import com.uniware.services.cache.ScriptVersionedCache;
import com.uniware.services.configuration.ShippingConfiguration;
import com.uniware.services.configuration.ShippingFacilityLevelConfiguration;
import com.uniware.services.configuration.ShippingSourceConfiguration;
import com.uniware.services.shipping.IShippingAdminService;

@Service
public class ShippingAdminServiceImpl implements IShippingAdminService {

    @Autowired
    private IShippingAdminDao   shippingAdminDao;

    @Autowired
    private IShippingAdminMao   shippingAdminMao;

    @Autowired
    private IShippingDao        shippingDao;

    @Autowired
    private IFacilityDao        facilityDao;

    private static final int    MAX_TRACKING_UPLOAD_LIMIT = 50000;

    private static final String DEFAULT_SHORT_NAME        = "DE";

    private static Logger       LOG                       = LoggerFactory.getLogger(ShippingAdminServiceImpl.class);

    @Override
    @Transactional
    public List<ShippingPackageType> getShippingPackageTypes() {
        return shippingAdminDao.getShippingPackageTypes();
    }

    @Override
    @MarkDirty(values = { ShippingFacilityLevelConfiguration.class })
    @Transactional
    public CreateShippingPackageTypeResponse createShippingPackageType(CreateShippingPackageTypeRequest request) {
        CreateShippingPackageTypeResponse response = new CreateShippingPackageTypeResponse();
        ShippingPackageType sPackageType = null;
        ValidationContext context = request.validate();
        if (context.hasErrors()) {
            response.setSuccessful(false);
            response.setErrors(context.getErrors());
        } else {
            if (StringUtils.isNotEmpty(request.getCode())) {
                sPackageType = shippingAdminDao.getShippingPackageTypeByCode(request.getCode());
            }
            if (sPackageType != null) {
                context.addError(WsResponseCode.DUPLICATE_CODE, "Shipping package type code already exists:" + request.getCode());
                response.setSuccessful(false);
                response.setErrors(context.getErrors());
            } else {
                sPackageType = new ShippingPackageType();
                sPackageType.setCode(request.getCode().toUpperCase());
                sPackageType.setBoxLength(request.getBoxLength());
                sPackageType.setBoxWidth(request.getBoxWidth());
                sPackageType.setBoxHeight(request.getBoxHeight());
                sPackageType.setBoxWeight(request.getBoxWeight());
                sPackageType.setPackingCost(request.getPackingCost());
                sPackageType.setEnabled(true);
                sPackageType.setEditable(true);
                sPackageType = shippingAdminDao.createShippingPackageType(sPackageType);

                response.setShippingPackageTypeDTO(new ShippingPackageTypeDTO(sPackageType));
                response.setSuccessful(true);
            }
        }
        return response;
    }

    @Override
    @MarkDirty(values = { ShippingFacilityLevelConfiguration.class })
    @Transactional
    public EditShippingPackageTypeResponse editShippingPackageType(EditShippingPackageTypeRequest request) {
        EditShippingPackageTypeResponse response = new EditShippingPackageTypeResponse();
        ValidationContext context = request.validate();
        ShippingPackageType sPackageType = null;
        if (context.hasErrors()) {
            response.setSuccessful(false);
            response.setErrors(context.getErrors());
        } else {
            if (StringUtils.isNotEmpty(request.getCode())) {
                sPackageType = shippingAdminDao.getShippingPackageTypeByCode(request.getCode());
            }
            if (sPackageType == null) {
                context.addError(WsResponseCode.INVALID_CODE, "Shipping package type does not exist" + request.getCode());
                response.setSuccessful(false);
                response.setErrors(context.getErrors());
            } else {
                sPackageType.setCode(request.getCode());
                sPackageType.setEnabled(request.isEnabled());
                sPackageType.setBoxLength(request.getBoxLength());
                sPackageType.setBoxWidth(request.getBoxWidth());
                sPackageType.setBoxHeight(request.getBoxHeight());
                sPackageType.setBoxWeight(request.getBoxWeight());
                sPackageType.setPackingCost(request.getPackingCost());
                sPackageType = shippingAdminDao.updateShippingPackageType(sPackageType);

                response.setShippingPackageTypeDTO(new ShippingPackageTypeDTO(sPackageType));
                response.setSuccessful(true);
            }
        }
        return response;
    }

    @Override
    @Transactional
    public List<ShippingProvider> getShippingProviders(boolean fetchHidden) {
        return shippingAdminDao.getShippingProviders(fetchHidden);
    }

    @Override
    @Transactional
    public AddShippingProviderMethodsResponse addShippingProviderMethods(AddShippingProviderMethodsRequest request) {
        AddShippingProviderMethodsResponse response = new AddShippingProviderMethodsResponse();
        ValidationContext context = request.validate();
        if (context.hasErrors()) {
            response.setSuccessful(false);
            response.setErrors(context.getErrors());
        } else {
            ShippingConfiguration shippingConfiguration = ConfigurationManager.getInstance().getConfiguration(ShippingConfiguration.class);
            for (MethodDTO methodDto : request.getShippingMethods()) {
                ShippingProviderMethod shippingProviderMethod = shippingDao.getShippingProviderMethod(methodDto.getProviderCode(), methodDto.getName());
                if (shippingProviderMethod != null) {
                    context.addError(WsResponseCode.INVALID_CODE, "Shipping Provider Method already exists with code \"" + methodDto.getProviderCode() + "\"");
                    response.setErrors(context.getErrors());
                } else {
                    shippingProviderMethod = new ShippingProviderMethod();
                    shippingProviderMethod.setShippingMethod(shippingConfiguration.getShippingMethod(methodDto.getName()));
                    shippingProviderMethod.setShippingProvider(getShippingProviderByCode(methodDto.getProviderCode()));
                    shippingProviderMethod.setEnabled(methodDto.isEnabled());
                    shippingProviderMethod.setCreated(DateUtils.getCurrentTime());
                    shippingProviderMethod.setTrackingNumberGeneration(methodDto.getTrackingNumberGeneration());
                    shippingProviderMethod = shippingAdminDao.updateShippingProviderMethod(shippingProviderMethod);
                }
            }
            response.setSuccessful(true);
        }
        return response;
    }

    @Override
    @Transactional
    public EditShippingServiceabilityResposne editShippingProviderServiceability(EditShippingServiceabilityRequest request) {
        EditShippingServiceabilityResposne response = new EditShippingServiceabilityResposne();
        ValidationContext context = request.validate();
        ShippingProvider shippingProvider = null;
        if (!context.hasErrors()) {
            shippingProvider = ConfigurationManager.getInstance().getConfiguration(ShippingConfiguration.class).getShippingProviderByCode(request.getShippingProviderCode());
            if (shippingProvider == null) {
                context.addError(WsResponseCode.INVALID_SHIPPING_PROVIDER_CODE, WsResponseCode.INVALID_SHIPPING_PROVIDER_CODE.message() + ":" + request.getShippingProviderCode());
            }
        }
        if (!context.hasErrors()) {
            shippingProvider.setShippingServiceablity(ShippingServiceablity.valueOf(request.getServiceabilityCode()));
            shippingAdminDao.updateShippingProvider(shippingProvider);
            response.setLocationDeleteCount(shippingDao.deleteLocationsforShippingProvider(shippingProvider.getId()));
            response.setSuccessful(true);
        }
        response.addErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    public GetShippingProviderResponse getShippingProviderByCode(GetShippingProviderRequest request) {
        GetShippingProviderResponse response = new GetShippingProviderResponse();
        ValidationContext context = request.validate();
        if (context.hasErrors()) {
            response.setSuccessful(false);
        } else {
            ShippingProviderDetailDTO detailDTO = getShippingProviderDTOByCode(request.getCode());
            response.setDetailDTO(detailDTO);
            response.setSuccessful(true);
        }
        return response;
    }

    @Override
    @Transactional
    public ShippingProvider getShippingProviderByCode(String providerCode) {
        return shippingAdminDao.getShippingProviderByCode(providerCode);
    }

    @Override
    @Transactional
    public List<ShippingProviderDetailDTO> getAllShippingProvidersDetailDTO() {
        List<ShippingProviderDetailDTO> detailDTOs = new ArrayList<>();
        List<ShippingProvider> shippingProviders = shippingAdminDao.getShippingProviders(false);
        ShippingSourceConfiguration sourceConfiguration = ConfigurationManager.getInstance().getConfiguration(ShippingSourceConfiguration.class);
        for (ShippingProvider shippingProvider : shippingProviders) {
            ShippingProviderSource providerSource = sourceConfiguration.getShippingSourceByCode(shippingProvider.getShippingProviderSourceCode());
            ShippingProviderDetailDTO detailDTO = new ShippingProviderDetailDTO(shippingProvider, providerSource);
            List<ShippingProviderMethod> shippingProviderMethods = shippingDao.getShippingProviderMethods(detailDTO.getId());
            for (ShippingProviderMethod providerMethod : shippingProviderMethods) {
                detailDTO.addShippingMethod(getShippingProviderMethodDTO(providerMethod));
            }
            detailDTOs.add(detailDTO);
        }
        return detailDTOs;
    }

    /* (non-Javadoc)
     * @see com.uniware.services.shipping.IShippingAdminService#getShippingProviderDTOByCode(java.lang.String)
     */
    @Transactional
    public ShippingProviderDetailDTO getShippingProviderDTOByCode(String shippingProviderCode) {
        ShippingProvider provider = getShippingProviderByCode(shippingProviderCode);
        ShippingProviderSource providerSource = ConfigurationManager.getInstance().getConfiguration(ShippingSourceConfiguration.class).getShippingSourceByCode(
                provider.getShippingProviderSourceCode());
        ShippingProviderDetailDTO detailDTO = new ShippingProviderDetailDTO(provider, providerSource);
        List<ShippingProviderMethod> shippingProviderMethods = shippingDao.getShippingProviderMethods(detailDTO.getId());
        for (ShippingProviderMethod shippingProviderMethod : shippingProviderMethods) {
            detailDTO.addShippingMethod(getShippingProviderMethodDTO(shippingProviderMethod));
        }
        return detailDTO;
    }

    @Override
    @Transactional
    public ShippingProviderMethod getShippingProviderMethod(String shippingProviderCode, String shippingMethodName) {
        return shippingDao.getShippingProviderMethod(shippingProviderCode, shippingMethodName);
    }

    @Override
    @MarkDirty(values = { ShippingFacilityLevelConfiguration.class })
    @Transactional
    public EditShippingProviderMethodResponse editShippingProviderMethod(EditShippingProviderMethodRequest request) {
        EditShippingProviderMethodResponse response = new EditShippingProviderMethodResponse();
        ValidationContext context = request.validate();
        if (context.hasErrors()) {
            response.setSuccessful(false);
            response.setErrors(context.getErrors());
        } else {
            for (MethodDTO methodDto : request.getShippingMethods()) {
                ShippingProviderMethod shippingProviderMethod = shippingDao.getShippingProviderMethod(methodDto.getProviderCode(), methodDto.getName());
                if (shippingProviderMethod == null) {
                    context.addError(WsResponseCode.INVALID_CODE, "No Method exists with code \"" + methodDto.getProviderCode() + "\"");
                    response.setSuccessful(false);
                    response.setErrors(context.getErrors());
                } else {
                    shippingProviderMethod.setEnabled(methodDto.isEnabled());
                    shippingProviderMethod.setTrackingNumberGeneration(methodDto.getTrackingNumberGeneration());
                    shippingProviderMethod = shippingAdminDao.updateShippingProviderMethod(shippingProviderMethod);
                }
            }
            response.setSuccessful(true);
        }
        return response;
    }

    @Override
    @Transactional
    public List<ShippingProviderDTO> getEnabledShippingProviders() {
        List<ShippingProviderDTO> providerDTOs = new ArrayList<ShippingProviderDTO>();
        ShippingSourceConfiguration sourceConfiguration = ConfigurationManager.getInstance().getConfiguration(ShippingSourceConfiguration.class);
        for (ShippingProvider provider : shippingAdminDao.getShippingProviders(false)) {
            if (provider.isEnabled()) {
                ShippingProviderSource providerSource = sourceConfiguration.getShippingSourceByCode(provider.getShippingProviderSourceCode());
                ShippingProviderDTO providerDTO = new ShippingProviderDTO(provider, providerSource);
                providerDTOs.add(providerDTO);
            }
        }
        return providerDTOs;
    }

    @Transactional
    private ShippingProviderMethodDTO getShippingProviderMethodDTO(ShippingProviderMethod spMethod) {
        ShippingProviderMethodDTO method = new ShippingProviderMethodDTO();
        method.setId(spMethod.getId());
        method.setEnabled(spMethod.isEnabled());
        method.setTrackingNumberGeneration(spMethod.getTrackingNumberGeneration());
        if (ShippingProviderMethod.TrackingNumberGeneration.LIST.name().equals(method.getTrackingNumberGeneration())) {
            method.setAvailableAWBs((shippingAdminDao.getAvailableAwbNumbers(spMethod.getShippingProvider().getId(), spMethod.getShippingMethod().getId(), false).intValue()));
        } else if (ShippingProviderMethod.TrackingNumberGeneration.GLOBAL_LIST.name().equals(method.getTrackingNumberGeneration())) {
            method.setAvailableAWBs((shippingAdminDao.getAvailableAwbNumbers(spMethod.getShippingProvider().getId(), spMethod.getShippingMethod().getId(), true).intValue()));
        }
        ShippingMethod sMethod = spMethod.getShippingMethod();
        method.setCOD(sMethod.isCashOnDelivery());
        method.setName(sMethod.getName());
        method.setCode(sMethod.getCode());
        return method;
    }

    @Override
    @Transactional
    public SearchAwbNumberResponse searchAwbNumbers(SearchAwbNumberRequest request) {
        SearchAwbNumberResponse response = new SearchAwbNumberResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            List<ShippingProvider> providers = new ArrayList<ShippingProvider>();
            if (StringUtils.isNotBlank(request.getProviderCode())) {
                providers.add(shippingAdminDao.getShippingProviderByCode(request.getProviderCode()));
            } else {
                providers = getShippingProviders(false);
            }
            for (ShippingProvider provider : providers) {
                if (provider.isEnabled()) {
                    for (ShippingProviderMethod spm : shippingDao.getShippingProviderMethods(provider.getId())) {
                        ShippingProviderMethodAwbDTO awbNumberDTO = new ShippingProviderMethodAwbDTO();
                        awbNumberDTO.setProviderCode(provider.getCode());
                        awbNumberDTO.setProviderName(provider.getName());
                        awbNumberDTO.setType(spm.getTrackingNumberGeneration());
                        awbNumberDTO.setCashOnDelivery(spm.getShippingMethod().isCashOnDelivery());
                        awbNumberDTO.setMethodName(spm.getShippingMethod().getName());
                        awbNumberDTO.setMethodCode(spm.getShippingMethod().getCode());

                        if (ShippingProviderMethod.TrackingNumberGeneration.LIST.name().equals(awbNumberDTO.getType())) {
                            awbNumberDTO.setAvailableAwbs((shippingAdminDao.getAvailableAwbNumbers(provider.getId(), spm.getShippingMethod().getId(), false).intValue()));
                        } else if (ShippingProviderMethod.TrackingNumberGeneration.GLOBAL_LIST.name().equals(awbNumberDTO.getType())) {
                            awbNumberDTO.setAvailableAwbs((shippingAdminDao.getAvailableAwbNumbers(provider.getId(), spm.getShippingMethod().getId(), true).intValue()));
                        }

                        response.addShippingProviderMethodAwbDTO(awbNumberDTO);
                    }
                    Collections.sort(response.getAwbNumbers(), new Comparator<ShippingProviderMethodAwbDTO>() {
                        private final String listName = ShippingProviderMethod.TrackingNumberGeneration.LIST.name();

                        @Override
                        public int compare(ShippingProviderMethodAwbDTO o1, ShippingProviderMethodAwbDTO o2) {
                            if (listName.equals(o1.getType()) && !listName.equals(o2.getType())) {
                                return -1;
                            }
                            if (!listName.equals(o1.getType()) && listName.equals(o2.getType())) {
                                return 1;
                            }
                            return 0;
                        }

                    });

                }
                response.setSuccessful(true);
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    public AddAwbNumberResponse addAwbNumbers(AddAwbNumberRequest request) {
        AddAwbNumberResponse response = new AddAwbNumberResponse();
        ValidationContext context = request.validate();
        ShippingProvider shippingProvider = null;
        ShippingMethod shippingMethod = null;
        boolean isGlobal = false;
        StringTokenizer st = new StringTokenizer(request.getAwbNumberText(), ",\n");
        if (st.countTokens() > MAX_TRACKING_UPLOAD_LIMIT) {
            context.addError(WsResponseCode.INVALID_TRACKING_NUMBER, "Upload limit is set to 5000 tracking number at a time");
        }
        if (!context.hasErrors()) {
            shippingProvider = getShippingProviderByCode(request.getShippingProviderCode());
            if (shippingProvider == null) {
                context.addError(WsResponseCode.INVALID_SHIPPING_PROVIDER_CODE, "Invalid Shipping Provider");
            }
        }

        if (!context.hasErrors()) {
            shippingMethod = ConfigurationManager.getInstance().getConfiguration(ShippingConfiguration.class).getShippingMethod(request.getShippingMethodName());
            if (shippingMethod == null) {
                context.addError(WsResponseCode.INVALID_SHIPPING_METHOD, "Invalid Shipping Method");
            }
        }

        if (!context.hasErrors()) {
            ShippingProviderMethod shippingProviderMethod = getShippingProviderMethod(request.getShippingProviderCode(), request.getShippingMethodName());
            if (shippingProviderMethod == null) {
                context.addError(WsResponseCode.INVALID_SHIPPING_PROVIDER_METHOD, "Invalid Shipping Provider Method");
            } else {
                isGlobal = TrackingNumberGeneration.GLOBAL_LIST.name().equals(shippingProviderMethod.getTrackingNumberGeneration());
            }
        }
        if (!context.hasErrors()) {
            while (st.hasMoreTokens()) {
                String token = st.nextToken().trim();
                if (StringUtils.isNotBlank(token)) {
                    try {
                        addAwbNumberInternal(shippingProvider, shippingMethod, token, isGlobal, response);
                    } catch (DataIntegrityViolationException e) {
                        response.addDuplicateNumberAdded();
                    }
                }
            }
            response.setSuccessful(true);
        }

        response.setErrors(context.getErrors());
        return response;
    }

    @Transactional
    public void addAwbNumberInternal(ShippingProvider shippingProvider, ShippingMethod shippingMethod, String token, boolean isGlobal, AddAwbNumberResponse response) {
        ShippingProviderTrackingNumber sptm = new ShippingProviderTrackingNumber();
        sptm.setShippingProvider(shippingProvider);
        sptm.setShippingMethod(shippingMethod);
        sptm.setTrackingNumber(token.trim());
        sptm.setUsed(false);
        sptm.setCreated(DateUtils.getCurrentTime());
        sptm.setUpdated(DateUtils.getCurrentTime());
        shippingAdminDao.createShippingProviderTrackingNumber(sptm, isGlobal);
        response.addNumberAdded();
    }

    @Override
    @Transactional
    public ViewAwbNumberResponse viewExistingAwbNumbers(ViewAwbNumberRequest request) {
        ViewAwbNumberResponse response = new ViewAwbNumberResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            ShippingProviderMethod spm = shippingDao.getShippingProviderMethod(request.getShippingProviderCode(), request.getShippingMethodName());
            if (spm == null) {
                context.addError(WsResponseCode.INVALID_SHIPPING_PROVIDER_METHOD_ID, "Invalid Shipping Provider Method");
            } else if (TrackingNumberGeneration.GLOBAL_LIST.name().equals(spm.getTrackingNumberGeneration())) {
                response.setAwbNumbers(shippingAdminDao.getShippingProviderTrackingNumbers(spm.getShippingProvider().getId(), spm.getShippingMethod().getId(), true));
                response.setSuccessful(true);
            } else if (TrackingNumberGeneration.LIST.name().equals(spm.getTrackingNumberGeneration())) {
                response.setAwbNumbers(shippingAdminDao.getShippingProviderTrackingNumbers(spm.getShippingProvider().getId(), spm.getShippingMethod().getId(), false));
                response.setSuccessful(true);
            } else {
                context.addError(WsResponseCode.INVALID_SHIPPING_PROVIDER_METHOD_TRACKING_NUMBER_GENERATION, "INVALID_TRACKING_NUMBER_GENERATION");
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    public DeleteAwbNumberResponse deleteAwbNumbers(DeleteAwbNumberRequest request) {
        DeleteAwbNumberResponse response = new DeleteAwbNumberResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            ShippingProviderMethod spm = shippingDao.getShippingProviderMethod(request.getShippingProviderCode(), request.getShippingMethodName());
            if (spm == null) {
                context.addError(WsResponseCode.INVALID_SHIPPING_PROVIDER_METHOD_ID, "Invalid Shipping Provider Method");
            } else if (!StringUtils.equalsAny(spm.getTrackingNumberGeneration(), ShippingProviderMethod.TrackingNumberGeneration.LIST.name(),
                    ShippingProviderMethod.TrackingNumberGeneration.GLOBAL_LIST.name())) {
                context.addError(WsResponseCode.INVALID_SHIPPING_PROVIDER_METHOD_TRACKING_NUMBER_GENERATION, "INVALID_TRACKING_NUMBER_GENERATION");
            } else {
                if (!context.hasErrors()) {
                    if (spm.getTrackingNumberGeneration().equals(ShippingProviderMethod.TrackingNumberGeneration.LIST.name())) {
                        Integer deletedNumber = shippingAdminDao.deleteShippingProviderTrackingNumbersList(spm.getShippingProvider().getId(), spm.getShippingMethod().getId());
                        response.setDeletedNumbers(deletedNumber);
                        response.setSuccessful(true);
                    } else {
                        Integer deletedNumber = shippingAdminDao.deleteShippingProviderTrackingNumbersGlobal(spm.getShippingProvider().getId(), spm.getShippingMethod().getId());
                        response.setDeletedNumbers(deletedNumber);
                        response.setSuccessful(true);
                    }
                }
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    public List<ShippingProviderAllocationRuleDTO> getShippingProviderAllocationRules() {
        List<ShippingProviderAllocationRuleDTO> allocatiRuleDTOs = new ArrayList<ShippingProviderAllocationRuleDTO>();
        for (ShippingProviderAllocationRule allocationRule : shippingAdminDao.getShippingProviderAllocationRules()) {
            ShippingProviderAllocationRuleDTO allocationRuleDTO = new ShippingProviderAllocationRuleDTO();
            allocationRuleDTO.setAllocationCriteria(allocationRule.getAllocationCriteria());
            allocationRuleDTO.setConditionExpressionText(allocationRule.getConditionExpressionText());
            allocationRuleDTO.setEnabled(allocationRule.isEnabled());
            allocationRuleDTO.setId(allocationRule.getId());
            allocationRuleDTO.setName(allocationRule.getName());
            allocationRuleDTO.setPreference(allocationRule.getPreference());
            ShippingProvider shippingProvider = allocationRule.getShippingProvider();
            allocationRuleDTO.setShippingProviderCode(shippingProvider.getCode());
            allocationRuleDTO.setShippingProviderId(shippingProvider.getId());

            allocatiRuleDTOs.add(allocationRuleDTO);
        }
        return allocatiRuleDTOs;
    }

    @Override
    @Transactional
    @MarkDirty(values = { ShippingFacilityLevelConfiguration.class })
    public CreateProviderAllocationRuleResponse createProviderAllocationRule(CreateProviderAllocationRuleRequest request) {
        CreateProviderAllocationRuleResponse response = new CreateProviderAllocationRuleResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            ShippingProviderAllocationRule allocationRule = shippingAdminDao.getShippingProviderAllocationRuleByName(request.getWsProviderAllocationRule().getName());
            if (allocationRule != null) {
                context.addError(WsResponseCode.DUPLICATE_SHIPPING_PROVIDER_ALLOCATION_RULE, "Shipping provider allocation rule already exist.");
            }
            if (!context.hasErrors()) {
                allocationRule = new ShippingProviderAllocationRule();
                prepareShippingProviderAllocationRule(allocationRule, request.getWsProviderAllocationRule(), context);
                if (!context.hasErrors()) {
                    allocationRule.setCreated(DateUtils.getCurrentTime());
                    allocationRule.setUpdated(DateUtils.getCurrentTime());
                    shippingAdminDao.createShippingProviderAllocationRule(allocationRule);
                    response.setProviderAllocationRuleDTO(new ShippingProviderAllocationRuleDTO(allocationRule));
                    response.setSuccessful(true);
                }
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Transactional
    private void prepareShippingProviderAllocationRule(ShippingProviderAllocationRule allocationRule, WsProviderAllocationRule wsProviderAllocationRule, ValidationContext context) {
        AllocationCriteria allocationCriteria = ShippingProviderAllocationRule.AllocationCriteria.valueOf(wsProviderAllocationRule.getAllocationCriteria());
        if (allocationCriteria == null) {
            context.addError(WsResponseCode.INVALID_ALLOCATION_CRITERIA, "Invalid Allocation Criteria");
        } else {
            allocationRule.setAllocationCriteria(allocationCriteria.name());
        }
        allocationRule.setConditionExpressionText(wsProviderAllocationRule.getConditionExpressionText());
        allocationRule.setEnabled(wsProviderAllocationRule.isEnabled());
        allocationRule.setName(wsProviderAllocationRule.getName());
        allocationRule.setPreference(wsProviderAllocationRule.getPreference());
        ShippingProvider shippingProvider = ConfigurationManager.getInstance().getConfiguration(ShippingConfiguration.class).getShippingProviderById(
                wsProviderAllocationRule.getShippingProviderId());
        if (shippingProvider == null) {
            context.addError(WsResponseCode.INVALID_SHIPPING_PROVIDER_ID, "Invalid Shipping Provider");
        }
        allocationRule.setShippingProvider(shippingProvider);
    }

    @Override
    @Transactional
    @MarkDirty(values = { ShippingFacilityLevelConfiguration.class })
    public EditProviderAllocationRuleResponse editProviderAllocationRule(EditProviderAllocationRuleRequest request) {
        EditProviderAllocationRuleResponse response = new EditProviderAllocationRuleResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            ShippingProviderAllocationRule allocationRule = shippingAdminDao.getShippingProviderAllocationRuleByName(request.getWsProviderAllocationRule().getName());
            if (allocationRule == null) {
                context.addError(WsResponseCode.INVALID_SHIPPING_PROVIDER_ALLOCATION_RULE, "Invalid shipping provider allocation rule");
            }
            if (!context.hasErrors()) {
                prepareShippingProviderAllocationRule(allocationRule, request.getWsProviderAllocationRule(), context);
                if (!context.hasErrors()) {
                    allocationRule.setUpdated(DateUtils.getCurrentTime());
                    response.setProviderAllocationRuleDTO(new ShippingProviderAllocationRuleDTO(allocationRule));
                    response.setSuccessful(true);
                }
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    @MarkDirty(values = { ShippingFacilityLevelConfiguration.class })
    public DeleteProviderAllocationRuleResponse deleteProviderAllocationRule(DeleteProviderAllocationRuleRequest request) {
        DeleteProviderAllocationRuleResponse response = new DeleteProviderAllocationRuleResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            ShippingProviderAllocationRule allocationRule = shippingAdminDao.getShippingProviderAllocationRuleByName(request.getProviderAllocationRuleName());
            if (allocationRule == null) {
                context.addError(WsResponseCode.INVALID_SHIPPING_PROVIDER_ALLOCATION_RULE, "Invalid shipping provider allocation rule");
            } else {
                shippingAdminDao.deleteShippingProviderAllocationRule(allocationRule);
                response.setSuccessful(true);
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    @MarkDirty(values = { ShippingFacilityLevelConfiguration.class })
    public ReorderProviderAllocationRulesResponse reorderProviderAllocationRules(ReorderProviderAllocationRulesRequest request) {
        ReorderProviderAllocationRulesResponse response = new ReorderProviderAllocationRulesResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            Map<String, Integer> allocationRuleNameToPreferenceMap = request.getAllocationRuleNameToPreferenceMap();
            List<ShippingProviderAllocationRule> shippingProviderAllocationRules = shippingAdminDao.getShippingProviderAllocationRules();
            for (ShippingProviderAllocationRule allocationRule : shippingProviderAllocationRules) {
                Integer preference = allocationRuleNameToPreferenceMap.get(allocationRule.getName());
                allocationRule.setPreference(preference);
            }
            response.setSuccessful(true);
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    public List<ShippingProviderSource> getAllShippingProviderSources() {
        return shippingAdminMao.getAllShippingProviderSources();
    }

    @Override
    @MarkDirty(values = { ShippingConfiguration.class, ShippingFacilityLevelConfiguration.class })
    @Transactional
    public AddShippingProviderResponse addShippingProvider(AddShippingProviderRequest request) {
        AddShippingProviderResponse response = new AddShippingProviderResponse();
        ValidationContext context = request.validate();
        String shippingProviderCode = constructCodeFromName(request.getWsShippingProvider().getName());
        ShippingProvider shippingProvider = getShippingProviderByCode(shippingProviderCode);
        ShippingProviderSource providerSource = ConfigurationManager.getInstance().getConfiguration(ShippingSourceConfiguration.class).getShippingSourceByCode(
                request.getWsShippingProvider().getShippingSourceCode());
        if (!context.hasErrors()) {
            if (providerSource == null) {
                context.addError(WsResponseCode.INVALID_SHIPPING_PROVIDER_CODE, "Invalid Shipping Provider: " + request.getWsShippingProvider().getShippingSourceCode());
            }
            if (shippingProvider != null) {
                context.addError(WsResponseCode.SHIPPING_PROVIDER_ALREADY_ADDED, WsResponseCode.SHIPPING_PROVIDER_ALREADY_ADDED.message() + ":"
                        + request.getWsShippingProvider().getName());
            }
        }

        for (WSShippingMethod wsShippingMethod : request.getWsShippingProvider().getShippingMethods()) {
            ShippingProviderMethod shippingProviderMethod = shippingDao.getShippingProviderMethod(shippingProviderCode, wsShippingMethod.getCode());
            if (shippingProviderMethod != null) {
                context.addError(WsResponseCode.INVALID_CODE, "Shipping Provider Method already exists with code \"" + shippingProviderCode + "\"");
                response.setErrors(context.getErrors());
            }
        }

        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        } else {
            shippingProvider = new ShippingProvider();
            shippingProvider.setCode(shippingProviderCode);
            shippingProvider.setName(request.getWsShippingProvider().getName());
            shippingProvider.setEnabled(request.getWsShippingProvider().isEnabled());
            shippingProvider.setShippingProviderSourceCode(providerSource.getCode());
            shippingProvider.setConfigured(true);
            shippingProvider.setIntegrated(true);
            shippingProvider.setHidden(request.getWsShippingProvider().isHidden());
            shippingProvider.setShippingServiceablity(ShippingServiceablity.valueOf(request.getWsShippingProvider().getServiceability()));
            shippingProvider.setShortName(constructChannelShortName(providerSource, request.getWsShippingProvider().getName()));
            shippingProvider.setColorCode(getShippingProviderColorCodeBySource(providerSource.getCode()));
            if (providerSource.isTrackingEnabled()) {
                if (StringUtils.isBlank(request.getWsShippingProvider().getTrackingSyncStatus())) {
                    shippingProvider.setTrackingSyncStatus(SyncStatus.OFF);
                } else {
                    shippingProvider.setTrackingSyncStatus(SyncStatus.valueOf(request.getWsShippingProvider().getTrackingSyncStatus()));
                }
            } else {
                shippingProvider.setTrackingSyncStatus(SyncStatus.NOT_AVAILABLE);
            }
            for (ShippingSourceConnector connector : providerSource.getShippingSourceConnectors()) {
                ShippingProviderConnector providerConnector = new ShippingProviderConnector();
                providerConnector.setShippingProvider(shippingProvider);
                providerConnector.setShippingSourceConnectorName(connector.getName());
                providerConnector.setStatusCode(ShippingProviderConnector.Status.NOT_CONFIGURED);
                providerConnector.setCreated(DateUtils.getCurrentTime());
                shippingProvider.getShippingProviderConnectors().add(providerConnector);
            }
            shippingProvider.setCreated(DateUtils.getCurrentTime());
            shippingAdminDao.addShippingProvider(shippingProvider);
            ShippingProviderDetailDTO detailDTO = new ShippingProviderDetailDTO(shippingProvider, providerSource);
            for (WSShippingMethod wsShippingMethod : request.getWsShippingProvider().getShippingMethods()) {
                ShippingProviderMethod shippingProviderMethod = new ShippingProviderMethod();
                shippingProviderMethod.setShippingMethod(ConfigurationManager.getInstance().getConfiguration(ShippingConfiguration.class).getShippingMethod(
                        wsShippingMethod.getCode()));
                shippingProviderMethod.setShippingProvider(getShippingProviderByCode(shippingProviderCode));
                shippingProviderMethod.setEnabled(wsShippingMethod.isEnabled());
                shippingProviderMethod.setCreated(DateUtils.getCurrentTime());
                shippingProviderMethod.setTrackingNumberGeneration(wsShippingMethod.getAwbGeneration());
                if (StringUtils.isNotBlank(wsShippingMethod.getFacilityCode())) {
                    shippingProviderMethod.setFacility(facilityDao.getFacilityByCode(wsShippingMethod.getFacilityCode()));
                }
                shippingProviderMethod = shippingAdminDao.updateShippingProviderMethod(shippingProviderMethod);
                detailDTO.addShippingMethod(getShippingProviderMethodDTO(shippingProviderMethod));
            }
            response.setShippingProvider(detailDTO);
            response.setSuccessful(true);
        }
        return response;
    }

    private String constructChannelShortName(ShippingProviderSource source, String name) {
        String sourceCode = source.getCode();
        String reservedKeywords = source.getReservedKeywordsForShortName();
        List<String> reservedWords = new ArrayList<>();
        if (reservedKeywords != null) {
            reservedWords.addAll(StringUtils.split(reservedKeywords));
        }
        reservedWords.add(sourceCode.toUpperCase());
        reservedWords.add(UserContext.current().getTenant().getCode().toUpperCase());

        String channelName = name.toUpperCase();
        for (String reservedWord : reservedWords) {
            channelName = channelName.replace(reservedWord.toUpperCase(), "");
        }
        if (channelName.trim().length() < 2) {
            reservedWords.remove(sourceCode.toUpperCase());
            reservedWords.remove(UserContext.current().getTenant().getCode().toUpperCase());
            channelName = name.toUpperCase();
            for (String reservedWord : reservedWords) {
                channelName = channelName.replace(reservedWord, "");
            }
        }
        channelName = channelName.replaceAll("\\W", "");
        char array[] = channelName.toCharArray();
        char[] shortName = new char[2];
        boolean found = false;
        for (int i = 0; i < array.length; i++) {
            for (int j = i + 1; j < array.length; j++) {
                shortName[0] = array[i];
                shortName[1] = array[j];
                if (checkShortNameAvailability(sourceCode, new String(shortName))) {
                    found = true;
                    break;
                }
            }
            if (found) {
                break;
            }
        }
        if (found) {
            return new String(shortName);
        } else {
            int retry = 0;
            while (retry < 20) {
                retry++;
                String generatedName = StringUtils.getRandomAlphaNumeric(2).toUpperCase();
                if (checkShortNameAvailability(sourceCode, generatedName)) {
                    return generatedName;
                }
            }
        }
        return DEFAULT_SHORT_NAME;
    }

    private String constructCodeFromName(String channelName) {
        return channelName.replaceAll("\\W", "_").toUpperCase();
    }

    @Transactional
    public boolean checkShortNameAvailability(String sourceCode, String shortName) {
        ShippingConfiguration shippingConfiguration = ConfigurationManager.getInstance().getConfiguration(ShippingConfiguration.class);
        boolean found = false;
        for (ShippingProvider shippingProvider : shippingConfiguration.getShippingProviders()) {
            if (shippingProvider.getShippingProviderSourceCode().equals(sourceCode)) {
                if (shippingProvider.getShortName().equals(shortName)) {
                    found = true;
                    break;
                }
            }
        }
        return found;
    }

    @Override
    @Transactional(readOnly = true)
    public String getShippingProviderColorCodeBySource(String sourceCode) {
        int channelCount = getProviderCountBySourceCode(sourceCode);
        int count = (channelCount % 10) + 1;
        ShippingSourceColor shippingSourceColor = shippingAdminDao.getShippingProviderColorBySourceAndCount(sourceCode, count);
        return (shippingSourceColor != null ? shippingSourceColor.getColor() : StringUtils.EMPTY_STRING);
    }

    @Override
    @Transactional(readOnly = true)
    public int getProviderCountBySourceCode(String sourceCode) {
        ShippingConfiguration shippingConfiguration = ConfigurationManager.getInstance().getConfiguration(ShippingConfiguration.class);
        int count = 0;
        for (ShippingProvider shippingProvider : shippingConfiguration.getAllShippingProviders()) {
            if (shippingProvider.getShippingProviderSourceCode().equals(sourceCode)) {
                count++;
            }
        }
        return count;
    }

    @Override
    @MarkDirty(values = { ShippingConfiguration.class, ShippingFacilityLevelConfiguration.class })
    @Transactional
    public EditShippingProviderResponse updateShippingProvider(EditShippingProviderRequest request) {
        EditShippingProviderResponse response = new EditShippingProviderResponse();
        ValidationContext context = request.validate();
        ShippingProvider shippingProvider = getShippingProviderByCode(StringUtils.isNotBlank(request.getWsShippingProvider().getCode()) ? request.getWsShippingProvider().getCode()
                : constructCodeFromName(request.getWsShippingProvider().getName()));
        if (!context.hasErrors()) {
            if (shippingProvider == null) {
                context.addError(WsResponseCode.INVALID_SHIPPING_PROVIDER_CODE, WsResponseCode.INVALID_SHIPPING_PROVIDER_CODE.message() + ":"
                        + request.getWsShippingProvider().getCode());
            }
        }

        /*for (WSShippingMethod wsShippingMethod : request.getWsShippingProvider().getShippingMethods()) {
            String[] methodPaymentCodes = wsShippingMethod.getCode().split("-");
            ShippingProviderMethod shippingProviderMethod = shippingDao.getShippingProviderMethod(request.getWsShippingProvider().getShippingSourceCode(), methodPaymentCodes[0],
                    methodPaymentCodes[1].equalsIgnoreCase("COD"));
            if (shippingProviderMethod == null) {
                context.addError(WsResponseCode.INVALID_CODE, "No Method exists with code \"" + request.getWsShippingProvider().getShippingSourceCode() + "\"");
                response.setSuccessful(false);
                response.setErrors(context.getErrors());
            }
        }*/

        if (StringUtils.isNotBlank(request.getWsShippingProvider().getTrackingSyncStatus())) {
            SyncStatus syncStatus = SyncStatus.valueOf(request.getWsShippingProvider().getTrackingSyncStatus());
            if (syncStatus.equals(SyncStatus.NOT_AVAILABLE) && !shippingProvider.getTrackingSyncStatus().equals(syncStatus)) {
                context.addError(WsResponseCode.INVALID_SHIPPING_PROVIDER_CONNECTOR_CODE, "Invalid shipping provider connector code");
            } else if (SyncStatus.ON.equals(syncStatus) || SyncStatus.OFF.equals(syncStatus)) {
                shippingProvider.setTrackingSyncStatus(syncStatus);
            }
        }

        if (context.hasErrors()) {
            response.addErrors(context.getErrors());
        } else {
            shippingProvider.setName(request.getWsShippingProvider().getName());
            shippingProvider.setEnabled(request.getWsShippingProvider().isEnabled());
            shippingProvider.setShippingServiceablity(ShippingServiceablity.valueOf(request.getWsShippingProvider().getServiceability()));
            shippingAdminDao.updateShippingProvider(shippingProvider);
            ShippingProviderDetailDTO detailDTO = new ShippingProviderDetailDTO(shippingProvider, ConfigurationManager.getInstance().getConfiguration(
                    ShippingSourceConfiguration.class).getShippingSourceByCode(shippingProvider.getShippingProviderSourceCode()));
            for (WSShippingMethod wsShippingMethod : request.getWsShippingProvider().getShippingMethods()) {
                ShippingProviderMethod shippingProviderMethod = null;
                if (StringUtils.isNotBlank(wsShippingMethod.getFacilityCode())) {
                    Facility facility = facilityDao.getFacilityByCode(wsShippingMethod.getFacilityCode());
                    shippingProviderMethod = shippingDao.getShippingProviderMethod(request.getWsShippingProvider().getCode(), wsShippingMethod.getCode(),
                            facility.getId());
                } else {
                    shippingProviderMethod = shippingDao.getShippingProviderMethod(request.getWsShippingProvider().getCode(), wsShippingMethod.getCode());
                }
                if (shippingProviderMethod == null) {
                    shippingProviderMethod = new ShippingProviderMethod();
                    shippingProviderMethod.setShippingMethod(ConfigurationManager.getInstance().getConfiguration(ShippingConfiguration.class).getShippingMethod(
                            wsShippingMethod.getCode()));
                    shippingProviderMethod.setShippingProvider(shippingProvider);
                    shippingProviderMethod.setEnabled(wsShippingMethod.isEnabled());
                    shippingProviderMethod.setCreated(DateUtils.getCurrentTime());
                    shippingProviderMethod.setTrackingNumberGeneration(wsShippingMethod.getAwbGeneration());
                    if (StringUtils.isNotBlank(wsShippingMethod.getFacilityCode())) {
                        shippingProviderMethod.setFacility(facilityDao.getFacilityByCode(wsShippingMethod.getFacilityCode()));
                    }
                    shippingProviderMethod = shippingAdminDao.updateShippingProviderMethod(shippingProviderMethod);
                } else {
                    shippingProviderMethod.setEnabled(wsShippingMethod.isEnabled());
                    shippingProviderMethod.setTrackingNumberGeneration(wsShippingMethod.getAwbGeneration());
                    if (StringUtils.isNotBlank(wsShippingMethod.getFacilityCode())) {
                        shippingProviderMethod.setFacility(facilityDao.getFacilityByCode(wsShippingMethod.getFacilityCode()));
                    }
                    shippingProviderMethod = shippingAdminDao.updateShippingProviderMethod(shippingProviderMethod);
                }
                detailDTO.addShippingMethod(getShippingProviderMethodDTO(shippingProviderMethod));
            }
            response.setShippingProviderDetailDTO(detailDTO);
            response.setSuccessful(true);
        }
        return response;
    }

    @Override
    @Transactional
    public ShippingProvider getShippingProviderByName(String shippingProviderName) {
        return shippingAdminDao.getShippingProviderByName(shippingProviderName);
    }

    @Override
    @MarkDirty(values = { ShippingConfiguration.class, ShippingFacilityLevelConfiguration.class })
    @Transactional
    public AddShippingProviderConnectorResponse addShippingProviderConnector(AddShippingProviderConnectorRequest request) {
        AddShippingProviderConnectorResponse response = new AddShippingProviderConnectorResponse();
        ValidationContext context = request.validate();
        ShippingProvider shippingProvider = null;
        ShippingSourceConnector sourceConnector = null;
        if (!context.hasErrors()) {
            shippingProvider = ConfigurationManager.getInstance().getConfiguration(ShippingConfiguration.class).getShippingProviderByCode(
                    request.getWsShippingProviderConnector().getShippingCode());
            if (shippingProvider == null) {
                context.addError(WsResponseCode.INVALID_SHIPPING_PROVIDER_CODE, "Invalid shipping provider code");
            } else {
                sourceConnector = ConfigurationManager.getInstance().getConfiguration(ShippingSourceConfiguration.class).getShippingSourceConnector(
                        shippingProvider.getShippingProviderSourceCode(), request.getWsShippingProviderConnector().getName());
                if (sourceConnector == null) {
                    context.addError(WsResponseCode.INVALID_SHIPPING_PROVIDER_CONNECTOR_CODE, "Invalid connector name for shipping provider");
                } else {
                    if (StringUtils.isNotBlank(sourceConnector.getVerificationScriptName())) {
                        ScraperScript script = CacheManager.getInstance().getCache(ScriptVersionedCache.class).getScriptByName(sourceConnector.getVerificationScriptName());
                        Map<String, String> params = new HashMap<>(request.getWsShippingProviderConnector().getShippingProviderConnectorParameters().size());
                        for (WsShippingProviderConnectorParameter param : request.getWsShippingProviderConnector().getShippingProviderConnectorParameters()) {
                            params.put(param.getName(), param.getValue());
                        }
                        if (script != null) {
                            try {
                                if (!verifyAndSyncChannelConnectorParameters(shippingProvider, params, script)) {
                                    context.addError(WsResponseCode.INVALID_SHIPPING_PROVIDER_CONNECTOR_CODE, "shippingProvider.add.connector.errors", "Invalid Credentials");
                                } else {
                                    response.setSuccessful(true);
                                }
                            } catch (Throwable e) {
                                response.setSuccessful(false);
                                context.addError(WsResponseCode.INVALID_SHIPPING_PROVIDER_CREDENTIALS, "provider.add.connector.errors", e.getMessage());
                                shippingAdminDao.updateShippingProviderConnector(setConnectorErrorMessage(shippingProvider, request.getWsShippingProviderConnector().getName(),
                                        e.getMessage(), WsResponseCode.INVALID_SHIPPING_PROVIDER_CREDENTIALS));
                            }
                        } else {
                            context.addError(WsResponseCode.INVALID_SCRIPT, "Script does not for exist for " + shippingProvider.getCode());
                        }
                    } else {
                        context.addError(WsResponseCode.INVALID_SCRIPT, "Script does not for exist for " + shippingProvider.getCode());
                    }
                }

            }

        }
        if (!context.hasErrors()) {
            ShippingProviderSource source = ConfigurationManager.getInstance().getConfiguration(ShippingSourceConfiguration.class).getShippingSourceByCode(
                    shippingProvider.getShippingProviderSourceCode());
            ShippingProviderConnector providerConnector = null;
            for (ShippingProviderConnector connector : shippingProvider.getShippingProviderConnectors()) {
                if (connector.getShippingSourceConnectorName().equals(request.getWsShippingProviderConnector().getName())) {
                    providerConnector = connector;
                }
            }
            if (providerConnector == null) {
                providerConnector = new ShippingProviderConnector();
                providerConnector.setShippingSourceConnectorName(sourceConnector.getName());
                providerConnector.setShippingProvider(shippingProvider);
                providerConnector.setCreated(DateUtils.getCurrentTime());
            }
            Map<String, ShippingSourceConnectorParameter> nameToSourceConnectorParameters = new HashMap<>();
            for (ShippingSourceConnectorParameter sourceConnectorParameter : sourceConnector.getShippingSourceConnectorParameters()) {
                nameToSourceConnectorParameters.put(sourceConnectorParameter.getName(), sourceConnectorParameter);
            }
            Map<String, ShippingProviderConnectorParameter> nameToConnectorParameters = new HashMap<String, ShippingProviderConnectorParameter>();
            for (ShippingProviderConnectorParameter providerConnectorParameter : providerConnector.getShippingProviderConnectorParameters()) {
                nameToConnectorParameters.put(providerConnectorParameter.getName(), providerConnectorParameter);
            }
            for (WsShippingProviderConnectorParameter wsShippingProviderConnectorParameter : request.getWsShippingProviderConnector().getShippingProviderConnectorParameters()) {
                ShippingProviderConnectorParameter providerConnectorParameter = nameToConnectorParameters.get(wsShippingProviderConnectorParameter.getName());
                if (providerConnectorParameter == null) {
                    providerConnectorParameter = new ShippingProviderConnectorParameter();
                    providerConnectorParameter.setShippingProviderConnector(providerConnector);
                    providerConnectorParameter.setName(wsShippingProviderConnectorParameter.getName());
                    providerConnectorParameter.setValue(wsShippingProviderConnectorParameter.getValue());
                    providerConnectorParameter.setCreated(DateUtils.getCurrentTime());
                    providerConnector.getShippingProviderConnectorParameters().add(providerConnectorParameter);
                } else {
                    ShippingSourceConnectorParameter sourceConnectorParameter = nameToSourceConnectorParameters.get(wsShippingProviderConnectorParameter.getName());
                    if (sourceConnectorParameter.getName().equals(providerConnectorParameter.getName())
                            && (!sourceConnectorParameter.getType().equals(SourceConnectorParameter.Type.HIDDEN))
                            || StringUtils.isNotBlank(wsShippingProviderConnectorParameter.getValue())) {
                        providerConnectorParameter.setValue(wsShippingProviderConnectorParameter.getValue());
                    }
                }
            }
            providerConnector.setStatusCode(Status.ACTIVE);
            providerConnector.resetCount();
            shippingProvider.getShippingProviderConnectors().remove(providerConnector);
            shippingProvider.getShippingProviderConnectors().add(providerConnector);
            if (source.isTrackingEnabled() && sourceConnector.isRequiredInTracking()) {
                SyncStatus syncStatus = (shippingProvider.getTrackingSyncStatus() == SyncStatus.ON) ? SyncStatus.ON : SyncStatus.OFF;
                for (ShippingProviderConnector connector : shippingProvider.getShippingProviderConnectors()) {
                    ShippingSourceConnector sc = ConfigurationManager.getInstance().getConfiguration(ShippingSourceConfiguration.class).getShippingSourceConnector(
                            source.getCode(), connector.getShippingSourceConnectorName());
                    if (sc.isRequiredInTracking() && Status.NOT_CONFIGURED.equals(connector.getStatusCode())) {
                        syncStatus = SyncStatus.OFF;
                        break;
                    }
                }
                shippingProvider.setTrackingSyncStatus(syncStatus);
            }
            shippingAdminDao.updateShippingProvider(shippingProvider);
            response.setSuccessful(true);
        }
        response.addErrors(context.getErrors());
        return response;
    }

    @Override
    public VerifyandSyncShippingConnectorParamsResponse verifyAndSyncChannelConnectorParameters(String shippingProviderCode, String connectorName) {
        ShippingProvider shippingProvider = ConfigurationManager.getInstance().getConfiguration(ShippingConfiguration.class).getShippingProviderByCode(shippingProviderCode);
        VerifyandSyncShippingConnectorParamsResponse response = new VerifyandSyncShippingConnectorParamsResponse();
        ShippingProviderConnector connector = null;
        String connectorVerificationName = null;
        for (ShippingProviderConnector spc : shippingProvider.getShippingProviderConnectors()) {
            if (connectorName.equalsIgnoreCase(spc.getShippingSourceConnectorName())) {
                connector = spc;
                ShippingSourceConnector sc = ConfigurationManager.getInstance().getConfiguration(ShippingSourceConfiguration.class).getShippingSourceConnector(
                        shippingProvider.getShippingProviderSourceCode(), connectorName);
                connectorVerificationName = sc.getVerificationScriptName();
            }
        }
        if (connector == null) {
            response.addError(new WsError("Invalid shipping provider connector"));
        } else {
            if (StringUtils.isNotBlank(connectorVerificationName)) {
                ScraperScript script = CacheManager.getInstance().getCache(ScriptVersionedCache.class).getScriptByName(connectorVerificationName);
                if (script == null) {
                    LOG.error("Missing provider connector verification script {}", shippingProviderCode);
                    response.addError(new WsError("Missing provider connector verification script"));
                } else {
                    Map<String, String> params = new HashMap<>(connector.getShippingProviderConnectorParameters().size());
                    for (ShippingProviderConnectorParameter spcp : connector.getShippingProviderConnectorParameters()) {
                        params.put(spcp.getName(), spcp.getValue());
                    }
                    try {
                        response.setGoodToSync(verifyAndSyncChannelConnectorParameters(shippingProvider, params, script));
                        response.setSuccessful(true);
                    } catch (Exception e) {
                        response.addError(new WsError(e.getMessage()));
                    }
                }
            } else {
                response.setGoodToSync(true);
                response.setSuccessful(true);
            }
        }
        return response;
    }

    private boolean verifyAndSyncChannelConnectorParameters(ShippingProvider shippingProvider, Map<String, String> shippingProviderParameters, ScraperScript verificationScript) {
        boolean verificationSuccess = false;
        try {
            ScriptExecutionContext context = ScriptExecutionContext.current();
            context.addVariable("shippingProvider", shippingProvider);
            context.addVariable(
                    "shippingProviderSource",
                    ConfigurationManager.getInstance().getConfiguration(ShippingSourceConfiguration.class).getShippingSourceByCode(shippingProvider.getShippingProviderSourceCode()));
            context.addVariable("shippingProviderParameters", shippingProviderParameters);
            context.setScriptProvider(new IScriptProvider() {
                @Override
                public ScraperScript getScript(String scriptName) {
                    return CacheManager.getInstance().getCache(ScriptVersionedCache.class).getScriptByName(scriptName);
                }
            });
            for (Entry<String, String> e : shippingProviderParameters.entrySet()) {
                context.addVariable(e.getKey(), e.getValue());
            }
            context.setTraceLoggingEnabled(UserContext.current().isTraceLoggingEnabled());
            verificationScript.execute();
            verificationSuccess = true;
            String respnseXML = context.getScriptOutput();
            LOG.info("Response for verification " + respnseXML);
        } finally {
            ScriptExecutionContext.destroy();
        }
        return verificationSuccess;
    }

    private ShippingProviderConnector setConnectorErrorMessage(ShippingProvider shippingProvider, String connectorName, String message, ResponseCode responseCode) {
        ShippingProviderConnector shippingProviderConnector = null;
        for (ShippingProviderConnector connector : shippingProvider.getShippingProviderConnectors()) {
            if (connector.getShippingSourceConnectorName().equals(connectorName)) {
                shippingProviderConnector = connector;
            }
        }
        if (shippingProviderConnector != null) {
            shippingProviderConnector.setErrorMessage(responseCode.code() + ":" + message);
        }
        return shippingProviderConnector;
    }

    @Override
    @MarkDirty(values = { ShippingConfiguration.class })
    @Transactional
    public ShippingProviderConnector updateShippingProviderConnector(ShippingProviderConnector shippingProviderConnector) {
        return shippingAdminDao.updateShippingProviderConnector(shippingProviderConnector);
    }

    /* (non-Javadoc)
     * @see com.uniware.services.shipping.IShippingAdminService#getShippingProviderConnectorParameter(com.uniware.core.api.shipping.GetShippingProviderParametersRequest)
     */
    @Override
    public GetShippingProviderParametersResponse getShippingProviderConnectorParameter(GetShippingProviderParametersRequest request) {
        GetShippingProviderParametersResponse response = new GetShippingProviderParametersResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            response.setSuccessful(true);
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    public GetShippingProvidersResponse getAllShippingProviders() {
        GetShippingProvidersResponse response = new GetShippingProvidersResponse();
        List<ShippingProvider> spList = ConfigurationManager.getInstance().getConfiguration(ShippingConfiguration.class).getAllShippingProviders();
        List<ShippingProviderSearchDTO> dtoList = new ArrayList<>(spList.size());
        for (ShippingProvider sp : spList) {
            dtoList.add(new ShippingProviderSearchDTO(sp.getCode(), sp.getName()));
        }
        response.setShippingProviders(dtoList);
        response.setSuccessful(true);
        return response;
    }

}