/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 2, 2012
 *  @author singla
 */
package com.uniware.services.shipping;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.uniware.core.api.shipping.ShipmentTrackingSyncStatusDTO;
import com.uniware.core.api.shipping.SyncShipmentTrackingStatusRequest;
import com.uniware.core.api.shipping.SyncShipmentTrackingStatusResponse;
import com.uniware.core.api.shipping.SyncTrackingStatusForAWBRequest;
import com.uniware.core.api.shipping.SyncTrackingStatusForAWBResponse;
import com.uniware.core.api.shipping.UpdateShipmentTrackingResponse;
import com.uniware.core.api.shipping.UpdateTrackingStatusResponse;
import com.uniware.core.entity.ShipmentTracking;
import com.uniware.core.entity.ShipmentTrackingStatus;
import com.uniware.core.entity.ShippingProvider;

/**
 * @author singla
 */
public interface IShipmentTrackingService {

    /**
     * @param shipments
     * @return
     */
    UpdateTrackingStatusResponse updateTrackingStatuses(ShippingProvider shippingProvider, List<ShipmentTracking> shipments, ShipmentTrackingSyncStatusDTO syncStatusDTO);

    /**
     * @param shipmentTracking
     * @return
     */
    ShipmentTracking addShipmentTracking(ShipmentTracking shipmentTracking);

    /**
     * @param shipmentTracking
     * @return
     */
    ShipmentTracking updateShipmentTracking(ShipmentTracking shipmentTracking);

    /**
     * @param trackingNumber
     * @param shippingProviderId
     * @return
     */
    ShipmentTracking getShipmentTracking(String trackingNumber, Integer shippingProviderId);

    List<ShipmentTrackingStatus> getShipmentTrackingStatuses();

    /* (non-Javadoc)
     * @see com.uniware.services.shipping.IShipmentTrackingService#getShipmentsForStatusPolling(int)
     */
    @Transactional(readOnly = true)
    List<ShipmentTracking> getShipmentsForStatusPolling(int shippingProviderId, int start, int pageSize);

    UpdateShipmentTrackingResponse updateShipmentTracking(String shippingProviderCode, ShipmentTracking shipmentTracking, ProviderShipmentStatus providerStatus,
            boolean manualTrackingUpdate);

    SyncShipmentTrackingStatusResponse syncTrackingStatuses(SyncShipmentTrackingStatusRequest request);

    ShipmentTrackingSyncStatusDTO getTrackingSyncStatusByProvider(String shippingProviderCode);

    void saveShippingProviderSyncStatus(ShipmentTrackingSyncStatusDTO statusDTO);

    ShipmentTrackingSyncStatusDTO getTrackingSyncStatusByProviderFromDB(String shippingProviderCode);

    SyncTrackingStatusForAWBResponse syncStatusForAWB(SyncTrackingStatusForAWBRequest request);

}
