/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 05-Aug-2014
 *  @author parijat
 */
package com.uniware.services.shipping.impl;

import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.email.EmailMessage;
import com.unifier.core.entity.EmailTemplate;
import com.unifier.core.sms.SmsMessage;
import com.unifier.core.utils.StringUtils;
import com.unifier.scraper.sl.runtime.IScriptProvider;
import com.unifier.scraper.sl.runtime.ScraperScript;
import com.unifier.scraper.sl.runtime.ScriptExecutionContext;
import com.unifier.services.email.IEmailService;
import com.unifier.services.sms.ISmsService;
import com.uniware.core.cache.FacilityCache;
import com.uniware.core.entity.Channel;
import com.uniware.core.entity.ChannelConfigurationParameter;
import com.uniware.core.entity.ChannelConnector;
import com.uniware.core.entity.ChannelConnectorParameter;
import com.uniware.core.entity.Facility;
import com.uniware.core.entity.FacilityProfile;
import com.uniware.core.entity.ShippingMethod;
import com.uniware.core.entity.ShippingPackage;
import com.uniware.core.entity.SmsTemplate;
import com.uniware.core.entity.Source;
import com.uniware.core.utils.EntityPersistenceContext;
import com.uniware.core.utils.UserContext;
import com.uniware.services.cache.ChannelCache;
import com.uniware.services.cache.ScriptVersionedCache;
import com.uniware.services.configuration.EmailConfiguration;
import com.uniware.services.configuration.EmailConfiguration.EmailTemplateVO;
import com.uniware.services.configuration.SmsConfiguration;
import com.uniware.services.configuration.SourceConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.support.TransactionSynchronizationAdapter;

/**
 * @author Sunny
 */
@Component(value = "createShippingPackagePostCommitHandler")
public class CreateShippingPackagePostCommitHandler extends TransactionSynchronizationAdapter {

    private static final Logger LOG = LoggerFactory.getLogger(CreateShippingPackagePostCommitHandler.class);

    @Autowired
    private ISmsService smsService;

    @Autowired
    private IEmailService emailService;

    @Override
    public void afterCommit() {
        ShippingPackage shippingPackage = (ShippingPackage) EntityPersistenceContext.current().getEntity();
        Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(shippingPackage.getSaleOrder().getChannel().getSourceCode());
        if (source.isPODRequired()) {
            notifyCustomer(shippingPackage);
        }
        Channel channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelByCode(shippingPackage.getSaleOrder().getChannel().getCode());
        ScraperScript scraperScript = CacheManager.getInstance().getCache(ChannelCache.class).getScriptByName(channel.getCode(),
                com.uniware.core.entity.Source.POST_SHIPPING_PACKAGE_CREATE_SCRIPT);
        if (scraperScript != null) {
            executePostShipppingPackageCreateScript(scraperScript, channel, shippingPackage);
        }
    }

    private void executePostShipppingPackageCreateScript(ScraperScript scraperScript, Channel channel, ShippingPackage shippingPackage) {
        try {
            ScriptExecutionContext scriptContext = ScriptExecutionContext.current();
            scriptContext.addVariable("channel", channel);
            scriptContext.addVariable("shippingPackage", shippingPackage);
            for (ChannelConnector channelConnector : channel.getChannelConnectors()) {
                for (ChannelConnectorParameter channelConnectorParameter : channelConnector.getChannelConnectorParameters()) {
                    scriptContext.addVariable(channelConnectorParameter.getName(), channelConnectorParameter.getValue());
                }
            }
            for (ChannelConfigurationParameter channelConfigurationParameter : channel.getChannelConfigurationParameters()) {
                scriptContext.addVariable(channelConfigurationParameter.getSourceConfigurationParameterName(), channelConfigurationParameter.getValue());
            }
            scriptContext.setScriptProvider(new IScriptProvider() {

                @Override
                public ScraperScript getScript(String scriptName) {
                    return CacheManager.getInstance().getCache(ScriptVersionedCache.class).getScriptByName(scriptName);
                }
            });
            scriptContext.setTraceLoggingEnabled(UserContext.current().isTraceLoggingEnabled());
            scraperScript.execute();
        } catch (Exception e) {
            LOG.error("unable to complete post shipping package create script for shipping package :" + shippingPackage.getCode(), e);
        } finally {
            ScriptExecutionContext.destroy();
        }
    }

    private void notifyCustomer(ShippingPackage shippingPackage) {
        SmsConfiguration.SmsTemplateVO smsTemplate = ConfigurationManager.getInstance().getConfiguration(SmsConfiguration.class).getTemplateByName(
                SmsTemplate.Name.PACKAGE_CREATED.name());
        String notificationMobile = shippingPackage.getSaleOrder().getNotificationMobile();
        LOG.info("Sending package creation sms");
        Facility facility = CacheManager.getInstance().getCache(FacilityCache.class).getFacilityById(shippingPackage.getFacility().getId());
        FacilityProfile facilityProfile = CacheManager.getInstance().getCache(FacilityCache.class).getFacilityProfileByCode(facility.getCode());
        if (smsTemplate != null && smsTemplate.isEnabled() && StringUtils.isNotBlank(notificationMobile)) {
            try {
                SmsMessage message = new SmsMessage(notificationMobile, SmsTemplate.Name.PACKAGE_CREATED.name());
                message.addTemplateParam("shippingPackage", shippingPackage);
                message.addTemplateParam("facility", facility);
                message.addTemplateParam("facilityProfile", facilityProfile);
                if (ShippingMethod.Code.STD.name().equals(shippingPackage.getShippingMethod().getCode())) {
                    if (facilityProfile != null) {
                        Integer SLA = facilityProfile.getDispatchSLA() + facilityProfile.getDeliverySLA();
                        message.addTemplateParam("SLA", SLA);
                    }
                }
                smsService.send(message, shippingPackage.getSaleOrder().getChannel().getSourceCode());
            } catch (Exception e) {
                LOG.error("Error occured while sending package dispatched sms:", e);
            }
        }
        EmailTemplateVO emailTemplate = ConfigurationManager.getInstance().getConfiguration(EmailConfiguration.class).getTemplateByType(EmailTemplate.Type.PACKAGE_CREATED.name());
        if (emailTemplate != null && emailTemplate.isEnabled() && StringUtils.isNotBlank(shippingPackage.getSaleOrder().getNotificationEmail())) {
            try {
                EmailMessage emailMessage = new EmailMessage(shippingPackage.getSaleOrder().getNotificationEmail(), EmailTemplate.Type.PACKAGE_CREATED.name());
                emailMessage.addTemplateParam("shippingPackage", shippingPackage);
                emailMessage.addTemplateParam("facility", facility);
                emailMessage.addTemplateParam("facilityProfile", facilityProfile);
                if (ShippingMethod.Code.STD.name().equals(shippingPackage.getShippingMethod().getCode())) {
                    if (facilityProfile != null) {
                        Integer SLA = facilityProfile.getDispatchSLA() + facilityProfile.getDeliverySLA();
                        emailMessage.addTemplateParam("SLA", SLA);
                    }
                }
                emailService.send(emailMessage, shippingPackage.getSaleOrder().getChannel().getSourceCode());
            } catch (Exception e) {
                LOG.error("Error occured while sending package dispatched email:", e);
            }
        }
    }

    @Override
    public void afterCompletion(int status) {
        EntityPersistenceContext.destroy();
    }
}
