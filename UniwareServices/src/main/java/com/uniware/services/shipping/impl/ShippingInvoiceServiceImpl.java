package com.uniware.services.shipping.impl;

import static com.uniware.core.entity.PicklistItem.StatusCode.PUTBACK_PENDING;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.jibx.runtime.JiBXException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.unifier.core.annotation.audit.LogActivity;
import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.api.validation.WsError;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.template.Template;
import com.unifier.core.utils.CollectionUtils;
import com.unifier.core.utils.EncryptionUtils;
import com.unifier.core.utils.FileUtils;
import com.unifier.core.utils.JibxUtils;
import com.unifier.core.utils.JsonUtils;
import com.unifier.core.utils.NumberUtils;
import com.unifier.core.utils.StringUtils;
import com.unifier.scraper.sl.runtime.ScraperScript;
import com.unifier.scraper.sl.runtime.ScriptExecutionContext;
import com.unifier.services.aspect.RollbackOnFailure;
import com.unifier.services.pdf.IPdfDocumentService;
import com.unifier.services.pdf.impl.PdfDocumentServiceImpl;
import com.unifier.services.tenantprofile.service.ITenantProfileService;
import com.unifier.services.vo.TenantProfileVO;
import com.uniware.core.api.inflow.AddSaleOrderItemDetailRequest;
import com.uniware.core.api.inflow.AddSaleOrderItemDetailResponse;
import com.uniware.core.api.inflow.ItemDetail;
import com.uniware.core.api.invoice.BulkCreateInvoiceRequest;
import com.uniware.core.api.invoice.BulkCreateInvoiceResponse;
import com.uniware.core.api.invoice.ChangeInvoiceCodeRequest;
import com.uniware.core.api.invoice.ChangeInvoiceCodeResponse;
import com.uniware.core.api.invoice.CreateInvoiceWithDetailsRequest;
import com.uniware.core.api.invoice.CreateInvoiceWithDetailsResponse;
import com.uniware.core.api.invoice.CreateShippingPackageInvoiceRequest;
import com.uniware.core.api.invoice.CreateShippingPackageInvoiceResponse;
import com.uniware.core.api.invoice.CreateShippingPackageReverseInvoiceRequest;
import com.uniware.core.api.invoice.CreateShippingPackageReverseInvoiceResponse;
import com.uniware.core.api.invoice.GenerateOrUpdateInvoiceRequest;
import com.uniware.core.api.invoice.GenerateOrUpdateInvoiceResponse;
import com.uniware.core.api.invoice.GetAllInvoicesDetailsRequest;
import com.uniware.core.api.invoice.GetAllInvoicesDetailsResponse;
import com.uniware.core.api.invoice.GetInvoiceDetailsByCodeRequest;
import com.uniware.core.api.invoice.GetInvoiceDetailsByCodeResponse;
import com.uniware.core.api.invoice.GetInvoiceDetailsRequest;
import com.uniware.core.api.invoice.GetInvoiceDetailsResponse;
import com.uniware.core.api.invoice.GetInvoiceLabelRequest;
import com.uniware.core.api.invoice.GetInvoiceLabelResponse;
import com.uniware.core.api.invoice.GetInvoiceTaxDetailsFromChannelRequest;
import com.uniware.core.api.invoice.InvoiceDTO;
import com.uniware.core.api.invoice.UpdateInvoiceRequest;
import com.uniware.core.api.invoice.UpdateInvoiceResponse;
import com.uniware.core.api.invoice.WsShippingProviderInfo;
import com.uniware.core.api.invoice.WsTaxInformation;
import com.uniware.core.api.item.ItemDetailFieldDTO;
import com.uniware.core.api.item.PendingItemDetailFieldDTO;
import com.uniware.core.api.model.WsInvoice;
import com.uniware.core.api.model.WsInvoiceItem;
import com.uniware.core.api.model.WsTaxPercentageDetail;
import com.uniware.core.api.packer.CompleteReceivedPicklistItemsForPackageRequest;
import com.uniware.core.api.packer.CompleteReceivedPicklistItemsForPackageResposne;
import com.uniware.core.api.packer.PutbackItemDTO;
import com.uniware.core.api.saleorder.CancelSaleOrderRequest;
import com.uniware.core.api.saleorder.CancelSaleOrderResponse;
import com.uniware.core.api.shipping.AssignManualShippingProviderRequest;
import com.uniware.core.api.shipping.AssignManualShippingProviderResponse;
import com.uniware.core.api.shipping.MarkShippingPackageReadyForPickupRequest;
import com.uniware.core.api.shipping.MarkShippingPackageReadyForPickupResponse;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.cache.FacilityCache;
import com.uniware.core.entity.AddressDetail;
import com.uniware.core.entity.BillingParty;
import com.uniware.core.entity.Bundle;
import com.uniware.core.entity.BundleItemType;
import com.uniware.core.entity.Channel;
import com.uniware.core.entity.ChannelConfigurationParameter;
import com.uniware.core.entity.ChannelConnector;
import com.uniware.core.entity.ChannelConnectorParameter;
import com.uniware.core.entity.ChannelItemType;
import com.uniware.core.entity.Invoice;
import com.uniware.core.entity.InvoiceItem;
import com.uniware.core.entity.InvoiceSaleOrderItem;
import com.uniware.core.entity.Item;
import com.uniware.core.entity.ItemType;
import com.uniware.core.entity.OutboundGatePass;
import com.uniware.core.entity.Party;
import com.uniware.core.entity.PartyAddress;
import com.uniware.core.entity.PartyAddressType;
import com.uniware.core.entity.Picklist;
import com.uniware.core.entity.PicklistItem;
import com.uniware.core.entity.ReversePickup;
import com.uniware.core.entity.SaleOrder;
import com.uniware.core.entity.SaleOrderItem;
import com.uniware.core.entity.Sequence;
import com.uniware.core.entity.Shelf;
import com.uniware.core.entity.ShippingMethod;
import com.uniware.core.entity.ShippingPackage;
import com.uniware.core.entity.Source;
import com.uniware.core.locking.Namespace;
import com.uniware.core.locking.annotation.Lock;
import com.uniware.core.locking.annotation.Locks;
import com.uniware.core.utils.ActivityContext;
import com.uniware.core.utils.UserContext;
import com.uniware.core.vo.ChannelItemTypeTaxVO;
import com.uniware.core.vo.PrintTemplateVO;
import com.uniware.core.vo.SamplePrintTemplateVO;
import com.uniware.services.audit.impl.ActivityEntityEnum;
import com.uniware.services.audit.impl.ActivityTypeEnum;
import com.uniware.services.audit.impl.ActivityUtils;
import com.uniware.services.bundle.IBundleService;
import com.uniware.services.cache.ChannelCache;
import com.uniware.services.cache.ItemTypeDetailCache;
import com.uniware.services.cache.PrintTemplateCache;
import com.uniware.services.cache.SamplePrintTemplateCache;
import com.uniware.services.cache.ScriptVersionedCache;
import com.uniware.services.catalog.ICatalogService;
import com.uniware.services.channel.IChannelCatalogService;
import com.uniware.services.common.ISequenceGenerator;
import com.uniware.services.configuration.ShippingConfiguration;
import com.uniware.services.configuration.SourceConfiguration;
import com.uniware.services.configuration.SystemConfiguration;
import com.uniware.services.configuration.data.manager.ProductConfiguration;
import com.uniware.services.inflow.IInflowService;
import com.uniware.services.inventory.IInventoryService;
import com.uniware.services.invoice.IInvoicePublisherService;
import com.uniware.services.invoice.IInvoiceService;
import com.uniware.services.material.IMaterialService;
import com.uniware.services.packer.IPackerService;
import com.uniware.services.picker.IPickerService;
import com.uniware.services.returns.IReturnsService;
import com.uniware.services.saleorder.ISaleOrderService;
import com.uniware.services.shipping.IShippingInvoiceService;
import com.uniware.services.shipping.IShippingProviderService;
import com.uniware.services.shipping.IShippingService;
import com.uniware.services.tax.ITaxService;
import com.uniware.services.warehouse.IShelfService;

/**
 * Created by Sagar Sahni on 31/05/17.
 */
@Service("shippingInvoiceService")
public class ShippingInvoiceServiceImpl implements IShippingInvoiceService {

    private static final Logger      LOG = LoggerFactory.getLogger(ShippingInvoiceServiceImpl.class);

    @Autowired
    private ApplicationContext       applicationContext;

    @Autowired
    private IInvoiceService          invoiceService;

    @Autowired
    private IShippingService         shippingService;

    @Autowired
    private ITenantProfileService    tenantProfileService;

    @Autowired
    private IInventoryService        inventoryService;

    @Autowired
    private ISaleOrderService        saleOrderService;

    @Autowired
    private IShippingProviderService shippingProviderService;

    @Autowired
    private IInvoicePublisherService invoicePublisherService;

    @Autowired
    private IPdfDocumentService      pdfDocumentService;

    @Autowired
    private ITaxService              taxService;

    @Autowired
    private ISequenceGenerator       iSequenceGenerator;

    @Autowired
    private IMaterialService         materialService;

    @Autowired
    private IInflowService           inflowService;

    @Autowired
    private IChannelCatalogService   channelCatalogService;

    @Autowired
    private IReturnsService          returnsService;

    @Autowired
    private IBundleService           bundleService;

    @Autowired
    private ICatalogService          catalogService;

    @Autowired
    private IPackerService           packerService;

    @Autowired
    private IPickerService           pickerService;

    @Autowired
    private IShelfService            shelfService;

    @Override
    public CreateShippingPackageInvoiceResponse createShippingPackageInvoice(CreateShippingPackageInvoiceRequest request) {
        CreateShippingPackageInvoiceResponse response = new CreateShippingPackageInvoiceResponse();
        ValidationContext context = request.validate();
        ShippingPackage shippingPackage;
        TenantProfileVO tenantProfile = tenantProfileService.getTenantProfileByCode(UserContext.current().getTenant().getCode());
        if (!context.hasErrors()) {
            shippingPackage = shippingService.getShippingPackageByCode(request.getShippingPackageCode());
            if (StringUtils.equalsAny(tenantProfile.getPaymentStatus(), TenantProfileVO.PaymentStatus.LIMITED_SERVICES, TenantProfileVO.PaymentStatus.BLOCKED)) {
                context.addError(WsResponseCode.ACCESS_DENIED, "Please recharge your account to allow this service");
            } else if (shippingPackage == null) {
                context.addError(WsResponseCode.INVALID_SHIPPING_PACKAGE_CODE, "Invalid package code");
            } else if (isPutbackPendingForPackageItems(shippingPackage, context, response)) {
                context.addError(WsResponseCode.INVALID_PACKAGE_STATE, "Putback Pending for cancelled items in picklist");
            } else if (isPackageReadyForInvoicing(shippingPackage, context, request.isSkipDetailing())) {
                try {
                    createShippingPackageInvoiceInternal(request, response, context, shippingPackage);
                } catch (Throwable t) {
                    LOG.error("Failed to create invoice", t);
                    throw t;
                }
                if (!context.hasErrors()) {
                    executePostInvoiceScript(shippingPackage);
                }
            } else {
                context.addError(WsResponseCode.INVALID_PACKAGE_STATE, "Package not ready invoicing");
            }
        }
        if (context.hasErrors()) {
            response.addErrors(context.getErrors());
            response.addWarnings(context.getWarnings());
        } else {
            response.setSuccessful(true);
        }
        return response;
    }

    @Transactional(readOnly = true)
    private boolean isPutbackPendingForPackageItems(ShippingPackage shippingPackage, ValidationContext context, CreateShippingPackageInvoiceResponse response) {
        SystemConfiguration.TraceabilityLevel traceabilityLevel = ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).getTraceabilityLevel();
        if (!SystemConfiguration.TraceabilityLevel.ITEM.equals(traceabilityLevel)) {
            shippingPackage = shippingService.getShippingPackageByCode(shippingPackage.getCode());
            Map<String, SaleOrderItem> codeToSaleOrderItem = new HashMap<>();
            shippingPackage.getSaleOrder().getSaleOrderItems().forEach(saleOrderItem -> codeToSaleOrderItem.put(saleOrderItem.getCode(), saleOrderItem));
            List<PicklistItem> picklistItems = pickerService.getPicklistItemsByShippingPackageCode(shippingPackage.getCode(), Picklist.Destination.INVOICING);
            List<PutbackItemDTO> putbackItems = new ArrayList<>();
            picklistItems.stream().filter(picklistItem -> PUTBACK_PENDING.equals(picklistItem.getStatusCode())).forEach(
                    picklistItem -> putbackItems.add(preparePutbackItemDto(codeToSaleOrderItem.get(picklistItem.getSaleOrderItemCode()))));
            if (!CollectionUtils.isEmpty(putbackItems)) {
                response.setPutbackItems(putbackItems);
                return true;
            }
        }
        return false;
    }

    private PutbackItemDTO preparePutbackItemDto(SaleOrderItem saleOrderItem) {
        PutbackItemDTO putbackItemDTO = new PutbackItemDTO();
        putbackItemDTO.setSaleOrderItemCode(saleOrderItem.getCode());
        putbackItemDTO.setItemSku(saleOrderItem.getItemType().getSkuCode());
        putbackItemDTO.setItemName(saleOrderItem.getItemType().getName());
        putbackItemDTO.setShelfCode(saleOrderItem.getItemTypeInventory().getShelf().getCode());
        return putbackItemDTO;
    }

    @Locks({ @Lock(ns = Namespace.SALE_ORDER, key = "#{#args[3].saleOrder.code}") })
    @Transactional
    @LogActivity
    @RollbackOnFailure
    public CreateShippingPackageInvoiceResponse createShippingPackageInvoiceInternal(CreateShippingPackageInvoiceRequest request, CreateShippingPackageInvoiceResponse response,
            ValidationContext context, ShippingPackage shippingPackage) {
        shippingPackage = shippingService.getShippingPackageByCode(shippingPackage.getCode());
        SaleOrder saleOrder = saleOrderService.getSaleOrderForUpdate(shippingPackage.getSaleOrder().getId());
        Party fromParty = null;
        Channel channel = saleOrder.getChannel();
        Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(channel.getSourceCode());
        WsShippingProviderInfo shippingProviderInfo = null;
        Date channelCreatedTime = null;
        GenerateOrUpdateInvoiceRequest generateInvoiceRequest = null;
        GenerateOrUpdateInvoiceResponse generateInvoiceResponse = null;
        response.setErrors(context.getErrors());
        if (isPutbackPendingForPackageItems(shippingPackage, context, response)) {
            context.addError(WsResponseCode.INVALID_PACKAGE_STATE, "Putback Pending for cancelled items in picklist");
        } else if (!isPackageReadyForInvoicing(shippingPackage, context, request.isSkipDetailing())) {
            context.addError(WsResponseCode.INVALID_PACKAGE_STATE, "Package not ready invoicing");
        }
        if (!context.hasErrors()) {
            String invoiceCode = null, invoiceDisplayCode = null;
            Map<String, WsTaxInformation.ProductTax> channelProductIdToTax = null;
            if (source.isInvoicingOfChannel()) {
                GetInvoiceTaxDetailsFromChannelRequest taxDetailsRequest = fetchInvoiceDetailsFromChannnel(shippingPackage, channel, context, request.isForceCreate());
                if (!context.hasErrors() && taxDetailsRequest != null) {
                    // invoice code on taxDetailsRequest can be null, even if taxDetailsRequest is not,
                    // in the case where third party invoice is unavailable.
                    if (StringUtils.isNotBlank(taxDetailsRequest.getInvoiceCode())) {
                        invoiceCode = channel.getCode() + "-" + taxDetailsRequest.getInvoiceCode();
                        invoiceDisplayCode = taxDetailsRequest.getInvoiceCode();
                    } // else generate it later.
                    channelCreatedTime = taxDetailsRequest.getChannelCreatedTime();
                    channelProductIdToTax = getChannelIdToProductTaxMap(taxDetailsRequest);
                    shippingProviderInfo = taxDetailsRequest.getShippingProviderInfo();
                    syncSaleOrderItemStatusFromChannel(saleOrder, taxDetailsRequest, context);
                }
            }
            if (!context.hasErrors()) {
                BillingParty billingParty = saleOrder.getChannel().getBillingParty();
                fromParty = billingParty != null ? billingParty : CacheManager.getInstance().getCache(FacilityCache.class).getCurrentFacility();
                if (StringUtils.isBlank(invoiceCode)) {
                    invoiceCode = computeInvoiceCode(context, shippingPackage, billingParty);
                }
            }
            if (!context.hasErrors()) {
                generateInvoiceRequest = prepareGenerateInvoiceRequest(Invoice.Type.SALE, shippingPackage.getSaleOrder(), shippingPackage.getShippingAddress(), fromParty,
                        request.getUserId(), shippingPackage.getSaleOrderItems(), invoiceCode, invoiceDisplayCode, channelCreatedTime,
                        channelProductIdToTax != null ? channelProductIdToTax : request.getChannelProductIdToTax());
            }
            if (generateInvoiceRequest != null && !context.hasErrors()) {
                generateInvoiceResponse = generateInvoiceAndMapInvoiceToShippingPackage(context, shippingPackage, generateInvoiceRequest, request.isCommitBlockedInventory(),
                        request.getUserId());
            } else {
                context.addError(WsResponseCode.UNABLE_TO_CREATE_INVOICE, "Unable To Generate Invoice");
            }
        }
        if (!context.hasErrors() && generateInvoiceResponse != null && generateInvoiceResponse.isSuccessful()) {
            if (shippingProviderInfo != null) {
                shippingProviderService.assignChannelShippingProvider(shippingPackage, shippingProviderInfo, context, true);
            }
            if (!context.hasErrors()
                    && !SystemConfiguration.TraceabilityLevel.ITEM.equals(ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).getTraceabilityLevel())) {
                CompleteReceivedPicklistItemsForPackageRequest completePicklistItemsRequest = new CompleteReceivedPicklistItemsForPackageRequest();
                completePicklistItemsRequest.setShippingPackageCode(shippingPackage.getCode());
                completePicklistItemsRequest.setDestination(Picklist.Destination.INVOICING);
                CompleteReceivedPicklistItemsForPackageResposne completePicklistItemsResponse = packerService.completePackagePicklistItems(completePicklistItemsRequest);
                if (!completePicklistItemsResponse.isSuccessful()) {
                    context.addErrors(completePicklistItemsResponse.getErrors());
                }
            }
            if (!context.hasErrors()) {
                response.setShippingPackageCode(shippingPackage.getCode());
                response.setInvoiceCode(generateInvoiceResponse.getInvoiceCode());
                response.setSuccessful(true);
            }

        }
        if (!context.hasErrors()) {
            invoicePublisherService.publish(shippingPackage);
        } else {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    private Map<String, WsTaxInformation.ProductTax> getChannelIdToProductTaxMap(GetInvoiceTaxDetailsFromChannelRequest taxDetailsRequest) {
        Map<String, WsTaxInformation.ProductTax> channelProductIdToTax = new HashMap<>();
        for (WsTaxInformation.ProductTax productTax : taxDetailsRequest.getTaxInformation().getProductTaxes()) {
            channelProductIdToTax.put(productTax.getChannelProductId(), productTax);
        }
        return channelProductIdToTax;
    }

    private void syncSaleOrderItemStatusFromChannel(SaleOrder saleOrder, GetInvoiceTaxDetailsFromChannelRequest taxDetailsFromChannelRequest, ValidationContext context) {
        List<String> cancelledSaleOrderItemCodes = taxDetailsFromChannelRequest.getCancelledSaleOrderItemCodes();
        if (CollectionUtils.isEmpty(cancelledSaleOrderItemCodes)) {
            return;
        }
        for (SaleOrderItem saleOrderItem : saleOrder.getSaleOrderItems()) {
            if (cancelledSaleOrderItemCodes.contains(saleOrderItem.getCode()) && SaleOrderItem.StatusCode.CANCELLED.name().equals(saleOrderItem.getStatusCode())) {
                cancelledSaleOrderItemCodes.remove(saleOrderItem.getCode());
            }
        }
        if (!CollectionUtils.isEmpty(cancelledSaleOrderItemCodes)) {
            CancelSaleOrderRequest cancelSaleOrderRequest = new CancelSaleOrderRequest();
            cancelSaleOrderRequest.setSaleOrderCode(saleOrder.getCode());
            cancelSaleOrderRequest.setSaleOrderItemCodes(cancelledSaleOrderItemCodes);
            cancelSaleOrderRequest.setCancellationReason("Cancelled on channel");
            cancelSaleOrderRequest.setCancelOnChannel(false);
            CancelSaleOrderResponse cancelSaleOrderResponse = saleOrderService.cancelSaleOrder(cancelSaleOrderRequest);
            if (!cancelSaleOrderResponse.isSuccessful()) {
                context.addErrors(cancelSaleOrderResponse.getErrors());
            }
        }
    }

    private GetInvoiceTaxDetailsFromChannelRequest fetchInvoiceDetailsFromChannnel(ShippingPackage shippingPackage, Channel channel, ValidationContext context,
            boolean forceCreate) {
        ScraperScript script = CacheManager.getInstance().getCache(ChannelCache.class).getScriptByName(channel.getCode(), Source.FETCH_INVOICE_SCRIPT_NAME);
        if (script == null) {
            return null;
        }
        GetInvoiceTaxDetailsFromChannelRequest taxDetailsRequest = null;
        try {
            LOG.info("Fetching invoice for shipping package : {}, from channel :{}", shippingPackage.getCode(), channel.getCode());
            taxDetailsRequest = getInvoiceFromChannel(channel, shippingPackage, script, forceCreate);
            boolean thirdPartyInvoicingNotAvailable = taxDetailsRequest.getThirdPartyInvoicingNotAvailable() != null && taxDetailsRequest.getThirdPartyInvoicingNotAvailable();
            if (thirdPartyInvoicingNotAvailable) {
                LOG.info("Third Party invoice not available for shipping package : {}, from channel :{}", shippingPackage.getCode(), channel.getCode());
            }
            if (StringUtils.isBlank(taxDetailsRequest.getInvoiceCode()) && CollectionUtils.isEmpty(taxDetailsRequest.getTaxInformation().getProductTaxes())
                    && !thirdPartyInvoicingNotAvailable) {
                context.addError(WsResponseCode.INVOICE_NOT_GENERATED, "Invoice not found on channel");
            }
        } catch (Throwable t) {
            context.addError(WsResponseCode.UNABLE_TO_CREATE_INVOICE, "Unable to fetch invoice from Channel: " + t);
        }
        return taxDetailsRequest;
    }

    /**
     * Figures out the invoice code to be used based on GST and billing party.
     *
     * @param context
     * @param shippingPackage
     * @param billingParty
     * @return
     */
    private String computeInvoiceCode(ValidationContext context, ShippingPackage shippingPackage, BillingParty billingParty) {
        String invoiceCode = null;
        if (ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).isInvoiceManagementSwitchedOff()) {
            invoiceCode = shippingPackage.getCode();
        } else {
            String sequenceName = null;
            if (!taxService.isGSTEnabled()) {
                if (billingParty != null) {
                    if (shippingPackage.getSaleOrder().getCustomer() != null && shippingPackage.getSaleOrder().getCustomer().isRegisteredDealer()) {
                        sequenceName = billingParty.getTaxInvoiceSequence();
                    } else {
                        sequenceName = billingParty.getRetailInvoiceSequence();
                    }
                } else {
                    if (shippingPackage.getSaleOrder().getCustomer() != null && shippingPackage.getSaleOrder().getCustomer().isRegisteredDealer()) {
                        sequenceName = Sequence.Name.TAX_INVOICE.name();
                    } else {
                        sequenceName = Sequence.Name.INVOICE.name();
                    }
                }
            } else if (billingParty != null) {
                sequenceName = shippingPackage.getSaleOrder().getChannel().getBillingParty().getSaleInvoiceSequence();
            }
            if (sequenceName != null) {
                try {
                    invoiceCode = iSequenceGenerator.generateNextInSameTransaction(sequenceName);
                } catch (IllegalArgumentException e) {
                    context.addError(WsResponseCode.FACILITY_CONFIG_ERROR,
                            "Invoice Sequence not set for Facility/Billing Party for creating invoices, required sequence : " + sequenceName);
                }
            }
        }
        return invoiceCode;
    }

    private GetInvoiceTaxDetailsFromChannelRequest getInvoiceFromChannel(Channel channel, ShippingPackage shippingPackage, ScraperScript channelInvoiceScript, boolean forceCreate)
            throws JiBXException, IOException {
        Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(channel.getSourceCode());
        ScriptExecutionContext seContext = ScriptExecutionContext.current();
        seContext.addVariable("channel", channel);
        seContext.addVariable("source", source);
        seContext.addVariable("shippingPackage", shippingPackage);
        seContext.addVariable("forceCreate", forceCreate);
        seContext.addVariable("applicationContext", applicationContext);
        for (ChannelConnector channelConnector : channel.getChannelConnectors()) {
            for (ChannelConnectorParameter channelConnectorParameter : channelConnector.getChannelConnectorParameters()) {
                seContext.addVariable(channelConnectorParameter.getName(), channelConnectorParameter.getValue());
            }
        }
        for (ChannelConfigurationParameter channelConfigurationParameter : channel.getChannelConfigurationParameters()) {
            seContext.addVariable(channelConfigurationParameter.getSourceConfigurationParameterName(), channelConfigurationParameter.getValue());
        }
        seContext.setScriptProvider(scriptName -> CacheManager.getInstance().getCache(ScriptVersionedCache.class).getScriptByName(scriptName));
        try {
            seContext.setTraceLoggingEnabled(UserContext.current().isTraceLoggingEnabled());
            channelInvoiceScript.execute();
            String invoiceXml = seContext.getScriptOutput();
            if (StringUtils.isNotBlank(invoiceXml)) {
                LOG.info("Creating invoice for request: {}", invoiceXml);
                return JibxUtils.unmarshall("unicommerceServicesBinding19", GetInvoiceTaxDetailsFromChannelRequest.class, invoiceXml);
            } else {
                return null;
            }
        } catch (Throwable e) {
            LOG.error("Unable to fetch invoice from channel", e);
            throw e;
        } finally {
            ScriptExecutionContext.destroy();
        }
    }

    private GenerateOrUpdateInvoiceResponse generateInvoiceAndMapInvoiceToShippingPackage(ValidationContext context, ShippingPackage shippingPackage,
            GenerateOrUpdateInvoiceRequest request, boolean commitBlockedInventory, Integer userId) {
        LOG.info("Creating Invoice for shipping package : {}", shippingPackage.getCode());
        GenerateOrUpdateInvoiceResponse generateInvoiceResponse = invoiceService.createInvoice(request);
        LOG.debug("Invoice Created for shipping package : {}", shippingPackage.getCode());
        if (generateInvoiceResponse.hasErrors()) {
            context.addErrors(generateInvoiceResponse.getErrors());
        } else {
            Invoice invoice = invoiceService.getInvoiceByCode(generateInvoiceResponse.getInvoiceCode());
            if (!context.hasErrors()) {
                if (commitBlockedInventory) {
                    Map<Integer, Integer> itemTypeInventories = new TreeMap<>();
                    for (SaleOrderItem saleOrderItem : shippingPackage.getSaleOrderItems()) {
                        if (saleOrderItem.getItemTypeInventory() != null) {
                            if (itemTypeInventories.containsKey(saleOrderItem.getItemTypeInventory().getId())) {
                                itemTypeInventories.put(saleOrderItem.getItemTypeInventory().getId(), itemTypeInventories.get(saleOrderItem.getItemTypeInventory().getId()) + 1);
                            } else {
                                itemTypeInventories.put(saleOrderItem.getItemTypeInventory().getId(), 1);
                            }
                        }
                    }
                    for (Map.Entry<Integer, Integer> entry : itemTypeInventories.entrySet()) {
                        inventoryService.commitBlockedInventory(entry.getKey(), entry.getValue());
                    }
                }
                Map<String, List<SaleOrderItem>> identifierToSaleOrderItems = new HashMap<>();
                shippingPackage.getSaleOrderItems().forEach(
                        saleOrderItem -> identifierToSaleOrderItems.computeIfAbsent(WsInvoiceItem.generateInvoiceItemIdentifier(WsInvoice.Source.SHIPPING_PACKAGE,
                                saleOrderItem.getItemType().getSkuCode(), saleOrderItem.getBundleSkuCode(), saleOrderItem.getSellingPrice()), k -> new ArrayList<>()).add(
                                        saleOrderItem));
                mapSaleOrderItemsToInvoiceItems(invoice, identifierToSaleOrderItems);
                shippingPackage.setInvoice(invoice);
                shippingPackage.setStatusCode(ShippingPackage.StatusCode.PACKED.name());
                if (shippingPackage.getShelfCode() != null) {
                    Shelf shelf = shelfService.getShelfByCode(shippingPackage.getShelfCode());
                    if (shelf != null) {
                        int saleOrderItemCount = shippingPackage.getSaleOrderItems().size();
                        shelf.setItemCount(shelf.getItemCount() > saleOrderItemCount ? shelf.getItemCount() - saleOrderItemCount : 0);
                        if (shelf.getItemCount() == 0) {
                            shippingPackage.setShelfCode(null);
                        }
                        shelfService.updateShelf(shelf);
                    }
                }
                shippingService.updateShippingPackage(shippingPackage);
                invoiceService.updateInvoice(invoice);
                LOG.debug("Moved ShippingPackage : {} to packed state", shippingPackage.getCode());
                String statusCode = shippingPackage.getStatusCode();
                ShippingMethod shippingMethod = ConfigurationManager.getInstance().getConfiguration(ShippingConfiguration.class).getShippingMethodById(
                        shippingPackage.getShippingMethod().getId());
                if (ShippingMethod.Code.PKP.name().equals(shippingMethod.getCode())) {
                    MarkShippingPackageReadyForPickupRequest markShippingPackageReadyForPickupRequest = new MarkShippingPackageReadyForPickupRequest();
                    markShippingPackageReadyForPickupRequest.setShippingPackageCode(shippingPackage.getCode());
                    markShippingPackageReadyForPickupRequest.setUserId(userId);
                    LOG.debug("Mark ShippingPackage : {} ready for pickup", shippingPackage.getCode());
                    MarkShippingPackageReadyForPickupResponse markShippingPackageReadyForPickupResponse = shippingProviderService.markShippingPackageReadyForPickup(
                            markShippingPackageReadyForPickupRequest);
                    if (markShippingPackageReadyForPickupResponse.hasErrors()) {
                        context.addErrors(markShippingPackageReadyForPickupResponse.getErrors());
                    } else {
                        statusCode = ShippingPackage.StatusCode.READY_TO_SHIP.name();
                    }
                }
                if (!context.hasErrors()) {
                    if (ActivityContext.current().isEnable()) {
                        ActivityUtils.appendActivity(shippingPackage.getCode(), ActivityEntityEnum.SHIPPING_PACKAGE.getName(), shippingPackage.getSaleOrder().getCode(),
                                Arrays.asList(new String[] {
                                        "Shipment package {" + ActivityEntityEnum.SHIPPING_PACKAGE + ":" + shippingPackage.getCode() + "} moved to " + statusCode
                                                + " with invoice code {" + ActivityEntityEnum.INVOICE + ":" + invoice.getCode() + "}" }),
                                ActivityTypeEnum.PROCESSING.name());
                    }
                }
            }
        }
        if (context.hasErrors()) {
            generateInvoiceResponse.setErrors(context.getErrors());
            generateInvoiceResponse.setSuccessful(false);
        }
        return generateInvoiceResponse;
    }

    private GenerateOrUpdateInvoiceRequest prepareGenerateInvoiceRequest(Invoice.Type type, SaleOrder saleOrder, AddressDetail shippingAddress, Party fromParty, Integer userId,
            Set<SaleOrderItem> saleOrderItems, String invoiceCode, String invoiceDisplayCode, Date channelCreated,
            Map<String, WsTaxInformation.ProductTax> channelProductIdToProductTax) {
        LOG.info("Preparing GenerateOrUpdateInvoiceRequest for saleOrder : {}, with fromParty : {}, destination state :{}, destination country: {}, skuToProductTax:{}",
                saleOrder.getCode(), fromParty.getCode(), shippingAddress.getStateCode(), shippingAddress.getCountryCode(), channelProductIdToProductTax);
        GenerateOrUpdateInvoiceRequest generateInvoiceRequest = new GenerateOrUpdateInvoiceRequest();
        WsInvoice wsInvoice = new WsInvoice();
        wsInvoice.setFromPartyCode(fromParty.getCode());
        wsInvoice.setChannelCode(saleOrder.getChannel().getCode());
        setDestinationCodeForInvoice(saleOrder, wsInvoice, shippingAddress);
        wsInvoice.setCode(invoiceCode);
        wsInvoice.setDisplayCode(invoiceDisplayCode);
        wsInvoice.setChannelCreated(channelCreated);
        wsInvoice.setType(type);
        wsInvoice.setSource(WsInvoice.Source.SHIPPING_PACKAGE);
        wsInvoice.setUserId(userId);
        wsInvoice.setCformProvided(saleOrder.isCformProvided());
        wsInvoice.setProductManagementSwitchedOff(saleOrder.isProductManagementSwitchedOff());
        wsInvoice.setTaxExempted(saleOrder.isTaxExempted());
        Map<String, List<SaleOrderItem>> identifierToSaleOrderItems = new HashMap<>();
        saleOrderItems.forEach(saleOrderItem -> identifierToSaleOrderItems.computeIfAbsent(WsInvoiceItem.generateInvoiceItemIdentifier(WsInvoice.Source.SHIPPING_PACKAGE,
                saleOrderItem.getItemType().getSkuCode(), saleOrderItem.getBundleSkuCode(), saleOrderItem.getSellingPrice()), k -> new ArrayList<>()).add(saleOrderItem));
        identifierToSaleOrderItems.forEach((identifier, saleOrderItemsList) -> {
            BigDecimal total = BigDecimal.ZERO, discount = BigDecimal.ZERO, shippingCharges = BigDecimal.ZERO, shippingMethodCharges = BigDecimal.ZERO,
                    giftWrapCharges = BigDecimal.ZERO, cashOnDeliveryCharges = BigDecimal.ZERO, prepaidAmount = BigDecimal.ZERO, voucherValue = BigDecimal.ZERO,
                    storeCredit = BigDecimal.ZERO, serviceTax = BigDecimal.ZERO;
            JsonArray jsonArray = new JsonArray();
            JsonArray itemDetailsJsonArray = null;
            WsTaxPercentageDetail taxPercentageDetail = null;
            SaleOrderItem firstSoi = saleOrderItemsList.get(0);
            for (SaleOrderItem saleOrderItem : saleOrderItemsList) {
                discount = discount.add(saleOrderItem.getDiscount());
                shippingCharges = shippingCharges.add(saleOrderItem.getShippingCharges());
                shippingMethodCharges = shippingMethodCharges.add(saleOrderItem.getShippingMethodCharges());
                giftWrapCharges = giftWrapCharges.add(saleOrderItem.getGiftWrapCharges());
                cashOnDeliveryCharges = cashOnDeliveryCharges.add(saleOrderItem.getCashOnDeliveryCharges());
                prepaidAmount = prepaidAmount.add(saleOrderItem.getPrepaidAmount());
                voucherValue = voucherValue.add(saleOrderItem.getVoucherValue());
                storeCredit = storeCredit.add(saleOrderItem.getStoreCredit());
                total = total.add(saleOrderItem.getSellingPrice());
                Item item = saleOrderItem.getItem();
                if (item != null) {
                    JsonObject jsonObject = new JsonObject();
                    jsonObject.add("itemCode", new JsonPrimitive(item.getCode()));
                    if (StringUtils.isNotBlank(item.getItemDetails())) {
                        String itemDetails = item.getItemDetails();
                        jsonObject.add("customFields", new JsonPrimitive(itemDetails));
                    }
                    jsonArray.add(jsonObject);
                }
                if (StringUtils.isNotEmpty(saleOrderItem.getItemDetails())) {
                    if (itemDetailsJsonArray == null) {
                        itemDetailsJsonArray = new JsonArray();
                    }
                    JsonObject itemDetailsJsonObj = new JsonObject();
                    itemDetailsJsonObj.addProperty(saleOrderItem.getCode(), saleOrderItem.getItemDetails());
                    itemDetailsJsonArray.add(itemDetailsJsonObj);
                }
                if (Invoice.Type.SALE.equals(type) && channelProductIdToProductTax != null && !channelProductIdToProductTax.isEmpty() && taxPercentageDetail == null) {
                    taxPercentageDetail = new WsTaxPercentageDetail();
                    taxPercentageDetail.setTaxPercentage(channelProductIdToProductTax.get(saleOrderItem.getChannelProductId()).getTaxPercentage());
                    taxPercentageDetail.setCentralGst(channelProductIdToProductTax.get(saleOrderItem.getChannelProductId()).getCentralGst());
                    taxPercentageDetail.setStateGst(channelProductIdToProductTax.get(saleOrderItem.getChannelProductId()).getStateGst());
                    taxPercentageDetail.setUnionTerritoryGst(channelProductIdToProductTax.get(saleOrderItem.getChannelProductId()).getUnionTerritoryGst());
                    taxPercentageDetail.setIntegratedGst(channelProductIdToProductTax.get(saleOrderItem.getChannelProductId()).getIntegratedGst());
                    taxPercentageDetail.setCompensationCess(channelProductIdToProductTax.get(saleOrderItem.getChannelProductId()).getCompensationCess());
                    LOG.info("Setting taxPercentageDetail : {} for saleOrder {}", taxPercentageDetail.toString(), saleOrder.getCode());
                } else if (Invoice.Type.SALE_RETURN.equals(type) && taxPercentageDetail == null && saleOrderItem.getInvoiceItem() != null) {
                    InvoiceItem invoiceItem = saleOrderItem.getInvoiceItem();
                    taxPercentageDetail = WsTaxPercentageDetail.prepareWsTaxPercentageDetail(invoiceItem);
                    LOG.info("Setting taxPercentageDetail in Return: {} for invoice Item Id {}", taxPercentageDetail.toString(), invoiceItem.getId());
                }
            }
            WsInvoiceItem wsInvoiceItem = new WsInvoiceItem();
            wsInvoiceItem.setSkuCode(firstSoi.getItemType().getSkuCode());
            wsInvoiceItem.setBundleSkuCode(firstSoi.getBundleSkuCode());
            wsInvoiceItem.setQuantity(saleOrderItemsList.size());
            wsInvoiceItem.setTotal(total);
            wsInvoiceItem.setDiscount(discount);
            if (!type.isReturn() || ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).isReturnInvoiceOtherChargesApplicable()) {
                wsInvoiceItem.setShippingCharges(shippingCharges);
                wsInvoiceItem.setShippingMethodCharges(shippingMethodCharges);
                wsInvoiceItem.setGiftWrapCharges(giftWrapCharges);
                wsInvoiceItem.setCashOnDeliveryCharges(cashOnDeliveryCharges);
                wsInvoiceItem.setVoucherValue(voucherValue);
                wsInvoiceItem.setStoreCredit(storeCredit);
            }
            wsInvoiceItem.setPrepaidAmount(prepaidAmount);
            wsInvoiceItem.setServiceTax(serviceTax);
            wsInvoiceItem.setTaxPercentageDetail(taxPercentageDetail);
            wsInvoiceItem.setChannelProductId(saleOrderItemsList.get(0).getChannelProductId());
            if (channelProductIdToProductTax != null && !channelProductIdToProductTax.isEmpty()) {
                WsTaxInformation.ProductTax productTax = channelProductIdToProductTax.get(wsInvoiceItem.getChannelProductId());
                wsInvoiceItem.setCustomFieldValues(productTax.getCustomFieldValues());
            }
            String additionalInfo = new Gson().toJson(jsonArray);
            wsInvoiceItem.setAdditionalInfo(additionalInfo);
            if (itemDetailsJsonArray != null) {
                wsInvoiceItem.setItemDetails(new Gson().toJson(itemDetailsJsonArray));
            }
            wsInvoice.getInvoiceItems().add(wsInvoiceItem);
        });
        generateInvoiceRequest.setInvoice(wsInvoice);
        return generateInvoiceRequest;
    }

    @Transactional(readOnly = true)
    private boolean isPackageReadyForInvoicing(ShippingPackage shippingPackage, ValidationContext context, boolean skipDetailing) {
        boolean readyForInvoicing = (ShippingPackage.StatusCode.PICKED.name().equals(shippingPackage.getStatusCode()) && !shippingPackage.isRequiresCustomization()
                || ShippingPackage.StatusCode.CUSTOMIZATION_COMPLETE.name().equals(shippingPackage.getStatusCode()))
                || (StringUtils.equalsAny(shippingPackage.getStatusCode(), ShippingPackage.StatusCode.CREATED.name(), ShippingPackage.StatusCode.PICKING.name(),
                        ShippingPackage.StatusCode.PENDING_CUSTOMIZATION.name())
                        && ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).getTraceabilityLevel() != SystemConfiguration.TraceabilityLevel.ITEM);
        LOG.debug("ShippingPackage status : " + shippingPackage.getStatusCode() + ", readyForInvoicing : " + readyForInvoicing);
        if (readyForInvoicing) {
            shippingPackage = shippingService.getShippingPackageByCode(shippingPackage.getCode());
            Map<String, Object> errorParams = new HashMap<>();
            Map<String, List<SaleOrderItem>> statusToSaleOrderItems = new HashMap<>();
            for (SaleOrderItem saleOrderItem : shippingPackage.getSaleOrderItems()) {
                if (!SaleOrderItem.StatusCode.CANCELLED.name().equals(saleOrderItem.getStatusCode())) {
                    statusToSaleOrderItems.computeIfAbsent(saleOrderItem.getStatusCode(), k -> new ArrayList<>()).add(saleOrderItem);
                }
                if (!skipDetailing && saleOrderItem.getItemDetailingStatus() == SaleOrderItem.ItemDetailingStatus.PENDING) {
                    List<ItemDetailFieldDTO> pendingItemDetailFields = new ArrayList<>();
                    Map<String, String> detailsAlreadyPresent = null;
                    if (StringUtils.isNotBlank(saleOrderItem.getItemDetails())) {
                        detailsAlreadyPresent = JsonUtils.jsonToMap(saleOrderItem.getItemDetails());
                    }
                    for (String itemDetailField : StringUtils.split(saleOrderItem.getItemDetailFields())) {
                        if (detailsAlreadyPresent == null || !detailsAlreadyPresent.containsKey(itemDetailField)) {
                            ItemDetailFieldDTO detailFieldDTO = CacheManager.getInstance().getCache(ItemTypeDetailCache.class).getItemDetailFieldByName(itemDetailField);
                            pendingItemDetailFields.add(detailFieldDTO);
                        }
                    }
                    PendingItemDetailFieldDTO pendingDetails = new PendingItemDetailFieldDTO();
                    pendingDetails.setName(saleOrderItem.getItemType().getName());
                    pendingDetails.setSkuCode(saleOrderItem.getItemType().getSkuCode());
                    pendingDetails.setPendingItemDetailFields(pendingItemDetailFields);
                    pendingDetails.setItemCode(saleOrderItem.getItem() != null ? saleOrderItem.getItem().getCode() : null);
                    errorParams.put(saleOrderItem.getCode(), pendingDetails);
                }
                if (ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).getTraceabilityLevel() == SystemConfiguration.TraceabilityLevel.ITEM
                        && saleOrderItem.getItem() == null) {
                    context.addError(WsResponseCode.INVALID_PACKAGE_STATE, "Item codes should be assigned to all items before invoicing");
                }
                Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(
                        shippingPackage.getSaleOrder().getChannel().getSourceCode());
                if (source.isPODRequired() && ShippingMethod.Code.PKP.name().equals(shippingPackage.getShippingMethod().getCode())
                        && ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).isVerifyPODBeforeInvoice() && !shippingPackage.isPodVerified()) {
                    LOG.error("Verifying POD is required before invoicing");
                    context.addError(WsResponseCode.POD_NOT_VERIFIED, "Verifying POD is required before invoicing");
                }
            }
            if (errorParams.size() > 0) {
                LOG.error("Item details must be added for Invoice");
                context.addError(WsResponseCode.ITEM_NOT_DETAILED_FOR_INVOICE, "Item details must be added for Invoice", errorParams);
                readyForInvoicing = false;
            }
            /* This check is to ensure that all the non-cancelled SaleOrderItems are either in FULFILLABLE status
            *  (if not using picklist) or PICKING_FOR_INVOICING status (if picklist is generated)
            */
            if (statusToSaleOrderItems.entrySet().size() > 1) {
                LOG.error("Package SaleOrderItems must be in FULFILLABLE/PICKING_FOR_INVOICING");
                context.addError(WsResponseCode.INVALID_SALE_ORDER_ITEM_STATE, "Package SaleOrderItems must be in FULFILLABLE/PICKING_FOR_INVOICING");
                readyForInvoicing = false;
            }
        }
        return readyForInvoicing;
    }

    @Transactional
    @Locks({ @Lock(ns = Namespace.SALE_ORDER, key = "#{#args[0].saleOrder.code}") })
    @RollbackOnFailure
    private void executePostInvoiceScript(ShippingPackage shippingPackage) {
        shippingPackage = shippingService.getShippingPackageById(shippingPackage.getId());
        Channel channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelById(shippingPackage.getSaleOrder().getChannel().getId());
        ScraperScript scraperScript = CacheManager.getInstance().getCache(ChannelCache.class).getScriptByName(channel.getCode(), Source.POST_INVOICE_SCRIPT_NAME);
        if (scraperScript != null) {
            try {
                ScriptExecutionContext scriptContext = ScriptExecutionContext.current();
                scriptContext.addVariable("channel", channel);
                scriptContext.addVariable("shippingPackage", shippingPackage);
                // Added just to support edit custom fields at sale order item during postInvoiceScript BAD IDEA :(
                scriptContext.addVariable("__applicationContext", applicationContext);
                for (ChannelConnector channelConnector : channel.getChannelConnectors()) {
                    for (ChannelConnectorParameter channelConnectorParameter : channelConnector.getChannelConnectorParameters()) {
                        scriptContext.addVariable(channelConnectorParameter.getName(), channelConnectorParameter.getValue());
                    }
                }
                for (ChannelConfigurationParameter channelConfigurationParameter : channel.getChannelConfigurationParameters()) {
                    scriptContext.addVariable(channelConfigurationParameter.getSourceConfigurationParameterName(), channelConfigurationParameter.getValue());
                }
                scriptContext.setTraceLoggingEnabled(UserContext.current().isTraceLoggingEnabled());
                scraperScript.execute();
            } catch (Exception e) {
                LOG.error("unable to execute post invoice script", e);
            } finally {
                ScriptExecutionContext.destroy();
            }
        }
    }

    private void mapSaleOrderItemsToInvoiceItems(Invoice invoice, Map<String, List<SaleOrderItem>> identifierToSaleOrderItems) {
        invoice.getInvoiceItems().forEach(invoiceItem -> {
            BigDecimal perUnitPrice = NumberUtils.divide(invoiceItem.getTotal(), invoiceItem.getQuantity());
            String bundleSkuCode = invoiceItem.getBundle() != null ? invoiceItem.getBundle().getItemType().getSkuCode() : null;
            identifierToSaleOrderItems.get(
                    WsInvoiceItem.generateInvoiceItemIdentifier(WsInvoice.Source.SHIPPING_PACKAGE, invoiceItem.getSellerSkuCode(), bundleSkuCode, perUnitPrice)).forEach(
                            saleOrderItem -> {
                                if (invoiceItem.getInvoiceSaleOrderItems() == null) {
                                    invoiceItem.setInvoiceSaleOrderItems(new HashSet<>());
                                }
                                invoiceItem.getInvoiceSaleOrderItems().add(new InvoiceSaleOrderItem(invoiceItem, saleOrderItem));
                                if (Invoice.Type.SALE.equals(invoice.getType())) {
                                    saleOrderItem.setInvoiceItem(invoiceItem);
                                } else if (Invoice.Type.SALE_RETURN.equals(invoice.getType())) {
                                    saleOrderItem.setReturnInvoiceItem(invoiceItem);
                                }

                            });
        });
    }

    @Override
    public BulkCreateInvoiceResponse bulkCreateInvoice(BulkCreateInvoiceRequest request) {
        BulkCreateInvoiceResponse response = new BulkCreateInvoiceResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            for (String shippingPackageCode : request.getShippingPackageCodes()) {
                CreateShippingPackageInvoiceRequest createShippingPackageInvoiceRequest = new CreateShippingPackageInvoiceRequest();
                createShippingPackageInvoiceRequest.setUserId(request.getUserId());
                createShippingPackageInvoiceRequest.setShippingPackageCode(shippingPackageCode);
                createShippingPackageInvoiceRequest.setCommitBlockedInventory(true);
                CreateShippingPackageInvoiceResponse createShippingPackageInvoiceResponse = createShippingPackageInvoice(createShippingPackageInvoiceRequest);
                if (!createShippingPackageInvoiceResponse.isSuccessful()) {
                    response.getFailures().put(shippingPackageCode, createShippingPackageInvoiceResponse.getErrors());
                } else {
                    response.getSuccessfulPackages().add(shippingPackageCode);
                }
            }
            if (response.getFailures().size() == 0) {
                response.setSuccessful(true);
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional(readOnly = true)
    public GetInvoiceDetailsResponse getInvoiceDetails(GetInvoiceDetailsRequest request) {
        GetInvoiceDetailsResponse response = new GetInvoiceDetailsResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            ShippingPackage shippingPackage = shippingService.getShippingPackageByCode(request.getShippingPackageCode());
            if (shippingPackage == null) {
                context.addError(WsResponseCode.INVALID_SHIPPING_PACKAGE_CODE);
            } else if (shippingPackage.getInvoice() == null) {
                context.addWarning(WsResponseCode.INVOICE_NOT_GENERATED);
            } else {
                Invoice invoice;
                if (request.getReturn() != null && request.getReturn()) {
                    invoice = shippingPackage.getReturnInvoice();
                } else {
                    invoice = shippingPackage.getInvoice();
                }
                InvoiceDTO invoiceDTO = prepareShippingPackageInvoiceDTO(invoice, shippingPackage);
                response.setInvoice(invoiceDTO);
                response.setSuccessful(true);
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    @Override
    @Transactional(readOnly = true)
    public GetAllInvoicesDetailsResponse getAllShippingPackageInvoicesDetails(GetAllInvoicesDetailsRequest request) {
        GetAllInvoicesDetailsResponse response = new GetAllInvoicesDetailsResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            ShippingPackage shippingPackage = shippingService.getShippingPackageByCode(request.getShippingPackageCode());
            if (shippingPackage == null) {
                context.addError(WsResponseCode.INVALID_SHIPPING_PACKAGE_CODE);
            } else if (shippingPackage.getInvoice() == null && shippingPackage.getReturnInvoice() == null) {
                context.addWarning(WsResponseCode.INVOICE_NOT_GENERATED);
            } else {
                List<InvoiceDTO> invoiceDTOs = new ArrayList<InvoiceDTO>();
                Invoice invoice = shippingPackage.getInvoice();
                if (invoice != null) {
                    InvoiceDTO invoiceDTO = prepareShippingPackageInvoiceDTO(invoice, shippingPackage);
                    invoiceDTOs.add(invoiceDTO);
                }
                Invoice returnInvoice = shippingPackage.getReturnInvoice();
                if (returnInvoice != null) {
                    InvoiceDTO returnInvoiceDTO = prepareShippingPackageInvoiceDTO(returnInvoice, shippingPackage);
                    invoiceDTOs.add(returnInvoiceDTO);
                }
                response.setInvoices(invoiceDTOs);
                response.setSuccessful(true);
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    private InvoiceDTO prepareShippingPackageInvoiceDTO(Invoice invoice, ShippingPackage shippingPackage) {
        InvoiceDTO invoiceDTO = invoiceService.prepareInvoiceDTO(invoice);
        invoiceDTO.setShippingPackageCode(shippingPackage.getCode());
        invoiceDTO.setPaymentMode(shippingPackage.getSaleOrder().getPaymentMethod().getCode());
        return invoiceDTO;
    }

    @Override
    public CreateInvoiceWithDetailsResponse createInvoiceWithDetails(CreateInvoiceWithDetailsRequest request) {
        CreateInvoiceWithDetailsResponse response = new CreateInvoiceWithDetailsResponse();
        ValidationContext context = request.validate();
        ShippingPackage shippingPackage = null;
        if (!context.hasErrors()) {
            SaleOrder saleOrder = saleOrderService.getSaleOrderByCode(request.getSaleOrderCode());
            if (saleOrder == null) {
                context.addError(WsResponseCode.INVALID_SALE_ORDER_CODE, "Invalid sale order code");
            } else {
                Channel channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelById(saleOrder.getChannel().getId());
                refactorCreateInvoiceWithDetailsRequestForBundle(request, channel, context);
                if (!context.hasErrors()) {
                    shippingPackage = getValidShippingPackage(request);
                    if (shippingPackage == null) {
                        context.addError(WsResponseCode.INVALID_SHIPPING_PACKAGE_CODE, "Sale order item codes do not correspond to a package");
                    } else {
                        try {
                            createInvoiceWithDetailsInternal(request, response, context, shippingPackage);
                        } catch (Throwable t) {
                            LOG.error("Failed to create invoice", t);
                            throw t;
                        }
                        if (!context.hasErrors()) {
                            executePostInvoiceScript(shippingPackage);
                        }
                    }
                }
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    /**
     * In case of bundle : we initially make request by bundle saleOrderItemCode as saleOrderItemCode in WsInvoiceItem.
     * So we will not get a valid shipping package with that saleOrderItemCode which is used as combination_identifier
     * in SaleOrderItem. Hence we replace saleOrderItemCode in WsInvoiceItem with actual saleOrderItemCodes in case of
     * BUNDLE and also change the corresponding request.
     */
    @Transactional
    private ShippingPackage getValidShippingPackage(CreateInvoiceWithDetailsRequest request) {
        List<String> saleOrderItemCodes = new ArrayList<>();
        ShippingPackage shippingPackage = null;
        // Invoice Item might contain bundle sku so we need to split.
        for (WsInvoiceItem wsInvoiceItem : request.getInvoice().getInvoiceItems()) {
            saleOrderItemCodes.addAll(wsInvoiceItem.getSaleOrderItemCodes());
        }
        shippingPackage = shippingService.getValidShippingPackage(request.getSaleOrderCode(), saleOrderItemCodes);
        return shippingPackage;
    }

    @Transactional
    public void refactorCreateInvoiceWithDetailsRequestForBundle(CreateInvoiceWithDetailsRequest request, Channel channel, ValidationContext context) {
        SaleOrder saleOrder = saleOrderService.getSaleOrderByCode(request.getSaleOrderCode());
        Map<String, List<String>> identifierToSoiCodes = new HashMap<>();
        Map<String, WsInvoiceItem> identifierToInvoiceItem = new HashMap<>();
        saleOrder.getSaleOrderItems().forEach(saleOrderItem -> identifierToSoiCodes.computeIfAbsent(WsInvoiceItem.generateInvoiceItemIdentifier(WsInvoice.Source.SHIPPING_PACKAGE, saleOrderItem.getItemType().getSkuCode(),
                saleOrderItem.getBundleSkuCode(), null), k -> new ArrayList<>()).add(saleOrderItem.getCode()));
        Iterator<WsInvoiceItem> invoiceItemIterator = request.getInvoice().getInvoiceItems().iterator();
        List<WsInvoiceItem> invoiceItemsToAdd = new ArrayList<>();
        while (invoiceItemIterator.hasNext() && !context.hasErrors()) {
            WsInvoiceItem wsInvoiceItem = invoiceItemIterator.next();
            ItemType itemType = null;
            if (StringUtils.isNotBlank(wsInvoiceItem.getSkuCode())) {
                itemType = catalogService.getItemTypeBySkuCode(wsInvoiceItem.getSkuCode());
            } else if (StringUtils.isNotBlank(wsInvoiceItem.getChannelProductId())) {
                ChannelItemType channelItemType = channelCatalogService.getChannelItemTypeByChannelAndChannelProductId(channel.getCode(), wsInvoiceItem.getChannelProductId());
                if (channelItemType == null || !ChannelItemType.Status.LINKED.equals(channelItemType.getStatusCode())) {
                    context.addError(WsResponseCode.INVALID_CHANNEL_ITEM_TYPE, "ChannelItemType not found for " + wsInvoiceItem.getChannelProductId());
                } else {
                    itemType = channelItemType.getItemType();
                    wsInvoiceItem.setChannelProductId(channelItemType.getChannelProductId());
                    wsInvoiceItem.setSkuCode(itemType.getSkuCode());
                }
            }
            if (itemType == null) {
                context.addError(WsResponseCode.INVALID_ITEM_SKU_CODE, "Invalid Item Sku Code in Invoice Item");
            } else if (!context.hasErrors()) {
                if (ItemType.Type.BUNDLE.equals(itemType.getType())) {
                    Bundle bundle = bundleService.getBundleBySkuCode(itemType.getSkuCode());
                    bundle.getBundleItemTypes().forEach(bundleItemType -> {
                        String identifier = WsInvoiceItem.generateInvoiceItemIdentifier(WsInvoice.Source.SHIPPING_PACKAGE, bundleItemType.getItemType().getSkuCode(), bundle.getItemType().getSkuCode(), null);
                        if (identifierToInvoiceItem.get(identifier) != null) {
                            mergeBundleComponentWithWsInvoiceItem(bundle, identifierToInvoiceItem.get(identifier), bundleItemType, wsInvoiceItem,
                                    identifierToSoiCodes.get(identifier));
                        } else {
                            WsInvoiceItem bundleComponentInvoiceItem = prepareWsInvoiceItemForBundleItemType(bundle, bundleItemType, wsInvoiceItem,
                                    identifierToSoiCodes.get(identifier));
                            invoiceItemsToAdd.add(bundleComponentInvoiceItem);
                            identifierToInvoiceItem.put(identifier, bundleComponentInvoiceItem);
                        }
                    });
                    invoiceItemIterator.remove();
                }
            }
        }
        request.getInvoice().getInvoiceItems().addAll(invoiceItemsToAdd);
    }

    private void mergeBundleComponentWithWsInvoiceItem(Bundle bundle, WsInvoiceItem wsInvoiceItem, BundleItemType bundleItemType, WsInvoiceItem bundleInvoiceItem, List<String> soiCodes) {
        WsInvoiceItem newInvoiceItem = prepareWsInvoiceItemForBundleItemType(bundle, bundleItemType, bundleInvoiceItem, soiCodes);
        wsInvoiceItem.setSaleOrderItemCodes(soiCodes);
        wsInvoiceItem.setUnitPrice(wsInvoiceItem.getUnitPrice());
        wsInvoiceItem.setSubtotal(wsInvoiceItem.getSubtotal().add(newInvoiceItem.getSubtotal()));
        wsInvoiceItem.setDiscount(wsInvoiceItem.getDiscount().add(newInvoiceItem.getDiscount()));
        wsInvoiceItem.setShippingCharges(wsInvoiceItem.getShippingCharges().add(newInvoiceItem.getShippingCharges()));
        wsInvoiceItem.setCashOnDeliveryCharges(wsInvoiceItem.getCashOnDeliveryCharges().add(newInvoiceItem.getCashOnDeliveryCharges()));
        wsInvoiceItem.setShippingMethodCharges(wsInvoiceItem.getShippingMethodCharges().add(newInvoiceItem.getShippingMethodCharges()));
        wsInvoiceItem.setTotal(wsInvoiceItem.getTotal().add(newInvoiceItem.getTotal()));
        wsInvoiceItem.setPrepaidAmount(wsInvoiceItem.getPrepaidAmount().add(newInvoiceItem.getPrepaidAmount()));
        wsInvoiceItem.setVoucherValue(wsInvoiceItem.getVoucherValue().add(newInvoiceItem.getVoucherValue()));
        wsInvoiceItem.setServiceTax(wsInvoiceItem.getServiceTax().add(newInvoiceItem.getServiceTax()));
        wsInvoiceItem.setAdditionalTax(wsInvoiceItem.getAdditionalTax().add(newInvoiceItem.getAdditionalTax()));
        wsInvoiceItem.setGiftWrapCharges(wsInvoiceItem.getGiftWrapCharges().add(newInvoiceItem.getGiftWrapCharges()));
        wsInvoiceItem.setStoreCredit(wsInvoiceItem.getStoreCredit().add(newInvoiceItem.getStoreCredit()));
        wsInvoiceItem.setQuantity(wsInvoiceItem.getQuantity() + newInvoiceItem.getQuantity());
        if (wsInvoiceItem.getCustomFieldValues() != null) {
            wsInvoiceItem.getCustomFieldValues().addAll(newInvoiceItem.getCustomFieldValues());
        }

    }

    private WsInvoiceItem prepareWsInvoiceItemForBundleItemType(Bundle bundle, BundleItemType bundleItemType, WsInvoiceItem bundleInvoiceItem, List<String> soiCodes) {
        WsInvoiceItem wsInvoiceItem = new WsInvoiceItem();
        wsInvoiceItem.setSkuCode(bundleItemType.getItemType().getSkuCode());
        wsInvoiceItem.setBundleSkuCode(bundle.getItemType().getSkuCode());
        wsInvoiceItem.setUnitPrice(bundleInvoiceItem.getUnitPrice().multiply(bundleItemType.getPriceRatio()));
        wsInvoiceItem.setSubtotal(bundleInvoiceItem.getSubtotal().multiply(bundleItemType.getPriceRatio()).multiply(BigDecimal.valueOf(bundleItemType.getQuantity())));
        wsInvoiceItem.setDiscount(bundleInvoiceItem.getDiscount().multiply(bundleItemType.getPriceRatio()).multiply(BigDecimal.valueOf(bundleItemType.getQuantity())));
        wsInvoiceItem.setShippingCharges(
                bundleInvoiceItem.getShippingCharges().multiply(bundleItemType.getPriceRatio()).multiply(BigDecimal.valueOf(bundleItemType.getQuantity())));
        wsInvoiceItem.setCashOnDeliveryCharges(
                bundleInvoiceItem.getCashOnDeliveryCharges().multiply(bundleItemType.getPriceRatio()).multiply(BigDecimal.valueOf(bundleItemType.getQuantity())));
        wsInvoiceItem.setShippingMethodCharges(
                bundleInvoiceItem.getShippingMethodCharges().multiply(bundleItemType.getPriceRatio()).multiply(BigDecimal.valueOf(bundleItemType.getQuantity())));
        wsInvoiceItem.setTotal(bundleInvoiceItem.getTotal().multiply(bundleItemType.getPriceRatio()).multiply(BigDecimal.valueOf(bundleItemType.getQuantity())));
        wsInvoiceItem.setPrepaidAmount(bundleInvoiceItem.getPrepaidAmount().multiply(bundleItemType.getPriceRatio()).multiply(BigDecimal.valueOf(bundleItemType.getQuantity())));
        wsInvoiceItem.setVoucherValue(bundleInvoiceItem.getVoucherValue().multiply(bundleItemType.getPriceRatio()).multiply(BigDecimal.valueOf(bundleItemType.getQuantity())));
        wsInvoiceItem.setServiceTax(bundleInvoiceItem.getServiceTax().multiply(bundleItemType.getPriceRatio()).multiply(BigDecimal.valueOf(bundleItemType.getQuantity())));
        wsInvoiceItem.setAdditionalTax(bundleInvoiceItem.getAdditionalTax().multiply(bundleItemType.getPriceRatio()).multiply(BigDecimal.valueOf(bundleItemType.getQuantity())));
        wsInvoiceItem.setGiftWrapCharges(
                bundleInvoiceItem.getGiftWrapCharges().multiply(bundleItemType.getPriceRatio()).multiply(BigDecimal.valueOf(bundleItemType.getQuantity())));
        wsInvoiceItem.setStoreCredit(bundleInvoiceItem.getStoreCredit().multiply(bundleItemType.getPriceRatio()).multiply(BigDecimal.valueOf(bundleItemType.getQuantity())));
        wsInvoiceItem.setCustomFieldValues(bundleInvoiceItem.getCustomFieldValues());
        wsInvoiceItem.setTaxPercentageDetail(bundleInvoiceItem.getTaxPercentageDetail());
        wsInvoiceItem.setAdditionalInfo(bundleInvoiceItem.getAdditionalInfo());
        wsInvoiceItem.setItemDetails(bundleInvoiceItem.getItemDetails());
        wsInvoiceItem.setQuantity(bundleInvoiceItem.getQuantity());
        wsInvoiceItem.setSaleOrderItemCodes(soiCodes);
        return wsInvoiceItem;
    }

    @Locks({ @Lock(ns = Namespace.SALE_ORDER, key = "#{#args[3].saleOrder.code}") })
    @Transactional
    @LogActivity
    @RollbackOnFailure
    private CreateInvoiceWithDetailsResponse createInvoiceWithDetailsInternal(CreateInvoiceWithDetailsRequest request, CreateInvoiceWithDetailsResponse response,
            ValidationContext context, ShippingPackage shippingPackage) {
        SaleOrder saleOrder = saleOrderService.getSaleOrderForUpdate(shippingPackage.getSaleOrder().getId());
        shippingPackage = shippingService.getShippingPackageByCode(shippingPackage.getCode());
        List<String> shippingPackageCodes = new ArrayList<>(1);
        shippingPackageCodes.add(shippingPackage.getCode());
        List<SaleOrderItem> saleOrderItemsWithPendingDetailingStatus = shippingService.getSaleOrderItemsWithPendingDetailingStatus(shippingPackageCodes);
        Map<String, List<ItemDetail>> saleOrderItemCodeToItemDetailing = new HashMap<>();
        List<String> requestedSoiCodes = new ArrayList<>();
        for (WsInvoiceItem wsInvoiceItem : request.getInvoice().getInvoiceItems()) {
            requestedSoiCodes.addAll(wsInvoiceItem.getSaleOrderItemCodes());
            wsInvoiceItem.setQuantity(wsInvoiceItem.getSaleOrderItemCodes().size());
            if (wsInvoiceItem.getSaleOrderItems() != null) {
                for (WsInvoiceItem.WsSaleOrderItem wsSaleOrderItem : wsInvoiceItem.getSaleOrderItems()) {
                    if (wsSaleOrderItem.getItemDetails() != null && !wsSaleOrderItem.getItemDetails().isEmpty()) {
                        saleOrderItemCodeToItemDetailing.put(wsSaleOrderItem.getSaleOrderItemCode(), wsSaleOrderItem.getItemDetails());
                    }
                }
            }
        }
        if (!context.hasErrors()) {
            if (!saleOrderItemsWithPendingDetailingStatus.isEmpty() && saleOrderItemsWithPendingDetailingStatus.size() != saleOrderItemCodeToItemDetailing.size()) {
                context.addError(WsResponseCode.INVALID_SALE_ORDER_LINE_ITEM, "Item Detailing not present for required sale order items");
            } else if (saleOrderItemsWithPendingDetailingStatus.size() > 0) {
                for (SaleOrderItem saleOrderItem : saleOrderItemsWithPendingDetailingStatus) {
                    if (!context.hasErrors() && requestedSoiCodes.contains(saleOrderItem.getCode())) {
                        if (saleOrderItemCodeToItemDetailing.get(saleOrderItem.getCode()) == null) {
                            context.addError(WsResponseCode.INVALID_SALE_ORDER_LINE_ITEM, "Item Detailing not present for required sale order item :" + saleOrderItem.getCode());
                        } else {
                            AddSaleOrderItemDetailRequest addSaleOrderItemDetailRequest = new AddSaleOrderItemDetailRequest();
                            addSaleOrderItemDetailRequest.setSaleOrderCode(saleOrder.getCode());
                            addSaleOrderItemDetailRequest.setSaleOrderItemCode(saleOrderItem.getCode());
                            addSaleOrderItemDetailRequest.setItemDetails(saleOrderItemCodeToItemDetailing.get(saleOrderItem.getCode()));
                            AddSaleOrderItemDetailResponse addSaleOrderItemDetailResponse = inflowService.addSaleOrderItemDetails(addSaleOrderItemDetailRequest);
                            if (!addSaleOrderItemDetailResponse.isSuccessful()) {
                                context.addErrors(addSaleOrderItemDetailResponse.getErrors());
                            }
                        }
                    }
                }
            }
        }

        if (!context.hasErrors() && isPackageReadyForInvoicing(shippingPackage, context, false)) {
            WsInvoice wsInvoice = request.getInvoice();
            wsInvoice.setType(Invoice.Type.SALE);
            wsInvoice.setChannelCode(saleOrder.getChannel().getCode());
            wsInvoice.setSource(WsInvoice.Source.SHIPPING_PACKAGE);
            wsInvoice.setUserId(request.getUserId());
            setDestinationCodeForInvoice(saleOrder, wsInvoice, shippingPackage.getShippingAddress());
            if (saleOrder.getChannel().getBillingParty() != null) {
                wsInvoice.setFromPartyCode(saleOrder.getChannel().getBillingParty().getCode());
            } else {
                wsInvoice.setFromPartyCode(saleOrder.getChannel().getAssociatedFacility().getCode());
            }
            if (saleOrder.getCustomer() != null) {
                wsInvoice.setToPartyCode(saleOrder.getCustomer().getCode());
            }
            GenerateOrUpdateInvoiceRequest generateInvoiceRequest = new GenerateOrUpdateInvoiceRequest();
            generateInvoiceRequest.setInvoice(wsInvoice);
            GenerateOrUpdateInvoiceResponse generateInvoiceResponse = generateInvoiceAndMapInvoiceToShippingPackage(context, shippingPackage, generateInvoiceRequest, true,
                    request.getUserId());
            if (generateInvoiceResponse.isSuccessful()) {
                response.setShippingPackageCode(shippingPackage.getCode());
                response.setInvoiceCode(generateInvoiceResponse.getInvoiceCode());
                String statusCode = shippingPackage.getStatusCode();
                ShippingMethod shippingMethod = ConfigurationManager.getInstance().getConfiguration(ShippingConfiguration.class).getShippingMethodById(
                        shippingPackage.getShippingMethod().getId());
                if (!ShippingMethod.Code.PKP.name().equals(shippingMethod.getCode())) {
                    AssignManualShippingProviderRequest assignManualShippingProviderRequest = new AssignManualShippingProviderRequest();
                    assignManualShippingProviderRequest.setShippingPackageCode(shippingPackage.getCode());
                    assignManualShippingProviderRequest.setShippingProviderCode(
                            StringUtils.isNotBlank(request.getShippingProviderCode()) ? request.getShippingProviderCode() : "SELF");
                    assignManualShippingProviderRequest.setTrackingNumber(StringUtils.isNotBlank(request.getTrackingNumber()) ? request.getTrackingNumber()
                            : shippingPackage.getCode() + shippingPackage.getSaleOrder().getChannel().getShortName());
                    AssignManualShippingProviderResponse assignManualShippingProviderResponse = shippingProviderService.assignManualShippingProvider(
                            assignManualShippingProviderRequest);
                    if (assignManualShippingProviderResponse.hasErrors()) {
                        context.addErrors(assignManualShippingProviderResponse.getErrors());
                    } else {
                        statusCode = ShippingPackage.StatusCode.READY_TO_SHIP.name();
                    }
                }
                if (!context.hasErrors()) {
                    if (ActivityContext.current().isEnable()) {
                        ActivityUtils.appendActivity(shippingPackage.getCode(), ActivityEntityEnum.SHIPPING_PACKAGE.getName(), shippingPackage.getSaleOrder().getCode(),
                                Arrays.asList(new String[] {
                                        "Shipment package {" + ActivityEntityEnum.SHIPPING_PACKAGE + ":" + shippingPackage.getCode() + "} moved to " + statusCode
                                                + " with invoice code {" + ActivityEntityEnum.INVOICE + ":" + generateInvoiceResponse.getInvoiceCode() + "}" }),
                                ActivityTypeEnum.PROCESSING.name());
                    }
                }
            }
        }
        if (!context.hasErrors()) {
            response.setSuccessful(true);
            invoicePublisherService.publish(shippingPackage);
        }
        return response;
    }

    private void setDestinationCodeForInvoice(SaleOrder saleOrder, WsInvoice wsInvoice, AddressDetail shippingAddress) {
        Party customer = saleOrder.getCustomer();
        if (customer != null) {
            LOG.debug("Setting to party : {} for saleOrder {}", customer.getCode(), saleOrder.getCode());
            wsInvoice.setToPartyCode(customer.getCode());
            PartyAddress customerAddress = customer.getPartyAddressByType(PartyAddressType.Code.SHIPPING.name());
            wsInvoice.setDestinationStateCode(customerAddress.getStateCode());
            wsInvoice.setDestinationCountryCode(customerAddress.getCountryCode());
        } else {
            wsInvoice.setDestinationStateCode(shippingAddress.getStateCode());
            wsInvoice.setDestinationCountryCode(shippingAddress.getCountryCode());
        }
    }

    @Override
    @Transactional(readOnly = true)
    public GetInvoiceDetailsByCodeResponse getInvoiceDetailsByCode(GetInvoiceDetailsByCodeRequest request) {
        GetInvoiceDetailsByCodeResponse response = new GetInvoiceDetailsByCodeResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            Invoice invoice = invoiceService.getInvoiceByCode(request.getInvoiceCode());
            if (invoice == null) {
                context.addError(WsResponseCode.INVALID_CODE, "Invalid invoice code");
            } else {
                ShippingPackage shippingPackage = shippingService.getShippingPackageByInvoiceId(invoice.getId());
                if (shippingPackage == null) {
                    context.addError(WsResponseCode.INVALID_CODE, "No Shipping package for requested invoice");
                } else {
                    InvoiceDTO invoiceDTO = prepareShippingPackageInvoiceDTO(invoice, shippingPackage);
                    response.setInvoice(invoiceDTO);
                    response.setSuccessful(true);
                }
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    @Override
    public CreateShippingPackageReverseInvoiceResponse createShippingPackageReverseInvoice(CreateShippingPackageReverseInvoiceRequest request) {
        CreateShippingPackageReverseInvoiceResponse response = new CreateShippingPackageReverseInvoiceResponse();
        ValidationContext context = request.validate();
        ShippingPackage shippingPackage = null;
        SaleOrder saleOrder = null;
        Set<String> saleOrderItems = new HashSet<>();
        if (StringUtils.isBlank(request.getShippingPackageCode()) && StringUtils.isBlank(request.getSaleOrderCode())) {
            context.addError(WsResponseCode.INVALID_REQUEST, "Either shippingPackageCode or saleOrderCode is required");
        }
        if (!context.hasErrors()) {
            if (StringUtils.isNotBlank(request.getShippingPackageCode())) {
                shippingPackage = shippingService.getShippingPackageByCode(request.getShippingPackageCode());
                if (shippingPackage == null) {
                    context.addError(WsResponseCode.INVALID_SHIPPING_PACKAGE_CODE, "Invalid package code");
                } else if (!shippingPackage.isReadyForReverseInvoicing()) {
                    context.addError(WsResponseCode.INVALID_PACKAGE_STATE, "Package not ready for reverse invoicing");
                }
            }
            if (StringUtils.isNotBlank(request.getSaleOrderCode())) {
                saleOrder = saleOrderService.getSaleOrderByCode(request.getSaleOrderCode());
                if (saleOrder == null) {
                    context.addError(WsResponseCode.INVALID_SALE_ORDER_CODE, "Invalid sale order code");
                }
            }
            if (shippingPackage == null && saleOrder == null) {
                context.addError(WsResponseCode.INVALID_REQUEST, "Invalid package/saleOrder Code");
            } else if (CollectionUtils.isEmpty(request.getSaleOrderItemCodes()) && shippingPackage == null) {
                context.addError(WsResponseCode.INVALID_REQUEST, "Shipping Package Code is required");
            }
        }
        if (!context.hasErrors()) {
            if (CollectionUtils.isEmpty(request.getSaleOrderItemCodes())) {
                try {
                    shippingPackage.getSaleOrderItems().forEach(saleOrderItem -> saleOrderItems.add(saleOrderItem.getCode()));
                    createShippingPackageReverseInvoiceInternal(request, response, context, shippingPackage.getSaleOrder(), saleOrderItems, shippingPackage);
                } catch (Throwable t) {
                    LOG.error("Failed to create invoice", t);
                    throw t;
                }
            } else {
                if (shippingPackage != null) {
                    shippingPackage.getSaleOrderItems().stream().filter(
                            soi -> request.getSaleOrderItemCodes().contains(soi.getCode()) && soi.getReturnInvoiceItem() == null).forEach(
                                    saleOrderItem -> saleOrderItems.add(saleOrderItem.getCode()));
                    saleOrder = shippingPackage.getSaleOrder();
                } else if (saleOrder != null) {
                    saleOrder.getSaleOrderItems().stream().filter(soi -> request.getSaleOrderItemCodes().contains(soi.getCode()) && soi.getReturnInvoiceItem() == null).forEach(
                            saleOrderItem -> saleOrderItems.add(saleOrderItem.getCode()));
                }
                if (saleOrderItems.size() != request.getSaleOrderItemCodes().size()) {
                    context.addError(WsResponseCode.INVALID_SALE_ORDER_ITEM_CODE,
                            "Not all sale order item codes belong to same order/package or Return Invoice Item already exist");
                } else {
                    try {
                        createShippingPackageReverseInvoiceInternal(request, response, context, saleOrder, saleOrderItems, shippingPackage);
                    } catch (Throwable t) {
                        LOG.error("Failed to create invoice", t);
                        throw t;
                    }
                }
            }
        }
        response.addWarnings(context.getWarnings());
        if (context.hasErrors()) {
            response.addErrors(context.getErrors());
        } else {
            response.setSuccessful(true);
        }
        return response;
    }

    @Locks({ @Lock(ns = Namespace.SALE_ORDER, key = "#{#args[3].code}") })
    @Transactional
    @LogActivity
    private void createShippingPackageReverseInvoiceInternal(CreateShippingPackageReverseInvoiceRequest request, CreateShippingPackageReverseInvoiceResponse response,
            ValidationContext context, SaleOrder saleOrder, Set<String> saleOrderItemCodes, ShippingPackage shippingPackage) {
        if (shippingPackage == null || shippingPackage.getReturnInvoice() == null) {
            saleOrder = saleOrderService.getSaleOrderForUpdate(saleOrder.getId());
            Set<SaleOrderItem> saleOrderItems = new HashSet<>(saleOrderItemCodes.size());
            saleOrder.getSaleOrderItems().stream().filter(soi -> saleOrderItemCodes.contains(soi.getCode())).forEach(saleOrderItems::add);
            BillingParty billingParty = saleOrder.getChannel().getBillingParty();
            Party fromParty = billingParty != null ? billingParty : CacheManager.getInstance().getCache(FacilityCache.class).getCurrentFacility();
            AddressDetail shippingAddressDetail = shippingPackage != null ? shippingPackage.getShippingAddress() : saleOrderItems.iterator().next().getShippingAddress();
            String invoiceCode = null;
            List<String> cancelledSaleOrderItemCodes = new ArrayList<>();
            if (StringUtils.isNotBlank(request.getInvoiceCode())) {
                invoiceCode = request.getInvoiceCode();
            } else if (billingParty != null) {
                String sequenceName = billingParty.getSaleReturnInvoiceSequence();
                if (sequenceName != null) {
                    try {
                        invoiceCode = iSequenceGenerator.generateNextInSameTransaction(sequenceName);
                    } catch (IllegalArgumentException e) {
                        context.addError(WsResponseCode.FACILITY_CONFIG_ERROR,
                                "Invoice Sequence not set for Facility/Billing Party for creating invoices, required sequence : " + sequenceName);
                    }
                }
            }
            if (!context.hasErrors()) {
                GenerateOrUpdateInvoiceRequest generateInvoiceRequest = prepareGenerateInvoiceRequest(Invoice.Type.SALE_RETURN, saleOrder, shippingAddressDetail, fromParty,
                        request.getUserId(), saleOrderItems, invoiceCode, null, null, null);
                GenerateOrUpdateInvoiceResponse generateInvoiceResponse = invoiceService.createInvoice(generateInvoiceRequest);
                if (generateInvoiceResponse.hasErrors()) {
                    context.addErrors(generateInvoiceResponse.getErrors());
                } else {
                    Invoice invoice = invoiceService.getInvoiceByCode(generateInvoiceResponse.getInvoiceCode());
                    Map<String, List<SaleOrderItem>> identifierToSaleOrderItems = new HashMap<>();
                    saleOrderItems.forEach(
                            saleOrderItem -> identifierToSaleOrderItems.computeIfAbsent(WsInvoiceItem.generateInvoiceItemIdentifier(WsInvoice.Source.SHIPPING_PACKAGE,
                                    saleOrderItem.getItemType().getSkuCode(), saleOrderItem.getBundleSkuCode(), saleOrderItem.getSellingPrice()), k -> new ArrayList<>()).add(
                                            saleOrderItem));
                    mapSaleOrderItemsToInvoiceItems(invoice, identifierToSaleOrderItems);
                    invoiceService.updateInvoice(invoice);
                    if (shippingPackage != null) {
                        shippingPackage.setReturnInvoice(invoice);
                        shippingService.updateShippingPackage(shippingPackage);
                        response.setShippingPackageCode(shippingPackage.getCode());
                    }
                    response.setInvoiceCode(invoice.getCode());
                    response.setInvoiceDisplayCode(invoice.getDisplayCode());
                    if (!context.hasErrors()) {
                        if (ActivityContext.current().isEnable()) {
                            if (shippingPackage != null) {
                                ActivityUtils.appendActivity(shippingPackage.getCode(), ActivityEntityEnum.SHIPPING_PACKAGE.getName(), shippingPackage.getSaleOrder().getCode(),
                                        Arrays.asList(new String[] { "Invoice for Return {" + ActivityEntityEnum.INVOICE + ":" + invoice.getCode() + "} updated" }),
                                        ActivityTypeEnum.PROCESSING.name());
                            } else {
                                ActivityUtils.appendActivity(saleOrder.getCode(), ActivityEntityEnum.SALE_ORDER.getName(), saleOrder.getCode(),
                                        Arrays.asList(new String[] { "Invoice for Return {" + ActivityEntityEnum.INVOICE + ":" + invoice.getCode() + "} updated" }),
                                        ActivityTypeEnum.PROCESSING.name());
                            }

                        }
                    }
                }
            }
        } else {
            context.addWarning(WsResponseCode.INVALID_REQUEST, "Return Invoice already Exist for Given Shipping Package");
            response.setShippingPackageCode(shippingPackage.getCode());
            response.setInvoiceCode(shippingPackage.getReturnInvoice().getCode());
            response.setInvoiceDisplayCode(shippingPackage.getReturnInvoice().getDisplayCode());
        }
    }

    @Override
    public UpdateInvoiceResponse updateInvoice(UpdateInvoiceRequest request) {
        UpdateInvoiceResponse response = new UpdateInvoiceResponse();
        ValidationContext context = request.validate();
        ShippingPackage shippingPackage;
        if (!context.hasErrors()) {
            shippingPackage = shippingService.getShippingPackageByCode(request.getShippingPackageCode());
            if (shippingPackage == null) {
                context.addError(WsResponseCode.INVALID_SHIPPING_PACKAGE_CODE, "Invalid package code");
            } else if (shippingPackage.getInvoice() == null) {
                context.addError(WsResponseCode.INVALID_SHIPPING_PACKAGE_STATE, "Invoice not yet created for shipping package");
            } else if (shippingPackage.getSaleOrder().isProductManagementSwitchedOff() && request.getChannelProductIdToTax() == null) {
                context.addError(WsResponseCode.INVALID_TAX_TYPE_CODE, "Taxes not specified");
            } else {
                response = updateInvoiceInternal(request, response, context, shippingPackage);
                if (!context.hasErrors()) {
                    executePostInvoiceScript(shippingPackage);
                }
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    @Locks({ @Lock(ns = Namespace.SALE_ORDER, key = "#{#args[3].saleOrder.code}") })
    @Transactional
    @RollbackOnFailure
    private UpdateInvoiceResponse updateInvoiceInternal(UpdateInvoiceRequest request, UpdateInvoiceResponse response, ValidationContext context, ShippingPackage shippingPackage) {
        response.setErrors(context.getErrors());
        shippingPackage = shippingService.getShippingPackageByCode(shippingPackage.getCode());
        Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(shippingPackage.getSaleOrder().getChannel().getSourceCode());
        Invoice invoice = shippingPackage.getInvoice();
        Channel channel = shippingPackage.getSaleOrder().getChannel();
        String invoiceCodeFromChannel = null;
        WsShippingProviderInfo shippingProviderInfo = null;
        Map<String, WsTaxInformation.ProductTax> channelProductIdToTax = null;
        if (source.isInvoicingOfChannel()) {
            GetInvoiceTaxDetailsFromChannelRequest taxDetailsRequest = fetchInvoiceDetailsFromChannnel(shippingPackage, channel, context, false);
            if (!context.hasErrors() && taxDetailsRequest != null) {
                invoiceCodeFromChannel = channel.getCode() + "-" + taxDetailsRequest.getInvoiceCode();
                if (!invoice.getCode().equals(invoiceCodeFromChannel)) {
                    context.addError(WsResponseCode.INVALID_VENDOR_INVOICE_CODE, "Invoice code mismatch from vendor!");
                } else {
                    channelProductIdToTax = getChannelIdToProductTaxMap(taxDetailsRequest);
                    shippingProviderInfo = taxDetailsRequest.getShippingProviderInfo();
                    syncSaleOrderItemStatusFromChannel(shippingPackage.getSaleOrder(), taxDetailsRequest, context);
                }
            }
        }

        if (!context.hasErrors()) {
            GenerateOrUpdateInvoiceRequest updateInvoiceRequest = prepareGenerateInvoiceRequest(invoice.getType(), shippingPackage.getSaleOrder(),
                    shippingPackage.getShippingAddress(), invoice.getFromParty(), invoice.getUser().getId(), shippingPackage.getSaleOrderItems(), invoice.getCode(),
                    invoice.getDisplayCode(), null, channelProductIdToTax == null ? request.getChannelProductIdToTax() : channelProductIdToTax);
            GenerateOrUpdateInvoiceResponse updateInvoiceResponse = invoiceService.updateInvoice(updateInvoiceRequest);
            if (updateInvoiceResponse.isSuccessful()) {
                invoice = invoiceService.getInvoiceByCode(updateInvoiceResponse.getInvoiceCode());
                Map<String, List<SaleOrderItem>> identifierToSaleOrderItems = new HashMap<>();
                shippingPackage.getSaleOrderItems().forEach(
                        saleOrderItem -> identifierToSaleOrderItems.computeIfAbsent(WsInvoiceItem.generateInvoiceItemIdentifier(WsInvoice.Source.SHIPPING_PACKAGE,
                                saleOrderItem.getItemType().getSkuCode(), saleOrderItem.getBundleSkuCode(), saleOrderItem.getSellingPrice()), k -> new ArrayList<>()).add(
                                saleOrderItem));
                mapSaleOrderItemsToInvoiceItems(invoice, identifierToSaleOrderItems);
                invoiceService.updateInvoice(invoice);
                if (shippingProviderInfo != null) {
                    shippingProviderService.assignChannelShippingProvider(shippingPackage, shippingProviderInfo, context, false);
                }
                if (!context.hasErrors()) {
                    response.setShippingPackageCode(shippingPackage.getCode());
                    response.setInvoiceId(invoice.getId());
                    response.setSuccessful(true);
                    if (ActivityContext.current().isEnable()) {
                        ActivityUtils.appendActivity(shippingPackage.getCode(), ActivityEntityEnum.SHIPPING_PACKAGE.getName(), shippingPackage.getSaleOrder().getCode(),
                                Arrays.asList(new String[] { "Invoice {" + ActivityEntityEnum.INVOICE + ":" + invoice.getCode() + "} updated" }),
                                ActivityTypeEnum.PROCESSING.name());
                    }
                }
            } else {
                context.addErrors(updateInvoiceResponse.getErrors());
            }
        }
        return response;
    }

    @Locks({ @Lock(ns = Namespace.SALE_ORDER, key = "#{#args[0].saleOrderCode}") })
    @Override
    @Transactional
    public ChangeInvoiceCodeResponse changeInvoiceCode(ChangeInvoiceCodeRequest request) {
        ChangeInvoiceCodeResponse response = new ChangeInvoiceCodeResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            SaleOrder saleOrder = saleOrderService.getSaleOrderByCode(request.getSaleOrderCode());
            if (saleOrder == null) {
                context.addError(WsResponseCode.INVALID_REQUEST, "Invalid sale order code");
            }
        }
        if (!context.hasErrors()) {
            ShippingPackage shippingPackage = shippingService.getValidShippingPackage(request.getSaleOrderCode(), request.getSaleOrderItemCodes());
            if (shippingPackage == null) {
                context.addError(WsResponseCode.INVALID_REQUEST, "Invalid sale order items combination with given sale order");
            } else if (shippingPackage.getInvoice() == null) {
                context.addError(WsResponseCode.INVALID_REQUEST, "No invoice has been generated for these order items");
            } else {
                Invoice invoice = shippingPackage.getInvoice();
                invoice.setCode(request.getInvoiceCode());
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        response.setSuccessful(!context.hasErrors());
        return response;
    }

    @Override
    @Transactional
    public GetInvoiceLabelResponse getInvoiceLabel(GetInvoiceLabelRequest request) {
        GetInvoiceLabelResponse response = new GetInvoiceLabelResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            ShippingPackage shippingPackage = shippingService.getShippingPackageByCode(request.getShippingPackageCode());
            if (shippingPackage == null) {
                context.addError(WsResponseCode.INVALID_SHIPPING_PACKAGE_CODE, "Invalid shipping package code");
            } else {
                generateInvoiceLabel(request.getShippingPackageCode(), response);
                if (!response.hasErrors()) {
                    response.setInvoiceCode(shippingPackage.getInvoice().getCode());
                    response.setInvoiceDisplayCode(shippingPackage.getInvoice().getDisplayCode());
                    response.setSuccessful(true);
                }
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Transactional
    private void generateInvoiceLabel(String shippingPackageCode, GetInvoiceLabelResponse response) {
        PrintTemplateVO invoiceLabelTemplate = CacheManager.getInstance().getCache(PrintTemplateCache.class).getPrintTemplateByType(PrintTemplateVO.Type.INVOICE);
        String invoiceLabelHtml = getInvoiceHtmlByShippingPackageCode(shippingPackageCode);
        String labelPath = "/tmp/" + EncryptionUtils.md5Encode(shippingPackageCode) + ".pdf";
        if (StringUtils.isNotBlank(invoiceLabelHtml)) {
            File shippingLabel = new File(labelPath);
            FileOutputStream fos = null;
            try {
                fos = new FileOutputStream(shippingLabel);
                SamplePrintTemplateVO samplePrintTemplate = CacheManager.getInstance().getCache(SamplePrintTemplateCache.class).getSamplePrintTemplate(
                        invoiceLabelTemplate.getSamplePrintTemplateCode());
                pdfDocumentService.writeHtmlToPdf(fos, invoiceLabelHtml, new PdfDocumentServiceImpl.PrintOptions(samplePrintTemplate));
                response.setLabel(EncryptionUtils.base64EncodeFile(labelPath));
            } catch (FileNotFoundException e) {
                response.addError(new WsError(WsResponseCode.SHIPPING_PROVIDER_NOT_YET_ALLOCATED.code(), "Unable to generate invoice label for package " + shippingPackageCode));
            } finally {
                FileUtils.safelyCloseOutputStream(fos);
            }
        } else {
            response.addError(new WsError(WsResponseCode.SHIPPING_PROVIDER_NOT_YET_ALLOCATED.code(), "Unable to generate invoice label for package " + shippingPackageCode));
        }
    }

    @Override
    @Transactional(readOnly = true)
    public String getInvoiceHtmlByShippingPackageCode(String shippingPackageCode) {
        ShippingPackage shippingPackage = shippingService.getShippingPackageByCode(shippingPackageCode);
        if (shippingPackage != null) {
            return getInvoiceHtml(shippingPackage.getInvoice().getCode());
        }
        return null;
    }

    @Override
    @Transactional(readOnly = true)
    public String getInvoiceHtml(String invoiceCode) {
        Invoice invoice = invoiceService.getInvoiceByCode(invoiceCode);
        ShippingPackage shippingPackage = null;

        Map<String, Object> templateParams = new HashMap<>();
        switch (invoice.getType()) {
            case SALE:
                shippingPackage = shippingService.getShippingPackageByInvoiceId(invoice.getId());
                templateParams.put("shippingPackage", shippingPackage);
                break;
            case SALE_RETURN:
                shippingPackage = shippingService.getShippingPackageByReturnInvoiceId(invoice.getId());
                if (shippingPackage == null) {
                    ReversePickup reversePickup = returnsService.getReversePickupByReturnInvoiceId(invoice.getId());
                    templateParams.put("reversePickup", reversePickup);
                } else {
                    templateParams.put("shippingPackage", shippingPackage);
                }
                break;
            case PURCHASE:
                break;
            default:
                OutboundGatePass ogp = materialService.getGatePassByInvoiceId(invoice.getId());
                templateParams.put("gatepass", ogp);
                break;
        }
        if (shippingPackage != null) {
            SaleOrder saleOrder = saleOrderService.getSaleOrderById(shippingPackage.getSaleOrder().getId());
            if (ConfigurationManager.getInstance().getConfiguration(ProductConfiguration.class).isProductManagementSwitchedOff()) {
                Map<String, ChannelItemTypeTaxVO> channelProductIdToItemTypeTaxVO = new HashMap<>(shippingPackage.getSaleOrderItems().size());
                shippingPackage.getSaleOrderItems().stream().filter(soi -> channelProductIdToItemTypeTaxVO.get(soi.getChannelProductId()) == null).forEach(soi -> {
                    channelCatalogService.getChannelItemTypeTax(saleOrder.getChannel().getCode(), soi.getChannelProductId());
                });
                templateParams.put("channelProductIdToItemTypeTaxVO", channelProductIdToItemTypeTaxVO);
            }
            templateParams.put("saleOrder", saleOrder);
            Map<String, Bundle> bundleSkuCodeToBundle = new HashMap<>();
            prepareSkuToBundleMap(bundleSkuCodeToBundle, saleOrder);
            templateParams.put("skuToBundleMap", bundleSkuCodeToBundle);

        }
        templateParams.put("invoice", invoice);
        PrintTemplateVO.Type templateType = PrintTemplateVO.Type.INVOICE;
        Template template = CacheManager.getInstance().getCache(PrintTemplateCache.class).getTemplateByType(templateType.name());
        if (template != null) {
            return prepareInvoiceHtml(templateParams, template, templateType);
        }
        return null;
    }

    private void prepareSkuToBundleMap(Map<String, Bundle> bundleSkuCodeToBundle, SaleOrder saleOrder) {
        for (SaleOrderItem saleOrderItem : saleOrder.getSaleOrderItems()) {
            String bundleSkuCode = saleOrderItem.getBundleSkuCode();
            if (StringUtils.isNotBlank(bundleSkuCode) && (!bundleSkuCodeToBundle.containsKey(bundleSkuCode))) {
                Bundle bundle = bundleService.getBundleBySkuCode(bundleSkuCode);
                bundleSkuCodeToBundle.put(bundleSkuCode, bundle);
            }
        }
    }

    @Transactional(readOnly = true)
    private String prepareInvoiceHtml(Map<String, Object> templateParams, Template template, PrintTemplateVO.Type templateType) {
        Map<String, Object> params = new HashMap<>();
        params.putAll(CacheManager.getInstance().getCache(PrintTemplateCache.class).getPrintTemplateByType(templateType).getCustomizationParameters());
        params.putAll(templateParams);
        return template.evaluate(params);
    }
}
