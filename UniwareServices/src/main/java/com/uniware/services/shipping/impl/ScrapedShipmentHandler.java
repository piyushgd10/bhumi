/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 15, 2012
 *  @author singla
 */
package com.uniware.services.shipping.impl;

import java.util.List;
import java.util.Map;

import com.uniware.core.entity.ShipmentTracking;
import com.uniware.services.shipping.AbstractShipmentHandler;
import com.uniware.services.shipping.ProviderShipmentStatus;

/**
 * @author singla
 */
public class ScrapedShipmentHandler extends AbstractShipmentHandler {

    /* (non-Javadoc)
     * @see com.uniware.services.shipping.AbstractShipmentHandler#getShipmentStatuses(java.util.List)
     */
    @Override
    public Map<String, ProviderShipmentStatus> getShipmentStatuses(List<ShipmentTracking> shipments) {
        return getShippingProviderService().scrapeShipmentStatuses(getShippingProvider(), getTrackingNumbers(shipments));
    }

}
