/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 2, 2012
 *  @author singla
 */
package com.uniware.services.shipping.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.utils.BatchProcessor;
import com.unifier.core.utils.CollectionUtils;
import com.unifier.core.utils.DateUtils;
import com.unifier.services.aspect.RollbackOnFailure;
import com.uniware.core.api.shipping.ShipmentTrackingSyncStatusDTO;
import com.uniware.core.api.shipping.SyncShipmentTrackingStatusRequest;
import com.uniware.core.api.shipping.SyncShipmentTrackingStatusResponse;
import com.uniware.core.api.shipping.SyncTrackingStatusForAWBRequest;
import com.uniware.core.api.shipping.SyncTrackingStatusForAWBResponse;
import com.uniware.core.api.shipping.UpdateShipmentTrackingResponse;
import com.uniware.core.api.shipping.UpdateTrackingStatusResponse;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.entity.ShipmentTracking;
import com.uniware.core.entity.ShipmentTrackingStatus;
import com.uniware.core.entity.ShippingPackage;
import com.uniware.core.entity.ShippingPackageStatus;
import com.uniware.core.entity.ShippingProvider;
import com.uniware.core.entity.ShippingProvider.SyncStatus;
import com.uniware.core.locking.Namespace;
import com.uniware.core.locking.annotation.Lock;
import com.uniware.core.locking.annotation.Locks;
import com.uniware.dao.shipping.IShippingDao;
import com.uniware.dao.shipping.IShippingMao;
import com.uniware.services.configuration.ShippingConfiguration;
import com.uniware.services.configuration.ShippingSourceConfiguration;
import com.uniware.services.shipping.AbstractShipmentHandler;
import com.uniware.services.shipping.IShipmentTrackingService;
import com.uniware.services.shipping.IShippingService;
import com.uniware.services.shipping.ProviderShipmentStatus;

/**
 * @author singla
 */
@Service
public class ShipmentTrackingServiceImpl implements IShipmentTrackingService {

    private static final Logger LOG = LoggerFactory.getLogger(ShipmentTrackingServiceImpl.class);

    @Autowired
    private IShippingDao        shippingDao;

    @Autowired
    private IShippingMao        shippingMao;

    @Autowired
    private IShippingService    shippingService;

    /* (non-Javadoc)
     * @see com.uniware.services.shipping.IShipmentTrackingService#getShipmentsForStatusPolling(int)
     */
    @Override
    @Transactional(readOnly = true)
    public List<ShipmentTracking> getShipmentsForStatusPolling(int shippingProviderId, int start, int pageSize) {
        ShippingConfiguration shippingConfiguration = ConfigurationManager.getInstance().getConfiguration(ShippingConfiguration.class);
        return shippingDao.getShipmentsForTracking(shippingProviderId, shippingConfiguration.getShipmentTrackingPollingStatuses(), start, pageSize);
    }

    /* (non-Javadoc)
     * @see com.uniware.services.shipping.IShipmentTrackingService#updateTrackingStatuses(java.util.List)
     */
    @Override
    public UpdateTrackingStatusResponse updateTrackingStatuses(ShippingProvider shippingProvider, List<ShipmentTracking> shipments, ShipmentTrackingSyncStatusDTO syncStatusDTO) {
        UpdateTrackingStatusResponse response = new UpdateTrackingStatusResponse();
        response.setTotalPolled(shipments.size());
        if (syncStatusDTO != null) {
            syncStatusDTO.setTotalMileStones(syncStatusDTO.getTotalMileStones() + shipments.size());
            syncStatusDTO.setLastSyncTime(DateUtils.getCurrentTime());
        }
        ShippingConfiguration shippingConfiguration = ConfigurationManager.getInstance().getConfiguration(ShippingConfiguration.class);
        AbstractShipmentHandler shipmentHandler = shippingConfiguration.getShipmentHandlerByProviderCode(shippingProvider.getCode());
        Iterator<List<ShipmentTracking>> iterator = CollectionUtils.sublistIterator(shipments, shipmentHandler.getMaxTrackingNumberPerRequest());
        while (iterator.hasNext()) {
            List<ShipmentTracking> shipmentsSublist = iterator.next();
            try {
                Map<String, ProviderShipmentStatus> statuses = shipmentHandler.getShipmentStatuses(shipmentsSublist);
                if (statuses != null && statuses.size() > 0) {
                    for (ShipmentTracking shipmentTracking : shipmentsSublist) {
                        syncStatusDTO.setCurrentMileStone(syncStatusDTO.getCurrentMileStone() + 1);
                        ProviderShipmentStatus shipmentStatus = statuses.get(shipmentTracking.getTrackingNumber());
                        if (shipmentStatus != null) {
                            String currentStatus = shipmentTracking.getStatusCode();
                            updateShipmentTracking(shippingProvider.getCode(), shipmentTracking, shipmentStatus, false);
                            if (!shipmentTracking.getStatusCode().equals(currentStatus)) {
                                response.setTotalChanged(response.getTotalChanged() + 1);
                            }
                        }
                    }
                }
                syncStatusDTO.setSuccessfulProcessed(syncStatusDTO.getSuccessfulProcessed() + response.getTotalChanged());
                syncStatusDTO.setLastSyncSuccessful(true);
                syncStatusDTO.setLastSyncTime(DateUtils.getCurrentTime());
                syncStatusDTO.setMessage("Sync in progress");
            } catch (Throwable e) {
                response.setFailures(response.getFailures() + shipmentsSublist.size());
                syncStatusDTO.setFailedProcessed(response.getFailures());
                syncStatusDTO.setLastSyncSuccessful(false);
                syncStatusDTO.setLastSyncFailedNotificationTime(DateUtils.getCurrentTime());
                syncStatusDTO.setMessage("Sync failed. Error " + e.getMessage());
            }
        }
        ConfigurationManager.getInstance().getConfiguration(ShippingConfiguration.class).updateShipmentTrackinfSyncStatusDTOByCode(syncStatusDTO);
        return response;
    }

    @Override
    public void saveShippingProviderSyncStatus(ShipmentTrackingSyncStatusDTO statusDTO) {
        shippingMao.save(statusDTO);
    }

    @Override
    public UpdateShipmentTrackingResponse updateShipmentTracking(String shippingProviderCode, ShipmentTracking shipmentTracking, ProviderShipmentStatus providerStatus,
            boolean manualTrackingUpdate) {
        UpdateShipmentTrackingResponse response = new UpdateShipmentTrackingResponse();
        ShippingPackage shippingPackage = shippingService.getShippingPackageByShippingProviderAndTrackingNumber(shippingProviderCode, shipmentTracking.getTrackingNumber());
        response = doUpdateShipmentTracking(shippingPackage.getSaleOrder().getCode(), shippingProviderCode, shipmentTracking, providerStatus, manualTrackingUpdate, response);
        return response;
    }

    @Locks({ @Lock(ns = Namespace.SALE_ORDER, key = "#{#args[0]}") })
    @Transactional
    @RollbackOnFailure
    private UpdateShipmentTrackingResponse doUpdateShipmentTracking(String saleOrderCode, String shippingProviderCode, ShipmentTracking shipmentTracking,
            ProviderShipmentStatus providerStatus, boolean manualTrackingUpdate, UpdateShipmentTrackingResponse response) {
        ValidationContext context = new ValidationContext();
        ShippingProvider shippingProvider = ConfigurationManager.getInstance().getConfiguration(ShippingConfiguration.class).getShippingProviderByCode(shippingProviderCode);
        if (shippingProvider == null) {
            context.addError(WsResponseCode.INVALID_SHIPPING_PROVIDER_CODE);
        }
        if (!context.hasErrors() && providerStatus.getStatusDate() != null) {
            if (DateUtils.isFutureTime(providerStatus.getStatusDate())) {
                context.addError(WsResponseCode.INVALID_DATE, "status date can not be a future time");
            } else if (shipmentTracking.getCreated().after(providerStatus.getStatusDate())) {
                if (DateUtils.isSameDay(shipmentTracking.getCreated(), providerStatus.getStatusDate())) {
                    providerStatus.setStatusDate(shipmentTracking.getCreated());
                } else {
                    context.addError(WsResponseCode.INVALID_DATE, "status date can not be less that shipment dispatched date");
                }
            }
        }

        if (!context.hasErrors()) {
            ShippingConfiguration shippingConfiguration = ConfigurationManager.getInstance().getConfiguration(ShippingConfiguration.class);
            ShippingSourceConfiguration sourceConfiguration = ConfigurationManager.getInstance().getConfiguration(ShippingSourceConfiguration.class);
            ShipmentTrackingStatus trackingStatus = shippingConfiguration.getShipmentTrackingStatusByCode(providerStatus.getStatus());

            if (trackingStatus == null) {
                trackingStatus = shippingConfiguration.getMappedShipmentTrackingStatus(
                        sourceConfiguration.getShippingSourceByCode(shippingProvider.getShippingProviderSourceCode()).getId(), providerStatus.getStatus());
            }
            if (trackingStatus != null) {
                if (!shipmentTracking.isDisableStatusUpdate()) {
                    ShipmentTrackingStatus oldTrackingStatus = shippingConfiguration.getShipmentTrackingStatusByCode(shipmentTracking.getStatusCode());
                    if (oldTrackingStatus != null && (manualTrackingUpdate || !oldTrackingStatus.isStopPolling())) {
                        try {
                            ShippingPackageStatus oldPackageStatus = shippingConfiguration.getShippingPackageStatusById(oldTrackingStatus.getShippingPackageStatus().getId());
                            if (ShipmentTracking.Type.SHIP_TO_CUSTOMER.name().equals(shipmentTracking.getType())) {
                                ShippingPackage shippingPackage = shippingService.getShippingPackageByShippingProviderAndTrackingNumber(shippingProviderCode,
                                        shipmentTracking.getTrackingNumber());
                                if (shippingPackage != null) {
                                    shippingService.updateShippingPackageTrackingStatus(shippingPackage, trackingStatus.getShippingPackageStatus().getCode(),
                                            trackingStatus.getCode(), providerStatus.getStatus(), providerStatus.getStatusDate(), trackingStatus.isStopPolling());
                                }
                            }
                            if (shipmentTracking.getFirstAttemptTime() == null && trackingStatus.isDeliveryAttempted()) {
                                shipmentTracking.setFirstAttemptTime(providerStatus.getStatusDate());
                            }
                            if (ShippingPackage.StatusCode.DELIVERED.name().equals(trackingStatus.getShippingPackageStatus().getCode())) {
                                shipmentTracking.setDeliveryTime(providerStatus.getStatusDate());
                            }
                            if (shipmentTracking.getShipTime() == null) {
                                shipmentTracking.setShipTime(providerStatus.getStatusDate());
                            }
                            shipmentTracking.setProviderStatus(providerStatus.getStatus());
                            shipmentTracking.setStatusCode(trackingStatus.getCode());
                            shipmentTracking = shippingDao.updateShipmentTracking(shipmentTracking);
                            response.setSuccessful(true);
                        } catch (Exception e) {
                            context.addError(WsResponseCode.UNKNOWN_ERROR, "Tracking status update disabled");
                            LOG.error("Tracking status update disabled", e);
                        }
                    } else {
                        context.addError(WsResponseCode.TRACKING_STATUS_UPDATE_DISABLED, "Tracking status update disabled");
                    }
                } else {
                    context.addError(WsResponseCode.TRACKING_STATUS_UPDATE_DISABLED, "Tracking status update disabled");
                }
            } else {
                context.addError(WsResponseCode.SHIPPING_PROVIDER_STATUS_MAPPING_NOT_FOUND, "Status Mapping does not exists shipping provider: " + shippingProvider.getCode()
                        + ", status: " + providerStatus.getStatus() + ", trackingNumber:" + providerStatus.getTrackingNumber());
                LOG.error("Status Mapping does not exists shipping provider:{} ,status:{}, trackingNumber:" + providerStatus.getTrackingNumber(), shippingProvider.getCode(),
                        providerStatus.getStatus());
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Transactional
    @Override
    public ShipmentTracking addShipmentTracking(ShipmentTracking shipmentTracking) {
        return shippingDao.addShipmentTracking(shipmentTracking);
    }

    /* (non-Javadoc)
     * @see com.uniware.services.shipping.IShipmentTrackingService#updateShipmentTracking(com.uniware.core.entity.ShipmentTracking)
     */
    @Override
    @Transactional
    public ShipmentTracking updateShipmentTracking(ShipmentTracking shipmentTracking) {
        return shippingDao.updateShipmentTracking(shipmentTracking);
    }

    @Override
    @Transactional
    public ShipmentTracking getShipmentTracking(String trackingNumber, Integer shippingProviderId) {
        return shippingDao.getShipmentTracking(trackingNumber, shippingProviderId);
    }

    @Override
    @Transactional
    public List<ShipmentTrackingStatus> getShipmentTrackingStatuses() {
        return shippingDao.getShipmentTrackingStatuses();
    }

    @Override
    public ShipmentTrackingSyncStatusDTO getTrackingSyncStatusByProviderFromDB(String shippingProviderCode) {
        return shippingMao.getTrackingSyncStatusByShippingProvider(shippingProviderCode);
    }

    @Override
    public ShipmentTrackingSyncStatusDTO getTrackingSyncStatusByProvider(String shippingProviderCode) {
        return ConfigurationManager.getInstance().getConfiguration(ShippingConfiguration.class).getShipmentTrackingSyncStatusDTOByCode(shippingProviderCode);
    }

    @Override
    public SyncTrackingStatusForAWBResponse syncStatusForAWB(SyncTrackingStatusForAWBRequest request) {
        SyncTrackingStatusForAWBResponse response = new SyncTrackingStatusForAWBResponse();
        ValidationContext context = request.validate();
        ShippingProvider shippingProvider = null;
        List<ShipmentTracking> shipments = new ArrayList<>();
        if (!context.hasErrors()) {
            shippingProvider = ConfigurationManager.getInstance().getConfiguration(ShippingConfiguration.class).getShippingProviderByCode(request.getShippingProviderCode());
            if (shippingProvider == null) {
                context.addError(WsResponseCode.INVALID_SHIPPING_PROVIDER_CODE, "No shipping provider exists for code : " + request.getShippingProviderCode());
            }
        }
        if (!context.hasErrors()) {
            ShippingConfiguration shippingConfiguration = ConfigurationManager.getInstance().getConfiguration(ShippingConfiguration.class);
            AbstractShipmentHandler shipmentHandler = shippingConfiguration.getShipmentHandlerByProviderCode(shippingProvider.getCode());
            shipments.add(getShipmentTracking(request.getTrackingNumber(), shippingProvider.getId()));
            try {
                Map<String, ProviderShipmentStatus> statuses = shipmentHandler.getShipmentStatuses(shipments);
                if (statuses != null && statuses.size() > 0) {
                    for (ShipmentTracking shipmentTracking : shipments) {
                        ProviderShipmentStatus shipmentStatus = statuses.get(shipmentTracking.getTrackingNumber());
                        if (shipmentStatus != null) {
                            updateShipmentTracking(shippingProvider.getCode(), shipmentTracking, shipmentStatus, false);
                            response.setStatusCode(shipmentTracking.getStatusCode());
                            response.setProviderStatusCode(shipmentTracking.getProviderStatus());
                            response.setSuccessful(true);
                        }
                    }
                } else {
                    context.addError(WsResponseCode.TRACKING_STATUS_UPDATE_DISABLED, "No tracking info found for given AWB");
                }

            } catch (Throwable e) {
                context.addError(WsResponseCode.TRACKING_STATUS_UPDATE_DISABLED, "Tracking failed for awb [" + request.getTrackingNumber() + "] error [" + e.getMessage() + "]");
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Locks({ @Lock(ns = Namespace.SHIPMENT_TRACKING_STATUS_SYNC, key = "#{#args[0].shippingProviderCode}") })
    public SyncShipmentTrackingStatusResponse syncTrackingStatuses(SyncShipmentTrackingStatusRequest request) {
        final SyncShipmentTrackingStatusResponse syncShipmentResponse = new SyncShipmentTrackingStatusResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            final ShippingProvider shippingProvider = ConfigurationManager.getInstance().getConfiguration(ShippingConfiguration.class).getShippingProviderByCode(
                    request.getShippingProviderCode());
            final ShipmentTrackingSyncStatusDTO syncStatusDTO = ConfigurationManager.getInstance().getConfiguration(ShippingConfiguration.class).getShipmentTrackingSyncStatusDTOByCode(
                    request.getShippingProviderCode());
            if (shippingProvider == null) {
                context.addError(WsResponseCode.INVALID_SHIPPING_PROVIDER_CODE, WsResponseCode.INVALID_SHIPPING_PROVIDER_CODE.message() + ":" + request.getShippingProviderCode());
            } else {
                try {
                    if (!shippingProvider.isEnabled()) {
                        syncShipmentResponse.getResultItems().add(
                                new SyncShipmentTrackingStatusResponse.ShippingProviderShipmentTrackingResponseDTO(request.getShippingProviderCode(), "Shipping Provider disabled."));
                        context.addError(WsResponseCode.INVALID_REQUEST, "Shipping Provider disabled");
                    } else if (!SyncStatus.ON.equals(shippingProvider.getTrackingSyncStatus())) {
                        syncShipmentResponse.getResultItems().add(
                                new SyncShipmentTrackingStatusResponse.ShippingProviderShipmentTrackingResponseDTO(request.getShippingProviderCode(), "Tracking not available."));
                        context.addError(WsResponseCode.INVALID_REQUEST, "Tracking not available.");
                    } else if (syncStatusDTO.isRunning()) {
                        syncShipmentResponse.getResultItems().add(
                                new SyncShipmentTrackingStatusResponse.ShippingProviderShipmentTrackingResponseDTO(request.getShippingProviderCode(), "Sync already running."));
                        context.addError(WsResponseCode.ALREADY_RUNNING, "Sync already running.");
                    } else {
                        syncStatusDTO.reset();
                        syncStatusDTO.setRunning(true);
                        LOG.info("Updating Statuses for: {}", shippingProvider.getName());
                        BatchProcessor<ShipmentTracking> processor = new BatchProcessor<ShipmentTracking>(100) {
                            @Override
                            protected void process(List<ShipmentTracking> shipments, int batchIndex) {
                                UpdateTrackingStatusResponse response = updateTrackingStatuses(shippingProvider, shipments, syncStatusDTO);
                                syncShipmentResponse.getResultItems().add(
                                        new SyncShipmentTrackingStatusResponse.ShippingProviderShipmentTrackingResponseDTO(shippingProvider.getCode(), response.getMessage()));
                            }

                            @Override
                            protected List<ShipmentTracking> next(int start, int batchSize) {
                                LOG.info("Processing shipment tracking for start:{}, batch:{}", start, batchSize);
                                return getShipmentsForStatusPolling(shippingProvider.getId(), start, batchSize);
                            }
                        };
                        processor.process();
                        syncShipmentResponse.setSuccessful(true);
                        syncShipmentResponse.getResultItems().add(
                                new SyncShipmentTrackingStatusResponse.ShippingProviderShipmentTrackingResponseDTO(request.getShippingProviderCode(), "Sync started"));
                    }
                } catch (Exception e) {
                    syncStatusDTO.setLastSyncFailedNotificationTime(DateUtils.getCurrentTime());
                    syncStatusDTO.setMessage(e.getMessage());
                    LOG.error("Error processing tracking statuses for {}", shippingProvider.getCode());
                }
            }
            syncStatusDTO.setRunning(false);
            ConfigurationManager.getInstance().getConfiguration(ShippingConfiguration.class).updateShipmentTrackinfSyncStatusDTOByCode(syncStatusDTO);
            saveShippingProviderSyncStatus(syncStatusDTO);
        }
        return syncShipmentResponse;
    }

}
