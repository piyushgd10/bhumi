/*
 *  Copyright 2013 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 28-Jun-2013
 *  @author sunny
 */
package com.uniware.services.shipping.impl;

import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.template.Template;
import com.uniware.core.api.packer.RegulatoryFormDTO;
import com.uniware.core.api.shipping.GetApplicableRegulatoryFormsRequest;
import com.uniware.core.api.shipping.GetApplicableRegulatoryFormsResponse;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.cache.FacilityCache;
import com.uniware.core.entity.Facility;
import com.uniware.core.entity.PartyAddress;
import com.uniware.core.entity.PartyAddressType;
import com.uniware.core.entity.RegulatoryForm;
import com.uniware.core.entity.RegulatoryForm.GOODS_MOVEMENT_TYPE;
import com.uniware.core.entity.ShippingPackage;
import com.uniware.dao.shipping.IRegulatoryMao;
import com.uniware.services.cache.RegulatoryFormCache;
import com.uniware.services.shipping.IRegulatoryService;
import com.uniware.services.shipping.IShippingService;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.slf4j.LoggerFactory.getLogger;

@Service
public class RegulatoryServiceImpl implements IRegulatoryService {

    private static final Logger LOG = getLogger(RegulatoryServiceImpl.class);

    @Autowired
    private IShippingService shippingService;

    @Autowired
    private IRegulatoryMao regulatoryMao;

    @Transactional(readOnly = true)
    @Override
    public List<RegulatoryForm> getAllRegulatoryForms() {
        return regulatoryMao.getAllStateRegulatoryForms();
    }

    @Transactional(readOnly = true)
    @Override
    public GetApplicableRegulatoryFormsResponse getApplicableRegulatoryForms(GetApplicableRegulatoryFormsRequest request) {
        GetApplicableRegulatoryFormsResponse response = new GetApplicableRegulatoryFormsResponse();
        ValidationContext context = request.validate();
        ShippingPackage shippingPackage = shippingService.getShippingPackageByCode(request.getShippingPackageCode());
        if (shippingPackage == null) {
            context.addError(WsResponseCode.INVALID_SHIPPING_PACKAGE_CODE);
        }
        if (!context.hasErrors()) {
            response.setStateRegulatoryForms(getApplicableRegulatoryForms(shippingPackage));
            response.setSuccessful(true);
        }

        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    @Override
    public List<RegulatoryFormDTO> getApplicableRegulatoryForms(ShippingPackage shippingPackage) {
        List<RegulatoryFormDTO> regulatoryFormDTOs = new ArrayList<>();
        RegulatoryFormCache regulatoryFormCache = CacheManager.getInstance().getCache(RegulatoryFormCache.class);
        Facility facility = CacheManager.getInstance().getCache(FacilityCache.class).getCurrentFacility();
        PartyAddress facilityAddress = facility.getPartyAddressByType(PartyAddressType.Code.SHIPPING.name());
        if ("IN".equals(facilityAddress.getCountryCode()) && "IN".equals(shippingPackage.getShippingAddress().getCountryCode())) {
            String customerStateCode = shippingPackage.getShippingAddress().getStateCode();
            addApplicableForms(regulatoryFormCache.getStateRegulatoryFormsByStateCode(customerStateCode, GOODS_MOVEMENT_TYPE.INBOUND.name()), shippingPackage,
                    regulatoryFormDTOs);
            addApplicableForms(regulatoryFormCache.getStateRegulatoryFormsByStateCode(facilityAddress.getStateCode(), GOODS_MOVEMENT_TYPE.OUTBOUND.name()), shippingPackage,
                    regulatoryFormDTOs);
        }
        return regulatoryFormDTOs;
    }

    private void addApplicableForms(List<RegulatoryFormDTO> availableForms, ShippingPackage shippingPackage, List<RegulatoryFormDTO> regulatoryFormDTOs) {
        for (RegulatoryFormDTO regulatoryFormDTO : availableForms) {
            if (regulatoryFormDTO.getConditionExpression() != null) {
                Map<String, Object> contextParams = new HashMap<>();
                contextParams.put("shippingPackage", shippingPackage);
                try {
                    boolean applicable = regulatoryFormDTO.getConditionExpression().evaluate(contextParams, Boolean.class);
                    if (applicable) {
                        regulatoryFormDTOs.add(regulatoryFormDTO);
                    }
                } catch (Exception e) {
                    LOG.error("Unable to evaluate expression for form: {}, shippingPackage:{}", regulatoryFormDTO.getCode(), shippingPackage.getCode());
                }
            }
        }
    }

    @Override
    @Transactional(readOnly = true)
    public String getStateRegulatoryFormHtml(String shippingPackageCode, Template template) {
        ShippingPackage shippingPackage = shippingService.getShippingPackageByCode(shippingPackageCode);
        Map<String, Object> params = new HashMap<>();
        if (shippingPackage != null) {
            params.put("shippingPackage", shippingPackage);
            return template.evaluate(params);
        } else {
            return null;
        }
    }
}
