/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 12, 2012
 *  @author praveeng
 */
package com.uniware.services.shipping.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.utils.NumberUtils;
import com.unifier.core.utils.StringUtils;
import com.uniware.core.api.admin.shipping.EditPaymentReconciliationRequest;
import com.uniware.core.api.admin.shipping.EditPaymentReconciliationResponse;
import com.uniware.core.api.admin.shipping.PaymentReconciliationDTO;
import com.uniware.core.api.shipping.MarkPackagesReconciledRequest;
import com.uniware.core.api.shipping.MarkPackagesReconciledResponse;
import com.uniware.core.api.shipping.PaymentReconciliationRequest;
import com.uniware.core.api.shipping.PaymentReconciliationResponse;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.entity.PaymentMethod;
import com.uniware.core.entity.PaymentReconciliation;
import com.uniware.core.entity.ShippingPackage;
import com.uniware.core.entity.ShippingProvider;
import com.uniware.core.locking.Namespace;
import com.uniware.core.locking.annotation.Lock;
import com.uniware.core.locking.annotation.Locks;
import com.uniware.dao.shipping.IPaymentReconciliationService;
import com.uniware.dao.shipping.IShippingDao;
import com.uniware.services.configuration.ShippingConfiguration;
import com.uniware.services.shipping.IPaymentReconciliationDao;
import com.uniware.services.shipping.IShippingService;

@Service
public class PaymentReconciliationServiceImpl implements IPaymentReconciliationService {

    @Autowired
    private IPaymentReconciliationDao paymentReconciliationDao;

    @Autowired
    private IShippingDao              shippingDao;

    @Autowired
    private IShippingService          shippingService;

    @Override
    @Transactional(readOnly = true)
    public List<PaymentReconciliationDTO> getPaymentReconciliations() {
        List<PaymentReconciliationDTO> reconciliationDTOs = new ArrayList<PaymentReconciliationDTO>();
        for (PaymentReconciliation paymentReconciliation : paymentReconciliationDao.getPaymentReconciliations()) {
            PaymentReconciliationDTO reconciliationDTO = new PaymentReconciliationDTO();
            reconciliationDTO.setPaymentMethodCode(paymentReconciliation.getPaymentMethod().getCode());
            reconciliationDTO.setTolerance(paymentReconciliation.getTolerance());
            reconciliationDTO.setEnabled(paymentReconciliation.isEnabled());
            reconciliationDTOs.add(reconciliationDTO);
        }
        return reconciliationDTOs;
    }

    @Override
    @Transactional
    public void reconcileShipmentPayments(String paymentMethodCode) {
        PaymentReconciliation reconciliation = paymentReconciliationDao.getPaymentReconciliationByPaymentMethodCode(paymentMethodCode);
        if (reconciliation.isEnabled()) {
            PaymentMethod paymentMethod = paymentReconciliationDao.getPaymentMethodByCode(paymentMethodCode);
            paymentReconciliationDao.markPaymentReconciled(paymentMethod.getCode(), reconciliation.getTolerance());
        }
    }

    @Override
    @Transactional
    public PaymentReconciliationResponse reconcileShipmentPayment(PaymentReconciliationRequest request) {
        PaymentReconciliationResponse response = new PaymentReconciliationResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            String shippingPackageCode = request.getShippingPackageCode();
            String trackingNumber = request.getTrackingNumber();
            ShippingPackage shippingPackage = null;
            if (StringUtils.isNotBlank(shippingPackageCode)) {
                shippingPackage = shippingDao.getShippingPackageByCode(request.getShippingPackageCode());
            } else if (StringUtils.isNotBlank(request.getTrackingNumber())) {
                ShippingProvider shippingProvider = ConfigurationManager.getInstance().getConfiguration(ShippingConfiguration.class).getShippingProviderByCode(
                        request.getShippingProviderCode());
                if (shippingProvider == null) {
                    context.addError(WsResponseCode.INVALID_SHIPPING_PROVIDER_CODE, "Invalid shipping provider code");
                } else {
                    shippingPackage = shippingDao.getShippingPackageByTrackingNumberOrShippingProvider(trackingNumber, shippingProvider.getCode());
                }
            } else {
                context.addError(WsResponseCode.INVALID_REQUEST, "Shipping Package Code or Tracking Number with shipping provider is required");
            }
            if (shippingPackage == null) {
                context.addError(WsResponseCode.INVALID_SHIPPING_PACKAGE_CODE, "Invalid shipping package code/tracking number");
            }

            if (!context.hasErrors()) {
                doReconcileShipmentPayment(request, response, context, shippingPackage);
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Locks({ @Lock(ns = Namespace.SALE_ORDER, key = "#{#args[3].saleOrder.code}") })
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    private void doReconcileShipmentPayment(PaymentReconciliationRequest request, PaymentReconciliationResponse response, ValidationContext context, ShippingPackage sp) {
        ShippingPackage shippingPackage = shippingService.getShippingPackageByCode(sp.getCode());
        if (shippingPackage.isPaymentReconciled()) {
            context.addError(WsResponseCode.INVALID_REQUEST, "Shipment has been reconciled already");
        } else {
            shippingPackage.setCollectedAmount(request.getCollectedAmount());
            shippingPackage.setPaymentReferenceNumber(request.getPaymentReferenceNumber());
            PaymentReconciliation paymentReconciliation = paymentReconciliationDao.getPaymentReconciliationByPaymentMethodCode(shippingPackage.getSaleOrder().getPaymentMethod().getCode());
            if (!paymentReconciliation.isEnabled()) {
                context.addError(WsResponseCode.INVALID_PAYMENT_METHOD, "Reconciliation is disabled for payment method: " + paymentReconciliation.getPaymentMethod().getName());
            } else {
                if (!NumberUtils.lessThan(paymentReconciliation.getTolerance(), shippingPackage.getCollectableAmount().subtract(shippingPackage.getCollectedAmount()).abs())) {
                    shippingPackage.setPaymentReconciled(true);
                }
                response.setSuccessful(true);
            }
        }
    }

    @Override
    @Transactional
    public EditPaymentReconciliationResponse editPaymentReconciliation(EditPaymentReconciliationRequest request) {
        EditPaymentReconciliationResponse response = new EditPaymentReconciliationResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            PaymentReconciliation reconciliation = paymentReconciliationDao.getPaymentReconciliationByPaymentMethodCode(request.getPaymentMethodCode());
            if (reconciliation == null) {
                context.addError(WsResponseCode.INVALID_PAYMENT_METHOD, "Invalid Payment Method Code");
            } else {
                if (request.getTolerance() != null) {
                    reconciliation.setTolerance(request.getTolerance());
                }

                if (request.getEnabled() != null) {
                    reconciliation.setEnabled(request.getEnabled());
                }
                response.setSuccessful(true);
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    public MarkPackagesReconciledResponse markPackagesReconciled(MarkPackagesReconciledRequest request) {
        MarkPackagesReconciledResponse response = new MarkPackagesReconciledResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            paymentReconciliationDao.markPackagesReconciled(request.getShippingPackageCodes());
            response.setSuccessful(true);
        }
        response.setErrors(context.getErrors());
        return response;
    }
}
