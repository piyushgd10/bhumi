/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 16, 2011
 *  @author singla
 */
package com.uniware.services.shipping;

import java.util.List;

import com.uniware.core.api.admin.shipping.AddAwbNumberRequest;
import com.uniware.core.api.admin.shipping.AddAwbNumberResponse;
import com.uniware.core.api.admin.shipping.AddShippingProviderMethodsRequest;
import com.uniware.core.api.admin.shipping.AddShippingProviderMethodsResponse;
import com.uniware.core.api.admin.shipping.CreateProviderAllocationRuleRequest;
import com.uniware.core.api.admin.shipping.CreateProviderAllocationRuleResponse;
import com.uniware.core.api.admin.shipping.DeleteAwbNumberRequest;
import com.uniware.core.api.admin.shipping.DeleteAwbNumberResponse;
import com.uniware.core.api.admin.shipping.DeleteProviderAllocationRuleRequest;
import com.uniware.core.api.admin.shipping.DeleteProviderAllocationRuleResponse;
import com.uniware.core.api.admin.shipping.EditProviderAllocationRuleRequest;
import com.uniware.core.api.admin.shipping.EditProviderAllocationRuleResponse;
import com.uniware.core.api.admin.shipping.EditShippingProviderMethodRequest;
import com.uniware.core.api.admin.shipping.EditShippingProviderMethodResponse;
import com.uniware.core.api.admin.shipping.EditShippingServiceabilityRequest;
import com.uniware.core.api.admin.shipping.EditShippingServiceabilityResposne;
import com.uniware.core.api.admin.shipping.GetShippingProviderRequest;
import com.uniware.core.api.admin.shipping.GetShippingProviderResponse;
import com.uniware.core.api.admin.shipping.GetShippingProvidersResponse;
import com.uniware.core.api.admin.shipping.ReorderProviderAllocationRulesRequest;
import com.uniware.core.api.admin.shipping.ReorderProviderAllocationRulesResponse;
import com.uniware.core.api.admin.shipping.SearchAwbNumberRequest;
import com.uniware.core.api.admin.shipping.SearchAwbNumberResponse;
import com.uniware.core.api.admin.shipping.ShippingProviderAllocationRuleDTO;
import com.uniware.core.api.admin.shipping.ShippingProviderDTO;
import com.uniware.core.api.admin.shipping.ShippingProviderDetailDTO;
import com.uniware.core.api.admin.shipping.ViewAwbNumberRequest;
import com.uniware.core.api.admin.shipping.ViewAwbNumberResponse;
import com.uniware.core.api.shipping.AddShippingProviderConnectorRequest;
import com.uniware.core.api.shipping.AddShippingProviderConnectorResponse;
import com.uniware.core.api.shipping.AddShippingProviderRequest;
import com.uniware.core.api.shipping.AddShippingProviderResponse;
import com.uniware.core.api.shipping.EditShippingProviderRequest;
import com.uniware.core.api.shipping.EditShippingProviderResponse;
import com.uniware.core.api.shipping.GetShippingProviderParametersRequest;
import com.uniware.core.api.shipping.GetShippingProviderParametersResponse;
import com.uniware.core.api.shipping.VerifyandSyncShippingConnectorParamsResponse;
import com.uniware.core.api.warehouse.CreateShippingPackageTypeRequest;
import com.uniware.core.api.warehouse.CreateShippingPackageTypeResponse;
import com.uniware.core.api.warehouse.EditShippingPackageTypeRequest;
import com.uniware.core.api.warehouse.EditShippingPackageTypeResponse;
import com.uniware.core.entity.ShippingPackageType;
import com.uniware.core.entity.ShippingProvider;
import com.uniware.core.entity.ShippingProviderConnector;
import com.uniware.core.entity.ShippingProviderMethod;
import com.uniware.core.entity.ShippingProviderSource;

/**
 * @author vibhu
 */
public interface IShippingAdminService {

    List<ShippingPackageType> getShippingPackageTypes();

    CreateShippingPackageTypeResponse createShippingPackageType(CreateShippingPackageTypeRequest request);

    EditShippingPackageTypeResponse editShippingPackageType(EditShippingPackageTypeRequest editShippingPackageTypeRequest);

    List<ShippingProviderDTO> getEnabledShippingProviders();

    //    EditShippingProviderResponse editShippingProvider(EditShippingProviderRequest request);

    EditShippingProviderMethodResponse editShippingProviderMethod(EditShippingProviderMethodRequest request);

    //    ShippingProviderDetailDTO getShippingProviderDetail(String code);

    SearchAwbNumberResponse searchAwbNumbers(SearchAwbNumberRequest request);

    ViewAwbNumberResponse viewExistingAwbNumbers(ViewAwbNumberRequest request);

    DeleteAwbNumberResponse deleteAwbNumbers(DeleteAwbNumberRequest request);

    AddAwbNumberResponse addAwbNumbers(AddAwbNumberRequest request);

    List<ShippingProviderAllocationRuleDTO> getShippingProviderAllocationRules();

    CreateProviderAllocationRuleResponse createProviderAllocationRule(CreateProviderAllocationRuleRequest request);

    EditProviderAllocationRuleResponse editProviderAllocationRule(EditProviderAllocationRuleRequest request);

    DeleteProviderAllocationRuleResponse deleteProviderAllocationRule(DeleteProviderAllocationRuleRequest request);

    ReorderProviderAllocationRulesResponse reorderProviderAllocationRules(ReorderProviderAllocationRulesRequest request);

    //    UpdateShippingProviderParametersResponse updateShippingProviderParameters(UpdateShippingProviderParametersRequest request);

    //    UpdateShippingParametersResponse updateShippingParameters(UpdateShippingParametersRequest request);

    AddShippingProviderMethodsResponse addShippingProviderMethods(AddShippingProviderMethodsRequest request);

    ShippingProviderMethod getShippingProviderMethod(String shippingProviderCode, String shippingMethodName);

    List<ShippingProviderSource> getAllShippingProviderSources();

    GetShippingProviderResponse getShippingProviderByCode(GetShippingProviderRequest request);

    ShippingProvider getShippingProviderByCode(String providerCode);

    //    ShippingProvider addShippingProvider(ShippingProvider shippingProvider);

    //    ShippingProvider updateShippingProvider(ShippingProvider shippingProvider);

    List<ShippingProvider> getShippingProviders(boolean fetchHidden);

    ShippingProvider getShippingProviderByName(String shippingProviderName);

    ShippingProviderConnector updateShippingProviderConnector(ShippingProviderConnector shippingProviderConnector);

    AddShippingProviderConnectorResponse addShippingProviderConnector(AddShippingProviderConnectorRequest connectorRequest);

    AddShippingProviderResponse addShippingProvider(AddShippingProviderRequest request);

    EditShippingProviderResponse updateShippingProvider(EditShippingProviderRequest request);

    GetShippingProviderParametersResponse getShippingProviderConnectorParameter(GetShippingProviderParametersRequest request);

    List<ShippingProviderDetailDTO> getAllShippingProvidersDetailDTO();

    EditShippingServiceabilityResposne editShippingProviderServiceability(EditShippingServiceabilityRequest request);

    int getProviderCountBySourceCode(String sourceCode);

    String getShippingProviderColorCodeBySource(String sourceCode);

    GetShippingProvidersResponse getAllShippingProviders();

    VerifyandSyncShippingConnectorParamsResponse verifyAndSyncChannelConnectorParameters(String shippingProviderCode, String connectorName);
}
