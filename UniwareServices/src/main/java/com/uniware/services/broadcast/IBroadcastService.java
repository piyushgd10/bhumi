/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 20-Jan-2014
 *  @author akshay
 */
package com.uniware.services.broadcast;

import com.uniware.core.api.broadcast.DeleteBroadcastedMessagesRequest;
import com.uniware.core.api.broadcast.DeleteBroadcastedMessagesResponse;
import com.uniware.core.api.broadcast.GetBroadcastedMessagesRequest;
import com.uniware.core.api.broadcast.GetBroadcastedMessagesResponse;
import com.uniware.core.api.broadcast.RefreshBroadcastMessageRequest;
import com.uniware.core.api.broadcast.RefreshBroadcastMessageResponse;

public interface IBroadcastService {

    GetBroadcastedMessagesResponse getBroadcastedMessages(GetBroadcastedMessagesRequest request);

    DeleteBroadcastedMessagesResponse deleteBroadcastedMessages(DeleteBroadcastedMessagesRequest request);

    RefreshBroadcastMessageResponse refreshBroadcastMessage(RefreshBroadcastMessageRequest request);

}
