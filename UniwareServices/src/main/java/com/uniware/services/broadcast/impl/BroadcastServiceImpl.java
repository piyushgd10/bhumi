/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 20-Jan-2014
 *  @author akshay
 */
package com.uniware.services.broadcast.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.DateUtils.Resolution;
import com.unifier.core.utils.StringUtils;
import com.unifier.services.users.IUsersService;
import com.uniware.core.api.broadcast.BroadcastedMessageDTO;
import com.uniware.core.api.broadcast.DeleteBroadcastedMessagesRequest;
import com.uniware.core.api.broadcast.DeleteBroadcastedMessagesResponse;
import com.uniware.core.api.broadcast.GetBroadcastedMessagesRequest;
import com.uniware.core.api.broadcast.GetBroadcastedMessagesResponse;
import com.uniware.core.api.broadcast.RefreshBroadcastMessageRequest;
import com.uniware.core.api.broadcast.RefreshBroadcastMessageResponse;
import com.uniware.core.api.channel.ChannelDetailDTO;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.cache.TenantCache;
import com.uniware.core.entity.Tenant;
import com.uniware.core.utils.UserContext;
import com.uniware.core.vo.BroadcastMessageVO;
import com.uniware.core.vo.UserProfileVO;
import com.uniware.dao.broadcast.IBroadcastMao;
import com.uniware.services.broadcast.IBroadcastService;
import com.uniware.services.cache.ChannelCache;
import com.uniware.services.publisher.IMessageBroadcaster;
import com.uniware.services.publisher.Message;

@Service
public class BroadcastServiceImpl implements IBroadcastService {

    @Autowired
    private IBroadcastMao       broadcastMao;

    @Autowired
    private IUsersService       userService;

    @Autowired
    private IMessageBroadcaster messageBroadcaster;

    @Override
    public GetBroadcastedMessagesResponse getBroadcastedMessages(GetBroadcastedMessagesRequest request) {
        GetBroadcastedMessagesResponse response = new GetBroadcastedMessagesResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            List<BroadcastMessageVO> broadcastedMessages = broadcastMao.getTenantBroadcastedMessages(Direction.ASC, "repeatInterval");
            if (!broadcastedMessages.isEmpty()) {
                UserProfileVO userProfile = userService.getUserProfileByUsername(request.getUsername());
                if (userProfile == null) {
                    context.addError(WsResponseCode.INVALID_USER_ID, "Invalid user");
                } else {
                    Set<ChannelDetailDTO> channels = CacheManager.getInstance().getCache(ChannelCache.class).getChannels();
                    Set<String> sourceCodes = new HashSet<String>(channels.size());
                    for (ChannelDetailDTO channel : channels) {
                        sourceCodes.add(channel.getSource().getCode());
                    }
                    List<BroadcastedMessageDTO> messages = new ArrayList<>();
                    Date lastReadTime = userProfile.getLastReadTime() != null ? userProfile.getLastReadTime() : DateUtils.getCurrentTime();
                    boolean displayMessages = lastReadTime == null;
                    if (!displayMessages && broadcastedMessages.get(0).getRepeatInterval() != null) {
                        displayMessages = broadcastedMessages.get(0).getRepeatInterval().intValue() >= DateUtils.diff(DateUtils.getCurrentTime(), lastReadTime, Resolution.MINUTE);
                    }
                    for (BroadcastMessageVO messageVO : broadcastedMessages) {
                        if (displayMessages || (lastReadTime.before(messageVO.getCreated()))) {
                            if (StringUtils.isNotBlank(messageVO.getSourceCode()) && sourceCodes.contains(messageVO.getSourceCode())) {
                                BroadcastedMessageDTO messageDetails = new BroadcastedMessageDTO();
                                messageDetails.setMessage(messageVO.getMessage());
                                messageDetails.setSourceCode(messageVO.getSourceCode());
                                messageDetails.setCreated(messageVO.getCreated());
                                messages.add(messageDetails);
                            } else if (StringUtils.isBlank(messageVO.getSourceCode())) {
                                BroadcastedMessageDTO messageDetails = new BroadcastedMessageDTO();
                                messageDetails.setMessage(messageVO.getMessage());
                                messageDetails.setCreated(messageVO.getCreated());
                                messages.add(messageDetails);
                            }
                        }
                    }
                    response.setBroadcastedMessages(messages);
                }
            }
        }
        if (!context.hasErrors()) {
            response.setSuccessful(true);
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    public DeleteBroadcastedMessagesResponse deleteBroadcastedMessages(DeleteBroadcastedMessagesRequest request) {
        DeleteBroadcastedMessagesResponse response = new DeleteBroadcastedMessagesResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            if (!request.getMessages().isEmpty()) {
                broadcastMao.deleteMessages(request.getMessages());
                response.setSuccessful(true);
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    public RefreshBroadcastMessageResponse refreshBroadcastMessage(RefreshBroadcastMessageRequest request) {
        RefreshBroadcastMessageResponse response = new RefreshBroadcastMessageResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            response.setSuccessful(true);
            for (Tenant tenant : CacheManager.getInstance().getCache(TenantCache.class).getActiveTenants()) {
                UserContext.current().setTenant(tenant);
                messageBroadcaster.broadcast(new Message("/broadcastMessages", Message.Type.TENANT, response));
            }
        } else {
            response.setSuccessful(false);
        }
        response.setErrors(context.getErrors());
        return response;
    }
}