/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 24-Mar-2014
 *  @author parijat
 */
package com.uniware.services.apiStatistics;

import com.uniware.core.api.apiStatistics.GetApiUserAccessStatisticsRequest;
import com.uniware.core.api.apiStatistics.GetApiUserAccessStatisticsResponse;
import com.uniware.core.vo.ApiLimitsVO;

/**
 * @author parijat
 */
public interface IApiAccessStatisticsService {

    GetApiUserAccessStatisticsResponse getApiAccessStatisticsForUser(GetApiUserAccessStatisticsRequest request);

    void saveApiAccessStatistics(String apiName, long responseTime, boolean successful);

    ApiLimitsVO getAPILimitsByAPIName(String apiName);

}
