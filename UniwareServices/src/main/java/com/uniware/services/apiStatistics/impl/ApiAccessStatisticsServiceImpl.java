/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 24-Mar-2014
 *  @author parijat
 */
package com.uniware.services.apiStatistics.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unifier.core.api.validation.ValidationContext;
import com.uniware.core.api.apiStatistics.ApiAccessStatisticsDTO;
import com.uniware.core.api.apiStatistics.GetApiUserAccessStatisticsRequest;
import com.uniware.core.api.apiStatistics.GetApiUserAccessStatisticsResponse;
import com.uniware.core.vo.ApiAccessStatisticsVO;
import com.uniware.core.vo.ApiLimitsVO;
import com.uniware.dao.user.notification.IApiAccessStatisticsMao;
import com.uniware.services.apiStatistics.IApiAccessStatisticsService;

@Service("apiUserAccessStatisticsService")
public class ApiAccessStatisticsServiceImpl implements IApiAccessStatisticsService {

    @Autowired
    private IApiAccessStatisticsMao apiUserAccessStatisticsMao;

    @Override
    public GetApiUserAccessStatisticsResponse getApiAccessStatisticsForUser(GetApiUserAccessStatisticsRequest request) {
        GetApiUserAccessStatisticsResponse response = new GetApiUserAccessStatisticsResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            List<ApiAccessStatisticsVO> statisticsVOs = apiUserAccessStatisticsMao.getAccessStatisticsForUser(request.getApiUserName());
            List<ApiAccessStatisticsDTO> apiUserStatistics = new ArrayList<ApiAccessStatisticsDTO>();
            for (ApiAccessStatisticsVO vo : statisticsVOs) {
                ApiAccessStatisticsDTO statistics = new ApiAccessStatisticsDTO();
                statistics.setApiName(vo.getApiName());
                statistics.setApiVersion(vo.getApiVersion());
                statistics.setSuccessfulExecutionCount(vo.getSuccessCount());
                statistics.setFailedExecutionCount(vo.getFailedCount());
                apiUserStatistics.add(statistics);
            }
            response.setApiUserStatisticsDTO(apiUserStatistics);
            response.setSuccessful(true);
        } else {
            response.setSuccessful(false);
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    public void saveApiAccessStatistics(String apiName,long responseTime, boolean successful) {
        ApiAccessStatisticsVO vo = new ApiAccessStatisticsVO();
        vo.setApiName(apiName);
        vo.setTotalResponseTime(responseTime);
        apiUserAccessStatisticsMao.save(vo, successful);
    }

    @Override
    public ApiLimitsVO getAPILimitsByAPIName(String apiName) {
        return apiUserAccessStatisticsMao.getAPILimitByAPIName(apiName);
    }
}
