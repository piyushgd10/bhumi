/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 20, 2012
 *  @author Sunny
 */
package com.uniware.services.customer;

import java.util.List;

import com.uniware.core.api.customer.CreateCustomerRequest;
import com.uniware.core.api.customer.CreateCustomerResponse;
import com.uniware.core.api.customer.EditCustomerRequest;
import com.uniware.core.api.customer.EditCustomerResponse;
import com.uniware.core.api.customer.GetCustomerRequest;
import com.uniware.core.api.customer.GetCustomerResponse;
import com.uniware.core.api.party.dto.CustomerDTO;
import com.uniware.core.api.party.dto.CustomerSearchDTO;
import com.uniware.core.entity.Customer;

public interface ICustomerService {

    Customer getCustomerByCode(String code);

    Customer getCustomerById(int vendorId);

    CreateCustomerResponse createCustomer(CreateCustomerRequest request);

    EditCustomerResponse editCustomer(EditCustomerRequest request);

    GetCustomerResponse getCustomerByCode(GetCustomerRequest request);

    List<CustomerDTO> lookupCustomersWithDetails(String keyword);

    List<CustomerDTO> getCustomersByCodeEmailOrMobile(String key);

    List<CustomerSearchDTO> lookupCustomers(String keyword);

}
