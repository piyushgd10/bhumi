/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 20, 2012
 *  @author Sunny
 */
package com.uniware.services.customer.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.utils.ValidatorUtils;
import com.unifier.services.aspect.RollbackOnFailure;
import com.uniware.core.api.customer.CreateCustomerRequest;
import com.uniware.core.api.customer.CreateCustomerResponse;
import com.uniware.core.api.customer.EditCustomerRequest;
import com.uniware.core.api.customer.EditCustomerResponse;
import com.uniware.core.api.customer.GetCustomerRequest;
import com.uniware.core.api.customer.GetCustomerResponse;
import com.uniware.core.api.customer.WsCustomer;
import com.uniware.core.api.party.WsPartyAddress;
import com.uniware.core.api.party.WsPartyContact;
import com.uniware.core.api.party.dto.CustomerDTO;
import com.uniware.core.api.party.dto.CustomerSearchDTO;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.entity.Customer;
import com.uniware.core.entity.PartyAddressType;
import com.uniware.core.entity.PartyContact;
import com.uniware.dao.customer.ICustomerDao;
import com.uniware.services.customer.ICustomerService;
import com.uniware.services.party.IGenericPartyService;
import com.uniware.services.party.IPartyService;

@Service("customerService")
@Transactional
public class CustomerServiceImpl implements ICustomerService {
    @Autowired
    private ICustomerDao         customerDao;

    @Autowired
    private IPartyService        partyService;

    @Autowired
    private IGenericPartyService genericPartyService;

    @Override
    public Customer getCustomerByCode(String code) {
        Customer v = customerDao.getCustomerByCode(code);
        if (v != null) {
            for (PartyContact partyContact : v.getPartyContacts()) {
                Hibernate.initialize(partyContact);
                Hibernate.initialize(partyContact.getPartyContactType());
            }
        }
        return v;
    }

    @Override
    public Customer getCustomerById(int vendorId) {
        return customerDao.getCustomerById(vendorId);
    }

    @Override
    @Transactional
    @RollbackOnFailure
    public CreateCustomerResponse createCustomer(CreateCustomerRequest request) {
        CreateCustomerResponse response = new CreateCustomerResponse();
        // TODO - should be removed when all Party creation/editing are moved to new services
        if (request.getCustomer() != null) {
            updateRequestBeforeValidation(request.getCustomer());
        }
        ValidationContext context = request.validate(true);
        Customer customer = null;
        if (!context.hasErrors()) {
            customer = customerDao.getCustomerByCode(request.getCustomer().getCode());
            if (customer != null) {
                context.addError(WsResponseCode.DUPLICATE_CUSTOMER_CODE, "Customer Code already exists");
            } else {
                customer = new Customer();
                genericPartyService.prepareParty(customer, request.getCustomer(), context, true);
                if (!context.hasErrors()) {
                    customer.setProvidesCform(request.getCustomer().isProvidesCform());
                    customer.setDualCompanyRetail(request.getCustomer().isDualCompanyRetail());
                    customer = customerDao.createCustomer(customer);
                }
            }
        }

        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        } else {
            response.setCustomer(new CustomerDTO(customer));
            response.setSuccessful(true);
        }
        return response;
    }

    @Override
    public EditCustomerResponse editCustomer(EditCustomerRequest request) {
        EditCustomerResponse response = new EditCustomerResponse();
        // TODO - should be removed when all Party creation/editing are moved to new services
        if (request.getCustomer() != null) {
            updateRequestBeforeValidation(request.getCustomer());
        }
        ValidationContext context = request.validate(false);
        Customer customer = null;
        if (!context.hasErrors()) {
            customer = customerDao.getCustomerByCode(request.getCustomer().getCode());
            if (customer == null) {
                context.addError(WsResponseCode.INVALID_CUSTOMER_CODE, "Invalid Customer Code");
            } else {
                genericPartyService.prepareParty(customer, request.getCustomer(), context);
                if (!context.hasErrors()) {
                    customer.setProvidesCform(ValidatorUtils.getOrDefaultValue(request.getCustomer().isProvidesCform(), customer.isProvidesCform()));
                    customer.setDualCompanyRetail(ValidatorUtils.getOrDefaultValue(request.getCustomer().isDualCompanyRetail(), customer.isDualCompanyRetail()));
                    customer = customerDao.updateCustomer(customer);
                }
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        } else {
            response.setCustomer(new CustomerDTO(customer));
            response.setSuccessful(true);
        }
        return response;
    }

    @Override
    public GetCustomerResponse getCustomerByCode(GetCustomerRequest request) {
        GetCustomerResponse response = new GetCustomerResponse();
        ValidationContext context = request.validate();
        Customer customer = null;
        if (!context.hasErrors()) {
            customer = customerDao.getCustomerByCode(request.getCode());
            if (customer == null) {
                context.addError(WsResponseCode.INVALID_CUSTOMER_CODE);
                response.setErrors(context.getErrors());
            } else {
                response.setCustomer(new CustomerDTO(customer));
                response.setSuccessful(true);
            }
        }

        return response;
    }

    @Override
    @Transactional(readOnly = true)
    public List<CustomerDTO> lookupCustomersWithDetails(String keyword) {
        List<CustomerDTO> customers = new ArrayList<CustomerDTO>();
        for (Customer customer : customerDao.getCustomersByCode(keyword)) {
            customers.add(new CustomerDTO(customer));
        }
        return customers;
    }

    @Override
    @Transactional(readOnly = true)
    public List<CustomerSearchDTO> lookupCustomers(String keyword) {
        List<CustomerSearchDTO> customers = new ArrayList<CustomerSearchDTO>();
        for (Customer customer : customerDao.getCustomersByCode(keyword)) {
            CustomerSearchDTO customerSearchDTO = new CustomerSearchDTO();
            customerSearchDTO.setId(customer.getId());
            customerSearchDTO.setCode(customer.getCode());
            customerSearchDTO.setName(customer.getName());
            customers.add(customerSearchDTO);
        }
        return customers;
    }

    @Override
    @Transactional(readOnly = true)
    public List<CustomerDTO> getCustomersByCodeEmailOrMobile(String key) {
        Set<CustomerDTO> customers = new HashSet<CustomerDTO>();
        List<Customer> dbCustomers = customerDao.getCustomerByCodeEmailOrMobile(key);
        for (Customer customer : dbCustomers) {
            customers.add(new CustomerDTO(customer));
        }
        return Arrays.asList(customers.toArray(new CustomerDTO[0]));
    }

    /**
     * adds partyCode, addressType to addresses before validating request, temporary, should be removed when all Party
     * creation/editing are moved to new services
     * 
     * @param customer
     */
    private void updateRequestBeforeValidation(WsCustomer customer) {
        if (customer.getCode() != null) {
            WsPartyAddress bAddress = customer.getBillingAddress();
            if (bAddress != null) {
                bAddress.setPartyCode(customer.getCode());
                bAddress.setAddressType(PartyAddressType.Code.BILLING.name());
            }
            WsPartyAddress sAddress = customer.getShippingAddress();
            if (sAddress != null) {
                sAddress.setPartyCode(customer.getCode());
                sAddress.setAddressType(PartyAddressType.Code.SHIPPING.name());
            }
            if (customer.getPartyContacts() != null) {
                for (WsPartyContact contact : customer.getPartyContacts()) {
                    contact.setPartyCode(customer.getCode());
                }
            }
        }
    }
}
