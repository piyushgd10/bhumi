/*
*  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
*  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
*  
*  @version     1.0, 29/08/14
*  @author sunny
*/

package com.uniware.services.oauth.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.uniware.core.entity.OAuthAccessToken;
import com.uniware.core.entity.OAuthRefreshToken;
import com.uniware.dao.oauth.IOAuthDao;
import com.uniware.services.oauth.IOAuthService;

@Service
public class OAuthServiceImpl implements IOAuthService {

    @Autowired
    private IOAuthDao oAuthDao;

    @Override
    @Transactional
    public void save(OAuthAccessToken oAuthAccessToken) {
        oAuthDao.save(oAuthAccessToken);
    }

    @Override
    @Transactional
    public void save(OAuthRefreshToken oAuthRefreshToken) {
        oAuthDao.save(oAuthRefreshToken);
    }

    @Override
    @Transactional
    public OAuthAccessToken getOAuthAccessTokenByTokenId(String tokenId) {
        return oAuthDao.getOAuthAccessTokenByTokenId(tokenId);
    }

    @Override
    @Transactional
    public OAuthRefreshToken getOAUthRefreshTokenByTokenId(String tokenId) {
        return oAuthDao.getOAUthRefreshTokenByTokenId(tokenId);
    }

    @Override
    @Transactional
    public OAuthAccessToken getOAuthAccessTokenByAuthenticationId(String authenticationId) {
        return oAuthDao.getOAuthAccessTokenByAuthenticationId(authenticationId);
    }

    @Override
    @Transactional
    public boolean deleteOAuthAccessTokenByTokenId(String tokenId) {
        return oAuthDao.deleteOAuthAccessTokenByTokenId(tokenId);
    }

    @Override
    @Transactional
    public boolean deleteOAuthRefreshTokenByTokenId(String tokenId) {
        return oAuthDao.deleteOAuthRefreshTokenByTokenId(tokenId);
    }

    @Override
    @Transactional
    public boolean deleteOAuthAccessTokenByRefreshToken(String refreshToken) {
        return oAuthDao.deleteOAuthAccessTokenByRefreshToken(refreshToken);
    }
}
