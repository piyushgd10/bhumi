/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jul 3, 2012
 *  @author singla
 */
package com.uniware.services.jabong;

import com.uniware.core.api.jabong.JabongCreateItemsForPORequest;
import com.uniware.core.api.jabong.JabongCreateItemsForPOResponse;
import com.uniware.core.api.jabong.JabongQualityAcceptItemRequest;
import com.uniware.core.api.jabong.JabongQualityAcceptItemResponse;
import com.uniware.core.api.jabong.JabongQualityRejectItemRequest;
import com.uniware.core.api.jabong.JabongQualityRejectItemResponse;
import com.uniware.core.api.jabong.JabongReceiveItemFromWholesaleRequest;
import com.uniware.core.api.jabong.JabongReceiveItemFromWholesaleResponse;
import com.uniware.core.api.jabong.JabongWholesaleCreateShipmentRequest;
import com.uniware.core.api.jabong.JabongWholesaleCreateShipmentResponse;
import com.uniware.core.api.jabong.JabongWholesaleHandleShipmentReturnRequest;
import com.uniware.core.api.jabong.JabongWholesaleHandleShipmentReturnResponse;
import com.uniware.core.api.jabong.ReleaseShelfRequest;
import com.uniware.core.api.jabong.ReleaseShelfResponse;

public interface IJabongService {

    JabongReceiveItemFromWholesaleResponse receiveItemFromWholesale(JabongReceiveItemFromWholesaleRequest request);

    JabongCreateItemsForPOResponse jabongCreateItemsForPO(JabongCreateItemsForPORequest request);

    JabongQualityAcceptItemResponse jabongQualityAcceptItem(JabongQualityAcceptItemRequest request);

    JabongQualityRejectItemResponse jabongQualityRejectItem(JabongQualityRejectItemRequest request);

    JabongWholesaleHandleShipmentReturnResponse jabongWholesaleHandleShipmentReturn(JabongWholesaleHandleShipmentReturnRequest request);

    JabongWholesaleCreateShipmentResponse jabongWholesaleCreateShipment(JabongWholesaleCreateShipmentRequest request);

    String getItemLabelHtml(String itemCode);

    ReleaseShelfResponse releaseShelf(ReleaseShelfRequest request);

}
