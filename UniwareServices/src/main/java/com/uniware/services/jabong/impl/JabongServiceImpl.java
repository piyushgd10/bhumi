/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jul 3, 2012
 *  @author singla
 */
package com.uniware.services.jabong.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.unifier.core.annotation.Level;
import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.api.validation.WsError;
import com.unifier.core.api.validation.WsWarning;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.entity.User;
import com.unifier.core.template.Template;
import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.NumberUtils;
import com.unifier.core.utils.StringUtils;
import com.unifier.services.aspect.RollbackOnFailure;
import com.uniware.core.api.currency.GetCurrencyConversionRateRequest;
import com.uniware.core.api.invoice.CreateShippingPackageInvoiceRequest;
import com.uniware.core.api.invoice.CreateShippingPackageInvoiceResponse;
import com.uniware.core.api.item.GetItemDetailRequest;
import com.uniware.core.api.item.GetItemDetailResponse;
import com.uniware.core.api.item.GetItemDetailResponse.SaleOrderItemDTO;
import com.uniware.core.api.jabong.JabongCreateItemsForPORequest;
import com.uniware.core.api.jabong.JabongCreateItemsForPOResponse;
import com.uniware.core.api.jabong.JabongCreateItemsForPOResponse.JabongItemDTO;
import com.uniware.core.api.jabong.JabongQualityAcceptItemRequest;
import com.uniware.core.api.jabong.JabongQualityAcceptItemResponse;
import com.uniware.core.api.jabong.JabongQualityRejectItemRequest;
import com.uniware.core.api.jabong.JabongQualityRejectItemResponse;
import com.uniware.core.api.jabong.JabongReceiveItemFromWholesaleRequest;
import com.uniware.core.api.jabong.JabongReceiveItemFromWholesaleRequest.WsJabongWholesaleSaleOrderItem;
import com.uniware.core.api.jabong.JabongReceiveItemFromWholesaleResponse;
import com.uniware.core.api.jabong.JabongWholesaleCreateShipmentRequest;
import com.uniware.core.api.jabong.JabongWholesaleCreateShipmentResponse;
import com.uniware.core.api.jabong.JabongWholesaleHandleShipmentReturnRequest;
import com.uniware.core.api.jabong.JabongWholesaleHandleShipmentReturnResponse;
import com.uniware.core.api.jabong.ReleaseShelfRequest;
import com.uniware.core.api.jabong.ReleaseShelfResponse;
import com.uniware.core.api.purchase.WsPurchaseOrderItem;
import com.uniware.core.api.shipping.AllocateShippingProviderRequest;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.cache.FacilityCache;
import com.uniware.core.entity.InflowReceipt;
import com.uniware.core.entity.InflowReceiptItem;
import com.uniware.core.entity.InventoryLedger;
import com.uniware.core.entity.Item;
import com.uniware.core.entity.ItemType;
import com.uniware.core.entity.ItemTypeInventory;
import com.uniware.core.entity.ItemTypeInventory.Type;
import com.uniware.core.entity.PurchaseOrder;
import com.uniware.core.entity.PurchaseOrderItem;
import com.uniware.core.entity.SaleOrder;
import com.uniware.core.entity.SaleOrderItem;
import com.uniware.core.entity.Shelf;
import com.uniware.core.entity.ShippingPackage;
import com.uniware.core.entity.Vendor;
import com.uniware.core.entity.VendorItemType;
import com.uniware.core.locking.Namespace;
import com.uniware.core.locking.annotation.Lock;
import com.uniware.core.locking.annotation.Locks;
import com.uniware.core.utils.Constants;
import com.uniware.core.utils.UserContext;
import com.uniware.core.vo.PrintTemplateVO;
import com.uniware.dao.inflow.IInflowDao;
import com.uniware.dao.inventory.IInventoryDao;
import com.uniware.dao.jabong.IJabongDao;
import com.uniware.dao.purchase.IPurchaseDao;
import com.uniware.dao.saleorder.ISaleOrderDao;
import com.uniware.services.cache.PrintTemplateCache;
import com.uniware.services.configuration.TenantSystemConfiguration;
import com.uniware.services.currency.ICurrencyService;
import com.uniware.services.exception.InventoryNotAvailableException;
import com.uniware.services.exception.ShelfBlockedForCycleCountException;
import com.uniware.services.inflow.IInflowService;
import com.uniware.services.inventory.IInventoryService;
import com.uniware.services.item.IItemService;
import com.uniware.services.jabong.IJabongService;
import com.uniware.services.ledger.IInventoryLedgerService;
import com.uniware.services.purchase.IPurchaseService;
import com.uniware.services.shipping.IShippingInvoiceService;
import com.uniware.services.shipping.IShippingProviderService;
import com.uniware.services.shipping.IShippingService;
import com.uniware.services.vendor.IVendorService;
import com.uniware.services.warehouse.IShelfService;

@Service("jabongService")
public class JabongServiceImpl implements IJabongService {

    public static final String       SHELF_CODE            = "STANDARD";
    private static final Logger      LOG                   = LoggerFactory.getLogger(JabongServiceImpl.class);
    private static final String      WHOLESALE_VENDOR_CODE = "JADE";

    private static final String      JABONG_KEY            = "JABONG";

    @Autowired
    private IInventoryService        inventoryService;

    @Autowired
    private IInventoryDao            inventoryDao;

    @Autowired
    private ISaleOrderDao            saleOrderDao;

    @Autowired
    private IJabongDao               jabongDao;

    @Autowired
    private IPurchaseService         purchaseService;

    @Autowired
    private IPurchaseDao             purchaseDao;

    @Autowired
    private IVendorService           vendorService;

    @Autowired
    private IShippingService         shippingService;

    @Autowired
    private IShippingProviderService shippingProviderService;

    @Autowired
    private IInflowService           inflowService;

    @Autowired
    private IItemService             itemService;

    @Autowired
    private IInflowDao               inflowDao;

    @Autowired
    private IShelfService            shelfService;

    @Autowired
    private ICurrencyService         currencyService;

    @Autowired
    private IShippingInvoiceService  iShippingInvoiceService;

    @Autowired
    private IInventoryLedgerService  inventoryLedgerService;

    @Override
    @Locks({ @Lock(ns = Namespace.JABONG, key = JABONG_KEY, level = Level.FACILITY) })
    @Transactional
    @RollbackOnFailure
    public JabongReceiveItemFromWholesaleResponse receiveItemFromWholesale(JabongReceiveItemFromWholesaleRequest request) {
        JabongReceiveItemFromWholesaleResponse response = new JabongReceiveItemFromWholesaleResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            Map<String, Item> codeToItems = new HashMap<>();
            Map<String, SaleOrderItem> codeToSaleOrderItems = new HashMap<>();
            for (WsJabongWholesaleSaleOrderItem wsSaleOrderItem : request.getSaleOrderItems()) {
                Item item = inventoryService.getItemByCode(wsSaleOrderItem.getItemCode());
                if (item != null && !Item.StatusCode.LIQUIDATED.name().equals(item.getStatusCode())) {
                    context.addError(WsResponseCode.DUPLICATE_ITEM_CODE);
                } else {
                    SaleOrderItem saleOrderItem = jabongDao.getSaleOrderItemByCodeByStatus(wsSaleOrderItem.getSaleOrderItemCode(), SaleOrderItem.StatusCode.UNFULFILLABLE.name());
                    if (saleOrderItem == null) {
                        context.addError(WsResponseCode.INVALID_SALE_ORDER_ITEM_STATE);
                    } else {
                        codeToItems.put(item.getCode(), item);
                        codeToSaleOrderItems.put(saleOrderItem.getCode(), saleOrderItem);
                    }
                }

            }

            if (!context.hasErrors()) {
                List<SaleOrderItem> saleOrderItems = new ArrayList<SaleOrderItem>();
                for (WsJabongWholesaleSaleOrderItem wsSaleOrderItem : request.getSaleOrderItems()) {
                    SaleOrderItem saleOrderItem = codeToSaleOrderItems.get(wsSaleOrderItem.getItemCode());
                    Item item = codeToItems.get(wsSaleOrderItem.getSaleOrderItemCode());
                    saleOrderItem.setStatusCode(SaleOrderItem.StatusCode.FULFILLABLE.name());
                    item = addPurchaseDetails(saleOrderItem.getItemType(), item, item.getCode(),
                            saleOrderItem.getSellingPrice().add(saleOrderItem.getVoucherValue()).add(saleOrderItem.getStoreCredit()), response);
                    saleOrderItem.setItemTypeInventory(getItemTypeInventory(saleOrderItem.getItemType(), response));
                    if (!response.hasErrors()) {
                        item.setStatusCode(Item.StatusCode.ALLOCATED.name());
                        saleOrderItem.setItem(item);
                        saleOrderItems.add(saleOrderItem);
                    }
                }
                if (!context.hasErrors()) {
                    ShippingPackage shippingPackage = shippingService.createShippingPackage(saleOrderItems, false);
                    shippingPackage.setStatusCode(ShippingPackage.StatusCode.PICKED.name());

                    CreateShippingPackageInvoiceRequest createShippingPackageInvoiceRequest = new CreateShippingPackageInvoiceRequest();
                    createShippingPackageInvoiceRequest.setUserId(ConfigurationManager.getInstance().getConfiguration(TenantSystemConfiguration.class).getSystemUser().getId());
                    createShippingPackageInvoiceRequest.setShippingPackageCode(shippingPackage.getCode());
                    CreateShippingPackageInvoiceResponse createShippingPackageInvoiceResponse = iShippingInvoiceService.createShippingPackageInvoice(
                            createShippingPackageInvoiceRequest);
                    response.addErrors(createShippingPackageInvoiceResponse.getErrors());
                    if (!response.hasErrors()) {
                        response.setSuccessful(true);
                    }
                }
            }
        }
        response.addErrors(context.getErrors());
        return response;
    }

    private ItemTypeInventory getItemTypeInventory(ItemType itemType, JabongReceiveItemFromWholesaleResponse response) {
        Shelf shelf = shelfService.getShelfByCode(SHELF_CODE);
        if (shelf == null) {
            response.addError(new WsError(10000, "INVALID_CONFIGURATION"));
        } else {
            ItemTypeInventory itemTypeInventory = inventoryDao.getInventory(itemType, Type.GOOD_INVENTORY, shelf);
            if (itemTypeInventory == null) {
                itemTypeInventory = new ItemTypeInventory(shelf, itemType, Type.GOOD_INVENTORY, Constants.ITEM_SKU_OR_NONE_TRACEABILITY_AGEING_DATE, 0, 0,
                        DateUtils.getCurrentTime(), DateUtils.getCurrentTime());
                itemTypeInventory = inventoryDao.addItemTypeInventory(itemTypeInventory);
            }
            return itemTypeInventory;
        }
        return null;
    }

    private Item addPurchaseDetails(ItemType itemType, Item item, String itemCode, BigDecimal sellingPrice, JabongReceiveItemFromWholesaleResponse response) {
        //get today's purchase order - pattern POyyyyMMdd
        PurchaseOrder purchaseOrder = purchaseService.getPurchaseOrderByCode(getPurchaseOrderCode());
        if (purchaseOrder == null) {
            purchaseOrder = createPurchaseOrder(response);
        }
        if (!response.hasErrors()) {
            PurchaseOrderItem purchaseOrderItem = null;
            for (PurchaseOrderItem orderItem : purchaseOrder.getPurchaseOrderItems()) {
                if (itemType.getId().equals(orderItem.getItemType().getId())) {
                    purchaseOrderItem = orderItem;
                }
            }
            if (purchaseOrderItem == null) {
                purchaseOrderItem = createPurchaseOrderItem(itemType, purchaseOrder, response, sellingPrice);
            } else {
                purchaseOrderItem.setQuantity(purchaseOrderItem.getQuantity() + 1);
            }
            if (!response.hasErrors()) {
                purchaseOrderItem.setReceivedQuantity(purchaseOrderItem.getReceivedQuantity() + 1);
                InflowReceipt inflowReceipt;
                if (purchaseOrder.getInflowReceipts().size() > 0) {
                    inflowReceipt = purchaseOrder.getInflowReceipts().iterator().next();
                } else {
                    inflowReceipt = createInflowReceipt(purchaseOrder);
                }
                InflowReceiptItem inflowReceiptItem = null;
                for (InflowReceiptItem receiptItem : inflowReceipt.getInflowReceiptItems()) {
                    if (itemType.getId().equals(receiptItem.getItemType().getId())) {
                        inflowReceiptItem = receiptItem;
                    }
                }
                if (inflowReceiptItem == null) {
                    inflowReceiptItem = createInflowReceiptItem(inflowReceipt, purchaseOrderItem, itemType);
                    inflowReceiptItem.setStatusCode(InflowReceiptItem.StatusCode.COMPLETE.name());
                }
                inflowReceiptItem.setQuantity(inflowReceiptItem.getQuantity() + 1);
                inflowReceiptItem.setSubtotal(NumberUtils.multiply(inflowReceiptItem.getUnitPrice(), inflowReceiptItem.getQuantity()));
                inflowService.prepareinflowReceiptItemTaxation(inflowReceiptItem, purchaseOrderItem);
                if (item == null) {
                    item = createItem(inflowReceiptItem, purchaseOrder.getVendor(), itemType, itemCode);
                }
                return item;
            }
        }
        return null;
    }

    private Item createItem(InflowReceiptItem inflowReceiptItem, Vendor vendor, ItemType itemType, String itemCode) {
        Item item = new Item(inflowReceiptItem, vendor, itemType, DateUtils.getCurrentTime(), DateUtils.getCurrentTime());
        item.setCode(itemCode);
        item.setStatusCode(Item.StatusCode.ALLOCATED.name());
        return inventoryDao.addItem(item, false);
    }

    private InflowReceiptItem createInflowReceiptItem(InflowReceipt inflowReceipt, PurchaseOrderItem purchaseOrderItem, ItemType itemType) {
        InflowReceiptItem inflowReceiptItem = new InflowReceiptItem(purchaseOrderItem, inflowReceipt, 0, purchaseOrderItem.getUnitPrice(), DateUtils.getCurrentTime(),
                DateUtils.getCurrentTime());
        inflowReceiptItem.setStatusCode(InflowReceiptItem.StatusCode.COMPLETE.name());
        inflowReceiptItem.setItemType(purchaseOrderItem.getItemType());
        inflowReceiptItem.setUnitPrice(purchaseOrderItem.getUnitPrice());
        inflowReceiptItem.setSubtotal(BigDecimal.ZERO);
        inflowReceiptItem.setTotal(BigDecimal.ZERO);
        inflowReceiptItem.setMaxRetailPrice(purchaseOrderItem.getMaxRetailPrice());
        inflowReceiptItem = inflowDao.addInflowReceiptItem(inflowReceiptItem);
        inflowReceipt.getInflowReceiptItems().add(inflowReceiptItem);
        return inflowReceiptItem;
    }

    private InflowReceipt createInflowReceipt(PurchaseOrder purchaseOrder) {
        InflowReceipt inflowReceipt = new InflowReceipt();
        inflowReceipt.setPurchaseOrder(purchaseOrder);
        inflowReceipt.setStatusCode(InflowReceipt.StatusCode.COMPLETE.name());
        inflowReceipt.setVendorInvoiceNumber(purchaseOrder.getCode());
        inflowReceipt.setVendorInvoiceDate(DateUtils.getCurrentTime());
        inflowReceipt.setUser(ConfigurationManager.getInstance().getConfiguration(TenantSystemConfiguration.class).getSystemUser());
        inflowReceipt.setCurrencyCode(purchaseOrder.getCurrencyCode());
        inflowReceipt.setCurrencyConversionRate(
                currencyService.getCurrencyConversionRate(new GetCurrencyConversionRateRequest(inflowReceipt.getCurrencyCode())).getConversionRate());
        inflowReceipt.setCreated(DateUtils.getCurrentTime());
        inflowReceipt.setUpdated(DateUtils.getCurrentTime());
        inflowReceipt = inflowDao.addInflowReceipt(inflowReceipt);
        return inflowReceipt;
    }

    private PurchaseOrderItem createPurchaseOrderItem(ItemType itemType, PurchaseOrder purchaseOrder, JabongReceiveItemFromWholesaleResponse response, BigDecimal sellingPrice) {
        PurchaseOrderItem purchaseOrderItem = new PurchaseOrderItem();
        WsPurchaseOrderItem wsPurchaseOrderItem = new WsPurchaseOrderItem();
        wsPurchaseOrderItem.setUnitPrice(sellingPrice);
        wsPurchaseOrderItem.setQuantity(1);
        Vendor vendor = vendorService.getVendorById(purchaseOrder.getVendor().getId());
        purchaseService.preparePurchaseOrderItem(purchaseOrderItem, purchaseOrder, vendor, itemType, itemType.getSkuCode(), null, wsPurchaseOrderItem);
        purchaseOrderItem = purchaseDao.addPuchaseOrderItem(purchaseOrderItem);
        purchaseOrder.getPurchaseOrderItems().add(purchaseOrderItem);
        return purchaseOrderItem;
    }

    private PurchaseOrder createPurchaseOrder(JabongReceiveItemFromWholesaleResponse response) {
        Vendor vendor = vendorService.getVendorByCode(WHOLESALE_VENDOR_CODE);
        if (vendor == null || vendor.getVendorAgreements().size() == 0) {
            response.addError(new WsError(10000, "INVALID_CONFIGURATION"));
        }
        PurchaseOrder purchaseOrder = new PurchaseOrder();
        purchaseOrder.setCode(getPurchaseOrderCode());
        purchaseOrder.setType(PurchaseOrder.Type.MANUAL.name());
        purchaseOrder.setVendor(vendor);
        purchaseOrder.setVendorAgreement(vendor.getVendorAgreements().iterator().next());
        purchaseOrder.setStatusCode(PurchaseOrder.StatusCode.COMPLETE.name());
        purchaseOrder.setFromParty(CacheManager.getInstance().getCache(FacilityCache.class).getCurrentFacility());
        User user = ConfigurationManager.getInstance().getConfiguration(TenantSystemConfiguration.class).getSystemUser();
        purchaseOrder.setUser(user);
        purchaseOrder.setCurrencyCode(ConfigurationManager.getInstance().getConfiguration(TenantSystemConfiguration.class).getBaseCurrency());
        purchaseOrder.setCurrencyConversionRate(
                currencyService.getCurrencyConversionRate(new GetCurrencyConversionRateRequest(purchaseOrder.getCurrencyCode())).getConversionRate());

        purchaseOrder.setLastUpdatedByUser(user);
        purchaseOrder.setCreated(DateUtils.getCurrentTime());
        purchaseOrder.setUpdated(DateUtils.getCurrentTime());
        purchaseOrder = purchaseDao.addPurchaseOrder(purchaseOrder);
        return purchaseOrder;
    }

    private String getPurchaseOrderCode() {
        return new StringBuilder().append("PO").append(DateUtils.dateToString(DateUtils.getCurrentTime(), "yyyyMMdd")).toString();
    }

    @Override
    @Transactional
    public JabongCreateItemsForPOResponse jabongCreateItemsForPO(JabongCreateItemsForPORequest request) {
        JabongCreateItemsForPOResponse response = new JabongCreateItemsForPOResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            PurchaseOrder purchaseOrder = purchaseDao.getPurchaseOrderByCode(request.getPurchaseOrderCode());
            if (purchaseOrder == null) {
                context.addError(WsResponseCode.INVALID_PURCHASE_ORDER_CODE);
            } else if (!PurchaseOrder.StatusCode.APPROVED.name().equals(purchaseOrder.getStatusCode())) {
                context.addError(WsResponseCode.INVALID_PURCHASE_ORDER_STATE);
            } else {
                List<PurchaseOrderItem> purchaseOrderItems = new ArrayList<PurchaseOrderItem>();
                purchaseOrderItems.addAll(purchaseOrder.getPurchaseOrderItems());
                Collections.sort(purchaseOrderItems, new Comparator<PurchaseOrderItem>() {
                    @Override
                    public int compare(PurchaseOrderItem o1, PurchaseOrderItem o2) {
                        return o1.getMaxRetailPrice().compareTo(o2.getMaxRetailPrice());
                    }
                });
                int i = 0;
                for (PurchaseOrderItem purchaseOrderItem : purchaseOrderItems) {
                    int pendingQuantity = purchaseOrderItem.getQuantity() - purchaseOrderItem.getReceivedQuantity();
                    if (pendingQuantity < 1) {
                        continue;
                    }
                    VendorItemType vendorItemType = vendorService.getVendorItemType(purchaseOrder.getVendor().getCode(), purchaseOrderItem.getItemType().getSkuCode());
                    List<Item> items = inventoryService.addItems(purchaseOrderItem.getItemType(), purchaseOrder.getVendor(), pendingQuantity, Item.StatusCode.UNPROCESSED,
                            vendorItemType);
                    i++;
                    for (Item item : items) {
                        Hibernate.initialize(item.getItemType());
                        response.getItems().add(new JabongItemDTO(item,
                                (purchaseOrder.getCode().length() > 3 ? purchaseOrder.getCode().substring(purchaseOrder.getCode().length() - 3) : purchaseOrder.getCode()) + "-"
                                        + i));
                    }
                }
                response.setSuccessful(true);
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Locks({ @Lock(ns = Namespace.JABONG, key = JABONG_KEY, level = Level.FACILITY, log = true) })
    @Transactional
    @RollbackOnFailure
    public JabongQualityAcceptItemResponse jabongQualityAcceptItem(JabongQualityAcceptItemRequest request) {
        JabongQualityAcceptItemResponse response = new JabongQualityAcceptItemResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            Item item = inventoryDao.getItemByCode(request.getItemCode());
            if (item == null) {
                context.addError(WsResponseCode.INVALID_ITEM_CODE, "Invalid Item Code");
            } else if (Item.StatusCode.QC_PENDING.name().equals(item.getStatusCode())) {
                SaleOrderItem saleOrderItem = jabongDao.getSaleOrderItemForAllocation(item.getItemType(), SaleOrderItem.StatusCode.UNFULFILLABLE);
                if (saleOrderItem != null) {
                    allocateSaleOrderItem(context, item, saleOrderItem);
                    if (!context.hasErrors()) {
                        item.setCreated(DateUtils.getCurrentTime());
                        item.setRejectionReason(request.getAcceptReason());
                        response.setNumberOfItems(getSaleOrderItemsCount(saleOrderItem));
                        response.setSaleOrderCode(saleOrderItem.getSaleOrder().getCode());
                        response.setSaleOrderItemCode(saleOrderItem.getCode());
                        response.setSuccessful(true);
                    }
                } else {
                    Shelf shelf = shelfService.getShelfByCode(SHELF_CODE);
                    if (shelf == null) {
                        response.addError(new WsError(10000, "INVALID_CONFIGURATION"));
                    } else {
                        item.setCreated(DateUtils.getCurrentTime());
                        item.setRejectionReason(request.getAcceptReason());
                        item.setStatusCode(Item.StatusCode.GOOD_INVENTORY.name());
                        item.setInventoryType(Type.GOOD_INVENTORY);
                        inventoryService.addInventory(item.getItemType(), Type.GOOD_INVENTORY, item.getAgeingStartDate(), shelf, 1);
                        response.addWarning(new WsWarning(10001, "No Sale Order pending for this item please putaway to default Shelf - " + shelf.getCode()));
                        response.setSuccessful(true);
                    }
                }
                if (!context.hasErrors()) {
                    InflowReceiptItem inflowReceiptItem = item.getInflowReceiptItem();
                    List<Item> pendingItems = inventoryDao.getItemsByInflowReceiptIdByStatus(inflowReceiptItem.getId(), Item.StatusCode.QC_PENDING);
                    if (pendingItems.size() == 0) {
                        inflowReceiptItem.setStatusCode(InflowReceiptItem.StatusCode.COMPLETE.name());
                        if (isInflowReceiptComplete(inflowReceiptItem.getInflowReceipt())) {
                            inflowReceiptItem.getInflowReceipt().setStatusCode(InflowReceipt.StatusCode.COMPLETE.name());
                        }
                    }
                }
            } else if (Item.StatusCode.GOOD_INVENTORY.name().equals(item.getStatusCode())) {
                SaleOrderItem saleOrderItem = jabongDao.getSaleOrderItemForAllocation(item.getItemType(), SaleOrderItem.StatusCode.FULFILLABLE);
                if (saleOrderItem != null) {
                    //                        saleOrderDao.getSaleOrderForUpdate(saleOrderItem.getSaleOrder().getId());
                    allocateSaleOrderItem(context, item, saleOrderItem);
                    if (!context.hasErrors()) {
                        response.setNumberOfItems(getSaleOrderItemsCount(saleOrderItem));
                        response.setSaleOrderCode(saleOrderItem.getSaleOrder().getCode());
                        response.setSaleOrderItemCode(saleOrderItem.getCode());
                        response.setSuccessful(true);
                    }
                } else {
                    response.addError(new WsError(10001, "No Sale Order pending for this item."));
                }
            } else {
                context.addError(WsResponseCode.INVALID_ITEM_STATE, "Only UNPROCESSED/GOOD_INVENTORY items can be QCed");
            }
            if (!context.hasErrors()) {
                addInventoryLedger(item);
            }
        }
        if (context.hasErrors()) {
            response.addErrors(context.getErrors());
        }
        return response;
    }

    private void addInventoryLedger(Item item) {
        InventoryLedger inventoryLedger = new InventoryLedger();
        inventoryLedger.setSkuCode(item.getItemType().getSkuCode());
        inventoryLedger.setTransactionIdentifier(item.getCode());
        inventoryLedger.setAdditionalInfo("");
        inventoryLedger.setTransactionType(InventoryLedger.TransactionType.CROSSDOCK);
        inventoryLedgerService.addInventoryLedgerEntry(inventoryLedger, 1, InventoryLedger.ChangeType.INCREASE);
    }

    private int getSaleOrderItemsCount(SaleOrderItem saleOrderItem) {
        int count = 0;
        for (SaleOrderItem orderItem : saleOrderItem.getSaleOrder().getSaleOrderItems()) {
            if (orderItem.getFacility() != null && saleOrderItem.getFacility().getId().equals(orderItem.getFacility().getId())) {
                count++;
            }
        }
        return count;
    }

    private boolean isInflowReceiptComplete(InflowReceipt inflowReceipt) {
        for (InflowReceiptItem inflowReceiptItem : inflowReceipt.getInflowReceiptItems()) {
            if (!InflowReceiptItem.StatusCode.COMPLETE.name().equals(inflowReceiptItem.getStatusCode())) {
                return false;
            }
        }
        return true;
    }

    //    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void allocateSaleOrderItem(ValidationContext context, Item item, SaleOrderItem saleOrderItem) {
        saleOrderItem = saleOrderDao.getSaleOrderItemById(saleOrderItem.getId());
        if (StringUtils.equalsAny(saleOrderItem.getStatusCode(), SaleOrderItem.StatusCode.FULFILLABLE.name(), SaleOrderItem.StatusCode.UNFULFILLABLE.name())) {
            item.setStatusCode(Item.StatusCode.ALLOCATED.name());
            saleOrderItem.setStatusCode(SaleOrderItem.StatusCode.FULFILLABLE.name());
            inventoryService.addInventory(saleOrderItem.getItemType(), Type.GOOD_INVENTORY, item.getAgeingStartDate(), shelfService.getShelfByCode(SHELF_CODE), 1);
            try {
                saleOrderItem.setItemTypeInventory(inventoryService.blockInventoryForLenskartAndJabongg(saleOrderItem.getItemType()));
            } catch (InventoryNotAvailableException e) {
                context.addError(WsResponseCode.INVENTORY_NOT_AVAILABLE, "Unexpected error: Not able to block added inventory");
                return;
            }
            saleOrderItem = saleOrderDao.updateSaleOrderItem(saleOrderItem);
            List<SaleOrderItem> orderItems = new ArrayList<SaleOrderItem>();
            for (SaleOrderItem orderItem : saleOrderItem.getSaleOrder().getSaleOrderItems()) {
                if (orderItem.getFacility() != null && orderItem.getFacility().getId().equals(saleOrderItem.getFacility().getId())
                        && (!orderItem.getStatusCode().equals(SaleOrderItem.StatusCode.CANCELLED.name()) || orderItem.getItem() == null)
                        && (orderItem.getShippingPackage() == null || orderItem.getShippingPackage().getStatusCode().equals(ShippingPackage.StatusCode.CREATED.name()))) {
                    orderItems.add(orderItem);
                }
            }
            if (orderItems.size() > 1) {
                Shelf shelf = null;
                for (SaleOrderItem orderItem : orderItems) {
                    if (orderItem.getItem() != null) {
                        shelf = orderItem.getItem().getShelf();
                        break;
                    }
                }
                if (shelf == null) {
                    shelf = jabongDao.getShelfForPutaway(orderItems.size());
                    if (shelf != null) {
                        shelf.setItemCount(1);
                    } else {
                        context.addError(WsResponseCode.INVALID_SHELF_CODE, "No shelf available for putaway");
                    }
                }
                if (!context.hasErrors()) {
                    item.setShelf(shelf);
                    saleOrderItem.setItem(item);
                }
            } else {
                item.setShelf(null);
                saleOrderItem.setItem(item);
            }
        } else {
            context.addError(WsResponseCode.INVALID_SALE_ORDER_ITEM_STATE);
        }
    }

    @Override
    @Locks({ @Lock(ns = Namespace.JABONG, key = JABONG_KEY, level = Level.FACILITY, log = true) })
    @Transactional
    @RollbackOnFailure
    public ReleaseShelfResponse releaseShelf(ReleaseShelfRequest request) {
        ReleaseShelfResponse response = new ReleaseShelfResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            Shelf shelf = jabongDao.getShelfByCode(request.getShelfCode());
            if (shelf == null) {
                context.addError(WsResponseCode.INVALID_SHELF_CODE, "Invalid Shelf Code");
            } else {
                shelf.setItemCount(0);
                response.setSuccessful(true);
            }
        }
        response.addErrors(context.getErrors());
        return response;
    }

    @Override
    @Locks({ @Lock(ns = Namespace.JABONG, key = JABONG_KEY, level = Level.FACILITY, log = true) })
    @Transactional
    @RollbackOnFailure
    public JabongQualityRejectItemResponse jabongQualityRejectItem(JabongQualityRejectItemRequest request) {
        // TODO: 21/07/17 @ADITYA @INVENTORY_AGEING_DATE Should I apply ageing changes here too? 
        JabongQualityRejectItemResponse response = new JabongQualityRejectItemResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            Item item = inventoryDao.getItemByCode(request.getItemCode());
            if (item == null) {
                context.addError(WsResponseCode.INVALID_ITEM_CODE, "Invalid Item Code");
            } else if (Item.StatusCode.QC_PENDING.name().equals(item.getStatusCode())) {
                item.setStatusCode(Item.StatusCode.QC_REJECTED.name());
                item.setInventoryType(Type.BAD_INVENTORY);
                item.setRejectionReason(request.getComments());
                item.setCreated(DateUtils.getCurrentTime());
                InflowReceiptItem inflowReceiptItem = item.getInflowReceiptItem();
                inflowReceiptItem.setRejectedQuantity(inflowReceiptItem.getRejectedQuantity() + 1);
                if (StringUtils.isNotBlank(request.getComments())
                        && (inflowReceiptItem.getComments() == null || !inflowReceiptItem.getComments().contains(request.getComments()))) {
                    inflowReceiptItem.setComments((StringUtils.isNotEmpty(inflowReceiptItem.getComments()) ? inflowReceiptItem.getComments() + "\n" : "") + request.getComments());
                }
                inflowReceiptItem.getPurchaseOrderItem().setRejectedQuantity(inflowReceiptItem.getPurchaseOrderItem().getRejectedQuantity() + 1);
                List<Item> pendingItems = inventoryDao.getItemsByInflowReceiptIdByStatus(inflowReceiptItem.getId(), Item.StatusCode.QC_PENDING);
                if (pendingItems.size() == 0) {
                    inflowReceiptItem.setStatusCode(InflowReceiptItem.StatusCode.COMPLETE.name());
                    if (isInflowReceiptComplete(inflowReceiptItem.getInflowReceipt())) {
                        inflowReceiptItem.getInflowReceipt().setStatusCode(InflowReceipt.StatusCode.COMPLETE.name());
                    }
                }
                response.setSuccessful(true);
            } else if (Item.StatusCode.GOOD_INVENTORY.name().equals(item.getStatusCode())) {
                item.setStatusCode(Item.StatusCode.BAD_INVENTORY.name());
                item.setInventoryType(Type.BAD_INVENTORY);
                SaleOrderItem saleOrderItem = jabongDao.getSaleOrderItemForInventoryDeallocation(item.getItemType());
                if (saleOrderItem != null) {
                    //                        saleOrderDao.getSaleOrderForUpdate(saleOrderItem.getSaleOrder().getId());
                    deallocateInventoryForSaleOrderItem(context, saleOrderItem);
                    if (!context.hasErrors()) {
                        response.setSuccessful(true);
                    }
                } else {
                    Shelf shelf = shelfService.getShelfByCode(SHELF_CODE);
                    if (shelf == null) {
                        response.addError(new WsError(10000, "INVALID_CONFIGURATION"));
                    } else {
                        try {
                            inventoryService.removeInventory(item.getItemType(), Type.GOOD_INVENTORY, shelf, 1);
                            response.setSuccessful(true);
                        } catch (InventoryNotAvailableException e) {
                            response.addError(new WsError(10001, "Invalid inventory state!!! Should not happen. Please contact product support team."));
                        } catch (ShelfBlockedForCycleCountException e) {
                            response.addError(new WsError(WsResponseCode.SHELF_BLOCKED_FOR_CYCLE_COUNT.message()));
                        }
                    }
                }
            } else {
                context.addError(WsResponseCode.INVALID_ITEM_STATE, "Only QC_PENDING items can be QCed");
            }
        }
        response.addErrors(context.getErrors());
        return response;
    }

    public void deallocateInventoryForSaleOrderItem(ValidationContext context, SaleOrderItem saleOrderItem) {
        saleOrderItem = saleOrderDao.getSaleOrderItemById(saleOrderItem.getId());
        if (!SaleOrderItem.StatusCode.FULFILLABLE.name().equals(saleOrderItem.getStatusCode())) {
            context.addError(WsResponseCode.INVALID_SALE_ORDER_ITEM_STATE);
        } else {
            saleOrderItem.setStatusCode(SaleOrderItem.StatusCode.CREATED.name());
            saleOrderItem.setItemTypeInventory(null);
        }
    }

    @Override
    @Locks({ @Lock(ns = Namespace.JABONG, key = JABONG_KEY, level = Level.FACILITY) })
    @Transactional
    @RollbackOnFailure
    public JabongWholesaleHandleShipmentReturnResponse jabongWholesaleHandleShipmentReturn(JabongWholesaleHandleShipmentReturnRequest request) {
        JabongWholesaleHandleShipmentReturnResponse response = new JabongWholesaleHandleShipmentReturnResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            SaleOrderItem saleOrderItem = jabongDao.getSaleOrderItemByCodeByStatus(request.getSaleOrderItemCode(), SaleOrderItem.StatusCode.DISPATCHED.name());
            if (saleOrderItem == null) {
                context.addError(WsResponseCode.INVALID_SALE_ORDER_ITEM_STATE);
            } else {
                saleOrderItem.setStatusCode(SaleOrderItem.StatusCode.CANCELLED.name());
                saleOrderItem.getItem().setInventoryType(Type.BAD_INVENTORY);
                saleOrderItem.getItem().setStatusCode(Item.StatusCode.BAD_INVENTORY.name());
                response.setSuccessful(true);
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Locks({ @Lock(ns = Namespace.JABONG, key = JABONG_KEY, level = Level.FACILITY, log = true) })
    @Transactional
    @RollbackOnFailure
    public JabongWholesaleCreateShipmentResponse jabongWholesaleCreateShipment(JabongWholesaleCreateShipmentRequest request) {
        long startTime = System.currentTimeMillis();
        JabongWholesaleCreateShipmentResponse response = new JabongWholesaleCreateShipmentResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            SaleOrder saleOrder = saleOrderDao.getSaleOrderByCode(request.getSaleOrderCode());
            List<SaleOrderItem> itemsToPackage = new ArrayList<SaleOrderItem>();
            List<SaleOrderItem> otherItems = new ArrayList<SaleOrderItem>();
            if (saleOrder == null) {
                context.addError(WsResponseCode.INVALID_SALE_ORDER_CODE);
            } else {
                for (SaleOrderItem saleOrderItem : saleOrder.getSaleOrderItems()) {
                    if (saleOrderItem.getFacility() != null && saleOrderItem.getFacility().getId().equals(UserContext.current().getFacilityId())) {
                        if (request.getSaleOrderItemCodes().remove(saleOrderItem.getCode())) {
                            if (!StringUtils.equalsAny(saleOrderItem.getStatusCode(), SaleOrderItem.StatusCode.FULFILLABLE.name(), SaleOrderItem.StatusCode.CANCELLED.name())
                                    || saleOrderItem.getItem() == null) {
                                context.addError(WsResponseCode.INVALID_SALE_ORDER_ITEM_STATE, "Item is not allocated for SaleOrderItem :" + saleOrderItem.getCode());
                            } else if (saleOrderItem.getShippingPackage() != null
                                    && ShippingPackage.StatusCode.READY_TO_SHIP.name().equals(saleOrderItem.getShippingPackage().getStatusCode())) {
                                context.addError(WsResponseCode.INVALID_SALE_ORDER_ITEM_STATE, "Shipment already created for SaleOrderItem :" + saleOrderItem.getCode());
                            } else {
                                itemsToPackage.add(saleOrderItem);
                            }
                        } else {
                            otherItems.add(saleOrderItem);
                        }
                    }
                }
                if (request.getSaleOrderItemCodes().size() > 0) {
                    context.addError(WsResponseCode.INVALID_SALE_ORDER_ITEM_CODE, "Invalid SaleOrderItem codes :[" + StringUtils.join('|', request.getSaleOrderItemCodes()) + "]");
                }
            }
            if (!context.hasErrors()) {
                ShippingPackage shippingPackage = shippingService.createShippingPackage(itemsToPackage, true);
                shippingPackage.setStatusCode(ShippingPackage.StatusCode.PICKED.name());
                if (!request.isDoNotCreateInvoice()) {

                    CreateShippingPackageInvoiceRequest createShippingPackageInvoiceRequest = new CreateShippingPackageInvoiceRequest();
                    createShippingPackageInvoiceRequest.setUserId(ConfigurationManager.getInstance().getConfiguration(TenantSystemConfiguration.class).getSystemUser().getId());
                    createShippingPackageInvoiceRequest.setShippingPackageCode(shippingPackage.getCode());
                    createShippingPackageInvoiceRequest.setCommitBlockedInventory(true);
                    CreateShippingPackageInvoiceResponse createShippingPackageInvoiceResponse = iShippingInvoiceService.createShippingPackageInvoice(
                            createShippingPackageInvoiceRequest);
                    response.addErrors(createShippingPackageInvoiceResponse.getErrors());
                    if (!response.hasErrors()) {
                        AllocateShippingProviderRequest aspRequest = new AllocateShippingProviderRequest();
                        aspRequest.setShippingPackageCode(shippingPackage.getCode());
                        response.addErrors(shippingProviderService.allocateShippingProvider(aspRequest).getErrors());
                    }
                }
                if (!response.hasErrors()) {
                    boolean releaseShelf = true;
                    if (otherItems.size() > 0) {
                        for (SaleOrderItem saleOrderItem : otherItems) {
                            if ((saleOrderItem.getShippingPackage() == null || ShippingPackage.StatusCode.CREATED.name().equals(saleOrderItem.getShippingPackage().getStatusCode()))
                                    && saleOrderItem.getItem() != null) {
                                releaseShelf = false;
                                break;
                            }
                        }
                    }
                    if (releaseShelf) {
                        Shelf shelf = itemsToPackage.iterator().next().getItem().getShelf();
                        if (shelf != null) {
                            shelf.setItemCount(0);
                        }
                    }
                    response.setSuccessful(true);
                    response.setShipmentCode(shippingPackage.getCode());
                }
            }
        }
        response.addErrors(context.getErrors());
        LOG.info("jabongWholesaleCreateShipmentInternal executed in {} ms", (System.currentTimeMillis() - startTime));
        return response;
    }

    @Override
    @Transactional
    public String getItemLabelHtml(String itemCode) {
        Item item = inventoryService.getItemByCode(itemCode);
        SaleOrderItem saleOrderItem = null;
        int fulfillableItems = 0;
        int totalItems = 0;
        if (item != null) {
            GetItemDetailRequest request = new GetItemDetailRequest();
            request.setItemCode(itemCode);
            GetItemDetailResponse response = itemService.getItemDetail(request);
            for (SaleOrderItemDTO saleOrderItemDTO : response.getItemDTO().getSaleOrderItemDTOs()) {
                if (saleOrderItemDTO.getSaleOrderItemStatus().equals(SaleOrderItem.StatusCode.FULFILLABLE.name())) {
                    saleOrderItem = saleOrderDao.getSaleOrderItemById(saleOrderItemDTO.getId());
                }
            }
            if (saleOrderItem != null) {
                for (SaleOrderItem temp : saleOrderItem.getSaleOrder().getSaleOrderItems()) {
                    if (temp.getFacility() != null && temp.getShippingPackage() == null && temp.getFacility().getId().equals(saleOrderItem.getFacility().getId())) {
                        totalItems++;
                        if (temp.getStatusCode().equals(SaleOrderItem.StatusCode.FULFILLABLE.name())) {
                            fulfillableItems++;
                        }
                    }
                }
                Template template = CacheManager.getInstance().getCache(PrintTemplateCache.class).getTemplateByType(PrintTemplateVO.Type.WORKORDER.name());
                Map<String, Object> params = new HashMap<String, Object>();
                params.put("item", item);
                params.put("fulfillableItems", fulfillableItems);
                params.put("totalItems", totalItems);
                params.put("saleOrderItem", saleOrderItem);
                return template.evaluate(params);
            }
        }
        return null;
    }
}
