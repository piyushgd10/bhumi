/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 *  @version     1.0, 23-Jul-2012
 *  @author praveeng
 */
package com.uniware.services.http.proxy.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.uniware.core.vo.HttpProxyVO;
import com.uniware.dao.http.proxy.IHttpProxyMao;
import com.uniware.services.http.proxy.IHttpProxyService;

@Service
public class HttpProxyServiceImpl implements IHttpProxyService {

    @Autowired
    private IHttpProxyMao httpProxyMao;

    @Override
    public HttpProxyVO getNextAvailableProxy(String sourceCode, Integer threshold) {
        HttpProxyVO httpProxy = httpProxyMao.getNextAvailableProxy(sourceCode,threshold);
        if(httpProxy.getUsedCount() == (threshold - 1)){
            httpProxy.setUsed(true);
            httpProxyMao.updateProxy(httpProxy);
        }
        return httpProxy;
    }
}