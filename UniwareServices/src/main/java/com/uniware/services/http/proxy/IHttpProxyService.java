/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 *  @version     1.0, 23-Jul-2012
 *  @author praveeng
 */
package com.uniware.services.http.proxy;

import com.uniware.core.vo.HttpProxyVO;

public interface IHttpProxyService {

    HttpProxyVO getNextAvailableProxy(String sourceCode, Integer threshold);

}
