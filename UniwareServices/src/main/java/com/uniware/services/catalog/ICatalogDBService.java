/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 14, 2012
 *  @author singla
 */
package com.uniware.services.catalog;

import com.uniware.core.entity.ItemType;

/**
 * @author singla
 */
public interface ICatalogDBService {

    /**
     * @param skuCode
     * @return
     */
    ItemType getItemTypeBySkuCodeDB(String skuCode);

    /**
     * @param itemTypeId
     * @return
     */
    ItemType getItemTypeByIdDB(int itemTypeId);

}
