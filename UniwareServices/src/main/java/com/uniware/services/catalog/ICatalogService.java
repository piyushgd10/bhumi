/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 *  @version     1.0, Dec 16, 2011
 *  @author singla
 */
package com.uniware.services.catalog;

import com.unifier.core.utils.DateUtils.DateRange;
import com.uniware.core.api.catalog.BulkCreateOrEditItemTypeRequest;
import com.uniware.core.api.catalog.BulkCreateOrEditItemTypeResponse;
import com.uniware.core.api.catalog.CreateCategoryRequest;
import com.uniware.core.api.catalog.CreateCategoryResponse;
import com.uniware.core.api.catalog.CreateItemTypeRequest;
import com.uniware.core.api.catalog.CreateItemTypeResponse;
import com.uniware.core.api.catalog.CreateOrEditCategoryRequest;
import com.uniware.core.api.catalog.CreateOrEditCategoryResponse;
import com.uniware.core.api.catalog.CreateOrEditFacilityItemTypeRequest;
import com.uniware.core.api.catalog.CreateOrEditFacilityItemTypeResponse;
import com.uniware.core.api.catalog.CreateOrEditItemTypeRequest;
import com.uniware.core.api.catalog.CreateOrEditItemTypeResponse;
import com.uniware.core.api.catalog.EditCategoryRequest;
import com.uniware.core.api.catalog.EditCategoryResponse;
import com.uniware.core.api.catalog.EditFacilityItemTypeRequest;
import com.uniware.core.api.catalog.EditFacilityItemTypeResponse;
import com.uniware.core.api.catalog.EditItemTypeRequest;
import com.uniware.core.api.catalog.EditItemTypeResponse;
import com.uniware.core.api.catalog.GetCategoryRequest;
import com.uniware.core.api.catalog.GetCategoryResponse;
import com.uniware.core.api.catalog.GetItemTypeByItemRequest;
import com.uniware.core.api.catalog.GetItemTypeByItemResponse;
import com.uniware.core.api.catalog.GetItemTypeRequest;
import com.uniware.core.api.catalog.GetItemTypeResponse;
import com.uniware.core.api.catalog.PrintItemTypeBarcodesRequest;
import com.uniware.core.api.catalog.PrintItemTypeBarcodesResponse;
import com.uniware.core.api.catalog.dto.CategorySearchDTO;
import com.uniware.core.api.catalog.dto.ItemTypeLookupDTO;
import com.uniware.core.api.channel.ChannelCatalogSyncMetadataVO;
import com.uniware.core.api.warehouse.SearchFacilityItemTypeRequest;
import com.uniware.core.api.warehouse.SearchFacilityItemTypeResponse;
import com.uniware.core.api.warehouse.SearchItemTypesRequest;
import com.uniware.core.api.warehouse.SearchItemTypesResponse;
import com.uniware.core.api.warehouse.SectionDTO;
import com.uniware.core.entity.Category;
import com.uniware.core.entity.FacilityItemType;
import com.uniware.core.entity.ItemDetailField;
import com.uniware.core.entity.ItemType;
import com.uniware.core.entity.Tag;
import com.uniware.core.entity.VendorItemType;
import com.uniware.services.cache.VendorCache.VendorVO;
import java.util.List;

/**
 * @author singla
 */
public interface ICatalogService {

    ItemType getItemTypeBySkuCode(String skuCode);

    ItemType getNonBundledItemTypeById(int itemTypeId);

    /**
     * @param categoryId
     * @return
     */
    Category getCategoryById(int categoryId);

    CreateItemTypeResponse createItemType(CreateItemTypeRequest itemTypeRequest);

    CreateCategoryResponse createCategory(CreateCategoryRequest categoryRequest);

    EditCategoryResponse editCategory(EditCategoryRequest categoryRequest);

    /**
     * @param name
     * @return
     */
    List<VendorVO> lookupVendors(String name);

    /**
     * @param name
     * @return
     */

    Tag addTag(String name);

    List<Tag> getAllTags();

    int removeTag(String name);

    SearchItemTypesResponse searchItemTypes(SearchItemTypesRequest request);

    EditItemTypeResponse editItemType(EditItemTypeRequest request);

    Category getCategoryByCode(String code);

    GetCategoryResponse listCategories(GetCategoryRequest request);

    GetCategoryResponse listCategories();

    List<SectionDTO> listSections();

    /**
     * @param vendorId
     * @param itemTypeId
     * @return
     */
    VendorItemType getVendorItemType(int vendorId, int itemTypeId);

    /**
     * @param id
     * @return
     */
    List<VendorItemType> getVendorItemTypeByItemTypeId(Integer id);

    CreateOrEditItemTypeResponse createOrEditItemType(CreateOrEditItemTypeRequest request);

    /**
     * @param keyword
     * @return
     */
    List<ItemTypeLookupDTO> lookupItemTypes(String keyword);

    ItemTypeLookupDTO prepareItemTypeLookupDTO(ItemType itemType);

    /**
     * @param name
     * @return
     */
    Tag getTagByName(String name);

    /**
     * @param request
     * @return
     */
    BulkCreateOrEditItemTypeResponse bulkCreateOrEditItemTypeRequest(BulkCreateOrEditItemTypeRequest request);

    List<ItemType> getItemTypes(int start, int noOfRecords, DateRange updatedIn);

    VendorItemType getVendorItemType(String vendorCode, Integer itemTypeId);

    List<VendorItemType> getVendorItemTypeByItemTypeIdByTenant(Integer itemTypeId);

    CreateOrEditFacilityItemTypeResponse createOrEditFacilityItemType(CreateOrEditFacilityItemTypeRequest request);

    CreateOrEditCategoryResponse createOrEditCategory(CreateOrEditCategoryRequest request);

    SearchFacilityItemTypeResponse searchFacilityItemType(SearchFacilityItemTypeRequest request);

    EditFacilityItemTypeResponse editFacilityItemType(EditFacilityItemTypeRequest editFacilityItemTypeRequest);

    List<Category> getCategories();

    List<ItemDetailField> getItemDetailFields();

    ItemType getAllItemTypeBySkuCode(String skuCode);

    List<ItemTypeLookupDTO> lookupAllItemTypes(String keyword);

    ItemType getAllItemTypeById(int itemTypeId);

    PrintItemTypeBarcodesResponse printItemTypeBarcodes(PrintItemTypeBarcodesRequest request);

    GetItemTypeResponse getItemType(GetItemTypeRequest request);

    ItemType getAllEnabledItemTypeBySkuCode(String skuCode);

    ItemType getEnabledItemTypeBySkuCodeDB(String skuCode);

    ItemType getEnabledItemTypeById(int itemTypeId);

    ItemType getAllEnabledItemTypeById(int itemTypeId);

    ChannelCatalogSyncMetadataVO updateChannelCatalogSyncMetadata(ChannelCatalogSyncMetadataVO catalogSyncPreprocessorDTO);

    ChannelCatalogSyncMetadataVO getChannelCatalogSyncPreprocessor(String sourceCode);

    List<CategorySearchDTO> lookupCategories(String keyword);

    FacilityItemType getFacilityItemType(String itemSku, Integer facilityId);

    String getTaxTypeCode(ItemType itemType, boolean isGst);

    boolean isItemTypeExpirable(ItemType itemType);

    Integer getShelfLifeForItemType(ItemType itemType);

    List<ItemType> getItemTypeByExpiryStatus(boolean expirable, int start, int batchSize);

    boolean isItemTypeExpirable(String skuCode);

    GetItemTypeByItemResponse getItemTypeByItem(GetItemTypeByItemRequest request);

    List<ItemType> getAllItemTypes(int start, int batchSize);
}
