/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 *  @version     1.0, Dec 18, 2011
 *  @author singla
 */
package com.uniware.services.catalog.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.codahale.metrics.annotation.Timed;
import com.unifier.core.api.base.ServiceRequest;
import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.template.Template;
import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.DateUtils.DateRange;
import com.unifier.core.utils.StringUtils;
import com.unifier.core.utils.ValidatorUtils;
import com.unifier.services.aspect.MarkDirty;
import com.unifier.services.aspect.RollbackOnFailure;
import com.unifier.services.utils.CustomFieldUtils;
import com.uniware.core.api.bundle.ComponentItemTypeDTO;
import com.uniware.core.api.bundle.CreateBundleRequest;
import com.uniware.core.api.bundle.CreateBundleResponse;
import com.uniware.core.api.bundle.EditBundleRequest;
import com.uniware.core.api.bundle.EditBundleResponse;
import com.uniware.core.api.bundle.WsBundle;
import com.uniware.core.api.catalog.BulkCreateOrEditItemTypeRequest;
import com.uniware.core.api.catalog.BulkCreateOrEditItemTypeResponse;
import com.uniware.core.api.catalog.CreateCategoryRequest;
import com.uniware.core.api.catalog.CreateCategoryResponse;
import com.uniware.core.api.catalog.CreateItemTypeRequest;
import com.uniware.core.api.catalog.CreateItemTypeResponse;
import com.uniware.core.api.catalog.CreateOrEditCategoryRequest;
import com.uniware.core.api.catalog.CreateOrEditCategoryResponse;
import com.uniware.core.api.catalog.CreateOrEditFacilityItemTypeRequest;
import com.uniware.core.api.catalog.CreateOrEditFacilityItemTypeResponse;
import com.uniware.core.api.catalog.CreateOrEditItemTypeRequest;
import com.uniware.core.api.catalog.CreateOrEditItemTypeResponse;
import com.uniware.core.api.catalog.EditCategoryRequest;
import com.uniware.core.api.catalog.EditCategoryResponse;
import com.uniware.core.api.catalog.EditFacilityItemTypeRequest;
import com.uniware.core.api.catalog.EditFacilityItemTypeResponse;
import com.uniware.core.api.catalog.EditItemTypeRequest;
import com.uniware.core.api.catalog.EditItemTypeResponse;
import com.uniware.core.api.catalog.GetCategoryRequest;
import com.uniware.core.api.catalog.GetCategoryResponse;
import com.uniware.core.api.catalog.GetItemTypeByItemRequest;
import com.uniware.core.api.catalog.GetItemTypeByItemResponse;
import com.uniware.core.api.catalog.GetItemTypeRequest;
import com.uniware.core.api.catalog.GetItemTypeResponse;
import com.uniware.core.api.catalog.PrintItemTypeBarcodesRequest;
import com.uniware.core.api.catalog.PrintItemTypeBarcodesResponse;
import com.uniware.core.api.catalog.WsCategory;
import com.uniware.core.api.catalog.WsFacilityItemType;
import com.uniware.core.api.catalog.WsItemType;
import com.uniware.core.api.catalog.dto.CategoryDTO;
import com.uniware.core.api.catalog.dto.CategorySearchDTO;
import com.uniware.core.api.catalog.dto.ItemTypeDTO;
import com.uniware.core.api.catalog.dto.ItemTypeFullDTO;
import com.uniware.core.api.catalog.dto.ItemTypeLookupDTO;
import com.uniware.core.api.channel.ChannelCatalogSyncMetadataVO;
import com.uniware.core.api.item.ItemDetailFieldDTO;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.api.warehouse.SearchFacilityItemTypeRequest;
import com.uniware.core.api.warehouse.SearchFacilityItemTypeResponse;
import com.uniware.core.api.warehouse.SearchFacilityItemTypeResponse.FacilityItemTypeDTO;
import com.uniware.core.api.warehouse.SearchItemTypesRequest;
import com.uniware.core.api.warehouse.SearchItemTypesResponse;
import com.uniware.core.api.warehouse.SectionDTO;
import com.uniware.core.cache.FacilityCache;
import com.uniware.core.configuration.TaxConfiguration;
import com.uniware.core.entity.Category;
import com.uniware.core.entity.Facility;
import com.uniware.core.entity.FacilityItemType;
import com.uniware.core.entity.Item;
import com.uniware.core.entity.ItemDetailField;
import com.uniware.core.entity.ItemType;
import com.uniware.core.entity.ItemTypeInventorySnapshot;
import com.uniware.core.entity.Section;
import com.uniware.core.entity.Tag;
import com.uniware.core.entity.TaxType;
import com.uniware.core.entity.VendorItemType;
import com.uniware.core.vo.PrintTemplateVO;
import com.uniware.dao.catalog.ICatalogDao;
import com.uniware.dao.channel.IChannelCatalogMao;
import com.uniware.services.admin.pickset.IPicksetService;
import com.uniware.services.bundle.IBundleService;
import com.uniware.services.cache.CategoryCache;
import com.uniware.services.cache.ItemTypeDetailCache;
import com.uniware.services.cache.PrintTemplateCache;
import com.uniware.services.cache.VendorCache;
import com.uniware.services.cache.VendorCache.VendorVO;
import com.uniware.services.catalog.ICatalogDBService;
import com.uniware.services.catalog.ICatalogService;
import com.uniware.services.channel.IChannelCatalogService;
import com.uniware.services.channel.IChannelService;
import com.uniware.services.configuration.SystemConfiguration;
import com.uniware.services.configuration.data.manager.ProductConfiguration;
import com.uniware.services.inventory.IInventoryService;
import com.uniware.services.purchase.IPurchaseService;

/**
 * @author singla
 */
@Service("catalogService")
public class CatalogServiceImpl implements ICatalogService, ICatalogDBService {

    @Autowired
    private ICatalogDao            catalogDao;

    @Autowired
    private IPicksetService        picksetService;

    @Autowired
    private IChannelService        channelService;

    @Autowired
    private IChannelCatalogService channelCatalogService;

    @Autowired
    private IPurchaseService       purchaseService;

    @Autowired
    private IBundleService         bundleService;

    @Autowired
    private IChannelCatalogMao     channelCatalogMao;

    @Autowired
    private IInventoryService      inventoryService;

    /**
     * Gets item type by sku code. Does not work for bundles.
     * 
     * @see IBundleService#getBundleByCode(String) for fetching bundled item types.
     */
    @Override
    @Timed
    @Transactional
    public ItemType getItemTypeBySkuCode(String skuCode) {
        return getItemTypeBySkuCodeDB(skuCode);
    }

    @Override
    @Timed
    @Transactional
    public ItemType getAllItemTypeBySkuCode(String skuCode) {
        return catalogDao.getAllItemTypeBySkuCode(skuCode);
    }

    @Override
    @Transactional
    public ItemType getItemTypeBySkuCodeDB(String skuCode) {
        return catalogDao.getItemTypeBySkuCode(skuCode);
    }

    @Override
    @Timed
    @Transactional
    public ItemType getAllEnabledItemTypeBySkuCode(String skuCode) {
        return catalogDao.getAllEnabledItemTypeBySkuCode(skuCode);
    }

    @Override
    @Transactional
    public ItemType getEnabledItemTypeBySkuCodeDB(String skuCode) {
        return catalogDao.getEnabledItemTypeBySkuCode(skuCode);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.uniware.services.catalog.ICatalogService#getNonBundledItemTypeById(int)
     */
    @Override
    @Timed
    public ItemType getNonBundledItemTypeById(int itemTypeId) {
        return getItemTypeByIdDB(itemTypeId);
    }

    @Override
    @Transactional
    public ItemType getEnabledItemTypeById(int itemTypeId) {
        return catalogDao.getEnabledItemTypeById(itemTypeId);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.uniware.services.catalog.ICatalogDBService#getItemTypeByIdDB(int)
     */
    @Override
    @Transactional
    public ItemType getItemTypeByIdDB(int itemTypeId) {
        return catalogDao.getItemTypeById(itemTypeId);
    }

    @Override
    @Timed
    @Transactional
    public ItemType getAllItemTypeById(int itemTypeId) {
        return catalogDao.getAllItemTypeById(itemTypeId);
    }

    @Override
    @Transactional
    public ItemType getAllEnabledItemTypeById(int itemTypeId) {
        return catalogDao.getAllEnabledItemTypeById(itemTypeId);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.uniware.services.catalog.ICatalogService#getCategoryById(int)
     */
    @Override
    @Transactional
    public Category getCategoryById(int categoryId) {
        return CacheManager.getInstance().getCache(CategoryCache.class).getCategoryById(categoryId);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.uniware.services.catalog.ICatalogService#getCategoryByCode(java.lang
     * .String)
     */
    @Override
    @Timed
    @Transactional
    public Category getCategoryByCode(String code) {
        return catalogDao.getCategoryByCode(code);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.uniware.services.catalog.ICatalogService#createItemType(com.uniware
     * .core.api.catalog.CreateItemTypeRequest)
     */
    @Override
    @Timed
    @Transactional
    @RollbackOnFailure
    public CreateItemTypeResponse createItemType(CreateItemTypeRequest itemTypeRequest) {
        CreateItemTypeResponse response = new CreateItemTypeResponse();
        ValidationContext context = itemTypeRequest.validate();
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        } else {
            WsItemType wsItemType = itemTypeRequest.getItemType();
            boolean expirable = wsItemType.getExpirable() != null && wsItemType.getExpirable();
            if (expirable && wsItemType.getShelfLife() == null) {
                context.addError(WsResponseCode.INVALID_REQUEST, "Shelf life and minimum order quantity mandatory for expirable products");
            } else if (expirable && (wsItemType.getMinOrderSize() == null || wsItemType.getMinOrderSize() == 0)) {
                context.addError(WsResponseCode.INVALID_REQUEST, "Min Order size cant be 0 for expirable products");
            }
            context = validateSku(itemTypeRequest.getItemType().getSkuCode(), context);
            if (!context.hasErrors()) {
                Integer totalPermittedSkusCount = ConfigurationManager.getInstance().getConfiguration(ProductConfiguration.class).getTotalPermittedSkusCount();
                /*
                    If Permitted SKU limit is present, only then we should check count from database.
                 */
                if (totalPermittedSkusCount != null) {
                    long itemTypeCount = catalogDao.getItemTypeCount();
                    if (totalPermittedSkusCount <= itemTypeCount) {
                        context.addError(WsResponseCode.SKU_LIMIT_EXCEDED,
                                "Sku limit reached.You already have " + itemTypeCount + " products, max limit set is :" + totalPermittedSkusCount);
                    }
                }
            }
            if (!context.hasErrors()) {
                ItemType itemType = catalogDao.getAllItemTypeBySkuCode(itemTypeRequest.getItemType().getSkuCode());
                if (StringUtils.isBlank(itemTypeRequest.getItemType().getScanIdentifier())) {
                    itemTypeRequest.getItemType().setScanIdentifier(itemTypeRequest.getItemType().getSkuCode());
                }
                if (itemType != null) {
                    //context.addError(WsResponseCode.SKU_LIMIT_EXCEDED, "Sku limit reached.");
                    context.addError(WsResponseCode.DUPLICATE_ITEM_SKU_CODE, "Sku Code already exists: " + itemTypeRequest.getItemType().getSkuCode());
                } else {
                    ItemType item = catalogDao.getItemTypeByScanIdentifier(itemTypeRequest.getItemType().getScanIdentifier());
                    if (item != null) {
                        context.addError(WsResponseCode.DUPLICATE_SCAN_IDENTIFIER, "Scannable Identifier already exists: " + itemTypeRequest.getItemType().getScanIdentifier());
                    }
                    if (!context.hasErrors()) {
                        Category category = catalogDao.getCategoryByCode(itemTypeRequest.getItemType().getCategoryCode());
                        if (category == null) {
                            context.addError(WsResponseCode.INVALID_CATEGORY_CODE, "Invalid Category Code");
                        } else {
                            itemType = new ItemType();
                            itemType.setType(itemTypeRequest.getItemType().getType());
                            itemType.setCreated(DateUtils.getCurrentTime());
                            prepareItemType(itemTypeRequest.getItemType(), itemType, category, context, false);
                            if (ItemType.Type.BUNDLE.equals(itemTypeRequest.getItemType().getType())
                                    && (itemTypeRequest.getItemType().getComponentItemTypes() == null || itemTypeRequest.getItemType().getComponentItemTypes().isEmpty())) {
                                context.addError(WsResponseCode.NO_COMPONENT_ITEM_TYPES_IN_BUNDLE);
                            }
                            if (!context.hasErrors()) {
                                List<ComponentItemTypeDTO> componentItemTypes = null;
                                itemType = catalogDao.createItemType(itemType);
                                response.setSuccessful(true);
                                if (ItemType.Type.BUNDLE.equals(itemTypeRequest.getItemType().getType())) {
                                    WsBundle wsBundle = new WsBundle(itemTypeRequest.getItemType().getSkuCode(), itemTypeRequest.getItemType().getComponentItemTypes());
                                    CreateBundleRequest createBundleRequest = new CreateBundleRequest(wsBundle);
                                    CreateBundleResponse createBundleResponse = bundleService.createBundle(createBundleRequest);
                                    response.setResponse(createBundleResponse);
                                    if (createBundleResponse.isSuccessful()) {
                                        componentItemTypes = createBundleResponse.getBundleDTO().getComponentItemTypes();
                                    } else {
                                        context.addErrors(createBundleResponse.getErrors());
                                    }
                                }
                                if (response.isSuccessful()) {
                                    channelCatalogService.linkChannelItemTypes(itemType);
                                    ItemTypeFullDTO itemTypeFullDTO = new ItemTypeFullDTO(itemType);
                                    itemTypeFullDTO.setComponentItemTypes(componentItemTypes);
                                    response.setItemType(itemTypeFullDTO);
                                }
                            }
                        }
                    }
                }

            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    /**
     * @param wsItemType
     * @param itemType
     * @param category
     */
    private void prepareItemType(WsItemType wsItemType, ItemType itemType, Category category, ValidationContext context, boolean update) {
        String customFieldsText = validateItemDetailFields(context, wsItemType.getItemDetailFieldsText());
        TaxType taxType = null;
        TaxType gstTaxType = null;
        if (!context.hasErrors()) {
            taxType = validateTaxType(context, wsItemType.getTaxTypeCode());
        }
        if (!context.hasErrors()) {
            gstTaxType = validateGstTaxType(context, wsItemType.getGstTaxTypeCode());
        }
        if (!context.hasErrors()) {
            validateTags(wsItemType, context);
        }

        if (!context.hasErrors()) {
            itemType.setCategory(ValidatorUtils.getOrDefaultValue(category, itemType.getCategory()));
            itemType.setSkuCode(ValidatorUtils.getOrDefaultValue(wsItemType.getSkuCode(), itemType.getSkuCode()));
            itemType.setUpdated(DateUtils.getCurrentTime());
            itemType.setLength(ValidatorUtils.getOrDefaultValue(wsItemType.getLength(), itemType.getLength()));
            itemType.setWidth(ValidatorUtils.getOrDefaultValue(wsItemType.getWidth(), itemType.getWidth()));
            itemType.setHeight(ValidatorUtils.getOrDefaultValue(wsItemType.getHeight(), itemType.getHeight()));
            itemType.setWeight(ValidatorUtils.getOrDefaultValue(wsItemType.getWeight(), itemType.getWeight()));
            itemType.setColor(ValidatorUtils.getOrDefaultValue(wsItemType.getColor(), itemType.getColor()));
            itemType.setBrand(ValidatorUtils.getOrDefaultValue(wsItemType.getBrand(), itemType.getBrand()));
            itemType.setSize(ValidatorUtils.getOrDefaultValue(wsItemType.getSize(), itemType.getSize()));
            itemType.setMinOrderSize(ValidatorUtils.getOrDefaultValue(wsItemType.getMinOrderSize(), itemType.getMinOrderSize()));
            itemType.setName(ValidatorUtils.getOrDefaultValue(wsItemType.getName(), itemType.getName()));
            itemType.setDescription(ValidatorUtils.getOrDefaultValue(wsItemType.getDescription(), itemType.getDescription()));
            itemType.setScanIdentifier(ValidatorUtils.getOrDefaultValue(wsItemType.getScanIdentifier(), itemType.getScanIdentifier()));
            itemType.setFeatures(ValidatorUtils.getOrDefaultValue(wsItemType.getFeatures(), itemType.getFeatures()));
            itemType.setMaxRetailPrice(ValidatorUtils.getOrDefaultValue(wsItemType.getMaxRetailPrice(), itemType.getMaxRetailPrice()));
            itemType.setBasePrice(ValidatorUtils.getOrDefaultValue(wsItemType.getBasePrice(), itemType.getBasePrice()));
            itemType.setCostPrice(ValidatorUtils.getOrDefaultValue(wsItemType.getCostPrice(), itemType.getCostPrice()));
            itemType.setTat(ValidatorUtils.getOrDefaultValue(wsItemType.getTat(), itemType.getTat()));
            itemType.setImageUrl(ValidatorUtils.getOrDefaultValue(wsItemType.getImageUrl(), itemType.getImageUrl()));
            itemType.setProductPageUrl(ValidatorUtils.getOrDefaultValue(wsItemType.getProductPageUrl(), itemType.getProductPageUrl()));
            itemType.setEan(ValidatorUtils.getOrDefaultValue(wsItemType.getEan(), itemType.getEan()));
            itemType.setUpc(ValidatorUtils.getOrDefaultValue(wsItemType.getUpc(), itemType.getUpc()));
            itemType.setIsbn(ValidatorUtils.getOrDefaultValue(wsItemType.getIsbn(), itemType.getIsbn()));
            itemType.setTags(StringUtils.join(',', ValidatorUtils.getOrDefaultValue(wsItemType.getTags(), itemType.getTagList())));
            itemType.setExpirable(wsItemType.getExpirable());
            itemType.setShelfLife(wsItemType.getShelfLife());
            itemType.setItemDetailFieldsText(ValidatorUtils.getOrDefaultValue(customFieldsText, itemType.getItemDetailFieldsText()));
            itemType.setTaxType(ValidatorUtils.getOrDefaultValue(taxType, itemType.getTaxType()));
            itemType.setGstTaxType(ValidatorUtils.getOrDefaultValue(gstTaxType, itemType.getGstTaxType()));
            itemType.setHsnCode(ValidatorUtils.getOrDefaultValue(wsItemType.getHsnCode(), itemType.getHsnCode()));
            itemType.setRequiresCustomization(ValidatorUtils.getOrDefaultValue(wsItemType.getRequiresCustomization(), itemType.isRequiresCustomization()));
            itemType.setEnabled(ValidatorUtils.getOrDefaultValue(wsItemType.getEnabled(), itemType.isEnabled()));

            if (wsItemType.getCustomFieldValues() != null) {
                Map<String, Object> customFieldValues = CustomFieldUtils.getCustomFieldValues(context, ItemType.class.getName(), wsItemType.getCustomFieldValues(), update);
                CustomFieldUtils.setCustomFieldValues(itemType, customFieldValues);
            }
        }
    }

    /**
     * @param wsItemType
     * @param context
     */
    private void validateTags(WsItemType wsItemType, ValidationContext context) {
        if (wsItemType.getTags() != null && wsItemType.getTags().size() > 0) {
            List<Tag> tags = catalogDao.getTagByNames(wsItemType.getTags());
            if (tags.size() != wsItemType.getTags().size()) {
                Set<String> tagNamesFromDb = tags.stream().map(tag -> tag.getName()).collect(Collectors.toSet());
                for (String name : wsItemType.getTags()) {
                    if (!tagNamesFromDb.contains(name)) {
                        context.addError(WsResponseCode.INVALID_TAG, "Tag '" + name + "' doesn't exist");
                    }
                }
            }
        }
    }

    /**
     * @param context
     * @param taxTypeCode
     * @return
     */
    private TaxType validateTaxType(ValidationContext context, String taxTypeCode) {
        if (StringUtils.isNotBlank(taxTypeCode)) {
            TaxConfiguration taxConfiguration = ConfigurationManager.getInstance().getConfiguration(TaxConfiguration.class);
            TaxType taxType = taxConfiguration.getTaxTypeByCode(taxTypeCode);
            if (taxType == null) {
                context.addError(WsResponseCode.INVALID_TAX_TYPE_CODE, "Invalid tax type code");
            } else if (taxType.isGst()) {
                context.addError(WsResponseCode.INVALID_TAX_TYPE_CODE, "Gst Tax Type cannot be set on Tax Type");
            } else {
                return taxType;
            }
        }
        return null;
    }

    private TaxType validateGstTaxType(ValidationContext context, String taxTypeCode) {
        if (StringUtils.isNotBlank(taxTypeCode)) {
            TaxConfiguration taxConfiguration = ConfigurationManager.getInstance().getConfiguration(TaxConfiguration.class);
            TaxType taxType = taxConfiguration.getTaxTypeByCode(taxTypeCode);
            if (taxType == null) {
                context.addError(WsResponseCode.INVALID_TAX_TYPE_CODE, "Invalid tax type code");
            } else if (!taxType.isGst()) {
                context.addError(WsResponseCode.INVALID_TAX_TYPE_CODE, "Tax Type cannot be set on Gst Tax Type");
            } else {
                return taxType;
            }
        }
        return null;
    }

    /**
     * @param context
     * @param itemDetailFieldsText
     * @return
     */
    private String validateItemDetailFields(ValidationContext context, String itemDetailFieldsText) {
        if (StringUtils.isNotBlank(itemDetailFieldsText)) {
            List<String> customFieldNames = StringUtils.split(itemDetailFieldsText, ",");
            Set<String> itemDetailFields = new HashSet<String>();
            for (String itemDetailFieldName : customFieldNames) {
                ItemDetailFieldDTO itemDetailField = CacheManager.getInstance().getCache(ItemTypeDetailCache.class).getItemDetailFieldByName(itemDetailFieldName);
                if (itemDetailField == null) {
                    context.addError(WsResponseCode.INVALID_CUSTOM_FIELD, "Invalid item details field:" + itemDetailFieldName);
                } else {
                    itemDetailFields.add(itemDetailField.getName());
                }
            }
            itemDetailFieldsText = StringUtils.join(itemDetailFields);
        }
        return itemDetailFieldsText;
    }

    @Override
    @Timed
    public BulkCreateOrEditItemTypeResponse bulkCreateOrEditItemTypeRequest(BulkCreateOrEditItemTypeRequest request) {
        BulkCreateOrEditItemTypeResponse response = new BulkCreateOrEditItemTypeResponse();
        response.setSuccessful(true);
        for (WsItemType wsItemType : request.getItemTypes()) {
            CreateOrEditItemTypeRequest createOrEditItemTypeRequest = new CreateOrEditItemTypeRequest();
            createOrEditItemTypeRequest.setItemType(wsItemType);
            CreateOrEditItemTypeResponse createOrEditItemTypeResponse = createOrEditItemType(createOrEditItemTypeRequest);
            if (!createOrEditItemTypeResponse.isSuccessful()) {
                response.addErrors(createOrEditItemTypeResponse.getErrors());
                response.addSuccessfulSkuCode(createOrEditItemTypeRequest.getItemType().getSkuCode());
                response.setSuccessful(false);
            } else {
                response.addFailedSkuCode(createOrEditItemTypeRequest.getItemType().getSkuCode());
            }
        }
        return response;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.uniware.services.catalog.ICatalogService#createCategory(com.uniware
     * .core.api.catalog.CreateCategoryRequest)
     */
    @Override
    @Timed
    @Transactional
    @MarkDirty(values = { CategoryCache.class })
    public CreateCategoryResponse createCategory(CreateCategoryRequest categoryRequest) {
        CreateCategoryResponse response = new CreateCategoryResponse();
        ValidationContext context = categoryRequest.validate();
        if (!context.hasErrors()) {
            Category category = catalogDao.getCategoryByCode(categoryRequest.getCategory().getCode());
            if (category != null) {
                context.addError(WsResponseCode.DUPLICATE_CATEGORY_CODE, "Category Code already exist");
            } else {
                category = new Category();
                prepareCategory(categoryRequest.getCategory(), category, context);
                category.setCreated(DateUtils.getCurrentTime());
                if (!context.hasErrors()) {
                    category = catalogDao.createCategory(category);
                    response.setCategory(new CategoryDTO(category));
                    response.setSuccessful(true);
                }
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    /**
     * @param wsCategory
     * @param category
     */
    private void prepareCategory(WsCategory wsCategory, Category category, ValidationContext context) {
        TaxType taxType = validateTaxType(context, wsCategory.getTaxTypeCode());
        TaxType gstTaxType = validateGstTaxType(context, wsCategory.getGstTaxTypeCode());
        String itemDetailFieldsText = validateItemDetailFields(context, wsCategory.getItemDetailFieldsText());
        validateExpirableField(context, wsCategory);
        if (!context.hasErrors()) {
            category.setCode(wsCategory.getCode());
            category.setName(wsCategory.getName());
            category.setTaxType(ValidatorUtils.getOrDefaultValue(taxType, category.getTaxType()));
            category.setGstTaxType(gstTaxType);
            category.setItemDetailFieldsText(itemDetailFieldsText);
            category.setHsnCode(ValidatorUtils.getOrDefaultValue(wsCategory.getHsnCode(), category.getHsnCode()));
            category.setGrnExpiryTolerance(wsCategory.getGrnExpiryTolerance());
            category.setDispatchExpiryTolerance(wsCategory.getDispatchExpiryTolerance());
            category.setReturnExpiryTolerance(wsCategory.getReturnExpiryTolerance());
            category.setExpirable(wsCategory.getExpirable());
            category.setShelfLife(wsCategory.getShelfLife());
        }
    }

    private void validateExpirableField(ValidationContext context, WsCategory wsCategory) {
        if (wsCategory.getExpirable()
                && ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).getTraceabilityLevel() == SystemConfiguration.TraceabilityLevel.ITEM
                && inventoryService.getNumberOfItemsWithoutManufacturingDateForCategory(wsCategory.getCode()) > 0) {
            context.addError(WsResponseCode.INVALID_ITEM_TYPE,
                    "Before marking expirable, Please complete putaways of all existing GRNs of this SKU and then ensure all the items have manufacturing date for this category");
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.uniware.services.catalog.ICatalogService#createCategory(com.uniware
     * .core.api.catalog.CreateCategoryRequest)
     */
    @Override
    @Timed
    @Transactional
    public EditCategoryResponse editCategory(EditCategoryRequest categoryRequest) {
        EditCategoryResponse response = new EditCategoryResponse();
        ValidationContext context = categoryRequest.validate();
        if (!context.hasErrors()) {
            if (!context.hasErrors()) {
                Category category = catalogDao.getCategoryByCode(categoryRequest.getCategory().getCode());
                if (category == null) {
                    context.addError(WsResponseCode.INVALID_CATEGORY_CODE, "Invalid Category Code");
                } else {
                    prepareCategory(categoryRequest.getCategory(), category, context);
                    if (!context.hasErrors()) {
                        category = catalogDao.updateCategory(category);
                        response.setCategory(new CategoryDTO(category));
                        response.setSuccessful(true);
                    }
                }
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.uniware.services.catalog.ICatalogService#lookVendors(java.lang.String
     * )
     */
    @Override
    @Timed
    public List<VendorVO> lookupVendors(String name) {
        return CacheManager.getInstance().getCache(VendorCache.class).lookupVendors(name);
    }

    @Override
    @Timed
    @Transactional
    public Tag addTag(String name) {
        return catalogDao.addTag(new Tag(name));

    }

    @Override
    @Timed
    @Transactional
    public List<Tag> getAllTags() {
        return catalogDao.getAllTags();
    }

    @Override
    @Timed
    @Transactional
    public int removeTag(String name) {
        return catalogDao.removeTag(name);
    }

    @Override
    @Timed
    @Transactional(readOnly = true)
    public SearchItemTypesResponse searchItemTypes(SearchItemTypesRequest request) {
        SearchItemTypesResponse response = new SearchItemTypesResponse();
        ValidationContext context = request.validate();
        if (context.hasErrors()) {
            response.setSuccessful(false);
            response.setErrors(context.getErrors());
        } else {
            List<ItemType> itemTypes = catalogDao.searchItemTypes(request);
            if (request.getSearchOptions() != null && request.getSearchOptions().isGetCount()) {
                Long count = catalogDao.getItemTypeCount(request);
                response.setTotalRecords(count);
            }
            for (ItemType itemType : itemTypes) {
                ItemTypeDTO itemTypeDTO = new ItemTypeDTO(itemType);
                if (request.getGetInventorySnapshot() != null && request.getGetInventorySnapshot()) {
                    itemTypeDTO.setInventorySnapshots(new ArrayList<ItemTypeDTO.InventorySnapshotDTO>());
                    for (ItemTypeInventorySnapshot inventorySnapshot : itemType.getItemTypeInventorySnapshots()) {
                        Facility facility = CacheManager.getInstance().getCache(FacilityCache.class).getFacilityById(inventorySnapshot.getFacility().getId());
                        itemTypeDTO.getInventorySnapshots().add(new ItemTypeDTO.InventorySnapshotDTO(facility.getCode(), inventorySnapshot.getPendingInventoryAssessment(),
                                inventorySnapshot.getOpenSale(), inventorySnapshot.getInventory(), inventorySnapshot.getOpenPurchase(), inventorySnapshot.getPendingStockTransfer(),
                                inventorySnapshot.getPendingStockTransfer(), inventorySnapshot.getVendorInventory()));
                    }
                }
                itemTypeDTO.setCustomFieldValues(CustomFieldUtils.getCustomFieldValuesDTO(itemType));
                response.getElements().add(itemTypeDTO);
            }
            response.setSuccessful(true);
        }
        return response;
    }

    @Override
    @Timed
    @Transactional
    @RollbackOnFailure
    public EditItemTypeResponse editItemType(EditItemTypeRequest itemTypeRequest) {
        EditItemTypeResponse response = new EditItemTypeResponse();
        ValidationContext context = itemTypeRequest.validate(false);
        if (!context.hasErrors()) {
            ItemType itemType = catalogDao.getAllItemTypeBySkuCode(itemTypeRequest.getItemType().getSkuCode());
            if (itemType == null) {
                context.addError(WsResponseCode.INVALID_ITEM_TYPE, "Invalid Item Type Code");
            } else {
                if (StringUtils.isBlank(itemTypeRequest.getItemType().getScanIdentifier())) {
                    itemTypeRequest.getItemType().setScanIdentifier(itemTypeRequest.getItemType().getSkuCode());
                } else {
                    ItemType item = catalogDao.getItemTypeByScanIdentifier(itemTypeRequest.getItemType().getScanIdentifier());
                    if (item != null && !item.equals(itemType)) {
                        context.addError(WsResponseCode.DUPLICATE_SCAN_IDENTIFIER, "Scannable Identifier already exists: " + itemTypeRequest.getItemType().getScanIdentifier());
                    }
                }
                if (!context.hasErrors()) {
                    Category category = null;
                    String categoryCode = itemTypeRequest.getItemType().getCategoryCode();
                    if (StringUtils.isNotBlank(categoryCode)) {
                        category = catalogDao.getCategoryByCode(categoryCode);
                        if (category == null) {
                            context.addError(WsResponseCode.INVALID_CATEGORY_CODE, "Invalid Category Code");
                        }
                    }
                    if (!context.hasErrors()) {
                        prepareItemType(itemTypeRequest.getItemType(), itemType, category, context, true);
                        if (!context.hasErrors()) {
                            itemType = catalogDao.updateItemType(itemType);
                            List<ComponentItemTypeDTO> componentItemTypes = new ArrayList<ComponentItemTypeDTO>();
                            if (ItemType.Type.BUNDLE.equals(itemTypeRequest.getItemType().getType())) {
                                WsBundle wsBundle = new WsBundle(itemTypeRequest.getItemType().getSkuCode(), itemTypeRequest.getItemType().getComponentItemTypes());
                                EditBundleRequest editBundleRequest = new EditBundleRequest(wsBundle);
                                EditBundleResponse editBundleResponse = bundleService.editBundle(editBundleRequest);
                                response.setResponse(editBundleResponse);
                                if (editBundleResponse.isSuccessful()) {
                                    componentItemTypes = editBundleResponse.getBundleDTO().getComponentItemTypes();
                                } else {
                                    context.addErrors(editBundleResponse.getErrors());
                                }
                            }
                            if (!context.hasErrors()) {
                                response.setSuccessful(true);
                                ItemTypeFullDTO itemTypeFullDTO = new ItemTypeFullDTO(itemType);
                                itemTypeFullDTO.setComponentItemTypes(componentItemTypes);
                                itemTypeFullDTO.setCustomFieldValues(CustomFieldUtils.getCustomFieldValuesDTO(itemType));
                                response.setItemType(itemTypeFullDTO);
                            }
                        }
                    }
                }
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Timed
    @Transactional
    public GetCategoryResponse listCategories(GetCategoryRequest request) {
        GetCategoryResponse response = new GetCategoryResponse();
        ValidationContext context = request.validate();
        if (context.hasErrors()) {
            response.setSuccessful(false);
            response.setErrors(context.getErrors());
        } else {
            List<Category> categories = catalogDao.listCategories();
            response.setSuccessful(true);
            for (Category category : categories) {
                response.getCategories().add(prepareCategortDTO(category));
            }
        }
        return response;
    }

    private CategoryDTO prepareCategortDTO(Category category) {
        CategoryDTO categoryDTO = new CategoryDTO();
        categoryDTO.setId(category.getId());
        categoryDTO.setCode(category.getCode());
        categoryDTO.setName(category.getName());
        categoryDTO.setTaxTypeCode(ConfigurationManager.getInstance().getConfiguration(TaxConfiguration.class).getTaxTypeById(category.getTaxType().getId()).getCode());
        categoryDTO.setGstTaxTypeCode(ConfigurationManager.getInstance().getConfiguration(TaxConfiguration.class).getTaxTypeById(category.getGstTaxType().getId()).getCode());
        return categoryDTO;
    }

    @Override
    @Timed
    @Transactional
    public GetCategoryResponse listCategories() {
        GetCategoryRequest request = new GetCategoryRequest();
        return listCategories(request);
    }

    @Override
    @Timed
    @Transactional
    public List<SectionDTO> listSections() {
        List<SectionDTO> sections = new ArrayList<SectionDTO>();
        for (Section section : catalogDao.listSections()) {
            sections.add(new SectionDTO(section));
        }
        return sections;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.uniware.services.catalog.ICatalogService#getVendorItemType(int,
     * int)
     */
    @Override
    @Timed
    @Transactional
    public VendorItemType getVendorItemType(int vendorId, int itemTypeId) {
        return catalogDao.getVendorItemType(vendorId, itemTypeId);
    }

    /* (non-Javadoc)
     * @see com.uniware.services.catalog.ICatalogService#getVendorItemTypeByItemTypeId(java.lang.Integer)
     */
    @Override
    @Timed
    public List<VendorItemType> getVendorItemTypeByItemTypeId(Integer itemTypeId) {
        return catalogDao.getVendorItemTypeByItemTypeId(itemTypeId);
    }

    @Override
    @Timed
    public List<VendorItemType> getVendorItemTypeByItemTypeIdByTenant(Integer itemTypeId) {
        return catalogDao.getVendorItemTypeByItemTypeIdByTenant(itemTypeId);
    }

    @Override
    @Timed
    public CreateOrEditItemTypeResponse createOrEditItemType(CreateOrEditItemTypeRequest request) {
        CreateOrEditItemTypeResponse response = new CreateOrEditItemTypeResponse();
        ValidationContext context = request.validate();
        if (context.hasErrors()) {
            response.setSuccessful(false);
            response.setErrors(context.getErrors());
        } else {
            ItemType itemType = getItemTypeBySkuCode(request.getItemType().getSkuCode());
            if (itemType == null) {
                CreateItemTypeRequest createItemTypeRequest = new CreateItemTypeRequest();
                createItemTypeRequest.setItemType(request.getItemType());
                CreateItemTypeResponse createResponse = createItemType(createItemTypeRequest);
                response.setErrors(createResponse.getErrors());
                response.setSuccessful(createResponse.isSuccessful());
            } else {
                EditItemTypeRequest editItemTypeRequest = new EditItemTypeRequest();
                editItemTypeRequest.setItemType(request.getItemType());
                EditItemTypeResponse editResponse = editItemType(editItemTypeRequest);
                response.setErrors(editResponse.getErrors());
                response.setSuccessful(editResponse.isSuccessful());
            }
        }
        return response;
    }

    @Override
    @Timed
    @Transactional
    public List<ItemTypeLookupDTO> lookupItemTypes(String keyword) {
        Set<ItemTypeLookupDTO> itemTypesSet = new LinkedHashSet<>();
        for (ItemType itemType : catalogDao.lookupExactItemTypes(keyword, 10)) {
            itemTypesSet.add(prepareItemTypeLookupDTO(itemType));
        }
        int count = itemTypesSet.size();
        if (count < 10) {
            for (ItemType itemType : catalogDao.lookupItemTypes(keyword, 10 - count)) {
                itemTypesSet.add(prepareItemTypeLookupDTO(itemType));
            }
        }
        return new ArrayList<>(itemTypesSet);
    }

    @Override
    @Timed
    @Transactional
    public List<ItemTypeLookupDTO> lookupAllItemTypes(String keyword) {
        Set<ItemTypeLookupDTO> itemTypesSet = new LinkedHashSet<>();
        for (ItemType itemType : catalogDao.lookupExactItemTypes(keyword, 10)) {
            itemTypesSet.add(prepareItemTypeLookupDTO(itemType));
        }
        int count = itemTypesSet.size();
        if (count < 10) {
            for (ItemType itemType : catalogDao.lookupAllItemTypes(keyword, 10 - count)) {
                itemTypesSet.add(prepareItemTypeLookupDTO(itemType));
            }
        }
        return new ArrayList<>(itemTypesSet);
    }

    @Override
    @Timed
    public ItemTypeLookupDTO prepareItemTypeLookupDTO(ItemType itemType) {
        ItemTypeLookupDTO itemTypeLookupDTO = new ItemTypeLookupDTO();
        itemTypeLookupDTO.setItemTypeId(itemType.getId());
        itemTypeLookupDTO.setName(itemType.getName());
        itemTypeLookupDTO.setItemSKU(itemType.getSkuCode());
        itemTypeLookupDTO.setScanIdentifier(itemType.getScanIdentifier());
        itemTypeLookupDTO.setMaxRetailPrice(itemType.getMaxRetailPrice());
        itemTypeLookupDTO.setBasePrice(itemType.getBasePrice());
        itemTypeLookupDTO.setCostPrice(itemType.getCostPrice());
        itemTypeLookupDTO.setImageUrl(itemType.getImageUrl());
        List<ItemDetailFieldDTO> itemDetailFieldDTOList = new ArrayList<>();
        String itemDetailFieldText = itemType.getItemDetailFieldsText();
        if (StringUtils.isNotBlank(itemDetailFieldText)) {
            for (String itemDetailField : StringUtils.split(itemDetailFieldText)) {
                ItemDetailFieldDTO detailFieldDTO = CacheManager.getInstance().getCache(ItemTypeDetailCache.class).getItemDetailFieldByName(itemDetailField);
                itemDetailFieldDTOList.add(detailFieldDTO);
            }
        }
        itemTypeLookupDTO.setItemDetailFieldDTOList(itemDetailFieldDTOList);
        return itemTypeLookupDTO;
    }

    @Override
    public Tag getTagByName(String name) {
        return catalogDao.getTagByName(name.trim());
    }

    @Override
    @Timed
    @Transactional(readOnly = true)
    public List<ItemType> getItemTypes(int start, int noOfRecords, DateRange updatedIn) {
        return catalogDao.getItemTypes(start, noOfRecords, updatedIn);
    }

    @Override
    @Timed
    @Transactional
    public VendorItemType getVendorItemType(String vendorCode, Integer itemTypeId) {
        return catalogDao.getVendorItemType(vendorCode, itemTypeId);
    }

    @Override
    @Timed
    public CreateOrEditFacilityItemTypeResponse createOrEditFacilityItemType(CreateOrEditFacilityItemTypeRequest request) {
        CreateOrEditFacilityItemTypeResponse response = new CreateOrEditFacilityItemTypeResponse();
        ValidationContext context = request.validate(false);
        if (!context.hasErrors()) {
            for (WsFacilityItemType wsFacilityItemType : request.getFacilityItemTypes()) {
                createOrEditFacilityItemType(wsFacilityItemType, context);
            }
            if (!context.hasErrors()) {
                response.setSuccessful(true);
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Transactional
    public void createOrEditFacilityItemType(WsFacilityItemType wsFacilityItemType, ValidationContext context) {
        Facility facility = CacheManager.getInstance().getCache(FacilityCache.class).getFacilityByCode(wsFacilityItemType.getFacilityCode());
        if (facility == null || !Facility.Type.DROPSHIP.name().equals(facility.getType())) {
            context.addError(WsResponseCode.INVALID_FACILITY_CODE, "Invalid facility code: " + wsFacilityItemType.getFacilityCode());
        } else {
            ItemType itemType = catalogDao.getItemTypeBySkuCode(wsFacilityItemType.getItemTypeSkuCode());
            if (itemType == null) {
                context.addError(WsResponseCode.INVALID_ITEM_TYPE, "Invalid item type skucode");
            } else {
                boolean isEditing = true;
                FacilityItemType facilityItemType = getFacilityItemType(itemType.getSkuCode(), facility.getId());
                if (facilityItemType == null) {
                    isEditing = false;
                    ServiceRequest.validate(wsFacilityItemType, context, true);
                    if (!context.hasErrors()) {
                        facilityItemType = new FacilityItemType();
                        facilityItemType.setFacility(facility);
                        facilityItemType.setItemType(itemType);
                        facilityItemType.setCreated(DateUtils.getCurrentTime());
                    }
                }
                if (!context.hasErrors()) {
                    prepareFacilityItemType(context, wsFacilityItemType, itemType, facilityItemType);

                }
                if (!context.hasErrors()) {
                    if (isEditing) {
                        facilityItemType = catalogDao.updateFacilityItemType(facilityItemType);
                    } else {
                        catalogDao.addFacilityItemType(facilityItemType);
                    }
                    itemType.getFacilityItemTypes().add(facilityItemType);
                }
            }
        }
    }

    @Override
    @Timed
    @Transactional
    public CreateOrEditCategoryResponse createOrEditCategory(CreateOrEditCategoryRequest request) {
        CreateOrEditCategoryResponse response = new CreateOrEditCategoryResponse();
        ValidationContext context = request.validate();
        if (context.hasErrors()) {
            response.setSuccessful(false);
            response.setErrors(context.getErrors());
        } else {
            Category category = catalogDao.getCategoryByCode(request.getCategory().getCode());
            if (category == null) {
                CreateCategoryRequest createCategoryRequest = new CreateCategoryRequest();
                createCategoryRequest.setCategory(request.getCategory());
                CreateCategoryResponse createResponse = createCategory(createCategoryRequest);
                response.setErrors(createResponse.getErrors());
                response.setSuccessful(createResponse.isSuccessful());
            } else {
                EditCategoryRequest editCategoryRequest = new EditCategoryRequest();
                editCategoryRequest.setCategory(request.getCategory());
                EditCategoryResponse editResponse = editCategory(editCategoryRequest);
                response.setErrors(editResponse.getErrors());
                response.setSuccessful(editResponse.isSuccessful());
            }
        }
        return response;
    }

    @Override
    @Timed
    @Transactional
    public SearchFacilityItemTypeResponse searchFacilityItemType(SearchFacilityItemTypeRequest request) {
        SearchFacilityItemTypeResponse response = new SearchFacilityItemTypeResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            for (FacilityItemType facilityItemType : catalogDao.searchFacilityItemType(request)) {
                FacilityItemTypeDTO facilityItemTypeDto = new FacilityItemTypeDTO();
                facilityItemTypeDto.setFacilityCode(facilityItemType.getFacility().getCode());
                facilityItemTypeDto.setFacilityDisplayName(facilityItemType.getFacility().getDisplayName());
                facilityItemTypeDto.setItemSkuCode(facilityItemType.getItemType().getSkuCode());
                facilityItemTypeDto.setInventory(facilityItemType.getInventory());
                facilityItemTypeDto.setPriority(facilityItemType.getPriority());
                facilityItemTypeDto.setEnabled(facilityItemType.isEnabled());
                facilityItemTypeDto.setCreated(facilityItemType.getCreated());
                facilityItemTypeDto.setUpdated(facilityItemType.getUpdated());
                response.getElements().add(facilityItemTypeDto);
            }

            if (request.getSearchOptions() != null && request.getSearchOptions().isGetCount()) {
                Long count = catalogDao.getFacilityItemTypeCount(request);
                response.setTotalRecords(count);
            }
            response.setSuccessful(true);
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    public EditFacilityItemTypeResponse editFacilityItemType(EditFacilityItemTypeRequest request) {
        EditFacilityItemTypeResponse response = new EditFacilityItemTypeResponse();
        ValidationContext context = request.validate(false);
        if (!context.hasErrors()) {
            WsFacilityItemType wsFacilityItemType = request.getWsFacilityItemType();
            Facility facility = CacheManager.getInstance().getCache(FacilityCache.class).getFacilityByCode(wsFacilityItemType.getFacilityCode());
            if (facility == null || !Facility.Type.DROPSHIP.name().equals(facility.getType())) {
                context.addError(WsResponseCode.INVALID_FACILITY_CODE, "Invalid facility code: " + wsFacilityItemType.getFacilityCode());
            } else {
                ItemType itemType = catalogDao.getItemTypeBySkuCode(wsFacilityItemType.getItemTypeSkuCode());
                if (itemType == null) {
                    context.addError(WsResponseCode.INVALID_ITEM_TYPE, "Invalid item type skucode");
                } else {
                    FacilityItemType facilityItemType = getFacilityItemType(itemType.getSkuCode(), facility.getId());
                    if (facilityItemType == null) {
                        context.addError(WsResponseCode.INVALID_FACILITY_ITEM_TYPE, "Invalid facility item type");
                    } else {
                        prepareFacilityItemType(context, wsFacilityItemType, itemType, facilityItemType);
                        if (!context.hasErrors()) {
                            facilityItemType = catalogDao.updateFacilityItemType(facilityItemType);
                            itemType.getFacilityItemTypes().add(facilityItemType);
                            response.setSuccessful(true);
                        }
                    }
                }
            }

        }
        response.setErrors(context.getErrors());
        return response;
    }

    private void prepareFacilityItemType(ValidationContext context, WsFacilityItemType wsFacilityItemType, ItemType itemType, FacilityItemType facilityItemType) {
        Map<String, Object> customFieldValues = CustomFieldUtils.getCustomFieldValues(context, FacilityItemType.class.getName(), wsFacilityItemType.getCustomFieldValues());
        CustomFieldUtils.setCustomFieldValues(facilityItemType, customFieldValues);
        facilityItemType.setEnabled(ValidatorUtils.getOrDefaultValue(wsFacilityItemType.getEnabled(), facilityItemType.isEnabled()));
        if (facilityItemType.isEnabled()) {
            facilityItemType.setInventory(ValidatorUtils.getOrDefaultValue(wsFacilityItemType.getInventory(), facilityItemType.getInventory()));
        } else {
            facilityItemType.setInventory(0);
        }
        if (wsFacilityItemType.getPriority() != null) {
            facilityItemType.setPriority(wsFacilityItemType.getPriority());
        }
        facilityItemType.setCommission(ValidatorUtils.getOrDefaultValue(wsFacilityItemType.getCommission(), facilityItemType.getCommission()));
        facilityItemType.setTransferPrice(ValidatorUtils.getOrDefaultValue(wsFacilityItemType.getTransferPrice(), facilityItemType.getTransferPrice()));
        facilityItemType.setShippingCharges(ValidatorUtils.getOrDefaultValue(wsFacilityItemType.getShippingCharges(), facilityItemType.getShippingCharges()));
    }

    @Override
    @Transactional
    public List<Category> getCategories() {
        return catalogDao.getCategories();
    }

    @Override
    @Timed
    @Transactional
    public List<ItemDetailField> getItemDetailFields() {
        return catalogDao.getItemDetailFields();
    }

    @Override
    @Timed
    public GetItemTypeResponse getItemType(GetItemTypeRequest request) {
        GetItemTypeResponse response = new GetItemTypeResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            ItemType itemType = getAllItemTypeBySkuCode(request.getSkuCode());
            if (itemType == null) {
                context.addError(WsResponseCode.INVALID_ITEM_TYPE, "Invalid item type:" + request.getSkuCode());
            } else {
                ItemTypeFullDTO itemTypeDTO = new ItemTypeFullDTO(itemType);
                itemTypeDTO.setCustomFieldValues(CustomFieldUtils.getCustomFieldValuesDTO(itemType));
                if (ItemType.Type.BUNDLE.equals(itemType.getType())) {
                    itemTypeDTO.setComponentItemTypes(bundleService.getBundleComponents(itemType.getSkuCode()));
                }
                response.setItemTypeDTO(itemTypeDTO);
                response.setSuccessful(true);
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Timed
    @Transactional(readOnly = true)
    public PrintItemTypeBarcodesResponse printItemTypeBarcodes(PrintItemTypeBarcodesRequest request) {
        PrintItemTypeBarcodesResponse response = new PrintItemTypeBarcodesResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            Template template = CacheManager.getInstance().getCache(PrintTemplateCache.class).getTemplateByType(PrintTemplateVO.Type.ITEM_TYPE_LABEL_CSV.name());
            Map<String, Object> templateParams = new HashMap<>();
            List<ItemTypeFullDTO> itemTypeFullDTOList = new ArrayList<>();
            for (Entry<String, Integer> e : request.getSkuToQtyMap().entrySet()) {
                GetItemTypeRequest getItemTypeRequest = new GetItemTypeRequest();
                getItemTypeRequest.setSkuCode(e.getKey());
                GetItemTypeResponse getItemTypeResponse = getItemType(getItemTypeRequest);
                if (getItemTypeResponse.hasErrors()) {
                    context.addErrors(getItemTypeResponse.getErrors());
                } else {
                    int quantity = e.getValue();
                    if (getItemTypeResponse.getItemTypeDTO() != null && quantity > 0) {
                        for (int counter = 0; counter < quantity; counter++) {
                            itemTypeFullDTOList.add(getItemTypeResponse.getItemTypeDTO());
                        }
                    }
                }
            }
            templateParams.put("itemTypes", itemTypeFullDTOList);
            response.setOutputCsv(template.evaluate(templateParams));
        }
        response.setErrors(context.getErrors());
        response.setSuccessful(!context.hasErrors());
        return response;
    }

    @Override
    @Timed
    public ChannelCatalogSyncMetadataVO updateChannelCatalogSyncMetadata(ChannelCatalogSyncMetadataVO catalogSyncMetadata) {
        return channelCatalogMao.save(catalogSyncMetadata);
    }

    @Override
    @Timed
    public ChannelCatalogSyncMetadataVO getChannelCatalogSyncPreprocessor(String sourceCode) {
        return channelCatalogMao.getChannelCatalogSyncMetadata(sourceCode);
    }

    @Override
    @Timed
    @Transactional
    public List<CategorySearchDTO> lookupCategories(String keyword) {
        List<CategorySearchDTO> categories = new ArrayList<CategorySearchDTO>();
        for (Category category : catalogDao.lookupCategories(keyword)) {
            CategorySearchDTO categorySearchDTO = new CategorySearchDTO(category);
            categories.add(categorySearchDTO);
        }
        return categories;
    }

    @Override
    @Timed
    @Transactional
    public FacilityItemType getFacilityItemType(String itemSku, Integer facilityId) {
        return catalogDao.getFacilityItemType(itemSku, facilityId);
    }

    private ValidationContext validateSku(String skuCode, ValidationContext context) {
        String skuPattern = ".-";
        if (skuPattern.indexOf(skuCode.charAt(0)) != -1) {
            context.addError(WsResponseCode.INVALID_ITEM_SKU_CODE, "Sku Code shouldn't start with . and -");
        }
        return context;
    }

    @Override
    public String getTaxTypeCode(ItemType itemType, boolean isGst) {
        if (!isGst) {
            TaxType itemTaxType = itemType.getTaxType() != null ? itemType.getTaxType() : itemType.getCategory().getTaxType();
            return itemTaxType != null ? itemTaxType.getCode() : null;
        } else {
            TaxType gstTaxType = itemType.getGstTaxType() != null ? itemType.getGstTaxType() : itemType.getCategory().getGstTaxType();
            return gstTaxType != null ? gstTaxType.getCode() : null;
        }

    }

    /**
     * Checks whether {@link ItemType}is {@code expirable} or not, by first querying {@code ItemType}, and if that is
     * null (which stands for finding the value of {@code expirable} from category), then by querying {@link Category}.
     * In case where {@link com.uniware.services.configuration.SystemConfiguration.TraceabilityLevel} is not
     * {@code ITEM}, {@code false} is returned, as the concept of expirable does not exist on traceability levels other
     * than {@code ITEM}
     */
    @Override
    public boolean isItemTypeExpirable(ItemType itemType) {
        SystemConfiguration.TraceabilityLevel traceabilityLevel = ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).getTraceabilityLevel();
        if (traceabilityLevel != SystemConfiguration.TraceabilityLevel.ITEM) {
            return false;
        } else {
            return itemType.getExpirable() != null ? itemType.getExpirable() : itemType.getCategory().isExpirable();
        }
    }

    @Override
    public Integer getShelfLifeForItemType(ItemType itemType) {
        return itemType.getShelfLife() != null ? itemType.getShelfLife() : itemType.getCategory().getShelfLife();

    }

    @Override
    @Transactional
    public List<ItemType> getItemTypeByExpiryStatus(boolean expirable, int start, int batchSize) {
        return catalogDao.getItemTypesByExpiryStatus(expirable, start, batchSize);
    }

    @Override
    @Transactional(readOnly = true)
    public boolean isItemTypeExpirable(String skuCode) {
        return isItemTypeExpirable(getItemTypeBySkuCode(skuCode));
    }

    @Override
    @Transactional(readOnly = true)
    public GetItemTypeByItemResponse getItemTypeByItem(GetItemTypeByItemRequest request) {
        GetItemTypeByItemResponse response = new GetItemTypeByItemResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            Item item = inventoryService.getItemByCode(request.getItemCode());
            if (item == null) {
                context.addError(WsResponseCode.INVALID_ITEM_CODE, "Invalid Item Code " + request.getItemCode());
            } else {
                ItemType itemType = getNonBundledItemTypeById(item.getItemType().getId());
                if (itemType == null) {
                    context.addError(WsResponseCode.INVALID_ITEM_TYPE, "Invalid item type:" + item.getItemType().getId());
                } else {
                    ItemTypeFullDTO itemTypeDTO = new ItemTypeFullDTO(itemType);
                    itemTypeDTO.setCustomFieldValues(CustomFieldUtils.getCustomFieldValuesDTO(itemType));
                    response.setItemTypeDTO(itemTypeDTO);
                    response.setSuccessful(true);
                }
            }

        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional(readOnly = true)
    public List<ItemType> getAllItemTypes(int start, int batchSize) {
        return catalogDao.getAllItemTypes(start, batchSize);
    }
}
