/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jul 3, 2012
 *  @author singla
 */
package com.uniware.services.locking.impl;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;

import org.springframework.util.Assert;

import com.unifier.core.annotation.Level;
import com.unifier.core.locking.DistributedLock;
import com.unifier.core.locking.DistributedReadWriteLock;
import com.unifier.core.locking.LockingClient;
import com.uniware.core.locking.ILockingService;
import com.uniware.core.locking.Namespace;
import com.uniware.core.utils.UserContext;
import com.uniware.services.locking.LockingContext;

public class LockingServiceImpl implements ILockingService {

    private static final Level  DEFAULT_LEVEL   = Level.TENANT;

    private static final String DEFAULT_SECTION = "";

    private final LockingClient lockingClient;
    private final String        prefix;

    public LockingServiceImpl(LockingClient lockingClient, String prefix) {
        Assert.notNull(lockingClient, "lockingClient implementation should be provided");
        Assert.notNull(prefix, "base prefix cannot be null");
        this.lockingClient = lockingClient;
        this.prefix = prefix;
    }

    /**
     * Build the lock path for Zookeeper.
     * <ul>
     * <li>{@link Level#GLOBAL}: /uniware/namespace/GLOBAL/key</li>
     * <li>{@link Level#TENANT}: /uniware/namespace/{tenantCode}/key</li>
     * <li>{@link Level#FACILITY}: /uniware/namespace/{tenantCode}/{facilityId}/key</li>
     * </ul>
     *
     * @param level
     * @return
     */
    private StringBuilder buildLockPath(Level level) {
        StringBuilder lockPathBuilder = new StringBuilder("/").append(prefix).append("/");
        switch (level) {
            case TENANT:
                lockPathBuilder.append(UserContext.current().getTenant().getCode());
                break;
            case FACILITY:
                lockPathBuilder.append(UserContext.current().getTenant().getCode()).append("/").append(UserContext.current().getFacilityId());
                break;
            case GLOBAL:
                lockPathBuilder.append(Level.GLOBAL.name());
        }
        return lockPathBuilder;
    }

    /**
     * Returns a tenant level distributed read-write lock for a given namespace and key.
     *
     * @param namespace
     * @param key
     * @return
     */
    @Override
    public ReadWriteLock getReadWriteLock(Namespace namespace, String key) {
        return getReadWriteLock(namespace, key, DEFAULT_LEVEL);
    }

    @Override
    public ReadWriteLock getReadWriteLock(Namespace namespace, String key, Level level) {
        StringBuilder lockPathBuilder = buildLockPath(level);
        String lockKey = lockPathBuilder.append("/").append(namespace.name()).append("/").append(key).toString();
        LockingContext lockingContext = LockingContext.current();
        ReadWriteLock lock = lockingContext.getReadWriteLock(lockKey);
        if (lock == null) {
            lock = new DistributedReadWriteLock(lockingClient, lockKey);
            lockingContext.addReadWriteLock(lockKey, lock);
        }
        return lock;
    }

    /**
     * Returns a tenant level distributed lock for a given namespace and key.
     *
     * @param namespace
     * @param key
     * @return
     */
    @Override
    public Lock getLock(Namespace namespace, String key) {
        return getLock(namespace, key, DEFAULT_LEVEL);
    }

    @Override
    public Lock getLock(Namespace namespace, String key, Level level) {
        return getLock(namespace, key, level, DEFAULT_SECTION);
    }

    @Override
    public Lock getLock(Namespace namespace, String key, Level level, String lockSection) {
        StringBuilder lockPathBuilder = buildLockPath(level);
        String lockKey = lockPathBuilder.append("/").append(namespace.name()).append("/").append(key).toString();
        LockingContext lockingContext = LockingContext.current();
        Lock lock = lockingContext.getLock(lockKey);
        if (lock == null) {
            lock = new DistributedLock(lockingClient, lockKey, lockSection);
            lockingContext.addLock(lockKey, lock);
        }
        return lock;
    }
}
