/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 02-Apr-2014
 *  @author harsh
 */
package com.uniware.services.tenant;

import java.util.List;

import com.uniware.core.api.metadata.InformationSchemaDTO;
import com.uniware.core.api.tenant.CreateTenantRequest;
import com.uniware.core.api.tenant.CreateTenantResponse;
import com.uniware.core.api.tenant.GetTenantSetupStateRequest;
import com.uniware.core.api.tenant.GetTenantSetupStateResponse;
import com.uniware.core.api.tenant.RefreshTenantSetupStateRequest;
import com.uniware.core.api.tenant.RefreshTenantSetupStateResponse;
import com.uniware.core.entity.TenantSetupState;

public interface ITenantSetupService {

    List<InformationSchemaDTO> getInformationSchema();

    List<TenantSetupState> getTenantSetupStateMappings();

    CreateTenantResponse createTenant(CreateTenantRequest request);

    CreateTenantResponse createTenantRequestHandler(CreateTenantRequest request);

    GetTenantSetupStateResponse getTenantStateByTenantCode(GetTenantSetupStateRequest request);

    RefreshTenantSetupStateResponse refreshSetupState(RefreshTenantSetupStateRequest request);
}
