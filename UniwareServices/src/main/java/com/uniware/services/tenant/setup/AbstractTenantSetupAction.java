package com.uniware.services.tenant.setup;

import com.unifier.core.api.base.ServiceResponse;
import com.unifier.services.aspect.RollbackOnFailure;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.entity.Tenant;
import com.uniware.core.utils.UserContext;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by karunsingla on 02/08/15.
 */
public abstract class AbstractTenantSetupAction {

    @Transactional
    @RollbackOnFailure
    public ServiceResponse execute(TenantSetupContext context) {
        ServiceResponse response = new ServiceResponse();
        if (this.getClass().isAnnotationPresent(ImpersonateTenant.class)) {
            Tenant currTenant = UserContext.current().getTenant();
            try {
                UserContext.current().setTenant(context.getTenantToSetup());
                setup(context);
            } catch (Throwable e) {
                context.addError(WsResponseCode.ERROR_SETTING_UP_TENANT,
                        "Tenant:" + context.getTenantToSetup().getCode() + ",Action:" + this.getClass().getSimpleName() + ", message:" + e.getMessage());
            } finally {
                UserContext.current().setTenant(currTenant);
            }
        } else {
            try {
                setup(context);
            } catch (Throwable e) {
                context.addError(WsResponseCode.ERROR_SETTING_UP_TENANT,
                        "Tenant:" + context.getTenantToSetup().getCode() + ",Action:" + this.getClass().getSimpleName() + ", message:" + e.getMessage());
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        } else {
            response.setSuccessful(true);
        }
        return response;
    }

    public abstract void setup(TenantSetupContext context);
}
