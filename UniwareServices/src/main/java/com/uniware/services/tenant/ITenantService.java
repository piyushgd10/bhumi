/*
 *  Copyright 2013 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 10-Apr-2013
 *  @author unicom
 */
package com.uniware.services.tenant;

import java.util.List;

import com.unifier.core.entity.TenantSource;
import com.uniware.core.api.tenant.AllowSourceRequest;
import com.uniware.core.api.tenant.AllowSourceResponse;
import com.uniware.core.api.tenant.ChangeProductTypeRequest;
import com.uniware.core.api.tenant.ChangeProductTypeResponse;
import com.uniware.core.api.tenant.CleanupTenantDataRequest;
import com.uniware.core.api.tenant.CleanupTenantDataResponse;
import com.uniware.core.api.tenant.DeactivateTenantRequest;
import com.uniware.core.api.tenant.DeactivateTenantResponse;
import com.uniware.core.api.tenant.MarkTenantLiveRequest;
import com.uniware.core.api.tenant.MarkTenantLiveResponse;
import com.uniware.core.api.tenant.ReactivateTenantRequest;
import com.uniware.core.api.tenant.ReactivateTenantResponse;
import com.uniware.core.entity.Tenant;
import org.springframework.transaction.annotation.Transactional;

public interface ITenantService {

    Tenant getTenantByCode(String code);

    MarkTenantLiveResponse markTenantLive(MarkTenantLiveRequest request);

    CleanupTenantDataResponse cleanupTenantData(CleanupTenantDataRequest request);

    @Transactional(readOnly = true) List<Tenant> getAllTenants();

    String getDownloadFileName(String prefix);

    DeactivateTenantResponse deactivateTenant(DeactivateTenantRequest request);

    String getDownloadFileName(String prefix, String postfix);

    ChangeProductTypeResponse changeProductType(ChangeProductTypeRequest request);

    AllowSourceResponse addTenantSource(AllowSourceRequest request);

    List<TenantSource> getAllTenantSources();

    ReactivateTenantResponse reactivateTenant(ReactivateTenantRequest request);
}
