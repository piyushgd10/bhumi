package com.uniware.services.tenant.setup;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Created by karunsingla on 02/08/15.
 */
@Target({ TYPE })
@Retention(RUNTIME)
public @interface ImpersonateTenant {
}
