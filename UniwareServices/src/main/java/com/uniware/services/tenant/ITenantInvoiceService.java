/*
 *  Copyright 2013 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 07-May-2014
 *  @author akshay
 */
package com.uniware.services.tenant;

import com.uniware.core.api.tenant.GetTenantTransactionDetailsRequest;
import com.uniware.core.api.tenant.GetTenantTransactionDetailsResponse;

public interface ITenantInvoiceService {

    GetTenantTransactionDetailsResponse getTenantTransactionDetails(GetTenantTransactionDetailsRequest request);

}
