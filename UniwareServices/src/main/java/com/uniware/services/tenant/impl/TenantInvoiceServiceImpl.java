/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 07-May-2014
 *  @author akshay
 */
package com.uniware.services.tenant.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.unifier.core.api.validation.ValidationContext;
import com.uniware.core.api.tenant.GetTenantTransactionDetailsRequest;
import com.uniware.core.api.tenant.GetTenantTransactionDetailsResponse;
import com.uniware.core.api.tenant.TenantTransactionDetailDTO;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.entity.Tenant;
import com.uniware.dao.tenant.ITenantInvoiceDao;
import com.uniware.services.channel.IChannelService;
import com.uniware.services.tenant.ITenantInvoiceService;
import com.uniware.services.tenant.ITenantService;

@Service("clientHelperService")
public class TenantInvoiceServiceImpl implements ITenantInvoiceService {

    public enum TransactionType {
        SI,
        SARI,
        SSARS,
        SS,
        SSX,
        SAR;
    }

    @Autowired
    private ITenantService tenantService;
    
    @Autowired
    private IChannelService channelService;
    
    @Autowired
    private ITenantInvoiceDao tenantInvoiceDao;

    @Override
    @Transactional
    public GetTenantTransactionDetailsResponse getTenantTransactionDetails(GetTenantTransactionDetailsRequest request) {
        GetTenantTransactionDetailsResponse response = new GetTenantTransactionDetailsResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            Tenant tenant = tenantService.getTenantByCode(request.getTenantCode());
            if (tenant == null) {
                context.addError(WsResponseCode.INVALID_TENANT, "Invalid tenant code");
            } else {
                TransactionType transactionType = TransactionType.valueOf(request.getTransactionType());
                TenantTransactionDetailDTO tenantTransactionDetailDTO = new TenantTransactionDetailDTO();
                switch (transactionType) {
                    case SI:
                        tenantTransactionDetailDTO.setDispatched(tenantInvoiceDao.getDispatchedItemsCount(request.getDaterange()));
                        break;
                    case SS:
                        tenantTransactionDetailDTO.setDispatched(tenantInvoiceDao.getDispatchedShipmentsCount(request.getDaterange()));
                        break;
                    case SARI:
                        tenantTransactionDetailDTO.setDispatched(tenantInvoiceDao.getDispatchedItemsCount(request.getDaterange()));
                        tenantTransactionDetailDTO.setReturns(tenantInvoiceDao.getReturnedItemsCount(request.getDaterange()));
                        tenantTransactionDetailDTO.setReversePickup(tenantInvoiceDao.getReversePickupItemsCount(request.getDaterange()));
                        break;
                    case SSARS:
                        tenantTransactionDetailDTO.setDispatched(tenantInvoiceDao.getDispatchedShipmentsCount(request.getDaterange()));
                        tenantTransactionDetailDTO.setReturns(tenantInvoiceDao.getReturnedShipmentsCount(request.getDaterange()));
                        tenantTransactionDetailDTO.setReversePickup(tenantInvoiceDao.getReversePickupShipmentsCount(request.getDaterange()));
                        break;
                    case SSX:
                        tenantTransactionDetailDTO.setDispatched(tenantInvoiceDao.getGroupedDispatchedShipmentsCount(request.getDaterange(), request.getPerOrderItems()).toBigInteger());
                        tenantTransactionDetailDTO.setReturns(tenantInvoiceDao.getGroupedReturnShipmentsCount(request.getDaterange(), request.getPerOrderItems()).toBigInteger());
                        tenantTransactionDetailDTO.setReversePickup(tenantInvoiceDao.getGroupedReversePickupItemsCount(request.getDaterange(), request.getPerOrderItems()).toBigInteger());
                        break;
                    case SAR:
                        tenantTransactionDetailDTO.setReturns(tenantInvoiceDao.getGroupedReturnShipmentsCount(request.getDaterange(), request.getPerOrderItems()).toBigInteger());
                        tenantTransactionDetailDTO.setReversePickup(tenantInvoiceDao.getGroupedReversePickupItemsCount(request.getDaterange(), request.getPerOrderItems()).toBigInteger());
                        break;
                    default:
                        context.addError(WsResponseCode.INVALID_TAG, "Invalid transaction type");
                        break;
                }
                if (!context.hasErrors()) {
                    tenantTransactionDetailDTO.setChannels(channelService.getAllChannels().size());
                    tenantTransactionDetailDTO.setWarehouses(tenantInvoiceDao.getTenantActiveWarehouses());
                    tenantTransactionDetailDTO.setTransactionType(request.getTransactionType());
                    response.setTenantTransactionDetailDTO(tenantTransactionDetailDTO);
                    response.setSuccessful(true);
                }
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }
}
