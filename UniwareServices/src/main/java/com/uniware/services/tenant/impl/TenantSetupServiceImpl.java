/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 02-Apr-2014
 *  @author harsh
 */
package com.uniware.services.tenant.impl;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.unifier.core.annotation.Level;
import com.unifier.core.api.validation.ResponseCode;
import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.api.validation.WsError;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.entity.Job;
import com.unifier.core.utils.DateUtils;
import com.unifier.services.application.IApplicationSetupService;
import com.unifier.services.aspect.MarkDirty;
import com.unifier.services.job.IJobService;
import com.unifier.services.tenantprofile.mao.ITenantProfileMao;
import com.unifier.services.users.IUsersService;
import com.unifier.services.vo.TenantProfileVO;
import com.unifier.services.vo.TenantProfileVO.SetupStatus;
import com.unifier.services.vo.TenantProfileVO.Type;
import com.uniware.core.api.channel.ChannelDetailDTO;
import com.uniware.core.api.metadata.InformationSchemaDTO;
import com.uniware.core.api.tenant.CreateTenantRequest;
import com.uniware.core.api.tenant.CreateTenantResponse;
import com.uniware.core.api.tenant.GetTenantSetupStateRequest;
import com.uniware.core.api.tenant.GetTenantSetupStateResponse;
import com.uniware.core.api.tenant.RefreshTenantSetupStateRequest;
import com.uniware.core.api.tenant.RefreshTenantSetupStateResponse;
import com.uniware.core.api.tenant.TenantSetupLog;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.cache.FacilityCache;
import com.uniware.core.cache.TenantCache;
import com.uniware.core.concurrent.ContextAwareRunnable;
import com.uniware.core.entity.Facility;
import com.uniware.core.entity.Product;
import com.uniware.core.entity.Tenant;
import com.uniware.core.entity.TenantSetupState;
import com.uniware.core.locking.Namespace;
import com.uniware.core.locking.annotation.Lock;
import com.uniware.core.locking.annotation.Locks;
import com.uniware.core.utils.Constants;
import com.uniware.core.utils.UserContext;
import com.uniware.core.vo.PrintTemplateVO;
import com.uniware.dao.tenant.ITenantDao;
import com.uniware.dao.tenant.ITenantSetupDao;
import com.uniware.services.cache.ChannelCache;
import com.uniware.services.cache.PrintTemplateCache;
import com.uniware.services.configuration.TenantSetupConfiguration;
import com.uniware.services.configuration.TenantSetupConfiguration.TenantStates;
import com.uniware.services.reload.IReloadService;
import com.uniware.services.tenant.ITenantService;
import com.uniware.services.tenant.ITenantSetupService;
import com.uniware.services.tenant.setup.AbstractTenantSetupAction;
import com.uniware.services.tenant.setup.TenantSetupContext;

@Service("tenantSetupService")
public class TenantSetupServiceImpl implements ITenantSetupService {

    private static final Logger      LOG           = LoggerFactory.getLogger(TenantSetupServiceImpl.class);

    @Autowired
    private ITenantDao               tenantDao;

    @Autowired
    private ITenantSetupDao          tenantSetupDao;

    @Autowired
    private IJobService              jobService;

    @Autowired
    private IUsersService            usersService;

    @Autowired
    private ITenantService           tenantService;

    @Autowired
    private ITenantProfileMao        tenantProfileMao;

    @Autowired
    private IApplicationSetupService applicationSetupService;

    @Autowired
    private IReloadService           reloadService;

    private final List<SetupStatus>  setupStatuses = Arrays.asList(SetupStatus.values());

    //@Autowired
    //private ActiveMQConnector        activeMQConnector;

    @Override
    @Locks({ @Lock(ns = Namespace.TENANT, key = "#{#args[0].code}", level = Level.GLOBAL) })
    public CreateTenantResponse createTenant(CreateTenantRequest request) {
        LOG.info("Kicking off tenant creation");
        CreateTenantResponse response = new CreateTenantResponse();
        Tenant tenant = tenantService.getTenantByCode(request.getCode());
        TenantSetupContext context = new TenantSetupContext(request, tenant);
        try {
            String status = tenant.getSetupState();
            if (!TenantStates.DONE.equals(TenantStates.valueOf(status))) {
                LOG.info("Setting up tenant");
                setupTenant(context);
                if (!context.hasErrors() && TenantStates.valueOf(tenant.getSetupState()).equals(TenantStates.DONE)) {
                    response.setSuccessful(true);
                    response.setAccessUrl(tenant.getAccessUrl());
                    response.setSuccessRedirectUrl(getSuccessRedirectUrl(request.getAccessUrl(), request.getUsername(), request.getPassword()));
                    response.setTenantId(tenant.getId());
                }
            } else {
                response.setMessage("Tenant Creation is already complete");
            }
        } catch (Exception e) {
            context.addError(new ResponseCode(100089, "Error Creating Tenant"), e.getMessage());
            LOG.error("Error Creating Tenant " + request.getCode(), e);
        } catch (Throwable t) {
            context.addError(new ResponseCode(100089, "Error Creating Tenant"), t.getMessage());
            LOG.error("Error Creating Tenant {}, exception {}", request.getCode(), t);
        }
        if (context.hasErrors()) {
            response.setSuccessful(false);
            response.setErrors(context.getErrors());
        }
        if (context.hasWarnings()) {
            response.setWarnings(context.getWarnings());
        }
        LOG.info("Request: {}, Response: {}", request.toString(), response.toString());
        if (!context.hasErrors()) {
            response.setSuccessful(true);
        }
        tenantDao.updateTenantSetupLog(request, response);
        return response;
    }

    @Override
    @Locks({ @Lock(ns = Namespace.TENANT, key = "#{#args[0].code}", level = Level.GLOBAL) })
    public CreateTenantResponse createTenantRequestHandler(final CreateTenantRequest request) {
        CreateTenantResponse response = new CreateTenantResponse();
        ValidationContext context = validateRequest(request);
        if (!context.hasErrors()) {
            Tenant tenant = tenantService.getTenantByCode(request.getCode());
            if (tenant != null && TenantStates.DONE.equals(TenantStates.valueOf(tenant.getSetupState()))) {
                response.setMessage("Tenant " + tenant.getCode() + " is already present");
                response.addError(new WsError(40001, "Tenant " + tenant.getCode() + " is already present"));
                response.setSuccessful(false);
            } else {
                addTenant(request);
                //CreateTenantEvent createTenantEvent = new CreateTenantEvent();
                //createTenantEvent.setName(tenantSetupLog.getRequest().getCode() + " " + tenantSetupLog.getRequest().getEmail());
                //createTenantEvent.setTenantSetupLog(tenantSetupLog);
                try {
                    new Thread(new ContextAwareRunnable("tenant-creation", new Runnable() {
                        @Override
                        public void run() {
                            createTenant(request);
                        }
                    })).start();
                    response.setSuccessful(true);
                    response.setMessage("Tenant Creation Started");
                    LOG.info("Successfully added Tenant Creation Task to MQ");
                } catch (Exception e) {
                    response.setSuccessful(false);
                    response.addError(new WsError(100089, "Error adding tenant  creation task"));
                    LOG.error("Unable to add tenant creation task to MQ:" + e);
                }
            }
        } else {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    TenantSetupLog addTenant(CreateTenantRequest request) {
        createTenantInternal(request);
        TenantSetupLog tenantSetupLog = new TenantSetupLog();
        tenantSetupLog.setRequest(request);
        tenantDao.addTenantSetupLog(tenantSetupLog);
        return tenantSetupLog;
    }

    private ValidationContext validateRequest(CreateTenantRequest request) {
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            boolean validType = false;
            for (Type t : Type.values()) {
                if (t.name().equals(request.getTenantType())) {
                    validType = true;
                    break;
                }
            }
            if (!validType) {
                context.addError(WsResponseCode.INVALID_PRODUCT_TYPE);
            } else {
                request.setCode(request.getCode().toLowerCase());
            }
        }
        return context;
    }

    private String getSuccessRedirectUrl(String accessUrl, String username, String password) {
        String loginToken = usersService.generateLoginToken(username, 30, null);
        return "https://" + accessUrl + "/" + Constants.DEFAULT_TOKEN_BASED_LOGIN_FILTER_URL + "?" + Constants.TOKEN_BASED_LOGIN + "=" + loginToken;
    }

    private void setupTenant(TenantSetupContext context) {
        String state = context.getTenantToSetup().getSetupState();
        TenantSetupConfiguration setupConfig = ConfigurationManager.getInstance().getConfiguration(TenantSetupConfiguration.class);
        AbstractTenantSetupAction action = setupConfig.getNextAction(state);

        while (action != null && !context.hasErrors()) {
            LOG.info("Start Setting up {} for Tenant {}", action.getClass().getSimpleName(), context.getTenantToSetup().getCode());
            long startTime = System.currentTimeMillis();
            action.execute(context);
            if (!context.hasErrors()) {
                TenantSetupState nextState = setupConfig.getNextStateByOrder(context.getTenantToSetup().getSetupState());
                context.getTenantToSetup().setSetupState(nextState.getCode());
                tenantSetupDao.updateTenant(context.getTenantToSetup());
                long endTime = System.currentTimeMillis();
                LOG.info("Completed Executing {} for Tenant {} in {} seconds", new Object[] {
                        action.getClass().getSimpleName(),
                        context.getTenantToSetup().getCode(),
                        (endTime - startTime) / 1000 });
                action = setupConfig.getNextAction(context.getTenantToSetup().getSetupState());
            }
        }
    }

    @Override
    @Transactional(readOnly = true)
    public List<InformationSchemaDTO> getInformationSchema() {
        return tenantSetupDao.getInformationSchema();
    }

    @Override
    @Transactional(readOnly = true)
    public List<TenantSetupState> getTenantSetupStateMappings() {
        return tenantSetupDao.getSetupStatusMappings();
    }

    @Override
    @Transactional(readOnly = true)
    public GetTenantSetupStateResponse getTenantStateByTenantCode(GetTenantSetupStateRequest request) {
        ValidationContext context = request.validate();
        GetTenantSetupStateResponse response = new GetTenantSetupStateResponse();
        Tenant tenant = tenantService.getTenantByCode(request.getTenantCode());
        if (tenant != null) {
            TenantSetupState state = ConfigurationManager.getInstance().getConfiguration(TenantSetupConfiguration.class).getSetupStateByCode(tenant.getSetupState());
            List<TenantSetupLog> tenantSetupLogs = tenantDao.getTenantSetupLogStatus(request.getTenantCode());
            if (tenantSetupLogs.size() > 0) {
                CreateTenantResponse createTenantResponse = tenantSetupLogs.get(0).getResponse();
                if (createTenantResponse != null) {
                    response.setCreateTenantResponse(createTenantResponse);
                } else {
                    context.addWarning(WsResponseCode.INVALID_STATE, "Result has not been posted yet. Task may still be running");
                }
                response.setSuccessful(true);
            } else {
                context.addError(WsResponseCode.INVALID_STATE, "No task found for creating this tenant");
            }
            response.setTenantState(state);
        } else {
            context.addError(WsResponseCode.INVALID_TENANT, "Tenant with this code does not exist");
        }
        response.addErrors(context.getErrors());
        response.addWarnings(context.getWarnings());
        return response;
    }

    /**
     * This methods returns the tenant if present otherwise creates a new tenant
     *
     * @param request used to create tenant if tenant was not already present
     * @return tenant if exists otherwise newly created
     */
    @Transactional
    @MarkDirty(values = { TenantCache.class })
    private Tenant createTenantInternal(CreateTenantRequest request) {
        Tenant tenant = tenantService.getTenantByCode(request.getCode());
        if (tenant == null) {
            Product product = applicationSetupService.getProductByCode(request.getTenantType());
            tenant = new Tenant(request.getCode(), request.getName(), request.getCompanyName(), request.getMode(), request.getAccessUrl(), null, Tenant.StatusCode.ACTIVE,
                    DateUtils.getCurrentTime(), UserContext.current().getTenant(), product);
            tenant = tenantDao.createTenant(tenant);
        }
        return tenant;
    }

    @Override
    @Transactional
    public RefreshTenantSetupStateResponse refreshSetupState(RefreshTenantSetupStateRequest request) {
        RefreshTenantSetupStateResponse response = new RefreshTenantSetupStateResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            TenantProfileVO tenantProfile = tenantProfileMao.getTenantByCode(request.getCode());
            SetupStatus currentSetupStatus = tenantProfile.getSetupStatus();
            switch (currentSetupStatus) {
                case ADD_FACILITY:
                    Facility facility = CacheManager.getInstance().getCache(FacilityCache.class).getCurrentFacility();
                    if (facility.isDummy()) {
                        context.addError(WsResponseCode.INVALID_STATE, "Facility is not configured");
                    }
                    break;
                case ADD_CHANNEL:
                    Set<ChannelDetailDTO> channels = CacheManager.getInstance().getCache(ChannelCache.class).getChannels();
                    if (channels.isEmpty()) {
                        context.addError(WsResponseCode.INVALID_STATE, "No channel is configured");
                    }
                    break;
                case INVOICE_CONFIGURATION:
                    PrintTemplateVO printTemplate = CacheManager.getInstance().getCache(PrintTemplateCache.class).getPrintTemplateByName(PrintTemplateVO.Type.INVOICE.name());
                    if (!printTemplate.isUserCustomized()) {
                        context.addError(WsResponseCode.INVALID_STATE, "Invoice is not configured");
                    }
                    break;
                default:
                    break;
            }
            if (!context.hasErrors()) {
                SetupStatus setupStatus = getNextSetupStatus(tenantProfile.getSetupStatus());
                if (setupStatus != null) {
                    tenantProfile.setSetupStatus(setupStatus);
                    tenantProfileMao.addOrUpdateTenant(tenantProfile);
                    response.setSetupStatus(setupStatus.name());
                    response.setSuccessful(true);
                } else {
                    context.addError(WsResponseCode.INVALID_STATE, "Tenant is already configured");
                }
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        response.setSuccessful(!context.hasErrors());
        return response;
    }

    private SetupStatus getNextSetupStatus(SetupStatus setupStatus) {
        int index = setupStatuses.indexOf(setupStatus);
        return (index == -1 || index == setupStatuses.size() - 1) ? null : setupStatuses.get(index + 1);
    }

}
