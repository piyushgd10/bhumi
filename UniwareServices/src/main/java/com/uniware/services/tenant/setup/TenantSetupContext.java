package com.uniware.services.tenant.setup;

import com.unifier.core.api.validation.ValidationContext;
import com.uniware.core.api.tenant.CreateTenantRequest;
import com.uniware.core.entity.Tenant;

/**
 * Created by karunsingla on 02/08/15.
 */
public class TenantSetupContext extends ValidationContext {

    private CreateTenantRequest request;
    private Tenant              tenantToSetup;

    public TenantSetupContext(CreateTenantRequest request, Tenant tenantToSetup) {
        this.request = request;
        this.tenantToSetup = tenantToSetup;
    }

    public CreateTenantRequest getRequest() {
        return request;
    }

    public void setRequest(CreateTenantRequest request) {
        this.request = request;
    }

    public Tenant getTenantToSetup() {
        return tenantToSetup;
    }

    public void setTenantToSetup(Tenant tenantToSetup) {
        this.tenantToSetup = tenantToSetup;
    }
}
