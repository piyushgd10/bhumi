/*
 *  Copyright 2013 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 10-Apr-2013
 *  @author unicom
 */
package com.uniware.services.tenant.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.unifier.core.annotation.Level;
import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.email.EmailMessage;
import com.unifier.core.entity.Role;
import com.unifier.core.entity.TenantSource;
import com.unifier.core.entity.User;
import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.EncryptionUtils;
import com.unifier.core.utils.StringUtils;
import com.unifier.services.aspect.MarkDirty;
import com.unifier.services.aspect.RollbackOnFailure;
import com.unifier.services.email.IEmailService;
import com.unifier.services.tenantprofile.service.ITenantProfileService;
import com.unifier.services.users.IUsersService;
import com.unifier.services.vo.TenantProfileVO;
import com.unifier.services.vo.TenantProfileVO.Type;
import com.uniware.core.api.saleorder.DeleteFailedSaleOrdersRequest;
import com.uniware.core.api.tenant.AllowSourceRequest;
import com.uniware.core.api.tenant.AllowSourceResponse;
import com.uniware.core.api.tenant.ChangeProductTypeRequest;
import com.uniware.core.api.tenant.ChangeProductTypeResponse;
import com.uniware.core.api.tenant.CleanupTenantDataRequest;
import com.uniware.core.api.tenant.CleanupTenantDataResponse;
import com.uniware.core.api.tenant.DeactivateTenantRequest;
import com.uniware.core.api.tenant.DeactivateTenantResponse;
import com.uniware.core.api.tenant.MarkTenantLiveRequest;
import com.uniware.core.api.tenant.MarkTenantLiveResponse;
import com.uniware.core.api.tenant.ReactivateTenantRequest;
import com.uniware.core.api.tenant.ReactivateTenantResponse;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.cache.TenantCache;
import com.uniware.core.entity.Facility;
import com.uniware.core.entity.Source;
import com.uniware.core.entity.Tenant;
import com.uniware.core.entity.Tenant.Mode;
import com.uniware.core.entity.Tenant.StatusCode;
import com.uniware.core.locking.ILockingService;
import com.uniware.core.locking.Namespace;
import com.uniware.core.locking.annotation.Locks;
import com.uniware.core.utils.Constants.EmailTemplateType;
import com.uniware.core.utils.UserContext;
import com.uniware.dao.product.IProductDao;
import com.uniware.dao.tenant.ITenantDao;
import com.uniware.dao.tenant.ITenantSetupDao;
import com.uniware.database.UniwareAppDataSource;
import com.uniware.database.UniwareReplicationDataSource;
import com.uniware.services.channel.IChannelService;
import com.uniware.services.configuration.SourceConfiguration;
import com.uniware.services.configuration.TenantSourceConfiguration;
import com.uniware.services.reload.IReloadService;
import com.uniware.services.saleorder.ISaleOrderService;
import com.uniware.services.tenant.ITenantService;
import com.uniware.services.warehouse.IFacilityService;

@Service("tenantService")
public class TenantServiceImpl implements ITenantService {

    private static final Logger          LOG = LoggerFactory.getLogger(TenantServiceImpl.class);

    @Autowired
    private ITenantDao                   tenantDao;

    @Autowired
    private IUsersService                usersService;

    @Autowired
    private ISaleOrderService            saleOrderService;

    @Autowired
    private IReloadService               reloadService;

    @Autowired
    private IChannelService              channelService;

    @Autowired
    private IEmailService                emailService;

    @Autowired
    private IFacilityService             facilityService;

    @Autowired
    private ITenantProfileService        tenantProfileService;

    @Autowired
    private IProductDao                  productDao;

    @Autowired
    private ITenantSetupDao              tenantSetupDao;

    @Autowired
    private ILockingService              lockingService;

    @Autowired
    private UniwareAppDataSource         uniwareAppDataSource;

    @Autowired
    private UniwareReplicationDataSource uniwareReplicationDataSource;

    @Transactional
    @RollbackOnFailure
    @MarkDirty(values = { TenantCache.class })
    public MarkTenantLiveResponse markTenantLive(MarkTenantLiveRequest request) {
        MarkTenantLiveResponse response = new MarkTenantLiveResponse();
        ValidationContext context = request.validate();
        Tenant tenant = getTenantByCode(UserContext.current().getTenant().getCode());
        if (!context.hasErrors()) {
            if (!Mode.TESTING.equals(tenant.getMode())) {
                context.addError(WsResponseCode.INVALID_TENANT_MODE);
            } else {
                Lock lock = lockingService.getLock(Namespace.TENANT, UserContext.current().getTenant().getCode(), Level.GLOBAL);
                try {
                    lock.lock();
                    boolean success = true;
                    if (request.isClearData()) {
                        CleanupTenantDataResponse cleanupTenantDataResponse = cleanupTenantData(new CleanupTenantDataRequest(request.isClearAWBs()));
                        if (!cleanupTenantDataResponse.isSuccessful()) {
                            success = false;
                            context.addErrors(cleanupTenantDataResponse.getErrors());
                        }
                    }
                    if (success) {
                        tenant.setMode(Mode.LIVE);
                        TenantProfileVO tenantProfile = tenantProfileService.getTenantProfileByCode(tenant.getCode());
                        tenantProfile.setGoLiveDate(DateUtils.getCurrentTime());
                        tenantProfileService.addOrUpdateTenantProfile(tenantProfile);
                        tenant.setGoLiveDate(DateUtils.getCurrentTime());
                        List<String> users = usersService.getUsersByRole(Role.Code.ADMIN.toString());
                        List<String> recipients = new ArrayList<String>();
                        String name = "";
                        for (String username : users) {
                            User user = usersService.getUserByUsername(username);
                            if (StringUtils.isNotBlank(user.getEmail())) {
                                recipients.add(user.getEmail());
                            }
                            if (StringUtils.isBlank(name)) {
                                name = user.getName();
                            }
                        }
                        List<Facility> facilities = facilityService.getFacilities();
                        if (!recipients.isEmpty()) {
                            sendGoLiveEmail(recipients, tenant, name, facilities.get(0).getName(), tenantProfile);
                        }
                        response.setSuccessful(true);
                    }
                } finally {
                    lock.unlock();
                }
            }
        }

        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    @Override
    public CleanupTenantDataResponse cleanupTenantData(CleanupTenantDataRequest request) {
        CleanupTenantDataResponse response = cleanupTenantDataInternal(request);
        if (response.isSuccessful()) {
            reloadService.reloadAll();
        }
        return response;
    }

    @Transactional
    @RollbackOnFailure
    private CleanupTenantDataResponse cleanupTenantDataInternal(CleanupTenantDataRequest request) {
        CleanupTenantDataResponse response = new CleanupTenantDataResponse();
        ValidationContext context = request.validate();
        Tenant tenant = getTenantByCode(UserContext.current().getTenant().getCode());
        if (!context.hasErrors()) {
            if (!(Mode.TESTING.equals(tenant.getMode()) || StatusCode.INACTIVE.equals(tenant.getStatusCode()))) {
                context.addError(WsResponseCode.INVALID_TENANT_MODE);
            } else {
                tenantDao.cleanupTenantData(UserContext.current().getTenantId(), request.isClearAWBs());
                saleOrderService.removeAllFailedSaleOrders(new DeleteFailedSaleOrdersRequest());
                channelService.clearChannelOrderSyncStatuses();
                channelService.clearChannelInventorySyncStatuses();
                channelService.clearChannelCatalogSyncStatuses();
                channelService.clearChannelShipmentSyncStatuses();
                response.setSuccessful(true);
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    private void sendGoLiveEmail(List<String> recipients, Tenant tenant, String name, String facilityName, TenantProfileVO tenantProfile) {
        EmailMessage message = new EmailMessage(recipients, EmailTemplateType.GO_LIVE_NOTIFICATION.name());
        message.addTemplateParam("tenant", tenant);
        message.addTemplateParam("tenantProfile", tenantProfile);
        message.addTemplateParam("userName", name);
        message.addTemplateParam("facilityName", facilityName);
        emailService.send(message);
    }

    @Override
    @Transactional
    public Tenant getTenantByCode(String code) {
        return tenantDao.getTenantByCode(code);
    }

    @Override
    public List<Tenant> getAllTenants() {
        getAllTenantsFromSecondaryDS();
        return getAllTenantsFromPrimaryDS();
    }

    @Transactional
    private List<Tenant> getAllTenantsFromPrimaryDS() {
        return uniwareAppDataSource.getAllTenants();
    }

    @Transactional(value = "txManagerReplication")
    private List<Tenant> getAllTenantsFromSecondaryDS() {
        return uniwareReplicationDataSource.getAllTenants();
    }

    @Override
    public String getDownloadFileName(String prefix) {
        return getDownloadFileName(prefix, null);
    }

    @Override
    public String getDownloadFileName(String prefix, String postfix) {
        String dateStr = DateUtils.dateToString(DateUtils.getCurrentDate(), "dd-MM-yyyy");
        String licenseKey = tenantProfileService.getTenantProfileByCode(UserContext.current().getTenant().getCode()).getLicenseKey();
        String hash = EncryptionUtils.md5Encode(licenseKey + "#" + dateStr);
        StringBuilder fileNameBuilder = new StringBuilder(prefix).append("_").append(dateStr).append("_").append(hash).append("_data");
        if (StringUtils.isNotEmpty(postfix)) {
            fileNameBuilder.append("_").append(postfix);
        }
        return fileNameBuilder.append(".csv").toString();
    }

    public DeactivateTenantResponse deactivateTenant(DeactivateTenantRequest request) {
        java.util.concurrent.locks.Lock lock = lockingService.getLock(Namespace.TENANT, UserContext.current().getTenant().getCode());
        try {
            lock.lock();
            return deactivateTenantInternal(request);
        } finally {
            lock.unlock();
        }
    }

    @Transactional
    @RollbackOnFailure
    @MarkDirty(values = { TenantCache.class })
    private DeactivateTenantResponse deactivateTenantInternal(DeactivateTenantRequest request) {
        DeactivateTenantResponse response = new DeactivateTenantResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            Tenant tenant = getTenantByCode(UserContext.current().getTenant().getCode());
            TenantProfileVO tenantProfile = tenantProfileService.getTenantProfileByCode(tenant.getCode());
            if (Tenant.StatusCode.ACTIVE.equals(tenant.getStatusCode())) {
                if (tenantDao.updateTenantStatus(StatusCode.INACTIVE)) {
                    LOG.info("Tenant: {} DEACTIVATED", tenant.getCode());
                } else {
                    context.addError(WsResponseCode.UNKNOWN_ERROR, "Unable to deactivate tenant");
                }
            }
            if (!context.hasErrors() && !tenantProfile.getStatusCode().equals(TenantProfileVO.Status.BLOCKED)) {
                tenantProfileService.deactivateTenant(tenantProfile);
                LOG.info("Tenant: {} BLOCKED TenantProfile", tenant.getCode());
            }
            //            if (!context.hasErrors()) {
            //                try {
            //                    jobService.deleteJobs();
            //                } catch (SchedulerException e) {
            //                    LOG.error("Failed to delete jobs", e);
            //                    context.addError(WsResponseCode.UNKNOWN_ERROR, "Failed to delete jobs");
            //                }
            //            }
        }
        response.addErrors(context.getErrors());
        response.setSuccessful(!context.hasErrors());
        return response;
    }

    @Locks({ @com.uniware.core.locking.annotation.Lock(ns = Namespace.TENANT, key = "#{#args[0].tenantCode}", level = Level.GLOBAL) })
    @Transactional
    @RollbackOnFailure
    @MarkDirty(values = { TenantCache.class })
    public ReactivateTenantResponse reactivateTenant(ReactivateTenantRequest request) {
        ReactivateTenantResponse response = new ReactivateTenantResponse();
        ValidationContext context = request.validate();
        Tenant currTenant = UserContext.current().getTenant();
        if (!context.hasErrors()) {
            Tenant tenant = getTenantByCode(request.getTenantCode());
            if (tenant == null) {
                context.addError(WsResponseCode.INVALID_TENANT, "No tenant with given code");
            } else if (Tenant.StatusCode.ACTIVE.equals(tenant.getStatusCode())) {
                context.addError(WsResponseCode.INVALID_TENANT, "Tenant already active");
            } else if (!TenantProfileVO.PaymentStatus.HEALTHY.equals(tenantProfileService.getTenantProfileByCode(tenant.getCode()).getPaymentStatus())) {
                context.addError(WsResponseCode.INVALID_PAYMENT_STATUS, "Payment status not healthy");
            } else {
                try {
                    UserContext.current().setTenant(tenant);
                    if (request.isCleanUpData()) {
                        CleanupTenantDataResponse cleanupTenantDataResponse = cleanupTenantData(new CleanupTenantDataRequest(request.isClearAWBs()));
                        if (!cleanupTenantDataResponse.isSuccessful()) {
                            context.addErrors(cleanupTenantDataResponse.getErrors());
                        }
                    }
                    if (!context.hasErrors()) {
                        if (!tenantDao.updateTenantStatus(StatusCode.ACTIVE)) {
                            context.addError(WsResponseCode.UNKNOWN_ERROR, "Unable to reactivate tenant");
                        } else {
                            tenantDao.updateTenantReactivationDate(DateUtils.getCurrentTime());
                            TenantProfileVO tenantProfile = tenantProfileService.getTenantProfileByCode(tenant.getCode());
                            if (!tenantProfile.getStatusCode().equals(TenantProfileVO.Status.ACTIVE)) {
                                tenantProfileService.activateTenant(tenantProfile);
                            }
                        }
                    }
                } finally {
                    UserContext.current().setTenant(currTenant);
                }
            }

        }
        response.addErrors(context.getErrors());
        response.setSuccessful(!context.hasErrors());
        return response;
    }

    @Override
    public ChangeProductTypeResponse changeProductType(ChangeProductTypeRequest request) {
        ChangeProductTypeResponse response = new ChangeProductTypeResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            LOG.info("Starting to change product type for tenant {} to {} ", UserContext.current().getTenant().getCode(), request.getProductType());
            Type type = null;
            try {
                type = Type.valueOf(request.getProductType());
            } catch (Exception e) {
                context.addError(WsResponseCode.INVALID_PRODUCT_TYPE, "No product exists for given product type");
            }
            if (!context.hasErrors()) {
                changeProductTypeInternal(type);
                reloadService.reloadAll();
            }
        }
        response.addErrors(context.getErrors());
        response.setSuccessful(!context.hasErrors());
        return response;
    }

    private void changeProductTypeInternal(Type type) {
        Lock lock = lockingService.getLock(Namespace.TENANT, UserContext.current().getTenant().getCode());
        try {
            lock.lock();
            doChangeProductTypeInternal(type);
        } finally {
            lock.unlock();
        }
    }

    @Transactional
    private void doChangeProductTypeInternal(Type type) {
        Tenant tenant = getTenantByCode(UserContext.current().getTenant().getCode());
        tenant.setProduct(productDao.getProductByCode(type.name()));
        tenant.setMode(Mode.TESTING);
        tenantSetupDao.updateTenant(tenant);
        TenantProfileVO profile = tenantProfileService.getTenantProfileByCode(tenant.getCode());
        profile.setTenantType(type);
        profile.setGoLiveDate(null);
        tenantProfileService.addOrUpdateTenantProfile(profile);
    }

    @Override
    @Transactional
    @RollbackOnFailure
    @MarkDirty(values = { TenantSourceConfiguration.class })
    public AllowSourceResponse addTenantSource(AllowSourceRequest request) {
        AllowSourceResponse response = new AllowSourceResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            SourceConfiguration config = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class);
            Source reqSource = config.getSourceByCode(request.getSourceCode());
            if (reqSource == null) {
                context.addError(WsResponseCode.INVALID_SOURCE, "Requested source code in invalid");
            } else {
                if (!config.getApplicableSources().contains(reqSource)) {
                    TenantSource existingTs = tenantDao.getTenantSource(reqSource.getCode());
                    if (existingTs == null) {
                        TenantSource ts = new TenantSource();
                        ts.setSourceCode(reqSource.getCode());
                        ts.setTenant(UserContext.current().getTenant());
                        ts.setCreated(DateUtils.getCurrentTime());
                        tenantDao.addTenantSource(ts);
                        LOG.info("Allowed source: {}", reqSource.getCode());
                    } else if (!existingTs.isEnabled()) {
                        existingTs.setEnabled(true);
                        LOG.info("Enabled source: {}", reqSource.getCode());
                    } else {
                        LOG.info("Source {} is already allowed", reqSource.getCode());
                    }
                } else {
                    LOG.info("Source {} is already allowed by product source", reqSource.getCode());
                }
            }
        }
        response.addErrors(context.getErrors());
        response.addWarnings(context.getWarnings());
        response.setSuccessful(!context.hasErrors());
        return response;
    }

    @Override
    public List<TenantSource> getAllTenantSources() {
        return tenantDao.getAllTenantSources();
    }
}
