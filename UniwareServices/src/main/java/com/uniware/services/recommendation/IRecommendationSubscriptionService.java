/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Nov 25, 2015
 *  @author akshay
 */
package com.uniware.services.recommendation;

import com.uniware.core.api.recommendation.SubscribeRecommendationsRequest;
import com.uniware.core.api.recommendation.SubscriptionRecommendationsResponse;
import com.uniware.core.api.recommendation.GetRecommendationProvidersRequest;
import com.uniware.core.api.recommendation.GetRecommendationProvidersResponse;
import com.uniware.core.api.recommendation.GetRecommendationTypesRequest;
import com.uniware.core.api.recommendation.GetRecommendationTypesResponse;
import com.uniware.core.api.recommendation.GetSubscribedRecommendationProvidersRequest;
import com.uniware.core.api.recommendation.GetSubscribedRecommendationProvidersResponse;
import com.uniware.core.api.recommendation.UnsubscribeRecommendationRequest;
import com.uniware.core.api.recommendation.UnsubscribeRecommendationResponse;


/**
 * Interface to manage tenant level recommendation interests.
 * <ul>
 * The responsibilities include:
 * <li>Get recommendation types
 * <li>Get recommendation providers
 * <li>Get recommendation providers subscribed to by the tenant
 * <li>Subscribe a tenant to a recommendation provider
 * <li>Un-subscribe a tenant to a recommendation provider
 * <li>Get list of recommendations based on certain criterion
 * </ul>
 */
public interface IRecommendationSubscriptionService {

    /** Get recommendation types */
    public GetRecommendationTypesResponse getRecommendationTypes(final GetRecommendationTypesRequest request);
    
    /** Get recommendation providers */
    public GetRecommendationProvidersResponse getRecommendationProviders(final GetRecommendationProvidersRequest request);
    
    /** Get Subscribed recommendation providers */
    public GetSubscribedRecommendationProvidersResponse getSubscribedRecommendationProviders(final GetSubscribedRecommendationProvidersRequest request);
    
    /** Add subscription  */
    public SubscriptionRecommendationsResponse addSubscription(final SubscribeRecommendationsRequest request);
    
    /** Remove subscription  */
    public UnsubscribeRecommendationResponse removeSubscription(final UnsubscribeRecommendationRequest request);
}
