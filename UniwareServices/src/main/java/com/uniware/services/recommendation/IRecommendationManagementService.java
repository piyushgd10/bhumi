/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Nov 25, 2015
 *  @author akshay
 */
package com.uniware.services.recommendation;

import com.uniware.core.api.recommendation.ApproveRecommendationRequest;
import com.uniware.core.api.recommendation.ApproveRecommendationResponse;
import com.uniware.core.api.recommendation.CancelRecommendationRequest;
import com.uniware.core.api.recommendation.CancelRecommendationResponse;
import com.uniware.core.api.recommendation.CreateRecommendationRequest;
import com.uniware.core.api.recommendation.CreateRecommendationResponse;
import com.uniware.core.api.recommendation.GetRecommendationsRequest;
import com.uniware.core.api.recommendation.GetRecommendationsResponse;
import com.uniware.core.api.recommendation.ListRecommendationsRequest;
import com.uniware.core.api.recommendation.RejectRecommendationRequest;
import com.uniware.core.api.recommendation.RejectRecommendationResponse;


/**
 * Manages the lifecyle of a recommendation.
 * 
 * <ul>
 * The responsibilities include:
 * <li>Create recommendation
 * <li>Cancels a recommendation
 * <li>Gets recommendations
 * <li>Approve recommendations
 * <li>Reject recommendations
 * <li>List recommendations 
 * </ul>
 */
public interface IRecommendationManagementService {
    
    /** Cancel channel item type price recommendation*/
    public CancelRecommendationResponse cancelRecommendation(final CancelRecommendationRequest request);

    /** Create recommendation for a channel item type*/
    public CreateRecommendationResponse createRecommendation(final CreateRecommendationRequest request);
    
    /** Reject recommendation for a channel item type*/
    public ApproveRecommendationResponse approveRecommendation(final ApproveRecommendationRequest request);
    
    /** Reject recommendation for a channel item type*/
    public RejectRecommendationResponse rejectRecommendation(final RejectRecommendationRequest request);

    /** Get recommendations based on identifiers*/
    public GetRecommendationsResponse getRecommendations(final GetRecommendationsRequest request);
    
    /** List recommendations on with default 50 results and in case of NON ACTIVE recommendation default 
     * date range filter is 7 days and it cannot be set to more than that*/
    public GetRecommendationsResponse listRecommendations(final ListRecommendationsRequest request);

}
