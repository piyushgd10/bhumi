/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Nov 25, 2015
 *  @author akshay
 */
package com.uniware.services.recommendation.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.unifier.core.api.base.ServiceRequest;
import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.jms.MessagingConstants;
import com.unifier.core.recommendation.RecommendationMessage;
import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.DateUtils.DateRange;
import com.unifier.core.utils.DateUtils.TextRange;
import com.unifier.core.utils.ValidatorUtils;
import com.unifier.services.aspect.RollbackOnFailure;
import com.uniware.core.api.prices.EditChannelItemTypePriceRequest;
import com.uniware.core.api.prices.EditChannelItemTypePriceResponse;
import com.uniware.core.api.prices.GetChannelItemTypePriceRequest;
import com.uniware.core.api.prices.GetChannelItemTypePriceResponse;
import com.uniware.core.api.prices.PushChannelItemTypePriceRequest;
import com.uniware.core.api.prices.dto.PriceDTO;
import com.uniware.core.api.recommendation.ApproveRecommendationRequest;
import com.uniware.core.api.recommendation.ApproveRecommendationResponse;
import com.uniware.core.api.recommendation.CancelRecommendationRequest;
import com.uniware.core.api.recommendation.CancelRecommendationResponse;
import com.uniware.core.api.recommendation.CreateRecommendationRequest;
import com.uniware.core.api.recommendation.CreateRecommendationResponse;
import com.uniware.core.api.recommendation.GetRecommendationProvidersRequest;
import com.uniware.core.api.recommendation.GetRecommendationsRequest;
import com.uniware.core.api.recommendation.GetRecommendationsResponse;
import com.uniware.core.api.recommendation.ListRecommendationsRequest;
import com.uniware.core.api.recommendation.PriceRecommendationBody;
import com.uniware.core.api.recommendation.RejectRecommendationRequest;
import com.uniware.core.api.recommendation.RejectRecommendationResponse;
import com.uniware.core.api.recommendation.dto.PriceRecommendationDTO;
import com.uniware.core.api.recommendation.dto.RecommendationDTO;
import com.uniware.core.api.recommendation.dto.RecommendationProviderDTO;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.concurrent.ContextAwareExecutorFactory;
import com.uniware.core.entity.Channel;
import com.uniware.core.entity.Channel.SyncStatus;
import com.uniware.core.entity.ChannelItemType;
import com.uniware.core.utils.UserContext;
import com.uniware.core.vo.PriceRecommendationVO;
import com.uniware.core.vo.PriceRecommendationVO.RecommendationStatus;
import com.uniware.mao.recommendation.IRecommendationMao;
import com.uniware.services.cache.ChannelCache;
import com.uniware.services.channel.IChannelCatalogService;
import com.uniware.services.messaging.jms.ActiveMQConnector;
import com.uniware.services.pricing.IPricingFacade;
import com.uniware.services.recommendation.IRecommendationManagementService;
import com.uniware.services.recommendation.IRecommendationSubscriptionService;
import com.uniware.services.recommendation.impl.RepairRecommendationHandler.RepairRecommendationJob;

/**
 * An implementation to manage recommendation lifecycle
 * <p>
 * All recommendation updates from creation to expiry is managed by this service
 */
@Service("recommendationManagementService")
public class RecommendationManagemetServiceImpl implements IRecommendationManagementService {

    private static final Logger LOG          = LoggerFactory.getLogger(RecommendationManagemetServiceImpl.class);
    private static final String SERVICE_NAME = "recommendationManager";

    @Autowired
    private IRecommendationMao recommendationMao;

    @Autowired
    private IRecommendationSubscriptionService recommendationSubscriptionService;

    @Autowired
    private IChannelCatalogService channelCatalogService;

    @Autowired
    private IPricingFacade pricing;

    @Autowired
    @Qualifier("activeMQConnector1")
    private ActiveMQConnector activeMQConnector;

    @Autowired
    private ContextAwareExecutorFactory executorFactory;

    public enum RecommendationType {
        PRICE("pr-");

        private final String prefix;

        public String getPrefix() {
            return this.prefix;
        }

        RecommendationType(String prefix) {
            this.prefix = prefix;
        }
    }

    @Override
    public CancelRecommendationResponse cancelRecommendation(final CancelRecommendationRequest request) {
        CancelRecommendationResponse response = new CancelRecommendationResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            PriceRecommendationVO recommendation = getRecommendationByIdentifier(request.getRecommendationIdentifier());
            if (recommendation == null) {
                context.addError(WsResponseCode.INVALID_RECOMMENDATION_CODE, "Invalid recommendation code " + request.getRecommendationIdentifier());
            } else {
                recommendation.setClosureReason(request.getReason());
                recommendation.setClosedBy(request.getRequestedBy());
                if (!updateActiveRecommendation(recommendation, PriceRecommendationVO.RecommendationStatus.CANCELLED.name())) {
                    context.addError(WsResponseCode.INVALID_RECOMMENDATION_STATUS, "Recommendation is no more active : " + recommendation.getRecommendationStatus());
                    response.setMessage("Status is " + recommendation.getRecommendationStatus());
                }
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        response.setSuccessful(!context.hasErrors());
        return response;
    }

    @Override
    public CreateRecommendationResponse createRecommendation(final CreateRecommendationRequest request) {
        CreateRecommendationResponse response = new CreateRecommendationResponse();
        ValidationContext context = request.validate();
        // String identifier = constructRequestType("CREATE", RecommendationType.valueOf(request.getRecommendationType()));
        PriceRecommendationBody priceRequest = (PriceRecommendationBody) request.getRecommendationBody();
        context = ServiceRequest.validate(priceRequest, context, true);
        if (!context.hasErrors()) {
            Channel channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelByCode(priceRequest.getChannelCode());
            if (channel == null) {
                context.addError(WsResponseCode.INVALID_CHANNEL_CODE, "Invalid channel code " + priceRequest.getChannelCode());
            } else if (!SyncStatus.ON.equals(channel.getPricingSyncStatus())) {
                context.addError(WsResponseCode.PRICE_SYNC_NOT_CONFIGURED, "Price sync not configured for channel " + channel.getCode());
            } else {
                ChannelItemType channelItemType = channelCatalogService.getChannelItemTypeByChannelAndChannelProductId(channel.getCode(), priceRequest.getChannelProductId());
                if (channelItemType == null) {
                    context.addError(WsResponseCode.INVALID_CHANNEL_ITEM_TYPE, "No such channel item type exists with " + priceRequest.getChannelProductId());
                } else {
                    GetChannelItemTypePriceRequest getItemTypePriceRequest = new GetChannelItemTypePriceRequest();
                    getItemTypePriceRequest.setChannelCode(channel.getCode());
                    getItemTypePriceRequest.setChannelProductId(priceRequest.getChannelProductId());
                    GetChannelItemTypePriceResponse priceResponse = pricing.getChannelItemTypePrice(getItemTypePriceRequest);
                    if (priceResponse.isSuccessful()) {
                        PriceDTO oldPrices = priceResponse.getChannelItemTypePrice();
                        BigDecimal msp = ValidatorUtils.getOrDefaultValue(priceRequest.getMsp(), oldPrices.getMsp());
                        BigDecimal mrp = ValidatorUtils.getOrDefaultValue(priceRequest.getMrp(), oldPrices.getMrp());
                        BigDecimal sellingPrice = ValidatorUtils.getOrDefaultValue(priceRequest.getSellingPrice(), oldPrices.getSellingPrice());
                        PriceDTO recommendedPrices = new PriceDTO(msp, mrp, sellingPrice, null, null, priceRequest.getCurrencyCode());
                        context.addErrors(pricing.validatePrices(oldPrices, recommendedPrices, channel.getCode()));
                    } else {
                        context.addError(WsResponseCode.INVALID_PRICES, "Error while validating prices for channelProductId " + priceRequest.getChannelProductId());
                    }
                }
            }
            // Validate if registered to recommend for type 
            if (!context.hasErrors() && !isRegisteredToRecommend(priceRequest.getRecommendationType(), request.getSource(), context)) {
                response.setErrors(context.getErrors());
                return response;
            }
        }
        ObjectId objectId = new ObjectId();
        String id = RecommendationType.PRICE.getPrefix() + objectId.toString();
        if (!context.hasErrors()) {
            PriceRecommendationVO priceRecommendationVO = new PriceRecommendationVO();
            priceRecommendationVO.setId(id);
            priceRecommendationVO.setChannelCode(priceRequest.getChannelCode());
            priceRecommendationVO.setChannelProductId(priceRequest.getChannelProductId());
            priceRecommendationVO.setRecommendationReason(request.getReason());
            priceRecommendationVO.setMrp(priceRequest.getMrp());
            priceRecommendationVO.setMsp(priceRequest.getMsp());
            priceRecommendationVO.setSellingPrice(priceRequest.getSellingPrice());
            priceRecommendationVO.setCurrencyCode(priceRequest.getCurrencyCode());
            priceRecommendationVO.setRecommendationProvider(request.getSource());
            priceRecommendationVO.setExpiryTime(request.getExpiryTime());
            priceRecommendationVO.setRecommendationStatus(RecommendationStatus.ACTIVE.name());
            priceRecommendationVO.setCreated(DateUtils.getCurrentTime());
            priceRecommendationVO.setCreatedBy(request.getCreatedBy());
            createRecommendation(priceRecommendationVO);
            response.setSuccessful(true);
            response.setId(priceRecommendationVO.getId());
            publishRecommendation(priceRequest.getRecommendationType(), getActiveRecommendationsCount()); // PUBLISH recommendation to GCM
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    @Override
    public ApproveRecommendationResponse approveRecommendation(final ApproveRecommendationRequest request) {
        ApproveRecommendationResponse response = new ApproveRecommendationResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            PriceRecommendationVO recommendation = getRecommendationByIdentifier(request.getRecommendationIdentifier());
            if (recommendation == null) {
                context.addError(WsResponseCode.INVALID_RECOMMENDATION_CODE, "Invalid recommendation code " + request.getRecommendationIdentifier());
            } else {
                doApproveRecommendation(request, response, context, recommendation);
                if (!context.hasErrors()) {
                    PushChannelItemTypePriceRequest pushRequest = new PushChannelItemTypePriceRequest();
                    pushRequest.setChannelCode(recommendation.getChannelCode());
                    pushRequest.setChannelProductId(recommendation.getChannelProductId());
                    pushRequest.setForcePush(true);
                    pricing.pushPricesToChannel(pushRequest);
                }
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    @Transactional
    @RollbackOnFailure
    private ApproveRecommendationResponse doApproveRecommendation(final ApproveRecommendationRequest request, ApproveRecommendationResponse response, ValidationContext context,
            PriceRecommendationVO recommendation) {
        EditChannelItemTypePriceRequest editPriceRequest = new EditChannelItemTypePriceRequest();
        editPriceRequest.setChannelCode(recommendation.getChannelCode());
        editPriceRequest.setChannelProductId(recommendation.getChannelProductId());
        editPriceRequest.setCurrencyCode(recommendation.getCurrencyCode());
        editPriceRequest.setMrp(recommendation.getMrp());
        editPriceRequest.setSellingPrice(recommendation.getSellingPrice());
        editPriceRequest.setMsp(recommendation.getMsp());
        EditChannelItemTypePriceResponse editPriceResponse = pricing.localEditChannelItemType(editPriceRequest);
        if (!editPriceResponse.isSuccessful()) {
            context.addErrors(editPriceResponse.getErrors());
        } else {
            // Update recommendation on successful price edit which if fails the service will rollback
            recommendation.setClosureReason(request.getReason());
            recommendation.setClosedBy(request.getRequestedBy());
            if (!updateActiveRecommendation(recommendation, RecommendationStatus.APPROVED.name())) {
                context.addError(WsResponseCode.INVALID_RECOMMENDATION_STATUS, "Recommendation is no more active");
                response.setMessage("Status is " + recommendation.getRecommendationStatus());
            } 
        }
        response.setSuccessful(!context.hasErrors());
        return response;
    }

    @Override
    public GetRecommendationsResponse getRecommendations(final GetRecommendationsRequest request) {
        GetRecommendationsResponse response = new GetRecommendationsResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            List<RecommendationDTO> recommendationDTOs = new ArrayList<>();
            List<PriceRecommendationVO> recommendations = getRecommendationsByIdentifiers(request.getReferenceIdentifiers());
            if (!recommendations.isEmpty()) {
                recommendationDTOs = getRecommendations(recommendations, null);
            }
            response.setRecommendations(recommendationDTOs);
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        response.setSuccessful(!context.hasErrors());
        return response;
    }

    @Override
    public GetRecommendationsResponse listRecommendations(final ListRecommendationsRequest request) {
        GetRecommendationsResponse response = new GetRecommendationsResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            List<RecommendationDTO> recommendationDTOs = new ArrayList<>();
            DateRange createdBetween = request.getCreatedBetween();
            // Get filter for more then 7 days for non ACTIVE recommendations 
            if (!RecommendationStatus.ACTIVE.name().equals(request.getRecommendationStatus())) {
                if (createdBetween != null && DateUtils.diff(createdBetween.getEnd(), createdBetween.getStart(), DateUtils.Resolution.WEEK) >= 1) {
                    context.addError(WsResponseCode.INVALID_REQUEST, "Interval cannot be more then 7 days recommendations which are not ACTIVE");
                    response.setErrors(context.getErrors());
                    return response;
                }
                if (createdBetween == null) {
                    createdBetween = new DateRange(TextRange.LAST_7_DAYS);
                }
            }
            List<PriceRecommendationVO> recommendations = getFilteredRecommendations(null, null, request.getRecommendationStatus(), null, createdBetween, null, request.getStart(),
                    request.getNoOfResults());
            if (!recommendations.isEmpty()) {
                recommendationDTOs = getRecommendations(recommendations, request.getRecommendationStatus());
            }
            response.setRecommendations(recommendationDTOs);
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        response.setSuccessful(!context.hasErrors());
        return response;
    }

    private List<RecommendationDTO> getRecommendations(List<PriceRecommendationVO> recommendations, String status) {
        // Skip if no such item exists anymore
        List<RecommendationDTO> recommendationDTOs = new ArrayList<>();
        Map<String, ChannelItemType> channelProductIdToChannelItemType = new HashMap<>();
        for (PriceRecommendationVO recommendation : recommendations) {
            if (!channelProductIdToChannelItemType.containsKey(recommendation.getChannelProductId())) {
                ChannelItemType channelItemType = channelCatalogService.getChannelItemTypeByChannelAndChannelProductId(recommendation.getChannelCode(),
                        recommendation.getChannelProductId());
                channelProductIdToChannelItemType.put(recommendation.getChannelProductId(), channelItemType);
            }
            ChannelItemType channelItemType = channelProductIdToChannelItemType.get(recommendation.getChannelProductId());
            if (channelItemType != null) {
                PriceRecommendationDTO priceDTO = new PriceRecommendationDTO(recommendation);
                priceDTO.setSellerSkuCode(channelItemType.getSellerSkuCode());
                RecommendationDTO recoDTO = new RecommendationDTO(recommendation, priceDTO);
                if (recommendation.getExpiryTime().before(DateUtils.getCurrentTime())) { // EXPIRED recomendation with update pending submitted for repair
                    submitRecommendationToRepair(recommendation.getId(), recommendation.getRecommendationStatus(), RecommendationStatus.EXPIRED.name(), 
                            RecommendationStatus.EXPIRED.name());
                    recoDTO.setRecommendationStatus(RecommendationStatus.EXPIRED.name());
                }
                if (status == null || recoDTO.getRecommendationStatus().equals(status)) { // ADD to list based status filter
                    recommendationDTOs.add(recoDTO);
                }
            } else {
                LOG.warn("Skipping fetch recommendation for channelProductId : {}, channel : {} as no such channel item exists in system anymore",
                        recommendation.getChannelProductId(), recommendation.getChannelCode());
            }
        }
        return recommendationDTOs;
    }

    @Override
    public RejectRecommendationResponse rejectRecommendation(final RejectRecommendationRequest request) {
        RejectRecommendationResponse response = new RejectRecommendationResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            //   RejectPriceRecommendationRequest priceRejectionRequest = (RejectPriceRecommendationRequest) request.getRecommendationBody(); NOT Required
            PriceRecommendationVO recommendation = getRecommendationByIdentifier(request.getRecommendationIdentifier());
            if (recommendation == null) {
                context.addError(WsResponseCode.INVALID_RECOMMENDATION_CODE, "Invalid recommendation code " + request.getRecommendationIdentifier());
            } else {
                recommendation.setClosedBy(request.getRequestedBy());
                recommendation.setClosureReason(request.getReason());
                if (!updateActiveRecommendation(recommendation, RecommendationStatus.REJECTED.name())) {
                    context.addError(WsResponseCode.INVALID_RECOMMENDATION_STATUS, "Recommendation is no more active");
                    response.setMessage("Status is " + recommendation.getRecommendationStatus());
                }
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        response.setSuccessful(!context.hasErrors());
        return response;
    }

    private boolean isRegisteredToRecommend(String recommendationType, String source, ValidationContext context) {
        List<RecommendationProviderDTO> recommendationProviders = recommendationSubscriptionService.getRecommendationProviders(
                new GetRecommendationProvidersRequest(recommendationType, source)).getRecommendationProviders();
        if (recommendationProviders.isEmpty()) {
            context.addError(WsResponseCode.INVALID_RECOMMENDATION_PROVIDER, "Not added to recommendors list for " + recommendationType);
        }
        return !context.hasErrors();
    }

    private void submitRecommendationToRepair(String recommendationIdentifier, String oldStatus, String newStatus, String reason) {
        List<RepairRecommendationJob> recommendationRepairJobs = new ArrayList<>();
        RepairRecommendationJob repairJob = new RepairRecommendationJob(recommendationIdentifier, oldStatus, newStatus, reason);
        recommendationRepairJobs.add(repairJob);
        executorFactory.getExecutor(SERVICE_NAME).submit(new RepairRecommendationHandler(recommendationRepairJobs, recommendationMao));
    }

    private PriceRecommendationVO getRecommendationByIdentifier(String identifier) {
        return recommendationMao.getRecommendationByIdentifier(identifier);
    }

    private boolean updateActiveRecommendation(PriceRecommendationVO recommendation, String recommendationStatus) {
        return recommendationMao.updateActiveRecommendation(recommendation, recommendationStatus);
    }

    private List<PriceRecommendationVO> getRecommendationsByIdentifiers(List<String> identifiers) {
        return recommendationMao.getRecommendationsByIdentifiers(identifiers);
    }

    private void createRecommendation(PriceRecommendationVO recommendation) {
        recommendationMao.create(recommendation);
    }

    private int getActiveRecommendationsCount() {
        return recommendationMao.getActiveRecommendationsCount();
    }

    private boolean updateRecommendation(String recommendationIdentifier, String recommendationStatus, String oldRecommendationStatus, String reason) {
        return recommendationMao.updateRecommendation(recommendationIdentifier, recommendationStatus, oldRecommendationStatus, reason);
    }

    private List<PriceRecommendationVO> getFilteredRecommendations(String channelCode, String channelProductId, String recommendationStatus, String recommendationProvider,
            DateRange createdDateRange, DateRange expiryTime, int start, int noOfResults) {
        return recommendationMao.getFilteredRecommendations(channelCode, channelProductId, recommendationStatus, recommendationProvider, createdDateRange, expiryTime, start,
                noOfResults);
    }

    private void publishRecommendation(String recommendationType, int count) {
        RecommendationMessage recommendationMessage = new RecommendationMessage();
        recommendationMessage.setType(recommendationType);
        recommendationMessage.setNumber(String.valueOf(getActiveRecommendationsCount()));
        recommendationMessage.setTenantCode(UserContext.current().getTenant().getCode());
        LOG.info("Adding recommendation {} to recommendation message queue.", recommendationMessage);
        activeMQConnector.produceMessage(MessagingConstants.Queue.RECOMMENDATION_MESSAGE_QUEUE, recommendationMessage);
    }
}
