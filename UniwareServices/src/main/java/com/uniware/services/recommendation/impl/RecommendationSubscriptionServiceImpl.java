/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Nov 25, 2015
 *  @author akshay
 */
package com.uniware.services.recommendation.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.utils.DateUtils;
import com.uniware.core.api.recommendation.GetRecommendationProvidersRequest;
import com.uniware.core.api.recommendation.GetRecommendationProvidersResponse;
import com.uniware.core.api.recommendation.GetRecommendationTypesRequest;
import com.uniware.core.api.recommendation.GetRecommendationTypesResponse;
import com.uniware.core.api.recommendation.GetSubscribedRecommendationProvidersRequest;
import com.uniware.core.api.recommendation.GetSubscribedRecommendationProvidersResponse;
import com.uniware.core.api.recommendation.SubscribeRecommendationsRequest;
import com.uniware.core.api.recommendation.SubscriptionRecommendationsResponse;
import com.uniware.core.api.recommendation.UnsubscribeRecommendationRequest;
import com.uniware.core.api.recommendation.UnsubscribeRecommendationResponse;
import com.uniware.core.api.recommendation.dto.RecommendationProviderDTO;
import com.uniware.core.api.recommendation.dto.RecommendationSubscriptionDTO;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.vo.RecommendationProviderVO;
import com.uniware.core.vo.RecommendationSubscriptionVO;
import com.uniware.core.vo.RecommendationTypeVO;
import com.uniware.mao.recommendation.IRecommendationMao;
import com.uniware.services.recommendation.IRecommendationSubscriptionService;

/**
 * An implementation to manage subscriptions
 */
@Service("recommendationSubscriptionService")
public class RecommendationSubscriptionServiceImpl implements IRecommendationSubscriptionService {
    
    @Autowired
    private IRecommendationMao recommendationMao;

    @Override
    public GetRecommendationTypesResponse getRecommendationTypes(final GetRecommendationTypesRequest request) {
        GetRecommendationTypesResponse response = new GetRecommendationTypesResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            List<RecommendationTypeVO> recommendationTypeVOs = getRecommendationTypes();
            if (!recommendationTypeVOs.isEmpty()) {
                List<String> recommendationTypes = new ArrayList<>(recommendationTypeVOs.size());
                for (RecommendationTypeVO recommendationTypeVO : recommendationTypeVOs) {
                    recommendationTypes.add(recommendationTypeVO.getType());
                }
                response.setRecommendationTypes(recommendationTypes);
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        response.setSuccessful(!context.hasErrors());
        return response;
    }

    @Override
    public GetRecommendationProvidersResponse getRecommendationProviders(final GetRecommendationProvidersRequest request) {
        GetRecommendationProvidersResponse response = new GetRecommendationProvidersResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            List<RecommendationProviderVO> recommendationProviders = getRecommendationProvidersByProviderFilters(request.getRecommendationType(), 
                    request.getProvider());
            if (!recommendationProviders.isEmpty()) {
                List<RecommendationProviderDTO> rpDTOs = new ArrayList<>(recommendationProviders.size());
                for (RecommendationProviderVO recommendationProviderVO : recommendationProviders) {
                    rpDTOs.add(new RecommendationProviderDTO(recommendationProviderVO));
                }
                response.setRecommendationProviders(rpDTOs);
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        response.setSuccessful(!context.hasErrors());
        return response;
    }

    @Override
    public GetSubscribedRecommendationProvidersResponse getSubscribedRecommendationProviders(final GetSubscribedRecommendationProvidersRequest request) {
        GetSubscribedRecommendationProvidersResponse response = new GetSubscribedRecommendationProvidersResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            List<RecommendationSubscriptionVO> recommendationSubscriptions = getFilteredRecommendationSubscriptions(request.getRecommendationProvider());
            if (!recommendationSubscriptions.isEmpty()) {
                List<RecommendationSubscriptionDTO> subscriptiions = new ArrayList<>(recommendationSubscriptions.size());
                for (RecommendationSubscriptionVO recommendationSubscription : recommendationSubscriptions) {
                    RecommendationSubscriptionDTO rsDTO = new RecommendationSubscriptionDTO(recommendationSubscription);
                    subscriptiions.add(rsDTO);
                }
                response.setSubscriptions(subscriptiions);
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        response.setSuccessful(!context.hasErrors());
        return response;
    }
    
    @Override
    public SubscriptionRecommendationsResponse addSubscription(final SubscribeRecommendationsRequest request) {
        SubscriptionRecommendationsResponse response = new SubscriptionRecommendationsResponse();
        ValidationContext context = request.validate();
        List<RecommendationProviderVO> recommendationProviders = getRecommendationProvidersByProviderFilters(request.getRecommendationType(), request.getRecommendationProvider());
        if (recommendationProviders.isEmpty()) {
            context.addError(WsResponseCode.INVALID_RECOMMENDATION_TYPE, "No such provider exists");
        }
        if (!context.hasErrors()) {
            RecommendationSubscriptionVO subscription = new RecommendationSubscriptionVO();
            subscription.setRecommendationType(request.getRecommendationType());
            subscription.setRecommendationProvider(request.getRecommendationProvider());
            subscription.setCreated(DateUtils.getCurrentTime());
            addSubscription(subscription);
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        response.setSuccessful(!context.hasErrors());
        return response;
    }
    
    @Override
    public UnsubscribeRecommendationResponse removeSubscription(final UnsubscribeRecommendationRequest request) {
        UnsubscribeRecommendationResponse response = new UnsubscribeRecommendationResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            removeSubscription(request.getRecommendationType(), request.getRecommendationProvider());
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        response.setSuccessful(!context.hasErrors());
        return response;
    }

    private List<RecommendationTypeVO> getRecommendationTypes() {
        return recommendationMao.getRecommendationTypes();
    }
    
    private List<RecommendationProviderVO> getRecommendationProvidersByProviderFilters(String recommendationType, String provider) {
        return recommendationMao.getRecommendationProvidersByProviderFilters(recommendationType, provider);
    }
    
    private List<RecommendationSubscriptionVO> getRecommendationSubscriptions() {
        return recommendationMao.getRecommendationSubscriptions();
    }
    
    private List<RecommendationSubscriptionVO> getFilteredRecommendationSubscriptions(String recommendationProvider) {
        return recommendationMao.getFilteredRecommendationSubscriptions(recommendationProvider);
    }
    
    private void addSubscription(RecommendationSubscriptionVO subscription) {
        recommendationMao.addSubscription(subscription);
    }
    
    private void removeSubscription(String recommendationType, String recommendationProvider) {
        recommendationMao.removeSubscription(recommendationType, recommendationProvider);
    }
}
