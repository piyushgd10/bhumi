/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Nov 25, 2015
 *  @author akshay
 */
package com.uniware.services.recommendation.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.uniware.mao.recommendation.IRecommendationMao;

public class RepairRecommendationHandler implements Runnable {

    private static final Logger           LOG        = LoggerFactory.getLogger(RepairRecommendationHandler.class);

    private IRecommendationMao            recommendationStore;

    private List<RepairRecommendationJob> repairJobs = new ArrayList<>();

    public RepairRecommendationHandler(List<RepairRecommendationJob> repairJobs, IRecommendationMao recommendationStore) {
        this.recommendationStore = recommendationStore;
        this.repairJobs = repairJobs;
    }

    @Override
    public void run() {
        for (RepairRecommendationJob repairJob : repairJobs) {
            LOG.info("Repairing dirty recommendation {} from {} to {}",
                    new Object[] { repairJob.getRecommendationIdentifier(), repairJob.getOldStatus(), repairJob.getNewStatus() });
            boolean successfull = recommendationStore.updateRecommendation(repairJob.getRecommendationIdentifier(), repairJob.getNewStatus(), repairJob.getOldStatus(),
                    repairJob.getReason());
            LOG.info("Update result recommendation : {} successfull : {}", repairJob.getRecommendationIdentifier(), successfull);
        }
    }

    public List<RepairRecommendationJob> getRepairJobs() {
        return repairJobs;
    }

    public void setRepairJobs(List<RepairRecommendationJob> repairJobs) {
        this.repairJobs = repairJobs;
    }

    public static class RepairRecommendationJob {

        private String recommendationIdentifier;
        private String oldStatus;
        private String newStatus;
        private String reason;

        public RepairRecommendationJob() {
        }

        public RepairRecommendationJob(String recommendationIdentifier, String oldStatus, String newStatus, String reason) {
            this.recommendationIdentifier = recommendationIdentifier;
            this.oldStatus = oldStatus;
            this.newStatus = newStatus;
            this.reason = reason;
        }

        public String getRecommendationIdentifier() {
            return recommendationIdentifier;
        }

        public void setRecommendationIdentifier(String recommendationIdentifier) {
            this.recommendationIdentifier = recommendationIdentifier;
        }

        public String getOldStatus() {
            return oldStatus;
        }

        public void setOldStatus(String oldStatus) {
            this.oldStatus = oldStatus;
        }

        public String getNewStatus() {
            return newStatus;
        }

        public void setNewStatus(String newStatus) {
            this.newStatus = newStatus;
        }

        public String getReason() {
            return reason;
        }

        public void setReason(String reason) {
            this.reason = reason;
        }
    }
}
