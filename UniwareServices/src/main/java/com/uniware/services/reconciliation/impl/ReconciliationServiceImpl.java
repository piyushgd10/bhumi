/*
 *  Copyright 2013 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 07-Dec-2013
 *  @author parijat
 */
package com.uniware.services.reconciliation.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.api.validation.WsError;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.entity.SaleOrderReconciliation;
import com.unifier.core.entity.SaleOrderReconciliation.ReconciliationStatus;
import com.unifier.core.utils.BatchProcessor;
import com.unifier.core.utils.CollectionUtils;
import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.StringUtils;
import com.uniware.core.api.channel.CreateChannelReconciliationInvoiceRequest;
import com.uniware.core.api.channel.CreateChannelReconciliationInvoiceResponse;
import com.uniware.core.api.channel.WsChannelReconciliationInvoice;
import com.uniware.core.api.channel.WsChannelReconciliationInvoiceItem;
import com.uniware.core.api.reconciliation.CreateChannelPaymentReconciliationRequest;
import com.uniware.core.api.reconciliation.CreateChannelPaymentReconciliationResponse;
import com.uniware.core.api.reconciliation.CreateUnreconciledSaleOrderRequest;
import com.uniware.core.api.reconciliation.CreateUnreconciledSaleOrderResponse;
import com.uniware.core.api.reconciliation.GetChannelReconciliationInvoiceItemsRequest;
import com.uniware.core.api.reconciliation.GetChannelReconciliationInvoiceItemsResponse;
import com.uniware.core.api.reconciliation.GetChannelReconciliationMetadataRequest;
import com.uniware.core.api.reconciliation.GetReconciliationInvoiceDetailRequest;
import com.uniware.core.api.reconciliation.GetReconciliationInvoiceDetailResponse;
import com.uniware.core.api.reconciliation.GetSaleOrderReconciliationItemRequest;
import com.uniware.core.api.reconciliation.GetSaleOrderReconciliationItemResponse;
import com.uniware.core.api.reconciliation.MarkChannelPaymentsDisputedRequest;
import com.uniware.core.api.reconciliation.MarkChannelPaymentsDisputedResponse;
import com.uniware.core.api.reconciliation.MarkChannelPaymentsReconciledResponse;
import com.uniware.core.api.reconciliation.MarkChannelPaymentsReconciliedRequest;
import com.uniware.core.api.reconciliation.PopulateInvoiceCodesForSaleOrderReconciliationRequest;
import com.uniware.core.api.reconciliation.PopulateInvoiceCodesForSaleOrderReconciliationResponse;
import com.uniware.core.api.reconciliation.WSChannelReconciliationIdentifier;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.entity.Channel;
import com.uniware.core.entity.SaleOrder;
import com.uniware.core.entity.SaleOrderItem;
import com.uniware.core.locking.ILockingService;
import com.uniware.core.locking.Namespace;
import com.uniware.core.locking.annotation.Lock;
import com.uniware.core.locking.annotation.Locks;
import com.uniware.core.utils.UserContext;
import com.uniware.core.vo.ChannelReconciliationInvoice;
import com.uniware.core.vo.ChannelReconciliationInvoiceItem;
import com.uniware.dao.reconciliation.IReconciliationMao;
import com.uniware.services.cache.ChannelCache;
import com.uniware.services.channel.IChannelService;
import com.uniware.services.reconciliation.IReconciliationService;
import com.uniware.services.saleorder.ISaleOrderService;

/**
 * @author parijat
 */
@Service
public class ReconciliationServiceImpl implements IReconciliationService {

    private static final Logger LOG = LoggerFactory.getLogger(ReconciliationServiceImpl.class);

    @Autowired
    private IReconciliationMao  reconciliationMao;

    @Autowired
    private ISaleOrderService   saleOrderService;

    @Autowired
    private ILockingService     lockingService;

    @Autowired
    private IChannelService     channelService;

    @Override
    @Transactional
    @Locks({ @Lock(ns = Namespace.SALE_ORDER, key = "#{#args[0].saleOrderCode}") })
    public CreateUnreconciledSaleOrderResponse createUnreconciliedSaleOrder(CreateUnreconciledSaleOrderRequest request) {
        CreateUnreconciledSaleOrderResponse response = new CreateUnreconciledSaleOrderResponse();
        ValidationContext context = request.validate();
        SaleOrder saleOrder = null;
        if (!context.hasErrors()) {
            saleOrder = saleOrderService.getSaleOrderForUpdate(request.getSaleOrderCode());
            Channel channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelById(saleOrder.getChannel().getId());
            Map<String, List<SaleOrderItem>> reconciliationIdentifierToSaleOrderItems = new HashMap<>(saleOrder.getSaleOrderItems().size());
            for (SaleOrderItem soi : saleOrder.getSaleOrderItems()) {
                List<SaleOrderItem> saleOrderItems = reconciliationIdentifierToSaleOrderItems.get(soi.getChannelSaleOrderItemCode());
                if (saleOrderItems == null) {
                    saleOrderItems = new ArrayList<>();
                    reconciliationIdentifierToSaleOrderItems.put(soi.getChannelSaleOrderItemCode(), saleOrderItems);
                }
                saleOrderItems.add(soi);
            }

            for (Map.Entry<String, List<SaleOrderItem>> e : reconciliationIdentifierToSaleOrderItems.entrySet()) {
                String reconciliationIdentifier = e.getKey();
                String invoiceCode = null, invoiceDisplayCode = null;
                SaleOrderReconciliation saleOrderReconciliation = getOrderReconciliationByIdentifier(reconciliationIdentifier, channel.getCode());
                if (saleOrderReconciliation == null) {
                    BigDecimal sellingPrice = BigDecimal.ZERO, shippingCharges = BigDecimal.ZERO, itemCostPrice = BigDecimal.ZERO;
                    int quantity = 0;
                    saleOrderReconciliation = new SaleOrderReconciliation();
                    saleOrderReconciliation.setChannelCode(channel.getCode());
                    saleOrderReconciliation.setStatusCode(ReconciliationStatus.AWAITING_PAYMENT.name());
                    saleOrderReconciliation.setReconciliationIdentifier(e.getKey());
                    saleOrderReconciliation.setChannelSaleOrderCode(saleOrder.getDisplayOrderCode());
                    saleOrderReconciliation.setSaleOrderCode(saleOrder.getCode());
                    saleOrderReconciliation.setCurrencyCode(saleOrder.getCurrencyCode());
                    saleOrderReconciliation.setOrderTransactionDate(saleOrder.getDisplayOrderDateTime());
                    saleOrderReconciliation.setCreated(DateUtils.getCurrentTime());
                    for (SaleOrderItem saleOrderItem : e.getValue()) {
                        if(saleOrderItem.getShippingPackage() != null && saleOrderItem.getShippingPackage().getInvoice() != null) {
                            invoiceCode = saleOrderItem.getShippingPackage().getInvoice().getCode();
                            invoiceDisplayCode = saleOrderItem.getShippingPackage().getInvoice().getDisplayCode();
                        }
                        saleOrderReconciliation.setChannelProductId(saleOrderItem.getChannelProductId());
                        saleOrderReconciliation.setChannelProductName(saleOrderItem.getChannelProductName());
                        if (saleOrderItem.getItemType().getCostPrice() != null) {
                            itemCostPrice = itemCostPrice.add(saleOrderItem.getItemType().getCostPrice());
                        }
                        sellingPrice = sellingPrice.add(saleOrderItem.getSellingPrice()).setScale(2, BigDecimal.ROUND_CEILING);
                        shippingCharges = shippingCharges.add(saleOrderItem.getShippingCharges()).setScale(2, BigDecimal.ROUND_CEILING);
                        quantity = quantity + 1;
                    }
                    saleOrderReconciliation.setInvoicingStatus(SaleOrderReconciliation.InvoicingStatus.AWAITING);
                    if (invoiceCode != null) {
                        saleOrderReconciliation.setInvoiceCode(invoiceCode);
                        saleOrderReconciliation.setInvoiceDisplayCode(invoiceDisplayCode);
                        saleOrderReconciliation.setInvoicingStatus(SaleOrderReconciliation.InvoicingStatus.DONE);
                    } else if (saleOrder.getStatusCode().equals(SaleOrder.StatusCode.CANCELLED.name())) {
                        saleOrderReconciliation.setInvoicingStatus(SaleOrderReconciliation.InvoicingStatus.CANCELLED);
                    }
                    saleOrderReconciliation.setItemCostPrice(itemCostPrice.doubleValue());
                    saleOrderReconciliation.setSellingPrice(sellingPrice.doubleValue());
                    saleOrderReconciliation.setShippingCharges(shippingCharges.doubleValue());
                    saleOrderReconciliation.setQuantity(quantity);
                }

                List<ChannelReconciliationInvoiceItem> invoiceItems = reconciliationMao.getUnprocessedReconciliationInvoiceItemByReconciliationIdentifier(
                        saleOrderReconciliation.getReconciliationIdentifier(), saleOrderReconciliation.getChannelCode());
                if (!CollectionUtils.isEmpty(invoiceItems)) {
                    saleOrderReconciliation.setStatusCode(ReconciliationStatus.UNRECONCILED.name());
                    saleOrderReconciliation.setLastSettledDate(DateUtils.getCurrentTime());
                    for (ChannelReconciliationInvoiceItem invoiceItemVO : invoiceItems) {
                        saleOrderReconciliation.setTotalReconcileAmount(
                                new BigDecimal(invoiceItemVO.getSettledValue() + saleOrderReconciliation.getTotalReconcileAmount()).setScale(2,
                                        RoundingMode.CEILING).doubleValue());
                        invoiceItemVO.setStatusCode(ChannelReconciliationInvoiceItem.InvoiceItemProcessedStatus.PROCESSED.name());
                        reconciliationMao.createOrUpdateInvoiceItem(invoiceItemVO);
                    }
                }
                saleOrderReconciliation.setSettlementPercentage(prepareSettlementPercentage(saleOrderReconciliation));
                reconciliationMao.update(saleOrderReconciliation);
            }
            saleOrder.setReconciliationPending(false);
        }
        if (!context.hasErrors()) {
            response.setSuccessful(true);
        } else {
            response.setErrors(context.getErrors());
            LOG.error("Error while creating unreconciled sale order for {}. Error- {}", saleOrder.getCode(), context.getErrors().toString());
        }
        return response;
    }

    @Override
    public CreateChannelReconciliationInvoiceResponse createChannelReconciliationInvoice(CreateChannelReconciliationInvoiceRequest request) {
        CreateChannelReconciliationInvoiceResponse response = new CreateChannelReconciliationInvoiceResponse();
        ValidationContext context = request.validate();
        Channel channel = null;
        Integer successfullyProcessed = 0;
        if (!context.hasErrors()) {
            channel = channelService.getChannelByCode(request.getChannelReconciliationInvoice().getChannelCode());
            if (channel == null) {
                context.addError(WsResponseCode.INVALID_CHANNEL_CODE,
                        WsResponseCode.INVALID_CHANNEL_CODE.message() + ": " + request.getChannelReconciliationInvoice().getChannelCode());
            }
        }
        if (!context.hasErrors()) {
            ChannelReconciliationInvoice invoice = getReconciliationInvoiceByCode(request.getChannelReconciliationInvoice().getInvoiceCode(),
                    request.getChannelReconciliationInvoice().getChannelCode());
            if (invoice == null) {
                invoice = prepareInvoice(request.getChannelReconciliationInvoice());
                invoice = reconciliationMao.createOrUpdate(invoice);
            }
            for (WsChannelReconciliationInvoiceItem wsInvoiceItem : request.getChannelReconciliationInvoice().getInvoiceItems()) {
                ChannelReconciliationInvoiceItem invoiceItem = reconciliationMao.getReconciliationInvoiceItemByIdentifier(wsInvoiceItem.getReconciliationIdentifier(),
                        channel.getCode(), invoice.getInvoiceCode(), wsInvoiceItem.getInvoiceItemOrderStatus());
                if (invoiceItem == null) {
                    invoiceItem = prepareInvoiceItem(wsInvoiceItem, invoice, channel.getCode());
                    reconciliationMao.createOrUpdateInvoiceItem(invoiceItem);
                    successfullyProcessed = doProcessUnreconciledOrder(invoiceItem, channel, successfullyProcessed);
                } else {
                    if (ChannelReconciliationInvoiceItem.InvoiceItemProcessedStatus.UNPROCESSED.name().equals(invoiceItem.getStatusCode())) {
                        successfullyProcessed = doProcessUnreconciledOrder(invoiceItem, channel, successfullyProcessed);
                    }
                    LOG.info("Skipping invoice item {} of invoice {} as it is already present", wsInvoiceItem.getReconciliationIdentifier(), invoice.getInvoiceCode());
                }
            }
            LOG.info("Successfully processed {} order for invoice {}", successfullyProcessed, invoice.getInvoiceCode());
            response.setSuccessful(true);
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    public MarkChannelPaymentsReconciledResponse markChannelPaymentsReconcilied(MarkChannelPaymentsReconciliedRequest request) {
        MarkChannelPaymentsReconciledResponse response = new MarkChannelPaymentsReconciledResponse();
        ValidationContext context = request.validate();
        List<String> invalidReconciledOrderCodes = new ArrayList<String>();
        if (!context.hasErrors()) {
            for (WSChannelReconciliationIdentifier wsChannelReconciliationIdentifier : request.getChannelRecoIdentifiers()) {
                Channel channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelByCode(wsChannelReconciliationIdentifier.getChannelCode());
                if (channel == null) {
                    invalidReconciledOrderCodes.add(wsChannelReconciliationIdentifier.getReconciliationIdentifier() + "- CHANNEL_NOT_FOUND");
                    continue;
                }
                SaleOrderReconciliation saleOrderReconciliation = reconciliationMao.getSaleOrderReconciliationByIdentifier(
                        wsChannelReconciliationIdentifier.getReconciliationIdentifier(), wsChannelReconciliationIdentifier.getChannelCode());
                if (saleOrderReconciliation == null || ReconciliationStatus.RECONCILED.name().equals(saleOrderReconciliation.getStatusCode())) {
                    invalidReconciledOrderCodes.add(wsChannelReconciliationIdentifier.getReconciliationIdentifier() + "- INVALID_IDENTIFIER/ INVALID_STATUS");
                    continue;
                }
                List<SaleOrder> saleOrders = saleOrderService.getSaleOrderByDisplayOrderCode(saleOrderReconciliation.getChannelSaleOrderCode(), channel.getId());
                if (CollectionUtils.isEmpty(saleOrders)) {
                    invalidReconciledOrderCodes.add(wsChannelReconciliationIdentifier.getReconciliationIdentifier() + "- NO ORDER FOUND. Discarding...");
                    doMarkSaleOrderReconciliationDiscarded(saleOrderReconciliation);
                    continue;
                }
                doMarkChannelPaymentsReconciled(saleOrderReconciliation, saleOrders, request.getTotalReconcileAmount());
            }
            if (invalidReconciledOrderCodes.isEmpty()) {
                response.setSuccessful(true);
            } else {
                response.addErrors(Collections.singletonList(new WsError(WsResponseCode.INVALID_REQUEST.code(), "INVALID_REQUEST", StringUtils.join(invalidReconciledOrderCodes))));
            }
        }
        response.addErrors(context.getErrors());
        return response;
    }

    @Locks({ @Lock(ns = Namespace.SALE_ORDER, key = "#{#args[0].channelSaleOrderCode}") })
    private void doMarkSaleOrderReconciliationDiscarded(SaleOrderReconciliation saleOrderReconciliation) {
        saleOrderReconciliation.setStatusCode(ReconciliationStatus.DISCARDED.name());
        saleOrderReconciliation.setComments("Sale order not found");
        reconciliationMao.update(saleOrderReconciliation);
    }

    @Locks({ @Lock(ns = Namespace.SALE_ORDER, key = "#{#args[0].channelSaleOrderCode}") })
    @Transactional
    private void doMarkChannelPaymentsReconciled(SaleOrderReconciliation saleOrderReconciliation, List<SaleOrder> saleOrders, String totalSettlement) {
        for (SaleOrder saleOrder : saleOrders) {
            saleOrder.setReconciliationStatus(SaleOrder.ReconciliationStatus.RECONCILED.name());
            saleOrder.setReconciliationPending(false);
            saleOrderService.updateSaleOrder(saleOrder);
        }
        if (StringUtils.isNotBlank(totalSettlement)) {
            saleOrderReconciliation.setTotalReconcileAmount(new BigDecimal(totalSettlement).doubleValue() + saleOrderReconciliation.getTotalReconcileAmount());
        }
        saleOrderReconciliation.setSettlementPercentage(prepareSettlementPercentage(saleOrderReconciliation));
        saleOrderReconciliation.setReconciledBy(UserContext.current().getUniwareUserName());
        saleOrderReconciliation.setStatusCode(ReconciliationStatus.RECONCILED.name());
        reconciliationMao.update(saleOrderReconciliation);
    }

    @Override
    public MarkChannelPaymentsDisputedResponse markChannelPaymentsDisputed(MarkChannelPaymentsDisputedRequest request) {
        MarkChannelPaymentsDisputedResponse response = new MarkChannelPaymentsDisputedResponse();
        ValidationContext context = request.validate();
        List<String> invalidReconciledOrderCodes = new ArrayList<>();
        if (!context.hasErrors()) {
            for (WSChannelReconciliationIdentifier wsChannelReconciliationIdentifier : request.getChannelRecoIdentifiers()) {
                Channel channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelByCode(wsChannelReconciliationIdentifier.getChannelCode());
                if (channel == null) {
                    invalidReconciledOrderCodes.add(wsChannelReconciliationIdentifier.getReconciliationIdentifier() + "- CHANNEL_NOT_FOUND");
                    continue;
                }
                SaleOrderReconciliation saleOrderReconciliation = reconciliationMao.getSaleOrderReconciliationByIdentifier(
                        wsChannelReconciliationIdentifier.getReconciliationIdentifier(), wsChannelReconciliationIdentifier.getChannelCode());
                if (saleOrderReconciliation == null) {
                    invalidReconciledOrderCodes.add(wsChannelReconciliationIdentifier.getReconciliationIdentifier() + "- INVALID_IDENTIFIER");
                    continue;
                }
                List<SaleOrder> saleOrders = saleOrderService.getSaleOrderByDisplayOrderCode(saleOrderReconciliation.getChannelSaleOrderCode(), channel.getId());
                if (CollectionUtils.isEmpty(saleOrders)) {
                    invalidReconciledOrderCodes.add(wsChannelReconciliationIdentifier.getReconciliationIdentifier() + "- NO ORDER FOUND. Discarding...");
                    doMarkSaleOrderReconciliationDiscarded(saleOrderReconciliation);
                    continue;
                }
                doMarkChannelPaymentsDisputed(saleOrderReconciliation, saleOrders, wsChannelReconciliationIdentifier);
            }
            if (invalidReconciledOrderCodes.isEmpty()) {
                response.setSuccessful(true);
            } else {
                response.addErrors(Collections.singletonList(new WsError(WsResponseCode.INVALID_REQUEST.code(), "INVALID_REQUEST", StringUtils.join(invalidReconciledOrderCodes))));
            }
        }
        if (!context.hasErrors()) {
            response.setSuccessful(true);
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    public GetSaleOrderReconciliationItemResponse getSaleOrderReconciliationItems(GetSaleOrderReconciliationItemRequest request) {
        GetSaleOrderReconciliationItemResponse response = new GetSaleOrderReconciliationItemResponse();
        ValidationContext context = request.validate();
        SaleOrderReconciliation saleOrderReconciliation = null;
        Channel channel = null;
        List<SaleOrderItem> saleOrderItems = new ArrayList<>();
        if (!context.hasErrors()) {
            channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelByCode(request.getChannelCode());
            if (channel == null) {
                context.addError(WsResponseCode.INVALID_REQUEST, "channel [" + request.getChannelCode() + "] does not exist.");
            } else {
                saleOrderReconciliation = reconciliationMao.getSaleOrderReconciliationByIdentifier(request.getReconciliationIdentifier(), channel.getCode());
                if (saleOrderReconciliation == null) {
                    context.addError(WsResponseCode.INVALID_REQUEST, "reconciliation order [" + request.getReconciliationIdentifier() + "] does not exist.");
                } else {
                    saleOrderItems = saleOrderService.getSaleOrderItemsByChannelOrderAndItemCode(saleOrderReconciliation.getChannelSaleOrderCode(),
                            saleOrderReconciliation.getReconciliationIdentifier(), channel.getId());
                    if (CollectionUtils.isEmpty(saleOrderItems)) {
                        context.addError(WsResponseCode.INVALID_REQUEST,
                                "Sale Order Item for channelSaleOrderCode [" + saleOrderReconciliation.getChannelSaleOrderCode() + "] does not exist.");
                    }
                }
            }
        }
        if (!context.hasErrors()) {
            for (SaleOrderItem saleOrderItem : saleOrderItems) {
                GetSaleOrderReconciliationItemResponse.SaleOrderReconciliationItemDTO itemDTO = new GetSaleOrderReconciliationItemResponse.SaleOrderReconciliationItemDTO();
                itemDTO.setSaleOrderItemCode(saleOrderItem.getCode());
                itemDTO.setStatusCode(saleOrderItem.getStatusCode());
                itemDTO.setCashOnDeliveryCharges(saleOrderItem.getCashOnDeliveryCharges());
                itemDTO.setSellingPrice(saleOrderItem.getSellingPrice());
                itemDTO.setDiscount(saleOrderItem.getDiscount());
                itemDTO.setTotalPrice(saleOrderItem.getTotalPrice());
                itemDTO.setDropshipCommission(saleOrderItem.getDropshipCommission());
                itemDTO.setGiftWrapCharges(saleOrderItem.getGiftWrapCharges());
                itemDTO.setDropshipShippingCharges(saleOrderItem.getDropshipShippingCharges());
                itemDTO.setPrepaidAmount(saleOrderItem.getPrepaidAmount());
                itemDTO.setShippingCharges(saleOrderItem.getShippingCharges());
                itemDTO.setShippingMethod(saleOrderItem.getShippingMethod().getShippingMethodCode());
                itemDTO.setShippingMethodCharges(saleOrderItem.getShippingMethodCharges());
                itemDTO.setStoreCredit(saleOrderItem.getStoreCredit());
                itemDTO.setVoucherValue(saleOrderItem.getVoucherValue());
                response.getSaleOrderReconciliationItems().add(itemDTO);
            }
        }
        response.setSuccessful(!context.hasErrors());
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    public PopulateInvoiceCodesForSaleOrderReconciliationResponse populateInvoiceCodesForSaleOrderReconciliation(PopulateInvoiceCodesForSaleOrderReconciliationRequest request) {
        PopulateInvoiceCodesForSaleOrderReconciliationResponse response = new PopulateInvoiceCodesForSaleOrderReconciliationResponse();
        ValidationContext context = request.validate();
        BatchProcessor<SaleOrderReconciliation> batchProcessor = new BatchProcessor<SaleOrderReconciliation>(50) {
            @Override
            protected void process(List<SaleOrderReconciliation> saleOrderReconciliations, int batchIndex) {
                for (SaleOrderReconciliation saleOrderReconciliation : saleOrderReconciliations) {
                    String invoiceCode = null, invoiceDisplayCode = null;
                    SaleOrder saleOrder = saleOrderService.getSaleOrderByCode(saleOrderReconciliation.getSaleOrderCode());
                    if (saleOrder == null) {
                        LOG.info("SaleOrder Not Found for orderCode: {}", saleOrderReconciliation.getSaleOrderCode());
                        continue;
                    }
                    for (SaleOrderItem saleOrderItem : saleOrder.getSaleOrderItems()) {
                        if (saleOrderItem.getChannelSaleOrderItemCode().equals(saleOrderReconciliation.getReconciliationIdentifier()) && invoiceCode == null) {
                            if(saleOrderItem.getShippingPackage() != null && saleOrderItem.getShippingPackage().getInvoice() != null) {
                                invoiceCode = saleOrderItem.getShippingPackage().getInvoice().getCode();
                                invoiceDisplayCode = saleOrderItem.getShippingPackage().getInvoice().getDisplayCode();
                            }
                        }
                    }
                    if (invoiceCode != null) {
                        saleOrderReconciliation.setInvoicingStatus(SaleOrderReconciliation.InvoicingStatus.DONE);
                        saleOrderReconciliation.setInvoiceCode(invoiceCode);
                        saleOrderReconciliation.setInvoiceDisplayCode(invoiceDisplayCode);
                        LOG.info("Successfully added invoice for {} reconciliationIdentifier ", saleOrderReconciliation.getReconciliationIdentifier());
                    } else if (saleOrder.getStatusCode().equals(SaleOrder.StatusCode.CANCELLED.name())) {
                        saleOrderReconciliation.setInvoicingStatus(SaleOrderReconciliation.InvoicingStatus.CANCELLED);
                    }
                    reconciliationMao.update(saleOrderReconciliation);
                }
            }

            @Override
            protected List<SaleOrderReconciliation> next(int start, int batchSize) {
                LOG.info("Fetching reconciliation sale orders, page: {}", start / batchSize);
                return reconciliationMao.getSaleOrderReconciliationsWithInvoiceMissing(request.getNumberOfDays(), start, batchSize);
            }
        };
        batchProcessor.process();
        if (!context.hasErrors()) {
            response.setSuccessful(true);
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Locks({ @Lock(ns = Namespace.SALE_ORDER, key = "#{#args[0].channelSaleOrderCode}") })
    @Transactional
    private void doMarkChannelPaymentsDisputed(SaleOrderReconciliation saleOrderReconciliation, List<SaleOrder> saleOrders,
            WSChannelReconciliationIdentifier wsChannelReconciliationIdentifier) {
        for (SaleOrder saleOrder : saleOrders) {
            saleOrder.setReconciliationStatus(SaleOrder.ReconciliationStatus.DISPUTED.name());
            saleOrderService.updateSaleOrder(saleOrder);
        }
        saleOrderReconciliation.setStatusCode(ReconciliationStatus.DISPUTED.name());
        saleOrderReconciliation.setReconciledBy(UserContext.current().getUniwareUserName());
        saleOrderReconciliation.setComments(wsChannelReconciliationIdentifier.getComments());
        reconciliationMao.update(saleOrderReconciliation);
    }

    @Override
    public CreateChannelPaymentReconciliationResponse manualReconciledOrderItem(CreateChannelPaymentReconciliationRequest request) {
        CreateChannelPaymentReconciliationResponse response = new CreateChannelPaymentReconciliationResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            SaleOrderReconciliation orderItemReconciliation = getOrderReconciliationByIdentifier(request.getReconciliationIdentifier(), request.getInvoiceVO().getChannelCode());
            if (orderItemReconciliation == null) {
                context.addError(WsResponseCode.INVALID_SALE_ORDER_ITEM_STATE, "No Reconciliation Record found for [" + request.getReconciliationIdentifier() + "]");
                response.setSuccessful(false);
            } else {
                java.util.concurrent.locks.Lock lock = lockingService.getLock(Namespace.SALE_ORDER, orderItemReconciliation.getChannelSaleOrderCode());
                try {
                    lock.lock();
                    doManualReconcileOrderItem(request, response, context);
                } finally {
                    lock.unlock();
                }
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Transactional
    private void doManualReconcileOrderItem(CreateChannelPaymentReconciliationRequest request, CreateChannelPaymentReconciliationResponse response, ValidationContext context) {
        SaleOrderReconciliation orderItemReconciliation = getOrderReconciliationByIdentifier(request.getReconciliationIdentifier(), request.getInvoiceVO().getChannelCode());
        if (orderItemReconciliation.getStatusCode().equalsIgnoreCase(ReconciliationStatus.UNRECONCILED.name())) {
            orderItemReconciliation.setTotalReconcileAmount(request.getTotalSettlementValue());
            orderItemReconciliation.setStatusCode(ReconciliationStatus.RECONCILED.name());
            //TODO
            //            reconciliationDao.update(orderItemReconciliation);
            SaleOrder saleOrder = saleOrderService.getSaleOrderForUpdate(orderItemReconciliation.getChannelSaleOrderCode());
            saleOrder.setReconciliationStatus(SaleOrder.ReconciliationStatus.RECONCILED.name());
            saleOrderService.updateSaleOrder(saleOrder);
            response.setSuccessful(true);
        } else {
            context.addError(WsResponseCode.INVALID_RECONCILIATION_STATE, "Manual reconcile only allowed for pending payments.");
            response.setSuccessful(false);
        }
    }

    @Override
    @Transactional
    public ChannelReconciliationInvoice getReconciliationInvoiceByCode(String invoiceCode, String channelCode) {
        return reconciliationMao.getReconciliationInvoiceByCode(invoiceCode, channelCode);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.uniware.services.reconciliation.IReconciliationService#
     * getReconciliationInvoiceByCode(com.uniware.core.api.reconciliation.
     * GetReconciliationInvoiceDetailRequest)
     */
    @Override
    public GetReconciliationInvoiceDetailResponse getReconciliationInvoiceByCode(GetReconciliationInvoiceDetailRequest request) {
        GetReconciliationInvoiceDetailResponse response = new GetReconciliationInvoiceDetailResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            ChannelReconciliationInvoice invoiceVO = getReconciliationInvoiceByCode(request.getInvoiceCode(), request.getChannelCode());
            if (invoiceVO == null) {
                context.addError(WsResponseCode.INVALID_REQUEST, "Reconciliation invoice not found for invoice code - [" + request.getChannelCode() + "]");
            } else {
                response.setInvoiceVO(invoiceVO);
                response.setSuccessful(true);
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    private SaleOrderReconciliation getOrderReconciliationByIdentifier(String reconciliationIdentifier, String channelCode) {
        return reconciliationMao.getSaleOrderReconciliationByIdentifier(reconciliationIdentifier, channelCode);
    }

    private int doProcessUnreconciledOrder(ChannelReconciliationInvoiceItem invoiceItem, Channel channel, int successfullyProcessed) {
        SaleOrderReconciliation saleOrderReconciliation = reconciliationMao.getSaleOrderReconciliationByIdentifier(invoiceItem.getReconciliationIdentifier(), channel.getCode());
        List<SaleOrder> saleOrders = saleOrderService.getSaleOrderByDisplayOrderCode(invoiceItem.getChannelSaleOrderCode(), channel.getId());
        if (saleOrderReconciliation != null) {
            List<ChannelReconciliationInvoiceItem> invoiceItems = reconciliationMao.getUnprocessedReconciliationInvoiceItemByReconciliationIdentifier(
                    saleOrderReconciliation.getReconciliationIdentifier(), saleOrderReconciliation.getChannelCode());
            if (!CollectionUtils.isEmpty(invoiceItems)) {
                saleOrderReconciliation.setStatusCode(ReconciliationStatus.UNRECONCILED.name());
                saleOrderReconciliation.setLastSettledDate(DateUtils.getCurrentTime());
                for (ChannelReconciliationInvoiceItem invoiceItemVO : invoiceItems) {
                    saleOrderReconciliation.setTotalReconcileAmount(
                            new BigDecimal(invoiceItemVO.getSettledValue() + saleOrderReconciliation.getTotalReconcileAmount()).setScale(2, RoundingMode.CEILING).doubleValue());
                    invoiceItemVO.setStatusCode(ChannelReconciliationInvoiceItem.InvoiceItemProcessedStatus.PROCESSED.name());
                    reconciliationMao.createOrUpdateInvoiceItem(invoiceItemVO);
                }
            }
            saleOrderReconciliation.setSettlementPercentage(prepareSettlementPercentage(saleOrderReconciliation));
            reconciliationMao.update(saleOrderReconciliation);
            for (SaleOrder saleOrder : saleOrders) {
                saleOrder.setReconciliationStatus(SaleOrder.ReconciliationStatus.UNRECONCILED.name());
                saleOrderService.updateSaleOrder(saleOrder);
            }
            successfullyProcessed++;
        } else {
            if (!CollectionUtils.isEmpty(saleOrders)) {
                invoiceItem.setStatusCode(ChannelReconciliationInvoiceItem.InvoiceItemProcessedStatus.UNPROCESSED.name());
            } else {
                invoiceItem.setStatusCode(ChannelReconciliationInvoiceItem.InvoiceItemProcessedStatus.ORDER_NOT_FOUND.name());
            }
            reconciliationMao.createOrUpdateInvoiceItem(invoiceItem);
        }
        return successfullyProcessed;
    }

    private double prepareSettlementPercentage(SaleOrderReconciliation saleOrderReconciliation) {
        BigDecimal totalPrice = BigDecimal.ZERO;
        totalPrice = totalPrice.add(new BigDecimal(saleOrderReconciliation.getSellingPrice()).add((new BigDecimal(saleOrderReconciliation.getShippingCharges())))).setScale(2,
                BigDecimal.ROUND_CEILING);
        if (totalPrice.intValue() > 0) {
            return new BigDecimal((saleOrderReconciliation.getTotalReconcileAmount() / totalPrice.doubleValue()) * 100).setScale(2, RoundingMode.CEILING).doubleValue();
        } else {
            return 0;
        }
    }

    private ChannelReconciliationInvoice prepareInvoice(WsChannelReconciliationInvoice wsInvoice) {
        ChannelReconciliationInvoice invoice = new ChannelReconciliationInvoice();
        invoice.setChannelCode(wsInvoice.getChannelCode());
        invoice.setInvoiceCode(wsInvoice.getInvoiceCode());
        invoice.setDepositDate(DateUtils.stringToDate(wsInvoice.getDepositDate(), "dd/MM/yyyy"));
        invoice.setSettlementDate(wsInvoice.getSettlementDate());
        invoice.setDownloadUrl(wsInvoice.getDownloadUrl());
        invoice.setTotalSettlementValue(wsInvoice.getTotalSettlemenValue());
        invoice.setCreated(DateUtils.getCurrentTime());
        invoice.setUpdated(DateUtils.getCurrentTime());
        return invoice;
    }

    private ChannelReconciliationInvoiceItem prepareInvoiceItem(WsChannelReconciliationInvoiceItem wsInvoiceItem, ChannelReconciliationInvoice invoiceVO, String channelCode) {
        ChannelReconciliationInvoiceItem invoiceItem = new ChannelReconciliationInvoiceItem();
        invoiceItem.setReconciliationIdentifier(wsInvoiceItem.getReconciliationIdentifier());
        invoiceItem.setChannelSaleOrderCode(wsInvoiceItem.getChannelSaleOrderCode());
        invoiceItem.setOrderDate(DateUtils.stringToDate(wsInvoiceItem.getOrderDate(), "dd/MM/yyyy"));
        invoiceItem.setInvoiceItemOrderStatus(wsInvoiceItem.getInvoiceItemOrderStatus());
        invoiceItem.setOrderItemValue(wsInvoiceItem.getOrderItemValue());
        invoiceItem.setSettledValue(wsInvoiceItem.getSettledValue());
        invoiceItem.setDepositDate(invoiceVO.getSettlementDate() != null ? invoiceVO.getDepositDate() : DateUtils.getCurrentTime());
        invoiceItem.setStatusCode(ChannelReconciliationInvoiceItem.InvoiceItemProcessedStatus.UNPROCESSED.name());
        invoiceItem.setQuantity(wsInvoiceItem.getQuantity());
        invoiceItem.setTotalRewards(wsInvoiceItem.getTotalRewards());
        invoiceItem.setTotalDiscounts(wsInvoiceItem.getTotalDiscounts());
        invoiceItem.setTotalOtherIncentive(wsInvoiceItem.getTotalOtherIncentive());
        invoiceItem.setTotalSellerEarning(wsInvoiceItem.getTotalSellerEarning());
        invoiceItem.setTotalCommission(wsInvoiceItem.getTotalCommission());
        invoiceItem.setTotalFixedFee(wsInvoiceItem.getTotalFixedFee());
        invoiceItem.setTotalShippingCharge(wsInvoiceItem.getTotalShippingCharge());
        invoiceItem.setTotalShippingFee(wsInvoiceItem.getTotalShippingFee());
        invoiceItem.setTotalReverseShippingFee(wsInvoiceItem.getTotalReverseShippingFee());
        invoiceItem.setTotalAdditionalFee(wsInvoiceItem.getTotalAdditionalFee());
        invoiceItem.setTotalTax(wsInvoiceItem.getTotalTax());
        invoiceItem.setTotalChannelRecovery(wsInvoiceItem.getTotalChannelRecovery());
        invoiceItem.setAdditionalFeeDescription(wsInvoiceItem.getAdditionalFeeDescription());
        invoiceItem.setAdditionalInfo(wsInvoiceItem.getAdditionalInfo());
        invoiceItem.setChannelProductName(wsInvoiceItem.getChannelProductName());
        invoiceItem.setInvoiceCode(invoiceVO.getInvoiceCode());
        invoiceItem.setChannelCode(channelCode);
        invoiceItem.setCreated(DateUtils.getCurrentTime());
        return invoiceItem;
    }

    @Override
    public GetChannelReconciliationInvoiceItemsResponse getChannnelReconciliationInvoiceItemsByIdentifier(GetChannelReconciliationMetadataRequest request) {
        GetChannelReconciliationInvoiceItemsResponse response = new GetChannelReconciliationInvoiceItemsResponse();
        ValidationContext context = request.validate();
        SaleOrderItem saleOrderItem = null;
        if (!context.hasErrors()) {
            Channel channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelByCode(request.getChannelCode());
            if (channel == null) {
                context.addError(WsResponseCode.INVALID_CHANNEL_CODE, WsResponseCode.INVALID_CHANNEL_CODE.message() + ": " + request.getChannelCode());
            } else {
                List<SaleOrderItem> saleOrderItems = saleOrderService.getSaleOrderItemsByChannelOrderAndItemCode(request.getChannelSaleOrderCode(),
                        request.getChannelSaleOrderItemCode(), channel.getId());
                if (!CollectionUtils.isEmpty(saleOrderItems)) {
                    saleOrderItem = saleOrderItems.iterator().next();
                } else {
                    context.addError(WsResponseCode.INVALID_REQUEST, "No sale order items found for this order. Contact support.");
                }
            }
        }
        if (!context.hasErrors()) {
            for (ChannelReconciliationInvoiceItem item : reconciliationMao.getReconciliationInvoiceItemsByChannelOrderCode(request.getChannelSaleOrderCode(),
                    request.getChannelSaleOrderItemCode(), request.getChannelCode())) {
                response.getInvoiceItems().add(new GetChannelReconciliationInvoiceItemsResponse.ChannelReconciliationInvoiceItemDTO(item, saleOrderItem));
            }
            response.setSuccessful(true);
        }
        response.setSuccessful(!context.hasErrors());
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    public GetChannelReconciliationInvoiceItemsResponse getChannnelReconciliationInvoiceItemsByInvoiceCode(GetChannelReconciliationInvoiceItemsRequest request) {
        GetChannelReconciliationInvoiceItemsResponse response = new GetChannelReconciliationInvoiceItemsResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            for (ChannelReconciliationInvoiceItem item : reconciliationMao.getReconciliationInvoiceItemsbyInvoiceCode(request.getInvoiceCode(), request.getChannelCode())) {
                response.getInvoiceItems().add(new GetChannelReconciliationInvoiceItemsResponse.ChannelReconciliationInvoiceItemDTO(item, null));
            }
            response.setSuccessful(true);
        } else {
            response.setSuccessful(false);
        }
        response.setErrors(context.getErrors());
        return response;
    }
}
