/*
 *  Copyright 2013 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 07-Dec-2013
 *  @author parijat
 */
package com.uniware.services.reconciliation;

import com.uniware.core.api.channel.CreateChannelReconciliationInvoiceRequest;
import com.uniware.core.api.channel.CreateChannelReconciliationInvoiceResponse;
import com.uniware.core.api.reconciliation.CreateChannelPaymentReconciliationRequest;
import com.uniware.core.api.reconciliation.CreateChannelPaymentReconciliationResponse;
import com.uniware.core.api.reconciliation.CreateUnreconciledSaleOrderRequest;
import com.uniware.core.api.reconciliation.CreateUnreconciledSaleOrderResponse;
import com.uniware.core.api.reconciliation.PopulateInvoiceCodesForSaleOrderReconciliationRequest;
import com.uniware.core.api.reconciliation.PopulateInvoiceCodesForSaleOrderReconciliationResponse;
import com.uniware.core.api.reconciliation.GetChannelReconciliationInvoiceItemsRequest;
import com.uniware.core.api.reconciliation.GetChannelReconciliationInvoiceItemsResponse;
import com.uniware.core.api.reconciliation.GetChannelReconciliationMetadataRequest;
import com.uniware.core.api.reconciliation.GetReconciliationInvoiceDetailRequest;
import com.uniware.core.api.reconciliation.GetReconciliationInvoiceDetailResponse;
import com.uniware.core.api.reconciliation.GetSaleOrderReconciliationItemRequest;
import com.uniware.core.api.reconciliation.GetSaleOrderReconciliationItemResponse;
import com.uniware.core.api.reconciliation.MarkChannelPaymentsDisputedRequest;
import com.uniware.core.api.reconciliation.MarkChannelPaymentsDisputedResponse;
import com.uniware.core.api.reconciliation.MarkChannelPaymentsReconciledResponse;
import com.uniware.core.api.reconciliation.MarkChannelPaymentsReconciliedRequest;
import com.uniware.core.vo.ChannelReconciliationInvoice;
import org.springframework.transaction.annotation.Transactional;

public interface IReconciliationService {

    CreateChannelPaymentReconciliationResponse manualReconciledOrderItem(CreateChannelPaymentReconciliationRequest request);

    CreateUnreconciledSaleOrderResponse createUnreconciliedSaleOrder(CreateUnreconciledSaleOrderRequest request);

    MarkChannelPaymentsReconciledResponse markChannelPaymentsReconcilied(MarkChannelPaymentsReconciliedRequest request);

    CreateChannelReconciliationInvoiceResponse createChannelReconciliationInvoice(CreateChannelReconciliationInvoiceRequest request);

    @Transactional ChannelReconciliationInvoice getReconciliationInvoiceByCode(String invoiceCode, String channelCode);

    GetReconciliationInvoiceDetailResponse getReconciliationInvoiceByCode(GetReconciliationInvoiceDetailRequest request);

    GetChannelReconciliationInvoiceItemsResponse getChannnelReconciliationInvoiceItemsByIdentifier(
            GetChannelReconciliationMetadataRequest request);

    GetChannelReconciliationInvoiceItemsResponse getChannnelReconciliationInvoiceItemsByInvoiceCode(
            GetChannelReconciliationInvoiceItemsRequest request);

	MarkChannelPaymentsDisputedResponse markChannelPaymentsDisputed(MarkChannelPaymentsDisputedRequest request);

    GetSaleOrderReconciliationItemResponse getSaleOrderReconciliationItems(GetSaleOrderReconciliationItemRequest request);

    PopulateInvoiceCodesForSaleOrderReconciliationResponse populateInvoiceCodesForSaleOrderReconciliation(PopulateInvoiceCodesForSaleOrderReconciliationRequest populateInvoiceCodesForSaleOrderReconciliationRequest);
}
