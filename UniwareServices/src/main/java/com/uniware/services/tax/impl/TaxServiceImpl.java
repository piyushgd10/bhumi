package com.uniware.services.tax.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.uniware.services.configuration.SystemConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.StringUtils;
import com.uniware.core.api.model.WsInvoice;
import com.uniware.core.api.tax.GetTaxPercentagesRequest;
import com.uniware.core.api.tax.GetTaxPercentagesResponse;
import com.uniware.core.api.tax.TaxPercentageDTO;
import com.uniware.core.api.tax.WsTaxClass;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.cache.EnvironmentPropertiesCache;
import com.uniware.core.cache.LocationCache;
import com.uniware.core.configuration.TaxConfiguration;
import com.uniware.core.entity.State;
import com.uniware.core.entity.TaxType;
import com.uniware.core.entity.TaxTypeConfiguration;
import com.uniware.core.entity.TaxTypeConfigurationRange;
import com.uniware.core.utils.Constants;
import com.uniware.services.tax.ITaxService;

/**
 * Created by Sagar Sahni on 29/05/17.
 */
@Service("taxService")
public class TaxServiceImpl implements ITaxService {

    private static final Logger LOG = LoggerFactory.getLogger(TaxServiceImpl.class);

    @Override
    @Transactional(readOnly = true)
    public GetTaxPercentagesResponse getTaxPercentages(GetTaxPercentagesRequest request) {
        GetTaxPercentagesResponse response = new GetTaxPercentagesResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            Map<String, TaxPercentageDTO> identifierToTaxDetails = new HashMap<>(request.getTaxClassMap().size());
            request.getTaxClassMap().entrySet().forEach(entry -> {
                if (!context.hasErrors()) {
                    WsTaxClass taxClass = entry.getValue();
                    TaxConfiguration taxConfiguration = ConfigurationManager.getInstance().getConfiguration(TaxConfiguration.class);
                    LocationCache locationCache = CacheManager.getInstance().getCache(LocationCache.class);
                    boolean gstEnabled = isGSTEnabled();
                    TaxType taxType = null;
                    if (gstEnabled) {
                        if (StringUtils.isNotBlank(taxClass.getGstTaxTypeCode())) {
                            taxType = taxConfiguration.getTaxTypeByCode(taxClass.getGstTaxTypeCode());
                        } else {
                            context.addError(WsResponseCode.TAX_TYPE_NOT_CONFIGURED, "Could not find GST tax type Code for sku_code : " + entry.getKey());
                        }
                    } else {
                        if (StringUtils.isNotBlank(taxClass.getTaxTypeCode())) {
                            taxType = taxConfiguration.getTaxTypeByCode(taxClass.getTaxTypeCode());
                        } else {
                            context.addError(WsResponseCode.TAX_TYPE_NOT_CONFIGURED, "Could not find tax type Code for sku_code : " + entry.getKey());
                        }
                    }
                    if (taxType == null) {
                        LOG.error("Could not find tax type for code : " + (gstEnabled ? taxClass.getGstTaxTypeCode() : taxClass.getTaxTypeCode()));
                        context.addError(WsResponseCode.TAX_TYPE_NOT_CONFIGURED,
                                "Could not find tax type for code : " + (gstEnabled ? taxClass.getGstTaxTypeCode() : taxClass.getTaxTypeCode()));
                    } else {
                        TaxPercentageDTO taxPercentageDTO = new TaxPercentageDTO();
                        SystemConfiguration configuration = ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class);
                        if (LocationCache.COUNTRY_CODE_INDIA.equals(request.getSourceCountry()) && (LocationCache.COUNTRY_CODE_INDIA.equals(request.getDestinationCountry()) || configuration.isTaxOnInternationalOrderAllowed())) {
                            String stateCode = gstEnabled ? null : request.getSourceState();
                            TaxTypeConfiguration taxTypeConfiguration = taxConfiguration.getTaxTypeConfiguration(taxType.getId(), stateCode);
                            if (taxTypeConfiguration == null) {
                                LOG.error("Tax configuration are missing for tax type {}", taxType.getCode());
                                context.addError(WsResponseCode.INVALID_TAX_TYPE_CODE,
                                        "Tax configuration are missing for " + (gstEnabled ? "Tax Type Code: " + taxType.getCode() + ", for GST"
                                                : "tax type code : " + taxType.getCode() + ", StateCode:" + request.getSourceState()));
                            } else {
                                LOG.debug("Applicable tax type for sku {} and stateCode {} is {} with taxTypeConfigurationId {}", entry.getKey(), stateCode, taxType.getCode(),
                                        taxTypeConfiguration.getId());
                                BigDecimal perUnitSellingPrice = taxClass.getSellingPrice();
                                State sourceState = locationCache.getStateByCode(request.getSourceState());
                                // Destination state can be null for International shipping address
                                State destinationState = locationCache.getStateByCode(request.getDestinationState());
                                if (sourceState == null) {
                                    context.addError(WsResponseCode.INVALID_STATE_CODE, "No state found for source state code : " + request.getSourceState());
                                } else {
                                    TaxTypeConfigurationRange taxRange = WsInvoice.Source.SHIPPING_PACKAGE.equals(request.getSource())
                                            ? TaxTypeUtils.evaluateTaxRangeForSellingPrice(perUnitSellingPrice, taxTypeConfiguration.getRanges(),
                                                    sourceState.equals(destinationState), request.getProvidesCForm())
                                            : TaxTypeUtils.evaluateTaxRangeForVendorPrice(perUnitSellingPrice, taxTypeConfiguration.getRanges(),
                                                    sourceState.equals(destinationState), request.getProvidesCForm());
                                    LOG.debug("Applicable taxRange {}", taxRange);
                                    calculateTax(gstEnabled, taxPercentageDTO, sourceState, destinationState, taxRange);
                                }
                            }
                        }
                        taxPercentageDTO.setAppliedTaxTypeCode(taxType.getCode());
                        LOG.debug("Evaluated taxPercentageDTO for sku {} : {}", entry.getKey(), taxPercentageDTO);
                        identifierToTaxDetails.put(entry.getKey(), taxPercentageDTO);
                    }
                }
            });
            if (!context.hasErrors()) {
                response.setIdentifierToTaxPercentages(identifierToTaxDetails);
                response.setSuccessful(true);
            }
        }
        response.addErrors(context.getErrors());
        response.addWarnings(context.getWarnings());
        return response;
    }

    private void calculateTax(boolean gstEnabled, TaxPercentageDTO taxPercentageDTO, State sourceState, State destinationState, TaxTypeConfigurationRange taxRange) {
        boolean sameStateShipping = sourceState.equals(destinationState);
        boolean sameCountry = destinationState != null && sourceState.getCountry().getCode().equals(destinationState.getCountry().getCode());
        boolean bothAreUT = destinationState != null && sourceState.getType().equals(destinationState.getType()) && State.Type.UNION_TERRITORY.equals(sourceState.getType());

        if (!gstEnabled) {
            if (sameCountry && sameStateShipping) {
                taxPercentageDTO.setVat(taxRange.getVat().compareTo(BigDecimal.ZERO) > 0 ? taxRange.getVat() : BigDecimal.ZERO);
            } else {
                taxPercentageDTO.setCst(taxRange.getCst().compareTo(BigDecimal.ZERO) > 0 ? taxRange.getCst() : BigDecimal.ZERO);
                taxPercentageDTO.setCstFormc(taxRange.getCstFormc().compareTo(BigDecimal.ZERO) > 0 ? taxRange.getCstFormc() : BigDecimal.ZERO);
            }
            taxPercentageDTO.setAdditionalTax(taxRange.getAdditionalTax());
        } else {
            //gst enabled
            if (!sameStateShipping || !sameCountry) {
                //inter state or inter country
                taxPercentageDTO.setIntegratedGst(taxRange.getIntegratedGst().compareTo(BigDecimal.ZERO) > 0 ? taxRange.getIntegratedGst() : BigDecimal.ZERO);
                taxPercentageDTO.setCompensationCess(taxRange.getCompensationCess().compareTo(BigDecimal.ZERO) > 0 ? taxRange.getCompensationCess() : BigDecimal.ZERO);
            } else {
                //intra state
                if (bothAreUT) {
                    taxPercentageDTO.setUnionTerritoryGst(taxRange.getUnionTerritoryGst().compareTo(BigDecimal.ZERO) > 0 ? taxRange.getUnionTerritoryGst() : BigDecimal.ZERO);
                } else {
                    taxPercentageDTO.setStateGst(taxRange.getStateGst().compareTo(BigDecimal.ZERO) > 0 ? taxRange.getStateGst() : BigDecimal.ZERO);
                }
                taxPercentageDTO.setCentralGst(taxRange.getCentralGst().compareTo(BigDecimal.ZERO) > 0 ? taxRange.getCentralGst() : BigDecimal.ZERO);
                taxPercentageDTO.setCompensationCess(taxRange.getCompensationCess().compareTo(BigDecimal.ZERO) > 0 ? taxRange.getCompensationCess() : BigDecimal.ZERO);
            }
        }
    }

    @Override
    public boolean isGSTEnabled() {
        Date gstDate = DateUtils.stringToDate(CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class).getGSTDate(), Constants.DATE_PATTERN);
        return gstDate != null && gstDate.before(DateUtils.getCurrentTime());
    }
}
