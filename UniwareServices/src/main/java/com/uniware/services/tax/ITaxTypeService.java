package com.uniware.services.tax;

import java.util.List;

import com.uniware.core.api.catalog.TaxTypeDTO;
import com.uniware.core.api.tax.CreateEditTaxTypesConfigurationRequest;
import com.uniware.core.api.tax.CreateTaxTypeConfigurationResponse;
import com.uniware.core.api.tax.EditTaxTypeRequest;
import com.uniware.core.api.tax.EditTaxTypeResponse;
import com.uniware.core.api.tax.GetTaxTypeConfigurationRequest;
import com.uniware.core.api.tax.GetTaxTypeConfigurationResponse;
import com.uniware.core.api.tax.GetTaxTypeStatesResponse;
import com.uniware.core.configuration.TaxConfiguration;
import com.uniware.core.entity.TaxType;
import com.uniware.core.entity.TaxTypeConfiguration;
import org.springframework.transaction.annotation.Transactional;

public interface ITaxTypeService {

    CreateTaxTypeConfigurationResponse createTaxTypeConfiguration(CreateEditTaxTypesConfigurationRequest createTaxTypeConfigurationRequest);

    TaxType getTaxTypeByCode(String taxTypeCode);

    TaxTypeConfiguration getTaxTypeConfigurationByCode(String taxTypeCode, String stateCode);

    List<TaxTypeDTO> getTaxTypes();

    List<TaxTypeDTO> getTaxTypes(boolean fetchGst);

    EditTaxTypeResponse editTaxType(EditTaxTypeRequest request);

    List<TaxConfiguration.TaxTypeVO> lookupTaxTypes(String code);

    List<TaxType> getAllTaxTypes();

    GetTaxTypeConfigurationResponse getTaxTypeConfigurations(GetTaxTypeConfigurationRequest request);

    GetTaxTypeStatesResponse getStatesForTaxTypes();

    TaxType update(TaxType taxType);

}
