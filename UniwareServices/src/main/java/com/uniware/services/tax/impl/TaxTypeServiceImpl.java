package com.uniware.services.tax.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.api.validation.WsError;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.JsonUtils;
import com.unifier.core.utils.NumberUtils;
import com.unifier.core.utils.StringUtils;
import com.unifier.core.utils.ValidatorUtils;
import com.unifier.services.aspect.MarkDirty;
import com.unifier.services.aspect.RollbackOnFailure;
import com.uniware.core.api.catalog.CreateTaxTypeRequest;
import com.uniware.core.api.catalog.CreateTaxTypeResponse;
import com.uniware.core.api.catalog.TaxTypeConfigurationDTO;
import com.uniware.core.api.catalog.TaxTypeConfigurationRangeDTO;
import com.uniware.core.api.catalog.TaxTypeDTO;
import com.uniware.core.api.tax.CreateEditTaxTypesConfigurationRequest;
import com.uniware.core.api.tax.CreateTaxTypeConfigurationResponse;
import com.uniware.core.api.tax.EditTaxTypeRequest;
import com.uniware.core.api.tax.EditTaxTypeResponse;
import com.uniware.core.api.tax.GetTaxTypeConfigurationRequest;
import com.uniware.core.api.tax.GetTaxTypeConfigurationResponse;
import com.uniware.core.api.tax.GetTaxTypeStatesResponse;
import com.uniware.core.api.tax.WsTaxType;
import com.uniware.core.api.tax.WsTaxTypeConfiguration;
import com.uniware.core.api.tax.WsTaxTypeConfiguration.WSTaxTypeConfigurationRange;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.cache.LocationCache;
import com.uniware.core.configuration.TaxConfiguration;
import com.uniware.core.entity.Facility;
import com.uniware.core.entity.PartyAddress;
import com.uniware.core.entity.PartyAddressType;
import com.uniware.core.entity.State;
import com.uniware.core.entity.TaxType;
import com.uniware.core.entity.TaxTypeConfiguration;
import com.uniware.core.entity.TaxTypeConfigurationRange;
import com.uniware.core.entity.Vendor;
import com.uniware.dao.taxtype.ITaxTypeDao;
import com.uniware.services.tax.ITaxTypeService;
import com.uniware.services.vendor.IVendorService;
import com.uniware.services.warehouse.IFacilityService;

@Service("taxTypeService")
public class TaxTypeServiceImpl implements ITaxTypeService {

    private static final Logger LOG = LoggerFactory.getLogger(TaxTypeServiceImpl.class);

    @Autowired
    private ITaxTypeDao taxTypeDao;

    @Autowired
    private IFacilityService facilityService;

    @Autowired
    private IVendorService vendorService;

    @Override
    public CreateTaxTypeConfigurationResponse createTaxTypeConfiguration(CreateEditTaxTypesConfigurationRequest request) {
        try {
            CreateTaxTypeConfigurationResponse response = createTaxTypeConfigurationRowInternal(request);
            if (response.isSuccessful()) {
                TaxType taxType = getTaxTypeByCode(request.getTaxTypeCode());
                taxType.setUpdated(DateUtils.getCurrentTime());
                update(taxType);
                response.getTaxTypeConfigurationDTO().setUpdated(taxType.getUpdated());
            }
            return response;
        } catch (DataIntegrityViolationException e) {
            /*if (ExceptionUtils.isDuplicateKeyException(e)) {
                return (CreateTaxTypeConfigurationResponse) new CreateTaxTypeConfigurationResponse().addError(new WsError("Duplicate Tax/State combination for :"
                        + request.getTaxTypeCode() + "-" + request.getTaxTypeConfigurations().get(0).getStateCode()));
            } else {*/
            LOG.error("Unkown error while adding tax config : {}", e);
            return (CreateTaxTypeConfigurationResponse) new CreateTaxTypeConfigurationResponse().addError(new WsError("Unknown error while adding tax configuration for :"
                    + request.getTaxTypeCode()));
            //            }
        }
    }

    @Transactional
    @RollbackOnFailure
    @MarkDirty(values = { TaxConfiguration.class })
    public CreateTaxTypeConfigurationResponse createTaxTypeConfigurationRowInternal(CreateEditTaxTypesConfigurationRequest request) {
        CreateTaxTypeConfigurationResponse response = new CreateTaxTypeConfigurationResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            TaxType taxType = taxTypeDao.getTaxTypeByCode(request.getTaxTypeCode());
            if (request.isEdit()) {
                if (taxType == null) {
                    context.addError(WsResponseCode.INVALID_TAX_TYPE_CODE, "invalid tax type code");
                    LOG.error("Invalid tax type code");
                }
            } else {
                CreateTaxTypeResponse taxTypeResponse = new CreateTaxTypeResponse();
                if (taxType == null) {
                    CreateTaxTypeRequest taxTypeRequest = new CreateTaxTypeRequest();
                    WsTaxType wsTaxType = new WsTaxType(request.getTaxTypeCode(), request.getTaxTypeName(), request.isGst());
                    taxTypeRequest.setWsTaxType(wsTaxType);
                    taxTypeResponse = createTaxType(taxTypeRequest);
                    context.addErrors(taxTypeResponse.getErrors());
                    taxType = taxTypeDao.getTaxTypeByCode(taxTypeResponse.getTaxTypeDTO().getCode());
                } else {
                    context.addError(WsResponseCode.INVALID_TAX_TYPE_CODE, "Duplicate tax type code");
                    LOG.error("Duplicate tax type code");
                }
            }

            if (!context.hasErrors()) {
                if (request.isEdit()) {
                    clearTaxConfigurationForTaxType(taxType);
                }
                for (WsTaxTypeConfiguration wsTaxTypeConfig : request.getTaxTypeConfigurations()) {
                    State state = null;
                    if (!request.isGst()) {
                        if(StringUtils.isBlank(wsTaxTypeConfig.getStateCode())){
                            context.addError(WsResponseCode.INVALID_REQUEST, "state code missing");
                        } else {
                            state = CacheManager.getInstance().getCache(LocationCache.class).getStateByCode(wsTaxTypeConfig.getStateCode());
                            if (state == null) {
                                context.addError(WsResponseCode.INVALID_STATE_CODE, "invalid state code [" + wsTaxTypeConfig.getStateCode() + "]");
                                LOG.error("Invalid state code [ {} ]", wsTaxTypeConfig.getStateCode());
                                response.setSuccessful(false);
                            }
                        }
                    } else {
                        if (!StringUtils.isBlank(wsTaxTypeConfig.getStateCode())) {
                            context.addError(WsResponseCode.INVALID_REQUEST, "GST Type Cannot have State Code");
                        }
                    }

                    if (!context.hasErrors()) {
                        if (wsTaxTypeConfig.getRanges() != null && !wsTaxTypeConfig.getRanges().isEmpty()) {
                            createPriceSlabsFromSplits(wsTaxTypeConfig.getRanges(), context);
                        } else {
                            context.addError(WsResponseCode.INVALID_TAX_TYPE_CONFIGURATION_RANGE, "Ranges cannot be empty for any configuration.");
                            LOG.error("Ranges cannot be empty for any configuration.");
                            response.setSuccessful(false);
                        }
                    }
                    if (!context.hasErrors()) {
                        if (wsTaxTypeConfig.getRanges() != null && !wsTaxTypeConfig.getRanges().isEmpty()) {
                            validateRangeSanity(wsTaxTypeConfig.getRanges(), context);
                        }
                    }
                    if (!context.hasErrors()) {
                        TaxTypeConfiguration taxTypeConfiguration = new TaxTypeConfiguration();
                        taxTypeConfiguration.setCreated(DateUtils.getCurrentTime());
                        prepareTaxTypeConfiguration(taxTypeConfiguration, wsTaxTypeConfig, taxType, state, context);
                        createEditTaxTypeConfiguration(taxTypeConfiguration);
                        response.setTaxTypeConfigurationDTO(prepareTaxTypeConfigurationDTO(taxTypeConfiguration));
                        response.setSuccessful(true);
                    }
                }
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    private void createPriceSlabsFromSplits(List<WSTaxTypeConfigurationRange> ranges, ValidationContext context) {
        if (ranges != null && !ranges.isEmpty()) {
            if (!NumberUtils.equals(ranges.get(0).getStartPrice(), BigDecimal.ZERO)) {
                context.addError(WsResponseCode.INVALID_TAX_TYPE_CONFIGURATION_RANGE, "Price split should always begin with zero.");
            }
            for (int j = 0; j < ranges.size() - 1; j++) {
                if (NumberUtils.equals(ranges.get(j).getStartPrice(), ranges.get(j + 1).getStartPrice())) {
                    context.addError(WsResponseCode.INVALID_TAX_TYPE_CONFIGURATION_RANGE, "Duplicate start prices found. Start prices should be unique across slabs.");
                }
            }
            if (!context.hasErrors()) {
                if (ranges.size() == 1) {
                    ranges.get(0).setStartPrice(BigDecimal.ZERO);
                    ranges.get(0).setEndPrice(TaxTypeConfigurationRange.TAX_RANGE_INFINITY);
                    return;
                } else {
                    for (int i = 0; i < ranges.size() - 1; i++) {
                        ranges.get(i).setEndPrice(ranges.get(i + 1).getStartPrice());
                    }
                    ranges.get(ranges.size() - 1).setEndPrice(TaxTypeConfigurationRange.TAX_RANGE_INFINITY);
                }
            }
        }

    }

    private void prepareTaxTypeConfigurationRanges(TaxTypeConfiguration taxTypeConfiguration, WsTaxTypeConfiguration wsTaxTypeConfiguration) {
        Set<TaxTypeConfigurationRange> ranges = new TreeSet<>();
        if (wsTaxTypeConfiguration.getRanges() != null && !wsTaxTypeConfiguration.getRanges().isEmpty()) {
            if (wsTaxTypeConfiguration.getRanges().get(wsTaxTypeConfiguration.getRanges().size() - 1).getEndPrice() == null
                    || wsTaxTypeConfiguration.getRanges().get(wsTaxTypeConfiguration.getRanges().size() - 1).getEndPrice().compareTo(TaxTypeConfigurationRange.TAX_RANGE_INFINITY) == -1) {
                wsTaxTypeConfiguration.getRanges().get(wsTaxTypeConfiguration.getRanges().size() - 1).setEndPrice(TaxTypeConfigurationRange.TAX_RANGE_INFINITY);
            }
            for (WSTaxTypeConfigurationRange range : wsTaxTypeConfiguration.getRanges()) {
                TaxTypeConfigurationRange configRange = prepareTaxTypeConfigRangeFromWS(new TaxTypeConfigurationRange(taxTypeConfiguration), range);
                configRange.setCreated(DateUtils.getCurrentTime());
                ranges.add(configRange);
            }
        }
        taxTypeConfiguration.setRanges(ranges);
    }

    private TaxTypeConfigurationRange prepareTaxTypeConfigRangeFromWS(TaxTypeConfigurationRange taxTypeConfigurationRange, WSTaxTypeConfigurationRange wsTaxTypeConfigurationRange) {
        taxTypeConfigurationRange.setVat(ValidatorUtils.getOrDefaultValue(wsTaxTypeConfigurationRange.getVat(), taxTypeConfigurationRange.getVat()));
        taxTypeConfigurationRange.setCst(ValidatorUtils.getOrDefaultValue(wsTaxTypeConfigurationRange.getCst(), taxTypeConfigurationRange.getCst()));
        taxTypeConfigurationRange.setAdditionalTax(ValidatorUtils.getOrDefaultValue(wsTaxTypeConfigurationRange.getAdditionalTax(), taxTypeConfigurationRange.getAdditionalTax()));
        taxTypeConfigurationRange.setCstFormc(ValidatorUtils.getOrDefaultValue(wsTaxTypeConfigurationRange.getCstWithCForm(), taxTypeConfigurationRange.getCstFormc()));
        taxTypeConfigurationRange.setCentralGst(ValidatorUtils.getOrDefaultValue(wsTaxTypeConfigurationRange.getCentralGst(), taxTypeConfigurationRange.getCentralGst()));
        taxTypeConfigurationRange.setStateGst(ValidatorUtils.getOrDefaultValue(wsTaxTypeConfigurationRange.getStateGst(), taxTypeConfigurationRange.getStateGst()));
        taxTypeConfigurationRange.setIntegratedGst(ValidatorUtils.getOrDefaultValue(wsTaxTypeConfigurationRange.getIntegratedGst(), taxTypeConfigurationRange.getIntegratedGst()));
        taxTypeConfigurationRange.setUnionTerritoryGst(ValidatorUtils.getOrDefaultValue(wsTaxTypeConfigurationRange.getUnionTerritoryGst(), taxTypeConfigurationRange.getUnionTerritoryGst()));
        taxTypeConfigurationRange.setCompensationCess(ValidatorUtils.getOrDefaultValue(wsTaxTypeConfigurationRange.getCompensationCess(), taxTypeConfigurationRange.getCompensationCess()));
        taxTypeConfigurationRange.setStartPrice(ValidatorUtils.getOrDefaultValue(wsTaxTypeConfigurationRange.getStartPrice(), BigDecimal.ZERO));
        taxTypeConfigurationRange.setEndPrice(ValidatorUtils.getOrDefaultValue(wsTaxTypeConfigurationRange.getEndPrice(), TaxTypeConfigurationRange.TAX_RANGE_INFINITY));
        return taxTypeConfigurationRange;
    }

    private void validateRangeSanity(List<WSTaxTypeConfigurationRange> ranges, ValidationContext context) {
        int index = 0;
        if (ranges != null && !ranges.isEmpty()) {
            for (WSTaxTypeConfigurationRange range : ranges) {
                if ((range.getStartPrice() != null && range.getEndPrice() == null) || (range.getEndPrice() != null && range.getStartPrice() == null)) {
                    context.addError(WsResponseCode.INVALID_TAX_TYPE_CONFIGURATION_RANGE, "Either of start price/ end price cannot be null for valid range.");
                    return;
                }
                if (range.getStartPrice() != null && range.getEndPrice() != null && NumberUtils.lessThan(range.getEndPrice(), range.getStartPrice())) {
                    context.addError(WsResponseCode.INVALID_TAX_TYPE_CONFIGURATION_RANGE, "End price cannot be less than start price");
                    return;
                }
            }
            if (ranges.get(0).getStartPrice() != null && ranges.get(0).getStartPrice().compareTo(BigDecimal.ZERO) != 0) {
                context.addError(WsResponseCode.INVALID_TAX_TYPE_CONFIGURATION_RANGE, "Price range should start with value zero.");
                return;
            }
            if (ranges.size() > 1) {
                while (index < (ranges.size() - 1)) {
                    /*if (!ranges.get(index + 1).getStartPrice().setScale(2, RoundingMode.HALF_EVEN).subtract(ranges.get(index).getEndPrice().setScale(2, RoundingMode.HALF_EVEN)).equals(
                            NumberUtils.newBigDecimal(1.00).setScale(2, RoundingMode.HALF_EVEN))) {
                        context.addError(WsResponseCode.INVALID_TAX_TYPE_CONFIGURATION_RANGE, "Consecutive range should not have break between them. Invalid next start price ["
                                + ranges.get(index + 1).getStartPrice() + "] for end price of previous range [" + ranges.get(index).getEndPrice() + "]");
                    }*/
                    if (ranges.get(index + 1).getStartPrice().compareTo(ranges.get(index).getEndPrice()) == -1) {
                        context.addError(WsResponseCode.INVALID_TAX_TYPE_CONFIGURATION_RANGE, "Start price [" + ranges.get(index + 1).getStartPrice()
                                + "] cannot be less than end price of previous range [" + ranges.get(index).getEndPrice() + "]");
                    }
                    index++;
                }
            }
        } else {
            context.addError(WsResponseCode.INVALID_TAX_TYPE_CONFIGURATION_RANGE, "Price ranges cannot be empty for a taxt configuration");
        }
    }

    private TaxTypeConfiguration prepareTaxTypeConfiguration(TaxTypeConfiguration taxTypeConfiguration, WsTaxTypeConfiguration wsTaxTypeConfiguration, TaxType taxType,
            State state, ValidationContext context) {
        taxTypeConfiguration.setTaxType(taxType);
        taxTypeConfiguration.setStateCode(state != null ? state.getIso2Code() : null);
        prepareTaxTypeConfigurationRanges(taxTypeConfiguration, wsTaxTypeConfiguration);
        return taxTypeConfiguration;
    }

    @Override
    @Transactional(readOnly = true)
    public TaxType getTaxTypeByCode(String taxTypeCode) {
        return taxTypeDao.getTaxTypeByCode(taxTypeCode);
    }

    @Override
    @Transactional(readOnly = true)
    public TaxTypeConfiguration getTaxTypeConfigurationByCode(String taxTypeCode, String stateCode) {
        return taxTypeDao.getTaxTypeConfigurationByCode(taxTypeCode, stateCode);
    }

    @Transactional
    private void createEditTaxTypeConfiguration(TaxTypeConfiguration taxTypeConfiguration) {
        taxTypeDao.editTaxTypeConfiguration(taxTypeConfiguration);
    }

    @Transactional
    private void clearTaxConfigurationForTaxType(TaxType taxType) {
        taxTypeDao.deleteTaxConfigurations(taxType);
    }

    @Override
    @Transactional
    public List<TaxTypeDTO> getTaxTypes() {
        List<TaxTypeDTO> taxTypes = new ArrayList<TaxTypeDTO>();
        for (TaxType taxType : taxTypeDao.getTaxTypes(false)) {
            taxTypes.add(new TaxTypeDTO(taxType));
        }
        return taxTypes;
    }

    @Override
    @Transactional
    public List<TaxTypeDTO> getTaxTypes(boolean fetchGst) {
        List<TaxTypeDTO> taxTypes = new ArrayList<TaxTypeDTO>();
        for (TaxType taxType : taxTypeDao.getTaxTypes(fetchGst)) {
            taxTypes.add(new TaxTypeDTO(taxType));
        }
        return taxTypes;
    }

    @Transactional
    public CreateTaxTypeResponse createTaxType(CreateTaxTypeRequest request) {
        CreateTaxTypeResponse response = new CreateTaxTypeResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            TaxType taxType = taxTypeDao.getTaxTypeByCode(request.getWsTaxType().getCode().trim());
            if (taxType != null) {
                context.addError(WsResponseCode.DUPLICATE_TAX_TYPE_CODE, "Tax Type already exists");
            } else {
                taxType = new TaxType();
                if (StringUtils.isNotBlank(request.getWsTaxType().getName())) {
                    taxType.setName(request.getWsTaxType().getName());
                } else {
                    taxType.setName(StringUtils.capitalizeString(request.getWsTaxType().getCode()));
                }
                taxType.setCode(request.getWsTaxType().getCode().trim());
                taxType.setGst(request.getWsTaxType().isGst());
                taxType.setCreated(DateUtils.getCurrentDate());
                taxType.setUpdated(DateUtils.getCurrentDayTime());
                taxType = taxTypeDao.addTaxType(taxType);
                response.setTaxTypeDTO(new TaxTypeDTO(taxType));
                response.setSuccessful(true);
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @MarkDirty(values = { TaxConfiguration.class })
    public EditTaxTypeResponse editTaxType(EditTaxTypeRequest request) {
        EditTaxTypeResponse response = new EditTaxTypeResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            if (!context.hasErrors()) {
                TaxType taxType = getTaxTypeByCode(request.getWsTaxType().getCode());
                if (taxType == null) {
                    context.addError(WsResponseCode.INVALID_TAX_TYPE_CODE, "Invalid Tax Type Code");
                } else {
                    taxType.setName(request.getWsTaxType().getName());
                    update(taxType);
                    taxType = getTaxTypeByCode(taxType.getCode());
                    response.setTaxTypeDTO(new TaxTypeDTO(taxType));
                    response.setSuccessful(true);
                }
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    public GetTaxTypeConfigurationResponse getTaxTypeConfigurations(GetTaxTypeConfigurationRequest request) {
        GetTaxTypeConfigurationResponse response = new GetTaxTypeConfigurationResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            TaxType taxtType = taxTypeDao.getTaxTypeByCode(request.getTaxTypeCode());
            if (taxtType == null) {
                context.addError(WsResponseCode.INVALID_TAX_TYPE_CODE, "No such tax type code exists");
            } else {
                List<TaxTypeConfigurationDTO> taxTypeConfigurationDTOs = new ArrayList<TaxTypeConfigurationDTO>();
                for (TaxTypeConfiguration taxTypeConfiguration : taxTypeDao.getTaxtypeConfigurationsByTaxTypeCode(request.getTaxTypeCode())) {
                    TaxTypeConfigurationDTO taxTypeConfigurationDTO = prepareTaxTypeConfigurationDTO(taxTypeConfiguration);
                    taxTypeConfigurationDTOs.add(taxTypeConfigurationDTO);
                }
                response.getTaxTypeConfigurations().addAll(taxTypeConfigurationDTOs);
                response.setSuccessful(true);
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    private TaxTypeConfigurationDTO prepareTaxTypeConfigurationDTO(TaxTypeConfiguration taxTypeConfiguration) {
        TaxTypeConfigurationDTO taxTypeConfigurationDTO = new TaxTypeConfigurationDTO();
        taxTypeConfigurationDTO.setStateCode(taxTypeConfiguration.getStateCode());
        taxTypeConfigurationDTO.setTaxTypeCode(taxTypeConfiguration.getTaxType().getCode());
        taxTypeConfigurationDTO.setUpdated(taxTypeConfiguration.getTaxType().getUpdated());
        List<TaxTypeConfigurationRangeDTO> ranges = new ArrayList<>();
        for (TaxTypeConfigurationRange range : taxTypeConfiguration.getRanges()) {
            TaxTypeConfigurationRangeDTO rangeDTO = new TaxTypeConfigurationRangeDTO();
            rangeDTO.setVat(range.getVat());
            rangeDTO.setCst(range.getCst());
            rangeDTO.setCstFormc(range.getCstFormc());
            rangeDTO.setAdditionalTax(range.getAdditionalTax());
            rangeDTO.setCentralGst(range.getCentralGst());
            rangeDTO.setStateGst(range.getStateGst());
            rangeDTO.setUnionTerritoryGst(range.getUnionTerritoryGst());
            rangeDTO.setIntegratedGst(range.getIntegratedGst());
            rangeDTO.setCompensationCess(range.getCompensationCess());
            rangeDTO.setStartPrice(range.getStartPrice());
            rangeDTO.setEndPrice(range.getEndPrice());
            ranges.add(rangeDTO);
        }
        Collections.sort(ranges);
        taxTypeConfigurationDTO.setRanges(ranges);
        return taxTypeConfigurationDTO;
    }

    @Override
    public List<TaxConfiguration.TaxTypeVO> lookupTaxTypes(String code) {
        return ConfigurationManager.getInstance().getConfiguration(TaxConfiguration.class).lookupTaxTypes(code);
    }

    @Transactional
    @Override
    public List<TaxType> getAllTaxTypes() {
        return taxTypeDao.getAllTaxTypes();
    }

    @Override
    @Transactional
    public GetTaxTypeStatesResponse getStatesForTaxTypes() {
        GetTaxTypeStatesResponse response = new GetTaxTypeStatesResponse();
        List<Facility> facilites = facilityService.getAllEnabledFacilities();
        Map<String, String> statesMap = new HashMap<>();
        for (Facility facility : facilites) {
            PartyAddress address = facility.getPartyAddressByType(PartyAddressType.Code.SHIPPING.name());
            if (address != null) {
                State state = address.getState();
                statesMap.put(state.getIso2Code(), state.getName());
            }
        }
        response.setFacilityStates(statesMap);
        statesMap = new HashMap<>();
        List<Vendor> vendors = vendorService.getAllVendorsForTenant();
        for (Vendor vendor : vendors) {
            PartyAddress address = vendor.getPartyAddressByType(PartyAddressType.Code.SHIPPING.name());
            if (address != null) {
                State state = address.getState();
                if (!response.getFacilityStates().containsKey(state.getIso2Code())) {
                    statesMap.put(state.getIso2Code(), state.getName());
                }
            }
        }
        response.setVendorStates(statesMap);
        statesMap = new HashMap<>();
        Map<String, String> allStatesMap = JsonUtils.jsonToMap(CacheManager.getInstance().getCache(LocationCache.class).getStateJson());
        for (Entry<String, String> state : allStatesMap.entrySet()) {
            if (!response.getFacilityStates().containsKey(state.getKey()) && !response.getVendorStates().containsKey(state.getKey())) {
                statesMap.put(state.getKey(), state.getValue());
            }
        }
        response.setRestOfIndiaStates(statesMap);
        response.setSuccessful(true);
        return response;
    }
    
    @Override
    @Transactional
    public TaxType update(TaxType taxType) {
        return taxTypeDao.update(taxType);
    }

}
