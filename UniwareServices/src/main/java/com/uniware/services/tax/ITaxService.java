package com.uniware.services.tax;

import com.uniware.core.api.tax.GetTaxPercentagesRequest;
import com.uniware.core.api.tax.GetTaxPercentagesResponse;

/**
 * Created by Sagar Sahni on 29/05/17.
 */
public interface ITaxService {

    GetTaxPercentagesResponse getTaxPercentages(GetTaxPercentagesRequest request);

    boolean isGSTEnabled();
}