/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 19-Nov-2014
 *  @author parijat
 */
package com.uniware.services.tax.impl;

import java.math.BigDecimal;
import java.util.Set;

import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.utils.NumberUtils;
import com.uniware.core.entity.TaxTypeConfigurationRange;
import com.uniware.services.configuration.SystemConfiguration;

public class TaxTypeUtils {

    public static TaxTypeConfigurationRange evaluateTaxRangeForSellingPrice(BigDecimal sellingPrice, Set<TaxTypeConfigurationRange> ranges, boolean sameShippingState,
            boolean providesCForm) {
        TaxTypeConfigurationRange range = null;
        boolean taxExclusiveSellingPrice = ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).isSellingPricesTaxExclusive();
        range = getTaxTypeConfigurationRange(sellingPrice, ranges, sameShippingState, providesCForm, range, taxExclusiveSellingPrice);
        if (range == null) {
            range = ranges.toArray(new TaxTypeConfigurationRange[0])[ranges.size() - 1];
        }
        return range;
    }

    private static TaxTypeConfigurationRange getTaxTypeConfigurationRange(BigDecimal sellingPrice, Set<TaxTypeConfigurationRange> ranges, boolean sameShippingState,
            boolean providesCForm, TaxTypeConfigurationRange range, boolean taxExclusiveSellingPrice) {
        for (TaxTypeConfigurationRange taxRange : ranges) {
            BigDecimal startPrice = taxExclusiveSellingPrice
                    ? taxRange.getStartPrice().subtract(
                            NumberUtils.divide(NumberUtils.multiply(taxRange.getStartPrice(), getTaxAmount(sameShippingState, providesCForm, taxRange)), new BigDecimal(100)))
                    : taxRange.getStartPrice();
            BigDecimal endPrice = taxExclusiveSellingPrice
                    ? taxRange.getEndPrice().subtract(
                            NumberUtils.divide(NumberUtils.multiply(taxRange.getEndPrice(), getTaxAmount(sameShippingState, providesCForm, taxRange)), new BigDecimal(100)))
                    : taxRange.getEndPrice();
            if (NumberUtils.equals(sellingPrice, startPrice)) {
                range = taxRange;
                break;
            }
            if (NumberUtils.greaterThan(sellingPrice, startPrice) && NumberUtils.lessThan(sellingPrice, endPrice)) {
                range = taxRange;
                break;
            }
        }
        return range;
    }

    public static TaxTypeConfigurationRange evaluateTaxRangeForVendorPrice(BigDecimal sellingPrice, Set<TaxTypeConfigurationRange> ranges, boolean sameShippingState,
            boolean providesCForm) {
        TaxTypeConfigurationRange range = null;
        boolean taxExclusiveSellingPrice = ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).isVendorPricesTaxExclusive();
        range = getTaxTypeConfigurationRange(sellingPrice, ranges, sameShippingState, providesCForm, range, taxExclusiveSellingPrice);
        if (range == null) {
            range = ranges.toArray(new TaxTypeConfigurationRange[0])[ranges.size() - 1];
        }
        return range;
    }

    private static BigDecimal getTaxAmount(boolean sameShippingState, boolean providesCForm, TaxTypeConfigurationRange range) {
        if (sameShippingState) {
            return range.getVat().multiply(new BigDecimal(100).add(range.getAdditionalTax()).divide(new BigDecimal(100)));
        } else if (providesCForm) {
            return range.getCstFormc();
        } else {
            return range.getCst().multiply(new BigDecimal(100).add(range.getAdditionalTax()).divide(new BigDecimal(100)));
        }
    }

}
