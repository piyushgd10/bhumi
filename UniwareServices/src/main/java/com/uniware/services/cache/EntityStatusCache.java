/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 27, 2014
 *  @author akshay
 */

package com.uniware.services.cache;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.unifier.core.annotation.Cache;
import com.unifier.core.annotation.Level;
import com.unifier.core.api.advanced.EntityStatusDTO;
import com.unifier.core.cache.ICache;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.ui.SelectItem;
import com.unifier.services.application.IApplicationSetupService;
import com.uniware.core.entity.Product;
import com.uniware.core.entity.SaleOrderItemStatus;
import com.uniware.core.entity.SaleOrderStatus;
import com.uniware.core.entity.ShippingPackageStatus;
import com.uniware.core.utils.UserContext;
import com.uniware.services.configuration.SourceConfiguration;
import com.uniware.services.configuration.data.manager.ProductConfiguration;

@Cache(type = "entityStatusCache", level = Level.GLOBAL, eager = true)
public class EntityStatusCache implements ICache {

    private final List<ShippingPackageStatus>                     shippingPackageStatuses = new ArrayList<>();
    private final List<SaleOrderStatus>                           saleOrderStatuses       = new ArrayList<>();
    private final List<SaleOrderItemStatus>                       saleOrderItemStatuses   = new ArrayList<>();
    private final Map<String, Map<String, List<EntityStatusDTO>>> entityProductStatuses   = new HashMap<>();

    @Autowired
    private transient IApplicationSetupService                              applicationSetupService;

    private void addShippingPackageStatus(ShippingPackageStatus shippingPackageStatus) {
        shippingPackageStatuses.add(shippingPackageStatus);
    }

    private void addSaleOrderStatus(SaleOrderStatus saleOrderStatus) {
        saleOrderStatuses.add(saleOrderStatus);
    }

    private void addSaleOrderItemStatus(SaleOrderItemStatus saleOrderItemStatus) {
        saleOrderItemStatuses.add(saleOrderItemStatus);
    }

    public void freeze() {
        List<Product> allProducts = ConfigurationManager.getInstance().getConfiguration(ProductConfiguration.class).getAllProducts();
        for (ShippingPackageStatus shippingPackageStatus : shippingPackageStatuses) {
            List<Product> applicableProducts = allProducts;
            if (shippingPackageStatus.getApplicableFor() != null) {
                applicableProducts = ConfigurationManager.getInstance().getConfiguration(ProductConfiguration.class).getProducts(shippingPackageStatus.getApplicableFor().getCode());
            }
            addEntityProductStatus(shippingPackageStatus.getClass().getSimpleName(), applicableProducts,
                    new EntityStatusDTO(shippingPackageStatus.getCode(), shippingPackageStatus.getDescription()));
        }

        for (SaleOrderStatus saleOrderStatus : saleOrderStatuses) {
            List<Product> applicableProducts = allProducts;
            if (saleOrderStatus.getApplicableFor() != null) {
                applicableProducts = ConfigurationManager.getInstance().getConfiguration(ProductConfiguration.class).getProducts(saleOrderStatus.getApplicableFor().getCode());
            }
            addEntityProductStatus(saleOrderStatus.getClass().getSimpleName(), applicableProducts, new EntityStatusDTO(saleOrderStatus.getCode(), saleOrderStatus.getDescription()));
        }

        for (SaleOrderItemStatus saleOrderItemStatus : saleOrderItemStatuses) {
            List<Product> applicableProducts = allProducts;
            if (saleOrderItemStatus.getApplicableFor() != null) {
                applicableProducts = ConfigurationManager.getInstance().getConfiguration(ProductConfiguration.class).getProducts(saleOrderItemStatus.getApplicableFor().getCode());
            }
            addEntityProductStatus(saleOrderItemStatus.getClass().getSimpleName(), applicableProducts,
                    new EntityStatusDTO(saleOrderItemStatus.getCode(), saleOrderItemStatus.getDescription()));
        }
    }

    private void addEntityProductStatus(String name, List<Product> products, EntityStatusDTO entityStatusDTO) {
        for (Product product : products) {
            Map<String, List<EntityStatusDTO>> productToStatusList = entityProductStatuses.get(name);
            if (productToStatusList == null) {
                productToStatusList = new HashMap<>();
                productToStatusList.put(product.getCode(), new ArrayList<EntityStatusDTO>());
            } else if (productToStatusList != null && productToStatusList.get(product.getCode()) == null) {
                productToStatusList.put(product.getCode(), new ArrayList<EntityStatusDTO>());
            }
            productToStatusList.get(product.getCode()).add(entityStatusDTO);
            entityProductStatuses.put(name, productToStatusList);
        }
    }

    public List<SelectItem> getShippingPackageStatus() {
        return getEntityStatus(ShippingPackageStatus.class.getSimpleName());
    }

    public List<SelectItem> getSaleOrderStatus() {
        return getEntityStatus(SaleOrderStatus.class.getSimpleName());
    }

    public List<SelectItem> getSaleOrderItemStatus() {
        return getEntityStatus(SaleOrderItemStatus.class.getSimpleName());
    }

    public List<SelectItem> getEntityStatus(String entityName) {
        List<SelectItem> selectItems = new ArrayList<SelectItem>();
        List<EntityStatusDTO> entityStatuses = getEntityStatuses(entityName, UserContext.current().getTenant().getProduct().getCode());
        if (entityStatuses != null) {
            for (EntityStatusDTO status : entityStatuses) {
                selectItems.add(new SelectItem(status.getDescription(), status.getCode()));
            }
            return selectItems;
        }
        return null;
    }

    public List<EntityStatusDTO> getEntityStatuses(String entityName, String productType) {
        return entityProductStatuses.get(entityName) != null ? entityProductStatuses.get(entityName).get(productType) : null;
    }

    @Override
    public void load() {
        for (ShippingPackageStatus shippingPackageStatus : applicationSetupService.getAllShippingPackageStatus()) {
            addShippingPackageStatus(shippingPackageStatus);
        }
        for (SaleOrderStatus saleOrderStatus : applicationSetupService.getAllSaleOrderStatus()) {
            addSaleOrderStatus(saleOrderStatus);
        }
        for (SaleOrderItemStatus saleOrderItemStatus : applicationSetupService.getAllSaleOrderItemStatus()) {
            addSaleOrderItemStatus(saleOrderItemStatus);
        }
    }
}
