package com.uniware.services.cache;

import com.unifier.core.annotation.Cache;
import com.unifier.core.annotation.Level;
import com.unifier.core.api.print.PrintTemplateDTO;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.cache.ICache;
import com.unifier.core.exception.TemplateCompilationException;
import com.unifier.core.template.Template;
import com.unifier.services.printing.IPrintConfigService;
import com.uniware.core.vo.SamplePrintTemplateVO;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Sunny Agarwal
 */
@Cache(type = "samplePrintTemplateCache", level = Level.TENANT)
public class SamplePrintTemplateCache implements ICache {

    private final List<PrintTemplateDTO>                     samplePrintTemplates       = new ArrayList<PrintTemplateDTO>();
    private final Map<String, List<PrintTemplateDTO>>        typeToSamplePrintTemplates = new HashMap<String, List<PrintTemplateDTO>>();
    private final Map<String, Map<String, PrintTemplateDTO>> typeToNameToPrintTemplate  = new HashMap<String, Map<String, PrintTemplateDTO>>();
    private final Map<String, SamplePrintTemplateVO>         codeToSamplePrintTemplate  = new HashMap<String, SamplePrintTemplateVO>();

    @Autowired
    private transient IPrintConfigService                              printConfigService;


    private void addSamplePrintTemplate(SamplePrintTemplateVO samplePrintTemplate) {
        PrintTemplateDTO printTemplateDTO = new PrintTemplateDTO(samplePrintTemplate);
        samplePrintTemplates.add(printTemplateDTO);
        List<PrintTemplateDTO> printTemplates = typeToSamplePrintTemplates.get(samplePrintTemplate.getType());
        if (printTemplates == null) {
            printTemplates = new ArrayList<>();
            typeToSamplePrintTemplates.put(samplePrintTemplate.getType(), printTemplates);
        }
        printTemplates.add(printTemplateDTO);

        Map<String, PrintTemplateDTO> nameToPrintTemplate = typeToNameToPrintTemplate.get(samplePrintTemplate.getType());
        if (nameToPrintTemplate == null) {
            nameToPrintTemplate = new HashMap<>();
            typeToNameToPrintTemplate.put(samplePrintTemplate.getType(), nameToPrintTemplate);
        }
        nameToPrintTemplate.put(samplePrintTemplate.getName(), printTemplateDTO);
        codeToSamplePrintTemplate.put(samplePrintTemplate.getCode(), samplePrintTemplate);
    }

    public List<PrintTemplateDTO> getAllSamplePrintTemplates() {
        return samplePrintTemplates;
    }

    public Map<String, List<PrintTemplateDTO>> getAllSamplePrintTemplatesByType() {
        return typeToSamplePrintTemplates;
    }

    public List<PrintTemplateDTO> getSamplePrintTemplatesByType(String type) {
        return typeToSamplePrintTemplates.get(type);
    }

    public PrintTemplateDTO getPrintTemplateByTypeAndName(String type, String name) {
        Map<String, PrintTemplateDTO> typeToPrintTemplateMap = typeToNameToPrintTemplate.get(type);
        if (typeToPrintTemplateMap != null) {
            return typeToPrintTemplateMap.get(name);
        }
        return null;
    }

    public Template getTemplateByTypeAndName(String type, String name) {
        Map<String, PrintTemplateDTO> typeToPrintTemplateMap = typeToNameToPrintTemplate.get(type);
        if (typeToPrintTemplateMap != null) {
            PrintTemplateDTO printTemplateDTO = typeToPrintTemplateMap.get(name);
            if (printTemplateDTO != null) {
                try {
                    return Template.compile(printTemplateDTO.getName(), printTemplateDTO.getTemplate());
                } catch (TemplateCompilationException ex) {
                }
            }
        }
        return null;
    }

    public SamplePrintTemplateVO getSamplePrintTemplate(String code) {
        SamplePrintTemplateVO samplePrintTemplate =  codeToSamplePrintTemplate.get(code);
        return samplePrintTemplate;
    }

    @Override
    public void load() {
        List<String> templateCodes = new ArrayList<>();
        List<SamplePrintTemplateVO> templates = printConfigService.getSamplePrintTemplates();
        for (SamplePrintTemplateVO template : templates) {
            addSamplePrintTemplate(template);
            templateCodes.add(template.getCode());
        }
        GlobalSamplePrintTemplateCache globalSamplePrintTemplateCache = CacheManager.getInstance().getCache(GlobalSamplePrintTemplateCache.class);
        Map<String, SamplePrintTemplateVO> codeToGlobalSamplePrintTemplate = globalSamplePrintTemplateCache.getAllGlobalSamplePrintTemplatesByCode();
        for (int i=0;i<codeToGlobalSamplePrintTemplate.size();i++) {
            if (!codeToSamplePrintTemplate.containsKey(codeToGlobalSamplePrintTemplate.keySet().toArray()[i])) {
                addSamplePrintTemplate(codeToGlobalSamplePrintTemplate.get(codeToGlobalSamplePrintTemplate.keySet().toArray()[i]));
            }
        }
    }
}
