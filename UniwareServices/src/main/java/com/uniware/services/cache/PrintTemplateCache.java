/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 17-Jan-2012
 *  @author vibhu
 */
package com.uniware.services.cache;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.unifier.core.annotation.Cache;
import com.unifier.core.annotation.Level;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.cache.ICache;
import com.unifier.core.exception.TemplateCompilationException;
import com.unifier.core.template.Template;
import com.unifier.core.utils.StringUtils;
import com.unifier.services.printing.IPrintConfigService;
import com.uniware.core.vo.PrintTemplateVO;
import com.uniware.core.vo.PrintTemplateVO.Type;
import com.uniware.core.vo.SamplePrintTemplateVO;

/**
 * @author vibhu
 */
@Cache(type = "printTemplateCache", level = Level.TENANT)
public class PrintTemplateCache implements ICache {

    private final Map<String, Template>              typeToTemplates                             = new HashMap<>();
    private final Map<String, PrintTemplateVO>       typeToPrintTemplates                        = new HashMap<>();
    private final Map<String, SamplePrintTemplateVO> fileNamePrefixToSelectedSamplePrintTemplate = new HashMap<>();
    private final List<PrintTemplateVO>              printTemplateList                           = new ArrayList<>();
    
    private static final Logger         LOG             = LoggerFactory.getLogger(PrintTemplateCache.class);

    @Autowired
    private transient IPrintConfigService                      printConfigService;


    private void addPrintTemplate(PrintTemplateVO printTemplate) {
        try {
            SamplePrintTemplateCache samplePrintTemplateCache = CacheManager.getInstance().getCache(SamplePrintTemplateCache.class);
            SamplePrintTemplateVO spt = samplePrintTemplateCache.getSamplePrintTemplate(printTemplate.getSamplePrintTemplateCode());
            if (spt != null) {
                if (StringUtils.isNotEmpty(spt.getDataFilenamePrefix())) {
                    fileNamePrefixToSelectedSamplePrintTemplate.put(spt.getDataFilenamePrefix(), spt);
                }
                typeToTemplates.put(printTemplate.getType(), Template.compile(spt.getName(), spt.getTemplate()));
                typeToPrintTemplates.put(printTemplate.getType(), printTemplate);
                printTemplateList.add(printTemplate);
            } else {
                LOG.error("Sample print template not found" + printTemplate.getSamplePrintTemplateCode());
            }
        } catch (TemplateCompilationException ex) {
            LOG.error("Compilation Exception : ", ex);
        }
    }

    public Template getTemplateByType(String type) {
        return typeToTemplates.get(type);
    }

    /**
     * @param templateName
     * @return
     */
    public PrintTemplateVO getPrintTemplateByType(Type templateName) {
        return typeToPrintTemplates.get(templateName.name());
    }

    public PrintTemplateVO getPrintTemplateByName(String templateName) {
        return typeToPrintTemplates.get(templateName);
    }

    public List<PrintTemplateVO> getPrintTemplates() {
        return printTemplateList;
    }

    public SamplePrintTemplateVO getSelectedSamplePrintTemplateByFileNamePrefix(String fileNamePrefix) {
        return fileNamePrefixToSelectedSamplePrintTemplate.get(fileNamePrefix);
    }

    @Override
    public void load() {
        for (PrintTemplateVO template : printConfigService.getAllPrintTemplates()) {
            addPrintTemplate(template);
        }
    }
}
