/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 12, 2011
 *  @author singla
 */
package com.uniware.services.cache;

import com.unifier.core.annotation.Cache;
import com.unifier.core.annotation.Level;
import com.unifier.core.cache.ICache;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.uniware.core.entity.Sequence;
import com.uniware.dao.common.ISequenceGeneratorDao;

/**
 * @author praveeng
 */
@Cache(type = "sequenceCache", level = Level.FACILITY)
public class SequenceCache implements ICache {

    private final Map<String, String> nameToPrefix = new HashMap<String, String>();

    @Autowired
    private transient ISequenceGeneratorDao     sequenceGeneratorDao;

    private void addSequence(Sequence sequence) {
        nameToPrefix.put(sequence.getName(), sequence.getPrefix());
    }

    public String getPrefixByName(String name) {
        return nameToPrefix.get(name);
    }

    @Override
    @Transactional
    public void load() {
        List<Sequence> sequences = sequenceGeneratorDao.getSequences();
        for (Sequence sequence : sequences) {
            addSequence(sequence);
        }
    }

}
