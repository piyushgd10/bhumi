package com.uniware.services.cache;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.unifier.core.annotation.Cache;
import com.unifier.core.annotation.Level;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.cache.ICache;
import com.unifier.core.expressions.Expression;
import com.unifier.core.utils.StringUtils;
import com.uniware.core.api.packer.RegulatoryFormDTO;
import com.uniware.core.cache.LocationCache;
import com.uniware.core.entity.RegulatoryForm;
import com.uniware.services.shipping.IRegulatoryService;

/**
 * @author Sunny Agarwal
 */
@Cache(type = "regulatoryFormCache", level = Level.GLOBAL, eager = true)
public class RegulatoryFormCache implements ICache {

    private final Map<String, Map<String, List<RegulatoryFormDTO>>> stateCodeToGoodsMovementTypeToRegulatoryForms   = new HashMap<String, Map<String, List<RegulatoryFormDTO>>>();
    private final Map<String, Map<String, List<RegulatoryFormDTO>>> countryCodeToGoodsMovementTypeToRegulatoryForms = new HashMap<String, Map<String, List<RegulatoryFormDTO>>>();

    @Autowired
    private transient IRegulatoryService                            regulatoryService;

    private void addRegulatoryForm(RegulatoryForm regulatoryForm) {
        Map<String, List<RegulatoryFormDTO>> goodsMovementTypeToRegulatoryForms;
        if (regulatoryForm.getStateCode() != null) {
            goodsMovementTypeToRegulatoryForms = stateCodeToGoodsMovementTypeToRegulatoryForms.get(regulatoryForm.getStateCode());
            if (goodsMovementTypeToRegulatoryForms == null) {
                goodsMovementTypeToRegulatoryForms = new HashMap<>();
                stateCodeToGoodsMovementTypeToRegulatoryForms.put(regulatoryForm.getStateCode(), goodsMovementTypeToRegulatoryForms);
            }
        } else {
            goodsMovementTypeToRegulatoryForms = countryCodeToGoodsMovementTypeToRegulatoryForms.get(regulatoryForm.getCountryCode());
            if (goodsMovementTypeToRegulatoryForms == null) {
                goodsMovementTypeToRegulatoryForms = new HashMap<>();
                countryCodeToGoodsMovementTypeToRegulatoryForms.put(regulatoryForm.getCountryCode(), goodsMovementTypeToRegulatoryForms);
            }
        }
        List<RegulatoryFormDTO> regulatoryForms = goodsMovementTypeToRegulatoryForms.get(regulatoryForm.getGoodsMovementType());
        if (regulatoryForms == null) {
            regulatoryForms = new ArrayList<>();
            goodsMovementTypeToRegulatoryForms.put(regulatoryForm.getGoodsMovementType(), regulatoryForms);
        }
        regulatoryForms.add(prepareRegulatoryFormDTO(regulatoryForm));
    }

    private RegulatoryFormDTO prepareRegulatoryFormDTO(RegulatoryForm regulatoryForm) {
        RegulatoryFormDTO regulatoryFormDTO = new RegulatoryFormDTO();
        regulatoryFormDTO.setCountryCode(regulatoryForm.getCountryCode());
        if (regulatoryForm.getStateCode() != null) {
            regulatoryFormDTO.setState(CacheManager.getInstance().getCache(LocationCache.class).getStateByCode(regulatoryForm.getStateCode(), regulatoryForm.getCountryCode()).getName());
        }
        regulatoryFormDTO.setName(regulatoryForm.getName());
        regulatoryFormDTO.setCode(regulatoryForm.getCode());
        regulatoryFormDTO.setGoodsMovementType(regulatoryForm.getGoodsMovementType());
        if (StringUtils.isNotBlank(regulatoryForm.getConditionalExpression())) {
            regulatoryFormDTO.setConditionExpression(Expression.compile(regulatoryForm.getConditionalExpression()));
        }
        regulatoryFormDTO.setEnabled(regulatoryForm.isEnabled());
        return regulatoryFormDTO;
    }

    public List<RegulatoryFormDTO> getRegulatoryFormsByCountryCode(String countryCode, String goodsMovementType) {
        Map<String, List<RegulatoryFormDTO>> goodsMovementTypeToRegulatoryForms = countryCodeToGoodsMovementTypeToRegulatoryForms.get(countryCode);
        if (goodsMovementTypeToRegulatoryForms != null) {
            List<RegulatoryFormDTO> regulatoryForms = goodsMovementTypeToRegulatoryForms.get(goodsMovementType);
            if (regulatoryForms != null) {
                return regulatoryForms;
            }
        }
        return Collections.emptyList();
    }

    public List<RegulatoryFormDTO> getStateRegulatoryFormsByStateCode(String stateCode, String goodsMovementType) {
        Map<String, List<RegulatoryFormDTO>> goodsMovementTypeToRegulatoryForms = stateCodeToGoodsMovementTypeToRegulatoryForms.get(stateCode);
        if (goodsMovementTypeToRegulatoryForms != null) {
            List<RegulatoryFormDTO> regulatoryForms = goodsMovementTypeToRegulatoryForms.get(goodsMovementType);
            if (regulatoryForms != null) {
                return regulatoryForms;
            }
        }
        return Collections.emptyList();
    }

    @Override
    public void load() {
        for (RegulatoryForm regulatoryForm : regulatoryService.getAllRegulatoryForms()) {
            addRegulatoryForm(regulatoryForm);
        }
    }
}
