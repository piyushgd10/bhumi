/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 12, 2011
 *  @author singla
 */
package com.uniware.services.cache;

import com.unifier.core.annotation.Cache;
import com.unifier.core.annotation.Level;
import com.unifier.core.cache.ICache;
import com.uniware.core.entity.Section;
import com.uniware.core.entity.Section.SectionCode;
import com.uniware.core.entity.ShelfType;
import com.uniware.services.warehouse.ISectionService;
import com.uniware.services.warehouse.IShelfService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author singla
 */
@Cache(type = "facilityLayoutCache", level = Level.FACILITY)
public class FacilityLayoutCache implements ICache {

    private static final Logger                 LOG            = LoggerFactory.getLogger(FacilityLayoutCache.class);
    private              Map<String, ShelfType> shelfTypeCodes = new HashMap<>();
    private              List<ShelfType>        shelfTypes     = new ArrayList<>();
    private              Map<String, Section>   sectionCodes   = new HashMap<>();
    private final        Map<Integer, Section>  idToSections   = new HashMap<>();
    private              List<Section>          sections       = new ArrayList<>();
    private Section defaultSection;

    @Autowired
    private transient ISectionService sectionService;

    @Autowired
    private transient IShelfService shelfService;

    public Map<String, ShelfType> getShelfTypeCodes() {
        return shelfTypeCodes;
    }

    /**
     * @return the sectionCodes
     */
    public Map<String, Section> getSectionCodes() {
        return sectionCodes;
    }

    /**
     * @param sectionCodes the sectionCodes to set
     */
    public void setSectionCodes(Map<String, Section> sectionCodes) {
        this.sectionCodes = sectionCodes;
    }

    /**
     * @return the sections
     */
    public List<Section> getSections() {
        return sections;
    }

    /**
     * @param sections the sections to set
     */
    public void setSections(List<Section> sections) {
        this.sections = sections;
    }

    /**
     * @return the shelfTypes
     */
    public List<ShelfType> getShelfTypes() {
        return shelfTypes;
    }

    /**
     * @param shelfTypes the shelfTypes to set
     */
    public void setShelfTypes(List<ShelfType> shelfTypes) {
        this.shelfTypes = shelfTypes;
    }

    /**
     * @param shelfTypeCodes the shelfTypeCodes to set
     */
    public void setShelfTypeCodes(Map<String, ShelfType> shelfTypeCodes) {
        this.shelfTypeCodes = shelfTypeCodes;
    }

    /**
     * @param shelfType
     */
    private void addShelfType(ShelfType shelfType) {
        shelfTypes.add(shelfType);
        shelfTypeCodes.put(shelfType.getCode(), shelfType);
    }

    /**
     * @param section
     */
    private void addSection(Section section) {
        sections.add(section);
        if (SectionCode.DEF.name().equals(section.getCode())) {
            defaultSection = section;
        }
        idToSections.put(section.getId(), section);
    }

    public Section getSectionById(int sectionId) {
        return idToSections.get(sectionId);
    }

    public void freeze() {
        if (defaultSection == null) {
            throw new IllegalStateException("Unable to load Facility cache. No default section");
        }
    }

    public Section getDefaultSection() {
        return defaultSection;
    }

    @Override
    public void load() {
        loadShelfTypes();
        loadSections();
    }

    private void loadSections() {
        LOG.info("Loading sections ...");
        List<Section> sections = sectionService.getSections();
        for (Section section : sections) {
            addSection(section);
        }
        LOG.info("Loaded sections... SUCCESSFULLY");
    }

    private void loadShelfTypes() {
        LOG.info("Loading Shelf types...");
        List<ShelfType> shelfTypes = shelfService.getShelfTypes();
        for (ShelfType shelfType : shelfTypes) {
            addShelfType(shelfType);
        }
        LOG.info("Loaded Shelf types... SUCCESSFULLY");
    }

}
