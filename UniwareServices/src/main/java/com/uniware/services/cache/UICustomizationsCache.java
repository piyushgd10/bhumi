/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 12, 2011
 *  @author singla
 */
package com.uniware.services.cache;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.uniware.core.vo.UICustomListVO;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.unifier.core.annotation.Cache;
import com.unifier.core.annotation.Level;
import com.unifier.core.cache.ICache;
import com.unifier.core.ui.SelectItem;
import com.unifier.core.utils.JsonUtils;
import com.unifier.services.application.IApplicationSetupService;

/**
 * @author singla
 */
@Cache(type = "uiCustomizations", level = Level.TENANT)
public class UICustomizationsCache implements ICache {
    private final Map<String, List<SelectItem>> customLists = new HashMap<String, List<SelectItem>>();

    @Autowired
    private transient IApplicationSetupService            applicationSetupService;

    private void addUICustomList(UICustomListVO uiCustomListVO) {
        JsonObject jsonObject = (JsonObject) JsonUtils.stringToJson(uiCustomListVO.getOptions());
        List<SelectItem> options = new ArrayList<SelectItem>();
        for (Entry<String, JsonElement> entry : jsonObject.entrySet()) {
            options.add(new SelectItem(entry.getKey(), entry.getValue().getAsString()));
        }
        customLists.put(uiCustomListVO.getName().toLowerCase(), options);
    }

    public List<SelectItem> getUICustomListOptions(String name) {
        return customLists.get(name.toLowerCase());
    }

    @Override
    public void load() {
        List<UICustomListVO> uiCustomListVOs = applicationSetupService.getUICustomLists();
        for (UICustomListVO uiCustomListVO : uiCustomListVOs) {
            addUICustomList(uiCustomListVO);
        }
    }

}
