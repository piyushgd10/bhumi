/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 10-Jun-2012
 *  @author praveeng
 */
package com.uniware.services.cache;

import java.io.Serializable;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.unifier.core.annotation.Cache;
import com.unifier.core.annotation.Level;
import com.unifier.core.cache.ICache;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.entity.DashboardWidget;
import com.unifier.core.expressions.Expression;
import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.StringUtils;
import com.uniware.core.utils.UserContext;
import com.uniware.services.configuration.DashboardWidgetConfiguration;
import com.uniware.services.dashboard.IDashboardWidgetService;

@Cache(type = "dashboardWidgetCache", level = Level.TENANT)
public class DashboardWidgetCache implements ICache {

    @Autowired
    private IDashboardWidgetService dashboardWidgetService;

    public String getCachedDashboardWidgetData(DashboardCacheKey key) {
        return getCachedDashboardWidgetData(key.getCacheKey());
    }

    public void cacheDashboardWidgetData(DashboardCacheKey key, String data) {
        DashboardWidget dashboardWidget = ConfigurationManager.getInstance().getConfiguration(DashboardWidgetConfiguration.class).getDashboardWidgetByCode(key.getWidgetCode());
        int timeToLive = getCacheTimeToLive(key, dashboardWidget.getSecondsToCache());
        if (timeToLive > 0) {
            dashboardWidgetService.insertDashboardWidgetData(key.getCacheKey(), data, DateUtils.addToDate(DateUtils.getCurrentTime(), Calendar.SECOND, timeToLive));
        }
    }

    public String getCachedDashboardWidgetData(String code) {
        return dashboardWidgetService.getWidgetData(code);
    }

    @Override
    public void load() {

    }

    private int getCacheTimeToLive(DashboardCacheKey objKey, String expressionText) {
        Expression expression = Expression.compile(expressionText);
        Map<String, Object> contextParams = new HashMap<>();
        contextParams.put("key", objKey);
        return expression.evaluate(contextParams, Integer.class);
    }

    public static class DashboardCacheKey implements Serializable {

        private static final long serialVersionUID = -1911027056743875164L;

        private String            widgetCode;
        private String            timeRange;
        private String            facilityId;
        private String            additionalCacheParam;

        public DashboardCacheKey(String widgetCode, String timeRange, String additionalCacheParam) {
            this.widgetCode = widgetCode;
            this.timeRange = timeRange;
            this.facilityId = UserContext.current().getFacilityId() != null ? UserContext.current().getFacilityId().toString() : "";
            this.additionalCacheParam = additionalCacheParam;
        }

        public String getWidgetCode() {
            return widgetCode;
        }

        public String getTimeRange() {
            return timeRange;
        }

        public String getFacilityId() {
            return facilityId;
        }

        public String getCacheKey() {
            StringBuilder cacheKeyBuilder = new StringBuilder(widgetCode).append(timeRange).append(facilityId);
            if (StringUtils.isNotBlank(additionalCacheParam)) {
                cacheKeyBuilder.append(additionalCacheParam);
            }
            return cacheKeyBuilder.toString();
        }

        public String getAdditionalCacheParam() {
            return additionalCacheParam;
        }

        @Override
        public String toString() {
            return "DashboardCacheKey{" + "widgetCode='" + widgetCode + '\'' + ", timeRange='" + timeRange + '\'' + ", facilityId='" + facilityId + '\''
                    + ", additionalCacheParam='" + additionalCacheParam + '\'' + '}';
        }
    }

}
