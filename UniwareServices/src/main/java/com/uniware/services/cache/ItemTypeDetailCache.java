/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 12, 2011
 *  @author singla
 */
package com.uniware.services.cache;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;

import com.unifier.core.annotation.Cache;
import com.unifier.core.annotation.Level;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.cache.ICache;
import com.unifier.core.utils.StringUtils;
import com.uniware.core.api.item.ItemDetailFieldDTO;
import com.uniware.core.entity.ItemDetailField;
import com.uniware.core.entity.ItemType;
import com.uniware.services.catalog.ICatalogService;

/**
 * @author singla
 */
@Cache(type = "itemTypeDetailCache", level = Level.TENANT)
public class ItemTypeDetailCache implements ICache {

    private final Map<String, ItemDetailField>    nameToItemDetailFields    = new HashMap<String, ItemDetailField>();
    private final Map<String, ItemDetailFieldDTO> nameToItemDetailFieldDTOs = new HashMap<String, ItemDetailFieldDTO>();

    @Autowired
    private  transient ICatalogService                       catalogService;

    public void addItemDetailField(ItemDetailField itemDetailField) {
        nameToItemDetailFields.put(StringUtils.normalizeCacheKey(itemDetailField.getName()), itemDetailField);
        nameToItemDetailFields.put(StringUtils.normalizeCacheKey(itemDetailField.getDisplayName()), itemDetailField);
        ItemDetailFieldDTO itemDetailFieldDTO = new ItemDetailFieldDTO(itemDetailField);
        nameToItemDetailFieldDTOs.put(StringUtils.normalizeCacheKey(itemDetailField.getName()), itemDetailFieldDTO);
        nameToItemDetailFieldDTOs.put(StringUtils.normalizeCacheKey(itemDetailField.getDisplayName()), itemDetailFieldDTO);
    }

    public ItemDetailFieldDTO getItemDetailFieldByName(String itemDetailFieldName) {
        return nameToItemDetailFieldDTOs.get(StringUtils.normalizeCacheKey(itemDetailFieldName));
    }

    public List<ItemDetailFieldDTO> getItemDetailFields(ItemType itemType) {
        List<String> itemDetailFieldNames;
        if (itemType.getItemDetailFields() != null) {
            itemDetailFieldNames = itemType.getItemDetailFields();
        } else {
            itemDetailFieldNames = CacheManager.getInstance().getCache(CategoryCache.class).getCategoryById(itemType.getCategory().getId()).getItemDetailFields();
        }
        if (itemDetailFieldNames != null) {
            List<ItemDetailFieldDTO> itemDetailFields = getItemDetailFieldsByName(itemDetailFieldNames);
            return itemDetailFields.size() > 0 ? itemDetailFields : null;
        } else {
            return null;
        }
    }

    public List<ItemDetailFieldDTO> getItemDetailFieldsByName(Collection<String> itemDetailFieldNames) {
        List<ItemDetailFieldDTO> itemDetailFields = new ArrayList<ItemDetailFieldDTO>(itemDetailFieldNames.size());
        for (String detailFieldName : itemDetailFieldNames) {
            detailFieldName = StringUtils.normalizeCacheKey(detailFieldName);
            if (nameToItemDetailFieldDTOs.containsKey(detailFieldName)) {
                itemDetailFields.add(nameToItemDetailFieldDTOs.get(detailFieldName));
            }
        }
        return itemDetailFields;
    }

    public Set<String> getItemDetailFieldNames(ItemType itemType, List<String> additionalFields) {
        List<String> itemDetailFieldNames;
        if (itemType.getItemDetailFields() != null) {
            itemDetailFieldNames = itemType.getItemDetailFields();
        } else {
            itemDetailFieldNames = CacheManager.getInstance().getCache(CategoryCache.class).getCategoryById(itemType.getCategory().getId()).getItemDetailFields();
        }
        Set<String> itemDetailFields = new HashSet<String>();
        if (itemDetailFieldNames != null) {
            for (String detailFieldName : itemDetailFieldNames) {
                detailFieldName = StringUtils.normalizeCacheKey(detailFieldName);
                if (nameToItemDetailFieldDTOs.containsKey(detailFieldName)) {
                    itemDetailFields.add(nameToItemDetailFieldDTOs.get(detailFieldName).getName());
                }
            }
        }
        if (additionalFields != null && additionalFields.size() > 0) {
            for (String detailFieldName : additionalFields) {
                detailFieldName = StringUtils.normalizeCacheKey(detailFieldName);
                if (nameToItemDetailFieldDTOs.containsKey(detailFieldName)) {
                    itemDetailFields.add(nameToItemDetailFieldDTOs.get(detailFieldName).getName());
                }
            }
        }
        return itemDetailFields.size() > 0 ? itemDetailFields : null;
    }

    public Set<String> getItemDetailFieldNames(ItemType itemType) {
        return getItemDetailFieldNames(itemType, null);
    }

    @Override
    public void load() {
        List<ItemDetailField> itemDetailFields = catalogService.getItemDetailFields();
        for (ItemDetailField itemDetailField : itemDetailFields) {
            addItemDetailField(itemDetailField);
        }
    }

}
