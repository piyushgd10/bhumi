/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 12, 2011
 *  @author singla
 */
package com.uniware.services.cache;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.unifier.core.annotation.DistributedCache;
import com.uniware.core.api.catalog.dto.CategorySearchDTO;
import org.springframework.beans.factory.annotation.Autowired;

import com.unifier.core.annotation.Cache;
import com.unifier.core.annotation.Level;
import com.unifier.core.cache.ICache;
import com.uniware.core.entity.Category;
import com.uniware.services.catalog.ICatalogService;

/**
 * @author singla
 */
@Cache(type = "categoryCache", level = Level.TENANT)
public class CategoryCache implements ICache, Serializable {

    private final List<Category>         categories       = new ArrayList<Category>();
    private final Map<Integer, Category> idToCategories   = new HashMap<Integer, Category>();
    private final Map<String, Category>  codeToCategories = new HashMap<String, Category>();

    @Autowired
    private transient ICatalogService catalogService;

    private void addCategory(Category category) {
        categories.add(category);
        idToCategories.put(category.getId(), category);
        codeToCategories.put(category.getCode(), category);
    }

    public Category getCategoryById(int categoryId) {
        return idToCategories.get(categoryId);
    }

    public Category getCategoryByCode(String categoryCode) {
        return codeToCategories.get(categoryCode);
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void freeze() {
        Collections.sort(categories, new Comparator<Category>() {
            @Override
            public int compare(Category arg0, Category arg1) {
                return arg0.getName().compareTo(arg1.getName());
            }
        });
    }

    @Override
    public void load() {
        List<Category> categories = catalogService.getCategories();
        for (Category category : categories) {
            addCategory(category);
        }
    }

}
