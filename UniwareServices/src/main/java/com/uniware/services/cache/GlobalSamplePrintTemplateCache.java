package com.uniware.services.cache;

import com.unifier.core.annotation.Cache;
import com.unifier.core.annotation.Level;
import com.unifier.core.api.print.PrintTemplateDTO;
import com.unifier.core.cache.ICache;
import com.unifier.core.exception.TemplateCompilationException;
import com.unifier.core.template.Template;
import com.unifier.services.printing.IGlobalPrintConfigService;
import com.uniware.core.vo.SamplePrintTemplateVO;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by admin on 9/4/15.
 */
@Cache(type = "globalSamplePrintTemplateCache", level = Level.GLOBAL)
public class GlobalSamplePrintTemplateCache implements ICache {

    private final List<PrintTemplateDTO>                     globalSamplePrintTemplates       = new ArrayList<PrintTemplateDTO>();
    private final Map<String, List<PrintTemplateDTO>>        typeToGlobalSamplePrintTemplates = new HashMap<String, List<PrintTemplateDTO>>();
    private final Map<String, Map<String, PrintTemplateDTO>> typeToNameToPrintTemplate        = new HashMap<String, Map<String, PrintTemplateDTO>>();
    private final Map<String, SamplePrintTemplateVO>         codeToGlobalSamplePrintTemplate  = new HashMap<String, SamplePrintTemplateVO>();

    @Autowired
    private transient IGlobalPrintConfigService globalPrintConfigService;


    private void addGlobalSamplePrintTemplate(SamplePrintTemplateVO samplePrintTemplate) {
        PrintTemplateDTO printTemplateDTO = new PrintTemplateDTO(samplePrintTemplate);
        globalSamplePrintTemplates.add(printTemplateDTO);
        List<PrintTemplateDTO> printTemplates = typeToGlobalSamplePrintTemplates.get(samplePrintTemplate.getType());
        if (printTemplates == null) {
            printTemplates = new ArrayList<>();
            typeToGlobalSamplePrintTemplates.put(samplePrintTemplate.getType(), printTemplates);
        }
        printTemplates.add(printTemplateDTO);

        Map<String, PrintTemplateDTO> nameToPrintTemplate = typeToNameToPrintTemplate.get(samplePrintTemplate.getType());
        if (nameToPrintTemplate == null) {
            nameToPrintTemplate = new HashMap<String, PrintTemplateDTO>();
            typeToNameToPrintTemplate.put(samplePrintTemplate.getType(), nameToPrintTemplate);
        }
        nameToPrintTemplate.put(samplePrintTemplate.getName(), printTemplateDTO);
        codeToGlobalSamplePrintTemplate.put(samplePrintTemplate.getCode(), samplePrintTemplate);
    }

    public List<PrintTemplateDTO> getAllGlobalSamplePrintTemplates() {
        return globalSamplePrintTemplates;
    }

    public Map<String, List<PrintTemplateDTO>> getAllGlobalSamplePrintTemplatesByType() {
        return typeToGlobalSamplePrintTemplates;
    }

    public Map<String, SamplePrintTemplateVO> getAllGlobalSamplePrintTemplatesByCode() {
        return codeToGlobalSamplePrintTemplate;
    }

    public List<PrintTemplateDTO> getGlobalSamplePrintTemplatesByType(String type) {
        return typeToGlobalSamplePrintTemplates.get(type);
    }

    public PrintTemplateDTO getPrintTemplateByTypeAndName(String type, String name) {
        Map<String, PrintTemplateDTO> typeToPrintTemplateMap = typeToNameToPrintTemplate.get(type);
        if (typeToPrintTemplateMap != null) {
            return typeToPrintTemplateMap.get(name);
        }
        return null;
    }

    public Template getTemplateByTypeAndName(String type, String name) {
        Map<String, PrintTemplateDTO> typeToPrintTemplateMap = typeToNameToPrintTemplate.get(type);
        if (typeToPrintTemplateMap != null) {
            PrintTemplateDTO printTemplateDTO = typeToPrintTemplateMap.get(name);
            if (printTemplateDTO != null) {
                try {
                    return Template.compile(printTemplateDTO.getName(), printTemplateDTO.getTemplate());
                } catch (TemplateCompilationException ex) {
                }
            }
        }
        return null;
    }

    public SamplePrintTemplateVO getGlobalSamplePrintTemplate(String code) {
        return codeToGlobalSamplePrintTemplate.get(code);
    }

    @Override
    public void load() {
        List<SamplePrintTemplateVO> templates = globalPrintConfigService.getGlobalSamplePrintTemplates();
        for (SamplePrintTemplateVO template : templates) {
            addGlobalSamplePrintTemplate(template);
        }
    }
}
