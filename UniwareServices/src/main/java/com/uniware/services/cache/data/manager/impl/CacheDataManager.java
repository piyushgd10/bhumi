/*
 *  Copyright 2013 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 11-Nov-2013
 *  @author sunny
 */
package com.uniware.services.cache.data.manager.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.unifier.core.cache.data.manager.ICacheDataManager;
import com.unifier.core.entity.AccessResource;
import com.unifier.core.entity.AccessResourceGroup;
import com.unifier.core.entity.EnvironmentProperty;
import com.unifier.core.entity.FacilityAccessResource;
import com.unifier.core.entity.Role;
import com.unifier.core.entity.RoleAccessResource;
import com.unifier.services.admin.access.IAccessControlService;
import com.unifier.services.application.IApplicationSetupService;
import com.uniware.core.api.metadata.InformationSchemaDTO;
import com.uniware.core.entity.Country;
import com.uniware.core.entity.Facility;
import com.uniware.core.entity.FacilityAllocationRule;
import com.uniware.core.entity.FacilityProfile;
import com.uniware.core.entity.Location;
import com.uniware.core.entity.State;
import com.uniware.core.entity.Tenant;
import com.uniware.core.entity.TenantSetupState;
import com.uniware.services.location.ILocationService;
import com.uniware.services.tenant.ITenantService;
import com.uniware.services.tenant.ITenantSetupService;
import com.uniware.services.warehouse.IFacilityService;

@Component("cacheDataManager")
@Transactional(readOnly = true)
public class CacheDataManager implements ICacheDataManager {

    @Autowired
    private ITenantService tenantService;

    @Autowired
    private ITenantSetupService tenantSetupService;

    @Autowired
    private IFacilityService facilityService;

    @Autowired
    private IAccessControlService accessControlService;

    @Autowired
    private IApplicationSetupService applicationSetupService;

    @Autowired
    private ILocationService     locationService;

    @Override
    public List<Tenant> getAllTenants() {
        return tenantService.getAllTenants();
    }

    @Override
    public List<Facility> getFacilities() {
        return facilityService.getFacilities();
    }
    
    @Override
    public FacilityProfile getFacilityProfileByCode(String code) {
        return facilityService.getFacilityProfileByCode(code);
    }

    @Override
    public List<FacilityAllocationRule> getFacilityAllocationRules() {
        return facilityService.getFacilityAllocationRules();
    }

    @Override
    public List<Role> getRoles() {
        return accessControlService.getRoles();
    }

    @Override
    public List<AccessResourceGroup> getAccessResourceGroups() {
        return accessControlService.getAccessResourceGroups();
    }

    @Override
    public List<FacilityAccessResource> getFacilityAccessResources() {
        return accessControlService.getFacilityAccessResources();
    }

    @Override
    public List<AccessResource> getAccessResources() {
        return accessControlService.getAccessResources();
    }

    @Override
    public List<RoleAccessResource> getRoleAccessResources(String accessResourceName) {
        return accessControlService.getRoleAccessResources(accessResourceName);
    }

    @Override
    public List<State> getStates() {
        return applicationSetupService.getStates();
    }

    @Override
    public List<Country> getCountries() {
        return applicationSetupService.getCountries();
    }

    @Override
    public List<EnvironmentProperty> getEnvironmentProperties() {
        return applicationSetupService.getEnvironmentProperties();
    }

    @Override
    public List<InformationSchemaDTO> getInformationSchema() {

        return tenantSetupService.getInformationSchema();
    }

    @Override
    public List<TenantSetupState> getTenantSetupStateMappings() {

        return tenantSetupService.getTenantSetupStateMappings();
    }

    @Override
    public List<Location> getAllLocations() {
        return locationService.getAllLocations();
    }
}
