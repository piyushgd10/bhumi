/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 16-Mar-2015
 *  @author akshaykochhar
 */
package com.uniware.services.cache;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.unifier.core.annotation.Cache;
import com.unifier.core.annotation.Level;
import com.unifier.core.cache.ICache;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.scraper.sl.runtime.ScraperScript;
import com.uniware.core.vo.ScriptVersionVO;
import com.uniware.services.configuration.ScriptConfiguration;
import com.uniware.services.script.service.IScriptService;

@Cache(type = "scriptVersionedCache", level = Level.TENANT, eager = true)
public class ScriptVersionedCache implements ICache {

    public Map<String, String> nameToVersion   = new HashMap<>();

    @Autowired
    private transient IScriptService     scriptService;

    private void addScriptVersion(ScriptVersionVO scriptVersion) {
        if (!nameToVersion.containsKey(scriptVersion.getName())) {
            nameToVersion.put(scriptVersion.getName(), scriptVersion.getVersion());
        }
    }

    public ScraperScript getScriptByName(String scriptName) {
        String version = nameToVersion.get(scriptName);
        return ConfigurationManager.getInstance().getConfiguration(ScriptConfiguration.class).getScriptByNameAndVersion(scriptName, version);
    }

    public ScraperScript getScriptByName(String scriptName, boolean required) {
        String version = nameToVersion.get(scriptName);
        return ConfigurationManager.getInstance().getConfiguration(ScriptConfiguration.class).getScriptByNameAndVersion(scriptName, version, required);
    }
    
    public String getScriptVersion(String scriptName) {
        return nameToVersion.get(scriptName);
    }

    @Override
    public void load() {
        for (ScriptVersionVO scriptVersion : scriptService.getSciptVersions()) {
            addScriptVersion(scriptVersion);
        }
    }

}
