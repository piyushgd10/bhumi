package com.uniware.services.cache;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.unifier.core.annotation.Cache;
import com.unifier.core.annotation.Level;
import com.unifier.core.cache.ICache;
import com.unifier.core.entity.SearchTypes;
import com.unifier.services.search.IGlobalSearchService;
import com.uniware.core.api.search.SearchTypeDTO;
import com.uniware.core.entity.Product;
import com.uniware.core.entity.Product.Type;

@Cache(type = "searchTypeCache", level = Level.GLOBAL)
public class SearchTypeCache implements ICache {

    private List<SearchTypeDTO>                    searchTypeDTOs             = new ArrayList<>();
    private final Map<String, SearchTypeDTO>       searchTypeToSearchTypeDTO  = new HashMap<>();
    private final Map<String, List<SearchTypeDTO>> productCodeToSearchTypeDTO = new HashMap<>();

    @Autowired
    private transient IGlobalSearchService                   searchService;

    public void addSearchType(SearchTypes searchType) {
        SearchTypeDTO searchTypeDTO = new SearchTypeDTO(searchType);
        searchTypeDTOs.add(searchTypeDTO);
        searchTypeToSearchTypeDTO.put(searchTypeDTO.getCode(), searchTypeDTO);
    }

    public List<SearchTypeDTO> getSearchTypeDTOs() {
        return searchTypeDTOs;
    }

    public SearchTypeDTO getSearchTypeDTOByType(String type) {
        return searchTypeToSearchTypeDTO.get(type);
    }
    
    public List<SearchTypeDTO> getSearchTypeDTOsByProduct(String productCode){
        return productCodeToSearchTypeDTO.get(productCode);
    }

    @Override
    public void load() {

        List<SearchTypes> searchTypes = searchService.getAllSearchTypes();
        for (SearchTypes searchType : searchTypes) {
            addSearchType(searchType);
        }
        
        for (Type productCode : Product.Type.values()){
            List<SearchTypeDTO> productSearchTypes = new ArrayList<>();
            for (SearchTypes searchType : searchService.getAllSearchTypesByProduct(productCode.name())){
                productSearchTypes.add(new SearchTypeDTO(searchType));
            }
            productCodeToSearchTypeDTO.put(productCode.name(), productSearchTypes);
        }
    }
}
