/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 21, 2011
 *  @author singla
 */
package com.uniware.services.cache;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.unifier.core.annotation.Cache;
import com.unifier.core.annotation.Level;
import com.unifier.core.annotation.Parameter;
import com.unifier.core.cache.ICache;

/**
 * @author singla
 */
@Cache(type = "parameterDescriptorCache", level = Level.GLOBAL)
@SuppressWarnings("rawtypes")
public class ParameterDescriptorCache implements ICache {

    private final transient Map<Class, Map<String, Field>> classToParametersMap = new HashMap<Class, Map<String, Field>>();
    private final List<Class>                    classes              = new ArrayList<Class>();

    public void addClass(Class taskClass) {
        if (!classToParametersMap.containsKey(taskClass)) {
            HashMap<String, Field> taskParamMap = new HashMap<>();
            Field[] fields = taskClass.getDeclaredFields();
            for (Field field : fields) {
                if (field.isAnnotationPresent(Parameter.class)) {
                    Parameter pAnnotation = field.getAnnotation(Parameter.class);
                    taskParamMap.put(pAnnotation.value(), field);
                }
            }
            classes.add(taskClass);
            classToParametersMap.put(taskClass, taskParamMap);
        }
    }

    /**
     * @param taskClass
     * @return
     */
    public Map<String, Field> getParamFieldMap(Class taskClass) {
        return classToParametersMap.get(taskClass);
    }

    @Override
    public void load() {

    }
}
