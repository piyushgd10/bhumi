/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Apr 4, 2012
 *  @author praveeng
 */
package com.uniware.services.cache;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.unifier.core.annotation.DistributedCache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.unifier.core.annotation.Cache;
import com.unifier.core.annotation.Level;
import com.unifier.core.cache.ICache;
import com.uniware.core.entity.InflowReceiptStatus;
import com.uniware.core.entity.OutboundGatePassStatus;
import com.uniware.core.entity.PicklistStatus;
import com.uniware.core.entity.PurchaseOrderStatus;
import com.uniware.core.entity.PutawayStatus;
import com.uniware.core.entity.ReturnManifestStatus;
import com.uniware.core.entity.SaleOrderItemStatus;
import com.uniware.core.entity.SaleOrderStatus;
import com.uniware.core.entity.ShippingManifestStatus;
import com.uniware.services.inflow.IInflowService;
import com.uniware.services.material.IMaterialService;
import com.uniware.services.picker.IPickerService;
import com.uniware.services.purchase.IPurchaseService;
import com.uniware.services.putaway.IPutawayService;
import com.uniware.services.returns.IReturnsService;
import com.uniware.services.saleorder.ISaleOrderService;
import com.uniware.services.shipping.IDispatchService;
import com.uniware.services.shipping.IShippingService;

@Cache(type = "statuscache", level = Level.GLOBAL, eager = true)
public class StatusCache implements ICache {
    private static final Logger LOG                      = LoggerFactory.getLogger(StatusCache.class);
    private final List<String>  picklistStatuses         = new ArrayList<String>();
    private final List<String>  orderStatuses            = new ArrayList<String>();
    private final List<String>  saleOrderItemStatuses    = new ArrayList<String>();
    private final List<String>  purchaseOrderStatuses    = new ArrayList<String>();
    private final List<String>  inflowReceiptStatuses    = new ArrayList<String>();
    private final List<String>  shippingManifestStatuses = new ArrayList<String>();
    private final List<String>  putawayStatuses          = new ArrayList<String>();
    private final List<String>  returnManifestStatuses   = new ArrayList<String>();
    private List<String>        gatePassStatuses         = new ArrayList<String>();

    @Autowired
    private transient IPickerService      pickerService;

    @Autowired
    private transient ISaleOrderService   saleOrderService;

    @Autowired
    private transient IPurchaseService    purchaseService;

    @Autowired
    private transient IInflowService      inflowService;

    @Autowired
    private transient IPutawayService     putawayService;

    @Autowired
    private transient IReturnsService     returnsService;

    @Autowired
    private transient IMaterialService    materialService;

    @Autowired
    private transient IDispatchService    dispatchService;

    private void addPicklistStatus(PicklistStatus status) {
        picklistStatuses.add(status.getCode());
    }

    public List<String> getPicklistStatuses() {
        return picklistStatuses;
    }

    private void addOrderStatus(SaleOrderStatus status) {
        orderStatuses.add(status.getCode());
    }

    public List<String> getOrderStatuses() {
        return orderStatuses;
    }

    public List<String> getSaleOrderItemStatuses() {
        return saleOrderItemStatuses;
    }

    private void addOrderItemStatus(SaleOrderItemStatus itemStatus) {
        saleOrderItemStatuses.add(itemStatus.getCode());
    }

    private void addPurchaseOrderStatus(PurchaseOrderStatus purchaseOrderStatus) {
        purchaseOrderStatuses.add(purchaseOrderStatus.getCode());
    }

    public List<String> getPurchaseOrderStatuses() {
        return purchaseOrderStatuses;
    }

    private void addInflowReceiptStatus(InflowReceiptStatus inflowReceiptStatus) {
        inflowReceiptStatuses.add(inflowReceiptStatus.getCode());
    }

    public List<String> getInflowReceiptStatuses() {
        return inflowReceiptStatuses;
    }

    private void addShippingManifestStatus(ShippingManifestStatus shippingManifestStatus) {
        shippingManifestStatuses.add(shippingManifestStatus.getCode());
    }

    public List<String> getShippingManifestStatuses() {
        return shippingManifestStatuses;
    }

    private void addPutawayStatus(PutawayStatus putawayStatus) {
        putawayStatuses.add(putawayStatus.getCode());
    }

    public List<String> getPutawayStatuses() {
        return putawayStatuses;
    }

    private void addReturnManifestStatus(ReturnManifestStatus returnManifestStatus) {
        returnManifestStatuses.add(returnManifestStatus.getCode());
    }

    public List<String> getReturnManifestStatuses() {
        return returnManifestStatuses;
    }

    public List<String> getGatePassStatuses() {
        return gatePassStatuses;
    }

    private void addOutboundGatePassStatus(OutboundGatePassStatus status) {
        gatePassStatuses.add(status.getCode());
    }

    @Override
    public void load() {
        List<PicklistStatus> statuses = pickerService.getPicklistStatuses();
        for (PicklistStatus status : statuses) {
            addPicklistStatus(status);
        }
        LOG.info("Loaded Picklist Statuses... SUCCESSFULLY");

        LOG.info("Loading Order Statuses...");
        List<SaleOrderStatus> orderStatuses = saleOrderService.getOrderStatuses();
        for (SaleOrderStatus status : orderStatuses) {
            addOrderStatus(status);
        }
        List<SaleOrderItemStatus> itemStatuses = saleOrderService.getOrderItemStatuses();
        for (SaleOrderItemStatus itemStatus : itemStatuses) {
            addOrderItemStatus(itemStatus);
        }
        LOG.info("Loaded Order Statuses... SUCCESSFULLY");

        LOG.info("Loading Purchase Order Statuses...");
        List<PurchaseOrderStatus> purchaseOrderStatuses = purchaseService.getPurchaseOrderStatuses();
        for (PurchaseOrderStatus status : purchaseOrderStatuses) {
            addPurchaseOrderStatus(status);
        }
        LOG.info("Loaded Purchase Statuses... SUCCESSFULLY");

        LOG.info("Loading Inflow Receipt Statuses...");
        List<InflowReceiptStatus> inflowReceiptStatuses = inflowService.getInflowReceiptStatuses();
        for (InflowReceiptStatus status : inflowReceiptStatuses) {
            addInflowReceiptStatus(status);
        }
        LOG.info("Loaded Inflow Receipt Statuses... SUCCESSFULLY");

        LOG.info("Loading Shipping Manifest Statuses...");
        List<ShippingManifestStatus> shippingManifestStatuses = dispatchService.getShippingManifestStatuses();
        for (ShippingManifestStatus status : shippingManifestStatuses) {
            addShippingManifestStatus(status);
        }
        LOG.info("Loaded Shipping Manifest Statuses... SUCCESSFULLY");

        LOG.info("Loading Putaway Statuses...");
        List<PutawayStatus> putawayStatuses = putawayService.getPutawayStatuses();
        for (PutawayStatus status : putawayStatuses) {
            addPutawayStatus(status);
        }
        LOG.info("Loaded Putaway Statuses... SUCCESSFULLY");

        LOG.info("Loading Return Manifest Statuses...");
        List<ReturnManifestStatus> returnManifestStatuses = returnsService.getReturnManifestStatuses();
        for (ReturnManifestStatus status : returnManifestStatuses) {
            addReturnManifestStatus(status);
        }
        LOG.info("Loaded Return Manifest Statuses... SUCCESSFULLY");

        LOG.info("Loading OutboundGatePass Statuses...");
        List<OutboundGatePassStatus> outboundGatePassStatuses = materialService.getOutboundGatePassStatuses();
        for (OutboundGatePassStatus status : outboundGatePassStatuses) {
            addOutboundGatePassStatus(status);
        }
        LOG.info("Loaded OutboundGatePass Statuses... SUCCESSFULLY");
    }

}
