/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 18, 2012
 *  @author praveeng
 */
package com.uniware.services.cache;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.unifier.core.annotation.Cache;
import com.unifier.core.annotation.Level;
import com.unifier.core.cache.ICache;
import com.uniware.core.entity.ReshipmentAction;
import com.uniware.core.entity.ReversePickupAction;
import com.uniware.services.returns.IReturnsService;

/**
 * @author praveeng
 */
@Cache(type = "returnsCache", level = Level.GLOBAL, eager = true)
public class ReturnsCache implements ICache {
    private final List<ReshipmentAction>           reshipmentActions          = new ArrayList<ReshipmentAction>();
    private final List<ReversePickupAction>        reversePickupActions       = new ArrayList<ReversePickupAction>();
    private final Map<String, ReshipmentAction>    codeToReshipmentActions    = new HashMap<String, ReshipmentAction>();
    private final Map<String, ReversePickupAction> codeToReversePickupActions = new HashMap<String, ReversePickupAction>();

    @Autowired
    private transient IReturnsService                        returnsService;

    private void addReshipmentActions(ReshipmentAction reshipmentAction) {
        reshipmentActions.add(reshipmentAction);
        codeToReshipmentActions.put(reshipmentAction.getCode(), reshipmentAction);
    }

    private void addReversePickupActions(ReversePickupAction reversePickupAction) {
        reversePickupActions.add(reversePickupAction);
        codeToReversePickupActions.put(reversePickupAction.getCode(), reversePickupAction);
    }

    /**
     * @return the reshipmentActions
     */
    public List<ReshipmentAction> getReshipmentActions() {
        return reshipmentActions;
    }

    /**
     * @return the reshipmentAction
     */
    public ReshipmentAction getReshipmentActionByCode(String code) {
        return codeToReshipmentActions.get(code);
    }

    /**
     * @return the reverse pickup action by code
     */
    public ReversePickupAction getReversePickupActionByCode(String code) {
        return codeToReversePickupActions.get(code);
    }

    @Override
    public void load() {
        List<ReshipmentAction> reshipmentActions = returnsService.getReshipmentActions();
        for (ReshipmentAction action : reshipmentActions) {
            addReshipmentActions(action);
        }

        List<ReversePickupAction> reversePickupActions = returnsService.getReversePickupActions();
        for (ReversePickupAction action : reversePickupActions) {
            addReversePickupActions(action);
        }
    }

}
