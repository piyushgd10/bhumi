/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 12, 2011
 *  @author singla
 */
package com.uniware.services.cache;

import com.unifier.core.annotation.Cache;
import com.unifier.core.annotation.Level;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.cache.ICache;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.utils.StringUtils;
import com.uniware.core.cache.FacilityCache;
import com.uniware.core.cache.LocationCache;
import com.uniware.core.entity.Facility;
import com.uniware.core.entity.FacilityProfile;
import com.uniware.core.entity.Location;
import com.uniware.core.entity.PartyAddressType;
import com.uniware.core.entity.ShippingProvider;
import com.uniware.core.entity.ShippingProviderMethod;
import com.uniware.services.configuration.ShippingConfiguration;
import com.uniware.services.location.ILocationService;
import com.uniware.services.shipping.IShippingProviderService;
import com.uniware.services.warehouse.IFacilityService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.geo.Distance;
import org.springframework.data.mongodb.core.geo.GeoResult;
import org.springframework.data.mongodb.core.geo.GeoResults;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Sunny
 */
@Cache(type = "serviceabilityCache", level = Level.TENANT)
public class ServiceabilityCache implements ICache {

    private static final Logger LOG = LoggerFactory.getLogger(ServiceabilityCache.class);

    private Map<String, Map<Integer, List<PickupFacility>>> pincodeToDistanceToPickupFacilities     = new HashMap<>();
    private Map<String, Set<String>>                        shippingMethodNameToPickupFacilityCodes = new HashMap<>();
    private Map<String, Map<String, BigDecimal>>            pincodeToPincodeToDistance              = new HashMap<>();

    @Autowired
    private transient IFacilityService facilityService;

    @Autowired
    private transient ILocationService locationService;

    @Autowired
    private transient IShippingProviderService shippingProviderService;

    public List<PickupFacility> getPickupFacilitiesWithinDistance(String pincode, Integer distanceInKm) {
        Map<Integer, List<PickupFacility>> distanceToServiceableFacilities = pincodeToDistanceToPickupFacilities.get(pincode);
        if (distanceToServiceableFacilities != null && distanceToServiceableFacilities.containsKey(distanceInKm)) {
            return distanceToServiceableFacilities.get(distanceInKm);
        } else {
            if (distanceToServiceableFacilities == null) {
                distanceToServiceableFacilities = new HashMap<>();
                pincodeToDistanceToPickupFacilities.put(pincode, distanceToServiceableFacilities);
            }
            Location location = CacheManager.getInstance().getCache(LocationCache.class).getLocationByPincode(pincode);
            if (location != null) {
                List<PickupFacility> pickupFacilities = getPickupFacilitiesWithinDistance(location.getPosition(), distanceInKm);
                distanceToServiceableFacilities.put(distanceInKm, pickupFacilities);
                return pickupFacilities;
            } else {
                LOG.warn("Location not found for pincode: {}", pincode);
                distanceToServiceableFacilities.put(distanceInKm, Collections.EMPTY_LIST);
            }
        }
        return Collections.emptyList();
    }

    public BigDecimal getDistanceBetweenPincodes(String originPincode, String destinationPincode) {
        Map<String, BigDecimal> pincodeToDistance = pincodeToPincodeToDistance.get(originPincode);
        if (pincodeToDistance != null && pincodeToDistance.containsKey(destinationPincode)) {
            return pincodeToDistance.get(destinationPincode);
        } else {
            if(pincodeToDistance == null) {
                pincodeToDistance = new HashMap<>();
                pincodeToPincodeToDistance.put(originPincode,pincodeToDistance);
            }
            BigDecimal distance = locationService.getDistanceBetweenPincodes(originPincode, destinationPincode);
            pincodeToDistance.put(destinationPincode,distance);
            return distance;
        }
    }

    public List<PickupFacility> getPickupFacilitiesWithinDistance(double[] position, Integer distanceInKm) {
        GeoResults<FacilityProfile> results = facilityService.getPickupFacilitiesWithinDistance(position, distanceInKm);
        List<PickupFacility> pickupFacilities = new ArrayList<>(results.getContent().size());
        for (GeoResult<FacilityProfile> result : results.getContent()) {
            Facility facility = CacheManager.getInstance().getCache(FacilityCache.class).getFacilityByCode(result.getContent().getFacilityCode());
            if (facility != null) {
                PickupFacility pickupFacility = prepareFacilityDTO(facility, result.getContent(), result.getDistance());
                for (ShippingMethodDTO shippingMethod : pickupFacility.getShippingMethods()) {
                    Set<String> facilityCodes = shippingMethodNameToPickupFacilityCodes.get(shippingMethod.getShippingMethodName());
                    if (facilityCodes == null) {
                        facilityCodes = new HashSet<>();
                    }
                    facilityCodes.add(pickupFacility.getCode());
                    shippingMethodNameToPickupFacilityCodes.put(shippingMethod.getShippingMethodName(), facilityCodes);
                }
                pickupFacilities.add(pickupFacility);
            }
        }
        return pickupFacilities;
    }
   
    public boolean isMethodAvailableInFacility(String shippingMethodName, String facilityCode) {
        Set<String> pickupFacilityCodes = shippingMethodNameToPickupFacilityCodes.get(shippingMethodName);
        if (pickupFacilityCodes != null) {
            return pickupFacilityCodes.contains(facilityCode);
        }
        return false;
    }

    public List<PickupFacility> getPickupFacilitiesWithinDistanceWithShippingMethod(double[] position, Integer distanceInKm, String shippingMethodName) {
        List<PickupFacility> allPickupFacilities = getPickupFacilitiesWithinDistance(position, distanceInKm);
        return validateMethodServiceability(shippingMethodName, allPickupFacilities);
    }
    
    public List<PickupFacility> getPickupFacilitiesWithinDistanceWithShippingMethod(String pincode, Integer distanceInKm, String shippingMethodName) {
        List<PickupFacility> allPickupFacilities = getPickupFacilitiesWithinDistance(pincode, distanceInKm);
        return validateMethodServiceability(shippingMethodName, allPickupFacilities);
    }

    private List<PickupFacility> validateMethodServiceability(String shippingMethodName, List<PickupFacility> allPickupFacilities) {
        if (StringUtils.isNotBlank(shippingMethodName)) {
            List<PickupFacility> pickupFacilities = new ArrayList<>();
            for (PickupFacility pickupFacility : allPickupFacilities) {
                if (isMethodAvailableInFacility(shippingMethodName, pickupFacility.getCode())) {
                    pickupFacilities.add(pickupFacility);
                }
            }
            return pickupFacilities;
        } else {
            return allPickupFacilities;
        }
    }

    private PickupFacility prepareFacilityDTO(Facility facility, FacilityProfile facilityProfile, Distance distance) {
        PickupFacility pickupFacility = new PickupFacility();
        pickupFacility.setId(facility.getId());
        pickupFacility.setCode(facility.getCode());
        pickupFacility.setTitle(facility.getDisplayName());
        pickupFacility.setShortAddress(facility.getPartyAddressByType(PartyAddressType.Code.SHIPPING.name()).getAddressLine1());
        pickupFacility.setDispatchSLA(facilityProfile.getDispatchSLA());
        pickupFacility.setDeliverySLA(facilityProfile.getDeliverySLA());
        ShippingProvider pickupShippingProvider = ConfigurationManager.getInstance().getConfiguration(ShippingConfiguration.class).getShippingProviderByCode(
                ShippingProvider.Code.SELF_PICKUP.name());
        List<ShippingProviderMethod> shippingProviderMethods = shippingProviderService.getShippingProviderMethods(pickupShippingProvider.getId(), facility.getId());
        List<ShippingMethodDTO> shippingMethods = new ArrayList<>();
        for (ShippingProviderMethod shippingProviderMethod : shippingProviderMethods) {
            if (shippingProviderMethod.isEnabled()) {
                ShippingMethodDTO shippingMethodDTO = new ShippingMethodDTO();
                shippingMethodDTO.setPaymentMethodCode(shippingProviderMethod.getShippingMethod().getPaymentMethod().getCode());
                shippingMethodDTO.setShippingMethodCode(shippingProviderMethod.getShippingMethod().getCode());
                shippingMethodDTO.setShippingMethodName(shippingProviderMethod.getShippingMethod().getName());
                shippingMethods.add(shippingMethodDTO);
            }
        }
        pickupFacility.setShippingMethods(shippingMethods);
        BigDecimal dist = new BigDecimal(distance.getValue()).setScale(3, RoundingMode.CEILING);
        pickupFacility.setDistance(dist);
        pickupFacility.setPosition(facilityProfile.getPosition());
        pickupFacility.setLastUpdated(facility.getUpdated());
        return pickupFacility;
    }

    public static class ShippingMethodDTO implements Serializable {
        private String shippingMethodCode;
        private String shippingMethodName;
        private String paymentMethodCode;

        public ShippingMethodDTO() {
        }

        public ShippingMethodDTO(String shippingMethodCode, String paymentMethodCode, String shippingMethodName) {
            this.shippingMethodCode = shippingMethodCode;
            this.paymentMethodCode = paymentMethodCode;
            this.shippingMethodName = shippingMethodName;
        }

        public String getShippingMethodCode() {
            return shippingMethodCode;
        }

        public void setShippingMethodCode(String shippingMethodCode) {
            this.shippingMethodCode = shippingMethodCode;
        }

        public String getPaymentMethodCode() {
            return paymentMethodCode;
        }

        public String getShippingMethodName() {
            return shippingMethodName;
        }

        public void setShippingMethodName(String shippingMethodName) {
            this.shippingMethodName = shippingMethodName;
        }

        public void setPaymentMethodCode(String paymentMethodCode) {
            this.paymentMethodCode = paymentMethodCode;
        }
    }

    public static class PickupFacility implements Serializable {
        private int                     id;
        private String                  code;
        private String                  title;
        private String                  shortAddress;
        private Integer                 dispatchSLA;
        private Integer                 deliverySLA;
        private List<ShippingMethodDTO> shippingMethods;
        private BigDecimal              distance;
        private double[]                position;
        private Date                    lastUpdated;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getShortAddress() {
            return shortAddress;
        }

        public void setShortAddress(String shortAddress) {
            this.shortAddress = shortAddress;
        }

        public Integer getDispatchSLA() {
            return dispatchSLA;
        }

        public void setDispatchSLA(Integer dispatchSLA) {
            this.dispatchSLA = dispatchSLA;
        }

        public Integer getDeliverySLA() {
            return deliverySLA;
        }

        public void setDeliverySLA(Integer deliverySLA) {
            this.deliverySLA = deliverySLA;
        }

        public List<ShippingMethodDTO> getShippingMethods() {
            return shippingMethods;
        }

        public void setShippingMethods(List<ShippingMethodDTO> shippingMethods) {
            this.shippingMethods = shippingMethods;
        }

        public BigDecimal getDistance() {
            return distance;
        }

        public void setDistance(BigDecimal distance) {
            this.distance = distance;
        }

        public double[] getPosition() {
            return position;
        }

        public void setPosition(double[] position) {
            this.position = position;
        }

        public Date getLastUpdated() {
            return lastUpdated;
        }

        public void setLastUpdated(Date lastUpdated) {
            this.lastUpdated = lastUpdated;
        }
    }

    @Override
    public void load() {
    }

}
