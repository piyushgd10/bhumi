/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 15-Feb-2012
 *  @author vibhu
 */
package com.uniware.services.cache;

import com.unifier.core.annotation.Cache;
import com.unifier.core.annotation.Level;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.cache.ICache;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.entity.ChannelProperty;
import com.unifier.scraper.sl.runtime.ScraperScript;
import com.unifier.services.utils.CustomFieldUtils;
import com.uniware.core.api.channel.ChannelAssociatedFacilityDTO;
import com.uniware.core.api.channel.ChannelConnectorDTO;
import com.uniware.core.api.channel.ChannelDetailDTO;
import com.uniware.core.api.channel.ChannelSyncDTO;
import com.uniware.core.entity.Channel;
import com.uniware.core.entity.ChannelAssociatedFacility;
import com.uniware.core.entity.ChannelConnector;
import com.uniware.core.entity.Source;
import com.uniware.core.entity.SourceConnector;
import com.uniware.services.channel.IChannelService;
import com.uniware.services.configuration.SourceConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Sunny
 */
@Cache(type = "channelCache", level = Level.TENANT)
public class ChannelCache implements ICache {

    /**
     * 
     */
    private static final long                    serialVersionUID              = 8407597449302361513L;

    private static final Logger                  LOG                           = LoggerFactory.getLogger(ChannelCache.class);

    private List<ChannelSyncDTO>                 channelSyncDTOs               = new ArrayList<>();
    private Set<ChannelDetailDTO>                channelDetailDTOs             = new HashSet<>();
    private final Map<String, Channel>           channelCodeToChannel          = new HashMap<>();
    private final Map<Integer, Channel>          channelIdToChannel            = new HashMap<>();
    private final Map<String, ChannelDetailDTO>  channelCodeToChannelDetailDTO = new HashMap<>();
    private final Map<Integer, ChannelDetailDTO> channelIdToChannelDetailDTO   = new HashMap<>();
    private Map<String, Map<String, String>>     channelCodeToChannelProperty  = new HashMap<>();

    @Autowired
    private transient IChannelService            channelService;

    private void addChannel(Channel channel) {
        LOG.info("Loading channel: {}, source: {}", channel.getCode(), channel.getSourceCode());
        SourceConfiguration sourceConfiguration = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class);
        Source source = sourceConfiguration.getSourceByCode(channel.getSourceCode());
        if (source != null) {
            ChannelSyncDTO channelDTO = new ChannelSyncDTO(channel, source);
            List<ChannelConnectorDTO> channelConnectors = new ArrayList<>(channel.getChannelConnectors().size());
            for (ChannelConnector cc : channel.getChannelConnectors()) {
                SourceConnector sourceConnector = sourceConfiguration.getSourceConnector(source.getCode(), cc.getSourceConnectorName());
                channelConnectors.add(new ChannelConnectorDTO(cc, sourceConnector));
            }
            channelDTO.setChannelConnectors(channelConnectors);

            if (!channelCodeToChannelProperty.containsKey(channel.getCode())) {
                channelCodeToChannelProperty.put(channel.getCode(), new HashMap<String, String>());
            }
            for (ChannelProperty channelProperty : channel.getChannelProperties()) {
                Map<String, String> property = channelCodeToChannelProperty.get(channel.getCode());
                property.put(channelProperty.getName(), channelProperty.getValue());
                channelCodeToChannelProperty.put(channel.getCode(), property);
            }

            channelSyncDTOs.add(channelDTO);
            ChannelDetailDTO channelDetailDTO = new ChannelDetailDTO(channel, source);
            channelDetailDTO.setChannelConnectors(channelConnectors);
            channelDetailDTO.setAssociatedFacilityCode(channel.getAssociatedFacility() != null ? channel.getAssociatedFacility().getCode() : null);
            List<ChannelAssociatedFacilityDTO> associatedFacilities = new ArrayList<>();
            List<Integer> associatedFacilityIds = new ArrayList<>();
            channel.getAssociatedFacilities().forEach(channelAssociatedFacility -> {
                associatedFacilityIds.add(channelAssociatedFacility.getFacility().getId());
                associatedFacilities.add(new ChannelAssociatedFacilityDTO(channelAssociatedFacility.getFacility()));
            });
            channelDetailDTO.setAssociatedFacilityIds(associatedFacilityIds);
            channelDetailDTO.setAssociatedFacilities(associatedFacilities);
            channelService.prepareChannelConfigurationParametersDTO(channelDetailDTO, channel);
            channelService.prepareSourceConfigurationParametersDTO(channelDetailDTO.getSource(), source);
            channelDetailDTO.setCustomFieldValues(CustomFieldUtils.getCustomFieldValuesDTO(channel));
            channelDetailDTOs.add(channelDetailDTO);
            channelCodeToChannelDetailDTO.put(channel.getCode().toUpperCase(), channelDetailDTO);
            channelIdToChannelDetailDTO.put(channel.getId(), channelDetailDTO);
            channelCodeToChannel.put(channel.getCode(), channel);
            channelIdToChannel.put(channel.getId(), channel);
        } else {
            LOG.error("No source with code {} is present. Ignoring channel: {}", channel.getSourceCode(), channel.getCode());
        }
    }

    public Set<ChannelDetailDTO> getChannels() {
        return channelDetailDTOs;
    }

    public List<ChannelSyncDTO> getChannelSyncDTOs() {
        return channelSyncDTOs;
    }

    public Channel getChannelByName(String channelName) {
        return getChannelByCode(channelName.replaceAll("\\W", "_").toUpperCase());
    }

    public ChannelDetailDTO getChannelDetailDTOByCode(String channelCode) {
        return channelCodeToChannelDetailDTO.get(channelCode.toUpperCase());
    }

    public ChannelDetailDTO getChannelDetailDTOById(Integer channelId) {
        return channelIdToChannelDetailDTO.get(channelId);
    }

    public Channel getChannelByCode(String channelCode) {
        return channelCodeToChannel.get(channelCode.toUpperCase());
    }

    public Channel getChannelById(Integer channelId) {
        return channelIdToChannel.get(channelId);
    }

    public void freeze() {
        Collections.sort(channelSyncDTOs, new Comparator<ChannelSyncDTO>() {
            @Override
            public int compare(ChannelSyncDTO o1, ChannelSyncDTO o2) {
                return o1.getSourceDTO().getPriority() - o2.getSourceDTO().getPriority();
            }
        });
        channelSyncDTOs = Collections.unmodifiableList(channelSyncDTOs);
        channelDetailDTOs = Collections.unmodifiableSet(channelDetailDTOs);
    }

    @Override
    @Transactional(readOnly = true, propagation = Propagation.REQUIRES_NEW)
    public void load() {
        List<Channel> channels = channelService.getAllChannels();
        for (Channel channel : channels) {
            addChannel(channel);
        }

    }

    public ScraperScript getScriptByName(String channelCode, String scriptName) {
        if (channelCodeToChannelProperty.containsKey(channelCode) && channelCodeToChannelProperty.get(channelCode).get(scriptName) != null) {
            return CacheManager.getInstance().getCache(ScriptVersionedCache.class).getScriptByName(channelCodeToChannelProperty.get(channelCode).get(scriptName));
        }
        scriptName = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getScriptName(channelCodeToChannel.get(channelCode).getSourceCode(), scriptName);
        return CacheManager.getInstance().getCache(ScriptVersionedCache.class).getScriptByName(scriptName);
    }

    public ScraperScript getScriptByName(String channelCode, String scriptName, boolean required) {
        if (channelCodeToChannelProperty.containsKey(channelCode) && channelCodeToChannelProperty.get(channelCode).get(scriptName) != null) {
            return CacheManager.getInstance().getCache(ScriptVersionedCache.class).getScriptByName(channelCodeToChannelProperty.get(channelCode).get(scriptName), required);
        }
        scriptName = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getScriptName(channelCodeToChannel.get(channelCode).getSourceCode(), scriptName);
        return CacheManager.getInstance().getCache(ScriptVersionedCache.class).getScriptByName(scriptName, required);
    }

    public String getScriptName(String channelCode, String scriptName) {
        if (channelCodeToChannelProperty.containsKey(channelCode) && channelCodeToChannelProperty.get(channelCode).get(scriptName) != null) {
            return channelCodeToChannelProperty.get(channelCode).get(scriptName);
        }
        return ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getScriptName(channelCodeToChannel.get(channelCode).getSourceCode(), scriptName);
    }

    public int getProcessNotificationCount(String channelCode) {
        Channel channel = getChannelByCode(channelCode);
        String processNotificationCount = channelCodeToChannelProperty.get(channelCode).get(Source.PROCESS_NOTIFICATION_COUNT);
        if (processNotificationCount != null) {
            LOG.info("ProcessNotificationCount: {}", processNotificationCount);
            return Integer.parseInt(processNotificationCount);
        }
        Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(channel.getSourceCode());
        return source.getProcessNotificationCount();
    }

    public int getProcessInventoryCount(String channelCode) {
        Channel channel = getChannelByCode(channelCode);
        String processInventoryCount = channelCodeToChannelProperty.get(channelCode).get(Source.PROCESS_INVENTORY_COUNT);
        if (processInventoryCount != null) {
            LOG.info("ProcessInventoryCount: {}", processInventoryCount);
            return Integer.parseInt(processInventoryCount);
        }
        Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(channel.getSourceCode());
        return source.getProcessInventoryCount();
    }

    public int getProcessPriceCount(String channelCode) {
        Channel channel = getChannelByCode(channelCode);
        String processPriceCount = channelCodeToChannelProperty.get(channelCode).get(Source.PROCESS_PRICE_COUNT);
        if (processPriceCount != null) {
            LOG.info("ProcessPriceCount: {}", processPriceCount);
            return Integer.parseInt(processPriceCount);
        }
        Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(channel.getSourceCode());
        return source.getProcessPriceCount();
    }

}
