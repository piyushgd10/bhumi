/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 26, 2011
 *  @author singla
 */
package com.uniware.services.cache;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ConcurrentMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.annotation.Autowired;

import com.hazelcast.core.HazelcastInstance;
import com.unifier.core.annotation.Cache;
import com.unifier.core.annotation.Level;
import com.unifier.core.cache.ICache;
import com.uniware.core.entity.Picklist;
import com.uniware.core.entity.ShippingPackage;
import com.uniware.core.utils.UserContext;
import com.uniware.services.picker.PickPlanner;

/**
 * @author singla
 */
@Cache(type = "pickerCache", level = Level.FACILITY)
public class PickerCache implements ICache, DisposableBean {

    private static final Logger                 LOG = LoggerFactory.getLogger(PickerCache.class);

    private ConcurrentMap<String, PickPlanner>  pickSetDestinationToPickPlanners;

    @Autowired
    private HazelcastInstance                   hazelcastInstance;

    public void reset() {
        for (PickPlanner pickPlanner : pickSetDestinationToPickPlanners.values()) {
            pickPlanner.reset();
        }
        pickSetDestinationToPickPlanners.clear();
    }

    /**
     * @param pickSetId
     * @param shippingPackages
     */
    public void resetShippingPackages(int pickSetId, List<ShippingPackage> shippingPackages) {
        LOG.info("Found {} packages for pickSet {} for replanning", shippingPackages.size(), pickSetId);
        Optional.ofNullable(pickSetDestinationToPickPlanners.get(getKey(pickSetId, Picklist.Destination.INVOICING))).ifPresent(PickPlanner::reset);
        Optional.ofNullable(pickSetDestinationToPickPlanners.get(getKey(pickSetId, Picklist.Destination.STAGING))).ifPresent(PickPlanner::reset);
        List<PickPlanner.ShippingPackageDTO> multiPickSetPackages = new ArrayList<>();
        List<PickPlanner.ShippingPackageDTO> singlePickSetPackages = new ArrayList<>();
        shippingPackages.forEach(shippingPackage -> {
            Integer itemCount = Math.toIntExact(shippingPackage.getSaleOrderItems().stream().filter(soi -> soi.getSection().getPickSet().getId().equals(pickSetId)).count());
            PickPlanner.ShippingPackageDTO packageDTO = new PickPlanner.ShippingPackageDTO(shippingPackage.getCode(), shippingPackage.getPickSections(),
                    shippingPackage.getPickItems(), shippingPackage.getPriority(), itemCount);
            if (shippingPackage.getSaleOrderItems().size() > itemCount) {
                multiPickSetPackages.add(packageDTO);
            } else {
                singlePickSetPackages.add(packageDTO);
            }
        });
        pickSetDestinationToPickPlanners.put(getKey(pickSetId, Picklist.Destination.INVOICING), new PickPlanner(singlePickSetPackages));
        pickSetDestinationToPickPlanners.put(getKey(pickSetId, Picklist.Destination.STAGING), new PickPlanner(multiPickSetPackages));
    }

    public int getShippingPackagesCount(int pickSetId) {
        return 0;
    }

    public int getPlannedItemsCount(int pickSetId, Picklist.Destination destination) {
        PickPlanner pickPlanner = pickSetDestinationToPickPlanners.get(getKey(pickSetId, destination));
        if (pickPlanner != null) {
            return pickPlanner.getPlannedItemsCount();
        } else {
            return 0;
        }
    }

    /**
     * @param pickSetId
     * @return
     */
    public PickPlanner getPickPlannerByPickSetId(int pickSetId, Picklist.Destination destination) {
        return pickSetDestinationToPickPlanners.get(getKey(pickSetId,destination));
    }

    public void updatePickplanner(int pickSetId, PickPlanner pickPlanner, Picklist.Destination destination) {
        pickSetDestinationToPickPlanners.put(getKey(pickSetId, destination), pickPlanner);
    }

    private String getKey(int pickSetId, Picklist.Destination destination) {
        return destination.name() + "|PickSet:" + pickSetId;
    }

    @Override
    public void load() {
        pickSetDestinationToPickPlanners = hazelcastInstance.getMap("pickSetDestinationToPickPlanners" + UserContext.current().getFacilityId());
    }

    @Override
    public void destroy() throws Exception {
        LOG.info("Shutting down Hazelcast instance..");
        hazelcastInstance.shutdown();
        LOG.info("Done shutting down Hazelcast instance");
    }
}
