/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 12, 2011
 *  @author singla
 */
package com.uniware.services.cache;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import org.springframework.beans.factory.annotation.Autowired;

import com.unifier.core.annotation.Cache;
import com.unifier.core.annotation.Level;
import com.unifier.core.cache.ICache;
import com.unifier.core.utils.LRUCache;
import com.unifier.core.utils.TernarySearchTree;
import com.uniware.core.entity.Vendor;
import com.uniware.core.entity.VendorAgreementStatus;
import com.uniware.services.vendor.IVendorService;

/**
 * @author singla
 */
@Cache(type = "vendorCache", level = Level.FACILITY)
public class VendorCache implements ICache {
    private final TernarySearchTree<VendorVO>        vendorIndexedTree           = new TernarySearchTree<VendorVO>();
    private final Map<Integer, Vendor>               idToVendors                 = new LRUCache<Integer, Vendor>(500);
    private final Map<String, Vendor>                codeToVendors               = new LRUCache<String, Vendor>(500);
    private final Map<String, VendorAgreementStatus> codeToVendorAgreementStatus = new HashMap<String, VendorAgreementStatus>();

    @Autowired
    private transient IVendorService                           vendorService;

    private void addVendor(Vendor vendor, boolean indexVendor) {
        if (vendor.isEnabled()) {
            if (indexVendor) {
                VendorVO vendorVO = new VendorVO(vendor);
                String name = vendor.getName().toLowerCase();
                Scanner scanner = new Scanner(name);
                scanner.useDelimiter("\\W+");
                //        scanner.skip("\\w{1,2}");
                while (scanner.hasNext()) {
                    vendorIndexedTree.put(scanner.next(), vendorVO);
                }
                scanner.close();
                vendorIndexedTree.put(name, vendorVO);
            }
            idToVendors.put(vendor.getId(), vendor);
            codeToVendors.put(vendor.getCode(), vendor);
        }
    }

    private void addVendorAgreementStatus(VendorAgreementStatus status) {
        codeToVendorAgreementStatus.put(status.getCode(), status);
    }

    public List<VendorVO> lookupVendors(String name) {
        List<VendorVO> vendors = new ArrayList<VendorVO>();
        vendors.addAll(vendorIndexedTree.matchPrefix(name.toLowerCase()));
        return vendors;
    }

    public Vendor getVendorById(int vendorId) {
        return idToVendors.get(vendorId);
    }

    public Vendor getVendorByCode(String vendorCode) {
        return codeToVendors.get(vendorCode);
    }

    public static class VendorVO implements Serializable {
        private String name;
        private String code;
        private int    id;

        public VendorVO(Vendor vendor) {
            this.id = vendor.getId();
            this.name = vendor.getName();
            this.code = vendor.getCode();
        }

        /**
         * @return the name
         */
        public String getName() {
            return name;
        }

        /**
         * @param name the name to set
         */
        public void setName(String name) {
            this.name = name;
        }

        /**
         * @param id the id to set
         */
        public void setId(int id) {
            this.id = id;
        }

        /**
         * @return the id
         */
        public int getId() {
            return id;
        }

        /**
         * @return the code
         */
        public String getCode() {
            return code;
        }

        /**
         * @param code the code to set
         */
        public void setCode(String code) {
            this.code = code;
        }
    }

    /**
     * @param vendorAgreementStatus
     * @return
     */
    public VendorAgreementStatus getVendorAgreementStatusByCode(String vendorAgreementStatus) {
        return codeToVendorAgreementStatus.get(vendorAgreementStatus);
    }

    @Override
    public void load() {
        List<Vendor> vendors = vendorService.getAllVendors();
        for (Vendor vendor : vendors) {
            addVendor(vendor, true);
        }

        List<VendorAgreementStatus> statuses = vendorService.getVendorsAgreementStatuses();
        for (VendorAgreementStatus status : statuses) {
            addVendorAgreementStatus(status);
        }
    }

}
