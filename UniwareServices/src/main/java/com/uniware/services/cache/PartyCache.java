/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 15-Feb-2012
 *  @author vibhu
 */
package com.uniware.services.cache;

import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.google.gson.Gson;
import com.unifier.core.annotation.Cache;
import com.unifier.core.annotation.Level;
import com.unifier.core.cache.ICache;
import com.uniware.core.entity.PartyAddressType;
import com.uniware.core.entity.PartyContactType;
import com.uniware.services.party.IPartyService;

/**
 * @author vibhu
 */
@Cache(type = "partyCache", level = Level.TENANT)
public class PartyCache implements ICache {

    private final Map<String, PartyAddressType> addressTypes = new LinkedHashMap<String, PartyAddressType>();
    private final Map<String, PartyContactType> contactTypes = new LinkedHashMap<String, PartyContactType>();
    private String                              addressTypeJson;
    private String                              contactTypeJson;

    @Autowired
    private transient IPartyService                       partyService;

    public void addAddressType(PartyAddressType partyAddressType) {
        addressTypes.put(partyAddressType.getCode(), partyAddressType);
    }

    public void addContactType(PartyContactType partyContactType) {
        contactTypes.put(partyContactType.getCode().toLowerCase().trim(), partyContactType);
    }

    public PartyAddressType getAddressTypeByCode(String code) {
        return addressTypes.get(code);
    }

    public PartyContactType getContactTypeByCode(String code) {
        return contactTypes.get(code.toLowerCase().trim());
    }

    /**
     * @return the addressTypeJson
     */
    public String getAddressTypeJson() {
        return addressTypeJson;
    }

    /**
     * @param addressTypeJson the addressTypeJson to set
     */
    public void setAddressTypeJson(String addressTypeJson) {
        this.addressTypeJson = addressTypeJson;
    }

    /**
     * @return the contactTypeJson
     */
    public String getContactTypeJson() {
        return contactTypeJson;
    }

    /**
     * @param contactTypeJson the contactTypeJson to set
     */
    public void setContactTypeJson(String contactTypeJson) {
        this.contactTypeJson = contactTypeJson;
    }

    public void freeze() {
        Map<String, PartyAddressTypeVO> addressTypeVOs = new LinkedHashMap<String, PartyAddressTypeVO>();
        for (Map.Entry<String, PartyAddressType> entry : addressTypes.entrySet()) {
            PartyAddressTypeVO partyAddressTypeVOs = new PartyAddressTypeVO(entry.getValue());
            addressTypeVOs.put(entry.getValue().getCode(), partyAddressTypeVOs);
        }
        this.addressTypeJson = new Gson().toJson(addressTypeVOs);

        Map<String, PartyContactTypeVO> contactTypeVOs = new LinkedHashMap<String, PartyContactTypeVO>();
        for (Map.Entry<String, PartyContactType> entry : contactTypes.entrySet()) {
            PartyContactTypeVO partyContactTypeVOs = new PartyContactTypeVO(entry.getValue());
            contactTypeVOs.put(entry.getValue().getCode(), partyContactTypeVOs);
        }
        this.contactTypeJson = new Gson().toJson(contactTypeVOs);
    }

    public static class PartyAddressTypeVO {
        private String code;
        private String name;

        public PartyAddressTypeVO(PartyAddressType partyAddressType) {
            this.code = partyAddressType.getCode();
            this.name = partyAddressType.getName();
        }

        /**
         * @return the code
         */
        public String getCode() {
            return code;
        }

        /**
         * @return the name
         */
        public String getName() {
            return name;
        }

    }

    public static class PartyContactTypeVO {
        private String code;
        private String name;

        public PartyContactTypeVO(PartyContactType partyContactType) {
            this.code = partyContactType.getCode();
            this.name = partyContactType.getName();
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

    }

    @Override
    public void load() {
        for (PartyAddressType partyAddressType : partyService.getPartyAddressTypes()) {
            addAddressType(partyAddressType);
        }
        for (PartyContactType partyContactType : partyService.getPartyContactTypes()) {
            addContactType(partyContactType);
        }
    }

}
