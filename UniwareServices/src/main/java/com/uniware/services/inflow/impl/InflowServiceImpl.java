/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 *  @version     1.0, Feb 29, 2012
 *  @author singla
 */
package com.uniware.services.inflow.impl;

import static java.util.Collections.singletonList;

import java.io.File;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;
import com.unifier.core.annotation.audit.LogActivity;
import com.unifier.core.api.validation.ResponseCode;
import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.email.EmailMessage;
import com.unifier.core.template.Template;
import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.DateUtils.DateRange;
import com.unifier.core.utils.FileUtils;
import com.unifier.core.utils.JsonUtils;
import com.unifier.core.utils.NumberUtils;
import com.unifier.core.utils.StringUtils;
import com.unifier.services.aspect.RollbackOnFailure;
import com.unifier.services.email.IEmailService;
import com.unifier.services.pdf.IPdfDocumentService;
import com.unifier.services.pdf.impl.PdfDocumentServiceImpl.PrintOptions;
import com.unifier.services.users.IUsersService;
import com.unifier.services.utils.CustomFieldUtils;
import com.uniware.core.api.currency.GetCurrencyConversionRateRequest;
import com.uniware.core.api.currency.GetCurrencyConversionRateResponse;
import com.uniware.core.api.inflow.AddItemDetailsRequest;
import com.uniware.core.api.inflow.AddItemDetailsResponse;
import com.uniware.core.api.inflow.AddItemToInflowReceiptRequest;
import com.uniware.core.api.inflow.AddItemToInflowReceiptResponse;
import com.uniware.core.api.inflow.AddItemToInflowReceiptWapRequest;
import com.uniware.core.api.inflow.AddItemToInflowReceiptWapResponse;
import com.uniware.core.api.inflow.AddOrEditInflowReceiptItemRequest;
import com.uniware.core.api.inflow.AddOrEditInflowReceiptItemResponse;
import com.uniware.core.api.inflow.AddSaleOrderItemDetailRequest;
import com.uniware.core.api.inflow.AddSaleOrderItemDetailResponse;
import com.uniware.core.api.inflow.CompleteInflowReceiptItemsQCRequest;
import com.uniware.core.api.inflow.CompleteInflowReceiptItemsQCResponse;
import com.uniware.core.api.inflow.CreateInflowReceiptRequest;
import com.uniware.core.api.inflow.CreateInflowReceiptResponse;
import com.uniware.core.api.inflow.CreateReceiptItemCodesRequest;
import com.uniware.core.api.inflow.CreateReceiptItemCodesResponse;
import com.uniware.core.api.inflow.DiscardInflowReceiptRequest;
import com.uniware.core.api.inflow.DiscardInflowReceiptResponse;
import com.uniware.core.api.inflow.DiscardTraceableInflowReceiptItemRequest;
import com.uniware.core.api.inflow.DiscardTraceableInflowReceiptItemResponse;
import com.uniware.core.api.inflow.EditInflowReceiptRequest;
import com.uniware.core.api.inflow.EditInflowReceiptResponse;
import com.uniware.core.api.inflow.GetInflowReceiptItemsRequest;
import com.uniware.core.api.inflow.GetInflowReceiptItemsResponse;
import com.uniware.core.api.inflow.GetInflowReceiptRequest;
import com.uniware.core.api.inflow.GetInflowReceiptResponse;
import com.uniware.core.api.inflow.GetInflowReceiptsRequest;
import com.uniware.core.api.inflow.GetInflowReceiptsResponse;
import com.uniware.core.api.inflow.GetItemDetailFieldsRequest;
import com.uniware.core.api.inflow.GetItemDetailFieldsResponse;
import com.uniware.core.api.inflow.GetPOInflowReceiptsRequest;
import com.uniware.core.api.inflow.GetPOInflowReceiptsResponse;
import com.uniware.core.api.inflow.GetPOInflowReceiptsResponse.POInflowReceipt;
import com.uniware.core.api.inflow.GetRejectionReportRequest;
import com.uniware.core.api.inflow.GetRejectionReportResponse;
import com.uniware.core.api.inflow.InflowReceiptDTO;
import com.uniware.core.api.inflow.InflowReceiptItemDTO;
import com.uniware.core.api.inflow.ItemDetail;
import com.uniware.core.api.inflow.RejectNonTraceableItemRequest;
import com.uniware.core.api.inflow.RejectNonTraceableItemResponse;
import com.uniware.core.api.inflow.RejectTraceableItemRequest;
import com.uniware.core.api.inflow.RejectTraceableItemResponse;
import com.uniware.core.api.inflow.SearchReceiptRequest;
import com.uniware.core.api.inflow.SearchReceiptResponse;
import com.uniware.core.api.inflow.SearchReceiptResponse.WsInflowReceipt;
import com.uniware.core.api.inflow.SendInflowReceiptForQCRequest;
import com.uniware.core.api.inflow.SendInflowReceiptForQCResponse;
import com.uniware.core.api.inflow.SendInflowReceiptItemsForQCRequest;
import com.uniware.core.api.inflow.SendInflowReceiptItemsForQCResponse;
import com.uniware.core.api.inflow.UpdateTraceableInflowReceiptItemRequest;
import com.uniware.core.api.inflow.UpdateTraceableInflowReceiptItemResponse;
import com.uniware.core.api.inflow.WsGRN;
import com.uniware.core.api.inflow.WsInflowReceiptItem;
import com.uniware.core.api.inventory.AddItemLabelsRequest;
import com.uniware.core.api.inventory.AddItemLabelsResponse;
import com.uniware.core.api.item.ItemDetailFieldDTO;
import com.uniware.core.api.purchase.PurchaseOrderDTO;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.entity.Category;
import com.uniware.core.entity.InflowReceipt;
import com.uniware.core.entity.InflowReceiptItem;
import com.uniware.core.entity.InflowReceiptStatus;
import com.uniware.core.entity.Item;
import com.uniware.core.entity.ItemType;
import com.uniware.core.entity.ItemTypeInventory.Type;
import com.uniware.core.entity.PurchaseOrder;
import com.uniware.core.entity.PurchaseOrderItem;
import com.uniware.core.entity.SaleOrderItem;
import com.uniware.core.entity.SaleOrderItem.ItemDetailingStatus;
import com.uniware.core.locking.Namespace;
import com.uniware.core.locking.annotation.Lock;
import com.uniware.core.locking.annotation.Locks;
import com.uniware.core.utils.ActivityContext;
import com.uniware.core.utils.Constants.EmailTemplateType;
import com.uniware.core.utils.EnvironmentProperties;
import com.uniware.core.vo.PrintTemplateVO;
import com.uniware.core.vo.SamplePrintTemplateVO;
import com.uniware.dao.inflow.IInflowDao;
import com.uniware.dao.purchase.IPurchaseDao;
import com.uniware.services.audit.impl.ActivityEntityEnum;
import com.uniware.services.audit.impl.ActivityTypeEnum;
import com.uniware.services.audit.impl.ActivityUtils;
import com.uniware.services.cache.ItemTypeDetailCache;
import com.uniware.services.cache.PrintTemplateCache;
import com.uniware.services.cache.SamplePrintTemplateCache;
import com.uniware.services.catalog.ICatalogService;
import com.uniware.services.configuration.SystemConfiguration;
import com.uniware.services.configuration.SystemConfiguration.TraceabilityLevel;
import com.uniware.services.currency.ICurrencyService;
import com.uniware.services.inflow.IInflowService;
import com.uniware.services.inventory.IInventoryService;
import com.uniware.services.saleorder.ISaleOrderService;

/**
 * @author singla
 */
@Service("inflowService")
public class InflowServiceImpl implements IInflowService {

    private static final Logger LOG = LoggerFactory.getLogger(InflowServiceImpl.class);

    @Autowired
    private IPurchaseDao        purchaseDao;

    @Autowired
    private IInflowDao          inflowDao;

    @Autowired
    private IInventoryService   inventoryService;

    @Autowired
    private IEmailService       emailService;

    @Autowired
    private IUsersService       usersService;

    @Autowired
    private IPdfDocumentService pdfDocumentService;

    @Autowired
    private ISaleOrderService   saleOrderService;

    @Autowired
    private ICurrencyService    currencyService;

    @Autowired
    private ICatalogService     catalogService;

    /* (non-Javadoc)
     * @see com.uniware.services.inflow.IInflowService#receivePurchaseOrder(com.uniware.core.api.inflow.ReceivePurchaseOrderRequest)
     */
    @Override
    @Transactional
    @LogActivity
    public CreateInflowReceiptResponse createInflowReceipt(CreateInflowReceiptRequest request) {
        CreateInflowReceiptResponse response = new CreateInflowReceiptResponse();
        ValidationContext context = request.validate();
        PurchaseOrder purchaseOrder = null;

        if (!context.hasErrors()) {
            WsGRN wsGRN = request.getWsGRN();
            purchaseOrder = purchaseDao.getPurchaseOrderByCode(request.getPurchaseOrderCode(), true);
            if (purchaseOrder == null) {
                context.addError(WsResponseCode.INVALID_PURCHASE_ORDER_CODE, "Invalid Purchase Order Code");
            } else if (!PurchaseOrder.StatusCode.APPROVED.name().equals(purchaseOrder.getStatusCode())) {
                context.addError(WsResponseCode.INVALID_PURCHASE_ORDER_STATE, "Purchase Order is not in APPROVED state");
            } else {
                Date poApproveTime = purchaseOrder.getApproveTime();
                if (purchaseOrder.getAmendedPurchaseOrder() != null) {
                    poApproveTime = purchaseOrder.getAmendedPurchaseOrder().getApproveTime();
                }
                if (!request.isVendorInvoiceDateCheckDisable() && wsGRN.getVendorInvoiceDate().before(DateUtils.addToDate(poApproveTime, Calendar.DATE, -1))) {
                    context.addError(WsResponseCode.INVALID_DATE, "vendor invoice date should come after purchase order approval date");
                } else {
                    InflowReceipt inflowReceipt = new InflowReceipt();
                    inflowReceipt.setPurchaseOrder(purchaseOrder);
                    inflowReceipt.setStatusCode(InflowReceipt.StatusCode.CREATED.name());
                    inflowReceipt.setCreated(DateUtils.getCurrentTime());
                    inflowReceipt.setUser(usersService.getUserWithDetailsById(wsGRN.getUserId()));
                    if (StringUtils.isNotBlank(request.getWsGRN().getCurrencyCode())) {
                        inflowReceipt.setCurrencyCode(request.getWsGRN().getCurrencyCode());
                    } else {
                        inflowReceipt.setCurrencyCode(purchaseOrder.getCurrencyCode());
                    }
                    GetCurrencyConversionRateResponse conversionRateResponse = currencyService.getCurrencyConversionRate(
                            new GetCurrencyConversionRateRequest(inflowReceipt.getCurrencyCode()));
                    if (conversionRateResponse.isSuccessful()) {
                        inflowReceipt.setCurrencyConversionRate(conversionRateResponse.getConversionRate());
                    } else {
                        context.addErrors(conversionRateResponse.getErrors());
                    }

                    if (!context.hasErrors()) {
                        prepareInflowReceipt(context, wsGRN, inflowReceipt);
                    }
                    if (!context.hasErrors()) {
                        inflowReceipt = inflowDao.addInflowReceipt(inflowReceipt);
                        response.setInflowReceiptCode(inflowReceipt.getCode());
                        response.setSuccessful(true);
                        if (ActivityContext.current().isEnable()) {
                            ActivityUtils.appendActivity(inflowReceipt.getCode(), ActivityEntityEnum.PURCHASE_ORDER.getName(), null,
                                    Arrays.asList(new String[] {
                                            "GRN {" + ActivityEntityEnum.INFLOW_RECEIPT + ":" + inflowReceipt.getCode() + "} for {" + ActivityEntityEnum.PURCHASE_ORDER + ":"
                                                    + purchaseOrder.getCode() + "} created" }),
                                    ActivityTypeEnum.CREATION.name());
                        }
                    }
                }
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    private void prepareInflowReceipt(ValidationContext context, WsGRN wsGRN, InflowReceipt inflowReceipt) {
        inflowReceipt.setVendorInvoiceNumber(wsGRN.getVendorInvoiceNumber());
        inflowReceipt.setVendorInvoiceDate(wsGRN.getVendorInvoiceDate());
        inflowReceipt.setUpdated(DateUtils.getCurrentTime());
        if (wsGRN.getCustomFieldValues() != null) {
            CustomFieldUtils.setCustomFieldValues(inflowReceipt, CustomFieldUtils.getCustomFieldValues(context, InflowReceipt.class.getName(), wsGRN.getCustomFieldValues()));
        }

    }

    @Override
    @Transactional
    public EditInflowReceiptResponse editInflowReceipt(EditInflowReceiptRequest request) {
        EditInflowReceiptResponse response = new EditInflowReceiptResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            WsGRN wsGRN = request.getWsGRN();
            InflowReceipt inflowReceipt = inflowDao.getInflowReceiptByCode(request.getInflowReceiptCode());
            if (inflowReceipt == null) {
                context.addError(WsResponseCode.INVALID_INFLOW_RECEIPT_CODE, "Invalid Inflow Receipt Code");
            } else {
                prepareInflowReceipt(context, wsGRN, inflowReceipt);
                inflowReceipt.setUpdated(DateUtils.getCurrentTime());
                inflowReceipt = inflowDao.updateInflowReceipt(inflowReceipt);
                response.setSuccessful(true);
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    public DiscardInflowReceiptResponse discardInflowReceipt(DiscardInflowReceiptRequest request) {
        DiscardInflowReceiptResponse response = new DiscardInflowReceiptResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            InflowReceipt inflowReceipt = inflowDao.getInflowReceiptByCode(request.getInflowReceiptCode());
            inflowReceipt = inflowDao.getInflowReceiptById(inflowReceipt.getId(), true);
            if (inflowReceipt == null) {
                context.addError(WsResponseCode.INVALID_INFLOW_RECEIPT_ID, "Invalid inflow receipt id");
            } else if (!InflowReceipt.StatusCode.CREATED.name().equals(inflowReceipt.getStatusCode())) {
                context.addError(WsResponseCode.INVALID_INFLOW_RECEIPT_STATE, "Inflow Receipt not in CREATED state");
            } else if (!inflowReceipt.getUser().getId().equals(request.getUserId())) {
                context.addError(WsResponseCode.INVALID_USER_ID, "inflow receipt created by another user");
            } else {
                for (InflowReceiptItem inflowReceiptItem : inflowReceipt.getInflowReceiptItems()) {
                    if (inflowReceiptItem.getQuantity() > 0) {
                        context.addError(WsResponseCode.INVALID_INFLOW_RECEIPT_STATE, "Items has already been added to Inflow Receipt");
                        break;
                    }
                }
                if (!context.hasErrors()) {
                    inflowReceipt.setStatusCode(InflowReceipt.StatusCode.DISCARDED.name());
                    for (InflowReceiptItem inflowReceiptItem : inflowReceipt.getInflowReceiptItems()) {
                        inflowReceiptItem.setStatusCode(InflowReceiptItem.StatusCode.DISCARDED.name());
                    }
                    response.setSuccessful(true);
                }
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    /**
     * Used for editing {@link InflowReceiptItem} of a {@link InflowReceipt}.
     * 
     * @param request
     * @return
     */
    @Transactional
    @Override
    @LogActivity
    public AddOrEditInflowReceiptItemResponse addOrEditInflowReceiptItem(AddOrEditInflowReceiptItemRequest request) {
        AddOrEditInflowReceiptItemResponse response = new AddOrEditInflowReceiptItemResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            PurchaseOrder purchaseOrder = inflowDao.getPurchaseOrderByInflowReceiptCode(request.getInflowReceiptCode(), true);
            if (purchaseOrder == null) {
                context.addError(WsResponseCode.INVALID_PURCHASE_ORDER_ID, "Invalid purchase order id");
            } else if (!PurchaseOrder.StatusCode.APPROVED.name().equals(purchaseOrder.getStatusCode())) {
                context.addError(WsResponseCode.INVALID_PURCHASE_ORDER_STATE, "Purchase Order is not in APPROVED state");
            } else {
                InflowReceipt inflowReceipt = inflowDao.getInflowReceiptByCode(request.getInflowReceiptCode());
                if (!InflowReceipt.StatusCode.CREATED.name().equals(inflowReceipt.getStatusCode())) {
                    context.addError(WsResponseCode.INVALID_INFLOW_RECEIPT_STATE, "Items can be added to receipt only in CREATED state");
                } else {
                    PurchaseOrderItem purchaseOrderItem = null;
                    for (PurchaseOrderItem poi : purchaseOrder.getPurchaseOrderItems()) {
                        if (poi.getId().equals(request.getPurchaseOrderItemId())) {
                            purchaseOrderItem = poi;
                            break;
                        }
                    }
                    if (purchaseOrderItem == null) {
                        context.addError(WsResponseCode.INVALID_PURCHASE_ORDER_CODE, "No purchase order item found for given SKU");
                    } else {
                        InflowReceiptItem inflowReceiptItem = null;
                        int previousQuantity = 0;
                        if (isAllowSingleSkuMultiBatchAtGRN()) {
                            if (request.getInflowReceiptItemId() != null) {
                                for (InflowReceiptItem iri : inflowReceipt.getInflowReceiptItems()) {
                                    if (iri.getId().equals(request.getInflowReceiptItemId())) {
                                        inflowReceiptItem = iri;
                                        previousQuantity = iri.getQuantity();
                                    }
                                }
                            }
                        } else {
                            for (InflowReceiptItem iri : inflowReceipt.getInflowReceiptItems()) {
                                if (iri.getPurchaseOrderItem().getId().equals(request.getPurchaseOrderItemId())) {
                                    inflowReceiptItem = iri;
                                    previousQuantity = iri.getQuantity();
                                }
                            }
                        }
                        if (inflowReceiptItem != null && inflowReceiptItem.isItemsLabelled() && inflowReceiptItem.getQuantity() != request.getInflowReceiptItem().getQuantity()) {
                            context.addError(WsResponseCode.EDIT_NOT_ALLOWED_AFTER_ITEMS_LABELLED, "quantity cannot be changed after items are labelled");
                        } else if (inflowReceiptItem != null && !InflowReceiptItem.StatusCode.CREATED.name().equals(inflowReceiptItem.getStatusCode())) {
                            context.addError(WsResponseCode.INVALID_INFLOW_RECEIPT_ITEM_STATE, "quantity cannot be changed after item sent to QC");
                        } else if (request.getInflowReceiptItem().getQuantity() - previousQuantity > purchaseOrderItem.getQuantity() - purchaseOrderItem.getReceivedQuantity()) {
                            context.addError(WsResponseCode.RECEIVED_QUANTITY_CANNOT_EXCEED_PENDING_QUANTITY, "Receiving quantity exceeds total pending quantity");
                        } else {
                            boolean expirable = catalogService.isItemTypeExpirable(purchaseOrderItem.getItemType());
                            if (expirable && (request.getInflowReceiptItem().getManufacturingDate() == null)) {
                                context.addError(WsResponseCode.INVALID_REQUEST, "Manufacturing Date cant be null for expirable products");
                            } else {
                                if (inflowReceiptItem == null) {
                                    inflowReceiptItem = new InflowReceiptItem();
                                    inflowReceiptItem.setInflowReceipt(inflowReceipt);
                                    prepareInflowReceiptItem(inflowReceiptItem, purchaseOrderItem, request.getInflowReceiptItem());
                                    inflowReceiptItem.setCreated(DateUtils.getCurrentTime());
                                    inflowReceiptItem.setStatusCode(InflowReceiptItem.StatusCode.CREATED.name());
                                    inflowReceiptItem = inflowDao.addInflowReceiptItem(inflowReceiptItem);
                                } else {
                                    if (!inflowReceiptItem.getAgeingStartDate().equals(request.getInflowReceiptItem().getManufacturingDate())) {
                                        List<Item> items = getItemsByReceiptItemId(inflowReceiptItem.getId());
                                        items.forEach(item -> {
                                            Date newManufacturingDate = request.getInflowReceiptItem().getManufacturingDate();
                                            item.setManufacturingDate(newManufacturingDate);
                                            if (newManufacturingDate != null) {
                                                item.setAgeingStartDate(newManufacturingDate);
                                            }
                                            inventoryService.updateItem(item);
                                            if (ActivityContext.current().isEnable()) {
                                                ActivityUtils.appendActivity(inflowReceipt.getCode(),
                                                        ActivityEntityEnum.ITEM.getName(), null, Arrays.asList("Item {" + ActivityEntityEnum.ITEM + ":" + item.getCode()
                                                                + "} manufacturing date changed to  : " + item.getAgeingStartDate() + "by GRN item update"),
                                                        ActivityTypeEnum.EDIT.name());
                                            }
                                        });
                                    }
                                    prepareInflowReceiptItem(inflowReceiptItem, purchaseOrderItem, request.getInflowReceiptItem());
                                    inflowReceiptItem = inflowDao.updateInflowReceiptItem(inflowReceiptItem);
                                }
                                purchaseDao.updatePuchaseOrderItem(purchaseOrderItem);
                                response.setInflowReceiptItemDTO(prepareInflowReceiptItemDTO(inflowReceiptItem, purchaseOrderItem));
                                response.setSuccessful(true);
                            }
                        }
                    }
                }
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    private boolean isAllowSingleSkuMultiBatchAtGRN() {
        return ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).isAllowSingleSkuMultiBatchAtGRN();
    }

    @Override
    public void prepareInflowReceiptItem(InflowReceiptItem inflowReceiptItem, PurchaseOrderItem purchaseOrderItem, WsInflowReceiptItem wsInflowReceiptItem) {
        inflowReceiptItem.setItemType(purchaseOrderItem.getItemType());
        inflowReceiptItem.setPurchaseOrderItem(purchaseOrderItem);
        inflowReceiptItem.setManufacturingDate(wsInflowReceiptItem.getManufacturingDate());
        inflowReceiptItem.setMaxRetailPrice(wsInflowReceiptItem.getMaxRetailPrice() != null ? wsInflowReceiptItem.getMaxRetailPrice()
                : (inflowReceiptItem.getMaxRetailPrice() != null ? inflowReceiptItem.getMaxRetailPrice() : purchaseOrderItem.getMaxRetailPrice()));
        if (wsInflowReceiptItem.getAdditionalCost() != null) {
            inflowReceiptItem.setAdditionalCost(wsInflowReceiptItem.getAdditionalCost());
        }
        inflowReceiptItem.setBatchCode(wsInflowReceiptItem.getBatchCode());
        purchaseOrderItem.setReceivedQuantity(purchaseOrderItem.getReceivedQuantity() + wsInflowReceiptItem.getQuantity() - inflowReceiptItem.getQuantity());
        inflowReceiptItem.setQuantity(wsInflowReceiptItem.getQuantity());
        if (ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).isVendorPricesTaxExclusive()) {
            inflowReceiptItem.setUnitPrice(wsInflowReceiptItem.getUnitPrice() != null ? wsInflowReceiptItem.getUnitPrice()
                    : (inflowReceiptItem.getUnitPrice() != null ? inflowReceiptItem.getUnitPrice() : purchaseOrderItem.getUnitPrice()));
            if (wsInflowReceiptItem.getDiscount() != null) {
                inflowReceiptItem.setDiscount(wsInflowReceiptItem.getDiscount());
            } else {
                inflowReceiptItem.setDiscount(purchaseOrderItem.getDiscount());
            }
        } else {
            BigDecimal taxPercentage = NumberUtils.getPercentageFromBaseAndPercentageAmount(purchaseOrderItem.getSubtotal(),
                    purchaseOrderItem.getTotal().subtract(purchaseOrderItem.getSubtotal()));
            if (wsInflowReceiptItem.getUnitPrice() != null) {
                inflowReceiptItem.setUnitPrice(wsInflowReceiptItem.getUnitPrice().subtract(NumberUtils.getPercentageFromTotal(wsInflowReceiptItem.getUnitPrice(), taxPercentage)));
            } else if (inflowReceiptItem.getUnitPrice() == null) {
                inflowReceiptItem.setUnitPrice(purchaseOrderItem.getUnitPrice());
            }
            if (wsInflowReceiptItem.getDiscount() != null) {
                inflowReceiptItem.setDiscount(wsInflowReceiptItem.getDiscount().subtract(NumberUtils.getPercentageFromTotal(wsInflowReceiptItem.getDiscount(), taxPercentage)));
            } else {
                inflowReceiptItem.setDiscount(purchaseOrderItem.getDiscount());
            }
        }
        inflowReceiptItem.setSubtotal(inflowReceiptItem.getUnitPrice().subtract(inflowReceiptItem.getDiscount()).multiply(new BigDecimal(inflowReceiptItem.getQuantity())).setScale(
                2, RoundingMode.HALF_EVEN));
        prepareinflowReceiptItemTaxation(inflowReceiptItem, purchaseOrderItem);
    }

    /**
     * Sets the tax related fields of {@code inflowReceiptItem} from {@code purchaseOrderItem}. Handles both GST and
     * non-GST cases.
     */
    @Override
    public void prepareinflowReceiptItemTaxation(InflowReceiptItem inflowReceiptItem, PurchaseOrderItem purchaseOrderItem) {
        if (NumberUtils.greaterThan(purchaseOrderItem.getCst(), BigDecimal.ZERO)) {
            BigDecimal cstPercentage = NumberUtils.getPercentageFromBaseAndPercentageAmount(purchaseOrderItem.getSubtotal(), purchaseOrderItem.getCst());
            inflowReceiptItem.setCst(NumberUtils.getPercentageFromBase(inflowReceiptItem.getSubtotal(), cstPercentage));
        } else if (NumberUtils.greaterThan(purchaseOrderItem.getVat(), BigDecimal.ZERO)) {
            BigDecimal vatPercentage = NumberUtils.getPercentageFromBaseAndPercentageAmount(purchaseOrderItem.getSubtotal(), purchaseOrderItem.getVat());
            inflowReceiptItem.setVat(NumberUtils.getPercentageFromBase(inflowReceiptItem.getSubtotal(), vatPercentage));
        }
        if (NumberUtils.greaterThan(purchaseOrderItem.getAdditionalTax(), BigDecimal.ZERO)) {
            BigDecimal additionalTaxPercentage = purchaseOrderItem.getAdditionalTaxPercentage();
            inflowReceiptItem.setAdditionalTax(NumberUtils.getPercentageFromBase(inflowReceiptItem.getSubtotal(), additionalTaxPercentage));
        }
        inflowReceiptItem.setCentralGst(NumberUtils.getPercentageFromBase(inflowReceiptItem.getSubtotal(), purchaseOrderItem.getCentralGstPercentage()));
        inflowReceiptItem.setStateGst(NumberUtils.getPercentageFromBase(inflowReceiptItem.getSubtotal(), purchaseOrderItem.getStateGstPercentage()));
        inflowReceiptItem.setUnionTerritoryGst(NumberUtils.getPercentageFromBase(inflowReceiptItem.getSubtotal(), purchaseOrderItem.getUnionTerritoryGstPercentage()));
        inflowReceiptItem.setIntegratedGst(NumberUtils.getPercentageFromBase(inflowReceiptItem.getSubtotal(), purchaseOrderItem.getIntegratedGstPercentage()));
        inflowReceiptItem.setCompensationCess(NumberUtils.getPercentageFromBase(inflowReceiptItem.getSubtotal(), purchaseOrderItem.getCompensationCessPercentage()));
        BigDecimal totalTaxAmount = inflowReceiptItem.getVat().add(inflowReceiptItem.getCst()).add(inflowReceiptItem.getCentralGst()).add(inflowReceiptItem.getStateGst()).add(
                inflowReceiptItem.getUnionTerritoryGst()).add(inflowReceiptItem.getIntegratedGst()).add(inflowReceiptItem.getCompensationCess());
        inflowReceiptItem.setTotal(inflowReceiptItem.getSubtotal().add(totalTaxAmount));
    }

    @Override
    @Transactional
    public CreateReceiptItemCodesResponse createItemCodes(CreateReceiptItemCodesRequest request) {
        CreateReceiptItemCodesResponse response = new CreateReceiptItemCodesResponse();
        ValidationContext context = request.validate();
        if (ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).isBarcodingBeforeGRN()) {
            context.addError(WsResponseCode.ACTION_NOT_APPLICABLE_FOR_BARCODING_BEFORE_GRN_CONFIGURATION, "Action not applicable with barcode before GRN configuration");
        }
        if (!context.hasErrors()) {
            InflowReceiptItem inflowReceiptItem = inflowDao.getInflowReceiptItemById(request.getInflowReceiptItemId(), true);
            if (inflowReceiptItem == null) {
                context.addError(WsResponseCode.INVALID_INFLOW_RECEIPT_ITEM_ID, "Invalid inflow receipt item id");
            } else if (inflowReceiptItem.isItemsLabelled()) {
                context.addError(WsResponseCode.ITEM_CODES_ALREADY_CREATED, "Items already created");
            } else if (ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).getTraceabilityLevel() != TraceabilityLevel.ITEM) {
                context.addError(WsResponseCode.ACTION_APPLICABLE_FOR_TRACEABLE_ITEMS, "Item codes cannot be generated only with ITEM level traceability");
            } else if ((ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).isBarcodingBeforeQualityCheck()
                    && !StringUtils.equalsAny(inflowReceiptItem.getInflowReceipt().getStatusCode(), InflowReceipt.StatusCode.CREATED.name(),
                            InflowReceipt.StatusCode.QC_PENDING.name()))
                    || (!ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).isBarcodingBeforeQualityCheck()
                            && !InflowReceipt.StatusCode.QC_COMPLETE.name().equals(inflowReceiptItem.getInflowReceipt().getStatusCode()))) {
                context.addError(WsResponseCode.INVALID_INFLOW_RECEIPT_STATE);
            } else {
                //Generate Item entities for Received Goods
                PurchaseOrderItem purchaseOrderItem = inflowReceiptItem.getPurchaseOrderItem();
                inventoryService.addItems(purchaseOrderItem.getItemType(), inflowReceiptItem.getInflowReceipt().getPurchaseOrder().getVendor(),
                        inflowReceiptItem.getQuantity() - inflowReceiptItem.getRejectedQuantity(), inflowReceiptItem,
                        InflowReceiptItem.StatusCode.QC_PENDING.name().equals(inflowReceiptItem.getInflowReceipt().getStatusCode()) ? Item.StatusCode.QC_PENDING
                                : (InflowReceiptItem.StatusCode.QC_COMPLETE.name().equals(inflowReceiptItem.getInflowReceipt().getStatusCode()) ? Item.StatusCode.QC_COMPLETE
                                        : Item.StatusCode.CREATED));
                inflowReceiptItem.setItemsLabelled(true);
                inflowDao.updateInflowReceiptItem(inflowReceiptItem);
                response.setSuccessful(true);
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    /**
     * Adds item to {@link InflowReceipt} (GRN) for a particular {@link PurchaseOrder} aka PO.
     * <ul>
     * Used by people who have:
     * <li>{@link SystemConfiguration#isBarcodingBeforeGRN()} as TRUE, i.e, they get pre-barcoded items at the time of
     * GRN.</li>
     * <li>{@link SystemConfiguration#getTraceabilityLevel()} == {@link TraceabilityLevel#ITEM}</li>
     * </ul>
     * 
     * @param request contains the inflow receipt code, item code and manufacturing date of item (if it exists).
     * @return
     */
    @Transactional
    @Override
    @Locks(@Lock(ns = Namespace.ITEM, key = "#{#args[0].itemCode}"))
    public AddItemToInflowReceiptResponse addItemToInflowReceipt(AddItemToInflowReceiptRequest request) {
        AddItemToInflowReceiptResponse response = new AddItemToInflowReceiptResponse();
        ValidationContext context = request.validate();
        if (!ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).isBarcodingBeforeGRN()) {
            context.addError(WsResponseCode.ACTION_ONLY_APPLICABLE_FOR_BARCODING_BEFORE_GRN_CONFIGURATION, "Action only applicable with barcode before GRN configuration");
        }
        if (!context.hasErrors()) {
            PurchaseOrder purchaseOrder = inflowDao.getPurchaseOrderByInflowReceiptCode(request.getInflowReceiptCode(), true);
            if (purchaseOrder == null) {
                context.addError(WsResponseCode.INVALID_PURCHASE_ORDER_CODE, "Invalid purchase order code");
            } else if (!PurchaseOrder.StatusCode.APPROVED.name().equals(purchaseOrder.getStatusCode())) {
                context.addError(WsResponseCode.INVALID_PURCHASE_ORDER_STATE, "Purchase Order is not in APPROVED state");
            } else {
                Item item = inventoryService.getItemByCode(request.getItemCode());
                validateItemWhenAddingToInflowReceipt(item, context);
                if (!context.hasErrors()) {
                    boolean expirable = catalogService.isItemTypeExpirable(item.getItemType());
                    if (expirable) {
                        validateAndSetManufacturingForExpirableItem(item, context, request.getManufacturingDate());
                    }
                }
                if (!context.hasErrors()) {
                    InflowReceipt inflowReceipt = inflowDao.getInflowReceiptByCode(request.getInflowReceiptCode());
                    if (!InflowReceipt.StatusCode.CREATED.name().equals(inflowReceipt.getStatusCode())) {
                        context.addError(WsResponseCode.INVALID_INFLOW_RECEIPT_STATE, "Items can be added to receipt only in CREATED state");
                    } else {
                        PurchaseOrderItem purchaseOrderItem = null;
                        for (PurchaseOrderItem poi : purchaseOrder.getPurchaseOrderItems()) {
                            if (poi.getItemType().getId().equals(item.getItemType().getId())) {
                                purchaseOrderItem = poi;
                                break;
                            }
                        }
                        if (purchaseOrderItem == null) {
                            context.addError(WsResponseCode.INVALID_PURCHASE_ORDER_ITEM_ID, "No purchase order item found for given SKU");
                        } else if (purchaseOrderItem.getQuantity() - purchaseOrderItem.getReceivedQuantity() == 0) {
                            context.addError(WsResponseCode.RECEIVED_QUANTITY_CANNOT_EXCEED_PENDING_QUANTITY, "Receiving quantity exceeds total pending quantity");
                        } else {
                            InflowReceiptItem inflowReceiptItem = null;
                            boolean expirable = catalogService.isItemTypeExpirable(item.getItemType());
                            for (InflowReceiptItem iri : inflowReceipt.getInflowReceiptItems()) {
                                if (iri.getItemType().getId().equals(item.getItemType().getId())) {
                                    if (!expirable || (expirable && iri.getManufacturingDate() != null && iri.getManufacturingDate().equals(request.getManufacturingDate()))) {
                                        inflowReceiptItem = iri;
                                    } else if (!isAllowSingleSkuMultiBatchAtGRN()) {
                                        context.addError(WsResponseCode.INVALID_INFLOW_RECEIPT_ITEM_STATE,
                                                "GRN item of different ageing found. Either allow multiple sku batching at GRN or create a new GRN.");
                                        break;
                                    }
                                }
                            }
                            if (inflowReceiptItem != null && !InflowReceiptItem.StatusCode.CREATED.name().equals(inflowReceiptItem.getStatusCode())) {
                                context.addError(WsResponseCode.INVALID_INFLOW_RECEIPT_ITEM_STATE, "items cannot be added after item sent to QC");
                            } else if (!context.hasErrors()) {
                                if (inflowReceiptItem == null) {
                                    inflowReceiptItem = new InflowReceiptItem();
                                    inflowReceiptItem.setInflowReceipt(inflowReceipt);
                                    WsInflowReceiptItem wsInflowReceiptItem = new WsInflowReceiptItem();
                                    wsInflowReceiptItem.setQuantity(1);
                                    wsInflowReceiptItem.setManufacturingDate(item.getManufacturingDate() != null ? item.getManufacturingDate() : request.getManufacturingDate());
                                    prepareInflowReceiptItem(inflowReceiptItem, purchaseOrderItem, wsInflowReceiptItem);
                                    inflowReceiptItem.setStatusCode(InflowReceiptItem.StatusCode.CREATED.name());
                                    inflowReceiptItem.setCreated(DateUtils.getCurrentTime());
                                    inflowReceiptItem = inflowDao.addInflowReceiptItem(inflowReceiptItem);
                                    inflowReceipt.getInflowReceiptItems().add(inflowReceiptItem);
                                } else {
                                    WsInflowReceiptItem wsInflowReceiptItem = new WsInflowReceiptItem();
                                    wsInflowReceiptItem.setQuantity(inflowReceiptItem.getQuantity() + 1);
                                    wsInflowReceiptItem.setManufacturingDate(inflowReceiptItem.getManufacturingDate());
                                    prepareInflowReceiptItem(inflowReceiptItem, purchaseOrderItem, wsInflowReceiptItem);
                                    inflowReceiptItem = inflowDao.updateInflowReceiptItem(inflowReceiptItem);
                                }
                                if (item.getAgeingStartDate() == null) {
                                    item.setAgeingStartDate(inflowReceiptItem.getAgeingStartDate());
                                }
                                item.setInflowReceiptItem(inflowReceiptItem);
                                item.setVendor(purchaseOrder.getVendor());
                                if (ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).isBarcodePreGenerated()) {
                                    item.setManufacturingDate(inflowReceiptItem.getManufacturingDate());
                                }
                                item.setStatusCode(Item.StatusCode.CREATED.name());
                                if (ActivityContext.current().isEnable()) {
                                    ActivityUtils.appendActivity(item.getCode(), ActivityEntityEnum.ITEM.getName(), null,
                                            Arrays.asList("Item {" + ActivityEntityEnum.ITEM + ":" + item.getCode() + "} created"), ActivityTypeEnum.CREATION.name());
                                }
                                purchaseDao.updatePuchaseOrderItem(purchaseOrderItem);
                                response.setInflowReceiptItemDTO(prepareInflowReceiptItemDTO(inflowReceiptItem, purchaseOrderItem));
                                response.setSuccessful(true);
                            }
                        }
                    }
                }
            }
        }
        response.setErrors(context.getErrors());
        response.setWarnings(context.getWarnings());
        return response;

    }

    /**
     * Checks various item conditions and performs validation. Validation is of 3 types:
     * <ol>
     * <li>Item should not be null.</li>
     * <li>Item should be unprocessed.</li>
     * </ol>
     * 
     * @param item Item on which validation is performed.
     * @param context Validation context
     * @see #addItemToInflowReceipt
     */
    private void validateItemWhenAddingToInflowReceipt(Item item, ValidationContext context) {
        if (item == null) {
            context.addError(WsResponseCode.INVALID_ITEM_ID, "Invalid item code");
        } else if (!Item.StatusCode.UNPROCESSED.name().equals(item.getStatusCode())) {
            context.addError(WsResponseCode.INVALID_ITEM_STATE, "Invalid item state");
        }
    }

    /**
     * Performs validation for different combination of request's manufacturing date and manufacturing date on item.
     * <ol>
     * <li>Item doesn't have an manufacturing date set, and request's manufacturing date was also null: This is an error
     * condition.</li>
     * <li>Item doesn't have an manufacturing date set, but request's manufacturing date is present: Simply set the
     * request's manufacturing date as manufacturing date for item.</li>
     * <li>Item already has an manufacturing date, and reuest's manufacturing date is null: In this case, no action is
     * required.</li>
     * <li>Item already has an manufacturing date, and a <b>different</b> manufacturing date was sent in request: In
     * this case, <b>retain</b> the original manufacturing date of item, and send a warning.</li>
     * </ol>
     * 
     * @see #validateItemWhenAddingToInflowReceipt
     */
    private void validateAndSetManufacturingForExpirableItem(Item item, ValidationContext context, Date requestManufacturingDate) {
        if (item.getManufacturingDate() == null && requestManufacturingDate == null) {
            context.addError(WsResponseCode.INVALID_REQUEST, "Please provide a manufacturing date as the sku is expirable.");
        } else if (item.getManufacturingDate() == null && requestManufacturingDate != null) {
            item.setManufacturingDate(requestManufacturingDate);
        } else if (item.getManufacturingDate() != null && requestManufacturingDate != null) {
            if (DateUtils.diff(item.getManufacturingDate(), requestManufacturingDate, DateUtils.Resolution.DAY) != 0) {
                context.addWarning(ResponseCode.GENERIC_WARNING,
                        "Mismatch in existing[" + item.getManufacturingDate() + "] and new manufacturing date[" + requestManufacturingDate + "]. Retaining existing one.");
            }
        }
    }

    @Override
    @Transactional
    public DiscardTraceableInflowReceiptItemResponse discardTraceableInflowReceiptItem(DiscardTraceableInflowReceiptItemRequest request) {
        DiscardTraceableInflowReceiptItemResponse response = new DiscardTraceableInflowReceiptItemResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            Item item = inventoryService.getItemByCode(request.getItemCode());
            if (item == null) {
                context.addError(WsResponseCode.INVALID_ITEM_ID, "Invalid item code");
            } else if (!Item.StatusCode.CREATED.name().equals(item.getStatusCode())) {
                context.addError(WsResponseCode.INVALID_ITEM_STATE, "Invalid item state");
            } else if (!PurchaseOrder.StatusCode.APPROVED.name().equals(item.getInflowReceiptItem().getPurchaseOrderItem().getPurchaseOrder().getStatusCode())) {
                context.addError(WsResponseCode.INVALID_PURCHASE_ORDER_STATE, "Purchase order not in APPROVED state");
            } else {
                InflowReceiptItem inflowReceiptItem = inflowDao.getInflowReceiptItemById(item.getInflowReceiptItem().getId(), true);
                if (StringUtils.isNotEmpty(item.getItemDetails())) {
                    inflowReceiptItem.setDetailedItemQuantity(inflowReceiptItem.getDetailedItemQuantity() - 1);
                }
                WsInflowReceiptItem wsInflowReceiptItem = new WsInflowReceiptItem();
                wsInflowReceiptItem.setQuantity(inflowReceiptItem.getQuantity() - 1);
                wsInflowReceiptItem.setManufacturingDate(inflowReceiptItem.getManufacturingDate());
                prepareInflowReceiptItem(inflowReceiptItem, inflowReceiptItem.getPurchaseOrderItem(), wsInflowReceiptItem);
                item.setStatusCode(Item.StatusCode.TRASHED.name());
                inventoryService.updateItem(item);
                inflowReceiptItem = inflowDao.updateInflowReceiptItem(inflowReceiptItem);
                purchaseDao.updatePuchaseOrderItem(inflowReceiptItem.getPurchaseOrderItem());
                response.setInflowReceiptItemDTO(prepareInflowReceiptItemDTO(inflowReceiptItem, inflowReceiptItem.getPurchaseOrderItem()));
                response.setSuccessful(true);
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    /* (non-Javadoc)
     * @see com.uniware.services.inflow.IInflowService#sendInflowReceiptForQC(com.uniware.core.api.inflow.SendInflowReceiptForQCRequest)
     */
    @Override
    @LogActivity
    public SendInflowReceiptForQCResponse sendInflowReceiptForQC(SendInflowReceiptForQCRequest request) {
        SendInflowReceiptForQCResponse response = new SendInflowReceiptForQCResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            InflowReceipt inflowReceipt = getInflowReceiptByCode(request.getInflowReceiptCode());
            if (inflowReceipt == null) {
                context.addError(WsResponseCode.INVALID_INFLOW_RECEIPT_CODE, "Invalid inflow receipt code");
            } else {
                sendInflowReceiptForQCInternal(response, context, inflowReceipt.getId(), inflowReceipt.getPurchaseOrder().getId());
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    /**
     * Quality Check(QC) of inflowReceipt
     * <li>Following conditions need to be checked for GRN:
     * <ol>
     * GRN should be in created state.
     * </ol>
     * </li>
     * <li>For every inflowReceiptItem(<b>iri</b>) following conditions should hold:
     * <ol>
     * If barcoding is not before GRN, and if barcoding is to done before QC, then check that for ITEM traceability, the
     * item of iri should be labelled at this stage.
     * </ol>
     * </li>
     * <p>
     * <b>Expirable items:</b> It is possible that grn items may be about to expire or expired. In that case, we need to
     * reject the items of that particular iri, and modify the item status accordingly (ABOUT_TO_EXPIRE or EXPIRED). See
     * {@link #rejectTraceableItemInternal(RejectTraceableItemRequest, RejectTraceableItemResponse, Item, ValidationContext)}
     * for doing that.
     * <p>
     * Now, the system supports partial putaway or partial QC of different grn items, i.e., it is possible that some iri
     * may be in state of CREATED, some in QC_PENDING and some in QC_COMPLETE. COMPLETE tatus of iri signifies that its
     * putaway has been done. <br>
     * So, we check for these conditions. Then, accordingly, we set the status of GRN, i.e., if even a single iri was
     * set to QC_PENDING, the status of GRN should be QC_PENDING. Similar logic is used for QC_COMPLETE. (Priority is
     * given to QC_PENDING)<br>
     * Note that a GRN is said to be complete only if every iri's status is COMPLETE, i.e., when all of their putaways
     * are done.<br>
     * Also, updates purchase order if it has been completed.
     */
    @Transactional
    public void sendInflowReceiptForQCInternal(SendInflowReceiptForQCResponse response, ValidationContext context, Integer inflowReceiptId, Integer purchaseOrderId) {
        PurchaseOrder purchaseOrder = purchaseDao.getPurchaseOrderById(purchaseOrderId, true);
        InflowReceipt inflowReceipt = inflowDao.getInflowReceiptById(inflowReceiptId, false, true);
        if (!InflowReceipt.StatusCode.CREATED.name().equals(inflowReceipt.getStatusCode())) {
            context.addError(WsResponseCode.INVALID_INFLOW_RECEIPT_STATE, "Inflow receipt not in CREATED state");
        } else {
            if (!ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).isBarcodingBeforeGRN()) {
                for (InflowReceiptItem inflowReceiptItem : inflowReceipt.getInflowReceiptItems()) {
                    if (inflowReceiptItem.getQuantity() > 0 && ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).isBarcodingBeforeQualityCheck()
                            && ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).getTraceabilityLevel() == TraceabilityLevel.ITEM
                            && !inflowReceiptItem.isItemsLabelled()) {
                        context.addError(WsResponseCode.ITEMS_LABELLING_MANDATORY_BEFORE_QC, "Item should be labelled before QC");
                    }
                }
            }
            if (!context.hasErrors()) {
                // Since statuses are moved to line item level, QC or PUTAWAY might be completed before GRN complete creation
                boolean isQCComplete = true;
                boolean isComplete = true;
                SimpleDateFormat format = new SimpleDateFormat("yyyy.MM.dd");
                for (InflowReceiptItem inflowReceiptItem : inflowReceipt.getInflowReceiptItems()) {
                    boolean isItemAboutToExpire = false;
                    boolean isItemExpired = false;
                    ItemType itemType = catalogService.getAllItemTypeBySkuCode(inflowReceiptItem.getItemType().getSkuCode());
                    int shelfLife = catalogService.getShelfLifeForItemType(itemType);
                    if (catalogService.isItemTypeExpirable(itemType)
                            && inflowReceiptItem.isItemExpired(shelfLife, itemType.getCategory().getExpiryToleranceInDays(Category.ToleranceType.GRN, shelfLife))) {
                        isItemAboutToExpire = true;
                        if (inflowReceiptItem.isItemExpired(shelfLife, 0)) {
                            isItemExpired = true;
                        }
                        LOG.info(inflowReceiptItem.getId() + ":Item already expired/close to expiry: Expiry Date:"
                                + format.format(DateUtils.addDaysToDate(inflowReceiptItem.getManufacturingDate(), shelfLife)));
                    }
                    if (InflowReceiptItem.StatusCode.CREATED.name().equals(inflowReceiptItem.getStatusCode())) {
                        if (inflowReceiptItem.getQuantity() > 0) {
                            inflowReceiptItem.setStatusCode(InflowReceiptItem.StatusCode.QC_PENDING.name());
                            inflowReceiptItem = inflowDao.updateInflowReceiptItem(inflowReceiptItem);
                            inflowDao.updateItemStatuses(inflowReceiptItem.getId(), Item.StatusCode.CREATED.name(), Item.StatusCode.QC_PENDING.name());
                            isQCComplete = false;
                            isComplete = false;
                        } else {
                            inflowReceiptItem.setStatusCode(InflowReceiptItem.StatusCode.COMPLETE.name());
                            inflowReceiptItem = inflowDao.updateInflowReceiptItem(inflowReceiptItem);
                        }
                    } else if (InflowReceiptItem.StatusCode.QC_PENDING.name().equals(inflowReceiptItem.getStatusCode())) {
                        isQCComplete = false;
                        isComplete = false;
                    } else if (InflowReceiptItem.StatusCode.QC_COMPLETE.name().equals(inflowReceiptItem.getStatusCode())) {
                        isComplete = false;
                    }
                    if (isItemAboutToExpire) {
                        List<Item> items = getItemsByReceiptItemId(inflowReceiptItem.getId(), true);
                        RejectTraceableItemRequest rejectTraceableItemRequest = new RejectTraceableItemRequest();
                        rejectTraceableItemRequest.setInflowReceiptCode(inflowReceipt.getCode());
                        for (Item item : items) {
                            RejectTraceableItemResponse rejectTraceableItemResponse = new RejectTraceableItemResponse();
                            rejectTraceableItemRequest.setRejectionReason(Item.RejectionReason.ABOUT_TO_EXPIRE.name());
                            if (isItemExpired) {
                                rejectTraceableItemRequest.setRejectionReason(Item.RejectionReason.EXPIRED_ITEM.name());
                            }
                            rejectTraceableItemInternal(rejectTraceableItemRequest, rejectTraceableItemResponse, item, context);
                            if (ActivityContext.current().isEnable()) {
                                ActivityUtils.appendActivity(item.getCode(), ActivityEntityEnum.ITEM.getName(), null,
                                        Arrays.asList("Item {" + ActivityEntityEnum.ITEM + ":" + item.getCode() + "}, inventory type: " + item.getInventoryType()
                                                + " moved to QC_REJECTED in GRN {" + ActivityEntityEnum.INFLOW_RECEIPT + ":" + inflowReceipt.getCode() + "} of {"
                                                + ActivityEntityEnum.PURCHASE_ORDER + ":" + purchaseOrder.getCode() + "}"),
                                        Item.ActivityType.QC_REJECTED.name());
                            }
                        }
                    }

                }
                inflowReceipt.setStatusCode(isComplete ? InflowReceipt.StatusCode.COMPLETE.name()
                        : (isQCComplete ? InflowReceipt.StatusCode.QC_COMPLETE.name() : InflowReceipt.StatusCode.QC_PENDING.name()));
                inflowReceipt = inflowDao.updateInflowReceipt(inflowReceipt);
                if (isPurchaseOrderComplete(purchaseOrder)) {
                    for (InflowReceipt ir : purchaseOrder.getInflowReceipts()) {
                        if (InflowReceipt.StatusCode.CREATED.name().equals(ir.getStatusCode())) {
                            ir.setStatusCode(InflowReceipt.StatusCode.DISCARDED.name());
                        }
                    }
                    purchaseOrder.setStatusCode(PurchaseOrder.StatusCode.COMPLETE.name());
                }
                response.setSuccessful(true);
                if (ActivityContext.current().isEnable()) {
                    ActivityUtils.appendActivity(
                            inflowReceipt.getCode(), ActivityEntityEnum.PURCHASE_ORDER.getName(), null, singletonList("GRN {" + ActivityEntityEnum.INFLOW_RECEIPT + ":"
                                    + inflowReceipt.getCode() + "} for {" + ActivityEntityEnum.PURCHASE_ORDER + ":" + purchaseOrder.getCode() + "} sent for QC"),
                            ActivityTypeEnum.QUALITY_CHECK.name());
                }
            }
        }
    }

    @Override
    @Transactional
    public SendInflowReceiptItemsForQCResponse sendInflowReceiptItemsForQC(SendInflowReceiptItemsForQCRequest request) {
        SendInflowReceiptItemsForQCResponse response = new SendInflowReceiptItemsForQCResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            PurchaseOrder purchaseOrder = inflowDao.getPurchaseOrderByInflowReceiptCode(request.getInflowReceiptCode(), true);
            if (purchaseOrder == null) {
                context.addError(WsResponseCode.INVALID_INFLOW_RECEIPT_ID, "Invalid inflow receipt id");
            } else {
                InflowReceipt inflowReceipt = purchaseOrder.getInflowReceipts().iterator().next();
                Map<Integer, InflowReceiptItem> idToInflowReceiptItems = new HashMap<Integer, InflowReceiptItem>();
                for (InflowReceiptItem inflowReceiptItem : inflowReceipt.getInflowReceiptItems()) {
                    idToInflowReceiptItems.put(inflowReceiptItem.getId(), inflowReceiptItem);
                }
                List<InflowReceiptItem> inflowReceiptItems = new ArrayList<InflowReceiptItem>();
                for (Integer inflowReceiptItemId : request.getInflowReceiptItemIds()) {
                    InflowReceiptItem inflowReceiptItem = idToInflowReceiptItems.get(inflowReceiptItemId);
                    if (inflowReceiptItem == null) {
                        context.addError(WsResponseCode.INVALID_INFLOW_RECEIPT_ITEM_ID, "Invalid inflow receipt item id:" + inflowReceiptItemId);
                        break;
                    } else if (!InflowReceiptItem.StatusCode.CREATED.name().equals(inflowReceiptItem.getStatusCode())) {
                        context.addError(WsResponseCode.INVALID_INFLOW_RECEIPT_STATE, "Only CREATED inflow receipts can be sent for QC");
                    } else if (ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).isBarcodingBeforeQualityCheck()
                            && ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).getTraceabilityLevel() == TraceabilityLevel.ITEM
                            && !inflowReceiptItem.isItemsLabelled()) {
                        context.addError(WsResponseCode.ITEMS_LABELLING_MANDATORY_BEFORE_QC, "Item should be labelled before QC");
                    } else {
                        inflowReceiptItems.add(inflowReceiptItem);
                    }
                }
                if (!context.hasErrors()) {
                    for (InflowReceiptItem inflowReceiptItem : inflowReceiptItems) {
                        inflowReceiptItem.setStatusCode(
                                inflowReceiptItem.getQuantity() > 0 ? InflowReceiptItem.StatusCode.QC_PENDING.name() : InflowReceiptItem.StatusCode.COMPLETE.name());
                        inflowDao.updateInflowReceiptItem(inflowReceiptItem);
                        inflowDao.updateItemStatuses(inflowReceiptItem.getId(), Item.StatusCode.CREATED.name(), Item.StatusCode.QC_PENDING.name());
                    }
                    response.setSuccessful(true);
                }
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    /**
     * Checks if purchse order has received the required quantity, by checking two conditions:
     * <li>
     * <ol>
     * Whether received quantity is less than total quantity of each sku in PO
     * </ol>
     * <ol>
     * For GRN corresponding to PO, whether its status code is CREATED and if so, whether it contains any skus(inflow
     * receipt items of GRN correspond to a sku)
     * </ol>
     * </li> If any of the above two is true, then PO has not been completed, and thus, return false. Else true
     * 
     * @param purchaseOrder
     * @return
     */
    private boolean isPurchaseOrderComplete(PurchaseOrder purchaseOrder) {
        for (PurchaseOrderItem purchaseOrderItem : purchaseOrder.getPurchaseOrderItems()) {
            if (purchaseOrderItem.getReceivedQuantity() < purchaseOrderItem.getQuantity()) {
                return false;
            }
        }
        for (InflowReceipt inflowReceipt : purchaseOrder.getInflowReceipts()) {
            if (InflowReceipt.StatusCode.CREATED.name().equals(inflowReceipt.getStatusCode()) && inflowReceipt.getInflowReceiptItems().size() > 0) {
                return false;
            }
        }
        return true;
    }

    @Transactional
    @Override
    public AddItemDetailsResponse addItemDetails(AddItemDetailsRequest request) {
        AddItemDetailsResponse response = new AddItemDetailsResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            Item item = inventoryService.getItemByCode(request.getItemCode());
            if (item == null) {
                context.addError(WsResponseCode.INVALID_ITEM_ID, "Invalid item code");
            } else {
                Map<String, String> itemDetails = ItemDetail.getItemDetailsMap(request.getItemDetails());
                List<ItemDetailFieldDTO> itemDetailFields = CacheManager.getInstance().getCache(ItemTypeDetailCache.class).getItemDetailFields(item.getItemType());
                if (itemDetailFields == null) {
                    context.addError(WsResponseCode.ITEM_DETAILS_NOT_EXPTECTED, "Item details not expected for this category");
                } else {
                    Map<String, String> itemFieldValues = new HashMap<>();
                    for (ItemDetailFieldDTO itemDetail : itemDetailFields) {
                        String value = itemDetails.get(itemDetail.getName());
                        if (StringUtils.isNotBlank(itemDetail.getValidatorPattern()) && !value.matches(itemDetail.getValidatorPattern())) {
                            context.addError(WsResponseCode.ITEM_DETAILS_INVALID_FORMAT, "Invalid item detail value for :" + itemDetail.getName());
                        } else {
                            itemFieldValues.put(itemDetail.getName(), value);
                        }
                    }
                    if (!context.hasErrors()) {
                        if (StringUtils.isBlank(item.getItemDetails()) && item.getInflowReceiptItem() != null) {
                            addInflowReceiptDetailedItemQuantity(item, item.getInflowReceiptItem().getId(), itemFieldValues);
                        }
                        item.setItemDetails(new Gson().toJson(itemFieldValues));
                        response.setSuccessful(true);
                    }
                }
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    /**
     * Do the detailing for item in inflow receipt.
     * 
     * @param item
     * @param inflowReceiptItemId
     * @param itemFieldValues
     */
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void addInflowReceiptDetailedItemQuantity(Item item, Integer inflowReceiptItemId, Map<String, String> itemFieldValues) {
        InflowReceiptItem inflowReceiptItem = inflowDao.getInflowReceiptItemById(inflowReceiptItemId, true);
        item = inventoryService.getItemById(item.getId());
        if (StringUtils.isBlank(item.getItemDetails())) {
            inflowReceiptItem.setDetailedItemQuantity(inflowReceiptItem.getDetailedItemQuantity() + 1);
            item.setItemDetails(new Gson().toJson(itemFieldValues));
        }
    }

    /* (non-Javadoc)
     * @see com.uniware.services.inflow.IInflowService#editInflowReceiptItem(com.uniware.core.api.inflow.EditInflowReceiptItemRequest)
     */
    @Transactional
    @Override
    public RejectNonTraceableItemResponse rejectNonTraceableItem(RejectNonTraceableItemRequest request) {
        RejectNonTraceableItemResponse response = new RejectNonTraceableItemResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            InflowReceiptItem inflowReceiptItem = inflowDao.getInflowReceiptItemById(request.getInflowReceiptItemId(), true);
            if (inflowReceiptItem == null) {
                context.addError(WsResponseCode.INVALID_INFLOW_RECEIPT_ITEM_ID, "Invalid inflow receipt item id");
            } else if (!InflowReceiptItem.StatusCode.QC_PENDING.name().equals(inflowReceiptItem.getStatusCode())) {
                context.addError(WsResponseCode.INVALID_INFLOW_RECEIPT_STATE, "Rejected quantity can be updated only in QC_PENDING state");
            } else if (ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).isBarcodingBeforeQualityCheck()
                    && ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).getTraceabilityLevel() == TraceabilityLevel.ITEM) {
                context.addError(WsResponseCode.ACTION_NOT_APPLICABLE_FOR_TRACEABLE_ITEMS, "Items are traceable");
            } else {
                if (inflowReceiptItem.getQuantity() < request.getRejectedQuantity()) {
                    context.addError(WsResponseCode.INFLOW_RECEIPT_REJECTED_QTY_CANNOT_EXCEED_TOTAL, "Inflow receipt rejected quantity cannot exceed total quantity");
                } else {
                    inflowReceiptItem.setRejectedQuantity(request.getRejectedQuantity());
                    inflowReceiptItem.setComments(request.getComments());
                    inflowReceiptItem.setExpiry(request.getExpiry());
                    inflowReceiptItem = inflowDao.updateInflowReceiptItem(inflowReceiptItem);
                    response.setSuccessful(true);
                    response.setInflowReceiptItemDTO(prepareInflowReceiptItemDTO(inflowReceiptItem, inflowReceiptItem.getPurchaseOrderItem()));
                }
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    /**
     * Reject item.
     * 
     * @param request contains item code, GRN code and rejection reason.
     * @see #rejectTraceableItemInternal(RejectTraceableItemRequest, RejectTraceableItemResponse, Item,
     *      ValidationContext)
     */
    @Override
    public RejectTraceableItemResponse rejectTraceableItem(RejectTraceableItemRequest request) {
        RejectTraceableItemResponse response = new RejectTraceableItemResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            Item item = inventoryService.getItemByCode(request.getItemCode());
            if (item == null) {
                context.addError(WsResponseCode.INVALID_ITEM_ID, "Invalid item code");
            } else {
                rejectTraceableItemInternal(request, response, item, context);
                if (!context.hasErrors()) {
                    response.setSuccessful(true);
                }
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    /**
     * Rejects item. Increases the rejected quantity of the corresponding inflow receipt item(iri) of the item by 1, and
     * modifies the detailed item quantity if necessary. Sets item status as QC_REJECTED.
     * 
     * @param response contains inflowReceiptItemDTO
     */
    @Transactional
    private void rejectTraceableItemInternal(RejectTraceableItemRequest request, RejectTraceableItemResponse response, Item item, ValidationContext context) {
        InflowReceiptItem inflowReceiptItem = inflowDao.getInflowReceiptItemById(item.getInflowReceiptItem().getId(), true);
        item = inventoryService.getItemByCode(item.getCode());
        if (!item.isQCPending() || !item.isGoodInventoryByType()) {
            context.addError(WsResponseCode.INVALID_ITEM_STATE, "Invalid item state");
        } else if (!StringUtils.equalsIngoreCaseAny(inflowReceiptItem.getInflowReceipt().getCode(), request.getInflowReceiptCode())) {
            context.addError(WsResponseCode.INVALID_ITEM_STATE, "Item doesn't belong to this GRN");
        } else {
            inflowReceiptItem.setRejectedQuantity(inflowReceiptItem.getRejectedQuantity() + 1);
            if (StringUtils.isNotEmpty(item.getItemDetails())) {
                inflowReceiptItem.setDetailedItemQuantity(inflowReceiptItem.getDetailedItemQuantity() - 1);
            }
            item.setInventoryType(Type.QC_REJECTED);
            item.setRejectionReason(request.getRejectionReason());
            inventoryService.updateItem(item);
            inflowReceiptItem = inflowDao.updateInflowReceiptItem(inflowReceiptItem);
            response.setInflowReceiptItemDTO(prepareInflowReceiptItemDTO(inflowReceiptItem, inflowReceiptItem.getPurchaseOrderItem()));
        }
    }

    @Override
    @Transactional
    @LogActivity
    public CompleteInflowReceiptItemsQCResponse completeInflowReceiptItemsQC(CompleteInflowReceiptItemsQCRequest request) {
        CompleteInflowReceiptItemsQCResponse response = new CompleteInflowReceiptItemsQCResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            PurchaseOrder purchaseOrder = inflowDao.getPurchaseOrderByInflowReceiptCode(request.getInflowReceiptCode(), true);
            if (purchaseOrder == null) {
                context.addError(WsResponseCode.INVALID_INFLOW_RECEIPT_ID, "Invalid inflow receipt id");
            } else {
                InflowReceipt inflowReceipt = purchaseOrder.getInflowReceipts().iterator().next();
                Map<Integer, InflowReceiptItem> idToInflowReceiptItems = new HashMap<Integer, InflowReceiptItem>();
                for (InflowReceiptItem inflowReceiptItem : inflowReceipt.getInflowReceiptItems()) {
                    idToInflowReceiptItems.put(inflowReceiptItem.getId(), inflowReceiptItem);
                }
                List<InflowReceiptItem> inflowReceiptItems = new ArrayList<InflowReceiptItem>();
                for (Integer inflowReceiptItemId : request.getInflowReceiptItemIds()) {
                    InflowReceiptItem inflowReceiptItem = idToInflowReceiptItems.get(inflowReceiptItemId);
                    if (inflowReceiptItem == null) {
                        context.addError(WsResponseCode.INVALID_INFLOW_RECEIPT_ITEM_ID, "Invalid inflow receipt item id:" + inflowReceiptItemId);
                        break;
                    } else if (!InflowReceiptItem.StatusCode.QC_PENDING.name().equals(inflowReceiptItem.getStatusCode())) {
                        context.addError(WsResponseCode.INVALID_INFLOW_RECEIPT_STATE, "Only QC_PENDING inflow receipts can be marked as QC_COMPLETE");
                    } else if (ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).isItemDetailingMandatoryBeforePutaway()
                            && TraceabilityLevel.ITEM.equals(ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).getTraceabilityLevel())
                            && CacheManager.getInstance().getCache(ItemTypeDetailCache.class).getItemDetailFields(inflowReceiptItem.getItemType()) != null
                            && (inflowReceiptItem.getQuantity() - inflowReceiptItem.getRejectedQuantity() > inflowReceiptItem.getDetailedItemQuantity())) {
                        context.addError(WsResponseCode.ITEMS_DETAILING_MANDATORY_BEFORE_PUTAWAY, "All Item details should be added before Quality Check complete");
                    } else {
                        inflowReceiptItems.add(inflowReceiptItem);
                    }
                }
                if (!context.hasErrors()) {
                    boolean hasRejectedItems = false;
                    for (InflowReceiptItem inflowReceiptItem : inflowReceiptItems) {
                        if (inflowReceiptItem.getRejectedQuantity() > 0) {
                            hasRejectedItems = true;
                        }
                        PurchaseOrderItem purchaseOrderItem = inflowReceiptItem.getPurchaseOrderItem();
                        purchaseOrderItem.setRejectedQuantity(purchaseOrderItem.getRejectedQuantity() + inflowReceiptItem.getRejectedQuantity());
                        purchaseDao.updatePuchaseOrderItem(purchaseOrderItem);
                        inflowReceiptItem.setStatusCode(InflowReceiptItem.StatusCode.QC_COMPLETE.name());
                        inflowDao.updateInflowReceiptItem(inflowReceiptItem);
                        inflowDao.updateItemStatuses(inflowReceiptItem.getId(), Item.StatusCode.QC_PENDING.name(), Item.StatusCode.QC_COMPLETE.name());
                    }
                    if (isInflowReceiptComplete(inflowReceipt)) {
                        inflowReceipt.setStatusCode(InflowReceipt.StatusCode.COMPLETE.name());
                    } else if (isInflowReceiptQCComplete(inflowReceipt)) {
                        inflowReceipt.setStatusCode(InflowReceipt.StatusCode.QC_COMPLETE.name());
                    }

                    if (hasRejectedItems) {
                        sendQCRejectionReport(inflowReceipt, request.getUserEmail());
                    }
                    response.setSuccessful(true);
                    if (ActivityContext.current().isEnable()) {
                        ActivityUtils.appendActivity(inflowReceipt.getCode(), ActivityEntityEnum.PURCHASE_ORDER.getName(), null,
                                Arrays.asList(new String[] {
                                        "QC Completed for GRN {" + ActivityEntityEnum.INFLOW_RECEIPT + ":" + inflowReceipt.getCode() + "} of {" + ActivityEntityEnum.PURCHASE_ORDER
                                                + ":" + purchaseOrder.getCode() + "}" }),
                                ActivityTypeEnum.QUALITY_CHECK.name());
                    }
                }
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    private boolean isInflowReceiptComplete(InflowReceipt inflowReceipt) {
        for (InflowReceiptItem inflowReceiptItem : inflowReceipt.getInflowReceiptItems()) {
            if (!InflowReceiptItem.StatusCode.COMPLETE.name().equals(inflowReceiptItem.getStatusCode())) {
                return false;
            }
        }
        return true;
    }

    /**
     * @param inflowReceipt
     * @return
     */
    private boolean isInflowReceiptQCComplete(InflowReceipt inflowReceipt) {
        if (InflowReceipt.StatusCode.QC_PENDING.name().equals(inflowReceipt.getStatusCode())) {
            for (InflowReceiptItem inflowReceiptItem : inflowReceipt.getInflowReceiptItems()) {
                if (InflowReceiptItem.StatusCode.QC_PENDING.name().equals(inflowReceiptItem.getStatusCode())) {
                    return false;
                }
            }
        } else {
            return false;
        }
        return true;
    }

    private InflowReceiptDTO prepareInflowReceiptDTO(InflowReceipt inflowReceipt) {
        InflowReceiptDTO inflowReceiptDTO = new InflowReceiptDTO();
        inflowReceiptDTO.setCode(inflowReceipt.getCode());
        inflowReceiptDTO.setCreated(inflowReceipt.getCreated());
        inflowReceiptDTO.setCreatedBy(inflowReceipt.getUser().getUsername());
        inflowReceiptDTO.setStatusCode(inflowReceipt.getStatusCode());
        inflowReceiptDTO.setVendorInvoiceNumber(inflowReceipt.getVendorInvoiceNumber());
        inflowReceiptDTO.setVendorInvoiceDate(inflowReceipt.getVendorInvoiceDate());
        inflowReceiptDTO.setCustomFieldValues(CustomFieldUtils.getCustomFieldValuesDTO(inflowReceipt));
        PurchaseOrderDTO purchaseOrder = new PurchaseOrderDTO(inflowReceipt.getPurchaseOrder());
        purchaseOrder.setCustomFieldValues(CustomFieldUtils.getCustomFieldValuesDTO(inflowReceipt.getPurchaseOrder()));
        inflowReceiptDTO.setPurchaseOrder(purchaseOrder);
        for (InflowReceiptItem item : inflowReceipt.getInflowReceiptItems()) {
            if (item.getQuantity() > 0) {
                InflowReceiptItemDTO inflowReceiptItemDTO = prepareInflowReceiptItemDTO(item, item.getPurchaseOrderItem());
                inflowReceiptDTO.getInflowReceiptItems().add(inflowReceiptItemDTO);
                inflowReceiptDTO.setTotalQuantity(inflowReceiptDTO.getTotalQuantity() + item.getQuantity());
                inflowReceiptDTO.setTotalRejectedQuantity(inflowReceiptDTO.getTotalRejectedQuantity() + item.getRejectedQuantity());
                inflowReceiptDTO.setTotalReceivedAmount(inflowReceiptDTO.getTotalReceivedAmount().add(item.getTotal()));
                inflowReceiptDTO.setTotalRejectedAmount(
                        inflowReceiptDTO.getTotalRejectedAmount().add(NumberUtils.multiply(NumberUtils.divide(item.getTotal(), item.getQuantity()), item.getRejectedQuantity())));
            }
        }
        return inflowReceiptDTO;
    }

    private InflowReceiptItemDTO prepareInflowReceiptItemDTO(InflowReceiptItem inflowReceiptItem, PurchaseOrderItem purchaseOrderItem) {
        InflowReceiptItemDTO inflowReceiptItemDTO = new InflowReceiptItemDTO();
        inflowReceiptItemDTO.setId(inflowReceiptItem.getId());
        inflowReceiptItemDTO.setPurchaseOrderItemId(inflowReceiptItem.getPurchaseOrderItem().getId());
        inflowReceiptItemDTO.setItemSKU(inflowReceiptItem.getPurchaseOrderItem().getItemType().getSkuCode());
        inflowReceiptItemDTO.setVendorSkuCode(inflowReceiptItem.getPurchaseOrderItem().getVendorSkuCode());
        inflowReceiptItemDTO.setItemTypeName(inflowReceiptItem.getPurchaseOrderItem().getItemType().getName());
        inflowReceiptItemDTO.setQuantity(inflowReceiptItem.getQuantity());
        inflowReceiptItemDTO.setAdditionalCost(inflowReceiptItem.getAdditionalCost());
        inflowReceiptItemDTO.setBatchCode(inflowReceiptItem.getBatchCode());
        inflowReceiptItemDTO.setItemTypeImageUrl(purchaseOrderItem.getItemType().getImageUrl());
        inflowReceiptItemDTO.setItemTypePageUrl(purchaseOrderItem.getItemType().getProductPageUrl());
        inflowReceiptItemDTO.setMaxRetailPrice(inflowReceiptItem.getMaxRetailPrice());
        inflowReceiptItemDTO.setManufacturingDate(inflowReceiptItem.getManufacturingDate());
        inflowReceiptItemDTO.setPendingQuantity(purchaseOrderItem.getQuantity() - purchaseOrderItem.getReceivedQuantity() - purchaseOrderItem.getCancelledQuantity());
        inflowReceiptItemDTO.setShelfLife(catalogService.getShelfLifeForItemType(inflowReceiptItem.getPurchaseOrderItem().getItemType()));
        inflowReceiptItemDTO.setExpirable(catalogService.isItemTypeExpirable(inflowReceiptItem.getPurchaseOrderItem().getItemType()));
        inflowReceiptItemDTO.setGrnExpiryTolerance(inflowReceiptItem.getPurchaseOrderItem().getItemType().getCategory().getGrnExpiryTolerance());
        inflowReceiptItemDTO.setRejectedQuantity(inflowReceiptItem.getRejectedQuantity());
        if (ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).isVendorPricesTaxExclusive()) {
            inflowReceiptItemDTO.setUnitPrice(inflowReceiptItem.getUnitPrice());
            inflowReceiptItemDTO.setDiscount(inflowReceiptItem.getDiscount());
            inflowReceiptItemDTO.setDiscountPercentage(
                    NumberUtils.getPercentageFromBaseAndPercentageAmount(inflowReceiptItemDTO.getUnitPrice(), inflowReceiptItemDTO.getDiscount()));

        } else {
            BigDecimal unitPriceWithTax = inflowReceiptItem.getQuantity() > 0 ? NumberUtils.divide(inflowReceiptItem.getTotal(), inflowReceiptItem.getQuantity()) : BigDecimal.ZERO;
            BigDecimal totalTaxAmount = inflowReceiptItem.getVat().add(inflowReceiptItem.getCst()).add(inflowReceiptItem.getCentralGst()).add(inflowReceiptItem.getStateGst()).add(
                    inflowReceiptItem.getUnionTerritoryGst()).add(inflowReceiptItem.getIntegratedGst()).add(inflowReceiptItem.getCompensationCess());
            BigDecimal percentage = NumberUtils.getPercentageFromBaseAndPercentageAmount(inflowReceiptItem.getSubtotal(), totalTaxAmount);
            BigDecimal discountWithTax = inflowReceiptItem.getDiscount().add(NumberUtils.getPercentageFromBase(inflowReceiptItem.getDiscount(), percentage));
            inflowReceiptItemDTO.setDiscount(discountWithTax);
            inflowReceiptItemDTO.setUnitPrice(unitPriceWithTax.add(discountWithTax));
            inflowReceiptItemDTO.setDiscountPercentage(
                    NumberUtils.getPercentageFromBaseAndPercentageAmount(inflowReceiptItemDTO.getUnitPrice(), inflowReceiptItemDTO.getDiscount()));
        }
        inflowReceiptItemDTO.setExpiry(inflowReceiptItem.getExpiry());
        inflowReceiptItemDTO.setRejectionComments(inflowReceiptItem.getComments());
        inflowReceiptItemDTO.setItemsLabelled(inflowReceiptItem.isItemsLabelled());
        inflowReceiptItemDTO.setStatus(inflowReceiptItem.getStatusCode());
        if (TraceabilityLevel.ITEM.equals(ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).getTraceabilityLevel())
                && CacheManager.getInstance().getCache(ItemTypeDetailCache.class).getItemDetailFields(purchaseOrderItem.getItemType()) != null) {
            inflowReceiptItemDTO.setDetailedQuantity(inflowReceiptItem.getDetailedItemQuantity());
        }
        return inflowReceiptItemDTO;
    }

    @Override
    @Transactional(readOnly = true)
    public GetInflowReceiptItemsResponse getInflowReceiptItems(GetInflowReceiptItemsRequest request) {
        GetInflowReceiptItemsResponse response = new GetInflowReceiptItemsResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            for (InflowReceiptItem receiptItem : inflowDao.getInflowReceiptItems(request.getInflowReceiptCode())) {
                response.getInflowReceiptItems().add(prepareInflowReceiptItemDTO(receiptItem, receiptItem.getPurchaseOrderItem()));
            }
            response.setSuccessful(true);
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional(readOnly = true)
    public GetInflowReceiptResponse getInflowReceipt(GetInflowReceiptRequest request) {
        GetInflowReceiptResponse response = new GetInflowReceiptResponse();
        ValidationContext context = request.validate();
        InflowReceipt inflowReceipt = null;
        if (!context.hasErrors()) {
            inflowReceipt = inflowDao.getInflowReceiptByCode(request.getInflowReceiptCode());
            if (inflowReceipt == null) {
                context.addError(WsResponseCode.INVALID_INFLOW_RECEIPT_CODE, "Invalid Inflow receipt code");
            } else {
                List<InflowReceiptItem> inflowReceiptItems = getInflowReceiptItems(inflowReceipt.getCode());
                inflowReceipt.getInflowReceiptItems().addAll(inflowReceiptItems);

                response.setInflowReceipt(prepareInflowReceiptDTO(inflowReceipt));
                response.setVendorCode(inflowReceipt.getPurchaseOrder().getVendor().getCode());
                response.setSuccessful(true);
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    private List<InflowReceiptItem> getInflowReceiptItems(String inflowReceiptCode) {
        return inflowDao.getInflowReceiptItems(inflowReceiptCode);
    }

    @Override
    @Transactional(readOnly = true)
    public GetPOInflowReceiptsResponse getAllInflowReceipts(GetPOInflowReceiptsRequest request) {
        GetPOInflowReceiptsResponse response = new GetPOInflowReceiptsResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            List<POInflowReceipt> inflowReceipts = new ArrayList<POInflowReceipt>();
            for (InflowReceipt receipt : inflowDao.getPOInflowReceipts(request.getPurchaseOrderCode())) {
                POInflowReceipt dto = new POInflowReceipt();
                dto.setCreated(receipt.getCreated());
                dto.setStatusCode(receipt.getStatusCode());
                dto.setCode(receipt.getCode());
                dto.setVendorInvoiceDate(receipt.getVendorInvoiceDate());
                dto.setVendorInvoiceNumber(receipt.getVendorInvoiceNumber());
                dto.setCustomFieldValues(CustomFieldUtils.getCustomFieldValuesDTO(receipt));
                inflowReceipts.add(dto);
            }
            response.setInflowReceipts(inflowReceipts);
            response.setSuccessful(true);
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional(readOnly = true)
    public SearchReceiptResponse searchReceipt(SearchReceiptRequest request) {
        SearchReceiptResponse response = new SearchReceiptResponse();
        ValidationContext context = request.validate();
        if (context.hasErrors()) {
            response.setSuccessful(false);
            response.setErrors(context.getErrors());
        } else {
            List<Object> ids = inflowDao.searchInflowReceipt(request);
            if (request.getSearchOptions() != null && request.getSearchOptions().isGetCount()) {
                Long count = inflowDao.getInflowReceiptCount(request);
                response.setTotalRecords(count);
            }
            if (ids.size() > 0) {
                List<InflowReceipt> inflowReceipts = inflowDao.getInflowReceipts(ids);
                for (InflowReceipt receipt : inflowReceipts) {
                    WsInflowReceipt inflowReceipt = new WsInflowReceipt();
                    inflowReceipt.setCode(receipt.getCode());
                    inflowReceipt.setCreated(receipt.getCreated());
                    inflowReceipt.setStatusCode(receipt.getStatusCode());
                    PurchaseOrder purchaseOrder = receipt.getPurchaseOrder();
                    inflowReceipt.setPurchaseOrderCode(purchaseOrder.getCode());
                    inflowReceipt.setVendorInvoiceNumber(receipt.getVendorInvoiceNumber());
                    inflowReceipt.setVendorName(purchaseOrder.getVendor().getName());
                    response.getElements().add(inflowReceipt);
                }
            }
            response.setSuccessful(true);
        }
        return response;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Item> getItemsByReceiptItemId(int receiptItemId) {
        return inflowDao.getItemsByReceiptItemId(receiptItemId);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Item> getItemsByReceiptItemId(int receiptItemId, boolean refresh) {
        return inflowDao.getItemsByReceiptItemId(receiptItemId, refresh);
    }

    @Override
    @Transactional(readOnly = true)
    public List<InflowReceipt> getInflowReceiptsByStatus(String statusCode, Date pastHours) {
        return inflowDao.getInflowReceiptsByStatus(statusCode, pastHours);
    }

    @Override
    @Transactional
    public UpdateTraceableInflowReceiptItemResponse updateTraceableItem(UpdateTraceableInflowReceiptItemRequest request) {
        UpdateTraceableInflowReceiptItemResponse response = new UpdateTraceableInflowReceiptItemResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            InflowReceiptItem inflowReceiptItem = inflowDao.getInflowReceiptItemById(request.getInflowReceiptItemId(), true);
            if (inflowReceiptItem == null) {
                context.addError(WsResponseCode.INVALID_INFLOW_RECEIPT_ITEM_ID, "Invalid inflow receipt item id");
            } else if (!InflowReceipt.StatusCode.QC_PENDING.name().equals(inflowReceiptItem.getInflowReceipt().getStatusCode())) {
                context.addError(WsResponseCode.INVALID_INFLOW_RECEIPT_STATE, "Item expiry can be updated only in QC_PENDING state");
            } else {
                inflowReceiptItem.setComments(request.getComments());
                inflowReceiptItem.setExpiry(request.getExpiry());
                inflowReceiptItem = inflowDao.updateInflowReceiptItem(inflowReceiptItem);
                response.setSuccessful(true);
                response.setInflowReceiptItemDTO(prepareInflowReceiptItemDTO(inflowReceiptItem, inflowReceiptItem.getPurchaseOrderItem()));
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    /* (non-Javadoc)
     * @see com.uniware.services.inflow.IInflowService#getRejectionReport(com.uniware.core.api.inflow.GetRejectionReportRequest)
     */
    @Override
    @Transactional(readOnly = true)
    public GetRejectionReportResponse getRejectionReport(GetRejectionReportRequest request) {
        GetRejectionReportResponse response = new GetRejectionReportResponse();
        ValidationContext context = request.validate();
        InflowReceipt inflowReceipt = null;
        if (!context.hasErrors()) {
            inflowReceipt = inflowDao.getInflowReceiptById(request.getInflowReceiptId());
            if (inflowReceipt == null) {
                context.addError(WsResponseCode.INVALID_INFLOW_RECEIPT_ID, "Invalid Inflow receipt id");
            } else if (InflowReceipt.StatusCode.CREATED.name().equals(inflowReceipt.getStatusCode())
                    || InflowReceipt.StatusCode.QC_PENDING.name().equals(inflowReceipt.getStatusCode())) {
                context.addError(WsResponseCode.INVALID_INFLOW_RECEIPT_STATE, "Rejection report can be created only after QC is complete");
            } else {
                sendQCRejectionReport(inflowReceipt, null);
                response.setSuccessful(true);
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    private void sendQCRejectionReport(InflowReceipt inflowReceipt, String email) {
        if (StringUtils.isNotBlank(email)) {
            InflowReceipt inflowReceiptDetailed = getDetailedInflowReceiptByCode(inflowReceipt.getCode());
            Template template = CacheManager.getInstance().getCache(PrintTemplateCache.class).getTemplateByType(PrintTemplateVO.Type.QC_REJECTION_REPORT.name());
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("inflowReceipt", inflowReceiptDetailed);
            String inflowReceiptReportHtml = template.evaluate(params);
            String directory = EnvironmentProperties.getQCRejectionReportPath();
            String attachmentName = "QC_REJECTION_REPORT-" + inflowReceipt.getCode();

            List<String> recipients = new ArrayList<String>();
            recipients.add(email);
            EmailMessage message = new EmailMessage(recipients, EmailTemplateType.QC_REJECTION_REPORT.name());
            message.addTemplateParam("inflowReceipt", inflowReceiptDetailed);

            File file = new File((directory.endsWith("/") ? directory : directory + "/" + attachmentName) + ".pdf");
            FileOutputStream fout = null;
            try {
                fout = new FileOutputStream(file);
                PrintTemplateVO qcRejectionReportTemplate = CacheManager.getInstance().getCache(PrintTemplateCache.class).getPrintTemplateByType(
                        PrintTemplateVO.Type.QC_REJECTION_REPORT);
                SamplePrintTemplateVO samplePrintTemplate = CacheManager.getInstance().getCache(SamplePrintTemplateCache.class).getSamplePrintTemplate(
                        qcRejectionReportTemplate.getSamplePrintTemplateCode());
                pdfDocumentService.writeHtmlToPdf(fout, inflowReceiptReportHtml, new PrintOptions(samplePrintTemplate));
                message.getAttachments().add(file);
            } catch (Exception e) {
                LOG.error("Error occured while sending qc rejection report mail:", e);
            } finally {
                FileUtils.safelyCloseOutputStream(fout);
            }

            emailService.send(message);
        }

    }

    @Override
    @Transactional
    public InflowReceiptItem getInflowReceiptItemById(int inflowReceiptItemId) {
        return inflowDao.getInflowReceiptItemById(inflowReceiptItemId);
    }

    @Override
    @Transactional
    public GetItemDetailFieldsResponse getItemDetailFields(GetItemDetailFieldsRequest request) {
        GetItemDetailFieldsResponse response = new GetItemDetailFieldsResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            Item item = inventoryService.getItemByCode(request.getItemCode());
            if (item == null) {
                context.addError(WsResponseCode.INVALID_ITEM_CODE, "Invalid Item Code");
            } else if (StringUtils.isNotBlank(request.getInflowReceiptCode()) && !item.getInflowReceiptItem().getInflowReceipt().getCode().equals(request.getInflowReceiptCode())) {
                context.addError(WsResponseCode.INVALID_INFLOW_RECEIPT_ID, "Item doesn't belong to this GRN");
            } else {
                List<ItemDetailFieldDTO> itemDetailFields = CacheManager.getInstance().getCache(ItemTypeDetailCache.class).getItemDetailFields(item.getItemType());
                response.setItemDetailFieldDTOs(itemDetailFields);
                response.setSuccessful(true);
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    public InflowReceipt getDetailedInflowReceiptByCode(String inflowReceiptCode) {
        InflowReceipt inflowReceipt = inflowDao.getInflowReceiptByCode(inflowReceiptCode);
        Hibernate.initialize(inflowReceipt.getUser());
        Hibernate.initialize(inflowReceipt.getInflowReceiptItems());
        Hibernate.initialize(inflowReceipt.getPurchaseOrder());
        Hibernate.initialize(inflowReceipt.getPurchaseOrder().getVendor());
        for (InflowReceiptItem inflowReceiptItem : inflowReceipt.getInflowReceiptItems()) {
            Hibernate.initialize(inflowReceiptItem.getItemType());
            Hibernate.initialize(inflowReceiptItem.getItemType().getCategory());
            Hibernate.initialize(inflowReceiptItem.getPurchaseOrderItem());
        }
        return inflowReceipt;
    }

    /**
     * @param request
     * @return
     */
    @Override
    @Transactional
    @RollbackOnFailure
    public AddItemToInflowReceiptWapResponse addItemToInflowReceiptWap(AddItemToInflowReceiptWapRequest request) {
        AddItemToInflowReceiptWapResponse response = new AddItemToInflowReceiptWapResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            AddItemLabelsRequest addItemLabelsRequest = new AddItemLabelsRequest();
            List<String> itemCodes = new ArrayList<>();
            itemCodes.add(request.getItemCode());
            addItemLabelsRequest.setItemCodes(itemCodes);
            addItemLabelsRequest.setItemSkuCode(request.getItemSKU());
            AddItemLabelsResponse addItemLabelsResponse = inventoryService.addItemLabels(addItemLabelsRequest);
            if (addItemLabelsResponse.isSuccessful()) {
                AddItemToInflowReceiptRequest addItemToInflowReceiptRequest = new AddItemToInflowReceiptRequest();
                addItemToInflowReceiptRequest.setManufacturingDate(request.getManufacturingDate());
                addItemToInflowReceiptRequest.setInflowReceiptCode(request.getInflowReceiptCode());
                addItemToInflowReceiptRequest.setItemCode(request.getItemCode());
                AddItemToInflowReceiptResponse addItemToInflowReceiptResponse = addItemToInflowReceipt(addItemToInflowReceiptRequest);
                if (addItemToInflowReceiptResponse.isSuccessful()) {
                    response.setSuccessful(true);
                    response.setInflowReceiptItemDTO(addItemToInflowReceiptResponse.getInflowReceiptItemDTO());
                } else {
                    context.getErrors().addAll(addItemToInflowReceiptResponse.getErrors());
                }
            } else {
                context.getErrors().addAll(addItemLabelsResponse.getErrors());
            }

        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Locks({ @Lock(ns = Namespace.SALE_ORDER, key = "#{#args[0].saleOrderCode}") })
    @Transactional
    public AddSaleOrderItemDetailResponse addSaleOrderItemDetails(AddSaleOrderItemDetailRequest request) {
        AddSaleOrderItemDetailResponse response = new AddSaleOrderItemDetailResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            saleOrderService.getSaleOrderForUpdate(request.getSaleOrderCode());
            SaleOrderItem saleOrderItem = saleOrderService.getSaleOrderItemByCode(request.getSaleOrderCode(), request.getSaleOrderItemCode());
            if (saleOrderItem == null) {
                context.addError(WsResponseCode.INVALID_SALE_ORDER_ITEM_CODE, "Invalid sale order item code");
            } else if (saleOrderItem.getItemDetailingStatus() != ItemDetailingStatus.PENDING) {
                context.addError(WsResponseCode.ITEM_DETAILS_NOT_EXPTECTED, "Item details not expected for this item");
            } else {
                Map<String, String> itemDetails = ItemDetail.getItemDetailsMap(request.getItemDetails());
                if (StringUtils.isNotBlank(saleOrderItem.getItemDetails())) {
                    Map<String, String> alreadyPresentDetails = JsonUtils.jsonToMap(saleOrderItem.getItemDetails());
                    alreadyPresentDetails.putAll(itemDetails);
                    itemDetails = alreadyPresentDetails;
                }
                Map<String, String> itemFieldValues = new HashMap<String, String>();
                List<ItemDetailFieldDTO> itemDetailFields = CacheManager.getInstance().getCache(ItemTypeDetailCache.class).getItemDetailFieldsByName(
                        StringUtils.split(saleOrderItem.getItemDetailFields()));
                for (ItemDetailFieldDTO itemDetail : itemDetailFields) {
                    String value = itemDetails.get(itemDetail.getName());
                    if (StringUtils.isBlank(value)) {
                        context.addError(WsResponseCode.ITEM_DETAILS_INVALID_FORMAT, "Item detail field " + itemDetail.getDisplayName() + " is required");
                    } else if (StringUtils.isNotBlank(itemDetail.getValidatorPattern()) && !value.matches(itemDetail.getValidatorPattern())) {
                        context.addError(WsResponseCode.ITEM_DETAILS_INVALID_FORMAT, "Invalid item detail value for :" + itemDetail.getDisplayName());
                    } else {
                        itemFieldValues.put(itemDetail.getName(), value);
                    }
                }
                if (!context.hasErrors()) {
                    saleOrderItem.setItemDetails(new Gson().toJson(itemFieldValues));
                    saleOrderItem.setItemDetailingStatus(ItemDetailingStatus.COMPLETE);
                    response.setSuccessful(true);
                }
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    public GetInflowReceiptsResponse getInflowReceipts(GetInflowReceiptsRequest request) {
        GetInflowReceiptsResponse response = new GetInflowReceiptsResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            for (InflowReceipt inflowReceipt : inflowDao.getInflowReceipts(request)) {
                response.getInflowReceiptCodes().add(inflowReceipt.getCode());
            }
            response.setSuccessful(true);
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    public InflowReceipt getInflowReceiptById(Integer inflowReceiptId) {
        return inflowDao.getInflowReceiptById(inflowReceiptId);
    }

    @Override
    @Transactional
    public List<InflowReceipt> getPreviousDayGrnSummary(DateRange dateRange, String vendorCode) {
        return inflowDao.getPreviousDayGrnSummary(dateRange, vendorCode);
    }

    @Override
    @Transactional
    public List<InflowReceiptStatus> getInflowReceiptStatuses() {
        return inflowDao.getInflowReceiptStatuses();
    }

    @Override
    @Transactional
    public InflowReceipt getInflowReceiptByCode(String inflowReceiptCode) {
        return inflowDao.getInflowReceiptByCode(inflowReceiptCode);
    }

    @Override
    @Transactional(readOnly = true)
    public String getInflowReceiptHtml(String inflowReceiptCode) {
        InflowReceipt inflowReceipt = getInflowReceiptByCode(inflowReceiptCode);
        if (inflowReceipt != null) {
            Map<String, Object> params = new HashMap<>();
            params.put("inflowReceipt", inflowReceipt);
            return CacheManager.getInstance().getCache(PrintTemplateCache.class).getTemplateByType(PrintTemplateVO.Type.INFLOW_RECEIPT.name()).evaluate(params);
        }
        return null;
    }
}
