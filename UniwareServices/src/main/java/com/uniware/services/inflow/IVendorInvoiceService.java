/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 14-Mar-2014
 *  @author karunsingla
 */
package com.uniware.services.inflow;

import com.unifier.core.template.Template;
import com.uniware.core.api.inflow.AddEditVendorInvoiceItemRequest;
import com.uniware.core.api.inflow.AddEditVendorInvoiceItemResponse;
import com.uniware.core.api.inflow.AddInflowReceiptToVendorInvoiceRequest;
import com.uniware.core.api.inflow.AddInflowReceiptToVendorInvoiceResponse;
import com.uniware.core.api.inflow.CompleteVendorInvoiceRequest;
import com.uniware.core.api.inflow.CompleteVendorInvoiceResponse;
import com.uniware.core.api.inflow.CreateDebitForCreditVendorInvoiceRequest;
import com.uniware.core.api.inflow.CreateDebitForCreditVendorInvoiceResponse;
import com.uniware.core.api.inflow.CreateVendorCreditInvoiceRequest;
import com.uniware.core.api.inflow.CreateVendorCreditInvoiceResponse;
import com.uniware.core.api.inflow.CreateVendorDebitInvoiceRequest;
import com.uniware.core.api.inflow.CreateVendorDebitInvoiceResponse;
import com.uniware.core.api.inflow.DiscardVendorInvoiceRequest;
import com.uniware.core.api.inflow.DiscardVendorInvoiceResponse;
import com.uniware.core.api.inflow.EditVendorInvoiceRequest;
import com.uniware.core.api.inflow.EditVendorInvoiceResponse;
import com.uniware.core.api.inflow.GetVendorInvoiceRequest;
import com.uniware.core.api.inflow.GetVendorInvoiceResponse;
import com.uniware.core.api.inflow.GetVendorInvoicesRequest;
import com.uniware.core.api.inflow.GetVendorInvoicesResponse;
import com.uniware.core.api.inflow.RemoveVendorInvoiceItemRequest;
import com.uniware.core.api.inflow.RemoveVendorInvoiceItemResponse;
import com.uniware.core.entity.VendorInvoice;

public interface IVendorInvoiceService {

    CreateVendorCreditInvoiceResponse createCreditVendorInvoice(CreateVendorCreditInvoiceRequest request);

    AddInflowReceiptToVendorInvoiceResponse addInflowReceiptToVendorInvoice(AddInflowReceiptToVendorInvoiceRequest request);

    GetVendorInvoiceResponse getVendorInvoice(GetVendorInvoiceRequest request);

    String getVendorInvoiceHtml(String vendorInvoiceCode, Template template);

    AddEditVendorInvoiceItemResponse addEditVendorInvoiceItem(AddEditVendorInvoiceItemRequest request);

    CreateDebitForCreditVendorInvoiceResponse createDebitForCreditVendorInvoice(CreateDebitForCreditVendorInvoiceRequest request);

    DiscardVendorInvoiceResponse discardVendorInvoice(DiscardVendorInvoiceRequest request);

    CompleteVendorInvoiceResponse completeVendorInvoice(CompleteVendorInvoiceRequest request);

    CreateVendorDebitInvoiceResponse createDebitVendorInvoice(CreateVendorDebitInvoiceRequest request);

    RemoveVendorInvoiceItemResponse removeVendorInvoiceItem(RemoveVendorInvoiceItemRequest request);

    EditVendorInvoiceResponse editVendorInvoice(EditVendorInvoiceRequest request);

    VendorInvoice getVendorInvoiceById(Integer vendorInvoiceId);

    GetVendorInvoicesResponse getVendorInvoices(GetVendorInvoicesRequest request);

}
