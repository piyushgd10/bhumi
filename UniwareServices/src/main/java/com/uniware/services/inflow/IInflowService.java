/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 *  @version     1.0, Feb 29, 2012
 *  @author singla
 */
package com.uniware.services.inflow;

import com.uniware.core.api.inflow.WsInflowReceiptItem;
import com.uniware.core.entity.PurchaseOrderItem;
import java.util.Date;
import java.util.List;

import com.unifier.core.utils.DateUtils.DateRange;
import com.uniware.core.api.inflow.AddItemDetailsRequest;
import com.uniware.core.api.inflow.AddItemDetailsResponse;
import com.uniware.core.api.inflow.AddItemToInflowReceiptRequest;
import com.uniware.core.api.inflow.AddItemToInflowReceiptResponse;
import com.uniware.core.api.inflow.AddItemToInflowReceiptWapRequest;
import com.uniware.core.api.inflow.AddItemToInflowReceiptWapResponse;
import com.uniware.core.api.inflow.AddOrEditInflowReceiptItemRequest;
import com.uniware.core.api.inflow.AddOrEditInflowReceiptItemResponse;
import com.uniware.core.api.inflow.AddSaleOrderItemDetailRequest;
import com.uniware.core.api.inflow.AddSaleOrderItemDetailResponse;
import com.uniware.core.api.inflow.CompleteInflowReceiptItemsQCRequest;
import com.uniware.core.api.inflow.CompleteInflowReceiptItemsQCResponse;
import com.uniware.core.api.inflow.CreateInflowReceiptRequest;
import com.uniware.core.api.inflow.CreateInflowReceiptResponse;
import com.uniware.core.api.inflow.CreateReceiptItemCodesRequest;
import com.uniware.core.api.inflow.CreateReceiptItemCodesResponse;
import com.uniware.core.api.inflow.DiscardInflowReceiptRequest;
import com.uniware.core.api.inflow.DiscardInflowReceiptResponse;
import com.uniware.core.api.inflow.DiscardTraceableInflowReceiptItemRequest;
import com.uniware.core.api.inflow.DiscardTraceableInflowReceiptItemResponse;
import com.uniware.core.api.inflow.EditInflowReceiptRequest;
import com.uniware.core.api.inflow.EditInflowReceiptResponse;
import com.uniware.core.api.inflow.GetInflowReceiptItemsRequest;
import com.uniware.core.api.inflow.GetInflowReceiptItemsResponse;
import com.uniware.core.api.inflow.GetInflowReceiptRequest;
import com.uniware.core.api.inflow.GetInflowReceiptResponse;
import com.uniware.core.api.inflow.GetInflowReceiptsRequest;
import com.uniware.core.api.inflow.GetInflowReceiptsResponse;
import com.uniware.core.api.inflow.GetItemDetailFieldsRequest;
import com.uniware.core.api.inflow.GetItemDetailFieldsResponse;
import com.uniware.core.api.inflow.GetPOInflowReceiptsRequest;
import com.uniware.core.api.inflow.GetPOInflowReceiptsResponse;
import com.uniware.core.api.inflow.GetRejectionReportRequest;
import com.uniware.core.api.inflow.GetRejectionReportResponse;
import com.uniware.core.api.inflow.RejectNonTraceableItemRequest;
import com.uniware.core.api.inflow.RejectNonTraceableItemResponse;
import com.uniware.core.api.inflow.RejectTraceableItemRequest;
import com.uniware.core.api.inflow.RejectTraceableItemResponse;
import com.uniware.core.api.inflow.SearchReceiptRequest;
import com.uniware.core.api.inflow.SearchReceiptResponse;
import com.uniware.core.api.inflow.SendInflowReceiptForQCRequest;
import com.uniware.core.api.inflow.SendInflowReceiptForQCResponse;
import com.uniware.core.api.inflow.SendInflowReceiptItemsForQCRequest;
import com.uniware.core.api.inflow.SendInflowReceiptItemsForQCResponse;
import com.uniware.core.api.inflow.UpdateTraceableInflowReceiptItemRequest;
import com.uniware.core.api.inflow.UpdateTraceableInflowReceiptItemResponse;
import com.uniware.core.entity.InflowReceipt;
import com.uniware.core.entity.InflowReceiptItem;
import com.uniware.core.entity.InflowReceiptStatus;
import com.uniware.core.entity.Item;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author singla
 */
public interface IInflowService {

    /**
     * @param request
     * @return
     */
    CreateInflowReceiptResponse createInflowReceipt(CreateInflowReceiptRequest request);

    /**
     * @param request
     * @return
     */
    RejectNonTraceableItemResponse rejectNonTraceableItem(RejectNonTraceableItemRequest request);

    /**
     * @param request
     * @return
     */
    GetInflowReceiptResponse getInflowReceipt(GetInflowReceiptRequest request);

    /**
     * @param request
     * @return
     */
    GetPOInflowReceiptsResponse getAllInflowReceipts(GetPOInflowReceiptsRequest request);

    /**
     * @param request
     * @return
     */
    SearchReceiptResponse searchReceipt(SearchReceiptRequest request);

    /**
     * @param receiptItemId
     * @return
     */
    List<Item> getItemsByReceiptItemId(int receiptItemId);

    @Transactional(readOnly = true) List<Item> getItemsByReceiptItemId(int receiptItemId, boolean refresh);

    /**
     * @param statusCode
     * @param beforeTime
     * @return
     */
    List<InflowReceipt> getInflowReceiptsByStatus(String statusCode, Date pastHours);

    /**
     * @param request
     * @return
     */
    AddOrEditInflowReceiptItemResponse addOrEditInflowReceiptItem(AddOrEditInflowReceiptItemRequest request);

    void prepareInflowReceiptItem(InflowReceiptItem inflowReceiptItem, PurchaseOrderItem purchaseOrderItem,
            WsInflowReceiptItem request);

    void prepareinflowReceiptItemTaxation(InflowReceiptItem inflowReceiptItem, PurchaseOrderItem purchaseOrderItem);

    /**
     * @param request
     * @return
     */
    CreateReceiptItemCodesResponse createItemCodes(CreateReceiptItemCodesRequest request);

    /**
     * @param request
     * @return
     */
    RejectTraceableItemResponse rejectTraceableItem(RejectTraceableItemRequest request);

    /**
     * This method is invoked when all GRN line items are added and after this method line item addition is not possible
     * 
     * @param request
     * @return
     */
    SendInflowReceiptForQCResponse sendInflowReceiptForQC(SendInflowReceiptForQCRequest request);

    /**
     * @param request
     * @return
     */
    AddItemDetailsResponse addItemDetails(AddItemDetailsRequest request);

    UpdateTraceableInflowReceiptItemResponse updateTraceableItem(UpdateTraceableInflowReceiptItemRequest request);

    /**
     * @param request
     * @return
     */
    GetRejectionReportResponse getRejectionReport(GetRejectionReportRequest request);

    /**
     * @param request
     * @return
     */
    DiscardTraceableInflowReceiptItemResponse discardTraceableInflowReceiptItem(DiscardTraceableInflowReceiptItemRequest request);

    /**
     * @param inflowReceiptId
     * @return
     */
    InflowReceiptItem getInflowReceiptItemById(int inflowReceiptItemId);

    /**
     * @param request
     * @return
     */
    SendInflowReceiptItemsForQCResponse sendInflowReceiptItemsForQC(SendInflowReceiptItemsForQCRequest request);

    /**
     * @param request
     * @return
     */
    CompleteInflowReceiptItemsQCResponse completeInflowReceiptItemsQC(CompleteInflowReceiptItemsQCRequest request);

    GetItemDetailFieldsResponse getItemDetailFields(GetItemDetailFieldsRequest request);

    /**
     * @param request
     * @return
     */
    AddItemToInflowReceiptResponse addItemToInflowReceipt(AddItemToInflowReceiptRequest request);

    /**
     * @param request
     * @return
     */
    DiscardInflowReceiptResponse discardInflowReceipt(DiscardInflowReceiptRequest request);

    /**
     * @param inflowReceiptId
     * @return
     */
    InflowReceipt getDetailedInflowReceiptByCode(String inflowReceiptCode);

    /**
     * @param request
     * @return
     */
    AddItemToInflowReceiptWapResponse addItemToInflowReceiptWap(AddItemToInflowReceiptWapRequest request);

    /**
     * @param request
     * @return
     */

    EditInflowReceiptResponse editInflowReceipt(EditInflowReceiptRequest request);

    GetInflowReceiptItemsResponse getInflowReceiptItems(GetInflowReceiptItemsRequest request);

    InflowReceipt getInflowReceiptById(Integer inflowReceiptId);

    List<InflowReceipt> getPreviousDayGrnSummary(DateRange dateRange, String vendorCode);

    List<InflowReceiptStatus> getInflowReceiptStatuses();

    AddSaleOrderItemDetailResponse addSaleOrderItemDetails(AddSaleOrderItemDetailRequest request);

    GetInflowReceiptsResponse getInflowReceipts(GetInflowReceiptsRequest request);

    InflowReceipt getInflowReceiptByCode(String inflowReceiptCode);

    String getInflowReceiptHtml(String inflowReceiptCode);
}