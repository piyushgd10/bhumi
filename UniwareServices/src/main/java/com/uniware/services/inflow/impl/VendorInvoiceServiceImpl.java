/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 14-Mar-2014
 *  @author karunsingla
 */
package com.uniware.services.inflow.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.template.Template;
import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.NumberUtils;
import com.unifier.core.utils.StringUtils;
import com.unifier.services.aspect.RollbackOnFailure;
import com.unifier.services.users.IUsersService;
import com.unifier.services.utils.CustomFieldUtils;
import com.uniware.core.api.inflow.AddEditVendorInvoiceItemRequest;
import com.uniware.core.api.inflow.AddEditVendorInvoiceItemResponse;
import com.uniware.core.api.inflow.AddInflowReceiptToVendorInvoiceRequest;
import com.uniware.core.api.inflow.AddInflowReceiptToVendorInvoiceResponse;
import com.uniware.core.api.inflow.CompleteVendorInvoiceRequest;
import com.uniware.core.api.inflow.CompleteVendorInvoiceResponse;
import com.uniware.core.api.inflow.CreateDebitForCreditVendorInvoiceRequest;
import com.uniware.core.api.inflow.CreateDebitForCreditVendorInvoiceResponse;
import com.uniware.core.api.inflow.CreateVendorCreditInvoiceRequest;
import com.uniware.core.api.inflow.CreateVendorCreditInvoiceResponse;
import com.uniware.core.api.inflow.CreateVendorDebitInvoiceRequest;
import com.uniware.core.api.inflow.CreateVendorDebitInvoiceResponse;
import com.uniware.core.api.inflow.DiscardVendorInvoiceRequest;
import com.uniware.core.api.inflow.DiscardVendorInvoiceResponse;
import com.uniware.core.api.inflow.EditVendorInvoiceRequest;
import com.uniware.core.api.inflow.EditVendorInvoiceResponse;
import com.uniware.core.api.inflow.GetVendorInvoiceRequest;
import com.uniware.core.api.inflow.GetVendorInvoiceResponse;
import com.uniware.core.api.inflow.GetVendorInvoiceResponse.VendorInvoiceDTO;
import com.uniware.core.api.inflow.GetVendorInvoiceResponse.VendorInvoiceItemDTO;
import com.uniware.core.api.inflow.GetVendorInvoicesRequest;
import com.uniware.core.api.inflow.GetVendorInvoicesResponse;
import com.uniware.core.api.inflow.GetVendorInvoicesResponse.VendorInvoiceDetailDTO;
import com.uniware.core.api.inflow.RemoveVendorInvoiceItemRequest;
import com.uniware.core.api.inflow.RemoveVendorInvoiceItemResponse;
import com.uniware.core.api.inflow.WsVendorInvoiceItem;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.cache.FacilityCache;
import com.uniware.core.entity.Facility;
import com.uniware.core.entity.InflowReceipt;
import com.uniware.core.entity.InflowReceiptItem;
import com.uniware.core.entity.ItemType;
import com.uniware.core.entity.Party;
import com.uniware.core.entity.PartyAddress;
import com.uniware.core.entity.PartyAddressType;
import com.uniware.core.entity.PurchaseOrder;
import com.uniware.core.entity.Vendor;
import com.uniware.core.entity.VendorInvoice;
import com.uniware.core.entity.VendorInvoice.Type;
import com.uniware.core.entity.VendorInvoiceItem;
import com.uniware.dao.inflow.IInflowDao;
import com.uniware.dao.inflow.IVendorInvoiceDao;
import com.uniware.dao.purchase.IPurchaseDao;
import com.uniware.dao.vendor.IVendorDao;
import com.uniware.services.catalog.ICatalogService;
import com.uniware.services.configuration.SystemConfiguration;
import com.uniware.services.inflow.IVendorInvoiceService;
import com.uniware.services.party.IPartyService;

@Service("vendorInvoiceService")
public class VendorInvoiceServiceImpl implements IVendorInvoiceService {

    @Autowired
    private IVendorInvoiceDao vendorInvoiceDao;

    @Autowired
    private IUsersService     usersService;

    @Autowired
    private IPurchaseDao      purchaseDao;

    @Autowired
    private ICatalogService   catalogService;

    @Autowired
    private IInflowDao        inflowDao;

    @Autowired
    private IPartyService     partyService;

    @Autowired
    private IVendorDao        vendorDao;

    @Override
    @Transactional
    public CreateVendorCreditInvoiceResponse createCreditVendorInvoice(CreateVendorCreditInvoiceRequest request) {
        CreateVendorCreditInvoiceResponse response = new CreateVendorCreditInvoiceResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            PurchaseOrder purchaseOrder = purchaseDao.getPurchaseOrderByCode(request.getVendorInvoice().getPurchaseOrderCode(), true);
            if (purchaseOrder == null) {
                context.addError(WsResponseCode.INVALID_PURCHASE_ORDER_CODE, "Invalid Purchase Order Code");
            } else if (!StringUtils.equalsAny(purchaseOrder.getStatusCode(), PurchaseOrder.StatusCode.APPROVED.name(), PurchaseOrder.StatusCode.COMPLETE.name())) {
                context.addError(WsResponseCode.INVALID_PURCHASE_ORDER_STATE, "Purchase Order is not in APPROVED/COMPLETE state");
            } else if (request.getVendorInvoice().getVendorInvoiceDate().before(DateUtils.clearTime(purchaseOrder.getApproveTime()))) {
                context.addError(WsResponseCode.INVALID_DATE, "vendor invoice date should be after purchase order approval date");
            } else {
                VendorInvoice vendorInvoice = vendorInvoiceDao.getVendorInvoiceByNumber(purchaseOrder.getVendor().getId(), request.getVendorInvoice().getVendorInvoiceNumber());
                if (vendorInvoice != null) {
                    context.addError(WsResponseCode.DUPLICATE_VENDOR_INVOICE_NUMBER, "Vendor invoice is already created for given vendor invoice number");
                } else {
                    vendorInvoice = new VendorInvoice();
                    vendorInvoice.setType(Type.CREDIT);
                    vendorInvoice.setPurchaseOrder(purchaseOrder);
                    vendorInvoice.setFromParty(purchaseOrder.getFromParty());
                    vendorInvoice.setToParty(purchaseOrder.getVendor());
                    vendorInvoice.setStatusCode(VendorInvoice.StatusCode.CREATED.name());
                    vendorInvoice.setCreated(DateUtils.getCurrentTime());
                    vendorInvoice.setUser(usersService.getUserWithDetailsById(request.getUserId()));
                    if (StringUtils.isNotBlank(request.getVendorInvoice().getCurrencyCode())) {
                        vendorInvoice.setCurrencyCode(request.getVendorInvoice().getCurrencyCode());
                    } else {
                        vendorInvoice.setCurrencyCode(purchaseOrder.getCurrencyCode());
                    }
                    if (request.getVendorInvoice().getCurrencyConversionRate() != null) {
                        vendorInvoice.setCurrencyConversionRate(request.getVendorInvoice().getCurrencyConversionRate());
                    } else {
                        vendorInvoice.setCurrencyConversionRate(purchaseOrder.getCurrencyConversionRate());
                    }
                    vendorInvoice.setVendorInvoiceDate(request.getVendorInvoice().getVendorInvoiceDate());
                    vendorInvoice.setVendorInvoiceNumber(request.getVendorInvoice().getVendorInvoiceNumber());
                    if (request.getVendorInvoice().getCustomFieldValues() != null) {
                        CustomFieldUtils.setCustomFieldValues(vendorInvoice,
                                CustomFieldUtils.getCustomFieldValues(context, VendorInvoice.class.getName(), request.getVendorInvoice().getCustomFieldValues()));
                    }
                    vendorInvoice = vendorInvoiceDao.addVendorCreditInvoice(vendorInvoice);
                    response.setVendorInvoiceCode(vendorInvoice.getCode());
                    response.setSuccessful(true);
                }
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    public CreateVendorDebitInvoiceResponse createDebitVendorInvoice(CreateVendorDebitInvoiceRequest request) {
        CreateVendorDebitInvoiceResponse response = new CreateVendorDebitInvoiceResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            Party toParty = partyService.getPartyByCode(request.getVendorInvoice().getPartyCode());

            if (toParty == null) {
                context.addError(WsResponseCode.INVALID_PARTY_CODE, "Invalid Vendor Code");
            } else {
                VendorInvoice vendorInvoice = new VendorInvoice();
                Facility facility = CacheManager.getInstance().getCache(FacilityCache.class).getCurrentFacility();
                vendorInvoice.setFromParty(facility);
                vendorInvoice.setToParty(toParty);
                vendorInvoice.setType(Type.DEBIT);
                vendorInvoice.setStatusCode(VendorInvoice.StatusCode.CREATED.name());
                vendorInvoice.setCreated(DateUtils.getCurrentTime());
                vendorInvoice.setUser(usersService.getUserWithDetailsById(request.getUserId()));
                vendorInvoice.setCurrencyCode(request.getVendorInvoice().getCurrencyCode());
                vendorInvoice.setCurrencyConversionRate(request.getVendorInvoice().getCurrencyConversionRate());
                if (request.getVendorInvoice().getCustomFieldValues() != null) {
                    CustomFieldUtils.setCustomFieldValues(vendorInvoice,
                            CustomFieldUtils.getCustomFieldValues(context, VendorInvoice.class.getName(), request.getVendorInvoice().getCustomFieldValues()));
                }
                vendorInvoice = vendorInvoiceDao.addVendorDebitInvoice(vendorInvoice);
                response.setVendorInvoiceCode(vendorInvoice.getCode());
                response.setSuccessful(true);
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    public AddInflowReceiptToVendorInvoiceResponse addInflowReceiptToVendorInvoice(AddInflowReceiptToVendorInvoiceRequest request) {
        AddInflowReceiptToVendorInvoiceResponse response = new AddInflowReceiptToVendorInvoiceResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            PurchaseOrder purchaseOrder = purchaseDao.getPurchaseOrderByCode(request.getPurchaseOrderCode(), true);
            VendorInvoice vendorInvoice = vendorInvoiceDao.getVendorInvoiceByCode(request.getVendorInvoiceCode());
            InflowReceipt inflowReceipt = inflowDao.getInflowReceiptByCode(request.getInflowReceiptCode());
            if (purchaseOrder == null) {
                context.addError(WsResponseCode.INVALID_PURCHASE_ORDER_CODE, "Invalid Purchase Order Code");
            } else if (vendorInvoice == null) {
                context.addError(WsResponseCode.INVALID_VENDOR_INVOICE_CODE, "Invalid Vendor Invoice Code");
            } else if (!VendorInvoice.StatusCode.CREATED.name().equals(vendorInvoice.getStatusCode())) {
                context.addError(WsResponseCode.INVALID_VENDOR_INVOICE_STATE, "Invalid Vendor Invoice not in CREATED state");
            } else if (inflowReceipt == null) {
                context.addError(WsResponseCode.INVALID_INFLOW_RECEIPT_CODE, "Invalid Inflow Receipt Code");
            } else if (!StringUtils.equalsAny(inflowReceipt.getStatusCode(), InflowReceipt.StatusCode.QC_COMPLETE.name(), InflowReceipt.StatusCode.COMPLETE.name())) {
                context.addError(WsResponseCode.INVALID_INFLOW_RECEIPT_STATE, "Inflow Receipt not in QC_COMPLETE/COMPLETE state");
            } else if (inflowReceipt.getVendorInvoice() != null) {
                context.addError(WsResponseCode.INVALID_INFLOW_RECEIPT_STATE, "Inflow Receipt already associated to a vendor invoice");
            } else if (!inflowReceipt.getPurchaseOrder().getId().equals(purchaseOrder.getId())) {
                context.addError(WsResponseCode.INVALID_INFLOW_RECEIPT_CODE, "Inflow Receipt doesnt belong to given purchase order");
            } else {
                Map<String, VendorInvoiceItem> productSkuToVendorInvoiceItems = new HashMap<String, VendorInvoiceItem>();
                for (VendorInvoiceItem vendorInvoiceItem : vendorInvoice.getVendorInvoiceItems()) {
                    if (vendorInvoiceItem.getItemType() != null) {
                        productSkuToVendorInvoiceItems.put(vendorInvoiceItem.getItemType().getSkuCode(), vendorInvoiceItem);
                    }
                }
                for (InflowReceiptItem inflowReceiptItem : inflowReceipt.getInflowReceiptItems()) {
                    VendorInvoiceItem vendorInvoiceItem = productSkuToVendorInvoiceItems.get(inflowReceiptItem.getItemType().getSkuCode());
                    if (vendorInvoiceItem == null) {
                        vendorInvoiceItem = new VendorInvoiceItem();
                        vendorInvoiceItem.setVendorInvoice(vendorInvoice);
                        vendorInvoiceItem.setItemType(inflowReceiptItem.getItemType());
                        vendorInvoiceItem.setDescription(new StringBuilder().append(inflowReceiptItem.getItemType().getName()).append(" (").append(
                                inflowReceiptItem.getItemType().getSkuCode()).append(")").toString());
                        vendorInvoiceItem.setCreated(DateUtils.getCurrentTime());
                        vendorInvoiceItem.setUnitPrice(inflowReceiptItem.getUnitPrice());
                        vendorInvoiceItem.setDiscount(inflowReceiptItem.getDiscount());
                        vendorInvoiceItem.setQuantity(inflowReceiptItem.getQuantity());
                        vendorInvoiceItem.setSubtotal(inflowReceiptItem.getSubtotal());
                        vendorInvoiceItem.setVat(inflowReceiptItem.getVat());
                        vendorInvoiceItem.setCst(inflowReceiptItem.getCst());
                        vendorInvoiceItem.setTaxPercentage(inflowReceiptItem.getPurchaseOrderItem().getTaxPercentage());
                        vendorInvoiceItem.setAdditionalTax(inflowReceiptItem.getAdditionalTax());
                        vendorInvoiceItem.setAdditionalTaxPercentage(inflowReceiptItem.getPurchaseOrderItem().getAdditionalTaxPercentage());
                        vendorInvoiceItem.setCentralGst(inflowReceiptItem.getCentralGst());
                        vendorInvoiceItem.setCentralGstPercentage(inflowReceiptItem.getPurchaseOrderItem().getCentralGstPercentage());
                        vendorInvoiceItem.setStateGst(inflowReceiptItem.getStateGst());
                        vendorInvoiceItem.setStateGstPercentage(inflowReceiptItem.getPurchaseOrderItem().getStateGstPercentage());
                        vendorInvoiceItem.setUnionTerritoryGst(inflowReceiptItem.getUnionTerritoryGst());
                        vendorInvoiceItem.setUnionTerritoryGstPercentage(inflowReceiptItem.getPurchaseOrderItem().getUnionTerritoryGstPercentage());
                        vendorInvoiceItem.setIntegratedGst(inflowReceiptItem.getIntegratedGst());
                        vendorInvoiceItem.setIntegratedGstPercentage(inflowReceiptItem.getPurchaseOrderItem().getIntegratedGstPercentage());
                        vendorInvoiceItem.setCompensationCess(inflowReceiptItem.getCompensationCess());
                        vendorInvoiceItem.setCompensationCessPercentage(inflowReceiptItem.getPurchaseOrderItem().getCompensationCessPercentage());
                        vendorInvoiceItem.setTotal(inflowReceiptItem.getTotal());
                        vendorInvoice.getVendorInvoiceItems().add(vendorInvoiceItem);
                    } else {
                        int currentQuantity = vendorInvoiceItem.getQuantity();
                        int totalQuantity = currentQuantity + inflowReceiptItem.getQuantity();
                        vendorInvoiceItem.setSubtotal(NumberUtils.multiply(vendorInvoiceItem.getUnitPrice().subtract(vendorInvoiceItem.getDiscount()), totalQuantity));
                        vendorInvoiceItem.setQuantity(totalQuantity);
                        vendorInvoiceItem.setCst(NumberUtils.divide(NumberUtils.multiply(vendorInvoiceItem.getCst(), totalQuantity), currentQuantity));
                        vendorInvoiceItem.setVat(NumberUtils.divide(NumberUtils.multiply(vendorInvoiceItem.getVat(), totalQuantity), currentQuantity));
                        vendorInvoiceItem.setAdditionalTax(NumberUtils.divide(NumberUtils.multiply(vendorInvoiceItem.getAdditionalTax(), totalQuantity), currentQuantity));
                        vendorInvoiceItem.setCentralGst(NumberUtils.divide(NumberUtils.multiply(vendorInvoiceItem.getCentralGst(), totalQuantity), currentQuantity));
                        vendorInvoiceItem.setStateGst(NumberUtils.divide(NumberUtils.multiply(vendorInvoiceItem.getStateGst(), totalQuantity), currentQuantity));
                        vendorInvoiceItem.setUnionTerritoryGst(NumberUtils.divide(NumberUtils.multiply(vendorInvoiceItem.getUnionTerritoryGst(), totalQuantity), currentQuantity));
                        vendorInvoiceItem.setIntegratedGst(NumberUtils.divide(NumberUtils.multiply(vendorInvoiceItem.getIntegratedGst(), totalQuantity), currentQuantity));
                        vendorInvoiceItem.setCompensationCess(NumberUtils.divide(NumberUtils.multiply(vendorInvoiceItem.getCompensationCess(), totalQuantity), currentQuantity));
                        BigDecimal totalTaxAmount = vendorInvoiceItem.getVat().add(vendorInvoiceItem.getCst()).add(vendorInvoiceItem.getCentralGst()).add(
                                vendorInvoiceItem.getStateGst()).add(vendorInvoiceItem.getUnionTerritoryGst()).add(vendorInvoiceItem.getIntegratedGst()).add(
                                        vendorInvoiceItem.getCompensationCess());
                        vendorInvoiceItem.setTotal(vendorInvoiceItem.getSubtotal().add(totalTaxAmount));
                    }
                }
                inflowReceipt.setVendorInvoice(vendorInvoice);
                vendorInvoice = vendorInvoiceDao.updateVendorInvoice(vendorInvoice);
                response.setSuccessful(true);
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    public GetVendorInvoiceResponse getVendorInvoice(GetVendorInvoiceRequest request) {
        GetVendorInvoiceResponse response = new GetVendorInvoiceResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            VendorInvoice vendorInvoice = vendorInvoiceDao.getVendorInvoiceByCode(request.getVendorInvoiceCode(), false);
            if (vendorInvoice != null) {
                VendorInvoiceDTO vendorInvoiceDTO = new VendorInvoiceDTO();
                vendorInvoiceDTO.setCode(vendorInvoice.getCode());
                vendorInvoiceDTO.setType(vendorInvoice.getType().name());
                vendorInvoiceDTO.setCreated(vendorInvoice.getCreated());
                vendorInvoiceDTO.setCreatedBy(vendorInvoice.getUser().getUsername());
                if (vendorInvoice.getPurchaseOrder() != null) {
                    vendorInvoiceDTO.setPurchaseOrderCode(vendorInvoice.getPurchaseOrder().getCode());
                }
                if (vendorInvoice.getCreditVendorInvoice() != null) {
                    vendorInvoiceDTO.setCreditInvoiceCode(vendorInvoice.getCreditVendorInvoice().getCode());
                }
                vendorInvoiceDTO.setVendorInvoiceNumber(vendorInvoice.getVendorInvoiceNumber());
                vendorInvoiceDTO.setVendorInvoiceDate(vendorInvoice.getVendorInvoiceDate());
                vendorInvoiceDTO.setVendorCode(vendorInvoice.getToParty().getCode());
                vendorInvoiceDTO.setVendorName(vendorInvoice.getToParty().getName());
                vendorInvoiceDTO.setStatusCode(vendorInvoice.getStatusCode());
                vendorInvoiceDTO.setCustomFieldValues(CustomFieldUtils.getCustomFieldValuesDTO(vendorInvoice));
                for (VendorInvoiceItem vendorInvoiceItem : vendorInvoice.getVendorInvoiceItems()) {
                    VendorInvoiceItemDTO vendorInvoiceItemDTO = new VendorInvoiceItemDTO();
                    if (vendorInvoiceItem.getItemType() != null) {
                        vendorInvoiceItemDTO.setItemTypeName(vendorInvoiceItem.getItemType().getName());
                        vendorInvoiceItemDTO.setItemTypeSkuCode(vendorInvoiceItem.getItemType().getSkuCode());
                    }
                    vendorInvoiceItemDTO.setQuantity(vendorInvoiceItem.getQuantity());
                    vendorInvoiceItemDTO.setCst(vendorInvoiceItem.getCst());
                    vendorInvoiceItemDTO.setVat(vendorInvoiceItem.getVat());
                    vendorInvoiceItemDTO.setDiscount(vendorInvoiceItem.getDiscount());
                    vendorInvoiceItemDTO.setTaxPercentage(vendorInvoiceItem.getTaxPercentage());
                    vendorInvoiceItemDTO.setCentralGst(vendorInvoiceItem.getCentralGst());
                    vendorInvoiceItemDTO.setCentralGstPercentage(vendorInvoiceItem.getCentralGstPercentage());
                    vendorInvoiceItemDTO.setStateGst(vendorInvoiceItem.getStateGst());
                    vendorInvoiceItemDTO.setStateGstPercentage(vendorInvoiceItem.getStateGstPercentage());
                    vendorInvoiceItemDTO.setUnionTerritoryGst(vendorInvoiceItem.getUnionTerritoryGst());
                    vendorInvoiceItemDTO.setUnionTerritoryGstPercentage(vendorInvoiceItem.getUnionTerritoryGstPercentage());
                    vendorInvoiceItemDTO.setIntegratedGst(vendorInvoiceItem.getIntegratedGst());
                    vendorInvoiceItemDTO.setIntegratedGstPercentage(vendorInvoiceItem.getIntegratedGstPercentage());
                    vendorInvoiceItemDTO.setCompensationCess(vendorInvoiceItem.getCompensationCess());
                    vendorInvoiceItemDTO.setCompensationCessPercentage(vendorInvoiceItem.getCompensationCessPercentage());
                    if (ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).isVendorPricesTaxExclusive()) {
                        vendorInvoiceItemDTO.setUnitPrice(vendorInvoiceItem.getUnitPrice());
                    } else {
                        vendorInvoiceItemDTO.setUnitPrice(NumberUtils.divide(vendorInvoiceItem.getTotal(), vendorInvoiceItem.getQuantity()));
                    }
                    vendorInvoiceItemDTO.setTotal(vendorInvoiceItem.getTotal());
                    vendorInvoiceItemDTO.setSubtotal(vendorInvoiceItem.getSubtotal());
                    vendorInvoiceItemDTO.setDescription(vendorInvoiceItem.getDescription());
                    vendorInvoiceDTO.addVendorInvoiceItemDTO(vendorInvoiceItemDTO);
                }
                List<String> inflowReceipts = new ArrayList<String>();
                for (InflowReceipt inflowReceipt : vendorInvoice.getInflowReceipts()) {
                    inflowReceipts.add(inflowReceipt.getCode());
                }
                vendorInvoiceDTO.setInflowReceipts(inflowReceipts);

                if (Type.CREDIT.equals(vendorInvoice.getType())) {
                    List<VendorInvoice> debitInvoices = vendorInvoiceDao.getDebitVendorInvoicesByCreditInvoice(vendorInvoice.getCode());
                    List<String> debitInvoiceCodes = new ArrayList<String>();
                    for (VendorInvoice debitInvoice : debitInvoices) {
                        debitInvoiceCodes.add(debitInvoice.getCode());
                    }
                    vendorInvoiceDTO.setDebitInvoiceCodes(debitInvoiceCodes);
                }

                response.setVendorInvoice(vendorInvoiceDTO);
                response.setSuccessful(true);
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    public GetVendorInvoicesResponse getVendorInvoices(GetVendorInvoicesRequest request) {
        GetVendorInvoicesResponse response = new GetVendorInvoicesResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            PurchaseOrder purchaseOrder = purchaseDao.getPurchaseOrderByCode(request.getPurchaseOrderCode());
            if (purchaseOrder == null) {
                context.addError(WsResponseCode.INVALID_PURCHASE_ORDER_CODE, "Invalid purchase order code");
            } else {
                List<VendorInvoice> vendorInvoices = vendorInvoiceDao.getVendorInvoicesByPurchaseOrderCode(purchaseOrder.getCode());
                List<VendorInvoiceDetailDTO> vendorInvoiceDTOs = new ArrayList<>();
                for (VendorInvoice vendorInvoice : vendorInvoices) {
                    VendorInvoiceDetailDTO invoiceDetails = new VendorInvoiceDetailDTO();
                    invoiceDetails.setCode(vendorInvoice.getCode());
                    invoiceDetails.setCreatedBy(vendorInvoice.getUser().getUsername());
                    invoiceDetails.setCreatedOn(vendorInvoice.getCreated());
                    invoiceDetails.setInvoiceDate(vendorInvoice.getVendorInvoiceDate());
                    invoiceDetails.setStatus(vendorInvoice.getStatusCode());
                    invoiceDetails.setType(vendorInvoice.getType().name());
                    int noOfItems = 0;
                    BigDecimal amount = BigDecimal.ZERO;
                    for (VendorInvoiceItem vendorInvoiceItem : vendorInvoice.getVendorInvoiceItems()) {
                        noOfItems += vendorInvoiceItem.getQuantity();
                        amount = amount.add(vendorInvoiceItem.getTotal());
                    }
                    invoiceDetails.setAmount(amount);
                    invoiceDetails.setNoOfItems(noOfItems);
                    if (!vendorInvoice.getInflowReceipts().isEmpty()) {
                        StringBuilder grnCodes = new StringBuilder();
                        for (InflowReceipt inflowReceipt : vendorInvoice.getInflowReceipts()) {
                            if (grnCodes.indexOf(inflowReceipt.getCode()) == -1) {
                                grnCodes.append(inflowReceipt.getCode()).append(',');
                            }
                        }
                        invoiceDetails.setGrnAdded(grnCodes.deleteCharAt(grnCodes.length() - 1).toString());
                    }
                    vendorInvoiceDTOs.add(invoiceDetails);
                }
                response.setVandorInvoices(vendorInvoiceDTOs);
            }
        }
        response.setSuccessful(!context.hasErrors());
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    @Override
    @Transactional
    public String getVendorInvoiceHtml(String vendorInvoiceCode, Template template) {
        VendorInvoice vendorInvoice = vendorInvoiceDao.getVendorInvoiceByCode(vendorInvoiceCode, false);
        if (vendorInvoice != null) {
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("vendorInvoice", vendorInvoice);
            return template.evaluate(params);
        } else {
            return null;
        }
    }

    @Override
    @Transactional
    @RollbackOnFailure
    public AddEditVendorInvoiceItemResponse addEditVendorInvoiceItem(AddEditVendorInvoiceItemRequest request) {
        AddEditVendorInvoiceItemResponse response = new AddEditVendorInvoiceItemResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            VendorInvoice vendorInvoice = vendorInvoiceDao.getVendorInvoiceByCode(request.getVendorInvoiceCode(), true);
            if (vendorInvoice == null) {
                context.addError(WsResponseCode.INVALID_VENDOR_INVOICE_CODE, "Invalid Vendor Invoice Code");
            } else if (!VendorInvoice.StatusCode.CREATED.name().equals(vendorInvoice.getStatusCode())) {
                context.addError(WsResponseCode.INVALID_VENDOR_INVOICE_STATE, "Invalid Vendor Invoice not in CREATED state");
            } else {
                for (WsVendorInvoiceItem wsVendorInvoiceItem : request.getVendorInvoiceItems()) {
                    ItemType itemType = null;
                    if (StringUtils.isNotBlank(wsVendorInvoiceItem.getItemSkuCode())) {
                        itemType = catalogService.getItemTypeBySkuCode(wsVendorInvoiceItem.getItemSkuCode());
                        if (itemType == null) {
                            context.addError(WsResponseCode.INVALID_ITEM_TYPE, "Invalid item sku code");
                        }
                    }
                    if (!context.hasErrors()) {
                        VendorInvoiceItem vendorInvoiceItem = null;
                        for (VendorInvoiceItem item : vendorInvoice.getVendorInvoiceItems()) {
                            if (itemType != null
                                    && item.getItemType() != null
                                    && item.getItemType().getId().equals(itemType.getId())
                                    || item.getItemType() == null
                                    && itemType == null
                                    && StringUtils.removeNonWordChars(wsVendorInvoiceItem.getDescription()).toLowerCase().equals(
                                            StringUtils.removeNonWordChars(item.getDescription()).toLowerCase())) {
                                vendorInvoiceItem = item;
                            }
                        }
                        if (vendorInvoiceItem == null) {
                            vendorInvoiceItem = new VendorInvoiceItem();
                            vendorInvoiceItem.setCreated(DateUtils.getCurrentTime());
                            vendorInvoiceItem.setVendorInvoice(vendorInvoice);
                        }
                        if (itemType != null) {
                            vendorInvoiceItem.setItemType(itemType);
                        }
                        vendorInvoiceItem.setDescription(wsVendorInvoiceItem.getDescription());
                        vendorInvoiceItem.setQuantity(wsVendorInvoiceItem.getQuantity());
                        PartyAddress toPartyShippingAddress = vendorInvoice.getToParty().getPartyAddressByType(PartyAddressType.Code.SHIPPING.name());
                        PartyAddress warehouseShippingAddress = vendorInvoice.getFromParty().getPartyAddressByType(PartyAddressType.Code.SHIPPING.name());
                        if (ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).isVendorPricesTaxExclusive()) {
                            vendorInvoiceItem.setUnitPrice(wsVendorInvoiceItem.getUnitPrice());
                            vendorInvoiceItem.setDiscount(wsVendorInvoiceItem.getDiscount());
                            vendorInvoiceItem.setSubtotal(vendorInvoiceItem.getUnitPrice().subtract(vendorInvoiceItem.getDiscount()).multiply(
                                    new BigDecimal(wsVendorInvoiceItem.getQuantity())).setScale(2, RoundingMode.HALF_EVEN));
                            BigDecimal taxAmount = NumberUtils.getPercentageFromBase(vendorInvoiceItem.getSubtotal(), wsVendorInvoiceItem.getTaxPercentage());
                            if (warehouseShippingAddress.getStateCode().equals(toPartyShippingAddress.getStateCode())) {
                                vendorInvoiceItem.setVat(taxAmount);
                            } else {
                                vendorInvoiceItem.setCst(taxAmount);
                            }
                            vendorInvoiceItem.setAdditionalTax(NumberUtils.getPercentageFromBase(taxAmount, wsVendorInvoiceItem.getAdditionalTaxPercentage()));
                            vendorInvoiceItem.setTaxPercentage(wsVendorInvoiceItem.getTaxPercentage());
                            vendorInvoiceItem.setAdditionalTaxPercentage(wsVendorInvoiceItem.getAdditionalTaxPercentage());
                            BigDecimal cGSTAmount = NumberUtils.getPercentageFromBase(vendorInvoiceItem.getSubtotal(), wsVendorInvoiceItem.getCentralGstPercentage());
                            vendorInvoiceItem.setCentralGst(cGSTAmount);
                            vendorInvoiceItem.setCentralGstPercentage(wsVendorInvoiceItem.getCentralGstPercentage());
                            BigDecimal sGSTAmount = NumberUtils.getPercentageFromBase(vendorInvoiceItem.getSubtotal(), wsVendorInvoiceItem.getStateGstPercentage());
                            vendorInvoiceItem.setStateGst(sGSTAmount);
                            vendorInvoiceItem.setStateGstPercentage(wsVendorInvoiceItem.getStateGstPercentage());
                            BigDecimal uGSTAmount = NumberUtils.getPercentageFromBase(vendorInvoiceItem.getSubtotal(), wsVendorInvoiceItem.getUnionTerritoryGstPercentage());
                            vendorInvoiceItem.setUnionTerritoryGst(uGSTAmount);
                            vendorInvoiceItem.setUnionTerritoryGstPercentage(wsVendorInvoiceItem.getUnionTerritoryGstPercentage());
                            BigDecimal iGSTAmount = NumberUtils.getPercentageFromBase(vendorInvoiceItem.getSubtotal(), wsVendorInvoiceItem.getIntegratedGstPercentage());
                            vendorInvoiceItem.setIntegratedGst(iGSTAmount);
                            vendorInvoiceItem.setIntegratedGstPercentage(wsVendorInvoiceItem.getIntegratedGstPercentage());
                            BigDecimal compensationCessAmount = NumberUtils.getPercentageFromBase(vendorInvoiceItem.getSubtotal(),
                                    wsVendorInvoiceItem.getCompensationCessPercentage());
                            vendorInvoiceItem.setCompensationCess(compensationCessAmount);
                            vendorInvoiceItem.setCompensationCessPercentage(wsVendorInvoiceItem.getCompensationCessPercentage());
                            BigDecimal totalTaxAmount = vendorInvoiceItem.getVat().add(vendorInvoiceItem.getCst()).add(vendorInvoiceItem.getAdditionalTax()).add(
                                    vendorInvoiceItem.getCentralGst()).add(vendorInvoiceItem.getStateGst()).add(vendorInvoiceItem.getUnionTerritoryGst()).add(
                                            vendorInvoiceItem.getIntegratedGst()).add(vendorInvoiceItem.getCompensationCess());
                            vendorInvoiceItem.setTotal(vendorInvoiceItem.getSubtotal().add(totalTaxAmount));
                        } else {
                            vendorInvoiceItem.setTotal(wsVendorInvoiceItem.getUnitPrice().subtract(wsVendorInvoiceItem.getDiscount()).multiply(
                                    new BigDecimal(wsVendorInvoiceItem.getQuantity())).setScale(2, RoundingMode.HALF_EVEN));
                            BigDecimal taxWithAdditionalTax = NumberUtils.getPercentageFromTotal(
                                    vendorInvoiceItem.getTotal(),
                                    wsVendorInvoiceItem.getTaxPercentage().multiply(
                                            new BigDecimal(100).add(wsVendorInvoiceItem.getAdditionalTaxPercentage()).divide(new BigDecimal(100))));
                            vendorInvoiceItem.setAdditionalTax(NumberUtils.getPercentageFromTotal(taxWithAdditionalTax, wsVendorInvoiceItem.getAdditionalTaxPercentage()));
                            if (warehouseShippingAddress.getStateCode().equals(toPartyShippingAddress.getStateCode())) {
                                vendorInvoiceItem.setVat(taxWithAdditionalTax.subtract(vendorInvoiceItem.getAdditionalTax()));
                            } else {
                                vendorInvoiceItem.setCst(taxWithAdditionalTax.subtract(vendorInvoiceItem.getAdditionalTax()));
                            }
                            vendorInvoiceItem.setTaxPercentage(wsVendorInvoiceItem.getTaxPercentage());
                            vendorInvoiceItem.setAdditionalTaxPercentage(wsVendorInvoiceItem.getAdditionalTaxPercentage());
                            BigDecimal totalGstTaxPercentage = wsVendorInvoiceItem.getCentralGstPercentage().add(wsVendorInvoiceItem.getStateGstPercentage()).add(
                                    wsVendorInvoiceItem.getUnionTerritoryGstPercentage()).add(wsVendorInvoiceItem.getIntegratedGstPercentage()).add(
                                            wsVendorInvoiceItem.getCompensationCessPercentage());
                            BigDecimal totalGstTax = NumberUtils.getPercentageFromTotal(vendorInvoiceItem.getTotal(), totalGstTaxPercentage);
                            if (totalGstTaxPercentage.compareTo(BigDecimal.ZERO) > 0) {
                                vendorInvoiceItem.setCentralGst(
                                        wsVendorInvoiceItem.getCentralGstPercentage().multiply(totalGstTax).divide(totalGstTaxPercentage, 2, RoundingMode.HALF_EVEN).setScale(2,
                                                RoundingMode.HALF_EVEN));
                                vendorInvoiceItem.setStateGst(
                                        wsVendorInvoiceItem.getStateGstPercentage().multiply(totalGstTax).divide(totalGstTaxPercentage, 2, RoundingMode.HALF_EVEN).setScale(2,
                                                RoundingMode.HALF_EVEN));
                                vendorInvoiceItem.setUnionTerritoryGst(wsVendorInvoiceItem.getUnionTerritoryGstPercentage().multiply(totalGstTax).divide(totalGstTaxPercentage, 2,
                                        RoundingMode.HALF_EVEN).setScale(2, RoundingMode.HALF_EVEN));
                                vendorInvoiceItem.setIntegratedGst(
                                        wsVendorInvoiceItem.getIntegratedGstPercentage().multiply(totalGstTax).divide(totalGstTaxPercentage, 2, RoundingMode.HALF_EVEN).setScale(2,
                                                RoundingMode.HALF_EVEN));
                                vendorInvoiceItem.setCompensationCess(
                                        wsVendorInvoiceItem.getCompensationCessPercentage().multiply(totalGstTax).divide(totalGstTaxPercentage, 2, RoundingMode.HALF_EVEN).setScale(
                                                2, RoundingMode.HALF_EVEN));
                                vendorInvoiceItem.setCentralGstPercentage(wsVendorInvoiceItem.getCentralGstPercentage());
                                vendorInvoiceItem.setStateGstPercentage(wsVendorInvoiceItem.getStateGstPercentage());
                                vendorInvoiceItem.setUnionTerritoryGstPercentage(wsVendorInvoiceItem.getUnionTerritoryGstPercentage());
                                vendorInvoiceItem.setIntegratedGstPercentage(wsVendorInvoiceItem.getIntegratedGstPercentage());
                                vendorInvoiceItem.setCompensationCessPercentage(wsVendorInvoiceItem.getCompensationCessPercentage());
                            }
                            BigDecimal totalTax = totalGstTax.add(vendorInvoiceItem.getVat()).add(vendorInvoiceItem.getCst()).add(vendorInvoiceItem.getAdditionalTax());
                            vendorInvoiceItem.setSubtotal(vendorInvoiceItem.getTotal().subtract(totalTax));
                            BigDecimal unitPriceWithoutDiscountAndTax = vendorInvoiceItem.getSubtotal().divide(new BigDecimal(vendorInvoiceItem.getQuantity()), 2,
                                    RoundingMode.HALF_EVEN);
                            BigDecimal percentage = NumberUtils.getPercentageFromBaseAndPercentageAmount(vendorInvoiceItem.getSubtotal(), totalTax);
                            BigDecimal discountWithoutTax = wsVendorInvoiceItem.getDiscount().subtract(
                                    NumberUtils.getPercentageFromTotal(wsVendorInvoiceItem.getDiscount(), percentage));
                            vendorInvoiceItem.setDiscount(discountWithoutTax);
                            vendorInvoiceItem.setUnitPrice(unitPriceWithoutDiscountAndTax.add(discountWithoutTax));
                        }
                        vendorInvoiceItem = vendorInvoiceDao.updateVendorInvoiceItem(vendorInvoiceItem);
                    }
                }
                if (!context.hasErrors()) {
                    response.setSuccessful(true);
                }
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    @RollbackOnFailure
    public CreateDebitForCreditVendorInvoiceResponse createDebitForCreditVendorInvoice(CreateDebitForCreditVendorInvoiceRequest request) {
        CreateDebitForCreditVendorInvoiceResponse response = new CreateDebitForCreditVendorInvoiceResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            VendorInvoice vendorInvoice = vendorInvoiceDao.getVendorInvoiceByCode(request.getCreditVendorInvoiceCode(), true);
            if (vendorInvoice == null) {
                context.addError(WsResponseCode.INVALID_VENDOR_INVOICE_CODE, "Invalid Vendor Invoice Code");
            } else if (!VendorInvoice.StatusCode.COMPLETE.name().equals(vendorInvoice.getStatusCode())) {
                context.addError(WsResponseCode.INVALID_VENDOR_INVOICE_STATE, "Invalid Vendor Invoice not in COMPLETE state");
            } else if (!VendorInvoice.Type.CREDIT.equals(vendorInvoice.getType())) {
                context.addError(WsResponseCode.INVALID_VENDOR_INVOICE_CODE, "Debit Note can only be created for CREDIT invoice type");
            } else {
                VendorInvoice debitVendorInvoice = new VendorInvoice();
                debitVendorInvoice.setType(Type.DEBIT);
                debitVendorInvoice.setFromParty(vendorInvoice.getFromParty());
                debitVendorInvoice.setToParty(vendorInvoice.getToParty());
                debitVendorInvoice.setStatusCode(VendorInvoice.StatusCode.CREATED.name());
                debitVendorInvoice.setPurchaseOrder(vendorInvoice.getPurchaseOrder());
                debitVendorInvoice.setVendorInvoiceNumber(vendorInvoice.getVendorInvoiceNumber());
                debitVendorInvoice.setVendorInvoiceDate(vendorInvoice.getVendorInvoiceDate());
                debitVendorInvoice.setCreated(DateUtils.getCurrentTime());
                debitVendorInvoice.setUser(usersService.getUserWithDetailsById(request.getUserId()));
                debitVendorInvoice.setCurrencyCode(vendorInvoice.getCurrencyCode());
                debitVendorInvoice.setCurrencyConversionRate(vendorInvoice.getCurrencyConversionRate());
                debitVendorInvoice.setCreditVendorInvoice(vendorInvoice);

                Map<Integer, VendorInvoiceDetails> itemTypeIdToVendorInvoiceDetails = new HashMap<Integer, VendorInvoiceDetails>();
                for (InflowReceipt inflowReceipt : vendorInvoice.getInflowReceipts()) {
                    for (InflowReceiptItem inflowReceiptItem : inflowReceipt.getInflowReceiptItems()) {
                        VendorInvoiceDetails vendorInvoiceDetails = itemTypeIdToVendorInvoiceDetails.get(inflowReceiptItem.getItemType().getId());
                        if (vendorInvoiceDetails == null) {
                            vendorInvoiceDetails = new VendorInvoiceDetails();
                            itemTypeIdToVendorInvoiceDetails.put(inflowReceiptItem.getItemType().getId(), vendorInvoiceDetails);
                        }
                        vendorInvoiceDetails.setTotalPrice(vendorInvoiceDetails.getTotalPrice().add(inflowReceiptItem.getTotal()));
                        vendorInvoiceDetails.setQuantity(vendorInvoiceDetails.getQuantity() + inflowReceiptItem.getQuantity());
                        vendorInvoiceDetails.setRejectedQuantity(vendorInvoiceDetails.getRejectedQuantity() + inflowReceiptItem.getRejectedQuantity());
                    }
                }
                for (VendorInvoiceItem vendorInvoiceItem : vendorInvoice.getVendorInvoiceItems()) {
                    if (vendorInvoiceItem.getItemType() == null || !itemTypeIdToVendorInvoiceDetails.containsKey(vendorInvoiceItem.getItemType().getId())) {
                        VendorInvoiceItem debitVendorInvoiceItem = new VendorInvoiceItem(vendorInvoiceItem);
                        debitVendorInvoiceItem.setVendorInvoice(debitVendorInvoice);
                        debitVendorInvoice.getVendorInvoiceItems().add(debitVendorInvoiceItem);
                    } else {
                        VendorInvoiceDetails vendorInvoiceDetails = itemTypeIdToVendorInvoiceDetails.get(vendorInvoiceItem.getItemType().getId());

                        if (vendorInvoiceItem.getQuantity() > vendorInvoiceDetails.getQuantity()) {
                            prepareDebitInvoiceItem(debitVendorInvoice, vendorInvoiceItem, vendorInvoiceItem.getQuantity() - vendorInvoiceDetails.getQuantity(),
                                    "Excess quantity billed for: " + vendorInvoiceItem.getDescription());
                        }

                        if (request.isAddQCRejectedItems() && vendorInvoiceDetails.getRejectedQuantity() > 0) {
                            prepareDebitInvoiceItem(debitVendorInvoice, vendorInvoiceItem, vendorInvoiceDetails.getRejectedQuantity(), "QC Rejected quantity for: "
                                    + vendorInvoiceItem.getDescription());
                        }
                        if (!request.isIgnorePriceDiscrepancy()) {
                            BigDecimal billedUnitPrice = NumberUtils.divide(vendorInvoiceItem.getTotal(), vendorInvoiceItem.getQuantity());
                            BigDecimal expectedUnitPrice = NumberUtils.divide(vendorInvoiceDetails.getTotalPrice(), vendorInvoiceDetails.getQuantity());
                            int quantity = request.isAddQCRejectedItems() ? vendorInvoiceDetails.getQuantity() - vendorInvoiceDetails.getRejectedQuantity()
                                    : vendorInvoiceDetails.getQuantity();
                            if (NumberUtils.greaterThan(billedUnitPrice, expectedUnitPrice) && quantity > 0) {
                                VendorInvoiceItem debitVendorInvoiceItem = new VendorInvoiceItem();
                                debitVendorInvoiceItem.setVendorInvoice(debitVendorInvoice);
                                debitVendorInvoiceItem.setItemType(vendorInvoiceItem.getItemType());
                                debitVendorInvoiceItem.setDescription("Price discrepancy for :" + vendorInvoiceItem.getDescription());
                                debitVendorInvoiceItem.setUnitPrice(billedUnitPrice.subtract(expectedUnitPrice));
                                debitVendorInvoiceItem.setQuantity(quantity);
                                debitVendorInvoiceItem.setSubtotal(NumberUtils.multiply(debitVendorInvoiceItem.getUnitPrice(), debitVendorInvoiceItem.getQuantity()));
                                debitVendorInvoiceItem.setTotal(debitVendorInvoiceItem.getSubtotal());
                                debitVendorInvoiceItem.setCreated(DateUtils.getCurrentTime());
                                debitVendorInvoice.getVendorInvoiceItems().add(debitVendorInvoiceItem);
                            }
                        }
                    }
                }
                if (debitVendorInvoice.getVendorInvoiceItems().size() > 0) {
                    debitVendorInvoice = vendorInvoiceDao.addVendorDebitInvoice(debitVendorInvoice);
                    response.setSuccessful(true);
                    response.setVendorInvoiceCode(debitVendorInvoice.getCode());
                } else {
                    context.addError(WsResponseCode.INVALID_VENDOR_INVOICE_STATE, "There are no items to debit");
                }
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    private void prepareDebitInvoiceItem(VendorInvoice debitVendorInvoice, VendorInvoiceItem vendorInvoiceItem, int quantity, String description) {
        VendorInvoiceItem debitVendorInvoiceItem = new VendorInvoiceItem(vendorInvoiceItem);
        debitVendorInvoiceItem.setDescription(description);
        debitVendorInvoiceItem.setQuantity(quantity);
        debitVendorInvoiceItem.setSubtotal(NumberUtils.divide(NumberUtils.multiply(debitVendorInvoiceItem.getSubtotal(), debitVendorInvoiceItem.getQuantity()),
                vendorInvoiceItem.getQuantity()));
        debitVendorInvoiceItem.setVat(NumberUtils.divide(NumberUtils.multiply(debitVendorInvoiceItem.getVat(), debitVendorInvoiceItem.getQuantity()),
                vendorInvoiceItem.getQuantity()));
        debitVendorInvoiceItem.setCst(NumberUtils.divide(NumberUtils.multiply(debitVendorInvoiceItem.getCst(), debitVendorInvoiceItem.getQuantity()),
                vendorInvoiceItem.getQuantity()));
        debitVendorInvoiceItem.setAdditionalTax(NumberUtils.divide(NumberUtils.multiply(debitVendorInvoiceItem.getAdditionalTax(), debitVendorInvoiceItem.getQuantity()),
                vendorInvoiceItem.getQuantity()));
        debitVendorInvoiceItem.setCentralGst(
                NumberUtils.divide(NumberUtils.multiply(debitVendorInvoiceItem.getCentralGst(), debitVendorInvoiceItem.getQuantity()), vendorInvoiceItem.getQuantity()));
        debitVendorInvoiceItem.setStateGst(
                NumberUtils.divide(NumberUtils.multiply(debitVendorInvoiceItem.getStateGst(), debitVendorInvoiceItem.getQuantity()), vendorInvoiceItem.getQuantity()));
        debitVendorInvoiceItem.setUnionTerritoryGst(
                NumberUtils.divide(NumberUtils.multiply(debitVendorInvoiceItem.getUnionTerritoryGst(), debitVendorInvoiceItem.getQuantity()), vendorInvoiceItem.getQuantity()));
        debitVendorInvoiceItem.setIntegratedGst(
                NumberUtils.divide(NumberUtils.multiply(debitVendorInvoiceItem.getIntegratedGst(), debitVendorInvoiceItem.getQuantity()), vendorInvoiceItem.getQuantity()));
        debitVendorInvoiceItem.setCompensationCess(
                NumberUtils.divide(NumberUtils.multiply(debitVendorInvoiceItem.getCompensationCess(), debitVendorInvoiceItem.getQuantity()), vendorInvoiceItem.getQuantity()));
        debitVendorInvoiceItem.setTotal(NumberUtils.divide(NumberUtils.multiply(debitVendorInvoiceItem.getTotal(), debitVendorInvoiceItem.getQuantity()),
                vendorInvoiceItem.getQuantity()));
        debitVendorInvoiceItem.setVendorInvoice(debitVendorInvoice);
        debitVendorInvoice.getVendorInvoiceItems().add(debitVendorInvoiceItem);
    }

    @Override
    @Transactional
    public RemoveVendorInvoiceItemResponse removeVendorInvoiceItem(RemoveVendorInvoiceItemRequest request) {
        RemoveVendorInvoiceItemResponse response = new RemoveVendorInvoiceItemResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            VendorInvoice vendorInvoice = vendorInvoiceDao.getVendorInvoiceByCode(request.getVendorInvoiceCode(), true);
            if (vendorInvoice == null) {
                context.addError(WsResponseCode.INVALID_VENDOR_INVOICE_CODE, "Invalid Vendor Invoice Code");
            } else if (!VendorInvoice.StatusCode.CREATED.name().equals(vendorInvoice.getStatusCode())) {
                context.addError(WsResponseCode.INVALID_VENDOR_INVOICE_STATE, "Invalid Vendor Invoice not in COMPLETE state");
            } else {
                VendorInvoiceItem vendorInvoiceItem = null;
                ItemType itemType = null;
                if (StringUtils.isNotBlank(request.getItemSkuCode())) {
                    itemType = catalogService.getItemTypeBySkuCode(request.getItemSkuCode());
                    if (itemType == null) {
                        context.addError(WsResponseCode.INVALID_ITEM_TYPE, "Invalid item sku code");
                    }
                }
                for (VendorInvoiceItem item : vendorInvoice.getVendorInvoiceItems()) {
                    if (itemType != null && item.getItemType() != null && item.getItemType().getId().equals(itemType.getId()) || item.getItemType() == null && itemType == null
                            && StringUtils.removeNonWordChars(request.getDescription()).toLowerCase().equals(StringUtils.removeNonWordChars(item.getDescription()).toLowerCase())) {
                        vendorInvoiceItem = item;
                    }
                }
                if (vendorInvoiceItem == null) {
                    context.addError(WsResponseCode.INVALID_VENDOR_INVOICE_ITEM, "Invalid vendor invoice item");
                } else {
                    vendorInvoiceDao.removeVendorInvoiceItem(vendorInvoiceItem);
                    response.setSuccessful(true);
                }
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    public DiscardVendorInvoiceResponse discardVendorInvoice(DiscardVendorInvoiceRequest request) {
        DiscardVendorInvoiceResponse response = new DiscardVendorInvoiceResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            VendorInvoice vendorInvoice = vendorInvoiceDao.getVendorInvoiceByCode(request.getVendorInvoiceCode(), true);
            if (vendorInvoice == null) {
                context.addError(WsResponseCode.INVALID_VENDOR_INVOICE_CODE, "Invalid Vendor Invoice Code");
            } else if (!VendorInvoice.StatusCode.CREATED.name().equals(vendorInvoice.getStatusCode())) {
                context.addError(WsResponseCode.INVALID_VENDOR_INVOICE_STATE, "Invalid Vendor Invoice not in CREATED state");
            } else {
                if (vendorInvoice.getInflowReceipts().size() > 0) {
                    for (InflowReceipt inflowReceipt : vendorInvoice.getInflowReceipts()) {
                        inflowReceipt.setVendorInvoice(null);
                    }
                }
                vendorInvoice.setStatusCode(VendorInvoice.StatusCode.DISCARDED.name());
                response.setSuccessful(true);
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    public CompleteVendorInvoiceResponse completeVendorInvoice(CompleteVendorInvoiceRequest request) {
        CompleteVendorInvoiceResponse response = new CompleteVendorInvoiceResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            VendorInvoice vendorInvoice = vendorInvoiceDao.getVendorInvoiceByCode(request.getVendorInvoiceCode(), true);
            if (vendorInvoice == null) {
                context.addError(WsResponseCode.INVALID_VENDOR_INVOICE_CODE, "Invalid Vendor Invoice Code");
            } else if (!VendorInvoice.StatusCode.CREATED.name().equals(vendorInvoice.getStatusCode())) {
                context.addError(WsResponseCode.INVALID_VENDOR_INVOICE_STATE, "Invalid Vendor Invoice not in CREATED state");
            } else {
                vendorInvoice.setStatusCode(VendorInvoice.StatusCode.COMPLETE.name());
                response.setSuccessful(true);
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    public EditVendorInvoiceResponse editVendorInvoice(EditVendorInvoiceRequest request) {
        EditVendorInvoiceResponse response = new EditVendorInvoiceResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            Vendor vendor = vendorDao.getVendorByCode(request.getVendorCode());
            if (vendor == null) {
                context.addError(WsResponseCode.INVALID_VENDOR_CODE, "Invalid vendor code " + request.getVendorCode());
            } else {
                VendorInvoice vendorInvoice = vendorInvoiceDao.getVendorInvoiceByCode(request.getVendorInvoiceCode());
                if (vendorInvoice == null) {
                    context.addError(WsResponseCode.INVALID_VENDOR_INVOICE_CODE, "Invalid vendor invoice number " + request.getVendorInvoiceNumber());
                } else {
                    vendorInvoice.setVendorInvoiceNumber(request.getVendorInvoiceNumber());
                    vendorInvoice.setVendorInvoiceDate(request.getVendorInvoiceDate());
                    if (request.getCustomFieldValues() != null) {
                        CustomFieldUtils.setCustomFieldValues(vendorInvoice,
                                CustomFieldUtils.getCustomFieldValues(context, VendorInvoice.class.getName(), request.getCustomFieldValues()), false);
                    }
                    vendorInvoiceDao.updateVendorInvoice(vendorInvoice);
                    response.setSuccessful(true);
                }
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    private static class VendorInvoiceDetails {
        private BigDecimal totalPrice = BigDecimal.ZERO;
        private int        quantity;
        private int        rejectedQuantity;

        public BigDecimal getTotalPrice() {
            return totalPrice;
        }

        public void setTotalPrice(BigDecimal totalPrice) {
            this.totalPrice = totalPrice;
        }

        public int getQuantity() {
            return quantity;
        }

        public void setQuantity(int quantity) {
            this.quantity = quantity;
        }

        public int getRejectedQuantity() {
            return rejectedQuantity;
        }

        public void setRejectedQuantity(int rejectedQuantity) {
            this.rejectedQuantity = rejectedQuantity;
        }
    }

    @Override
    public VendorInvoice getVendorInvoiceById(Integer vendorInvoiceId) {
        return vendorInvoiceDao.getVendorInvoiceById(vendorInvoiceId);
    }

}
