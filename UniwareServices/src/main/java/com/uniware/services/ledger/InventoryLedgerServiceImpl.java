/**
 * Copyright 2017 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version 1.0, 19/09/17
 * @author aditya
 */
package com.uniware.services.ledger;

import static com.unifier.core.utils.CollectionUtils.isEmpty;
import static com.uniware.core.api.ledger.FetchInventoryLedgerSummaryResponse.LedgerEntryGroup;
import static com.uniware.core.api.ledger.FetchInventoryLedgerSummaryResponse.LedgerKey;
import static com.uniware.core.entity.InventoryLedger.ChangeType;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.unifier.core.api.validation.ResponseCode;
import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.utils.CollectionUtils;
import com.unifier.core.utils.DateUtils;
import com.uniware.core.api.ledger.BulkInventoryLedgerSummaryRequest;
import com.uniware.core.api.ledger.BulkInventoryLedgerSummaryResponse;
import com.uniware.core.api.ledger.FetchInventoryLedgerRequest;
import com.uniware.core.api.ledger.FetchInventoryLedgerResponse;
import com.uniware.core.api.ledger.FetchInventoryLedgerSummaryRequest;
import com.uniware.core.api.ledger.FetchInventoryLedgerSummaryResponse;
import com.uniware.core.api.ledger.FetchInventoryLedgerSummaryResponse.LedgerEntryDTO;
import com.uniware.core.api.ledger.FetchInventoryLedgerSummaryResponse.LedgerEntryGroupDTO;
import com.uniware.core.api.ledger.InventoryLedgerDTO;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.entity.InventoryLedger;
import com.uniware.core.entity.InventoryLedger.StatusCode;
import com.uniware.core.entity.ItemType;
import com.uniware.dao.ledger.IInventoryLedgerDao;
import com.uniware.services.catalog.ICatalogService;

@Service("inventoryLedgerService")
public class InventoryLedgerServiceImpl implements IInventoryLedgerService {

    private static final Logger LOG = LoggerFactory.getLogger(InventoryLedgerServiceImpl.class);

    @Autowired
    private IInventoryLedgerDao inventoryLedgerDao;

    @Autowired
    private ICatalogService     catalogService;

    @Override
    public List<InventoryLedger> getInventoryLedgerEntries(Date from, int start, int pageSize) {
        return inventoryLedgerDao.getInventoryLedgerEntries(from, start, pageSize);
    }

    @Override
    public void addMonthlyOpeningBalanceForItemTypes(List<ItemType> itemTypes) {
    }

    /**
     * Sets old and new balance for an {@link InventoryLedger} and forwards it to
     * {@link IInventoryLedgerDao#addInventoryLedgerEntry(InventoryLedger)} for persistence in db.
     * <ul>
     * <li>If there is no previous entry for the sku of {@link InventoryLedger}, then the old balance is set to new
     * balance of that entry. Else, the old balance is set to 0.</li>
     * <li>New balance is calculate by {@code changeQuantity} and {@code changeType}</li>
     * </ul>
     * <p>
     * This method is usually called whenever inventory leaves or gets in the warehouse.
     * </p>
     * 
     * @param inventoryLedger InventoryLedger whose old and new balances are to be changed.
     * @param changeType Whether inventory was increased, decreased or replaced.
     * @param changeQuantity The amount of inventory
     */
    @Override
    @Transactional
    public InventoryLedger addInventoryLedgerEntry(InventoryLedger inventoryLedger, int changeQuantity, ChangeType changeType) {
        assert inventoryLedger != null;
        int oldBalance = 0;
        InventoryLedger lastLedgerEntry = inventoryLedgerDao.getLastInventoryLedgerEntry(inventoryLedger.getSkuCode());
        if (lastLedgerEntry != null) {
            oldBalance = lastLedgerEntry.getNewBalance();
        }
        inventoryLedger.setOldBalance(oldBalance);
        inventoryLedger.setNewBalance(prepareNewBalance(oldBalance, changeQuantity, changeType));
        inventoryLedger.setCreated(DateUtils.getCurrentTime());
        inventoryLedger.setStatusCode(StatusCode.LIVE);
        return inventoryLedgerDao.addInventoryLedgerEntry(inventoryLedger);

    }

    /**
     * Determines the new balance based on {@code oldBalance} and {@code changeType}. See {@link ChangeType} for
     * details.
     *
     * @see ChangeType
     */
    private int prepareNewBalance(int oldBalance, int changeQuantity, ChangeType changeType) {
        int newBalance = 0;
        switch (changeType) {
            case INCREASE:
                newBalance = oldBalance + changeQuantity;
                break;
            case DECREASE:
                newBalance = oldBalance - changeQuantity;
                break;
        }
        return newBalance;
    }

    /**
     * Returns the ledger entries of a particular sku. See documentation of request and response for details.
     */
    @Override
    @Transactional(readOnly = true)
    public FetchInventoryLedgerResponse fetchInventoryLedger(FetchInventoryLedgerRequest request) {
        FetchInventoryLedgerResponse response = new FetchInventoryLedgerResponse();
        ValidationContext context = request.validate();
        response.setErrors(context.getErrors());
        response.setWarnings(context.getWarnings());
        if (context.hasErrors()) {
            return response;
        }
        ItemType itemType = catalogService.getItemTypeBySkuCode(request.getSkuCode());
        if (itemType == null) {
            context.addError(WsResponseCode.INVALID_ITEM_SKU_CODE, "No item type found with given sku");
            return response;
        }
        List<InventoryLedger> inventoryLedgers = inventoryLedgerDao.fetchInventoryLedger(itemType.getSkuCode(), request.getFacilityCodes(), request.getFrom(), request.getTo(),
                request.getStart(), request.getPageSize());
        if (!isEmpty(inventoryLedgers)) {
            // We can't simply return the oldBalance of first ledger entry in inventoryLedgers, due to pagination.
            Integer openingBalance = inventoryLedgerDao.getOpeningBalance(itemType.getSkuCode(), request.getFrom());
            Long countLedgers = inventoryLedgerDao.getInventoryLedgerEntriesCountInDateRange(request.getSkuCode(), request.getFacilityCodes(), request.getFrom(), request.getTo());
            List<InventoryLedgerDTO> ledgers = inventoryLedgers.stream().map(InventoryLedgerDTO::new).collect(Collectors.toList());
            response.setOpeningBalance(openingBalance);
            response.setTotalCount(countLedgers);
            response.setInventoryLedgerDTOs(ledgers);
            response.setSuccessful(true);
        } else {
            response.setMessage("No ledger entry found for given sku and date range.");
        }
        return response;
    }

    /**
     *
     *
     */
    @Override
    @Transactional(readOnly = true)
    public FetchInventoryLedgerSummaryResponse fetchInventoryLedgerSummary(FetchInventoryLedgerSummaryRequest request) {
        FetchInventoryLedgerSummaryResponse response = new FetchInventoryLedgerSummaryResponse();
        ValidationContext context = request.validate();
        response.setErrors(context.getErrors());
        if (context.hasErrors()) {
            return response;
        }
        ItemType itemType = catalogService.getItemTypeBySkuCode(request.getSkuCode());
        if (itemType == null) {
            context.addError(WsResponseCode.INVALID_ITEM_SKU_CODE, "No item type found with given sku");
            return response;
        }
        boolean wasEntryFoundInDateRange = true;
        List<InventoryLedger> inventoryLedgers = inventoryLedgerDao.fetchInventoryLedgerSummary(itemType.getSkuCode(), request.getFacilityCodes(), request.getFrom(),
                request.getTo());
        if (CollectionUtils.isEmpty(inventoryLedgers)) {
            LOG.info("Could not find any ledger in specified date range, fetching latest ledger entry before the {} date", request.getFrom());
            wasEntryFoundInDateRange = false;
            inventoryLedgers = Collections.singletonList(inventoryLedgerDao.getLastInventoryLedgerEntry(itemType.getSkuCode(), request.getFacilityCodes(), request.getFrom()));
        }
        LOG.debug("sellerInventoryLedgers: {}", inventoryLedgers.toString());
        Map<String, LedgerEntryGroup> facilityToLedgerEntryGroupMap = prepareFacilityToLedgerEntryGroupsMap(inventoryLedgers, wasEntryFoundInDateRange);
        prepareResponseFromFacilityLedgerMap(facilityToLedgerEntryGroupMap, response, wasEntryFoundInDateRange);
        return response;
    }

    @Override
    public BulkInventoryLedgerSummaryResponse bulkInventoryLedgerSummary(BulkInventoryLedgerSummaryRequest request) {
        BulkInventoryLedgerSummaryResponse response = new BulkInventoryLedgerSummaryResponse();
        ValidationContext context = request.validate();
        response.setErrors(context.getErrors());
        response.setWarnings(context.getWarnings());
        if (request.getSkus() == null && (request.getPageSize() == null || request.getStartIndex() == null)) {
            context.addError(ResponseCode.MISSING_REQUIRED_PARAMETERS, "Either 'channelProductIds'  OR ('pageSize' and 'startIndex') should be present");
        }
        if (context.hasErrors()) {
            return response;
        }
        if (request.getSkus() == null) {
            List<String> skuCodes = catalogService.getAllItemTypes(request.getStartIndex(), request.getPageSize()).stream().map(ItemType::getSkuCode).collect(Collectors.toList());
            request.setSkus(skuCodes);
        }
        request.getSkus().forEach(skuCode -> {

            FetchInventoryLedgerSummaryRequest fetchInventoryLedgerSummaryRequest = new FetchInventoryLedgerSummaryRequest();
            fetchInventoryLedgerSummaryRequest.setFacilityCodes(request.getFacilityCodes());
            fetchInventoryLedgerSummaryRequest.setFrom(request.getFrom());
            fetchInventoryLedgerSummaryRequest.setTo(request.getTo());
            fetchInventoryLedgerSummaryRequest.setSkuCode(skuCode);
            FetchInventoryLedgerSummaryResponse fetchInventoryLedgerSummaryResponse = fetchInventoryLedgerSummary(fetchInventoryLedgerSummaryRequest);
            LOG.debug("fetchInventoryLedgerSummaryResponse for {}: {}", fetchInventoryLedgerSummaryRequest, fetchInventoryLedgerSummaryResponse);
            if (fetchInventoryLedgerSummaryResponse.isSuccessful()) {
                if (fetchInventoryLedgerSummaryResponse.getFacilityToLedgerEntries().size() > 0) {
                    BulkInventoryLedgerSummaryResponse.LedgerSummary ledgerSummary = new BulkInventoryLedgerSummaryResponse.LedgerSummary();
                    ledgerSummary.setSkuCode(skuCode);
                    ledgerSummary.setLedgerEntries(fetchInventoryLedgerSummaryResponse.getFacilityToLedgerEntries());
                    response.getLedgerSummary().add(ledgerSummary);
                } else {
                    response.getZeroInventorySkus().add(fetchInventoryLedgerSummaryRequest.getSkuCode());
                }

            } else if (fetchInventoryLedgerSummaryResponse.hasErrors()) {
                response.getInvalidSkus().add(skuCode);
                context.addErrors(fetchInventoryLedgerSummaryResponse.getErrors());
            } else {
                response.getInvalidSkus().add(skuCode);
                context.addError(WsResponseCode.UNKNOWN_ERROR, "Error fetching details for skuCode :[" + skuCode + "]");
            }
        });
        response.setSuccessful(!response.hasErrors());
        return response;
    }

    private Map<String, LedgerEntryGroup> prepareFacilityToLedgerEntryGroupsMap(List<InventoryLedger> inventoryLedgers, boolean wasEntryFoundInDateRange) {
        Map<String, LedgerEntryGroup> facilityToLedgerEntryGroupMap = new HashMap<>();
        for (InventoryLedger ledger : inventoryLedgers) {
            LedgerKey ledgerKey = new LedgerKey(ledger.getTransactionType().name(), ledger.getAdditionalInfo());
            String facilityCode = ledger.getFacility().getCode();
            if (!facilityToLedgerEntryGroupMap.containsKey(facilityCode)) {
                facilityToLedgerEntryGroupMap.put(facilityCode, new LedgerEntryGroup());
                facilityToLedgerEntryGroupMap.get(facilityCode).setOpeningBalance(wasEntryFoundInDateRange ? ledger.getOldBalance() : ledger.getNewBalance());
            }
            if (!facilityToLedgerEntryGroupMap.get(facilityCode).getCountMap().containsKey(ledgerKey)) {
                facilityToLedgerEntryGroupMap.get(facilityCode).getCountMap().put(ledgerKey, 0);
            }
            int count = facilityToLedgerEntryGroupMap.get(facilityCode).getCountMap().get(ledgerKey);
            facilityToLedgerEntryGroupMap.get(facilityCode).getCountMap().put(ledgerKey, count + (ledger.getNewBalance() - ledger.getOldBalance()));
        }
        return facilityToLedgerEntryGroupMap;
    }

    private void prepareResponseFromFacilityLedgerMap(Map<String, LedgerEntryGroup> facilityToLedgerEntryGroupMap, FetchInventoryLedgerSummaryResponse response,
            boolean wasEntryFoundInDateRange) {
        facilityToLedgerEntryGroupMap.forEach((facilityCode, ledgerGroupEntry) -> {
            LedgerEntryGroupDTO groupDTO = new LedgerEntryGroupDTO();
            groupDTO.setOpeningBalance(ledgerGroupEntry.getOpeningBalance());
            if (wasEntryFoundInDateRange) {
                ledgerGroupEntry.getCountMap().forEach((ledgerKey, count) -> {
                    LedgerEntryDTO ledgerEntryDTO = new LedgerEntryDTO();
                    ledgerEntryDTO.setTransactionType(ledgerKey.getTransactionType());
                    ledgerEntryDTO.setAdditionalInfo(ledgerKey.getAdditionalInfo());
                    ledgerEntryDTO.setCount(count);
                    groupDTO.getLedgerEntryDTOs().add(ledgerEntryDTO);
                });
            }
            response.getFacilityToLedgerEntries().put(facilityCode, groupDTO);
        });
    }

}
