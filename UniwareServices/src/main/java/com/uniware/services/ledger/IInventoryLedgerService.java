package com.uniware.services.ledger;

import java.util.Date;
import java.util.List;

import com.uniware.core.api.ledger.BulkInventoryLedgerSummaryRequest;
import com.uniware.core.api.ledger.BulkInventoryLedgerSummaryResponse;
import com.uniware.core.api.ledger.FetchInventoryLedgerRequest;
import com.uniware.core.api.ledger.FetchInventoryLedgerResponse;
import com.uniware.core.api.ledger.FetchInventoryLedgerSummaryRequest;
import com.uniware.core.api.ledger.FetchInventoryLedgerSummaryResponse;
import com.uniware.core.entity.InventoryLedger;
import com.uniware.core.entity.ItemType;

public interface IInventoryLedgerService {
    List<InventoryLedger> getInventoryLedgerEntries(Date from, int start, int pageSize);

    void addMonthlyOpeningBalanceForItemTypes(List<ItemType> batchItems);

    InventoryLedger addInventoryLedgerEntry(InventoryLedger inventoryLedger, int changeQuantity, InventoryLedger.ChangeType changeType);

    FetchInventoryLedgerResponse fetchInventoryLedger(FetchInventoryLedgerRequest request);

    FetchInventoryLedgerSummaryResponse fetchInventoryLedgerSummary(FetchInventoryLedgerSummaryRequest request);

    BulkInventoryLedgerSummaryResponse bulkInventoryLedgerSummary(BulkInventoryLedgerSummaryRequest request);
}
