package com.uniware.services.dashboard;

import com.unifier.core.api.reports.GetWidgetDataRequest;
import com.unifier.core.entity.DashboardWidget;

import java.util.Date;

/**
 * Created by Samdeesh on 7/15/15.
 */
public interface IDashboardWidgetService {

    String getDashboardWidgetData(GetWidgetDataRequest request, DashboardWidget dashboardWidget);

    void insertDashboardWidgetData(String cacheWidgetIdentifier, String data, Date expireAt);

    String getWidgetData(String identifier);
}
