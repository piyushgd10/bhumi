package com.uniware.services.dashboard;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unifier.core.annotation.Level;
import com.unifier.core.api.reports.GetWidgetDataRequest;
import com.unifier.core.api.reports.GetWidgetDataResponse;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.entity.DashboardWidget;
import com.unifier.core.utils.StringUtils;
import com.unifier.services.report.IReportService;
import com.uniware.core.locking.Namespace;
import com.uniware.core.locking.annotation.Lock;
import com.uniware.core.locking.annotation.Locks;
import com.uniware.core.vo.DashboardWidgetVO;
import com.uniware.dao.dashboard.IDashboardMao;
import com.uniware.services.cache.DashboardWidgetCache;
import com.uniware.services.cache.DashboardWidgetCache.DashboardCacheKey;

/**
 * Created by Samdeesh on 7/15/15.
 */

@Service("dashboardWidgetService")
public class DashboardWidgetServiceImpl implements IDashboardWidgetService {

    @Autowired
    private IReportService reportService;

    @Autowired
    private IDashboardMao  dashboardMao;

    @Override
    public String getDashboardWidgetData(GetWidgetDataRequest request, DashboardWidget dashboardWidget) {
        DashboardCacheKey key = new DashboardCacheKey(request.getWidgetCode(), request.getTimeRange(), request.getAdditionalCacheParam());
        return doGet(key, request, dashboardWidget);
    }

    @Locks({ @Lock(ns = Namespace.DASHBOARD_WIDGET, key = "#{#args[0].cacheKey}", level = Level.TENANT) })
    private String doGet(DashboardCacheKey key, GetWidgetDataRequest request, DashboardWidget dashboardWidget) {
        DashboardWidgetCache cache = CacheManager.getInstance().getCache(DashboardWidgetCache.class);
        String jsonData = cache.getCachedDashboardWidgetData(key);
        if (jsonData == null) {
            GetWidgetDataResponse response = reportService.getWidgetData(request);
            if (response.isSuccessful()) {
                jsonData = response.getData();
                if (StringUtils.isNotBlank(dashboardWidget.getSecondsToCache())) {
                    cache.cacheDashboardWidgetData(key, jsonData);
                }
            }
        }
        return jsonData;
    }

    @Override
    public void insertDashboardWidgetData(String cacheWidgetIdentifier, String data, Date expireAt) {
        if (dashboardMao.getWidgetData(cacheWidgetIdentifier) == null) {
            DashboardWidgetVO widgetVO = new DashboardWidgetVO();
            widgetVO.setCacheWidgetIdentifier(cacheWidgetIdentifier);
            widgetVO.setJsonData(data);
            widgetVO.setExpireAt(expireAt);
            dashboardMao.insertDashboardWidgetData(widgetVO);
        }
    }

    @Override
    public String getWidgetData(String identifier) {
        DashboardWidgetVO widgetVO = dashboardMao.getWidgetData(identifier);
        return widgetVO != null ? widgetVO.getJsonData() : null;
    }
}
