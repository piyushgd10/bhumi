/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 07/05/14
 *  @author amit
 */

package com.uniware.services.templateCustomization;

import java.util.Map;

import com.unifier.core.api.templatecustomization.CustomizeTemplateRequest;
import com.unifier.core.api.templatecustomization.CustomizeTemplateResponse;
import com.unifier.core.api.templatecustomization.GetTemplateCustomizationConfigRequest;
import com.unifier.core.api.templatecustomization.GetTemplateCustomizationConfigResponse;
import com.unifier.core.api.templatecustomization.MarkTemplateCustomizedRequest;
import com.unifier.core.api.templatecustomization.MarkTemplateCustomizedResponse;
import com.unifier.core.api.templatecustomization.SaveCustomizedTemplateRequest;
import com.unifier.core.api.templatecustomization.SaveCustomizedTemplateResponse;
import com.unifier.core.api.validation.ValidationContext;
import com.uniware.core.api.templatecustomization.GetCustomizedDummyTemplateRequest;
import com.uniware.core.api.templatecustomization.GetCustomizedDummyTemplateResponse;

public interface ITemplateCustomizationService {

    /**
     * Evaluates given {@link com.uniware.core.vo.PrintTemplateVO.Type} on given customization parameters and dummy
     * order. Response will give a preview of given template type on a dummy order. This will be called on base tenant
     * <b>ONLY</b> by all others.
     * 
     * @param request
     * @return
     */
    GetCustomizedDummyTemplateResponse getCustomizedDummyTemplate(GetCustomizedDummyTemplateRequest request);

    /**
     * Returns a HTML after evaluating template on dummy order and given customization parameters. Calls base tenant's
     * {@code ITemplateCustomizationService#getCustomizedDummyTemplate}.
     * 
     * @param templateType
     * @param customizationParameterMap
     * @param samplePrintTemplateCode
     * @return
     */
    String getCustomizedTemplateHtml(String templateType, Map<String, String> customizationParameterMap, String samplePrintTemplateCode, ValidationContext Context);

    /**
     * Returns the initial config.
     * 
     * @param request
     * @return
     */
    GetTemplateCustomizationConfigResponse getTemplateCustomizationConfig(GetTemplateCustomizationConfigRequest request);

    SaveCustomizedTemplateResponse saveCustomizedTemplate(SaveCustomizedTemplateRequest request);

    /**
     * Returns a URL of preview document after evaluating template on dummy order and given customization parameters.
     * Calls base tenant's {@code ITemplateCustomizationService#getCustomizedDummyTemplate}.
     * 
     * @param request
     * @return
     */
    CustomizeTemplateResponse customizeTemplate(CustomizeTemplateRequest request);

    MarkTemplateCustomizedResponse markTemplateCustomized(MarkTemplateCustomizedRequest request);
}
