/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 07/05/14
 *  @author amit
 */

package com.uniware.services.templateCustomization.impl;

import com.unifier.core.api.templatecustomization.CustomizationParameterDTO;
import com.unifier.core.api.templatecustomization.CustomizationParameterGroupDTO;
import com.unifier.core.api.templatecustomization.CustomizeTemplateRequest;
import com.unifier.core.api.templatecustomization.CustomizeTemplateResponse;
import com.unifier.core.api.templatecustomization.GetTemplateCustomizationConfigRequest;
import com.unifier.core.api.templatecustomization.GetTemplateCustomizationConfigResponse;
import com.unifier.core.api.templatecustomization.MarkTemplateCustomizedRequest;
import com.unifier.core.api.templatecustomization.MarkTemplateCustomizedResponse;
import com.unifier.core.api.templatecustomization.SaveCustomizedTemplateRequest;
import com.unifier.core.api.templatecustomization.SaveCustomizedTemplateResponse;
import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.exception.TemplateCompilationException;
import com.unifier.core.template.Template;
import com.unifier.core.utils.EncryptionUtils;
import com.unifier.core.utils.StringUtils;
import com.unifier.core.utils.XMLParser;
import com.unifier.dao.printing.IPrintConfigMao;
import com.unifier.scraper.sl.runtime.ScraperScript;
import com.unifier.scraper.sl.runtime.ScriptExecutionContext;
import com.unifier.services.aspect.MarkDirty;
import com.unifier.services.pdf.IPdfDocumentService;
import com.unifier.services.pdf.impl.PdfDocumentServiceImpl;
import com.unifier.services.tenantprofile.service.ITenantProfileService;
import com.unifier.services.vo.TenantProfileVO;
import com.uniware.core.api.templatecustomization.GetCustomizedDummyTemplateRequest;
import com.uniware.core.api.templatecustomization.GetCustomizedDummyTemplateResponse;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.cache.EnvironmentPropertiesCache;
import com.uniware.core.cache.FacilityCache;
import com.uniware.core.entity.SaleOrder;
import com.uniware.core.utils.UserContext;
import com.uniware.core.vo.PrintTemplateVO;
import com.uniware.core.vo.SamplePrintTemplateVO;
import com.uniware.core.vo.TemplateCustomizationFieldsVO;
import com.uniware.core.vo.UniwareScriptVO;
import com.uniware.dao.templatecustomization.ITemplateCustomizationMao;
import com.uniware.services.cache.PrintTemplateCache;
import com.uniware.services.cache.SamplePrintTemplateCache;
import com.uniware.services.cache.ScriptVersionedCache;
import com.uniware.services.configuration.data.manager.ProductConfiguration;
import com.uniware.services.saleorder.ISaleOrderService;
import com.uniware.services.templateCustomization.ITemplateCustomizationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

@Service("templateCustomizationService")
public class TemplateCustomizationServiceImpl implements ITemplateCustomizationService {

    private static final String       TEMPLATE_HTML_TAG_NAME = "TemplateHtml";

    @Autowired
    private ISaleOrderService         saleOrderService;

    @Autowired
    private ITemplateCustomizationMao templateCustomizationMao;

    @Autowired
    private IPrintConfigMao           printConfigMao;

    @Autowired
    private ITenantProfileService     tenantProfileService;

    @Autowired
    private IPdfDocumentService       pdfDocumentService;
    
    private static final Logger  LOG        = LoggerFactory.getLogger(TemplateCustomizationServiceImpl.class);

    /**
     * Evaluates given {@link com.uniware.core.vo.PrintTemplateVO.Type} on given customization parameters and dummy
     * order. Response will give a preview of given template type on a dummy order. This will be called on base tenant
     * (initially 3) <b>ONLY</b> by all others.
     * 
     * @param request
     * @return
     */
    @Override
    @Transactional(readOnly = true)
    public GetCustomizedDummyTemplateResponse getCustomizedDummyTemplate(GetCustomizedDummyTemplateRequest request) {
        GetCustomizedDummyTemplateResponse response = new GetCustomizedDummyTemplateResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            // There is a dummy order with code "ORDER_CODE" on baseStandard3.
            SaleOrder order = saleOrderService.getSaleOrderByCode("ORDER_CODE");
            if (order == null) {
                context.addError(WsResponseCode.INVALID_API_REQUEST, "No dummy order found on this tenant");
            } else {
                Template template = null;
                try {
                    template = Template.compile("invoice", EncryptionUtils.hexDecodeAsString(request.getTemplateString()));
                } catch (TemplateCompilationException e) {
                    context.addError(WsResponseCode.INVALID_SAMPLE_TEMPLATE, "Invalid template");
                }
                if (template == null) {
                    context.addError(WsResponseCode.INVALID_API_REQUEST, "No template found for given request");
                }
                if (!context.hasErrors()) {
                    Map<String, Object> evaluationParameterMap = new HashMap<>();
                    for (GetCustomizedDummyTemplateRequest.CustomizationParameter parameter : request.getParameterList()) {
                        evaluationParameterMap.put(parameter.getKey(), parameter.getValue());
                    }
                    evaluationParameterMap.put("invoice", order.getSaleOrderItems().iterator().next().getShippingPackage().getInvoice());
                    evaluationParameterMap.put("saleOrder", order);
                    evaluationParameterMap.put("shippingPackage", order.getSaleOrderItems().iterator().next().getShippingPackage());
                    response.setEvaluatedTemplate(template.evaluate(evaluationParameterMap));
                }
            }
        }
        response.addErrors(context.getErrors());
        response.setSuccessful(!context.hasErrors());
        return response;
    }

    /**
     * * Returns a HTML after evaluating template on dummy order and given customization parameters. Calls base tenant's
     * if samplePrintTemplateCode is passed null, then it takes the selected template as sapmlePrintTemplateCode.
     * {@code ITemplateCustomizationService#getCustomizedDummyTemplate}.
     * 
     * @param templateType
     * @param customizationParameterMap
     * @param samplePrintTemplateCode
     * @return
     */
    @Override
    public String getCustomizedTemplateHtml(String templateType, Map<String, String> customizationParameterMap, String samplePrintTemplateCode, ValidationContext context) {
        String templateHtml = null;
        ScraperScript script = CacheManager.getInstance().getCache(ScriptVersionedCache.class).getScriptByName(UniwareScriptVO.Name.EVALUATE_TEMPLATE_ON_DUMMY_ORDER_SCRIPT.name());
        if (script != null) {
            TenantProfileVO tenantProfileVO = tenantProfileService.getTenantProfileByCode(CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class).getTenantWithDummyOrder());
            ScriptExecutionContext seContext = ScriptExecutionContext.current();
            SamplePrintTemplateVO sptVO = null;
            if (samplePrintTemplateCode != null) {
                if (CacheManager.getInstance().getCache(SamplePrintTemplateCache.class).getSamplePrintTemplate(samplePrintTemplateCode) != null) {
                    sptVO = CacheManager.getInstance().getCache(SamplePrintTemplateCache.class).getSamplePrintTemplate(samplePrintTemplateCode);
                } else {
                    context.addError(WsResponseCode.INVALID_SAMPLE_TEMPLATE, "No template found for sample print template code : " + samplePrintTemplateCode);
                }
            } else {
                PrintTemplateVO ptVO = CacheManager.getInstance().getCache(PrintTemplateCache.class).getPrintTemplateByName(templateType);
                sptVO = CacheManager.getInstance().getCache(SamplePrintTemplateCache.class).getSamplePrintTemplate(ptVO.getSamplePrintTemplateCode());
            }
            if (!context.hasErrors()) {
                seContext.addVariable("templateString", EncryptionUtils.hexEncode(sptVO.getTemplate()));
                seContext.addVariable("customizationParameterMap", customizationParameterMap);
                seContext.addVariable("username", tenantProfileVO.getApiUsername());
                seContext.addVariable("password", tenantProfileVO.getApiPassword());
                seContext.addVariable("baseTenantUrl", "https://" + tenantProfileVO.getAccessUrl());
                String evaluatedTemplateXml = null;
                try {
                    seContext.setTraceLoggingEnabled(UserContext.current().isTraceLoggingEnabled());
                    script.execute();
                    evaluatedTemplateXml = seContext.getScriptOutput();
                } finally {
                    ScriptExecutionContext.destroy();
                }
                if (StringUtils.isNotBlank(evaluatedTemplateXml)) {
                    templateHtml = XMLParser.parse(evaluatedTemplateXml).text(TEMPLATE_HTML_TAG_NAME);
                }
            }
        }
        return templateHtml;
    }

    /**
     * Returns a URL of preview document after evaluating template on dummy order and given customization parameters.
     * Calls base tenant's {@code ITemplateCustomizationService#getCustomizedDummyTemplate}.
     * 
     * @param request
     * @return
     */
    @Override
    public CustomizeTemplateResponse customizeTemplate(CustomizeTemplateRequest request) {
        CustomizeTemplateResponse response = new CustomizeTemplateResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            PrintTemplateVO ptVO = CacheManager.getInstance().getCache(PrintTemplateCache.class).getPrintTemplateByType(request.getTemplateType());
            if (ptVO == null) {
                context.addError(WsResponseCode.INVALID_API_REQUEST, "No template found for type: " + request.getTemplateType());
            } else {
                String fileDir = CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class).getExportDirectoryPath() + "/customizedTemplates/";
                String fileName = EncryptionUtils.md5Encode(ptVO.getType() + CacheManager.getInstance().getCache(FacilityCache.class).getCurrentFacilityCode()
                        + UserContext.current().getTenant().getCode())
                        + ".pdf";
                String templateHtml = null;
                if (request.getCustomizationParameterMap() != null) {
                    templateHtml = getCustomizedTemplateHtml(ptVO.getType(), request.getCustomizationParameterMap(), request.getSamplePrintTemplateCode(), context);
                } else {
                    templateHtml = getCustomizedTemplateHtml(ptVO.getType(), ptVO.getCustomizationParameters(), request.getSamplePrintTemplateCode(), context);
                }
                try {
                    File evaluatedTemplate = new File(fileDir + fileName);
                    evaluatedTemplate.getParentFile().mkdirs();
                    SamplePrintTemplateVO sptVO = CacheManager.getInstance().getCache(SamplePrintTemplateCache.class).getSamplePrintTemplate(request.getSamplePrintTemplateCode());
                    PdfDocumentServiceImpl.PrintOptions po = new PdfDocumentServiceImpl.PrintOptions(sptVO);
                    po.setPrintDialog(PdfDocumentServiceImpl.PrintOptions.PrintDialog.DEFAULT.name());
                    pdfDocumentService.writeHtmlToPdf(new FileOutputStream(evaluatedTemplate), templateHtml, po);
                } catch (IOException e) {
                	LOG.error("File not found : ", e);
                    context.addError(WsResponseCode.UNKNOWN_ERROR, "File not found");
                }
                if (!context.hasErrors()) {
                    String url = new StringBuilder("https://").append(UserContext.current().getTenant().getAccessUrl()).append("/files/customizedTemplates/").append(fileName).append(
                            "?_=").append(System.currentTimeMillis()).toString();
                    response.setCustomizedTemplateUrl(url);
                }
            }
        }
        response.addErrors(context.getErrors());
        response.setSuccessful(!context.hasErrors());
        return response;
    }

    /**
     * Returns the initial config with selected values.
     * 
     * @param request
     * @return
     */
    @Override
    public GetTemplateCustomizationConfigResponse getTemplateCustomizationConfig(GetTemplateCustomizationConfigRequest request) {
        GetTemplateCustomizationConfigResponse response = new GetTemplateCustomizationConfigResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            PrintTemplateVO ptVO = CacheManager.getInstance().getCache(PrintTemplateCache.class).getPrintTemplateByType(request.getTemplateType());
            if (ptVO == null) {
                context.addError(WsResponseCode.INVALID_API_REQUEST, "No template found for requested type");
            } else {
                TemplateCustomizationFieldsVO customizationFieldsVO = templateCustomizationMao.getTemplateCustomizationFields(request.getTemplateType());
                if (customizationFieldsVO == null) {
                    context.addError(WsResponseCode.INVALID_API_REQUEST, "No customization fields found for requested template");
                } else {
                    ProductConfiguration configuration = ConfigurationManager.getInstance().getConfiguration(ProductConfiguration.class);
                    for (TemplateCustomizationFieldsVO.CustomizationFieldGroup fieldGroup : customizationFieldsVO.getCustomizationFieldGroupList()) {
                        CustomizationParameterGroupDTO parameterGroupDTO = new CustomizationParameterGroupDTO();
                        parameterGroupDTO.setCode(fieldGroup.getCode());
                        parameterGroupDTO.setDisplayName(fieldGroup.getDisplayName());
                        parameterGroupDTO.setPosition(fieldGroup.getPosition());
                        for (TemplateCustomizationFieldsVO.CustomizationField field : fieldGroup.getFieldList()) {
                            if (field.isEnabled()) {
                                boolean applicable = true;
                                if (field.getApplicableForProductFeatures() != null && field.getApplicableForProductFeatures().size() > 0) {
                                    for (String featureCode : field.getApplicableForProductFeatures()) {
                                        if (!configuration.hasFeature(featureCode)) {
                                            applicable = false;
                                            break;
                                        }
                                    }
                                }
                                if (applicable) {
                                    CustomizationParameterDTO parameterDTO = new CustomizationParameterDTO();
                                    parameterDTO.setName(field.getName());
                                    parameterDTO.setCode(field.getCode());
                                    parameterDTO.setRequired(field.isRequired());
                                    parameterDTO.setType(field.isType().name());
                                    parameterDTO.setPossibleValues(field.getPossibleValues());
                                    parameterDTO.setParentFieldCode(field.getParentFieldCode());
                                    parameterDTO.setTooltipKey(field.getTooltipKey());
                                    parameterDTO.setSelectedValue(ptVO.getCustomizationParameters().get(field.getCode()));
                                    parameterDTO.setUiConfigMetadata(field.getUiConfigMetadata());
                                    parameterGroupDTO.getParameterList().add(parameterDTO);
                                }
                            }
                        }
                        response.getParameterGroupDTOList().add(parameterGroupDTO);
                    }
                }
            }
        }
        response.addErrors(context.getErrors());
        response.setSuccessful(!context.hasErrors());
        return response;
    }

    /**
     * Modify customization parameters for a template.
     * 
     * @param request
     * @return
     */
    @Override
    @MarkDirty(values = { SamplePrintTemplateCache.class, PrintTemplateCache.class })
    public SaveCustomizedTemplateResponse saveCustomizedTemplate(SaveCustomizedTemplateRequest request) {
        SaveCustomizedTemplateResponse response = new SaveCustomizedTemplateResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            PrintTemplateVO printTemplate = printConfigMao.getPrintTemplateByType(request.getTemplateType().name());
            if (printTemplate == null) {
                context.addError(WsResponseCode.INVALID_API_REQUEST, "No print template found for give type");
            } else {
                if (request.getSamplePrintTemplateCode() != null) {
                    printTemplate.setSamplePrintTemplateCode(request.getSamplePrintTemplateCode());
                }
                TemplateCustomizationFieldsVO fieldsVO = templateCustomizationMao.getTemplateCustomizationFields(request.getTemplateType());
                for (SaveCustomizedTemplateRequest.ModifyParameterDTO requestParam : request.getParameterDTOList()) {
                    if (!isCustomizationParameterValid(fieldsVO, requestParam.getGroupCode(), requestParam.getParameterCode(), requestParam.getParameterValue())) {
                        context.addError(WsResponseCode.INVALID_API_REQUEST, "Invalid parameter found with code: " + requestParam.getParameterCode());
                    } else {
                        if (StringUtils.isBlank(requestParam.getParameterValue())) {
                            context.addError(WsResponseCode.INVALID_API_REQUEST, "Invalid value of parameter: " + requestParam.getParameterCode());
                        } else {
                            printTemplate.getCustomizationParameters().put(requestParam.getParameterCode(), requestParam.getParameterValue());
                            printTemplate.setUserCustomized(true);
                            printConfigMao.updatePrintTemplate(printTemplate);
                        }
                    }
                }
            }
        }
        response.addErrors(context.getErrors());
        response.setSuccessful(!context.hasErrors());
        return response;
    }
    
    @Override
    @MarkDirty(values = {PrintTemplateCache.class })
    public MarkTemplateCustomizedResponse markTemplateCustomized(MarkTemplateCustomizedRequest request) {
        MarkTemplateCustomizedResponse response = new MarkTemplateCustomizedResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            PrintTemplateVO printTemplate = printConfigMao.getPrintTemplateByType(request.getTemplateType().name());
            if (printTemplate == null) {
                context.addError(WsResponseCode.INVALID_API_REQUEST, "No print template found for give type");
            } else {
                printTemplate.setUserCustomized(true);
                printConfigMao.updatePrintTemplate(printTemplate);
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        response.setSuccessful(!context.hasErrors());
        return response;
    }

    private boolean isCustomizationParameterValid(TemplateCustomizationFieldsVO fieldsVO, String groupCode, String paramCode, String value) {
        boolean isParameterValid = false;
        Iterator<TemplateCustomizationFieldsVO.CustomizationFieldGroup> groupIterator = fieldsVO.getCustomizationFieldGroupList().iterator();
        while (groupIterator.hasNext() && !isParameterValid) {
            TemplateCustomizationFieldsVO.CustomizationFieldGroup group = groupIterator.next();
            if (group.getCode().equals(groupCode)) {
                Iterator<TemplateCustomizationFieldsVO.CustomizationField> fieldIterator = group.getFieldList().iterator();
                while (fieldIterator.hasNext() && !isParameterValid) {
                    TemplateCustomizationFieldsVO.CustomizationField field = fieldIterator.next();
                    if ((field.isEnabled() && field.getCode().equals(paramCode)) && (field.getPossibleValues() == null || field.getPossibleValues().contains(value))) {
                        isParameterValid = true;
                    }
                }
            }
        }
        return isParameterValid;
    }
}
