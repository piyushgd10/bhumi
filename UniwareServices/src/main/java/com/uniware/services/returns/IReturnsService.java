/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 9, 2012
 *  @author singla
 */
package com.uniware.services.returns;

import java.util.List;
import java.util.Set;

import com.unifier.core.template.Template;
import com.uniware.core.api.returns.AddShippingPackageToReturnManifestRequest;
import com.uniware.core.api.returns.AddShippingPackageToReturnManifestResponse;
import com.uniware.core.api.returns.AssignShippingProviderToReversePickupRequest;
import com.uniware.core.api.returns.AssignShippingProviderToReversePickupResponse;
import com.uniware.core.api.returns.CancelReversePickupRequest;
import com.uniware.core.api.returns.CancelReversePickupResponse;
import com.uniware.core.api.returns.CloseReturnManifestRequest;
import com.uniware.core.api.returns.CloseReturnManifestResponse;
import com.uniware.core.api.returns.CompleteReversePickupRequest;
import com.uniware.core.api.returns.CompleteReversePickupResponse;
import com.uniware.core.api.returns.ConfirmShippingPackageReturnToOriginRequest;
import com.uniware.core.api.returns.ConfirmShippingPackageReturnToOriginResponse;
import com.uniware.core.api.returns.CreateReshipmentRequest;
import com.uniware.core.api.returns.CreateReshipmentResponse;
import com.uniware.core.api.returns.CreateReturnManifestRequest;
import com.uniware.core.api.returns.CreateReturnManifestResponse;
import com.uniware.core.api.returns.CreateReversePickupRequest;
import com.uniware.core.api.returns.CreateReversePickupResponse;
import com.uniware.core.api.returns.DiscardReturnManifestRequest;
import com.uniware.core.api.returns.DiscardReturnManifestResponse;
import com.uniware.core.api.returns.EditReturnManifestItemRequest;
import com.uniware.core.api.returns.EditReturnManifestItemResponse;
import com.uniware.core.api.returns.GetEligibleShippingProvidersForReturnManifestRequest;
import com.uniware.core.api.returns.GetEligibleShippingProvidersForReturnManifestResponse;
import com.uniware.core.api.returns.GetReturnManifestItemDetailsRequest;
import com.uniware.core.api.returns.GetReturnManifestItemDetailsResponse;
import com.uniware.core.api.returns.GetReturnManifestShipmentsSummaryRequest;
import com.uniware.core.api.returns.GetReturnManifestShipmentsSummaryResponse;
import com.uniware.core.api.returns.GetReturnManifestSummaryRequest;
import com.uniware.core.api.returns.GetReturnManifestSummaryResponse;
import com.uniware.core.api.returns.GetReversePickupInvoiceRequest;
import com.uniware.core.api.returns.GetReversePickupInvoiceResponse;
import com.uniware.core.api.returns.MarkSaleOrderReturnedRequest;
import com.uniware.core.api.returns.MarkSaleOrderReturnedResponse;
import com.uniware.core.api.returns.RedispatchReversePickupRequest;
import com.uniware.core.api.returns.RedispatchReversePickupResponse;
import com.uniware.core.api.returns.RemoveReturnManifestItemRequest;
import com.uniware.core.api.returns.RemoveReturnManifestItemResponse;
import com.uniware.core.api.returns.ReshipShippingPackageRequest;
import com.uniware.core.api.returns.ReshipShippingPackageResponse;
import com.uniware.core.api.returns.ReshipmentDTO;
import com.uniware.core.api.returns.ReversePickupDTO;
import com.uniware.core.api.returns.ScanReturnShipmentRequest;
import com.uniware.core.api.returns.ScanReturnShipmentResponse;
import com.uniware.core.api.returns.SearchAwaitingShippingPackageRequest;
import com.uniware.core.api.returns.SearchAwaitingShippingPackageResponse;
import com.uniware.core.api.reversepickup.EditReversePickupMetadataRequest;
import com.uniware.core.api.reversepickup.EditReversePickupMetadataResponse;
import com.uniware.core.api.warehouse.GetReversePickupRequest;
import com.uniware.core.api.warehouse.GetReversePickupResponse;
import com.uniware.core.entity.Reshipment;
import com.uniware.core.entity.ReshipmentAction;
import com.uniware.core.entity.ReturnManifest;
import com.uniware.core.entity.ReturnManifestStatus;
import com.uniware.core.entity.ReversePickup;
import com.uniware.core.entity.ReversePickupAction;
import com.uniware.core.entity.SaleOrder;
import com.uniware.core.entity.SaleOrderItem;

/**
 * @author singla
 */
public interface IReturnsService {

    /**
     * @param request
     * @return
     */
    CreateReversePickupResponse createReversePickup(CreateReversePickupRequest request);

    /**
     * @param request
     * @return
     */
    AssignShippingProviderToReversePickupResponse assignShippingProviderToReversePickup(AssignShippingProviderToReversePickupRequest request);

    /**
     * @param request
     * @return
     */
    CreateReshipmentResponse createReshipment(CreateReshipmentRequest request);

    List<ReshipmentDTO> getPendingReshipments();

    List<ReversePickupDTO> getUnassignedPendingReversePickups();

    GetReversePickupResponse getReversePickup(GetReversePickupRequest request);

    /**
     * @param request
     * @return
     */
    ScanReturnShipmentResponse getShipment(ScanReturnShipmentRequest request);

    /**
     * @param request
     * @return
     */
    CreateReturnManifestResponse createReturnManifest(CreateReturnManifestRequest request);

    /**
     * @param request
     * @return
     */
    AddShippingPackageToReturnManifestResponse addShippingPackageToReturnManifest(AddShippingPackageToReturnManifestRequest request);

    ConfirmShippingPackageReturnToOriginResponse confirmShippingPackageReturn(ConfirmShippingPackageReturnToOriginRequest request);

    /**
     * @param request
     * @return
     */
    CloseReturnManifestResponse closeReturnManifest(CloseReturnManifestRequest request);

    /**
     * @param request
     * @return
     */
    DiscardReturnManifestResponse discardReturnManifest(DiscardReturnManifestRequest request);

    /**
     * @param reversePickupCode
     * @return
     */
    ReversePickup getReversePickupByCode(String reversePickupCode);

    /**
     * @param reversePickup
     * @return
     */
    ReversePickup updateReversePickup(ReversePickup reversePickup);

    Reshipment getReshipmentById(Integer reshipmentId);

    void markSaleOrderItemsReshipped(Set<SaleOrderItem> saleOrderItems);

    SaleOrder createReshipmentOrder(Reshipment reshipment);

    SearchAwaitingShippingPackageResponse searchAwaitingShippingPackages(SearchAwaitingShippingPackageRequest request);

    SaleOrder createReplacement(ReversePickup reversePickup, Set<SaleOrderItem> saleOrderItemsToReplace);

    void completeReshipment(int reshipmentId);

    CancelReversePickupResponse cancelReversePickup(CancelReversePickupRequest request);

    EditReturnManifestItemResponse editReturnManifestItem(EditReturnManifestItemRequest request);

    ReturnManifest getReturnManifestById(int parseInt);

    List<ReturnManifestStatus> getReturnManifestStatuses();

    List<ReshipmentAction> getReshipmentActions();

    List<ReversePickupAction> getReversePickupActions();

    ReturnManifest getReturnManifestByCode(String returnManifestCode);

    RedispatchReversePickupResponse redispatchReversePickup(RedispatchReversePickupRequest request);

    String getRedispatchHtml(String reversePickupCode, Template template);

    ReversePickup getReversePickupById(int reversePickupId);

    ReshipShippingPackageResponse reshipShippingPackage(ReshipShippingPackageRequest request);

    EditReversePickupMetadataResponse editReversePickupMetadata(EditReversePickupMetadataRequest request);

    MarkSaleOrderReturnedResponse markSaleOrderReturned(MarkSaleOrderReturnedRequest request);

    ReversePickupDTO prepareReversePickupDTO(ReversePickup reversePickup);

    CompleteReversePickupResponse completeReversePickup(CompleteReversePickupRequest request);

    GetReturnManifestSummaryResponse getReturnManifestSummary(GetReturnManifestSummaryRequest request);

    GetReturnManifestItemDetailsResponse getReturnManifestItemDetails(GetReturnManifestItemDetailsRequest request);

    RemoveReturnManifestItemResponse removeReturnManifestItem(RemoveReturnManifestItemRequest request);

    GetReturnManifestShipmentsSummaryResponse getReturnManifestShipmentsSummary(GetReturnManifestShipmentsSummaryRequest request);

    GetEligibleShippingProvidersForReturnManifestResponse getShippingProvidersForReturnManifest(GetEligibleShippingProvidersForReturnManifestRequest request);

    GetReversePickupInvoiceResponse getReversePickupInvoicesDetails(GetReversePickupInvoiceRequest request);

    ReversePickup getReversePickupByReturnInvoiceId(Integer invoiceId);
}