/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 9, 2012
 *  @author singla
 */
package com.uniware.services.returns.impl;

import com.unifier.core.annotation.Level;
import com.unifier.core.annotation.audit.LogActivity;
import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.email.EmailMessage;
import com.unifier.core.entity.User;
import com.unifier.core.template.Template;
import com.unifier.core.utils.CollectionUtils;
import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.FileUtils;
import com.unifier.core.utils.NumberUtils;
import com.unifier.core.utils.StringUtils;
import com.unifier.scraper.sl.runtime.ScraperScript;
import com.unifier.services.aspect.RollbackOnFailure;
import com.unifier.services.email.IEmailService;
import com.unifier.services.users.IUsersService;
import com.unifier.services.utils.CustomFieldUtils;
import com.uniware.core.api.admin.shipping.ShippingProviderSearchDTO;
import com.uniware.core.api.currency.GetCurrencyConversionRateRequest;
import com.uniware.core.api.invoice.CreateShippingPackageInvoiceRequest;
import com.uniware.core.api.invoice.CreateShippingPackageInvoiceResponse;
import com.uniware.core.api.invoice.CreateShippingPackageReverseInvoiceRequest;
import com.uniware.core.api.invoice.CreateShippingPackageReverseInvoiceResponse;
import com.uniware.core.api.invoice.InvoiceDTO;
import com.uniware.core.api.packer.SaleOrderItemDTO;
import com.uniware.core.api.putaway.CompletePutawayRequest;
import com.uniware.core.api.putaway.CompletePutawayResponse;
import com.uniware.core.api.putaway.CreatePutawayListRequest;
import com.uniware.core.api.putaway.CreatePutawayListResponse;
import com.uniware.core.api.putaway.CreatePutawayRequest;
import com.uniware.core.api.putaway.CreatePutawayResponse;
import com.uniware.core.api.returns.AddReturnsSaleOrderItemsToPutawayRequest;
import com.uniware.core.api.returns.AddReturnsSaleOrderItemsToPutawayResponse;
import com.uniware.core.api.returns.AddShippingPackageToReturnManifestRequest;
import com.uniware.core.api.returns.AddShippingPackageToReturnManifestResponse;
import com.uniware.core.api.returns.AssignShippingProviderToReversePickupRequest;
import com.uniware.core.api.returns.AssignShippingProviderToReversePickupResponse;
import com.uniware.core.api.returns.CancelReversePickupRequest;
import com.uniware.core.api.returns.CancelReversePickupResponse;
import com.uniware.core.api.returns.CloseReturnManifestRequest;
import com.uniware.core.api.returns.CloseReturnManifestResponse;
import com.uniware.core.api.returns.CompleteReversePickupRequest;
import com.uniware.core.api.returns.CompleteReversePickupResponse;
import com.uniware.core.api.returns.ConfirmShippingPackageReturnToOriginRequest;
import com.uniware.core.api.returns.ConfirmShippingPackageReturnToOriginResponse;
import com.uniware.core.api.returns.CreateReshipmentRequest;
import com.uniware.core.api.returns.CreateReshipmentResponse;
import com.uniware.core.api.returns.CreateReturnManifestRequest;
import com.uniware.core.api.returns.CreateReturnManifestResponse;
import com.uniware.core.api.returns.CreateReversePickupRequest;
import com.uniware.core.api.returns.CreateReversePickupResponse;
import com.uniware.core.api.returns.DiscardReturnManifestRequest;
import com.uniware.core.api.returns.DiscardReturnManifestResponse;
import com.uniware.core.api.returns.EditReturnManifestItemRequest;
import com.uniware.core.api.returns.EditReturnManifestItemResponse;
import com.uniware.core.api.returns.GetEligibleShippingProvidersForReturnManifestRequest;
import com.uniware.core.api.returns.GetEligibleShippingProvidersForReturnManifestResponse;
import com.uniware.core.api.returns.GetReturnManifestItemDetailsRequest;
import com.uniware.core.api.returns.GetReturnManifestItemDetailsResponse;
import com.uniware.core.api.returns.GetReturnManifestShipmentsSummaryRequest;
import com.uniware.core.api.returns.GetReturnManifestShipmentsSummaryResponse;
import com.uniware.core.api.returns.GetReturnManifestSummaryRequest;
import com.uniware.core.api.returns.GetReturnManifestSummaryResponse;
import com.uniware.core.api.returns.GetReturnManifestSummaryResponse.ReturnManifestSummaryDTO;
import com.uniware.core.api.returns.GetReversePickupInvoiceRequest;
import com.uniware.core.api.returns.GetReversePickupInvoiceResponse;
import com.uniware.core.api.returns.MarkSaleOrderReturnedRequest;
import com.uniware.core.api.returns.MarkSaleOrderReturnedResponse;
import com.uniware.core.api.returns.PackageAwaitingActionDTO;
import com.uniware.core.api.returns.RedispatchReversePickupRequest;
import com.uniware.core.api.returns.RedispatchReversePickupResponse;
import com.uniware.core.api.returns.RemoveReturnManifestItemRequest;
import com.uniware.core.api.returns.RemoveReturnManifestItemResponse;
import com.uniware.core.api.returns.ReshipShippingPackageRequest;
import com.uniware.core.api.returns.ReshipShippingPackageResponse;
import com.uniware.core.api.returns.ReshipmentDTO;
import com.uniware.core.api.returns.ReturnManifestItemDetailDTO;
import com.uniware.core.api.returns.ReturnManifestItemDetailDTO.ReturnManifestLineItem;
import com.uniware.core.api.returns.ReversePickupDTO;
import com.uniware.core.api.returns.ReversePickupDTO.ReplacedSaleOrderItemDTO;
import com.uniware.core.api.returns.ScanReturnShipmentRequest;
import com.uniware.core.api.returns.ScanReturnShipmentResponse;
import com.uniware.core.api.returns.SearchAwaitingShippingPackageRequest;
import com.uniware.core.api.returns.SearchAwaitingShippingPackageResponse;
import com.uniware.core.api.returns.WsReversePickupAlternate;
import com.uniware.core.api.returns.WsReversePickupItem;
import com.uniware.core.api.reversepickup.AddReversePickupSaleOrderItemsToPutawayRequest;
import com.uniware.core.api.reversepickup.AddReversePickupSaleOrderItemsToPutawayRequest.WsSaleOrderItem;
import com.uniware.core.api.reversepickup.AddReversePickupSaleOrderItemsToPutawayResponse;
import com.uniware.core.api.reversepickup.EditReversePickupMetadataRequest;
import com.uniware.core.api.reversepickup.EditReversePickupMetadataResponse;
import com.uniware.core.api.saleorder.AddressDTO;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.api.warehouse.GetReversePickupRequest;
import com.uniware.core.api.warehouse.GetReversePickupResponse;
import com.uniware.core.cache.EnvironmentPropertiesCache;
import com.uniware.core.cache.FacilityCache;
import com.uniware.core.cache.LocationCache;
import com.uniware.core.entity.AddressDetail;
import com.uniware.core.entity.Channel;
import com.uniware.core.entity.Country;
import com.uniware.core.entity.Facility;
import com.uniware.core.entity.Invoice;
import com.uniware.core.entity.Item;
import com.uniware.core.entity.ItemType;
import com.uniware.core.entity.PaymentMethod;
import com.uniware.core.entity.Putaway;
import com.uniware.core.entity.Reshipment;
import com.uniware.core.entity.ReshipmentAction;
import com.uniware.core.entity.ReturnManifest;
import com.uniware.core.entity.ReturnManifestItem;
import com.uniware.core.entity.ReturnManifestStatus;
import com.uniware.core.entity.ReversePickup;
import com.uniware.core.entity.ReversePickup.Action;
import com.uniware.core.entity.ReversePickupAction;
import com.uniware.core.entity.ReversePickupAlternate;
import com.uniware.core.entity.SaleOrder;
import com.uniware.core.entity.SaleOrderItem;
import com.uniware.core.entity.ShipmentTracking;
import com.uniware.core.entity.ShipmentTrackingStatus;
import com.uniware.core.entity.ShippingMethod;
import com.uniware.core.entity.ShippingPackage;
import com.uniware.core.entity.ShippingProvider;
import com.uniware.core.entity.ShippingProviderMethod;
import com.uniware.core.entity.ShippingProviderMethod.TrackingNumberGeneration;
import com.uniware.core.entity.Source;
import com.uniware.core.locking.ILockingService;
import com.uniware.core.locking.Namespace;
import com.uniware.core.locking.annotation.Lock;
import com.uniware.core.locking.annotation.Locks;
import com.uniware.core.utils.ActivityContext;
import com.uniware.core.utils.Constants.EmailTemplateType;
import com.uniware.core.utils.EnvironmentProperties;
import com.uniware.core.utils.UserContext;
import com.uniware.core.vo.PrintTemplateVO;
import com.uniware.dao.returns.IReturnsDao;
import com.uniware.dao.shipping.IShippingDao;
import com.uniware.services.audit.impl.ActivityEntityEnum;
import com.uniware.services.audit.impl.ActivityTypeEnum;
import com.uniware.services.audit.impl.ActivityUtils;
import com.uniware.services.cache.ChannelCache;
import com.uniware.services.cache.PrintTemplateCache;
import com.uniware.services.cache.ReturnsCache;
import com.uniware.services.catalog.ICatalogService;
import com.uniware.services.configuration.ShippingConfiguration;
import com.uniware.services.configuration.ShippingFacilityLevelConfiguration;
import com.uniware.services.configuration.SourceConfiguration;
import com.uniware.services.configuration.SystemConfiguration;
import com.uniware.services.configuration.TenantSystemConfiguration;
import com.uniware.services.currency.ICurrencyService;
import com.uniware.services.exception.NoAvailableShippingProviderException;
import com.uniware.services.exception.NoAvailableTrackingNumberException;
import com.uniware.services.invoice.IInvoiceService;
import com.uniware.services.putaway.IPutawayService;
import com.uniware.services.returns.IReturnsService;
import com.uniware.services.saleorder.ISaleOrderService;
import com.uniware.services.shipping.IShipmentTrackingService;
import com.uniware.services.shipping.IShippingInvoiceService;
import com.uniware.services.shipping.IShippingProviderService;
import com.uniware.services.shipping.IShippingService;
import com.uniware.services.tax.ITaxService;
import java.io.File;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author singla
 */
@Service("returnsService")
public class ReturnsServiceImpl implements IReturnsService {

    private static final Logger      LOG = LoggerFactory.getLogger(ReturnsServiceImpl.class);

    @Autowired
    private ISaleOrderService        saleOrderService;

    @Autowired
    private IShipmentTrackingService shipmentTrackingService;

    @Autowired
    private IShippingService         shippingService;

    @Autowired
    private IShippingProviderService shippingProviderService;

    @Autowired
    private IReturnsDao              returnsDao;

    @Autowired
    private IEmailService            emailService;

    @Autowired
    private ICatalogService          catalogService;

    @Autowired
    private ICurrencyService         currencyService;

    @Autowired
    private IInvoiceService          invoiceService;

    @Autowired
    private IPutawayService          putawayService;

    @Autowired
    private IShippingDao             shippingDao;

    @Autowired
    private ILockingService          lockingService;

    @Autowired
    private IShippingInvoiceService  shippingInvoiceService;

    @Autowired
    private IUsersService            usersService;

    @Autowired
    private ITaxService              taxService;

    /* (non-Javadoc)
     * @see com.uniware.services.returns.IReturnsService#createReversePickup(com.uniware.core.api.returns.CreateReversePickupRequest)
     */
    @Override
    @Locks({ @Lock(ns = Namespace.SALE_ORDER, key = "#{#args[0].saleOrderCode}") })
    @Transactional
    @LogActivity
    public CreateReversePickupResponse createReversePickup(CreateReversePickupRequest request) {
        CreateReversePickupResponse response = new CreateReversePickupResponse();
        ValidationContext context = request.validate();
        Action action = Action.valueOfCode(request.getActionCode());
        SaleOrder saleOrder = saleOrderService.getSaleOrderForUpdate(request.getSaleOrderCode());
        if (saleOrder == null) {
            context.addError(WsResponseCode.INVALID_SALE_ORDER_CODE);
        } else if (action == null) {
            context.addError(WsResponseCode.INVALID_REVERSE_PICKUP_ACTION, "Invalid Reverse Pickup Action");
        } else if (action != Action.WAIT_FOR_RETURN_AND_CANCEL) {
            if (request.getShippingAddress() == null) {
                context.addError(WsResponseCode.INVALID_REVERSE_PICKUP_ACTION, "Shipping address is mandatory for replacements");
            } else {
                Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(saleOrder.getChannel().getSourceCode());
                if (Source.Type.MARKETPLACE.equals(source.getType())) {
                    context.addError(WsResponseCode.INVALID_SALE_ORDER_CODE, "For marketplace orders, only WAIT_FOR_RETURN_AND_CANCEL option is applicable");
                }
            }
        }

        if (!context.hasErrors() && request.getShippingAddress() != null) {
            LocationCache locationCache = CacheManager.getInstance().getCache(LocationCache.class);
            Country country = locationCache.getCountryByCode(request.getShippingAddress().getCountry());
            Pattern postcodePattern = locationCache.getPostcodePatternByCountryCode(request.getShippingAddress().getCountry());
            if (country == null) {
                context.addError(WsResponseCode.INVALID_ADDRESS_DETAIL, "Country has invalid code");
            } else {
                if (request.getShippingAddress().getPincode() != null && postcodePattern != null) {
                    if (!postcodePattern.matcher(request.getShippingAddress().getPincode()).matches()) {
                        context.addError(WsResponseCode.INVALID_ADDRESS_DETAIL, "Pincode has invalid format");
                    }
                }
            }
        }

        Facility currentFacility = CacheManager.getInstance().getCache(FacilityCache.class).getCurrentFacility();
        SystemConfiguration.TraceabilityLevel currentTraceabilityLevel = ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).getTraceabilityLevel();
        UserContext userContext = UserContext.current();
        if (!context.hasErrors()) {
            List<SaleOrderItem> itemsToReplace = new ArrayList<SaleOrderItem>();
            Map<String, WsReversePickupItem> saleOrderItemToReversePickupItems = new HashMap<String, WsReversePickupItem>();
            Map<String, ItemType> saleOrderItemToAlternateSKUs = new HashMap<String, ItemType>();
            for (WsReversePickupItem wsReversePickupItem : request.getReversePickItems()) {
                saleOrderItemToReversePickupItems.put(wsReversePickupItem.getSaleOrderItemCode(), wsReversePickupItem);
            }
            Set<String> saleOrderItemCodes = new HashSet<String>();
            saleOrderItemCodes.addAll(saleOrderItemToReversePickupItems.keySet());
            for (SaleOrderItem saleOrderItem : saleOrder.getSaleOrderItems()) {
                WsReversePickupItem wsReversePickupItem = saleOrderItemToReversePickupItems.get(saleOrderItem.getCode());
                if (wsReversePickupItem != null) {
                    saleOrderItemCodes.remove(saleOrderItem.getCode());
                    if (saleOrderItem.getReversePickup() != null) {
                        context.addError(WsResponseCode.REVERSE_PICKUP_ALREADY_CREATED_FOR_THIS_ITEM,
                                "SaleOrderItem[" + saleOrderItem.getCode() + "] already requested to be reverse picked");
                    } else if (!StringUtils.equalsAny(saleOrderItem.getStatusCode(), SaleOrderItem.StatusCode.DISPATCHED.name(), SaleOrderItem.StatusCode.DELIVERED.name())) {
                        context.addError(WsResponseCode.INVALID_SALE_ORDER_ITEM_STATE, "SaleOrderItem[" + saleOrderItem.getCode() + "] not yet delivered/dispatched");
                    } else if (saleOrderItem.getShippingPackage() != null && StringUtils.equalsAny(saleOrderItem.getShippingPackage().getStatusCode(),
                            ShippingPackage.StatusCode.RETURN_ACKNOWLEDGED.name(), ShippingPackage.StatusCode.RETURNED.name())) {
                        context.addError(WsResponseCode.INVALID_SALE_ORDER_ITEM_STATE,
                                "SaleOrderItem[" + saleOrderItem.getCode() + "] already acknowledged as returned by courier");
                    } else if (saleOrderItem.getShippingPackage() != null
                            && StringUtils.equalsAny(saleOrderItem.getShippingPackage().getStatusCode(), ShippingPackage.StatusCode.RETURN_EXPECTED.name())) {
                        context.addError(WsResponseCode.INVALID_SALE_ORDER_ITEM_STATE, "Shipping package " + saleOrderItem.getShippingPackage().getCode() + " is in invalid state");
                    } else {
                        // Check if saleOrderItem being returned is of same traceability as current facility's traceability.
                        Facility facility = saleOrderItem.getFacility();
                        if (!facility.getCode().equals(currentFacility.getCode())) {
                            userContext.setFacility(facility);
                            SystemConfiguration.TraceabilityLevel traceabilityLevel = ConfigurationManager.getInstance().getConfiguration(
                                    SystemConfiguration.class).getTraceabilityLevel();
                            userContext.setFacility(currentFacility);
                            if (!traceabilityLevel.equals(currentTraceabilityLevel)) {
                                context.addError(WsResponseCode.INVALID_REVERSE_PICKUP_ACTION, "Traceability mismatch");
                                break;
                            }
                        }
                        itemsToReplace.add(saleOrderItem);
                        if (wsReversePickupItem.getReversePickupAlternate() != null) {
                            ItemType itemType = catalogService.getItemTypeBySkuCode(wsReversePickupItem.getReversePickupAlternate().getItemSku());
                            if (itemType == null) {
                                context.addError(WsResponseCode.INVALID_ITEM_TYPE, "Invalid item type SKU:[" + wsReversePickupItem.getReversePickupAlternate().getItemSku() + "]");
                            } else {
                                saleOrderItemToAlternateSKUs.put(saleOrderItem.getCode(), itemType);
                            }
                        }
                    }
                }
            }
            if (saleOrderItemCodes.size() > 0) {
                context.addError(WsResponseCode.INVALID_SALE_ORDER_ITEM_CODE, "Invalid SaleOrderItem codes :[" + StringUtils.join('|', saleOrderItemCodes) + "]");
            }
            if (request.getReversePickupCode() != null) {
                ReversePickup reversePickup = getReversePickupByCode(request.getReversePickupCode());
                if (reversePickup != null) {
                    context.addError(WsResponseCode.REVERSE_PICKUP_ALREADY_CREATED_FOR_THIS_ITEM, "Duplicate reverse pickup code: " + request.getReversePickupCode());
                }
            }

            if (!context.hasErrors()) {
                ReversePickup reversePickup = new ReversePickup();
                reversePickup.setSaleOrder(saleOrder);
                reversePickup.setStatusCode(ReversePickup.StatusCode.CREATED.name());
                reversePickup.setReversePickupAction(action.code());
                if (request.getReversePickupCode() != null) {
                    reversePickup.setCode(request.getReversePickupCode());
                }
                if (action != Action.WAIT_FOR_RETURN_AND_CANCEL) {
                    AddressDetail shippingAddress = new AddressDetail(request.getShippingAddress().getName(), request.getShippingAddress().getAddressLine1(),
                            request.getShippingAddress().getAddressLine2(), request.getShippingAddress().getCity(), request.getShippingAddress().getState(),
                            request.getShippingAddress().getCountry(), request.getShippingAddress().getPincode(), request.getShippingAddress().getPhone(),
                            request.getShippingAddress().getEmail(), DateUtils.getCurrentTime(), DateUtils.getCurrentTime());
                    shippingAddress = saleOrderService.addAddressDetail(shippingAddress);
                    reversePickup.setShippingAddress(shippingAddress);
                }
                reversePickup.setCreated(DateUtils.getCurrentTime());
                reversePickup.setUpdated(DateUtils.getCurrentTime());
                Map<String, Object> customFieldValues = CustomFieldUtils.getCustomFieldValues(context, ReversePickup.class.getName(), request.getCustomFieldValues());
                CustomFieldUtils.setCustomFieldValues(reversePickup, customFieldValues);
                reversePickup = returnsDao.addReversePickup(reversePickup);
                for (SaleOrderItem saleOrderItem : itemsToReplace) {
                    if (saleOrderItemToAlternateSKUs.containsKey(saleOrderItem.getCode())) {
                        WsReversePickupAlternate alternate = saleOrderItemToReversePickupItems.get(saleOrderItem.getCode()).getReversePickupAlternate();
                        ReversePickupAlternate reversePickupAlternate = new ReversePickupAlternate(reversePickup, saleOrderItem,
                                saleOrderItemToAlternateSKUs.get(saleOrderItem.getCode()), alternate.getTotalPrice(), alternate.getPrepaidAmount(), alternate.getSellingPrice(),
                                alternate.getDiscount(), alternate.getShippingCharges(), DateUtils.getCurrentTime());
                        returnsDao.addReversePickupAlternate(reversePickupAlternate);
                        reversePickup.getReversePickupAlternates().add(reversePickupAlternate);
                    }
                    reversePickup.getSaleOrderItems().add(saleOrderItem);
                    saleOrderItem.setReversePickup(reversePickup);
                    saleOrderItem.setReversePickupReason(saleOrderItemToReversePickupItems.get(saleOrderItem.getCode()).getReason());
                    saleOrderService.updateSaleOrderItem(saleOrderItem);
                }
                switch (action) {
                    case REPLACE_IMMEDIATELY_DONT_EXPECT_RETURN:
                        reversePickup.setReplacementSaleOrder(createReplacement(reversePickup, reversePickup.getSaleOrderItems()));
                        reversePickup.setStatusCode(ReversePickup.StatusCode.COMPLETE.name());
                        markSaleOrderItemsReplaced(reversePickup);
                        break;
                    case REPLACE_IMMEDIATELY_EXPECT_RETURN:
                        reversePickup.setReplacementSaleOrder(createReplacement(reversePickup, reversePickup.getSaleOrderItems()));
                        markSaleOrderItemsReplaced(reversePickup);
                        break;
                    case WAIT_FOR_RETURN_AND_REPLACE:
                    case WAIT_FOR_RETURN_AND_CANCEL:
                        break;
                }
                response.setReversePickupCode(reversePickup.getCode());
                for (SaleOrderItem saleOrderItem : reversePickup.getSaleOrderItems()) {
                    response.addSaleOrderItemCode(saleOrderItem.getCode());
                }
                if (ActivityContext.current().isEnable()) {
                    switch (action) {
                        case REPLACE_IMMEDIATELY_DONT_EXPECT_RETURN:
                        case REPLACE_IMMEDIATELY_EXPECT_RETURN:
                            ActivityUtils.appendActivity(reversePickup.getSaleOrder().getCode(), ActivityEntityEnum.SALE_ORDER.getName(), null,
                                    Arrays.asList(new String[] {
                                            "Reverse pick {" + ActivityEntityEnum.REVERSE_PICKUP + ":" + reversePickup.getCode() + "} created for sale order {"
                                                    + ActivityEntityEnum.SALE_ORDER + ":" + reversePickup.getSaleOrder().getCode() + "} for order items "
                                                    + response.getSaleOrderItemCodes() + " with action " + reversePickup.getReversePickupAction() }),
                                    ActivityTypeEnum.REPLACEMENT.name());
                            break;
                        case WAIT_FOR_RETURN_AND_CANCEL:
                        case WAIT_FOR_RETURN_AND_REPLACE:
                            ActivityUtils.appendActivity(reversePickup.getSaleOrder().getCode(), ActivityEntityEnum.SALE_ORDER.getName(), null,
                                    Arrays.asList(new String[] {
                                            "Reverse pick {" + ActivityEntityEnum.REVERSE_PICKUP + ":" + reversePickup.getCode() + "} created for sale order {"
                                                    + ActivityEntityEnum.REVERSE_PICKUP + ":" + reversePickup.getSaleOrder().getCode() + "} for order items {"
                                                    + ActivityEntityEnum.SALE_ORDER_ITEM + ":" + response.getSaleOrderItemCodes() + "} with action "
                                                    + reversePickup.getReversePickupAction() }),
                                    ActivityTypeEnum.RETURN.name());
                            break;
                    }

                }
                response.setSuccessful(true);
            }

        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    @LogActivity
    public CancelReversePickupResponse cancelReversePickup(CancelReversePickupRequest request) {
        CancelReversePickupResponse response = new CancelReversePickupResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            ReversePickup reversePickup = returnsDao.getReversePickupByCode(request.getReversePickupCode());
            if (reversePickup == null) {
                context.addError(WsResponseCode.INVALID_REVERSE_PICKUP_CODE, "Invalid reverse pickup code");
            } else if (!StringUtils.equalsAny(reversePickup.getReversePickupAction(), ReversePickup.Action.WAIT_FOR_RETURN_AND_CANCEL.code(),
                    ReversePickup.Action.WAIT_FOR_RETURN_AND_REPLACE.code())) {
                context.addError(WsResponseCode.INVALID_REVERSE_PICKUP_STATE,
                        "Reverse pickups only with action WAIT_FOR_RETURN_AND_CANCEL/WAIT_FOR_RETURN_AND_REPLACE can be cancelled");
            } else {
                cancelReversePickup(reversePickup, reversePickup.getSaleOrderItems().iterator().next().getSaleOrder().getCode(), context);
                if (!context.hasErrors()) {
                    List<String> saleOrderItemCodes = new ArrayList<>();
                    for (SaleOrderItem saleOrderItem : reversePickup.getSaleOrderItems()) {
                        saleOrderItemCodes.add(saleOrderItem.getCode());
                    }
                    if (ActivityContext.current().isEnable()) {
                        ActivityUtils.appendActivity(reversePickup.getSaleOrderItems().iterator().next().getSaleOrder().getCode(), ActivityEntityEnum.SALE_ORDER.getName(), null,
                                Arrays.asList(new String[] {
                                        "{" + ActivityEntityEnum.SALE_ORDER_ITEM + ":" + saleOrderItemCodes + "} reverse pick up {" + ActivityEntityEnum.REVERSE_PICKUP + ":"
                                                + reversePickup.getCode() + "} cancelled." }),
                                ActivityTypeEnum.CANCEL.name());
                    }
                    response.setSuccessful(true);
                }
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Locks({ @Lock(ns = Namespace.SALE_ORDER, key = "#{#args[1]}") })
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void cancelReversePickup(ReversePickup reversePickup, String saleOrderCode, ValidationContext context) {
        reversePickup = returnsDao.getReversePickupById(reversePickup.getId());
        if (!ReversePickup.StatusCode.CREATED.name().equals(reversePickup.getStatusCode())) {
            context.addError(WsResponseCode.INVALID_REVERSE_PICKUP_STATE, "Reverse pickups can be cancelled only in CREATED state");
        } else {
            reversePickup.setStatusCode(ReversePickup.StatusCode.CANCELLED.name());
            for (SaleOrderItem saleOrderItem : reversePickup.getSaleOrderItems()) {
                saleOrderItem.setReversePickup(null);
            }
        }
    }

    /**
     * @param reversePickup
     */
    private void markSaleOrderItemsReplaced(ReversePickup reversePickup) {
        for (SaleOrderItem saleOrderItem : reversePickup.getSaleOrderItems()) {
            saleOrderItem.setStatusCode(SaleOrderItem.StatusCode.REPLACED.name());
            saleOrderService.updateSaleOrderItem(saleOrderItem);
        }
    }

    @Override
    @Transactional
    public SaleOrder createReplacement(ReversePickup reversePickup, Set<SaleOrderItem> saleOrderItemsToReplace) {
        SaleOrder rSaleOrder = new SaleOrder(reversePickup.getSaleOrder());
        rSaleOrder.setCurrencyConversionRate(currencyService.getCurrencyConversionRate(new GetCurrencyConversionRateRequest(rSaleOrder.getCurrencyCode())).getConversionRate());
        rSaleOrder.setPriority(SaleOrder.Priority.CRITICAL.ordinal());
        CustomFieldUtils.setCustomFieldValues(rSaleOrder, CustomFieldUtils.getCustomFieldValues(reversePickup.getSaleOrder()));
        rSaleOrder.setCode(getReplacementSaleOrderCode(reversePickup.getSaleOrder(), reversePickup));
        PaymentMethod paymentMethod = ConfigurationManager.getInstance().getConfiguration(ShippingConfiguration.class).getPaymentMethodByCode(PaymentMethod.Code.PREPAID.name());
        rSaleOrder.setPaymentMethod(paymentMethod);
        Map<Integer, ReversePickupAlternate> saleOrderItemToAlternates = new HashMap<Integer, ReversePickupAlternate>();
        for (ReversePickupAlternate alternate : reversePickup.getReversePickupAlternates()) {
            saleOrderItemToAlternates.put(alternate.getSaleOrderItem().getId(), alternate);
        }
        for (SaleOrderItem saleOrderItem : saleOrderItemsToReplace) {
            SaleOrderItem rSaleOrderItem = new SaleOrderItem(saleOrderItem);
            rSaleOrderItem.setShippingAddress(reversePickup.getShippingAddress());
            rSaleOrderItem.setPrepaidAmount(saleOrderItem.getTotalPrice());
            rSaleOrderItem.setSaleOrder(rSaleOrder);
            rSaleOrderItem.setVoucherValue(BigDecimal.ZERO);
            rSaleOrderItem.setVoucherCode(null);
            rSaleOrderItem.setStoreCredit(BigDecimal.ZERO);
            if (saleOrderItemToAlternates.containsKey(saleOrderItem.getId())) {
                ReversePickupAlternate alternate = saleOrderItemToAlternates.get(saleOrderItem.getId());
                rSaleOrderItem.setItemType(alternate.getItemType());
                rSaleOrderItem.setPrepaidAmount(alternate.getPrepaidAmount());
                rSaleOrderItem.setTotalPrice(alternate.getTotalPrice());
                rSaleOrderItem.setSellingPrice(alternate.getSellingPrice());
                rSaleOrderItem.setShippingCharges(alternate.getShippingCharges());
            }
            if (NumberUtils.greaterThan(rSaleOrderItem.getTotalPrice(), rSaleOrderItem.getPrepaidAmount())) {
                paymentMethod = ConfigurationManager.getInstance().getConfiguration(ShippingConfiguration.class).getPaymentMethodByCode(PaymentMethod.Code.COD.name());
                rSaleOrder.setPaymentMethod(paymentMethod);
                rSaleOrderItem.setShippingMethod(shippingService.getShippingMethodByCode(rSaleOrderItem.getShippingMethod().getCode(), true));
            } else {
                rSaleOrderItem.setShippingMethod(shippingService.getShippingMethodByCode(rSaleOrderItem.getShippingMethod().getCode(), false));
            }
            if (NumberUtils.greaterThan(rSaleOrderItem.getPrepaidAmount(), rSaleOrderItem.getTotalPrice())) {
                rSaleOrderItem.setPrepaidAmount(rSaleOrderItem.getTotalPrice());
            }
            CustomFieldUtils.setCustomFieldValues(rSaleOrderItem, CustomFieldUtils.getCustomFieldValues(saleOrderItem));
            rSaleOrder.getSaleOrderItems().add(rSaleOrderItem);
        }

        saleOrderService.addSaleOrder(rSaleOrder);
        if (ActivityContext.current().isEnable()) {
            String soiStr = "";
            int index = 0;
            for (SaleOrderItem soi : reversePickup.getSaleOrderItems()) {
                soiStr += (soi.getCode() + (index++ < reversePickup.getSaleOrderItems().size() ? "," : ""));
            }
            ActivityUtils.appendActivity(reversePickup.getSaleOrder().getCode(), ActivityEntityEnum.SALE_ORDER.getName(), null, Arrays.asList(new String[] {
                    "Replacement sale order {" + ActivityEntityEnum.REPLACEMENT_ORDER + ":" + rSaleOrder.getCode() + "} created for sale order {" + ActivityEntityEnum.SALE_ORDER
                            + ":" + reversePickup.getSaleOrder().getCode() + "} with sale order items {" + ActivityEntityEnum.SALE_ORDER_ITEM + ":" + soiStr + "}" }),
                    ActivityTypeEnum.REPLACEMENT.name());
        }
        return rSaleOrder;
    }

    /**
     * @param saleOrder
     * @param reversePickup
     * @return
     */
    private String getReplacementSaleOrderCode(SaleOrder saleOrder, ReversePickup reversePickup) {
        return new StringBuilder().append(saleOrder.getCode()).append("-R").append(reversePickup.getCode()).toString();
    }

    /* (non-Javadoc)
     * @see com.uniware.services.returns.IReturnsService#assignShippingProviderToReversePickup(com.uniware.core.api.returns.AssignShippingProviderToReversePickupRequest)
     */
    @Override
    @Transactional
    public AssignShippingProviderToReversePickupResponse assignShippingProviderToReversePickup(AssignShippingProviderToReversePickupRequest request) {
        AssignShippingProviderToReversePickupResponse response = new AssignShippingProviderToReversePickupResponse();
        ValidationContext context = request.validate();
        ReversePickup reversePickup = null;
        if (!context.hasErrors()) {
            ShippingProvider shippingProvider = ConfigurationManager.getInstance().getConfiguration(ShippingConfiguration.class).getShippingProviderByCode(
                    request.getShippingProviderCode());
            if (shippingProvider == null) {
                context.addError(WsResponseCode.INVALID_SHIPPING_PROVIDER_CODE, "Invalid Shipping Provider");
            } else {
                reversePickup = returnsDao.getReversePickupByCode(request.getReversePickupCode());
                if (reversePickup == null) {
                    context.addError(WsResponseCode.INVALID_REVERSE_PICKUP_ID, "Invalid Reverse Pickup Id");
                } else if (ReversePickup.StatusCode.COMPLETE.name().equals(reversePickup.getStatusCode())) {
                    context.addError(WsResponseCode.INVALID_REVERSE_PICKUP_STATE, "Invalid Reverse Pickup State: Completed");
                } else {
                    reversePickup.setShippingProvider(shippingProvider);
                    if (StringUtils.isNotBlank(request.getTrackingNumber())) {
                        ShipmentTracking shipmentTracking = new ShipmentTracking(ShipmentTracking.StatusCode.NO_INFORMATION.name(), ShipmentTracking.Type.REVERSE_PICKUP.name(),
                                shippingProvider, request.getTrackingNumber(), DateUtils.getCurrentTime(), DateUtils.getCurrentTime());
                        shipmentTrackingService.addShipmentTracking(shipmentTracking);
                        reversePickup.setTrackingNumber(request.getTrackingNumber());
                        reversePickup.setShipmentTracking(shipmentTracking);
                    }
                    returnsDao.updateReversePickup(reversePickup);
                    response.setSuccessful(true);
                }
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Locks({ @Lock(ns = Namespace.SALE_ORDER, key = "#{#args[0].saleOrderCode}") })
    @Transactional
    @RollbackOnFailure
    @LogActivity
    public ReshipShippingPackageResponse reshipShippingPackage(ReshipShippingPackageRequest request) {
        ReshipShippingPackageResponse response = new ReshipShippingPackageResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            SaleOrder saleOrder = saleOrderService.getSaleOrderForUpdate(request.getSaleOrderCode());
            ShippingPackage shippingPackage = shippingService.getShippingPackageByCode(request.getShippingPackageCode());
            if (saleOrder == null) {
                context.addError(WsResponseCode.INVALID_SALE_ORDER_CODE, "Invalid Sale Order Code");
            } else if (shippingPackage == null) {
                context.addError(WsResponseCode.INVALID_SHIPPING_PACKAGE_CODE, "Invalid Shipping Package Code");
            } else if (!shippingPackage.getSaleOrder().getId().equals(saleOrder.getId())) {
                context.addError(WsResponseCode.INVALID_PACKAGE_STATE, "Shipping package doesnt belong to given sale order");
            } else if (!ShippingPackage.StatusCode.RETURN_ACKNOWLEDGED.name().equals(shippingPackage.getStatusCode())) {
                context.addError(WsResponseCode.INVALID_PACKAGE_STATE, "Only RETURN_ACKNOWLEDGED packages can be reshipped directly");
            }

            if (!context.hasErrors()) {
                CreateReshipmentRequest createReshipmentRequest = new CreateReshipmentRequest();
                createReshipmentRequest.setShippingPackageCode(request.getShippingPackageCode());
                createReshipmentRequest.setActionCode(Reshipment.Action.WAIT_FOR_RETURN_AND_RESHIP.code());
                CreateReshipmentResponse createReshipmentResponse = createReshipment(createReshipmentRequest);
                if (!createReshipmentResponse.hasErrors()) {
                    String reshipmentCode = createReshipmentResponse.getReshipmentCode();
                    Reshipment reshipment = returnsDao.getReshipmentByCode(reshipmentCode);
                    completeReshipment(reshipment.getId());
                    SaleOrder reshipmentSaleOrder = saleOrderService.getSaleOrderForUpdate(reshipment.getReshipmentSaleOrder().getId());
                    reshipmentSaleOrder.setStatusCode(SaleOrder.StatusCode.PROCESSING.name());
                    Map<String, Item> saleOrderItemToItems = new HashMap<String, Item>();
                    for (SaleOrderItem saleOrderItem : shippingPackage.getSaleOrderItems()) {
                        saleOrderItemToItems.put(saleOrderItem.getCode(), saleOrderItem.getItem());
                    }
                    for (SaleOrderItem saleOrderItem : reshipmentSaleOrder.getSaleOrderItems()) {
                        saleOrderItem.setStatusCode(SaleOrderItem.StatusCode.FULFILLABLE.name());
                        Item item = saleOrderItemToItems.get(saleOrderItem.getCode());
                        if (item != null) {
                            saleOrderItem.setItem(item);
                        }
                    }
                    shippingPackage.setStatusCode(ShippingPackage.StatusCode.RETURNED.name());
                    ShippingPackage reshipmentShippingPackage = shippingService.createShippingPackage(CollectionUtils.asList(reshipmentSaleOrder.getSaleOrderItems()), false);
                    reshipmentShippingPackage.setRequiresCustomization(false);
                    reshipmentShippingPackage.setStatusCode(ShippingPackage.StatusCode.PICKED.name());

                    CreateShippingPackageInvoiceRequest createShippingPackageInvoiceRequest = new CreateShippingPackageInvoiceRequest();
                    createShippingPackageInvoiceRequest.setUserId(ConfigurationManager.getInstance().getConfiguration(TenantSystemConfiguration.class).getSystemUser().getId());
                    createShippingPackageInvoiceRequest.setShippingPackageCode(reshipmentShippingPackage.getCode());
                    CreateShippingPackageInvoiceResponse createShippingPackageInvoiceResponse = shippingInvoiceService.createShippingPackageInvoice(
                            createShippingPackageInvoiceRequest);
                    response.addErrors(createShippingPackageInvoiceResponse.getErrors());
                    if (!response.hasErrors()) {
                        response.setSuccessful(true);
                    }
                } else {
                    response.addErrors(createReshipmentResponse.getErrors());
                }
            }
        }
        response.addErrors(context.getErrors());
        return response;
    }

    @Override
    public CreateReshipmentResponse createReshipment(CreateReshipmentRequest request) {
        ShippingPackage shippingPackage = shippingService.getShippingPackageByCode(request.getShippingPackageCode());
        return doCreateReshipment(request, shippingPackage.getSaleOrder().getCode());
    }

    @Locks({ @Lock(ns = Namespace.SALE_ORDER, key = "#{#args[1]}") })
    @Transactional
    @LogActivity
    private CreateReshipmentResponse doCreateReshipment(CreateReshipmentRequest request, String saleOrderCode) {
        CreateReshipmentResponse response = new CreateReshipmentResponse();
        ValidationContext context = request.validate();
        Reshipment.Action action = Reshipment.Action.valueOfCode(request.getActionCode());
        if (action == null) {
            context.addError(WsResponseCode.INVALID_RESHIPMENT_ACTION, "Invalid Reshipment Action");
        }
        if (!context.hasErrors()) {
            ShippingPackage shippingPackage = shippingService.getShippingPackageByCode(request.getShippingPackageCode());
            if (shippingPackage == null) {
                context.addError(WsResponseCode.INVALID_SHIPPING_PACKAGE_CODE, "Invalid Shipping Package Code");
            } else {
                SaleOrder saleOrder = saleOrderService.getSaleOrderForUpdate(shippingPackage.getSaleOrder().getId());
                shippingPackage = shippingService.getShippingPackageById(shippingPackage.getId(), true);
                if (!StringUtils.equalsAny(shippingPackage.getStatusCode(), ShippingPackage.StatusCode.DISPATCHED.name(), ShippingPackage.StatusCode.RETURN_EXPECTED.name(),
                        ShippingPackage.StatusCode.RETURN_ACKNOWLEDGED.name(), ShippingPackage.StatusCode.RETURNED.name())) {
                    context.addError(WsResponseCode.INVALID_PACKAGE_STATE, "Invalid shipping package state");
                } else if (shippingPackage.getReshipment() != null) {
                    context.addError(WsResponseCode.RESHIPMENT_ALREADY_CREATED_FOR_THIS_PACKAGE);
                } else {
                    Reshipment reshipment = new Reshipment();
                    reshipment.setSaleOrder(saleOrder);
                    reshipment.setShippingPackage(shippingPackage);
                    reshipment.setStatusCode(ReversePickup.StatusCode.CREATED.name());
                    reshipment.setReshipmentAction(action.code());
                    reshipment.setShippingAddress(shippingPackage.getShippingAddress());
                    reshipment.setCreated(DateUtils.getCurrentTime());
                    reshipment.setUpdated(DateUtils.getCurrentTime());
                    reshipment = returnsDao.addReshipment(reshipment);
                    shippingPackage.setReshipment(reshipment);
                    shippingPackage = shippingService.updateShippingPackage(shippingPackage);
                    if (StringUtils.equalsAny(shippingPackage.getStatusCode(), ShippingPackage.StatusCode.RETURNED.name())) {
                        completeReshipment(reshipment.getId());
                    } else {
                        switch (action) {
                            case RESHIP_IMMEDIATELY_DONT_EXPECT_RETURN:
                                reshipment.setReshipmentSaleOrder(createReshipmentOrder(reshipment));
                                reshipment.setStatusCode(Reshipment.StatusCode.COMPLETE.name());
                                reshipment = returnsDao.updateReshipment(reshipment);
                                markSaleOrderItemsReshipped(reshipment.getShippingPackage().getSaleOrderItems());
                                break;
                            case RESHIP_IMMEDIATELY_EXPECT_RETURN:
                                reshipment.setReshipmentSaleOrder(createReshipmentOrder(reshipment));
                                reshipment = returnsDao.updateReshipment(reshipment);
                                markSaleOrderItemsReshipped(reshipment.getShippingPackage().getSaleOrderItems());
                                break;
                            case WAIT_FOR_RETURN_AND_RESHIP:
                            case WAIT_FOR_RETURN_AND_CANCEL:
                                break;
                        }
                    }
                    response.setReshipmentCode(reshipment.getCode());
                    if (ActivityContext.current().isEnable()) {
                        ActivityUtils.appendActivity(shippingPackage.getCode(), ActivityEntityEnum.SHIPPING_PACKAGE.getName(), shippingPackage.getSaleOrder().getCode(),
                                Arrays.asList(new String[] {
                                        "Reshipment {" + ActivityEntityEnum.RESHIPMENT_ORDER + ":" + reshipment.getCode() + "} created for shipment {"
                                                + ActivityEntityEnum.SHIPPING_PACKAGE + ":" + shippingPackage.getCode() + "} for action " + action.name() }),
                                ActivityTypeEnum.RESHIPMENT.name());
                    }
                    response.setSuccessful(true);
                }
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Transactional
    @Override
    public void completeReshipment(int reshipmentId) {
        Reshipment reshipment = getReshipmentById(reshipmentId);
        if (!Reshipment.StatusCode.COMPLETE.name().equals(reshipment.getStatusCode())) {
            reshipment.setStatusCode(Reshipment.StatusCode.COMPLETE.name());
            if (StringUtils.equalsAny(reshipment.getReshipmentAction(), Reshipment.Action.RESHIP_IMMEDIATELY_DONT_EXPECT_RETURN.code(),
                    Reshipment.Action.RESHIP_IMMEDIATELY_EXPECT_RETURN.code(), Reshipment.Action.WAIT_FOR_RETURN_AND_RESHIP.code())
                    && reshipment.getReshipmentSaleOrder() == null) {
                reshipment.setReshipmentSaleOrder(createReshipmentOrder(reshipment));
                markSaleOrderItemsReshipped(reshipment.getShippingPackage().getSaleOrderItems());
            } else if (reshipment.getReshipmentAction().equals(Reshipment.Action.WAIT_FOR_RETURN_AND_CANCEL.code())) {
                List<String> saleOrderItemCodes = new ArrayList<>();
                for (SaleOrderItem saleOrderItem : reshipment.getShippingPackage().getSaleOrderItems()) {
                    saleOrderItem.setStatusCode(SaleOrderItem.StatusCode.CANCELLED.name());
                    saleOrderItem.setCancellationTime(DateUtils.getCurrentTime());
                    saleOrderItemCodes.add(saleOrderItem.getCode());
                    saleOrderService.updateSaleOrderItem(saleOrderItem);
                }
                if (ActivityContext.current().isEnable()) {
                    ActivityUtils.appendActivity(reshipment.getSaleOrder().getCode(), ActivityEntityEnum.SALE_ORDER.getName(), null,
                            Arrays.asList(new String[] {
                                    "{" + ActivityEntityEnum.SALE_ORDER_ITEM + ":" + saleOrderItemCodes + "} cancelled due to reshipment action "
                                            + Reshipment.Action.WAIT_FOR_RETURN_AND_CANCEL.name() }),
                            ActivityTypeEnum.RESHIPMENT.name());
                }
            }
        }

    }

    /**
     * @param saleOrderItems
     */
    @Override
    @Transactional
    public void markSaleOrderItemsReshipped(Set<SaleOrderItem> saleOrderItems) {
        List<String> saleOrderItemCodes = new ArrayList<>();
        for (SaleOrderItem saleOrderItem : saleOrderItems) {
            saleOrderItem.setStatusCode(SaleOrderItem.StatusCode.RESHIPPED.name());
            saleOrderItemCodes.add(saleOrderItem.getCode());
            saleOrderService.updateSaleOrderItem(saleOrderItem);
        }
        if (ActivityContext.current().isEnable()) {
            ActivityUtils.appendActivity(saleOrderItems.iterator().next().getSaleOrder().getCode(), ActivityEntityEnum.SALE_ORDER.getName(), null,
                    Arrays.asList(new String[] { "{" + ActivityEntityEnum.SALE_ORDER_ITEM + ":" + saleOrderItemCodes + "} marked " + SaleOrderItem.StatusCode.RESHIPPED.name() }),
                    ActivityTypeEnum.RESHIPMENT.name());
        }
    }

    /**
     * @param reshipment
     * @return
     */
    @Override
    @Transactional
    public SaleOrder createReshipmentOrder(Reshipment reshipment) {
        SaleOrder rSaleOrder = new SaleOrder(reshipment.getSaleOrder());
        rSaleOrder.setCurrencyConversionRate(currencyService.getCurrencyConversionRate(new GetCurrencyConversionRateRequest(rSaleOrder.getCurrencyCode())).getConversionRate());
        rSaleOrder.setPriority(SaleOrder.Priority.CRITICAL.ordinal());
        CustomFieldUtils.setCustomFieldValues(rSaleOrder, CustomFieldUtils.getCustomFieldValues(reshipment.getSaleOrder()));
        rSaleOrder.setCode(getReshipmentSaleOrderCode(reshipment.getSaleOrder(), reshipment));
        for (SaleOrderItem saleOrderItem : reshipment.getShippingPackage().getSaleOrderItems()) {
            SaleOrderItem rSaleOrderItem = new SaleOrderItem(saleOrderItem);
            rSaleOrderItem.setSaleOrder(rSaleOrder);
            CustomFieldUtils.setCustomFieldValues(rSaleOrderItem, CustomFieldUtils.getCustomFieldValues(saleOrderItem));
            rSaleOrder.getSaleOrderItems().add(rSaleOrderItem);
        }
        if (ActivityContext.current().isEnable()) {
            ActivityUtils.appendActivity(reshipment.getSaleOrder().getCode(), ActivityEntityEnum.SALE_ORDER.getName(), null,
                    Arrays.asList(new String[] {
                            "New Reshipment order {" + ActivityEntityEnum.RESHIPMENT_ORDER + ":" + rSaleOrder.getCode() + "} created for sale order {"
                                    + ActivityEntityEnum.SALE_ORDER + ":" + reshipment.getSaleOrder().getCode() + "}" }),
                    ActivityTypeEnum.RESHIPMENT.name());
        }
        saleOrderService.addSaleOrder(rSaleOrder);
        return rSaleOrder;
    }

    /**
     * @param saleOrder
     * @param reshipment
     * @return
     */
    private String getReshipmentSaleOrderCode(SaleOrder saleOrder, Reshipment reshipment) {
        return new StringBuilder().append(saleOrder.getCode()).append("-S").append(reshipment.getCode()).toString();
    }

    @Override
    @Transactional
    public List<ReshipmentDTO> getPendingReshipments() {
        List<ReshipmentDTO> reshipments = new ArrayList<ReshipmentDTO>();
        for (Reshipment reshipment : returnsDao.getPendingReshipments()) {
            ReshipmentDTO reshipmentDTO = new ReshipmentDTO();
            reshipmentDTO.setId(reshipment.getId());
            reshipmentDTO.setCode(reshipment.getCode());
            reshipmentDTO.setShippingPackageId(reshipment.getShippingPackage().getId());
            reshipmentDTO.setShippingPackageCode(reshipment.getShippingPackage().getCode());
            reshipmentDTO.setShippingAddress(new AddressDTO(reshipment.getShippingAddress()));
            reshipmentDTO.setSaleOrderCode(reshipment.getSaleOrder().getCode());
            if (reshipment.getReshipmentSaleOrder() != null) {
                reshipmentDTO.setReshipmentSaleOrderCode(reshipment.getReshipmentSaleOrder().getCode());
            }
            reshipmentDTO.setActionCode(reshipment.getReshipmentAction());
            reshipmentDTO.setActionDescription(
                    CacheManager.getInstance().getCache(ReturnsCache.class).getReshipmentActionByCode(reshipment.getReshipmentAction()).getDescription());
            reshipments.add(reshipmentDTO);
        }
        return reshipments;
    }

    @Override
    @Transactional
    public List<ReversePickupDTO> getUnassignedPendingReversePickups() {
        List<ReversePickupDTO> reversePickups = new ArrayList<ReversePickupDTO>();
        for (ReversePickup reversePickup : returnsDao.getUnassignedPendingReversePickups()) {
            ReversePickupDTO reversePickupDTO = new ReversePickupDTO();
            reversePickupDTO.setId(reversePickup.getId());
            reversePickupDTO.setCode(reversePickup.getCode());
            reversePickupDTO.setStatusCode(reversePickup.getStatusCode());
            if (reversePickup.getReplacementSaleOrder() != null) {
                reversePickupDTO.setReplacementSaleOrderCode(reversePickup.getReplacementSaleOrder().getCode());
            }

            reversePickupDTO.setSaleOrderCode(reversePickup.getSaleOrder().getCode());
            reversePickupDTO.setActionCode(
                    CacheManager.getInstance().getCache(ReturnsCache.class).getReversePickupActionByCode(reversePickup.getReversePickupAction()).getDescription());
            reversePickupDTO.setCreated(reversePickup.getCreated());
            ShippingProvider shippingProvider = reversePickup.getShippingProvider();
            if (shippingProvider != null) {
                reversePickupDTO.setShippingProvider(shippingProvider.getCode());
            }
            reversePickupDTO.setReason(reversePickup.getSaleOrderItems().iterator().next().getReversePickupReason());
            reversePickupDTO.setTrackingNumber(reversePickup.getTrackingNumber());
            reversePickups.add(reversePickupDTO);
        }
        return reversePickups;
    }

    @Override
    @Transactional(readOnly = true)
    public GetReversePickupResponse getReversePickup(GetReversePickupRequest request) {
        GetReversePickupResponse response = new GetReversePickupResponse();
        ValidationContext context = request.validate();
        ReversePickup reversePickup = null;
        if (!context.hasErrors()) {
            if (StringUtils.isNotEmpty(request.getTrackingNumber())) {
                reversePickup = returnsDao.getReversePickupByTrackingNumber(request.getTrackingNumber());
            } else if (request.getReversePickupCode() != null) {
                reversePickup = returnsDao.getReversePickupByCode(request.getReversePickupCode());
            }

            if (reversePickup != null) {
                ReversePickupDTO reversePickupDTO = prepareReversePickupDTO(reversePickup);
                response.setReversePickup(reversePickupDTO);
                response.setSuccessful(true);
            } else {
                context.addError(WsResponseCode.INVALID_REVERSE_PICKUP_ID, "Reverse Pickup doesn't exist");
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    public ReversePickupDTO prepareReversePickupDTO(ReversePickup reversePickup) {
        ReversePickupDTO reversePickupDTO = new ReversePickupDTO();
        reversePickupDTO.setId(reversePickup.getId());
        reversePickupDTO.setCode(reversePickup.getCode());
        reversePickupDTO.setStatusCode(reversePickup.getStatusCode());
        if (reversePickup.getReplacementSaleOrder() != null) {
            reversePickupDTO.setReplacementSaleOrderCode(reversePickup.getReplacementSaleOrder().getCode());
            reversePickupDTO.setReplacementSaleOrderStatus(reversePickup.getReplacementSaleOrder().getStatusCode());
        }

        reversePickupDTO.setSaleOrderCode(reversePickup.getSaleOrder().getCode());
        reversePickupDTO.setSaleOrderStatus(reversePickup.getSaleOrder().getStatusCode());
        reversePickupDTO.setActionCode(
                CacheManager.getInstance().getCache(ReturnsCache.class).getReversePickupActionByCode(reversePickup.getReversePickupAction()).getDescription());
        reversePickupDTO.setCreated(reversePickup.getCreated());
        reversePickupDTO.setCustomFieldValues(CustomFieldUtils.getCustomFieldValuesDTO(reversePickup));
        if (reversePickup.getShipmentTracking() != null) {
            reversePickupDTO.setShippingProvider(reversePickup.getShipmentTracking().getShippingProvider().getName());
            reversePickupDTO.setProviderStatus(reversePickup.getShipmentTracking().getProviderStatus());
            reversePickupDTO.setTrackingStatus(reversePickup.getShipmentTracking().getStatusCode());
        }

        if (reversePickup.getRedispatchShippingProvider() != null) {
            reversePickupDTO.setRedispatchShippingProvider(reversePickup.getRedispatchShippingProvider().getName());
        }

        reversePickupDTO.setRedispatchManifestCode(reversePickup.getRedispatchManifestCode());
        reversePickupDTO.setRedispatchReason(reversePickup.getRedispatchReason());
        reversePickupDTO.setRedispatchTrackingNumber(reversePickup.getRedispatchTrackingNumber());

        if (reversePickup.getShipmentTracking() != null) {
            reversePickupDTO.setTrackingNumber(reversePickup.getShipmentTracking().getTrackingNumber());
        }

        if (reversePickup.getSaleOrderItems().size() > 0) {
            reversePickupDTO.setReason(reversePickup.getSaleOrderItems().iterator().next().getReversePickupReason());
        }

        for (SaleOrderItem saleOrderItem : reversePickup.getSaleOrderItems()) {
            SaleOrderItemDTO saleOrderItemDTO = new SaleOrderItemDTO(saleOrderItem);
            Item item = saleOrderItem.getItem();
            if (item != null) {
                saleOrderItemDTO.setItemCode(item.getCode());
            }
            reversePickupDTO.getSaleOrderItems().add(saleOrderItemDTO);
        }

        for (ReversePickupAlternate reversePickupAlternate : reversePickup.getReversePickupAlternates()) {
            ItemType itemType = reversePickupAlternate.getItemType();
            ReplacedSaleOrderItemDTO replacedSaleOrderItemDTO = new ReplacedSaleOrderItemDTO();
            replacedSaleOrderItemDTO.setItemSku(itemType.getSkuCode());
            replacedSaleOrderItemDTO.setItemName(itemType.getName());
            replacedSaleOrderItemDTO.setSaleOrderItemCode(reversePickupAlternate.getSaleOrderItem().getCode());
            reversePickupDTO.getReplacedSaleOrderItems().add(replacedSaleOrderItemDTO);
        }
        return reversePickupDTO;

    }

    @Override
    @Transactional
    public ScanReturnShipmentResponse getShipment(ScanReturnShipmentRequest request) {
        ScanReturnShipmentResponse response = new ScanReturnShipmentResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            ShippingPackage shippingPackage = shippingService.getShippingPackageByCode(request.getShipmentCode());
            if (shippingPackage == null) {
                context.addError(WsResponseCode.INVALID_SHIPPING_PACKAGE_CODE, "Invalid Shipping Package Code");
            } else if (!ShippingPackage.StatusCode.RETURN_EXPECTED.name().equals(shippingPackage.getStatusCode())
                    && !ShippingPackage.StatusCode.DISPATCHED.name().equals(shippingPackage.getStatusCode())) {
                context.addError(WsResponseCode.INVALID_STATE, "Invalid Shipping Package State. 'RETURNED' or 'DISPATCHED' expected");
            } else {
                for (SaleOrderItem saleOrderItem : shippingPackage.getSaleOrderItems()) {
                    response.getSaleOrderItems().add(new SaleOrderItemDTO(saleOrderItem));
                }
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    public CreateReturnManifestResponse createReturnManifest(CreateReturnManifestRequest request) {
        CreateReturnManifestResponse response = new CreateReturnManifestResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            ShippingConfiguration configuration = ConfigurationManager.getInstance().getConfiguration(ShippingConfiguration.class);
            ShippingProvider shippingProvider = configuration.getShippingProviderByCode(request.getShippingProviderCode());
            if (shippingProvider == null) {
                context.addError(WsResponseCode.INVALID_SHIPPING_PROVIDER_ID);
            }
            Map<String, Object> customFieldValues = CustomFieldUtils.getCustomFieldValues(context, ReturnManifest.class.getName(), request.getCustomFieldValues());
            if (!context.hasErrors()) {
                User user = new User();
                user.setId(request.getUserId());
                ReturnManifest returnManifest = new ReturnManifest();
                returnManifest.setUser(user);
                returnManifest.setStatusCode(ReturnManifest.StatusCode.CREATED.name());
                returnManifest.setShippingProviderCode(shippingProvider.getCode());
                returnManifest.setShippingProviderName(shippingProvider.getName());
                returnManifest.setCreated(DateUtils.getCurrentTime());
                if (StringUtils.isNotBlank(request.getReturnManifestCode())) {
                    returnManifest.setCode(request.getReturnManifestCode());
                }
                CustomFieldUtils.setCustomFieldValues(returnManifest, customFieldValues);
                returnsDao.addReturnManifest(returnManifest);
                response.setSuccessful(true);
                response.setReturnManifestId(returnManifest.getId());
                response.setReturnManifestCode(returnManifest.getCode());
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    public DiscardReturnManifestResponse discardReturnManifest(DiscardReturnManifestRequest request) {
        DiscardReturnManifestResponse response = new DiscardReturnManifestResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            ReturnManifest returnManifest = returnsDao.getReturnManifestDetailedByCode(request.getReturnManifestCode());
            if (returnManifest == null) {
                context.addError(WsResponseCode.INVALID_RETURN_MANIFEST_ID, "Invalid Return Manifest Id");
            } else if (!ReturnManifest.StatusCode.CREATED.name().equals(returnManifest.getStatusCode())) {
                context.addError(WsResponseCode.INVALID_RETURN_MANIFEST_STATE, "Return manifest not in CREATED state");
            } else if (returnManifest.getReturnManifestItems().size() > 0) {
                context.addError(WsResponseCode.INVALID_RETURN_MANIFEST_STATE, "Shipping packages has already been added to Return Manifest");
            } else if (!returnManifest.getUser().getId().equals(request.getUserId())) {
                context.addError(WsResponseCode.INVALID_USER_ID, "return manifest created by another user");
            } else {
                returnManifest.setStatusCode(ReturnManifest.StatusCode.DISCARDED.name());
                response.setSuccessful(true);
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    @RollbackOnFailure
    @LogActivity
    @Locks({ @Lock(ns = Namespace.RETURN_MANIFEST, key = "#{#args[0].returnManifestCode}") })
    public AddShippingPackageToReturnManifestResponse addShippingPackageToReturnManifest(AddShippingPackageToReturnManifestRequest request) {
        AddShippingPackageToReturnManifestResponse response = new AddShippingPackageToReturnManifestResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            Integer maxAllowedPackagesInManifest = CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class).getMaxNumberOfItemsInManifest();
            ReturnManifest returnManifest = returnsDao.getReturnManifestByCode(request.getReturnManifestCode());
            if (returnManifest == null || !returnManifest.getStatusCode().equals(ReturnManifest.StatusCode.CREATED.name())) {
                context.addError(WsResponseCode.INVALID_RETURN_MANIFEST_ID, "Return Manifest ID is invalid or manifest is closed");
            } else if (returnManifest.getReturnManifestItems().size() >= maxAllowedPackagesInManifest) {
                context.addError(WsResponseCode.SHIPPING_MANIFEST_ITEM_LIMIT_REACHED, "Max package limit in manifest exceeded, limit: " + maxAllowedPackagesInManifest);
            } else {
                ShippingPackage shippingPackage = shippingService.getShippingPackageByCode(request.getAwbOrShipmentCode());
                if (shippingPackage == null) {
                    shippingPackage = shippingService.getShippingPackageWithDetailsByTrackingNumber(request.getAwbOrShipmentCode(), returnManifest.getShippingProviderCode());
                }
                if (shippingPackage == null) {
                    context.addError(WsResponseCode.INVALID_SHIPPING_PACKAGE_CODE, "Invalid Package Code/AWB Number entered, try rescanning the package");
                } else {
                    if (shippingPackage.isThirdPartyShipping()) {
                        Channel channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelById(shippingPackage.getSaleOrder().getChannel().getId());
                        if (!com.uniware.core.entity.Source.Code.CUSTOM.name().equals(channel.getSourceCode())) {
                            context.addError(WsResponseCode.INVALID_SHIPPING_PACKAGE_STATE,
                                    "Packages for which shipping is provided by channel, cannot be added to this manifest.");
                        }
                    }
                    if (!context.hasErrors()) {
                        if (!shippingPackage.getShippingProviderCode().equalsIgnoreCase(returnManifest.getShippingProviderCode())) {
                            context.addError(WsResponseCode.SHIPPING_PROVIDER_MISMATCH_PACKAGE_TO_MANIFEST,
                                    "Only \"" + returnManifest.getShippingProviderName() + "\" package can be added to this manifest");
                        } else {
                            addShippingPackageToReturnManifest(response, context, returnManifest, shippingPackage);
                        }
                    }
                }
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    /**
     * @param response
     * @param context
     * @param returnManifest
     * @param shippingPackage
     */
    @Locks({ @Lock(ns = Namespace.SALE_ORDER, key = "#{#args[3].saleOrder.code}") })
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void addShippingPackageToReturnManifest(AddShippingPackageToReturnManifestResponse response, ValidationContext context, ReturnManifest returnManifest,
            ShippingPackage shippingPackage) {
        shippingPackage = shippingService.getShippingPackageById(shippingPackage.getId(), true);
        ReturnManifestItem returnManifestItem = new ReturnManifestItem(returnManifest, shippingPackage, DateUtils.getCurrentTime(), DateUtils.getCurrentTime());
        ReturnManifestItemDetailDTO returnManifestItemDetailDTO = new ReturnManifestItemDetailDTO();
        prepareReturnManifestItemDetailDTO(returnManifestItemDetailDTO, returnManifestItem);
        response.setManifestItem(returnManifestItemDetailDTO);
        if (!StringUtils.equalsAny(shippingPackage.getStatusCode(), ShippingPackage.StatusCode.DELIVERED.name(), ShippingPackage.StatusCode.DISPATCHED.name(),
                ShippingPackage.StatusCode.RETURN_EXPECTED.name(), ShippingPackage.StatusCode.SHIPPED.name())) {
            context.addError(WsResponseCode.INVALID_PACKAGE_STATE, "Invalid Shipping Package State");
        } else {
            for (SaleOrderItem saleOrderItem : shippingPackage.getSaleOrderItems()) {
                if (saleOrderItem.getReversePickup() != null) {
                    context.addError(WsResponseCode.INVALID_PACKAGE_STATE, "Reverse pickup has already been created for one or more items in this packages");
                    return;
                }
            }
            markShippingPackageReturnAcknowledged(shippingPackage, context,
                    ConfigurationManager.getInstance().getConfiguration(TenantSystemConfiguration.class).getSystemUser().getId());
            returnManifestItem = returnsDao.addReturnManifestItem(returnManifestItem);
            returnManifest.getReturnManifestItems().add(returnManifestItem);
            if (ActivityContext.current().isEnable()) {
                ActivityUtils.appendActivity(shippingPackage.getCode(), ActivityEntityEnum.SHIPPING_PACKAGE.getName(), shippingPackage.getSaleOrder().getCode(),
                        Arrays.asList(
                                new String[] {
                                        "{" + ActivityEntityEnum.SHIPPING_PACKAGE + ":" + shippingPackage.getCode() + "} added to return manifest {"
                                                + ActivityEntityEnum.RETURN_MANIFEST + ":"
                                                + returnManifest.getCode() + "}" + (StringUtils.isNotBlank(returnManifestItem.getReturnReason())
                                                        ? "for return reason :" + returnManifestItem.getReturnReason() : StringUtils.EMPTY_STRING) }),
                        ActivityTypeEnum.RETURN.name());
            }
            response.setSuccessful(true);
            response.setTotalManifestItemCount(returnManifest.getReturnManifestItems().size());
        }
    }

    @Override
    @LogActivity
    public ConfirmShippingPackageReturnToOriginResponse confirmShippingPackageReturn(ConfirmShippingPackageReturnToOriginRequest request) {
        ConfirmShippingPackageReturnToOriginResponse response = new ConfirmShippingPackageReturnToOriginResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            ShippingPackage shippingPackage = shippingService.getShippingPackageByCode(request.getShippingPackageCode());
            if (shippingPackage == null) {
                context.addError(WsResponseCode.INVALID_SHIPPING_PACKAGE_CODE);
            } else if (!StringUtils.equalsAny(shippingPackage.getStatusCode(), ShippingPackage.StatusCode.DELIVERED.name(), ShippingPackage.StatusCode.DISPATCHED.name(),
                    ShippingPackage.StatusCode.RETURN_EXPECTED.name(), ShippingPackage.StatusCode.SHIPPED.name())) {
                context.addError(WsResponseCode.INVALID_PACKAGE_STATE, "Invalid Shipping Package State");
            } else {
                confirmShippingPackageReturnInternal(shippingPackage, request, response, context);
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        } else {
            response.setSuccessful(true);
        }
        return response;
    }

    @Locks({ @Lock(ns = Namespace.SALE_ORDER, key = "#{#args[0].saleOrder.code}") })
    @Transactional
    @RollbackOnFailure
    private ConfirmShippingPackageReturnToOriginResponse confirmShippingPackageReturnInternal(ShippingPackage shippingPackage, ConfirmShippingPackageReturnToOriginRequest request,
            ConfirmShippingPackageReturnToOriginResponse response, ValidationContext context) {
        shippingPackage = shippingService.getShippingPackageByCode(shippingPackage.getCode());
        if (!StringUtils.equalsAny(shippingPackage.getStatusCode(), ShippingPackage.StatusCode.DELIVERED.name(), ShippingPackage.StatusCode.DISPATCHED.name(),
                ShippingPackage.StatusCode.RETURN_EXPECTED.name(), ShippingPackage.StatusCode.SHIPPED.name())) {
            context.addError(WsResponseCode.INVALID_PACKAGE_STATE, "Invalid Shipping Package State");
        } else {
            for (SaleOrderItem saleOrderItem : shippingPackage.getSaleOrderItems()) {
                if (saleOrderItem.getReversePickup() != null) {
                    context.addError(WsResponseCode.INVALID_PACKAGE_STATE, "Reverse pickup has already been created for one or more items in this packages");
                    break;
                }
            }
            if (!context.hasErrors()) {
                markShippingPackageReturnAcknowledged(shippingPackage, context, request.getUserId());
                if (!ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).isInventoryManagementSwitchedOff()) {
                    {
                        // Create a putaway.
                        CreatePutawayRequest createPutawayRequest = new CreatePutawayRequest();
                        createPutawayRequest.setUserId(request.getUserId());
                        createPutawayRequest.setType(Putaway.Type.PUTAWAY_COURIER_RETURNED_ITEMS.name());
                        CreatePutawayResponse createPutawayResponse = putawayService.createPutaway(createPutawayRequest);
                        if (!createPutawayResponse.isSuccessful()) {
                            context.addErrors(createPutawayResponse.getErrors());
                        } else {
                            // Add returns sale order items to putaway.
                            AddReturnsSaleOrderItemsToPutawayRequest addReturnsSaleOrderItemsToPutawayRequest = new AddReturnsSaleOrderItemsToPutawayRequest();
                            addReturnsSaleOrderItemsToPutawayRequest.setPutawayCode(createPutawayResponse.getPutawayDTO().getCode());
                            addReturnsSaleOrderItemsToPutawayRequest.setShipmentCode(shippingPackage.getCode());
                            addReturnsSaleOrderItemsToPutawayRequest.setUserId(request.getUserId());
                            List<AddReturnsSaleOrderItemsToPutawayRequest.SaleOrderItemDTO> saleOrderItems = new ArrayList<>();
                            for (SaleOrderItem saleOrderItem : shippingPackage.getSaleOrderItems()) {
                                saleOrderItems.add(new AddReturnsSaleOrderItemsToPutawayRequest.SaleOrderItemDTO(saleOrderItem.getCode(), Item.StatusCode.GOOD_INVENTORY.name()));
                            }
                            addReturnsSaleOrderItemsToPutawayRequest.setSaleOrderItems(saleOrderItems);
                            AddReturnsSaleOrderItemsToPutawayResponse addReturnsSaleOrderItemsToPutawayResponse = putawayService.addReturnsSaleOrderItemsToPutaway(
                                    addReturnsSaleOrderItemsToPutawayRequest);
                            if (!addReturnsSaleOrderItemsToPutawayResponse.isSuccessful()) {
                                context.addErrors(addReturnsSaleOrderItemsToPutawayResponse.getErrors());
                            }
                            if (!context.hasErrors()) {
                                CreatePutawayListRequest createPutawayListRequest = new CreatePutawayListRequest();
                                createPutawayListRequest.setPutawayCode(createPutawayResponse.getPutawayDTO().getCode());
                                createPutawayListRequest.setUserId(request.getUserId());
                                CreatePutawayListResponse createPutawayListResponse = putawayService.createPutawayList(createPutawayListRequest);
                                if (!createPutawayListResponse.isSuccessful()) {
                                    context.addErrors(createPutawayListResponse.getErrors());
                                }
                            }

                            if (!context.hasErrors()) {
                                CompletePutawayRequest completePutawayRequest = new CompletePutawayRequest();
                                completePutawayRequest.setPutawayCode(createPutawayResponse.getPutawayDTO().getCode());
                                CompletePutawayResponse completePutawayResponse = putawayService.completePutaway(completePutawayRequest);
                                if (!completePutawayResponse.isSuccessful()) {
                                    context.addErrors(completePutawayResponse.getErrors());
                                } else {
                                    if (ActivityContext.current().isEnable()) {
                                        ActivityUtils.appendActivity(shippingPackage.getCode(), ActivityEntityEnum.SHIPPING_PACKAGE.getName(),
                                                shippingPackage.getSaleOrder().getCode(),
                                                Arrays.asList(new String[] { "{" + ActivityEntityEnum.SHIPPING_PACKAGE + ":" + shippingPackage.getCode() + "} marked returned." }),
                                                ActivityTypeEnum.RETURN.name());
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        if (!context.hasErrors()) {
            response.setSuccessful(true);
        } else {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    private void markShippingPackageReturnAcknowledged(ShippingPackage shippingPackage, ValidationContext context, Integer userId) {
        ShippingProvider shippingProvider = ConfigurationManager.getInstance().getConfiguration(ShippingConfiguration.class).getShippingProviderByCode(
                shippingPackage.getShippingProviderCode());
        if (shippingProvider != null) {
            ShipmentTracking shipmentTracking = shipmentTrackingService.getShipmentTracking(shippingPackage.getTrackingNumber(), shippingProvider.getId());
            if (shipmentTracking != null) {
                ShipmentTrackingStatus trackingStatus = ConfigurationManager.getInstance().getConfiguration(ShippingConfiguration.class).getShipmentTrackingStatusByCode(
                        shipmentTracking.getStatusCode());
                if (!trackingStatus.isStopPolling()) {
                    shipmentTracking.setDisableStatusUpdate(true);
                }
            }
        }
        shippingPackage.setStatusCode(ShippingPackage.StatusCode.RETURN_ACKNOWLEDGED.name());
        // prepare credit note for returned shipment
        if (taxService.isGSTEnabled()) {
            CreateShippingPackageReverseInvoiceRequest createReverseInvoiceRequest = new CreateShippingPackageReverseInvoiceRequest();
            createReverseInvoiceRequest.setShippingPackageCode(shippingPackage.getCode());
            createReverseInvoiceRequest.setUserId(userId);
            CreateShippingPackageReverseInvoiceResponse createReverseInvoiceResponse = shippingInvoiceService.createShippingPackageReverseInvoice(createReverseInvoiceRequest);
            if (context.hasErrors()) {
                context.addErrors(createReverseInvoiceResponse.getErrors());
            } else {
                shippingPackage.setReturnInvoice(invoiceService.getInvoiceByCode(createReverseInvoiceResponse.getInvoiceCode()));
            }
        }
        if (!context.hasErrors()) {
            if (ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).isInventoryManagementSwitchedOff()) {
                if (shippingPackage.getReshipment() != null) {
                    completeReshipment(shippingPackage.getReshipment().getId());
                }
                shippingPackage.setStatusCode(ShippingPackage.StatusCode.RETURNED.name());
            }
        }
    }

    @Override
    @Transactional
    @LogActivity
    public CloseReturnManifestResponse closeReturnManifest(CloseReturnManifestRequest request) {
        CloseReturnManifestResponse response = new CloseReturnManifestResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            ReturnManifest returnManifest = returnsDao.getReturnManifestByCode(request.getReturnManifestCode());
            if (returnManifest == null) {
                context.addError(WsResponseCode.INVALID_RETURN_MANIFEST_ID, "Return Manifest ID is invalid");
            } else if (returnManifest.getReturnManifestItems().isEmpty()) {
                context.addError(WsResponseCode.INVALID_RETURN_MANIFEST_STATE, "Return Manifest is empty");
            } else {
                returnManifest.setStatusCode(ReturnManifest.StatusCode.CLOSED.name());
                returnManifest.setClosed(DateUtils.getCurrentTime());
                response.setReturnManifestCode(returnManifest.getCode());
                if (StringUtils.isNotBlank(returnManifest.getUser().getEmail())) {
                    sendReturnManifestClosureEmail(returnManifest);
                }
                if (ActivityContext.current().isEnable()) {
                    for (ReturnManifestItem manifestItem : returnManifest.getReturnManifestItems()) {
                        ActivityUtils.appendActivity(manifestItem.getShippingPackage().getCode(), ActivityEntityEnum.SHIPPING_PACKAGE.getName(),
                                manifestItem.getShippingPackage().getSaleOrder().getCode(),
                                Arrays.asList(new String[] { "{" + ActivityEntityEnum.RETURN_MANIFEST + ":" + returnManifest.getCode() + "} closed." }),
                                ActivityTypeEnum.COMPLETE.name());
                    }
                }
                response.setSuccessful(true);
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    private void sendReturnManifestClosureEmail(ReturnManifest returnManifest) {
        String userEmail = returnManifest.getUser().getEmail();
        Template template = CacheManager.getInstance().getCache(PrintTemplateCache.class).getTemplateByType(PrintTemplateVO.Type.RETURN_MANIFEST_CSV.name());
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("returnManifest", returnManifest);
        String returnManifestText = template.evaluate(params);

        List<String> recipients = new ArrayList<String>();
        recipients.add(userEmail);
        EmailMessage message = new EmailMessage(recipients, EmailTemplateType.RETURN_MANIFEST.name());
        message.addTemplateParam("returnManifest", returnManifest);

        String directory = EnvironmentProperties.getReturnManifestDirectoryPath();
        File file = new File((directory.endsWith("/") ? directory : directory + "/") + returnManifest.getCode() + ".csv");
        FileOutputStream fout = null;
        try {
            fout = new FileOutputStream(file);
            FileUtils.write(returnManifestText.getBytes(), fout);
            message.getAttachments().add(file);
        } catch (Exception e) {
            LOG.error("Error occurred while sending return manifest mail:", e);
        } finally {
            FileUtils.safelyCloseOutputStream(fout);
        }

        emailService.send(message);
    }

    @Override
    @Transactional
    public ReversePickup getReversePickupByCode(String reversePickupCode) {
        return returnsDao.getReversePickupByCode(reversePickupCode);
    }

    @Override
    public ReversePickup updateReversePickup(ReversePickup reversePickup) {
        return returnsDao.updateReversePickup(reversePickup);
    }

    @Override
    @Transactional
    public Reshipment getReshipmentById(Integer reshipmentId) {
        return returnsDao.getReshipmentById(reshipmentId);
    }

    @Override
    @Transactional
    public ReversePickup getReversePickupById(int reversePickupId) {
        return returnsDao.getReversePickupById(reversePickupId);
    }

    @Override
    @Transactional
    public SearchAwaitingShippingPackageResponse searchAwaitingShippingPackages(SearchAwaitingShippingPackageRequest request) {
        SearchAwaitingShippingPackageResponse response = new SearchAwaitingShippingPackageResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            List<ShippingPackage> packages = returnsDao.searchAwaitingShippingPackages(request);
            PackageAwaitingActionDTO packageAwaitingActionDTO;
            for (ShippingPackage spackage : packages) {
                packageAwaitingActionDTO = new PackageAwaitingActionDTO();
                packageAwaitingActionDTO.setShippingPackageId(spackage.getId());
                packageAwaitingActionDTO.setSaleOrderCode(spackage.getSaleOrder().getCode());
                packageAwaitingActionDTO.setShippingPackageCode(spackage.getCode());
                packageAwaitingActionDTO.setShippingAddress(new AddressDTO(spackage.getShippingAddress()));
                packageAwaitingActionDTO.setShippingPackageStatus(spackage.getStatusCode());
                packageAwaitingActionDTO.setTrackingStatus(spackage.getShipmentTrackingStatusCode());
                packageAwaitingActionDTO.setProviderStatus(spackage.getProviderStatusCode());
                packageAwaitingActionDTO.setShippingMethod(spackage.getShippingMethod().getCode());
                packageAwaitingActionDTO.setShippingProvider(spackage.getShippingProviderName());
                packageAwaitingActionDTO.setTrackingNumber(spackage.getTrackingNumber());
                response.getElements().add(packageAwaitingActionDTO);
            }
            if (request.getSearchOptions() != null && request.getSearchOptions().isGetCount()) {
                Long count = returnsDao.getAwaitingShippingPackageCount(request);
                response.setTotalRecords(count);
            }
            response.setSuccessful(true);
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    @LogActivity
    public EditReturnManifestItemResponse editReturnManifestItem(EditReturnManifestItemRequest request) {
        EditReturnManifestItemResponse response = new EditReturnManifestItemResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            ReturnManifest returnManifest = returnsDao.getReturnManifestByCode(request.getReturnManifestCode());
            for (ReturnManifestItem rmi : returnManifest.getReturnManifestItems()) {
                if (rmi.getShippingPackage().getCode().equals(request.getPackageCode())) {
                    rmi.setReturnReason(request.getReturnReason());
                    returnsDao.updateReturnManifestItem(rmi);
                    ReturnManifestItemDetailDTO returnManifestItemDetailDTO = new ReturnManifestItemDetailDTO();
                    prepareReturnManifestItemDetailDTO(returnManifestItemDetailDTO, rmi);
                    response.setReturnManifestItem(returnManifestItemDetailDTO);
                    if (ActivityContext.current().isEnable()) {
                        ActivityUtils.appendActivity(rmi.getShippingPackage().getCode(), ActivityEntityEnum.SHIPPING_PACKAGE.getName(),
                                rmi.getShippingPackage().getSaleOrder().getCode(),
                                Arrays.asList(new String[] {
                                        "{" + ActivityEntityEnum.SHIPPING_PACKAGE + ":" + rmi.getShippingPackage().getCode() + "} shipment return reason changed "
                                                + rmi.getReturnReason() }),
                                ActivityTypeEnum.RETURN.name());
                    }
                    break;
                }
            }
            response.setSuccessful(true);
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    public ReturnManifest getReturnManifestById(int returnManifestId) {
        return returnsDao.getReturnManifestById(returnManifestId);
    }

    @Override
    @Transactional
    public List<ReturnManifestStatus> getReturnManifestStatuses() {
        return returnsDao.getReturnManifestStatuses();
    }

    @Override
    @Transactional
    public List<ReshipmentAction> getReshipmentActions() {
        return returnsDao.getReshipmentActions();
    }

    @Override
    @Transactional
    public List<ReversePickupAction> getReversePickupActions() {
        return returnsDao.getReversePickupActions();
    }

    @Override
    @Transactional
    public ReturnManifest getReturnManifestByCode(String returnManifestCode) {
        ReturnManifest returnManifest = returnsDao.getReturnManifestDetailedByCode(returnManifestCode);
        for (ReturnManifestItem returnManifestItem : returnManifest.getReturnManifestItems()) {
            Hibernate.initialize(returnManifestItem.getShippingPackage().getSaleOrder().getCode());
        }
        return returnManifest;
    }

    @Override
    @Transactional
    @LogActivity
    public RedispatchReversePickupResponse redispatchReversePickup(RedispatchReversePickupRequest request) {
        RedispatchReversePickupResponse response = new RedispatchReversePickupResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            ReversePickup reversePickup = returnsDao.getReversePickupByCode(request.getReversePickupCode(), true);
            ShippingConfiguration shippingConfiguration = ConfigurationManager.getInstance().getConfiguration(ShippingConfiguration.class);
            ShippingProvider shippingProvider = shippingConfiguration.getShippingProviderByCode(request.getShippingProviderCode());
            if (reversePickup == null) {
                context.addError(WsResponseCode.INVALID_REVERSE_PICKUP_CODE, "Invalid Reverse Pickup Code:" + request.getReversePickupCode());
            } else if (!ReversePickup.StatusCode.REDISPATCH_PENDING.name().equals(reversePickup.getStatusCode())) {
                context.addError(WsResponseCode.INVALID_REVERSE_PICKUP_CODE, "Invalid Reverse Pickup Code:" + request.getReversePickupCode());
            } else if (shippingProvider == null) {
                context.addError(WsResponseCode.INVALID_SHIPPING_PROVIDER_CODE, "Invalid Shipping provider Code:" + request.getShippingProviderCode());
            } else {
                reversePickup.setRedispatchShippingProvider(shippingProvider);
                SaleOrderItem saleOrderItem = reversePickup.getSaleOrderItems().iterator().next();
                ShippingMethod shippingMethod = shippingConfiguration.getShippingMethod(saleOrderItem.getShippingMethod().getName());
                try {
                    List<ShippingProvider> availableProviders = shippingProviderService.getAvailableShippingProviders(shippingMethod.getId(),
                            saleOrderItem.getShippingAddress().getPincode(), null);
                    boolean providerApplicable = false;
                    for (ShippingProvider availableProvider : availableProviders) {
                        if (availableProvider.getId().equals(shippingProvider.getId())) {
                            providerApplicable = true;
                            break;
                        }
                    }
                    if (providerApplicable) {
                        ShippingProviderMethod shippingProviderMethod = ConfigurationManager.getInstance().getConfiguration(
                                ShippingFacilityLevelConfiguration.class).getShippingProviderMethod(shippingProvider.getCode(), shippingMethod.getCode(),
                                        shippingMethod.isCashOnDelivery());
                        String trackingNumber = null;
                        if (TrackingNumberGeneration.MANUAL.name().equals(shippingProviderMethod.getTrackingNumberGeneration())) {
                            if (StringUtils.isNotBlank(request.getTrackingNumber())) {
                                trackingNumber = request.getTrackingNumber().trim();
                            } else {
                                context.addError(WsResponseCode.MANUAL_TRACKING_NUMBER_NOT_ASSIGNED, "tracking number is mandatory in case of manual tracking generation");
                            }
                        } else {
                            Set<SaleOrderItem> itemsToRedispatch = new HashSet<SaleOrderItem>();
                            for (SaleOrderItem soi : reversePickup.getSaleOrderItems()) {
                                if (!StringUtils.equalsAny(soi.getStatusCode(), SaleOrderItem.StatusCode.CANCELLED.name(), SaleOrderItem.StatusCode.REPLACED.name())) {
                                    itemsToRedispatch.add(soi);
                                }
                            }
                            try {
                                trackingNumber = shippingProviderService.generateTrackingNumber(prepareVirtualShippingPackage(reversePickup, itemsToRedispatch, shippingMethod));
                            } catch (NoAvailableTrackingNumberException e) {
                                context.addError(WsResponseCode.NO_TRACKING_NUMBER_AVAILABLE_FOR_GIVEN_PROVIDER, "no tracking number available for given provider");
                            }
                        }
                        if (!context.hasErrors()) {
                            reversePickup.setRedispatchShippingProvider(shippingProvider);
                            reversePickup.setRedispatchTrackingNumber(trackingNumber);
                            reversePickup.setRedispatchReason(request.getRedispatchReason());
                            reversePickup.setRedispatchManifestCode(request.getRedispatchManifestCode());
                            reversePickup.setStatusCode(ReversePickup.StatusCode.REDISPATCHED.name());
                            if (ActivityContext.current().isEnable()) {
                                if (reversePickup.getReplacementSaleOrder() != null) {
                                    ActivityUtils.appendActivity(reversePickup.getReplacementSaleOrder().getCode(), ActivityEntityEnum.SALE_ORDER.getName(),
                                            reversePickup.getSaleOrder().getCode(),
                                            Arrays.asList(new String[] {
                                                    "Reverse pick up {" + ActivityEntityEnum.REVERSE_PICKUP + ":" + reversePickup.getCode() + "} with replacement sale order {"
                                                            + ActivityEntityEnum.REPLACEMENT_ORDER + ":" + reversePickup.getReplacementSaleOrder().getCode() + "} for sale order {"
                                                            + ActivityEntityEnum.SALE_ORDER + ":" + reversePickup.getSaleOrder().getCode() + "} redispatched with ["
                                                            + reversePickup.getRedispatchShippingProvider().getCode() + "] with AWB " + reversePickup.getRedispatchTrackingNumber()
                                                            + " for reason <" + reversePickup.getRedispatchReason() + ">. Added to manifest {" + ActivityEntityEnum.MANIFEST + ":"
                                                            + reversePickup.getRedispatchManifestCode() + "}" }),
                                            ActivityTypeEnum.REPLACEMENT.name());
                                } else {
                                    ActivityUtils.appendActivity(reversePickup.getSaleOrder().getCode(), ActivityEntityEnum.SALE_ORDER.getName(), null,
                                            Arrays.asList(new String[] {
                                                    "Reverse pick up {" + ActivityEntityEnum.REVERSE_PICKUP + ":" + reversePickup.getCode() + "} for sale order {"
                                                            + ActivityEntityEnum.SALE_ORDER + ":" + reversePickup.getSaleOrder().getCode() + "} redispatched with ["
                                                            + reversePickup.getRedispatchShippingProvider().getCode() + "] with AWB " + reversePickup.getRedispatchTrackingNumber()
                                                            + " for reason <" + reversePickup.getRedispatchReason() + ">. Added to manifest {" + ActivityEntityEnum.MANIFEST + ":"
                                                            + reversePickup.getRedispatchManifestCode() + "}" }),
                                            ActivityTypeEnum.RETURN.name());
                                }
                            }
                            response.setSuccessful(true);
                        }

                    } else {
                        context.addError(WsResponseCode.LOCATION_NOT_SERVICEABLE_WITH_GIVEN_PROVIDER_METHOD, "location not serviceable for for given shipping provider");
                    }

                } catch (NoAvailableShippingProviderException e) {
                    context.addError(WsResponseCode.LOCATION_NOT_SERVICEABLE_WITH_GIVEN_PROVIDER_METHOD, "location not serviceable for for given shipping provider");
                }
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    private ShippingPackage prepareVirtualShippingPackage(ReversePickup reversePickup, Set<SaleOrderItem> itemsToRedispatch, ShippingMethod shippingMethod) {
        SaleOrderItem saleOrderItem = itemsToRedispatch.iterator().next();
        ShippingPackage shippingPackage = new ShippingPackage();
        shippingPackage.setSaleOrder(reversePickup.getSaleOrder());
        shippingPackage.getSaleOrderItems().addAll(itemsToRedispatch);
        shippingPackage.setShippingAddress(saleOrderItem.getShippingAddress());
        shippingPackage.setShippingProviderCode(reversePickup.getRedispatchShippingProvider().getCode());
        shippingPackage.setShippingProviderName(reversePickup.getRedispatchShippingProvider().getName());
        shippingPackage.setShippingMethod(shippingMethod);
        return shippingPackage;
    }

    @Override
    @Transactional(readOnly = true)
    public String getRedispatchHtml(String reversePickupCode, Template template) {
        ReversePickup reversePickup = getReversePickupByCode(reversePickupCode);
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("reversePickup", reversePickup);
        params.put("saleOrder", reversePickup.getSaleOrder());
        return template.evaluate(params);
    }

    @Override
    @Transactional
    public EditReversePickupMetadataResponse editReversePickupMetadata(EditReversePickupMetadataRequest request) {
        EditReversePickupMetadataResponse response = new EditReversePickupMetadataResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            ReversePickup reversePickup = returnsDao.getReversePickupByCode(request.getReversePickupCode(), true);
            if (reversePickup == null) {
                context.addError(WsResponseCode.INVALID_REVERSE_PICKUP_CODE, "Invalid Reverse Pickup Code:" + request.getReversePickupCode());
            } else {
                if (request.getCustomFieldValues() != null) {
                    CustomFieldUtils.setCustomFieldValues(reversePickup,
                            CustomFieldUtils.getCustomFieldValues(context, ReversePickup.class.getName(), request.getCustomFieldValues()), false);
                }
                response.setSuccessful(true);
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Locks({ @Lock(ns = Namespace.SALE_ORDER, key = "#{#args[0].saleOrderCode}") })
    @Transactional
    @RollbackOnFailure
    @LogActivity
    public MarkSaleOrderReturnedResponse markSaleOrderReturned(MarkSaleOrderReturnedRequest request) {
        MarkSaleOrderReturnedResponse response = new MarkSaleOrderReturnedResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            SaleOrder saleOrder = saleOrderService.getSaleOrderForUpdate(request.getSaleOrderCode());
            if (saleOrder == null) {
                context.addError(WsResponseCode.INVALID_SALE_ORDER_CODE);
            }
        }

        CreateReversePickupResponse createReversePickupResponse = null;
        if (!context.hasErrors()) {
            CreateReversePickupRequest createReversePickupRequest = new CreateReversePickupRequest();
            createReversePickupRequest.setSaleOrderCode(request.getSaleOrderCode());
            createReversePickupRequest.setActionCode(Action.WAIT_FOR_RETURN_AND_CANCEL.code());
            if (StringUtils.isNotBlank(request.getReturnCode())) {
                createReversePickupRequest.setReversePickupCode(request.getReturnCode());
            }
            List<WsReversePickupItem> reversePickupItems = new ArrayList<>(request.getSaleOrderItems().size());
            for (MarkSaleOrderReturnedRequest.WsSaleOrderItem saleOrderItem : request.getSaleOrderItems()) {
                WsReversePickupItem reversePickupItem = new WsReversePickupItem(saleOrderItem.getCode(), request.getReturnReason());
                reversePickupItems.add(reversePickupItem);
            }
            createReversePickupRequest.setReversePickItems(reversePickupItems);
            createReversePickupResponse = createReversePickup(createReversePickupRequest);
            if (!createReversePickupResponse.isSuccessful()) {
                context.addErrors(createReversePickupResponse.getErrors());
            }
        }

        if (!context.hasErrors()) {
            CompleteReversePickupRequest completeReversePickupRequest = new CompleteReversePickupRequest();
            completeReversePickupRequest.setReversePickupCode(createReversePickupResponse.getReversePickupCode());
            completeReversePickupRequest.setUserId(request.getUserId());
            completeReversePickupRequest.setSaleOrderItems(request.getSaleOrderItems());
            response.setResponse(completeReversePickup(completeReversePickupRequest));
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    @Override
    public CompleteReversePickupResponse completeReversePickup(CompleteReversePickupRequest request) {
        CompleteReversePickupResponse response = new CompleteReversePickupResponse();
        ValidationContext context = request.validate();
        ReversePickup reversePickup;
        if (!context.hasErrors()) {
            reversePickup = getReversePickupByCode(request.getReversePickupCode());
            if (reversePickup == null) {
                context.addError(WsResponseCode.INVALID_REVERSE_PICKUP_CODE);
            } else {
                response = doCompleteReversePickup(reversePickup, request, response, context);
            }
        }
        response.setSuccessful(!context.hasErrors());
        response.setErrors(context.getErrors());
        return response;
    }

    @Transactional
    @RollbackOnFailure
    @LogActivity
    @Locks({ @Lock(ns = Namespace.SALE_ORDER, key = "#{#args[0].saleOrder.code}") })
    private CompleteReversePickupResponse doCompleteReversePickup(ReversePickup reversePickup, CompleteReversePickupRequest request, CompleteReversePickupResponse response,
            ValidationContext context) {
        //fetch again as previous fetch in another transaction
        reversePickup = getReversePickupByCode(reversePickup.getCode());
        if (!ReversePickup.StatusCode.CREATED.name().equals(reversePickup.getStatusCode())) {
            context.addError(WsResponseCode.INVALID_REVERSE_PICKUP_STATE, "Reverse pickup should be in created state.");
            response.setSuccessful(!context.hasErrors());
            response.addErrors(context.getErrors());
            return response;
        }
        if (ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).isInventoryManagementSwitchedOff()) {
            Map<String, List<SaleOrderItem>> typeToSaleOrderItemDetails = new HashMap<>();
            Map<String, SaleOrderItem> saleOrderItemsMap = new HashMap<>();
            for (SaleOrderItem saleOrderItem : reversePickup.getSaleOrderItems()) {
                saleOrderItem.setStatusCode(SaleOrderItem.StatusCode.CANCELLED.name());
                saleOrderItem.setCancellationTime(DateUtils.getCurrentTime());
                MarkSaleOrderReturnedRequest.WsSaleOrderItem wsSaleOrderItem=MarkSaleOrderReturnedRequest.getSaleOrderItemByCodeFromList(request.getSaleOrderItems(),saleOrderItem.getCode());
                if(wsSaleOrderItem.getReason() !=null && !wsSaleOrderItem.getReason().isEmpty()){
                    saleOrderItem.setReversePickupReason(wsSaleOrderItem.getReason());
                }
                saleOrderItemsMap.put(saleOrderItem.getCode(), saleOrderItem);
            }
            for (MarkSaleOrderReturnedRequest.WsSaleOrderItem wsSaleOrderItem : request.getSaleOrderItems()) {
                if (!WsSaleOrderItem.StatusCode.RETURN_TO_CUSTOMER.name().equals(wsSaleOrderItem.getStatus())) {
                    List<SaleOrderItem> itemsToUpdateOnChannel = typeToSaleOrderItemDetails.get(wsSaleOrderItem.getStatus());
                    if (itemsToUpdateOnChannel == null) {
                        itemsToUpdateOnChannel = new ArrayList<>();
                        typeToSaleOrderItemDetails.put(wsSaleOrderItem.getStatus(), itemsToUpdateOnChannel);
                    }
                    itemsToUpdateOnChannel.add(saleOrderItemsMap.get(wsSaleOrderItem.getCode()));
                }
            }
            if (!context.hasErrors() && typeToSaleOrderItemDetails.size() > 0) {
                if (StringUtils.isNotBlank(CacheManager.getInstance().getCache(ChannelCache.class).getScriptName(reversePickup.getSaleOrder().getChannel().getCode(),
                        Source.MARK_SALE_ORDER_RETURNED_SCRIPT_NAME))) {
                    ScraperScript markSaleOrderReturnedScript = CacheManager.getInstance().getCache(ChannelCache.class).getScriptByName(
                            reversePickup.getSaleOrder().getChannel().getCode(), Source.MARK_SALE_ORDER_RETURNED_SCRIPT_NAME);
                    if (markSaleOrderReturnedScript != null) {
                        putawayService.markSaleOrderReturnedOnChannel(context, reversePickup.getSaleOrder().getChannel(), typeToSaleOrderItemDetails, markSaleOrderReturnedScript);
                    }
                }
            }
            if (!context.hasErrors() && taxService.isGSTEnabled()) {
                // prepare credit note for returned shipment
                Set<String> saleOrderItemCodes = reversePickup.getSaleOrderItems().stream().map(saleOrderItem -> saleOrderItem.getCode()).collect(Collectors.toSet());
                CreateShippingPackageReverseInvoiceRequest createReverseInvoiceRequest = new CreateShippingPackageReverseInvoiceRequest();
                createReverseInvoiceRequest.setSaleOrderCode(reversePickup.getSaleOrder().getCode());
                createReverseInvoiceRequest.setSaleOrderItemCodes(saleOrderItemCodes);
                createReverseInvoiceRequest.setUserId(request.getUserId());
                CreateShippingPackageReverseInvoiceResponse createReverseInvoiceResponse = shippingInvoiceService.createShippingPackageReverseInvoice(createReverseInvoiceRequest);
                if (!createReverseInvoiceResponse.isSuccessful()) {
                    context.addErrors(createReverseInvoiceResponse.getErrors());
                } else {
                    reversePickup.setReturnInvoice(invoiceService.getInvoiceByCode(createReverseInvoiceResponse.getInvoiceCode()));
                }
            }
            if (!context.hasErrors()) {
                reversePickup.setStatusCode(ReversePickup.StatusCode.COMPLETE.name());
                if (ActivityContext.current().isEnable()) {
                    ActivityUtils.appendActivity(reversePickup.getSaleOrder().getCode(), ActivityEntityEnum.SALE_ORDER.getName(), null,
                            Arrays.asList(new String[] { "Reverse pick up {" + ActivityEntityEnum.REVERSE_PICKUP + ":" + request.getReversePickupCode() + "} COMPLETED" }),
                            ActivityTypeEnum.COMPLETE.name());
                }
            }
        } else {
            // Reverse pickup generated successfully. Create a putaway.
            CreatePutawayRequest createPutawayRequest = new CreatePutawayRequest();
            createPutawayRequest.setUserId(request.getUserId());
            createPutawayRequest.setType(Putaway.Type.PUTAWAY_REVERSE_PICKUP_ITEM.name());
            CreatePutawayResponse createPutawayResponse = putawayService.createPutaway(createPutawayRequest);
            if (!createPutawayResponse.isSuccessful()) {
                context.addErrors(createPutawayResponse.getErrors());
            } else {
                // Add reverse pickup to putaway.
                AddReversePickupSaleOrderItemsToPutawayRequest addReversePickupSaleOrderItemsToPutawayRequest = new AddReversePickupSaleOrderItemsToPutawayRequest();
                addReversePickupSaleOrderItemsToPutawayRequest.setPutawayCode(createPutawayResponse.getPutawayDTO().getCode());
                addReversePickupSaleOrderItemsToPutawayRequest.setReversePickupCode(request.getReversePickupCode());
                addReversePickupSaleOrderItemsToPutawayRequest.setUserId(request.getUserId());
                List<WsSaleOrderItem> saleOrderItems = new ArrayList<WsSaleOrderItem>();
                for (com.uniware.core.api.returns.MarkSaleOrderReturnedRequest.WsSaleOrderItem saleOrderItem : request.getSaleOrderItems()) {
                    WsSaleOrderItem wsSaleOrderItem=  new WsSaleOrderItem(saleOrderItem.getCode(), saleOrderItem.getStatus());
                    wsSaleOrderItem.setReturnReason(saleOrderItem.getReason());
                    wsSaleOrderItem.setShelfCode(saleOrderItem.getShelfCode());
                    saleOrderItems.add(wsSaleOrderItem);
                }
                addReversePickupSaleOrderItemsToPutawayRequest.setSaleOrderItems(saleOrderItems);
                AddReversePickupSaleOrderItemsToPutawayResponse addReversePickupSaleOrderItemsToPutawayResponse = putawayService.addReversePickupSaleOrderItemsToPutaway(
                        addReversePickupSaleOrderItemsToPutawayRequest);
                if (!addReversePickupSaleOrderItemsToPutawayResponse.isSuccessful()) {
                    context.addErrors(addReversePickupSaleOrderItemsToPutawayResponse.getErrors());
                } else {
                    if (ActivityContext.current().isEnable()) {
                        ActivityUtils.appendActivity(reversePickup.getSaleOrder().getCode(), ActivityEntityEnum.SALE_ORDER.getName(), null,
                                Arrays.asList(new String[] {
                                        "Reverse pick up {" + ActivityEntityEnum.REVERSE_PICKUP + ":" + request.getReversePickupCode() + "} for sale order {"
                                                + ActivityEntityEnum.SALE_ORDER + ":" + reversePickup.getSaleOrder().getCode() + "} with items {"
                                                + ActivityEntityEnum.SALE_ORDER_ITEM + ":" + request.getSaleOrderItems() + "} added to putaway {" + ActivityEntityEnum.PUTWAWAY
                                                + ":" + createPutawayResponse.getPutawayDTO().getCode() + "}" }),
                                ActivityTypeEnum.RETURN.name());
                    }
                }
                if (!context.hasErrors()) {
                    CreatePutawayListRequest createPutawayListRequest = new CreatePutawayListRequest();
                    createPutawayListRequest.setPutawayCode(createPutawayResponse.getPutawayDTO().getCode());
                    createPutawayListRequest.setUserId(request.getUserId());
                    CreatePutawayListResponse createPutawayListResponse = putawayService.createPutawayList(createPutawayListRequest);
                    if (!createPutawayListResponse.isSuccessful()) {
                        context.addErrors(createPutawayListResponse.getErrors());
                    }
                }

                if ((!context.hasErrors()) && (ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).isWarehouseStorageBinManagement())) {
                    CompletePutawayRequest completePutawayRequest = new CompletePutawayRequest();
                    completePutawayRequest.setPutawayCode(createPutawayResponse.getPutawayDTO().getCode());
                    CompletePutawayResponse completePutawayResponse = putawayService.completePutaway(completePutawayRequest);
                    if (!completePutawayResponse.isSuccessful()) {
                        context.addErrors(completePutawayResponse.getErrors());
                    } else {
                        if (ActivityContext.current().isEnable()) {
                            ActivityUtils.appendActivity(reversePickup.getSaleOrder().getCode(), ActivityEntityEnum.SALE_ORDER.getName(), null,
                                    Arrays.asList(new String[] {
                                            "Reverse pick up {" + ActivityEntityEnum.REVERSE_PICKUP + ":" + request.getReversePickupCode() + "} putaway {"
                                                    + ActivityEntityEnum.PUTWAWAY + ":" + createPutawayResponse.getPutawayDTO().getCode() + "} COMPLETED" }),
                                    ActivityTypeEnum.COMPLETE.name());
                        }
                    }
                }
            }
        }
        response.setSuccessful(!context.hasErrors());
        response.addErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    public GetReturnManifestSummaryResponse getReturnManifestSummary(GetReturnManifestSummaryRequest request) {
        GetReturnManifestSummaryResponse response = new GetReturnManifestSummaryResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            ReturnManifest returnManifest = returnsDao.getReturnManifestByCode(request.getCode());
            if (returnManifest == null) {
                context.addError(WsResponseCode.INVALID_RETURN_MANIFEST_ID);
            } else {
                response.setSuccessful(true);
                response.setSummary(prepareReturnManifestSummary(returnManifest));
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    private ReturnManifestSummaryDTO prepareReturnManifestSummary(ReturnManifest returnManifest) {
        ReturnManifestSummaryDTO returnManifestSummary = new ReturnManifestSummaryDTO();
        returnManifestSummary.setCode(returnManifest.getCode());
        returnManifestSummary.setCreated(returnManifest.getCreated());
        returnManifestSummary.setStatusCode(returnManifest.getStatusCode());
        returnManifestSummary.setUsername(returnManifest.getUser().getUsername());
        returnManifestSummary.setShippingProvider(returnManifest.getShippingProviderName());
        returnManifestSummary.setShippingProviderCode(returnManifest.getShippingProviderCode());
        returnManifestSummary.setCustomFieldValues(CustomFieldUtils.getCustomFieldValuesDTO(returnManifest));
        returnManifestSummary.setClosed(returnManifest.getClosed());
        return returnManifestSummary;
    }

    @Override
    @Transactional
    public GetReturnManifestItemDetailsResponse getReturnManifestItemDetails(GetReturnManifestItemDetailsRequest request) {
        GetReturnManifestItemDetailsResponse response = new GetReturnManifestItemDetailsResponse();
        ValidationContext context = request.validate();
        ReturnManifest returnManifest = null;
        ShippingPackage shippingPackage = null;
        if (!context.hasErrors()) {
            returnManifest = returnsDao.getReturnManifestByCode(request.getReturnManifestCode());
            if (returnManifest == null) {
                context.addError(WsResponseCode.INVALID_RETURN_MANIFEST_ID);
            } else {
                for (ReturnManifestItem returnManifestItem : returnManifest.getReturnManifestItems()) {
                    if (returnManifestItem.getShippingPackage().getCode().equals(request.getShippingPackageCode())) {
                        shippingPackage = returnManifestItem.getShippingPackage();
                        ReturnManifestItemDetailDTO returnManifestItemDetailDTO = new ReturnManifestItemDetailDTO();
                        prepareReturnManifestItemDetailDTO(returnManifestItemDetailDTO, returnManifestItem);
                        response.setReturnManifestItemDetail(returnManifestItemDetailDTO);
                    }
                }
            }
        }
        if (shippingPackage == null) {
            context.addError(WsResponseCode.INVALID_SHIPPING_PACKAGE_CODE, "Shipping package is invalid or does not belong to manifest : " + returnManifest.getCode());
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        response.setSuccessful(!context.hasErrors());
        return response;
    }

    private void prepareReturnManifestItemDetailDTO(ReturnManifestItemDetailDTO returnManifestItemDetailDTO, ReturnManifestItem returnManifestItem) {
        ShippingPackage sp = returnManifestItem.getShippingPackage();
        returnManifestItemDetailDTO.setShippingPackageCode(sp.getCode());
        returnManifestItemDetailDTO.setShippingProviderCode(sp.getShippingProviderCode());
        returnManifestItemDetailDTO.setShippingMethodCode(sp.getShippingMethod() != null ? sp.getShippingMethod().getShippingMethodCode() : null);
        returnManifestItemDetailDTO.setTrackingNumber(sp.getTrackingNumber());
        returnManifestItemDetailDTO.setShippingAddress(new AddressDTO(sp.getShippingAddress()));
        returnManifestItemDetailDTO.setChannel(sp.getSaleOrder().getChannel().getName());
        returnManifestItemDetailDTO.setActualWeight(new BigDecimal(sp.getActualWeight()).divide(new BigDecimal(1000.0), 3, RoundingMode.HALF_EVEN));
        returnManifestItemDetailDTO.setUpdated(sp.getUpdated());
        returnManifestItemDetailDTO.setTotalPrice(sp.getTotalPrice());
        returnManifestItemDetailDTO.setNoOfItems(sp.getNoOfItems());
        returnManifestItemDetailDTO.setNoOfBoxes(sp.getNoOfBoxes());
        returnManifestItemDetailDTO.setStatusCode(sp.getStatusCode());
        returnManifestItemDetailDTO.setCashOnDelivery(sp.getShippingMethod().isCashOnDelivery());
        returnManifestItemDetailDTO.setReturnReason(returnManifestItem.getReturnReason());
        Map<String, List<SaleOrderItem>> lineItemToSaleOrderItems = new HashMap<>();
        for (SaleOrderItem soi : sp.getSaleOrderItems()) {
            String lineItemIdentifier = sp.getSaleOrder().isProductManagementSwitchedOff() ? soi.getChannelProductId() : soi.getItemType().getSkuCode();
            List<SaleOrderItem> saleOrderItems = lineItemToSaleOrderItems.get(lineItemIdentifier);
            if (saleOrderItems == null) {
                saleOrderItems = new ArrayList<>();
                lineItemToSaleOrderItems.put(lineItemIdentifier, saleOrderItems);
            }
            saleOrderItems.add(soi);
        }

        List<ReturnManifestLineItem> returnManifestLineItems = new ArrayList<>(lineItemToSaleOrderItems.size());
        for (Entry<String, List<SaleOrderItem>> lineItem : lineItemToSaleOrderItems.entrySet()) {
            ReturnManifestLineItem returnManifestLineItem = new ReturnManifestLineItem();
            SaleOrderItem soi = lineItem.getValue().iterator().next();
            returnManifestLineItem.setLineItemIdentifier(lineItem.getKey());
            returnManifestLineItem.setItemName(sp.getSaleOrder().isProductManagementSwitchedOff() ? soi.getChannelProductName() : soi.getItemType().getName());
            returnManifestLineItem.setSellerSkuCode(soi.getChannelSkuCode());
            returnManifestLineItem.setQuantity(lineItem.getValue().size());
            returnManifestLineItems.add(returnManifestLineItem);
        }
        returnManifestItemDetailDTO.setReturnManifestLineItems(returnManifestLineItems);
    }

    @Override
    public RemoveReturnManifestItemResponse removeReturnManifestItem(RemoveReturnManifestItemRequest request) {
        ReadWriteLock readWriteLock = lockingService.getReadWriteLock(Namespace.RETURN_MANIFEST, request.getReturnManifestCode(), Level.FACILITY);
        try {
            readWriteLock.writeLock().lock();
            return removeReturnManifestItemInternal(request);
        } finally {
            readWriteLock.writeLock().unlock();
        }
    }

    private RemoveReturnManifestItemResponse removeReturnManifestItemInternal(RemoveReturnManifestItemRequest request) {
        RemoveReturnManifestItemResponse response = new RemoveReturnManifestItemResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            ReturnManifest returnManifest = getReturnManifestByCode(request.getReturnManifestCode());
            if (returnManifest == null) {
                context.addError(WsResponseCode.INVALID_RETURN_MANIFEST_CODE, "Return Manifest Code is invalid");
            } else if (!ReturnManifest.StatusCode.CREATED.name().equals(returnManifest.getStatusCode())) {
                context.addError(WsResponseCode.INVALID_RETURN_MANIFEST_STATE, "Return Manifest not in CREATED state");
            } else {
                ReturnManifestItem returnManifestItem = null;
                for (ReturnManifestItem item : returnManifest.getReturnManifestItems()) {
                    if (item.getShippingPackage().getCode().equals(request.getShippingPackageCode())) {
                        returnManifestItem = item;
                    }
                }
                if (returnManifestItem == null) {
                    context.addError(WsResponseCode.INVALID_SHIPPING_PACKAGE_CODE, "Return manifest " + request.getReturnManifestCode() + " doesn't contain the requested item");
                } else {
                    doRemoveShippingManifestItemInternal(returnManifestItem);
                    response.setSuccessful(true);
                }
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    @Locks({ @Lock(ns = Namespace.SALE_ORDER, key = "#{#args[0].shippingPackage.saleOrder.code}") })
    @Transactional
    private void doRemoveShippingManifestItemInternal(ReturnManifestItem returnManifestItem) {
        ShippingPackage shippingPackage = shippingDao.getShippingPackageByCode(returnManifestItem.getShippingPackage().getCode());
        shippingPackage.setStatusCode(returnManifestItem.getLastShippingPackageStatusCode());
        returnsDao.removeReturnManifestItem(returnManifestItem);
    }

    @Override
    public GetReturnManifestShipmentsSummaryResponse getReturnManifestShipmentsSummary(GetReturnManifestShipmentsSummaryRequest request) {
        GetReturnManifestShipmentsSummaryResponse response = new GetReturnManifestShipmentsSummaryResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            ReturnManifest returnManifest = getReturnManifestByCode(request.getReturnManifestCode());
            if (returnManifest == null) {
                context.addError(WsResponseCode.INVALID_RETURN_MANIFEST_CODE, "Invalid return manifest code");
            } else {
                response.setTotalShipments(returnManifest.getReturnManifestItems().size());
                response.setSuccessful(true);
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    @Override
    public GetEligibleShippingProvidersForReturnManifestResponse getShippingProvidersForReturnManifest(GetEligibleShippingProvidersForReturnManifestRequest request) {
        GetEligibleShippingProvidersForReturnManifestResponse response = new GetEligibleShippingProvidersForReturnManifestResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            List<ShippingProvider> spList = ConfigurationManager.getInstance().getConfiguration(ShippingConfiguration.class).getAllShippingProviders();
            List<ShippingProviderSearchDTO> dtoList = new ArrayList<>(spList.size());
            for (ShippingProvider sp : spList) {
                dtoList.add(new ShippingProviderSearchDTO(sp.getCode(), sp.getName()));
            }
            response.setShippingProviders(dtoList);
        }
        response.addErrors(context.getErrors());
        response.setSuccessful(!context.hasErrors());
        return response;
    }

    @Override
    public GetReversePickupInvoiceResponse getReversePickupInvoicesDetails(GetReversePickupInvoiceRequest request) {
        GetReversePickupInvoiceResponse response = new GetReversePickupInvoiceResponse();
        ValidationContext context = request.validate();
        InvoiceDTO returnInvoiceDTO = null;
        if (!context.hasErrors()) {
            ReversePickup reversePickup = getReversePickupByCode(request.getReversePickupCode());
            if (reversePickup == null) {
                context.addError(WsResponseCode.INVALID_REVERSE_PICKUP_CODE);
            } else {
                Invoice returnInvoice = reversePickup.getReturnInvoice();
                if (returnInvoice != null) {
                    returnInvoiceDTO = prepareReversePickupInvoiceDTO(returnInvoice, reversePickup);
                } else {
                    context.addError(WsResponseCode.INVOICE_NOT_GENERATED);
                }
            }
        }
        if (!context.hasErrors()) {
            response.setInvoice(returnInvoiceDTO);
            response.setSuccessful(true);
        }
        return response;
    }

    @Override
    @Transactional(readOnly = true)
    public ReversePickup getReversePickupByReturnInvoiceId(Integer invoiceId) {
        ReversePickup reversePickup = returnsDao.getReversePickupByReturnInvoice(invoiceId);
        Hibernate.initialize(reversePickup.getShippingProvider());
        Hibernate.initialize(reversePickup.getShippingAddress());
        Hibernate.initialize(reversePickup.getShipmentTracking());
        Hibernate.initialize(reversePickup.getReturnInvoice());
        for (SaleOrderItem saleOrderItem : reversePickup.getSaleOrderItems()) {
            Hibernate.initialize(saleOrderItem.getItemType());
        }
        return reversePickup;

    }

    private InvoiceDTO prepareReversePickupInvoiceDTO(Invoice invoice, ReversePickup reversePickup) {
        InvoiceDTO invoiceDTO = invoiceService.prepareInvoiceDTO(invoice);
        invoiceDTO.setReversePickupCode(reversePickup.getCode());
        invoiceDTO.setPaymentMode(PaymentMethod.Code.PREPAID.name());
        return invoiceDTO;
    }
}
