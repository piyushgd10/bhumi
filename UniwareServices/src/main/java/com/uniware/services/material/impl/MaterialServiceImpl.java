/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 30-Jun-2012
 *  @author vibhu
 */
package com.uniware.services.material.impl;

import com.uniware.core.utils.UserContext;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.email.EmailMessage;
import com.unifier.core.entity.User;
import com.unifier.core.template.Template;
import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.DateUtils.DateRange;
import com.unifier.core.utils.FileUtils;
import com.unifier.core.utils.NumberUtils;
import com.unifier.core.utils.StringUtils;
import com.unifier.core.utils.ValidatorUtils;
import com.unifier.services.aspect.RollbackOnFailure;
import com.unifier.services.email.IEmailService;
import com.unifier.services.pdf.IPdfDocumentService;
import com.unifier.services.pdf.impl.PdfDocumentServiceImpl.PrintOptions;
import com.unifier.services.utils.CustomFieldUtils;
import com.uniware.core.api.invoice.GenerateOrUpdateInvoiceRequest;
import com.uniware.core.api.invoice.GenerateOrUpdateInvoiceResponse;
import com.uniware.core.api.material.AddItemToGatePassRequest;
import com.uniware.core.api.material.AddItemToGatePassResponse;
import com.uniware.core.api.material.AddNonTraceableGatePassItemRequest;
import com.uniware.core.api.material.AddNonTraceableGatePassItemResponse;
import com.uniware.core.api.material.AddOrEditNonTraceableGatePassItemRequest;
import com.uniware.core.api.material.AddOrEditNonTraceableGatePassItemResponse;
import com.uniware.core.api.material.CompleteGatePassRequest;
import com.uniware.core.api.material.CompleteGatePassResponse;
import com.uniware.core.api.material.CreateGatePassRequest;
import com.uniware.core.api.material.CreateGatePassResponse;
import com.uniware.core.api.material.CreateInboundGatePassRequest;
import com.uniware.core.api.material.CreateInboundGatePassResponse;
import com.uniware.core.api.material.DiscardGatePassRequest;
import com.uniware.core.api.material.DiscardGatePassResponse;
import com.uniware.core.api.material.EditGatePassRequest;
import com.uniware.core.api.material.EditGatePassResponse;
import com.uniware.core.api.material.EditInboundGatePassRequest;
import com.uniware.core.api.material.EditInboundGatePassResponse;
import com.uniware.core.api.material.EditNonTraceableGatePassItemRequest;
import com.uniware.core.api.material.EditNonTraceableGatePassItemResponse;
import com.uniware.core.api.material.EditTraceableOutboundGatepassItemRequest;
import com.uniware.core.api.material.EditTraceableOutboundGatepassItemResponse;
import com.uniware.core.api.material.GatePassDTO;
import com.uniware.core.api.material.GatePassDTO.GatePassItemDTO;
import com.uniware.core.api.material.GetGatePassRequest;
import com.uniware.core.api.material.GetGatePassResponse;
import com.uniware.core.api.material.GetGatepassScannableItemDetailsRequest;
import com.uniware.core.api.material.GetGatepassScannableItemDetailsResponse;
import com.uniware.core.api.material.GetGatepassScannableItemDetailsResponse.ScannedItemDetailDTO;
import com.uniware.core.api.material.GetGatepassScannableItemSkuDetailRequest;
import com.uniware.core.api.material.GetGatepassScannableItemSkuDetailResponse;
import com.uniware.core.api.material.GetGatepassScannableItemSkuDetailResponse.ScannedItemSkuDetailDTO;
import com.uniware.core.api.material.GetGatepassScannableItemSkuDetailResponse.ScannedItemSkuDetailDTO.ShelfwiseInventoryDTO;
import com.uniware.core.api.material.GetGatepassScannableItemSkuDetailResponse.ScannedItemSkuDetailDTO.ShelfwiseInventoryDTO.ShelfInventoryTypeDetailDTO;
import com.uniware.core.api.material.GetGatepassSummaryRequest;
import com.uniware.core.api.material.GetGatepassSummaryResponse;
import com.uniware.core.api.material.GetGatepassSummaryResponse.GatepassSummaryDTO;
import com.uniware.core.api.material.GetInboundGatePassRequest;
import com.uniware.core.api.material.GetInboundGatePassResponse;
import com.uniware.core.api.material.GetOutboundGatepassTypesRequest;
import com.uniware.core.api.material.GetOutboundGatepassTypesResponse;
import com.uniware.core.api.material.InboundGatePassDTO;
import com.uniware.core.api.material.RemoveItemFromGatePassRequest;
import com.uniware.core.api.material.RemoveItemFromGatePassResponse;
import com.uniware.core.api.material.RemoveNonTraceableGatePassItemRequest;
import com.uniware.core.api.material.RemoveNonTraceableGatePassItemResponse;
import com.uniware.core.api.material.SearchGatePassRequest;
import com.uniware.core.api.material.SearchGatePassResponse;
import com.uniware.core.api.material.SearchGatePassResponse.SearchGatePassDTO;
import com.uniware.core.api.material.WsGatePass;
import com.uniware.core.api.material.WsInboundGatePass;
import com.uniware.core.api.model.WsInvoice;
import com.uniware.core.api.model.WsInvoiceItem;
import com.uniware.core.api.model.WsTaxPercentageDetail;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.cache.FacilityCache;
import com.uniware.core.entity.Facility;
import com.uniware.core.entity.InboundGatePass;
import com.uniware.core.entity.InventoryLedger;
import com.uniware.core.entity.Invoice;
import com.uniware.core.entity.InvoiceItem;
import com.uniware.core.entity.Item;
import com.uniware.core.entity.ItemType;
import com.uniware.core.entity.ItemTypeInventory;
import com.uniware.core.entity.ItemTypeInventory.Type;
import com.uniware.core.entity.OutboundGatePass;
import com.uniware.core.entity.OutboundGatePassItem;
import com.uniware.core.entity.OutboundGatePassStatus;
import com.uniware.core.entity.Party;
import com.uniware.core.entity.PartyAddressType;
import com.uniware.core.entity.PartyContact;
import com.uniware.core.entity.PartyContactType;
import com.uniware.core.entity.Shelf;
import com.uniware.core.utils.Constants.EmailTemplateType;
import com.uniware.core.utils.EnvironmentProperties;
import com.uniware.core.vo.PrintTemplateVO;
import com.uniware.core.vo.SamplePrintTemplateVO;
import com.uniware.dao.inventory.IInventoryDao;
import com.uniware.dao.material.IMaterialDao;
import com.uniware.services.cache.PrintTemplateCache;
import com.uniware.services.cache.SamplePrintTemplateCache;
import com.uniware.services.catalog.ICatalogService;
import com.uniware.services.configuration.SystemConfiguration;
import com.uniware.services.configuration.SystemConfiguration.TraceabilityLevel;
import com.uniware.services.exception.InventoryAccuracyException;
import com.uniware.services.exception.InventoryNotAvailableException;
import com.uniware.services.exception.ShelfBlockedForCycleCountException;
import com.uniware.services.inventory.IInventoryService;
import com.uniware.services.invoice.IInvoiceService;
import com.uniware.services.ledger.IInventoryLedgerService;
import com.uniware.services.material.IMaterialService;
import com.uniware.services.party.IPartyService;
import com.uniware.services.tax.ITaxService;
import com.uniware.services.vendor.IVendorService;
import com.uniware.services.warehouse.IShelfService;


@Service("materialService")
@Transactional
public class MaterialServiceImpl implements IMaterialService {

    private static final Logger     LOG = LoggerFactory.getLogger(MaterialServiceImpl.class);

    @Autowired
    private IMaterialDao            materialDao;

    @Autowired
    private IPartyService           partyService;

    @Autowired
    private IInventoryService       inventoryService;

    @Autowired
    private IInventoryDao           inventoryDao;

    @Autowired
    private ICatalogService         catalogService;

    @Autowired
    private IShelfService           shelfService;

    @Autowired
    private IEmailService           emailService;

    @Autowired
    private IPdfDocumentService     pdfDocumentService;

    @Autowired
    private IVendorService          vendorService;

    @Autowired
    private IInvoiceService         invoiceService;

    @Autowired
    private ITaxService             taxService;

    @Autowired
    private IInventoryLedgerService inventoryLedgerService;

    @Override
    public CreateGatePassResponse createGatePass(CreateGatePassRequest request) {
        CreateGatePassResponse response = new CreateGatePassResponse();
        WsGatePass wsGatePass = request.getWsGatePass();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            Party toParty = null;
            if (OutboundGatePass.Type.STOCK_TRANSFER.name().equalsIgnoreCase(request.getType())) {
                toParty = CacheManager.getInstance().getCache(FacilityCache.class).getFacilityByCode(request.getPartyCode());
                if (toParty == null || toParty.getCode().equals(CacheManager.getInstance().getCache(FacilityCache.class).getCurrentFacility().getCode())
                        || !((Facility) toParty).getType().equals(Facility.Type.WAREHOUSE.name())) {
                    context.addError(WsResponseCode.INVALID_PARTY_CODE, "Stock transfers can be created only for other facilities of type warehouse");
                }
            } else {
                toParty = partyService.getPartyByCode(request.getPartyCode());
                if (toParty == null) {
                    context.addError(WsResponseCode.INVALID_PARTY_CODE, "Invalid party code");
                } else if (!toParty.isEnabled()) {
                    context.addError(WsResponseCode.INVALID_PARTY_CODE, "Party is disabled");
                }
            }
            if (!context.hasErrors()) {
                OutboundGatePass gatePass = new OutboundGatePass();
                Map<String, Object> customFieldValues = CustomFieldUtils.getCustomFieldValues(context, OutboundGatePass.class.getName(),
                        request.getWsGatePass().getCustomFieldValues());
                if (StringUtils.isNotBlank(wsGatePass.getCode())) {
                    gatePass.setCode(wsGatePass.getCode());
                }
                gatePass.setToParty(toParty);
                gatePass.setType(request.getType());
                gatePass.setTransferAmount(wsGatePass.getTransferAmount());
                gatePass.setPurpose(wsGatePass.getPurpose());
                gatePass.setReferenceNumber(wsGatePass.getReferenceNumber());
                gatePass.setUser(new User(request.getUserId()));
                gatePass.setCreated(DateUtils.getCurrentTime());
                gatePass.setStatusCode(OutboundGatePass.StatusCode.CREATED.name());
                CustomFieldUtils.setCustomFieldValues(gatePass, customFieldValues);
                gatePass = materialDao.addGatePass(gatePass);
                response.setSuccessful(true);
                response.setGatePassCode(gatePass.getCode());
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    public EditGatePassResponse editGatePass(EditGatePassRequest request) {
        EditGatePassResponse response = new EditGatePassResponse();
        WsGatePass wsGatePass = request.getWsGatePass();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            OutboundGatePass gatePass = materialDao.getGatePassByCode(request.getGatePassCode());
            if (gatePass == null) {
                context.addError(WsResponseCode.INVALID_OUTBOUND_GATE_PASS_CODE);
            } else {
                gatePass.setTransferAmount(wsGatePass.getTransferAmount());
                gatePass.setPurpose(wsGatePass.getPurpose());
                gatePass.setReferenceNumber(wsGatePass.getReferenceNumber());
                if (wsGatePass.getCustomFieldValues() != null) {
                    CustomFieldUtils.setCustomFieldValues(gatePass,
                            CustomFieldUtils.getCustomFieldValues(context, OutboundGatePass.class.getName(), wsGatePass.getCustomFieldValues()));
                }
                gatePass = materialDao.updateGatePass(gatePass);
                response.setSuccessful(true);
                response.setGatePassCode(gatePass.getCode());
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    public CompleteGatePassResponse completeGatePass(CompleteGatePassRequest request) {
        CompleteGatePassResponse response = new CompleteGatePassResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            OutboundGatePass gatePass = materialDao.getGatePassByCode(request.getGatePassCode(), true);
            if (gatePass == null) {
                context.addError(WsResponseCode.INVALID_OUTBOUND_GATE_PASS_CODE, "Invalid Gate pass code");
            } else if (!OutboundGatePass.StatusCode.CREATED.name().equals(gatePass.getStatusCode())) {
                context.addError(WsResponseCode.INVALID_OUTBOUND_GATE_PASS_STATE, "Invalid Gate pass state");
            } else {
                if (taxService.isGSTEnabled() && (isReturnInvoiceNeeded(gatePass) || !gatePass.isReturnAwaited())
                        && ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).isGatepassInvoicingRequired()) {
                    createGatepassInvoice(gatePass, context, request.getUserId());
                }
                if (!context.hasErrors()) {
                    OutboundGatePassItem.StatusCode status = (OutboundGatePass.Type.RETURNABLE.name().equals(gatePass.getType())
                            || OutboundGatePass.Type.STOCK_TRANSFER.name().equals(gatePass.getType())) ? OutboundGatePassItem.StatusCode.RETURN_AWAITED
                                    : OutboundGatePassItem.StatusCode.CLOSED;
                    for (OutboundGatePassItem gatePassItem : gatePass.getOutboundGatePassItems()) {
                        gatePassItem.setStatusCode(status.name());
                    }
                    addInventoryLedgerEntryForGatepassOut(gatePass);
                    gatePass.setStatusCode(
                            (OutboundGatePass.Type.RETURNABLE.name().equals(gatePass.getType()) || OutboundGatePass.Type.STOCK_TRANSFER.name().equals(gatePass.getType()))
                                    ? OutboundGatePass.StatusCode.RETURN_AWAITED.name() : OutboundGatePass.StatusCode.CLOSED.name());
                    PartyContact primaryContact = gatePass.getToParty().getPartyContactByType(PartyContactType.Code.PRIMARY.name());
                    if (primaryContact != null && StringUtils.isNotBlank(primaryContact.getEmail())) {
                        sendGatePassMail(gatePass, primaryContact);
                    }
                    response.setSuccessful(true);
                }
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    /**
     * @param outboundGatePass
     */
    private void addInventoryLedgerEntryForGatepassOut(OutboundGatePass outboundGatePass) {
        List<OutboundGatePassItem> outboundGatePassItems = new ArrayList<>(outboundGatePass.getOutboundGatePassItems()); //typecasting from set
        Map<String, GatepassItemDetails> skuCodeToGatepassItemDetailsMap = populateSkuToGatepassItemDetailsMap(outboundGatePassItems);
        skuCodeToGatepassItemDetailsMap.forEach((skuCode, itemSkuQuantityVendorSku) -> {
            InventoryLedger inventoryLedger = new InventoryLedger();
            inventoryLedger.setTransactionType(InventoryLedger.TransactionType.OUTBOUND_GATE_PASS);
            inventoryLedger.setTransactionIdentifier(outboundGatePass.getCode());
            inventoryLedger.setSkuCode(skuCode);
            inventoryLedger.setAdditionalInfo(outboundGatePass.getType());
            inventoryLedgerService.addInventoryLedgerEntry(inventoryLedger, itemSkuQuantityVendorSku.getQuantity(), InventoryLedger.ChangeType.DECREASE);

        });
    }

    @Override
    public Map<String, GatepassItemDetails> populateSkuToGatepassItemDetailsMap(
            List<OutboundGatePassItem> outboundGatePassItems) {
        Map<String, GatepassItemDetails> skuCodeToGatepassItemDetailsMap = new HashMap<>();
        for (OutboundGatePassItem outboundGatePassItem : outboundGatePassItems) {
            String skuCode = outboundGatePassItem.getItemType().getSkuCode();
            GatepassItemDetails gatepassItemDetails = skuCodeToGatepassItemDetailsMap.computeIfAbsent(skuCode, k -> new GatepassItemDetails(outboundGatePassItem.getQuantity()));
            gatepassItemDetails.setQuantity(gatepassItemDetails.getQuantity() + 1);
        }
        return skuCodeToGatepassItemDetailsMap;
    }

    private void sendGatePassMail(OutboundGatePass gatePass, PartyContact primaryContact) {
        try {
            List<String> recipients = new ArrayList<String>();
            recipients.add(primaryContact.getEmail());
            EmailMessage message = new EmailMessage(recipients, EmailTemplateType.OUTBOUND_GATEPASS.name());
            message.addTemplateParam("gatePass", gatePass);

            String directory = EnvironmentProperties.getGatepassCompletionReport();
            Map<String, Object> contextParams = new HashMap<>();
            contextParams.put("gatePass", gatePass);

            String attachmentName = StringUtils.underscorify(gatePass.getCode() + " " + gatePass.getToParty().getName());

            attachGatePassPdf(gatePass, message, directory, attachmentName);
            emailService.send(message);
        } catch (Exception e) {
            LOG.error("Error occured while sending GatePass mail to Party:", e);
        }
    }

    private void attachGatePassPdf(OutboundGatePass gatePass, EmailMessage message, String directory, String attachmentName) throws FileNotFoundException {
        Template template = CacheManager.getInstance().getCache(PrintTemplateCache.class).getTemplateByType(PrintTemplateVO.Type.OUTBOUND_GATE_PASS.name());
        PrintTemplateVO globalGatePassTemplate = CacheManager.getInstance().getCache(PrintTemplateCache.class).getPrintTemplateByType(PrintTemplateVO.Type.OUTBOUND_GATE_PASS);
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("gatePass", gatePass);
        String gatePassHtml = template.evaluate(params);

        File file = new File((directory.endsWith("/") ? directory : directory + "/") + attachmentName + ".pdf");
        FileOutputStream fout = null;
        try {
            fout = new FileOutputStream(file);
            SamplePrintTemplateVO samplePrintTemplate = CacheManager.getInstance().getCache(SamplePrintTemplateCache.class).getSamplePrintTemplate(
                    globalGatePassTemplate.getSamplePrintTemplateCode());
            pdfDocumentService.writeHtmlToPdf(fout, gatePassHtml, new PrintOptions(samplePrintTemplate));
            message.getAttachments().add(file);
        } finally {
            FileUtils.safelyCloseOutputStream(fout);
        }

    }

    @Override
    public AddItemToGatePassResponse addItemToGatePass(AddItemToGatePassRequest request) {
        AddItemToGatePassResponse response = new AddItemToGatePassResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            OutboundGatePass gatePass = materialDao.getGatePassByCode(request.getGatePassCode(), true);
            if (gatePass == null) {
                context.addError(WsResponseCode.INVALID_OUTBOUND_GATE_PASS_CODE, "Invalid Gate pass code");
            } else if (!OutboundGatePass.StatusCode.CREATED.name().equals(gatePass.getStatusCode())) {
                context.addError(WsResponseCode.INVALID_OUTBOUND_GATE_PASS_STATE, "Invalid Gate pass state");
            } else {
                Item item = inventoryService.getItemByCode(request.getItemCode());
                if (item == null) {
                    context.addError(WsResponseCode.INVALID_ITEM_CODE, "Invalid item code");
                } else if (!StringUtils.equalsAny(item.getStatusCode(), Item.StatusCode.BAD_INVENTORY.name(), Item.StatusCode.QC_REJECTED.name(),
                        Item.StatusCode.GOOD_INVENTORY.name())) {
                    context.addError(WsResponseCode.INVALID_ITEM_STATE, "Only BAD_INVENTORY/QC_REJECTED/GOOD_INVENTORY items can be added to gate pass");
                } else if (!StringUtils.equalsAny(item.getStatusCode(), Item.StatusCode.BAD_INVENTORY.name(), Item.StatusCode.GOOD_INVENTORY.name())
                        && OutboundGatePass.Type.RETURNABLE.name().equals(gatePass.getType())) {
                    context.addError(WsResponseCode.INVALID_ITEM_STATE, "Only GOOD_INVENTORY/BAD_INVENTORY items can only be added to returable gate pass");
                } else if (OutboundGatePass.Type.RETURN_TO_VENDOR.name().equals(gatePass.getType()) && !item.getVendor().getCode().equals(gatePass.getToParty().getCode())) {
                    context.addError(WsResponseCode.INVALID_ITEM_STATE, "Only '" + gatePass.getToParty().getName() + "' items can be added to this gatepass");
                } else {
                    BigDecimal itemUnitPrice = NumberUtils.divide(item.getInflowReceiptItem().getTotal(), item.getInflowReceiptItem().getQuantity());
                    Optional<OutboundGatePassItem> alreadyAddedGatepassItem = gatePass.getOutboundGatePassItems().stream().filter(
                            gpi -> gpi.getItemType().getId().equals(item.getItemType().getId())).findFirst();
                    if (alreadyAddedGatepassItem.isPresent()) {
                        if (alreadyAddedGatepassItem.get().getUnitPrice().compareTo(itemUnitPrice) != 0) {
                            context.addError(WsResponseCode.INVALID_ITEM_CODE, "Item with same sku already present with different price");
                        }
                    }
                    if (!context.hasErrors()) {
                        try {
                            inventoryService.blockInventory(item);
                        } catch (InventoryNotAvailableException e) {
                            // Ignoring this exception if BAD inventory is added to gatepass
                            if (Type.GOOD_INVENTORY.equals(item.getInventoryType())) {
                                context.addError(WsResponseCode.INVENTORY_NOT_AVAILABLE, "Item inventory has already been assigned");
                            }
                        } catch (InventoryAccuracyException e) {
                            context.addError(WsResponseCode.INVENTORY_NOT_AVAILABLE, "Inventory should be available on assigned shelf only");
                        } catch (ShelfBlockedForCycleCountException e) {
                            LOG.info("Error added in context. Shelf is blocked for cycle count");
                            context.addError(WsResponseCode.SHELF_BLOCKED_FOR_CYCLE_COUNT, e.getMessage());
                        }
                    }
                    if (!context.hasErrors()) {
                        OutboundGatePassItem gatePassItem = new OutboundGatePassItem();
                        gatePassItem.setItemType(item.getItemType());
                        gatePassItem.setQuantity(1);
                        gatePassItem.setItem(item);
                        // This price should always be tax inclusive
                        gatePassItem.setUnitPrice(itemUnitPrice);
                        gatePassItem.setOutboundGatePass(gatePass);
                        gatePassItem.setStatusCode(OutboundGatePassItem.StatusCode.CREATED.name());
                        gatePassItem.setReason(request.getReason());
                        gatePassItem.setCreated(DateUtils.getCurrentTime());
                        Shelf shelf = item.getShelf();
                        if (shelf == null) {
                            shelf = shelfService.getShelfByCode(Shelf.ShelfCode.DEFAULT.name());
                        }
                        gatePassItem.setShelf(shelf);
                        gatePassItem.setInventoryType(item.getInventoryType());
                        gatePassItem = materialDao.addGatePassItem(gatePassItem);
                        if (OutboundGatePass.Type.RETURNABLE.name().equals(gatePass.getType())) {
                            item.setStatusCode(Item.StatusCode.RETURN_AWAITED.name());
                        } else {
                            item.setStatusCode(Item.StatusCode.LIQUIDATED.name());
                        }
                        response.setSuccessful(true);
                    }
                }

            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    public RemoveItemFromGatePassResponse removeItemFromGatePass(RemoveItemFromGatePassRequest request) {
        RemoveItemFromGatePassResponse response = new RemoveItemFromGatePassResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            OutboundGatePass gatePass = materialDao.getGatePassByCode(request.getGatePassCode(), true);
            if (gatePass == null) {
                context.addError(WsResponseCode.INVALID_OUTBOUND_GATE_PASS_CODE, "Invalid Gate pass code");
            } else if (!OutboundGatePass.StatusCode.CREATED.name().equals(gatePass.getStatusCode())) {
                context.addError(WsResponseCode.INVALID_OUTBOUND_GATE_PASS_STATE, "Invalid Gate pass state");
            } else {
                Item item = inventoryService.getItemByCode(request.getItemCode());
                if (item == null) {
                    context.addError(WsResponseCode.INVALID_ITEM_CODE, "Invalid item code");
                } else if (!StringUtils.equalsAny(item.getStatusCode(), Item.StatusCode.LIQUIDATED.name(), Item.StatusCode.RETURN_AWAITED.name())) {
                    context.addError(WsResponseCode.INVALID_ITEM_STATE, "Item not added to gatepass");
                } else {
                    OutboundGatePassItem gatePassItem = null;
                    for (OutboundGatePassItem outboundGatePassItem : gatePass.getOutboundGatePassItems()) {
                        if (outboundGatePassItem.getItem().getId().equals(item.getId())) {
                            gatePassItem = outboundGatePassItem;
                        }
                    }
                    if (gatePassItem == null) {
                        context.addError(WsResponseCode.INVALID_ITEM_STATE, "Item not added to gatepass");
                    }

                    if (!context.hasErrors()) {
                        gatePass.getOutboundGatePassItems().remove(gatePassItem);
                        inventoryService.addInventory(gatePassItem.getItemType().getSkuCode(), gatePassItem.getInventoryType(), gatePassItem.getItem().getAgeingStartDate(),
                                gatePassItem.getShelf(), 1);
                        item.setStatusCode(gatePassItem.getInventoryType().name());
                        gatePassItem.setQuantity(0);
                        gatePassItem = materialDao.updateGatePassItem(gatePassItem);
                        materialDao.removeGatePassItem(gatePassItem);
                        response.setSuccessful(true);
                    }
                }

            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @RollbackOnFailure
    public AddOrEditNonTraceableGatePassItemResponse addOrEditNonTraceableGatePassItem(AddOrEditNonTraceableGatePassItemRequest request) {
        AddOrEditNonTraceableGatePassItemResponse response = new AddOrEditNonTraceableGatePassItemResponse();
        request.setInventoryType(ValidatorUtils.getOrDefaultValue(request.getInventoryType(), Type.GOOD_INVENTORY));
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            OutboundGatePass gatePass = materialDao.getGatePassByCode(request.getGatePassCode(), true);
            if (gatePass == null) {
                context.addError(WsResponseCode.INVALID_OUTBOUND_GATE_PASS_CODE, "Invalid Gate pass code");
            } else if (!OutboundGatePass.StatusCode.CREATED.name().equals(gatePass.getStatusCode())) {
                context.addError(WsResponseCode.INVALID_OUTBOUND_GATE_PASS_STATE, "Invalid Gate pass state");
            } else if (ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).getTraceabilityLevel() == TraceabilityLevel.ITEM) {
                context.addError(WsResponseCode.ACTION_APPLICABLE_FOR_TRACEABLE_ITEMS, "This action can only be used with ITEM_SKU or NONE level traceability");
            } else {
                ItemType itemType = catalogService.getItemTypeBySkuCode(request.getItemSKU());
                if (itemType == null) {
                    context.addError(WsResponseCode.INVALID_ITEM_TYPE, "Invalid item sku code");
                } else if (request.getUnitPrice() == null && itemType.getBasePrice() == null) {
                    context.addError(WsResponseCode.INVALID_ITEM_TYPE, "Unit price not defined for sku :" + itemType.getSkuCode());
                } else {
                    OutboundGatePassItem gatePassItem = null;
                    Shelf addToShelf = null;
                    for (OutboundGatePassItem item : gatePass.getOutboundGatePassItems()) {
                        if (item.getItemType().getId().equals(itemType.getId()) && item.getInventoryType() == request.getInventoryType()) {
                            gatePassItem = item;
                            break;
                        }
                    }
                    int quantityToBlock = gatePassItem != null ? request.getQuantity() - gatePassItem.getQuantity() : request.getQuantity();
                    if (quantityToBlock > 0) {
                        for (int i = 0; i < quantityToBlock; i++) {
                            try {
                                ItemTypeInventory itemTypeInventory = inventoryService.removeAgeAgnosticInventory(itemType, request.getInventoryType());
                                addToShelf = itemTypeInventory.getShelf();
                            } catch (InventoryNotAvailableException e) {
                                context.addError(WsResponseCode.INVENTORY_NOT_AVAILABLE, "Item inventory has already been assigned");
                                break;
                            } catch (ShelfBlockedForCycleCountException e) {
                                context.addError(WsResponseCode.SHELF_BLOCKED_FOR_CYCLE_COUNT);
                                break;
                            }
                        }
                    } else {
                        ItemTypeInventory itemTypeInventory = inventoryDao.getLatestItemTypeInventoryByItemType(gatePassItem.getItemType().getId(),
                                gatePassItem.getInventoryType());
                        addToShelf = itemTypeInventory != null ? itemTypeInventory.getShelf() : shelfService.getShelfByCode(Shelf.ShelfCode.DEFAULT.name());
                        inventoryService.addInventory(itemType, request.getInventoryType(), addToShelf, -quantityToBlock);
                    }
                    if (!context.hasErrors()) {
                        if (gatePassItem != null) {
                            gatePassItem.setQuantity(request.getQuantity());
                        } else {
                            gatePassItem = new OutboundGatePassItem();
                            gatePassItem.setItemType(itemType);
                            gatePassItem.setQuantity(request.getQuantity());
                            gatePassItem.setOutboundGatePass(gatePass);
                            gatePassItem.setUnitPrice(request.getUnitPrice() != null ? request.getUnitPrice() : itemType.getBasePrice());
                            gatePassItem.setStatusCode(OutboundGatePassItem.StatusCode.CREATED.name());
                            gatePassItem.setCreated(DateUtils.getCurrentTime());
                            gatePassItem.setInventoryType(request.getInventoryType());
                            gatePassItem.setShelf(addToShelf);
                            materialDao.addGatePassItem(gatePassItem);
                        }
                        response.setSuccessful(true);
                    }
                }
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    /**
     * Sku level, so no ageing required.
     *
     * @param request
     * @return
     */
    @Override
    @Transactional
    @RollbackOnFailure
    public AddNonTraceableGatePassItemResponse addNonTraceableGatePassItem(AddNonTraceableGatePassItemRequest request) {
        AddNonTraceableGatePassItemResponse response = new AddNonTraceableGatePassItemResponse();
        request.setInventoryType(ValidatorUtils.getOrDefaultValue(request.getInventoryType(), Type.GOOD_INVENTORY));
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            OutboundGatePass gatePass = materialDao.getGatePassByCode(request.getGatePassCode(), true);
            if (gatePass == null) {
                context.addError(WsResponseCode.INVALID_OUTBOUND_GATE_PASS_CODE, "Invalid Gate pass code");
            } else if (!OutboundGatePass.StatusCode.CREATED.name().equals(gatePass.getStatusCode())) {
                context.addError(WsResponseCode.INVALID_OUTBOUND_GATE_PASS_STATE, "Invalid Gate pass state");
            } else if (ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).getTraceabilityLevel() == TraceabilityLevel.ITEM) {
                context.addError(WsResponseCode.ACTION_APPLICABLE_FOR_TRACEABLE_ITEMS, "This action can only be used with ITEM_SKU or NONE level traceability");
            } else {
                Shelf shelf = null;
                if (StringUtils.isNotBlank(request.getShelfCode())) {
                    shelf = shelfService.getShelfByCode(request.getShelfCode());
                    if (shelf == null) {
                        context.addError(WsResponseCode.INVALID_SHELF_CODE, "Invalid shelf code " + request.getShelfCode());
                    }
                }
                if (!context.hasErrors()) {
                    ItemType itemType = catalogService.getItemTypeBySkuCode(request.getItemSKU());
                    if (itemType == null) {
                        context.addError(WsResponseCode.INVALID_ITEM_TYPE, "Invalid item sku code");
                    } else if ((request.getUnitPrice() == null && itemType.getBasePrice() == null)
                            || (request.getUnitPrice() != null && request.getUnitPrice().compareTo(BigDecimal.ZERO) < 1)
                            || (itemType.getBasePrice() != null && itemType.getBasePrice().compareTo(BigDecimal.ZERO) < 1)) {
                        context.addError(WsResponseCode.INVALID_ITEM_TYPE, "Unit price not defined for sku :" + itemType.getSkuCode());
                    } else {
                        int quantityToBlock = request.getQuantity();
                        Map<String, Integer> shelfToInventory = new HashMap<>();
                        for (int i = 0; i < quantityToBlock; i++) {
                            try {
                                String shelfCode = null;
                                if (shelf != null) {
                                    shelfCode = shelf.getCode();
                                    inventoryService.removeInventory(itemType, request.getInventoryType(), shelf, 1);
                                } else {
                                    ItemTypeInventory itemTypeInventory = inventoryService.removeAgeAgnosticInventory(itemType, request.getInventoryType());
                                    shelfCode = itemTypeInventory.getShelf().getCode();
                                }
                                Integer shelfInventory = shelfToInventory.get(shelfCode);
                                if (shelfInventory == null) {
                                    shelfToInventory.put(shelfCode, 0);
                                }
                                shelfToInventory.put(shelfCode, shelfToInventory.get(shelfCode) + 1);
                            } catch (InventoryNotAvailableException e) {
                                context.addError(WsResponseCode.INVENTORY_NOT_AVAILABLE, "Item inventory has already been assigned");
                                break;
                            } catch (ShelfBlockedForCycleCountException e) {
                                context.addError(WsResponseCode.SHELF_BLOCKED_FOR_CYCLE_COUNT);
                                break;
                            }
                        }
                        if (!context.hasErrors()) {
                            for (String shelfCode : shelfToInventory.keySet()) {
                                OutboundGatePassItem gatePassItem = null;
                                for (OutboundGatePassItem item : gatePass.getOutboundGatePassItems()) {
                                    if (item.getItemType().getId().equals(itemType.getId()) && item.getInventoryType() == request.getInventoryType()
                                            && item.getShelf().getCode().equals(shelfCode)) {
                                        gatePassItem = item;
                                        break;
                                    }
                                }
                                if (gatePassItem != null) {
                                    gatePassItem.setQuantity(gatePassItem.getQuantity() + shelfToInventory.get(shelfCode));
                                } else {
                                    Shelf gatePassItemShelf = shelfService.getShelfByCode(shelfCode);
                                    gatePassItem = new OutboundGatePassItem();
                                    gatePassItem.setItemType(itemType);
                                    gatePassItem.setQuantity(shelfToInventory.get(shelfCode));
                                    gatePassItem.setUnitPrice(request.getUnitPrice() != null ? request.getUnitPrice() : itemType.getBasePrice());
                                    gatePassItem.setOutboundGatePass(gatePass);
                                    gatePassItem.setStatusCode(OutboundGatePassItem.StatusCode.CREATED.name());
                                    gatePassItem.setCreated(DateUtils.getCurrentTime());
                                    gatePassItem.setShelf(gatePassItemShelf);
                                    gatePassItem.setInventoryType(request.getInventoryType());
                                    materialDao.addGatePassItem(gatePassItem);
                                }
                            }
                        }
                    }
                }
            }
        }
        response.setSuccessful(!context.hasErrors());
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    @Override
    @Transactional
    public EditNonTraceableGatePassItemResponse editNonTraceableGatePassItem(EditNonTraceableGatePassItemRequest request) {
        EditNonTraceableGatePassItemResponse response = new EditNonTraceableGatePassItemResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            OutboundGatePass gatePass = materialDao.getGatePassByCode(request.getGatePassCode(), true);
            if (gatePass == null) {
                context.addError(WsResponseCode.INVALID_OUTBOUND_GATE_PASS_CODE, "Invalid Gate pass code");
            } else if (!OutboundGatePass.StatusCode.CREATED.name().equals(gatePass.getStatusCode())) {
                context.addError(WsResponseCode.INVALID_OUTBOUND_GATE_PASS_STATE, "Invalid Gate pass state");
            } else if (ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).getTraceabilityLevel() == TraceabilityLevel.ITEM) {
                context.addError(WsResponseCode.ACTION_APPLICABLE_FOR_TRACEABLE_ITEMS, "This action can only be used with ITEM_SKU or NONE level traceability");
            } else {
                Shelf shelf = shelfService.getShelfByCode(request.getShelfCode());
                if (shelf == null) {
                    context.addError(WsResponseCode.INVALID_SHELF_CODE, "Invalid shelf code " + request.getShelfCode());
                }
                if (!context.hasErrors()) {
                    ItemType itemType = catalogService.getItemTypeBySkuCode(request.getItemSKU());
                    if (itemType == null) {
                        context.addError(WsResponseCode.INVALID_ITEM_TYPE, "Invalid item sku code");
                    } else {
                        OutboundGatePassItem gatePassItem = null;
                        for (OutboundGatePassItem item : gatePass.getOutboundGatePassItems()) {
                            if (item.getItemType().getId().equals(itemType.getId()) && item.getInventoryType() == request.getInventoryType()
                                    && item.getShelf().getCode().equals(request.getShelfCode())) {
                                gatePassItem = item;
                                break;
                            }
                        }
                        if (gatePassItem == null) {
                            context.addError(WsResponseCode.INVALID_OUTBOUND_GATE_PASS_ITEM_ID, "Invalid gate pass item");
                        } else if (gatePassItem.getQuantity() < request.getQuantity()) {
                            context.addError(WsResponseCode.INVALID_OUTBOUND_GATEPASS_ITEM_QUANTITY, "Quantity cannot be greater then existing quantity");
                        }
                        if (!context.hasErrors()) {
                            int quantityToBlock = request.getQuantity() - gatePassItem.getQuantity();
                            if (quantityToBlock < 0) {
                                inventoryService.addInventory(itemType, request.getInventoryType(), shelf, -quantityToBlock);
                            }
                        }
                        if (!context.hasErrors()) {
                            gatePassItem.setQuantity(request.getQuantity());
                            response.setSuccessful(true);
                        }
                    }
                }
            }
        }
        response.setSuccessful(!context.hasErrors());
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    @Override
    @Transactional
    public RemoveNonTraceableGatePassItemResponse removeNonTraceableGatePassItem(RemoveNonTraceableGatePassItemRequest request) {
        RemoveNonTraceableGatePassItemResponse response = new RemoveNonTraceableGatePassItemResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            OutboundGatePass gatePass = materialDao.getGatePassByCode(request.getGatePassCode(), true);
            if (gatePass == null) {
                context.addError(WsResponseCode.INVALID_OUTBOUND_GATE_PASS_CODE, "Invalid Gate pass code");
            } else if (!OutboundGatePass.StatusCode.CREATED.name().equals(gatePass.getStatusCode())) {
                context.addError(WsResponseCode.INVALID_OUTBOUND_GATE_PASS_STATE, "Invalid Gate pass state");
            } else if (!gatePass.getFacility().getId().equals(UserContext.current().getFacilityId())) {
                context.addError(WsResponseCode.INVALID_REQUEST, "Can remove gatepass items of this gatepass in " + gatePass.getFacility().getDisplayName() + " only.");
            } else {
                Shelf shelf = shelfService.getShelfByCode(request.getShelfCode());
                if (shelf == null) {
                    context.addError(WsResponseCode.INVALID_SHELF_CODE, "Invalid shelf code " + request.getShelfCode());
                } else {
                    ItemType itemType = catalogService.getItemTypeBySkuCode(request.getItemSKU());
                    if (itemType == null) {
                        context.addError(WsResponseCode.INVALID_ITEM_TYPE, "Invalid item sku code");
                    } else {
                        OutboundGatePassItem gatePassItem = null;
                        for (OutboundGatePassItem item : gatePass.getOutboundGatePassItems()) {
                            if (item.getItemType().getId().equals(itemType.getId()) && item.getInventoryType() == request.getInventoryType()
                                    && item.getShelf().getCode().equals(request.getShelfCode())) {
                                gatePassItem = item;
                                break;
                            }
                        }
                        if (gatePassItem == null) {
                            context.addError(WsResponseCode.INVALID_ITEM_STATE, "No such gatepass items exists");
                        }
                        if (!context.hasErrors()) {
                            gatePass.getOutboundGatePassItems().remove(gatePassItem);
                            inventoryService.addInventory(gatePassItem.getItemType(), gatePassItem.getInventoryType(), gatePassItem.getShelf(), gatePassItem.getQuantity());
                            gatePassItem.setQuantity(0);
                            gatePassItem = materialDao.updateGatePassItem(gatePassItem);
                            materialDao.removeGatePassItem(gatePassItem);
                        }
                    }
                }

            }
        }
        response.setSuccessful(!context.hasErrors());
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    @Override
    public SearchGatePassResponse searchGatePass(SearchGatePassRequest request) {
        SearchGatePassResponse response = new SearchGatePassResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            for (OutboundGatePass gatepass : materialDao.searchGatePass(request)) {
                SearchGatePassDTO gatepassDTO = new SearchGatePassDTO();
                gatepassDTO.setCode(gatepass.getCode());
                gatepassDTO.setStatusCode(gatepass.getStatusCode());
                gatepassDTO.setType(gatepass.getType());
                gatepassDTO.setUsername(gatepass.getUser().getUsername());
                gatepassDTO.setToParty(gatepass.getToParty().getName());
                gatepassDTO.setReference(gatepass.getReferenceNumber());
                gatepassDTO.setCreated(gatepass.getCreated());
                response.getElements().add(gatepassDTO);
            }
            if (request.getSearchOptions() != null && request.getSearchOptions().isGetCount()) {
                response.setTotalRecords(materialDao.getSearchGatePassCount(request));
            }
            response.setSuccessful(true);
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    public GetGatePassResponse getGatePass(GetGatePassRequest request) {
        GetGatePassResponse response = new GetGatePassResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            OutboundGatePass gatePass = materialDao.getGatePassByCode(request.getCode());
            if (gatePass == null) {
                context.addError(WsResponseCode.INVALID_CODE, "Invalid Gate Pass Code");
            } else {
                GatePassDTO gatePassDTO = new GatePassDTO();
                gatePassDTO.setId(gatePass.getId());
                gatePassDTO.setType(gatePass.getType());
                gatePassDTO.setCode(gatePass.getCode());
                if (gatePass.getInvoice() != null) {
                    gatePassDTO.setInvoiceCode(gatePass.getInvoice().getCode());
                    gatePassDTO.setInvoiceDisplayCode(gatePass.getInvoice().getDisplayCode());
                }
                if (gatePass.getReturnInvoice() != null) {
                    gatePassDTO.setReturnInvoiceCode(gatePass.getReturnInvoice().getCode());
                    gatePassDTO.setReturnInvoiceDisplayCode(gatePass.getReturnInvoice().getDisplayCode());
                }
                gatePassDTO.setUsername(gatePass.getUser().getUsername());
                gatePassDTO.setReference(gatePass.getReferenceNumber());
                gatePassDTO.setPurpose(gatePass.getPurpose());
                gatePassDTO.setToPartyName(gatePass.getToParty().getName());
                gatePassDTO.setStatusCode(gatePass.getStatusCode());
                gatePassDTO.setCreated(gatePass.getCreated());
                gatePassDTO.setCustomFieldValues(CustomFieldUtils.getCustomFieldValuesDTO(gatePass));
                List<OutboundGatePassItem> outboundGatePassItemList = materialDao.getOutboundGatepassItemByGatepass(gatePass.getId());
                for (OutboundGatePassItem gatePassItem : outboundGatePassItemList) {
                    GatePassItemDTO gatePassItemDTO = new GatePassItemDTO();
                    gatePassItemDTO.setGatePassItemId(gatePassItem.getId());
                    gatePassItemDTO.setGatePassItemStatus(gatePassItem.getStatusCode());
                    gatePassItemDTO.setInventoryType(gatePassItem.getInventoryType().name());
                    gatePassItemDTO.setItemTypeName(gatePassItem.getItemType().getName());
                    gatePassItemDTO.setItemTypeSKU(gatePassItem.getItemType().getSkuCode());
                    gatePassItemDTO.setItemTypeImageUrl(gatePassItem.getItemType().getImageUrl());
                    gatePassItemDTO.setItemTypePageUrl(gatePassItem.getItemType().getProductPageUrl());
                    gatePassItemDTO.setQuantity(gatePassItem.getQuantity());
                    gatePassItemDTO.setReason(gatePassItem.getReason());
                    gatePassItemDTO.setPendingQuantity(gatePassItem.getQuantity() - gatePassItem.getReceivedQuantity());
                    if (gatePassItem.getItem() != null) {
                        Item item = gatePassItem.getItem();
                        gatePassItemDTO.setItemCode(item.getCode());
                        if (request.isFetchInflowReceiptDetail() && item.getInflowReceiptItem() != null) {
                            gatePassItemDTO.setInflowReceiptCode(item.getInflowReceiptItem().getInflowReceipt().getCode());
                            gatePassItemDTO.setTotal(NumberUtils.divide(item.getInflowReceiptItem().getTotal(), item.getInflowReceiptItem().getQuantity()));
                            gatePassItemDTO.setUnitPrice(item.getInflowReceiptItem().getPurchaseOrderItem().getUnitPrice());
                            gatePassItemDTO.setTaxPercentage(item.getInflowReceiptItem().getPurchaseOrderItem().getTaxPercentage());
                            gatePassItemDTO.setCentralGstPercentage(item.getInflowReceiptItem().getPurchaseOrderItem().getCentralGstPercentage());
                            gatePassItemDTO.setStateGstPercentage(item.getInflowReceiptItem().getPurchaseOrderItem().getStateGstPercentage());
                            gatePassItemDTO.setUnionTerritoryGstPercentage(item.getInflowReceiptItem().getPurchaseOrderItem().getUnionTerritoryGstPercentage());
                            gatePassItemDTO.setIntegratedGstPercentage(item.getInflowReceiptItem().getPurchaseOrderItem().getIntegratedGstPercentage());
                            gatePassItemDTO.setCompensationCessPercentage(item.getInflowReceiptItem().getPurchaseOrderItem().getCompensationCessPercentage());
                        }
                        gatePassItemDTO.setItemStatus(item.getStatusCode());
                        gatePassItemDTO.setItemCondition(gatePassItemDTO.getItemCondition());
                    }
                    gatePassDTO.getGatePassItemDTOs().add(gatePassItemDTO);
                }
                response.setGatePassDTO(gatePassDTO);
                response.setSuccessful(true);
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    public OutboundGatePass getGatePassByCode(String gatePassCode, boolean lock) {
        return materialDao.getGatePassByCode(gatePassCode, lock);
    }

    @Override
    public OutboundGatePass getGatePassByInvoiceId(Integer invoiceId) {
        OutboundGatePass outboundGatePass = materialDao.getGatePassByInvoiceId(invoiceId);
        for (OutboundGatePassItem outboundGatePassItem : outboundGatePass.getOutboundGatePassItems()) {
            Hibernate.initialize(outboundGatePassItem.getInvoiceItem());
        }
        return outboundGatePass;
    }

    @Override
    public String getGatePassHtml(String gatePassCode, Template template) {
        OutboundGatePass gatePass = getGatePassByCode(gatePassCode, false);
        if (gatePass != null) {
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("gatePass", gatePass);
            return template.evaluate(params);
        } else {
            return null;
        }
    }

    @Override
    public DiscardGatePassResponse discardGatePass(DiscardGatePassRequest request) {
        DiscardGatePassResponse response = new DiscardGatePassResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            OutboundGatePass gatePass = materialDao.getGatePassByCode(request.getGatePassCode());
            if (gatePass == null) {
                context.addError(WsResponseCode.INVALID_OUTBOUND_GATE_PASS_CODE, "Invalid gatepass code");
            } else if (!OutboundGatePass.StatusCode.CREATED.name().equals(gatePass.getStatusCode())) {
                context.addError(WsResponseCode.INVALID_OUTBOUND_GATE_PASS_STATE, "gatepass not in CREATED state");
            } else if (gatePass.getOutboundGatePassItems().size() > 0) {
                context.addError(WsResponseCode.INVALID_OUTBOUND_GATE_PASS_STATE, "Items already added to gatepass");
            } else if (!gatePass.getUser().getId().equals(request.getUserId())) {
                context.addError(WsResponseCode.INVALID_USER_ID, "gatePass created by another user");
            } else {
                gatePass.setStatusCode(OutboundGatePass.StatusCode.DISCARDED.name());
                response.setSuccessful(true);
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    public CreateInboundGatePassResponse createInboundGatePass(CreateInboundGatePassRequest request) {
        CreateInboundGatePassResponse response = new CreateInboundGatePassResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            Party party = null;
            WsInboundGatePass wsInboundGatePass = request.getWsInboundGatePass();
            String partyCode = wsInboundGatePass.getPartyCode();
            if (StringUtils.isNotBlank(partyCode)) {
                party = partyService.getPartyByCode(partyCode);
                if (party == null) {
                    context.addError(WsResponseCode.INVALID_PARTY_CODE, "invalid party");
                }
            }
            if (!context.hasErrors()) {
                InboundGatePass inboundGatePass = new InboundGatePass();
                inboundGatePass = prepareInboundGatePass(wsInboundGatePass, inboundGatePass, party, context);
                inboundGatePass.setUser(new User(request.getUserId()));
                inboundGatePass.setCreated(DateUtils.getCurrentTime());
                inboundGatePass = materialDao.addInboundGatePass(inboundGatePass);
                response.setInboundGatePassDTO(prepareInboundGatePassDTO(inboundGatePass));
                response.setSuccessful(true);
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    private InboundGatePass prepareInboundGatePass(WsInboundGatePass wsInboundGatePass, InboundGatePass inboundGatePass, Party party, ValidationContext context) {
        inboundGatePass.setMaterialDescription(ValidatorUtils.getOrDefaultValue(wsInboundGatePass.getMaterialDescription(), inboundGatePass.getMaterialDescription()));
        inboundGatePass.setNoOfParcel(ValidatorUtils.getOrDefaultValue(wsInboundGatePass.getNoOfParcel(), inboundGatePass.getNoOfParcel()));
        inboundGatePass.setParty(ValidatorUtils.getOrDefaultValue(party, inboundGatePass.getParty()));
        inboundGatePass.setReferenceNumber(ValidatorUtils.getOrDefaultValue(wsInboundGatePass.getReferenceNumber(), inboundGatePass.getReferenceNumber()));
        inboundGatePass.setRemarks(ValidatorUtils.getOrDefaultValue(wsInboundGatePass.getRemarks(), inboundGatePass.getRemarks()));
        inboundGatePass.setTrackingNumber(ValidatorUtils.getOrDefaultValue(wsInboundGatePass.getTrackingNumber(), inboundGatePass.getTrackingNumber()));
        inboundGatePass.setTransporter(ValidatorUtils.getOrDefaultValue(wsInboundGatePass.getTransporter(), inboundGatePass.getTransporter()));
        inboundGatePass.setVehicleNumber(ValidatorUtils.getOrDefaultValue(wsInboundGatePass.getVehicleNumber(), inboundGatePass.getVehicleNumber()));
        return inboundGatePass;
    }

    @Override
    public EditInboundGatePassResponse editInboundGatePass(EditInboundGatePassRequest request) {
        EditInboundGatePassResponse response = new EditInboundGatePassResponse();
        ValidationContext context = request.validate(false);
        if (!context.hasErrors()) {
            WsInboundGatePass wsInboundGatePass = request.getWsInboundGatePass();
            InboundGatePass inboundGatePass = materialDao.getInboundGatePassByCode(request.getWsInboundGatePass().getCode());
            if (inboundGatePass == null) {
                context.addError(WsResponseCode.INVALID_CODE, "invalid inbound gatepass code");
            } else {
                Party party = null;
                String partyCode = wsInboundGatePass.getPartyCode();
                if (StringUtils.isNotBlank(partyCode)) {
                    party = partyService.getPartyByCode(partyCode);
                    if (party == null) {
                        context.addError(WsResponseCode.INVALID_PARTY_CODE, "invalid party");
                    }
                }
                inboundGatePass = prepareInboundGatePass(wsInboundGatePass, inboundGatePass, party, context);
                inboundGatePass = materialDao.updateInboundGatePass(inboundGatePass);
                response.setInboundGatePassDTO(prepareInboundGatePassDTO(inboundGatePass));
                response.setSuccessful(true);
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    private InboundGatePassDTO prepareInboundGatePassDTO(InboundGatePass gatePass) {
        InboundGatePassDTO gatePassDTO = new InboundGatePassDTO();
        gatePassDTO.setCode(gatePass.getCode());
        gatePassDTO.setMaterialDescription(gatePass.getMaterialDescription());
        gatePassDTO.setNoOfParcel(gatePass.getNoOfParcel());
        if (gatePass.getParty() != null) {
            gatePassDTO.setPartyName(gatePass.getParty().getName());
            gatePassDTO.setPartyCode(gatePass.getParty().getCode());
        }
        gatePassDTO.setReferenceNumber(gatePass.getReferenceNumber());
        gatePassDTO.setRemarks(gatePass.getRemarks());
        gatePassDTO.setTrackingNumber(gatePass.getTrackingNumber());
        gatePassDTO.setTransporter(gatePass.getTransporter());
        gatePassDTO.setVehicleNumber(gatePass.getVehicleNumber());
        return gatePassDTO;
    }

    @Override
    @Transactional
    public List<OutboundGatePass> getOutboundGatepassByRange(DateRange dateRange, String vendorCode) {
        return materialDao.getOutboundGatepassByRange(dateRange, vendorCode);
    }

    @Override
    @Transactional(readOnly = true)
    public GetInboundGatePassResponse getInboundGatePass(GetInboundGatePassRequest request) {
        GetInboundGatePassResponse response = new GetInboundGatePassResponse();
        ValidationContext context = request.validate(false);
        if (!context.hasErrors()) {
            InboundGatePass inboundGatePass = materialDao.getInboundGatePassByCode(request.getCode());
            response.setInboundGatePassDTO(prepareInboundGatePassDTO(inboundGatePass));
            response.setSuccessful(true);
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional(readOnly = true)
    public List<OutboundGatePassStatus> getOutboundGatePassStatuses() {
        return materialDao.getOutboundGatePassStatuses();
    }

    @Override
    public OutboundGatePass getGatePassById(int outboundGatepassId) {
        return materialDao.getGatePassById(outboundGatepassId);
    }

    @Override
    public OutboundGatePass getLastGatePassByItemId(int itemId) {
        OutboundGatePass lastGatePass = null;
        List<OutboundGatePass> gatePasses = materialDao.getOutboundGatepassByItemId(itemId);
        for (OutboundGatePass gatePass : gatePasses) {
            if (lastGatePass == null) {
                lastGatePass = gatePass;
            } else if (gatePass.getCreated().after(lastGatePass.getCreated())) {
                lastGatePass = gatePass;
            }
        }
        return lastGatePass;
    }

    @Override
    public GetOutboundGatepassTypesResponse getOutboundGatepassTypes(GetOutboundGatepassTypesRequest request) {
        GetOutboundGatepassTypesResponse response = new GetOutboundGatepassTypesResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            for (com.uniware.core.entity.OutboundGatePass.Type type : OutboundGatePass.Type.values()) {
                response.getTypes().add(type.name());
            }
        }
        response.setSuccessful(!context.hasErrors());
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    @Override
    @Transactional
    public GetGatepassSummaryResponse getGatepassSummary(GetGatepassSummaryRequest request) {
        GetGatepassSummaryResponse response = new GetGatepassSummaryResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            OutboundGatePass gatePass = materialDao.getGatePassByCode(request.getCode());
            if (gatePass == null) {
                context.addError(WsResponseCode.INVALID_OUTBOUND_GATE_PASS_CODE, "Invalid Gate Pass Code");
            } else {
                GatepassSummaryDTO gatePassDTO = new GatepassSummaryDTO();
                Set<String> itemSku = new HashSet<String>();
                gatePassDTO.setCode(gatePass.getCode());
                gatePassDTO.setType(gatePass.getType());
                gatePassDTO.setUsername(gatePass.getUser().getUsername());
                gatePassDTO.setReference(gatePass.getReferenceNumber());
                gatePassDTO.setPurpose(gatePass.getPurpose());
                if (gatePass.getInvoice() != null) {
                    gatePassDTO.setInvoiceCode(gatePass.getInvoice().getCode());
                    gatePassDTO.setInvoiceDisplayCode(gatePass.getInvoice().getDisplayCode());
                }
                if (gatePass.getReturnInvoice() != null) {
                    gatePassDTO.setReturnInvoiceCode(gatePass.getReturnInvoice().getCode());
                    gatePassDTO.setReturnInvoiceDisplayCode(gatePass.getReturnInvoice().getDisplayCode());
                }
                gatePassDTO.setToPartyName(gatePass.getToParty().getName());
                gatePassDTO.setStatusCode(gatePass.getStatusCode());
                gatePassDTO.setCreated(gatePass.getCreated());
                gatePassDTO.setCustomFieldValues(CustomFieldUtils.getCustomFieldValuesDTO(gatePass));
                Integer quantity = new Integer(0);
                for (OutboundGatePassItem gatePassItem : gatePass.getOutboundGatePassItems()) {
                    quantity += gatePassItem.getQuantity();
                    itemSku.add(gatePassItem.getItemType().getSkuCode());
                    Item item = gatePassItem.getItem();
                    if (item != null && item.getInflowReceiptItem() != null) {
                        BigDecimal total = gatePassDTO.getTotal().add(NumberUtils.divide(item.getInflowReceiptItem().getTotal(), item.getInflowReceiptItem().getQuantity()));
                        gatePassDTO.setTotal(total);
                    }
                }
                gatePassDTO.setTotalSku(itemSku.size());
                gatePassDTO.setTotalItems(quantity);
                response.setSummary(gatePassDTO);
            }
        }
        response.setSuccessful(!context.hasErrors());
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    @Override
    @Transactional
    public GetGatepassScannableItemDetailsResponse getGatepassScannableItemDetail(GetGatepassScannableItemDetailsRequest request) {
        GetGatepassScannableItemDetailsResponse response = new GetGatepassScannableItemDetailsResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            OutboundGatePass gatePass = materialDao.getGatePassByCode(request.getGatePassCode(), true);
            if (gatePass == null) {
                context.addError(WsResponseCode.INVALID_OUTBOUND_GATE_PASS_CODE, "Invalid Gate pass code");
            } else if (!OutboundGatePass.StatusCode.CREATED.name().equals(gatePass.getStatusCode())) {
                context.addError(WsResponseCode.INVALID_OUTBOUND_GATE_PASS_STATE, "Invalid Gate pass state");
            } else {
                Item item = inventoryService.getItemByCode(request.getItemCode());
                if (item == null) {
                    context.addError(WsResponseCode.INVALID_ITEM_CODE, "Invalid item code");
                } else if (!StringUtils.equalsAny(item.getStatusCode(), Item.StatusCode.BAD_INVENTORY.name(), Item.StatusCode.QC_REJECTED.name(),
                        Item.StatusCode.GOOD_INVENTORY.name())) {
                    context.addError(WsResponseCode.INVALID_ITEM_STATE, "Only BAD_INVENTORY/QC_REJECTED/GOOD_INVENTORY items can be added to gate pass");
                } else if (!StringUtils.equalsAny(item.getStatusCode(), Item.StatusCode.BAD_INVENTORY.name(), Item.StatusCode.GOOD_INVENTORY.name())
                        && OutboundGatePass.Type.RETURNABLE.name().equals(gatePass.getType())) {
                    context.addError(WsResponseCode.INVALID_ITEM_STATE, "Only GOOD_INVENTORY/BAD_INVENTORY items can only be added to returable gate pass");
                } else if (OutboundGatePass.Type.RETURN_TO_VENDOR.name().equals(gatePass.getType()) && !item.getVendor().getCode().equals(gatePass.getToParty().getCode())) {
                    context.addError(WsResponseCode.INVALID_ITEM_STATE, "Only '" + gatePass.getToParty().getName() + "' items can be added to this gatepass");
                } else {
                    ScannedItemDetailDTO scannedItemDetail = new ScannedItemDetailDTO();
                    scannedItemDetail.setItemCode(item.getCode());
                    scannedItemDetail.setItemName(item.getItemType().getName());
                    scannedItemDetail.setInventoryType(item.getInventoryType().name());
                    scannedItemDetail.setItemSKU(item.getItemType().getSkuCode());
                    if (item.getShelf() != null) {
                        scannedItemDetail.setShelfCode(item.getShelf().getCode());
                    }
                    response.setScannedItem(scannedItemDetail);
                }
            }
        }
        response.setSuccessful(!context.hasErrors());
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    @Override
    @Transactional(readOnly = true)
    public GetGatepassScannableItemSkuDetailResponse getGatepassScannableItemSkuDetails(GetGatepassScannableItemSkuDetailRequest request) {
        GetGatepassScannableItemSkuDetailResponse response = new GetGatepassScannableItemSkuDetailResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            OutboundGatePass gatePass = materialDao.getGatePassByCode(request.getGatePassCode(), true);
            if (gatePass == null) {
                context.addError(WsResponseCode.INVALID_OUTBOUND_GATE_PASS_CODE, "Invalid Gate pass code");
            } else if (!OutboundGatePass.StatusCode.CREATED.name().equals(gatePass.getStatusCode())) {
                context.addError(WsResponseCode.INVALID_OUTBOUND_GATE_PASS_STATE, "Invalid Gate pass state");
            } else if (ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).getTraceabilityLevel() == TraceabilityLevel.ITEM) {
                context.addError(WsResponseCode.ACTION_APPLICABLE_FOR_TRACEABLE_ITEMS, "This action can only be used with ITEM_SKU or NONE level traceability");
            } else {
                ItemType itemType = catalogService.getItemTypeBySkuCode(request.getItemSKU());
                if (itemType == null) {
                    context.addError(WsResponseCode.INVALID_ITEM_TYPE, "Invalid item sku code");
                } else {
                    Integer existingQuantity = 0;
                    for (OutboundGatePassItem gatepassItem : gatePass.getOutboundGatePassItems()) {
                        if (gatepassItem.getItemType().getSkuCode().equals(request.getItemSKU())) {
                            existingQuantity += gatepassItem.getQuantity();
                        }
                    }
                    ScannedItemSkuDetailDTO scannedItemSku = new ScannedItemSkuDetailDTO();
                    scannedItemSku.setItemName(itemType.getName());
                    scannedItemSku.setItemSKU(itemType.getSkuCode());
                    scannedItemSku.setUnitPrice(itemType.getBasePrice());
                    scannedItemSku.setExistingQuantity(existingQuantity);
                    List<ItemTypeInventory> itemTypeInventories = inventoryDao.getItemTypeInventoryByItemType(itemType.getId());
                    final Map<String, Map<String, Integer>> shelfToTypeToInventory = new HashMap<>();
                    itemTypeInventories.stream().filter(iti -> !iti.getType().equals(Type.VIRTUAL_INVENTORY)).forEach(iti -> {
                        Map<String, Integer> typeToInventory = shelfToTypeToInventory.computeIfAbsent(iti.getShelf().getCode(), k -> new HashMap<>());
                        typeToInventory.put(iti.getType().name(), iti.getQuantity());
                        shelfToTypeToInventory.put(iti.getShelf().getCode(), typeToInventory);
                    });
                    for (String shelfCode : shelfToTypeToInventory.keySet()) {
                        ShelfwiseInventoryDTO shelfInventory = new ShelfwiseInventoryDTO();
                        for (String type : shelfToTypeToInventory.get(shelfCode).keySet()) {
                            Integer availableInventory = shelfToTypeToInventory.get(shelfCode).get(type);
                            if (availableInventory > 0) {
                                ShelfInventoryTypeDetailDTO inventoryType = new ShelfInventoryTypeDetailDTO();
                                inventoryType.setQuantity(availableInventory);
                                inventoryType.setType(type);
                                shelfInventory.getInventory().add(inventoryType);
                            }
                        }
                        if (!shelfInventory.getInventory().isEmpty()) {
                            shelfInventory.setShelfCode(shelfCode);
                            scannedItemSku.getAvailableInventory().add(shelfInventory);
                        }
                    }
                    response.setScannedItem(scannedItemSku);
                }
            }
        }
        response.setSuccessful(!context.hasErrors());
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    @Override
    @Transactional
    public EditTraceableOutboundGatepassItemResponse editTraceableOutboundGatepassItem(EditTraceableOutboundGatepassItemRequest request) {
        EditTraceableOutboundGatepassItemResponse response = new EditTraceableOutboundGatepassItemResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            OutboundGatePass gatePass = materialDao.getGatePassByCode(request.getGatepassCode(), true);
            if (gatePass == null) {
                context.addError(WsResponseCode.INVALID_OUTBOUND_GATE_PASS_CODE, "Invalid Gate pass code");
            } else {
                Item item = inventoryService.getItemByCode(request.getItemCode());
                if (item == null) {
                    context.addError(WsResponseCode.INVALID_ITEM_CODE, "Invalid item code");
                } else {
                    OutboundGatePassItem gatePassItem = materialDao.getOutboundGatepassItemByGatepassAndItemCode(request.getGatepassCode(), request.getItemCode());
                    if (gatePassItem == null) {
                        context.addError(WsResponseCode.INVALID_OUTBOUND_GATEPASS_ITEM, "No such item exits with gatepass ");
                    } else {
                        gatePassItem.setReason(request.getReason());
                        materialDao.updateGatePassItem(gatePassItem);
                    }
                }
            }
        }
        response.setSuccessful(!context.hasErrors());
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    @Override
    public boolean isReturnInvoiceNeeded(OutboundGatePass gatePass) {
        if (!ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).isGatepassInvoicingRequired()) {
            return false;
        }
        boolean sameStateMovement = gatePass.getFacility().getPartyAddressByType(PartyAddressType.Code.SHIPPING.name()).getStateCode().equals(
                gatePass.getToParty().getPartyAddressByType(PartyAddressType.Code.SHIPPING.name()).getStateCode());
        return OutboundGatePass.Type.RETURNABLE.name().equals(gatePass.getType()) && !sameStateMovement && gatePass.isReturnAwaited();
    }

    private boolean isDeliveryChallanNeeded(OutboundGatePass gatePass) {
        if (!ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).isGatepassInvoicingRequired()) {
            return false;
        }
        OutboundGatePass.Type type = OutboundGatePass.Type.valueOf(gatePass.getType());
        boolean sameStateMovement = gatePass.getFacility().getPartyAddressByType(PartyAddressType.Code.SHIPPING.name()).getStateCode().equals(
                gatePass.getToParty().getPartyAddressByType(PartyAddressType.Code.SHIPPING.name()).getStateCode());
        boolean sameGstNumber = (StringUtils.isBlank(gatePass.getFacility().getGstNumber()) || StringUtils.isBlank(gatePass.getToParty().getGstNumber())
                || gatePass.getFacility().getGstNumber().equals(gatePass.getToParty().getGstNumber()));
        if (type.equals(OutboundGatePass.Type.STOCK_TRANSFER)) {
            return sameStateMovement && !gatePass.isReturnAwaited() && sameGstNumber;
        } else if (type.equals(OutboundGatePass.Type.RETURNABLE)) {
            return sameStateMovement && !gatePass.isReturnAwaited();
        }
        return false;
    }

    @Override
    public void createGatepassInvoice(OutboundGatePass gatePass, ValidationContext context, Integer userId) {
        GenerateOrUpdateInvoiceRequest generateInvoiceRequest = prepareGenerateInvoiceRequest(gatePass, userId);
        GenerateOrUpdateInvoiceResponse generateInvoiceResponse = invoiceService.createInvoice(generateInvoiceRequest);
        if (generateInvoiceResponse.hasErrors()) {
            context.addErrors(generateInvoiceResponse.getErrors());
        } else {
            Invoice invoice = invoiceService.getInvoiceByCode(generateInvoiceResponse.getInvoiceCode());
            if (invoice == null) {
                context.addError(WsResponseCode.UNABLE_TO_CREATE_INVOICE, "Unable to create invoice for Gatepass");
            } else {
                Map<Integer, InvoiceItem> itemTypeIdToInvoiceItem = new HashMap<>(invoice.getInvoiceItems().size());
                boolean isReturnInvoice = invoice.getType().isReturn();
                if (!isReturnInvoice) {
                    gatePass.setInvoice(invoice);
                } else {
                    gatePass.setReturnInvoice(invoice);
                }
                invoice.getInvoiceItems().forEach(invoiceItem -> itemTypeIdToInvoiceItem.put(invoiceItem.getItemType().getId(), invoiceItem));
                for (OutboundGatePassItem outboundGatePassItem : gatePass.getOutboundGatePassItems()) {
                    if (itemTypeIdToInvoiceItem.get(outboundGatePassItem.getItemType().getId()) == null) {
                        context.addError(WsResponseCode.UNABLE_TO_CREATE_INVOICE, "Unable to create invoice for all the skus in Gatepass");
                        break;
                    } else {
                        if (!isReturnInvoice) {
                            outboundGatePassItem.setInvoiceItem(itemTypeIdToInvoiceItem.get(outboundGatePassItem.getItemType().getId()));
                        } else {
                            outboundGatePassItem.setReturnInvoiceItem(itemTypeIdToInvoiceItem.get(outboundGatePassItem.getItemType().getId()));
                        }
                    }
                }
            }
        }
    }

    private GenerateOrUpdateInvoiceRequest prepareGenerateInvoiceRequest(OutboundGatePass gatePass, Integer userId) {
        GenerateOrUpdateInvoiceRequest generateInvoiceRequest = new GenerateOrUpdateInvoiceRequest();
        WsInvoice wsInvoice = new WsInvoice();
        wsInvoice.setFromPartyCode(gatePass.getFacility().getCode());
        wsInvoice.setToPartyCode(gatePass.getToParty().getCode());
        wsInvoice.setDestinationStateCode(gatePass.getToParty().getPartyAddressByType(PartyAddressType.Code.SHIPPING.name()).getStateCode());
        wsInvoice.setDestinationCountryCode(gatePass.getToParty().getPartyAddressByType(PartyAddressType.Code.SHIPPING.name()).getCountryCode());
        wsInvoice.setUserId(userId);
        OutboundGatePass.Type type = OutboundGatePass.Type.valueOf(gatePass.getType());
        // Delivery challan to be issue for forward movement only
        if (isDeliveryChallanNeeded(gatePass)) {
            wsInvoice.setType(Invoice.Type.DELIVERY_CHALLAN);
        } else if (isReturnInvoiceNeeded(gatePass)) {
            wsInvoice.setType(Invoice.Type.GATEPASS_RETURN);
        } else if (type.equals(OutboundGatePass.Type.RETURN_TO_VENDOR)) {
            wsInvoice.setType(Invoice.Type.PURCHASE_RETURN);
        } else {
            wsInvoice.setType(Invoice.Type.GATEPASS);
        }

        wsInvoice.setSource(WsInvoice.Source.GATEPASS);
        Map<Integer, WsInvoiceItem> itemTypeIdToInvoiceItem = new HashMap<>();
        gatePass.getOutboundGatePassItems().forEach(gatepassItem -> {
            itemTypeIdToInvoiceItem.compute(gatepassItem.getItemType().getId(), (sku, invoiceItem) -> invoiceItem == null ? prepareWsInvoiceItem(gatepassItem, wsInvoice.getType())
                    : addGatepassItemToWsInvoiceItem(invoiceItem, gatepassItem));
        });

        itemTypeIdToInvoiceItem.values().forEach(wsInvoiceItem -> wsInvoice.getInvoiceItems().add(wsInvoiceItem));
        generateInvoiceRequest.setInvoice(wsInvoice);
        return generateInvoiceRequest;
    }

    private WsInvoiceItem addGatepassItemToWsInvoiceItem(WsInvoiceItem invoiceItem, OutboundGatePassItem gatepassItem) {
        BigDecimal unitPrice = gatepassItem.getUnitPrice();
        invoiceItem.setQuantity(invoiceItem.getQuantity() + gatepassItem.getQuantity());
        invoiceItem.setTotal(NumberUtils.multiply(unitPrice, invoiceItem.getQuantity()));
        return invoiceItem;
    }

    private WsInvoiceItem prepareWsInvoiceItem(OutboundGatePassItem gatepassItem, Invoice.Type type) {
        WsInvoiceItem wsInvoiceItem = new WsInvoiceItem();
        BigDecimal unitPrice = gatepassItem.getUnitPrice();
        ItemType itemType = catalogService.getNonBundledItemTypeById(gatepassItem.getItemType().getId());
        wsInvoiceItem.setSkuCode(itemType.getSkuCode());
        wsInvoiceItem.setQuantity(gatepassItem.getQuantity());
        wsInvoiceItem.setTotal(NumberUtils.multiply(unitPrice, wsInvoiceItem.getQuantity()));
        InvoiceItem invoiceItem = gatepassItem.getInvoiceItem();
        if (invoiceItem != null) {
            WsTaxPercentageDetail taxPercentageDetail = WsTaxPercentageDetail.prepareWsTaxPercentageDetail(invoiceItem);
            wsInvoiceItem.setTaxPercentageDetail(taxPercentageDetail);
        }
        return wsInvoiceItem;
    }

    public class GatepassItemDetails {
        int          quantity;
        List<String> itemCodes;

        public GatepassItemDetails(int sizeOfItemCodes) {
            quantity = 0;
            itemCodes = new ArrayList<>(sizeOfItemCodes);
        }

        public int getQuantity() {
            return quantity;
        }

        public void setQuantity(int quantity) {
            this.quantity = quantity;
        }

        public List<String> getItemCodes() {
            return itemCodes;
        }
    }
}
