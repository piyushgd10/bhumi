/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 30-Jun-2012
 *  @author vibhu
 */
package com.uniware.services.material;

import java.util.List;
import java.util.Map;

import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.template.Template;
import com.unifier.core.utils.DateUtils.DateRange;
import com.uniware.core.api.material.AddItemToGatePassRequest;
import com.uniware.core.api.material.AddItemToGatePassResponse;
import com.uniware.core.api.material.AddNonTraceableGatePassItemRequest;
import com.uniware.core.api.material.AddNonTraceableGatePassItemResponse;
import com.uniware.core.api.material.AddOrEditNonTraceableGatePassItemRequest;
import com.uniware.core.api.material.AddOrEditNonTraceableGatePassItemResponse;
import com.uniware.core.api.material.CompleteGatePassRequest;
import com.uniware.core.api.material.CompleteGatePassResponse;
import com.uniware.core.api.material.CreateGatePassRequest;
import com.uniware.core.api.material.CreateGatePassResponse;
import com.uniware.core.api.material.CreateInboundGatePassRequest;
import com.uniware.core.api.material.CreateInboundGatePassResponse;
import com.uniware.core.api.material.DiscardGatePassRequest;
import com.uniware.core.api.material.DiscardGatePassResponse;
import com.uniware.core.api.material.EditGatePassRequest;
import com.uniware.core.api.material.EditGatePassResponse;
import com.uniware.core.api.material.EditInboundGatePassRequest;
import com.uniware.core.api.material.EditInboundGatePassResponse;
import com.uniware.core.api.material.EditNonTraceableGatePassItemRequest;
import com.uniware.core.api.material.EditNonTraceableGatePassItemResponse;
import com.uniware.core.api.material.EditTraceableOutboundGatepassItemRequest;
import com.uniware.core.api.material.EditTraceableOutboundGatepassItemResponse;
import com.uniware.core.api.material.GetGatePassRequest;
import com.uniware.core.api.material.GetGatePassResponse;
import com.uniware.core.api.material.GetGatepassScannableItemDetailsRequest;
import com.uniware.core.api.material.GetGatepassScannableItemDetailsResponse;
import com.uniware.core.api.material.GetGatepassScannableItemSkuDetailRequest;
import com.uniware.core.api.material.GetGatepassScannableItemSkuDetailResponse;
import com.uniware.core.api.material.GetGatepassSummaryRequest;
import com.uniware.core.api.material.GetGatepassSummaryResponse;
import com.uniware.core.api.material.GetInboundGatePassRequest;
import com.uniware.core.api.material.GetInboundGatePassResponse;
import com.uniware.core.api.material.GetOutboundGatepassTypesRequest;
import com.uniware.core.api.material.GetOutboundGatepassTypesResponse;
import com.uniware.core.api.material.RemoveItemFromGatePassRequest;
import com.uniware.core.api.material.RemoveItemFromGatePassResponse;
import com.uniware.core.api.material.RemoveNonTraceableGatePassItemRequest;
import com.uniware.core.api.material.RemoveNonTraceableGatePassItemResponse;
import com.uniware.core.api.material.SearchGatePassRequest;
import com.uniware.core.api.material.SearchGatePassResponse;
import com.uniware.core.entity.OutboundGatePass;
import com.uniware.core.entity.OutboundGatePassItem;
import com.uniware.core.entity.OutboundGatePassStatus;
import com.uniware.services.material.impl.MaterialServiceImpl;

public interface IMaterialService {

    CreateGatePassResponse createGatePass(CreateGatePassRequest request);

    CompleteGatePassResponse completeGatePass(CompleteGatePassRequest request);

    Map<String, MaterialServiceImpl.GatepassItemDetails> populateSkuToGatepassItemDetailsMap(
            List<OutboundGatePassItem> outboundGatePassItems);

    AddItemToGatePassResponse addItemToGatePass(AddItemToGatePassRequest request);

    SearchGatePassResponse searchGatePass(SearchGatePassRequest request);

    GetGatePassResponse getGatePass(GetGatePassRequest request);

    OutboundGatePass getGatePassByCode(String gatePassCode, boolean lock);

    DiscardGatePassResponse discardGatePass(DiscardGatePassRequest request);

    EditGatePassResponse editGatePass(EditGatePassRequest request);

    AddOrEditNonTraceableGatePassItemResponse addOrEditNonTraceableGatePassItem(AddOrEditNonTraceableGatePassItemRequest request);

    OutboundGatePass getGatePassByInvoiceId(Integer invoiceId);

    String getGatePassHtml(String gatePassCode, Template template);

    CreateInboundGatePassResponse createInboundGatePass(CreateInboundGatePassRequest request);

    EditInboundGatePassResponse editInboundGatePass(EditInboundGatePassRequest request);

    List<OutboundGatePass> getOutboundGatepassByRange(DateRange dateRange, String vendorCode);

    GetInboundGatePassResponse getInboundGatePass(GetInboundGatePassRequest request);

    List<OutboundGatePassStatus> getOutboundGatePassStatuses();

    OutboundGatePass getGatePassById(int outboundGatepassId);

    RemoveItemFromGatePassResponse removeItemFromGatePass(RemoveItemFromGatePassRequest request);

    OutboundGatePass getLastGatePassByItemId(int itemId);

    GetOutboundGatepassTypesResponse getOutboundGatepassTypes(GetOutboundGatepassTypesRequest request);

    GetGatepassSummaryResponse getGatepassSummary(GetGatepassSummaryRequest request);

    GetGatepassScannableItemDetailsResponse getGatepassScannableItemDetail(GetGatepassScannableItemDetailsRequest request);

    GetGatepassScannableItemSkuDetailResponse getGatepassScannableItemSkuDetails(GetGatepassScannableItemSkuDetailRequest request);

    EditNonTraceableGatePassItemResponse editNonTraceableGatePassItem(EditNonTraceableGatePassItemRequest request);

    AddNonTraceableGatePassItemResponse addNonTraceableGatePassItem(AddNonTraceableGatePassItemRequest request);

    RemoveNonTraceableGatePassItemResponse removeNonTraceableGatePassItem(RemoveNonTraceableGatePassItemRequest request);

    EditTraceableOutboundGatepassItemResponse editTraceableOutboundGatepassItem(EditTraceableOutboundGatepassItemRequest request);

    boolean isReturnInvoiceNeeded(OutboundGatePass gatePass);

    void createGatepassInvoice(OutboundGatePass gatePass, ValidationContext context, Integer userId);
}
