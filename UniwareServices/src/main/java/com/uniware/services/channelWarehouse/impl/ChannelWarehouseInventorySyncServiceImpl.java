package com.uniware.services.channelWarehouse.impl;

import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.entity.AsyncScriptInstruction;
import com.unifier.core.entity.AsyncScriptInstructionParameter;
import com.unifier.core.jms.MessagingConstants;
import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.JibxUtils;
import com.unifier.core.utils.StringUtils;
import com.unifier.core.utils.XMLParser;
import com.unifier.scraper.sl.runtime.IScriptProvider;
import com.unifier.scraper.sl.runtime.ScraperScript;
import com.unifier.scraper.sl.runtime.ScriptExecutionContext;
import com.unifier.services.tasks.IAsyncScriptService;
import com.unifier.services.tasks.engine.callback.IResumable;
import com.unifier.services.tenantprofile.service.ITenantProfileService;
import com.uniware.core.api.channelWarehouse.CreateChannelWarehouseInventoryRequest;
import com.uniware.core.api.channelWarehouse.CreateChannelWarehouseInventoryResponse;
import com.uniware.core.api.channelWarehouse.SyncChannelWarehouseInventoryRequest;
import com.uniware.core.api.channelWarehouse.SyncChannelWarehouseInventoryResponse;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.concurrent.ContextAwareExecutorFactory;
import com.uniware.core.entity.Channel;
import com.uniware.core.entity.Source;
import com.uniware.core.locking.ILockingService;
import com.uniware.core.locking.Namespace;
import com.uniware.core.utils.ActivityContext;
import com.uniware.core.utils.UserContext;
import com.uniware.core.vo.ChannelWarehouseInventorySyncStatusVO;
import com.uniware.services.audit.impl.ActivityEntityEnum;
import com.uniware.services.audit.impl.ActivityTypeEnum;
import com.uniware.services.audit.impl.ActivityUtils;
import com.uniware.services.cache.ChannelCache;
import com.uniware.services.cache.ScriptVersionedCache;
import com.uniware.services.channel.IChannelService;
import com.uniware.services.channel.impl.ChannelServiceImpl;
import com.uniware.services.channelWarehouse.IChannelWarehouseInventoryService;
import com.uniware.services.channelWarehouse.IChannelWarehouseInventorySyncService;
import com.uniware.services.configuration.SourceConfiguration;
import com.uniware.services.messaging.jms.ActiveMQConnector;
import com.uniware.services.messaging.jms.SyncChannelWarehouseInventoryEvent;
import com.uniware.services.tasks.imports.ChannelCatalogSyncTask;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import org.jibx.runtime.JiBXException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 * Created by digvijaysharma on 20/01/17.
 */
@Service("channelWarehouseInventorySyncService") public class ChannelWarehouseInventorySyncServiceImpl implements IChannelWarehouseInventorySyncService, IResumable {

    private static final Logger LOG = LoggerFactory.getLogger(ChannelWarehouseInventorySyncServiceImpl.class);

    public static final String TOTAL_PAGES = "TotalPages";

    private static final String SERVICE_NAME = "channelWarehouseInventorySyncService";

    @Autowired private IChannelService channelService;

    @Autowired private IChannelWarehouseInventoryService channelWarehouseInventoryService;

    @Autowired private ITenantProfileService tenantProfileService;

    @Autowired private IAsyncScriptService asyncScriptTaskService;

    @Autowired private ILockingService lockingService;

    @Autowired @Qualifier("activeMQConnector1") private ActiveMQConnector activeMQConnector;

    @Autowired private ContextAwareExecutorFactory executorFactory;

    @Override public SyncChannelWarehouseInventoryResponse syncChannelWarehouseInventory(SyncChannelWarehouseInventoryRequest request) {
        SyncChannelWarehouseInventoryResponse response = new SyncChannelWarehouseInventoryResponse();
        ValidationContext context = request.validate();
        String channelCode = request.getChannelCode();
        if (!context.hasErrors()) {
            Channel channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelByCode(channelCode);
            if (channel == null) {
                response.getResultItems().add(new SyncChannelWarehouseInventoryResponse.ChannelWarehouseInventorySyncResponseDTO(channelCode, "Invalid channel code"));
                context.addError(WsResponseCode.INVALID_REQUEST, "Invalid channel code");
            } else if (!channel.isEnabled()) {
                response.getResultItems().add(new SyncChannelWarehouseInventoryResponse.ChannelWarehouseInventorySyncResponseDTO(channelCode, "Channel disabled"));
                context.addError(WsResponseCode.INVALID_REQUEST, "Channel disabled");
            } else if (!Channel.SyncStatus.ON.equals(channel.getInventorySyncStatus())) {
                response.getResultItems().add(new SyncChannelWarehouseInventoryResponse.ChannelWarehouseInventorySyncResponseDTO(channelCode, "Sync not active"));
                context.addError(WsResponseCode.INVALID_REQUEST, "Sync not active");
            } else {
                SyncChannelWarehouseInventoryEvent syncChannelWarehouseInventoryEvent = new SyncChannelWarehouseInventoryEvent();
                syncChannelWarehouseInventoryEvent.setChannelCode(channelCode);
                syncChannelWarehouseInventoryEvent.setRequestTimestamp(DateUtils.getCurrentTime());
                activeMQConnector.produceContextAwareMessage(MessagingConstants.Queue.CHANNEL_WAREHOUSE_INVENTORY_SYNC_QUEUE, syncChannelWarehouseInventoryEvent);
                response.setSuccessful(true);
                response.setMessage("Request queued");
                LOG.info("Queued SyncChannelWarehouseInventoryEvent for channel: {}", channel.getCode());
            }
        }
        response.addWarnings(context.getWarnings());
        response.addErrors(context.getErrors());
        response.setSuccessful(!context.hasErrors());
        return response;
    }

    @Override public void syncChannelWarehouseInventory(SyncChannelWarehouseInventoryEvent event) {
        Channel channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelByCode(event.getChannelCode());
        Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(channel.getSourceCode());
        java.util.concurrent.locks.Lock lock = lockingService.getLock(Namespace.CHANNEL_WAREHOUSE_INVENTORY_DO, channel.getCode());
        boolean acquired = false;
        try {
            acquired = lock.tryLock();
            if (!acquired) {
                LOG.warn("Failed to obtain {} lock for channel {}. Sync is already running.", Namespace.CHANNEL_WAREHOUSE_INVENTORY_DO.name(), channel.getCode());
            } else {
                ChannelWarehouseInventorySyncStatusVO channelWarehouseInventorySyncStatus = channelService.getChannelWarehouseInventorySyncStatus(channel.getCode());
                if (channelWarehouseInventorySyncStatus.getLastSyncTime() != null && channelWarehouseInventorySyncStatus.getLastSyncTime().after(event.getRequestTimestamp())) {
                    LOG.warn("Ignoring SyncChannelWarehouseInventoryEvent as lastSyncTime: {} is newer than requestTimestamp: {}",
                            channelWarehouseInventorySyncStatus.getLastSyncTime(), event.getRequestTimestamp());
                } else {
                    boolean alreadyRunning = false;
                    if (source.isAsyncChannelWarehouseInventorySyncSupported() && Channel.SyncExecutionStatus.RUNNING.equals(
                            channelWarehouseInventorySyncStatus.getSyncExecutionStatus())) {
                        LOG.info("Checking if there is any pending async instruction");
                        AsyncScriptInstruction asyncScriptInstruction = asyncScriptTaskService.getPendingAsyncScriptInstruction("channel", channel.getCode(), this.getClass().getName());
                        if (asyncScriptInstruction != null) {
                            // A special check for channels like AMAZON in which we fetch the inventory report file asynchronously.
                            alreadyRunning = true;
                        }
                    }

                    if (!alreadyRunning) {
                        ScraperScript channelWarehouseInventorySyncScript = CacheManager.getInstance().getCache(ChannelCache.class).getScriptByName(channel.getCode(),
                                Source.CHANNEL_WAREHOUSE_INVENTORY_SCRIPT_NAME);
                        ScraperScript channelWarehouseInventoryPreprocessorScript = CacheManager.getInstance().getCache(ChannelCache.class).getScriptByName(channel.getCode(),
                                Source.CHANNEL_WAREHOUSE_INVENTORY_SYNC_PREPROCESSOR_SCRIPT_NAME);
                        if (channelWarehouseInventorySyncScript == null || channelWarehouseInventoryPreprocessorScript == null) {
                            LOG.error("Scripts missing for channel {}", channel.getName());
                        } else {
                            if (ActivityContext.current().isEnable() && StringUtils.isNotBlank(UserContext.current().getUniwareUserName())) {
                                ActivityUtils.appendActivity(channel.getCode(), ActivityEntityEnum.CHANNEL.getName(), channel.getSourceCode(),
                                        Arrays.asList(new String[] { "Channel Warehouse Inventory Sync was triggered" }), ActivityTypeEnum.SYNC_TRIGGERED.name());
                            }
                            runChannelWarehouseInventorySync(channel, channelWarehouseInventorySyncStatus, null);
                        }
                    } else {
                        LOG.warn("Ignoring SyncChannelWarehouseInventoryEvent as channel warehouse inventory sync is already running");
                    }
                }
            }
        } finally {
            if (acquired) {
                lock.unlock();
            }
        }
    }

    @Override public void resume(final AsyncScriptInstruction task) {
        executorFactory.getExecutor(SERVICE_NAME).submit(new Runnable() {
            @Override public void run() {
                String channelCode = null;
                try {
                    for (AsyncScriptInstructionParameter param : task.getInstructionParameters()) {
                        if (param.getName().equals("channel")) {
                            channelCode = (String) param.getValue();
                            break;
                        }
                    }
                    if (channelCode != null) {
                        Channel channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelByCode(channelCode);
                        ChannelWarehouseInventorySyncStatusVO channelWarehouseInventorySyncStatus = channelService.getChannelWarehouseInventorySyncStatus(channel.getCode());
                        runChannelWarehouseInventorySync(channel, channelWarehouseInventorySyncStatus, task);
                    }
                } catch (Throwable th) {
                    LOG.error("Error running AsyncScriptInstruction: " + task, th);
                }
            }
        });
    }

    private void runChannelWarehouseInventorySync(Channel channel, ChannelWarehouseInventorySyncStatusVO channelWarehouseInventorySyncStatus, AsyncScriptInstruction task) {
        LOG.info("Starting catalog sync for channel {}", channel.getName());
        channelWarehouseInventorySyncStatus.reset();
        channelWarehouseInventorySyncStatus.setLastSyncTime(DateUtils.getCurrentTime());
        channelWarehouseInventorySyncStatus.setSyncExecutionStatus(Channel.SyncExecutionStatus.RUNNING);
        channelWarehouseInventorySyncStatus.setMessage("Sync started");
        channelService.updateChannelWarehouseInventorySyncStatus(channelWarehouseInventorySyncStatus);

        Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(channel.getSourceCode());
        try {
            ChannelServiceImpl.ChannelGoodToSyncResponse channelGoodToSyncResponse = channelService.isChannelGoodToSync(channel.getCode(),
                    ChannelServiceImpl.Sync.CHANNEL_WAREHOUSE_INVENTORY);
            if (!channelGoodToSyncResponse.isGoodToSync()) {
                channelWarehouseInventorySyncStatus.setMessage("Connector is broken");
                LOG.warn("Connector broken for channel {}", channel.getName());
            } else {
                ChannelWarehouseInventorySyncPreProcessorResponse channelWarehouseInventorySyncPreProcessorResponse = runChannelWarehouseInventorySyncPreprocessor(channel,
                        channelGoodToSyncResponse.getParams(), task);
                if (!channelWarehouseInventorySyncPreProcessorResponse.isPreProcessingPending()) {
                    LOG.info("Pre-processing complete for channel: {}, Params: {}", channel.getName(), channelWarehouseInventorySyncPreProcessorResponse.getParams());
                    doRunChannelWarehouseInventorySync(channel, channelWarehouseInventorySyncStatus, channelGoodToSyncResponse.getParams(),
                            channelWarehouseInventorySyncPreProcessorResponse.getParams());
                } else {
                    LOG.info("Pre-processing pending for channel: {}", channel.getName());
                }
            }
        } catch (Throwable e) {
            LOG.error("Error executing channel warehouse inventory sync task for channel: {}, Message: {}", source.getName(), e.getMessage());
            channelWarehouseInventorySyncStatus.setMessage(e.getMessage());
            channelWarehouseInventorySyncStatus.setSyncExecutionStatus(Channel.SyncExecutionStatus.IDLE);
            channelWarehouseInventorySyncStatus.setSuccessful(false);
        } finally {
            channelWarehouseInventorySyncStatus.setLastSyncTime(DateUtils.getCurrentTime());
            channelWarehouseInventorySyncStatus.setSyncExecutionStatus(Channel.SyncExecutionStatus.IDLE);
            LOG.info("Updating last channel warehouse inventory sync time for channel {}", channel.getName());
            channelService.updateChannelWarehouseInventorySyncStatus(channelWarehouseInventorySyncStatus);
            LOG.info("Updated last channel warehouse inventory sync time for channel {}", channel.getName());
        }
        LOG.info("Completed channel warehouse inventory sync for channel {}", channel.getName());
    }

    private void doRunChannelWarehouseInventorySync(Channel channel, ChannelWarehouseInventorySyncStatusVO channelWarehouseInventorySyncStatus, Map<String, String> channelParams,
            Map<String, Object> preprocessorParams) {
        Integer pageNumber = 1;
        int successfulImports = 0, failedImports = 0;
        CreateChannelWarehouseInventoryResponse response = new CreateChannelWarehouseInventoryResponse();
        try {
            do {
                LOG.info("Importing page: {}", pageNumber);
                CreateChannelWarehouseInventoryRequest request = getChannelWarehouseInventories(channel, pageNumber, channelParams, preprocessorParams);
                request.setChannelId(channel.getId());
                response = channelWarehouseInventoryService.createChannelWarehouseInventory(request);
                successfulImports += response.getSuccessfulImports();
                failedImports += response.getFailedImports();
                pageNumber++;
            } while (response.isSuccessful() && response.isHasMoreResults());
        } catch (Exception e) {
            LOG.error("Error importing channel warehouse inventory", e);
        }
        channelWarehouseInventorySyncStatus.setSuccessfulImports(successfulImports);
        channelWarehouseInventorySyncStatus.setFailedImports(failedImports);
        channelService.updateChannelWarehouseInventorySyncStatus(channelWarehouseInventorySyncStatus);
        updateStatus(channelWarehouseInventorySyncStatus, channel, successfulImports, failedImports);
    }

    private CreateChannelWarehouseInventoryRequest getChannelWarehouseInventories(Channel channel, Integer pageNumber, Map<String, String> channelParams,
            Map<String, Object> metadata) throws JiBXException, IOException {
        ScriptExecutionContext context = ScriptExecutionContext.current();
        CreateChannelWarehouseInventoryResponse response = new CreateChannelWarehouseInventoryResponse();
        Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(channel.getSourceCode());
        ScraperScript channelWarehouseInventorySyncScript = CacheManager.getInstance().getCache(ChannelCache.class).getScriptByName(channel.getCode(),
                Source.CHANNEL_WAREHOUSE_INVENTORY_SCRIPT_NAME);
        context.addVariable("source", source);
        context.addVariable("channel", channel);
        context.addVariable("metadata", metadata);
        context.addVariable("pageNumber", pageNumber);
        for (Map.Entry<String, String> e : channelParams.entrySet()) {
            context.addVariable(e.getKey(), e.getValue());
        }
        try {
            context.setScriptProvider(new IScriptProvider() {
                @Override public ScraperScript getScript(String scriptName) {
                    return CacheManager.getInstance().getCache(ScriptVersionedCache.class).getScriptByName(scriptName);
                }
            });
            context.setTraceLoggingEnabled(UserContext.current().isTraceLoggingEnabled());
            channelWarehouseInventorySyncScript.execute();
            String channelWarehouseInventoryXml = context.getScriptOutput();
            LOG.info("ChannelWarehouseInventoryXml : " + channelWarehouseInventoryXml);
            XMLParser.Element rootElement = XMLParser.parse(channelWarehouseInventoryXml);
            Integer totalPages = Integer.parseInt(rootElement.text(TOTAL_PAGES));
            response.setHasMoreResults(totalPages > pageNumber);
            response.setTotalPages(totalPages);
            return JibxUtils.unmarshall("unicommerceServicesBinding17", CreateChannelWarehouseInventoryRequest.class, channelWarehouseInventoryXml);
        } finally {
            ScriptExecutionContext.destroy();
        }
    }

    private ChannelWarehouseInventorySyncPreProcessorResponse runChannelWarehouseInventorySyncPreprocessor(Channel channel, Map<String, String> channelParams,
            AsyncScriptInstruction asyncScriptInstruction) {
        ChannelWarehouseInventorySyncPreProcessorResponse channelWarehouseInventorySyncPreProcessorResponse = new ChannelWarehouseInventorySyncPreProcessorResponse();
        if (StringUtils.isNotBlank(CacheManager.getInstance().getCache(ChannelCache.class).getScriptName(channel.getCode(),
                Source.CHANNEL_WAREHOUSE_INVENTORY_SYNC_PREPROCESSOR_SCRIPT_NAME))) {
            ScraperScript channelWarehouseInventorySyncPreprocessorScript = CacheManager.getInstance().getCache(ChannelCache.class).getScriptByName(channel.getCode(),
                    com.uniware.core.entity.Source.CHANNEL_WAREHOUSE_INVENTORY_SYNC_PREPROCESSOR_SCRIPT_NAME);
            if (channelWarehouseInventorySyncPreprocessorScript != null) {
                Map<String, Object> resultItems = new HashMap<>();
                ScriptExecutionContext context = ScriptExecutionContext.current();
                String instructionName = null;
                if (asyncScriptInstruction != null) {
                    instructionName = asyncScriptInstruction.getInstructionName();
                    for (AsyncScriptInstructionParameter param : asyncScriptInstruction.getInstructionParameters()) {
                        context.addVariable(param.getName(), param.getValue());
                    }
                }
                context.addVariable("resultItems", resultItems);
                context.addVariable("channel", channel);
                for (Map.Entry<String, String> e : channelParams.entrySet()) {
                    context.addVariable(e.getKey(), e.getValue());
                }
                context.setScriptProvider(new IScriptProvider() {
                    @Override public ScraperScript getScript(String scriptName) {
                        return CacheManager.getInstance().getCache(ScriptVersionedCache.class).getScriptByName(scriptName);
                    }
                });
                context.setTraceLoggingEnabled(UserContext.current().isTraceLoggingEnabled());
                try {
                    LOG.info("Running channel warehouse inventory sync preprocessor for channel {}, instructionName {}", channel.getName(), instructionName);
                    channelWarehouseInventorySyncPreprocessorScript.execute(instructionName);
                    if (!context.getScheduleScriptContext().isEmpty()) {
                        asyncScriptTaskService.scheduleScriptTask(channelWarehouseInventorySyncPreprocessorScript.getScriptName(), channel, context, this.getClass(),
                                ChannelCatalogSyncTask.class.getName());
                        channelWarehouseInventorySyncPreProcessorResponse.setPreProcessingPending(true);
                    } else {
                        LOG.info("Pre-processing complete, resultItems: {}", resultItems);
                        channelWarehouseInventorySyncPreProcessorResponse.setParams(resultItems);
                    }
                    LOG.info("Completed channel warehouse inventory sync preprocessor for channel {}", channel.getName());
                } finally {
                    ScriptExecutionContext.destroy();
                }
            }
        }
        return channelWarehouseInventorySyncPreProcessorResponse;
    }

    private void updateStatus(ChannelWarehouseInventorySyncStatusVO channelWarehouseInventorySyncStatus, Channel channel, int successfulImportCount, int failedImportCount) {
        Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(channel.getSourceCode());
        String message = "Successful imports:" + successfulImportCount + ", failed imports:" + failedImportCount + ". Task Completed";
        channelWarehouseInventorySyncStatus.setSuccessful(failedImportCount == 0 ? true : false);
        LOG.info("Channel: {}, Result: {}", source.getName(), message);
        if (channelWarehouseInventorySyncStatus.getFailedImports() > 0) {
            channelWarehouseInventorySyncStatus.setLastSyncFailedNotificationTime(DateUtils.getCurrentTime());
        }
        channelWarehouseInventorySyncStatus.setLastSyncTime(DateUtils.getCurrentTime());
        channelWarehouseInventorySyncStatus.setSuccessfulImports(successfulImportCount);
        channelWarehouseInventorySyncStatus.setFailedImports(failedImportCount);
        channelWarehouseInventorySyncStatus.setMessage(message);
        channelWarehouseInventorySyncStatus.setSyncExecutionStatus(Channel.SyncExecutionStatus.IDLE);
        LOG.info("Channel warehouse inventory sync for channel {} completed with {}", channel.getName(), message);
        channelService.updateChannelWarehouseInventorySyncStatus(channelWarehouseInventorySyncStatus);
    }

    private static class ChannelWarehouseInventorySyncPreProcessorResponse {
        private boolean             preProcessingPending;
        private Map<String, Object> params;

        public boolean isPreProcessingPending() {
            return preProcessingPending;
        }

        public void setPreProcessingPending(boolean preProcessingPending) {
            this.preProcessingPending = preProcessingPending;
        }

        public Map<String, Object> getParams() {
            return params;
        }

        public void setParams(Map<String, Object> params) {
            this.params = params;
        }
    }

}

