package com.uniware.services.channelWarehouse.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.utils.DateUtils;
import com.uniware.core.api.channelWarehouse.CreateChannelWarehouseInventoryRequest;
import com.uniware.core.api.channelWarehouse.CreateChannelWarehouseInventoryResponse;
import com.uniware.core.api.channelWarehouse.WsChannelWarehouseInventory;
import com.uniware.core.entity.Channel;
import com.uniware.core.entity.ChannelItemType;
import com.uniware.core.entity.ChannelWarehouseInventory;
import com.uniware.core.entity.Facility;
import com.uniware.dao.channel.IChannelCatalogDao;
import com.uniware.dao.channelWarehouse.IChannelWarehouseInventoryDao;
import com.uniware.services.cache.ChannelCache;
import com.uniware.services.channelWarehouse.IChannelWarehouseInventoryService;

/**
 * Created by digvijaysharma on 23/01/17.
 */
@Service("channelWarehouseInventoryService") public class ChannelWarehouseInventoryServiceImpl implements IChannelWarehouseInventoryService {

    @Autowired private IChannelWarehouseInventoryDao channelWarehouseInventoryDao;

    @Autowired private IChannelCatalogDao channelCatalogDao;

    private static final Logger LOG = LoggerFactory.getLogger(ChannelWarehouseInventoryServiceImpl.class);

    @Override
    @Transactional
    public CreateChannelWarehouseInventoryResponse createChannelWarehouseInventory(CreateChannelWarehouseInventoryRequest request) {
        CreateChannelWarehouseInventoryResponse response = new CreateChannelWarehouseInventoryResponse();
        response.setSuccessfulImports(0);
        response.setFailedImports(0);
        int successfulImportCount = 0, failedImportCount = 0;
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            for (WsChannelWarehouseInventory inventory : request.getChannelWarehouseInventories()) {
                LOG.info("Importing channel warehouse inventory {}", inventory.toString());
                ChannelItemType channelItemType = channelCatalogDao.getChannelItemTypeByChannelAndSellerSku(request.getChannelId(), inventory.getSellerSkuCode());
                if (channelItemType == null || !channelItemType.getStatusCode().equals(ChannelItemType.Status.LINKED)) {
                    LOG.error("Seller Sku Code does not exist : {}", inventory.getSellerSkuCode());
                } else {
                    Channel channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelById(request.getChannelId());
                    doCreateChannelWarehouseInventory(channel.getAssociatedFacility(), inventory, channelItemType);
                }
                if (!context.hasErrors()) {
                    successfulImportCount++;
                } else {
                    failedImportCount++;
                    LOG.error("Error creating channel warehouse inventory {}", inventory);
                }
            }
        }
        if (successfulImportCount > 0) {
            response.setSuccessful(true);
        }
        response.setFailedImports(failedImportCount);
        response.setSuccessfulImports(successfulImportCount);
        response.setErrors(context.getErrors());
        return response;
    }

    @Transactional private void doCreateChannelWarehouseInventory(Facility facility, WsChannelWarehouseInventory inventory, ChannelItemType channelItemType) {
        ChannelWarehouseInventory channelWarehouseInventory = prepareChannelWarehouseInventory(channelItemType, inventory);
        channelWarehouseInventoryDao.addChannelWarehouseInventory(facility, channelWarehouseInventory);
    }

    public ChannelWarehouseInventory prepareChannelWarehouseInventory(ChannelItemType channelItemType, WsChannelWarehouseInventory channelWarehouseInventory) {
        ChannelWarehouseInventory inventory = new ChannelWarehouseInventory();
        inventory.setCreated(DateUtils.getCurrentTime());
        inventory.setFulfillableQuantity(channelWarehouseInventory.getFulfillableQuantity());
        inventory.setUnfulfillableQuantity(channelWarehouseInventory.getUnfulfillableQuantity());
        inventory.setTotalQuantity(channelWarehouseInventory.getTotalQuantity());
        inventory.setReservedQuantity(channelWarehouseInventory.getReservedQuantity());
        inventory.setInboundQuantity(channelWarehouseInventory.getInboundQuantity());
        inventory.setChannelItemType(channelItemType);
        return inventory;
    }

}
