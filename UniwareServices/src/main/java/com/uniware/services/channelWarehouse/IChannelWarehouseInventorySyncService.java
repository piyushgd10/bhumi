package com.uniware.services.channelWarehouse;

import com.uniware.core.api.channelWarehouse.SyncChannelWarehouseInventoryRequest;
import com.uniware.core.api.channelWarehouse.SyncChannelWarehouseInventoryResponse;
import com.uniware.services.messaging.jms.SyncChannelWarehouseInventoryEvent;

/**
 * Created by digvijaysharma on 20/01/17.
 */
public interface IChannelWarehouseInventorySyncService {
    SyncChannelWarehouseInventoryResponse syncChannelWarehouseInventory(SyncChannelWarehouseInventoryRequest request);

    void syncChannelWarehouseInventory(SyncChannelWarehouseInventoryEvent event);
}
