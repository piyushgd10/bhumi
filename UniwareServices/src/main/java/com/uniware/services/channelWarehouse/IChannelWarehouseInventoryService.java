package com.uniware.services.channelWarehouse;

import com.uniware.core.api.channelWarehouse.CreateChannelWarehouseInventoryRequest;
import com.uniware.core.api.channelWarehouse.CreateChannelWarehouseInventoryResponse;

/**
 * Created by digvijaysharma on 23/01/17.
 */
public interface IChannelWarehouseInventoryService {
    CreateChannelWarehouseInventoryResponse createChannelWarehouseInventory(
            CreateChannelWarehouseInventoryRequest request);
}
