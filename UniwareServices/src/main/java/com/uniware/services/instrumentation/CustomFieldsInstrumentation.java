/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jun 22, 2012
 *  @author singla
 */
package com.uniware.services.instrumentation;

import com.unifier.core.entity.AccessPattern;

import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.IllegalClassFormatException;
import java.security.ProtectionDomain;

import javassist.ClassPool;
import javassist.CtBehavior;
import javassist.CtClass;
import javassist.CtMethod;

/**
 * @author singla
 */
public class CustomFieldsInstrumentation implements ClassFileTransformer {

    /* (non-Javadoc)
     * @see java.lang.instrument.ClassFileTransformer#transform(java.lang.ClassLoader, java.lang.String, java.lang.Class, java.security.ProtectionDomain, byte[])
     */
    @Override
    public byte[] transform(ClassLoader arg0, String arg1, Class<?> clazz, ProtectionDomain arg3, byte[] b) throws IllegalClassFormatException {
        if (clazz.getName().equals(AccessPattern.class.getName())) {
            ClassPool pool = ClassPool.getDefault();
            CtClass cl = null;
            try {
                System.out.println("Instrumenting class :" + AccessPattern.class.getName());
                cl = pool.makeClass(new java.io.ByteArrayInputStream(b));
                String minfo = "public void doSomething() {}";
                CtMethod.make(minfo, cl);
                CtBehavior[] methods = cl.getDeclaredBehaviors();
                for (int i = 0; i < methods.length; i++) {
                    if (methods[i].isEmpty() == false) {
                    }
                }
                b = cl.toBytecode();
            } catch (Exception e) {
                System.err.println("Could not instrument  " + clazz.getName() + ",  exception : " + e.getMessage());
            } finally {
                if (cl != null) {
                    cl.detach();
                }
            }
            return b;
        }
        return b;
    }
}
