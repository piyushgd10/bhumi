/*
 *  Copyright 2013 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 24-Dec-2013
 *  @author sunny
 */
package com.uniware.services.reload.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unifier.core.api.tasks.ReloadConfigurationRequest;
import com.unifier.core.api.tasks.ReloadConfigurationResponse;
import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.transport.http.HttpClientFactory;
import com.unifier.core.utils.StringUtils;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.concurrent.ContextAwareExecutorFactory;
import com.uniware.services.reload.IReloadService;

@Service
public class ReloadServiceImpl implements IReloadService {

    private static final Logger         LOG = LoggerFactory.getLogger(ReloadServiceImpl.class);

    @Autowired
    private ContextAwareExecutorFactory executorFactory;

    @Override
    public void reloadAll() {
        CacheManager.getInstance().clearTenantCaches();
        ConfigurationManager.getInstance().clearTenantConfigurations();
    }

    private void reloadConfigurationInternal(String reloadType) {
        try {
            if ("All".equals(reloadType)) {
                reloadAll();
            } else if ("Executors".equals(reloadType)) {
                executorFactory.resetExecutors();
            } else if (reloadType.toLowerCase().contains("configuration")) {
                ConfigurationManager.getInstance().markConfigurationDirty(reloadType);
            } else if (reloadType.toLowerCase().contains("httpclient")) {
                HttpClientFactory.clearCache();
            } else {
                CacheManager.getInstance().markCacheDirty(reloadType);
            }
        } catch (Exception e) {
            throw new IllegalArgumentException("Unable to load cache: " + e.getMessage());
        }
    }

    @Override
    public ReloadConfigurationResponse reload(ReloadConfigurationRequest request) {
        ReloadConfigurationResponse response = new ReloadConfigurationResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            if (StringUtils.isNotEmpty(request.getType())) {
                List<String> reloadTypes = StringUtils.split(request.getType(), ",");
                try {
                    if (reloadTypes.contains("All")) {
                        reloadConfigurationInternal("All");
                    } else {
                        for (String type : reloadTypes) {
                            reloadConfigurationInternal(type);
                        }
                    }
                } catch (IllegalArgumentException e) {
                    context.addError(WsResponseCode.INVALID_API_REQUEST);
                }
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        } else {
            response.setSuccessful(true);
        }
        return response;
    }
}
