/*
 *  Copyright 2013 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 24-Dec-2013
 *  @author sunny
 */
package com.uniware.services.reload;

import com.unifier.core.api.tasks.ReloadConfigurationRequest;
import com.unifier.core.api.tasks.ReloadConfigurationResponse;

public interface IReloadService {

    ReloadConfigurationResponse reload(ReloadConfigurationRequest request);

    void reloadAll();

}
