package com.uniware.services.cyclecount;

import com.unifier.core.api.base.ServiceResponse;
import com.uniware.core.api.cyclecount.AddShelfToSubCycleCountRequest;
import com.uniware.core.api.cyclecount.AddShelfToSubCycleCountResponse;
import com.uniware.core.api.cyclecount.BlockShelvesForCycleCountRequest;
import com.uniware.core.api.cyclecount.BlockShelvesForCycleCountResponse;
import com.uniware.core.api.cyclecount.CloseCycleCountRequest;
import com.uniware.core.api.cyclecount.CloseCycleCountResponse;
import com.uniware.core.api.cyclecount.CompleteCountForShelfRequest;
import com.uniware.core.api.cyclecount.CompleteCountForShelfResponse;
import com.uniware.core.api.cyclecount.CreateCycleCountRequest;
import com.uniware.core.api.cyclecount.CreateCycleCountResponse;
import com.uniware.core.api.cyclecount.CreateManualSubCycleCountRequest;
import com.uniware.core.api.cyclecount.CreateManualSubCycleCountResponse;
import com.uniware.core.api.cyclecount.CreateSemiAutomaticSubCycleCountRequest;
import com.uniware.core.api.cyclecount.CreateSubCycleCountRequest;
import com.uniware.core.api.cyclecount.CreateSubCycleCountResponse;
import com.uniware.core.api.cyclecount.EditCycleCountRequest;
import com.uniware.core.api.cyclecount.EditCycleCountResponse;
import com.uniware.core.api.cyclecount.GetActiveCycleCountRequest;
import com.uniware.core.api.cyclecount.GetBadItemsForCycleCountRequest;
import com.uniware.core.api.cyclecount.GetBadItemsForShelfRequest;
import com.uniware.core.api.cyclecount.GetBadItemsForShelfResponse;
import com.uniware.core.api.cyclecount.GetCycleCountResponse;
import com.uniware.core.api.cyclecount.GetErrorItemsForCycleCountResponse;
import com.uniware.core.api.cyclecount.GetItemRequest;
import com.uniware.core.api.cyclecount.GetItemResponse;
import com.uniware.core.api.cyclecount.GetShelfDetailsRequest;
import com.uniware.core.api.cyclecount.GetShelfDetailsResponse;
import com.uniware.core.api.cyclecount.GetSubCycleCountShelvesRequest;
import com.uniware.core.api.cyclecount.GetSubCycleCountShelvesResponse;
import com.uniware.core.api.cyclecount.GetZonesForCycleCountRequest;
import com.uniware.core.api.cyclecount.GetZonesForCycleCountResponse;
import com.uniware.core.api.cyclecount.RecountShelfRequest;
import com.uniware.core.api.cyclecount.RecountShelfResponse;
import com.uniware.core.api.cyclecount.RemoveShelvesFromCycleCountRequest;
import com.uniware.core.api.cyclecount.RemoveShelvesFromCycleCountResponse;
import com.uniware.core.api.cyclecount.SearchCycleCountShelfRequest;
import com.uniware.core.api.cyclecount.SearchCycleCountShelfResponse;
import com.uniware.core.api.cyclecount.StartCountingForShelfRequest;
import com.uniware.core.api.cyclecount.StartCountingForShelfResponse;
import com.uniware.core.api.cyclecount.UnblockShelvesRequest;
import com.uniware.core.api.cyclecount.UnblockShelvesResponse;
import com.uniware.core.entity.CycleCount;
import com.uniware.core.entity.CycleCountItem;
import java.util.List;

/**
 * Created by harshpal on 2/16/16.
 */
public interface ICycleCountService {

    CreateCycleCountResponse createCycleCount(CreateCycleCountRequest request);

    EditCycleCountResponse editCycleCount(EditCycleCountRequest request);

    CreateSubCycleCountResponse createAutomaticSubCycleCount(CreateSubCycleCountRequest request);

    CreateManualSubCycleCountResponse createManualSubCycleCount(CreateManualSubCycleCountRequest request);

    CreateSubCycleCountResponse createSemiAutomaticSubCycleCount(CreateSemiAutomaticSubCycleCountRequest request);

    AddShelfToSubCycleCountResponse addShelfToSubCycleCount(AddShelfToSubCycleCountRequest request);

    RecountShelfResponse recountShelf(RecountShelfRequest request);

    UnblockShelvesResponse unblockShelves(UnblockShelvesRequest request);

    RemoveShelvesFromCycleCountResponse removeShelvesFromCycleCount(RemoveShelvesFromCycleCountRequest request);

    BlockShelvesForCycleCountResponse blockShelvesForCycleCountRequest(BlockShelvesForCycleCountRequest request);

    StartCountingForShelfResponse startCountingForShelf(StartCountingForShelfRequest request);

    CompleteCountForShelfResponse completeCountForShelf(CompleteCountForShelfRequest request);

    GetCycleCountResponse getActiveCycleCount(GetActiveCycleCountRequest request);

    CycleCount getActiveCycleCount();

    GetShelfDetailsResponse getShelfDetails(GetShelfDetailsRequest request);

    GetZonesForCycleCountResponse getZonesForCycleCount(GetZonesForCycleCountRequest request);

    ServiceResponse preProcessShelvesForCycleCount();

    SearchCycleCountShelfResponse searchCycleCountShelf(SearchCycleCountShelfRequest request);

    GetSubCycleCountShelvesResponse getSubCycleCount(GetSubCycleCountShelvesRequest request);

    GetItemResponse getItem(GetItemRequest request);

    GetErrorItemsForCycleCountResponse getErrorItemsForCycleCount(GetBadItemsForCycleCountRequest request);

    GetBadItemsForShelfResponse getErrorItemsForShelf(GetBadItemsForShelfRequest request);

    List<CycleCountItem> getUnreconciledCycleCountItemsForShelf(Integer shelfId);

    CloseCycleCountResponse closeCycleCount(CloseCycleCountRequest request);
}
