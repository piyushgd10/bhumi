package com.uniware.services.cyclecount.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateOptimisticLockingFailureException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.unifier.core.annotation.Level;
import com.unifier.core.annotation.audit.LogActivity;
import com.unifier.core.api.base.ServiceResponse;
import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.entity.User;
import com.unifier.core.utils.CollectionUtils;
import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.StringUtils;
import com.unifier.services.aspect.RollbackOnFailure;
import com.unifier.services.users.IUsersService;
import com.uniware.core.api.cyclecount.AddShelfToSubCycleCountRequest;
import com.uniware.core.api.cyclecount.AddShelfToSubCycleCountResponse;
import com.uniware.core.api.cyclecount.BlockShelvesForCycleCountRequest;
import com.uniware.core.api.cyclecount.BlockShelvesForCycleCountResponse;
import com.uniware.core.api.cyclecount.CloseCycleCountRequest;
import com.uniware.core.api.cyclecount.CloseCycleCountResponse;
import com.uniware.core.api.cyclecount.CompleteCountForShelfRequest;
import com.uniware.core.api.cyclecount.CompleteCountForShelfResponse;
import com.uniware.core.api.cyclecount.CreateCycleCountRequest;
import com.uniware.core.api.cyclecount.CreateCycleCountResponse;
import com.uniware.core.api.cyclecount.CreateManualSubCycleCountRequest;
import com.uniware.core.api.cyclecount.CreateManualSubCycleCountResponse;
import com.uniware.core.api.cyclecount.CreateSemiAutomaticSubCycleCountRequest;
import com.uniware.core.api.cyclecount.CreateSubCycleCountRequest;
import com.uniware.core.api.cyclecount.CreateSubCycleCountResponse;
import com.uniware.core.api.cyclecount.CycleCountDTO;
import com.uniware.core.api.cyclecount.EditCycleCountRequest;
import com.uniware.core.api.cyclecount.EditCycleCountResponse;
import com.uniware.core.api.cyclecount.ErrorItemDTO;
import com.uniware.core.api.cyclecount.GetActiveCycleCountRequest;
import com.uniware.core.api.cyclecount.GetBadItemsForCycleCountRequest;
import com.uniware.core.api.cyclecount.GetBadItemsForShelfRequest;
import com.uniware.core.api.cyclecount.GetBadItemsForShelfResponse;
import com.uniware.core.api.cyclecount.GetCycleCountResponse;
import com.uniware.core.api.cyclecount.GetErrorItemsForCycleCountResponse;
import com.uniware.core.api.cyclecount.GetItemRequest;
import com.uniware.core.api.cyclecount.GetItemResponse;
import com.uniware.core.api.cyclecount.GetShelfDetailsRequest;
import com.uniware.core.api.cyclecount.GetShelfDetailsResponse;
import com.uniware.core.api.cyclecount.GetSubCycleCountShelvesRequest;
import com.uniware.core.api.cyclecount.GetSubCycleCountShelvesResponse;
import com.uniware.core.api.cyclecount.GetZonesForCycleCountRequest;
import com.uniware.core.api.cyclecount.GetZonesForCycleCountResponse;
import com.uniware.core.api.cyclecount.RecountShelfRequest;
import com.uniware.core.api.cyclecount.RecountShelfResponse;
import com.uniware.core.api.cyclecount.RemoveShelvesFromCycleCountRequest;
import com.uniware.core.api.cyclecount.RemoveShelvesFromCycleCountResponse;
import com.uniware.core.api.cyclecount.SearchCycleCountShelfRequest;
import com.uniware.core.api.cyclecount.SearchCycleCountShelfResponse;
import com.uniware.core.api.cyclecount.StartCountingForShelfRequest;
import com.uniware.core.api.cyclecount.StartCountingForShelfResponse;
import com.uniware.core.api.cyclecount.SubCycleCountDTO;
import com.uniware.core.api.cyclecount.UnblockShelvesRequest;
import com.uniware.core.api.cyclecount.UnblockShelvesResponse;
import com.uniware.core.api.saleorder.UnblockSaleOrderItemsInventoryRequest;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.entity.CycleCount;
import com.uniware.core.entity.CycleCountErrorItem;
import com.uniware.core.entity.CycleCountItem;
import com.uniware.core.entity.CycleCountShelfSummary;
import com.uniware.core.entity.Item;
import com.uniware.core.entity.ItemTypeInventory;
import com.uniware.core.entity.PickSet;
import com.uniware.core.entity.SaleOrderItem;
import com.uniware.core.entity.Shelf;
import com.uniware.core.entity.SubCycleCount;
import com.uniware.core.locking.Namespace;
import com.uniware.core.locking.annotation.Lock;
import com.uniware.core.locking.annotation.Locks;
import com.uniware.core.utils.ActivityContext;
import com.uniware.core.utils.UserContext;
import com.uniware.dao.cyclecount.ICycleCountDao;
import com.uniware.dao.inventory.IInventoryDao;
import com.uniware.services.admin.pickset.IPicksetService;
import com.uniware.services.audit.impl.ActivityEntityEnum;
import com.uniware.services.audit.impl.ActivityUtils;
import com.uniware.services.catalog.ICatalogService;
import com.uniware.services.configuration.PickConfiguration;
import com.uniware.services.configuration.SystemConfiguration;
import com.uniware.services.configuration.SystemConfiguration.TraceabilityLevel;
import com.uniware.services.cyclecount.ICycleCountService;
import com.uniware.services.inventory.IInventoryService;
import com.uniware.services.putaway.IPutawayService;
import com.uniware.services.saleorder.ISaleOrderService;
import com.uniware.services.shipping.IShippingService;
import com.uniware.services.warehouse.IShelfService;

/**
 * Imported by aditya from SdUniware by aditya on 6/13/17.
 */
@Service("cycleCountService")
public class CycleCountServiceImpl implements ICycleCountService {

    private static final Logger LOG = LoggerFactory.getLogger(CycleCountServiceImpl.class);

    @Autowired
    private ICycleCountDao      cycleCountDao;

    @Autowired
    private IShelfService       shelfService;

    @Autowired
    private IPicksetService     picksetService;

    @Autowired
    private IInventoryService   inventoryService;

    @Autowired
    private IInventoryDao       inventoryDao;

    @Autowired
    private IShippingService    shippingService;

    @Autowired
    private IPutawayService     putawayService;

    @Autowired
    private ISaleOrderService   saleOrderService;

    @Autowired
    private IUsersService       usersService;

    @Autowired
    private ICatalogService     catalogService;

    /**
     * Creates a new cycle count. Fails if there already is an active cycle count in progress or if the entered target
     * completion date is in the past. Only works for ITEM level traceability.
     *
     * @param request contains target completion date of cycle count
     * @return Code of the created cycle count
     */
    @Override
    @Transactional
    @Locks({ @Lock(ns = Namespace.CYCLE_COUNT, level = Level.FACILITY, key = "CYCLE_COUNT") })
    public CreateCycleCountResponse createCycleCount(CreateCycleCountRequest request) {
        ValidationContext context = request.validate();
        CreateCycleCountResponse response = new CreateCycleCountResponse();
        TraceabilityLevel traceabilityLevel = ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).getTraceabilityLevel();
        if (!traceabilityLevel.equals(TraceabilityLevel.ITEM)) {
            context.addError(WsResponseCode.ACTION_APPLICABLE_FOR_TRACEABLE_ITEMS, "Can create cycle count only in ITEM level traceability facilities.");
        }
        if (!context.hasErrors()) {
            CycleCount activeCycleCount = getActiveCycleCount();
            if (activeCycleCount != null) {
                context.addError(WsResponseCode.CYCLE_COUNT_ALREADY_IN_PROGRESS, "A cycle count is already in progress.");
            } else if (!DateUtils.isFutureTime(request.getTargetCompletionDate())) {
                context.addError(WsResponseCode.INVALID_DATE, "Target completion date can not be a past time.");
            } else {
                CycleCount cycleCount = new CycleCount();
                cycleCount.setStatusCode(CycleCount.StatusCode.CREATED.name());
                cycleCount.setCreated(DateUtils.getCurrentTime());
                cycleCount.setTargetCompletion(request.getTargetCompletionDate());
                cycleCount = cycleCountDao.addCycleCount(cycleCount);
                response.setCycleCountCode(cycleCount.getCode());
            }
        }
        response.setSuccessful(!context.hasErrors());
        response.addErrors(context.getErrors());
        return response;
    }

    /**
     * Used to change the target completion date of an active cycle count.
     *
     * @param request contains cycle count code and new target completion date.
     */
    @Override
    @Transactional
    @Locks({ @Lock(ns = Namespace.CYCLE_COUNT, level = Level.FACILITY, key = "#{#args[0].cycleCountCode}") })
    public EditCycleCountResponse editCycleCount(EditCycleCountRequest request) {
        EditCycleCountResponse response = new EditCycleCountResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            CycleCount cycleCount = cycleCountDao.getCycleCountByCode(request.getCycleCountCode());
            if (!cycleCount.isActive()) {
                context.addError(WsResponseCode.INVALID_CODE, "Can not edit, Cycle count is not active");
            } else if (!DateUtils.isFutureTime(request.getTargetCompletionDate())) {
                context.addError(WsResponseCode.INVALID_DATE, "Target completion date can not be a past time.");
            } else {
                cycleCount.setTargetCompletion(request.getTargetCompletionDate());
                cycleCountDao.updateCycleCount(cycleCount);
            }
        }
        response.setSuccessful(!context.hasErrors());
        response.addErrors(context.getErrors());
        return response;
    }

    /**
     * Creates automatic sub cycle count of the given cycle count. It first find shelves which are to be added to the
     * yet-to-be-created sub cycle count using {@link #findShelvesForZonesInParallel} or
     * {@link #findShelvesForZonesInSeries} methods. Afterwards, adds the obtained shelves in the sub cycle count.<br>
     * For example, if 10 days remain till the completion date, and there are 200 shelves remaining, then the
     * automatically created sub cycle count will have 20 shelves to be completed by the next day.<br>
     * This way, shelves are equally distributed in sub cycle counts.
     *
     * @param request contains cycle count code
     * @return Code of the created sub cycle count
     */
    @Override
    @Transactional
    @Locks({ @Lock(ns = Namespace.CYCLE_COUNT, level = Level.FACILITY, key = "#{#args[0].cycleCountCode}") })
    public CreateSubCycleCountResponse createAutomaticSubCycleCount(CreateSubCycleCountRequest request) {
        CreateSubCycleCountResponse response = new CreateSubCycleCountResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            int numberOfShelves;
            CycleCount cycleCount = cycleCountDao.getCycleCountByCode(request.getCycleCountCode());
            SystemConfiguration configuration = ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class);
            if (cycleCount == null || !cycleCount.isActive()) {
                context.addError(WsResponseCode.INVALID_REQUEST, "Invalid cycle count code");
            } else if (!configuration.isAutomaticSubCycleCountAllowed()) {
                context.addError(WsResponseCode.INVALID_REQUEST, "Automatic sub cycle count Creation is not allowed");
            } else {
                List<Shelf> shelves;
                int numberOfDaysLeft = DateUtils.diff(cycleCount.getTargetCompletion(), DateUtils.getCurrentDate(), DateUtils.Resolution.DAY);
                // Number of days checks whether difference between two dates is 1 day or not. Future time checks whether the diff is due to future time
                if (numberOfDaysLeft > 0 && DateUtils.isFutureTime(cycleCount.getTargetCompletion())) {
                    Long pendingShelvesCount = cycleCountDao.getNumberOfShelvesPendingCycleCount(cycleCount.getId());
                    numberOfShelves = pendingShelvesCount < (long) numberOfDaysLeft ? pendingShelvesCount.intValue() : pendingShelvesCount.intValue() / (numberOfDaysLeft);
                    if (configuration.isAddShelvesToSubCycleCountFromShelvesInParallel()) {
                        shelves = findShelvesForZonesInParallel(cycleCount.getId(), numberOfShelves);
                    } else {
                        shelves = findShelvesForZonesInSeries(cycleCount.getId(), numberOfShelves);
                    }
                } else {
                    shelves = cycleCountDao.getShelvesPendingForCycleCount(cycleCount.getId(), null, null);
                }
                LOG.info("Shelves obtained for creating automatic sub cycle count: {}", shelves);
                if (!shelves.isEmpty()) {
                    int maxBatchSize = configuration.getSubCycleCountMaxBatchSize();
                    int minBatchSize = configuration.getSubCycleCountMinBatchSize();
                    List<String> subCycleCountCodes = new ArrayList<>();
                    Iterator<List<Shelf>> subListIterator = CollectionUtils.sublistIterator(shelves, maxBatchSize);
                    SubCycleCount scc = null;
                    while (subListIterator.hasNext()) {
                        List<Shelf> subList = subListIterator.next();
                        // do not create sub cycle counts of less than minBatchSize
                        // :( tried to write lambda but couldn't
                        if (scc == null || subList.size() > minBatchSize) {
                            scc = new SubCycleCount();
                            scc.setCycleCount(cycleCount);
                            scc.setCreated(DateUtils.getCurrentTime());
                            scc.setStatusCode(CycleCount.StatusCode.CREATED.name());
                            scc.setTargetCompletion(DateUtils.addDaysToDate(DateUtils.getCurrentDate(), 1));
                        }
                        int failedShelfCount = 0;
                        for (Shelf shelf : subList) {
                            if (!addShelfToSubCycleCount(shelf.getCode(), scc, context, false)) {
                                failedShelfCount++;
                            }
                        }
                        if (failedShelfCount < subList.size()) {
                            subCycleCountCodes.add(scc.getCode());
                        }
                    }
                    if (subCycleCountCodes.isEmpty()) {
                        context.addError(WsResponseCode.NO_SHELVES_LEFT_FOR_CYCLE_COUNT, "Could not add any shelf to sub cycle count");
                    } else {
                        response.setSubCycleCountCode(subCycleCountCodes.toString());
                    }
                } else {
                    context.addError(WsResponseCode.NO_SHELVES_LEFT_FOR_CYCLE_COUNT, "Could not find any shelves to create a sub cycle count");
                }
            }
        }
        response.setSuccessful(!context.hasErrors());
        response.addErrors(context.getErrors());
        return response;
    }

    /**
     * Finds shelves in parallal. For example, if number of picksets is p, and number of shelves is s, then from each
     * zone, s/p shelves will be fetched.
     *
     * @param cycleCountId
     * @param numberOfShelves
     * @return
     */
    private List<Shelf> findShelvesForZonesInParallel(int cycleCountId, int numberOfShelves) {
        // TODO: 09/06/17 @ADITYA Handle inventory too, along with number of shelf. Try for equal inventory per day
        List<Shelf> shelves = new ArrayList<>();
        List<PickSet> pickSets = ConfigurationManager.getInstance().getConfiguration(PickConfiguration.class).getPickSets();
        if (pickSets.isEmpty()) {
            return shelves;
        } else {
            int shelvesPerZone = numberOfShelves / pickSets.size();
            int picksetIndex = 0;
            List<Shelf> shelvesInPickset;
            while (numberOfShelves > 0 && picksetIndex < pickSets.size()) {
                shelvesInPickset = cycleCountDao.getShelvesPendingForCycleCount(cycleCountId, pickSets.get(picksetIndex++).getId(), shelvesPerZone);
                shelves.addAll(shelvesInPickset);
                numberOfShelves = numberOfShelves - shelvesInPickset.size();
                if (shelvesInPickset.size() < shelvesPerZone) {
                    shelvesPerZone = numberOfShelves / (pickSets.size() - picksetIndex);
                }
            }

        }
        return shelves;
    }

    private List<Shelf> findShelvesForZonesInSeries(int cycleCountId, int numberOfShelves) {
        LOG.debug("Adding shelves in series.");
        int shelvesLeftCount = numberOfShelves;
        Shelf shelf = cycleCountDao.getLastCountedShelf(cycleCountId);
        Integer lastPickSetId = null;
        List<PickSet> pickSets = new ArrayList<>();
        List<Shelf> shelves = new ArrayList<>();
        List<Shelf> shelvesInPickset = null;
        if (shelf != null) {
            lastPickSetId = shelf.getSection().getPickSet().getId();
            shelvesInPickset = cycleCountDao.getShelvesPendingForCycleCount(cycleCountId, lastPickSetId, shelvesLeftCount);
            shelvesLeftCount -= shelvesInPickset.size();
            shelves.addAll(shelvesInPickset);
        }
        LOG.debug("Shelves in pickset with pickset id: {} are: {}", lastPickSetId, shelvesInPickset);
        if (shelvesLeftCount > 0) {
            pickSets = ConfigurationManager.getInstance().getConfiguration(PickConfiguration.class).getPickSets();
        }
        int picksetIndex = 0;
        Integer pickSetId;
        if (!pickSets.isEmpty()) {
            LOG.debug("Picksets: {}", pickSets);
            // Previous code: while (shelvesLeftCount > 0 && picksetIndex < pickSets.size() - 1) {
            // TODO: 12/09/17 @ADITYA Why was the -1 included? Ask @AMIT
            while (shelvesLeftCount > 0 && picksetIndex < pickSets.size()) {
                pickSetId = pickSets.get(picksetIndex++).getId();
                if (pickSetId.equals(lastPickSetId)) {
                    continue;
                }
                shelvesInPickset = cycleCountDao.getShelvesPendingForCycleCount(cycleCountId, pickSetId, shelvesLeftCount);
                LOG.debug("shelvesInPickset: {}", shelvesInPickset);
                shelvesLeftCount = shelvesLeftCount - shelvesInPickset.size();
                shelves.addAll(shelvesInPickset);
            }
        }
        return shelves;
    }

    /**
     * Used for creating manual sub cycle count of shelves specified in request. Allows partial entry of shelves if some
     * shelf codes are invalid. Is successful if at least one shelf can be added to the sub cycle count, fails
     * otherwise.
     * 
     * @param request Contains shelf to be added in the cycle count.
     * @return Code of the created sub cycle count if sucessful.
     */
    @Transactional
    @RollbackOnFailure
    @Override
    @Locks({ @Lock(ns = Namespace.CYCLE_COUNT, level = Level.FACILITY, key = "#{#args[0].cycleCountCode}") })
    public CreateManualSubCycleCountResponse createManualSubCycleCount(CreateManualSubCycleCountRequest request) {
        CreateManualSubCycleCountResponse response = new CreateManualSubCycleCountResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            CycleCount cycleCount = cycleCountDao.getCycleCountByCode(request.getCycleCountCode());
            if (cycleCount == null || !cycleCount.isActive()) {
                context.addError(WsResponseCode.INVALID_REQUEST, "Invalid cycle count code");
            } else {
                SubCycleCount scc = new SubCycleCount();
                scc.setCycleCount(cycleCount);
                scc.setCreated(DateUtils.getCurrentTime());
                scc.setStatusCode(CycleCount.StatusCode.CREATED.name());
                scc.setTargetCompletion(request.getTargetCompletionDate());
                Set<String> erroneousShelfCodes = request.getShelfCodes().stream().filter(shelfCode -> !addShelfToSubCycleCount(shelfCode, scc, context, false)).collect(
                        Collectors.toSet());
                if (erroneousShelfCodes.size() == request.getShelfCodes().size()) {
                    context.addError(WsResponseCode.INVALID_REQUEST, "Could not add any shelf to sub cycle count");
                }
                response.setInvalidShelfCodes(erroneousShelfCodes);
            }
        }
        response.addErrors(context.getErrors());
        response.setWarnings(context.getWarnings());
        response.setSuccessful(!context.hasErrors());
        return response;
    }

    /**
     * Adds a {@code shelf} to a {@code subCycleCount}. One shelf cannot belong to multiple sub cycle counts, unless the
     * sub cycle counts are complete. Shelf must be in ACTIVE state to be added. If it cannot be added, this method adds
     * an appropriate warning in {@code context}.
     * 
     * @return true if shelf was added without error, false otherwise.
     */
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Locks({ @Lock(ns = Namespace.SHELF, level = Level.FACILITY, key = "#{#args[0]}") })
    private boolean addShelfToSubCycleCount(String shelfCode, SubCycleCount subCycleCount, ValidationContext context, boolean blockOnAdd) {
        Shelf shelf = shelfService.getShelfByCode(shelfCode);
        if (shelf == null) {
            context.addWarning(WsResponseCode.INVALID_SHELF_CODE, shelfCode + "- No shelf exists with given code");
        } else if ((cycleCountDao.getShelfSummaryForCycleCount(shelf.getId(), subCycleCount.getCycleCount().getId()) != null) && subCycleCount.isIncomplete()) {
            context.addWarning(WsResponseCode.INVALID_SHELF_CODE,
                    "Shelf:[" + shelfCode + "] added to another sub cycle count:[" + subCycleCount.getCode() + "]. Please complete the sub cycle count first.");
        } else if (!shelf.isActive()) {
            context.addWarning(WsResponseCode.INVALID_SHELF_CODE, shelfCode + "- Shelf is not in ACTIVE state");
        } else {
            if (subCycleCount.getId() == null) {
                subCycleCount = cycleCountDao.addSubCycleCount(subCycleCount);
            }
            if (blockOnAdd) {
                shelf.setStatusCode(Shelf.StatusCode.COUNT_REQUESTED.name());
            } else {
                shelf.setStatusCode(Shelf.StatusCode.QUEUED_FOR_COUNT.name());
            }
            addShelfSummary(shelf, subCycleCount);
            shelfService.updateShelf(shelf);
            return true;
        }
        return false;

    }

    /**
     * Creates a sub cycle count, governed by target completion date as specified in request. Unlike in case of
     * {@link #createManualSubCycleCount(CreateManualSubCycleCountRequest)}, user does not have to specify each
     * individual shelf. Only the zones from which shelves are to be added are required. Adds all pending shelves of
     * specified zones to the newly created sub cycle count.
     * 
     * @param request contains list of zones and target completion date
     * @see #createManualSubCycleCount(CreateManualSubCycleCountRequest)
     * @see #createAutomaticSubCycleCount(CreateSubCycleCountRequest)
     */
    @Transactional
    @Override
    @Locks({ @Lock(ns = Namespace.CYCLE_COUNT, level = Level.FACILITY, key = "#{#args[0].cycleCountCode}") })
    public CreateSubCycleCountResponse createSemiAutomaticSubCycleCount(CreateSemiAutomaticSubCycleCountRequest request) {
        CreateSubCycleCountResponse response = new CreateSubCycleCountResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            CycleCount cycleCount = cycleCountDao.getCycleCountByCode(request.getCycleCountCode());
            if (cycleCount == null || !cycleCount.isActive()) {
                context.addError(WsResponseCode.INVALID_REQUEST, "Invalid cycle count code");
            } else {
                List<Shelf> shelves = new ArrayList<>();
                for (CreateSemiAutomaticSubCycleCountRequest.WsCycleCountZone zone : request.getZones()) {
                    // @ADITYA Why only default?
                    PickSet pickSet = picksetService.getActivePickSetByNameAndType(zone.getZoneName());
                    if (pickSet != null) {
                        shelves.addAll(cycleCountDao.getShelvesPendingForCycleCount(cycleCount.getId(), pickSet.getId(), zone.getShelfCount()));
                    } else {
                        context.addError(WsResponseCode.INVALID_ZONE_NAME, "No zone exists with given name");
                        break;
                    }
                }
                if (!shelves.isEmpty() && !context.hasErrors()) {
                    SubCycleCount scc = new SubCycleCount();
                    scc.setCycleCount(cycleCount);
                    scc.setCreated(DateUtils.getCurrentTime());
                    scc.setStatusCode(CycleCount.StatusCode.CREATED.name());
                    scc.setTargetCompletion(request.getTargetCompletionDate());
                    List<String> failedShelves = new ArrayList<>();
                    for (Shelf shelf : shelves) {
                        if (!addShelfToSubCycleCount(shelf.getCode(), scc, context, false)) {
                            failedShelves.add(shelf.getCode());
                        }
                    }
                    if (failedShelves.size() > 0) {
                        response.setMessage("List of failed shelves: " + failedShelves.toString());
                    }
                    if (failedShelves.size() == shelves.size()) {
                        context.addError(WsResponseCode.NO_SHELVES_LEFT_FOR_CYCLE_COUNT, "Could not add any shelf to sub cycle count");
                    } else {
                        response.setSubCycleCountCode(scc.getCode());
                    }
                } else if (shelves.isEmpty()) {
                    context.addError(WsResponseCode.NO_SHELVES_LEFT_FOR_CYCLE_COUNT, "Could not find any shelves to create a sub cycle count");
                }
            }
        }
        response.addErrors(context.getErrors());
        response.addWarnings(context.getWarnings());
        response.setSuccessful(!context.hasErrors());
        return response;
    }

    /**
     * Adds shelf to sub cycle count.
     * <ol>
     * We should only add the shelf if it matches <b>all</b> of the following criteria:
     * <li>Shelf is in ACTIVE state.</li>
     * <li>Shelf was not already added to the current {@code subCycleCount}</li>
     * </ol>
     */
    // TODO: 27/06/17 @ADITYA Use the private method that was created above. Otherwise, code duplication.
    @SuppressWarnings("ConstantConditions")
    @Transactional
    @Override
    @Locks({
            @Lock(ns = Namespace.SUB_CYCLE_COUNT, level = Level.FACILITY, key = "#{#args[0].subCycleCountCode}"),
            @Lock(ns = Namespace.SHELF, level = Level.FACILITY, key = "#{#args[0].shelfCode}") })
    public AddShelfToSubCycleCountResponse addShelfToSubCycleCount(AddShelfToSubCycleCountRequest request) {
        AddShelfToSubCycleCountResponse response = new AddShelfToSubCycleCountResponse();
        ValidationContext context = request.validate();
        if (request.isBlockOnAdd() && !isValidTimeForCounting()) {
            context.addError(WsResponseCode.TIME_NOT_IN_VALID_COUNTING_HOURS, "Can not block shelf at this hour");
        }
        if (!context.hasErrors()) {
            Shelf shelf = shelfService.getShelfByCode(request.getShelfCode());
            if (shelf == null) {
                context.addError(WsResponseCode.INVALID_SHELF_CODE, "No shelf found with code - " + request.getShelfCode());
            } else if (!shelf.isActive()) {
                context.addError(WsResponseCode.INVALID_STATE, "Shelf must be in ACTIVE status to be added to a sub cycle count");
            } else {
                SubCycleCount subCycleCount = cycleCountDao.getSubCycleCountByCode(request.getSubCycleCountCode());
                if (subCycleCount != null && subCycleCount.isStatusCreated()) {
                    CycleCountShelfSummary cycleCountShelfSummary = cycleCountDao.getShelfSummaryForSubCycleCount(shelf.getId(), subCycleCount.getCycleCount().getId());
                    if (cycleCountShelfSummary != null) {
                        context.addError(WsResponseCode.INVALID_STATE, "Shelf already added to current sub cycle count.");
                        response.setMessage("Use recount for modifying shelf.");
                    } else {
                        LOG.info("Adding shelf: {} to subCycleCount: {}", shelf.getCode(), subCycleCount.getCode());
                        if (request.isBlockOnAdd()) {
                            shelf.setStatusCode(Shelf.StatusCode.COUNT_REQUESTED.name());
                        } else {
                            shelf.setStatusCode(Shelf.StatusCode.QUEUED_FOR_COUNT.name());
                        }
                        addShelfSummary(shelf, subCycleCount);
                        response.setZoneName(shelf.getSection().getPickSet().getName());
                        response.setPendingPutawaysCount(putawayService.getPutawaysPendingForShelfCount(shelf.getId()).intValue());
                        response.setPickupPendingCount(shippingService.getPicklistPendingForShelfCount(shelf).intValue());
                        shelfService.updateShelf(shelf);
                    }
                } else {
                    context.addError(WsResponseCode.INVALID_CODE, "Sub cycle count does not exist/Invalid sub cycle count state.");
                }
            }
        }
        response.addErrors(context.getErrors());
        response.setSuccessful(!context.hasErrors());
        return response;
    }

    /**
     * Recounts shelf in a {@code subCycleCount}. An existing count (cycle count shelf summary) for this sub cycle count
     * must be there, else this will fail. If the sub cycle count is {@code COMPLETE}, this will fail. If no putaways or
     * picklists are pending on shelf, set its status in READY_FOR_COUNT, else set it to COUNT_REQUESTED.
     * 
     * @param request
     * @return
     */
    @Transactional
    @Override
    @Locks({
            @Lock(ns = Namespace.SUB_CYCLE_COUNT, level = Level.FACILITY, key = "#{#args[0].subCycleCountCode}"),
            @Lock(ns = Namespace.SHELF, level = Level.FACILITY, key = "#{#args[0].shelfCode}") })
    public RecountShelfResponse recountShelf(RecountShelfRequest request) {
        RecountShelfResponse response = new RecountShelfResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            Shelf shelf = shelfService.getShelfByCode(request.getShelfCode());
            SubCycleCount subCycleCount = cycleCountDao.getSubCycleCountByCode(request.getSubCycleCountCode());
            if (subCycleCount == null) {
                context.addError(WsResponseCode.INVALID_CODE, "Invalid sub cycle count");
            } else if (!subCycleCount.getCycleCount().isActive()) {
                context.addError(WsResponseCode.INVALID_STATE, "Can't redo in an inactive Cycle Count");
            } else if (subCycleCount.isComplete()) {
                context.addError(WsResponseCode.INVALID_STATE, "Sub cycle count is already complete.");
                response.setMessage("Create a new sub cycle count, and add the shelf in that to do a recount.");
            } else {
                if (shelf == null) {
                    context.addError(WsResponseCode.INVALID_SHELF_CODE, "No shelf exists with given code");
                } else if (!shelf.isActive()) {
                    context.addError(WsResponseCode.INVALID_STATE, "Can not recount before completing a count.");
                } else if (cycleCountDao.getShelfSummaryForSubCycleCount(shelf.getId(), subCycleCount.getId()) == null) {
                    context.addError(WsResponseCode.INVALID_SHELF_CODE, "Can not recount before a count.");
                } else {
                    if (putawayService.getPutawaysPendingForShelfCount(shelf.getId()) == 0 && shippingService.getPicklistPendingForShelfCount(shelf) == 0) {
                        shelf.setStatusCode(Shelf.StatusCode.READY_FOR_COUNT.name());
                    } else {
                        shelf.setStatusCode(Shelf.StatusCode.COUNT_REQUESTED.name());
                    }
                    addShelfSummary(shelf, subCycleCount);
                    shelfService.updateShelf(shelf);
                }
            }
        }
        response.addErrors(context.getErrors());
        response.setSuccessful(!context.hasErrors());
        return response;
    }

    @Override
    public UnblockShelvesResponse unblockShelves(UnblockShelvesRequest request) {
        UnblockShelvesResponse response = new UnblockShelvesResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            request.getShelfCodes().forEach(s -> unblockShelf(s, context));
        }
        response.setSuccessful(!context.hasErrors());
        response.addErrors(context.getErrors());
        return response;
    }

    @Locks({ @Lock(ns = Namespace.SHELF, level = Level.FACILITY, key = "#{#args[0]}") })
    @Transactional
    private void unblockShelf(String shelfCode, ValidationContext context) {
        Shelf shelf = shelfService.getShelfByCode(shelfCode);
        if (shelf == null) {
            context.addError(WsResponseCode.INVALID_SHELF_CODE, "Invalid shelf code:[" + shelfCode + "]");
        } else if (!StringUtils.equalsAny(shelf.getStatusCode(), Shelf.StatusCode.COUNT_REQUESTED.name(), Shelf.StatusCode.READY_FOR_COUNT.name())) {
            context.addError(WsResponseCode.INVALID_STATE, "Shelf:[" + shelfCode + "] is not in a valid state to unblock (State:" + shelf.getStatusCode() + ").");
        } else {
            shelf.setStatusCode(Shelf.StatusCode.QUEUED_FOR_COUNT.name());
            shelfService.updateShelf(shelf);
        }
    }

    @Override
    public RemoveShelvesFromCycleCountResponse removeShelvesFromCycleCount(RemoveShelvesFromCycleCountRequest request) {
        RemoveShelvesFromCycleCountResponse response = new RemoveShelvesFromCycleCountResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            Iterator<String> it = request.getShelfCodes().iterator();
            while (it.hasNext() && !context.hasErrors()) {
                removeShelf(it.next(), context);
            }
        }
        response.setSuccessful(!context.hasErrors());
        response.addErrors(context.getErrors());
        return response;
    }

    @Locks({ @Lock(ns = Namespace.SHELF, level = Level.FACILITY, key = "#{#args[0]}") })
    @Transactional
    private void removeShelf(String shelfCode, ValidationContext context) {
        Shelf shelf = shelfService.getShelfByCode(shelfCode);
        if (shelf == null) {
            context.addError(WsResponseCode.INVALID_SHELF_CODE, "No shelf exists with given code");
        } else if (!StringUtils.equalsAny(shelf.getStatusCode(), Shelf.StatusCode.QUEUED_FOR_COUNT.name(), Shelf.StatusCode.COUNT_REQUESTED.name(),
                Shelf.StatusCode.READY_FOR_COUNT)) {
            context.addError(WsResponseCode.INVALID_SHELF_CODE, "Shelf not in a valid status to be removed");
        } else {
            shelf.setStatusCode(Shelf.StatusCode.ACTIVE.name());
            deleteCycleCountShelfSummary(shelf.getId());
            shelfService.updateShelf(shelf);
        }
    }

    @Override
    public BlockShelvesForCycleCountResponse blockShelvesForCycleCountRequest(BlockShelvesForCycleCountRequest request) {
        ValidationContext context = request.validate();
        BlockShelvesForCycleCountResponse response = new BlockShelvesForCycleCountResponse();
        if (!isValidTimeForCounting()) {
            context.addError(WsResponseCode.TIME_NOT_IN_VALID_COUNTING_HOURS, "Can not count at this hour");
        }
        if (!context.hasErrors()) {
            for (String s : request.getShelfCodes()) {
                blockShelf(s, context);
            }
        }
        response.setSuccessful(!context.hasErrors());
        response.addErrors(context.getErrors());
        return response;
    }

    private boolean isValidTimeForCounting() {
        SystemConfiguration configuration = ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class);
        int startHour = configuration.getAllowedStartHourForCycleCount();
        int endHour = configuration.getAllowedEndHourForCycleCount();
        int currentHour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
        //4-12 or 16-8
        if (endHour > startHour) {
            return (currentHour >= startHour && currentHour < endHour);
        } else if (endHour < startHour) {
            return ((currentHour < startHour && currentHour < endHour) || currentHour >= startHour);
        }
        return true;
    }

    @Locks({ @Lock(ns = Namespace.SHELF, level = Level.FACILITY, key = "#{#args[0]}") })
    @Transactional
    private void blockShelf(String shelfCode, ValidationContext context) {
        Shelf shelf = shelfService.getShelfByCode(shelfCode);
        if (shelf == null) {
            context.addError(WsResponseCode.INVALID_SHELF_CODE, "No shelf exists with given code");
        } else if (!shelf.isQueuedForCount()) {
            context.addError(WsResponseCode.INVALID_SHELF_CODE, "Shelf status not valid for blocking shelf for cycle count");
        } else {
            updateShelfSummaryUser(shelf.getId());
            if (putawayService.getPutawaysPendingForShelfCount(shelf.getId()) == 0 && shippingService.getPicklistPendingForShelfCount(shelf) == 0) {
                shelf.setStatusCode(Shelf.StatusCode.READY_FOR_COUNT.name());
            } else {
                shelf.setStatusCode(Shelf.StatusCode.COUNT_REQUESTED.name());
            }
            shelfService.updateShelf(shelf);
        }
    }

    @Transactional
    private void addShelfSummary(Shelf shelf, SubCycleCount subCycleCount) {
        User user = usersService.getUserByUsername(UserContext.current().getUniwareUserName());
        CycleCountShelfSummary ccss = new CycleCountShelfSummary();
        ccss.setSubCycleCount(subCycleCount);
        ccss.setUser(user);
        ccss.setShelf(shelf);
        ccss.setActive(true);
        ccss.setCreated(DateUtils.getCurrentTime());
        ccss.setUpdated(DateUtils.getCurrentTime());
        LOG.info("Shelf summary of shelf: {} was created with following parameters: {}", shelf.getCode(), ccss);

        cycleCountDao.addCycleCountShelfSummary(ccss);
    }

    private void deleteCycleCountShelfSummary(int shelfId) {
        CycleCountShelfSummary activeShelfSummary = cycleCountDao.getActiveCycleCountShelfSummary(shelfId);
        cycleCountDao.deleteCycleCountShelfSummary(activeShelfSummary);
    }

    /**
     * Marks shelf status as <i>COUNT_IN_PROGRESS</i> to indicate that counting is being done.
     *
     * @param request contains shelf code for which counting is to be done
     * @return Generic service response
     */
    @Transactional
    @Override
    @Locks({ @Lock(ns = Namespace.SHELF, level = Level.FACILITY, key = "#{#args[0].shelfCode}") })
    public StartCountingForShelfResponse startCountingForShelf(StartCountingForShelfRequest request) {
        ValidationContext context = request.validate();
        StartCountingForShelfResponse response = new StartCountingForShelfResponse();
        if (!isValidTimeForCounting()) {
            context.addError(WsResponseCode.TIME_NOT_IN_VALID_COUNTING_HOURS, "Can not count at this hour");
        }
        if (!context.hasErrors()) {
            Shelf shelf = shelfService.getShelfByCode(request.getShelfCode());
            if (shelf == null) {
                context.addError(WsResponseCode.INVALID_SHELF_CODE, "No shelf exists with given code");
            } else if (!shelf.isReadyForCount()) {
                context.addError(WsResponseCode.INVALID_STATE, "Shelf status not valid for starting cycle count");
            } else {
                updateShelfSummaryUser(shelf.getId());
                shelf.setStatusCode(Shelf.StatusCode.COUNT_IN_PROGRESS.name());
                shelfService.updateShelf(shelf);
            }
        }
        response.setSuccessful(!context.hasErrors());
        response.addErrors(context.getErrors());
        return response;
    }

    private void updateShelfSummaryUser(int shelfId) {
        CycleCountShelfSummary activeShelfSummary = cycleCountDao.getActiveCycleCountShelfSummary(shelfId);
        User user = usersService.getUserByUsername(UserContext.current().getUniwareUserName());
        activeShelfSummary.setUser(user);
        cycleCountDao.updateCycleCountShelfSummary(activeShelfSummary);
    }

    @Override
    public CompleteCountForShelfResponse completeCountForShelf(CompleteCountForShelfRequest request) {
        CompleteCountForShelfResponse response = new CompleteCountForShelfResponse();
        ValidationContext context = request.validate();
        CycleCount cycleCount = getActiveCycleCount();
        if (cycleCount == null) {
            context.addError(WsResponseCode.INVALID_REQUEST, "No active Cycle Count");
        }
        if (!context.hasErrors()) {
            int retries = 3;
            while (retries-- > 0) {
                try {
                    response = completeCountForShelfInternal(request, context, cycleCount.getCode());
                    if (response.getInvalidItemCodes() != null && !response.getInvalidItemCodes().isEmpty()) {
                        //make the user to start counting all over again
                        markShelfReadyForCount(request.getShelfCode());
                    }
                    return response;
                } catch (HibernateOptimisticLockingFailureException e) {
                    LOG.error("Optimistic Lock failed {}", e.getMessage());
                    if (retries == 0) {
                        context.addError(WsResponseCode.INTERNAL_ERROR, "Failed to submit results, please retry");
                    }
                } catch (Exception e) {
                    context.addError(WsResponseCode.INTERNAL_ERROR, e.getMessage());
                    LOG.error("Exception completing count for shelf", e);
                    break;
                }
            }
        }
        response.addErrors(context.getErrors());
        response.setSuccessful(false);
        return response;
    }

    /**
     * This method is used to complete the cycle count for a shelf. Workflow:
     * <ol>
     * <li>Fetch shelf, cyclecount, subcyclecount and cycleCountShelfSummary (<b>ccss</b>) with error checking of
     * states. Fetch expected items on the shelf {@link IInventoryService#getItemsExpectedOnShelf(int)}</li>
     * <li>Group found barcodes(items) into inventory per sku per inventoryType into
     * {@code skuToInventoryTypeToCycleCountItemDetail}</li>
     * <li>ageingQuantities of CycleCountItemDetail denotes, while ageingInventories denote</li>
     * <li></li>
     * </ol>
     * 
     * @param request contains {@code shelf code}, {@code sub cycle count code} and {@code itemCodes}.
     * @param context
     * @param cycleCountCode
     * @return
     */
    @RollbackOnFailure
    @Transactional
    @LogActivity
    @Locks({
            @Lock(ns = Namespace.CYCLE_COUNT, level = Level.FACILITY, key = "#{#args[2]}"),
            @Lock(ns = Namespace.SUB_CYCLE_COUNT, level = Level.FACILITY, key = "#{#args[0].subCycleCountCode}"),
            @Lock(ns = Namespace.SHELF, level = Level.FACILITY, key = "#{#args[0].shelfCode}") })
    private CompleteCountForShelfResponse completeCountForShelfInternal(CompleteCountForShelfRequest request, ValidationContext context,
            @SuppressWarnings("unused") String cycleCountCode) {
        //third param (cycleCount) required for lock
        LOG.info("Completing counting for shelf {} in Cycle count {}", request.getShelfCode(), cycleCountCode);
        CompleteCountForShelfResponse response = new CompleteCountForShelfResponse();
        Shelf shelf = shelfService.getShelfByCode(request.getShelfCode());
        SubCycleCount subCycleCount = cycleCountDao.getSubCycleCountByCode(request.getSubCycleCountCode());
        if (shelf == null) {
            context.addError(WsResponseCode.INVALID_SHELF_CODE, "No shelf exists with given code");
        } else if (subCycleCount == null) {
            context.addError(WsResponseCode.INVALID_CODE, "Invalid sub cycle count code");
        } else if (!shelf.isCountInProgress()) {
            context.addError(WsResponseCode.INVALID_STATE, "Cycle count has not been started yet for this shelf");
        } else {
            CycleCountShelfSummary activeSummary = cycleCountDao.getActiveCycleCountShelfSummary(shelf.getId());
            if (!activeSummary.getSubCycleCount().getId().equals(subCycleCount.getId())) {
                context.addError(WsResponseCode.INVALID_SHELF_CODE, "Shelf added to some other sub cycle count");
            } else if (!validateUser(activeSummary)) {
                context.addError(WsResponseCode.INVALID_USER_ID, "Can not count shelf started by some other user");
            } else {
                SubCycleCount currentSubCycleCount = activeSummary.getSubCycleCount();
                CycleCount cycleCount = currentSubCycleCount.getCycleCount();
                List<Item> allItemsForShelf = inventoryService.getItemsExpectedOnShelf(shelf.getId());
                Map<String, Map<ItemTypeInventory.Type, CycleCountItemDetail>> skuToInventoryTypeToCycleCountItemDetail = new HashMap<>();
                Set<Item> scannedItems = new HashSet<>();
                List<ErrorItemDTO> errorItems = new ArrayList<>();
                List<String> invalidItemCodes = new ArrayList<>();
                response.setErrorItems(errorItems);
                response.setInvalidItemCodes(invalidItemCodes);
                // group found barcodes into inventory per sku per inventoryType into skuToInventoryTypeToCycleCountItemDetail
                request.getItemCodes().forEach(itemCode -> {
                    Item item = inventoryService.getItemByCode(itemCode);
                    if (item != null) {
                        String reasonCode = null;
                        String comment = null;

                        if (!item.isInValidState()) {
                            reasonCode = item.getStatusCode();
                            comment = "Item found in unexpected state";
                        }
                        // allow if (item was not counted in the current cycle count) or (scanning on same shelf again) or
                        // (the shelf was set to null (item missing on some shelf after cycle count))
                        else if (!cycleCount.equals(item.getLastCycleCount()) || shelf.equals(item.getShelf()) || item.getShelf() == null) {
                            skuToInventoryTypeToCycleCountItemDetail.computeIfAbsent(item.getItemType().getSkuCode(), v -> new HashMap<>()).computeIfAbsent(item.getInventoryType(),
                                    k -> new CycleCountItemDetail(item.getItemType().getSkuCode(), item.getItemType().getName())).ageingQuantities.compute(
                                            item.getAgeingStartDate(), (ageingStartDate, quantity) -> quantity == null ? 1 : quantity + 1);
                            updateItem(item, shelf, cycleCount);
                        } else {
                            reasonCode = "DUPLICATE";
                            comment = "Already found on shelf - " + item.getShelf().getCode();
                        }
                        if (reasonCode != null) {
                            errorItems.add(new ErrorItemDTO(item, reasonCode, comment));
                            CycleCountErrorItem ccbi = new CycleCountErrorItem(currentSubCycleCount, shelf, itemCode, reasonCode, comment, DateUtils.getCurrentTime());
                            cycleCountDao.addCycleCountErrorItem(ccbi);
                        }
                        scannedItems.add(item);
                    } else {
                        invalidItemCodes.add(itemCode);
                    }
                });

                if (!invalidItemCodes.isEmpty()) {
                    context.addError(WsResponseCode.INVALID_ITEM_CODE, "You have scanned one or more item/s incorrectly. Please recount the shelf inventory");
                }

                if (!context.hasErrors()) {
                    // scannedItems contains valid items in request. These may be less than allItemsForShelf.
                    allItemsForShelf.removeAll(scannedItems);
                    if (!allItemsForShelf.isEmpty()) {
                        // if item was not scanned, then remove the shelf from item and update its last cycle count accordingly.
                        removeItemShelf(allItemsForShelf);
                    }
                    discardExistingCycleCountItemsIfRecount(currentSubCycleCount, shelf);
                    List<ItemTypeInventory> itemTypeInventories = inventoryService.getAllItemTypeInventoryForShelf(shelf, true);
                    // group expected inventory per sku per inventoryType into skuToInventoryTypeToCycleCountItemDetail
                    itemTypeInventories.forEach(itemTypeInventory -> skuToInventoryTypeToCycleCountItemDetail.computeIfAbsent(itemTypeInventory.getItemType().getSkuCode(),
                            v -> new HashMap<>()).computeIfAbsent(itemTypeInventory.getType(),
                                    k -> new CycleCountItemDetail(itemTypeInventory.getItemType().getSkuCode(), itemTypeInventory.getItemType().getName())).ageingInventories.put(
                                            itemTypeInventory.getAgeingStartDate(), itemTypeInventory));
                    //compare actual found and expected inventories and act accordingly
                    skuToInventoryTypeToCycleCountItemDetail.forEach((skuCode, inventoryTypeToCycleCountItemDetail) -> {
                        inventoryTypeToCycleCountItemDetail.forEach((inventoryType, cycleCountItemDetail) -> {
                            int quantityKnown = cycleCountItemDetail.ageingInventories.values().stream().mapToInt(
                                    iti -> iti.getQuantity() + iti.getQuantityBlocked() + iti.getQuantityNotFound()).sum();
                            int actualQuantity = cycleCountItemDetail.ageingQuantities.values().stream().mapToInt(value -> value).sum();
                            int inventoryDifference = actualQuantity - quantityKnown;
                            int pendingReconciliation;
                            if (inventoryDifference > 0) {
                                pendingReconciliation = adjustExtraInventory(currentSubCycleCount, skuCode, inventoryType, inventoryDifference);
                                Map<Date, Integer> pendingReconciliationAgeingInventory = removePendingReconciliationFromActual(cycleCountItemDetail.ageingQuantities,
                                        pendingReconciliation);
                                pendingReconciliationAgeingInventory.forEach((ageingDate, quantity) -> {
                                    CycleCountItem cycleCountItem = new CycleCountItem(shelf, cycleCountItemDetail.skuCode, cycleCountItemDetail.itemTypeName, inventoryType,
                                            activeSummary, ageingDate, quantityKnown, inventoryDifference, quantity);
                                    cycleCountDao.addCycleCountItem(cycleCountItem);
                                });
                            } else {
                                pendingReconciliation = adjustMissingInventory(currentSubCycleCount, skuCode, inventoryType, Math.abs(inventoryDifference));
                                CycleCountItem cycleCountItem = new CycleCountItem(shelf, cycleCountItemDetail.skuCode, cycleCountItemDetail.itemTypeName, inventoryType,
                                        activeSummary, null, quantityKnown, inventoryDifference, pendingReconciliation);
                                cycleCountDao.addCycleCountItem(cycleCountItem);
                            }
                            reconcileItemTypeInventories(cycleCountItemDetail, shelf, inventoryType);
                        });
                    });
                    shelf.setStatusCode(Shelf.StatusCode.ACTIVE.name());
                    markCountingComplete(activeSummary, request.getNonBarcodedItemCount(), request.getItemCodes());
                    shelfService.updateShelf(shelf);
                    //we can be here even after subcycle count complete as recount is allowed
                    if (cycleCountDao.getActiveSubCycleCountShelfSummaries(currentSubCycleCount.getId()).isEmpty()
                            && currentSubCycleCount.getStatusCode().equals(CycleCount.StatusCode.CREATED.name())) {
                        currentSubCycleCount.setStatusCode(CycleCount.StatusCode.COMPLETED.name());
                        currentSubCycleCount.setCompleted(DateUtils.getCurrentTime());
                        cycleCountDao.updateSubCycleCount(currentSubCycleCount);
                    }

                }
            }
        }
        response.addErrors(context.getErrors());
        response.setSuccessful(!context.hasErrors());
        return response;
    }

    /**
     * @param ageingQuantities map of item type inventory ageing with the quantity
     * @param pendingReconciliation The quantity that is left after adjusting
     * @return
     */
    private Map<Date, Integer> removePendingReconciliationFromActual(Map<Date, Integer> ageingQuantities, int pendingReconciliation) {
        HashMap<Date, Integer> pendingReconciliationInventoryMap = new HashMap<>();
        Iterator<Map.Entry<Date, Integer>> iterator = ageingQuantities.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<Date, Integer> dateIntegerEntry = iterator.next();
            if (dateIntegerEntry.getValue() <= pendingReconciliation) {
                pendingReconciliationInventoryMap.put(dateIntegerEntry.getKey(), dateIntegerEntry.getValue());
                iterator.remove();
                pendingReconciliation = pendingReconciliation - dateIntegerEntry.getValue();
            } else {
                pendingReconciliationInventoryMap.put(dateIntegerEntry.getKey(), pendingReconciliation);
                ageingQuantities.put(dateIntegerEntry.getKey(), dateIntegerEntry.getValue() - pendingReconciliation);
                pendingReconciliation = 0;
            }
            if (pendingReconciliation == 0)
                break;
        }
        return pendingReconciliationInventoryMap;
    }

    /**
     * @param cycleCountItemDetail
     * @param shelf
     * @param inventoryType
     */
    private void reconcileItemTypeInventories(CycleCountItemDetail cycleCountItemDetail, Shelf shelf, ItemTypeInventory.Type inventoryType) {
        cycleCountItemDetail.ageingInventories.forEach((ageingStartDate, itemTypeInventory) -> {
            int quantityToSet = Optional.ofNullable(cycleCountItemDetail.ageingQuantities.remove(ageingStartDate)).orElse(0);
            itemTypeInventory.setQuantityNotFound(0);
            if (quantityToSet < itemTypeInventory.getQuantityBlocked()) {
                releaseSaleOrderItems(itemTypeInventory.getId(), itemTypeInventory.getQuantityBlocked() - quantityToSet);
                itemTypeInventory.setQuantity(0);
            } else {
                itemTypeInventory.setQuantity(quantityToSet - itemTypeInventory.getQuantityBlocked());
            }
            inventoryDao.updateItemTypeInventory(itemTypeInventory);
        });
        cycleCountItemDetail.ageingQuantities.forEach((ageingDate, quantity) -> {
            ItemTypeInventory itemTypeInventory = new ItemTypeInventory();
            itemTypeInventory.setShelf(shelf);
            itemTypeInventory.setItemType(catalogService.getItemTypeBySkuCode(cycleCountItemDetail.skuCode));
            itemTypeInventory.setType(inventoryType);
            itemTypeInventory.setAgeingStartDate(ageingDate);
            itemTypeInventory.setQuantity(quantity);
            itemTypeInventory.setCreated(DateUtils.getCurrentTime());
            itemTypeInventory.setUpdated(itemTypeInventory.getCreated());
            inventoryDao.addItemTypeInventory(itemTypeInventory);
        });
    }

    /**
     * Discard cycle count
     * 
     * @param subCycleCount
     * @param shelf
     * @see com.uniware.dao.cyclecount.impl.CycleCountDaoImpl#discardExistingCycleCountItemsIfRecount(SubCycleCount,
     *      Shelf)
     */
    private void discardExistingCycleCountItemsIfRecount(SubCycleCount subCycleCount, Shelf shelf) {
        cycleCountDao.discardExistingCycleCountItemsIfRecount(subCycleCount, shelf);
    }

    private boolean validateUser(CycleCountShelfSummary shelfSummary) {
        User user = usersService.getUserByUsername(UserContext.current().getUniwareUserName());
        return user.getId().equals(shelfSummary.getUser().getId());
    }

    /**
     * @param items
     */
    private void removeItemShelf(List<Item> items) {
        for (Item item : items) {
            String shelfCode = item.getShelf().getCode();
            item.setShelf(null);
            inventoryService.updateItemOptimistic(item, null, item.getLastCycleCount() != null ? item.getLastCycleCount().getId() : null);
            if (ActivityContext.current().isEnable()) {
                ActivityUtils.appendActivity(item.getCode(), ActivityEntityEnum.ITEM.getName(), null,
                        Collections.singletonList("Item {" + ActivityEntityEnum.ITEM.name() + ":" + item.getCode() + "} not found on shelf: " + shelfCode),
                        Item.ActivityType.NOT_FOUND.name());
            }
        }
    }

    /**
     * Marks cycleCountShelfSummary as complete. (Makes it inactive, sets the values of its parameters)
     * 
     * @param ccss cycleCountShelfSummary
     * @param nonBarcodedItemCount number of tems without barcodes
     * @param itemCodes List of tems with barcodes
     */
    private void markCountingComplete(CycleCountShelfSummary ccss, int nonBarcodedItemCount, Set<String> itemCodes) {
        User user = usersService.getUserByUsername(UserContext.current().getUniwareUserName());
        ccss.setUser(user);
        ccss.setNonBarcodedItemCount(nonBarcodedItemCount);
        ccss.setActive(false);
        ccss.setItemCodes(String.join(",", itemCodes.toArray(new String[] {})));
        cycleCountDao.updateCycleCountShelfSummary(ccss);
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Locks({ @Lock(ns = Namespace.SHELF, level = Level.FACILITY, key = "#{#args[0]}") })
    private void markShelfReadyForCount(String shelfCode) {
        Shelf shelf = shelfService.getShelfByCode(shelfCode);
        shelf.setStatusCode(Shelf.StatusCode.READY_FOR_COUNT.name());
        shelfService.updateShelf(shelf);
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    private void markShelfQueuedForCount(Shelf shelf) {
        shelf = shelfService.getShelfByCode(shelf.getCode());
        shelf.setStatusCode(Shelf.StatusCode.QUEUED_FOR_COUNT.name());
        shelfService.updateShelf(shelf);
    }

    /**
     * Adjust extra inventory against already found pending reconciliation missing inventory i.e inventoryDifference < 0
     *
     * @param subCycleCount currency cycle count
     * @param skuCode SKU for which adjustment is required
     * @param quantityToAdjust maximum quantity to adjust
     * @param inventoryType type of inventory to be adjusted
     * @return pending adjustment quantity
     */
    private int adjustExtraInventory(SubCycleCount subCycleCount, String skuCode, ItemTypeInventory.Type inventoryType, int quantityToAdjust) {
        List<CycleCountItem> unreconciledItems = cycleCountDao.getUnreconciledCycleCountItems(subCycleCount.getCycleCount().getId(), skuCode, inventoryType, true);
        int quantityPending = quantityToAdjust;
        Iterator<CycleCountItem> it = unreconciledItems.iterator();
        while (it.hasNext() && quantityPending > 0) {
            CycleCountItem cci = it.next();
            int adjustedQuantity = Math.min(cci.getPendingReconciliation(), quantityPending);
            cci.setPendingReconciliation(cci.getPendingReconciliation() - adjustedQuantity);
            quantityPending -= adjustedQuantity;
        }
        return quantityPending;
    }

    /**
     * Adjust missing inventory against already found pending reconciliation extra inventory i.e inventoryDifference >
     * 0. This method also add adjusted quantity to inventory
     *
     * @param subCycleCount current cycle count
     * @param skuCode SKU for which adjustment is required
     * @param missingQuantity maximum quantity to adjust
     * @return ending adjustment quantity
     */
    private int adjustMissingInventory(SubCycleCount subCycleCount, String skuCode, ItemTypeInventory.Type inventoryType, int missingQuantity) {
        if (missingQuantity == 0) {
            return missingQuantity;
        }
        List<CycleCountItem> unreconciledItems = cycleCountDao.getUnreconciledCycleCountItems(subCycleCount.getCycleCount().getId(), skuCode, inventoryType, false);
        Iterator<CycleCountItem> it = unreconciledItems.iterator();
        int quantityPending = missingQuantity;
        while (it.hasNext() && quantityPending > 0) {
            CycleCountItem cci = it.next();
            int adjustedQuantity = Math.min(cci.getPendingReconciliation(), quantityPending);
            cci.setPendingReconciliation(cci.getPendingReconciliation() - adjustedQuantity);
            inventoryService.addInventory(cci.getSkuCode(), inventoryType, cci.getAgeingStartDate(), cci.getShelf(), adjustedQuantity);
            quantityPending -= adjustedQuantity;
        }
        return quantityPending;
    }

    private void releaseSaleOrderItems(int itemTypeInventoryId, int numberOfItemsToRelease) {
        List<SaleOrderItem> saleOrderItemsToRelease = saleOrderService.getFulfillableSaleOrderItemsForItemTypeInventory(itemTypeInventoryId, numberOfItemsToRelease);
        Map<String, Set<String>> orderCodeToOrderItemCodes = new HashMap<>();
        for (SaleOrderItem soi : saleOrderItemsToRelease) {
            String orderCode = soi.getSaleOrder().getCode();
            Set<String> orderItemCodes = orderCodeToOrderItemCodes.computeIfAbsent(orderCode, k -> new HashSet<>());
            orderItemCodes.add(soi.getCode());
        }
        for (String orderCode : orderCodeToOrderItemCodes.keySet()) {
            UnblockSaleOrderItemsInventoryRequest request = new UnblockSaleOrderItemsInventoryRequest();
            request.setSaleOrderCode(orderCode);
            request.setSaleOrderItemCodes(new ArrayList<>(orderCodeToOrderItemCodes.get(orderCode)));
            if (!saleOrderService.unblockSaleOrderItemsInventory(request).isSuccessful()) {
                throw new RuntimeException("Could not unblock sale order items");
            }
        }
    }

    @Locks({ @Lock(ns = Namespace.ITEM, key = "#{#args[0].code}", level = Level.FACILITY) })
    private void updateItem(Item item, Shelf shelf, CycleCount cycleCount) {
        if (!(shelf.equals(item.getShelf()) && cycleCount.equals(item.getLastCycleCount()))) {
            item.setLastCycleCount(cycleCount);
            item.setShelf(shelf);
            inventoryService.updateItemOptimistic(item, shelf.getId(), cycleCount.getId());
        }
        if (ActivityContext.current().isEnable()) {
            ActivityUtils.appendActivity(item.getCode(), ActivityEntityEnum.ITEM.getName(), null,
                    Collections.singletonList(
                            "Item {" + ActivityEntityEnum.ITEM.name() + ":" + item.getCode() + "scanned in cycle count: " + cycleCount.getCode() + " on shelf: " + shelf.getCode()),
                    Item.ActivityType.UNPROCESSED.name());
        }
    }

    /**
     * Get active cycle count code.
     * 
     * @param request
     * @return
     */
    @Transactional
    @Override
    public GetCycleCountResponse getActiveCycleCount(GetActiveCycleCountRequest request) {
        GetCycleCountResponse response = new GetCycleCountResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            CycleCount cycleCount = getActiveCycleCount();
            if (cycleCount == null) {
                context.addError(WsResponseCode.INVALID_REQUEST, "No active cycle count exists");
            } else {
                List<SubCycleCount> subCycleCounts = cycleCountDao.getSubCycleCountsForCycleCount(request, cycleCount.getId());
                response.setResultCount(cycleCountDao.getTotalSubcycleCountsForCycleCount(request, cycleCount.getId()));
                List<SubCycleCountDTO> subCycleCountDTOs = new ArrayList<>();
                for (SubCycleCount subCycleCount : subCycleCounts) {
                    SubCycleCountDTO subCycleCountDTO = cycleCountDao.getSubCycleCountSummary(subCycleCount.getId());
                    subCycleCountDTO.prepare(subCycleCount);
                    subCycleCountDTOs.add(subCycleCountDTO);
                }
                CycleCountDTO cycleCountDTO = new CycleCountDTO();
                cycleCountDTO.setCycleCountCode(cycleCount.getCode());
                cycleCountDTO.setSubCycleCounts(subCycleCountDTOs);
                response.setCycleCount(cycleCountDTO);
            }
        }
        response.addErrors(context.getErrors());
        response.setSuccessful(!context.hasErrors());
        return response;
    }

    /**
     * Gets active cycle count. Facility is obtained from user context in the DAO layer.
     * 
     * @return Active cycle count
     */
    @Transactional(readOnly = true)
    @Override
    public CycleCount getActiveCycleCount() {
        return cycleCountDao.getActiveCycleCount();
    }

    @Override
    @Transactional
    public GetShelfDetailsResponse getShelfDetails(GetShelfDetailsRequest request) {
        ValidationContext context = request.validate();
        GetShelfDetailsResponse response = new GetShelfDetailsResponse();
        if (!context.hasErrors()) {
            Shelf shelf = shelfService.getShelfByCode(request.getShelfCode());
            if (shelf == null) {
                context.addError(WsResponseCode.INVALID_SHELF_CODE, "No shelf exists with given code");
            } else {
                SubCycleCountDTO.ShelfDTO shelfDTO = new SubCycleCountDTO.ShelfDTO();
                shelfDTO.setShelfCode(shelf.getCode());
                shelfDTO.setPutawayPendingCount(putawayService.getPutawaysPendingForShelfCount(shelf.getId()));
                shelfDTO.setPicklistPendingCount(shippingService.getPicklistPendingForShelfCount(shelf));
                List<ItemTypeInventory> itemTypeInventories = inventoryService.getAllItemTypeInventoryForShelf(shelf, false);
                int quantity = getExpectedQuantity(itemTypeInventories);
                shelfDTO.setQuantity(quantity);
                response.setShelf(shelfDTO);
            }
        }
        response.addErrors(context.getErrors());
        response.setSuccessful(!context.hasErrors());
        return response;
    }

    @Override
    @Transactional
    public GetZonesForCycleCountResponse getZonesForCycleCount(GetZonesForCycleCountRequest request) {
        GetZonesForCycleCountResponse response = new GetZonesForCycleCountResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            CycleCount cycleCount = cycleCountDao.getCycleCountByCode(request.getCycleCountCode());
            if (cycleCount == null) {
                context.addError(WsResponseCode.INVALID_REQUEST, "No cycle count exists for given code");
            } else {
                response.setZones(cycleCountDao.fetchZonesForCycleCount(cycleCount.getId()));
            }
        }
        response.addErrors(context.getErrors());
        response.setSuccessful(!context.hasErrors());
        return response;
    }

    /**
     * This method is used to process shelves which are either in {@code COUNT_REQUESTED} or {@code READY_FOR_COUNT}
     * state. It checks for two conditions:
     * <ol>
     * <li>The time should be right for counting. ({@link #isValidTimeForCounting()})<br>
     * If not, then make the shelves as {@code QUEUED_FOR_COUNT} and returns.</li>
     * <li>Shelf shouldn't be blocked for too long. ({@link #handleShelfBlockingTimeLimit(String, Integer)})<br>
     * This method transfers all shelves, which meet above condition, from {@code COUNT_REQUESTED} state to
     * {@code READY_FOR_COUNT} state. The cycle count can now begin.</li>
     * </ol>
     */
    @Override
    @Transactional
    public ServiceResponse preProcessShelvesForCycleCount() {
        ServiceResponse response = new ServiceResponse();
        List<Shelf> shelves = shelfService.getShelvesForCycleCountPreProcessing();
        if (shelves.isEmpty()) {
            response.setMessage("No shelves to process");
        } else if (!isValidTimeForCounting()) {
            LOG.info("Trying to unblock all shelves as time out of allowed counting time range");
            shelves.forEach(this::markShelfQueuedForCount);
            response.setMessage("Unblocked all shelves as time out of allowed counting time range");
        } else {
            Integer allowedHours = ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).getMaxAllowedShelfBlockedHours();
            shelves.stream().filter(shelf -> !handleShelfBlockingTimeLimit(shelf.getCode(), allowedHours)).filter(shelf -> !shelf.isReadyForCount()).forEach(shelf -> {
                prepareShelfForCycleCount(shelf.getCode());
            });
            response.setMessage("Successfully pre-processed shelves");
            response.setSuccessful(true);
        }
        return response;
    }

    /**
     * If the shelf has been blocked for a time exceeding > {@code timeLimitHours}, then unblock it and mark it
     * {@code QUEUED_FOR_COUNT}. (Used to prevent premature blocking on shelf.)
     * 
     * @param shelfCode shelf code
     * @param timeLimitHours the amount of time a shelf can be blocked for cycle count in hours.
     * @return true if shelf was unblocked, false otherwise
     */
    @Locks({ @Lock(ns = Namespace.SHELF, level = Level.FACILITY, key = "#{#args[0]}") })
    private boolean handleShelfBlockingTimeLimit(String shelfCode, Integer timeLimitHours) {
        Shelf shelf = shelfService.getShelfByCode(shelfCode);
        if (shelf.isBlockedForCycleCount() && !shelf.isCycleCountInProgress()
                && DateUtils.isPastTime(DateUtils.addToDate(shelf.getUpdated(), Calendar.HOUR_OF_DAY, timeLimitHours))) {
            shelf.setStatusCode(Shelf.StatusCode.QUEUED_FOR_COUNT.name());
            shelfService.updateShelf(shelf);
            return true;
        }
        return false;
    }

    /**
     * This method checks if the status code of shelf is {@code COUNT_REQUESTED} and there are no picklists or putaways
     * pending for it, and then marks it as {@code READY_FOR_COUNT}.
     * 
     * @param shelfCode shelf to be prepared
     */
    @Locks({ @Lock(ns = Namespace.SHELF, level = Level.FACILITY, key = "#{#args[0]}") })
    private void prepareShelfForCycleCount(String shelfCode) {
        Shelf shelf = shelfService.getShelfByCode(shelfCode);
        if (shelf.isCountRequested()) {
            if (putawayService.getPutawaysPendingForShelfCount(shelf.getId()) == 0 && shippingService.getPicklistPendingForShelfCount(shelf) == 0) {
                markShelfReadyForCount(shelf.getCode());
            }
        }
    }

    /**
     * Returns the shelf DTO {@link com.uniware.core.api.cyclecount.SubCycleCountDTO.ShelfDTO}. See
     * {@link #prepareShelfDTO(Shelf, CycleCountShelfSummary)} for more details.
     * 
     * @param request contains shelf code
     * @return Generic service response containing ShelfDTO
     */
    @Override
    @Transactional
    public SearchCycleCountShelfResponse searchCycleCountShelf(SearchCycleCountShelfRequest request) {
        SearchCycleCountShelfResponse response = new SearchCycleCountShelfResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            Shelf shelf = shelfService.getShelfByCode(request.getShelfCode());
            if (shelf != null) {
                CycleCountShelfSummary activeShelfSummary = cycleCountDao.getActiveCycleCountShelfSummary(shelf.getId());
                if (activeShelfSummary != null) {
                    response.setShelf(prepareShelfDTO(shelf, activeShelfSummary));
                }
            }
        }
        response.addErrors(context.getErrors());
        response.setSuccessful(!context.hasErrors());
        return response;
    }

    /**
     * If {@code cycleCountShelfSummary} is active, this means that only {@code quantity} of {@code ShelfDTO} needs to
     * be set, and all other quantities are zero (because this denotes that processing for shelf in cycle count has not
     * been completed yet, and hence only {@code quantity}, which is actually the expected quantity on the shelf is
     * known.
     * <p>
     * If {@code cycleCountShelfSummary} is inactive, this means two things: either {@code shelf} was not processed, or
     * the processing is complete. Then, in the latter case, we set the shelfDTO's {@code quantity},
     * {@code foundQuantity}, {@code missingQuantity}, {@code extraQuantity} appropriately.
     * <p>
     * Note that in a particular {@code cycleCountShelfSummary}, if one {@code cycleCountItem} has been discarded, it
     * means all have been discarded (According to current flow).
     * 
     * @param shelf
     * @param ccss
     * @see {@link #discardExistingCycleCountItemsIfRecount(SubCycleCount, Shelf)}
     * @return
     */
    private SubCycleCountDTO.ShelfDTO prepareShelfDTO(Shelf shelf, CycleCountShelfSummary ccss) {
        SubCycleCountDTO.ShelfDTO shelfDTO = new SubCycleCountDTO.ShelfDTO(ccss.getSubCycleCount().getCode(), shelf.getCode(), ccss.getUser().getUsername(),
                shelf.getSection().getPickSet().getName(), ccss.getUpdated());
        shelfDTO.setPutawayPendingCount(putawayService.getPutawaysPendingForShelfCount(shelf.getId()));
        shelfDTO.setPicklistPendingCount(shippingService.getPicklistPendingForShelfCount(shelf));
        shelfDTO.setNonBarcodedCount(ccss.getNonBarcodedItemCount() != null ? ccss.getNonBarcodedItemCount() : 0);
        int expectedQuantity = 0;
        int found = 0;
        int extra = 0;
        int missing = 0;
        boolean discarded = false;
        if (ccss.isActive()) {
            List<ItemTypeInventory> itemTypeInventories = inventoryService.getAllItemTypeInventoryForShelf(shelf, false);
            expectedQuantity = getExpectedQuantity(itemTypeInventories);
            shelfDTO.setStatusCode(shelf.getStatusCode());
        } else {
            List<CycleCountItem> cycleCountItems = cycleCountDao.getCycleCountItemsForShelf(ccss.getId(), shelf.getId());
            for (CycleCountItem cci : cycleCountItems) {
                found += cci.getExpectedInventory() + cci.getInventoryDifference();
                expectedQuantity += cci.getExpectedInventory();
                if (cci.getInventoryDifference() > 0) {
                    discarded = cci.isDiscarded();
                    extra += cci.getInventoryDifference();
                } else {
                    // We don't set discarded in case of missing.
                    missing += Math.abs(cci.getInventoryDifference());
                }
            }
            //for all completed shelves
            shelfDTO.setStatusCode(Shelf.StatusCode.ACTIVE.name());
        }

        shelfDTO.setDiscarded(discarded);
        shelfDTO.setQuantity(expectedQuantity);
        shelfDTO.setFoundQuantity(found);
        shelfDTO.setExtraQuantity(extra);
        shelfDTO.setMissingQuantity(missing);
        return shelfDTO;
    }

    /**
     * @param itemTypeInventories List of item type inventories
     * @return Sum of {@code quantity} and {@code quantityBlocked} for all entries.
     * @see ItemTypeInventory
     */
    private int getExpectedQuantity(List<ItemTypeInventory> itemTypeInventories) {
        return itemTypeInventories.stream().mapToInt(iti -> iti.getQuantity() + iti.getQuantityBlocked()).sum();
    }

    /**
     * Does paginated fetching of shelves for a particular sub cycle count
     * 
     * @param request {@link GetSubCycleCountShelvesRequest} contains sub cycle count code and filters on shelf.
     * @return {@link GetSubCycleCountShelvesResponse}
     */
    @Override
    @Transactional(readOnly = true)
    public GetSubCycleCountShelvesResponse getSubCycleCount(GetSubCycleCountShelvesRequest request) {
        GetSubCycleCountShelvesResponse response = new GetSubCycleCountShelvesResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            SubCycleCount subCycleCount = cycleCountDao.getSubCycleCountByCode(request.getSubCycleCountCode());
            if (subCycleCount == null) {
                context.addError(WsResponseCode.INVALID_CODE, "Sub cycle count code does not exist");
            } else {
                if (request.isCurrentUserShelvesOnly()) {
                    User user = usersService.getUserByUsername(UserContext.current().getUniwareUserName());
                    request.setUserId(user.getId());
                }
                List<CycleCountShelfSummary> shelfSummaries = cycleCountDao.getSubCycleCountShelfSummaries(request, subCycleCount.getId());
                SubCycleCountDTO subCycleCountDTO = new SubCycleCountDTO(subCycleCount);
                List<SubCycleCountDTO.ShelfDTO> shelfDTOs = new ArrayList<>();
                subCycleCountDTO.setShelves(shelfDTOs);
                for (CycleCountShelfSummary shelfSummary : shelfSummaries) {
                    Shelf shelf = shelfSummary.getShelf();
                    if (shelf.isActive()) {
                        subCycleCountDTO.setCompletedShelfCount(subCycleCountDTO.getCompletedShelfCount() + 1);
                    } else if (shelf.isQueuedForCount()) {
                        subCycleCountDTO.setPendingShelfCount(subCycleCountDTO.getPendingShelfCount() + 1);
                    } else if (shelf.isCountRequested()) {
                        subCycleCountDTO.setBlockedShelfCount(subCycleCountDTO.getBlockedShelfCount() + 1);
                    } else if (shelf.isReadyForCount()) {
                        subCycleCountDTO.setReadyShelfCount(subCycleCountDTO.getReadyShelfCount() + 1);
                    } else if (shelf.isCountInProgress()) {
                        subCycleCountDTO.setInProgressShelfCount(subCycleCountDTO.getInProgressShelfCount() + 1);
                    }
                    shelfDTOs.add(prepareShelfDTO(shelf, shelfSummary));
                }
                response.setResultCount(cycleCountDao.getSubCycleCountShelfSummariesCount(request, subCycleCount.getId()));
                response.setSubCycleCount(subCycleCountDTO);
            }
        }
        response.addErrors(context.getErrors());
        response.setSuccessful(!context.hasErrors());
        return response;
    }

    /**
     * Gets item from code
     * 
     * @param request contains itemCode
     * @return GetItemResponse- contains {@code ItemDTO}
     */
    @Override
    public GetItemResponse getItem(GetItemRequest request) {
        GetItemResponse response = new GetItemResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            Item item = inventoryService.getItemByCode(request.getItemCode());
            if (item == null) {
                context.addError(WsResponseCode.INVALID_ITEM_CODE, "Item with this code does not exists");
            } else {
                response.setItem(new GetItemResponse.ItemDTO(item));
            }
        }
        response.addErrors(context.getErrors());
        response.setSuccessful(!context.hasErrors());
        return response;
    }

    /**
     * Gets all cycle count error items for a cycle count
     * 
     * @param request
     * @return
     */
    @Override
    @Transactional
    public GetErrorItemsForCycleCountResponse getErrorItemsForCycleCount(GetBadItemsForCycleCountRequest request) {
        GetErrorItemsForCycleCountResponse response = new GetErrorItemsForCycleCountResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            CycleCount cycleCount = cycleCountDao.getCycleCountByCode(request.getCycleCountCode());
            if (cycleCount == null) {
                context.addError(WsResponseCode.INVALID_CODE, "No Cycle count found with given code");
            } else {
                response.setShelves(cycleCountDao.getCycleCountErrorItemsPerShelf(cycleCount.getId()));
            }
        }
        response.addErrors(context.getErrors());
        response.setSuccessful(!context.hasErrors());
        return response;
    }

    /**
     * @return all cycle count error items for a shelf.
     */
    @Override
    @Transactional
    public GetBadItemsForShelfResponse getErrorItemsForShelf(GetBadItemsForShelfRequest request) {
        GetBadItemsForShelfResponse response = new GetBadItemsForShelfResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            Shelf shelf = shelfService.getShelfByCode(request.getShelfCode());
            CycleCount cycleCount = cycleCountDao.getCycleCountByCode(request.getCycleCountCode());
            if (cycleCount == null) {
                context.addError(WsResponseCode.INVALID_CODE, "No Cycle count found with given code");
            } else if (shelf == null) {
                context.addError(WsResponseCode.INVALID_SHELF_CODE, "No shelf exists with given code");
            } else {
                List<CycleCountErrorItem> badItems = cycleCountDao.getCycleCountErrorItemsForShelf(shelf.getId(), cycleCount.getId());
                List<ErrorItemDTO> items = new ArrayList<>();
                response.setItems(items);
                for (CycleCountErrorItem badItem : badItems) {
                    Item item = inventoryService.getItemByCode(badItem.getItemCode());
                    items.add(new ErrorItemDTO(item, badItem.getReasonCode(), badItem.getComment()));
                }
            }
        }
        response.addErrors(context.getErrors());
        response.setSuccessful(!context.hasErrors());
        return response;
    }

    /**
     * @ @param request Contains cycleCountCode
     * @return CloseCycleCountResponse: Generic service response
     */
    @Override
    @Transactional
    public List<CycleCountItem> getUnreconciledCycleCountItemsForShelf(Integer shelfId) {
        return cycleCountDao.getUnreconciledCycleCountItemsForShelf(shelfId);
    }

    /**
     * Used for closing cycle count. Checks the validity of cycle count, and then calls the internal method after taking
     * a lock on the cycle count code.
     * 
     * @param request Generric service request
     * @return Generic service response
     * @see #closeCycleCountInternal(String, ValidationContext, CloseCycleCountResponse)
     */
    @Override
    public CloseCycleCountResponse closeCycleCount(CloseCycleCountRequest request) {
        CloseCycleCountResponse response = new CloseCycleCountResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            CycleCount cycleCount = getActiveCycleCount();
            if (cycleCount == null) {
                context.addError(WsResponseCode.INVALID_CODE, "Cycle count does not exist.");
            } else {
                response = closeCycleCountInternal(cycleCount.getCode(), context, response);
            }
        }
        response.setSuccessful(!context.hasErrors());
        response.addErrors(context.getErrors());
        response.addWarnings(context.getWarnings());
        return response;
    }

    /**
     * Used for closing cycle count. First, checks if there are any shelves for which count is in progress. If so, adds
     * an error in {@code context} and returns. Otherwise, it fetches missing inventory from cycle count items, updates
     * item type inventory with the correct value. Then, it updates the status of shelves by calling
     * {@link #updateShelfAndCycleCountShelfSummary(Shelf, ValidationContext)}. Finally, it marks every subCycleCount as
     * COMPLETED and marks the cycle count as COMPLETED too.
     * 
     * @param cycleCountCode cycle count to be closed
     */
    @Transactional
    @RollbackOnFailure
    @Locks({ @Lock(ns = Namespace.CYCLE_COUNT, level = Level.FACILITY, key = "#{#args[0]}") })
    private CloseCycleCountResponse closeCycleCountInternal(String cycleCountCode, ValidationContext context, CloseCycleCountResponse response) {
        CycleCount cycleCount = cycleCountDao.getCycleCountByCode(cycleCountCode);
        List<Shelf> shelves = shelfService.getAllShelvesPendingInCycleCount();
        List<Shelf> countInProgressShelves = shelves.stream().filter(Shelf::isCountInProgress).collect(Collectors.toList());
        if (countInProgressShelves.size() != 0) {
            context.addError(WsResponseCode.INVALID_CODE,
                    "Can not close Cycle Count before completing the shelves : " + countInProgressShelves.stream().map(Shelf::getCode).collect(Collectors.toList()));
        } else {
            // update itemTypeInventory. Put notFound inventory at its corresponding place in itemTypeInventory
            LOG.info("Fetching missing inventory for cycle count with id: {}", cycleCount.getId());
            cycleCountDao.getMissingInventoryPerShelf(cycleCount.getId()).forEach((missingInventory) -> {
                Shelf shelf = shelfService.getShelfById(missingInventory.getShelfId());
                ItemTypeInventory itemTypeInventory = null;
                if (missingInventory.getAgeingStartDate() != null) {
                    itemTypeInventory = inventoryService.getItemTypeInventory(missingInventory.getSkuCode(), missingInventory.getInventoryType(), shelf,
                            missingInventory.getAgeingStartDate());
                }
                if (itemTypeInventory == null) {
                    itemTypeInventory = inventoryService.getItemTypeInventory(missingInventory.getSkuCode(), missingInventory.getInventoryType(), shelf, false);
                }
                itemTypeInventory.setQuantityNotFound(itemTypeInventory.getQuantityNotFound() + missingInventory.getNotFound().intValue());
                inventoryService.updateItemTypeInventory(itemTypeInventory);
            });

            // update shelf and cycleCountShelfSummary
            LOG.info("Updating cycle count shelf summaries");
            shelves.stream().filter(s -> !context.hasErrors()).forEach(s -> updateShelfAndCycleCountShelfSummary(s, context));

            // update subCycleCounts and cycleCount
            if (!context.hasErrors()) {
                cycleCountDao.getSubCycleCountByCycleCountId(cycleCount.getId()).stream().filter(SubCycleCount::isIncomplete).forEach(scc -> {
                    scc.setStatusCode(CycleCount.StatusCode.COMPLETED.name());
                    scc.setCompleted(DateUtils.getCurrentTime());
                    cycleCountDao.updateSubCycleCount(scc);
                });
                LOG.info("Marking cycle count complete.");
                cycleCount.setStatusCode(CycleCount.StatusCode.COMPLETED.name());
                cycleCount.setTotalShelves(shelfService.getTotalStockableActiveShelfCount());
                cycleCount.setCompletedShelves(cycleCountDao.getCompletedShelves(cycleCount.getId()));
                cycleCount.setCompleted(DateUtils.getCurrentTime());
                cycleCountDao.updateCycleCount(cycleCount);
            }
        }
        // setting successful here to avoid rollback.
        response.setSuccessful(!context.hasErrors());
        return response;
    }

    /**
     * Used when cycle count is being closed. Marks shelf as ACTIVE and marks its cycle count shelf summary as
     * incactive.
     * 
     * @param shelf shelf on which action is performed
     * @param context Context in which errors are put.
     * @see #closeCycleCountInternal(String, ValidationContext, CloseCycleCountResponse)
     */
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Locks({ @Lock(ns = Namespace.SHELF, level = Level.FACILITY, key = "#{#args[0].code}") })
    private void updateShelfAndCycleCountShelfSummary(Shelf shelf, ValidationContext context) {
        try {
            shelf.setStatusCode(Shelf.StatusCode.ACTIVE.name());
            shelfService.updateShelf(shelf);
            CycleCountShelfSummary ccss = cycleCountDao.getActiveCycleCountShelfSummary(shelf.getId());
            ccss.setActive(false);
            cycleCountDao.updateCycleCountShelfSummary(ccss);
        } catch (Exception e) {
            context.addError(WsResponseCode.INVALID_SHELF_CODE, "Failed to update shelf and cycle count shelf summary for shelf: " + shelf.getCode() + " Exception: " + e);
        }
    }

    /**
     * Used in {@link #completeCountForShelfInternal(CompleteCountForShelfRequest, ValidationContext, String)},
     * {@link #reconcileItemTypeInventories(CycleCountItemDetail, Shelf, ItemTypeInventory.Type)}
     */
    class CycleCountItemDetail {
        private String                       skuCode;
        private String                       itemTypeName;
        private Map<Date, ItemTypeInventory> ageingInventories = new HashMap<>();
        private Map<Date, Integer>           ageingQuantities  = new HashMap<>();

        CycleCountItemDetail(String skuCode, String itemTypeName) {
            this.skuCode = skuCode;
            this.itemTypeName = itemTypeName;
        }
    }
}
