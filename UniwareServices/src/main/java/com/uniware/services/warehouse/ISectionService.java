package com.uniware.services.warehouse;

import java.util.List;

import com.uniware.core.api.warehouse.CreateSectionRequest;
import com.uniware.core.api.warehouse.CreateSectionResponse;
import com.uniware.core.api.warehouse.EditSectionRequest;
import com.uniware.core.api.warehouse.EditSectionResponse;
import com.uniware.core.entity.Section;

public interface ISectionService {

    CreateSectionResponse createSection(CreateSectionRequest request);

    List<Section> getSections();

    List<Section> getEnabledSections();

    EditSectionResponse editSection(EditSectionRequest request);

    Section getSectionByCode(String code);

}