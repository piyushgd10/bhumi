package com.uniware.services.warehouse.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.unifier.core.annotation.Level;
import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.StringUtils;
import com.unifier.services.aspect.MarkDirty;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.api.warehouse.CreateShelfRequest;
import com.uniware.core.api.warehouse.CreateShelfTypeRequest;
import com.uniware.core.api.warehouse.CreateShelfTypeResponse;
import com.uniware.core.api.warehouse.CreateShelvesWithPredefinedCodesRequest;
import com.uniware.core.api.warehouse.CreateShelvesWithPredefinedCodesResponse;
import com.uniware.core.api.warehouse.EditShelfTypeRequest;
import com.uniware.core.api.warehouse.EditShelfTypeResponse;
import com.uniware.core.api.warehouse.GetActiveStockingShelvesCountResponse;
import com.uniware.core.api.warehouse.GetAllShelfCodesRequest;
import com.uniware.core.api.warehouse.GetAllShelfCodesResponse;
import com.uniware.core.api.warehouse.SearchShelfRequest;
import com.uniware.core.api.warehouse.SearchShelfResponse;
import com.uniware.core.api.warehouse.ShelfTypeDTO;
import com.uniware.core.api.warehouse.UpdateShelfRequest;
import com.uniware.core.api.warehouse.UpdateShelfResponse;
import com.uniware.core.entity.CycleCountItem;
import com.uniware.core.entity.ItemType;
import com.uniware.core.entity.PickSet;
import com.uniware.core.entity.Section;
import com.uniware.core.entity.Shelf;
import com.uniware.core.entity.ShelfType;
import com.uniware.core.locking.Namespace;
import com.uniware.core.locking.annotation.Lock;
import com.uniware.core.locking.annotation.Locks;
import com.uniware.dao.warehouse.ISectionDao;
import com.uniware.dao.warehouse.IShelfDao;
import com.uniware.services.admin.pickset.IPicksetService;
import com.uniware.services.cache.FacilityLayoutCache;
import com.uniware.services.cyclecount.ICycleCountService;
import com.uniware.services.inventory.IInventoryService;
import com.uniware.services.warehouse.CreateShelfResponse;
import com.uniware.services.warehouse.IShelfService;

import edu.umd.cs.findbugs.annotations.Nullable;

@Service
@Transactional
public class ShelfServiceImpl implements IShelfService {

    @Autowired
    private IShelfDao       shelfDao;

    @Autowired
    private ISectionDao     sectionDao;

    @Autowired
    private IPicksetService picksetService;

    @Autowired
    private SectionServiceImpl sectionService;

    @Autowired
    private IInventoryService  inventoryService;

    @Autowired
    private ICycleCountService cycleCountService;

    @Override
    public List<Shelf> assignSelf(ItemType itemType, int quantity) {
        return null;
    }

    /**
     * Returns shelf from code. Can return null if code is empty or null.
     * 
     * @param code
     * @return Shelf
     */
    @Nullable
    @Override
    public Shelf getShelfByCode(String code) {
        if (code == null || code.trim().equals("")) {
            return null;
        }
        return shelfDao.getShelfByCode(code);
    }

    @Override
    @Transactional(readOnly = true)
    public ShelfType getShelfTypeByCode(String code) {
        if (code == null || code.trim().equals("")) {
            return null;
        }
        return shelfDao.getShelfTypeByCode(code);
    }

    @Override
    @MarkDirty(values = { FacilityLayoutCache.class })
    public CreateShelfTypeResponse createShelfType(CreateShelfTypeRequest request) {
        CreateShelfTypeResponse response = new CreateShelfTypeResponse();
        ShelfType shelfType = new ShelfType();
        ValidationContext context = request.validate();
        if (context.hasErrors()) {
            response.setSuccessful(false);
            response.setErrors(context.getErrors());
        } else {
            if (getShelfTypeByCode(request.getCode()) != null) {
                context.addError(WsResponseCode.DUPLICATE_CODE, "Shelf code already exists:" + request.getCode());
                response.setSuccessful(false);
                response.setErrors(context.getErrors());
            } else {
                shelfType.setCode(request.getCode().toUpperCase());
                shelfType.setLength(request.getLength());
                shelfType.setWidth(request.getWidth());
                shelfType.setHeight(request.getHeight());
                shelfType.setEditable(true);
                shelfType.setEnabled(true);
                shelfType = shelfDao.createShelfType(shelfType);

                response.setShelfTypeDTO(new ShelfTypeDTO(shelfType));
                response.setSuccessful(true);
            }
        }
        return response;
    }

    @Override
    @MarkDirty(values = { FacilityLayoutCache.class })
    public EditShelfTypeResponse editShelfType(EditShelfTypeRequest request) {
        EditShelfTypeResponse response = new EditShelfTypeResponse();
        ValidationContext context = request.validate();
        if (context.hasErrors()) {
            response.setSuccessful(false);
            response.setErrors(context.getErrors());
        } else {
            ShelfType shelfType = getShelfTypeByCode(request.getCode());
            if (shelfType == null) {
                context.addError(WsResponseCode.INVALID_CODE, "Shelf type code does not exist" + request.getCode());
                response.setSuccessful(false);
                response.setErrors(context.getErrors());
            } else {
                shelfType.setCode(request.getCode());
                shelfType.setEnabled(request.isEnabled());
                shelfType = shelfDao.updateShelfType(shelfType);

                response.setShelfTypeDTO(new ShelfTypeDTO(shelfType));
                response.setSuccessful(true);
            }
        }
        return response;
    }

    @Override
    @Transactional
    public List<ShelfType> getShelfTypes() {
        return shelfDao.getShelfTypes();
    }

    @Override
    public SearchShelfResponse searchShelfs(SearchShelfRequest request) {
        SearchShelfResponse response = new SearchShelfResponse();
        ValidationContext context = request.validate();
        if (context.hasErrors()) {
            response.setSuccessful(false);
            response.setErrors(context.getErrors());
        } else {
            ShelfType shelfType = getShelfTypeByCode(request.getShelfTypeCode());
            Section section = sectionDao.getSectionByCode(request.getSectionCode());
            if (section == null) {
                context.addError(WsResponseCode.INVALID_CODE, "Please select a valid section");
                response.setSuccessful(false);
                response.setErrors(context.getErrors());
            } else {
                List<Shelf> shelfs = shelfDao.searchShelfs(section.getId(), shelfType == null ? null : shelfType.getId(), request.getCodeContains());
                for (Shelf shelf : shelfs) {
                    shelf.getSection().getCode();
                    response.addShelf(shelf);
                }
                response.setSuccessful(true);
            }
        }
        return response;
    }

    @Override
    public List<ShelfType> getEnabledShelfTypes() {
        List<ShelfType> shelfTypes = shelfDao.getShelfTypes();
        List<ShelfType> enabledShelfTypes = new ArrayList<ShelfType>();
        for (ShelfType shelfType : shelfTypes) {
            if (shelfType.isEnabled()) {
                enabledShelfTypes.add(shelfType);
            }
        }
        return enabledShelfTypes;
    }

    @Override
    @Transactional
    public List<Shelf> getShelfs(List<String> shelfCodes) {
        return shelfDao.getShelfs(shelfCodes);
    }

    @Override
    public CreateShelvesWithPredefinedCodesResponse createShelfWithPredefinedCodes(CreateShelvesWithPredefinedCodesRequest request) {
        CreateShelvesWithPredefinedCodesResponse response = new CreateShelvesWithPredefinedCodesResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            ShelfType shelfType = shelfDao.getShelfTypeByCode(request.getShelfTypeCode());
            if (shelfType == null) {
                context.addError(WsResponseCode.INVALID_CODE, "Invalid shelf type code");
            } else {
                Section section = sectionDao.getSectionByCode(request.getSectionCode());
                if (section == null) {
                    context.addError(WsResponseCode.INVALID_SECTION, "Invalid section code");
                } else {
                    List<String> duplicateCodes = new ArrayList<String>();
                    Iterator<String> it = request.getShelfCodes().iterator();
                    while (it.hasNext()) {
                        String shelfCode = it.next();
                        if (StringUtils.isBlank(shelfCode)) {
                            it.remove();
                        } else {
                            Shelf shelf = shelfDao.getShelfByCode(shelfCode);
                            if (shelf != null) {
                                duplicateCodes.add(shelfCode);
                            }
                        }
                    }

                    if (request.getShelfCodes().size() == 0) {
                        context.addError(WsResponseCode.INVALID_REQUEST, "No shelves to add");
                    } else if (duplicateCodes.size() > 0) {
                        context.addError(WsResponseCode.DUPLICATE_CODE, "Duplicate shelf codes:" + duplicateCodes);
                    } else {
                        for (String shelfCode : request.getShelfCodes()) {
                            if (StringUtils.isNotBlank(shelfCode)) {
                                Shelf shelf = new Shelf();
                                shelf.setShelfType(shelfType);
                                shelf.setTotalVolume(shelfType.getVolume());
                                shelf.setSection(section);
                                shelf.setStatusCode(Shelf.StatusCode.ACTIVE.name());
                                shelf.setCode(shelfCode);
                                shelf.setFacility(shelfType.getFacility());
                                shelf.setCreated(DateUtils.getCurrentTime());
                                shelf.setUpdated(DateUtils.getCurrentTime());
                                shelf = shelfDao.createShelf(shelf);
                                section.setLastShelfId(shelf.getId());
                                response.getShelfCodes().add(shelfCode);
                            }
                        }
                        response.setSuccessful(true);
                    }
                }
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    public GetAllShelfCodesResponse getAllShelfCodes(GetAllShelfCodesRequest request) {
        GetAllShelfCodesResponse response = new GetAllShelfCodesResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            response.setShelfCodes(getAllShelfs().stream().map(Shelf::getCode).collect(Collectors.toList()));
        }
        response.setSuccessful(true);
        return response;
    }

    @Override
    @Transactional
    public Shelf getNextAvailableShelfInStagingArea(String pickSetName) {
        Shelf shelf = null;
        PickSet pickSet = picksetService.getPickSetByName(pickSetName);
        if (pickSet != null) {
            for (PickSet stagingPickSet : pickSet.getPickArea().getPickSets()) {
                if (stagingPickSet.getType().equals(PickSet.Type.STAGING)) {
                    shelf = shelfDao.getNextShelfByPickSet(stagingPickSet.getId());
                    if (shelf != null) {
                        break;
                    }
                }
            }
        }
        return shelf;
    }

    @Override
    @Transactional
    public void updateShelf(Shelf shelf) {
        shelfDao.updateShelf(shelf);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Shelf> getShelvesForCycleCountPreProcessing() {
        return shelfDao.getShelvesForCycleCountPreProcessing();

    }

    /**
     * Gets all shelves which are in the following states: {@code READY_FOR_COUNT}, {@code QUEUED_FOR_COUNT},
     * {@code COUNT_IN_PROGRESS}, {@code COUNT_REQUESTED} in the facility, which is taken from user context.
     * 
     * @return list of shelves
     */
    @Override
    @Transactional(readOnly = true)
    public List<Shelf> getAllShelvesPendingInCycleCount() {
        return shelfDao.getAllShelvesPendingInCycleCount();
    }

    @Override
    @Transactional(readOnly = true)
    public Shelf getShelfById(int id) {
        return shelfDao.getShelfById(id);
    }

    /**
     * @return number of shelves in facility which are not {@code INACTIVE}
     */
    @Override
    @Transactional(readOnly = true)
    public Long getTotalStockableActiveShelfCount() {
        return shelfDao.getTotalStockableActiveShelfCount();
    }

    @Override
    public GetActiveStockingShelvesCountResponse getTotalActiveShelfCount() {
        GetActiveStockingShelvesCountResponse response = new GetActiveStockingShelvesCountResponse();
        response.setCount(getTotalStockableActiveShelfCount());
        response.setSuccessful(true);
        return response;
    }

    @Override
    public UpdateShelfResponse updateShelf(UpdateShelfRequest request) {
        UpdateShelfResponse response = new UpdateShelfResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            updateShelfInternal(request, context);
        }
        response.setErrors(context.getErrors());
        response.setSuccessful(!context.hasErrors());
        return response;
    }

    @Locks({ @Lock(ns = Namespace.SHELF, key = "args[0].shelfCode", level = Level.FACILITY) })
    @Transactional
    private void updateShelfInternal(UpdateShelfRequest request, ValidationContext context) {
        Shelf shelf = shelfDao.getShelfByCode(request.getShelfCode(), false);
        if (shelf == null) {
            context.addError(WsResponseCode.INVALID_SHELF_CODE, "No such shelf : " + request.getShelfCode());
        } else {
            if (request.isEnabled() != null) {
                if (!request.isEnabled() && !shelf.getStatusCode().equals(Shelf.StatusCode.ACTIVE.name())) {
                    context.addError(WsResponseCode.INVALID_SHELF_CODE, "Only Active shelves can be disabled");
                } else if (request.isEnabled() && !shelf.getStatusCode().equals(Shelf.StatusCode.INACTIVE.name())) {
                    context.addError(WsResponseCode.INVALID_SHELF_CODE, "Shelf must be in INACTIVE state to be enabled");
                } else {
                    if (!request.isEnabled()) {
                        if (isShelfEmpty(shelf)) {
                            shelf.setStatusCode(Shelf.StatusCode.INACTIVE.name());
                        } else {
                            context.addError(WsResponseCode.INVALID_SHELF_CODE, "Can not disable shelf having inventory");
                        }
                    } else {
                        shelf.setStatusCode(Shelf.StatusCode.ACTIVE.name());
                    }
                }
            }
            if (!context.hasErrors()) {
                if (StringUtils.isNotBlank(request.getShelfTypeCode()) && !shelf.getShelfType().getCode().equals(request.getShelfTypeCode())) {
                    ShelfType shelfType = shelfDao.getShelfTypeByCode(request.getShelfTypeCode());
                    if (shelfType == null) {
                        context.addError(WsResponseCode.INVALID_SHELF_CODE, "Invalid shelf type code");
                    } else {
                        shelf.setShelfType(shelfType);
                    }
                }
            }
            if (!context.hasErrors()) {
                shelfDao.updateShelf(shelf);
            }
        }
    }

    @Transactional
    public Boolean isShelfEmpty(Shelf shelf) {
        Long inventoryCount = inventoryService.getInventoryCountByShelfId(shelf.getId());
        // check shelf item count for STAGED items
        boolean isEmpty = shelf.getItemCount() <= 0 && (inventoryCount == null || inventoryCount <= 0);
        if (isEmpty) {
            List<CycleCountItem> cycleCountItems = cycleCountService.getUnreconciledCycleCountItemsForShelf(shelf.getId());
            if (cycleCountItems.stream().anyMatch(cci -> !cci.isDiscarded() && cci.getInventoryDifference() > 0)) {
                isEmpty = false;
            }
        }
        return isEmpty;
    }

    @Override
    public CreateShelfResponse createShelf(CreateShelfRequest request) {
        CreateShelfResponse response = new CreateShelfResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            createShelfInternal(request, context);
        }
        response.setErrors(context.getErrors());
        response.setSuccessful(!context.hasErrors());
        return response;
    }

    private void createShelfInternal(CreateShelfRequest request, ValidationContext context) {
        ShelfType shelfType = getShelfTypeByCode(request.getShelfTypeCode());
        if (shelfType == null) {
            context.addError(WsResponseCode.INVALID_CODE, "Invalid shelf type code");
        } else {
            Section section = sectionService.getSectionByCode(request.getSectionCode());
            if (section == null) {
                context.addError(WsResponseCode.INVALID_SECTION, "Invalid section code");
            } else if (!isShelfCodeValid(request.getShelfCode())) {
                context.addError(WsResponseCode.INVALID_CODE, "Invalid shelf code : Allowed character [a-zA-Z0-9-_,. ]");
            } else {
                createShelfInternalWithLocking(request, context, shelfType, section);
            }
        }
    }

    @Locks({ @Lock(ns = Namespace.SHELF, key = "args[0].shelfCode", level = Level.FACILITY) })
    @Transactional
    private void createShelfInternalWithLocking(CreateShelfRequest request, ValidationContext context, ShelfType shelfType, Section section) {
        section = sectionService.getSectionByCode(section.getCode());
        String shelfCode = request.getShelfCode().trim();
        Shelf shelf = shelfDao.getShelfByCode(shelfCode);
        if (shelf != null) {
            context.addError(WsResponseCode.DUPLICATE_CODE, "Shelf already exist with code:" + shelfCode);
        } else {
            shelf = new Shelf();
            shelf.setShelfType(shelfType);
            shelf.setStatusCode(request.isEnabled() ? Shelf.StatusCode.ACTIVE.name() : Shelf.StatusCode.INACTIVE.name());
            shelf.setTotalVolume(shelfType.getVolume());
            shelf.setSection(section);
            shelf.setFacility(shelfType.getFacility());
            shelf.setCode(shelfCode.trim());
            shelf.setCreated(DateUtils.getCurrentTime());
            shelf.setUpdated(DateUtils.getCurrentTime());
            shelf = shelfDao.createShelf(shelf);
            section.setLastShelfId(shelf.getId());
        }
    }

    private boolean isShelfCodeValid(String shelfCode) {
        shelfCode = shelfCode.trim();
        return StringUtils.isNotBlank(shelfCode) && shelfCode.matches("^[.,-_/ \\ta-zA-Z0-9]+$");
    }

    @Transactional(readOnly = true)
    private List<Shelf> getAllShelfs() {
        return shelfDao.getAllShelfs();
    }

}