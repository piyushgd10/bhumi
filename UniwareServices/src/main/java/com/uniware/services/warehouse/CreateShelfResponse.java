/**
 * Copyright 2017 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version 1.0, 06/09/17
 * @author aditya
 */
package com.uniware.services.warehouse;

import com.unifier.core.api.base.ServiceResponse;

public class CreateShelfResponse extends ServiceResponse{
}
