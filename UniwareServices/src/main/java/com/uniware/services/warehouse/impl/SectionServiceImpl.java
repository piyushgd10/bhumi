package com.uniware.services.warehouse.impl;

import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.utils.DateUtils;
import com.unifier.services.aspect.MarkDirty;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.api.warehouse.CreateSectionRequest;
import com.uniware.core.api.warehouse.CreateSectionResponse;
import com.uniware.core.api.warehouse.EditSectionRequest;
import com.uniware.core.api.warehouse.EditSectionResponse;
import com.uniware.core.api.warehouse.SectionDTO;
import com.uniware.core.entity.PickSet;
import com.uniware.core.entity.Section;
import com.uniware.dao.admin.pickset.IPicksetDao;
import com.uniware.dao.warehouse.ISectionDao;
import com.uniware.services.cache.FacilityLayoutCache;
import com.uniware.services.warehouse.ISectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class SectionServiceImpl implements ISectionService {

    @Autowired
    private ISectionDao sectionDao;

    @Autowired
    private IPicksetDao picksetDao;

    @Override
    @MarkDirty(values = { FacilityLayoutCache.class })
    public CreateSectionResponse createSection(CreateSectionRequest request) {
        CreateSectionResponse response = new CreateSectionResponse();
        ValidationContext context = request.validate();
        if (context.hasErrors()) {
            response.setSuccessful(false);
            response.setErrors(context.getErrors());
        } else {
            if (sectionDao.getSectionByCode(request.getCode()) != null) {
                context.addError(WsResponseCode.DUPLICATE_CODE, "Section exists with code \"" + request.getCode() + "\"");
                response.setSuccessful(false);
                response.setErrors(context.getErrors());
            } else {
                Section section = new Section();
                section.setCode(request.getCode().toUpperCase());
                section.setName(request.getName());
                section.setLastShelfId(0);
                section.setEnabled(true);
                section.setEditable(!request.isUseAsdefault());

                section.setPickSet(new PickSet(request.getPicksetId()));
                section.setCreated(DateUtils.getCurrentTime());
                section.setUpdated(DateUtils.getCurrentTime());
                section = sectionDao.createSection(section);

                response.setSuccessful(true);
                SectionDTO sectionDTO = new SectionDTO(section);
                sectionDTO.setPicksetId(request.getPicksetId());
                response.setSectionDTO(sectionDTO);
            }
        }
        return response;
    }

    @Override
    @MarkDirty(values = { FacilityLayoutCache.class })
    public EditSectionResponse editSection(EditSectionRequest request) {
        EditSectionResponse response = new EditSectionResponse();
        ValidationContext context = request.validate();
        if (context.hasErrors()) {
            response.setSuccessful(false);
            response.setErrors(context.getErrors());
        } else {
            Section section = sectionDao.getSectionByCode(request.getCode());
            if (section == null) {
                context.addError(WsResponseCode.INVALID_CODE, "No Section exists with code \"" + request.getCode() + "\"");
                response.setSuccessful(false);
                response.setErrors(context.getErrors());
            } else {
                section.setName(request.getName());
                section.setEnabled(request.isEnabled());
                section = sectionDao.editSection(section);
                response.setSuccessful(true);
                response.setSectionDTO(new SectionDTO(section));
            }
        }
        return response;
    }

    @Override
    @Transactional
    public List<Section> getSections() {
        return sectionDao.getSections();
    }

    @Override
    @Transactional(readOnly = true)
    public Section getSectionByCode(String code) {
        return sectionDao.getSectionByCode(code);
    }

    @Override
    public List<Section> getEnabledSections() {
        List<Section> sections = sectionDao.getSections();
        List<Section> enabledSections = new ArrayList<Section>();
        for (Section section : sections) {
            if (section.isEnabled()) {
                enabledSections.add(section);
            }
        }
        return enabledSections;
    }

}