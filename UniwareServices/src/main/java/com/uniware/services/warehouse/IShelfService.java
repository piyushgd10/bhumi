package com.uniware.services.warehouse;

import java.util.List;

import com.uniware.core.api.warehouse.CreateShelfRequest;
import com.uniware.core.api.warehouse.CreateShelfTypeRequest;
import com.uniware.core.api.warehouse.CreateShelfTypeResponse;
import com.uniware.core.api.warehouse.CreateShelvesWithPredefinedCodesRequest;
import com.uniware.core.api.warehouse.CreateShelvesWithPredefinedCodesResponse;
import com.uniware.core.api.warehouse.EditShelfTypeRequest;
import com.uniware.core.api.warehouse.EditShelfTypeResponse;
import com.uniware.core.api.warehouse.GetActiveStockingShelvesCountResponse;
import com.uniware.core.api.warehouse.GetAllShelfCodesRequest;
import com.uniware.core.api.warehouse.GetAllShelfCodesResponse;
import com.uniware.core.api.warehouse.SearchShelfRequest;
import com.uniware.core.api.warehouse.SearchShelfResponse;
import com.uniware.core.api.warehouse.UpdateShelfRequest;
import com.uniware.core.api.warehouse.UpdateShelfResponse;
import com.uniware.core.entity.ItemType;
import com.uniware.core.entity.Shelf;
import com.uniware.core.entity.ShelfType;

public interface IShelfService {

    /**
     * @param itemType
     * @param quantity
     * @return
     */
    List<Shelf> assignSelf(ItemType itemType, int quantity);

    /**
     * @param code
     * @return
     */
    Shelf getShelfByCode(String code);

    /**
     * @param code
     * @return
     */
    ShelfType getShelfTypeByCode(String code);

    /**
     * @param request
     * @return
     */
    CreateShelfTypeResponse createShelfType(CreateShelfTypeRequest request);

    /**
     * @param request
     * @return
     */
    EditShelfTypeResponse editShelfType(EditShelfTypeRequest request);

    /**
     * @return
     */
    List<ShelfType> getShelfTypes();

    List<ShelfType> getEnabledShelfTypes();

    /**
     * @param request
     * @return
     */
    SearchShelfResponse searchShelfs(SearchShelfRequest request);

    /**
     * @param shelfCodes
     * @return
     */
    List<Shelf> getShelfs(List<String> shelfCodes);

    /**
     * @param request
     * @return
     */
    CreateShelvesWithPredefinedCodesResponse createShelfWithPredefinedCodes(CreateShelvesWithPredefinedCodesRequest request);

    GetAllShelfCodesResponse getAllShelfCodes(GetAllShelfCodesRequest request);

    Shelf getNextAvailableShelfInStagingArea(String pickSetName);

    void updateShelf(Shelf shelf);

    List<Shelf> getShelvesForCycleCountPreProcessing();

    List<Shelf> getAllShelvesPendingInCycleCount();

    Shelf getShelfById(int shelfId);

    Long getTotalStockableActiveShelfCount();

    GetActiveStockingShelvesCountResponse getTotalActiveShelfCount();

    UpdateShelfResponse updateShelf(UpdateShelfRequest request);

    CreateShelfResponse createShelf(CreateShelfRequest request);

}