package com.uniware.services.warehouse.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.uniware.core.api.admin.pickset.CreatePickAreaRequest;
import com.uniware.core.api.admin.pickset.CreatePickAreaResponse;
import com.uniware.core.entity.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.geo.GeoResults;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.unifier.core.api.application.FacilityAllocationRuleDTO;
import com.unifier.core.api.application.SystemConfigurationDTO;
import com.unifier.core.api.user.AddUserRolesRequest;
import com.unifier.core.api.user.AddUserRolesResponse;
import com.unifier.core.api.validation.ResponseCode;
import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.entity.BinaryObject;
import com.unifier.core.entity.Role.Code;
import com.unifier.core.entity.SystemConfig;
import com.unifier.core.utils.CollectionUtils;
import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.GeolocationUtils;
import com.unifier.core.utils.JibxUtils;
import com.unifier.core.utils.NumberUtils;
import com.unifier.core.utils.StringUtils;
import com.unifier.core.utils.ValidatorUtils;
import com.unifier.scraper.sl.runtime.ScraperScript;
import com.unifier.scraper.sl.runtime.ScriptExecutionContext;
import com.unifier.services.application.IApplicationSetupService;
import com.unifier.services.aspect.MarkDirty;
import com.unifier.services.aspect.RollbackOnFailure;
import com.unifier.services.users.IUsersService;
import com.unifier.services.utils.CustomFieldUtils;
import com.unifier.services.vo.TenantSetupPropertyVO;
import com.uniware.core.api.admin.pickset.CreatePicksetRequest;
import com.uniware.core.api.admin.pickset.CreatePicksetResponse;
import com.uniware.core.api.catalog.CreateShippingProviderLocationRequest;
import com.uniware.core.api.channel.AddChannelConnectorRequest;
import com.uniware.core.api.channel.AddChannelConnectorResponse;
import com.uniware.core.api.channel.AddChannelResponse;
import com.uniware.core.api.channel.EditChannelResponse;
import com.uniware.core.api.facility.CreateFacilityAllocationRuleRequest;
import com.uniware.core.api.facility.CreateFacilityAllocationRuleResponse;
import com.uniware.core.api.facility.CreateFacilityChannelRequest;
import com.uniware.core.api.facility.CreateFacilityChannelResponse;
import com.uniware.core.api.facility.CreateFacilityProfileRequest;
import com.uniware.core.api.facility.CreateFacilityRequest;
import com.uniware.core.api.facility.CreateFacilityResponse;
import com.uniware.core.api.facility.DeleteFacilityAllocationRuleRequest;
import com.uniware.core.api.facility.DeleteFacilityAllocationRuleResponse;
import com.uniware.core.api.facility.EditFacilityAllocationRuleRequest;
import com.uniware.core.api.facility.EditFacilityAllocationRuleResponse;
import com.uniware.core.api.facility.EditFacilityChannelRequest;
import com.uniware.core.api.facility.EditFacilityChannelResponse;
import com.uniware.core.api.facility.GetAllFacilityDetailsResponse;
import com.uniware.core.api.facility.GetSellerDetailsFromTinRequest;
import com.uniware.core.api.facility.GetSellerDetailsFromTinResponse;
import com.uniware.core.api.facility.ReorderFacilityAllocationRulesRequest;
import com.uniware.core.api.facility.ReorderFacilityAllocationRulesResponse;
import com.uniware.core.api.location.GetPincodesWithinRangeRequest;
import com.uniware.core.api.location.GetPincodesWithinRangeResponse;
import com.uniware.core.api.model.WSShippingMethod;
import com.uniware.core.api.model.WsShippingProviderDetail;
import com.uniware.core.api.party.PartyAddressDTO;
import com.uniware.core.api.party.WsFacility;
import com.uniware.core.api.party.WsFacilityProfile;
import com.uniware.core.api.party.WsPartyAddress;
import com.uniware.core.api.party.WsPartyContact;
import com.uniware.core.api.party.WsStoreHoliday;
import com.uniware.core.api.party.WsStoreTimings;
import com.uniware.core.api.sequence.WsSequence;
import com.uniware.core.api.shipping.AddShippingProviderRequest;
import com.uniware.core.api.shipping.AddShippingProviderResponse;
import com.uniware.core.api.shipping.EditShippingProviderRequest;
import com.uniware.core.api.shipping.EditShippingProviderResponse;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.api.warehouse.CreateSectionRequest;
import com.uniware.core.api.warehouse.CreateSectionResponse;
import com.uniware.core.api.warehouse.CreateShelfTypeRequest;
import com.uniware.core.api.warehouse.CreateShelfTypeResponse;
import com.uniware.core.api.warehouse.CreateShelvesWithPredefinedCodesRequest;
import com.uniware.core.api.warehouse.CreateShelvesWithPredefinedCodesResponse;
import com.uniware.core.api.warehouse.CreateShippingPackageTypeRequest;
import com.uniware.core.api.warehouse.CreateShippingPackageTypeResponse;
import com.uniware.core.api.warehouse.EditFacilityRequest;
import com.uniware.core.api.warehouse.EditFacilityResponse;
import com.uniware.core.api.warehouse.EditPartySequenceRequest;
import com.uniware.core.api.warehouse.EditPartySequenceResponse;
import com.uniware.core.api.warehouse.FacilityDTO;
import com.uniware.core.api.warehouse.GetFacilityRequest;
import com.uniware.core.api.warehouse.GetFacilityResponse;
import com.uniware.core.cache.FacilityCache;
import com.uniware.core.concurrent.ContextAwareExecutorFactory;
import com.uniware.core.entity.Facility.Type;
import com.uniware.core.entity.FacilityProfile.DayTiming;
import com.uniware.core.entity.FacilityProfile.DayTiming.DayOfWeek;
import com.uniware.core.entity.FacilityProfile.FacilityHoliday;
import com.uniware.core.entity.FacilityProfile.StoreTiming;
import com.uniware.core.entity.PickSet.PickSetType;
import com.uniware.core.entity.Section.SectionCode;
import com.uniware.core.entity.Sequence.Level;
import com.uniware.core.entity.Sequence.Name;
import com.uniware.core.entity.ShelfType.ShelfTypeCode;
import com.uniware.core.utils.UserContext;
import com.uniware.core.vo.UniwareScriptVO;
import com.uniware.dao.tenant.ITenantSetupMao;
import com.uniware.dao.warehouse.IFacilityDao;
import com.uniware.dao.warehouse.IFacilityMao;
import com.uniware.services.admin.pickset.IPicksetService;
import com.uniware.services.billing.party.IBillingPartyService;
import com.uniware.services.cache.ChannelCache;
import com.uniware.services.cache.ScriptVersionedCache;
import com.uniware.services.cache.ServiceabilityCache;
import com.uniware.services.channel.IChannelService;
import com.uniware.services.channel.saleorder.impl.ChannelOrderSyncServiceImpl;
import com.uniware.services.common.ISequenceGenerator;
import com.uniware.services.invoice.IInvoiceService;
import com.uniware.services.location.ILocationService;
import com.uniware.services.party.IGenericPartyService;
import com.uniware.services.pickup.IStoreAvailabilityNotifyService;
import com.uniware.services.pickup.SellerRegistrationMessage;
import com.uniware.services.shipping.IShippingAdminService;
import com.uniware.services.shipping.IShippingProviderService;
import com.uniware.services.shipping.IShippingService;
import com.uniware.services.tenant.ITenantService;
import com.uniware.services.warehouse.IFacilityService;
import com.uniware.services.warehouse.ISectionService;
import com.uniware.services.warehouse.IShelfService;

@Service
public class FacilityServiceImpl implements IFacilityService {

    private static final String             SERVICE_NAME                      = "facilityService";
    private static final Logger             LOG                               = LoggerFactory.getLogger(ChannelOrderSyncServiceImpl.class);

    @Autowired
    private IFacilityDao                    facilityDao;

    @Autowired
    private IFacilityMao                    facilityMao;

    @Autowired
    private IUsersService                   usersService;

    @Autowired
    private IShippingAdminService           shippingAdminService;

    @Autowired
    private IPicksetService                 picksetService;

    @Autowired
    private ISectionService                 sectionService;

    @Autowired
    private IShelfService                   shelfService;

    @Autowired
    private ISequenceGenerator              sequenceGenerator;

    @Autowired
    ITenantService                          tenantService;

    @Autowired
    private IGenericPartyService            genericPartyService;

    @Autowired
    private ITenantSetupMao                 tenantSetupConfigurationMao;

    @Autowired
    private IBillingPartyService            billingPartyService;

    @Autowired
    private ILocationService                locationService;

    @Autowired
    private IShippingProviderService        shippingProviderService;

    @Autowired
    private IShippingService                shippingService;

    @Autowired
    private IChannelService                 channelService;

    @Autowired
    private ContextAwareExecutorFactory     executorFactory;

    @Autowired
    private IApplicationSetupService        applicationSetupService;

    @Autowired
    private IStoreAvailabilityNotifyService storeAvailabilityNotifyService;

    @Autowired
    private IInvoiceService                 invoiceService;

    private static final String             INVENTORY_MANAGEMENT_SWITCHED_OFF = "inventory.management.switched.off";

    @Override
    @Transactional
    @RollbackOnFailure
    @MarkDirty(values = { FacilityCache.class, ServiceabilityCache.class })
    public EditFacilityResponse editFacility(EditFacilityRequest request) {
        EditFacilityResponse response = new EditFacilityResponse();
        //TODO - should be removed when all Party creation/editing are moved to new services
        if (request.getFacility() != null) {
            updateRequestBeforeValidation(request.getFacility());
        }
        ValidationContext context = request.validate(false);
        if (context.hasErrors()) {
            response.setSuccessful(false);
            response.setErrors(context.getErrors());
        } else {
            WsFacility reqFacility = request.getFacility();
            Facility facility = facilityDao.getAllFacilityByCode(reqFacility.getCode());

            if (facility == null) {
                context.addError(WsResponseCode.INVALID_FACILITY_CODE, "Could not find any facility with code : " + reqFacility.getCode());
            } else {
                facility.setType(ValidatorUtils.getOrDefaultValue(reqFacility.getType(), facility.getType()));
                facility.setDisplayName(ValidatorUtils.getOrDefaultValue(reqFacility.getDisplayName(), facility.getDisplayName()));
                facility.setDummy(false);
                facility.setSalesTaxOnShippingCharges(ValidatorUtils.getOrDefaultValue(reqFacility.isSalesTaxOnShippingCharges(), facility.isSalesTaxOnShippingCharges()));
                if (request.getFacility().getBinaryObjectId() != null) {
                    facility.setBinaryObject(new BinaryObject(reqFacility.getBinaryObjectId()));
                }
                if (request.getFacility().getSignatureBinaryObjectId() != null) {
                    facility.setSignatureBinaryObject(new BinaryObject(reqFacility.getSignatureBinaryObjectId()));
                }
                Map<String, Object> customFieldValues = CustomFieldUtils.getCustomFieldValues(context, Facility.class.getName(), reqFacility.getCustomFieldValues());
                CustomFieldUtils.setCustomFieldValues(facility, customFieldValues, false);
                genericPartyService.prepareParty(facility, reqFacility, context);
                if (!context.hasErrors()) {
                    facility = facilityDao.editFacility(facility);
                    if (!facility.isEnabled()) {
                        facilityDao.resetFacilityUsers(facility);
                    }
                    //edit sequences
                    try {
                        Sequence retailInvoiceSequence = sequenceGenerator.getSequenceByName(Sequence.Name.INVOICE.name(), facility.getId());
                        Sequence taxInvoiceSequence = sequenceGenerator.getSequenceByName(Sequence.Name.TAX_INVOICE.name(), facility.getId());
                        if (request.getRetailInvoiceSequence() != null) {
                            String newRetailPrefix = request.getRetailInvoiceSequence().getPrefix();
                            String newRetailNextYearPrefix = request.getRetailInvoiceSequence().getNextYearPrefix();
                            if (StringUtils.isNotBlank(newRetailPrefix) && newRetailPrefix.equalsIgnoreCase(taxInvoiceSequence.getPrefix())) {
                                context.addError(WsResponseCode.DUPLICATE_SEQUENCE_PREFIX, "Retail Invoice and Tax Invoice sequence prefix can not be same");
                            } else if(StringUtils.isNotBlank(newRetailNextYearPrefix) && newRetailNextYearPrefix.equalsIgnoreCase(taxInvoiceSequence.getNextYearPrefix())) {
                                context.addError(WsResponseCode.DUPLICATE_SEQUENCE_PREFIX, "Retail Invoice and Tax Invoice sequence next year prefix can not be same");
                            } else {
                                BillingParty bp = null;
                                if(StringUtils.isNotBlank(newRetailPrefix)) {
                                    bp = findBillingPartyWithSameSequencePrefix(newRetailPrefix);
                                }
                                if (bp == null) {
                                    updateSequences(Sequence.Name.INVOICE.name(), retailInvoiceSequence, request.getRetailInvoiceSequence(), facility,
                                            context);
                                } else {
                                    context.addError(WsResponseCode.DUPLICATE_SEQUENCE_PREFIX, "Prefix already exists for Billing Party : " + bp.getName());
                                }
                            }
                        }
                        if (request.getTaxInvoiceSequence() != null) {
                            String newTaxPrefix = request.getTaxInvoiceSequence().getPrefix();
                            String newTaxNextYearPrefix = request.getTaxInvoiceSequence().getNextYearPrefix();
                            if (StringUtils.isNotBlank(newTaxPrefix) && newTaxPrefix.equalsIgnoreCase(retailInvoiceSequence.getPrefix())) {
                                context.addError(WsResponseCode.DUPLICATE_SEQUENCE_PREFIX, "Retail Invoice and Tax Invoice sequence prefix can not be same");
                            } else if(StringUtils.isNotBlank(newTaxNextYearPrefix) && newTaxNextYearPrefix.equalsIgnoreCase(retailInvoiceSequence.getNextYearPrefix())) {
                                context.addError(WsResponseCode.DUPLICATE_SEQUENCE_PREFIX, "Retail Invoice and Tax Invoice sequence next year prefix can not be same");
                            } else {
                                BillingParty bp = null;
                                if(StringUtils.isNotBlank(newTaxPrefix)) {
                                    bp = findBillingPartyWithSameSequencePrefix(newTaxPrefix);
                                }
                                if (bp == null) {
                                    updateSequences(Sequence.Name.TAX_INVOICE.name(), taxInvoiceSequence, request.getTaxInvoiceSequence(), facility, context);
                                } else {
                                    context.addError(WsResponseCode.DUPLICATE_SEQUENCE_PREFIX, "Prefix already exists for Billing Party : " + bp.getName());
                                }
                            }
                        }
                        response.setSuccessful(true);
                        FacilityDTO facilityDTO = prepareFacilityDTO(facility, retailInvoiceSequence, taxInvoiceSequence);
                        response.setFacilityDTO(facilityDTO);
                    } catch (IllegalArgumentException e) {
                        context.addError(ResponseCode.INVALID_FORMAT, e.getMessage());
                    }
                }

            }
            if (context.hasErrors()) {
                response.setSuccessful(false);
                response.setErrors(context.getErrors());
            }
        }
        return response;
    }

    @Override
    public ValidationContext updateSequences(String invoiceSequenceName, Sequence invoiceSequence, WsSequence sequence, Facility facility, ValidationContext context) {
        /* Null and Blank NextYearPrefix can be on multiple facilities */
        if (StringUtils.isBlank(sequence.getNextYearPrefix()) || invoiceService.isNextInvoicePrefixValid(sequence.getNextYearPrefix())) {
            sequenceGenerator.updateSequence(invoiceSequenceName, (facility == null) ? null : facility.getId(), sequence.getPrefix(), sequence.getCurrentValue() - 1, sequence.getNextYearPrefix(),
                    sequence.isResetCounterNextYear());
        } else {
            context.addError(WsResponseCode.DUPLICATE_SEQUENCE_PREFIX, "Next Year Sequence Prefix already used Facility or Billing Party, prefix  " + sequence.getNextYearPrefix());
        }
        return context;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Facility> getAllEnabledFacilities() {
        return facilityDao.getAllEnabledFacilities();
    }

    @Override
    @Transactional(readOnly = true)
    public List<Facility> getAllEnabledFacilitiesWithGstNumber() {
        return facilityDao.getAllEnabledFacilitiesWithGstNumber();
    }

    @Override
    @Transactional(readOnly = true)
    public List<Facility> getAllFacilities() {
        return facilityDao.getAllFacilities();
    }

    @Override
    @Transactional(readOnly = true)
    public GetFacilityResponse getFacilityByCode(GetFacilityRequest request) {
        GetFacilityResponse response = new GetFacilityResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            Facility facility = facilityDao.getAllFacilityByCode(request.getCode());
            if (facility != null) {
                Sequence retailInvoiceSequence = sequenceGenerator.getSequenceByName(Sequence.Name.INVOICE.name(), facility.getId());
                Sequence taxInvoiceSequence = sequenceGenerator.getSequenceByName(Sequence.Name.TAX_INVOICE.name(), facility.getId());
                FacilityDTO facilityDTO = prepareFacilityDTO(facility, retailInvoiceSequence, taxInvoiceSequence);
                facilityDTO.setDummy(facility.isDummy());
                response.setFacilityDTO(facilityDTO);
                response.setSuccessful(true);
            } else {
                response.setSuccessful(false);
                context.addError(WsResponseCode.INVALID_FACILITY_CODE, "Could not find any facility for provided code");
            }
        } else {
            response.setSuccessful(false);
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional(readOnly = true)
    public PartyAddressDTO getAddresssByFacilityCode(String code, String partyAddressType) {
        Facility facility = facilityDao.getFacilityByCode(code);
        if (facility != null) {
            PartyAddress partyAddress = facility.getPartyAddressByType(partyAddressType);
            if (partyAddress != null) {
                return new PartyAddressDTO(partyAddress);
            }
        }
        return null;
    }

    private FacilityAllocationRuleDTO prepareAllocationRuleDTO(FacilityAllocationRule facilityAllocationRule) {
        FacilityAllocationRuleDTO facilityAllocationRuleDTO = new FacilityAllocationRuleDTO(facilityAllocationRule);
        return facilityAllocationRuleDTO;
    }

    @Override
    @Transactional
    @MarkDirty(values = { FacilityCache.class })
    public CreateFacilityAllocationRuleResponse createAllocationRule(CreateFacilityAllocationRuleRequest request) {
        CreateFacilityAllocationRuleResponse response = new CreateFacilityAllocationRuleResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            if (facilityDao.getAllocationRuleByName(request.getName()) == null) {
                FacilityAllocationRule facilityAllocationRule = new FacilityAllocationRule();
                facilityAllocationRule.setName(request.getName());
                facilityAllocationRule.setFacility(facilityDao.getFacilityByCode(request.getFacilityCode()));
                facilityAllocationRule.setConditionExpressionText(request.getConditionExpressionText());
                facilityAllocationRule.setCreated(DateUtils.getCurrentTime());
                facilityAllocationRule.setEnabled(true);
                facilityAllocationRule.setPreference(request.getPreference());
                facilityAllocationRule = facilityDao.createAllocationRule(facilityAllocationRule);
                response.setFacilityAllocationRule(prepareAllocationRuleDTO(facilityAllocationRule));
                response.setSuccessful(true);
            } else {
                context.addError(WsResponseCode.INVALID_REQUEST, request.getName() + " name already exist");
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional(readOnly = true)
    public GetAllFacilityDetailsResponse getAllFacilityDetails() {
        GetAllFacilityDetailsResponse response = new GetAllFacilityDetailsResponse();
        List<FacilityDTO> facilities = new ArrayList<FacilityDTO>();
        for (Facility facility : getAllFacilities()) {
            Sequence retailInvoiceSequence = sequenceGenerator.getSequenceByName(Sequence.Name.INVOICE.name(), facility.getId());
            Sequence taxInvoiceSequence = sequenceGenerator.getSequenceByName(Sequence.Name.TAX_INVOICE.name(), facility.getId());
            FacilityDTO facilityDTO = prepareFacilityDTO(facility, retailInvoiceSequence, taxInvoiceSequence);
            facilities.add(facilityDTO);
        }
        response.getFacilities().addAll(facilities);
        response.setSuccessful(true);
        return response;
    }

    @Override
    @Transactional
    @MarkDirty(values = { FacilityCache.class })
    public EditFacilityAllocationRuleResponse editAllocationRule(EditFacilityAllocationRuleRequest request) {
        EditFacilityAllocationRuleResponse response = new EditFacilityAllocationRuleResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            FacilityAllocationRule facilityAllocationRule = facilityDao.getAllocationRuleByName(request.getName());
            if (facilityAllocationRule != null) {
                facilityAllocationRule.setFacility(facilityDao.getFacilityByCode(request.getFacilityCode()));
                facilityAllocationRule.setConditionExpressionText(request.getConditionExpressionText());
                facilityAllocationRule.setEnabled(request.isEnabled());
                facilityAllocationRule = facilityDao.editAllocationRule(facilityAllocationRule);
                response.setFacilityAllocationRule(prepareAllocationRuleDTO(facilityAllocationRule));
                response.setSuccessful(true);
            } else {
                context.addError(WsResponseCode.INVALID_REQUEST, request.getName() + " can't be edited");
            }
        }
        response.setErrors(context.getErrors());
        return response;

    }

    @Override
    @Transactional
    @MarkDirty(values = { FacilityCache.class })
    public DeleteFacilityAllocationRuleResponse deleteAllocationRule(DeleteFacilityAllocationRuleRequest request) {
        DeleteFacilityAllocationRuleResponse response = new DeleteFacilityAllocationRuleResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            FacilityAllocationRule facilityAllocationRule = facilityDao.getAllocationRuleByName(request.getName());
            if (facilityAllocationRule != null) {
                facilityDao.deleteAllocationRule(facilityAllocationRule.getName());
                response.setSuccessful(true);
            } else {
                context.addError(WsResponseCode.INVALID_REQUEST, request.getName() + " does not exist");
            }

        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    @MarkDirty(values = { FacilityCache.class })
    public ReorderFacilityAllocationRulesResponse reorderAllocationRules(ReorderFacilityAllocationRulesRequest request) {
        ReorderFacilityAllocationRulesResponse response = new ReorderFacilityAllocationRulesResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            Map<String, Integer> allocationRuleNameToPreferenceMap = request.getAllocationRuleNameToPreferenceMap();
            List<FacilityAllocationRule> facilityAllocationRules = facilityDao.getAllocationRules();
            for (FacilityAllocationRule allocationRule : facilityAllocationRules) {
                Integer preference = allocationRuleNameToPreferenceMap.get(allocationRule.getName());
                allocationRule.setPreference(preference);
            }
            response.setSuccessful(true);
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    public List<AnnualHoliday> getAllAnnualHolidays() {
        List<AnnualHoliday> holidays = facilityMao.getAllHolidaysByCountryCode("IN");
        for (AnnualHoliday holiday : holidays) {
            holiday.setTimestamp(holiday.getDate().getTime());
        }
        return holidays;
    }

    @Override
    @MarkDirty(values = { FacilityCache.class, ServiceabilityCache.class })
    public EditFacilityResponse editFacilityProfile(CreateFacilityProfileRequest request) {
        EditFacilityResponse response = new EditFacilityResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            Facility facility = getAllFacilityByCode(request.getFacilityProfile().getFacilityCode());
            if (facility == null) {
                context.addError(WsResponseCode.INVALID_FACILITY_CODE, "Provided facility code is invalid.");
            } else {
                long startTime = System.currentTimeMillis();
                FacilityProfile facilityProfile = facilityMao.getFacilityProfileByCode(request.getFacilityProfile().getFacilityCode());
                LOG.info("Time taken to fetch facility profile " + (System.currentTimeMillis() - startTime) / 1000);
                if (facilityProfile == null) {
                    context.addError(WsResponseCode.INVALID_FACILITY_CODE, "Provided facility code is invalid.");
                } else {
                    WsFacilityProfile wsFacilityProfile = request.getFacilityProfile();
                    if (wsFacilityProfile.getAutoSetupServiceability() && (wsFacilityProfile.getDeliveryRadius() == null || wsFacilityProfile.getDeliveryRadius() == 0)) {
                        context.addError(WsResponseCode.INVALID_FACILITY_CONFIGURATION, "Delivery radius should be filled up for auto serviceability set up");
                    }
                    if (!context.hasErrors()) {
                        facilityProfile.setAutoSetupServiceability(wsFacilityProfile.getAutoSetupServiceability());
                        facilityProfile.setAutoSetupPickupServiceability(wsFacilityProfile.getAutoSetupPickupServiceability());
                        facilityProfile.setSellerName(wsFacilityProfile.getSellerName());
                        facilityProfile.setContactEmail(wsFacilityProfile.getContactEmail());
                        facilityProfile.setMobile(wsFacilityProfile.getMobile());
                        facilityProfile.setAlternatePhone(wsFacilityProfile.getAlternatePhone());
                        double[] position = new double[2];
                        position[0] = wsFacilityProfile.getPosition().get(0);
                        position[1] = wsFacilityProfile.getPosition().get(1);
                        facilityProfile.setPosition(position);
                        facilityProfile.setStoreDeliveryEnabled(wsFacilityProfile.getStoreDeliveryEnabled());
                        facilityProfile.setDeliveryRadius(wsFacilityProfile.getDeliveryRadius());
                        facilityProfile.setPickupRadius(wsFacilityProfile.getPickupRadius());
                        facilityProfile.setStorePickupEnabled(wsFacilityProfile.getStorePickupEnabled());
                        facilityProfile.setDeliverySLA(wsFacilityProfile.getDeliverySLA());
                        facilityProfile.setDispatchSLA(wsFacilityProfile.getDispatchSLA());
                        facilityProfile.setPickupShippingMethods(wsFacilityProfile.getPickupShippingMethods());
                        facilityProfile.setSelfShippingMethods(wsFacilityProfile.getSelfShippingMethods());
                        StoreTiming storeTiming = new StoreTiming();
                        List<DayTiming> timings = new ArrayList<>();
                        List<DayOfWeek> days = new ArrayList<>();
                        List<FacilityHoliday> holidays = new ArrayList<>();
                        storeTiming.setDayTimings(timings);
                        facilityProfile.setHolidays(holidays);
                        facilityProfile.setStoreTimings(storeTiming);
                        for (WsStoreTimings storeTimings : wsFacilityProfile.getStoreTimings()) {
                            DayTiming dayTiming = new DayTiming();
                            dayTiming.setDayOfWeek(DayOfWeek.valueOf(storeTimings.getDayOfWeek()));
                            days.add(DayOfWeek.valueOf(storeTimings.getDayOfWeek()));
                            dayTiming.setClosingTime(storeTimings.getClosingTime());
                            dayTiming.setOpeningTime(storeTimings.getOpeningTime());
                            dayTiming.setClosed(false);
                            timings.add(dayTiming);
                        }
                        for (DayOfWeek day : DayOfWeek.values()) {
                            if (!days.contains(day)) {
                                DayTiming dayTiming = new DayTiming();
                                dayTiming.setDayOfWeek(day);
                                dayTiming.setClosed(true);
                                timings.add(dayTiming);
                            }
                        }
                        for (WsStoreHoliday wsHoliday : wsFacilityProfile.getStoreHolidays()) {
                            FacilityHoliday holiday = new FacilityHoliday();
                            holiday.setName(wsHoliday.getName());
                            holiday.setDate(wsHoliday.getDate());
                            holidays.add(holiday);
                        }
                        setupFacilityProfileConfiguration(facility, context, facilityProfile, wsFacilityProfile.getPickupShippingMethods(),
                                wsFacilityProfile.getSelfShippingMethods());
                        if (!context.hasErrors()) {
                            startTime = System.currentTimeMillis();
                            facilityMao.save(facilityProfile);
                            facilityMao.ensureIndexOnFacilityProfile();
                            LOG.info("Save facility profile mao " + (System.currentTimeMillis() - startTime) / 1000);
                            Sequence retailInvoiceSequence = sequenceGenerator.getSequenceByName(Sequence.Name.INVOICE.name(), facility.getId());
                            Sequence taxInvoiceSequence = sequenceGenerator.getSequenceByName(Sequence.Name.TAX_INVOICE.name(), facility.getId());
                            response.setFacilityDTO(prepareFacilityDTO(facility, retailInvoiceSequence, taxInvoiceSequence));
                            response.setSuccessful(true);
                        }
                    }
                }
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Integer> getFacilityIdsWithPendingSaleOrders(Integer pendingSinceInHours, List<String> pendingOrderStatuses) {
        return facilityDao.getFacilityIdsWithPendingSaleOrders(pendingSinceInHours, pendingOrderStatuses);
    }

    @Override
    @MarkDirty(values = { FacilityCache.class, ServiceabilityCache.class })
    public CreateFacilityResponse createFacilityProfile(CreateFacilityProfileRequest request) {
        CreateFacilityResponse response = new CreateFacilityResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            Facility facility = getAllFacilityByCode(request.getFacilityProfile().getFacilityCode());
            if (facility == null) {
                context.addError(WsResponseCode.INVALID_FACILITY_CODE, "Provided facility code is invalid.");
            } else {
                WsFacilityProfile wsFacilityProfile = request.getFacilityProfile();
                if (wsFacilityProfile.getAutoSetupServiceability() && (wsFacilityProfile.getDeliveryRadius() == null || wsFacilityProfile.getDeliveryRadius() == 0)) {
                    context.addError(WsResponseCode.INVALID_FACILITY_CONFIGURATION, "Delivery radius should be filled up for auto serviceability set up");
                }
                if (!context.hasErrors()) {
                    FacilityProfile facilityProfile = new FacilityProfile();
                    facilityProfile.setFacilityCode(facility.getCode());
                    facilityProfile.setSellerName(wsFacilityProfile.getSellerName());
                    facilityProfile.setContactEmail(wsFacilityProfile.getContactEmail());
                    facilityProfile.setMobile(wsFacilityProfile.getMobile());
                    facilityProfile.setAlternatePhone(wsFacilityProfile.getAlternatePhone());
                    facilityProfile.setAutoSetupServiceability(wsFacilityProfile.getAutoSetupServiceability());
                    facilityProfile.setAutoSetupPickupServiceability(wsFacilityProfile.getAutoSetupPickupServiceability());
                    double[] position = new double[2];
                    position[0] = wsFacilityProfile.getPosition().get(0);
                    position[1] = wsFacilityProfile.getPosition().get(1);
                    facilityProfile.setPosition(position);
                    facilityProfile.setStoreDeliveryEnabled(wsFacilityProfile.getStoreDeliveryEnabled());
                    facilityProfile.setDeliveryRadius(wsFacilityProfile.getDeliveryRadius());
                    facilityProfile.setPickupRadius(wsFacilityProfile.getPickupRadius());
                    facilityProfile.setStorePickupEnabled(wsFacilityProfile.getStorePickupEnabled());
                    facilityProfile.setDeliverySLA(wsFacilityProfile.getDeliverySLA());
                    facilityProfile.setDispatchSLA(wsFacilityProfile.getDispatchSLA());
                    facilityProfile.setPickupShippingMethods(wsFacilityProfile.getPickupShippingMethods());
                    facilityProfile.setSelfShippingMethods(wsFacilityProfile.getSelfShippingMethods());

                    StoreTiming storeTiming = new StoreTiming();
                    List<DayTiming> timings = new ArrayList<>();
                    List<DayOfWeek> days = new ArrayList<>();
                    List<FacilityHoliday> holidays = new ArrayList<>();
                    storeTiming.setDayTimings(timings);
                    facilityProfile.setHolidays(holidays);
                    facilityProfile.setStoreTimings(storeTiming);
                    for (WsStoreTimings storeTimings : wsFacilityProfile.getStoreTimings()) {
                        DayTiming dayTiming = new DayTiming();
                        dayTiming.setDayOfWeek(DayOfWeek.valueOf(storeTimings.getDayOfWeek()));
                        days.add(DayOfWeek.valueOf(storeTimings.getDayOfWeek()));
                        dayTiming.setClosingTime(storeTimings.getClosingTime());
                        dayTiming.setOpeningTime(storeTimings.getOpeningTime());
                        dayTiming.setClosed(false);
                        timings.add(dayTiming);
                    }
                    for (DayOfWeek day : DayOfWeek.values()) {
                        if (!days.contains(day)) {
                            DayTiming dayTiming = new DayTiming();
                            dayTiming.setDayOfWeek(day);
                            dayTiming.setClosed(true);
                            timings.add(dayTiming);
                        }
                    }
                    for (WsStoreHoliday wsHoliday : wsFacilityProfile.getStoreHolidays()) {
                        FacilityHoliday holiday = new FacilityHoliday();
                        holiday.setName(wsHoliday.getName());
                        holiday.setDate(wsHoliday.getDate());
                        holidays.add(holiday);
                    }
                    setupFacilityProfileConfiguration(facility, context, facilityProfile, wsFacilityProfile.getPickupShippingMethods(), wsFacilityProfile.getSelfShippingMethods());
                    if (!context.hasErrors()) {
                        facilityMao.save(facilityProfile);
                        facilityMao.ensureIndexOnFacilityProfile();
                        Sequence retailInvoiceSequence = sequenceGenerator.getSequenceByName(Sequence.Name.INVOICE.name(), facility.getId());
                        Sequence taxInvoiceSequence = sequenceGenerator.getSequenceByName(Sequence.Name.TAX_INVOICE.name(), facility.getId());
                        response.setFacilityDTO(prepareFacilityDTO(facility, retailInvoiceSequence, taxInvoiceSequence));
                        response.setSuccessful(true);
                    }
                }
            }

        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @MarkDirty(values = { FacilityCache.class, ChannelCache.class })
    public EditFacilityChannelResponse editFacilityAndChannel(EditFacilityChannelRequest request) {
        EditFacilityChannelResponse response = new EditFacilityChannelResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            Channel channel = channelService.getChannelByName(request.getChannelRequest().getWsChannel().getChannelName());
            EditChannelResponse channelResponse = null;
            if (channel == null) {
                context.addError(WsResponseCode.INVALID_CHANNEL_CODE, "No channel found with given name");
            } else {
                channelResponse = channelService.editChannel(request.getChannelRequest());
                if (!channelResponse.isSuccessful()) {
                    response.addErrors(channelResponse.getErrors());
                }
            }
            if (!response.hasErrors()) {
                doEditFacilityAndChannel(request, response, channelResponse);
            }
        }
        response.addErrors(context.getErrors());
        return response;
    }

    private void doEditFacilityAndChannel(EditFacilityChannelRequest request, EditFacilityChannelResponse response, EditChannelResponse channelResponse) {
        EditFacilityResponse facilityResponse = editFacility(request.getFacilityRequest());
        if (facilityResponse.isSuccessful()) {
            response.setFacilityResponse(facilityResponse);
            for (AddChannelConnectorRequest channelConnectorRequest : request.getChannelConnectorRequests()) {
                AddChannelConnectorResponse connectorResponse = channelService.addChannelConnector(channelConnectorRequest);
                if (!connectorResponse.isSuccessful()) {
                    LOG.info("Unable to add channel connector for channel : " + channelConnectorRequest.getChannelConnector().getChannelCode());
                    break;
                }
            }
            if (!response.hasErrors()) {
                Channel channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelByCode(channelResponse.getChannelDetailDTO().getCode());
                if (channel != null) {
                    Facility facility = CacheManager.getInstance().getCache(FacilityCache.class).getFacilityByCode(facilityResponse.getFacilityDTO().getCode());
                    if (facility != null) {
                        channel.setAssociatedFacility(facility);
                        channelService.updateChannel(channel);
                    }
                }
            }
            if (!response.hasErrors()) {
                FacilityProfile facilityProfile = facilityMao.getFacilityProfileByCode(facilityResponse.getFacilityDTO().getCode());
                if (facilityProfile == null) {
                    CreateFacilityResponse facilityProfileResponse = createFacilityProfile(request.getFacilityProfileRequest());
                    if (!facilityProfileResponse.isSuccessful()) {
                        response.addErrors(facilityProfileResponse.getErrors());
                    } else {
                        response.setSuccessful(true);
                    }
                } else {
                    EditFacilityResponse facilityProfileResponse = editFacilityProfile(request.getFacilityProfileRequest());
                    if (!facilityProfileResponse.isSuccessful()) {
                        response.addErrors(facilityProfileResponse.getErrors());
                    } else {
                        response.setSuccessful(true);
                    }
                }
            }
        } else {
            response.addErrors(facilityResponse.getErrors());
        }
    }

    @Override
    @MarkDirty(values = { FacilityCache.class, ChannelCache.class })
    public CreateFacilityChannelResponse createFacilityAndChannel(CreateFacilityChannelRequest request) {
        CreateFacilityChannelResponse response = new CreateFacilityChannelResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            Channel channel = channelService.getChannelByName(request.getChannelRequest().getWsChannel().getChannelName());
            AddChannelResponse channelResponse = null;
            if (channel == null) {
                channelResponse = channelService.addChannel(request.getChannelRequest());
                if (!channelResponse.isSuccessful()) {
                    response.addErrors(channelResponse.getErrors());
                }
            }
            if (!response.hasErrors()) {
                doCreateFacilityAndChannel(request, response, channelResponse);
            }
        }
        response.addErrors(context.getErrors());
        return response;
    }

    private CreateFacilityChannelResponse doCreateFacilityAndChannel(CreateFacilityChannelRequest request, CreateFacilityChannelResponse response,
            AddChannelResponse channelResponse) {
        CreateFacilityResponse facilityResponse = createFacility(request.getFacilityRequest());
        if (facilityResponse.isSuccessful()) {
            response.setFacilityResponse(facilityResponse);
            for (AddChannelConnectorRequest channelConnectorRequest : request.getChannelConnectorRequests()) {
                AddChannelConnectorResponse connectorResponse = channelService.addChannelConnector(channelConnectorRequest);
                if (!connectorResponse.isSuccessful()) {
                    LOG.info("Unable to add channel connector for channel : " + channelConnectorRequest.getChannelConnector().getChannelCode());
                    break;
                }
            }
            if (!response.hasErrors()) {
                Channel channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelByCode(channelResponse.getChannelDetailDto().getCode());
                if (channel != null) {
                    Facility facility = CacheManager.getInstance().getCache(FacilityCache.class).getFacilityByCode(facilityResponse.getFacilityDTO().getCode());
                    if (facility != null) {
                        channel.setAssociatedFacility(facility);
                        channelService.updateChannel(channel);
                    }
                }
            }
            if (!response.hasErrors()) {
                CreateFacilityResponse facilityProfileResponse = createFacilityProfile(request.getFacilityProfileRequest());
                if (!facilityProfileResponse.isSuccessful()) {
                    response.addErrors(facilityProfileResponse.getErrors());
                } else {
                    response.setSuccessful(true);
                }
            }
        } else {
            response.addErrors(facilityResponse.getErrors());
        }
        return response;
    }

    @Transactional
    @RollbackOnFailure
    @Override
    @MarkDirty(values = { FacilityCache.class })
    public CreateFacilityResponse createFacility(CreateFacilityRequest request) {
        CreateFacilityResponse response = new CreateFacilityResponse();
        //TODO - should be removed when all Party creation/editing are moved to new services
        if (request.getFacility() != null) {
            updateRequestBeforeValidation(request.getFacility());
        }
        ValidationContext context = validateRequest(request);
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        } else {
            Facility facility = facilityDao.getFacilityByCode(request.getFacility().getCode());
            WsFacility reqFacility = request.getFacility();
            if (facility != null) {
                context.addError(WsResponseCode.DUPLICATE_FACILITY_CODE, "Facility for this code already exists");
            } else {
                facility = new Facility();
                genericPartyService.prepareParty(facility, reqFacility, context, true);
            }
            if (context.hasErrors()) {
                response.setErrors(context.getErrors());
            } else {
                facility.setType(reqFacility.getType());
                facility.setDummy(request.getFacility().isDummy());
                facility.setSalesTaxOnShippingCharges(request.getFacility().isSalesTaxOnShippingCharges());
                facility.setDisplayName(reqFacility.getDisplayName());
                facility.setInventorySynDisabled(reqFacility.isInventorySynDisabled());
                Map<String, Object> customFieldValues = CustomFieldUtils.getCustomFieldValues(context, Facility.class.getName(), reqFacility.getCustomFieldValues());
                CustomFieldUtils.setCustomFieldValues(facility, customFieldValues, false);
                //persist facility
                facility = facilityDao.createFacility(facility);
                //addInvoiceSequences(facility);
                context = setupFacilityConfiguration(facility, context, request.getAdditionalSystemConfigs());
                if (context.hasErrors()) {
                    response.setErrors(context.getErrors());
                } else {
                    // Give admin role to the requesting user on this facility.
                    AddUserRolesRequest addRoleRequest = new AddUserRolesRequest();
                    addRoleRequest.setUsername(request.getUsername());
                    addRoleRequest.addFacilityCode(facility.getCode());
                    addRoleRequest.addFacilityRoleCode(Code.ADMIN.name());
                    AddUserRolesResponse addRoleResponse = usersService.addUserRoles(addRoleRequest);
                    if (addRoleResponse.hasErrors()) {
                        response.setErrors(addRoleResponse.getErrors());
                    }
                }
            }
            if (!response.hasErrors()) {
                TenantSetupPropertyVO config = tenantSetupConfigurationMao.getTenantSetupProperty();
                // Give SUPER role to SUPER user in this facility
                AddUserRolesRequest addRoleRequest = new AddUserRolesRequest();
                addRoleRequest.setUsername(config.getSuperUserUsername());
                addRoleRequest.addFacilityCode(facility.getCode());
                addRoleRequest.addFacilityRoleCode(Code.SUPER.name());
                usersService.addUserRoles(addRoleRequest);

                // Give roles to support users on this facility.
                AddUserRolesRequest addRoleToTechSupportRequest = new AddUserRolesRequest();
                addRoleToTechSupportRequest.setUsername(config.getTechSupportUserName());
                addRoleToTechSupportRequest.addFacilityCode(facility.getCode());
                addRoleToTechSupportRequest.addFacilityRoleCode(Code.UNI_TECH_SUPPORT.name());
                usersService.addUserRoles(addRoleToTechSupportRequest);

                AddUserRolesRequest addRoleToCustomerSupportRequest = new AddUserRolesRequest();
                addRoleToCustomerSupportRequest.setUsername(config.getCustomerSupportUserName());
                addRoleToCustomerSupportRequest.addFacilityCode(facility.getCode());
                addRoleToCustomerSupportRequest.addFacilityRoleCode(Code.UNI_CUSTOMER_SUPPORT.name());
                usersService.addUserRoles(addRoleToCustomerSupportRequest);

                // Give roles to system users
                AddUserRolesRequest addRoleToSystemUserRequest = new AddUserRolesRequest();
                addRoleToSystemUserRequest.setUsername(config.getSystemUserEmail());
                addRoleToSystemUserRequest.addFacilityCode(facility.getCode());
                addRoleToSystemUserRequest.addFacilityRoleCode(Code.UNI_TECH_SUPPORT.name());
                usersService.addUserRoles(addRoleToSystemUserRequest);

                Sequence retailInvoiceSequence = sequenceGenerator.getSequenceByName(Sequence.Name.INVOICE.name(), facility.getId());
                Sequence taxInvoiceSequence = sequenceGenerator.getSequenceByName(Sequence.Name.TAX_INVOICE.name(), facility.getId());
                response.setFacilityDTO(prepareFacilityDTO(facility, retailInvoiceSequence, taxInvoiceSequence));
                response.setSuccessful(true);
                if (Type.STORE.name().equals(facility.getType())) {
                    publishNewStoreRegistrationMessage(facility);
                }
            }
        }
        return response;
    }

    private void publishNewStoreRegistrationMessage(Facility facility) {
        SellerRegistrationMessage message = new SellerRegistrationMessage();
        FacilityProfile facilityProfile = getFacilityProfileByCode(facility.getCode());
        if (facilityProfile != null) {
            message.setEndpoint(UserContext.current().getTenant().getAccessUrl());
            message.setStoreCode(facility.getCode());
            message.setSellerCode(facilityProfile.getSellerCode());
            GeolocationUtils.Coordinate coordinate = new GeolocationUtils.Coordinate(facilityProfile.getPosition()[1], facilityProfile.getPosition()[0]);
            message.setCoordinate(coordinate);
            storeAvailabilityNotifyService.publishMessage(message);
        } else {
            LOG.error("No facility profile found for facility : {}", facility.getCode());
        }
    }

    private ValidationContext validateRequest(CreateFacilityRequest request) {
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            boolean validType = false;
            for (Type t : Facility.Type.values()) {
                if (t.name().equals(request.getFacility().getType())) {
                    validType = true;
                    break;
                }
            }
            if (!validType) {
                context.addError(WsResponseCode.INVALID_FACILITY_TYPE);
            }
            if (request.getFacility().getBillingAddress() == null || request.getFacility().getShippingAddress() == null) {
                context.addError(WsResponseCode.BILLING_SHIPPING_ADDRESS_REQUIRED_FOR_FACILITY);
            }
        }
        return context;
    }

    private ValidationContext setupFacilityProfileConfiguration(Facility facility, ValidationContext context, FacilityProfile facilityProfile, List<String> pickUpShippingMethods,
            List<String> deliveryShippingMethods) {
        setupPickupConfiguration(facility, context, facilityProfile, pickUpShippingMethods);
        if (!context.hasErrors() && facilityProfile.isStorePickupEnabled() && facilityProfile.isAutoSetupPickupServiceability()) {
            BigDecimal radius = facilityProfile.getDeliveryRadius() > 0.0 ? NumberUtils.newBigDecimal(facilityProfile.getDeliveryRadius()) : NumberUtils.newBigDecimal(10);
            executorFactory.getExecutor(SERVICE_NAME).submit(new AutoGenerateServiceabilityTask(facility.getCode(), ShippingProvider.Code.SELF_PICKUP.getCode(), radius));
        }
        setupDeliveryConfiguration(facility, context, facilityProfile, deliveryShippingMethods);
        if (!context.hasErrors() && facilityProfile.isStoreDeliveryEnabled() && facilityProfile.isAutoSetupServiceability()) {
            executorFactory.getExecutor(SERVICE_NAME).submit(new AutoGenerateServiceabilityTask(facility.getCode(), ShippingProvider.Code.SELF_STORE.getCode(),
                    NumberUtils.newBigDecimal(facilityProfile.getDeliveryRadius())));
        }
        return context;
    }

    private class AutoGenerateServiceabilityTask implements Runnable {
        private String     faclityCode;
        private String     shippingProviderCode;
        private BigDecimal radius;

        private AutoGenerateServiceabilityTask(String facilityCode, String shippingProviderCode, BigDecimal radius) {
            this.faclityCode = facilityCode;
            this.shippingProviderCode = shippingProviderCode;
            this.radius = radius;
        }

        public void run() {
            Facility facility = CacheManager.getInstance().getCache(FacilityCache.class).getFacilityByCode(faclityCode);
            ShippingProvider shippingProvider = shippingAdminService.getShippingProviderByCode(shippingProviderCode);
            GetPincodesWithinRangeRequest request = new GetPincodesWithinRangeRequest();
            request.setPincode(facility.getPartyAddressByType(PartyAddressType.Code.SHIPPING.name()).getPincode());
            request.setDistance(radius);
            long startTime = System.currentTimeMillis();
            GetPincodesWithinRangeResponse response = locationService.getAllPincodesWithinRange(request);
            LOG.info("Fetched locations for pincode in " + (System.currentTimeMillis() - startTime) / 1000);
            for (ShippingProviderMethod providerMethod : shippingProviderService.getShippingProviderMethods(shippingProvider.getId(), facility.getId())) {
                Set<String> serviceablePincodes = new HashSet<>();
                startTime = System.currentTimeMillis();
                for (Location proximateLocation : response.getLocations()) {
                    ShippingProviderLocation providerLocation = shippingService.getShippingProviderLocation(providerMethod.getShippingMethod().getName(),
                            shippingProvider.getCode(), proximateLocation.getPincode(), facility.getCode());
                    if (providerLocation == null) {
                        CreateShippingProviderLocationRequest locationRequest = new CreateShippingProviderLocationRequest(shippingProvider.getCode(),
                                providerMethod.getShippingMethod().getName(), proximateLocation.getPincode(), 1, facility.getCode());
                        shippingService.createShippingProviderLocation(locationRequest);
                        serviceablePincodes.add(proximateLocation.getPincode());
                    } else {
                        if (!providerLocation.isEnabled()) {
                            providerLocation.setEnabled(true);
                            shippingService.updateShippingProviderLocation(providerLocation);
                        }
                        serviceablePincodes.add(providerLocation.getPincode());
                    }
                }
                LOG.info("Updated shipping provider locations in " + (System.currentTimeMillis() - startTime) / 1000);
                startTime = System.currentTimeMillis();
                Set<String> pincodes = shippingService.getServiceablePincodesByShippingProviderMethod(shippingProvider, providerMethod.getShippingMethod(), facility.getCode());
                LOG.info("Fetched serviceable pincodes in " + (System.currentTimeMillis() - startTime) / 1000);
                pincodes.removeAll(serviceablePincodes);
                if (pincodes.size() > 0) {
                    startTime = System.currentTimeMillis();
                    shippingService.updateNonServiceablePincodes(shippingProvider, providerMethod.getShippingMethod(), pincodes, facility.getId());
                    LOG.info("Updated non serviceable pincodes in " + (System.currentTimeMillis() - startTime) / 1000);
                }
            }
        }
    }

    @Transactional
    private void setupDeliveryConfiguration(final Facility facility, ValidationContext context, final FacilityProfile facilityProfile, List<String> deliveryShippingMethods) {
        if (facilityProfile.isStoreDeliveryEnabled()) {
            List<WSShippingMethod> shippingMethods = new ArrayList<>();
            for (String deliveryShippingMethodName : deliveryShippingMethods) {
                if (StringUtils.isNotBlank(ShippingMethod.Name.getByName(deliveryShippingMethodName))) {
                    shippingMethods.add(new WSShippingMethod(ShippingMethod.Name.getByName(deliveryShippingMethodName), true,
                            ShippingProviderMethod.TrackingNumberGeneration.MANUAL.name(), facility.getCode()));
                } else {
                    context.addError(WsResponseCode.INVALID_SHIPPING_METHOD, "Invalid shipping method for pick up courier " + deliveryShippingMethodName);
                }
            }
            if (!context.hasErrors()) {
                WsShippingProviderDetail providerDetail = new WsShippingProviderDetail("CUSTOM", ShippingProvider.Code.SELF_STORE.getName(),
                        ShippingProvider.Code.SELF_STORE.getCode(), true, false, ShippingProvider.SyncStatus.NOT_AVAILABLE.name(),
                        ShippingProvider.ShippingServiceablity.LIMITED_SERVICEABILITY.name(), shippingMethods);
                long startTime = System.currentTimeMillis();
                ShippingProvider shippingProvider = shippingAdminService.getShippingProviderByCode(ShippingProvider.Code.SELF_STORE.getCode());
                LOG.info("Delivery. Fetch shipping provider in " + (System.currentTimeMillis() - startTime) / 1000);
                if (shippingProvider != null) {
                    EditShippingProviderRequest request = new EditShippingProviderRequest();
                    startTime = System.currentTimeMillis();
                    List<ShippingProviderMethod> shippingMethodsAdded = shippingProviderService.getShippingProviderMethods(shippingProvider.getId(), facility.getId());
                    LOG.info("Delivery. Shipping methods fetch " + (System.currentTimeMillis() - startTime) / 1000);
                    for (ShippingProviderMethod spm : shippingMethodsAdded) {
                        if (!deliveryShippingMethods.contains(spm.getShippingMethod().getName())) {
                            providerDetail.getShippingMethods().add(new WSShippingMethod(spm.getShippingMethod().getName(), false,
                                    ShippingProviderMethod.TrackingNumberGeneration.MANUAL.name(), facility.getCode()));
                        }
                    }
                    request.setWsShippingProvider(providerDetail);
                    startTime = System.currentTimeMillis();
                    EditShippingProviderResponse response = shippingAdminService.updateShippingProvider(request);
                    LOG.info("Delivery. Update shipping provider in " + (System.currentTimeMillis() - startTime) / 1000);
                    if (response.hasErrors()) {
                        context.addErrors(response.getErrors());
                    }
                } else {
                    AddShippingProviderRequest request = new AddShippingProviderRequest();
                    request.setWsShippingProvider(providerDetail);
                    startTime = System.currentTimeMillis();
                    AddShippingProviderResponse response = shippingAdminService.addShippingProvider(request);
                    LOG.info("Delivery. Add shipping provider in " + (System.currentTimeMillis() - startTime) / 1000);
                    if (response.hasErrors()) {
                        context.addErrors(response.getErrors());
                    }
                }
            }
        } else {
            long startTime = System.currentTimeMillis();
            ShippingProvider shippingProvider = shippingAdminService.getShippingProviderByCode(ShippingProvider.Code.SELF_STORE.getCode());
            LOG.info("Delivery off. Fetch shipping provider in " + (System.currentTimeMillis() - startTime) / 1000);
            if (shippingProvider != null) {
                EditShippingProviderRequest request = new EditShippingProviderRequest();
                List<WSShippingMethod> shippingMethods = new ArrayList<>();
                startTime = System.currentTimeMillis();
                List<ShippingProviderMethod> shippingMethodsAdded = shippingProviderService.getShippingProviderMethods(shippingProvider.getId(), facility.getId());
                LOG.info("Delivery off. Fetch shipping methods in " + (System.currentTimeMillis() - startTime) / 1000);
                for (ShippingProviderMethod spm : shippingMethodsAdded) {
                    shippingMethods.add(
                            new WSShippingMethod(spm.getShippingMethod().getName(), false, ShippingProviderMethod.TrackingNumberGeneration.MANUAL.name(), facility.getCode()));
                }
                if (shippingMethods.size() > 0) {
                    WsShippingProviderDetail providerDetail = new WsShippingProviderDetail("CUSTOM", ShippingProvider.Code.SELF_STORE.getName(),
                            ShippingProvider.Code.SELF_STORE.getCode(), true, false, ShippingProvider.SyncStatus.NOT_AVAILABLE.name(),
                            ShippingProvider.ShippingServiceablity.LIMITED_SERVICEABILITY.name(), shippingMethods);
                    request.setWsShippingProvider(providerDetail);
                    startTime = System.currentTimeMillis();
                    EditShippingProviderResponse response = shippingAdminService.updateShippingProvider(request);
                    LOG.info("Delivery off. Update shipping provider in " + (System.currentTimeMillis() - startTime) / 1000);
                    if (response.hasErrors()) {
                        context.addErrors(response.getErrors());
                    }
                }
            }
        }
    }

    @Transactional
    private void setupPickupConfiguration(Facility facility, ValidationContext context, FacilityProfile facilityProfile, List<String> pickUpShippingMethods) {
        if (facilityProfile.isStorePickupEnabled()) {
            List<WSShippingMethod> shippingMethods = new ArrayList<>();
            for (String pickupShippingMethodName : pickUpShippingMethods) {
                if (StringUtils.isNotBlank(ShippingMethod.Name.getByName(pickupShippingMethodName))) {
                    shippingMethods.add(new WSShippingMethod(ShippingMethod.Name.getByName(pickupShippingMethodName), true,
                            ShippingProviderMethod.TrackingNumberGeneration.MANUAL.name(), facility.getCode()));
                } else {
                    context.addError(WsResponseCode.INVALID_SHIPPING_METHOD, "Invalid shipping method for pick up courier " + pickupShippingMethodName);
                }
            }
            if (!context.hasErrors()) {
                WsShippingProviderDetail providerDetail = new WsShippingProviderDetail(ShippingProvider.Code.SELF_PICKUP.getCode(), ShippingProvider.Code.SELF_PICKUP.getName(),
                        ShippingProvider.Code.SELF_PICKUP.getCode(), true, true, ShippingProvider.SyncStatus.NOT_AVAILABLE.name(),
                        ShippingProvider.ShippingServiceablity.LIMITED_SERVICEABILITY.name(), shippingMethods);
                long startTime = System.currentTimeMillis();
                ShippingProvider shippingProvider = shippingAdminService.getShippingProviderByCode(ShippingProvider.Code.SELF_PICKUP.getCode());
                LOG.info("Fetch shipping provider " + (System.currentTimeMillis() - startTime) / 1000);
                if (shippingProvider != null) {
                    EditShippingProviderRequest request = new EditShippingProviderRequest();
                    List<ShippingProviderMethod> shippingMethodsAdded = shippingProviderService.getShippingProviderMethods(shippingProvider.getId(), facility.getId());
                    for (ShippingProviderMethod spm : shippingMethodsAdded) {
                        if (!pickUpShippingMethods.contains(spm.getShippingMethod().getName())) {
                            providerDetail.getShippingMethods().add(new WSShippingMethod(spm.getShippingMethod().getName(), false,
                                    ShippingProviderMethod.TrackingNumberGeneration.MANUAL.name(), facility.getCode()));
                        }
                    }
                    request.setWsShippingProvider(providerDetail);
                    EditShippingProviderResponse response = shippingAdminService.updateShippingProvider(request);
                    if (response.hasErrors()) {
                        context.addErrors(response.getErrors());
                    }
                } else {
                    AddShippingProviderRequest request = new AddShippingProviderRequest();
                    request.setWsShippingProvider(providerDetail);
                    AddShippingProviderResponse response = shippingAdminService.addShippingProvider(request);
                    if (response.hasErrors()) {
                        context.addErrors(response.getErrors());
                    }
                }
            }
        } else {
            ShippingProvider shippingProvider = shippingAdminService.getShippingProviderByCode(ShippingProvider.Code.SELF_PICKUP.getCode());
            if (shippingProvider != null) {
                EditShippingProviderRequest request = new EditShippingProviderRequest();
                List<WSShippingMethod> shippingMethods = new ArrayList<>();
                long startTime = System.currentTimeMillis();
                List<ShippingProviderMethod> shippingMethodsAdded = shippingProviderService.getShippingProviderMethods(shippingProvider.getId(), facility.getId());
                LOG.info("Pick up disabled. Fecth shipping methods in " + (System.currentTimeMillis() - startTime) / 1000);
                for (ShippingProviderMethod spm : shippingMethodsAdded) {
                    shippingMethods.add(
                            new WSShippingMethod(spm.getShippingMethod().getName(), false, ShippingProviderMethod.TrackingNumberGeneration.MANUAL.name(), facility.getCode()));
                }
                if (shippingMethods.size() > 0) {
                    WsShippingProviderDetail providerDetail = new WsShippingProviderDetail(ShippingProvider.Code.SELF_PICKUP.getCode(), ShippingProvider.Code.SELF_PICKUP.getName(),
                            ShippingProvider.Code.SELF_PICKUP.getCode(), true, true, ShippingProvider.SyncStatus.NOT_AVAILABLE.name(),
                            ShippingProvider.ShippingServiceablity.LIMITED_SERVICEABILITY.name(), shippingMethods);
                    request.setWsShippingProvider(providerDetail);
                    startTime = System.currentTimeMillis();
                    EditShippingProviderResponse response = shippingAdminService.updateShippingProvider(request);
                    LOG.info("Pick up disabled. Update shipping provider in " + (System.currentTimeMillis() - startTime) / 1000);
                    if (response.hasErrors()) {
                        context.addErrors(response.getErrors());
                    }
                }
            }
        }
    }

    private ValidationContext setupFacilityConfiguration(Facility facility, ValidationContext context, List<SystemConfigurationDTO> additionalSystemConfigs) {
        UserContext.current().setTenant(facility.getTenant());
        UserContext.current().setFacility(facility);
        boolean setupFacilityResource = setupFacilityResources(context, facility);
        if (!context.hasErrors() && !setupFacilityResource) {
            context.addError(WsResponseCode.INVALID_FACILITY_CODE, "Unable to create default facility for tenant");
        } else if (additionalSystemConfigs != null && !additionalSystemConfigs.isEmpty()) {
            Facility currentFacility = UserContext.current().getFacility();
            Map<String, SystemConfigurationDTO> nameToSystemConfig = new HashMap<>(additionalSystemConfigs.size());
            for (SystemConfigurationDTO systemConfigurationDTO : additionalSystemConfigs) {
                nameToSystemConfig.put(systemConfigurationDTO.getName(), systemConfigurationDTO);
            }
            try {
                UserContext.current().setFacility(facility);
                for (SystemConfig systemConfig : applicationSetupService.getSystemConfigs()) {
                    if (nameToSystemConfig.get(systemConfig.getName()) != null) {
                        systemConfig.setValue(nameToSystemConfig.get(systemConfig.getName()).getValue());
                        nameToSystemConfig.remove(systemConfig.getName());
                        applicationSetupService.updateSystemConfig(systemConfig);
                    }
                }
                if (!nameToSystemConfig.isEmpty()) {
                    Iterator<Map.Entry<String, SystemConfigurationDTO>> iterator = nameToSystemConfig.entrySet().iterator();
                    while (iterator.hasNext()) {
                        Map.Entry<String, SystemConfigurationDTO> additionalSystemConfig = iterator.next();
                        SystemConfigurationDTO systemConfigurationDTO = additionalSystemConfig.getValue();
                        SystemConfig systemConfig = new SystemConfig();
                        systemConfig.setTenant(UserContext.current().getTenant());
                        systemConfig.setName(systemConfigurationDTO.getName());
                        systemConfig.setValue(systemConfigurationDTO.getValue());
                        systemConfig.setDisplayName(systemConfigurationDTO.getDisplayName());
                        systemConfig.setAccessResourceName(systemConfigurationDTO.getAccessResourceName());
                        systemConfig.setType(systemConfigurationDTO.getType());
                        systemConfig.setSequence(0);
                        systemConfig.setGroupName(systemConfigurationDTO.getGroupName());
                        systemConfig.setCreated(DateUtils.getCurrentTime());
                        systemConfig.setUpdated(DateUtils.getCurrentDayTime());
                        applicationSetupService.addSystemConfig(systemConfig);
                    }
                }

            } catch (Exception e) {
                LOG.error("error while adding facility : ", e);
                context.addError(WsResponseCode.INVALID_REQUEST, "Unable to Configure facility for channel");
            } finally {
                UserContext.current().setFacility(currentFacility);
            }
        }
        if (!context.hasErrors()) {
            for (Name name : Name.values()) {
                if (Level.FACILITY.equals(name.level())) {
                    String prefix = name.prefix();
                    if (name == Name.SHIPPING_PACKAGE) {
                        prefix = StringUtils.removeNonWordChars(facility.getCode()).toUpperCase();
                        if (prefix.length() > 4) {
                            prefix = prefix.substring(0, 4);
                        }
                    } else if (name == Name.INVOICE) {
                        prefix = "R" + facility.getCode().toUpperCase();
                        if (prefix.length() > 10) {
                            prefix = prefix.substring(0, 10);
                        }
                    } else if (name == Name.TAX_INVOICE) {
                        prefix = "T" + facility.getCode().toUpperCase();
                        if (prefix.length() > 10) {
                            prefix = prefix.substring(0, 10);
                        }
                    } else if (name == Name.OUTBOUND_GATE_PASS) {
                        prefix = "GP" + facility.getCode().toUpperCase();
                        if (prefix.length() > 10) {
                            prefix = prefix.substring(0, 10);
                        }
                    }
                    Sequence sequence = new Sequence(facility.getTenant(), facility, name.name(), Level.FACILITY.name(), 0, 1, prefix, name.prefixExpression(), name.padToLength(),
                            name.resetInterval(), DateUtils.getCurrentTime(), DateUtils.getCurrentTime());
                    sequenceGenerator.createSequence(sequence);
                }
            }

            // Create shipping provider methods (disabled) corresponding to all providers and shipping methods.
            /* ShippingConfiguration shippingConfiguration = ConfigurationManager.getInstance().getConfiguration(ShippingConfiguration.class);
             List<MethodDTO> shippingProviderMethods = new ArrayList<>();
             for (ShippingProvider provider : shippingConfiguration.getAllShippingProviders()) {
                 for (ShippingMethod shippingMethod : shippingConfiguration.getShippingMethods()) {
                     MethodDTO methodDTO = new MethodDTO();
                     methodDTO.setProviderCode(provider.getCode());
                     methodDTO.setMethodCode(shippingMethod.getCode());
                     methodDTO.setCod(PaymentMethod.Code.COD.name().equals(shippingMethod.getPaymentMethod().getCode()));
                     methodDTO.setEnabled(false);
                     methodDTO.setTrackingNumberGeneration(TrackingNumberGeneration.MANUAL.name());
                     shippingProviderMethods.add(methodDTO);
                 }
             }
            
             AddShippingProviderMethodsResponse addShippingProviderMethodsResponse = shippingAdminService.addShippingProviderMethods(new AddShippingProviderMethodsRequest(
                     shippingProviderMethods));
             if (!addShippingProviderMethodsResponse.isSuccessful()) {
                 context.addErrors(addShippingProviderMethodsResponse.getErrors());
             }*/
        }
        if (!context.hasErrors()) {
            // Create a default shipping package type with 0x0x0 dimension.
            CreateShippingPackageTypeResponse createPackageTypeResponse = shippingAdminService.createShippingPackageType(
                    new CreateShippingPackageTypeRequest("DEFAULT", 0, 0, 0, 0, new BigDecimal(0)));
            if (createPackageTypeResponse.hasErrors()) {
                context.addErrors(createPackageTypeResponse.getErrors());
            }
        }

        if (Facility.Type.WAREHOUSE.name().equals(facility.getType()) || Facility.Type.STORE.name().equals(facility.getType())) {
            String sectionCode = null;
            if (!context.hasErrors() && setupFacilityResource) {
                // Create a default default pickset, default section and map all the categories to this section.
                CreatePickAreaRequest createPickAreaRequest = new CreatePickAreaRequest();
                createPickAreaRequest.setName(PickArea.DEFAULT_PICK_AREA);
                CreatePickAreaResponse createPickAreaResponse = picksetService.createPickArea(createPickAreaRequest);
                if(createPickAreaResponse.isSuccessful()){
                    CreatePicksetResponse createPicksetResponse = picksetService.createPickset(new CreatePicksetRequest(PickSetType.DEFAULT.name(), 1000, true));
                    if (createPicksetResponse.isSuccessful()) {
                        CreateSectionResponse createSectionResponse = sectionService.createSection(
                                new CreateSectionRequest(SectionCode.DEF.name(), "DEFAULT", createPicksetResponse.getPicksetDTO().getId(), true));
                        if (createSectionResponse.isSuccessful()) {
                            sectionCode = createSectionResponse.getSectionDTO().getCode();
                        } else {
                            context.addErrors(createSectionResponse.getErrors());
                        }
                    } else {
                        context.addErrors(createPicksetResponse.getErrors());
                    }
                }else{
                    context.addErrors(createPickAreaResponse.getErrors());
                }
            }

            if (!context.hasErrors() && sectionCode != null) {
                // Create a default shelf type and shelf
                CreateShelfTypeResponse createShelfTypeResponse = shelfService.createShelfType(new CreateShelfTypeRequest(ShelfTypeCode.DEFAULT.name(), 0, 0, 0));
                if (createShelfTypeResponse.isSuccessful()) {
                    CreateShelvesWithPredefinedCodesResponse createShelfResponse = shelfService.createShelfWithPredefinedCodes(
                            new CreateShelvesWithPredefinedCodesRequest(ShelfTypeCode.DEFAULT.name(), Section.SectionCode.DEF.name(), Shelf.ShelfCode.DEFAULT.name()));
                    if (createShelfResponse.hasErrors()) {
                        context.addErrors(createShelfResponse.getErrors());
                    }
                } else {
                    context.addErrors(createShelfTypeResponse.getErrors());
                }
            }
        }
        if (!context.hasErrors()) {
            CacheManager.getInstance().markCacheDirty(FacilityCache.class);
        }
        return context;
    }

    private boolean setupFacilityResources(ValidationContext context, Facility facility) {
        Facility baseFacility = null;
        List<Facility> facilities = facility.getTenant().getBaseTenant() != null ? facilityDao.getFacilitiesByTenantId(facility.getTenant().getBaseTenant().getId())
                : facilityDao.getFacilitiesByTenantId(facility.getTenant().getId());
        if (!facilities.isEmpty()) {
            for (Facility base : facilities) {
                if (base.getType().equals(facility.getType()) && !base.getId().equals(facility.getId())) {
                    baseFacility = base;
                    break;
                }
            }
            if (baseFacility == null) {
                baseFacility = facilities.get(0);
            }
        }
        if (baseFacility == null || facility.getId().equals(baseFacility.getId())) {
            context.addError(WsResponseCode.INVALID_TENANT, "No base facility found to copy resources from");
        }
        if (!context.hasErrors()) {
            return facilityDao.setupFacilityResources(baseFacility, facility);
        }
        return false;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Facility> getFacilities() {
        return facilityDao.getFacilities();
    }

    @Override
    @Transactional(readOnly = true)
    public List<FacilityAllocationRule> getFacilityAllocationRules() {
        return facilityDao.getFacilityAllocationRules();
    }

    @Override
    @Transactional(readOnly = true)
    public List<Integer> getFacilityIdsWithPendingSaleOrderItems() {
        return facilityDao.getFacilityIdsWithPendingSaleOrderItems();
    }

    @Override
    @Transactional
    public List<FacilityDTO> lookUpFacilities(String keyword) {
        List<FacilityDTO> facilities = new ArrayList<FacilityDTO>();
        for (Facility facility : facilityDao.lookUpFacilities(keyword)) {
            Sequence retailInvoiceSequence = sequenceGenerator.getSequenceByName(Sequence.Name.INVOICE.name(), facility.getId());
            Sequence taxInvoiceSequence = sequenceGenerator.getSequenceByName(Sequence.Name.TAX_INVOICE.name(), facility.getId());
            FacilityDTO facilityDTO = new FacilityDTO(facility, retailInvoiceSequence, taxInvoiceSequence,
                    Facility.Type.STORE.equals(facility.getType()) ? facilityMao.getFacilityProfileByCode(facility.getCode()) : null);
            facilities.add(facilityDTO);
        }
        return facilities;
    }

    @Override
    @Transactional(readOnly = true)
    public List<FacilityDTO> getUserFacilities(Set<Integer> facilityIds) {
        List<FacilityDTO> facilities = new ArrayList<FacilityDTO>();
        for (Integer id : facilityIds) {
            Facility facility = facilityDao.getFacilityById(id);
            if (facility != null) {
                Sequence retailInvoiceSequence = sequenceGenerator.getSequenceByName(Sequence.Name.INVOICE.name(), facility.getId());
                Sequence taxInvoiceSequence = sequenceGenerator.getSequenceByName(Sequence.Name.TAX_INVOICE.name(), facility.getId());
                FacilityDTO facilityDTO = new FacilityDTO(facility, retailInvoiceSequence, taxInvoiceSequence,
                        Facility.Type.STORE.equals(facility.getType()) ? facilityMao.getFacilityProfileByCode(facility.getCode()) : null);
                facilities.add(facilityDTO);
            }
        }
        return facilities;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Facility> getFacilitiesById(Set<Integer> facilityIds) {
        List<Facility> facilities = new ArrayList<>();
        for (Integer id : facilityIds) {
            facilities.add(facilityDao.getFacilityById(id));
        }
        return facilities;
    }

    /**
     * adds partyCode, addressType to addresses before validating request, temporary, should be removed when all Party
     * creation/editing are moved to new services
     *
     * @param facility
     */
    private void updateRequestBeforeValidation(WsFacility facility) {
        if (facility.getCode() != null) {
            WsPartyAddress bAddress = facility.getBillingAddress();
            WsPartyAddress sAddress = facility.getShippingAddress();
            if (bAddress != null) {
                bAddress.setPartyCode(facility.getCode());
                bAddress.setAddressType(PartyAddressType.Code.BILLING.name());
            }
            if (sAddress != null) {
                sAddress.setPartyCode(facility.getCode());
                sAddress.setAddressType(PartyAddressType.Code.SHIPPING.name());
            }
            if (facility.getPartyContacts() != null) {
                for (WsPartyContact contact : facility.getPartyContacts()) {
                    contact.setPartyCode(facility.getCode());
                }
            }
        }
    }

    private BillingParty findBillingPartyWithSameSequencePrefix(String newPrefix) {
        for (BillingParty bp : billingPartyService.getAllBillingParties()) {
            Sequence bpRetailSequence = sequenceGenerator.getSequenceByName(bp.getRetailInvoiceSequence(), null);
            Sequence bpTaxSequence = sequenceGenerator.getSequenceByName(bp.getTaxInvoiceSequence(), null);
            if (newPrefix.equalsIgnoreCase(bpRetailSequence.getPrefix()) || newPrefix.equalsIgnoreCase(bpTaxSequence.getPrefix())) {
                return bp;
            }
        }
        return null;
    }

    @Transactional
    private FacilityDTO prepareFacilityDTO(Facility facility, Sequence retailInvoiceSequence, Sequence taxInvoiceSequence) {
        facility = facilityDao.getAllFacilityByCode(facility.getCode());
        LOG.info("Preparing DTO for facility " + facility.getCode() + " " + facility);
        FacilityDTO dto = new FacilityDTO(facility, retailInvoiceSequence, taxInvoiceSequence,
                Facility.Type.STORE.name().equals(facility.getType()) ? facilityMao.getFacilityProfileByCode(facility.getCode()) : null);
        /*        if (dto.getFacilityProfile() != null) {
                    List<String> pickupMethods = new ArrayList<>();
                    List<String> selfMethods = new ArrayList<>();
                    for (ShippingProviderMethod shippingProviderMethod : shippingProviderService.getShippingProviderMethods(
                            shippingAdminService.getShippingProviderByCode(ShippingProvider.Code.SELF_PICKUP.getCode()).getId(), facility.getId())) {
                        if (shippingProviderMethod.isEnabled()) {
                            pickupMethods.add(shippingProviderMethod.getShippingMethod().getName());
                        }
                    }
                    for (ShippingProviderMethod shippingProviderMethod : shippingProviderService.getShippingProviderMethods(
                            shippingAdminService.getShippingProviderByCode(ShippingProvider.Code.SELF_STORE.getCode()).getId(), facility.getId())) {
                        if (shippingProviderMethod.isEnabled()) {
                            selfMethods.add(shippingProviderMethod.getShippingMethod().getName());
                        }
                    }
                    dto.getFacilityProfile().setPickupShippingMethods(pickupMethods);
                    dto.getFacilityProfile().setSelfShippingMethods(selfMethods);
                }
        */
        dto.setCustomFieldValues(CustomFieldUtils.getCustomFieldValuesDTO(facility));
        dto.setDummy(false);
        return dto;
    }

    @Override
    public FacilityProfile getFacilityProfileByCode(String facilityCode) {
        return facilityMao.getFacilityProfileByCode(facilityCode);
    }

    @Override
    public void save(FacilityProfile facilityProfile) {
        facilityMao.save(facilityProfile);
    }

    @Override
    public GeoResults<FacilityProfile> getPickupFacilitiesWithinDistance(double[] position, double distance) {
        return facilityMao.getPickupFacilitiesWithinDistance(position, distance);
    }

    @Override
    public GetSellerDetailsFromTinResponse getSellerDetailsUsingTin(GetSellerDetailsFromTinRequest request) {
        GetSellerDetailsFromTinResponse response = new GetSellerDetailsFromTinResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            ScraperScript tinInfoScript = CacheManager.getInstance().getCache(ScriptVersionedCache.class).getScriptByName(
                    UniwareScriptVO.Name.FETCH_SELLER_DETAILS_USING_TIN.name(), true);
            ScriptExecutionContext seContext = ScriptExecutionContext.current();
            seContext.addVariable("tin", request.getTin());
            try {
                tinInfoScript.execute();
                String sellerTinInformationResponseXml = seContext.getScriptOutput();
                if (StringUtils.isNotBlank(sellerTinInformationResponseXml)) {
                    return JibxUtils.unmarshall("unicommerceServicesBinding18", GetSellerDetailsFromTinResponse.class, sellerTinInformationResponseXml);
                }
            } catch (Exception e) {
                response.setMessage(e.getMessage());
                LOG.error("Error while executing facility detailing script " + e);
            } finally {
                ScriptExecutionContext.destroy();
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    @Override
    @Transactional
    public Facility getAllFacilityByCode(String facilityCode) {
        return facilityDao.getAllFacilityByCode(facilityCode);
    }

    @Override
    @Transactional
    public EditPartySequenceResponse editFacilitySequence(EditPartySequenceRequest request){
        EditPartySequenceResponse response = new EditPartySequenceResponse();
        ValidationContext context = request.validate();
        if(!context.hasErrors()){
            Facility facility = null;
            Sequence sequence = null;
            if(request.isBillingParty()){
                BillingParty billingParty = billingPartyService.getBillingPartyByCode(request.getCode());
                sequence = sequenceGenerator.getSequenceByName(request.getSequence().getName(), null);
                if(billingParty == null){
                    context.addError(WsResponseCode.INVALID_FACILITY_CODE, "Invalid billing party Code");
                } else if(sequence == null){
                    context.addError(WsResponseCode.INVALID_REQUEST, "Invalid billing party invoice sequence name");
                } else if(!billingPartyService.checkIfValidBillingPartySequence(billingParty, request.getSequence().getName())) {
                    context.addError(WsResponseCode.INVALID_REQUEST, "Sequence name does not belong to billing party : " + billingParty.getCode());
                }
            } else {
                facility = getAllFacilityByCode(request.getCode());
                if(facility == null){
                    context.addError(WsResponseCode.INVALID_FACILITY_CODE, "Invalid facility Code");
                } else {
                    try {
                        Sequence.Name sequenceName = Sequence.Name.valueOf(request.getSequence().getName());
                        if(!sequenceName.isValidInvoiceSequence()){
                            context.addError(WsResponseCode.INVALID_REQUEST, "Invalid invoice sequence name");
                        } else {
                            sequence = sequenceGenerator.getSequenceByName(sequenceName.name(), facility.getId());

                        }
                    } catch (Exception e){
                        context.addError(WsResponseCode.INVALID_REQUEST, "Invalid sequence name");
                    }
                }
            }
            if(!context.hasErrors() && sequence != null){
                String newPrefix = request.getSequence().getPrefix();
                String newNextYearPrefix = request.getSequence().getNextYearPrefix();
                if(StringUtils.isNotBlank(newNextYearPrefix) && newNextYearPrefix.equalsIgnoreCase(sequence.getPrefix())) {
                    context.addError(WsResponseCode.DUPLICATE_SEQUENCE_PREFIX, "sequence next year prefix can not be same");
                } else if(StringUtils.isNotBlank(newPrefix) && !newPrefix.equals(sequence.getPrefix()) && !CollectionUtils.isEmpty(invoiceService.searchInvoice(newPrefix))){
                    context.addError(WsResponseCode.DUPLICATE_SEQUENCE_PREFIX, "Sequence prefix Already used");
                }
                updateSequences(sequence.getName(), sequence, request.getSequence(), facility, context);
            }
        }
        if(context.hasErrors()){
            response.addErrors(context.getErrors());
        } else {
            response.setSuccessful(true);
        }
        return response;
    }
}
