package com.uniware.services.warehouse;

import com.unifier.core.api.validation.ValidationContext;
import com.unifier.services.aspect.MarkDirty;
import com.uniware.core.api.facility.CreateFacilityAllocationRuleRequest;
import com.uniware.core.api.facility.CreateFacilityAllocationRuleResponse;
import com.uniware.core.api.facility.CreateFacilityChannelRequest;
import com.uniware.core.api.facility.CreateFacilityChannelResponse;
import com.uniware.core.api.facility.CreateFacilityProfileRequest;
import com.uniware.core.api.facility.CreateFacilityRequest;
import com.uniware.core.api.facility.CreateFacilityResponse;
import com.uniware.core.api.facility.DeleteFacilityAllocationRuleRequest;
import com.uniware.core.api.facility.DeleteFacilityAllocationRuleResponse;
import com.uniware.core.api.facility.EditFacilityAllocationRuleRequest;
import com.uniware.core.api.facility.EditFacilityAllocationRuleResponse;
import com.uniware.core.api.facility.EditFacilityChannelRequest;
import com.uniware.core.api.facility.EditFacilityChannelResponse;
import com.uniware.core.api.facility.GetAllFacilityDetailsResponse;
import com.uniware.core.api.facility.GetSellerDetailsFromTinRequest;
import com.uniware.core.api.facility.GetSellerDetailsFromTinResponse;
import com.uniware.core.api.facility.ReorderFacilityAllocationRulesRequest;
import com.uniware.core.api.facility.ReorderFacilityAllocationRulesResponse;
import com.uniware.core.api.party.PartyAddressDTO;
import com.uniware.core.api.sequence.WsSequence;
import com.uniware.core.api.warehouse.EditFacilityRequest;
import com.uniware.core.api.warehouse.EditFacilityResponse;
import com.uniware.core.api.warehouse.EditPartySequenceRequest;
import com.uniware.core.api.warehouse.EditPartySequenceResponse;
import com.uniware.core.api.warehouse.FacilityDTO;
import com.uniware.core.api.warehouse.GetFacilityRequest;
import com.uniware.core.api.warehouse.GetFacilityResponse;
import com.uniware.core.cache.FacilityCache;
import com.uniware.core.entity.AnnualHoliday;
import com.uniware.core.entity.Facility;
import com.uniware.core.entity.FacilityAllocationRule;
import com.uniware.core.entity.FacilityProfile;
import com.uniware.core.entity.Sequence;
import com.uniware.services.cache.ChannelCache;
import java.util.List;
import java.util.Set;
import org.springframework.data.mongodb.core.geo.GeoResults;

public interface IFacilityService {

    EditFacilityResponse editFacility(EditFacilityRequest req);

    ValidationContext updateSequences(String invoiceSequenceName, Sequence invoiceSequence, WsSequence sequence, Facility facility, ValidationContext context);

    List<Facility> getAllEnabledFacilities();

    GetFacilityResponse getFacilityByCode(GetFacilityRequest request);

    CreateFacilityAllocationRuleResponse createAllocationRule(CreateFacilityAllocationRuleRequest request);

    DeleteFacilityAllocationRuleResponse deleteAllocationRule(DeleteFacilityAllocationRuleRequest request);

    EditFacilityAllocationRuleResponse editAllocationRule(EditFacilityAllocationRuleRequest request);

    ReorderFacilityAllocationRulesResponse reorderAllocationRules(ReorderFacilityAllocationRulesRequest request);

    CreateFacilityResponse createFacility(CreateFacilityRequest request);

    List<Facility> getFacilities();

    List<FacilityAllocationRule> getFacilityAllocationRules();

    List<Integer> getFacilityIdsWithPendingSaleOrderItems();

    List<FacilityDTO> lookUpFacilities(String keyword);

    GetAllFacilityDetailsResponse getAllFacilityDetails();

    PartyAddressDTO getAddresssByFacilityCode(String code, String partyAddressType);

    List<FacilityDTO> getUserFacilities(Set<Integer> facilityIds);

    List<Facility> getAllEnabledFacilitiesWithGstNumber();

    List<Facility> getAllFacilities();

    List<Facility> getFacilitiesById(Set<Integer> facilityIds);

    GeoResults<FacilityProfile> getPickupFacilitiesWithinDistance(double[] position, double distance);

    GetSellerDetailsFromTinResponse getSellerDetailsUsingTin(GetSellerDetailsFromTinRequest request);

    FacilityProfile getFacilityProfileByCode(String facilityCode);

    List<AnnualHoliday> getAllAnnualHolidays();

    CreateFacilityResponse createFacilityProfile(CreateFacilityProfileRequest request);

    void save(FacilityProfile facilityProfile);

    EditFacilityResponse editFacilityProfile(CreateFacilityProfileRequest request);

    List<Integer> getFacilityIdsWithPendingSaleOrders(Integer pendingSinceInHours, List<String> pendingOrderStatuses);

    @MarkDirty(values = { FacilityCache.class, ChannelCache.class }) EditFacilityChannelResponse editFacilityAndChannel(EditFacilityChannelRequest request);

    CreateFacilityChannelResponse createFacilityAndChannel(CreateFacilityChannelRequest request);

    Facility getAllFacilityByCode(String facilityCode);

    EditPartySequenceResponse editFacilitySequence(EditPartySequenceRequest request);
}