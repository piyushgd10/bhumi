/*
 *  Copyright 2013 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 14-Jun-2013
 *  @author Sunny
 */
package com.uniware.services.channel.saleorder.status;

import com.uniware.core.api.channel.ForceSyncSaleOrderStatusRequest;
import com.uniware.core.api.channel.ForceSyncSaleOrderStatusResponse;
import com.uniware.core.api.channel.GetChannelSaleOrderStatusRequest;
import com.uniware.core.api.channel.GetChannelSaleOrderStatusResponse;
import com.uniware.core.api.channel.SyncOrderStatusRequest;
import com.uniware.core.api.channel.SyncOrderStatusResponse;
import com.uniware.services.channel.saleorder.status.event.ChannelOrderStatusChangeEvent;

public interface IChannelOrderStatusSyncService {

    SyncOrderStatusResponse syncSaleOrderStatus(SyncOrderStatusRequest request);

    void registerEventHandler(ChannelOrderStatusChangeEvent.Type type, IChannelOrderStatusChangeEventHandler handler);

    GetChannelSaleOrderStatusResponse getChannelSaleOrderStatus(GetChannelSaleOrderStatusRequest request);

    ForceSyncSaleOrderStatusResponse forceSyncSaleOrderStatus(ForceSyncSaleOrderStatusRequest request);

}
