/*
*  Copyright 2015 Unicommerce eSolutions (P) Limited . All Rights Reserved.
*  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
*  
*  @version     1.0, 18/11/15
*  @author sunny
*/

package com.uniware.services.channel.saleorder.status;

import com.uniware.services.channel.saleorder.status.event.ChannelOrderStatusChangeEvent;

public interface IChannelOrderStatusChangeEventHandler {

    void handle(ChannelOrderStatusChangeEvent event);
}
