/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Apr 29, 2012
 *  @author singla
 */
package com.uniware.services.channel.saleorder;

import com.uniware.core.api.channel.FetchPendencyRequest;
import com.uniware.core.api.channel.FetchPendencyResponse;
import com.uniware.core.api.channel.UpdatePendencyRequest;
import com.uniware.core.api.channel.UpdatePendencyResponse;

/**
 * @author Sunny
 */
public interface IChannelPendencySyncService {

    FetchPendencyResponse fetchPendency(FetchPendencyRequest request);

    UpdatePendencyResponse updatePendency(UpdatePendencyRequest request);

    void refreshPendency(String channelCode);

}
