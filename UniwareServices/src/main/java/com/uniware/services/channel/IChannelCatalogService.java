/**
 * 
 */
package com.uniware.services.channel;

import com.unifier.core.utils.DateUtils.DateRange;
import com.uniware.core.api.channel.ChannelItemTypeLookupDTO;
import com.uniware.core.api.channel.CreateChannelItemTypeRequest;
import com.uniware.core.api.channel.CreateChannelItemTypeResponse;
import com.uniware.core.api.channel.CreateItemTypeFromChannelItemTypeRequest;
import com.uniware.core.api.channel.CreateItemTypeFromChannelItemTypeResponse;
import com.uniware.core.api.channel.DeleteChannelItemTypeRequest;
import com.uniware.core.api.channel.DeleteChannelItemTypeResponse;
import com.uniware.core.api.channel.DisableChannelItemTypeRequest;
import com.uniware.core.api.channel.DisableChannelItemTypeResponse;
import com.uniware.core.api.channel.GetChannelItemTypeDetailsRequest;
import com.uniware.core.api.channel.GetChannelItemTypeDetailsResponse;
import com.uniware.core.api.channel.IgnoreChannelItemTypeRequest;
import com.uniware.core.api.channel.IgnoreChannelItemTypeResponse;
import com.uniware.core.api.channel.LinkChannelItemTypeRequest;
import com.uniware.core.api.channel.LinkChannelItemTypeResponse;
import com.uniware.core.api.channel.ReenableCITInventorySyncStatusRequest;
import com.uniware.core.api.channel.ReenableCITInventorySyncStatusResponse;
import com.uniware.core.api.channel.ReenableInventorySyncRequest;
import com.uniware.core.api.channel.ReenableInventorySyncResponse;
import com.uniware.core.api.channel.RelistChannelItemTypeRequest;
import com.uniware.core.api.channel.RelistChannelItemTypeResponse;
import com.uniware.core.api.channel.UnlinkChannelItemTypeRequest;
import com.uniware.core.api.channel.UnlinkChannelItemTypeResponse;
import com.uniware.core.api.channel.UpdateChannelItemTypeTaxRequest;
import com.uniware.core.api.channel.UpdateChannelItemTypeTaxResponse;
import com.uniware.core.api.saleorder.CreateSaleOrderRequest;
import com.uniware.core.entity.ChannelItemType;
import com.uniware.core.entity.ChannelItemType.ListingStatus;
import com.uniware.core.entity.ItemType;
import com.uniware.core.vo.ChannelItemTypeTaxVO;
import com.uniware.core.vo.ChannelItemTypeVO;
import java.util.Date;
import java.util.List;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author parijat
 */
public interface IChannelCatalogService {

    CreateChannelItemTypeResponse createChannelItemType(CreateChannelItemTypeRequest request);

    ChannelItemType getChannelItemTypeByChannelAndChannelProductId(int channelId, String channelProductId, boolean lock);

    ChannelItemType blockChannelItemTypeInventory(String channelCode, String channelSkuCode, int quantity);

    ChannelItemTypeVO updateChannelItemType(ChannelItemTypeVO channelItemType);

    ChannelItemType lookupChannelItemType(String channelCode, String channelProductId, String sellerSkuCode, String productName);

    LinkChannelItemTypeResponse linkChannelItemType(LinkChannelItemTypeRequest request);

    IgnoreChannelItemTypeResponse ignoreChannelItemType(IgnoreChannelItemTypeRequest request);

    UnlinkChannelItemTypeResponse unlinkChannelItemType(UnlinkChannelItemTypeRequest request);

    ChannelItemType updateChannelItemType(ChannelItemType channelItemType);

    long getLinkedChannelItemTypeCount(int channelId);

    long getUnlinkedChannelItemTypeCount(int channelId);

    ChannelItemType getChannelItemTypeByChannelAndChannelProductId(int channelId, String channelProductId);

    List<ChannelItemType> getChannelItemTypesByItemTypeId(int itemTypeId);

    ChannelItemType getChannelItemTypeByChannelAndChannelProductId(String channelCode, String channelProductId);

    List<ChannelItemType> getChannelItemTypesByChannelAndItemTypeId(int channelId, int itemTypeId);

    List<ChannelItemType> getDirtyChannelItemTypes(int channelId, int start, int batchSize, Date lastUpdated);

    long getDirtyStockoutChannelItemTypeCount(int channelId, Date lastUpdated);

    long getDirtyChannelItemTypeCount(int channelId, Date lastUpdated);

    List<ChannelItemType> getDirtyStockoutChannelItemTypes(int channelId, int start, int batchSize, Date lastUpdated);

    ChannelItemType getChannelItemTypeByChannelAndChannelProductId(String channelCode, String channelProductId, boolean lock);

    ChannelItemTypeVO getChannelItemTypeVOByChannelAndChannelProductId(String channelCode, String channelProductId);

    boolean markChannelItemTypesForRecalculation(Integer channelId);

    boolean resetFailedChannelItemTypes(Integer id);

    void resetChannelPendency(Integer integer);

    List<ChannelItemType> getPendingChannelItemTypeForRecalculation(int start, int pageSize);

    ReenableCITInventorySyncStatusResponse reenableCITInventorySyncStatus(ReenableCITInventorySyncStatusRequest request);

    ReenableInventorySyncResponse reenableInventorySync(ReenableInventorySyncRequest request);

    boolean markAllChannelItemTypesForRecalculation(Integer channelId);

    void linkChannelItemTypes(ItemType itemType);

    CreateItemTypeFromChannelItemTypeResponse createItemTypeFromChannelItemType(CreateItemTypeFromChannelItemTypeRequest request);

    GetChannelItemTypeDetailsResponse getChannelItemTypeDetails(GetChannelItemTypeDetailsRequest request);

    RelistChannelItemTypeResponse relistChannelItemType(RelistChannelItemTypeRequest request);

    DeleteChannelItemTypeResponse deleteChannelItemType(DeleteChannelItemTypeRequest request);

    List<ChannelItemType> getPossiblyDirtyChannelItemTypes(int channelId, int start, int batchSize, Date lastUpdated);

    long getPossiblyDirtyChannelItemTypeCount(Integer id, Date lastUpdated);

    boolean updateChannelItemTypeInventoryStatusIfFresh(ChannelItemType cit);

    long getIgnoredChannelItemTypeCount(int channelId);

    long getChannelItemTypeCount(int channelId, ListingStatus status);

    DisableChannelItemTypeResponse disableChannelItemType(DisableChannelItemTypeRequest request);

    Date getLatestPossiblyDirtyTimestamp(int channelId);

    Date getLatestDirtyTimestamp(int channelId);

    Date getLatestStockoutDirtyTimestamp(int channelId);

    List<ChannelItemTypeLookupDTO> lookupLinkedChannelItemTypes(String channelCode, String keyword);

    ChannelItemTypeTaxVO update(ChannelItemTypeTaxVO channelItemTypeTax);

    ChannelItemTypeTaxVO getChannelItemTypeTax(String channelCode, String channelProductId);

    UpdateChannelItemTypeTaxResponse updateChannelItemTypeTaxes(UpdateChannelItemTypeTaxRequest request);

    ChannelItemType decrementNextInventoryUpdate(ChannelItemType cit, int quantity);

    long getChannelItemTypeCount(int channelId, String lastInventoryUpdateStatus);

    long getChannelItemTypeCount(int channelId, String lastInventoryUpdateStatus, DateRange dateRange);

    boolean markChannelItemTypeUnlinked(int channelId, String channelProductId);

    @Transactional boolean resetFailedOrderInventory();

    void updateFailedOrderInventory(CreateSaleOrderRequest request, boolean isNewFailedOrder);

    List<ChannelItemType> getChannelItemTypesWithPendency(String channelCode);

    List<ChannelItemType> getChannelItemTypeBySellerSkuCode(String sellerSkuCode);

    int disableStaleCatalog(String syncId, Integer channelId);
}
