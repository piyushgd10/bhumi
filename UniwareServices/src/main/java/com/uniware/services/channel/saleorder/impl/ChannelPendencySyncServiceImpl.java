/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Apr 29, 2012
 *  @author singla
 */
package com.uniware.services.channel.saleorder.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.uniware.services.channel.impl.ChannelServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.utils.StringUtils;
import com.unifier.core.utils.XMLParser;
import com.unifier.core.utils.XMLParser.Element;
import com.unifier.scraper.sl.runtime.IScriptProvider;
import com.unifier.scraper.sl.runtime.ScraperScript;
import com.unifier.scraper.sl.runtime.ScriptExecutionContext;
import com.uniware.core.api.channel.CreateChannelItemTypeRequest;
import com.uniware.core.api.channel.FetchPendencyDTO;
import com.uniware.core.api.channel.FetchPendencyRequest;
import com.uniware.core.api.channel.FetchPendencyResponse;
import com.uniware.core.api.channel.SyncChannelOrdersRequest;
import com.uniware.core.api.channel.UpdatePendencyRequest;
import com.uniware.core.api.channel.UpdatePendencyResponse;
import com.uniware.core.api.channel.WsChannelItemType;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.entity.Bundle;
import com.uniware.core.entity.BundleItemType;
import com.uniware.core.entity.Channel;
import com.uniware.core.entity.ChannelConfigurationParameter;
import com.uniware.core.entity.ChannelConnector;
import com.uniware.core.entity.ChannelConnectorParameter;
import com.uniware.core.entity.ChannelItemType;
import com.uniware.core.entity.ItemType;
import com.uniware.core.entity.ItemTypeInventorySnapshot;
import com.uniware.core.entity.Source;
import com.uniware.core.utils.UserContext;
import com.uniware.services.bundle.IBundleService;
import com.uniware.services.cache.ChannelCache;
import com.uniware.services.cache.ScriptVersionedCache;
import com.uniware.services.catalog.ICatalogService;
import com.uniware.services.channel.IChannelCatalogService;
import com.uniware.services.channel.IChannelInventorySyncService;
import com.uniware.services.channel.saleorder.IChannelOrderSyncService;
import com.uniware.services.channel.saleorder.IChannelPendencySyncService;
import com.uniware.services.channel.IChannelService;
import com.uniware.services.configuration.SourceConfiguration;
import com.uniware.services.configuration.data.manager.ProductConfiguration;
import com.uniware.services.inventory.IInventoryService;

/**
 * @author Sunny
 */
@Service
public class ChannelPendencySyncServiceImpl implements IChannelPendencySyncService {

    private static final Logger          LOG = LoggerFactory.getLogger(ChannelPendencySyncServiceImpl.class);

    @Autowired
    private ICatalogService              catalogService;

    @Autowired
    private IBundleService               bundleService;

    @Autowired
    private IChannelCatalogService       channelCatalogService;

    @Autowired
    private IInventoryService            inventoryService;

    @Autowired
    private IChannelOrderSyncService     channelOrderSyncService;

    @Autowired
    private IChannelInventorySyncService channelInventorySyncService;

    @Autowired
    private IChannelService              channelService;

    @Override
    public void refreshPendency(String channelCode) {
        Channel channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelByCode(channelCode);
        channelCatalogService.resetChannelPendency(channel.getId());
        FetchPendencyResponse fetchPendencyResponse = fetchPendency(new FetchPendencyRequest(channelCode, true));
        for (FetchPendencyDTO pendencyDTO : fetchPendencyResponse.getPendencyDTOs()) {
            ChannelItemType channelItemType = channelCatalogService.getChannelItemTypeByChannelAndChannelProductId(channel.getId(), pendencyDTO.getChannelProductId());
            if (channelItemType != null) {
                setChannelItemTypePendency(channelItemType, pendencyDTO.getRequiredInventory() == null ? 0 : pendencyDTO.getRequiredInventory());
            }
        }
    }

    //
    // Set the pendency for a channel item type. In case it is mapped to a bundle sku, then set the pendency for individual
    // components. We also need to take care of a scenario where a channel item type is part of a bundle and there are orders
    // for this channel item type individually and as part of bundle.
    // For example: A = 2B + 3C, now if there are 2 orders for A and 2 orders for C, the total pendency for C's channel item type
    // should be 8.
    //
    @Transactional
    private void setChannelItemTypePendency(ChannelItemType channelItemType, int pendency) {
        channelItemType = channelCatalogService.getChannelItemTypeByChannelAndChannelProductId(channelItemType.getChannel().getId(), channelItemType.getChannelProductId());
        ItemType itemType = channelItemType.getItemType();
        if (itemType != null) {
            inventoryService.markInventorySnapshotDirty(channelItemType.getItemType().getId());
        }
        channelItemType.setPendency(pendency);
        channelCatalogService.updateChannelItemType(channelItemType);
    }

    @Override
    public FetchPendencyResponse fetchPendency(FetchPendencyRequest request) {
        FetchPendencyResponse response = new FetchPendencyResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            Channel channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelByCode(request.getChannelCode());
            if (channel == null) {
                context.addError(WsResponseCode.INVALID_CHANNEL_CODE, WsResponseCode.INVALID_CHANNEL_CODE.message() + ": " + channel);
            } else {
                Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(channel.getSourceCode());
                if (!source.isPendencyConfigurationEnabled()) {
                    context.addError(WsResponseCode.PENDENCY_CONFIGURATION_NOT_ENABLED, WsResponseCode.PENDENCY_CONFIGURATION_NOT_ENABLED.message() + ": " + channel);
                } else {
                    List<FetchPendencyDTO> pendencyDTOs;
                    if (source.isCommitPendencyOnChannel() || request.isForceRefresh()) {
                        pendencyDTOs = fetchCurrentPendencyFromChannel(channel, context);
                    } else {
                        List<ChannelItemType> channelItemTypes = channelCatalogService.getChannelItemTypesWithPendency(channel.getCode());
                        pendencyDTOs = new ArrayList<>();
                        if (channelItemTypes != null) {
                            for (ChannelItemType channelItemType : channelItemTypes) {
                                FetchPendencyDTO pendencyDTO = new FetchPendencyDTO();
                                pendencyDTO.setProductName(channelItemType.getProductName());
                                pendencyDTO.setSellerSkuCode(channelItemType.getSellerSkuCode());
                                pendencyDTO.setChannelProductId(channelItemType.getChannelProductId());

                                if (ConfigurationManager.getInstance().getConfiguration(ProductConfiguration.class).isProductManagementSwitchedOff()) {
                                    pendencyDTO.setRequiredInventory(channelItemType.getPendency());
                                    pendencyDTO.setItemSku(channelItemType.getSellerSkuCode());
                                    pendencyDTO.setAvailableInventory(channelItemType.getPendency());
                                } else {
                                    ItemType itemType = catalogService.getAllItemTypeById(channelItemType.getItemType().getId());
                                    pendencyDTO.setItemSku(itemType.getSkuCode());
                                    pendencyDTO.setRequiredInventory(channelItemType.getPendency());
                                    pendencyDTO.setItemTypeId(channelItemType.getItemType().getId());
                                    if (ItemType.Type.BUNDLE.equals(itemType.getType())) {
                                        pendencyDTO.setAvailableInventory(channelInventorySyncService.getAvailableBundleInventory(itemType.getSkuCode()));
                                    } else {
                                        ItemTypeInventorySnapshot inventorySnapshot = inventoryService.getItemTypeInventorySnapshot(itemType.getId());
                                        pendencyDTO.setAvailableInventory(inventorySnapshot != null ? inventorySnapshot.getInventory() : 0);
                                    }
                                }
                                pendencyDTOs.add(pendencyDTO);
                            }
                        }
                    }

                    response.setChannelCode(channel.getCode());
                    response.setPendencyDTOs(pendencyDTOs);

                    Collections.sort(pendencyDTOs, new Comparator<FetchPendencyDTO>() {
                        @Override
                        public int compare(FetchPendencyDTO o1, FetchPendencyDTO o2) {
                            if (o1.getAvailableInventory() == null) {
                                if (o2.getAvailableInventory() == null) {
                                    return (o1.getProductName() == null || o2.getProductName() == null) ? 1 : o1.getProductName().compareTo(o2.getProductName());
                                } else {
                                    return 1;
                                }
                            } else {
                                if (o2.getAvailableInventory() == null) {
                                    return -1;
                                } else {
                                    return (o1.getProductName() == null || o2.getProductName() == null) ? -1 : o1.getProductName().compareTo(o2.getProductName());
                                }
                            }
                        }
                    });
                }
            }
        }
        response.setSuccessful(!context.hasErrors());
        response.addErrors(context.getErrors());
        return response;
    }

    private List<FetchPendencyDTO> fetchCurrentPendencyFromChannel(Channel channel, ValidationContext context) {
        List<FetchPendencyDTO> pendencyDTOs = new ArrayList<>();
        ChannelServiceImpl.ChannelGoodToSyncResponse channelGoodToSyncResponse = channelService.isChannelGoodToSync(channel.getCode(), ChannelServiceImpl.Sync.ORDER);
        if (!channelGoodToSyncResponse.isGoodToSync()) {
            LOG.warn("Connector broken for channel {}", channel.getName());
            context.addError(WsResponseCode.INVALID_STATE, "Connector broken for channel " + channel.getName());
        } else {
            ScraperScript fetchPendencyScript = CacheManager.getInstance().getCache(ChannelCache.class).getScriptByName(channel.getCode(),
                    com.uniware.core.entity.Source.FETCH_PENDENCY_SCRIPT_NAME);
            ScriptExecutionContext seContext = ScriptExecutionContext.current();
            for (ChannelConnector channelConnector : channel.getChannelConnectors()) {
                for (ChannelConnectorParameter channelConnectorParameter : channelConnector.getChannelConnectorParameters()) {
                    seContext.addVariable(channelConnectorParameter.getName(), channelConnectorParameter.getValue());
                }
            }
            for (ChannelConfigurationParameter channelConfigurationParameter : channel.getChannelConfigurationParameters()) {
                seContext.addVariable(channelConfigurationParameter.getSourceConfigurationParameterName(), channelConfigurationParameter.getValue());
            }
            Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(channel.getSourceCode());
            seContext.addVariable("channel", channel);
            seContext.addVariable("source", source);
            seContext.setScriptProvider(new IScriptProvider() {
                @Override
                public ScraperScript getScript(String scriptName) {
                    return CacheManager.getInstance().getCache(ScriptVersionedCache.class).getScriptByName(scriptName);
                }
            });
            try {
                seContext.setTraceLoggingEnabled(UserContext.current().isTraceLoggingEnabled());
                fetchPendencyScript.execute();
            } catch (Exception e) {
                context.addError(WsResponseCode.INVALID_STATE, e.getMessage());
                LOG.error("Error executing fetchPendencyScript: {}", fetchPendencyScript.getScriptName(), e);
            } finally {
                ScriptExecutionContext.destroy();
            }
            if (!context.hasErrors()) {
                String pendencyXml = seContext.getScriptOutput();
                LOG.info("Channel: {}, Fetch pendency response: {}", channel.getName(), pendencyXml);
                Element rootElement = XMLParser.parse(pendencyXml);
                for (Element pendency : rootElement.list("Pendency")) {
                    String channelProductId = pendency.text("ChannelProductId");
                    String sellerSkuCode = pendency.text("SellerSkuCode");
                    String productName = StringUtils.getNotNullValue(pendency.text("ProductName"));
                    String channelProductIdentifier = pendency.text("ChannelProductIdentifier");
                    ChannelItemType channelItemType = channelCatalogService.lookupChannelItemType(channel.getCode(), channelProductId, sellerSkuCode, productName);
                    FetchPendencyDTO pendencyDTO = new FetchPendencyDTO();
                    pendencyDTO.setProductName(productName);
                    pendencyDTO.setSellerSkuCode(sellerSkuCode);
                    pendencyDTO.setChannelProductId(channelProductId);
                    pendencyDTO.setChannelProductIdentifier(channelProductIdentifier);
                    if (channelItemType == null) {
                        WsChannelItemType wsChannelItemType = new WsChannelItemType();
                        wsChannelItemType.setChannelCode(channel.getCode());
                        wsChannelItemType.setChannelProductId(channelProductId);
                        wsChannelItemType.setSellerSkuCode(sellerSkuCode);
                        wsChannelItemType.setProductName(productName);
                        channelCatalogService.createChannelItemType(new CreateChannelItemTypeRequest(wsChannelItemType, ChannelItemType.SyncedBy.SYSTEM.toString()));
                    }

                    if (channelItemType != null && ChannelItemType.Status.LINKED.equals(channelItemType.getStatusCode())) {
                        ItemType itemType = catalogService.getAllItemTypeById(channelItemType.getItemType().getId());
                        pendencyDTO.setRequiredInventory(Integer.parseInt(pendency.text("RequiredInventory")));
                        pendencyDTO.setItemTypeId(itemType.getId());
                        pendencyDTO.setItemSku(itemType.getSkuCode());
                        if (ItemType.Type.BUNDLE.equals(itemType.getType())) {
                            pendencyDTO.setAvailableInventory(channelInventorySyncService.getAvailableBundleInventory(itemType.getSkuCode()));
                        } else {
                            ItemTypeInventorySnapshot inventorySnapshot = inventoryService.getItemTypeInventorySnapshot(itemType.getId());
                            pendencyDTO.setAvailableInventory(inventorySnapshot != null ? inventorySnapshot.getInventory() : 0);
                        }
                    } else if (ConfigurationManager.getInstance().getConfiguration(ProductConfiguration.class).isProductManagementSwitchedOff()) {
                        pendencyDTO.setRequiredInventory(Integer.parseInt(pendency.text("RequiredInventory")));
                        //pendencyDTO.setItemTypeId(itemType.getId());
                        pendencyDTO.setItemSku(sellerSkuCode);
                        pendencyDTO.setAvailableInventory(Integer.parseInt(pendency.text("RequiredInventory")));
                    }
                    pendencyDTOs.add(pendencyDTO);
                }
            }
        }
        return pendencyDTOs;
    }

    @Override
    public UpdatePendencyResponse updatePendency(UpdatePendencyRequest request) {
        UpdatePendencyResponse response = new UpdatePendencyResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            Channel channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelByCode(request.getChannelCode());
            if (channel == null) {
                context.addError(WsResponseCode.INVALID_CHANNEL_CODE, WsResponseCode.INVALID_CHANNEL_CODE.message() + ": " + channel);
            } else {
                Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(channel.getSourceCode());
                if (!source.isPendencyConfigurationEnabled()) {
                    context.addError(WsResponseCode.PENDENCY_CONFIGURATION_NOT_ENABLED, WsResponseCode.PENDENCY_CONFIGURATION_NOT_ENABLED.message() + ": " + channel);
                } else {
                    ScraperScript updatePendencyScript = CacheManager.getInstance().getCache(ChannelCache.class).getScriptByName(channel.getCode(),
                            com.uniware.core.entity.Source.PROCESS_PENDENCY_SCRIPT_NAME);
                    if (updatePendencyScript == null) {
                        LOG.error("Scripts missing for channel {}. Cannot run Channel Pendency Sync Task", channel.getName());
                    }
                    else {
                        ScriptExecutionContext seContext = ScriptExecutionContext.current();
                        seContext.addVariable("pendencyDTOs", request.getPendencyDTOs());
                        seContext.addVariable("channel", channel);
                        seContext.addVariable("source", source);
                        for (ChannelConnector channelConnector : channel.getChannelConnectors()) {
                            for (ChannelConnectorParameter channelConnectorParameter : channelConnector.getChannelConnectorParameters()) {
                                seContext.addVariable(channelConnectorParameter.getName(), channelConnectorParameter.getValue());
                            }
                        }
                        for (ChannelConfigurationParameter channelConfigurationParameter : channel.getChannelConfigurationParameters()) {
                            seContext.addVariable(channelConfigurationParameter.getSourceConfigurationParameterName(),
                                    channelConfigurationParameter.getValue());
                        }
                        seContext.setScriptProvider(new IScriptProvider() {
                            @Override public ScraperScript getScript(String scriptName) {
                                return CacheManager.getInstance().getCache(ScriptVersionedCache.class).getScriptByName(scriptName);
                            }
                        });
                        try {
                            seContext.setTraceLoggingEnabled(UserContext.current().isTraceLoggingEnabled());
                            updatePendencyScript.execute();
                            response.setMessage(seContext.getScriptOutput());
                        }
                        catch (Exception e) {
                            LOG.error("Error executing updatePendencyScript: {}", updatePendencyScript.getScriptName());
                            LOG.error("Stack trace: ", e);
                            context.addError(WsResponseCode.ERROR_UPDATING_PENDENCY, e.getMessage());
                        }
                        finally {
                            ScriptExecutionContext.destroy();
                        }
                    }
                }
            }
        }
        if (context.hasErrors()) {
            response.addErrors(context.getErrors());
        } else {
            channelOrderSyncService.syncChannelOrders(new SyncChannelOrdersRequest(request.getChannelCode()));
            response.setSuccessful(true);
        }
        return response;
    }

}
