/*
 *  Copyright 2013 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 14-Jun-2013
 *  @author unicom
 */
package com.uniware.services.channel.saleorder.status.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.uniware.core.entity.*;
import com.uniware.services.shipping.IShippingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.unifier.core.annotation.audit.LogActivity;
import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.utils.BatchProcessor;
import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.StringUtils;
import com.unifier.core.utils.XMLParser;
import com.unifier.core.utils.XMLParser.Element;
import com.unifier.scraper.sl.runtime.IScriptProvider;
import com.unifier.scraper.sl.runtime.ScraperScript;
import com.unifier.scraper.sl.runtime.ScriptExecutionContext;
import com.uniware.core.api.channel.ChannelOrderStatusDTO;
import com.uniware.core.api.channel.ForceSyncSaleOrderStatusRequest;
import com.uniware.core.api.channel.ForceSyncSaleOrderStatusResponse;
import com.uniware.core.api.channel.GetChannelSaleOrderStatusRequest;
import com.uniware.core.api.channel.GetChannelSaleOrderStatusResponse;
import com.uniware.core.api.channel.SyncOrderStatusRequest;
import com.uniware.core.api.channel.SyncOrderStatusResponse;
import com.uniware.core.concurrent.ContextAwareExecutorFactory;
import com.uniware.core.locking.ILockingService;
import com.uniware.core.locking.Namespace;
import com.uniware.core.locking.annotation.Lock;
import com.uniware.core.locking.annotation.Locks;
import com.uniware.core.utils.ActivityContext;
import com.uniware.core.utils.UserContext;
import com.uniware.services.audit.impl.ActivityEntityEnum;
import com.uniware.services.audit.impl.ActivityTypeEnum;
import com.uniware.services.audit.impl.ActivityUtils;
import com.uniware.services.cache.ChannelCache;
import com.uniware.services.cache.ScriptVersionedCache;
import com.uniware.services.channel.IChannelService;
import com.uniware.services.channel.impl.ChannelServiceImpl;
import com.uniware.services.channel.impl.ChannelServiceImpl.ChannelGoodToSyncResponse;
import com.uniware.services.channel.saleorder.status.IChannelOrderStatusChangeEventHandler;
import com.uniware.services.channel.saleorder.status.IChannelOrderStatusSyncService;
import com.uniware.services.channel.saleorder.status.event.ChannelOrderStatusChangeEvent;
import com.uniware.services.configuration.SourceConfiguration;
import com.uniware.services.saleorder.ISaleOrderService;

@Service("channelOrderStatusSyncService")
public class ChannelOrderStatusSyncServiceImpl implements IChannelOrderStatusSyncService {

    private static final Logger LOG = LoggerFactory.getLogger(ChannelOrderStatusSyncServiceImpl.class);
    private static final String SERVICE_NAME = "channelOrderStatusSyncService";

    @Autowired
    private ILockingService lockingService;

    @Autowired
    private ISaleOrderService saleOrderService;

    @Autowired
    private IShippingService shippingService;

    @Autowired
    private IChannelService channelService;

    @Autowired
    private ContextAwareExecutorFactory executorFactory;

    private Map<ChannelOrderStatusChangeEvent.Type, IChannelOrderStatusChangeEventHandler> statusCodeToHandler = new HashMap<>();

    @Override
    public void registerEventHandler(ChannelOrderStatusChangeEvent.Type type, IChannelOrderStatusChangeEventHandler handler) {
        statusCodeToHandler.put(type, handler);
    }

    @Override
    public GetChannelSaleOrderStatusResponse getChannelSaleOrderStatus(GetChannelSaleOrderStatusRequest request) {
        GetChannelSaleOrderStatusResponse response = new GetChannelSaleOrderStatusResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            response.setChannelOrderStatusDTO(channelService.getOrderSyncStatusOnChannel(request.getChannelCode()));
            response.setSuccessful(true);
        } else {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    @Override
    @Locks({@Lock(ns = Namespace.CHANNEL_ORDER_STATUS_SYNC, key = "#{#args[0].channelCode}")})
    @Transactional
    public SyncOrderStatusResponse syncSaleOrderStatus(SyncOrderStatusRequest request) {
        SyncOrderStatusResponse response = new SyncOrderStatusResponse();
        Channel channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelByCode(request.getChannelCode());
        ChannelOrderStatusDTO status = channelService.getOrderSyncStatusOnChannel(request.getChannelCode());
        if (status.isRunning()) {
            response.setMessage("Sync already running");
        } else {
            status.reset();
            status.setRunning(true);
            status.setLastSyncTime(DateUtils.getCurrentTime());
            executorFactory.getExecutor(SERVICE_NAME).submit(new SyncChannelOrderStatus(channel, request.getNumOfDays(), request.isSyncPendingOrdersOnly(), status));
            response.setSuccessful(true);
            response.setMessage("Sync started");
        }
        return response;
    }

    private class SyncChannelOrderStatus implements Runnable {
        private final Channel channel;
        private final Integer numberOfDays;
        private final boolean syncPendingOrdersOnly;
        private final ChannelOrderStatusDTO status;
        private int ordersScanned;

        public SyncChannelOrderStatus(Channel channel, Integer numberOfDays, boolean syncPendingOrdersOnly, ChannelOrderStatusDTO status) {
            this.channel = channel;
            this.status = status;
            this.numberOfDays = numberOfDays;
            this.syncPendingOrdersOnly = syncPendingOrdersOnly;
        }

        @Override
        public void run() {
            java.util.concurrent.locks.Lock lock = lockingService.getLock(Namespace.CHANNEL_ORDER_STATUS_SYNC_DO, channel.getCode());
            try {
                lock.lock();
                LOG.info("Verifying order status sync connectors for channel {}", channel.getName());
                final ChannelGoodToSyncResponse channelGoodToSyncResponse = channelService.isChannelGoodToSync(channel.getCode(), ChannelServiceImpl.Sync.ORDER);
                if (!channelGoodToSyncResponse.isGoodToSync()) {
                    LOG.info("Connector broken for channel {}", channel.getName());
                    updateStatus();
                    return;
                }
                final Map<String, Object> metadata = getChannelOrderStatusSyncMetadata(channel, channelGoodToSyncResponse.getParams());
                final Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(channel.getSourceCode());
                LOG.info("Running status sync for channel {}", channel.getName());
                final Long milestones = saleOrderService.getSaleOrderCountForStatusSync(channel.getId(), syncPendingOrdersOnly, numberOfDays);
                LOG.info("Statuses of total {} orders to be synced with channel {}. syncPendingOrdersOnly : {}", milestones, channel.getName(), syncPendingOrdersOnly);
                status.setTotalMileStones(milestones);
                BatchProcessor<SaleOrder> batchProcessor = new BatchProcessor<SaleOrder>(100) {
                    private int maxId = 0;

                    @Override
                    protected void process(List<SaleOrder> batchItems, int batchIndex) {
                        status.incrementMileStone();
                        int errorCount = 0;
                        for (SaleOrder saleOrder : batchItems) {
                            try {
                                syncSaleOrderStatus(saleOrder.getCode(), channel, channelGoodToSyncResponse.getParams(), metadata);
                                ordersScanned++;
                                errorCount = 0;
                            } catch (Exception e) {
                                errorCount++;
                                LOG.error("Error executing status sync for saleOrder: " + saleOrder.getCode(), e);
                                if (errorCount >= 5) {
                                    LOG.error("[FATAL ERROR] Error executing status sync task for channel: {}", source.getName());
                                    LOG.error("Stack trace: ", e);
                                    updateStatus();
                                    forceExit();
                                    return;
                                }
                            }
                        }
                        LOG.info("ordersScanned {}, errorCount {}, syncPendingOrdersOnly {}", milestones, channel.getName(), syncPendingOrdersOnly);
                        if (!batchItems.isEmpty()) {
                            maxId = batchItems.get(batchItems.size() - 1).getId();
                        }
                    }

                    @Override
                    protected List<SaleOrder> next(int start, int batchSize) {
                        return saleOrderService.getSaleOrdersForStatusSync(channel.getId(), syncPendingOrdersOnly, numberOfDays, maxId, batchSize);
                    }
                };
                batchProcessor.process();
            } catch (Throwable e) {
                LOG.error("Error running ChannelOrderStatusSync for channel: " + channel.getCode(), e);
            } finally {
                updateStatus();
                lock.unlock();
            }
        }

        private void updateStatus() {
            status.setShipmentsScanned(ordersScanned);
            status.setLastSyncTime(DateUtils.getCurrentTime());
            status.setRunning(false);
            channelService.updateChannelShipmentSyncStatus(status);
        }
    }

    @Override
    @LogActivity
    public ForceSyncSaleOrderStatusResponse forceSyncSaleOrderStatus(ForceSyncSaleOrderStatusRequest request) {
        ForceSyncSaleOrderStatusResponse response = new ForceSyncSaleOrderStatusResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            Map<Integer, List<String>> channelIdToSaleOrderCodes = new HashMap<>();
            for (String saleOrderCode : request.getSaleOrderCodes()) {
                SaleOrder saleOrder = saleOrderService.getSaleOrderByCode(saleOrderCode);
                List<String> channelSaleOrderCodes = channelIdToSaleOrderCodes.get(saleOrder.getChannel().getId());
                if (channelSaleOrderCodes == null) {
                    channelSaleOrderCodes = new ArrayList<>();
                    channelIdToSaleOrderCodes.put(saleOrder.getChannel().getId(), channelSaleOrderCodes);
                }
                channelSaleOrderCodes.add(saleOrderCode);
            }

            for (Entry<Integer, List<String>> e : channelIdToSaleOrderCodes.entrySet()) {
                Channel channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelById(e.getKey());
                if (StringUtils.isNotBlank(CacheManager.getInstance().getCache(ChannelCache.class).getScriptName(channel.getCode(),
                        com.uniware.core.entity.Source.SALE_ORDER_STATUS_SYNC_SCRIPT_NAME))) {
                    ChannelGoodToSyncResponse channelGoodToSyncResponse = channelService.isChannelGoodToSync(channel.getCode(), ChannelServiceImpl.Sync.ORDER);
                    if (!channelGoodToSyncResponse.isGoodToSync()) {
                        LOG.warn("Connector broken for channel {}", channel.getName());
                    } else {
                        Map<String, Object> metadata = getChannelOrderStatusSyncMetadata(channel, channelGoodToSyncResponse.getParams());
                        for (String saleOrderCode : e.getValue()) {
                            try {
                                syncSaleOrderStatus(saleOrderCode, channel, channelGoodToSyncResponse.getParams(), metadata);
                            } catch (Exception ex) {
                                LOG.error("Error syncing status for saleOrder: " + saleOrderCode, ex);
                            }
                        }
                    }
                }
                response.setSuccessful(true);
            }
        }
        return response;
    }

    private Map<String, Object> getChannelOrderStatusSyncMetadata(Channel channel, Map<String, String> channelParams) {
        ScraperScript saleOrderStatusSyncMetadataScript = null;
        Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(channel.getSourceCode());
        if (CacheManager.getInstance().getCache(ChannelCache.class).getScriptName(channel.getCode(), com.uniware.core.entity.Source.SALE_ORDER_STATUS_SYNC_METADATA) != null) {
            saleOrderStatusSyncMetadataScript = CacheManager.getInstance().getCache(ChannelCache.class).getScriptByName(channel.getCode(),
                    com.uniware.core.entity.Source.SALE_ORDER_STATUS_SYNC_METADATA);
        }
        final Map<String, Object> resultItems = new HashMap<>();
        if (saleOrderStatusSyncMetadataScript != null) {
            ScriptExecutionContext seContext = ScriptExecutionContext.current();
            seContext.addVariable("channel", channel);
            seContext.addVariable("source", source);
            seContext.addVariable("resultItems", resultItems);
            for (Entry<String, String> e : channelParams.entrySet()) {
                seContext.addVariable(e.getKey(), e.getValue());
            }
            seContext.setScriptProvider(new IScriptProvider() {
                @Override
                public ScraperScript getScript(String scriptName) {
                    return CacheManager.getInstance().getCache(ScriptVersionedCache.class).getScriptByName(scriptName);
                }
            });
            try {
                seContext.setTraceLoggingEnabled(UserContext.current().isTraceLoggingEnabled());
                saleOrderStatusSyncMetadataScript.execute();
            } catch (Exception e) {
                LOG.error("Error fetching sale order sync metadata for channel:" + channel.getCode(), e);
            } finally {
                ScriptExecutionContext.destroy();
            }
        }
        return resultItems;
    }

    @Locks({@Lock(ns = Namespace.SALE_ORDER, key = "#{#args[0]}")})
    @Transactional
    private void syncSaleOrderStatus(String saleOrderCode, Channel channel, Map<String, String> channelParams, Map<String, Object> metadata) {
        Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(channel.getSourceCode());
        ScraperScript saleOrderStatusSyncScript = CacheManager.getInstance().getCache(ChannelCache.class).getScriptByName(channel.getCode(),
                Source.SALE_ORDER_STATUS_SYNC_SCRIPT_NAME);
        SaleOrder saleOrder = saleOrderService.getSaleOrderForUpdate(saleOrderCode);
        Map<String, SaleOrderItem> codeToSaleOrderItem = new HashMap<>(saleOrder.getSaleOrderItems().size());
        for (SaleOrderItem soi : saleOrder.getSaleOrderItems()) {
            codeToSaleOrderItem.put(soi.getCode(), soi);
            if (soi.getFacility() != null) {
                LOG.info("Setting current facility id to {}", soi.getFacility().getId());
                UserContext.current().setFacility(soi.getFacility());
            }
        }
        final ScriptExecutionContext context = ScriptExecutionContext.current();
        context.addVariable("source", source);
        context.addVariable("channel", channel);
        context.addVariable("saleOrder", saleOrder);
        context.addVariable("cancelledSaleOrdersMap", metadata);
        for (Entry<String, String> e : channelParams.entrySet()) {
            context.addVariable(e.getKey(), e.getValue());
        }
        context.setScriptProvider(new IScriptProvider() {
            @Override
            public ScraperScript getScript(String scriptName) {
                return CacheManager.getInstance().getCache(ScriptVersionedCache.class).getScriptByName(scriptName);
            }
        });
        Map<ChannelOrderStatusChangeEvent.Type, List<Element>> statusCodeToSaleOrderItems = new HashMap<>();
        try {
            context.setTraceLoggingEnabled(UserContext.current().isTraceLoggingEnabled());
            saleOrderStatusSyncScript.execute();
            String saleOrderItemSyncStatusXml = context.getScriptOutput();
            LOG.info("SaleOrder: {}, saleOrderItemSyncStatusXml: {}", saleOrder.getCode(), saleOrderItemSyncStatusXml);
            if (StringUtils.isNotBlank(saleOrderItemSyncStatusXml)) {
                Element rootElement = XMLParser.parse(saleOrderItemSyncStatusXml);
                Element saleOrderNode = rootElement.get("SaleOrderCodeChange");
                if (saleOrderNode != null) {
                    //
                    // A special case handled for snapdeal where a dispatched order comes back as pending due
                    // to qc rejection.
                    //
                    handleSaleOrderCodeChange(saleOrderCode, saleOrder, saleOrderNode);
                } else {
                    for (Element saleOrderItemElement : rootElement.list("SaleOrderItem")) {
                        ChannelOrderStatusChangeEvent.Type eventType = ChannelOrderStatusChangeEvent.Type.valueOf(saleOrderItemElement.text("StatusCode"));
                        List<Element> saleOrderItemElements = statusCodeToSaleOrderItems.get(eventType);
                        if (saleOrderItemElements == null) {
                            saleOrderItemElements = new ArrayList<>();
                            statusCodeToSaleOrderItems.put(eventType, saleOrderItemElements);
                        }
                        saleOrderItemElements.add(saleOrderItemElement);
                    }
                }
            }
        } finally {
            ScriptExecutionContext.destroy();
        }
        for (Entry<ChannelOrderStatusChangeEvent.Type, List<Element>> e : statusCodeToSaleOrderItems.entrySet()) {
            IChannelOrderStatusChangeEventHandler handler = statusCodeToHandler.get(e.getKey());
            ChannelOrderStatusChangeEvent event = new ChannelOrderStatusChangeEvent();
            event.setSaleOrderCode(saleOrderCode);
            event.setType(e.getKey());
            List<ChannelOrderStatusChangeEvent.WsSaleOrderItem> saleOrderItems = new ArrayList<>(e.getValue().size());
            for (Element element : e.getValue()) {
                ChannelOrderStatusChangeEvent.WsSaleOrderItem saleOrderItem = new ChannelOrderStatusChangeEvent.WsSaleOrderItem();
                saleOrderItem.setCode(element.text("Code"));
                Map<String, String> attributes = new HashMap<>();
                for (Element el : element.list()) {
                    attributes.put(el.getName(), el.text());
                }
                saleOrderItem.setAttributes(attributes);
                saleOrderItems.add(saleOrderItem);
            }
            event.setSaleOrderItems(saleOrderItems);
            try {
                LOG.info("Passing event: {} to handler: {}", event, handler.getClass().getName());
                handler.handle(event);
                LOG.info("Event: {} handled successfully", event);
            } catch (Exception ex) {
                LOG.error("Error handling event " + event, ex);
            }
        }
    }

    private SaleOrder handleSaleOrderCodeChange(String saleOrderCode, SaleOrder saleOrder, Element saleOrderNode) {
        LOG.info("Sale Order Code Changed on Channel {}", saleOrder.getCode());
        LOG.info("Changing SaleOrder code to {}", saleOrderNode.text("SaleOrderCode"));
        saleOrder.setCode(saleOrderNode.text("SaleOrderCode"));
        saleOrder.setDisplayOrderCode(saleOrderNode.text("DisplayOrderCode"));
        saleOrder = saleOrderService.updateSaleOrder(saleOrder);
        List<ShippingPackage> shippingPackages = shippingService.getAllShippingPackagesBySaleOrder(saleOrder.getCode());
        for (ShippingPackage shippingPackage : shippingPackages) {
            if (StringUtils.isNotBlank(shippingPackage.getTrackingNumber()))  {
                LOG.info("Changing tracking number from {} to {}", shippingPackage.getTrackingNumber(), shippingPackage.getTrackingNumber() + "-D" );
                shippingPackage.setTrackingNumber(shippingPackage.getTrackingNumber() + "-D");
                shippingService.updateShippingPackage(shippingPackage);
            }
        }
        if (ActivityContext.current().isEnable()) {
            ActivityUtils.appendActivity(saleOrderCode, ActivityEntityEnum.SALE_ORDER.getName(), saleOrderCode,
                    Arrays.asList(new String[]{"Sale Order Code " + saleOrder.getCode() + " : Changed on Channel"}), ActivityTypeEnum.SYNC_TRIGGERED.name());
        }
        return saleOrder;
    }
}
