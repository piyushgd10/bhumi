/*
 *  Copyright 2013 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 14-Jun-2013
 *  @author unicom
 */
package com.uniware.services.channel.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.scraper.sl.runtime.IScriptProvider;
import com.unifier.scraper.sl.runtime.ScraperScript;
import com.unifier.scraper.sl.runtime.ScriptExecutionContext;
import com.uniware.core.entity.Channel;
import com.uniware.core.entity.ChannelConfigurationParameter;
import com.uniware.core.entity.ChannelConnector;
import com.uniware.core.entity.ChannelConnectorParameter;
import com.uniware.core.utils.UserContext;
import com.uniware.core.vo.ChannelCaptchaVO;
import com.uniware.core.entity.Source;
import com.uniware.dao.channel.IChannelMao;
import com.uniware.services.cache.ChannelCache;
import com.uniware.services.cache.ScriptVersionedCache;
import com.uniware.services.channel.IChannelHeartbeatSyncService;
import com.uniware.services.configuration.SourceConfiguration;

@Service("channelHeartbeatSyncService")
public class ChannelHeartbeatSyncServiceImpl implements IChannelHeartbeatSyncService {

    private static final Logger LOG = LoggerFactory.getLogger(ChannelHeartbeatSyncServiceImpl.class);

    @Autowired
    private IChannelMao channelMao;

    @Override
    public ChannelCaptchaVO getChannelCaptcha(String channelCode) {
        return channelMao.getChannelCaptcha(channelCode);
    }

    @Override
    public void removeChannelCaptcha(String channelCode) {
        channelMao.removeChannelCaptcha(channelCode);
    }

    @Override
    public void save(ChannelCaptchaVO channelCaptcha) {
        channelMao.save(channelCaptcha);
    }

    @Override
    public boolean syncChannelHeartbeat(String channelCode) {
        boolean success = true;
        Channel channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelByCode(channelCode);
        Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(channel.getSourceCode());
        ScraperScript heartbeatSyncScript = CacheManager.getInstance().getCache(ChannelCache.class).getScriptByName(channel.getCode(), Source.HEARTBEAT_SYNC_SCRIPT_NAME);
        if (heartbeatSyncScript != null) {
            final ScriptExecutionContext context = ScriptExecutionContext.current();
            context.addVariable("source", source);
            context.addVariable("channel", channel);
            for (ChannelConnector cc : channel.getChannelConnectors()) {
                for (ChannelConnectorParameter channelConnectorParameter : cc.getChannelConnectorParameters()) {
                    context.addVariable(channelConnectorParameter.getName(), channelConnectorParameter.getValue());
                }
            }
            for (ChannelConfigurationParameter channelConfigurationParameter : channel.getChannelConfigurationParameters()) {
                context.addVariable(channelConfigurationParameter.getSourceConfigurationParameterName(), channelConfigurationParameter.getValue());
            }
            context.setScriptProvider(scriptName -> CacheManager.getInstance().getCache(ScriptVersionedCache.class).getScriptByName(scriptName));
            try {
                context.setTraceLoggingEnabled(UserContext.current().isTraceLoggingEnabled());
                LOG.info("Syncing heartbeat for channel: {}", channel.getCode());
                heartbeatSyncScript.execute();
                LOG.info("Done Syncing heartbeat for channel: {}", channel.getCode());
            } catch (Exception e) {
                success = false;
                LOG.error("Unable to sync channel heartbeat for channel: {}", channel.getCode(), e);
            } finally {
                ScriptExecutionContext.destroy();
            }
        }
        return success;
    }

    //    @Override
    //    public void fetchChannelCaptcha(String channelCode) {
    //        Channel channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelByCode(channelCode);
    //        Source source = channel.getSource();
    //        ScraperScript fetchCaptchaScript = ConfigurationManager.getInstance().getConfiguration(ScriptConfiguration.class).getScriptByName(source.getCaptchaFetchScriptName());
    //        final ScriptExecutionContext context = ScriptExecutionContext.current();
    //        context.addVariable("source", channel.getSource());
    //        context.addVariable("channel", channel);
    //        try {
    //            context.setTraceLoggingEnabled(UserContext.current().isTraceLoggingEnabled());
    //            fetchCaptchaScript.execute();
    //            String captchaXml = context.getScriptOutput();
    //            if (StringUtils.isNotBlank(captchaXml)) {
    //                Element rootElement = XMLParser.parse(captchaXml);
    //                ChannelCaptchaVO channelCaptcha1 = new ChannelCaptchaVO();
    //                channelCaptcha1.setChannelCode(channel.getCode());
    //                channelCaptcha1.setCreated(DateUtils.getCurrentTime());
    //                channelCaptcha1.setChallengeId(rootElement.text("ChallengeId"));
    //                channelCaptcha1.setImageUrl(rootElement.text("ImageUrl"));
    //                save(channelCaptcha1);
    //            }
    //        } catch (Exception e) {
    //            LOG.error("[FATAL ERROR] Unable to fetch captcha for channel: {}", channel.getCode());
    //        } finally {
    //            ScriptExecutionContext.destroy();
    //        }
    //    }
    //
    //    @Override
    //    public void updateChannelCaptcha(String channelCode) {
    //        Channel channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelByCode(channelCode);
    //        Source source = channel.getSource();
    //
    //        // Attempt login
    //        boolean success = true;
    //        ScraperScript captchaUpdateScript = ConfigurationManager.getInstance().getConfiguration(ScriptConfiguration.class).getScriptByName(source.getCaptchaUpdateScriptName());
    //        if (captchaUpdateScript != null) {
    //            final ScriptExecutionContext context = ScriptExecutionContext.current();
    //            context.addVariable("source", channel.getSource());
    //            context.addVariable("channel", channel);
    //            for (ChannelConnector cc : channel.getChannelConnectors()) {
    //                for (ChannelConnectorParameter channelConnectorParameter : cc.getChannelConnectorParameters()) {
    //                    context.addVariable(channelConnectorParameter.getName(), channelConnectorParameter.getValue());
    //                }
    //            }
    //            try {
    //                context.setTraceLoggingEnabled(UserContext.current().isTraceLoggingEnabled());
    //                captchaUpdateScript.execute();
    //            } catch (Exception e) {
    //                LOG.error("Unable to login on channel: {}", channel.getCode());
    //                success = false;
    //            } finally {
    //                ScriptExecutionContext.destroy();
    //            }
    //            if (success) {
    //                removeChannelCaptcha(channel.getCode());
    //            }
    //        }
    //
    //    }
}
