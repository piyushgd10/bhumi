/*
*  Copyright 2015 Unicommerce eSolutions (P) Limited . All Rights Reserved.
*  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
*  
*  @version     1.0, 20/11/15
*  @author sunny
*/

package com.uniware.services.channel.saleorder.status.event;

import java.util.List;
import java.util.Map;

public class ChannelOrderStatusChangeEvent {

    public enum Type {
        DELIVERED,
        CANCELLED,
        DISPATCHED,
        RETURN_EXPECTED,
        RETURNED
    }

    public static class WsSaleOrderItem {
        private String              code;
        private Map<String, String> attributes;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public Map<String, String> getAttributes() {
            return attributes;
        }

        public void setAttributes(Map<String, String> attributes) {
            this.attributes = attributes;
        }

        @Override
        public String toString() {
            return "WsSaleOrderItem{" + "code='" + code + '\'' + ", attributes=" + attributes + '}';
        }
    }

    private String                saleOrderCode;

    private Type                  type;

    private List<WsSaleOrderItem> saleOrderItems;

    public String getSaleOrderCode() {
        return saleOrderCode;
    }

    public void setSaleOrderCode(String saleOrderCode) {
        this.saleOrderCode = saleOrderCode;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public List<WsSaleOrderItem> getSaleOrderItems() {
        return saleOrderItems;
    }

    public void setSaleOrderItems(List<WsSaleOrderItem> saleOrderItems) {
        this.saleOrderItems = saleOrderItems;
    }

    @Override
    public String toString() {
        return "ChannelOrderStatusChangeEvent{" + "saleOrderCode='" + saleOrderCode + '\'' + ", type=" + type + ", saleOrderItems=" + saleOrderItems + '}';
    }
}
