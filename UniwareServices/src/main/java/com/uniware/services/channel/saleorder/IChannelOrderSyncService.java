/*
 *  Copyright 2013 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 14-Jun-2013
 *  @author Sunny
 */
package com.uniware.services.channel.saleorder;

import com.uniware.core.api.channel.GetChannelOrderSyncStatusRequest;
import com.uniware.core.api.channel.GetChannelOrderSyncStatusResponse;
import com.uniware.core.api.channel.SyncChannelOrdersRequest;
import com.uniware.core.api.channel.SyncChannelOrdersResponse;
import com.uniware.core.api.saleorder.RefreshSaleOrderRequest;
import com.uniware.core.api.saleorder.RefreshSaleOrderResponse;
import com.uniware.services.messaging.jms.SyncChannelOrdersEvent;

public interface IChannelOrderSyncService {

    SyncChannelOrdersResponse syncChannelOrders(SyncChannelOrdersRequest request);

    RefreshSaleOrderResponse refreshSaleOrder(RefreshSaleOrderRequest request);

    GetChannelOrderSyncStatusResponse getChannelOrderSyncStatus(GetChannelOrderSyncStatusRequest request);

    void syncChannelOrders(SyncChannelOrdersEvent event);
}
