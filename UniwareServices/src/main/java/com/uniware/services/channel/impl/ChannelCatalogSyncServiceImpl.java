/*
 * Copyright 2013 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 * UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license
 * terms.
 * 
 * @version 1.0, 14-Jun-2013
 * 
 * @author unicom
 */
package com.uniware.services.channel.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.jibx.runtime.JiBXException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import com.unifier.core.annotation.audit.LogActivity;
import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.email.EmailMessage;
import com.unifier.core.entity.AsyncScriptInstruction;
import com.unifier.core.entity.AsyncScriptInstructionParameter;
import com.unifier.core.jms.MessagingConstants;
import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.JibxUtils;
import com.unifier.core.utils.StringUtils;
import com.unifier.core.utils.XMLParser;
import com.unifier.scraper.sl.runtime.IScriptProvider;
import com.unifier.scraper.sl.runtime.ScraperScript;
import com.unifier.scraper.sl.runtime.ScriptExecutionContext;
import com.unifier.services.email.IEmailService;
import com.unifier.services.tasks.IAsyncScriptService;
import com.unifier.services.tasks.engine.callback.IResumable;
import com.uniware.core.api.channel.ChannelCatalogSyncStatusDTO;
import com.uniware.core.api.channel.CreateChannelItemTypeRequest;
import com.uniware.core.api.channel.CreateChannelItemTypeResponse;
import com.uniware.core.api.channel.GetCatalogSyncResponse;
import com.uniware.core.api.channel.GetChannelCatalogSyncStatusRequest;
import com.uniware.core.api.channel.GetChannelCatalogSyncStatusResponse;
import com.uniware.core.api.channel.SyncChannelCatalogRequest;
import com.uniware.core.api.channel.SyncChannelCatalogResponse;
import com.uniware.core.api.model.WsChannelProductIdentifiers;
import com.uniware.core.concurrent.ContextAwareExecutorFactory;
import com.uniware.core.entity.Channel;
import com.uniware.core.entity.ChannelItemType;
import com.uniware.core.entity.Source;
import com.uniware.core.locking.ILockingService;
import com.uniware.core.locking.Namespace;
import com.uniware.core.utils.ActivityContext;
import com.uniware.core.utils.Constants;
import com.uniware.core.utils.UserContext;
import com.uniware.services.audit.impl.ActivityEntityEnum;
import com.uniware.services.audit.impl.ActivityTypeEnum;
import com.uniware.services.audit.impl.ActivityUtils;
import com.uniware.services.cache.ChannelCache;
import com.uniware.services.cache.ScriptVersionedCache;
import com.uniware.services.channel.IChannelCatalogService;
import com.uniware.services.channel.IChannelCatalogSyncService;
import com.uniware.services.channel.IChannelService;
import com.uniware.services.configuration.SourceConfiguration;
import com.uniware.services.configuration.SystemConfiguration;
import com.uniware.services.messaging.jms.ActiveMQConnector;
import com.uniware.services.messaging.jms.SyncChannelCatalogEvent;
import com.uniware.services.tasks.imports.ChannelCatalogSyncTask;

@Service("channelCatalogSyncService")
public class ChannelCatalogSyncServiceImpl implements IChannelCatalogSyncService, IResumable {

    private static final Logger         LOG               = LoggerFactory.getLogger(ChannelCatalogSyncServiceImpl.class);
    public static final String          TOTAL_PAGES       = "TotalPages";
    public static final String          CHANNEL_ITEM_TYPE = "ChannelItemType";
    private static final String         SERVICE_NAME      = "channelcatalogsyncservice";

    @Autowired
    private ApplicationContext          applicationContext;

    @Autowired
    private IChannelService             channelService;

    @Autowired
    private IChannelCatalogService      channelCatalogService;

    @Autowired
    private IAsyncScriptService         asyncScriptTaskService;

    @Autowired
    private ILockingService             lockingService;

    @Autowired
    private IEmailService               emailService;

    @Autowired
    @Qualifier("activeMQConnector1")
    private ActiveMQConnector           activeMQConnector;

    @Autowired
    private ContextAwareExecutorFactory executorFactory;

    @Override
    public SyncChannelCatalogResponse syncChannelCatalog(SyncChannelCatalogRequest request) {

        SyncChannelCatalogResponse response = new SyncChannelCatalogResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            Channel channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelByCode(request.getChannelCode());
            if (channel == null) {
                response.setMessage("Invalid channel");
            } else if (!channel.isEnabled()) {
                LOG.info("Channel disabled: {}", channel.getCode());
                response.setMessage("Channel disabled");
            } else {
                SyncChannelCatalogEvent syncChannelCatalogEvent = new SyncChannelCatalogEvent();
                syncChannelCatalogEvent.setSyncChannelCatalogRequest(request);
                syncChannelCatalogEvent.setRequestTimestamp(DateUtils.getCurrentTime());
                activeMQConnector.produceContextAwareMessage(MessagingConstants.Queue.CATALOG_SYNC_QUEUE, syncChannelCatalogEvent);
                response.setSuccessful(true);
                response.setMessage("Request queued");
                LOG.info("G for channel: {}", channel.getCode());
            }
        }
        return response;
    }

    @Override
    public GetChannelCatalogSyncStatusResponse getChannelCatalogSyncStatus(GetChannelCatalogSyncStatusRequest request) {
        GetChannelCatalogSyncStatusResponse response = new GetChannelCatalogSyncStatusResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            response.setCatalogSyncStatus(channelService.getChannelCatalogSyncStatus(request.getChannelCode()));
            response.setSuccessful(true);
        } else {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    @LogActivity
    @Override
    public void syncChannelCatalog(SyncChannelCatalogEvent syncChannelCatalogEvent) {
        Channel channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelByCode(syncChannelCatalogEvent.getSyncChannelCatalogRequest().getChannelCode());
        Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(channel.getSourceCode());
        java.util.concurrent.locks.Lock catalogSyncRunningLock = lockingService.getLock(Namespace.CHANNEL_CATALOG_SYNC_DO, channel.getCode());
        boolean acquired = false;
        try {
            acquired = catalogSyncRunningLock.tryLock();
            if (!acquired) {
                LOG.warn("Failed to obtain {} lock for channel {}. Sync is already running.", Namespace.CHANNEL_CATALOG_SYNC_DO.name(), channel.getCode());
            } else {
                ChannelCatalogSyncStatusDTO channelCatalogSyncStatus = channelService.getChannelCatalogSyncStatus(channel.getCode());
                if (channelCatalogSyncStatus.getLastSyncTime() != null && channelCatalogSyncStatus.getLastSyncTime().after(syncChannelCatalogEvent.getRequestTimestamp())) {
                    LOG.warn("Ignoring SyncChannelCatalogEvent as lastSyncTime: {} is newer than requestTimestamp: {}", channelCatalogSyncStatus.getLastSyncTime(),
                            syncChannelCatalogEvent.getRequestTimestamp());
                } else {
                    boolean alreadyRunning = false;
                    if (source.isAsyncCatalogSyncSupported() && Channel.SyncExecutionStatus.RUNNING.equals(channelCatalogSyncStatus.getSyncExecutionStatus())) {
                        LOG.info("Checking if there is any pending async instruction");
                        AsyncScriptInstruction asyncScriptInstruction = asyncScriptTaskService.getPendingAsyncScriptInstruction("channel", channel.getCode(), this.getClass().getName());
                        if (asyncScriptInstruction != null) {
                            // A special check for channels like AMAZON in which we fetch the catalog file asynchronously.
                            alreadyRunning = true;
                        }
                    }

                    if (!alreadyRunning) {
                        ScraperScript channelItemTypeListScript = CacheManager.getInstance().getCache(ChannelCache.class).getScriptByName(channel.getCode(),
                                com.uniware.core.entity.Source.CHANNEL_ITEM_TYPE_LIST_SCRIPT_NAME);
                        ScraperScript channelItemTypeDetailsScript = CacheManager.getInstance().getCache(ChannelCache.class).getScriptByName(channel.getCode(),
                                com.uniware.core.entity.Source.CHANNEL_ITEM_TYPE_DETAILS_SCRIPT_NAME);
                        if (channelItemTypeListScript == null || channelItemTypeDetailsScript == null) {
                            LOG.error("Scripts missing for channel {}", channel.getName());
                        } else {
                            if (ActivityContext.current().isEnable() && StringUtils.isNotBlank(UserContext.current().getUniwareUserName())) {
                                ActivityUtils.appendActivity(channel.getCode(), ActivityEntityEnum.CHANNEL.getName(), channel.getSourceCode(),
                                        Arrays.asList(new String[] { "Catalog Sync was triggered" }), ActivityTypeEnum.SYNC_TRIGGERED.name());
                            }
                            runCatalogSync(channel, channelCatalogSyncStatus, syncChannelCatalogEvent.getSyncChannelCatalogRequest().getChannelProductIdentifiers(), null);
                        }
                    } else {
                        LOG.warn("Ignoring SyncChannelCatalogEvent as catalog sync is already running");
                    }
                }
            }
        } finally {
            if (acquired) {
                catalogSyncRunningLock.unlock();
            }
        }
    }

    @Override
    public void resume(final AsyncScriptInstruction task) {
        executorFactory.getExecutor(SERVICE_NAME).submit(new Runnable() {
            @Override
            public void run() {
                String channelCode = null;
                try {
                    for (AsyncScriptInstructionParameter param : task.getInstructionParameters()) {
                        if (param.getName().equals("channel")) {
                            channelCode = (String) param.getValue();
                            break;
                        }
                    }
                    if (channelCode != null) {
                        Channel channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelByCode(channelCode);
                        ChannelCatalogSyncStatusDTO channelCatalogSyncStatus = channelService.getChannelCatalogSyncStatus(channel.getCode());
                        java.util.concurrent.locks.Lock catalogSyncRunningLock = lockingService.getLock(Namespace.CHANNEL_CATALOG_SYNC_DO, channel.getCode());
                        boolean acquired = false;
                        try {
                            acquired = catalogSyncRunningLock.tryLock();
                            if (!acquired) {
                                LOG.warn("Failed to obtain {} lock for channel {}. Sync is already running.", Namespace.CHANNEL_CATALOG_SYNC_DO.name(), channel.getCode());
                            } else {
                                runCatalogSync(channel, channelCatalogSyncStatus, null, task);
                            }
                        } finally {
                            if (acquired) {
                                catalogSyncRunningLock.unlock();
                            }
                        }
                    }
                } catch (Throwable th) {
                    LOG.error("Error running AsyncScriptInstruction: " + task, th);
                }
            }
        });
    }

    private void runCatalogSync(Channel channel, ChannelCatalogSyncStatusDTO channelCatalogSyncStatus, List<WsChannelProductIdentifiers> channelProductIdentifiers,
            AsyncScriptInstruction task) {
        LOG.info("Starting catalog sync for channel {}", channel.getName());
        Long lastCountOnChannel = channelCatalogSyncStatus.getLastCountOnChannel();
        channelCatalogSyncStatus.reset();
        channelCatalogSyncStatus.setLastSyncTime(DateUtils.getCurrentTime());
        channelCatalogSyncStatus.setSyncExecutionStatus(Channel.SyncExecutionStatus.RUNNING);
        channelCatalogSyncStatus.setMessage("Sync started");
        channelService.updateChannelCatalogSyncStatus(channelCatalogSyncStatus);

        Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(channel.getSourceCode());
        try {
            ChannelServiceImpl.ChannelGoodToSyncResponse channelGoodToSyncResponse = channelService.isChannelGoodToSync(channel.getCode(), ChannelServiceImpl.Sync.CATALOG);
            if (!channelGoodToSyncResponse.isGoodToSync()) {
                channelCatalogSyncStatus.setMessage("Connector is broken");
                channelCatalogSyncStatus.setSyncExecutionStatus(Channel.SyncExecutionStatus.IDLE);
                LOG.warn("Connector broken for channel {}", channel.getName());
            } else {
                CatalogSyncPreProcessorResponse catalogSyncPreProcessorResponse = runCatalogSyncPreprocessor(channel, channelGoodToSyncResponse.getParams(), task);
                if (catalogSyncPreProcessorResponse.isPreProcessingPending()) {
                    LOG.info("Pre-processing pending for channel: {}, will continue in next run", channel.getName());
                } else if (lastCountOnChannel != null && catalogSyncPreProcessorResponse.getTotalCountOnChannel() != null
                        && lastCountOnChannel.longValue() == catalogSyncPreProcessorResponse.getTotalCountOnChannel().longValue()) {
                    LOG.info("Total product count matches the previous count, aborting run", channel.getName());
                    channelCatalogSyncStatus.setMessage("Total product count matches the previous count, aborting run");
                    channelCatalogSyncStatus.setSyncExecutionStatus(Channel.SyncExecutionStatus.IDLE);
                } else {
                    LOG.info("Pre-processing complete for channel: {}, Params: {}", channel.getName(), catalogSyncPreProcessorResponse.getParams());
                    String syncId = UUID.randomUUID().toString();
                    channelCatalogSyncStatus.setSyncId(syncId);
                    channelService.updateChannelCatalogSyncStatus(channelCatalogSyncStatus);
                    doRunCatalogSync(catalogSyncPreProcessorResponse.getTotalCountOnChannel(), channel, channelCatalogSyncStatus, channelProductIdentifiers,
                            channelGoodToSyncResponse.getParams(), catalogSyncPreProcessorResponse.getParams(), syncId);
                }
            }
        } catch (Throwable e) {
            LOG.error("Error executing catalog sync task for channel: {}, Message: {}", source.getName(), e.getMessage());
            channelCatalogSyncStatus.setMessage(e.getMessage());
            channelCatalogSyncStatus.setSyncExecutionStatus(Channel.SyncExecutionStatus.IDLE);
            channelCatalogSyncStatus.setSuccessful(false);
        } finally {
            channelCatalogSyncStatus.setLastSyncTime(DateUtils.getCurrentTime());
            //            channelCatalogSyncStatus.setSyncExecutionStatus(Channel.SyncExecutionStatus.IDLE);
            channelService.updateChannelCatalogSyncStatus(channelCatalogSyncStatus);
        }
        LOG.info("Completed catalog sync for channel {}", channel.getName());
    }

    private void doRunCatalogSync(Long preprocessorCount, Channel channel, ChannelCatalogSyncStatusDTO channelCatalogSyncStatus,
            List<WsChannelProductIdentifiers> channelProductIdentifiers, Map<String, String> channelParams, Map<String, Object> preprocessorParams, String syncId) {
        int successfulImportCount = 0, failedImportCount = 0;
        Integer pageNumber = 1;
        boolean ignoreException = true;
        GetCatalogSyncResponse response;
        do {
            LOG.info("Importing page: {}", pageNumber);
            response = getChannelItemTypeList(channel, channelParams, pageNumber, preprocessorParams, channelProductIdentifiers);
            channelCatalogSyncStatus.setMileStone("Importing page: ", pageNumber);
            channelCatalogSyncStatus.setTotalMileStones(response.getTotalPages());
            channelService.updateChannelCatalogSyncStatus(channelCatalogSyncStatus);
            for (XMLParser.Element channelItemTypeElement : response.getChannelItemTypeElements()) {
                ignoreException = true;
                Map<String, Object> channelItemTypeToDetail = response.getChannelSkuCodeToChannelItemTypeDetail();
                String channelProductId = channelItemTypeElement.text();
                CreateChannelItemTypeRequest request;
                try {
                    LOG.info("Fetching detail for channelProductId {}", channelProductId);
                    request = getChannelItemType(channel, channelParams, channelItemTypeElement, channelItemTypeToDetail.get(channelProductId), preprocessorParams);
                    request.getChannelItemType().setVerified(true);
                    request.setSyncId(syncId);
                    LOG.info("Importing channel item type {}", request.getChannelItemType());
                    CreateChannelItemTypeResponse itemTypeResponse = channelCatalogService.createChannelItemType(request);
                    if (itemTypeResponse.isSuccessful()) {
                        successfulImportCount++;
                    } else {
                        LOG.error("Updating Catalog sync id {} of failed cit {}", syncId, channelProductId);
                        updateCatalogSyncId(channelProductId, channel, syncId);
                        failedImportCount++;
                        LOG.error("Error creating channel item type - {}. Error - {}", channelProductId, itemTypeResponse.getErrors());
                    }
                } catch (Throwable e) {
                    LOG.error("Updating Catalog sync id {} of erroneous cit {}", syncId, channelProductId);
                    updateCatalogSyncId(channelProductId, channel, syncId);
                    LOG.error("Error get channel item type {} , detail- {} ", channelProductId, e.getStackTrace());
                    //                    e.printStackTrace();
                    failedImportCount++;
                    // To handle the cases where totalPages = currentPage + 1. So catalog sync will pause
                    // in middle with exception and we should not disable stale catalog.
                    ignoreException = false;
                }
                channelCatalogSyncStatus.setSuccessfulImports(successfulImportCount);
                channelCatalogSyncStatus.setFailedImports(failedImportCount);
                if (failedImportCount == 0) {
                    channelCatalogSyncStatus.setLastCountOnChannel(preprocessorCount);
                }
                channelService.updateChannelCatalogSyncStatus(channelCatalogSyncStatus);
            }
            pageNumber++;
        } while (response.isSuccessful() && response.isHasMoreResults());
        updateStatus(channelCatalogSyncStatus, channel, successfulImportCount, failedImportCount);
        Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(channel.getSourceCode());
        boolean disableCatalogConfigured = source.isAutoDisableStaleCatalog()
                && ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).isAutoDisableStaleCatalog();
        if (disableCatalogConfigured && ignoreException) { //
            int disabledCount = channelCatalogService.disableStaleCatalog(syncId, channel.getId());
            if (disabledCount > 0) {
                LOG.info("{} channel item types disabled for channel {} after catalog sync", disabledCount, channel.getCode());
            }
        } else {
            LOG.info("Not disabling stale catalog as property is off");
        }
    }

    @Override
    public void updateCatalogSyncId(String channelProductId, Channel channel, String syncId) {
        ChannelItemType channelItemType = channelCatalogService.getChannelItemTypeByChannelAndChannelProductId(channel.getId(), channelProductId);
        if (channelItemType != null) {
            channelItemType.setCatalogSyncId(syncId);
            channelCatalogService.updateChannelItemType(channelItemType);
        }
    }

    private CreateChannelItemTypeRequest getChannelItemType(Channel channel, Map<String, String> channelParams, XMLParser.Element channelItemTypeElement,
            Object channelItemTypeDetail, Map<String, Object> metadata) throws JiBXException, IOException {
        ScriptExecutionContext context = ScriptExecutionContext.current();
        Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(channel.getSourceCode());
        ScraperScript channelItemTypeDetailsScript = CacheManager.getInstance().getCache(ChannelCache.class).getScriptByName(channel.getCode(),
                Source.CHANNEL_ITEM_TYPE_DETAILS_SCRIPT_NAME);
        context.addVariable("source", source);
        context.addVariable("channel", channel);
        context.addVariable("channelItemTypeElement", channelItemTypeElement);
        context.addVariable("channelItemTypeDetail", channelItemTypeDetail);
        context.addVariable("metadata", metadata);

        for (Map.Entry<String, String> e : channelParams.entrySet()) {
            context.addVariable(e.getKey(), e.getValue());
        }
        try {
            context.setScriptProvider(new IScriptProvider() {
                @Override
                public ScraperScript getScript(String scriptName) {
                    return CacheManager.getInstance().getCache(ScriptVersionedCache.class).getScriptByName(scriptName);
                }
            });
            context.setTraceLoggingEnabled(UserContext.current().isTraceLoggingEnabled());
            channelItemTypeDetailsScript.execute();
            String channelItemTypeXml = context.getScriptOutput();
            LOG.info(channelItemTypeXml);
            return JibxUtils.unmarshall("unicommerceServicesBinding19", CreateChannelItemTypeRequest.class, channelItemTypeXml);
        } finally {
            ScriptExecutionContext.destroy();
        }
    }

    private GetCatalogSyncResponse getChannelItemTypeList(Channel channel, Map<String, String> channelParams, Integer pageNumber, Map<String, Object> preprocessorParams,
            List<WsChannelProductIdentifiers> channelProductIdentifiers) {
        Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(channel.getSourceCode());
        ScraperScript channelItemTypeListScript = CacheManager.getInstance().getCache(ChannelCache.class).getScriptByName(channel.getCode(),
                Source.CHANNEL_ITEM_TYPE_LIST_SCRIPT_NAME);
        GetCatalogSyncResponse response = new GetCatalogSyncResponse();
        ScriptExecutionContext context = ScriptExecutionContext.current();
        Map<String, Object> resultItems = new HashMap<>();
        context.addVariable("resultItems", resultItems);
        context.addVariable("source", source);
        context.addVariable("channel", channel);
        context.addVariable("pageNumber", pageNumber);
        context.addVariable("preprocessorParams", preprocessorParams);
        context.addVariable("channelProductIdentifiers", channelProductIdentifiers);
        for (Map.Entry<String, String> e : channelParams.entrySet()) {
            context.addVariable(e.getKey(), e.getValue());
        }
        try {
            context.setScriptProvider(new IScriptProvider() {
                @Override
                public ScraperScript getScript(String scriptName) {
                    return CacheManager.getInstance().getCache(ScriptVersionedCache.class).getScriptByName(scriptName);
                }
            });
            context.setTraceLoggingEnabled(UserContext.current().isTraceLoggingEnabled());
            channelItemTypeListScript.execute();
            String channelItemTypesXml = context.getScriptOutput();
            if (StringUtils.isNotBlank(channelItemTypesXml)) {
                List<XMLParser.Element> channelItemTypeElements = new ArrayList<>();
                XMLParser.Element rootElement = XMLParser.parse(channelItemTypesXml);
                Integer totalPages = Integer.parseInt(rootElement.text(TOTAL_PAGES));
                response.setHasMoreResults(totalPages > pageNumber);
                response.setTotalPages(totalPages);
                for (XMLParser.Element channelItemTypeElement : rootElement.list(CHANNEL_ITEM_TYPE)) {
                    channelItemTypeElements.add(channelItemTypeElement);
                }
                response.setSuccessful(true);
                response.setChannelItemTypeElements(channelItemTypeElements);
                response.setChannelSkuCodeToChannelItemTypeDetail(resultItems);
            } else {
                response.setMessage("No item types found for channel: " + channel);
            }
        } finally {
            ScriptExecutionContext.destroy();
        }
        return response;
    }

    private CatalogSyncPreProcessorResponse runCatalogSyncPreprocessor(Channel channel, Map<String, String> channelParams, AsyncScriptInstruction asyncScriptInstruction) {
        CatalogSyncPreProcessorResponse catalogSyncPreProcessorResponse = new CatalogSyncPreProcessorResponse();
        if (StringUtils.isNotBlank(CacheManager.getInstance().getCache(ChannelCache.class).getScriptName(channel.getCode(),
                com.uniware.core.entity.Source.CHANNEL_CATALOG_SYNC_PREPROCESSOR_SCRIPT_NAME))) {
            ScraperScript channelCatalogSyncPreprocessorScript = CacheManager.getInstance().getCache(ChannelCache.class).getScriptByName(channel.getCode(),
                    com.uniware.core.entity.Source.CHANNEL_CATALOG_SYNC_PREPROCESSOR_SCRIPT_NAME);
            if (channelCatalogSyncPreprocessorScript != null) {
                Map<String, Object> resultItems = new HashMap<>();
                ScriptExecutionContext context = ScriptExecutionContext.current();
                String instructionName = null;
                if (asyncScriptInstruction != null) {
                    instructionName = asyncScriptInstruction.getInstructionName();
                    for (AsyncScriptInstructionParameter param : asyncScriptInstruction.getInstructionParameters()) {
                        context.addVariable(param.getName(), param.getValue());
                    }
                }
                context.addVariable("resultItems", resultItems);
                context.addVariable("channel", channel);
                context.addVariable("new", true);
                for (Map.Entry<String, String> e : channelParams.entrySet()) {
                    context.addVariable(e.getKey(), e.getValue());
                }
                context.setScriptProvider(new IScriptProvider() {
                    @Override
                    public ScraperScript getScript(String scriptName) {
                        return CacheManager.getInstance().getCache(ScriptVersionedCache.class).getScriptByName(scriptName);
                    }
                });
                context.setTraceLoggingEnabled(UserContext.current().isTraceLoggingEnabled());
                try {
                    LOG.info("Running catalog sync preprocessor for channel {}, instructionName {}", channel.getName(), instructionName);
                    channelCatalogSyncPreprocessorScript.execute(instructionName);
                    if (!context.getScheduleScriptContext().isEmpty()) {
                        AsyncScriptInstruction existingAsyncScriptInstruction = asyncScriptTaskService.getPendingAsyncScriptInstruction("channel", channel.getCode(),
                                this.getClass().getName());
                        if (existingAsyncScriptInstruction == null) {
                            asyncScriptTaskService.scheduleScriptTask(channelCatalogSyncPreprocessorScript.getScriptName(), channel, context, this.getClass(),
                                    ChannelCatalogSyncTask.class.getName());
                        } else {
                            LOG.info("Skipping catalog async instruction for channel {} as already scheduled", channel.getCode());
                        }
                        catalogSyncPreProcessorResponse.setPreProcessingPending(true);
                    } else {
                        LOG.info("Pre-processing complete, resultItems: {}", resultItems);
                        catalogSyncPreProcessorResponse.setParams(resultItems);
                        if (resultItems.containsKey("totalCountOnChannel")) {
                            catalogSyncPreProcessorResponse.setTotalCountOnChannel((Long) resultItems.get("totalCountOnChannel"));
                        }
                    }
                    LOG.info("Completed catalog sync preprocessor for channel {}", channel.getName());
                } finally {
                    ScriptExecutionContext.destroy();
                }
            }
        }
        return catalogSyncPreProcessorResponse;
    }

    private void updateStatus(ChannelCatalogSyncStatusDTO channelCatalogSyncStatus, Channel channel, int successfulImportCount, int failedImportCount) {
        Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(channel.getSourceCode());
        String message = "Successful imports:" + successfulImportCount + ", failed imports:" + failedImportCount + ". Task Completed";
        channelCatalogSyncStatus.setSuccessful(failedImportCount == 0 ? true : false);
        LOG.info("Channel: {}, Result: {}", source.getName(), message);
        if ((failedImportCount == 0 && channelCatalogSyncStatus.getFailedImports() > 0)
                || (failedImportCount > 0 && (channelCatalogSyncStatus.getFailedImports() == 0 || channelCatalogSyncStatus.getLastSyncFailedNotificationTime() == null
                        || DateUtils.diff(DateUtils.getCurrentTime(), channelCatalogSyncStatus.getLastSyncFailedNotificationTime(), DateUtils.Resolution.HOUR) > 3))) {
            sendSyncFailedNotification(channelCatalogSyncStatus, channel);
            channelCatalogSyncStatus.setLastSyncFailedNotificationTime(DateUtils.getCurrentTime());
        }
        channelCatalogSyncStatus.setLastSyncTime(DateUtils.getCurrentTime());
        channelCatalogSyncStatus.setSuccessfulImports(successfulImportCount);
        channelCatalogSyncStatus.setFailedImports(failedImportCount);
        channelCatalogSyncStatus.setMessage(message);
        channelCatalogSyncStatus.setSyncExecutionStatus(Channel.SyncExecutionStatus.IDLE);
        LOG.info("Catalog sync for channel {} completed with {}", channel.getName(), message);
        channelService.updateChannelCatalogSyncStatus(channelCatalogSyncStatus);
    }

    private void sendSyncFailedNotification(ChannelCatalogSyncStatusDTO channelResultStatus, Channel channel) {
        EmailMessage message = new EmailMessage(Constants.EmailTemplateType.CHANNEL_SYNC_FAILED_NOTIFICATION.name());
        message.addTemplateParam("channelStatus", channelResultStatus);
        message.addTemplateParam("channel", channel);
        emailService.send(message);
    }

    private static class CatalogSyncPreProcessorResponse {
        private boolean             preProcessingPending;
        private Long                totalCountOnChannel;
        private Map<String, Object> params;

        public boolean isPreProcessingPending() {
            return preProcessingPending;
        }

        public void setPreProcessingPending(boolean preProcessingPending) {
            this.preProcessingPending = preProcessingPending;
        }

        public Long getTotalCountOnChannel() {
            return totalCountOnChannel;
        }

        public void setTotalCountOnChannel(Long totalCountOnChannel) {
            this.totalCountOnChannel = totalCountOnChannel;
        }

        public Map<String, Object> getParams() {
            return params;
        }

        public void setParams(Map<String, Object> params) {
            this.params = params;
        }

    }

}
