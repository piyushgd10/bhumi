/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 29-Sep-2014
 *  @author parijat
 */
package com.uniware.services.channel;

import com.unifier.core.entity.AsyncScriptInstruction;
import com.uniware.core.api.channel.ChannelReconciliationInvoiceSyncStatusVO;
import com.uniware.core.api.channel.GetChannelReconciliationSyncStatusRequest;
import com.uniware.core.api.channel.GetChannelReconciliationSyncStatusResponse;
import com.uniware.core.api.channel.SyncChannelReconciliationInvoiceRequest;
import com.uniware.core.api.channel.SyncChannelReconciliationInvoiceResponse;
import com.uniware.services.messaging.jms.SyncReconciliationInvoiceEvent;

public interface IChannelReconciliationInvoiceSyncService {

    void syncChannelReconciliationInvoices(SyncReconciliationInvoiceEvent syncReconciliationInvoiceEvent);

    SyncChannelReconciliationInvoiceResponse syncChannelReconciliationInvoices(SyncChannelReconciliationInvoiceRequest request);

    void resume(AsyncScriptInstruction task);

    void updateChannelReconciliationInvoiceSyncStatus(ChannelReconciliationInvoiceSyncStatusVO channelStatus);

    GetChannelReconciliationSyncStatusResponse getChannelCatalogSyncStatus(GetChannelReconciliationSyncStatusRequest request);

}
