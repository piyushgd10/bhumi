/*
*  Copyright 2015 Unicommerce eSolutions (P) Limited . All Rights Reserved.
*  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
*  
*  @version     1.0, 18/11/15
*  @author sunny
*/

package com.uniware.services.channel.saleorder.status.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.StringUtils;
import com.unifier.core.utils.XMLParser;
import com.unifier.services.users.IUsersService;
import com.uniware.core.api.saleorder.MarkSaleOrderItemsCompleteRequest;
import com.uniware.core.api.saleorder.MarkSaleOrderItemsCompleteResponse;
import com.uniware.core.api.shipping.UpdateShippingPackageTrackingStatusRequest;
import com.uniware.core.api.shipping.UpdateShippingPackageTrackingStatusResponse;
import com.uniware.core.entity.SaleOrder;
import com.uniware.core.entity.SaleOrderItem;
import com.uniware.core.entity.ShippingPackage;
import com.uniware.core.utils.ActivityContext;
import com.uniware.core.utils.Constants;
import com.uniware.services.audit.impl.ActivityEntityEnum;
import com.uniware.services.audit.impl.ActivityTypeEnum;
import com.uniware.services.audit.impl.ActivityUtils;
import com.uniware.services.channel.saleorder.status.event.ChannelOrderStatusChangeEvent;
import com.uniware.services.channel.saleorder.status.IChannelOrderStatusChangeEventHandler;
import com.uniware.services.channel.saleorder.status.IChannelOrderStatusSyncService;
import com.uniware.services.saleorder.ISaleOrderService;
import com.uniware.services.shipping.IShippingService;

@Component
public class ChannelOrderDispatchedEventHandler implements IChannelOrderStatusChangeEventHandler {

    private static final Logger            LOG = LoggerFactory.getLogger(ChannelOrderDispatchedEventHandler.class);

    @Autowired
    private ISaleOrderService              saleOrderService;

    @Autowired
    private IUsersService                  usersService;

    @Autowired
    private IShippingService               shippingService;

    @Autowired
    private IChannelOrderStatusSyncService channelOrderStatusSyncService;

    @PostConstruct
    public void init() {
        channelOrderStatusSyncService.registerEventHandler(ChannelOrderStatusChangeEvent.Type.DISPATCHED, this);
        channelOrderStatusSyncService.registerEventHandler(ChannelOrderStatusChangeEvent.Type.DELIVERED, this);
    }

    @Override
    @Transactional
    public void handle(ChannelOrderStatusChangeEvent event) {
        switch (event.getType()) {
            case DISPATCHED:
                handleDispatch(event);
                break;
            case DELIVERED:
                handleDispatch(event);
                handleDelivery(event);
                break;
            default:
                break;
        }
    }

    private void handleDispatch(ChannelOrderStatusChangeEvent event) {
        String saleOrderCode = event.getSaleOrderCode();
        List<String> dispatchedSaleOrderItemCodes = new ArrayList<>(event.getSaleOrderItems().size());
        for (ChannelOrderStatusChangeEvent.WsSaleOrderItem wsSaleOrderItem : event.getSaleOrderItems()) {
            String saleOrderItemCode = wsSaleOrderItem.getCode();
            SaleOrderItem saleOrderItem = saleOrderService.getSaleOrderItemByCode(saleOrderCode, saleOrderItemCode);
            if (StringUtils.equalsAny(saleOrderItem.getStatusCode(), SaleOrderItem.StatusCode.CREATED.name(), SaleOrderItem.StatusCode.FULFILLABLE.name(),
                    SaleOrderItem.StatusCode.UNFULFILLABLE.name(), SaleOrderItem.StatusCode.MANIFESTED.name())) {
                dispatchedSaleOrderItemCodes.add(saleOrderItemCode);
            }
        }

        // Mark sale order items dispatched
        if (!dispatchedSaleOrderItemCodes.isEmpty()) {
            MarkSaleOrderItemsCompleteRequest markSaleOrderItemsCompleteRequest = new MarkSaleOrderItemsCompleteRequest();
            markSaleOrderItemsCompleteRequest.setSaleOrderCode(saleOrderCode);
            markSaleOrderItemsCompleteRequest.setSaleOrderItemCodes(dispatchedSaleOrderItemCodes);
            markSaleOrderItemsCompleteRequest.setUserId(usersService.getUserByUsername(Constants.SYSTEM_USER_EMAIL).getId());
            MarkSaleOrderItemsCompleteResponse markSaleOrderItemsCompleteResponse = saleOrderService.markSaleOrderItemsComplete(markSaleOrderItemsCompleteRequest);
            if (!markSaleOrderItemsCompleteResponse.isSuccessful()) {
                LOG.warn("Unable to mark saleOrder {} complete, error {}", saleOrderCode, markSaleOrderItemsCompleteResponse.getErrors().toString());
            } else if (ActivityContext.current().isEnable()) {
                ActivityUtils.appendActivity(saleOrderCode, ActivityEntityEnum.SALE_ORDER.getName(), saleOrderCode,
                        Arrays.asList(new String[] { "Order Item Codes : " + markSaleOrderItemsCompleteRequest.getSaleOrderItemCodes().toString() + " successfully marked dispatched automatically" }),
                        ActivityTypeEnum.SYNC_TRIGGERED.name());
            }
        }
    }

    private void handleDelivery(ChannelOrderStatusChangeEvent event) {
        String saleOrderCode = event.getSaleOrderCode();
        SaleOrder saleOrder = saleOrderService.getSaleOrderForUpdate(saleOrderCode);
        Map<String, SaleOrderItem> codeToSaleOrderItem = new HashMap<>(saleOrder.getSaleOrderItems().size());
        for (SaleOrderItem soi : saleOrder.getSaleOrderItems()) {
            codeToSaleOrderItem.put(soi.getCode(), soi);
        }
        Map<String, Date> deliveredSaleOrderItems = new HashMap<>();
        for (ChannelOrderStatusChangeEvent.WsSaleOrderItem wsSaleOrderItem : event.getSaleOrderItems()) {
            String saleOrderItemCode = wsSaleOrderItem.getCode();
            SaleOrderItem saleOrderItem = codeToSaleOrderItem.get(saleOrderItemCode);
            if (!SaleOrderItem.StatusCode.DELIVERED.name().equals(saleOrderItem.getStatusCode())) {
                String deliveryDateStr = wsSaleOrderItem.getAttributes().get("StatusDate");
                Date deliveryDate = StringUtils.isNotBlank(deliveryDateStr) ? DateUtils.stringToDate(deliveryDateStr, "yyyy-MM-dd") : DateUtils.getCurrentTime();
                deliveredSaleOrderItems.put(saleOrderItemCode, deliveryDate);
            }
        }

        Set<String> deliveredShipments = new HashSet<>();
        for (String saleOrderItemCode : deliveredSaleOrderItems.keySet()) {
            SaleOrderItem saleOrderItem = codeToSaleOrderItem.get(saleOrderItemCode);
            ShippingPackage shippingPackage = saleOrderItem.getShippingPackage();
            if (shippingPackage != null && ShippingPackage.StatusCode.DISPATCHED.name().equals(shippingPackage.getStatusCode())) {
                if (!deliveredShipments.contains(shippingPackage.getCode())) {
                    UpdateShippingPackageTrackingStatusRequest request = new UpdateShippingPackageTrackingStatusRequest();
                    request.setShippingPackageCode(shippingPackage.getCode());
                    request.setShippingProviderCode(shippingPackage.getShippingProviderCode());
                    request.setTrackingNumber(shippingPackage.getTrackingNumber());
                    request.setStatusCode(ShippingPackage.StatusCode.DELIVERED.name());
                    request.setStatusDate(deliveredSaleOrderItems.get(saleOrderItemCode));
                    request.setProviderStatusCode(ShippingPackage.StatusCode.DELIVERED.name());
                    request.setDisableStatusUpdate(true);
                    LOG.info("Marking package {} delivered", shippingPackage.getCode());
                    UpdateShippingPackageTrackingStatusResponse response = shippingService.updateShippingPackageTrackingStatus(request);
                    if (response.isSuccessful()) {
                        LOG.info("Successfully marked package {} delivered", shippingPackage.getCode());
                        if (ActivityContext.current().isEnable()) {
                            ActivityUtils.appendActivity(saleOrderCode, ActivityEntityEnum.SALE_ORDER.getName(), saleOrderCode,
                                    Arrays.asList(new String[] { "Order Item Code : " + saleOrderItemCode + " successfully marked delivered" }),
                                    ActivityTypeEnum.SYNC_TRIGGERED.name());
                        }
                    } else {
                        LOG.warn("Unable to mark package {} delivered, error {}", shippingPackage.getCode(), response.getErrors().toString());
                    }
                    deliveredShipments.add(saleOrderItem.getShippingPackage().getCode());
                }
            }
        }
    }
}
