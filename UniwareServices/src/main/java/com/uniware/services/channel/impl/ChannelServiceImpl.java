/*
 *  Copyright 2013 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 18-Apr-2013
 *  @author unicom
 */
package com.uniware.services.channel.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.uniware.core.api.channel.ChannelAssociatedFacilityDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.core.convert.support.DefaultConversionService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.unifier.core.annotation.audit.LogActivity;
import com.unifier.core.api.application.SystemConfigurationDTO;
import com.unifier.core.api.audit.FetchEntityActivityRequest;
import com.unifier.core.api.audit.FetchEntityActivityResponse;
import com.unifier.core.api.validation.ResponseCode;
import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.api.validation.WsError;
import com.unifier.core.api.validation.WsWarning;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.entity.ChannelProperty;
import com.unifier.core.expressions.Expression;
import com.unifier.core.utils.CollectionUtils;
import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.EncryptionUtils;
import com.unifier.core.utils.StringUtils;
import com.unifier.core.utils.XMLParser;
import com.unifier.core.utils.XMLParser.Element;
import com.unifier.scraper.redirection.Form;
import com.unifier.scraper.sl.exception.ScriptExecutionException;
import com.unifier.scraper.sl.runtime.IScriptProvider;
import com.unifier.scraper.sl.runtime.ScraperScript;
import com.unifier.scraper.sl.runtime.ScriptExecutionContext;
import com.unifier.services.aspect.MarkDirty;
import com.unifier.services.aspect.RollbackOnFailure;
import com.unifier.services.utils.CustomFieldUtils;
import com.uniware.core.api.channel.AddChannelConnectorRequest;
import com.uniware.core.api.channel.AddChannelConnectorResponse;
import com.uniware.core.api.channel.AddChannelRequest;
import com.uniware.core.api.channel.AddChannelResponse;
import com.uniware.core.api.channel.ChangeChannelInventorySyncStatusRequest;
import com.uniware.core.api.channel.ChangeChannelInventorySyncStatusResponse;
import com.uniware.core.api.channel.ChangeChannelOrderSyncStatusRequest;
import com.uniware.core.api.channel.ChangeChannelOrderSyncStatusResponse;
import com.uniware.core.api.channel.ChangeChannelPricingSyncStatusRequest;
import com.uniware.core.api.channel.ChangeChannelPricingSyncStatusResponse;
import com.uniware.core.api.channel.ChangeChannelReconciliationSyncStatusRequest;
import com.uniware.core.api.channel.ChangeChannelReconciliationSyncStatusResponse;
import com.uniware.core.api.channel.ChannelCatalogSyncStatusDTO;
import com.uniware.core.api.channel.ChannelConfigurationParameterDTO;
import com.uniware.core.api.channel.ChannelConnectorDTO;
import com.uniware.core.api.channel.ChannelDetailDTO;
import com.uniware.core.api.channel.ChannelInventorySyncStatusDTO;
import com.uniware.core.api.channel.ChannelOrderStatusDTO;
import com.uniware.core.api.channel.ChannelOrderSyncStatusDTO;
import com.uniware.core.api.channel.ChannelPricingPushStatusDTO;
import com.uniware.core.api.channel.ChannelReconciliationInvoiceSyncStatusVO;
import com.uniware.core.api.channel.ChannelSaleOrderImportDTO;
import com.uniware.core.api.channel.ChannelSyncAttributesDTO;
import com.uniware.core.api.channel.ChannelSyncDTO;
import com.uniware.core.api.channel.EditChannelAssociatedFacilityRequest;
import com.uniware.core.api.channel.EditChannelAssociatedFacilityResponse;
import com.uniware.core.api.channel.EditChannelRequest;
import com.uniware.core.api.channel.EditChannelResponse;
import com.uniware.core.api.channel.GetChannelActivitiesRequest;
import com.uniware.core.api.channel.GetChannelActivitiesResponse;
import com.uniware.core.api.channel.GetChannelDetailsRequest;
import com.uniware.core.api.channel.GetChannelDetailsResponse;
import com.uniware.core.api.channel.GetChannelOrderSummaryRequest;
import com.uniware.core.api.channel.GetChannelOrderSummaryResponse;
import com.uniware.core.api.channel.GetChannelProductSummaryRequest;
import com.uniware.core.api.channel.GetChannelProductSummaryResponse;
import com.uniware.core.api.channel.GetChannelSyncAttributesRequest;
import com.uniware.core.api.channel.GetChannelSyncAttributesResponse;
import com.uniware.core.api.channel.GetChannelSyncRequest;
import com.uniware.core.api.channel.GetChannelSyncResponse;
import com.uniware.core.api.channel.GetChannelsForSaleOrderImportRequest;
import com.uniware.core.api.channel.GetChannelsForSaleOrderImportResponse;
import com.uniware.core.api.channel.GetConnectorStatusResponse;
import com.uniware.core.api.channel.GetConnectorStatusResponse.ChannelConnectorStatus;
import com.uniware.core.api.channel.GoToChannelLandingPageRequest;
import com.uniware.core.api.channel.GoToChannelLandingPageResponse;
import com.uniware.core.api.channel.PostConfigureChannelConnectorRequest;
import com.uniware.core.api.channel.PostConfigureChannelConnectorResponse;
import com.uniware.core.api.channel.PreConfigureChannelConnectorRequest;
import com.uniware.core.api.channel.PreConfigureChannelConnectorResponse;
import com.uniware.core.api.channel.SourceConfigurationParameterDTO;
import com.uniware.core.api.channel.SourceDetailDTO;
import com.uniware.core.api.channel.SourceSearchDTO;
import com.uniware.core.api.channel.SyncChannelCatalogRequest;
import com.uniware.core.api.channel.SyncChannelOrdersRequest;
import com.uniware.core.api.channel.TestChannelInventoryUpdateFormulaRequest;
import com.uniware.core.api.channel.TestChannelInventoryUpdateFormulaResponse;
import com.uniware.core.api.channel.VerifyAndSyncParamsResponse;
import com.uniware.core.api.channel.WsChannel;
import com.uniware.core.api.channel.WsChannelConfigurationParameter;
import com.uniware.core.api.channel.WsChannelConnector;
import com.uniware.core.api.channel.WsChannelConnectorParameter;
import com.uniware.core.api.facility.CreateFacilityAllocationRuleRequest;
import com.uniware.core.api.facility.CreateFacilityAllocationRuleResponse;
import com.uniware.core.api.facility.CreateFacilityRequest;
import com.uniware.core.api.facility.CreateFacilityResponse;
import com.uniware.core.api.inventory.CalculateChannelItemTypeInventoryResponse;
import com.uniware.core.api.inventory.SearchInventorySnapshotResponse.InventorySnapshotDTO;
import com.uniware.core.api.party.WsFacility;
import com.uniware.core.api.party.WsPartyAddress;
import com.uniware.core.api.saleorder.display.GetSaleOrderActivitiesResponse.ActivityDTO;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.cache.FacilityCache;
import com.uniware.core.entity.BillingParty;
import com.uniware.core.entity.Channel;
import com.uniware.core.entity.Channel.SyncStatus;
import com.uniware.core.entity.ChannelAssociatedFacility;
import com.uniware.core.entity.ChannelConfigurationParameter;
import com.uniware.core.entity.ChannelConnector;
import com.uniware.core.entity.ChannelConnector.Status;
import com.uniware.core.entity.ChannelConnectorParameter;
import com.uniware.core.entity.ChannelItemType;
import com.uniware.core.entity.ChannelItemType.ListingStatus;
import com.uniware.core.entity.Customer;
import com.uniware.core.entity.Facility;
import com.uniware.core.entity.ItemType;
import com.uniware.core.entity.PartyAddressType;
import com.uniware.core.entity.SaleOrderItem;
import com.uniware.core.entity.Source;
import com.uniware.core.entity.SourceColor;
import com.uniware.core.entity.SourceConfigurationParameter;
import com.uniware.core.entity.SourceConfigurationParameter.Type;
import com.uniware.core.entity.SourceConnector;
import com.uniware.core.entity.SourceConnectorParameter;
import com.uniware.core.script.error.ChannelScriptError;
import com.uniware.core.utils.ActivityContext;
import com.uniware.core.utils.AuditContext;
import com.uniware.core.utils.UserContext;
import com.uniware.core.vo.ActivityMetaVO;
import com.uniware.core.vo.ChannelInventoryFormulaVO;
import com.uniware.core.vo.ChannelWarehouseInventorySyncStatusVO;
import com.uniware.core.vo.HttpProxyVO;
import com.uniware.core.vo.SourceSaleOrderImportInstructionsVO;
import com.uniware.dao.channel.IChannelDao;
import com.uniware.dao.channel.IChannelMao;
import com.uniware.dao.channel.ISourceMao;
import com.uniware.events.BillingPartyDisableEvent;
import com.uniware.services.audit.IActivityLogService;
import com.uniware.services.audit.impl.ActivityEntityEnum;
import com.uniware.services.audit.impl.ActivityTypeEnum;
import com.uniware.services.audit.impl.ActivityUtils;
import com.uniware.services.billing.party.IBillingPartyService;
import com.uniware.services.cache.ChannelCache;
import com.uniware.services.cache.ScriptVersionedCache;
import com.uniware.services.catalog.ICatalogService;
import com.uniware.services.channel.IChannelCatalogService;
import com.uniware.services.channel.IChannelCatalogSyncService;
import com.uniware.services.channel.IChannelInventorySyncService;
import com.uniware.services.channel.IChannelService;
import com.uniware.services.channel.saleorder.IChannelOrderSyncService;
import com.uniware.services.configuration.ExportJobConfiguration;
import com.uniware.services.configuration.SourceConfiguration;
import com.uniware.services.configuration.data.manager.ProductConfiguration;
import com.uniware.services.customer.ICustomerService;
import com.uniware.services.http.proxy.IHttpProxyService;
import com.uniware.services.inventory.IInventoryService;
import com.uniware.services.saleorder.ISaleOrderService;
import com.uniware.services.warehouse.IFacilityService;

import edu.umd.cs.findbugs.annotations.Nullable;

@Service("channelService")
public class ChannelServiceImpl implements IChannelService, ApplicationListener<BillingPartyDisableEvent> {

    private static final Logger LOG = LoggerFactory.getLogger(ChannelServiceImpl.class);

    public enum Sync {
        ORDER,
        INVENTORY,
        CATALOG,
        RECONCILIATION,
        CHANNEL_WAREHOUSE_INVENTORY
    }

    @Autowired
    private ApplicationContext              applicationContext;

    @Autowired
    private IChannelDao                     channelDao;

    @Autowired
    private IInventoryService               inventoryService;

    @Autowired
    private ICustomerService                customerService;

    @Autowired
    private IChannelMao                     channelMao;

    @Autowired
    private IBillingPartyService            billingPartyService;

    @Autowired
    private IChannelCatalogService          channelCatalogService;

    @Autowired
    private IChannelCatalogSyncService      channelCatalogSyncService;

    @Autowired
    private ICatalogService                 catalogService;

    @Autowired
    private ISaleOrderService               saleOrderService;

    @Autowired
    private IChannelInventorySyncService    channelInventorySyncService;

    @Autowired
    private IChannelOrderSyncService        channelOrderSyncService;

    @Autowired
    private IHttpProxyService               httpProxyService;

    @Autowired
    private ISourceMao                      sourceMao;

    @Autowired
    private IFacilityService                facilityService;

    @Autowired
    private IActivityLogService             activityLogService;

    private static final String             DEFAULT_SHORT_NAME                             = "DE";

    private static final String             INVENTORY_MANAGEMENT_SWITCHED_OFF              = "inventory.management.switched.off";

    private static final String             INVENTORY_MANAGEMENT_SWITCHED_OFF_DISPLAY_NAME = "Inventory Management switched off";

    private static final String             DUMMY_ADDRESS                                  = "ADDRESS";
    private static final String             DUMMY_CITY                                     = "Delhi";
    private static final String             DUMMY_STATE                                    = "DL";
    private static final String             DUMMY_COUNTRY                                  = "IN";
    private static final String             DUMMY_PIN                                      = "110088";
    private static final String             DUMMY_MOBILE                                   = "9999999999";

    private static final String             DEFAULT_ALLOCATION_RULE_NAME                   = "Default";
    private static final Integer            DEFAULT_ALLOCATION_RULE_PREFERENCE             = 1;
    private static final String             DEFAULT_ALLOCATION_RULE_EXPRESSION             = "#{true}";

    private static DefaultConversionService conversionService                              = new DefaultConversionService();

    @Override
    @Transactional(readOnly = true)
    public Channel getChannelByCode(String channelCode) {
        return CacheManager.getInstance().getCache(ChannelCache.class).getChannelByCode(channelCode.toUpperCase());
    }

    @Override
    @Transactional(readOnly = true)
    public Channel getChannelByName(String channelName) {
        return getChannelByCode(constructChannelCodeByName(channelName));
    }

    @Override
    @Transactional
    @MarkDirty(values = { ChannelCache.class, ExportJobConfiguration.class })
    @LogActivity
    @RollbackOnFailure
    public AddChannelResponse addChannel(AddChannelRequest request) {
        AddChannelResponse response = new AddChannelResponse();
        ValidationContext context = request.validate();
        String channelCode = constructChannelCodeByName(request.getWsChannel().getChannelName());
        Channel channel = getChannelByCode(channelCode);
        Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(request.getWsChannel().getSourceCode());
        Set<ChannelDetailDTO> channels = CacheManager.getInstance().getCache(ChannelCache.class).getChannels();
        Integer totalPermittedChannelCount = ConfigurationManager.getInstance().getConfiguration(ProductConfiguration.class).getTotalPermittedChannelsCount();
        Integer totalPermittedChannelCountForSource = ConfigurationManager.getInstance().getConfiguration(ProductConfiguration.class).getTotalPermittedChannelsCountPerSource();
        if (totalPermittedChannelCount != null && totalPermittedChannelCount <= channels.size()) {
            context.addError(WsResponseCode.CHANNEL_LIMIT_EXCEDED, "Channel limit reached.");
        }
        if (totalPermittedChannelCountForSource != null && totalPermittedChannelCountForSource <= getChannelCountBySourceCode(source.getCode())) {
            context.addError(WsResponseCode.CHANNEL_LIMIT_EXCEDED, "Channel limit reached for source" + source.getName());
        }
        if (!context.hasErrors()) {
            if (source == null) {
                context.addError(WsResponseCode.INVALID_CHANNEL_CODE, "Invalid Channel: " + request.getWsChannel().getSourceCode());
            } else if (channel != null) {
                context.addError(WsResponseCode.CHANNEL_ALREADY_ADDED, WsResponseCode.CHANNEL_ALREADY_ADDED.message() + ": " + request.getWsChannel().getChannelName());
            }
        }

        if (!context.hasErrors() && request.getWsChannel().getPackageType() != null) {
            if (!(request.getWsChannel().getPackageType().equals(SaleOrderItem.PackageType.FIXED.name())
                    || request.getWsChannel().getPackageType().equals(SaleOrderItem.PackageType.FLEXIBLE.name()))) {
                context.addError(WsResponseCode.INVALID_PACKAGE_SPECIFICATIONS, "Invalid value for package type");
            }
        }

        Customer customer = null;
        if (!context.hasErrors() && StringUtils.isNotBlank(request.getWsChannel().getCustomerCode())) {
            customer = customerService.getCustomerByCode(request.getWsChannel().getCustomerCode());
            if (customer == null) {
                context.addError(WsResponseCode.INVALID_CUSTOMER_CODE, "Invalid value for customer code");
            }
        }

        BillingParty billingParty = null;
        if (!context.hasErrors() && StringUtils.isNotBlank(request.getWsChannel().getBillingPartyCode())) {
            billingParty = billingPartyService.getBillingPartyByCode(request.getWsChannel().getBillingPartyCode());
            if (billingParty == null) {
                context.addError(WsResponseCode.INVALID_BILLING_PARTY_CODE, "Invalid billing party code");
            }
        }

        HttpProxyVO httpProxyVO = null;
        if (source.isHttpProxyRequired()) {
            Integer threshold = source.getHttpProxyThreshold();
            httpProxyVO = httpProxyService.getNextAvailableProxy(source.getCode(), threshold);
            if (httpProxyVO == null) {
                context.addError(WsResponseCode.INVALID_REQUEST, "No http proxy found to be added to channel");
            }
        }

        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        } else {
            channel = new Channel();
            channel.setSourceCode(source.getCode());
            channel.setName(request.getWsChannel().getChannelName());
            channel.setCode(channelCode);
            channel.setColorCode(getChannelColorCodeBySourceCode(source.getCode()));
            channel.setShortName(constructChannelShortName(source, request.getWsChannel().getChannelName()));
            channel.setCreated(DateUtils.getCurrentTime());
            channel.setEnabled(request.getWsChannel().isEnabled());
            channel.setThirdPartyShipping(request.getWsChannel().isThirdPartyShipping());
            channel.setBillingParty(billingParty);

            if (source.isOrderSyncConfigured()) {
                if (StringUtils.isNotBlank(request.getWsChannel().getOrderSyncStatus())) {
                    channel.setOrderSyncStatus(SyncStatus.valueOf(request.getWsChannel().getOrderSyncStatus()));
                } else {
                    channel.setOrderSyncStatus(SyncStatus.OFF);
                }
            } else {
                channel.setOrderSyncStatus(SyncStatus.NOT_AVAILABLE);
            }

            if (source.isInventorySyncConfigured()) {
                if (StringUtils.isNotBlank(request.getWsChannel().getInventorySyncStatus())) {
                    channel.setInventorySyncStatus(SyncStatus.valueOf(request.getWsChannel().getInventorySyncStatus()));
                } else {
                    channel.setInventorySyncStatus(SyncStatus.OFF);
                }
            } else {
                channel.setInventorySyncStatus(SyncStatus.NOT_AVAILABLE);
            }

            if (source.isOrderReconciliationEnabled()) {
                if (StringUtils.isNotBlank(request.getWsChannel().getOrderReconciliationSyncStatus())) {
                    channel.setReconciliationSyncStatus(SyncStatus.valueOf(request.getWsChannel().getOrderReconciliationSyncStatus()));
                } else {
                    channel.setReconciliationSyncStatus(SyncStatus.OFF);
                }
            } else {
                channel.setReconciliationSyncStatus(SyncStatus.NOT_AVAILABLE);
            }
            if (source.isPricingSyncConfigured() && StringUtils.isNotBlank(request.getWsChannel().getPricingSyncStatus())) {
                channel.setPricingSyncStatus(SyncStatus.valueOf(request.getWsChannel().getPricingSyncStatus()));
            } else {
                channel.setPricingSyncStatus(SyncStatus.NOT_AVAILABLE);
            }

            if (StringUtils.isNotBlank(request.getWsChannel().getLedgerName())) {
                channel.setLedgerName(request.getWsChannel().getLedgerName());
            } else {
                channel.setLedgerName(channel.getName());
            }
            if (com.uniware.core.entity.Source.Type.B2B.equals(source.getType())) {
                channel.setCustomer(customer);
            }
            if (source.isHttpProxyRequired()) {
                channel.setHttpProxyHost(httpProxyVO.getIpAddress());
                channel.setHttpProxyPort(String.valueOf(httpProxyVO.getPort()));
            }
            for (SourceConnector sourceConnector : source.getSourceConnectors()) {
                ChannelConnector channelConnector = new ChannelConnector();
                channelConnector.setChannel(channel);
                channelConnector.setSourceConnectorName(sourceConnector.getName());
                channelConnector.setStatusCode(Status.NOT_CONFIGURED);
                channelConnector.setCreated(DateUtils.getCurrentTime());
                channel.getChannelConnectors().add(channelConnector);
            }
            if (request.getWsChannel().getChannelConfigurationParameters() != null) {
                setChannelConfigurationParameters(context, channel, request.getWsChannel(), true);
            }
            channelDao.addChannel(channel);
            ChannelDetailDTO channelDetailDTO = new ChannelDetailDTO(channel, source);
            prepareChannelConnectorDTO(channel, source, channelDetailDTO);
            prepareChannelConfigurationParametersDTO(channelDetailDTO, channel);
            prepareSourceConfigurationParametersDTO(channelDetailDTO.getSource(), source);
            channelDetailDTO.setCustomFieldValues(CustomFieldUtils.getCustomFieldValuesDTO(channel));

            // for FBA type we required facility associated with channel
            if (source.isFacilityAssociationRequired() && source.isFetchCompleteOrders()) {
                addAssociatedFacility(channel, context);
            }
            if (!context.hasErrors()) {
                response.setChannelDetailDto(channelDetailDTO);
                response.setSuccessful(true);
            } else {
                response.addErrors(context.getErrors());
            }

            if (ActivityContext.current().isEnable()) {
                ActivityUtils.appendActivity(channelCode, ActivityEntityEnum.CHANNEL.getName(), channel.getCode(),
                        Arrays.asList(new String[] { "Channel " + channel.getCode() + " added" }), ActivityTypeEnum.CHANNEL_ADD.name());
            }
        }
        return response;
    }

    private void addAssociatedFacility(Channel channel, ValidationContext context) {
        Facility currentFacility = UserContext.current().getFacility();
        boolean deafaultFacilityAllocationRuleRequired = (CacheManager.getInstance().getCache(FacilityCache.class).getFacilities().size() == 1
                && facilityService.getFacilityAllocationRules().isEmpty());
        WsFacility wsFacility = new WsFacility();
        wsFacility.setType(Facility.Type.THIRD_PARTY_WAREHOUSE.name());
        wsFacility.setDisplayName(channel.getName());
        wsFacility.setName(channel.getName());
        wsFacility.setCode(channel.getCode());
        wsFacility.setInventorySynDisabled(true);
        wsFacility.setBillingAddress(prepareDummyAddressByType(PartyAddressType.Code.BILLING.name()));
        wsFacility.setShippingAddress(prepareDummyAddressByType(PartyAddressType.Code.SHIPPING.name()));
        List<SystemConfigurationDTO> systemConfigurationDTOs = new ArrayList<>(1);
        SystemConfigurationDTO systemConfigurationDTO = new SystemConfigurationDTO();
        systemConfigurationDTO.setName(INVENTORY_MANAGEMENT_SWITCHED_OFF);
        systemConfigurationDTO.setValue(Boolean.TRUE.toString());
        systemConfigurationDTO.setType(Type.CHECKBOX.toString().toLowerCase());
        systemConfigurationDTO.setAccessResourceName("MINIMAL");
        systemConfigurationDTO.setGroupName("System");
        systemConfigurationDTOs.add(systemConfigurationDTO);
        systemConfigurationDTO.setDisplayName(INVENTORY_MANAGEMENT_SWITCHED_OFF_DISPLAY_NAME);
        CreateFacilityRequest createFacilityRequest = new CreateFacilityRequest();
        createFacilityRequest.setUsername(UserContext.current().getUniwareUserName());
        createFacilityRequest.setFacility(wsFacility);
        createFacilityRequest.setAdditionalSystemConfigs(systemConfigurationDTOs);
        CreateFacilityResponse createFacilityResponse = facilityService.createFacility(createFacilityRequest);
        if (!createFacilityResponse.isSuccessful()) {
            context.addErrors(createFacilityResponse.getErrors());
        } else {
            Facility associatedFacility = facilityService.getAllFacilityByCode(createFacilityResponse.getFacilityDTO().getCode());
            try {
                channel.setAssociatedFacility(associatedFacility);
            } catch (Exception e) {
                LOG.error("error while adding facility : ", e);
                context.addError(WsResponseCode.INVALID_REQUEST, "Unable to Configure facility for channel");
            } finally {
                UserContext.current().setFacility(currentFacility);
            }
            if (deafaultFacilityAllocationRuleRequired) {
                CreateFacilityAllocationRuleResponse response = doAddDefaultFacilityAllocationRule(UserContext.current().getFacilityId());
                if (!response.isSuccessful()) {
                    LOG.error("Error while adding default facility allocation rule");
                    context.addError(WsResponseCode.INVALID_REQUEST, "Unable to add default facility allocation rule");
                }
            }
        }
    }

    private CreateFacilityAllocationRuleResponse doAddDefaultFacilityAllocationRule(Integer facilityId) {
        Facility facility = CacheManager.getInstance().getCache(FacilityCache.class).getFacilityById(facilityId);
        CreateFacilityAllocationRuleRequest request = new CreateFacilityAllocationRuleRequest();
        request.setName(DEFAULT_ALLOCATION_RULE_NAME);
        request.setConditionExpressionText(DEFAULT_ALLOCATION_RULE_EXPRESSION);
        request.setEnabled(true);
        request.setPreference(DEFAULT_ALLOCATION_RULE_PREFERENCE);
        request.setFacilityCode(facility.getCode());
        return facilityService.createAllocationRule(request);
    }

    private WsPartyAddress prepareDummyAddressByType(String addressType) {
        WsPartyAddress wsPartyAddress = new WsPartyAddress();
        wsPartyAddress.setAddressLine1(DUMMY_ADDRESS);
        wsPartyAddress.setAddressType(addressType);
        wsPartyAddress.setCity(DUMMY_CITY);
        wsPartyAddress.setStateCode(DUMMY_STATE);
        wsPartyAddress.setPincode(DUMMY_PIN);
        wsPartyAddress.setCountryCode(DUMMY_COUNTRY);
        wsPartyAddress.setPhone(DUMMY_MOBILE);
        return wsPartyAddress;
    }

    private void prepareChannelConnectorDTO(Channel channel, Source source, ChannelDetailDTO channelDetailDTO) {
        SourceConfiguration sourceConfiguration = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class);
        List<ChannelConnectorDTO> channelConnectors = new ArrayList<>(channel.getChannelConnectors().size());
        for (ChannelConnector cc : channel.getChannelConnectors()) {
            SourceConnector sourceConnector = sourceConfiguration.getSourceConnector(source.getCode(), cc.getSourceConnectorName());
            channelConnectors.add(new ChannelConnectorDTO(cc, sourceConnector));
        }
        channelDetailDTO.setChannelConnectors(channelConnectors);
    }

    private void setChannelConfigurationParameters(ValidationContext context, Channel channel, WsChannel wsChannel, boolean setAll) {
        Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(wsChannel.getSourceCode());
        Map<String, String> rawConfigurationParameters = new HashMap<>();
        for (WsChannelConfigurationParameter param : wsChannel.getChannelConfigurationParameters()) {
            rawConfigurationParameters.put(param.getName(), param.getValue());
        }

        for (SourceConfigurationParameter parameter : source.getSourceConfigurationParameters()) {
            if (setAll || (!parameter.getType().equals(Type.HIDDEN) && isSourceConfigurationParameterApplicable(source, parameter.getName()))) {
                try {
                    String rawValue = rawConfigurationParameters.get(parameter.getName());
                    if (StringUtils.isBlank(rawValue)) {
                        rawValue = parameter.getDefaultValue();
                    }
                    // XXX Hack: move formula to a column
                    if (Channel.INVENTORY_UPDATE_FORMULA.equals(parameter.getName()) && StringUtils.isNotBlank(rawValue)) {
                        String checksum = EncryptionUtils.md5Encode(rawValue.trim());
                        ChannelInventoryFormulaVO channelInventoryFormula = channelInventorySyncService.getChannelInventoryFormulaByChecksum(checksum);
                        rawValue = channelInventoryFormula.getCode();
                    }
                    if (StringUtils.isNotBlank(rawValue)) {
                        Object parameterValue = conversionService.convert(rawValue, Class.forName(parameter.getJavaType()));
                        if (parameterValue != null) {
                            ChannelConfigurationParameter ccp = channel.getOrCreateParameter(parameter.getName());
                            ccp.setValue(conversionService.convert(parameterValue, String.class));
                            channel.getChannelConfigurationParameters().add(ccp);
                        } else {
                            context.addError(ResponseCode.INVALID_FORMAT, "Invalid value for channel configuration parameter " + parameter.getName());
                        }
                    }
                } catch (Exception e) {
                    LOG.error("Error saving parameter " + parameter.getName(), e);
                    context.addError(ResponseCode.INVALID_FORMAT, "Invalid value for channel configuration parameter " + parameter.getName());
                }
            }
        }

    }

    @Override
    public ChannelOrderSyncStatusDTO updateChannelOrderSyncStatus(ChannelOrderSyncStatusDTO channelOrderSyncStatus) {
        LOG.info("Updating order sync status in mongo for channel {}", channelOrderSyncStatus.getChannelCode());
        channelMao.save(channelOrderSyncStatus);
        LOG.info("Updated order sync status in mongo for channel {}", channelOrderSyncStatus.getChannelCode());
        return channelOrderSyncStatus;
    }

    @Override
    public GoToChannelLandingPageResponse goToChannelLandingPage(GoToChannelLandingPageRequest request) {
        ValidationContext context = request.validate();
        GoToChannelLandingPageResponse response = new GoToChannelLandingPageResponse();
        if (!context.hasErrors()) {
            Channel channel = getChannelByCode(request.getChannelCode());
            if (channel == null) {
                context.addError(WsResponseCode.INVALID_CHANNEL_CODE);
            } else {
                if (StringUtils.isBlank(
                        CacheManager.getInstance().getCache(ChannelCache.class).getScriptName(channel.getCode(), com.uniware.core.entity.Source.LANDING_PAGE_SCRIPT_NAME))) {
                    context.addError(WsResponseCode.INVALID_CHANNEL_CODE);
                } else {
                    ScraperScript script = CacheManager.getInstance().getCache(ChannelCache.class).getScriptByName(channel.getCode(),
                            com.uniware.core.entity.Source.LANDING_PAGE_SCRIPT_NAME);
                    ScriptExecutionContext seContext = ScriptExecutionContext.current();
                    for (ChannelConnector channelConnector : channel.getChannelConnectors()) {
                        for (ChannelConnectorParameter channelConnectorParameter : channelConnector.getChannelConnectorParameters()) {
                            seContext.addVariable(channelConnectorParameter.getName(), channelConnectorParameter.getValue());
                        }
                    }
                    for (ChannelConfigurationParameter channelConfigurationParameter : channel.getChannelConfigurationParameters()) {
                        seContext.addVariable(channelConfigurationParameter.getSourceConfigurationParameterName(), channelConfigurationParameter.getValue());
                    }
                    List<Form> forms = new ArrayList<>();
                    seContext.addVariable("forms", forms);
                    try {
                        seContext.setTraceLoggingEnabled(UserContext.current().isTraceLoggingEnabled());
                        script.execute();
                        response.setSuccessful(true);
                        response.setRedirectForms(forms);
                    } finally {
                        ScriptExecutionContext.destroy();
                    }
                }
            }
        }
        return response;
    }

    @Override
    public PreConfigureChannelConnectorResponse preConfigureChannelConnector(PreConfigureChannelConnectorRequest request) {
        ValidationContext context = request.validate();
        PreConfigureChannelConnectorResponse response = new PreConfigureChannelConnectorResponse();
        Channel channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelByCode(request.getChannelCode());
        if (channel == null) {
            context.addError(WsResponseCode.INVALID_CHANNEL_CODE, WsResponseCode.INVALID_CHANNEL_CODE.message() + ": " + request.getChannelCode());
        } else {
            Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(channel.getSourceCode());
            ScraperScript preConfigurationScript = CacheManager.getInstance().getCache(ChannelCache.class).getScriptByName(channel.getCode(),
                    com.uniware.core.entity.Source.PRE_CONFIGURATION_SCRIPT_NAME, true);
            if (preConfigurationScript == null) {
                context.addError(WsResponseCode.INVALID_SCRIPT);
            }
            if (!context.hasErrors()) {
                ScriptExecutionContext seContext = ScriptExecutionContext.current();
                seContext.addVariable("channel", channel);
                seContext.addVariable("source", source);
                seContext.addVariable("connector", request.getConnectorName());
                for (WsChannelConnectorParameter connectorParameter : request.getChannelConnectorParameters()) {
                    seContext.addVariable(connectorParameter.getName(), connectorParameter.getValue());
                }
                try {
                    seContext.setTraceLoggingEnabled(UserContext.current().isTraceLoggingEnabled());
                    preConfigurationScript.execute();
                    response.setRedirectForm((Form) seContext.getVariable("preConfigureForm"));
                    response.setSuccessful(true);
                } catch (Throwable e) {
                    context.addError(WsResponseCode.INVALID_CHANNEL_CREDENTIALS, "Unable to connect to third party");
                } finally {
                    ScriptExecutionContext.destroy();
                }
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    public PostConfigureChannelConnectorResponse postConfigureChannelConnector(PostConfigureChannelConnectorRequest request) {
        ValidationContext context = request.validate();
        PostConfigureChannelConnectorResponse response = new PostConfigureChannelConnectorResponse();
        String channelCode = request.getRequestParams().get("channelCode");
        Channel channel = null;
        Source source = null;
        if (!context.hasErrors()) {
            channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelByCode(channelCode);
            if (channel == null) {
                context.addError(WsResponseCode.INVALID_CHANNEL_CODE, WsResponseCode.INVALID_CHANNEL_CODE.message() + ": " + channelCode);
            }
        }

        ScraperScript postConfigurationScript = null;
        if (!context.hasErrors()) {
            source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(channel.getSourceCode());
            postConfigurationScript = CacheManager.getInstance().getCache(ChannelCache.class).getScriptByName(channel.getCode(),
                    com.uniware.core.entity.Source.POST_CONFIGURATION_SCRIPT_NAME, true);
            if (postConfigurationScript == null) {
                context.addError(WsResponseCode.INVALID_SCRIPT);
            }
        }
        if (context.hasErrors()) {
            response.setSuccessful(false);
            response.setErrors(context.getErrors());
        } else {
            ScriptExecutionContext seContext = ScriptExecutionContext.current();
            Map<String, String> responseParams = new HashMap<String, String>();
            try {
                seContext.addVariable("channel", channel);
                seContext.addVariable("source", source);
                seContext.addVariable("requestParams", request.getRequestParams());
                seContext.addVariable("responseParams", responseParams);
                seContext.setTraceLoggingEnabled(UserContext.current().isTraceLoggingEnabled());
                postConfigurationScript.execute();
            } catch (Exception e) {
                LOG.error("Error adding/editing channel: {}", request.getRequestParams().get("channelName"));
                LOG.error("Stack trace: ", e);
                response.setMessage(e.getMessage());
                context.addError(WsResponseCode.CHANNEL_NOT_CONFIGURED, e.getMessage());
            } finally {
                ScriptExecutionContext.destroy();
            }
            if (!context.hasErrors()) {
                String connectorName = request.getRequestParams().get("connector");
                WsChannelConnector channelConnector = new WsChannelConnector();
                channelConnector.setChannelCode(channel.getCode());
                channelConnector.setName(connectorName);
                for (Entry<String, String> e : responseParams.entrySet()) {
                    WsChannelConnectorParameter param = new WsChannelConnectorParameter(e.getKey(), e.getValue());
                    channelConnector.addChannelConnectorParameter(param);
                }
                AddChannelConnectorRequest addChannelConnectorRequest = new AddChannelConnectorRequest();
                addChannelConnectorRequest.setChannelConnector(channelConnector);
                AddChannelConnectorResponse addChannelConnectorResponse = addChannelConnector(addChannelConnectorRequest);
                response.setResponse(addChannelConnectorResponse);
            }
        }

        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    @Override
    public AddChannelConnectorResponse addChannelConnector(AddChannelConnectorRequest request) {
        AddChannelConnectorResponse response = doAddChannelConnector(request);
        if (response.isSuccessful()) {
            channelCatalogSyncService.syncChannelCatalog(new SyncChannelCatalogRequest(request.getChannelConnector().getChannelCode()));
            channelOrderSyncService.syncChannelOrders(new SyncChannelOrdersRequest(request.getChannelConnector().getChannelCode()));
        }
        return response;
    }

    @MarkDirty(values = { ChannelCache.class })
    @Transactional
    @LogActivity
    private AddChannelConnectorResponse doAddChannelConnector(AddChannelConnectorRequest request) {
        AddChannelConnectorResponse response = new AddChannelConnectorResponse();
        ValidationContext context = request.validate();
        Channel channel = null;
        SourceConnector sourceConnector = null;
        VerifyAndSyncParamsResponse verifyConnectorResponse = null;
        Channel dummyChannel = null;
        if (!context.hasErrors()) {
            channel = channelDao.getChannelByCode(request.getChannelConnector().getChannelCode());
            if (channel == null) {
                context.addError(WsResponseCode.INVALID_CHANNEL_CODE, "Invalid channel code");
            } else {
                dummyChannel = createDummyChannelObject(channel, true);
                sourceConnector = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceConnector(channel.getSourceCode(),
                        request.getChannelConnector().getName());
                if (sourceConnector == null) {
                    context.addError(WsResponseCode.INVALID_CHANNEL_CONNECTOR, "Invalid channel connector name");
                } else {
                    if (!isChannelConnectorExist(request.getChannelConnector().getName(), channel)) {
                        createChannelConnector(channel, sourceConnector.getName(), request.getChannelConnector());
                    }
                    if (StringUtils.isNotBlank(sourceConnector.getVerificationScriptName())) {
                        ScraperScript script = CacheManager.getInstance().getCache(ScriptVersionedCache.class).getScriptByName(sourceConnector.getVerificationScriptName());
                        Map<String, Object> params = new HashMap<>(request.getChannelConnector().getChannelConnectorParameters().size());
                        for (WsChannelConnectorParameter channelConnectorParam : request.getChannelConnector().getChannelConnectorParameters()) {
                            params.put(channelConnectorParam.getName(), channelConnectorParam.getValue());
                        }
                        if (request.getChannelConnector().getChallengeReplies() != null) {
                            for (VerifyAndSyncParamsResponse.WsAuthenticationChallenge challenge : request.getChannelConnector().getChallengeReplies()) {
                                params.put(challenge.getCode(), challenge);
                            }
                        }
                        params.put("userTriggeredVerification", true);
                        if (script != null) {
                            verifyConnectorResponse = verifyAndSyncChannelConnectorParameters(channel, params, script);
                            if (verifyConnectorResponse.hasErrors()) {
                                channelDao.updateChannelConnector(
                                        setConnectorErrorStatus(channel, request.getChannelConnector().getName(), verifyConnectorResponse.getErrors().get(0)));
                                context.addError(WsResponseCode.INVALID_CHANNEL_CREDENTIALS, "channel.add.connector.errors",
                                        verifyConnectorResponse.getErrors().get(0).getDescription());
                                response.setChallenges(verifyConnectorResponse.getChallenges());
                            }
                        }
                    }
                }
            }
        }
        if (!context.hasErrors()) {
            SourceConfiguration sourceConfiguration = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class);
            Source source = sourceConfiguration.getSourceByCode(channel.getSourceCode());
            ChannelConnector channelConnector = null;
            for (ChannelConnector connector : channel.getChannelConnectors()) {
                if (connector.getSourceConnectorName().equals(request.getChannelConnector().getName())) {
                    channelConnector = connector;
                }
            }
            if (channelConnector == null) {
                channelConnector = new ChannelConnector();
                channelConnector.setSourceConnectorName(sourceConnector.getName());
                channelConnector.setChannel(channel);
                channelConnector.setCreated(DateUtils.getCurrentTime());
            }
            Map<String, SourceConnectorParameter> nameToSourceConnectorParameters = new HashMap<>();
            for (SourceConnectorParameter sourceConnectorParameter : sourceConnector.getSourceConnectorParameters()) {
                nameToSourceConnectorParameters.put(sourceConnectorParameter.getName(), sourceConnectorParameter);
            }
            Map<String, ChannelConnectorParameter> nameToConnectorParameters = new HashMap<String, ChannelConnectorParameter>();
            for (ChannelConnectorParameter channelConnectorParameter : channelConnector.getChannelConnectorParameters()) {
                nameToConnectorParameters.put(channelConnectorParameter.getName(), channelConnectorParameter);
            }
            for (WsChannelConnectorParameter wsChannelConnectorParameter : request.getChannelConnector().getChannelConnectorParameters()) {
                ChannelConnectorParameter channelConnectorParameter = nameToConnectorParameters.get(wsChannelConnectorParameter.getName());
                if (channelConnectorParameter == null) {
                    channelConnectorParameter = new ChannelConnectorParameter();
                    channelConnectorParameter.setChannelConnector(channelConnector);
                    channelConnectorParameter.setName(wsChannelConnectorParameter.getName());
                    channelConnectorParameter.setValue(wsChannelConnectorParameter.getValue());
                    channelConnectorParameter.setCreated(DateUtils.getCurrentTime());
                    channelConnector.getChannelConnectorParameters().add(channelConnectorParameter);
                    nameToConnectorParameters.put(channelConnectorParameter.getName(), channelConnectorParameter);
                } else {
                    SourceConnectorParameter sourceConnectorParameter = nameToSourceConnectorParameters.get(wsChannelConnectorParameter.getName());
                    if (sourceConnectorParameter.getName().equals(channelConnectorParameter.getName()) && (!sourceConnectorParameter.getType().equals(SourceConnectorParameter.Type.HIDDEN)) || StringUtils.isNotBlank(wsChannelConnectorParameter.getValue())) {
                        channelConnectorParameter.setValue(wsChannelConnectorParameter.getValue());
                    }
                }
            }
            if (verifyConnectorResponse != null && !verifyConnectorResponse.getPersistentParams().isEmpty()) {
                for (Entry<String, String> e : verifyConnectorResponse.getPersistentParams().entrySet()) {
                    ChannelConnectorParameter channelConnectorParameter = nameToConnectorParameters.get(e.getKey());
                    if (channelConnectorParameter == null) {
                        channelConnectorParameter = new ChannelConnectorParameter();
                        channelConnectorParameter.setName(e.getKey());
                        channelConnectorParameter.setChannelConnector(channelConnector);
                        channelConnectorParameter.setCreated(DateUtils.getCurrentTime());
                        channelConnector.getChannelConnectorParameters().add(channelConnectorParameter);
                    }
                    channelConnectorParameter.setValue(e.getValue());
                }
            }

            channelConnector.setStatusCode(Status.ACTIVE);
            channelConnector.resetCount();
            channel.getChannelConnectors().remove(channelConnector);
            channel.getChannelConnectors().add(channelConnector);
            if (source.isInventorySyncConfigured() && sourceConnector.isRequiredInInventorySync()) {
                SyncStatus syncStatus = (channel.getInventorySyncStatus() == SyncStatus.ON) ? SyncStatus.ON : SyncStatus.OFF;
                for (ChannelConnector connector : channel.getChannelConnectors()) {
                    SourceConnector sc = sourceConfiguration.getSourceConnector(source.getCode(), connector.getSourceConnectorName());
                    if (sc.isRequiredInInventorySync() && Status.NOT_CONFIGURED.equals(connector.getStatusCode())) {
                        syncStatus = SyncStatus.OFF;
                        break;
                    }
                }
                channel.setInventorySyncStatus(syncStatus);
            }
            if (source.isOrderSyncConfigured() && sourceConnector.isRequiredInOrderSync()) {
                SyncStatus syncStatus = (channel.getOrderSyncStatus() == SyncStatus.ON) ? SyncStatus.ON : SyncStatus.OFF;
                for (ChannelConnector connector : channel.getChannelConnectors()) {
                    SourceConnector sc = sourceConfiguration.getSourceConnector(source.getCode(), connector.getSourceConnectorName());
                    if (sc.isRequiredInOrderSync() && Status.NOT_CONFIGURED.equals(connector.getStatusCode())) {
                        syncStatus = SyncStatus.OFF;
                        break;
                    }
                }
                channel.setOrderSyncStatus(syncStatus);
            }
            if (source.isOrderReconciliationEnabled() && sourceConnector.isRequiredInReconciliationSync()) {
                SyncStatus syncStatus = (channel.getReconciliationSyncStatus() == SyncStatus.ON) ? SyncStatus.ON : SyncStatus.OFF;
                for (ChannelConnector connector : channel.getChannelConnectors()) {
                    SourceConnector sc = sourceConfiguration.getSourceConnector(source.getCode(), connector.getSourceConnectorName());
                    if (sc.isRequiredInReconciliationSync() && Status.NOT_CONFIGURED.equals(connector.getStatusCode())) {
                        syncStatus = SyncStatus.OFF;
                        break;
                    }
                }
                channel.setReconciliationSyncStatus(syncStatus);
            }
            channelDao.updateChannel(channel);
            response.setSuccessful(true);
            HashMap<String, ArrayList<String>> entityDifferenceMap = new HashMap<String, ArrayList<String>>();
            ActivityUtils.getObjectDiff(dummyChannel, channel, entityDifferenceMap);
            String channelActivityLogs = ActivityUtils.getChannelSpecificActivityLogs(entityDifferenceMap);
            if (StringUtils.isNotBlank(channelActivityLogs)) {
                ActivityUtils.appendActivity(channel.getCode(), ActivityEntityEnum.CHANNEL.getName(), channel.getSourceCode(), Arrays.asList(new String[] { channelActivityLogs }),
                        ActivityTypeEnum.CONNECTOR_PARAMS.name());
            }
        }
        if (!response.isSuccessful()) {
            AuditContext.current().setAuditEnabled(false);
        }
        response.setErrors(context.getErrors());
        return response;
    }

    private void createChannelConnector(Channel channel, String connectorName, WsChannelConnector channelConnectorRequest) {
        ChannelConnector channelConnector = new ChannelConnector();
        channelConnector.setSourceConnectorName(connectorName);
        channelConnector.setStatusCode(Status.BROKEN);
        channelConnector.setChannel(channel);
        channelConnector.setCreated(DateUtils.getCurrentTime());

        ChannelConnectorParameter channelConnectorParameter = new ChannelConnectorParameter();
        for (WsChannelConnectorParameter wsChannelConnectorParameter : channelConnectorRequest.getChannelConnectorParameters()) {
            channelConnectorParameter.setChannelConnector(channelConnector);
            channelConnectorParameter.setName(wsChannelConnectorParameter.getName());
            channelConnectorParameter.setValue("");
            channelConnectorParameter.setCreated(DateUtils.getCurrentTime());
            channelConnector.getChannelConnectorParameters().add(channelConnectorParameter);
        }
        channel.getChannelConnectors().add(channelConnector);
        channelDao.updateChannel(channel);
    }

    private boolean isChannelConnectorExist(String connectorName, Channel channel) {
        for (ChannelConnector connector : channel.getChannelConnectors()) {
            if (connector.getSourceConnectorName().equals(connectorName)) {
                return true;
            }
        }
        return false;
    }

    private ChannelConnector setConnectorErrorStatus(Channel channel, String connectorName, WsError error) {
        ChannelConnector channelConnector = null;
        for (ChannelConnector connector : channel.getChannelConnectors()) {
            if (connector.getSourceConnectorName().equals(connectorName)) {
                channelConnector = connector;
            }
        }
        if (channelConnector != null) {
            channelConnector.setErrorMessage(error.getCode() + ":" + error.getDescription());
            if (WsResponseCode.INVALID_CHANNEL_CREDENTIALS.message().equals(error.getMessage())) {
                channelConnector.setStatusCode(Status.INVALID_CREDENTIALS);
            } else {
                channelConnector.setStatusCode(Status.BROKEN);
            }
        }
        return channelConnector;
    }

    public static class ChannelGoodToSyncResponse {
        private boolean             goodToSync;
        private String              message;
        private Map<String, String> params;

        public boolean isGoodToSync() {
            return goodToSync;
        }

        public void setGoodToSync(boolean goodToSync) {
            this.goodToSync = goodToSync;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public Map<String, String> getParams() {
            return params;
        }

        public void setParams(Map<String, String> params) {
            this.params = params;
        }

    }

    /*
    * Does health check for channel connectors
    */
    @Override
    public ChannelGoodToSyncResponse isChannelGoodToSync(String channelCode, Sync sync) {
        ChannelGoodToSyncResponse response = new ChannelGoodToSyncResponse();
        Channel channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelByCode(channelCode);
        boolean goodToSync = true;
        Map<String, String> params = new HashMap<>();
        SourceConfiguration sourceConfiguration = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class);
        for (ChannelConnector cc : channel.getChannelConnectors()) {
            if (cc.getStatusCode().equals(Status.BROKEN) && cc.getFailedCount() > 5) {
                LOG.info("Skipping connector check for channel: {} as failed count is: {}", cc.getChannel().getCode(), cc.getFailedCount());
                goodToSync = false;
            } else {
                SourceConnector sourceConnector = sourceConfiguration.getSourceConnector(channel.getSourceCode(), cc.getSourceConnectorName());
                if ((Sync.ORDER.equals(sync) && sourceConnector.isRequiredInOrderSync()) || (Sync.INVENTORY.equals(sync) && sourceConnector.isRequiredInInventorySync())
                        || (Sync.RECONCILIATION.equals(sync) && sourceConnector.isRequiredInReconciliationSync())
                        || (Sync.CHANNEL_WAREHOUSE_INVENTORY.equals(sync) && sourceConnector.isRequiredInInventorySync())
                        || (Sync.CATALOG.equals(sync) && (sourceConnector.isRequiredInInventorySync() || sourceConnector.isRequiredInReconciliationSync()))) {
                    if (Status.INVALID_CREDENTIALS.equals(cc.getStatusCode()) || Status.NOT_CONFIGURED.equals(cc.getStatusCode())) {
                        goodToSync = false;
                        break;
                    }
                    boolean connectorBroken = false;
                    boolean invalidCredentials = false;
                    LOG.info("Checking connector {}", cc.getSourceConnectorName());
                    VerifyAndSyncParamsResponse verifyParamsResponse = verifyAndSyncChannelConnector(channel.getCode(), cc.getSourceConnectorName());
                    if (!verifyParamsResponse.isSuccessful()) {
                        WsError error = verifyParamsResponse.getErrors().get(0);
                        cc.setErrorMessage(error.getMessage());
                        goodToSync = false;
                        if (!ChannelScriptError.ScriptErrorCodes.CHANNEL_REFUSED_SYNC.name().equals(error.getMessage())) {
                            connectorBroken = true;
                        }
                        invalidCredentials = WsResponseCode.INVALID_CHANNEL_CREDENTIALS.message().equals(error.getMessage());
                    } else {
                        params.putAll(verifyParamsResponse.getPersistentParams());
                        params.putAll(verifyParamsResponse.getTransientParams());
                    }
                    if (connectorBroken) {
                        cc.setFailedCount(cc.getFailedCount() + 1);
                    }
                    if (connectorBroken && Status.ACTIVE.equals(cc.getStatusCode())) {
                        cc.setStatusCode(invalidCredentials ? Status.INVALID_CREDENTIALS : Status.BROKEN);
                        updateChannelConnector(cc);
                    } else if (!connectorBroken && Status.BROKEN.equals(cc.getStatusCode())) {
                        cc.setStatusCode(Status.ACTIVE);
                        cc.resetCount();
                        updateChannelConnector(cc);
                    }
                    LOG.info("Done checking connector {}", cc.getSourceConnectorName());
                }
            }
        }
        response.setParams(params);
        response.setGoodToSync(goodToSync);
        return response;
    }

    @Override
    public VerifyAndSyncParamsResponse verifyAndSyncChannelConnector(String channelCode, String connectorName) {
        VerifyAndSyncParamsResponse response = new VerifyAndSyncParamsResponse();
        Channel channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelByCode(channelCode);
        ChannelConnector channelConnector = null;
        for (ChannelConnector cc : channel.getChannelConnectors()) {
            if (connectorName.equals(cc.getSourceConnectorName())) {
                channelConnector = cc;
                break;
            }
        }
        if (channelConnector == null) {
            response.addError(new WsError("Invalid Channel Connector"));
        } else {
            SourceConfiguration sourceConfiguration = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class);
            SourceConnector sourceConnector = sourceConfiguration.getSourceConnector(channel.getSourceCode(), connectorName);
            String connectorVerificationScriptName = sourceConnector.getVerificationScriptName();
            if (StringUtils.isNotBlank(connectorVerificationScriptName)) {
                ScraperScript scraperScript = CacheManager.getInstance().getCache(ScriptVersionedCache.class).getScriptByName(connectorVerificationScriptName);
                if (scraperScript == null) {
                    LOG.error("Missing connector verification script: {}", connectorVerificationScriptName);
                    response.addError(new WsError("Missing channel verification script"));
                } else {
                    Map<String, ChannelConnectorParameter> channelParameters = new HashMap<>(channelConnector.getChannelConnectorParameters().size());
                    Map<String, Object> params = new HashMap<>(channelConnector.getChannelConnectorParameters().size());
                    for (ChannelConnectorParameter ccp : channelConnector.getChannelConnectorParameters()) {
                        channelParameters.put(ccp.getName(), ccp);
                        params.put(ccp.getName(), ccp.getValue());
                    }
                    for (ChannelConfigurationParameter ccp : channel.getChannelConfigurationParameters()) {
                        params.put(ccp.getSourceConfigurationParameterName(), ccp.getValue());
                    }

                    response = verifyAndSyncChannelConnectorParameters(channel, params, scraperScript);

                    if (response.isSuccessful()) {
                        if (!response.getPersistentParams().isEmpty()) {
                            for (Entry<String, String> e : response.getPersistentParams().entrySet()) {
                                ChannelConnectorParameter ccp = null;
                                if (channelParameters.containsKey(e.getKey())) {
                                    ccp = channelParameters.get(e.getKey());
                                } else {
                                    ccp = new ChannelConnectorParameter();
                                    ccp.setName(e.getKey());
                                    ccp.setChannelConnector(channelConnector);
                                    ccp.setCreated(DateUtils.getCurrentTime());
                                    channelConnector.getChannelConnectorParameters().add(ccp);
                                }
                                ccp.setValue(e.getValue());
                            }
                            channelConnector = updateChannelConnector(channelConnector);
                        }
                        for (ChannelConnectorParameter ccp : channelConnector.getChannelConnectorParameters()) {
                            response.getPersistentParams().put(ccp.getName(), ccp.getValue());
                        }
                        for (ChannelConfigurationParameter ccp : channel.getChannelConfigurationParameters()) {
                            response.getPersistentParams().put(ccp.getSourceConfigurationParameterName(), ccp.getValue());
                        }
                    }
                }
            } else {
                for (ChannelConnectorParameter ccp : channelConnector.getChannelConnectorParameters()) {
                    response.getPersistentParams().put(ccp.getName(), ccp.getValue());
                }
                for (ChannelConfigurationParameter ccp : channel.getChannelConfigurationParameters()) {
                    response.getPersistentParams().put(ccp.getSourceConfigurationParameterName(), ccp.getValue());
                }
                response.setSuccessful(true);
            }
        }
        return response;
    }

    private VerifyAndSyncParamsResponse verifyAndSyncChannelConnectorParameters(Channel channel, Map<String, Object> channelParameters, ScraperScript verificationScript) {
        VerifyAndSyncParamsResponse response = new VerifyAndSyncParamsResponse();
        Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(channel.getSourceCode());
        try {
            ScriptExecutionContext context = ScriptExecutionContext.current();
            context.addVariable("channel", channel);
            context.addVariable("source", source);
            context.addVariable("applicationContext", applicationContext);
            context.setScriptProvider(new IScriptProvider() {
                @Override
                public ScraperScript getScript(String scriptName) {
                    return CacheManager.getInstance().getCache(ScriptVersionedCache.class).getScriptByName(scriptName);
                }
            });
            for (Entry<String, Object> e : channelParameters.entrySet()) {
                context.addVariable(e.getKey(), e.getValue());
            }
            context.setTraceLoggingEnabled(UserContext.current().isTraceLoggingEnabled());
            verificationScript.execute();

            String connectorParameterXml = context.getScriptOutput();
            if (StringUtils.isNotBlank(connectorParameterXml)) {
                Element rootElement = XMLParser.parse(connectorParameterXml);
                List<Element> challengeList = rootElement.list("Challenge");
                if (challengeList != null && challengeList.size() > 0) {
                    response.addError(new WsError(WsResponseCode.CHANNEL_CONNECTOR_CHALLENGE_RECEIVED.code(), WsResponseCode.CHANNEL_CONNECTOR_CHALLENGE_RECEIVED.message(),
                            "Challenge received"));
                    List<VerifyAndSyncParamsResponse.WsAuthenticationChallenge> challenges = new ArrayList<>(challengeList.size());
                    for (Element eChallenge : challengeList) {
                        VerifyAndSyncParamsResponse.WsAuthenticationChallenge challenge = new VerifyAndSyncParamsResponse.WsAuthenticationChallenge(eChallenge.text("Code"),
                                eChallenge.text("DisplayText"), VerifyAndSyncParamsResponse.ChallengeType.valueOf(eChallenge.text("Type")), eChallenge.text("CallbackParams"));
                        challenges.add(challenge);
                    }
                    response.setChallenges(challenges);
                } else {
                    List<Element> persistentParamList = rootElement.list("PersistentParams");
                    if (persistentParamList != null) {
                        for (Element param : persistentParamList) {
                            response.getPersistentParams().put(param.text("Name"), param.text("Value"));
                        }
                    }
                    List<Element> transientParamList = rootElement.list("TransientParams");
                    if (transientParamList != null) {
                        response.setTransientParams(new HashMap<String, String>(transientParamList.size()));
                        for (Element param : transientParamList) {
                            response.getTransientParams().put(param.text("Name"), param.text("Value"));
                        }
                    }
                    response.setSuccessful(true);
                }
            } else {
                response.setSuccessful(true);
            }
        } catch (ScriptExecutionException ex) {
            if (ChannelScriptError.ScriptErrorCodes.INVALID_CREDENTIALS.name().equals(ex.getReasonCode())) {
                response.addError(new WsError(WsResponseCode.INVALID_CHANNEL_CREDENTIALS.code(), WsResponseCode.INVALID_CHANNEL_CREDENTIALS.message(), ex.getMessage()));
            } else {
                response.addError(new WsError(WsResponseCode.UNABLE_TO_LOG_IN.code(), WsResponseCode.UNABLE_TO_LOG_IN.message(), ex.getMessage()));
            }
        } catch (Throwable e) {
            LOG.error("Exception while executing verification script - {}", e);
            response.addError(new WsError(WsResponseCode.UNABLE_TO_LOG_IN.code(), WsResponseCode.UNABLE_TO_LOG_IN.message(), e.getMessage()));
        } finally {
            ScriptExecutionContext.destroy();
        }
        return response;
    }

    @Override
    @Transactional(readOnly = true)
    public GetChannelSyncResponse getChannelSync(GetChannelSyncRequest request) {
        GetChannelSyncResponse response = new GetChannelSyncResponse();
        List<ChannelSyncDTO> channels = CacheManager.getInstance().getCache(ChannelCache.class).getChannelSyncDTOs();
        for (ChannelSyncDTO syncDTO : channels) {
            syncDTO.setLastInventorySyncResult(getChannelInventorySyncStatus(syncDTO.getCode()));
            syncDTO.setLastOrderSyncResult(getChannelOrderSyncStatus(syncDTO.getCode()));
        }
        response.setChannels(channels);
        response.setSuccessful(true);
        return response;
    }

    @Override
    @Transactional(readOnly = true)
    public GetChannelSyncAttributesResponse getChannelSyncAttributes(GetChannelSyncAttributesRequest request) {
        GetChannelSyncAttributesResponse response = new GetChannelSyncAttributesResponse();
        List<ChannelSyncAttributesDTO> channelSyncAttributesDTOs = new ArrayList<>();
        List<ChannelSyncDTO> channels = CacheManager.getInstance().getCache(ChannelCache.class).getChannelSyncDTOs();
        for (ChannelSyncDTO channelSyncDTO : channels) {
            channelSyncAttributesDTOs.add(new ChannelSyncAttributesDTO(channelSyncDTO));
        }
        response.setChannels(channelSyncAttributesDTOs);
        response.setSuccessful(true);
        return response;
    }

    @Override
    @Transactional(readOnly = true)
    public GetChannelDetailsResponse getChannelDetailsByChannelCode(GetChannelDetailsRequest request) {
        GetChannelDetailsResponse response = new GetChannelDetailsResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            response.setChannelDetailDTO(CacheManager.getInstance().getCache(ChannelCache.class).getChannelDetailDTOByCode(request.getChannelCode()));
            response.setSuccessful(true);
        }
        return response;
    }

    @Override
    public void prepareChannelConfigurationParametersDTO(ChannelDetailDTO channelDTO, Channel channel) {
        List<ChannelConfigurationParameterDTO> channelConfigurationParameters = new ArrayList<>();
        SourceConfiguration sourceConfiguration = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class);
        Source source = sourceConfiguration.getSourceByCode(channel.getSourceCode());
        for (ChannelConfigurationParameter param : channel.getChannelConfigurationParameters()) {
            SourceConfigurationParameter sourceConfigurationParameter = sourceConfiguration.getSourceConfigurationParameter(channel.getSourceCode(),
                    param.getSourceConfigurationParameterName());
            if (sourceConfigurationParameter != null && !sourceConfigurationParameter.getType().equals(Type.HIDDEN)
                    && isSourceConfigurationParameterApplicable(source, sourceConfigurationParameter.getName())) {
                Object value;
                try {
                    String rawValue = param.getValue();
                    if (!sourceConfigurationParameter.getJavaType().equals(String.class)) {
                        value = StringUtils.isNotBlank(rawValue) ? conversionService.convert(rawValue, Class.forName(sourceConfigurationParameter.getJavaType())) : null;
                    } else {
                        value = rawValue;
                    }
                    if ("inventoryUpdateFormula".equals(sourceConfigurationParameter.getName()) && value != null) {
                        value = channelInventorySyncService.getChannelInventoryFormulaByCode((String) value).getFormula();
                    }
                    ChannelConfigurationParameterDTO paramDTO = new ChannelConfigurationParameterDTO(param);
                    paramDTO.setValue(value);
                    channelConfigurationParameters.add(paramDTO);
                } catch (Throwable t) {
                    LOG.error("Error parsing configuration parameter for channel", t);
                    LOG.error("Error in param {}", sourceConfigurationParameter.getName());
                    throw new RuntimeException(t);
                }
            }
        }
        Collections.sort(channelConfigurationParameters, new Comparator<ChannelConfigurationParameterDTO>() {
            @Override
            public int compare(ChannelConfigurationParameterDTO o1, ChannelConfigurationParameterDTO o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });
        channelDTO.setChannelConfigurationParameters(channelConfigurationParameters);
    }

    @Override
    public void prepareSourceConfigurationParametersDTO(SourceDetailDTO sourceDTO, Source source) {
        List<SourceConfigurationParameterDTO> sourceConfigurationParameters = new ArrayList<>();
        for (SourceConfigurationParameter scp : source.getSourceConfigurationParameters()) {
            if (!scp.getType().equals(Type.HIDDEN) && isSourceConfigurationParameterApplicable(source, scp.getName())) {
                Object value = null;
                try {
                    SourceConfigurationParameterDTO paramDTO = new SourceConfigurationParameterDTO(scp);
                    String rawValue = scp.getDefaultValue();
                    if (!scp.getJavaType().equals(String.class) && StringUtils.isNotBlank(rawValue)) {
                        value = conversionService.convert(rawValue, Class.forName(scp.getJavaType()));
                    } else {
                        value = rawValue;
                    }
                    paramDTO.setDefaultValue(value);
                    sourceConfigurationParameters.add(paramDTO);
                } catch (Throwable t) {
                    LOG.error("Error in source param {}", scp.getName());
                    LOG.error("Error parsing source configuration parameter for channel", t);
                    throw new RuntimeException(t);
                }
            }
        }
        sourceDTO.setSourceConfigurationParameters(sourceConfigurationParameters);
    }

    @Override
    @Transactional(readOnly = true)
    public GetChannelOrderSummaryResponse getChannelOrderSummaryByChannelCode(GetChannelOrderSummaryRequest request) {
        GetChannelOrderSummaryResponse response = new GetChannelOrderSummaryResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            ChannelCache channelCache = CacheManager.getInstance().getCache(ChannelCache.class);
            Channel channel = channelCache.getChannelByCode(request.getChannelCode());
            response.setTodaysRevenue(channelDao.getChannelRevenue(channel.getId(), DateUtils.getCurrentDate()));
            ChannelOrderSyncStatusDTO orderSyncStatus = getChannelOrderSyncStatus(request.getChannelCode());
            response.setTodaysFailedOrderCount(orderSyncStatus.getTodaysFailedImportCount());
            response.setTodaysSuccessfulOrderCount(orderSyncStatus.getTodaysSuccessfulImportCount());
            response.setSuccessful(true);
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    @Override
    @Transactional(readOnly = true)
    public GetChannelProductSummaryResponse getChannelProductSummaryByChannelCode(GetChannelProductSummaryRequest request) {
        GetChannelProductSummaryResponse response = new GetChannelProductSummaryResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            Channel channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelByCode(request.getChannelCode());
            response.setUnmappedProductCount(channelCatalogService.getUnlinkedChannelItemTypeCount(channel.getId()));
            response.setMappedProductCount(channelCatalogService.getLinkedChannelItemTypeCount(channel.getId()));
            response.setIgnoredProductCount(channelCatalogService.getIgnoredChannelItemTypeCount(channel.getId()));
            response.setLiveProductCount(channelCatalogService.getChannelItemTypeCount(channel.getId(), ListingStatus.ACTIVE));
            response.setNonLiveProductCount(channelCatalogService.getChannelItemTypeCount(channel.getId(), ListingStatus.INACTIVE));
            response.setSuccessful(true);
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    @Override
    @Transactional
    @MarkDirty(values = { ChannelCache.class })
    @LogActivity
    public ChangeChannelReconciliationSyncStatusResponse changeReconciliationSyncStatus(ChangeChannelReconciliationSyncStatusRequest request) {
        ChangeChannelReconciliationSyncStatusResponse response = new ChangeChannelReconciliationSyncStatusResponse();
        ValidationContext context = request.validate();
        Channel channel = getChannelByCode(request.getChannelCode());
        if (!context.hasErrors()) {
            if (channel == null) {
                context.addError(WsResponseCode.INVALID_CHANNEL_CODE);
            } else if (SyncStatus.NOT_AVAILABLE.equals(channel.getReconciliationSyncStatus())) {
                context.addError(WsResponseCode.INVALID_CHANNEL_CONNECTOR);
            } else {
                SyncStatus syncStatus = SyncStatus.valueOf(request.getStatusCode());
                if (SyncStatus.ON.equals(syncStatus) || SyncStatus.OFF.equals(syncStatus)) {
                    channel.setReconciliationSyncStatus(syncStatus);
                    channelDao.updateChannel(channel);
                    LOG.info("ReconciliationSyncStatus changed to: {} for channel: {}", syncStatus.name(), channel.getCode());
                    response.setSuccessful(true);
                    if (ActivityContext.current().isEnable()) {
                        ActivityUtils.appendActivity(channel.getCode(), ActivityEntityEnum.CHANNEL.getName(), null,
                                Arrays.asList(new String[] { "Reconciliation was put" + syncStatus.name().toLowerCase() }), ActivityTypeEnum.SYNC_ON_OFF.name());
                    }

                } else {
                    context.addError(WsResponseCode.INVALID_CHANNEL_CONNECTOR);
                }
            }
        }
        if (!response.isSuccessful()) {
            AuditContext.current().setAuditEnabled(false);
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    @MarkDirty(values = { ChannelCache.class })
    @LogActivity
    public ChangeChannelOrderSyncStatusResponse changeOrderSyncStatus(ChangeChannelOrderSyncStatusRequest request) {
        ChangeChannelOrderSyncStatusResponse response = new ChangeChannelOrderSyncStatusResponse();
        ValidationContext context = request.validate();
        Channel channel = getChannelByCode(request.getChannelCode());
        if (!context.hasErrors()) {
            if (channel == null) {
                context.addError(WsResponseCode.INVALID_CHANNEL_CODE);
            } else if (SyncStatus.NOT_AVAILABLE.equals(channel.getOrderSyncStatus())) {
                context.addError(WsResponseCode.INVALID_CHANNEL_CONNECTOR);
            } else {
                SyncStatus syncStatus = SyncStatus.valueOf(request.getStatusCode());
                if (SyncStatus.ON.equals(syncStatus) || SyncStatus.OFF.equals(syncStatus)) {
                    channel.setOrderSyncStatus(syncStatus);
                    if (SyncStatus.OFF.equals(channel.getOrderSyncStatus()) && SyncStatus.ON.equals(channel.getInventorySyncStatus())) {
                        LOG.info("InventorySyncStatus changed to: {} for channel: {}", syncStatus.name(), channel.getCode());
                        channel.setInventorySyncStatus(SyncStatus.OFF);
                        if (ActivityContext.current().isEnable()) {
                            ActivityUtils.appendActivity(channel.getCode(), ActivityEntityEnum.CHANNEL.getName(), channel.getSourceCode(),
                                    Arrays.asList(new String[] { "Inventory sync was put " + syncStatus.name().toLowerCase() + " due to disabling order sync" }),
                                    ActivityTypeEnum.SYNC_ON_OFF.name());
                        }
                    }
                    channelDao.updateChannel(channel);
                    LOG.info("OrderSyncStatus changed to: {} for channel: {}", syncStatus.name(), channel.getCode());
                    response.setSuccessful(true);
                    if (ActivityContext.current().isEnable()) {
                        ActivityUtils.appendActivity(channel.getCode(), ActivityEntityEnum.CHANNEL.getName(), channel.getSourceCode(),
                                Arrays.asList(new String[] { "Order sync was put" + syncStatus.name().toLowerCase() }), ActivityTypeEnum.SYNC_ON_OFF.name());
                    }
                } else {
                    context.addError(WsResponseCode.INVALID_CHANNEL_CONNECTOR);
                }
            }
        }
        if (!response.isSuccessful()) {
            AuditContext.current().setAuditEnabled(false);
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    @MarkDirty(values = { ChannelCache.class })
    @LogActivity
    public ChangeChannelInventorySyncStatusResponse changeInventorySyncStatus(ChangeChannelInventorySyncStatusRequest request) {
        ChangeChannelInventorySyncStatusResponse response = new ChangeChannelInventorySyncStatusResponse();
        ValidationContext context = request.validate();
        Channel channel = getChannelByCode(request.getChannelCode());
        if (!context.hasErrors()) {
            if (channel == null) {
                context.addError(WsResponseCode.INVALID_CHANNEL_CODE);
            } else {
                Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(channel.getSourceCode());
                if (!source.isInventorySyncConfigured()) {
                    context.addError(WsResponseCode.INVALID_CHANNEL_CONNECTOR);
                } else {
                    SyncStatus syncStatus = SyncStatus.valueOf(request.getStatusCode());
                    if (SyncStatus.ON.equals(syncStatus) && SyncStatus.OFF.equals(channel.getInventorySyncStatus()) && SyncStatus.OFF.equals(channel.getOrderSyncStatus())) {
                        context.addError(WsResponseCode.INVALID_STATE, "To enable inventory sync, order sync should be enabled.");
                    } else if (SyncStatus.ON.equals(syncStatus) || SyncStatus.OFF.equals(syncStatus)) {
                        channel.setInventorySyncStatus(syncStatus);
                        channelDao.updateChannel(channel);
                        LOG.info("InventorySyncStatus changed to: {} for channel: {}", syncStatus.name(), channel.getCode());
                        response.setSuccessful(true);
                        if (ActivityContext.current().isEnable()) {
                            ActivityUtils.appendActivity(channel.getCode(), ActivityEntityEnum.CHANNEL.getName(), channel.getSourceCode(),
                                    Arrays.asList(new String[] { "Inventory sync was put " + syncStatus.name().toLowerCase() }), ActivityTypeEnum.SYNC_ON_OFF.name());
                        }
                    } else {
                        context.addError(WsResponseCode.INVALID_CHANNEL_CONNECTOR);
                    }
                }
            }
        }
        if (!response.isSuccessful()) {
            AuditContext.current().setAuditEnabled(false);
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    @MarkDirty(values = { ChannelCache.class })
    @LogActivity
    public ChangeChannelPricingSyncStatusResponse changeChannelPricingSyncStatus(ChangeChannelPricingSyncStatusRequest request) {
        ChangeChannelPricingSyncStatusResponse response = new ChangeChannelPricingSyncStatusResponse();
        ValidationContext context = request.validate();
        Channel channel = getChannelByCode(request.getChannelCode());
        if (!context.hasErrors()) {
            if (channel == null) {
                context.addError(WsResponseCode.INVALID_CHANNEL_CODE);
            } else {
                Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(channel.getSourceCode());
                if (!source.isPricingSyncConfigured()) {
                    context.addError(WsResponseCode.INVALID_CHANNEL_CONNECTOR);
                } else {
                    SyncStatus syncStatus = SyncStatus.valueOf(request.getStatusCode());
                    if (SyncStatus.ON.equals(syncStatus) || SyncStatus.OFF.equals(syncStatus)) {
                        channel.setPricingSyncStatus(syncStatus);
                        channelDao.updateChannel(channel);
                        LOG.info("PricingSyncStatus changed to: {} for channel: {}", syncStatus.name(), channel.getCode());
                        response.setSuccessful(true);
                        if (ActivityContext.current().isEnable()) {
                            ActivityUtils.appendActivity(channel.getCode(), ActivityEntityEnum.CHANNEL.getName(), channel.getSourceCode(),
                                    Arrays.asList(new String[] { "Pricing sync was put " + syncStatus.name().toLowerCase() }), ActivityTypeEnum.SYNC_ON_OFF.name());
                        }
                    } else {
                        context.addError(WsResponseCode.INVALID_CHANNEL_CONNECTOR);
                    }
                }
            }
        }
        if (!response.isSuccessful()) {
            AuditContext.current().setAuditEnabled(false);
        }
        response.setErrors(context.getErrors());
        return null;
    }

    @Override
    @Transactional
    @RollbackOnFailure
    @MarkDirty(values = { ChannelCache.class })
    @LogActivity
    public EditChannelResponse editChannel(EditChannelRequest request) {
        EditChannelResponse response = new EditChannelResponse();
        ValidationContext context = request.validate();
        Channel dummyChannel = null;
        Channel channel = getChannelByCode(constructChannelCodeByName(request.getWsChannel().getChannelName()));
        if (!context.hasErrors()) {
            if (channel == null) {
                context.addError(WsResponseCode.CHANNEL_NOT_CONFIGURED, WsResponseCode.CHANNEL_NOT_CONFIGURED.message() + ": " + request.getWsChannel().getChannelName());
            } else {
                dummyChannel = createDummyChannelObject(channel, false);
            }
        }

        if (!context.hasErrors() && request.getWsChannel().getPackageType() != null) {
            if (!(request.getWsChannel().getPackageType().equals(SaleOrderItem.PackageType.FIXED.name())
                    || request.getWsChannel().getPackageType().equals(SaleOrderItem.PackageType.FLEXIBLE.name()))) {
                context.addError(WsResponseCode.INVALID_PACKAGE_SPECIFICATIONS, "Invalid value for package type");
            }
        }

        Customer customer = null;
        if (!context.hasErrors() && StringUtils.isNotBlank(request.getWsChannel().getCustomerCode())) {
            customer = customerService.getCustomerByCode(request.getWsChannel().getCustomerCode());
            if (customer == null) {
                context.addError(WsResponseCode.INVALID_CUSTOMER_CODE, "Invalid value for customer code");
            }
        }

        if (channel.getOrderSyncStatus().equals(SyncStatus.OFF) && request.getWsChannel().getOrderSyncStatus().equals(SyncStatus.OFF.name())
                && request.getWsChannel().getInventorySyncStatus().equals(SyncStatus.ON.name())) {
            response.addWarning(new WsWarning(WsResponseCode.INVALID_STATE.code(), "Please enable order sync for enabling inventory sync"));
        }

        BillingParty billingParty = null;
        if (!context.hasErrors() && StringUtils.isNotBlank(request.getWsChannel().getBillingPartyCode())) {
            billingParty = billingPartyService.getBillingPartyByCode(request.getWsChannel().getBillingPartyCode());
            if (billingParty == null) {
                context.addError(WsResponseCode.INVALID_BILLING_PARTY_CODE, "Invalid billing party code");
            }
        }

        if (!context.hasErrors() && StringUtils.isNotBlank(request.getWsChannel().getInventoryUpdateFormula())) {
            try {
                Expression.compile(request.getWsChannel().getInventoryUpdateFormula());
            } catch (Exception e) {
                context.addError(WsResponseCode.INVALID_CHANNEL_INVENTORY_UPDATE_FORMULA, "INVALID_CHANNEL_INVENTORY_UPDATE_FORMULA: " + e.getMessage());
            }
        }

        SyncStatus syncStatus = null;
        if (!context.hasErrors() && StringUtils.isNotBlank(request.getWsChannel().getOrderSyncStatus())) {
            syncStatus = SyncStatus.valueOf(request.getWsChannel().getOrderSyncStatus());
            if (syncStatus.equals(SyncStatus.NOT_AVAILABLE) && !channel.getOrderSyncStatus().equals(syncStatus)) {
                context.addError(WsResponseCode.INVALID_CHANNEL_CONNECTOR);
            } else if (SyncStatus.ON.equals(syncStatus) || SyncStatus.OFF.equals(syncStatus)) {
                channel.setOrderSyncStatus(syncStatus);
            }
        }

        if (!context.hasErrors() && channel.getOrderSyncStatus().equals(SyncStatus.OFF)) {
            channel.setInventorySyncStatus(SyncStatus.OFF);
        } else if (!context.hasErrors() && StringUtils.isNotBlank(request.getWsChannel().getInventorySyncStatus())) {
            syncStatus = SyncStatus.valueOf(request.getWsChannel().getInventorySyncStatus());
            if (syncStatus.equals(SyncStatus.NOT_AVAILABLE) && !channel.getInventorySyncStatus().equals(syncStatus)) {
                context.addError(WsResponseCode.INVALID_CHANNEL_CONNECTOR);
            } else if (SyncStatus.ON.equals(syncStatus) || SyncStatus.OFF.equals(syncStatus)) {
                channel.setInventorySyncStatus(syncStatus);
            }
        }

        if (!context.hasErrors() && StringUtils.isNotBlank(request.getWsChannel().getOrderReconciliationSyncStatus())) {
            syncStatus = SyncStatus.valueOf(request.getWsChannel().getOrderReconciliationSyncStatus());
            if (syncStatus.equals(SyncStatus.NOT_AVAILABLE) && !channel.getReconciliationSyncStatus().equals(syncStatus)) {
                context.addError(WsResponseCode.INVALID_CHANNEL_CONNECTOR);
            } else if (SyncStatus.ON.equals(syncStatus) || SyncStatus.OFF.equals(syncStatus)) {
                channel.setReconciliationSyncStatus(syncStatus);
            }
        }

        if (!context.hasErrors() && StringUtils.isNotBlank(request.getWsChannel().getPricingSyncStatus())) {
            syncStatus = SyncStatus.valueOf(request.getWsChannel().getPricingSyncStatus());
            if (syncStatus.equals(SyncStatus.NOT_AVAILABLE) && !channel.getPricingSyncStatus().equals(syncStatus)) {
                context.addError(WsResponseCode.INVALID_CHANNEL_CONNECTOR);
            } else if (SyncStatus.ON.equals(syncStatus) || SyncStatus.OFF.equals(syncStatus)) {
                channel.setPricingSyncStatus(syncStatus);
            }
        }

        if (context.hasErrors()) {
            response.addErrors(context.getErrors());
        } else {
            Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(channel.getSourceCode());
            channel.setName(request.getWsChannel().getChannelName());
            channel.setEnabled(request.getWsChannel().isEnabled());
            channel.setThirdPartyShipping(request.getWsChannel().isThirdPartyShipping());
            channel.setBillingParty(billingParty);

            if (com.uniware.core.entity.Source.Type.B2B.equals(source.getType())) {
                channel.setCustomer(customer);
            }

            if (StringUtils.isNotBlank(request.getWsChannel().getLedgerName())) {
                channel.setLedgerName(request.getWsChannel().getLedgerName());
            } else {
                channel.setLedgerName(channel.getName());
            }

            Map<String, Object> customFieldValues = CustomFieldUtils.getCustomFieldValues(context, Channel.class.getName(), request.getWsChannel().getCustomFieldValues());
            CustomFieldUtils.setCustomFieldValues(channel, customFieldValues);
            if (request.getWsChannel().getChannelConfigurationParameters() != null) {
                setChannelConfigurationParameters(context, channel, request.getWsChannel(), false);
            }
            channelDao.updateChannel(channel);
            ChannelDetailDTO channelDetailDTO = new ChannelDetailDTO(channel, source);
            prepareChannelConnectorDTO(channel, source, channelDetailDTO);
            prepareChannelConfigurationParametersDTO(channelDetailDTO, channel);
            prepareSourceConfigurationParametersDTO(channelDetailDTO.getSource(), source);
            channelDetailDTO.setCustomFieldValues(CustomFieldUtils.getCustomFieldValuesDTO(channel));
            response.setChannelDetailDTO(channelDetailDTO);
            response.setSuccessful(true);
            if (ActivityContext.current().isEnable()) {
                HashMap<String, ArrayList<String>> entityDifferenceMap = new HashMap<String, ArrayList<String>>();
                ActivityUtils.getObjectDiff(dummyChannel, channel, entityDifferenceMap);
                String channelActivityLogs = ActivityUtils.getChannelSpecificActivityLogs(entityDifferenceMap);
                if (StringUtils.isNotBlank(channelActivityLogs)) {
                    ActivityUtils.appendActivity(channel.getCode(), ActivityEntityEnum.CHANNEL.getName(), channel.getSourceCode(),
                            Arrays.asList(new String[] { channelActivityLogs }), ActivityTypeEnum.CONFIGURATION_PARAM_CHANGED.name());
                }
            }
        }
        if (!response.isSuccessful()) {
            AuditContext.current().setAuditEnabled(false);
        }
        return response;
    }

    private Channel createDummyChannelObject(Channel channel, boolean isEditConnector) {
        Channel dummyChannel = new Channel();
        SourceConfiguration sourceConfiguration = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class);
        Source source = sourceConfiguration.getSourceByCode(channel.getSourceCode());
        dummyChannel.setName(channel.getName());
        dummyChannel.setBillingParty(channel.getBillingParty());
        Set<ChannelConfigurationParameter> configParams = new LinkedHashSet<>();
        for (ChannelConfigurationParameter parameter : channel.getChannelConfigurationParameters()) {
            SourceConfigurationParameter sourceConfigurationParameter = sourceConfiguration.getSourceConfigurationParameter(channel.getSourceCode(),
                    parameter.getSourceConfigurationParameterName());
            if (!sourceConfigurationParameter.getType().equals(Type.HIDDEN) && isSourceConfigurationParameterApplicable(source, sourceConfigurationParameter.getName())) {
                ChannelConfigurationParameter clone = new ChannelConfigurationParameter();
                clone.setId(parameter.getId());
                clone.setChannel(parameter.getChannel());
                clone.setSourceConfigurationParameterName(parameter.getSourceConfigurationParameterName());
                clone.setValue(parameter.getValue());
                clone.setCreated(parameter.getCreated());
                clone.setUpdated(parameter.getUpdated());
                configParams.add(clone);
            }
        }
        dummyChannel.setChannelConfigurationParameters(configParams);
        Set<ChannelConnector> channelConnectors = new LinkedHashSet<>();
        if (isEditConnector) {
            for (ChannelConnector connector : channel.getChannelConnectors()) {
                ChannelConnector cloneConnector = new ChannelConnector();
                cloneConnector.setId(connector.getId());
                cloneConnector.setChannel(connector.getChannel());
                cloneConnector.setSourceConnectorName(connector.getSourceConnectorName());
                cloneConnector.setStatusCode(connector.getStatusCode());
                cloneConnector.setFailedCount(connector.getFailedCount());
                cloneConnector.setErrorMessage(connector.getErrorMessage());
                Set<ChannelConnectorParameter> connectorParams = new LinkedHashSet<>();
                for (ChannelConnectorParameter connectorParameter : connector.getChannelConnectorParameters()) {
                    ChannelConnectorParameter cloneConnParam = new ChannelConnectorParameter();
                    cloneConnParam.setId(connectorParameter.getId());
                    cloneConnParam.setChannelConnector(cloneConnector);
                    cloneConnParam.setName(connectorParameter.getName());
                    cloneConnParam.setValue(connectorParameter.getValue());
                    connectorParams.add(cloneConnParam);
                }
                cloneConnector.setChannelConnectorParameters(connectorParams);
                channelConnectors.add(cloneConnector);
            }
        }
        dummyChannel.setChannelConnectors(channelConnectors);
        Set<ChannelProperty> channelProperties = new LinkedHashSet<>();
        channelProperties.addAll(channel.getChannelProperties());
        dummyChannel.setChannelProperties(channelProperties);
        dummyChannel.setCode(channel.getCode());
        dummyChannel.setColorCode(channel.getColorCode());
        dummyChannel.setCreated(channel.getCreated());
        dummyChannel.setUpdated(channel.getUpdated());
        dummyChannel.setCustomer(channel.getCustomer());
        dummyChannel.setEnabled(channel.isEnabled());
        dummyChannel.setHttpProxyHost(channel.getHttpProxyHost());
        dummyChannel.setHttpProxyPort(channel.getHttpProxyPort());
        dummyChannel.setId(channel.getId());
        dummyChannel.setInventorySyncStatus(channel.getInventorySyncStatus());
        dummyChannel.setInventoryUpdateFormula(channel.getInventoryUpdateFormula());
        dummyChannel.setLedgerName(channel.getLedgerName());
        dummyChannel.setName(channel.getName());
        dummyChannel.setOrderSyncStatus(channel.getOrderSyncStatus());
        dummyChannel.setReconciliationSyncStatus(channel.getReconciliationSyncStatus());
        dummyChannel.setPricingSyncStatus(channel.getPricingSyncStatus());
        dummyChannel.setShortName(channel.getShortName());
        dummyChannel.setSourceCode(channel.getSourceCode());
        dummyChannel.setTenant(channel.getTenant());
        dummyChannel.setThirdPartyShipping(channel.isThirdPartyShipping());
        return dummyChannel;
    }

    @Override
    @LogActivity
    public TestChannelInventoryUpdateFormulaResponse testChannelInventoryUpdateFormula(TestChannelInventoryUpdateFormulaRequest request) {
        TestChannelInventoryUpdateFormulaResponse response = new TestChannelInventoryUpdateFormulaResponse();
        ValidationContext context = request.validate();
        ChannelDetailDTO channelDTO = null;
        Channel channel = getChannelByCode(request.getChannelCode());
        ChannelItemType channelItemType = null;
        if (!context.hasErrors()) {
            channelDTO = CacheManager.getInstance().getCache(ChannelCache.class).getChannelDetailDTOByCode(request.getChannelCode());
            if (channelDTO == null) {
                context.addError(WsResponseCode.INVALID_CHANNEL_CODE);
            } else {
                channelItemType = channelCatalogService.getChannelItemTypeByChannelAndChannelProductId(channelDTO.getId(), request.getChannelProductId());
                if (channelItemType == null || !ChannelItemType.Status.LINKED.equals(channelItemType.getStatusCode())) {
                    context.addError(WsResponseCode.INVALID_ITEM_TYPE);
                }
            }
        }
        if (!context.hasErrors()) {
            try {
                testChannelInventoryUpdateFormulaInternal(request, response, channelDTO, channelItemType);
            } catch (Throwable e) {
                response.setMessage(e.getMessage());
                context.addError(WsResponseCode.INVALID_CHANNEL_INVENTORY_UPDATE_FORMULA, "Invalid formula");
            }
        }
        if (!context.hasErrors()) {
            response.setSuccessful(true);
            String checksum = EncryptionUtils.md5Encode(request.getFormula().trim());
            ChannelInventoryFormulaVO channelInventoryFormula = channelInventorySyncService.getChannelInventoryFormulaByChecksum(checksum);
            if (channelInventoryFormula == null) {
                channelInventoryFormula = new ChannelInventoryFormulaVO();
                channelInventoryFormula.setCode(StringUtils.getRandomAlphaNumeric(8));
                channelInventoryFormula.setChecksum(checksum);
                channelInventoryFormula.setFormula(request.getFormula().trim());
                channelInventoryFormula.setCreated(DateUtils.getCurrentTime());
                channelInventorySyncService.create(channelInventoryFormula);
            }
            channel.setInventoryUpdateFormula(channelInventoryFormula.getCode());
            updateChannel(channel);
            channelCatalogService.markAllChannelItemTypesForRecalculation(channel.getId());
            if (ActivityContext.current().isEnable()) {
                ActivityUtils.appendActivity(channel.getCode(), ActivityEntityEnum.CHANNEL.getName(), channel.getSourceCode(),
                        Arrays.asList(new String[] { "Inventory formula was updated" }), ActivityTypeEnum.CONFIGURATION_PARAM_CHANGED.name());
            }

        } else {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    @Transactional
    private void testChannelInventoryUpdateFormulaInternal(TestChannelInventoryUpdateFormulaRequest request, TestChannelInventoryUpdateFormulaResponse response,
            ChannelDetailDTO channelDTO, ChannelItemType channelItemType) {
        InventorySnapshotDTO inventorySnapshot = inventoryService.getItemTypeInventoryNotification(channelItemType.getItemType().getId());
        ItemType itemType = catalogService.getNonBundledItemTypeById(channelItemType.getItemType().getId());
        Long unprocessedOrderInventory = saleOrderService.getUnprocessedSaleOrderItemCountByChannelProductId(channelItemType.getChannel().getId(),
                channelItemType.getChannelProductId());
        CalculateChannelItemTypeInventoryResponse calculateInventoryResponse = channelInventorySyncService.calculateChannelItemTypeInventory(channelDTO,
                request.getFormula().trim(), inventorySnapshot, itemType, channelItemType, unprocessedOrderInventory);
        if (calculateInventoryResponse.isSuccessful()) {
            response.setCalculatedInventory(calculateInventoryResponse.getCalculatedInventory());
        }
    }

    @Override
    @Transactional
    @MarkDirty(values = { ChannelCache.class })
    public Channel updateChannel(Channel channel) {
        return channelDao.updateChannel(channel);
    }

    @Override
    @Transactional
    @MarkDirty(values = { ChannelCache.class })
    public List<Channel> disableBillingPartyOnAllChannels(BillingParty billingParty) {
        List<Channel> updatedChannels = new ArrayList<Channel>();
        for (Channel channel : getAllChannels()) {
            BillingParty bp = channel.getBillingParty();
            if (bp != null && bp.getId().equals(billingParty.getId())) {
                channel.setBillingParty(null);
                updatedChannels.add(channel);
                channelDao.updateChannel(channel);
            }
        }
        LOG.info("Billing Party " + billingParty.getName() + " has been disabled on " + updatedChannels.size() + " channels");
        return updatedChannels;
    }

    @Override
    @Transactional
    public List<Channel> getAllChannels() {
        return channelDao.getChannels();
    }

    @Override
    @Transactional
    public List<Source> getAllSources() {
        return sourceMao.getAllSources();
    }

    @Override
    @Transactional
    public List<SourceSearchDTO> lookupSource(String keyword) {
        List<SourceSearchDTO> sources = new ArrayList<>();
        for (Source source : sourceMao.lookupSources(keyword)) {
            SourceSearchDTO sourceDTO = new SourceSearchDTO();
            sourceDTO.setCode(source.getCode());
            sourceDTO.setName(source.getName());
            sources.add(sourceDTO);
        }
        return sources;
    }

    private String constructChannelCodeByName(String channelName) {
        return channelName.replaceAll("\\W", "_").toUpperCase();
    }

    @Override
    @Transactional
    @MarkDirty(values = { ChannelCache.class })
    public ChannelConnector updateChannelConnector(ChannelConnector channelConnector) {
        return channelDao.updateChannelConnector(channelConnector);
    }

    @Override
    public List<ChannelOrderSyncStatusDTO> getChannelOrderSyncStatuses() {
        return channelMao.getChannelOrderSyncStatuses();
    }

    @Override
    public ChannelOrderSyncStatusDTO getChannelOrderSyncStatus(String channelCode) {
        ChannelOrderSyncStatusDTO orderSyncStatusDTO = channelMao.getChannelOrderSyncStatus(channelCode);
        if (null == orderSyncStatusDTO) {
            orderSyncStatusDTO = updateChannelOrderSyncStatus(new ChannelOrderSyncStatusDTO(channelCode));
        }
        return orderSyncStatusDTO;
    }

    @Override
    public List<ChannelPricingPushStatusDTO> getChannelPricingSyncStatuses() {
        return channelMao.getChannelPriceSyncStatuses();
    }

    @Override
    public ChannelPricingPushStatusDTO getChannelPricingSyncStatus(String channelCode) {
        ChannelPricingPushStatusDTO priceSyncStatus = channelMao.getChannelPriceSyncStatus(channelCode);
        if (null == priceSyncStatus) {
            priceSyncStatus = updateChannelPricingSyncStatus(new ChannelPricingPushStatusDTO(channelCode));
        }
        return priceSyncStatus;
    }

    @Override
    public List<ChannelInventorySyncStatusDTO> getChannelInventorySyncStatuses() {
        return channelMao.getChannelInventorySyncStatuses();
    }

    @Override
    public ChannelInventorySyncStatusDTO getChannelInventorySyncStatus(String channelCode) {
        ChannelInventorySyncStatusDTO inventorySyncStatusDTO = channelMao.getChannelInventorySyncStatus(channelCode);
        if (null == inventorySyncStatusDTO) {
            inventorySyncStatusDTO = updateChannelInventorySyncStatus(new ChannelInventorySyncStatusDTO(channelCode));
        }
        return inventorySyncStatusDTO;
    }

    @Override
    public List<ChannelCatalogSyncStatusDTO> getChannelCatalogSyncStatuses() {
        return channelMao.getChannelCatalogSyncStatuses();
    }

    @Override
    public ChannelCatalogSyncStatusDTO getChannelCatalogSyncStatus(String channelCode) {
        ChannelCatalogSyncStatusDTO channelCatalogSyncStatusDTO = channelMao.getChannelCatalogSyncStatus(channelCode);
        if (null == channelCatalogSyncStatusDTO) {
            channelCatalogSyncStatusDTO = updateChannelCatalogSyncStatus(new ChannelCatalogSyncStatusDTO(channelCode));
        }
        return channelCatalogSyncStatusDTO;
    }

    @Override
    public ChannelWarehouseInventorySyncStatusVO getChannelWarehouseInventorySyncStatus(String channelCode) {
        ChannelWarehouseInventorySyncStatusVO channelWarehouseInventorySyncStatus = channelMao.getChannelWarehouseInventorySyncStatus(channelCode);
        if (null == channelWarehouseInventorySyncStatus) {
            channelWarehouseInventorySyncStatus = updateChannelWarehouseInventorySyncStatus(new ChannelWarehouseInventorySyncStatusVO(channelCode));
        }
        return channelWarehouseInventorySyncStatus;
    }

    @Override
    public List<ChannelOrderStatusDTO> getChannelShipmentSyncStatuses() {
        return channelMao.getChannelShipmentSyncStatuses();
    }

    @Override
    public ChannelOrderStatusDTO getOrderSyncStatusOnChannel(String channelCode) {
        ChannelOrderStatusDTO orderStatusDTO = channelMao.getOrderSyncStatusOnChannel(channelCode);
        if (null == orderStatusDTO) {
            orderStatusDTO = updateChannelShipmentSyncStatus(new ChannelOrderStatusDTO(channelCode));
        }
        return orderStatusDTO;
    }

    @Override
    public ChannelInventorySyncStatusDTO updateChannelInventorySyncStatus(ChannelInventorySyncStatusDTO inventorySyncStatusDTO) {
        LOG.info("Updating inventory sync status in mongo for channel {}", inventorySyncStatusDTO.getChannelCode());
        channelMao.save(inventorySyncStatusDTO);
        LOG.info("Updated inventory sync status in mongo for channel {}", inventorySyncStatusDTO.getChannelCode());
        return inventorySyncStatusDTO;
    }

    @Override
    public ChannelCatalogSyncStatusDTO updateChannelCatalogSyncStatus(ChannelCatalogSyncStatusDTO catalogSyncStatusDTO) {
        LOG.info("Updating catalog sync status in mongo for channel {}", catalogSyncStatusDTO.getChannelCode());
        channelMao.save(catalogSyncStatusDTO);
        LOG.info("Updated catalog sync status in mongo for channel {}", catalogSyncStatusDTO.getChannelCode());
        return catalogSyncStatusDTO;
    }

    @Override
    public ChannelWarehouseInventorySyncStatusVO updateChannelWarehouseInventorySyncStatus(ChannelWarehouseInventorySyncStatusVO channelWarehouseInventorySyncStatus) {
        LOG.info("Updating channel warehouse inventory sync status in mongo for channel {}", channelWarehouseInventorySyncStatus.getChannelCode());
        channelMao.save(channelWarehouseInventorySyncStatus);
        LOG.info("Updated channel warehouse inventory sync status in mongo for channel {}", channelWarehouseInventorySyncStatus.getChannelCode());
        return channelWarehouseInventorySyncStatus;
    }

    @Override
    public ChannelPricingPushStatusDTO updateChannelPricingSyncStatus(ChannelPricingPushStatusDTO pricingSyncStatusDTO) {
        LOG.info("Updating pricing sync status in mongo for channel {}", pricingSyncStatusDTO.getChannelCode());
        channelMao.save(pricingSyncStatusDTO);
        LOG.info("Updated pricing sync status in mongo for channel {}", pricingSyncStatusDTO.getChannelCode());
        return pricingSyncStatusDTO;
    }

    @Override
    public ChannelOrderStatusDTO updateChannelShipmentSyncStatus(ChannelOrderStatusDTO shipmentSyncStatusDTO) {
        return channelMao.save(shipmentSyncStatusDTO);
    }

    @Override
    public ChannelReconciliationInvoiceSyncStatusVO getChannelReconciliationSyncStatus(final String channelCode) {
        ChannelReconciliationInvoiceSyncStatusVO syncStatusVO = channelMao.getChannelReconciliationSyncStatus(channelCode);
        if (syncStatusVO == null) {
            syncStatusVO = new ChannelReconciliationInvoiceSyncStatusVO(channelCode);
        }
        return syncStatusVO;
    }

    @Override
    public ChannelReconciliationInvoiceSyncStatusVO updateChannelReconciliationInvoiceSyncStatus(ChannelReconciliationInvoiceSyncStatusVO channelStatus) {
        return channelMao.save(channelStatus);
    }

    @Override
    public void clearChannelOrderSyncStatuses() {
        channelMao.clearChannelOrderSyncStatuses();
    }

    @Override
    public void clearChannelInventorySyncStatuses() {
        channelMao.clearChannelInventorySyncStatuses();
    }

    @Override
    public void clearChannelCatalogSyncStatuses() {
        channelMao.clearChannelCatalogSyncStatuses();
    }

    @Override
    public void clearChannelShipmentSyncStatuses() {
        channelMao.clearChannelShipmentSyncStatuses();
    }

    @Override
    public GetChannelsForSaleOrderImportResponse getChannelsForOrderImport(GetChannelsForSaleOrderImportRequest request) {
        GetChannelsForSaleOrderImportResponse response = new GetChannelsForSaleOrderImportResponse();
        List<ChannelSaleOrderImportDTO> channels = new ArrayList<>();
        List<SourceSaleOrderImportInstructionsVO> sourceSaleOrderImportInstructions = channelMao.getSourceImportInstructionsByType(request.getImportType());
        Map<String, String> sourceToInstructions = new HashMap<>();
        for (SourceSaleOrderImportInstructionsVO sourceSaleOrderImportInstruction : sourceSaleOrderImportInstructions) {
            sourceToInstructions.put(sourceSaleOrderImportInstruction.getSourceCode(), sourceSaleOrderImportInstruction.getInstructions());
        }
        ChannelCache channelCache = CacheManager.getInstance().getCache(ChannelCache.class);
        for (ChannelDetailDTO channelDetailDTO : channelCache.getChannels()) {
            if (request.getImportType().equalsIgnoreCase(SourceSaleOrderImportInstructionsVO.ImportType.SALE_ORDER.name())) {
                if ("CUSTOM".equals(channelDetailDTO.getSource().getCode()) || (com.uniware.core.entity.Source.Type.B2B.name().equals(channelDetailDTO.getSource().getType())
                        && CacheManager.getInstance().getCache(ChannelCache.class).getScriptName(channelDetailDTO.getCode(),
                                com.uniware.core.entity.Source.SALE_ORDER_PRE_PROCESSING_SCRIPT_NAME) != null)) {
                    ChannelSaleOrderImportDTO channel = new ChannelSaleOrderImportDTO(channelDetailDTO);
                    channel.setInstructions(sourceToInstructions.get(channelDetailDTO.getSource().getCode()));
                    channels.add(channel);
                }
            } else if (request.getImportType().equalsIgnoreCase(SourceSaleOrderImportInstructionsVO.ImportType.RECONCILIATION.name())) {
                if (channelDetailDTO.getOrderReconciliationSyncStatus().equals(Channel.SyncStatus.ON.name())
                        && CacheManager.getInstance().getCache(ChannelCache.class).getScriptName(channelDetailDTO.getCode(),
                                com.uniware.core.entity.Source.ORDER_ITEM_PRERECONCILIATION_SCRIPT_NAME) != null) {
                    ChannelSaleOrderImportDTO channel = new ChannelSaleOrderImportDTO(channelDetailDTO);
                    channel.setInstructions(sourceToInstructions.get(channelDetailDTO.getSource().getCode()));
                    channels.add(channel);
                }
            }
        }
        response.setChannels(channels);
        response.setSuccessful(true);
        return response;
    }

    @Override
    public String constructChannelShortName(Source source, String name) {
        String sourceCode = source.getCode();
        String reservedKeywords = source.getReservedKeywordsForShortName();
        List<String> reservedWords = new ArrayList<>();
        if (reservedKeywords != null) {
            reservedWords.addAll(StringUtils.split(reservedKeywords));
        }
        reservedWords.add(sourceCode.toUpperCase());
        reservedWords.add(UserContext.current().getTenant().getCode().toUpperCase());

        String channelName = name.toUpperCase();
        for (String reservedWord : reservedWords) {
            channelName = channelName.replace(reservedWord.toUpperCase(), "");
        }
        if (channelName.trim().length() < 2) {
            reservedWords.remove(sourceCode.toUpperCase());
            reservedWords.remove(UserContext.current().getTenant().getCode().toUpperCase());
            channelName = name.toUpperCase();
            for (String reservedWord : reservedWords) {
                channelName = channelName.replace(reservedWord, "");
            }
        }
        channelName = channelName.replaceAll("\\W", "");
        char array[] = channelName.toCharArray();
        char[] shortName = new char[2];
        boolean found = false;
        for (int i = 0; i < array.length; i++) {
            for (int j = i + 1; j < array.length; j++) {
                shortName[0] = array[i];
                shortName[1] = array[j];
                if (checkShortNameAvailability(sourceCode, new String(shortName))) {
                    found = true;
                    break;
                }
            }
            if (found) {
                break;
            }
        }
        if (found) {
            return new String(shortName);
        } else {
            int retry = 0;
            while (retry < 20) {
                retry++;
                String generatedName = StringUtils.getRandomAlphaNumeric(2).toUpperCase();
                if (checkShortNameAvailability(sourceCode, generatedName)) {
                    return generatedName;
                }
            }
        }
        return DEFAULT_SHORT_NAME;
    }

    @Override
    @Transactional(readOnly = true)
    public String getChannelColorCodeBySourceCode(String sourceCode) {
        int channelCount = getChannelCountBySourceCode(sourceCode);
        int count = (channelCount % 10) + 1;
        SourceColor sourceColor = sourceMao.getColorBySourceCodeAndCount(sourceCode, count);
        return sourceColor.getColor();
    }

    @Override
    public boolean checkShortNameAvailability(String sourceCode, String shortName) {
        ChannelCache channelCache = CacheManager.getInstance().getCache(ChannelCache.class);
        boolean available = true;
        for (ChannelDetailDTO channelDetailDTO : channelCache.getChannels()) {
            if (channelDetailDTO.getSource().getCode().equals(sourceCode)) {
                if (channelDetailDTO.getShortName().equals(shortName)) {
                    available = false;
                    break;
                }
            }
        }
        return available;
    }

    @Override
    public int getChannelCountBySourceCode(String sourceCode) {
        ChannelCache channelCache = CacheManager.getInstance().getCache(ChannelCache.class);
        int count = 0;
        for (ChannelDetailDTO channelDetailDTO : channelCache.getChannels()) {
            if (channelDetailDTO.getSource().getCode().equals(sourceCode)) {
                count++;
            }
        }
        return count;
    }

    @Override
    public void resetAllSyncStatuses() {
        channelMao.resetChannelOrderSyncStatus();
        channelMao.resetChannelCatalogSyncStatus();
        channelMao.resetStatusSyncOnchannel();
        channelMao.resetChannelReconciliationSyncStatus();
        channelInventorySyncService.resetChannelInventorySyncStatus();
    }

    @Override
    @Transactional(readOnly = true)
    public GetChannelActivitiesResponse getChannelActivities(GetChannelActivitiesRequest request) {
        GetChannelActivitiesResponse response = new GetChannelActivitiesResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            Channel channel = channelDao.getChannelByCode(request.getChannelCode());
            if (channel == null) {
                context.addError(WsResponseCode.INVALID_CHANNEL_CODE);
            } else {
                List<ActivityDTO> activities = new ArrayList<>();
                FetchEntityActivityRequest fetchEntityActivityRequest = new FetchEntityActivityRequest();
                fetchEntityActivityRequest.setEntityName(ActivityEntityEnum.CHANNEL.getName());
                fetchEntityActivityRequest.setEntityIdentifier(channel.getCode());
                fetchEntityActivityRequest.setSort(false);
                FetchEntityActivityResponse fetchEntityActivityResponse = activityLogService.getActivityLogsByIdentifier(fetchEntityActivityRequest);
                if (fetchEntityActivityResponse.isSuccessful()) {
                    for (ActivityMetaVO activityMetaVO : fetchEntityActivityResponse.getEntityActivityLogs()) {
                        ActivityDTO activityDTO = new ActivityDTO();
                        prepareActivityDTO(activityDTO, activityMetaVO);
                        activities.add(activityDTO);
                    }
                }
                Collections.sort(activities);
                response.setActivities(activities);
                response.setSuccessful(true);
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    @Override
    @Transactional(readOnly = true)
    public GetConnectorStatusResponse getChannelConnectorStatus() {
        List<Channel> channelList = channelDao.getChannelConnectorStatus();
        GetConnectorStatusResponse response = new GetConnectorStatusResponse();
        response.setTenantCode(UserContext.current().getTenant().getCode());
        response.setTenantId(UserContext.current().getTenantId().toString());
        List<ChannelConnectorStatus> channelConnectorStatus = new LinkedList<>();
        for (Channel channel : channelList) {
            for (ChannelConnector connector : channel.getChannelConnectors()) {
                ChannelConnectorStatus status = new ChannelConnectorStatus();
                status.setCode(channel.getCode());
                status.setName(channel.getName());
                status.setSourceCode(channel.getSourceCode());
                status.setSourceConnectorName(connector.getSourceConnectorName());
                status.setStatusCode(connector.getStatusCode().name());
                channelConnectorStatus.add(status);
            }
        }
        response.setChannelConnectorStatus(channelConnectorStatus);
        return response;
    }

    @Override
    @Transactional
    @MarkDirty(values = { ChannelCache.class })
    @LogActivity
    @RollbackOnFailure
    public EditChannelAssociatedFacilityResponse editChannelAssociatedFacility(EditChannelAssociatedFacilityRequest request) {
        EditChannelAssociatedFacilityResponse response = new EditChannelAssociatedFacilityResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            Channel channel = getChannelByCode(request.getChannelCode());
            if (channel == null) {
                context.addError(WsResponseCode.INVALID_CHANNEL_CODE, "Invalid Channel Code : " + request.getChannelCode());
            } else if (!channel.isEnabled()) {
                context.addError(WsResponseCode.BAD_CHANNEL_STATE, "Channel is disabled");
            } else if (CollectionUtils.isEmpty(request.getFacilityCodes())) {
                context.addError(WsResponseCode.INVALID_REQUEST, "facility codes required");
            } else {
                List<ChannelAssociatedFacility> channelAssociatedFacilities = channelDao.getChannelAssociatedFacilities(channel.getId());
                Set<String> facilityCodes = new HashSet<>();
                Map<Integer, ChannelAssociatedFacility> facilityIdToAssociatedFacility = new HashMap<>(channelAssociatedFacilities.size());
                channelAssociatedFacilities.forEach(channelAssociatedFacility -> {
                    facilityIdToAssociatedFacility.put(channelAssociatedFacility.getFacility().getId(), channelAssociatedFacility);
                    facilityCodes.add(channelAssociatedFacility.getFacility().getCode());
                });
                request.getFacilityCodes().stream().filter(f -> !context.hasErrors()).forEach(facilityCode -> {
                    Facility facility = CacheManager.getInstance().getCache(FacilityCache.class).getFacilityByCode(facilityCode);
                    if (facility == null) {
                        context.addError(WsResponseCode.INVALID_FACILITY_CODE, "Invalid facility code : " + facilityCode);
                    } else if (!facility.isEnabled()) {
                        context.addError(WsResponseCode.FACILITY_CONFIG_ERROR, "Facility : " + facilityCode + " is Disabled");
                    } else {
                        List<Integer> associatedFacilitiesToRemove = new ArrayList<>();
                        switch (request.getAction()) {
                            case ADD:
                                if (facilityIdToAssociatedFacility.get(facility.getId()) != null) {
                                    context.addError(WsResponseCode.INVALID_REQUEST, "Facility : " + facilityCode + " already associated with channel");
                                } else {
                                    ChannelAssociatedFacility channelAssociatedFacility = new ChannelAssociatedFacility();
                                    channelAssociatedFacility.setChannel(channel);
                                    channelAssociatedFacility.setFacility(facility);
                                    channelAssociatedFacility.setCreated(DateUtils.getCurrentTime());
                                    channelDao.addChannelAssociatedFacility(channelAssociatedFacility);
                                    facilityCodes.add(channelAssociatedFacility.getFacility().getCode());
                                    if (ActivityContext.current().isEnable()) {
                                        ActivityUtils.appendActivity(channel.getCode(), ActivityEntityEnum.CHANNEL.getName(), channel.getCode(),
                                                Arrays.asList(new String[] { "Facility : " + facility.getCode() + " associated with Channel : " + channel.getCode() }),
                                                ActivityTypeEnum.CONFIGURATION_PARAM_CHANGED.name());
                                    }
                                }
                                break;
                            case REMOVE:
                                ChannelAssociatedFacility channelAssociatedFacility = facilityIdToAssociatedFacility.get(facility.getId());
                                if (channelAssociatedFacility == null) {
                                    context.addError(WsResponseCode.INVALID_REQUEST, "Facility : " + facilityCode + " cannot be removed, not associated with channel");
                                } else {
                                    associatedFacilitiesToRemove.add(channelAssociatedFacility.getId());
                                    facilityCodes.remove(channelAssociatedFacility.getFacility().getCode());
                                    if (ActivityContext.current().isEnable()) {
                                        ActivityUtils.appendActivity(channel.getCode(), ActivityEntityEnum.CHANNEL.getName(), channel.getCode(),
                                                Arrays.asList(new String[] { "Facility : " + facility.getCode() + " de-associated with Channel : " + channel.getCode() }),
                                                ActivityTypeEnum.CONFIGURATION_PARAM_CHANGED.name());
                                    }
                                }
                                break;
                        }
                        if (!CollectionUtils.isEmpty(associatedFacilitiesToRemove)) {
                            channelDao.removeChannelAssociatedFacilities(associatedFacilitiesToRemove);
                        }
                    }
                    if (!context.hasErrors()) {
                        List<ChannelAssociatedFacilityDTO> associatedFacilities = new ArrayList<>(facilityCodes.size());
                        facilityCodes.forEach(fCode -> associatedFacilities.add(new ChannelAssociatedFacilityDTO(CacheManager.getInstance().getCache(FacilityCache.class).getFacilityByCode(fCode))));
                        response.setFacilities(associatedFacilities);
                    }
                });
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        } else {
            response.setSuccessful(true);
        }
        return response;
    }

    private void prepareActivityDTO(ActivityDTO activityDTO, ActivityMetaVO activityMetaVO) {
        activityDTO.setActivityType(activityMetaVO.getActivityType());
        activityDTO.setLog(activityMetaVO.getLog());
        activityDTO.setCreated(activityMetaVO.getCreated());
        if (StringUtils.isNotBlank(activityMetaVO.getUsername())) {
            activityDTO.setUsername(activityMetaVO.getUsername());
        } else if (StringUtils.isNotBlank(activityMetaVO.getApiUsername())) {
            activityDTO.setUsername(activityMetaVO.getApiUsername());
        } else {
            activityDTO.setUsername("System");
        }
    }

    private boolean isSourceConfigurationParameterApplicable(Source source, String paramName) {
        switch (paramName) {
            case Channel.TAT:
                return !source.isTatAvailableFromChannel();
            case Channel.SHIPPING_LABEL_AGGREGATION_FORMAT:
                return source.isShippingLabelAggregationConfigured();
            case Channel.PRODUCT_DELISTING_ENABLED:
                return source.isProductDelistingConfigured();
            case Channel.AUTO_VERIFY_ORDERS:
                return source.isAutoVerifyOrders();
            case Channel.SHIPMENT_LABEL_FORMAT:
                return StringUtils.isBlank(source.getShipmentLabelFormat());
            case Channel.ALLOW_COMBINED_MANIFEST:
                return source.isAllowCombinedManifest();
            case Channel.PACKAGE_TYPE:
                return source.isPackageTypeConfigured();
            case Channel.NOTIFICATIONS_ENABLED:
                return source.isNotificationsEnabled();
        }
        return true;
    }

    //Listens to a BillingPartyDisableEvent
    @Override
    public void onApplicationEvent(BillingPartyDisableEvent event) {
        Object source = event.getSource();
        if (source instanceof BillingParty) {
            disableBillingPartyOnAllChannels((BillingParty) source);
        }
    }

    @Override
    @Nullable
    public boolean isFixedGSTOnOtherChargesApplicable(String channelCode) {
        Assert.notNull(channelCode);
        return getChannelByCode(channelCode).deductFixedGstOnOtherCharges();
    }

}
