/*
 *  Copyright 2013 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 14-Jun-2013
 *  @author Sunny
 */
package com.uniware.services.channel;

import com.uniware.core.vo.ChannelCaptchaVO;

public interface IChannelHeartbeatSyncService {

    ChannelCaptchaVO getChannelCaptcha(String channelCode);

    void removeChannelCaptcha(String channelCode);

    void save(ChannelCaptchaVO channelCaptcha1);

    boolean syncChannelHeartbeat(String channelCode);
}
