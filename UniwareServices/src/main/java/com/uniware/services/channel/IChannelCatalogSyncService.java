/*
 *  Copyright 2013 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 14-Jun-2013
 *  @author Sunny
 */
package com.uniware.services.channel;

import com.uniware.core.api.channel.GetChannelCatalogSyncStatusRequest;
import com.uniware.core.api.channel.GetChannelCatalogSyncStatusResponse;
import com.uniware.core.api.channel.SyncChannelCatalogRequest;
import com.uniware.core.api.channel.SyncChannelCatalogResponse;
import com.uniware.core.entity.Channel;
import com.uniware.services.messaging.jms.SyncChannelCatalogEvent;

public interface IChannelCatalogSyncService {

    SyncChannelCatalogResponse syncChannelCatalog(SyncChannelCatalogRequest request);

    GetChannelCatalogSyncStatusResponse getChannelCatalogSyncStatus(GetChannelCatalogSyncStatusRequest request);

    void syncChannelCatalog(SyncChannelCatalogEvent syncChannelCatalogEvent);

    void updateCatalogSyncId(String channelProductId, Channel channel, String syncId);
}
