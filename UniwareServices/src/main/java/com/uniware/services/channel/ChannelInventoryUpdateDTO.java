package com.uniware.services.channel;

import com.uniware.core.entity.Channel;
import com.uniware.core.entity.ChannelItemType;
import com.uniware.core.entity.ItemType;
import com.uniware.core.vo.ChannelInventoryUpdateSnapshotVO;
import com.uniware.core.vo.ChannelItemTypeVO;

public class ChannelInventoryUpdateDTO {

    private Channel                          channel;
    private ItemType                         itemType;
    private ChannelItemType                  channelItemType;
    private ChannelItemTypeVO                channelItemTypeVO;
    private ChannelInventoryUpdateSnapshotVO snapshot;
    private String                           channelSkuCode;
    private String                           channelProductId;
    private int                              calculatedInventory;

    public ChannelInventoryUpdateDTO() {
    }

    public ChannelInventoryUpdateDTO(ChannelItemType cit, Channel c, ItemType it) {
        channel = c;
        itemType = it;
        channelItemType = cit;
        channelSkuCode = cit.getSellerSkuCode();
        channelProductId = cit.getChannelProductId();
        calculatedInventory = cit.getNextInventoryUpdate() != null ? cit.getNextInventoryUpdate() : 0;
    }

    public Channel getChannel() {
        return channel;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }

    public ItemType getItemType() {
        return itemType;
    }

    public void setItemType(ItemType itemType) {
        this.itemType = itemType;
    }

    public ChannelItemType getChannelItemType() {
        return channelItemType;
    }

    public ChannelInventoryUpdateSnapshotVO getSnapshot() {
        return snapshot;
    }

    public void setSnapshot(ChannelInventoryUpdateSnapshotVO snapshot) {
        this.snapshot = snapshot;
    }

    public void setChannelItemType(ChannelItemType channelItemType) {
        this.channelItemType = channelItemType;
    }

    public String getChannelSkuCode() {
        return channelSkuCode;
    }

    public void setChannelSkuCode(String channelSkuCode) {
        this.channelSkuCode = channelSkuCode;
    }

    public String getChannelProductId() {
        return channelProductId;
    }

    public void setChannelProductId(String channelProductId) {
        this.channelProductId = channelProductId;
    }

    public int getCalculatedInventory() {
        return calculatedInventory;
    }

    public void setCalculatedInventory(int calculatedInventory) {
        this.calculatedInventory = calculatedInventory;
    }

    public ChannelItemTypeVO getChannelItemTypeVO() {
        return channelItemTypeVO;
    }

    public void setChannelItemTypeVO(ChannelItemTypeVO channelItemTypeVO) {
        this.channelItemTypeVO = channelItemTypeVO;
    }

}
