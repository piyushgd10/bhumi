/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 29-Sep-2014
 *  @author parijat
 */
package com.uniware.services.channel.impl;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;

import org.jibx.runtime.JiBXException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import com.unifier.core.annotation.audit.LogActivity;
import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.entity.AsyncScriptInstruction;
import com.unifier.core.entity.AsyncScriptInstructionParameter;
import com.unifier.core.jms.MessagingConstants;
import com.unifier.core.transport.http.HttpSender;
import com.unifier.core.utils.CollectionUtils;
import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.DateUtils.Resolution;
import com.unifier.core.utils.EncryptionUtils;
import com.unifier.core.utils.JibxUtils;
import com.unifier.core.utils.StringUtils;
import com.unifier.core.utils.XMLParser;
import com.unifier.core.utils.XMLParser.Element;
import com.unifier.scraper.sl.runtime.IScriptProvider;
import com.unifier.scraper.sl.runtime.ScraperScript;
import com.unifier.scraper.sl.runtime.ScriptExecutionContext;
import com.unifier.services.tasks.IAsyncScriptService;
import com.unifier.services.tasks.engine.callback.IResumable;
import com.unifier.services.tenantprofile.service.ITenantProfileService;
import com.unifier.services.vo.TenantProfileVO;
import com.uniware.core.api.channel.ChannelReconciliationInvoiceSyncStatusVO;
import com.uniware.core.api.channel.CreateChannelReconciliationInvoiceRequest;
import com.uniware.core.api.channel.CreateChannelReconciliationInvoiceResponse;
import com.uniware.core.api.channel.GetChannelReconciliationListResponse;
import com.uniware.core.api.channel.GetChannelReconciliationSyncStatusRequest;
import com.uniware.core.api.channel.GetChannelReconciliationSyncStatusResponse;
import com.uniware.core.api.channel.SyncChannelReconciliationInvoiceRequest;
import com.uniware.core.api.channel.SyncChannelReconciliationInvoiceResponse;
import com.uniware.core.api.channel.WsChannelReconciliationInvoice;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.cache.EnvironmentPropertiesCache;
import com.uniware.core.entity.Channel;
import com.uniware.core.entity.Source;
import com.uniware.core.locking.ILockingService;
import com.uniware.core.locking.Namespace;
import com.uniware.core.locking.annotation.Lock;
import com.uniware.core.locking.annotation.Locks;
import com.uniware.core.utils.ActivityContext;
import com.uniware.core.utils.Constants;
import com.uniware.core.utils.UserContext;
import com.uniware.services.audit.impl.ActivityEntityEnum;
import com.uniware.services.audit.impl.ActivityTypeEnum;
import com.uniware.services.audit.impl.ActivityUtils;
import com.uniware.services.cache.ChannelCache;
import com.uniware.services.cache.ScriptVersionedCache;
import com.uniware.services.channel.IChannelReconciliationInvoiceSyncService;
import com.uniware.services.channel.IChannelService;
import com.uniware.services.configuration.SourceConfiguration;
import com.uniware.services.document.IDocumentService;
import com.uniware.services.messaging.jms.ActiveMQConnector;
import com.uniware.services.messaging.jms.SyncReconciliationInvoiceEvent;
import com.uniware.services.reconciliation.IReconciliationService;
import com.uniware.services.tasks.imports.ChannelReconciliationInvoiceFetchTask;

/**
 * @author parijat
 */
@Service("channelReconciliationInvoiceSyncService")
public class ChannelReconciliationInvoiceSyncServiceImpl implements IChannelReconciliationInvoiceSyncService, IResumable {

    public static final String     TOTAL_PAGES                    = "TotalPages";
    public static final String     CHANNEL_RECONCILIATION_INVOICE = "ChannelReconciliationInvoice";
    public static final String     LINE_SEPARATOR                 = System.getProperty("line.separator");
    private static final Logger    LOG                            = LoggerFactory.getLogger(ChannelReconciliationInvoiceSyncServiceImpl.class);
    private static final String    SERVICE_NAME                   = "channelreconciliationinvoicesyncservice";
    private HttpSender             httpSender                     = new HttpSender();

    @Autowired
    private IAsyncScriptService    asyncScriptTaskService;

    @Autowired
    private IChannelService        channelService;

    @Autowired
    private ILockingService        lockingService;

    @Autowired
    private ITenantProfileService  tenantProfileService;

    @Autowired
    private IReconciliationService reconciliationService;

    @Autowired
    private ApplicationContext     applicationContext;

    @Autowired
    private IDocumentService       documentService;

    @Autowired
    @Qualifier("activeMQConnector1")
    private ActiveMQConnector      activeMQConnector;

    @Override
    public GetChannelReconciliationSyncStatusResponse getChannelCatalogSyncStatus(GetChannelReconciliationSyncStatusRequest request) {
        GetChannelReconciliationSyncStatusResponse response = new GetChannelReconciliationSyncStatusResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            response.setStatusVO(channelService.getChannelReconciliationSyncStatus(request.getChannelCode()));
            response.setSuccessful(true);
        } else {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    @Override
    @Locks({ @Lock(ns = Namespace.CHANNEL_RECONCILIATION_INVOICE_SYNC, key = "#{#args[0].channelCode}") })
    public SyncChannelReconciliationInvoiceResponse syncChannelReconciliationInvoices(SyncChannelReconciliationInvoiceRequest request) {
        SyncChannelReconciliationInvoiceResponse response = new SyncChannelReconciliationInvoiceResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            Channel channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelByCode(request.getChannelCode());
            Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(channel.getSourceCode());
            boolean alreadyRunning = false;
            ChannelReconciliationInvoiceSyncStatusVO syncStatusVO = channelService.getChannelReconciliationSyncStatus(request.getChannelCode());
            if (syncStatusVO.getSyncExecutionStatus() != null && syncStatusVO.getSyncExecutionStatus().equals(Channel.SyncExecutionStatus.RUNNING)) {
                if (source.isAsyncReconciliationSyncSupported()) {
                    LOG.info("Checking if there is any pending reconciliation async instruction for channel {}", channel.getCode());
                    AsyncScriptInstruction asyncScriptInstruction = asyncScriptTaskService.getPendingAsyncScriptInstruction("channel", channel.getCode(),
                            ChannelReconciliationInvoiceSyncServiceImpl.class.getName());
                    if (asyncScriptInstruction != null) {
                        alreadyRunning = true;
                    }
                } else {
                    alreadyRunning = true;
                }
                if (alreadyRunning) {
                    LOG.error("channelReconciliationInvoiceSync for channel {} is already running.", channel.getCode());
                    response.getResultItems().add(new SyncChannelReconciliationInvoiceResponse.ChannelReconciliationInvoiceDTO(request.getChannelCode(), "Sync Already Running"));
                    context.addError(WsResponseCode.ALREADY_RUNNING);
                }
            }
            if (!context.hasErrors()) {
                if (syncStatusVO.isSuccessful() && syncStatusVO.getLastSyncTime() != null
                        && DateUtils.diff(DateUtils.getCurrentTime(), syncStatusVO.getLastSyncTime(), Resolution.MINUTE) < 60) {
                    LOG.error("channelReconciliationInvoiceSync Task had successfully run in last 60 minutes for channel {}. Ignoring....", channel.getCode());
                    response.getResultItems().add(new SyncChannelReconciliationInvoiceResponse.ChannelReconciliationInvoiceDTO(request.getChannelCode(),
                            "RECONCILIATION_INVOICE_SYNC_CAN_RUN_ONCE_IN_HOUR"));
                    context.addError(WsResponseCode.RECONCILIATION_INVOICE_SYNC_CAN_RUN_ONCE_IN_HOUR);
                } else if (!Channel.SyncStatus.ON.equals(channel.getReconciliationSyncStatus())) {
                    LOG.error("channelReconciliationInvoiceSync not active for channel {}.", channel.getCode());
                    response.getResultItems().add(new SyncChannelReconciliationInvoiceResponse.ChannelReconciliationInvoiceDTO(request.getChannelCode(), "Sync not active"));
                    context.addError(WsResponseCode.INVALID_REQUEST, "Sync not active");
                } else {
                    syncStatusVO.reset();
                    syncStatusVO.setSyncExecutionStatus(Channel.SyncExecutionStatus.QUEUED);
                    syncStatusVO.setMessage("Request queued");
                    updateChannelReconciliationInvoiceSyncStatus(syncStatusVO);
                    produceChannelReconciliationInvoiceSyncEvent(request, channel.getCode());
                    response.setSuccessful(true);
                    response.getResultItems().add(new SyncChannelReconciliationInvoiceResponse.ChannelReconciliationInvoiceDTO(channel.getCode(), "Request queued"));
                    LOG.info("Queued channelReconciliationInvoiceSyncStatus for channel: {}", channel.getCode());
                    response.setSuccessful(true);
                    response.setMessage("Sync started");
                }
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    private boolean produceChannelReconciliationInvoiceSyncEvent(SyncChannelReconciliationInvoiceRequest request, String channelCode) {
        SyncReconciliationInvoiceEvent syncReconciliationInvoiceEvent = new SyncReconciliationInvoiceEvent();
        syncReconciliationInvoiceEvent.setChannelCode(channelCode);
        syncReconciliationInvoiceEvent.setRequestTimestamp(DateUtils.getCurrentTime());
        return activeMQConnector.produceContextAwareMessage(MessagingConstants.Queue.RECONCILIATION_INVOICE_QUEUE, syncReconciliationInvoiceEvent);
    }

    @Override
    public void updateChannelReconciliationInvoiceSyncStatus(ChannelReconciliationInvoiceSyncStatusVO channelStatus) {
        channelService.updateChannelReconciliationInvoiceSyncStatus(channelStatus);
    }

    @Override
    public void syncChannelReconciliationInvoices(SyncReconciliationInvoiceEvent event) {
        TenantProfileVO tenantProfile = tenantProfileService.getTenantProfileByCode(UserContext.current().getTenant().getCode());
        Channel channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelByCode(event.getChannelCode());
        java.util.concurrent.locks.Lock reconciliationInvoiceSyncLock = lockingService.getLock(Namespace.CHANNEL_RECONCILIATION_INVOICE_SYNC, channel.getCode());
        boolean acquired = false;
        if (!StringUtils.equalsAny(tenantProfile.getPaymentStatus(), TenantProfileVO.PaymentStatus.LIMITED_SERVICES, TenantProfileVO.PaymentStatus.BLOCKED)) {
            try {
                ChannelReconciliationInvoiceSyncStatusVO syncStatusVO = channelService.getChannelReconciliationSyncStatus(channel.getCode());
                acquired = acquireReconciliationInvoiceSyncLock(syncStatusVO, reconciliationInvoiceSyncLock);
                if (acquired) {
                    ScraperScript channelReconciliationInvoiceFetchScript = CacheManager.getInstance().getCache(ChannelCache.class).getScriptByName(channel.getCode(),
                            Source.CHANNEL_RECONCILIATION_INVOICE_LIST_SCRIPT_NAME);
                    ScraperScript channelReconciliationInvoiceDetailFetchScript = CacheManager.getInstance().getCache(ChannelCache.class).getScriptByName(channel.getCode(),
                            Source.CHANNEL_RECONCILIATION_INVOICE_DETAIL_SCRIPT_NAME);
                    if (channelReconciliationInvoiceFetchScript == null || channelReconciliationInvoiceDetailFetchScript == null) {
                        LOG.error("Scripts missing for channel {}. Cannot run reconciliation invoice task", channel.getName());
                    } else {
                        if (ActivityContext.current().isEnable() && StringUtils.isNotBlank(UserContext.current().getUniwareUserName())) {
                            ActivityUtils.appendActivity(channel.getCode(), ActivityEntityEnum.CHANNEL.getName(), channel.getSourceCode(),
                                    Collections.singletonList("Reconciliation invoice sync was triggered manually"), ActivityTypeEnum.SYNC_TRIGGERED.name());
                        }
                        runReconciliationInvoiceSync(syncStatusVO, channel, null);
                    }
                } else {
                    LOG.error("Failed to obtain {} lock for channel {}. Reconciliation invoice sync is already running.", Namespace.CHANNEL_RECONCILIATION_INVOICE_SYNC.name(),
                            channel.getCode());
                }
            } finally {
                if (acquired) {
                    reconciliationInvoiceSyncLock.unlock();
                }
            }
        } else {
            LOG.info("Please recharge your account to avail reconciliation services");
        }
    }

    @LogActivity
    private void runReconciliationInvoiceSync(ChannelReconciliationInvoiceSyncStatusVO syncStatusVO, Channel channel, AsyncScriptInstruction task) {
        LOG.info("Starting reconciciliation invoice sync for channel {}", channel.getName());
        syncStatusVO.reset();
        syncStatusVO.setLastSyncTime(DateUtils.getCurrentTime());
        syncStatusVO.setSyncExecutionStatus(Channel.SyncExecutionStatus.RUNNING);
        syncStatusVO.setMessage("Sync Started");
        updateChannelReconciliationInvoiceSyncStatus(syncStatusVO);
        try {
            ChannelServiceImpl.ChannelGoodToSyncResponse channelGoodToSyncResponse = channelService.isChannelGoodToSync(channel.getCode(), ChannelServiceImpl.Sync.RECONCILIATION);
            if (!channelGoodToSyncResponse.isGoodToSync()) {
                LOG.warn("Connector broken for channel {}", channel.getName());
                syncStatusVO.setMessage("Connector is broken");
                syncStatusVO.setSyncExecutionStatus(Channel.SyncExecutionStatus.IDLE);
            } else {
                ReconciliationInvoicePreprocessorResponse reconciliationInvoicePreprocessorResponse = runReconciliationSyncPreprocessor(channel,
                        channelGoodToSyncResponse.getParams(), task, syncStatusVO);
                if (!reconciliationInvoicePreprocessorResponse.isPreProcessingPending()) {
                    LOG.info("Reconciliation Pre-processing complete for channel: {}, Params: {}", channel.getName(), reconciliationInvoicePreprocessorResponse.getParams());
                    doRunReconciliationInvoiceSync(channel, syncStatusVO, channelGoodToSyncResponse.getParams(), reconciliationInvoicePreprocessorResponse.getParams());
                } else {
                    LOG.info("Reconciliation Pre-processing pending for channel: {}", channel.getName());
                }
            }
        } catch (Throwable e) {
            LOG.error("Error executing reconciliation invoice sync task for channel: {}, Message: {}", channel.getName(), Arrays.toString(e.getStackTrace()));
            syncStatusVO.setMessage(e.getMessage());
            syncStatusVO.setSyncExecutionStatus(Channel.SyncExecutionStatus.IDLE);
            syncStatusVO.setSuccessful(false);
        } finally {
            syncStatusVO.setLastSyncTime(DateUtils.getCurrentTime());
            updateChannelReconciliationInvoiceSyncStatus(syncStatusVO);
        }
        LOG.info("Completed reconciliation invoice sync for channel {}", channel.getName());
    }

    private void doRunReconciliationInvoiceSync(Channel channel, ChannelReconciliationInvoiceSyncStatusVO syncStatusVO, Map<String, String> channelParams,
            Map<String, Object> preprocessorParams) {
        int successfulImportCount = 0, failedImportCount = 0;
        Integer pageNumber = 1;
        Map<String, Object> metadata = new HashMap<>();
        GetChannelReconciliationListResponse response;
        do {
            LOG.info("Reconciliation Invoice - Importing page {} for channel {}", pageNumber, channel.getCode());
            response = getChannelReconciliationList(channel, channelParams, pageNumber, preprocessorParams, metadata);
            syncStatusVO.setMileStone("Importing page: " + pageNumber);
            syncStatusVO.setTotalMileStones(response.getTotalPages());
            updateChannelReconciliationInvoiceSyncStatus(syncStatusVO);
            for (Element channelReconciliationElement : response.getChannelReconciliationElements()) {
                Map<String, Object> channelReconciliationDetailElements = response.getChannelCodeToReconciliationElement();
                String reconciliationInvoiceId = channelReconciliationElement.text();
                if (reconciliationService.getReconciliationInvoiceByCode(reconciliationInvoiceId, channel.getCode()) != null) {
                    LOG.info("Skipping details for reconciliation invoice: {}", reconciliationInvoiceId);
                    continue;
                }
                try {
                    LOG.info("Fetching invoice details for invoiceId - {}", reconciliationInvoiceId);
                    CreateChannelReconciliationInvoiceRequest request = getChannelInvoiceItems(channel, channelParams, channelReconciliationElement,
                            channelReconciliationDetailElements.get(reconciliationInvoiceId), preprocessorParams, metadata);
                    LOG.info("Importing channel reconciliation invoice {}", request.getChannelReconciliationInvoice());
                    if (request.getChannelReconciliationInvoice() != null && !CollectionUtils.isEmpty(request.getChannelReconciliationInvoice().getInvoiceItems())) {
                        CreateChannelReconciliationInvoiceResponse invoiceItemsResponse = reconciliationService.createChannelReconciliationInvoice(request);
                        if (invoiceItemsResponse.isSuccessful()) {
                            successfulImportCount++;
                        } else {
                            failedImportCount++;
                            LOG.error("Error creating reconciliation invoice {}. Error - {}", reconciliationInvoiceId, invoiceItemsResponse.getErrors());
                        }
                    } else {
                        failedImportCount++;
                        LOG.error("Skipping creating reconciliation invoice {} as no transactions are fetched for it.", reconciliationInvoiceId);
                    }
                } catch (Exception e) {
                    LOG.error("Exception in creating reconciliation invoice {}, Message - {}, Stack Trace: {}", reconciliationInvoiceId, e.getMessage(),
                            Arrays.toString(e.getStackTrace()));
                    failedImportCount++;
                }
                syncStatusVO.setSuccessfulImports(successfulImportCount);
                syncStatusVO.setFailedImports(failedImportCount);
                updateChannelReconciliationInvoiceSyncStatus(syncStatusVO);
            }
            pageNumber++;
        } while (response.isSuccessful() && response.isHasMoreResults());
        syncStatusVO.setSuccessful(failedImportCount == 0);
        syncStatusVO.setSuccessfulImports(successfulImportCount);
        syncStatusVO.setFailedImports(failedImportCount);
        syncStatusVO.setSyncExecutionStatus(Channel.SyncExecutionStatus.IDLE);
    }

    public CreateChannelReconciliationInvoiceRequest getChannelInvoiceItems(Channel channel, Map<String, String> channelParams, Element channelReconciliationElement,
            Object channelReconciliationDetail, Map<String, Object> preprocessorParams, Map<String, Object> metadata) throws JiBXException, IOException {
        ScriptExecutionContext context = ScriptExecutionContext.current();
        Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(channel.getSourceCode());
        ScraperScript channelReconciliationDetailScript = CacheManager.getInstance().getCache(ChannelCache.class).getScriptByName(channel.getCode(),
                Source.CHANNEL_RECONCILIATION_INVOICE_DETAIL_SCRIPT_NAME);
        context.addVariable("source", source);
        context.addVariable("channel", channel);
        context.addVariable("channelReconciliationDetail", channelReconciliationDetail);
        context.addVariable("channelReconciliationElement", channelReconciliationElement);
        context.addVariable("preprocessorParams", preprocessorParams);
        context.addVariable("metadata", metadata);
        context.addVariable("applicationContext", applicationContext);
        for (Map.Entry<String, String> e : channelParams.entrySet()) {
            context.addVariable(e.getKey(), e.getValue());
        }
        try {
            context.setScriptProvider(new IScriptProvider() {
                @Override
                public ScraperScript getScript(String scriptName) {
                    return CacheManager.getInstance().getCache(ScriptVersionedCache.class).getScriptByName(scriptName);
                }
            });
            context.setTraceLoggingEnabled(UserContext.current().isTraceLoggingEnabled());
            channelReconciliationDetailScript.execute();
            String channelInvoiceItemDetailsXML = context.getScriptOutput();
            LOG.info("CreateChannelReconciliationInvoiceRequest - {}", channelInvoiceItemDetailsXML);
            CreateChannelReconciliationInvoiceRequest createChannelReconciliationInvoiceRequest = JibxUtils.unmarshall("unicommerceServicesBinding17",
                    CreateChannelReconciliationInvoiceRequest.class, channelInvoiceItemDetailsXML);
            if (createChannelReconciliationInvoiceRequest.getChannelReconciliationInvoice() != null
                    && StringUtils.isNotBlank(createChannelReconciliationInvoiceRequest.getChannelReconciliationInvoice().getDownloadUrl())) {
                String reconciliationInvoiceLink = createChannelReconciliationInvoiceRequest.getChannelReconciliationInvoice().getDownloadUrl();
                updateReconciliationDownloadUrl(createChannelReconciliationInvoiceRequest, reconciliationInvoiceLink);
            }

            return createChannelReconciliationInvoiceRequest;
        } finally {
            ScriptExecutionContext.destroy();
        }
    }

    private void updateReconciliationDownloadUrl(CreateChannelReconciliationInvoiceRequest createChannelReconciliationInvoiceRequest, String reconciliationInvoiceLink) {
        if (StringUtils.isNotBlank(reconciliationInvoiceLink)) {
            String filePath = CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class).getExportDirectoryPath() + reconciliationInvoiceLink;
            reconciliationInvoiceLink = documentService.uploadFile(new File(filePath), Constants.CHANNEL_RECONCILIATION_REPORT_BUCKET_NAME);
            WsChannelReconciliationInvoice wsChannelReconciliationInvoice = createChannelReconciliationInvoiceRequest.getChannelReconciliationInvoice();
            wsChannelReconciliationInvoice.setDownloadUrl(reconciliationInvoiceLink);
            createChannelReconciliationInvoiceRequest.setChannelReconciliationInvoice(wsChannelReconciliationInvoice);

        }
    }

    private GetChannelReconciliationListResponse getChannelReconciliationList(Channel channel, Map<String, String> channelParams, Integer pageNumber,
            Map<String, Object> preprocessorParams, Map<String, Object> metadata) {
        GetChannelReconciliationListResponse response = new GetChannelReconciliationListResponse();
        Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(channel.getSourceCode());
        ScraperScript channelReconciliationListScript = CacheManager.getInstance().getCache(ChannelCache.class).getScriptByName(channel.getCode(),
                Source.CHANNEL_RECONCILIATION_INVOICE_LIST_SCRIPT_NAME);
        ScriptExecutionContext context = ScriptExecutionContext.current();
        Map<String, Object> resultItems = new HashMap<>();
        context.addVariable("resultItems", resultItems);
        context.addVariable("source", source);
        context.addVariable("channel", channel);
        context.addVariable("pageNumber", pageNumber);
        context.addVariable("preprocessorParams", preprocessorParams);
        context.addVariable("metadata", metadata);
        for (Entry<String, String> e : channelParams.entrySet()) {
            context.addVariable(e.getKey(), e.getValue());
        }
        try {
            context.setScriptProvider(new IScriptProvider() {
                @Override
                public ScraperScript getScript(String scriptName) {
                    return CacheManager.getInstance().getCache(ScriptVersionedCache.class).getScriptByName(scriptName);
                }
            });
            context.setTraceLoggingEnabled(UserContext.current().isTraceLoggingEnabled());
            channelReconciliationListScript.execute();
            String channelReconciliationListXML = context.getScriptOutput();
            if (StringUtils.isNotBlank(channelReconciliationListXML)) {
                List<Element> channelReconciliationElements = new ArrayList<XMLParser.Element>();
                Element rootElement = XMLParser.parse(channelReconciliationListXML);
                Integer totalPages = Integer.parseInt(rootElement.text(TOTAL_PAGES));
                response.setHasMoreResults(totalPages > pageNumber);
                response.setTotalPages(totalPages);
                for (Element channelItemTypeElement : rootElement.list(CHANNEL_RECONCILIATION_INVOICE)) {
                    channelReconciliationElements.add(channelItemTypeElement);
                }
                response.setSuccessful(true);
                response.setChannelReconciliationElements(channelReconciliationElements);
                response.setChannelCodeToReconciliationElement(resultItems);
            } else {
                response.setMessage("No reconciliation invoice found for channel: " + channel);
            }
        } finally {
            ScriptExecutionContext.destroy();
        }
        return response;
    }

    private ReconciliationInvoicePreprocessorResponse runReconciliationSyncPreprocessor(Channel channel, Map<String, String> channelParams,
            AsyncScriptInstruction asyncScriptInstruction, ChannelReconciliationInvoiceSyncStatusVO syncStatusVO) {
        ReconciliationInvoicePreprocessorResponse reconciliationInvoicePreprocessorResponse = new ReconciliationInvoicePreprocessorResponse();
        if (StringUtils.isNotBlank(
                CacheManager.getInstance().getCache(ChannelCache.class).getScriptName(channel.getCode(), Source.CHANNEL_RECONCILIATION_INVOICE_SYNC_PREPROCESSOR_SCRIPT_NAME))) {
            ScraperScript channelReconciliationPreprocessorScript = CacheManager.getInstance().getCache(ChannelCache.class).getScriptByName(channel.getCode(),
                    com.uniware.core.entity.Source.CHANNEL_RECONCILIATION_INVOICE_SYNC_PREPROCESSOR_SCRIPT_NAME);
            if (channelReconciliationPreprocessorScript != null) {
                Map<String, Object> resultItems = new HashMap<>();
                ScriptExecutionContext context = ScriptExecutionContext.current();
                String instructionName = null;
                if (asyncScriptInstruction != null) {
                    instructionName = asyncScriptInstruction.getInstructionName();
                    for (AsyncScriptInstructionParameter param : asyncScriptInstruction.getInstructionParameters()) {
                        context.addVariable(param.getName(), param.getValue());
                    }
                }
                context.addVariable("resultItems", resultItems);
                context.addVariable("channel", channel);
                for (Map.Entry<String, String> e : channelParams.entrySet()) {
                    context.addVariable(e.getKey(), e.getValue());
                }
                context.setScriptProvider(new IScriptProvider() {
                    @Override
                    public ScraperScript getScript(String scriptName) {
                        return CacheManager.getInstance().getCache(ScriptVersionedCache.class).getScriptByName(scriptName);
                    }
                });
                context.setTraceLoggingEnabled(UserContext.current().isTraceLoggingEnabled());
                try {
                    LOG.info("Running reconciliation sync preprocessor for channel {}, instructionName {}", channel.getName(), instructionName);
                    channelReconciliationPreprocessorScript.execute(instructionName);
                    if (!context.getScheduleScriptContext().isEmpty()) {
                        LOG.info("Scheduling async task for channel- {}", channel.getCode());
                        asyncScriptTaskService.scheduleScriptTask(channelReconciliationPreprocessorScript.getScriptName(), channel, context, this.getClass(),
                                ChannelReconciliationInvoiceFetchTask.class.getName());
                        reconciliationInvoicePreprocessorResponse.setPreProcessingPending(true);
                        syncStatusVO.setMessage("Preprocessing Scheduled");
                    } else {
                        LOG.info("Pre-processing complete for reconciliation sync, resultItems: {}", resultItems);
                        reconciliationInvoicePreprocessorResponse.setParams(resultItems);
                    }
                    LOG.info("Completed reconciliation sync preprocessor for channel {}", channel.getName());
                } finally {
                    ScriptExecutionContext.destroy();
                }
            }
        }
        return reconciliationInvoicePreprocessorResponse;
    }

    private String createFailedLogFile(String invoiceCode, List<String> failureLogs) throws IOException {
        String errorLogFilePath = null;
        if (failureLogs != null && failureLogs.size() > 1) {
            errorLogFilePath = "/tmp/" + EncryptionUtils.md5Encode(invoiceCode) + ".csv";
            File file = new File(errorLogFilePath);
            file.createNewFile();
            BufferedWriter writer = new BufferedWriter(new FileWriter(file));
            try {
                writer.append("Order/Sub-order Code/Reconciliation Identifier,Failure Message").append(LINE_SEPARATOR);
                int logCounter = 0;
                for (String failureMessage : failureLogs) {
                    writer.append(failureMessage);
                    if (logCounter++ != failureLogs.size()) {
                        writer.append(LINE_SEPARATOR);
                    }
                }
                writer.flush();
            } finally {
                writer.close();
            }
        }
        return errorLogFilePath;
    }

    @Override
    public void resume(AsyncScriptInstruction task) {
        String channelCode = null;
        for (AsyncScriptInstructionParameter param : task.getInstructionParameters()) {
            if (param.getName().equals("channel")) {
                channelCode = (String) param.getValue();
                break;
            }
        }
        Channel channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelByCode(channelCode);
        ChannelReconciliationInvoiceSyncStatusVO syncStatusVO = channelService.getChannelReconciliationSyncStatus(channel.getCode());
        runReconciliationInvoiceSync(syncStatusVO, channel, task);
    }

    private void updateStatus(int successfulImportCount, int failedImportCount, int successfulProcessed, Channel channel, ChannelReconciliationInvoiceSyncStatusVO syncStatusVO) {
        Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(channel.getSourceCode());
        String message = "Successful imports:" + successfulImportCount + ", failed imports:" + failedImportCount;
        message += ". Task Completed";
        LOG.info("Channel: {}, Result: {}", source.getName(), message);
        syncStatusVO.setSuccessful(failedImportCount == 0);
        syncStatusVO.setLastSyncTime(DateUtils.getCurrentTime());
        syncStatusVO.setSuccessfulImports(successfulImportCount);
        syncStatusVO.setFailedImports(failedImportCount);
        syncStatusVO.setSuccessfulProcessed(successfulProcessed);
        syncStatusVO.setMessage(message);
        syncStatusVO.setSyncExecutionStatus(Channel.SyncExecutionStatus.IDLE);
        updateChannelReconciliationInvoiceSyncStatus(syncStatusVO);
    }

    private boolean acquireReconciliationInvoiceSyncLock(ChannelReconciliationInvoiceSyncStatusVO syncStatusVO, java.util.concurrent.locks.Lock reconciliationInvoiceSyncLock) {
        boolean acquired;
        switch (syncStatusVO.getSyncExecutionStatus()) {
            case QUEUED:
                // if we are here before the producer releases the lock, the
                // status would be QUEUED.
                try {
                    long defaultTimeout = CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class).getLockWaitTimeoutInSeconds();
                    acquired = reconciliationInvoiceSyncLock.tryLock(defaultTimeout, TimeUnit.SECONDS);
                } catch (InterruptedException e) {
                    LOG.error("InterruptedException while waiting for lock", e);
                    throw new RuntimeException(e);
                }
                break;
            default:
                acquired = reconciliationInvoiceSyncLock.tryLock();
                break;
        }
        return acquired;
    }

    public static class ReconciliationInvoicePreprocessorResponse {
        private boolean             preProcessingPending;
        private Map<String, Object> params;

        public boolean isPreProcessingPending() {
            return preProcessingPending;
        }

        public void setPreProcessingPending(boolean preProcessingPending) {
            this.preProcessingPending = preProcessingPending;
        }

        public Map<String, Object> getParams() {
            return params;
        }

        public void setParams(Map<String, Object> params) {
            this.params = params;

        }
    }
}
