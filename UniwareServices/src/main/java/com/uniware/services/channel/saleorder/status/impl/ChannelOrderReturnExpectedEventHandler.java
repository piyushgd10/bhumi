/*
*  Copyright 2015 Unicommerce eSolutions (P) Limited . All Rights Reserved.
*  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
*  
*  @version     1.0, 18/11/15
*  @author sunny
*/

package com.uniware.services.channel.saleorder.status.impl;

import com.unifier.core.utils.StringUtils;
import com.unifier.services.users.IUsersService;
import com.uniware.core.api.returns.CompleteReversePickupRequest;
import com.uniware.core.api.returns.CompleteReversePickupResponse;
import com.uniware.core.api.returns.CreateReversePickupRequest;
import com.uniware.core.api.returns.CreateReversePickupResponse;
import com.uniware.core.api.returns.MarkSaleOrderReturnedRequest;
import com.uniware.core.api.returns.WsReversePickupItem;
import com.uniware.core.api.saleorder.MarkSaleOrderItemsCompleteRequest;
import com.uniware.core.api.saleorder.MarkSaleOrderItemsCompleteResponse;
import com.uniware.core.entity.ItemTypeInventory;
import com.uniware.core.entity.ReversePickup;
import com.uniware.core.entity.SaleOrder;
import com.uniware.core.entity.SaleOrderItem;
import com.uniware.core.utils.Constants;
import com.uniware.services.channel.saleorder.status.IChannelOrderStatusChangeEventHandler;
import com.uniware.services.channel.saleorder.status.IChannelOrderStatusSyncService;
import com.uniware.services.channel.saleorder.status.event.ChannelOrderStatusChangeEvent;
import com.uniware.services.returns.IReturnsService;
import com.uniware.services.saleorder.ISaleOrderService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class ChannelOrderReturnExpectedEventHandler implements IChannelOrderStatusChangeEventHandler {

    private static final Logger LOG = LoggerFactory.getLogger(ChannelOrderReturnExpectedEventHandler.class);

    @Autowired
    private ISaleOrderService saleOrderService;

    @Autowired
    private IReturnsService returnsService;

    @Autowired
    private IUsersService usersService;

    @Autowired
    private IChannelOrderStatusSyncService channelOrderStatusSyncService;

    @PostConstruct
    public void init() {
        channelOrderStatusSyncService.registerEventHandler(ChannelOrderStatusChangeEvent.Type.RETURN_EXPECTED, this);
        channelOrderStatusSyncService.registerEventHandler(ChannelOrderStatusChangeEvent.Type.RETURNED, this);
    }

    @Override
    @Transactional
    public void handle(ChannelOrderStatusChangeEvent event) {
        switch (event.getType()) {
            case RETURN_EXPECTED:
                handleReturnExpected(event);
                break;
            case RETURNED:
                handleReturnExpected(event);
                handleReturned(event);
                break;
            default:
                break;
        }
    }

    public void handleReturnExpected(ChannelOrderStatusChangeEvent event) {
        String saleOrderCode = event.getSaleOrderCode();
        SaleOrder saleOrder = saleOrderService.getSaleOrderForUpdate(saleOrderCode);
        Map<String, SaleOrderItem> codeToSaleOrderItem = new HashMap<>(saleOrder.getSaleOrderItems().size());
        for (SaleOrderItem soi : saleOrder.getSaleOrderItems()) {
            codeToSaleOrderItem.put(soi.getCode(), soi);
        }
        Map<String, String> saleOrderItemCodeToReturnReason = new HashMap<>();
        Map<String, String> incompleteSaleOrderItemCodeToReturnReason = new HashMap<>();
        for (ChannelOrderStatusChangeEvent.WsSaleOrderItem wsSaleOrderItem : event.getSaleOrderItems()) {
            String saleOrderItemCode = wsSaleOrderItem.getCode();
            SaleOrderItem saleOrderItem = codeToSaleOrderItem.get(saleOrderItemCode);
            String returnReason = wsSaleOrderItem.getAttributes().get("ReturnReason");
            returnReason = StringUtils.isNotBlank(returnReason) ? returnReason : "Return expected on panel";
            if (StringUtils.equalsAny(saleOrderItem.getStatusCode(), SaleOrderItem.StatusCode.CREATED.name(), SaleOrderItem.StatusCode.FULFILLABLE.name(),
                    SaleOrderItem.StatusCode.UNFULFILLABLE.name(), SaleOrderItem.StatusCode.MANIFESTED.name())) {
                incompleteSaleOrderItemCodeToReturnReason.put(saleOrderItemCode, returnReason);
            } else if (StringUtils.equalsAny(saleOrderItem.getStatusCode(), SaleOrderItem.StatusCode.DISPATCHED.name(), SaleOrderItem.StatusCode.DELIVERED.name())) {
                saleOrderItemCodeToReturnReason.put(saleOrderItemCode, returnReason);
            }
        }
        if (!incompleteSaleOrderItemCodeToReturnReason.isEmpty()) {
            MarkSaleOrderItemsCompleteRequest markSaleOrderItemsCompleteRequest = new MarkSaleOrderItemsCompleteRequest();
            markSaleOrderItemsCompleteRequest.setSaleOrderCode(saleOrder.getCode());
            markSaleOrderItemsCompleteRequest.setSaleOrderItemCodes(new ArrayList<String>(incompleteSaleOrderItemCodeToReturnReason.keySet()));
            markSaleOrderItemsCompleteRequest.setUserId(usersService.getUserByUsername(Constants.SYSTEM_USER_EMAIL).getId());
            MarkSaleOrderItemsCompleteResponse markSaleOrderItemsCompleteResponse = saleOrderService.markSaleOrderItemsComplete(markSaleOrderItemsCompleteRequest);
            if (!markSaleOrderItemsCompleteResponse.isSuccessful()) {
                LOG.info("Unable to mark saleOrder {} complete for return sync, error {}", saleOrderCode, markSaleOrderItemsCompleteResponse.getErrors().toString());
                return;
            } else {
                LOG.info("Successfully marked saleOrder {} complete for return sync", saleOrderCode);
                saleOrderItemCodeToReturnReason.putAll(incompleteSaleOrderItemCodeToReturnReason);
            }
        }
        if (!saleOrderItemCodeToReturnReason.isEmpty()) {
            LOG.info("Creating Reverse pick up for saleOrder {}", saleOrderCode);
            CreateReversePickupRequest createReversePickupRequest = new CreateReversePickupRequest();
            createReversePickupRequest.setSaleOrderCode(saleOrderCode);
            createReversePickupRequest.setActionCode(ReversePickup.Action.WAIT_FOR_RETURN_AND_CANCEL.code());
            List<WsReversePickupItem> reversePickupItems = new ArrayList<>(saleOrderItemCodeToReturnReason.size());
            for (Map.Entry<String, String> e : saleOrderItemCodeToReturnReason.entrySet()) {
                WsReversePickupItem reversePickupItem = new WsReversePickupItem(e.getKey(), e.getValue());
                reversePickupItems.add(reversePickupItem);
            }
            createReversePickupRequest.setReversePickItems(reversePickupItems);
            CreateReversePickupResponse reversePickupResponse = returnsService.createReversePickup(createReversePickupRequest);
            if (reversePickupResponse.isSuccessful()) {
                LOG.info("Successfully created reverse pick up for saleOrder {}", saleOrderCode);
            } else {
                LOG.info("Unable to create reverse pick up for saleOrder {} with errors {}", saleOrderCode, reversePickupResponse.getErrors());
            }
        }
    }

    public void handleReturned(ChannelOrderStatusChangeEvent event) {
        String saleOrderCode = event.getSaleOrderCode();
        SaleOrder saleOrder = saleOrderService.getSaleOrderForUpdate(saleOrderCode);
        List<ChannelOrderStatusChangeEvent.WsSaleOrderItem> eventSaleOrderItems = event.getSaleOrderItems();
        Map<String, Map<String, String>> soiCodeToAttributesMap = new HashMap<>();
        for (ChannelOrderStatusChangeEvent.WsSaleOrderItem eventSaleOrderItem : eventSaleOrderItems) {
            soiCodeToAttributesMap.put(eventSaleOrderItem.getCode(), eventSaleOrderItem.getAttributes());
        }
        for (ReversePickup reversePickup : saleOrder.getReversePickups()) {
            CompleteReversePickupRequest completeReversePickupRequest = new CompleteReversePickupRequest();
            completeReversePickupRequest.setReversePickupCode(reversePickup.getCode());

            List<MarkSaleOrderReturnedRequest.WsSaleOrderItem> wsSaleOrderItems = new ArrayList<>();
            for (SaleOrderItem saleOrderItem : reversePickup.getSaleOrderItems()) {
                String returnedItemStatus = ItemTypeInventory.Type.GOOD_INVENTORY.name();
                if (soiCodeToAttributesMap.get(saleOrderItem.getCode()).get("QualityOfProduct") != null && soiCodeToAttributesMap.get(saleOrderItem.getCode()).get("QualityOfProduct").equals("CUSTOMER_DAMAGED")) {
                    returnedItemStatus = ItemTypeInventory.Type.BAD_INVENTORY.name();
                }
                MarkSaleOrderReturnedRequest.WsSaleOrderItem wsSaleOrderItem = new MarkSaleOrderReturnedRequest.WsSaleOrderItem(saleOrderItem.getCode(),
                        returnedItemStatus);
                wsSaleOrderItems.add(wsSaleOrderItem);
            }

            completeReversePickupRequest.setSaleOrderItems(wsSaleOrderItems);
            completeReversePickupRequest.setUserId(usersService.getUserByUsername(Constants.SYSTEM_USER_EMAIL).getId());
            CompleteReversePickupResponse completeReversePickupResponse = returnsService.completeReversePickup(completeReversePickupRequest);
            if (completeReversePickupResponse.isSuccessful()) {
                LOG.info("Successfully completed reverse pick up for reversePickup {}", reversePickup.getCode());
            } else {
                LOG.info("Unable to complete reverse pick up for reversePickup {} with errors {}", reversePickup.getCode(), completeReversePickupResponse.getErrors());
            }
        }
    }
}
