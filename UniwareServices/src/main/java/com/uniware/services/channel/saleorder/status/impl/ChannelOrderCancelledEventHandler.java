/*
*  Copyright 2015 Unicommerce eSolutions (P) Limited . All Rights Reserved.
*  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
*  
*  @version     1.0, 18/11/15
*  @author sunny
*/

package com.uniware.services.channel.saleorder.status.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.unifier.core.utils.StringUtils;
import com.uniware.core.api.saleorder.CancelSaleOrderRequest;
import com.uniware.core.api.saleorder.CancelSaleOrderResponse;
import com.uniware.core.entity.SaleOrderItem;
import com.uniware.services.channel.saleorder.status.IChannelOrderStatusChangeEventHandler;
import com.uniware.services.channel.saleorder.status.IChannelOrderStatusSyncService;
import com.uniware.services.channel.saleorder.status.event.ChannelOrderStatusChangeEvent;
import com.uniware.services.saleorder.ISaleOrderService;

@Component
public class ChannelOrderCancelledEventHandler implements IChannelOrderStatusChangeEventHandler {

    private static final Logger            LOG = LoggerFactory.getLogger(ChannelOrderCancelledEventHandler.class);

    @Autowired
    private ISaleOrderService              saleOrderService;

    @Autowired
    private IChannelOrderStatusSyncService channelOrderStatusSyncService;

    @PostConstruct
    public void init() {
        channelOrderStatusSyncService.registerEventHandler(ChannelOrderStatusChangeEvent.Type.CANCELLED, this);
    }

    @Override
    @Transactional
    public void handle(ChannelOrderStatusChangeEvent event) {
        String saleOrderCode = event.getSaleOrderCode();
        String cancellationReason = null;
        List<String> saleOrderItemCodes = new ArrayList<>(event.getSaleOrderItems().size());
        for (ChannelOrderStatusChangeEvent.WsSaleOrderItem wsSaleOrderItem : event.getSaleOrderItems()) {
            String saleOrderItemCode = wsSaleOrderItem.getCode();
            SaleOrderItem saleOrderItem = saleOrderService.getSaleOrderItemByCode(saleOrderCode, saleOrderItemCode);
            if (!ChannelOrderStatusChangeEvent.Type.CANCELLED.name().equals(saleOrderItem.getStatusCode()) && saleOrderItem.isCancellable()) {
                if (cancellationReason == null) {
                    cancellationReason = wsSaleOrderItem.getAttributes().get("CancellationReason");
                }
                saleOrderItemCodes.add(saleOrderItemCode);
            }
        }
        cancellationReason = StringUtils.isNotBlank(cancellationReason) ? cancellationReason : "cancelled on panel";
        if (!saleOrderItemCodes.isEmpty()) {
            LOG.info("Marking saleOrder cancelled: {}", saleOrderCode);
            CancelSaleOrderRequest cancelSaleOrderRequest = new CancelSaleOrderRequest();
            cancelSaleOrderRequest.setSaleOrderCode(saleOrderCode);
            cancelSaleOrderRequest.setSaleOrderItemCodes(saleOrderItemCodes);
            cancelSaleOrderRequest.setCancelOnChannel(false);
            cancelSaleOrderRequest.setCancellationReason(cancellationReason);
            CancelSaleOrderResponse cancelSaleOrderResponse = saleOrderService.cancelSaleOrder(cancelSaleOrderRequest);
            if (cancelSaleOrderResponse.isSuccessful()) {
                LOG.info("Successfully cancelled saleOrder {}", saleOrderCode);
            } else {
                LOG.warn("Unable to cancel saleOrder {}, error {}", saleOrderCode, cancelSaleOrderResponse.getErrors().toString());
            }
        }
    }
}
