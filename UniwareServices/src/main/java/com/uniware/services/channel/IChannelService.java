/*
 *  Copyright 2013 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 18-Apr-2013
 *  @author unicom
 */
package com.uniware.services.channel;

import com.unifier.core.annotation.audit.LogActivity;
import com.unifier.services.aspect.MarkDirty;
import com.unifier.services.aspect.RollbackOnFailure;
import com.uniware.core.api.channel.AddChannelConnectorRequest;
import com.uniware.core.api.channel.AddChannelConnectorResponse;
import com.uniware.core.api.channel.AddChannelRequest;
import com.uniware.core.api.channel.AddChannelResponse;
import com.uniware.core.api.channel.ChangeChannelInventorySyncStatusRequest;
import com.uniware.core.api.channel.ChangeChannelInventorySyncStatusResponse;
import com.uniware.core.api.channel.ChangeChannelOrderSyncStatusRequest;
import com.uniware.core.api.channel.ChangeChannelOrderSyncStatusResponse;
import com.uniware.core.api.channel.ChangeChannelPricingSyncStatusRequest;
import com.uniware.core.api.channel.ChangeChannelPricingSyncStatusResponse;
import com.uniware.core.api.channel.ChangeChannelReconciliationSyncStatusRequest;
import com.uniware.core.api.channel.ChangeChannelReconciliationSyncStatusResponse;
import com.uniware.core.api.channel.ChannelCatalogSyncStatusDTO;
import com.uniware.core.api.channel.ChannelDetailDTO;
import com.uniware.core.api.channel.ChannelInventorySyncStatusDTO;
import com.uniware.core.api.channel.ChannelOrderStatusDTO;
import com.uniware.core.api.channel.ChannelOrderSyncStatusDTO;
import com.uniware.core.api.channel.ChannelPricingPushStatusDTO;
import com.uniware.core.api.channel.ChannelReconciliationInvoiceSyncStatusVO;
import com.uniware.core.api.channel.EditChannelAssociatedFacilityRequest;
import com.uniware.core.api.channel.EditChannelAssociatedFacilityResponse;
import com.uniware.core.api.channel.EditChannelRequest;
import com.uniware.core.api.channel.EditChannelResponse;
import com.uniware.core.api.channel.GetChannelActivitiesRequest;
import com.uniware.core.api.channel.GetChannelActivitiesResponse;
import com.uniware.core.api.channel.GetChannelDetailsRequest;
import com.uniware.core.api.channel.GetChannelDetailsResponse;
import com.uniware.core.api.channel.GetChannelOrderSummaryRequest;
import com.uniware.core.api.channel.GetChannelOrderSummaryResponse;
import com.uniware.core.api.channel.GetChannelProductSummaryRequest;
import com.uniware.core.api.channel.GetChannelProductSummaryResponse;
import com.uniware.core.api.channel.GetChannelSyncAttributesRequest;
import com.uniware.core.api.channel.GetChannelSyncAttributesResponse;
import com.uniware.core.api.channel.GetChannelSyncRequest;
import com.uniware.core.api.channel.GetChannelSyncResponse;
import com.uniware.core.api.channel.GetChannelsForSaleOrderImportRequest;
import com.uniware.core.api.channel.GetChannelsForSaleOrderImportResponse;
import com.uniware.core.api.channel.GetConnectorStatusResponse;
import com.uniware.core.api.channel.GoToChannelLandingPageRequest;
import com.uniware.core.api.channel.GoToChannelLandingPageResponse;
import com.uniware.core.api.channel.PostConfigureChannelConnectorRequest;
import com.uniware.core.api.channel.PostConfigureChannelConnectorResponse;
import com.uniware.core.api.channel.PreConfigureChannelConnectorRequest;
import com.uniware.core.api.channel.PreConfigureChannelConnectorResponse;
import com.uniware.core.api.channel.SourceDetailDTO;
import com.uniware.core.api.channel.SourceSearchDTO;
import com.uniware.core.api.channel.TestChannelInventoryUpdateFormulaRequest;
import com.uniware.core.api.channel.TestChannelInventoryUpdateFormulaResponse;
import com.uniware.core.api.channel.VerifyAndSyncParamsResponse;
import com.uniware.core.entity.BillingParty;
import com.uniware.core.entity.Channel;
import com.uniware.core.entity.ChannelConnector;
import com.uniware.core.entity.Source;
import com.uniware.core.vo.ChannelWarehouseInventorySyncStatusVO;
import com.uniware.services.cache.ChannelCache;
import com.uniware.services.channel.impl.ChannelServiceImpl;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface IChannelService {

    Channel getChannelByCode(String channelCode);

    AddChannelResponse addChannel(AddChannelRequest request);

    EditChannelResponse editChannel(EditChannelRequest request);

    Channel updateChannel(Channel channel);

    List<Channel> disableBillingPartyOnAllChannels(BillingParty billingParty);

    ChannelConnector updateChannelConnector(ChannelConnector channelConnector);

    GetChannelSyncResponse getChannelSync(GetChannelSyncRequest request);

    GetChannelSyncAttributesResponse getChannelSyncAttributes(GetChannelSyncAttributesRequest request);

    PreConfigureChannelConnectorResponse preConfigureChannelConnector(PreConfigureChannelConnectorRequest request);

    PostConfigureChannelConnectorResponse postConfigureChannelConnector(PostConfigureChannelConnectorRequest request);

    Channel getChannelByName(String channelName);

    List<Channel> getAllChannels();

    List<Source> getAllSources();

    GoToChannelLandingPageResponse goToChannelLandingPage(GoToChannelLandingPageRequest request);

    ChangeChannelOrderSyncStatusResponse changeOrderSyncStatus(ChangeChannelOrderSyncStatusRequest request);

    List<SourceSearchDTO> lookupSource(String keyword);

    AddChannelConnectorResponse addChannelConnector(AddChannelConnectorRequest request);

    GetChannelDetailsResponse getChannelDetailsByChannelCode(GetChannelDetailsRequest request);

    void prepareSourceConfigurationParametersDTO(SourceDetailDTO sourceDTO, Source source);

    GetChannelOrderSummaryResponse getChannelOrderSummaryByChannelCode(GetChannelOrderSummaryRequest request);

    GetChannelProductSummaryResponse getChannelProductSummaryByChannelCode(GetChannelProductSummaryRequest request);

    ChangeChannelInventorySyncStatusResponse changeInventorySyncStatus(ChangeChannelInventorySyncStatusRequest request);

    ChannelOrderSyncStatusDTO updateChannelOrderSyncStatus(ChannelOrderSyncStatusDTO channelOrderSyncStatus);

    List<ChannelOrderSyncStatusDTO> getChannelOrderSyncStatuses();

    List<ChannelInventorySyncStatusDTO> getChannelInventorySyncStatuses();

    List<ChannelCatalogSyncStatusDTO> getChannelCatalogSyncStatuses();

    ChannelOrderStatusDTO getOrderSyncStatusOnChannel(String channelCode);

    ChannelInventorySyncStatusDTO updateChannelInventorySyncStatus(ChannelInventorySyncStatusDTO inventorySyncStatusDTO);

    ChannelCatalogSyncStatusDTO updateChannelCatalogSyncStatus(ChannelCatalogSyncStatusDTO catalogSyncStatusDTO);

    void clearChannelOrderSyncStatuses();

    void clearChannelInventorySyncStatuses();

    void clearChannelCatalogSyncStatuses();

    GetChannelsForSaleOrderImportResponse getChannelsForOrderImport(GetChannelsForSaleOrderImportRequest request);

    TestChannelInventoryUpdateFormulaResponse testChannelInventoryUpdateFormula(TestChannelInventoryUpdateFormulaRequest request);

    ChannelWarehouseInventorySyncStatusVO getChannelWarehouseInventorySyncStatus(String channelCode);

    List<ChannelOrderStatusDTO> getChannelShipmentSyncStatuses();

    ChannelOrderStatusDTO updateChannelShipmentSyncStatus(ChannelOrderStatusDTO shipmentSyncStatusDTO);

    void clearChannelShipmentSyncStatuses();

    ChannelInventorySyncStatusDTO getChannelInventorySyncStatus(String channelCode);

    ChannelServiceImpl.ChannelGoodToSyncResponse isChannelGoodToSync(String channelCode, ChannelServiceImpl.Sync sync);

    VerifyAndSyncParamsResponse verifyAndSyncChannelConnector(String channelCode, String connectorName);

    ChannelReconciliationInvoiceSyncStatusVO getChannelReconciliationSyncStatus(String channelCode);

    ChannelReconciliationInvoiceSyncStatusVO updateChannelReconciliationInvoiceSyncStatus(ChannelReconciliationInvoiceSyncStatusVO channelStatus);

    ChangeChannelReconciliationSyncStatusResponse changeReconciliationSyncStatus(ChangeChannelReconciliationSyncStatusRequest request);

    String constructChannelShortName(Source source, String name);

    String getChannelColorCodeBySourceCode(String sourceCode);

    boolean checkShortNameAvailability(String sourceCode, String shortName);

    int getChannelCountBySourceCode(String sourceCode);

    void prepareChannelConfigurationParametersDTO(ChannelDetailDTO channelDTO, Channel channel);

    ChannelCatalogSyncStatusDTO getChannelCatalogSyncStatus(String channelCode);

    ChannelOrderSyncStatusDTO getChannelOrderSyncStatus(String channelCode);

    void resetAllSyncStatuses();

    GetChannelActivitiesResponse getChannelActivities(GetChannelActivitiesRequest request);

    ChangeChannelPricingSyncStatusResponse changeChannelPricingSyncStatus(ChangeChannelPricingSyncStatusRequest request);

    ChannelPricingPushStatusDTO getChannelPricingSyncStatus(String channelCode);

    List<ChannelPricingPushStatusDTO> getChannelPricingSyncStatuses();

    ChannelWarehouseInventorySyncStatusVO updateChannelWarehouseInventorySyncStatus(
            ChannelWarehouseInventorySyncStatusVO channelWarehouseInventorySyncStatus);

    ChannelPricingPushStatusDTO updateChannelPricingSyncStatus(ChannelPricingPushStatusDTO pricingSyncStatusDTO);

    GetConnectorStatusResponse getChannelConnectorStatus();

    @Transactional
    @MarkDirty(values = { ChannelCache.class})
    @LogActivity
    @RollbackOnFailure EditChannelAssociatedFacilityResponse editChannelAssociatedFacility(EditChannelAssociatedFacilityRequest request);

    boolean isFixedGSTOnOtherChargesApplicable(String channelCode);
}
