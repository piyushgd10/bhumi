/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Apr 29, 2012
 *  @author singla
 */
package com.uniware.services.channel.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.unifier.core.annotation.audit.LogActivity;
import com.unifier.core.api.customfields.CustomFieldMetadataDTO;
import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.api.validation.WsError;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.email.EmailMessage;
import com.unifier.core.entity.AsyncScriptInstruction;
import com.unifier.core.entity.AsyncScriptInstructionParameter;
import com.unifier.core.expressions.Expression;
import com.unifier.core.jms.MessagingConstants;
import com.unifier.core.transport.http.HttpTransportException;
import com.unifier.core.utils.BatchProcessor;
import com.unifier.core.utils.CollectionUtils;
import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.StringUtils;
import com.unifier.scraper.sl.exception.AsyncScriptExecutionException;
import com.unifier.scraper.sl.exception.ScriptExecutionException;
import com.unifier.scraper.sl.runtime.ScraperScript;
import com.unifier.scraper.sl.runtime.ScriptExecutionContext;
import com.unifier.services.email.IEmailService;
import com.unifier.services.tasks.IAsyncScriptService;
import com.unifier.services.tasks.engine.callback.IResumable;
import com.unifier.services.tenantprofile.service.ITenantProfileService;
import com.unifier.services.vo.TenantProfileVO;
import com.unifier.services.vo.TenantProfileVO.PaymentStatus;
import com.uniware.core.api.channel.ChannelDetailDTO;
import com.uniware.core.api.channel.ChannelInventorySyncStatusDTO;
import com.uniware.core.api.channel.CreateChannelItemTypeRequest;
import com.uniware.core.api.channel.GetChannelInventorySyncStatusRequest;
import com.uniware.core.api.channel.GetChannelInventorySyncStatusResponse;
import com.uniware.core.api.channel.GetChannelInventoryUpdateSnapshotRequest;
import com.uniware.core.api.channel.GetChannelInventoryUpdateSnapshotResponse;
import com.uniware.core.api.channel.SyncChannelItemTypeInventoryRequest;
import com.uniware.core.api.channel.SyncChannelItemTypeInventoryResponse;
import com.uniware.core.api.channel.UpdateInventoryByChannelProductIdRequest;
import com.uniware.core.api.channel.UpdateInventoryByChannelProductIdResponse;
import com.uniware.core.api.channel.WsChannelItemType;
import com.uniware.core.api.inventory.CalculateChannelItemTypeInventoryResponse;
import com.uniware.core.api.inventory.SearchInventorySnapshotResponse.InventorySnapshotDTO;
import com.uniware.core.api.inventory.UpdateInventoryOnAllChannelsRequest;
import com.uniware.core.api.inventory.UpdateInventoryOnAllChannelsResponse;
import com.uniware.core.api.inventory.UpdateInventoryOnChannelRequest;
import com.uniware.core.api.inventory.UpdateInventoryOnChannelResponse;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.cache.EnvironmentPropertiesCache;
import com.uniware.core.cache.FacilityCache;
import com.uniware.core.cache.TenantCache;
import com.uniware.core.concurrent.ContextAwareExecutorFactory;
import com.uniware.core.entity.Bundle;
import com.uniware.core.entity.BundleItemType;
import com.uniware.core.entity.Channel;
import com.uniware.core.entity.Channel.SyncStatus;
import com.uniware.core.entity.ChannelItemType;
import com.uniware.core.entity.ChannelItemType.ListingStatus;
import com.uniware.core.entity.Facility;
import com.uniware.core.entity.ItemType;
import com.uniware.core.entity.ItemTypeInventorySnapshot;
import com.uniware.core.entity.Source;
import com.uniware.core.entity.Tenant;
import com.uniware.core.locking.ILockingService;
import com.uniware.core.locking.Namespace;
import com.uniware.core.locking.annotation.Lock;
import com.uniware.core.locking.annotation.Locks;
import com.uniware.core.script.error.ChannelScriptError;
import com.uniware.core.utils.ActivityContext;
import com.uniware.core.utils.Constants.EmailTemplateType;
import com.uniware.core.utils.UserContext;
import com.uniware.core.vo.ChannelInventoryFormulaVO;
import com.uniware.core.vo.ChannelInventoryUpdateSnapshotVO;
import com.uniware.core.vo.ChannelInventoryUpdateSnapshotVO.ComponentItemTypeInventory;
import com.uniware.core.vo.ChannelInventoryUpdateVO;
import com.uniware.dao.channel.IChannelInventoryMao;
import com.uniware.dao.inventory.IInventoryDao;
import com.uniware.services.audit.impl.ActivityEntityEnum;
import com.uniware.services.audit.impl.ActivityTypeEnum;
import com.uniware.services.audit.impl.ActivityUtils;
import com.uniware.services.bundle.IBundleService;
import com.uniware.services.cache.ChannelCache;
import com.uniware.services.catalog.ICatalogService;
import com.uniware.services.channel.ChannelInventoryUpdateDTO;
import com.uniware.services.channel.IChannelCatalogService;
import com.uniware.services.channel.IChannelInventorySyncService;
import com.uniware.services.channel.IChannelService;
import com.uniware.services.channel.impl.ChannelServiceImpl.ChannelGoodToSyncResponse;
import com.uniware.services.configuration.SourceConfiguration;
import com.uniware.services.configuration.TenantSystemConfiguration;
import com.uniware.services.configuration.data.manager.ProductConfiguration;
import com.uniware.services.inventory.IInventoryService;
import com.uniware.services.messaging.jms.ActiveMQConnector;
import com.uniware.services.messaging.jms.SyncChannelInventoryEvent;
import com.uniware.services.saleorder.ISaleOrderService;
import com.uniware.services.tasks.notifications.UpdateInventoryTask;

/**
 * @author Sunny
 */
@Service
public class ChannelInventorySyncServiceImpl implements IChannelInventorySyncService, IResumable {

    private static final Logger LOG                  = LoggerFactory.getLogger(ChannelInventorySyncServiceImpl.class);
    private static final int    DEFAULT_BATCH_SIZE   = 500;
    private static final String SERVER_DOWN_MESSAGE  = "SERVER DOWN";
    private static final String DEFAULT_FORMULA      = "#{#inventorySnapshot.inventory - #inventorySnapshot.openSale - #pendency - (#failedOrderInventory?:0) - #inventoryBlockedOnOtherChannels - #inventorySnapshot.pendingInventoryAssessment}";
    private static final String DEFAULT_FORMULA_CODE = "64e953aa";
    private static final String SERVICE_NAME         = "channelinventorysyncservice";
    private static final int    MAX_RETRY_COUNT      = 5;

    public enum FailureReason {
        NOT_FOUND,
        ENDED,
        UNDER_MODERATION,
        UNKNOWN,
        SKIPPED,
        CHANNEL_ERROR
    }

    public enum SkipReason {
        DISABLED,
        NOT_VERIFIED,
        REDUNDANT,
        DELISTED,
        RELISTED,
        DISABLED_DUE_TO_ERRORS,
        DISCONTINUED
    }

    @Autowired
    private ApplicationContext          applicationContext;

    @Autowired
    private IChannelInventoryMao        channelInventoryMao;

    @Autowired
    private IInventoryDao               inventoryDao;

    @Autowired
    private ICatalogService             catalogService;

    @Autowired
    private IEmailService               emailService;

    @Autowired
    private IChannelService             channelService;

    @Autowired
    private IChannelCatalogService      channelCatalogService;

    @Autowired
    private IInventoryService           inventoryService;

    @Autowired
    private IBundleService              bundleService;

    @Autowired
    private ContextAwareExecutorFactory executorFactory;

    @Autowired
    private ILockingService             lockingService;

    @Autowired
    private ITenantProfileService       tenantProfileService;

    @Autowired
    @Qualifier("activeMQConnector1")
    private ActiveMQConnector           activeMQConnector;

    @Autowired
    private IAsyncScriptService         asyncScriptService;

    @Autowired
    private ISaleOrderService           saleOrderService;

    @Override
    @LogActivity
    public UpdateInventoryOnChannelResponse updateAllInventoryOnChannel(UpdateInventoryOnChannelRequest request) {
        UpdateInventoryOnChannelResponse response = new UpdateInventoryOnChannelResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            for (String channelCode : request.getChannelCodes()) {
                Channel channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelByCode(channelCode);
                if (channel == null) {
                    response.getResultItems().add(new UpdateInventoryOnChannelResponse.ChannelInventorySyncResponseDTO(channelCode, "Invalid channel code"));
                } else {
                    channelCatalogService.markAllChannelItemTypesForRecalculation(channel.getId());
                    response.getResultItems().addAll(processChannelInventory(channel.getCode(), false, true).getResultItems());
                    if (ActivityContext.current().isEnable() && StringUtils.isNotBlank(UserContext.current().getUniwareUserName())) {
                        ActivityUtils.appendActivity(channelCode, ActivityEntityEnum.CHANNEL.getName(), channel.getSourceCode(),
                                Arrays.asList(new String[] { "Inventory Sync was triggered manually}" }), ActivityTypeEnum.SYNC_TRIGGERED.name());
                    }
                }
            }
        }
        response.setSuccessful(!context.hasErrors());
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    public UpdateInventoryOnAllChannelsResponse updateInventoryOnAllChannels(UpdateInventoryOnAllChannelsRequest request) {
        UpdateInventoryOnAllChannelsResponse response = new UpdateInventoryOnAllChannelsResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            Set<ChannelDetailDTO> channelsToProcess = new HashSet<>();
            for (ChannelDetailDTO channelDetailDTO : CacheManager.getInstance().getCache(ChannelCache.class).getChannels()) {
                if (channelDetailDTO.isEnabled() && SyncStatus.ON.name().equals(channelDetailDTO.getInventorySyncStatus())) {
                    channelsToProcess.add(channelDetailDTO);
                }
            }
            for (ChannelDetailDTO channel : channelsToProcess) {
                processChannelInventory(channel.getCode(), false, true);
            }
            response.setSuccessful(true);
        }
        response.setErrors(context.getErrors());
        return response;

    }

    @Override
    public UpdateInventoryOnChannelResponse updateInventoryOnChannel(UpdateInventoryOnChannelRequest request) {
        UpdateInventoryOnChannelResponse response = new UpdateInventoryOnChannelResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            for (String channelCode : request.getChannelCodes()) {
                Channel channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelByCode(channelCode);
                if (channel == null) {
                    response.getResultItems().add(new UpdateInventoryOnChannelResponse.ChannelInventorySyncResponseDTO(channelCode, "Invalid channel code"));
                } else {
                    UpdateInventoryOnChannelResponse processChannelInventoryResponse = processChannelInventory(channel.getCode(), false, true);
                    response.getResultItems().addAll(processChannelInventoryResponse.getResultItems());
                    if (processChannelInventoryResponse.hasErrors()) {
                        context.addErrors(processChannelInventoryResponse.getErrors());
                    }
                }
            }
        }
        response.setSuccessful(!context.hasErrors());
        response.addErrors(context.getErrors());
        return response;
    }

    @Override
    public GetChannelInventorySyncStatusResponse getChannelInventorySyncStatus(GetChannelInventorySyncStatusRequest request) {
        GetChannelInventorySyncStatusResponse response = new GetChannelInventorySyncStatusResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            response.setInventorySyncStatus(channelService.getChannelInventorySyncStatus(request.getChannelCode()));
            response.setSuccessful(true);
        } else {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    @Override
    public UpdateInventoryOnChannelResponse processChannelInventory(String channelCode, boolean processStockoutsOnly, boolean processPendingInventorySnapshots) {
        UpdateInventoryOnChannelResponse response = new UpdateInventoryOnChannelResponse();
        Channel channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelByCode(channelCode);
        if (channel == null) {
            response.getResultItems().add(new UpdateInventoryOnChannelResponse.ChannelInventorySyncResponseDTO(channelCode, "Invalid channel"));
        } else if (!channel.isEnabled()) {
            LOG.info("Channel disabled: {}", channel.getCode());
            response.getResultItems().add(new UpdateInventoryOnChannelResponse.ChannelInventorySyncResponseDTO(channel.getCode(), "Channel disabled"));
        } else if (!SyncStatus.ON.equals(channel.getInventorySyncStatus())) {
            LOG.info("Channel inventory sync disabled: {}", channel.getCode());
            response.getResultItems().add(new UpdateInventoryOnChannelResponse.ChannelInventorySyncResponseDTO(channel.getCode(), "Inventory Sync Disabled"));
        } else {
            java.util.concurrent.locks.Lock inventorySyncLock = lockingService.getLock(Namespace.CHANNEL_INVENTORY_SYNC_DO, channelCode);
            boolean lockAcquired = false;
            try {
                lockAcquired = inventorySyncLock.tryLock();
                if (lockAcquired) {
                    String message = "Request queued";
                    updateChannelInventorySyncStatus(channelCode, Channel.SyncExecutionStatus.QUEUED, message);
                    publishSyncChannelInventoryEvent(channelCode, processStockoutsOnly, processPendingInventorySnapshots);
                    response.setSuccessful(true);
                    response.getResultItems().add(new UpdateInventoryOnChannelResponse.ChannelInventorySyncResponseDTO(channelCode, message));
                    LOG.info("Queued SyncChannelInventoryEvent for channel: {}", channelCode);
                } else {
                    LOG.info("Failed to obtain {} lock for channel {}", Namespace.CHANNEL_INVENTORY_SYNC_DO.name(), channelCode);
                    response.addError(new WsError(WsResponseCode.ALREADY_RUNNING.code(), "Inventory Sync already running for channel " + channelCode));
                }
            } catch (Throwable t) {
                LOG.warn("Failed to trigger inventory sync. Marking syncStatus as IDLE", t);
                updateChannelInventorySyncStatus(channel.getCode(), Channel.SyncExecutionStatus.IDLE, "Last run trigger failed");
            } finally {
                if (lockAcquired) {
                    inventorySyncLock.unlock();
                }
            }
        }
        return response;
    }

    /**
     * @param channelCode
     * @param processStockoutsOnly
     * @param processPendingInventorySnapshots
     */
    private void publishSyncChannelInventoryEvent(String channelCode, boolean processStockoutsOnly, boolean processPendingInventorySnapshots) {
        SyncChannelInventoryEvent syncChannelInventoryEvent = new SyncChannelInventoryEvent();
        syncChannelInventoryEvent.setChannelCode(channelCode);
        syncChannelInventoryEvent.setProcessPendingInventorySnapshots(processPendingInventorySnapshots);
        syncChannelInventoryEvent.setProcessStockoutsOnly(processStockoutsOnly);
        syncChannelInventoryEvent.setRequestTimestamp(DateUtils.getCurrentTime());
        activeMQConnector.produceContextAwareMessage(MessagingConstants.Queue.INVENTORY_SYNC_QUEUE, syncChannelInventoryEvent);
    }

    /**
     * @param channelCode
     * @param syncExecutionStatus
     * @param message
     */
    private void updateChannelInventorySyncStatus(String channelCode, Channel.SyncExecutionStatus syncExecutionStatus, String message) {
        ChannelInventorySyncStatusDTO channelInventorySyncStatusDTO = channelService.getChannelInventorySyncStatus(channelCode);
        channelInventorySyncStatusDTO.setSyncExecutionStatus(syncExecutionStatus);
        channelInventorySyncStatusDTO.setMessage(message);
        channelService.updateChannelInventorySyncStatus(channelInventorySyncStatusDTO);
    }

    @Override
    public void doProcessChannelInventory(SyncChannelInventoryEvent syncChannelInventoryEvent) {
        executorFactory.getExecutor(SERVICE_NAME).submit(new SyncInventoryOnChannel(syncChannelInventoryEvent));
    }

    private class SyncInventoryOnChannel implements Runnable {

        private final SyncChannelInventoryEvent syncChannelInventoryEvent;

        public SyncInventoryOnChannel(SyncChannelInventoryEvent syncChannelInventoryEvent) {
            this.syncChannelInventoryEvent = syncChannelInventoryEvent;
        }

        @Override
        public void run() {
            TenantProfileVO tenantProfile = tenantProfileService.getTenantProfileByCode(UserContext.current().getTenant().getCode());
            if (StringUtils.equalsAny(tenantProfile.getPaymentStatus(), PaymentStatus.LIMITED_SERVICES, PaymentStatus.BLOCKED)) {
                LOG.info("Please recharge your account to allow this service");
            } else {
                final Channel channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelByCode(syncChannelInventoryEvent.getChannelCode());
                final ChannelInventorySyncStatusDTO channelInventorySyncStatusDTO = channelService.getChannelInventorySyncStatus(channel.getCode());
                java.util.concurrent.locks.Lock inventorySyncRunningLock = lockingService.getLock(Namespace.CHANNEL_INVENTORY_SYNC_DO, channel.getCode());
                boolean acquired = false;
                try {
                    acquired = acquireChannelInventorySyncLock(channelInventorySyncStatusDTO, inventorySyncRunningLock);
                    if (!acquired) {
                        LOG.info("Failed to obtain {} lock for channel {}. Sync is already running.", Namespace.CHANNEL_INVENTORY_SYNC_DO.name(), channel.getCode());
                    } else {
                        if (channelInventorySyncStatusDTO.getLastSyncTime() != null
                                && channelInventorySyncStatusDTO.getLastSyncTime().after(syncChannelInventoryEvent.getRequestTimestamp())) {
                            LOG.info("Ignoring SyncChannelInventoryEvent as lastSyncTime: {} is newer than requestTimestamp: {}", channelInventorySyncStatusDTO.getLastSyncTime(),
                                    syncChannelInventoryEvent.getRequestTimestamp());
                        } else {
                            runInventorySync(channelInventorySyncStatusDTO, channel);
                        }
                    }
                } finally {
                    if (acquired) {
                        inventorySyncRunningLock.unlock();
                    }
                }
            }
        }

        /**
         * Tries to acquire {@link Namespace#CHANNEL_INVENTORY_SYNC_DO} lock. If {@link ChannelInventorySyncStatusDTO}
         * was {@link com.uniware.core.entity.Channel.SyncExecutionStatus#QUEUED}, a try lock with a timeout of 60 sec
         * would be attempted. Else, a try lock without any timeout would be attempted.
         *
         * @param channelInventorySyncStatusDTO
         * @param inventorySyncRunningLock
         * @return
         */
        private boolean acquireChannelInventorySyncLock(ChannelInventorySyncStatusDTO channelInventorySyncStatusDTO, java.util.concurrent.locks.Lock inventorySyncRunningLock) {
            boolean acquired;
            switch (channelInventorySyncStatusDTO.getSyncExecutionStatus()) {
                case QUEUED:
                    // if we are here before the producer releases the lock, the
                    // status would be QUEUED.
                    try {
                        long defaultTimeout = CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class).getLockWaitTimeoutInSeconds();
                        acquired = inventorySyncRunningLock.tryLock(defaultTimeout, TimeUnit.SECONDS);
                    } catch (InterruptedException e) {
                        LOG.error("InterruptedException while waiting for lock", e);
                        throw new RuntimeException(e);
                    }
                    break;
                default:
                    acquired = inventorySyncRunningLock.tryLock();
                    break;
            }
            return acquired;
        }

        private void runInventorySync(final ChannelInventorySyncStatusDTO channelInventorySyncStatusDTO, final Channel channel) {
            channelInventorySyncStatusDTO.reset();
            channelInventorySyncStatusDTO.setLastSyncTime(DateUtils.getCurrentTime());
            channelInventorySyncStatusDTO.setSyncExecutionStatus(Channel.SyncExecutionStatus.RUNNING);
            channelInventorySyncStatusDTO.setMessage("Sync Started");
            LOG.info("Channel inventory sync scheduled: {}", channel.getCode());
            channelService.updateChannelInventorySyncStatus(channelInventorySyncStatusDTO);
            // process pending inventory snapshots, if required
            if (syncChannelInventoryEvent.isProcessPendingInventorySnapshots()) {
                try {
                    long startTime = System.currentTimeMillis();
                    LOG.info("Acknowledging pending inventory snapshots, channel: {}", channel.getCode());
                    channelInventorySyncStatusDTO.setMessage("Acknowledging pending inventory snapshots");
                    channelService.updateChannelInventorySyncStatus(channelInventorySyncStatusDTO);
                    acknowledgePendingInventorySnapshots();
                    LOG.info("Done acknowledging pending inventory snapshots in {} ms, channel: {}", System.currentTimeMillis() - startTime, channel.getCode());
                } catch (Throwable e) {
                    LOG.error("Error acknowledging pending inventory snapshots", e);
                }
            } else {
                LOG.info("Channel inventory sync acknowledging snapshot skipped: {}", channel.getCode());
            }
            // sync inventory
            try {
                LOG.info("Running SyncInventoryOnChannelTask for Channel {}", channel.getName());
                long startTime = System.currentTimeMillis();
                LOG.info("Verifying inventory update connectors for channel {}", channel.getName());
                channelInventorySyncStatusDTO.setMessage("Verifying connectors");
                channelService.updateChannelInventorySyncStatus(channelInventorySyncStatusDTO);
                final ChannelGoodToSyncResponse channelGoodToSyncResponse = channelService.isChannelGoodToSync(channel.getCode(), ChannelServiceImpl.Sync.INVENTORY);
                if (!channelGoodToSyncResponse.isGoodToSync()) {
                    channelInventorySyncStatusDTO.reset();
                    channelInventorySyncStatusDTO.setMessage("Connector is broken");
                    // TODO remove
                    channelService.updateChannelInventorySyncStatus(channelInventorySyncStatusDTO);
                    LOG.warn("Connector broken for channel {}", channel.getName());
                    return;
                }
                channelInventorySyncStatusDTO.setMessage("Connectors verified");
                final Date lastUpdated = syncChannelInventoryEvent.isProcessStockoutsOnly() ? channelCatalogService.getLatestStockoutDirtyTimestamp(channel.getId())
                        : channelCatalogService.getLatestDirtyTimestamp(channel.getId());
                if (lastUpdated == null) {
                    LOG.warn("No pending notifications for channel {}", channel.getName());
                    return;
                }
                long totalCount = syncChannelInventoryEvent.isProcessStockoutsOnly() ? channelCatalogService.getDirtyStockoutChannelItemTypeCount(channel.getId(), lastUpdated)
                        : channelCatalogService.getDirtyChannelItemTypeCount(channel.getId(), lastUpdated);
                channelInventorySyncStatusDTO.setTotalMileStones(totalCount);
                channelService.updateChannelInventorySyncStatus(channelInventorySyncStatusDTO);
                LOG.info("Channel: {}, Number of pending notifications: {}", channel.getName(), totalCount);
                final Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(channel.getSourceCode());
                final int processInventoryCount = CacheManager.getInstance().getCache(ChannelCache.class).getProcessInventoryCount(channel.getCode());
                int batchSize = DEFAULT_BATCH_SIZE;
                if (processInventoryCount > DEFAULT_BATCH_SIZE) {
                    batchSize = processInventoryCount;
                }
                BatchProcessor<ChannelItemType> processor = new BatchProcessor<ChannelItemType>(batchSize) {
                    @Override
                    protected void process(List<ChannelItemType> inventorySnapshots, int batchIndex) {
                        if (ConfigurationManager.getInstance().getConfiguration(ProductConfiguration.class).isProductManagementSwitchedOff()) {
                            Iterator<ChannelItemType> citIterator = inventorySnapshots.iterator();
                            while (citIterator.hasNext()) {
                                ChannelItemType cit = citIterator.next();
                                SkipReason skipReason = validateChannelItemTypeForInventoryUpdate(source.isUseSellerSkuForInventoryUpdate(), channel.isProductRelistingEnabled(),
                                        cit, cit.getNextInventoryUpdate());
                                if (skipReason != null) {
                                    LOG.info("Skipping inventory update for channelProductID: {}. Reason: {}", cit.getChannelProductId(), skipReason);
                                    citIterator.remove();
                                }
                            }
                        }
                        ScraperScript updateInventoryScript = CacheManager.getInstance().getCache(ChannelCache.class).getScriptByName(channel.getCode(),
                                com.uniware.core.entity.Source.INVENTORY_UPDATE_SCRIPT_NAME);
                        if (updateInventoryScript != null) {
                            Iterator<List<ChannelItemType>> iterator = CollectionUtils.sublistIterator(inventorySnapshots, processInventoryCount);
                            while (iterator.hasNext()) {
                                List<ChannelItemType> channelQuanta = iterator.next();
                                syncInventoryOnChannel(channel, channelGoodToSyncResponse.getParams(), source, channelQuanta, updateInventoryScript, channelInventorySyncStatusDTO,
                                        null);
                                channelService.updateChannelInventorySyncStatus(channelInventorySyncStatusDTO);
                            }
                        }
                    }

                    @Override
                    protected List<ChannelItemType> next(int start, int batchSize) {
                        LOG.info("Page: {}, BatchSize: {}", start + 1, batchSize);
                        return syncChannelInventoryEvent.isProcessStockoutsOnly()
                                ? channelCatalogService.getDirtyStockoutChannelItemTypes(channel.getId(), 0, batchSize, lastUpdated)
                                : channelCatalogService.getDirtyChannelItemTypes(channel.getId(), 0, batchSize, lastUpdated);
                    }
                };
                processor.process();
                channelCatalogService.resetFailedChannelItemTypes(channel.getId());
                LOG.info("Completed SyncInventoryOnChannelTask for channel: {} in {} ms", channel.getName(), System.currentTimeMillis() - startTime);
            } catch (Throwable e) {
                LOG.error("Error while updating inventory on channel: {}", channel.getCode());
                LOG.error("Stack trace: ", e);
            } finally {
                updateStatus(channelInventorySyncStatusDTO, channel);
            }
        }

        private void updateStatus(ChannelInventorySyncStatusDTO inventorySyncStatusDTO, Channel channel) {
            try {
                if (inventorySyncStatusDTO.getLastFailedImportCount() > 0) {
                    sendSyncFailedNotification(inventorySyncStatusDTO, channel);
                    inventorySyncStatusDTO.setLastSyncFailedNotificationTime(DateUtils.getCurrentTime());
                }
                if (inventorySyncStatusDTO.getLastSuccessfulImportCount() > 0) {
                    inventorySyncStatusDTO.setLastSuccessfulInventorySyncTime(DateUtils.getCurrentTime());
                    inventorySyncStatusDTO.setLastSyncSuccessful(true);
                } else if (inventorySyncStatusDTO.getCurrentMileStone() == 0) {
                    inventorySyncStatusDTO.setLastSyncSuccessful(true);
                }
                inventorySyncStatusDTO.setFailedCount(channelCatalogService.getChannelItemTypeCount(channel.getId(), ChannelInventoryUpdateVO.Status.FAILED.name()));
                inventorySyncStatusDTO.setTodaysSuccessfulImportCount(channelCatalogService.getChannelItemTypeCount(channel.getId(), ChannelInventoryUpdateVO.Status.SUCCESS.name(),
                        new DateUtils.DateRange(DateUtils.getCurrentDate(), DateUtils.getCurrentTime())));
                inventorySyncStatusDTO.setLastSyncTime(DateUtils.getCurrentTime());
                inventorySyncStatusDTO.setSyncExecutionStatus(Channel.SyncExecutionStatus.IDLE);
                LOG.info("Updating inventorySynStatus: {}", inventorySyncStatusDTO);
                channelService.updateChannelInventorySyncStatus(inventorySyncStatusDTO);
                LOG.info("Channel: {}, Done updating status in mongo", channel.getName());
            } catch (Throwable t) {
                LOG.error("UpdateStatus threw exception", t);
            }
        }
    }

    private void acknowledgePendingNonBundleInventorySnapshotsV2(Set<ChannelDetailDTO> channels, Map<Integer, Map<String, Long>> channelIdToUnprocessedOrderInventory) {
        LOG.info("Acknowledging pending inventory snapshots for non-bundle item types.");
        BatchProcessor<Integer> snapshotBatchProcessor = new BatchProcessor<Integer>(100) {
            @Override
            protected List<Integer> next(int start, int batchSize) {
                return inventoryService.getPendingNonBundleItemTypesForInventoryNotifications(start, batchSize);
            }

            @Override
            protected void process(List<Integer> itemTypeIds, int batchIndex) {
                if (itemTypeIds.size() > 0) {
                    Date lastAcknowledgedTime = DateUtils.getCurrentTime();
                    Map<Integer, Map<Integer, ItemTypeInventorySnapshot>> itemTypeIdToFacilityToSnapshot = new HashMap<>(100);
                    inventoryService.getItemTypeInventorySnapshots(itemTypeIds).forEach(
                            itis -> itemTypeIdToFacilityToSnapshot.computeIfAbsent(itis.getItemType().getId(), k -> new HashMap<>()).put(itis.getFacility().getId(), itis));
                    itemTypeIdToFacilityToSnapshot.forEach((itemTypeId, facilityToSnapshot) -> {
                        try {
                            acknowledgeInventorySnapshot(itemTypeId, facilityToSnapshot, channels, lastAcknowledgedTime, channelIdToUnprocessedOrderInventory);
                        } catch (Exception e) {
                            LOG.error("Error in acknowledging inventory snapshot for itemType : {}", itemTypeId);
                            LOG.error("Stack trace: ", e);
                        }
                    });
                }
            }
        };
        snapshotBatchProcessor.process();
        LOG.info("Done Acknowledging pending inventory snapshots for non-bundle item types.");
    }

    @Transactional
    private void acknowledgeInventorySnapshot(Integer itemTypeId, Map<Integer, ItemTypeInventorySnapshot> facilityToSnapshot, Set<ChannelDetailDTO> channels,
            Date lastAcknowledgedTime, Map<Integer, Map<String, Long>> channelIdToUnprocessedOrderInventory) {
        ItemType itemType = catalogService.getNonBundledItemTypeById(itemTypeId);
        for (ChannelDetailDTO channelDTO : channels) {
            if (channelDTO.isEnabled()) {
                Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(channelDTO.getSource().getCode());
                Map<String, Long> channelProductIdToUnprocessedOrderInventory = channelIdToUnprocessedOrderInventory.get(channelDTO.getId());
                List<Integer> associatedFacilityIds = null;
                if (source.isFacilityAssociationRequired()) {
                    Facility facility = CacheManager.getInstance().getCache(FacilityCache.class).getFacilityByCode(channelDTO.getAssociatedFacility());
                    if (facility != null) {
                        associatedFacilityIds = Collections.singletonList(facility.getId());
                    } else {
                        LOG.warn("Facility not found for code: {}", channelDTO.getAssociatedFacility());
                        continue;
                    }
                }
                if (CollectionUtils.isEmpty(associatedFacilityIds)) {
                    associatedFacilityIds = channelDTO.getAssociatedFacilityIds();
                }
                InventorySnapshotDTO processingInventorySnapshot = getInventorySnapshotToProcess(itemType, facilityToSnapshot, associatedFacilityIds);
                if (processingInventorySnapshot != null) {
                    processChannelItemTypesForSnapshot(itemType, channelDTO, source, channelProductIdToUnprocessedOrderInventory, processingInventorySnapshot);
                }
            }
        }
        inventoryDao.markInventoryUpdateAcknowledged(itemType.getId(), lastAcknowledgedTime);
        inventoryService.markBundlesUnacknowledged(bundleService.getBundlesByComponentItemType(itemType.getSkuCode()));

    }

    private void processChannelItemTypesForSnapshot(ItemType itemType, ChannelDetailDTO channelDTO, Source source, Map<String, Long> channelProductIdToUnprocessedOrderInventory,
            InventorySnapshotDTO processingInventorySnapshot) {
        List<ChannelItemType> channelItemTypes = channelCatalogService.getChannelItemTypesByChannelAndItemTypeId(channelDTO.getId(), itemType.getId());
        if (channelItemTypes.isEmpty() && Source.Code.CUSTOM.name().equals(source.getCode()) && SyncStatus.ON.name().equals(channelDTO.getInventorySyncStatus())) {
            WsChannelItemType wsChannelItemType = new WsChannelItemType();
            wsChannelItemType.setChannelCode(channelDTO.getCode());
            wsChannelItemType.setChannelProductId(itemType.getSkuCode());
            wsChannelItemType.setSellerSkuCode(itemType.getSkuCode());
            wsChannelItemType.setSkuCode(itemType.getSkuCode());
            wsChannelItemType.setProductName(itemType.getName());
            wsChannelItemType.setVerified(true);
            wsChannelItemType.setLive(true);
            channelCatalogService.createChannelItemType(new CreateChannelItemTypeRequest(wsChannelItemType, ChannelItemType.SyncedBy.SYSTEM.toString()));
        } else {
            for (ChannelItemType channelItemType : channelItemTypes) {
                try {
                    final Long unprocessedOrderInventory = channelProductIdToUnprocessedOrderInventory != null
                            ? channelProductIdToUnprocessedOrderInventory.get(channelItemType.getChannelProductId()) : null;
                    markChannelItemTypeDirty(channelItemType, itemType, processingInventorySnapshot, channelDTO, unprocessedOrderInventory);
                } catch (Exception e) {
                    LOG.error("Error evaluating inventory for channelItemType: {}", channelItemType);
                }
            }
        }
    }

    private InventorySnapshotDTO getInventorySnapshotToProcess(ItemType itemType, Map<Integer, ItemTypeInventorySnapshot> facilityIdToSnapshot, List<Integer> facilityIds) {
        InventorySnapshotDTO inventorySnapshotDTO = null;
        for (Entry<Integer, ItemTypeInventorySnapshot> entry : facilityIdToSnapshot.entrySet()) {
            ItemTypeInventorySnapshot snapshot = entry.getValue();
            if (CollectionUtils.isEmpty(facilityIds) || facilityIds.contains(entry.getKey())) {
                inventorySnapshotDTO = inventorySnapshotDTO == null ? new InventorySnapshotDTO(itemType, snapshot) : inventorySnapshotDTO.add(snapshot);
            }
        }
        return inventorySnapshotDTO;
    }

    @Override
    @Locks({ @Lock(ns = Namespace.ACKNOWLEDGE_PENDING_SNAPSHOTS, key = "ACKNOWLEDGE_PENDING_SNAPSHOTS") })
    public void acknowledgePendingInventorySnapshots() {
        LOG.info("Acknowledging pending inventory snapshots.");
        long startTime = System.currentTimeMillis();
        Set<ChannelDetailDTO> channels = CacheManager.getInstance().getCache(ChannelCache.class).getChannels();
        final Map<Integer, Map<String, Long>> channelIdToUnprocessedOrderInventory = new HashMap<>();
        channels.forEach(channel -> channelIdToUnprocessedOrderInventory.put(channel.getId(), saleOrderService.getUnprocessedSaleOrderItemsCount(channel.getId())));

        // Acknowledge Non bundle inventory snapshots.
        if (ConfigurationManager.getInstance().getConfiguration(TenantSystemConfiguration.class).isSelectiveFacilityInventorySyncEnabled()) {
            LOG.info("Acknowledging pending inventory snapshots for channel associated facilities only");
            acknowledgePendingNonBundleInventorySnapshotsV2(channels, channelIdToUnprocessedOrderInventory);
        } else {
            acknowledgePendingNonBundleInventorySnapshots(channels, channelIdToUnprocessedOrderInventory);
        }

        // Acknowledge bundle inventory snapshots.
        acknowledgePendingBundleInventorySnapshots(channels, channelIdToUnprocessedOrderInventory);
        // Process pending for recalculation channel item types
        processPendingChannelItemTypesForRecalculation();
        LOG.info("Done acknowledging pending inventory snapshots in {} ms.", System.currentTimeMillis() - startTime);
    }

    private void acknowledgePendingNonBundleInventorySnapshots(Set<ChannelDetailDTO> channels, Map<Integer, Map<String, Long>> channelIdToUnprocessedOrderInventory) {
        Date lastAcknowledgedTime = DateUtils.getCurrentTime();
        List<InventorySnapshotDTO> inventorySnapshots = inventoryService.getPendingNonBundleItemTypeInventoryNotifications();
        if (!inventorySnapshots.isEmpty()) {
            for (InventorySnapshotDTO inventorySnapshotDTO : inventorySnapshots) {
                try {
                    acknowledgeInventorySnapshot(inventorySnapshotDTO, channels, lastAcknowledgedTime, channelIdToUnprocessedOrderInventory);
                } catch (Exception e) {
                    LOG.error("Error in acknowledging inventory snapshot for itemType : {}", inventorySnapshotDTO.getItemTypeId());
                    LOG.error("Stack trace: ", e);
                }
                inventoryService.markBundlesUnacknowledged(bundleService.getBundlesByComponentItemType(inventorySnapshotDTO.getItemSku()));
            }
        }
    }

    private void acknowledgePendingBundleInventorySnapshots(Set<ChannelDetailDTO> channels, Map<Integer, Map<String, Long>> channelIdToUnprocessedOrderInventory) {
        Date lastAcknowledgedTime = DateUtils.getCurrentTime();
        List<InventorySnapshotDTO> bundleInventorySnapshots = inventoryService.getPendingBundleItemTypeInventoryNotifications();
        for (InventorySnapshotDTO bundleInventorySnapshotDTO : bundleInventorySnapshots) {
            try {
                acknowledgeInventorySnapshot(bundleInventorySnapshotDTO, channels, lastAcknowledgedTime, channelIdToUnprocessedOrderInventory);
            } catch (Exception e) {
                LOG.error("Error in acknowledging bundle inventory snapshot for itemType : {}", bundleInventorySnapshotDTO.getItemTypeId());
                LOG.error("Stack trace: ", e);
            }
        }
    }

    private void processPendingChannelItemTypesForRecalculation() {
        BatchProcessor<ChannelItemType> batchProcessor = new BatchProcessor<ChannelItemType>(100) {
            @Override
            protected void process(List<ChannelItemType> channelItemTypes, int batchIndex) {
                for (ChannelItemType channelItemType : channelItemTypes) {
                    markChannelItemTypeDirty(channelItemType);
                }
            }

            @Override
            protected List<ChannelItemType> next(int start, int batchSize) {
                LOG.info("Reapplying inventory formula for channel item types, page: {}", start / batchSize);
                return channelCatalogService.getPendingChannelItemTypeForRecalculation(0, batchSize);
            }
        };
        batchProcessor.process();
    }

    @Override
    @Transactional
    public void acknowledgeInventorySnapshot(InventorySnapshotDTO inventorySnapshot, Set<ChannelDetailDTO> channels, Date lastAcknowledgedTime,
            Map<Integer, Map<String, Long>> channelIdToUnprocessedOrderInventory) {
        ItemType itemType;
        for (ChannelDetailDTO channelDTO : channels) {
            if (channelDTO.isEnabled()) {
                boolean skipInventorySnapshot = false;
                Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(channelDTO.getSource().getCode());
                itemType = catalogService.getAllItemTypeBySkuCode(inventorySnapshot.getItemSku());
                Map<String, Long> channelProductIdToUnprocessedOrderInventory = channelIdToUnprocessedOrderInventory.get(channelDTO.getId());
                InventorySnapshotDTO processingInventorySnapshot = inventorySnapshot;
                if (source.isFacilityAssociationRequired()) {
                    Facility facility = CacheManager.getInstance().getCache(FacilityCache.class).getFacilityByCode(channelDTO.getAssociatedFacility());
                    if (facility != null) {
                        processingInventorySnapshot = getItemTypeInventoryNotification(inventorySnapshot.getItemTypeId(), Arrays.asList(facility.getId()));
                        if (processingInventorySnapshot == null) {
                            skipInventorySnapshot = true;
                            LOG.info("No inventory snapshot for facility : {}, channel : {}", facility.getCode(), channelDTO.getCode());
                        } else {
                            itemType = catalogService.getAllItemTypeBySkuCode(processingInventorySnapshot.getItemSku());
                        }
                    } else {
                        LOG.warn("Facility not found for code: {}", channelDTO.getAssociatedFacility());
                    }
                }
                if (!skipInventorySnapshot) {
                    processChannelItemTypesForSnapshot(itemType, channelDTO, source, channelProductIdToUnprocessedOrderInventory, processingInventorySnapshot);
                }
            }
        }
        inventoryDao.markInventoryUpdateAcknowledged(inventorySnapshot.getItemTypeId(), lastAcknowledgedTime);
    }

    @Override
    @Transactional
    public ChannelItemType markChannelItemTypeDirty(ChannelItemType channelItemType) {
        ChannelDetailDTO channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelDetailDTOById(channelItemType.getChannel().getId());
        channelItemType = channelCatalogService.getChannelItemTypeByChannelAndChannelProductId(channel.getCode(), channelItemType.getChannelProductId());
        if (ChannelItemType.Status.LINKED.equals(channelItemType.getStatusCode())) {
            ItemType itemType = catalogService.getAllItemTypeById(channelItemType.getItemType().getId());
            Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(channel.getSource().getCode());
            InventorySnapshotDTO inventorySnapshot = inventoryDao.getItemTypeInventoryNotification(itemType.getId(), channel.getFacilitiesForChannelInventorySync());
            Long unprocessedOrderInventory = saleOrderService.getUnprocessedSaleOrderItemCountByChannelProductId(channel.getId(), channelItemType.getChannelProductId());
            return markChannelItemTypeDirty(channelItemType, itemType, inventorySnapshot, channel, unprocessedOrderInventory);
        }
        return channelItemType;
    }

    private ChannelItemType markChannelItemTypeDirty(ChannelItemType channelItemType, ItemType itemType, InventorySnapshotDTO inventorySnapshot, ChannelDetailDTO channelDTO,
            Long unprocessedOrderInventory) {
        String inventoryUpdateFormula = channelDTO.getInventoryUpdateFormula();
        CalculateChannelItemTypeInventoryResponse calculateInventoryResponse;
        try {
            calculateInventoryResponse = calculateChannelItemTypeInventory(channelDTO, inventoryUpdateFormula, inventorySnapshot, itemType, channelItemType,
                    unprocessedOrderInventory);
        } catch (Exception e) {
            LOG.error("Unable to calculate channel item type inventory for channel: " + channelDTO.getCode() + ", channelProductId: " + channelItemType.getChannelProductId(), e);
            throw e;
        }
        channelItemType.setRecalculateInventory(false);
        long calculatedInventory = calculateInventoryResponse.getCalculatedInventory();
        channelItemType.setNextInventoryUpdate((int) calculatedInventory);
        channelItemType.setRecalculateInventory(false);
        if (channelItemType.getSnapshotReferenceId() == null || !calculateInventoryResponse.getChannelInventoryUpdateSnapshot().equals(
                channelInventoryMao.getInventoryUpdateSnapshotById(channelItemType.getSnapshotReferenceId()))) {
            ChannelInventoryUpdateSnapshotVO snapshot = channelInventoryMao.create(calculateInventoryResponse.getChannelInventoryUpdateSnapshot());
            channelItemType.setSnapshotReferenceId(snapshot.getId());
        }
        SkipReason skipReason = validateChannelItemTypeForInventoryUpdate(channelDTO.getSource().isUseSellerSkuForInventoryUpdate(), channelDTO.isProductRelistingEnabled(),
                channelItemType, calculatedInventory);
        if (skipReason != null) {
            LOG.info("Skipping channelItemType {}, calculatedInventory {}, skipReason: {}", new Object[] { channelItemType, calculatedInventory, skipReason });
            if (skipReason.equals(SkipReason.REDUNDANT)) {
                channelItemType.setPossiblyDirty(true);
            }
        } else {
            LOG.info("Marking channelItemType {} dirty, calculatedInventory {}", channelItemType, calculatedInventory);
            channelItemType.setDirty(true);
            channelItemType.setPossiblyDirty(false);
            if (ListingStatus.DELISTED.equals(channelItemType.getListingStatus())) {
                channelItemType.setRelist(true);
            }
        }
        return channelCatalogService.updateChannelItemType(channelItemType);
    }

    private SkipReason validateChannelItemTypeForInventoryUpdate(boolean useSellerSkuForInventoryUpdate, boolean relistingEnabled, ChannelItemType channelItemType,
            long calculatedInventory) {
        SkipReason skipReason = null;
        if (channelItemType.isDisabled()) {
            skipReason = SkipReason.DISABLED;
        } else if (ListingStatus.DISCONTINUED.equals(channelItemType.getListingStatus())) {
            skipReason = SkipReason.DISCONTINUED;
        } else if (ListingStatus.RELISTED.equals(channelItemType.getListingStatus())) {
            skipReason = SkipReason.RELISTED;
        } else if (ListingStatus.DELISTED.equals(channelItemType.getListingStatus()) && !relistingEnabled) {
            skipReason = SkipReason.DELISTED;
        } else if (channelItemType.isDisabledDueToErrors()) {
            skipReason = SkipReason.DISABLED_DUE_TO_ERRORS;
        } else if (!useSellerSkuForInventoryUpdate && !channelItemType.isVerified()) {
            skipReason = SkipReason.NOT_VERIFIED;
        } else if (ChannelInventoryUpdateVO.Status.SUCCESS.name().equals(channelItemType.getLastInventoryUpdateStatus()) && channelItemType.getLastInventoryUpdate() != null
                && calculatedInventory == channelItemType.getLastInventoryUpdate()) {
            skipReason = SkipReason.REDUNDANT;
        }
        return skipReason;
    }

    @Override
    @Transactional
    public CalculateChannelItemTypeInventoryResponse calculateChannelItemTypeInventory(ChannelDetailDTO channelDTO, String formula, InventorySnapshotDTO inventorySnapshot,
            ItemType itemType, ChannelItemType channelItemType, Long unprocessedOrderInventory) {
        Channel channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelByCode(channelDTO.getCode());
        CalculateChannelItemTypeInventoryResponse response = new CalculateChannelItemTypeInventoryResponse();
        long calculatedInventory = Long.MAX_VALUE;
        ChannelInventoryUpdateSnapshotVO snapshot = new ChannelInventoryUpdateSnapshotVO(inventorySnapshot, channelItemType, channel.getInventoryUpdateFormula());
        if (ItemType.Type.BUNDLE.equals(itemType.getType())) {
            Bundle bundle = bundleService.getBundleByCode(itemType.getSkuCode());

            // Calculate total component quantity to evaluate unprocessed order inventory
            if (unprocessedOrderInventory != null) {
                int totalComponentQuantity = 0;
                for (BundleItemType bundleItemType : bundle.getBundleItemTypes()) {
                    totalComponentQuantity += bundleItemType.getQuantity();
                }
                unprocessedOrderInventory = unprocessedOrderInventory / totalComponentQuantity;
            }

            List<ComponentItemTypeInventory> componentItemTypeInventories = new ArrayList<>();
            for (BundleItemType bundleItemType : bundle.getBundleItemTypes()) {
                boolean bundleItemTypeIncluded = false;
                for (ChannelItemType cit : channelCatalogService.getChannelItemTypesByChannelAndItemTypeId(channelDTO.getId(), bundleItemType.getItemType().getId())) {
                    if (cit.getNextInventoryUpdate() != null) {
                        Long channelItemTypeInventory = (long) (cit.getNextInventoryUpdate() / bundleItemType.getQuantity());
                        calculatedInventory = Math.min(calculatedInventory, channelItemTypeInventory);
                        bundleItemTypeIncluded = true;
                        componentItemTypeInventories.add(
                                new ComponentItemTypeInventory(bundleItemType.getItemType().getSkuCode(), cit.getChannelProductId(), channelItemTypeInventory));
                    }
                }
                if (!bundleItemTypeIncluded) {
                    InventorySnapshotDTO bundleItemInventorySnapshot = inventoryDao.getItemTypeInventoryNotification(bundleItemType.getItemType().getId(),
                            channelDTO.getFacilitiesForChannelInventorySync());
                    Long channelItemTypeInventory = doCalculateChannelItemTypeInventory(channelDTO, formula, bundleItemInventorySnapshot, bundleItemType.getItemType(),
                            new ChannelItemType(), null, unprocessedOrderInventory);
                    channelItemTypeInventory = channelItemTypeInventory / bundleItemType.getQuantity();
                    calculatedInventory = Math.min(calculatedInventory, channelItemTypeInventory);
                    componentItemTypeInventories.add(new ComponentItemTypeInventory(bundleItemType.getItemType().getSkuCode(), null, channelItemTypeInventory));
                }
            }
            snapshot.setComponentItemTypeInventories(componentItemTypeInventories);
        } else {
            calculatedInventory = doCalculateChannelItemTypeInventory(channelDTO, formula, inventorySnapshot, itemType, channelItemType, snapshot, unprocessedOrderInventory);
        }
        snapshot.setCalculatedInventory(calculatedInventory);
        response.setSuccessful(true);
        response.setChannelInventoryUpdateSnapshot(snapshot);
        response.setCalculatedInventory(calculatedInventory);
        return response;
    }

    // This method only runs for itemType with type as {@link com.uniware.core.entity.ItemType.Type.SIMPLE}
    @Transactional
    private long doCalculateChannelItemTypeInventory(ChannelDetailDTO channel, String formula, InventorySnapshotDTO inventorySnapshot, ItemType itemType,
            ChannelItemType channelItemType, ChannelInventoryUpdateSnapshotVO snapshot, Long unprocessedOrderInventory) {
        long calculatedInventory = inventorySnapshot != null ? inventorySnapshot.getInventory() : 0;
        if (inventorySnapshot != null) {
            Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(channel.getSource().getCode());
            formula = StringUtils.isNotBlank(formula) ? formula
                    : StringUtils.isNotBlank(source.getDefaultInventoryUpdateFormula()) ? source.getDefaultInventoryUpdateFormula() : DEFAULT_FORMULA;
            String formulaCode;
            if (StringUtils.isBlank(formula)) {
                if (StringUtils.isNotBlank(source.getDefaultInventoryUpdateFormula())) {
                    formulaCode = source.getDefaultInventoryUpdateFormulaCode();
                    formula = source.getDefaultInventoryUpdateFormula();
                } else {
                    formulaCode = DEFAULT_FORMULA_CODE;
                    formula = DEFAULT_FORMULA;
                }
            } else {
                // Need to get formula code from channel entity as channel dto contains the actual formula.
                Channel c = CacheManager.getInstance().getCache(ChannelCache.class).getChannelByCode(channel.getCode());
                formulaCode = c.getInventoryUpdateFormula();
            }

            Expression calculatedInventoryExpression = Expression.compile(formula);
            Map<String, Object> contextParams = new HashMap<>();
            contextParams.put("itemType", itemType);
            contextParams.put("inventorySnapshot", inventorySnapshot);
            if (channelItemType != null) {
                int totalBlockedInventory = 0;
                int totalFailedOrderInventory = 0;
                int totalPendency = 0;
                List<ChannelItemType> channelItemTypes = channelCatalogService.getChannelItemTypesByItemTypeId(itemType.getId());
                for (ChannelItemType cit : channelItemTypes) {
                    totalBlockedInventory += Math.max(cit.getBlockedInventory() - cit.getFailedOrderInventory(), 0);
                    totalFailedOrderInventory += cit.getFailedOrderInventory();
                    totalPendency += cit.getPendency();
                }
                List<Bundle> bundles = bundleService.getBundlesByComponentItemType(itemType.getSkuCode());
                int bundlePendency = 0;
                for (Bundle bundle : bundles) {
                    List<ChannelItemType> bundleChannelItemTypes = channelCatalogService.getChannelItemTypesByItemTypeId(bundle.getItemType().getId());
                    if (!CollectionUtils.isEmpty(bundleChannelItemTypes)) {
                        BundleItemType bundleItemType = bundle.getBundleItemTypes().stream().filter(
                                componentItemType -> componentItemType.getItemType().getId().equals(itemType.getId())).findFirst().orElse(null);
                        if (bundleItemType != null) {
                            int componentQuantity = bundleItemType.getQuantity();
                            for (ChannelItemType bundleCit : bundleChannelItemTypes) {
                                bundlePendency += bundleCit.getPendency() * componentQuantity;
                            }
                        }
                    }
                }
                totalPendency += bundlePendency;
                for (CustomFieldMetadataDTO customFieldMetadataDTO : channel.getCustomFieldValues()) {
                    contextParams.put(customFieldMetadataDTO.getFieldName(), customFieldMetadataDTO.getFieldValue());
                }
                contextParams.put("channelItemType", channelItemType);
                int blockedInventoryOnOtherChannels = totalBlockedInventory - Math.max(channelItemType.getBlockedInventory() - channelItemType.getFailedOrderInventory(), 0);
                long unserviceableSaleOrderItemCount = saleOrderService.getUnserviceableSaleOrderItemsCountByChannelProductId(channelItemType.getChannelProductId());
                blockedInventoryOnOtherChannels += unserviceableSaleOrderItemCount;
                contextParams.put("inventoryBlockedOnOtherChannels", blockedInventoryOnOtherChannels);
                contextParams.put("pendencyOnOtherChannels", totalPendency - channelItemType.getPendency());
                contextParams.put("pendency", totalPendency);
                contextParams.put("unprocessedOrderInventory", unprocessedOrderInventory != null ? unprocessedOrderInventory : 0);
                contextParams.put("failedOrderInventory", totalFailedOrderInventory);
                if (snapshot != null) {
                    snapshot.setFormulaCode(formulaCode);
                    snapshot.setInventoryBlockedOnOtherChannels(blockedInventoryOnOtherChannels);
                    snapshot.setUnprocessedOrderInventory(unprocessedOrderInventory != null ? unprocessedOrderInventory : 0);
                    snapshot.setTotalPendency(totalPendency);
                    snapshot.setPendencyOnOtherChannels(totalPendency - channelItemType.getPendency());
                }
            }
            try {
                calculatedInventory = calculatedInventoryExpression.evaluate(contextParams, Long.class);
            } catch (Exception e) {
                LOG.error("Error evaluating inventory formula for channel {}, formula {}", channel.getCode(), formula);
                LOG.error("Error: ", e);
                throw new RuntimeException("Error evaluating inventory formula for channel: " + channel.getCode(), e);
            }
            calculatedInventory = Math.max(calculatedInventory, 0);
        }
        return calculatedInventory;
    }

    @Transactional
    private void syncInventoryOnChannel(Channel channel, Map<String, String> channelParams, Source source, List<ChannelItemType> channelItemTypes,
            ScraperScript updateInventoryScript, ChannelInventorySyncStatusDTO inventorySyncStatusVO, AsyncScriptInstruction task) {
        ScriptExecutionContext context = ScriptExecutionContext.current();
        ChannelInventoryUpdateVO.Status status = ChannelInventoryUpdateVO.Status.SUCCESS;
        int processInventoryCount = CacheManager.getInstance().getCache(ChannelCache.class).getProcessInventoryCount(channel.getCode());
        String failureReason;
        String failureCode;
        for (Entry<String, String> e : channelParams.entrySet()) {
            context.addVariable(e.getKey(), e.getValue());
        }
        context.addVariable("source", source);
        context.addVariable("channel", channel);
        context.addVariable("applicationContext", applicationContext);
        Map<String, ChannelInventoryUpdateDTO> channelProductIdToInventoryUpdate = prepareChannelInventoryUpdateDTOS(channelItemTypes);
        List<ChannelInventoryUpdateDTO> channelInventoryUpdates = new ArrayList<>(channelProductIdToInventoryUpdate.values());
        if (processInventoryCount == 1) {
            context.addVariable("inventorySnapshot", channelInventoryUpdates.get(0));
        } else {
            context.addVariable("inventorySnapshot", channelInventoryUpdates);
        }
        Map<String, ChannelScriptError> failedInventorySnapshots = new HashMap(channelInventoryUpdates.size());
        Map<String, String> successfulInventorySnapshots = new HashMap(channelInventoryUpdates.size());
        Map<String, String> processingInventorySnapshots = new HashMap(channelInventoryUpdates.size());
        context.addVariable("failedInventorySnapshots", failedInventorySnapshots);
        context.addVariable("successfulInventorySnapshots", successfulInventorySnapshots);
        context.addVariable("processingInventorySnapshots", processingInventorySnapshots);
        String asyncInstructionName = null;
        if (task != null) {
            asyncInstructionName = task.getInstructionName();
            for (AsyncScriptInstructionParameter param : task.getInstructionParameters()) {
                ScriptExecutionContext.current().addVariable(param.getName(), param.getValue());
            }
        }
        try {
            context.setTraceLoggingEnabled(UserContext.current().isTraceLoggingEnabled());
            LOG.info("Running script: {} with async instruction: {}", updateInventoryScript.getScriptName(), asyncInstructionName);
            updateInventoryScript.execute(asyncInstructionName);
            LOG.info("Done executing script: {}", updateInventoryScript.getScriptName());
            if (!context.getScheduleScriptContext().isEmpty()) {
                asyncScriptService.scheduleScriptTask(updateInventoryScript.getScriptName(), channel, context, ChannelInventorySyncServiceImpl.class,
                        UpdateInventoryTask.class.getName());
                LOG.info("A task has been scheduled to get the status of updated inventory from " + channel.getCode());
                saveAsyncChannelItemTypesInventorySyncStatus(channel, inventorySyncStatusVO, channelItemTypes, successfulInventorySnapshots, failedInventorySnapshots,
                        processingInventorySnapshots, false);
            } else {
                saveChannelItemTypeStatus(channel, source, channelItemTypes, inventorySyncStatusVO, status, null, null, channelProductIdToInventoryUpdate, failedInventorySnapshots,
                        successfulInventorySnapshots);
            }
        } catch (Exception e) {
            status = ChannelInventoryUpdateVO.Status.FAILED;
            LOG.error("Error while sending inventory notification for channel {}, message: {}", source.getName(), e.getMessage());
            if (SERVER_DOWN_MESSAGE.equals(e.getMessage()) || (e.getCause() instanceof HttpTransportException)) {
                throw new RuntimeException("Server down for channel: " + source.getName() + ", will retry in next run.");
            } else if (e instanceof AsyncScriptExecutionException) {
                LOG.info("Inventory Sync failed after maximum retries for some skus");
                saveAsyncChannelItemTypesInventorySyncStatus(channel, inventorySyncStatusVO, channelItemTypes, successfulInventorySnapshots, failedInventorySnapshots,
                        processingInventorySnapshots, true);
            } else if (e instanceof ScriptExecutionException) {
                failureCode = ((ScriptExecutionException) e).getReasonCode();
                failureReason = e.getMessage();
                saveChannelItemTypeStatus(channel, source, channelItemTypes, inventorySyncStatusVO, status, failureReason, failureCode, channelProductIdToInventoryUpdate,
                        failedInventorySnapshots, successfulInventorySnapshots);
            } else {
                failureReason = e.getMessage();
                failureCode = failureReason;
                saveChannelItemTypeStatus(channel, source, channelItemTypes, inventorySyncStatusVO, status, failureReason, failureCode, channelProductIdToInventoryUpdate,
                        failedInventorySnapshots, successfulInventorySnapshots);
            }
        } finally {
            ScriptExecutionContext.destroy();
        }
    }

    private void saveChannelItemTypeStatus(Channel channel, Source source, List<ChannelItemType> channelItemTypes, ChannelInventorySyncStatusDTO inventorySyncStatusVO,
            ChannelInventoryUpdateVO.Status status, String failureReason, String failureCode, Map<String, ChannelInventoryUpdateDTO> channelProductIdToInventoryUpdate,
            Map<String, ChannelScriptError> failedInventorySnapshots, Map<String, String> successfulInventorySnapshots) {
        inventorySyncStatusVO.setMileStone("Updating inventory on channel " + source.getName(), channelItemTypes.size());
        for (ChannelItemType cit : channelItemTypes) {
            ChannelInventoryUpdateVO.Status channelItemTypeStatus = null;
            String inventoryUpdateMessage = failureReason;
            cit.setDirty(false);
            cit.setPossiblyDirty(false);
            if (ChannelInventoryUpdateVO.Status.FAILED.equals(status) || failedInventorySnapshots.containsKey(cit.getChannelProductId())) {
                inventorySyncStatusVO.incrementFailedSyncCount(1);
                channelItemTypeStatus = ChannelInventoryUpdateVO.Status.FAILED;
                if (failedInventorySnapshots.containsKey(cit.getChannelProductId())) {
                    failureCode = failedInventorySnapshots.get(cit.getChannelProductId()).getCode();
                    inventoryUpdateMessage = failedInventorySnapshots.get(cit.getChannelProductId()).getMessage();
                }
                if (FailureReason.SKIPPED.name().equals(failureCode)) {
                    LOG.warn("Skipping channelProductId: {}", cit.getChannelProductId());
                    channelCatalogService.updateChannelItemTypeInventoryStatusIfFresh(cit);
                    continue;
                } else if (FailureReason.ENDED.name().equals(failureCode) || FailureReason.NOT_FOUND.name().equals(failureCode)) {
                    cit.setFailedCount(0);
                    cit.setListingStatus(ListingStatus.DELISTED);
                } else if (cit.getFailedCount() > MAX_RETRY_COUNT && source.isInventorySyncDisablingAfterMaxRetryOn()) {
                    cit.setDisabledDueToErrors(true);
                } else if (!ChannelScriptError.ScriptErrorCodes.CHANNEL_ERROR.name().equals(failureCode)) {
                    cit.setFailedCount(cit.getFailedCount() + 1);
                }
            } else {
                inventoryUpdateMessage = successfulInventorySnapshots.get(cit.getChannelProductId());
                inventorySyncStatusVO.incrementSuccessFulSyncCount(1);
                channelItemTypeStatus = ChannelInventoryUpdateVO.Status.SUCCESS;
                cit.setFailedCount(0);
            }
            cit.setLastInventoryUpdateStatus(channelItemTypeStatus.name());
            cit.setLastInventoryUpdate(cit.getNextInventoryUpdate());
            cit.setLastInventoryUpdateAt(DateUtils.getCurrentTime());
            cit.setLastMessage(inventoryUpdateMessage);
            channelCatalogService.updateChannelItemTypeInventoryStatusIfFresh(cit);
            ChannelInventoryUpdateVO cits = prepareChannelInventoryUpdateVO(channel.getCode(),
                    channelProductIdToInventoryUpdate.get(cit.getChannelProductId()).getItemType().getSkuCode(), cit);
            channelInventoryMao.create(cits);
        }
    }

    @Transactional
    private void saveAsyncChannelItemTypesInventorySyncStatus(Channel channel, ChannelInventorySyncStatusDTO inventorySyncStatusVO, List<ChannelItemType> channelItemTypes,
            Map<String, String> successfulInventorySnapshots, Map<String, ChannelScriptError> failedInventorySnapshots, Map<String, String> processingInventorySnapshots,
            boolean lastTry) {
        Map<String, ChannelInventoryUpdateDTO> channelProductIdToInventoryUpdate = prepareChannelInventoryUpdateDTOS(channelItemTypes);
        for (ChannelItemType cit : channelItemTypes) {
            ChannelInventoryUpdateVO.Status channelItemTypeStatus;
            String inventoryUpdateMessage;
            cit.setDirty(false);
            cit.setPossiblyDirty(false);
            if (successfulInventorySnapshots.containsKey(cit.getChannelProductId())) {
                inventoryUpdateMessage = successfulInventorySnapshots.get(cit.getChannelProductId());
                inventorySyncStatusVO.incrementSuccessFulSyncCount(1);
                channelItemTypeStatus = ChannelInventoryUpdateVO.Status.SUCCESS;
                cit.setFailedCount(0);
            } else if (failedInventorySnapshots.containsKey(cit.getChannelProductId())) {
                inventorySyncStatusVO.incrementFailedSyncCount(1);
                cit.setFailedCount(cit.getFailedCount() + 1);
                channelItemTypeStatus = ChannelInventoryUpdateVO.Status.FAILED;
                inventoryUpdateMessage = failedInventorySnapshots.get(cit.getChannelProductId()).getMessage();
                cit.setDirty(true);
            } else if (lastTry && processingInventorySnapshots.containsKey(cit.getChannelProductId())) {
                // if in still in processing after last try then mark as failed
                inventorySyncStatusVO.incrementFailedSyncCount(1);
                cit.setFailedCount(cit.getFailedCount() + 1);
                channelItemTypeStatus = ChannelInventoryUpdateVO.Status.FAILED;
                inventoryUpdateMessage = "Failed after maximum retries";
                cit.setDirty(true);
            } else if (!lastTry && processingInventorySnapshots.containsKey(cit.getChannelProductId())) {
                // if not last try then no need to do anything
                cit.setLastMessage("Request Queued");
                channelCatalogService.updateChannelItemTypeInventoryStatusIfFresh(cit);
                continue;
            } else {
                // if nowhere then the response for this supc was not returned from snapdeal
                inventorySyncStatusVO.incrementFailedSyncCount(1);
                cit.setFailedCount(cit.getFailedCount() + 1);
                channelItemTypeStatus = ChannelInventoryUpdateVO.Status.FAILED;
                inventoryUpdateMessage = "Channel Item Type not created on channel : " + cit.getItemType().getSkuCode();
                LOG.info("Channel Item Type not created on channel : " + cit.getItemType().getSkuCode());
                cit.setDirty(true);
            }
            Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(cit.getChannel().getSourceCode());
            if (ChannelInventoryUpdateVO.Status.FAILED.equals(channelItemTypeStatus) && cit.getFailedCount() > MAX_RETRY_COUNT
                    && source.isInventorySyncDisablingAfterMaxRetryOn()) {
                cit.setDisabledDueToErrors(true);
            }
            cit.setLastInventoryUpdateStatus(channelItemTypeStatus.name());
            cit.setLastInventoryUpdate(cit.getNextInventoryUpdate());
            cit.setLastInventoryUpdateAt(DateUtils.getCurrentTime());
            cit.setLastMessage(inventoryUpdateMessage);
            channelCatalogService.updateChannelItemTypeInventoryStatusIfFresh(cit);
            ChannelInventoryUpdateVO cits = prepareChannelInventoryUpdateVO(channel.getCode(),
                    channelProductIdToInventoryUpdate.get(cit.getChannelProductId()).getItemType().getSkuCode(), cit);
            channelInventoryMao.create(cits);
        }
    }

    @Override
    @Transactional
    public void resume(AsyncScriptInstruction task) {
        String channelCode = null;
        List<String> channelProductIdList = new ArrayList<>();
        for (AsyncScriptInstructionParameter param : task.getInstructionParameters()) {
            if (param.getName().equals("channel")) {
                channelCode = (String) param.getValue();
            }
            if (param.getName().equals("channelProductIdList")) {
                channelProductIdList.addAll((List<String>) param.getValue());
            }
        }
        List<ChannelItemType> channelItemTypes = new ArrayList<>();
        Channel channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelByCode(channelCode);
        for (String channelProductId : channelProductIdList) {
            ChannelItemType channelItemType = channelCatalogService.getChannelItemTypeByChannelAndChannelProductId(channel.getCode(), channelProductId);
            channelItemTypes.add(channelItemType);
        }
        final Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(channel.getSourceCode());
        final ChannelGoodToSyncResponse channelGoodToSyncResponse = channelService.isChannelGoodToSync(channel.getCode(), ChannelServiceImpl.Sync.INVENTORY);
        if (!channelGoodToSyncResponse.isGoodToSync()) {
            LOG.warn("Connector broken for channel {}", channel.getName());
        } else {
            ChannelInventorySyncStatusDTO channelInventorySyncStatusDTO = channelService.getChannelInventorySyncStatus(channel.getCode());
            channelInventorySyncStatusDTO.reset();
            channelInventorySyncStatusDTO.setSyncExecutionStatus(Channel.SyncExecutionStatus.RUNNING);
            channelInventorySyncStatusDTO.setMessage("Async Inventory Sync started");
            ScraperScript updateInventoryScript = CacheManager.getInstance().getCache(ChannelCache.class).getScriptByName(channel.getCode(),
                    com.uniware.core.entity.Source.INVENTORY_UPDATE_SCRIPT_NAME);
            syncInventoryOnChannel(channel, channelGoodToSyncResponse.getParams(), source, channelItemTypes, updateInventoryScript, channelInventorySyncStatusDTO, task);
        }
    }

    private ChannelInventoryUpdateVO prepareChannelInventoryUpdateVO(String channelCode, String skuCode, ChannelItemType cit) {
        ChannelInventoryUpdateVO channelInventoryUpdate = new ChannelInventoryUpdateVO();
        channelInventoryUpdate.setChannelCode(channelCode);
        channelInventoryUpdate.setChannelProductId(cit.getChannelProductId());
        channelInventoryUpdate.setSkuCode(skuCode);
        channelInventoryUpdate.setSellerSkuCode(cit.getSellerSkuCode());
        channelInventoryUpdate.setProductName(cit.getProductName());
        channelInventoryUpdate.setCalculatedInventory(cit.getLastInventoryUpdate());
        channelInventoryUpdate.setStatusCode(cit.getLastInventoryUpdateStatus());
        channelInventoryUpdate.setMessage(cit.getLastMessage());
        channelInventoryUpdate.setSnapshotReferenceId(cit.getSnapshotReferenceId());
        channelInventoryUpdate.setCreated(DateUtils.getCurrentTime());
        return channelInventoryUpdate;
    }

    private Map<String, ChannelInventoryUpdateDTO> prepareChannelInventoryUpdateDTOS(List<ChannelItemType> channelItemTypes) {
        Map<String, ChannelInventoryUpdateDTO> channelProductIdToChannelInventoryUpdate = new HashMap<>();
        for (ChannelItemType channelItemType : channelItemTypes) {
            Channel channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelById(channelItemType.getChannel().getId());
            ItemType itemType;
            if (ConfigurationManager.getInstance().getConfiguration(ProductConfiguration.class).isProductManagementSwitchedOff()) {
                itemType = catalogService.getItemTypeBySkuCode(ItemType.UNKNOWN_SKU);
            } else {
                itemType = catalogService.getAllItemTypeById(channelItemType.getItemType().getId());
            }
            ChannelInventoryUpdateDTO channelInventoryUpdateDTO = new ChannelInventoryUpdateDTO(channelItemType, channel, itemType);
            channelInventoryUpdateDTO.setChannelItemTypeVO(
                    channelCatalogService.getChannelItemTypeVOByChannelAndChannelProductId(channel.getCode(), channelItemType.getChannelProductId()));
            channelInventoryUpdateDTO.setSnapshot(channelInventoryMao.getInventoryUpdateSnapshotById(channelItemType.getSnapshotReferenceId()));
            channelProductIdToChannelInventoryUpdate.put(channelItemType.getChannelProductId(), channelInventoryUpdateDTO);
        }
        return channelProductIdToChannelInventoryUpdate;
    }

    private void sendSyncFailedNotification(ChannelInventorySyncStatusDTO inventorySyncStatusVO, Channel channel) {
        try {
            EmailMessage message = new EmailMessage(EmailTemplateType.CHANNEL_INVENTORY_SYNC_FAILED_NOTIFICATION.name());
            message.addTemplateParam("channelStatus", inventorySyncStatusVO);
            message.addTemplateParam("channel", channel);
            emailService.send(message);
        } catch (Throwable e) {
            LOG.error("Ignoring email send failure. channel:" + channel.getCode(), e);
        }
    }

    @Override
    public GetChannelInventoryUpdateSnapshotResponse getInventoryUpdateSnapshotById(GetChannelInventoryUpdateSnapshotRequest request) {
        GetChannelInventoryUpdateSnapshotResponse response = new GetChannelInventoryUpdateSnapshotResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            ChannelInventoryUpdateSnapshotVO snapshot = channelInventoryMao.getInventoryUpdateSnapshotById(request.getSnapshotId());
            GetChannelInventoryUpdateSnapshotResponse.ChannelInventoryUpdateSnapshotDTO snapshotDTO = new GetChannelInventoryUpdateSnapshotResponse.ChannelInventoryUpdateSnapshotDTO(
                    snapshot);
            if (StringUtils.isNotBlank(snapshot.getFormulaCode())) {
                snapshotDTO.setFormula(channelInventoryMao.getChannelInventoryFormulaByCode(snapshot.getFormulaCode()).getFormula());
            }
            response.setInventoryUpdate(snapshotDTO);
            response.setSuccessful(true);
        } else {
            response.setSuccessful(false);
            response.setErrors(context.getErrors());
        }
        return response;
    }

    @Override
    public void resetChannelInventorySyncStatusForAllTenants() {
        List<String> tenantCodes = new ArrayList<>();
        List<Tenant> tenants = CacheManager.getInstance().getCache(TenantCache.class).getActiveTenants();
        for (Tenant tenant : tenants) {
            tenantCodes.add(tenant.getCode());
        }
        channelInventoryMao.resetChannelInventorySyncStatus(tenantCodes);
    }

    @Override
    public void resetChannelInventorySyncStatus() {
        channelInventoryMao.resetChannelInventorySyncStatus();
    }

    @Override
    public SyncChannelItemTypeInventoryResponse syncChannelItemTypeInventory(SyncChannelItemTypeInventoryRequest request) {
        SyncChannelItemTypeInventoryResponse response = new SyncChannelItemTypeInventoryResponse();
        ValidationContext context = request.validate();
        Channel channel = null;
        if (!context.hasErrors()) {
            channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelByCode(request.getChannelCode());
            if (channel == null) {
                context.addError(WsResponseCode.INVALID_CHANNEL_CODE);
            } else if (!channel.isEnabled()) {
                context.addError(WsResponseCode.BAD_CHANNEL_STATE, "Channel is disabled");
            }
        }
        if (!context.hasErrors()) {
            try {
                Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(channel.getSourceCode());
                ChannelItemType channelItemType = channelCatalogService.getChannelItemTypeByChannelAndChannelProductId(request.getChannelCode(), request.getChannelProductId());
                if (channelItemType == null || !SyncStatus.ON.equals(channel.getInventorySyncStatus())) {
                    context.addError(WsResponseCode.INVALID_CHANNEL_ITEM_TYPE);
                    response.setMessage("Inventory sync is not configured");
                } else if (channelItemType.isDisabled()) {
                    context.addError(WsResponseCode.INVALID_CHANNEL_ITEM_TYPE);
                    response.setMessage("Can't sync when channel item type is disabled.");
                } else {
                    boolean productMgmtSwitchedOff = ConfigurationManager.getInstance().getConfiguration(ProductConfiguration.class).isProductManagementSwitchedOff();
                    if (!productMgmtSwitchedOff) {
                        channelItemType = markChannelItemTypeDirty(channelItemType);
                    }
                    if (productMgmtSwitchedOff || (channelItemType.isDirty() || channelItemType.isPossiblyDirty())) {
                        channelItemType = channelCatalogService.getChannelItemTypeByChannelAndChannelProductId(request.getChannelCode(), request.getChannelProductId());
                        List<ChannelItemType> channelItemTypes = new ArrayList<>(1);
                        channelItemTypes.add(channelItemType);
                        ScraperScript updateInventoryScript = CacheManager.getInstance().getCache(ChannelCache.class).getScriptByName(channel.getCode(),
                                com.uniware.core.entity.Source.INVENTORY_UPDATE_SCRIPT_NAME);
                        if (updateInventoryScript != null) {
                            ChannelGoodToSyncResponse channelGoodToSyncResponse = channelService.isChannelGoodToSync(channel.getCode(), ChannelServiceImpl.Sync.INVENTORY);
                            if (channelGoodToSyncResponse.isGoodToSync()) {
                                syncInventoryOnChannel(channel, channelGoodToSyncResponse.getParams(), source, channelItemTypes, updateInventoryScript,
                                        new ChannelInventorySyncStatusDTO(channel.getCode()), null);
                                channelItemType = channelCatalogService.getChannelItemTypeByChannelAndChannelProductId(request.getChannelCode(), request.getChannelProductId());
                                response.setLastInventorySyncStatus(channelItemType.getLastInventoryUpdateStatus());
                                response.setLastInventoryUpdate(channelItemType.getLastInventoryUpdate());
                                response.setMessage(channelItemType.getLastMessage());
                                response.setSuccessful(channelItemType.getLastInventoryUpdateStatus() != null
                                        && channelItemType.getLastInventoryUpdateStatus().equals(ChannelInventoryUpdateVO.Status.SUCCESS.name()));
                            }
                        }
                    } else {
                        context.addError(WsResponseCode.INVALID_CHANNEL_ITEM_TYPE, "Channel Item type not marked dirty.");
                        response.setMessage("Channel Item type not marked dirty.");
                    }
                }
            } catch (Exception e) {
                LOG.error("Error syncing channelProductId: " + request.getChannelProductId(), e);
                context.addError(WsResponseCode.INVALID_CHANNEL_ITEM_TYPE, e.getMessage());
                response.setMessage(e.getMessage());
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    @Override
    @Transactional
    public Integer getAvailableBundleInventory(String skuCode) {
        Bundle bundle = bundleService.getBundleByCode(skuCode);
        int bundleInventory = Integer.MAX_VALUE;
        if (bundle != null) {
            for (BundleItemType bundleItemType : bundle.getBundleItemTypes()) {
                ItemTypeInventorySnapshot inventorySnapshot = inventoryService.getItemTypeInventorySnapshot(bundleItemType.getItemType().getId());
                if (inventorySnapshot == null) {
                    bundleInventory = 0;
                    break;
                }
                int componentInventory = inventorySnapshot.getInventory() / bundleItemType.getQuantity();
                if (componentInventory < bundleInventory) {
                    bundleInventory = componentInventory;
                }
            }
            return bundleInventory;
        }
        return 0;
    }

    @Override
    @Transactional
    public UpdateInventoryByChannelProductIdResponse updateInventoryByChannelProductId(UpdateInventoryByChannelProductIdRequest request) {
        UpdateInventoryByChannelProductIdResponse response = new UpdateInventoryByChannelProductIdResponse();
        ValidationContext context = request.validate();
        ChannelItemType cit = null;
        if (!context.hasErrors()) {
            if (!ConfigurationManager.getInstance().getConfiguration(ProductConfiguration.class).isProductManagementSwitchedOff()) {
                context.addError(WsResponseCode.INVENTORY_UPDATE_NOT_ALLOWED, "Inventory Update not allowed for this configuration");
            } else {
                cit = channelCatalogService.getChannelItemTypeByChannelAndChannelProductId(request.getChannelCode(), request.getChannelProductId(), true);
                if (cit == null || cit.isDisabled()) {
                    context.addError(WsResponseCode.INVALID_REQUEST, "Channel item is disable or does not exist");
                }
            }
            if (!context.hasErrors()) {
                cit.setNextInventoryUpdate(request.getInventory());
                cit.setDirty(true);
                cit.setDisabledDueToErrors(false);
                channelCatalogService.updateChannelItemType(cit);
            }
        }
        response.addErrors(context.getErrors());
        response.addWarnings(context.getWarnings());
        response.setSuccessful(!context.hasErrors());
        return response;
    }

    @Override
    public void create(ChannelInventoryFormulaVO channelInventoryFormula) {
        channelInventoryMao.create(channelInventoryFormula);
    }

    @Override
    public ChannelInventoryFormulaVO getChannelInventoryFormulaByChecksum(String checksum) {
        return channelInventoryMao.getChannelInventoryFormulaByChecksum(checksum);
    }

    @Override
    public ChannelInventoryFormulaVO getChannelInventoryFormulaByCode(String code) {
        return channelInventoryMao.getChannelInventoryFormulaByCode(code);
    }

    @Override
    @Transactional
    public InventorySnapshotDTO getItemTypeInventoryNotification(int itemTypeId, List<Integer> facilityIds) {
        return inventoryDao.getItemTypeInventoryNotification(itemTypeId, facilityIds);
    }
}
