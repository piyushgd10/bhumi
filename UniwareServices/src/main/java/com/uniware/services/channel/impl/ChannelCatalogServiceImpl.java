/**
 *
 */
package com.uniware.services.channel.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.uniware.core.api.channel.ReenableCITInventorySyncStatusRequest;
import com.uniware.core.api.channel.ReenableCITInventorySyncStatusResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.DateUtils.DateRange;
import com.unifier.core.utils.StringUtils;
import com.unifier.core.utils.ValidatorUtils;
import com.unifier.services.aspect.MarkDirty;
import com.unifier.services.aspect.RollbackOnFailure;
import com.uniware.core.api.catalog.CreateItemTypeRequest;
import com.uniware.core.api.catalog.CreateItemTypeResponse;
import com.uniware.core.api.catalog.WsItemType;
import com.uniware.core.api.channel.ChannelCatalogSyncStatusDTO;
import com.uniware.core.api.channel.ChannelItemTypeDTO;
import com.uniware.core.api.channel.ChannelItemTypeLookupDTO;
import com.uniware.core.api.channel.CreateChannelItemTypeRequest;
import com.uniware.core.api.channel.CreateChannelItemTypeResponse;
import com.uniware.core.api.channel.CreateItemTypeFromChannelItemTypeRequest;
import com.uniware.core.api.channel.CreateItemTypeFromChannelItemTypeResponse;
import com.uniware.core.api.channel.DeleteChannelItemTypeRequest;
import com.uniware.core.api.channel.DeleteChannelItemTypeResponse;
import com.uniware.core.api.channel.DisableChannelItemTypeRequest;
import com.uniware.core.api.channel.DisableChannelItemTypeResponse;
import com.uniware.core.api.channel.GetChannelItemTypeDetailsRequest;
import com.uniware.core.api.channel.GetChannelItemTypeDetailsResponse;
import com.uniware.core.api.channel.IgnoreChannelItemTypeRequest;
import com.uniware.core.api.channel.IgnoreChannelItemTypeResponse;
import com.uniware.core.api.channel.LinkChannelItemTypeRequest;
import com.uniware.core.api.channel.LinkChannelItemTypeResponse;
import com.uniware.core.api.channel.ReenableInventorySyncRequest;
import com.uniware.core.api.channel.ReenableInventorySyncRequest.WsReenableChannelItemType;
import com.uniware.core.api.channel.ReenableInventorySyncResponse;
import com.uniware.core.api.channel.RelistChannelItemTypeRequest;
import com.uniware.core.api.channel.RelistChannelItemTypeResponse;
import com.uniware.core.api.channel.UnlinkChannelItemTypeRequest;
import com.uniware.core.api.channel.UnlinkChannelItemTypeResponse;
import com.uniware.core.api.channel.UpdateChannelItemTypeTaxRequest;
import com.uniware.core.api.channel.UpdateChannelItemTypeTaxRequest.WsChannelItemTypeTax;
import com.uniware.core.api.channel.UpdateChannelItemTypeTaxResponse;
import com.uniware.core.api.channel.WsChannelItemType;
import com.uniware.core.api.channel.WsUnlinkedChannelItemType;
import com.uniware.core.api.model.WsSaleOrderItem;
import com.uniware.core.api.prices.EditChannelItemTypePriceResponse;
import com.uniware.core.api.prices.MergeChannelItemTypePriceRequest;
import com.uniware.core.api.prices.WsChannelItemTypePrice;
import com.uniware.core.api.saleorder.CreateSaleOrderRequest;
import com.uniware.core.api.saleorder.RemoveMappingErrorRetrySaleOrderRequest;
import com.uniware.core.api.saleorder.RemoveMappingErrorRetrySaleOrderResponse;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.cache.EnvironmentPropertiesCache;
import com.uniware.core.cache.FacilityCache;
import com.uniware.core.entity.Channel;
import com.uniware.core.entity.ChannelItemType;
import com.uniware.core.entity.ChannelItemType.ListingStatus;
import com.uniware.core.entity.ChannelItemType.Status;
import com.uniware.core.entity.Facility;
import com.uniware.core.entity.ItemType;
import com.uniware.core.vo.ChannelItemTypeTaxVO;
import com.uniware.core.vo.ChannelItemTypeVO;
import com.uniware.dao.channel.IChannelCatalogDao;
import com.uniware.dao.channel.IChannelCatalogMao;
import com.uniware.dao.warehouse.IFacilityDao;
import com.uniware.services.cache.ChannelCache;
import com.uniware.services.catalog.ICatalogService;
import com.uniware.services.channel.IChannelCatalogService;
import com.uniware.services.channel.IChannelInventorySyncService;
import com.uniware.services.channel.IChannelService;
import com.uniware.services.configuration.data.manager.ProductConfiguration;
import com.uniware.services.inventory.IInventoryService;
import com.uniware.services.pricing.IPricingService;
import com.uniware.services.saleorder.ISaleOrderService;

/**
 * @author Sunny
 */
@Service("channelCatalogService")
public class ChannelCatalogServiceImpl implements IChannelCatalogService {

    @Autowired
    private IChannelCatalogMao           channelCatalogMao;

    @Autowired
    private IChannelCatalogDao           channelCatalogDao;

    @Autowired
    private ICatalogService              catalogService;

    @Autowired
    private IChannelService              channelService;

    @Autowired
    private IChannelInventorySyncService channelInventorySyncService;

    @Autowired
    private IInventoryService            inventoryService;

    @Autowired
    private ISaleOrderService            saleOrderService;

    @Autowired
    private IFacilityDao                 facilityDao;

    @Autowired
    @Qualifier("uniwarePricingService")
    private IPricingService              uniwarePricingService;

    private static final Logger          LOG = LoggerFactory.getLogger(ChannelCatalogServiceImpl.class);

    @Override
    @Transactional
    public LinkChannelItemTypeResponse linkChannelItemType(LinkChannelItemTypeRequest request) {
        LinkChannelItemTypeResponse response = new LinkChannelItemTypeResponse();
        ValidationContext context = request.validate();
        Channel channel;
        ItemType itemType = null;
        if (!context.hasErrors()) {
            channel = channelService.getChannelByCode(request.getChannelCode());
            if (channel == null) {
                context.addError(WsResponseCode.INVALID_CHANNEL_CODE, WsResponseCode.INVALID_CHANNEL_CODE.message() + ": " + request.getChannelCode());
            } else {
                itemType = catalogService.getAllItemTypeBySkuCode(request.getSkuCode());
                if (itemType == null) {
                    context.addError(WsResponseCode.INVALID_ITEM_TYPE, WsResponseCode.INVALID_ITEM_TYPE.message() + ": " + request.getSkuCode());
                }
            }
        }
        if (!context.hasErrors()) {
            ChannelItemType channelItemType = getChannelItemTypeByChannelAndChannelProductId(request.getChannelCode(), request.getChannelProductId());
            if (channelItemType == null || !Status.UNLINKED.equals(channelItemType.getStatusCode())) {
                context.addError(WsResponseCode.INVALID_CHANNEL_ITEM_TYPE);
            } else {
                // See how many item types have been mapped to this item type
                List<ChannelItemType> channelItemTypes = getChannelItemTypesByItemTypeId(itemType.getId());
                if (channelItemTypes.size() >= CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class).getMaxChannelItemTypeMappingAllowed()) {
                    context.addError(WsResponseCode.CHANNEL_LIMIT_EXCEDED, "Max listings mapping limit reached to a same item type");
                } else {
                    channelItemType.setItemType(itemType);
                    channelItemType.setStatusCode(Status.LINKED);
                    channelItemType = updateChannelItemType(channelItemType);
                    inventoryService.markInventorySnapshotDirty(itemType.getId());
                    channelInventorySyncService.markChannelItemTypeDirty(channelItemType);
                    response.setSuccessful(true);
                    RemoveMappingErrorRetrySaleOrderRequest req = new RemoveMappingErrorRetrySaleOrderRequest();
                    req.setChannelProductId(request.getChannelProductId());
                    RemoveMappingErrorRetrySaleOrderResponse resp = saleOrderService.removeMappingErrorFromSaleOrderVOs(req);
                    if (resp.isSuccessful()) {
                        response.setSuccessfulOrdersCount(resp.getSuccessfulOrderCount());
                        response.setUpdatedOrdersCount(resp.getNumOfOrdersUpdated());
                    }
                }
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    @Override
    @Transactional
    public IgnoreChannelItemTypeResponse ignoreChannelItemType(IgnoreChannelItemTypeRequest request) {
        IgnoreChannelItemTypeResponse response = new IgnoreChannelItemTypeResponse();
        ValidationContext context = request.validate();
        Channel channel = null;
        if (!context.hasErrors()) {
            channel = channelService.getChannelByCode(request.getChannelCode());
            if (channel == null) {
                context.addError(WsResponseCode.INVALID_CHANNEL_CODE, WsResponseCode.INVALID_CHANNEL_CODE.message() + ": " + request.getChannelCode());
            } else {
                ChannelItemType channelItemType = getChannelItemTypeByChannelAndChannelProductId(request.getChannelCode(), request.getChannelProductId());
                if (channelItemType == null) {
                    context.addError(WsResponseCode.INVALID_CHANNEL_ITEM_TYPE);
                } else {
                    channelItemType.setItemType(null);
                    channelItemType.setStatusCode(Status.IGNORED);
                    updateChannelItemType(channelItemType);
                    response.setSuccessful(true);
                }
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    @Override
    @Transactional
    public UnlinkChannelItemTypeResponse unlinkChannelItemType(UnlinkChannelItemTypeRequest request) {
        UnlinkChannelItemTypeResponse response = new UnlinkChannelItemTypeResponse();
        ValidationContext context = request.validate();
        Channel channel = null;
        if (!context.hasErrors()) {
            channel = channelService.getChannelByCode(request.getChannelCode());
            if (channel == null) {
                context.addError(WsResponseCode.INVALID_CHANNEL_CODE, WsResponseCode.INVALID_CHANNEL_CODE.message() + ": " + request.getChannelCode());
            } else {
                ChannelItemType channelItemType = getChannelItemTypeByChannelAndChannelProductId(request.getChannelCode(), request.getChannelProductId());
                if (channelItemType == null || !Status.LINKED.equals(channelItemType.getStatusCode())) {
                    context.addError(WsResponseCode.INVALID_CHANNEL_ITEM_TYPE);
                } else {
                    channelItemType.setItemType(null);
                    channelItemType.setStatusCode(Status.UNLINKED);
                    channelItemType = updateChannelItemType(channelItemType);
                    response.setSuccessful(true);
                }
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    @Override
    @Transactional
    public DisableChannelItemTypeResponse disableChannelItemType(DisableChannelItemTypeRequest request) {
        DisableChannelItemTypeResponse response = new DisableChannelItemTypeResponse();
        ValidationContext context = request.validate();
        Channel channel = null;
        if (!context.hasErrors()) {
            channel = channelService.getChannelByCode(request.getChannelCode());
            if (channel == null) {
                context.addError(WsResponseCode.INVALID_CHANNEL_CODE, WsResponseCode.INVALID_CHANNEL_CODE.message() + ": " + request.getChannelCode());
            } else {
                ChannelItemType channelItemType = getChannelItemTypeByChannelAndChannelProductId(request.getChannelCode(), request.getChannelProductId());
                if (channelItemType == null) {
                    context.addError(WsResponseCode.INVALID_CHANNEL_ITEM_TYPE);
                } else {
                    channelItemType.setDisabled(true);
                    channelItemType.setPossiblyDirty(false);
                    channelItemType.setDirty(false);
                    channelCatalogDao.update(channelItemType);
                    response.setSuccessful(true);
                }
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    @Override
    @Transactional
    public RelistChannelItemTypeResponse relistChannelItemType(RelistChannelItemTypeRequest request) {
        RelistChannelItemTypeResponse response = new RelistChannelItemTypeResponse();
        ValidationContext context = request.validate();
        Channel channel = null;
        if (!context.hasErrors()) {
            channel = channelService.getChannelByCode(request.getChannelCode());
            if (channel == null) {
                context.addError(WsResponseCode.INVALID_CHANNEL_CODE, WsResponseCode.INVALID_CHANNEL_CODE.message() + ": " + request.getChannelCode());
            } else {
                ChannelItemType channelItemType = getChannelItemTypeByChannelAndChannelProductId(request.getChannelCode(), request.getChannelProductId());
                if (channelItemType == null) {
                    context.addError(WsResponseCode.INVALID_CHANNEL_ITEM_TYPE);
                } else {
                    if (ListingStatus.DELISTED.equals(channelItemType.getListingStatus())) {
                        channelItemType.setListingStatus(ListingStatus.ACTIVE);
                        channelItemType.setFailedCount(0);
                        channelItemType.setDisabledDueToErrors(false);
                        channelCatalogDao.update(channelItemType);
                        response.setSuccessful(true);
                    } else {
                        context.addError(WsResponseCode.CHANNEL_ITEM_NOT_DELISTED, WsResponseCode.CHANNEL_ITEM_NOT_DELISTED.message() + ": " + request.getChannelProductId());
                    }
                }
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    @Override
    @Transactional
    public DeleteChannelItemTypeResponse deleteChannelItemType(DeleteChannelItemTypeRequest request) {
        DeleteChannelItemTypeResponse response = new DeleteChannelItemTypeResponse();
        ValidationContext context = request.validate();
        Channel channel = null;
        if (!context.hasErrors()) {
            channel = channelService.getChannelByCode(request.getChannelCode());
            if (channel == null) {
                context.addError(WsResponseCode.INVALID_CHANNEL_CODE, WsResponseCode.INVALID_CHANNEL_CODE.message() + ": " + request.getChannelCode());
            } else {
                ChannelItemType channelItemType = getChannelItemTypeByChannelAndChannelProductId(request.getChannelCode(), request.getChannelProductId());
                if (channelItemType == null) {
                    context.addError(WsResponseCode.INVALID_CHANNEL_ITEM_TYPE);
                } else {
                    channelCatalogMao.deleteChannelItemType(request.getChannelCode(), request.getChannelProductId());
                    channelCatalogDao.deleteChannelItemType(channel.getId(), request.getChannelProductId());
                    ChannelCatalogSyncStatusDTO channelCatalogSyncStatus = channelService.getChannelCatalogSyncStatus(channel.getCode());
                    channelCatalogSyncStatus.setLastCountOnChannel(new Long(0));
                    channelCatalogSyncStatus.setMessage("Resetting lastCountOnChannel due to ChannelItemType deletion");
                    channelService.updateChannelCatalogSyncStatus(channelCatalogSyncStatus);
                    response.setSuccessful(true);
                }
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    @Override
    @Transactional
    public CreateChannelItemTypeResponse createChannelItemType(CreateChannelItemTypeRequest request) {
        LOG.info("Create ChannelItemType Request - {}", request.toString());
        CreateChannelItemTypeResponse response = new CreateChannelItemTypeResponse();
        ValidationContext context = request.validate();
        ItemType itemType = null;
        Channel channel = null;
        if (!context.hasErrors()) {
            channel = channelService.getChannelByCode(request.getChannelItemType().getChannelCode());
            if (channel == null) {
                context.addError(WsResponseCode.INVALID_CHANNEL_CODE, WsResponseCode.INVALID_CHANNEL_CODE.message() + ": " + request.getChannelItemType().getChannelCode());
            }
        }

        if (!context.hasErrors()) {
            if (StringUtils.isNotBlank(request.getChannelItemType().getSkuCode())) {
                itemType = catalogService.getAllEnabledItemTypeBySkuCode(request.getChannelItemType().getSkuCode());
                if (itemType == null) {
                    context.addError(WsResponseCode.INVALID_ITEM_TYPE, WsResponseCode.INVALID_ITEM_TYPE.message() + ": " + request.getChannelItemType().getSkuCode());
                }
            } else {
                itemType = catalogService.getAllEnabledItemTypeBySkuCode(request.getChannelItemType().getSellerSkuCode());
            }
        }

        boolean channelLimitExceded = false;
        ChannelItemType channelItemType = getChannelItemTypeByChannelAndChannelProductId(channel.getCode(), request.getChannelItemType().getChannelProductId(), true);
        if (itemType != null && (channelItemType == null || Status.UNLINKED.equals(channelItemType.getStatusCode()))) {
            // See how many item types have been mapped to this item type
            List<ChannelItemType> channelItemTypes = getChannelItemTypesByItemTypeId(itemType.getId());
            if (channelItemTypes.size() >= CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class).getMaxChannelItemTypeMappingAllowed()) {
                channelLimitExceded = true;
                if (channelItemType != null) {
                    context.addError(WsResponseCode.CHANNEL_LIMIT_EXCEDED, "At max 500 listings can be mapped to same item type");
                }
            }
        }
        if (!context.hasErrors()) {
            if (channelItemType == null) {
                channelItemType = new ChannelItemType();
                channelItemType.setChannel(channel);
                channelItemType.setSellerSkuCode(request.getChannelItemType().getSellerSkuCode());
                channelItemType.setChannelProductId(request.getChannelItemType().getChannelProductId());
                channelItemType.setProductName(request.getChannelItemType().getProductName());
                channelItemType.setVertical(request.getChannelItemType().getVertical());
                channelItemType.setCreated(DateUtils.getCurrentTime());
            }
            if (request.getChannelItemType().isVerified()) {
                channelItemType.setDisabledDueToErrors(false);
                channelItemType.setFailedCount(0);
                channelItemType.setLastCatalogSyncAt(DateUtils.getCurrentTime());
            }
            if (request.getChannelItemType().getBlockedInventory() != null) {
                channelItemType.setBlockedInventory(request.getChannelItemType().getBlockedInventory());
            }
            if (request.getChannelItemType().getDisabled() != null) {
                channelItemType.setDisabled(request.getChannelItemType().getDisabled());
            } else if (ListingStatus.INACTIVE.equals(channelItemType.getListingStatus())){ // Re-enable the catalogs disabled by system. Do not reenable the catalog manually disabled by seller
                channelItemType.setDisabled(false);
            }
            channelItemType.setListingStatus(request.getChannelItemType().isLive() ? ListingStatus.ACTIVE : ListingStatus.INACTIVE);

            if (request.getChannelItemType().getTransferPrice() != null) {
                channelItemType.setTransferPrice(request.getChannelItemType().getTransferPrice());
            }
            if (request.getChannelItemType().isVerified()) {
                channelItemType.setSellerSkuCode(request.getChannelItemType().getSellerSkuCode());
                channelItemType.setVertical(request.getChannelItemType().getVertical());
                channelItemType.setProductName(request.getChannelItemType().getProductName());
                channelItemType.setProductUrl(request.getChannelItemType().getProductUrl());
                channelItemType.setSize(request.getChannelItemType().getSize());
                channelItemType.setColor(request.getChannelItemType().getColor());
                channelItemType.setVerified(request.getChannelItemType().isVerified());
                channelItemType.setCommissionPercentage(request.getChannelItemType().getCommissionPercentage());
                channelItemType.setPaymentGatewayCharge(request.getChannelItemType().getPaymentGatewayCharge());
                channelItemType.setLogisticsCost(request.getChannelItemType().getLogisticsCost());
                // Create ChannelItemTypeVO
                ChannelItemTypeVO channelItemTypeVO = new ChannelItemTypeVO(request.getChannelItemType());
                channelItemTypeVO.setChannelProductId(channelItemType.getChannelProductId());
                channelCatalogMao.update(channelItemTypeVO);
            }
            if(channelItemType.getStatusCode() != null && Status.IGNORED.equals(channelItemType.getStatusCode())){
                channelItemType.setStatusCode(Status.IGNORED);
            }else if (itemType != null && !channelLimitExceded && (StringUtils.isNotBlank(request.getChannelItemType().getSkuCode()) || channelItemType.getItemType() == null)) {
                channelItemType.setItemType(itemType);
                channelItemType.setStatusCode(Status.LINKED);
            } else if (channelItemType.getItemType() == null && !Status.IGNORED.equals(channelItemType.getStatusCode())) {
                channelItemType.setStatusCode(Status.UNLINKED);
            }
            channelItemType.setCatalogSyncId(request.getSyncId());
            channelItemType = channelCatalogDao.update(channelItemType);

            // For LINKED channel item types, see if inventory needs to be synced on channel.
            if (channelItemType.getItemType() != null  && !Status.IGNORED.equals(channelItemType.getStatusCode())) {
                inventoryService.markInventorySnapshotDirty(channelItemType.getItemType().getId());
                channelInventorySyncService.markChannelItemTypeDirty(channelItemType);

                //
                // If channel item type is not marked dirty and we've got the current inventory on channel, check if it is equal to the last update sent.
                // If not, set it to dirty.
                //
                if (!channelItemType.isDirty() && request.getChannelItemType().getCurrentInventoryOnChannel() != null) {
                    if (channelItemType.getLastInventoryUpdate() == null
                            || !channelItemType.getLastInventoryUpdate().equals(request.getChannelItemType().getCurrentInventoryOnChannel())) {
                        channelItemType.setDirty(true);
                        channelCatalogDao.update(channelItemType);
                    }
                }
            } else if (ConfigurationManager.getInstance().getConfiguration(ProductConfiguration.class).isProductManagementSwitchedOff()) {
                channelItemType.setNextInventoryUpdate(request.getChannelItemType().getCurrentInventoryOnChannel());
                channelItemType.setDirty(true);
                channelCatalogDao.update(channelItemType);
            }

            // Update Prices
            String priceSyncStatus = CacheManager.getInstance().getCache(ChannelCache.class).getChannelByCode(channel.getCode()).getPricingSyncStatus().name();
            if (Channel.SyncStatus.ON.name().equals(priceSyncStatus) && request.getChannelItemTypePrice() != null) {
                EditChannelItemTypePriceResponse priceResponse = createChannelItemTypePrice(channel, request.getChannelItemTypePrice());
                if (priceResponse.hasErrors()) {
                    LOG.warn("Unable to pull prices for channel product id {}, error {}", request.getChannelItemType().getChannelProductId(), priceResponse.getErrors().toString());
                }
            }
        }

        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        } else {
            response.setSuccessful(true);
        }
        return response;
    }

    private EditChannelItemTypePriceResponse createChannelItemTypePrice(final Channel channel, final WsChannelItemTypePrice channelItemTypePrice) {
        EditChannelItemTypePriceResponse response = new EditChannelItemTypePriceResponse();
        try {
            MergeChannelItemTypePriceRequest priceRequest = new MergeChannelItemTypePriceRequest();
            priceRequest.setChannelCode(channelItemTypePrice.getChannelCode());
            priceRequest.setChannelProductId(channelItemTypePrice.getChannelProductId());
            priceRequest.setTransferPrice(channelItemTypePrice.getTransferPrice());
            priceRequest.setCompetitivePrice(channelItemTypePrice.getCompetitivePrice());
            priceRequest.setMrp(channelItemTypePrice.getMrp());
            priceRequest.setMsp(channelItemTypePrice.getMsp());
            priceRequest.setSellingPrice(channelItemTypePrice.getSellingPrice());
            priceRequest.setCurrencyCode(channelItemTypePrice.getCurrencyCode());
            // We don't want dirty prices to be overwritten
            priceRequest.setIsForceEdit(false);
            response = uniwarePricingService.editChannelItemTypePrice(priceRequest);
        } catch (Exception e) {
            LOG.error("Error while updating price on channel {} : {}", channel.getCode(), e.getMessage());
            response.setMessage(e.getMessage());
            response.setSuccessful(false);
        }
        return response;
    }

    @Override
    @Transactional
    public GetChannelItemTypeDetailsResponse getChannelItemTypeDetails(GetChannelItemTypeDetailsRequest request) {
        GetChannelItemTypeDetailsResponse response = new GetChannelItemTypeDetailsResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            Channel channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelByCode(request.getChannelCode());
            if (channel == null) {
                context.addError(WsResponseCode.INVALID_CHANNEL_CODE, WsResponseCode.INVALID_CHANNEL_CODE.message() + ": " + request.getChannelCode());
            } else {
                ChannelItemType channelItemType = getChannelItemTypeByChannelAndChannelProductId(channel.getId(), request.getChannelProductId());
                if (channelItemType == null && request.getSellerSku() != null) {
                    WsChannelItemType wsChannelItemType = new WsChannelItemType();
                    wsChannelItemType.setSellerSkuCode(request.getSellerSku());
                    wsChannelItemType.setChannelProductId(request.getChannelProductId());
                    wsChannelItemType.setChannelCode(request.getChannelCode());
                    CreateChannelItemTypeResponse createChannelItemTypeResponse = createChannelItemType(
                            new CreateChannelItemTypeRequest(wsChannelItemType, ChannelItemType.SyncedBy.SYSTEM.toString()));
                    if (createChannelItemTypeResponse.isSuccessful()) {
                        channelItemType = getChannelItemTypeByChannelAndChannelProductId(channel.getId(), request.getChannelProductId());
                    }
                }
                if (channelItemType == null || ChannelItemType.Status.LINKED.equals(channelItemType.getStatusCode())) {
                    context.addError(WsResponseCode.INVALID_CHANNEL_ITEM_TYPE);
                } else {
                    ChannelItemTypeVO channelItemTypeVO = getChannelItemTypeVOByChannelAndChannelProductId(channel.getCode(), channelItemType.getChannelProductId());
                    ChannelItemTypeDTO channelItemTypeDTO = new ChannelItemTypeDTO(channelItemType, channelItemTypeVO);
                    response.setChannelItemTypeDTO(channelItemTypeDTO);
                    response.setSuccessful(true);
                }
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    @Override
    @Transactional
    @RollbackOnFailure
    public CreateItemTypeFromChannelItemTypeResponse createItemTypeFromChannelItemType(CreateItemTypeFromChannelItemTypeRequest request) {
        CreateItemTypeFromChannelItemTypeResponse response = new CreateItemTypeFromChannelItemTypeResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            for (WsUnlinkedChannelItemType wsChannelItemType : request.getChannelItemTypes()) {
                Channel channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelByCode(wsChannelItemType.getChannelCode());
                if (channel == null) {
                    context.addError(WsResponseCode.INVALID_CHANNEL_CODE, WsResponseCode.INVALID_CHANNEL_CODE.message() + ": " + wsChannelItemType.getChannelCode());
                    continue;
                }
                ChannelItemType channelItemType = getChannelItemTypeByChannelAndChannelProductId(channel.getId(), wsChannelItemType.getChannelProductId());
                if (channelItemType == null || ChannelItemType.Status.LINKED.equals(channelItemType.getStatusCode())) {
                    context.addError(WsResponseCode.INVALID_CHANNEL_ITEM_TYPE, "Invalid channelProductId: " + wsChannelItemType.getChannelProductId());
                } else {
                    ItemType itemType = catalogService.getItemTypeBySkuCode(channelItemType.getSellerSkuCode());
                    if (itemType != null) {
                        context.addError(WsResponseCode.INVALID_CHANNEL_ITEM_TYPE, "Item type for seller sku " + channelItemType.getSellerSkuCode() + " already exists.");
                        continue;
                    }
                    String skuCode = channelItemType.getSellerSkuCode().replaceAll("\\s", "_");
                    ChannelItemTypeVO channelItemTypeVO = getChannelItemTypeVOByChannelAndChannelProductId(channel.getCode(), channelItemType.getChannelProductId());
                    WsItemType wsItemType = new WsItemType();
                    wsItemType.setCategoryCode(wsChannelItemType.getCategoryCode());
                    wsItemType.setSkuCode(skuCode);
                    wsItemType.setName(StringUtils.isNotBlank(channelItemType.getProductName()) ? channelItemType.getProductName() : channelItemType.getSellerSkuCode());
                    wsItemType.setProductPageUrl(channelItemType.getProductUrl());
                    wsItemType.setSize(channelItemType.getSize());
                    wsItemType.setColor(channelItemType.getColor());
                    if (channelItemTypeVO != null && channelItemTypeVO.getImageUrls() != null && !channelItemTypeVO.getImageUrls().isEmpty()) {
                        wsItemType.setImageUrl(channelItemTypeVO.getImageUrls().iterator().next());
                    }
                    CreateItemTypeRequest createItemTypeRequest = new CreateItemTypeRequest();
                    createItemTypeRequest.setItemType(wsItemType);
                    CreateItemTypeResponse createItemTypeResponse = catalogService.createItemType(createItemTypeRequest);
                    if (!createItemTypeResponse.isSuccessful()) {
                        context.addErrors(createItemTypeResponse.getErrors());
                    } else {
                        LinkChannelItemTypeRequest linkChannelItemTypeRequest = new LinkChannelItemTypeRequest();
                        linkChannelItemTypeRequest.setChannelCode(channel.getCode());
                        linkChannelItemTypeRequest.setSkuCode(skuCode);
                        linkChannelItemTypeRequest.setChannelProductId(wsChannelItemType.getChannelProductId());
                        linkChannelItemType(linkChannelItemTypeRequest);
                    }
                }
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        } else {
            response.setSuccessful(true);
        }
        return response;
    }

    @Override
    @Transactional
    public List<ChannelItemType> getChannelItemTypesByChannelAndItemTypeId(int channelId, int itemTypeId) {
        return channelCatalogDao.getChannelItemTypesByChannelAndItemTypeId(channelId, itemTypeId);
    }

    @Override
    @Transactional(readOnly = true)
    public List<ChannelItemType> getChannelItemTypesByItemTypeId(int itemTypeId) {
        return channelCatalogDao.getChannelItemTypesByItemTypeId(itemTypeId);
    }

    @Override
    @Transactional(readOnly = true)
    public ChannelItemType getChannelItemTypeByChannelAndChannelProductId(String channelCode, String channelProductId) {
        return getChannelItemTypeByChannelAndChannelProductId(channelCode, channelProductId, false);
    }

    @Override
    @Transactional(readOnly = true)
    public ChannelItemType getChannelItemTypeByChannelAndChannelProductId(String channelCode, String channelProductId, boolean lock) {
        Channel channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelByCode(channelCode);
        if (channel != null) {
            return getChannelItemTypeByChannelAndChannelProductId(channel.getId(), channelProductId, lock);
        }
        return null;
    }

    @Override
    public ChannelItemTypeVO getChannelItemTypeVOByChannelAndChannelProductId(String channelCode, String channelProductId) {
        return channelCatalogMao.getChannelItemTypeByChannelAndChannelProductId(channelCode, channelProductId);
    }

    @Override
    @Transactional(readOnly = true)
    public ChannelItemType getChannelItemTypeByChannelAndChannelProductId(int channelId, String channelProductId) {
        return getChannelItemTypeByChannelAndChannelProductId(channelId, channelProductId, false);
    }

    @Override
    @Transactional(readOnly = true)
    public ChannelItemType getChannelItemTypeByChannelAndChannelProductId(int channelId, String channelProductId, boolean lock) {
        return channelCatalogDao.getChannelItemTypeByChannelAndChannelProductId(channelId, channelProductId, lock);
    }

    @Override
    @Transactional
    public ChannelItemType blockChannelItemTypeInventory(String channelCode, String channelSkuCode, int quantity) {
        ChannelItemType channelItemType = getChannelItemTypeByChannelAndChannelProductId(channelCode, channelSkuCode);
        if (channelItemType != null) {
            int newBlockedInventory = channelItemType.getBlockedInventory() - quantity;
            channelItemType.setBlockedInventory(newBlockedInventory > 0 ? newBlockedInventory : 0);
            return channelCatalogDao.update(channelItemType);
        }
        return null;
    }

    @Override
    @Transactional
    public ChannelItemType decrementNextInventoryUpdate(ChannelItemType cit, int quantity) {
        if (cit.getNextInventoryUpdate() != null) {
            int newNextInventoryUpdate = cit.getNextInventoryUpdate() - quantity;
            cit.setNextInventoryUpdate(newNextInventoryUpdate > 0 ? newNextInventoryUpdate : 0);
            return channelCatalogDao.update(cit);
        } else {
            return null;
        }
    }

    @Override
    @Transactional
    public ChannelItemType updateChannelItemType(ChannelItemType channelItemType) {
        return channelCatalogDao.update(channelItemType);
    }

    @Override
    @Transactional
    public boolean updateChannelItemTypeInventoryStatusIfFresh(ChannelItemType cit) {
        return channelCatalogDao.updateChannelItemTypeIfFresh(cit);
    }

    @Override
    @Transactional
    public ChannelItemType lookupChannelItemType(String channelCode, String channelProductId, String sellerSkuCode, String productName) {
        ChannelItemType channelItemType = getChannelItemTypeByChannelAndChannelProductId(channelCode, channelProductId);
        if (channelItemType == null) {
            ItemType itemType = catalogService.getAllEnabledItemTypeBySkuCode(sellerSkuCode);
            if (itemType != null) {
                channelItemType = new ChannelItemType();
                channelItemType.setChannel(CacheManager.getInstance().getCache(ChannelCache.class).getChannelByCode(channelCode));
                channelItemType.setSellerSkuCode(sellerSkuCode);
                channelItemType.setItemType(itemType);
                channelItemType.setChannelProductId(channelProductId);
                channelItemType.setProductName(productName);
                channelItemType.setListingStatus(ListingStatus.ACTIVE);
                channelItemType.setStatusCode(Status.LINKED);
                channelItemType.setCreated(DateUtils.getCurrentTime());
                channelItemType = channelCatalogDao.update(channelItemType);
            }
        }
        return channelItemType;
    }

    @Override
    public ChannelItemTypeVO updateChannelItemType(ChannelItemTypeVO channelItemType) {
        return channelCatalogMao.update(channelItemType);
    }

    @Override
    @Transactional(readOnly = true)
    public long getLinkedChannelItemTypeCount(int channelId) {
        return channelCatalogDao.getLinkedChannelItemTypeCount(channelId);
    }

    @Override
    @Transactional(readOnly = true)
    public long getUnlinkedChannelItemTypeCount(int channelId) {
        return channelCatalogDao.getUnlinkedChannelItemTypeCount(channelId);
    }

    @Override
    @Transactional(readOnly = true)
    public long getIgnoredChannelItemTypeCount(int channelId) {
        return channelCatalogDao.getIgnoredChannelItemTypeCount(channelId);
    }

    @Override
    @Transactional(readOnly = true)
    public long getChannelItemTypeCount(int channelId, ListingStatus status) {
        return channelCatalogDao.getChannelItemTypeCount(channelId, status);
    }

    @Override
    @Transactional(readOnly = true)
    public long getChannelItemTypeCount(int channelId, String lastInventoryUpdateStatus) {
        return channelCatalogDao.getChannelItemTypeCount(channelId, lastInventoryUpdateStatus);
    }

    @Override
    @Transactional(readOnly = true)
    public long getChannelItemTypeCount(int channelId, String lastInventoryUpdateStatus, DateRange lastInventoryUpdateDateRange) {
        return channelCatalogDao.getChannelItemTypeCount(channelId, lastInventoryUpdateStatus, lastInventoryUpdateDateRange);
    }

    @Override
    @Transactional(readOnly = true)
    public List<ChannelItemType> getDirtyChannelItemTypes(int channelId, int start, int batchSize, Date lastUpdated) {
        if (ConfigurationManager.getInstance().getConfiguration(ProductConfiguration.class).isProductManagementSwitchedOff()) {
            return channelCatalogDao.getDirtyChannelItemTypes(channelId, start, batchSize, lastUpdated, Status.UNLINKED);
        } else {
            return channelCatalogDao.getDirtyChannelItemTypes(channelId, start, batchSize, lastUpdated, Status.LINKED);
        }
    }

    @Override
    @Transactional(readOnly = true)
    public List<ChannelItemType> getPossiblyDirtyChannelItemTypes(int channelId, int start, int batchSize, Date lastUpdated) {
        return channelCatalogDao.getPossiblyDirtyChannelItemTypes(channelId, start, batchSize, lastUpdated);
    }

    @Override
    @Transactional(readOnly = true)
    public Date getLatestPossiblyDirtyTimestamp(int channelId) {
        return channelCatalogDao.getLatestPossiblyDirtyTimestamp(channelId);
    }

    @Override
    @Transactional(readOnly = true)
    public long getPossiblyDirtyChannelItemTypeCount(Integer channelId, Date lastUpdated) {
        return channelCatalogDao.getPossiblyDirtyChannelItemTypeCount(channelId, lastUpdated);
    }

    @Override
    @Transactional(readOnly = true)
    public long getDirtyStockoutChannelItemTypeCount(int channelId, Date lastUpdated) {
        if (ConfigurationManager.getInstance().getConfiguration(ProductConfiguration.class).isProductManagementSwitchedOff()) {
            return channelCatalogDao.getDirtyStockoutChannelItemTypeCount(channelId, lastUpdated, Status.UNLINKED);
        } else {
            return channelCatalogDao.getDirtyStockoutChannelItemTypeCount(channelId, lastUpdated, Status.LINKED);
        }
    }

    @Override
    @Transactional(readOnly = true)
    public long getDirtyChannelItemTypeCount(int channelId, Date lastUpdated) {
        if (ConfigurationManager.getInstance().getConfiguration(ProductConfiguration.class).isProductManagementSwitchedOff()) {
            return channelCatalogDao.getDirtyChannelItemTypeCount(channelId, lastUpdated, Status.UNLINKED);
        } else {
            return channelCatalogDao.getDirtyChannelItemTypeCount(channelId, lastUpdated, Status.LINKED);
        }
    }

    @Override
    @Transactional(readOnly = true)
    public Date getLatestDirtyTimestamp(int channelId) {
        if (ConfigurationManager.getInstance().getConfiguration(ProductConfiguration.class).isProductManagementSwitchedOff()) {
            return channelCatalogDao.getLatestDirtyTimestamp(channelId, Status.UNLINKED);
        } else {
            return channelCatalogDao.getLatestDirtyTimestamp(channelId, Status.LINKED);
        }
    }

    @Override
    @Transactional(readOnly = true)
    public List<ChannelItemType> getDirtyStockoutChannelItemTypes(int channelId, int start, int batchSize, Date lastUpdated) {
        if (ConfigurationManager.getInstance().getConfiguration(ProductConfiguration.class).isProductManagementSwitchedOff()) {
            return channelCatalogDao.getDirtyStockoutChannelItemTypes(channelId, start, batchSize, lastUpdated, Status.UNLINKED);
        } else {
            return channelCatalogDao.getDirtyStockoutChannelItemTypes(channelId, start, batchSize, lastUpdated, Status.LINKED);
        }
    }

    @Override
    @Transactional(readOnly = true)
    public Date getLatestStockoutDirtyTimestamp(int channelId) {
        if (ConfigurationManager.getInstance().getConfiguration(ProductConfiguration.class).isProductManagementSwitchedOff()) {
            return channelCatalogDao.getLatestStockoutDirtyTimestamp(channelId, Status.UNLINKED);
        } else {
            return channelCatalogDao.getLatestStockoutDirtyTimestamp(channelId, Status.LINKED);
        }
    }

    @Override
    @Transactional
    public boolean markChannelItemTypesForRecalculation(Integer channelId) {
        return channelCatalogDao.markChannelItemTypesForRecalculation(channelId);
    }

    @Override
    @Transactional
    public boolean markAllChannelItemTypesForRecalculation(Integer channelId) {
        return channelCatalogDao.markAllChannelItemTypesForRecalculation(channelId);
    }

    @Override
    @Transactional
    public boolean resetFailedChannelItemTypes(Integer channelId) {
        return channelCatalogDao.resetFailedChannelItemTypes(channelId);
    }

    @Override
    @Transactional(readOnly = true)
    public List<ChannelItemType> getPendingChannelItemTypeForRecalculation(int start, int pageSize) {
        return channelCatalogDao.getPendingChannelItemTypeForRecalculation(start, pageSize);
    }

    @Override
    @Transactional
    public void resetChannelPendency(Integer channelId) {
        channelCatalogDao.resetChannelPendency(channelId);
    }

    @Override
    @Transactional
    public void linkChannelItemTypes(ItemType itemType) {
        List<ChannelItemType> unlinkedChannelItemTypes = channelCatalogDao.getUnlinkedChannelItemTypesBySellerSku(itemType.getSkuCode());
        for (ChannelItemType channelItemType : unlinkedChannelItemTypes) {
            channelItemType.setItemType(itemType);
            channelItemType.setStatusCode(Status.LINKED);
            channelCatalogDao.update(channelItemType);
        }
    }

    @Override
    public ReenableCITInventorySyncStatusResponse reenableCITInventorySyncStatus(ReenableCITInventorySyncStatusRequest request){
        ReenableCITInventorySyncStatusResponse response = new ReenableCITInventorySyncStatusResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            reenableInventorySync(request.getChannelItemType(), context);
        }
        if (!context.hasErrors()) {
            response.setSuccessful(true);
        } else {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    @Override
    public ReenableInventorySyncResponse reenableInventorySync(ReenableInventorySyncRequest request) {
        ReenableInventorySyncResponse response = new ReenableInventorySyncResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            for (WsReenableChannelItemType channelItemType : request.getChannelItemTypes()) {
                reenableInventorySync(channelItemType, context);
            }
        }
        if (!context.hasErrors()) {
            response.setSuccessful(true);
        } else {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    @Transactional
    private void reenableInventorySync(WsReenableChannelItemType wsChannelItemType, ValidationContext context) {
        ChannelItemType channelItemType = getChannelItemTypeByChannelAndChannelProductId(wsChannelItemType.getChannelCode(), wsChannelItemType.getChannelProductId());
        if (channelItemType == null) {
            context.addError(WsResponseCode.INVALID_CHANNEL_ITEM_TYPE,
                    "Invalid channel item type: " + wsChannelItemType.getChannelProductId() + ", channel: " + wsChannelItemType.getChannelCode());
        } else {
            channelItemType.setFailedCount(0);
            channelItemType.setDisabledDueToErrors(false);
            channelItemType.setDisabled(false);
            channelItemType.setRecalculateInventory(true);
        }
    }

    @Override
    @Transactional(readOnly = true)
    public List<ChannelItemTypeLookupDTO> lookupLinkedChannelItemTypes(String channelCode, String keyword) {
        List<ChannelItemTypeLookupDTO> channelItemTypeLookupDTOs = new ArrayList<>();
        Channel channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelByCode(channelCode);
        if (channel != null) {
            List<ChannelItemType> linkedChannelItemTypes = channelCatalogDao.lookupLinkedChannelItemTypes(channel.getId(), keyword);
            for (ChannelItemType channelItemType : linkedChannelItemTypes) {
                channelItemTypeLookupDTOs.add(new ChannelItemTypeLookupDTO(channelItemType));
            }
        }
        return channelItemTypeLookupDTOs;
    }

    @Override
    public UpdateChannelItemTypeTaxResponse updateChannelItemTypeTaxes(UpdateChannelItemTypeTaxRequest request) {
        UpdateChannelItemTypeTaxResponse response = new UpdateChannelItemTypeTaxResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            if (!context.hasErrors()) {
                for (WsChannelItemTypeTax channelItemTypeTax : request.getChannelItemTypeTaxes()) {
                    ChannelItemTypeTaxVO channelItemTypeTaxVO = getChannelItemTypeTax(channelItemTypeTax.getChannelCode(), channelItemTypeTax.getChannelProductId());
                    if (channelItemTypeTaxVO == null) {
                        channelItemTypeTaxVO = new ChannelItemTypeTaxVO();
                        channelItemTypeTaxVO.setChannelCode(channelItemTypeTax.getChannelCode());
                        channelItemTypeTaxVO.setChannelProductId(channelItemTypeTax.getChannelProductId());
                        channelItemTypeTaxVO.setHsnCode(channelItemTypeTax.getHsnCode());
                        channelItemTypeTaxVO.setCreated(DateUtils.getCurrentTime());
                    }
                    channelItemTypeTaxVO.setVatPercentage(ValidatorUtils.getOrDefaultValue(channelItemTypeTax.getVatPercentage(), channelItemTypeTaxVO.getVatPercentage()));
                    channelItemTypeTaxVO.setCstPercentage(ValidatorUtils.getOrDefaultValue(channelItemTypeTax.getCstPercentage(), channelItemTypeTaxVO.getVatPercentage()));
                    channelItemTypeTaxVO.setCentralGstPercentage(
                            ValidatorUtils.getOrDefaultValue(channelItemTypeTax.getCentralGstPercentage(), channelItemTypeTaxVO.getCentralGstPercentage()));
                    channelItemTypeTaxVO.setStateGstPercentage(
                            ValidatorUtils.getOrDefaultValue(channelItemTypeTax.getStateGstPercentage(), channelItemTypeTaxVO.getStateGstPercentage()));
                    channelItemTypeTaxVO.setUnionTerritoryGstPercentage(
                            ValidatorUtils.getOrDefaultValue(channelItemTypeTax.getUnionTerritoryGstPercentage(), channelItemTypeTaxVO.getUnionTerritoryGstPercentage()));
                    channelItemTypeTaxVO.setCompensationCessPercentage(
                            ValidatorUtils.getOrDefaultValue(channelItemTypeTax.getCompensationCessPercentage(), channelItemTypeTaxVO.getCompensationCessPercentage()));
                    channelItemTypeTaxVO.setUpdated(DateUtils.getCurrentTime());
                    if (request.getGlobalTaxPercentage() != null) {
                        Facility facility = CacheManager.getInstance().getCache(FacilityCache.class).getCurrentFacility();
                        facility.setTaxPercentage(request.getGlobalTaxPercentage());
                        update(facility);
                    }
                    update(channelItemTypeTaxVO);
                }
                response.setSuccessful(true);
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    @MarkDirty(values = { FacilityCache.class })
    @Transactional
    private void update(Facility facility) {
        facilityDao.save(facility);
    }

    @Override
    public ChannelItemTypeTaxVO getChannelItemTypeTax(String channelCode, String channelProductId) {
        return channelCatalogMao.getChannelItemTypeTax(channelCode, channelProductId);
    }

    @Override
    public ChannelItemTypeTaxVO update(ChannelItemTypeTaxVO channelItemTypeTax) {
        return channelCatalogMao.update(channelItemTypeTax);
    }

    @Override
    @Transactional
    public boolean markChannelItemTypeUnlinked(int channelId, String channelProductId) {
        return channelCatalogDao.markChannelItemTypeUnlinked(channelId, channelProductId);
    }

    @Override
    @Transactional
    public boolean resetFailedOrderInventory() {
        return channelCatalogDao.resetFailedOrderInventory();
    }

    /**
     * @param channelItemTypeId
     * @param failedOrderInventory can be negative (when deleting failed orders)
     * @return
     */
    @Transactional
    private ChannelItemType updateFailedOrderInventoryOnChannelItemType(int channelItemTypeId, int failedOrderInventory) {
        ChannelItemType channelItemType = getChannelItemTypeById(channelItemTypeId, true);
        channelItemType.setFailedOrderInventory(Math.max(channelItemType.getFailedOrderInventory() + failedOrderInventory, 0));
        return updateChannelItemType(channelItemType);
    }

    private ChannelItemType getChannelItemTypeById(int channelItemTypeId, boolean lock) {
        return channelCatalogDao.getChannelItemTypeById(channelItemTypeId, lock);
    }

    /**
     * Adjusts {@link ChannelItemType#failedOrderInventory}
     *
     * @param request
     * @param isNewFailedOrder
     */
    @Override
    public void updateFailedOrderInventory(CreateSaleOrderRequest request, boolean isNewFailedOrder) {
        Map<Integer, Integer> channelItemTypeIdToFailedOrderInventoryAdjustment = new HashMap<>();
        for (WsSaleOrderItem wsSaleOrderItem : request.getSaleOrder().getSaleOrderItems()) {
            ChannelItemType channelItemType = getChannelItemTypeByChannelAndChannelProductId(request.getSaleOrder().getChannel(), wsSaleOrderItem.getChannelProductId());
            if (channelItemType != null) {
                Integer adjustmentValue = channelItemTypeIdToFailedOrderInventoryAdjustment.get(channelItemType.getId());
                if (adjustmentValue == null) {
                    adjustmentValue = 0;
                }
                if (saleOrderService.isChannelItemTypeValidForSaleOrderCreation(channelItemType, request.getSaleOrder().isUseVerifiedListings())) {
                    if (isNewFailedOrder) {
                        channelItemTypeIdToFailedOrderInventoryAdjustment.put(channelItemType.getId(), adjustmentValue + 1);
                    } else {
                        channelItemTypeIdToFailedOrderInventoryAdjustment.put(channelItemType.getId(), adjustmentValue - 1);
                    }
                }
            }
        }
        for (Map.Entry<Integer, Integer> entry : channelItemTypeIdToFailedOrderInventoryAdjustment.entrySet()) {
            updateFailedOrderInventoryOnChannelItemType(entry.getKey(), entry.getValue());
        }
    }

    @Override
    @Transactional(readOnly = true)
    public List<ChannelItemType> getChannelItemTypesWithPendency(String channelCode) {
        Channel channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelByCode(channelCode);
        if (channel != null) {
            return channelCatalogDao.getChannelItemTypesWithPendency(channel.getId());
        }
        return null;
    }

    @Override
    public List<ChannelItemType> getChannelItemTypeBySellerSkuCode(String sellerSkuCode) {
        return channelCatalogDao.getChannelItemTypeBySellerSkuCode(sellerSkuCode);
    }

    @Override
    @Transactional
    public int disableStaleCatalog(String syncId, Integer channelId) {
        return channelCatalogDao.disableStaleCatalog(syncId, channelId);
    }
}
