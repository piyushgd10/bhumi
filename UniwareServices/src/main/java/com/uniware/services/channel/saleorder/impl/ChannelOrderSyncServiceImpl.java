/*
 *  Copyright 2013 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *  @version     1.0, 14-Jun-2013
 *  @author unicom
 */
package com.uniware.services.channel.saleorder.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.jibx.runtime.JiBXException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.unifier.core.annotation.audit.LogActivity;
import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.api.validation.WsError;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.email.EmailMessage;
import com.unifier.core.jms.MessagingConstants;
import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.DateUtils.Resolution;
import com.unifier.core.utils.JibxUtils;
import com.unifier.core.utils.JsonUtils;
import com.unifier.core.utils.StringUtils;
import com.unifier.core.utils.XMLParser;
import com.unifier.core.utils.XMLParser.Element;
import com.unifier.scraper.sl.runtime.IScriptProvider;
import com.unifier.scraper.sl.runtime.ScraperScript;
import com.unifier.scraper.sl.runtime.ScriptExecutionContext;
import com.unifier.services.email.IEmailService;
import com.unifier.services.tenantprofile.service.ITenantProfileService;
import com.unifier.services.vo.TenantProfileVO;
import com.unifier.services.vo.TenantProfileVO.PaymentStatus;
import com.uniware.core.api.channel.ChannelOrderSyncStatusDTO;
import com.uniware.core.api.channel.GetChannelOrderSyncStatusRequest;
import com.uniware.core.api.channel.GetChannelOrderSyncStatusResponse;
import com.uniware.core.api.channel.GetSaleOrderListScriptResponse;
import com.uniware.core.api.channel.SyncChannelCatalogRequest;
import com.uniware.core.api.channel.SyncChannelOrdersRequest;
import com.uniware.core.api.channel.SyncChannelOrdersResponse;
import com.uniware.core.api.model.WsChannelProductIdentifiers;
import com.uniware.core.api.saleorder.CreateSaleOrderRequest;
import com.uniware.core.api.saleorder.CreateSaleOrderResponse;
import com.uniware.core.api.saleorder.CreateSaleOrderResponse.ChannelItemTypeDTO;
import com.uniware.core.api.saleorder.RefreshSaleOrderRequest;
import com.uniware.core.api.saleorder.RefreshSaleOrderResponse;
import com.uniware.core.api.systemnotification.sandbox.SandboxParams;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.concurrent.ContextAwareExecutorFactory;
import com.uniware.core.entity.Channel;
import com.uniware.core.entity.Channel.SyncStatus;
import com.uniware.core.entity.ChannelConfigurationParameter;
import com.uniware.core.entity.ChannelConnector;
import com.uniware.core.entity.ChannelConnectorParameter;
import com.uniware.core.entity.SaleOrder;
import com.uniware.core.entity.Source;
import com.uniware.core.entity.SourceConnector;
import com.uniware.core.locking.Namespace;
import com.uniware.core.locking.annotation.Lock;
import com.uniware.core.locking.annotation.Locks;
import com.uniware.core.utils.ActivityContext;
import com.uniware.core.utils.Constants.EmailTemplateType;
import com.uniware.core.utils.UserContext;
import com.uniware.core.vo.SaleOrderVO;
import com.uniware.core.vo.SaleOrderVO.StatusCode;
import com.uniware.dao.saleorder.ISaleOrderDao;
import com.uniware.services.audit.impl.ActivityEntityEnum;
import com.uniware.services.audit.impl.ActivityTypeEnum;
import com.uniware.services.audit.impl.ActivityUtils;
import com.uniware.services.cache.ChannelCache;
import com.uniware.services.cache.ScriptVersionedCache;
import com.uniware.services.channel.IChannelCatalogService;
import com.uniware.services.channel.IChannelCatalogSyncService;
import com.uniware.services.channel.IChannelService;
import com.uniware.services.channel.impl.ChannelServiceImpl;
import com.uniware.services.channel.impl.ChannelServiceImpl.ChannelGoodToSyncResponse;
import com.uniware.services.channel.saleorder.IChannelOrderSyncService;
import com.uniware.services.channel.saleorder.IChannelPendencySyncService;
import com.uniware.services.configuration.SourceConfiguration;
import com.uniware.core.locking.ILockingService;
import com.uniware.services.messaging.jms.ActiveMQConnector;
import com.uniware.services.messaging.jms.SyncChannelOrdersEvent;
import com.uniware.services.saleorder.ISaleOrderService;

@Service("channelOrderSyncService")
public class ChannelOrderSyncServiceImpl implements IChannelOrderSyncService {

    private static final Logger LOG = LoggerFactory.getLogger(ChannelOrderSyncServiceImpl.class);

    private static final String SERVICE_NAME = "channelordersyncservice";

    public static final String TOTAL_PAGES = "TotalPages";

    @Autowired
    private ISaleOrderService saleOrderService;

    @Autowired
    private IEmailService emailService;

    @Autowired
    private IChannelService channelService;

    @Autowired
    private IChannelPendencySyncService channelPendencySyncService;

    @Autowired
    private ContextAwareExecutorFactory executorFactory;

    @Autowired
    private ISaleOrderDao               saleOrderDao;

    @Autowired
    private IChannelCatalogSyncService  channelCatalogSyncService;

    @Autowired
    private IChannelCatalogService      channelCatalogService;

    @Autowired
    private ILockingService             lockingService;

    @Autowired
    @Qualifier("activeMQConnector1")
    private ActiveMQConnector           activeMQConnector;

    @Autowired
    private ITenantProfileService       tenantProfileService;


    @Override
    public GetChannelOrderSyncStatusResponse getChannelOrderSyncStatus(GetChannelOrderSyncStatusRequest request) {
        GetChannelOrderSyncStatusResponse response = new GetChannelOrderSyncStatusResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            response.setOrderSyncStatus(channelService.getChannelOrderSyncStatus(request.getChannelCode()));
            response.setSuccessful(true);
        } else {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    @Override
    public SyncChannelOrdersResponse syncChannelOrders(SyncChannelOrdersRequest request) {
        SyncChannelOrdersResponse response = new SyncChannelOrdersResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            for (String channelCode : request.getChannelCodes()) {
                Channel channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelByCode(channelCode);
                if (channel == null) {
                    response.getResultItems().add(new SyncChannelOrdersResponse.ChannelOrderSyncResponseDTO(channelCode, "Invalid channel code"));
                    context.addError(WsResponseCode.INVALID_REQUEST, "Invalid channel code");
                } else if (!channel.isEnabled()) {
                    response.getResultItems().add(new SyncChannelOrdersResponse.ChannelOrderSyncResponseDTO(channelCode, "Channel disabled"));
                    context.addError(WsResponseCode.INVALID_REQUEST, "Channel disabled");
                } else if (!SyncStatus.ON.equals(channel.getOrderSyncStatus())) {
                    response.getResultItems().add(new SyncChannelOrdersResponse.ChannelOrderSyncResponseDTO(channelCode, "Sync not active"));
                    context.addError(WsResponseCode.INVALID_REQUEST, "Sync not active");
                } else {
                    TenantProfileVO tenantProfile = tenantProfileService.getTenantProfileByCode(UserContext.current().getTenant().getCode());
                    if (StringUtils.equalsAny(tenantProfile.getPaymentStatus(), PaymentStatus.LIMITED_SERVICES, PaymentStatus.BLOCKED)) {
                        context.addError(WsResponseCode.INVALID_REQUEST, "Account blocked");
                    } else {
                        SyncChannelOrdersEvent syncChannelOrdersEvent = new SyncChannelOrdersEvent();
                        syncChannelOrdersEvent.setChannelCode(channelCode);
                        syncChannelOrdersEvent.setSyncNewOrders(request.isSyncNewOrders());
                        syncChannelOrdersEvent.setRequestTimestamp(DateUtils.getCurrentTime());
                        activeMQConnector.produceContextAwareMessage(MessagingConstants.Queue.ORDER_SYNC_QUEUE, syncChannelOrdersEvent);
                        response.setSuccessful(true);
                        response.setMessage("Request queued");
                        LOG.info("Queued SyncChannelOrdersEvent for channel: {}", channel.getCode());
                    }
                }
            }
        }
        response.addWarnings(context.getWarnings());
        response.addErrors(context.getErrors());
        response.setSuccessful(!context.hasErrors());
        return response;
    }

    @Override
    public void syncChannelOrders(SyncChannelOrdersEvent syncChannelOrdersEvent) {
        executorFactory.getExecutor(SERVICE_NAME).submit(new SyncChannelOrders(syncChannelOrdersEvent));
    }

    private class SyncChannelOrders implements Runnable {
        private final SyncChannelOrdersEvent           syncChannelOrdersEvent;
        private final Set<WsChannelProductIdentifiers> channelProductIdentifiers = new HashSet<>();

        public SyncChannelOrders(SyncChannelOrdersEvent syncChannelOrdersEvent) {
            this.syncChannelOrdersEvent = syncChannelOrdersEvent;
        }

        @Override
        public String toString() {
            return syncChannelOrdersEvent.getChannelCode() + ":syncChannelOrders";
        }

        @Override
        public void run() {
            Channel channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelByCode(syncChannelOrdersEvent.getChannelCode());
            ChannelOrderSyncStatusDTO channelOrderSyncStatusDTO = channelService.getChannelOrderSyncStatus(syncChannelOrdersEvent.getChannelCode());
            java.util.concurrent.locks.Lock orderSynRunningLock = lockingService.getLock(Namespace.CHANNEL_ORDER_SYNC_DO, channel.getCode());
            boolean acquired = false;
            try {
                acquired = orderSynRunningLock.tryLock();
                if (!acquired) {
                    LOG.info("Failed to obtain {} lock for channel {}. Sync is already running.", Namespace.CHANNEL_ORDER_SYNC_DO.name(), channel.getCode());
                } else if (channelOrderSyncStatusDTO.getLastSyncTime() != null
                        && channelOrderSyncStatusDTO.getLastSyncTime().after(syncChannelOrdersEvent.getRequestTimestamp())) {
                    LOG.info("Ignoring SyncChannelOrdersEvent as lastSyncTime: {} is newer than requestTimestamp: {}", channelOrderSyncStatusDTO.getLastSyncTime(),
                            syncChannelOrdersEvent.getRequestTimestamp());
                } else {
                    runOrderSync(channelOrderSyncStatusDTO, channel);
                }
            } catch (Exception e) {
                LOG.error("Error while running order sync", e);
            } finally {
                if (acquired) {
                    orderSynRunningLock.unlock();
                    channelOrderSyncStatusDTO.setLastSyncTime(DateUtils.getCurrentTime());
                    channelOrderSyncStatusDTO.setSyncExecutionStatus(Channel.SyncExecutionStatus.IDLE);
                    LOG.info("Updating last order sync time mongo for channel {}", channel.getName());
                    channelService.updateChannelOrderSyncStatus(channelOrderSyncStatusDTO);
                    LOG.info("Updated last order sync time mongo for channel {}", channel.getName());
                }
            }
        }

        @LogActivity
        private void runOrderSync(ChannelOrderSyncStatusDTO channelOrderSyncStatusDTO, Channel channel) {
            channelOrderSyncStatusDTO.reset();
            channelOrderSyncStatusDTO.setLastSyncTime(DateUtils.getCurrentTime());
            channelOrderSyncStatusDTO.setSyncExecutionStatus(Channel.SyncExecutionStatus.RUNNING);
            channelService.updateChannelOrderSyncStatus(channelOrderSyncStatusDTO);
            if (ActivityContext.current().isEnable() && StringUtils.isNotBlank(UserContext.current().getUniwareUserName())) {
                ActivityUtils.appendActivity(channel.getCode(), ActivityEntityEnum.CHANNEL.getName(), channel.getSourceCode(),
                        Arrays.asList(new String[] { "Order Sync was triggered manually" }), ActivityTypeEnum.SYNC_TRIGGERED.name());
            }
            LOG.info("Verifying order sync connectors for channel {}", channel.getName());
            channelOrderSyncStatusDTO.setMessage("Verifying connectors");
            channelService.updateChannelOrderSyncStatus(channelOrderSyncStatusDTO);
            ChannelGoodToSyncResponse channelGoodToSyncResponse = channelService.isChannelGoodToSync(channel.getCode(), ChannelServiceImpl.Sync.ORDER);
            if (!channelGoodToSyncResponse.isGoodToSync()) {
                channelOrderSyncStatusDTO.reset();
                channelOrderSyncStatusDTO.setMessage("Connector is broken");
                channelService.updateChannelOrderSyncStatus(channelOrderSyncStatusDTO);
                LOG.info("Connector broken for channel {}", channel.getName());
                return;
            }
            channelOrderSyncStatusDTO.setMessage("Connectors verified for order sync");
            channelService.updateChannelOrderSyncStatus(channelOrderSyncStatusDTO);
            Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(channel.getSourceCode());
            if (source.isPendencyConfigurationEnabled()) {
                LOG.info("Refreshing pendency for channel {}", channel.getName());
                channelPendencySyncService.refreshPendency(channel.getCode());
            }

            LOG.info("Running order sync for channel {}", channel.getName());
            if (UserContext.current().isTraceLoggingEnabled()) {
                LOG.info("SandboxParameters for channel order sync: {}", JsonUtils.objectToString(getSandboxParameters(channel)));
            }
            ScraperScript saleOrderDetailsScript = CacheManager.getInstance().getCache(ChannelCache.class).getScriptByName(channel.getCode(),
                    com.uniware.core.entity.Source.SALE_ORDER_DETAILS_SCRIPT_NAME, true);
            GetSaleOrderListScriptResponse response = null;
            int successfulImportCount = 0, failedImportCount = 0;
            Map<String, Object> metadata = new HashMap<>();
            Integer pageNumber = 1;
            do {
                response = getSaleOrderList(channel, channelGoodToSyncResponse.getParams(), pageNumber++, syncChannelOrdersEvent.isSyncNewOrders(), metadata);
                if (!response.isSuccessful()) {
                    LOG.error("[FATAL ERROR] Error executing sync task for channel: {}, Message: {}", source.getName(), response.getMessage());
                    updateStatus(0, saleOrderDao.getOrderCountCreatedTodayForChannel(channel.getId()), 0, true, response.getMessage(), channelOrderSyncStatusDTO, channel);
                    return;
                }
                if (response.getTotalPages() != null) {
                    channelOrderSyncStatusDTO.setTotalMileStones(response.getTotalPages());
                    channelOrderSyncStatusDTO.setMileStone("Importing page number:" + pageNumber);
                } else {
                    channelOrderSyncStatusDTO.setTotalMileStones(response.getSaleOrderElements().size());
                }
                channelService.updateChannelOrderSyncStatus(channelOrderSyncStatusDTO);
                for (Element saleOrderElement : response.getSaleOrderElements()) {
                    String saleOrderCode = saleOrderElement.text();
                    if (response.getTotalPages() == null) {
                        channelOrderSyncStatusDTO.setMileStone("Importing sale order:" + saleOrderCode);
                        channelService.updateChannelOrderSyncStatus(channelOrderSyncStatusDTO);
                    }
                    SaleOrder existingOrder = saleOrderService.getSaleOrderByCode(saleOrderCode);
                    if (existingOrder != null) {
                        if (!existingOrder.getChannel().getId().equals(channel.getId())) {
                            failedImportCount++;
                            LOG.warn("Skipping saleOrderCode {} as it is already present in the system with channel code {}", saleOrderCode, existingOrder.getChannel().getId());
                        }
                        continue;
                    }
                    SaleOrderVO failedSaleOrder = saleOrderService.getFailedSaleOrderByCode(saleOrderCode);
                    if (failedSaleOrder != null) {
                        LOG.warn("Skipping saleOrderCode {} as it is already present in failed Orders", saleOrderCode);
                        continue;
                    }

                    try {
                        CreateSaleOrderRequest request = getSaleOrderDetailsRequest(saleOrderCode, saleOrderElement, response.getSaleOrderCodeToOrderElement().get(saleOrderCode),
                                saleOrderDetailsScript, channel, channelGoodToSyncResponse.getParams(), metadata);
                        if (request == null) {
                            LOG.warn("Import skipped for saleOrder:{} ", new Object[] { saleOrderCode });
                        } else if (source.isDisplayOrderCodeUnique() && request.getSaleOrder().getDisplayOrderCode() != null
                                && !saleOrderService.getSaleOrderByDisplayOrderCode(request.getSaleOrder().getDisplayOrderCode(), channel.getId()).isEmpty()) {
                            LOG.warn("Skipping saleOrderCode {}, displayOrderCode {} as it is already present in the system.", saleOrderCode,
                                    request.getSaleOrder().getDisplayOrderCode());
                        } else if (source.isDisplayOrderCodeToSaleOrderCodeUnique() && request.getSaleOrder().getDisplayOrderCode() != null
                                && saleOrderService.getSaleOrderByCode(request.getSaleOrder().getDisplayOrderCode(), channel.getId()) != null) {
                            LOG.warn("Skipping saleOrderCode {}, displayOrderCode {} as it is already present in the system.", saleOrderCode,
                                    request.getSaleOrder().getDisplayOrderCode());
                        } else {
                            request.getSaleOrder().setChannel(channel.getCode());
                            if (request.getSaleOrder().getPriority() == null) {
                                request.getSaleOrder().setPriority(channel.getInventoryAllocationPriority());
                            }
                            if (source.isSelectiveCatalogSyncEnabled() && source.isOrderReconciliationEnabled() && (channel.getReconciliationSyncStatus().equals(SyncStatus.ON))) {
                                request.getSaleOrder().setUseVerifiedListings(true);
                            }
                            CreateSaleOrderResponse saleOrderResponse = saleOrderService.createSaleOrder(request);
                            if (saleOrderResponse.isSuccessful()) {
                                successfulImportCount++;
                                LOG.info("Import successful for saleOrder:{} ", new Object[] { saleOrderCode });
                            } else if (saleOrderResponse.getErrors().get(0).getCode() == WsResponseCode.DUPLICATE_SALE_ORDER.code()) {
                                LOG.warn("Duplicate request for saleOrder: {}", request.getSaleOrder().getCode());
                            } else {
                                failedImportCount++;
                                LOG.warn("Import failed for saleOrder:{}, errors: {} ", new Object[] { saleOrderCode, saleOrderResponse.getErrors() });
                                if (UserContext.current().isTraceLoggingEnabled()) {
                                    SandboxParams sandboxParams = getSandboxParameters(channel);
                                    sandboxParams.setSaleOrderCode(saleOrderCode);
                                    LOG.info("SandboxParameters for sale order import failure: {}", JsonUtils.objectToString(sandboxParams));
                                }
                                for (WsError error : saleOrderResponse.getErrors()) {
                                    if (WsResponseCode.SKU_NOT_MAPPED.code() == error.getCode()) {
                                        ChannelItemTypeDTO channelItemTypeDTO = (ChannelItemTypeDTO) error.getErrorParams().get("channelItemType");
                                        if (request.getSaleOrder().isUseVerifiedListings()
                                                && (ChannelItemTypeDTO.Status.MISSING.equals(channelItemTypeDTO.getStatusCode()) || ChannelItemTypeDTO.Status.UNVERIFIED.equals(channelItemTypeDTO.getStatusCode()))) {
                                            channelProductIdentifiers.add(new WsChannelProductIdentifiers(channelItemTypeDTO.getSellerSkuCode(),
                                                    channelItemTypeDTO.getChannelProductId()));
                                        }
                                        if (ChannelItemTypeDTO.Status.IGNORED.equals(channelItemTypeDTO.getStatusCode())) {
                                            LOG.info("Marking IGNORED channel item type {} as UNLINKED", channelItemTypeDTO.getChannelProductId());
                                            channelCatalogService.markChannelItemTypeUnlinked(channel.getId(), channelItemTypeDTO.getChannelProductId());
                                        }
                                    }
                                }
                                persistFailedOrder(request.getSaleOrder().getCode(), request, saleOrderResponse);
                            }
                        }
                    } catch (Throwable e) {
                        failedImportCount++;
                        LOG.error("[FATAL ERROR] Error importing sale order: " + saleOrderCode, e);
                       if (UserContext.current().isTraceLoggingEnabled()) {
                           SandboxParams sandboxParams = getSandboxParameters(channel);
                           sandboxParams.setSaleOrderCode(saleOrderCode);
                           LOG.info("SandboxParameters for exception in sale order import: {}", JsonUtils.objectToString(sandboxParams));
                        }
                        continue;
                    }
                }
                LOG.info("Created [{}] order, failed order [{}] for page number {}", new Object[] { successfulImportCount, failedImportCount, pageNumber - 1 });
            } while (response != null && response.isHasMoreResults());
            channelOrderSyncStatusDTO.setLastSyncSuccessful(true);
            LOG.info("Updating order sync status in mongo for channel {}", channel.getName());
            updateStatus(successfulImportCount, saleOrderDao.getOrderCountCreatedTodayForChannel(channel.getId()), failedImportCount, false,
                    channelOrderSyncStatusDTO.getMessage(), channelOrderSyncStatusDTO, channel);
            LOG.info("Updated order sync status in mongo for channel {}", channel.getName());
            if (!channelProductIdentifiers.isEmpty()) {
                syncChannelCatalogForProductIdentifiers(channel, channelProductIdentifiers);
            }
        }

		private SandboxParams getSandboxParameters(Channel channel) {
			SandboxParams sandboxParams = new SandboxParams();
			Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class)
					.getSourceByCode(channel.getSourceCode());
			try {
				sandboxParams.setChannel(channel);
				sandboxParams.setSource(source);
				sandboxParams.setChannelConnectorParameters(channel.getChannelConnectorParameters());
			} catch (Exception e) {
				LOG.error("Error creating sandbox params", e);
			}
			return sandboxParams;
		}

		@LogActivity
		private void persistFailedOrder(String saleOrderCode, CreateSaleOrderRequest request,
				CreateSaleOrderResponse response) {
			SaleOrderVO saleOrderVO = new SaleOrderVO();
			boolean activityEnabled = ActivityContext.current().isEnable();
			String errorDescription = null;
			List<String> activityLogs = new ArrayList<>();
			saleOrderVO.setCode(saleOrderCode);
			saleOrderVO.setCreated(DateUtils.getCurrentTime());
			saleOrderVO.setRequest(request);
			saleOrderVO.setResponse(response);
			saleOrderVO.setStatusCode(StatusCode.FAILED);
			saleOrderService.createOrUpdateFailedSaleOrder(saleOrderVO);
            Channel channel = null;
            if (StringUtils.isNotBlank(request.getSaleOrder().getChannel())) {
                channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelByCode(request.getSaleOrder().getChannel());
            } else {
                channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelByName(com.uniware.core.entity.Source.Code.CUSTOM.name());
            }
            if (channel == null || !ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(channel.getSourceCode()).isFetchCompleteOrders()) {
                channelCatalogService.updateFailedOrderInventory(saleOrderVO.getRequest(), true);
            }
            if (activityEnabled) {
                for (WsError responseErrorElement : response.getErrors()) {
                    errorDescription = errorDescription + responseErrorElement.getDescription() + ',';
                }
                errorDescription = errorDescription.substring(0, errorDescription.length() - 1);
                activityLogs.add("Import Failed - " + errorDescription);
                ActivityUtils.appendActivity(request.getSaleOrder().getCode(), ActivityEntityEnum.SALE_ORDER.getName(), request.getSaleOrder().getCode(), activityLogs,
                        ActivityTypeEnum.FAILED.name());
                if (!activityLogs.isEmpty()) {
                    ActivityUtils.appendActivity(request.getSaleOrder().getCode(), ActivityEntityEnum.SALE_ORDER.getName(), request.getSaleOrder().getCode(), activityLogs,
                            ActivityTypeEnum.FAILED.name());
                }
            }
        }

        private void updateStatus(int successfulImportCount, long todaysSuccessfulCount, int failedImportCount, boolean failed, String message,
                ChannelOrderSyncStatusDTO channelOrderSyncStatusDTO, Channel channel) {
            if ((failedImportCount == 0 && channelOrderSyncStatusDTO.getLastFailedImportCount() > 0)
                    || (failedImportCount > 0 && (channelOrderSyncStatusDTO.getLastFailedImportCount() == 0
                            || channelOrderSyncStatusDTO.getLastSyncFailedNotificationTime() == null || DateUtils.diff(DateUtils.getCurrentTime(),
                            channelOrderSyncStatusDTO.getLastSyncFailedNotificationTime(), Resolution.HOUR) > 3))) {
                sendSyncFailedNotification(channelOrderSyncStatusDTO, channel);
                channelOrderSyncStatusDTO.setLastSyncFailedNotificationTime(DateUtils.getCurrentTime());
            }
            if (successfulImportCount > 0) {
                channelOrderSyncStatusDTO.setLastSuccessfulOrderReceivedTime(DateUtils.getCurrentTime());
            }
            channelOrderSyncStatusDTO.setLastSuccessfulImportCount(successfulImportCount);
            channelOrderSyncStatusDTO.setLastFailedImportCount(failedImportCount);
            channelOrderSyncStatusDTO.setTodaysSuccessfulImportCount(todaysSuccessfulCount);
            // change failed only when Sync task did not fail
            if (!failed) {
                channelOrderSyncStatusDTO.setTodaysFailedImportCount(failedImportCount);
            }
            channelOrderSyncStatusDTO.setLastSyncTime(DateUtils.getCurrentTime());
            channelOrderSyncStatusDTO.setMessage(message);
            channelOrderSyncStatusDTO.setSyncExecutionStatus(Channel.SyncExecutionStatus.IDLE);
            channelService.updateChannelOrderSyncStatus(channelOrderSyncStatusDTO);
        }

        private void sendSyncFailedNotification(ChannelOrderSyncStatusDTO channelResultStatus, Channel channel) {
            EmailMessage message = new EmailMessage(EmailTemplateType.CHANNEL_SYNC_FAILED_NOTIFICATION.name());
            message.addTemplateParam("channelStatus", channelResultStatus);
            message.addTemplateParam("channel", channel);
            emailService.send(message);
        }

        private CreateSaleOrderRequest getSaleOrderDetailsRequest(String saleOrderCode, Element saleOrderElement, Object saleOrderDetailElement,
                ScraperScript saleOrderDetailsScript, Channel channel, Map<String, String> channelParams, Map<String, Object> metadata) throws JiBXException, IOException {
            Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(channel.getSourceCode());
            ScriptExecutionContext context = ScriptExecutionContext.current();
            context.addVariable("channel", channel);
            context.addVariable("source", source);
            context.addVariable("saleOrderCode", saleOrderCode);
            context.addVariable("saleOrderElement", saleOrderElement);
            context.addVariable("saleOrderDetailElement", saleOrderDetailElement);
            context.addVariable("metadata", metadata);
            for (Entry<String, String> e : channelParams.entrySet()) {
                context.addVariable(e.getKey(), e.getValue());
            }
            context.setScriptProvider(new IScriptProvider() {

                @Override
                public ScraperScript getScript(String scriptName) {
                    return CacheManager.getInstance().getCache(ScriptVersionedCache.class).getScriptByName(scriptName);
                }
            });
            try {
                context.setTraceLoggingEnabled(UserContext.current().isTraceLoggingEnabled());
                saleOrderDetailsScript.execute();
                String saleOrderRequestXml = context.getScriptOutput();
                if (StringUtils.isNotBlank(saleOrderRequestXml)) {
                    LOG.info("Creating sale order for request: {}", saleOrderRequestXml);
                    return JibxUtils.unmarshall("unicommerceServicesBinding19", CreateSaleOrderRequest.class, saleOrderRequestXml);
                } else {
                    return null;
                }
            } finally {
                ScriptExecutionContext.destroy();
            }
        }

        private GetSaleOrderListScriptResponse getSaleOrderList(Channel channel, Map<String, String> channelParams, Integer pageNumber, boolean syncNewOrders,
                Map<String, Object> metadata) {
            Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(channel.getSourceCode());
            ScraperScript saleOrderListScript = CacheManager.getInstance().getCache(ChannelCache.class).getScriptByName(channel.getCode(),
                    com.uniware.core.entity.Source.SALE_ORDER_LIST_SCRIPT_NAME, true);
            GetSaleOrderListScriptResponse response = new GetSaleOrderListScriptResponse();
            ScriptExecutionContext context = ScriptExecutionContext.current();
            context.addVariable("source", source);
            context.addVariable("channel", channel);
            context.addVariable("isFrequentOrderSync", syncNewOrders);
            context.addVariable("pageNumber", pageNumber);
            context.addVariable("metadata", metadata);
            for (Entry<String, String> e : channelParams.entrySet()) {
                context.addVariable(e.getKey(), e.getValue());
            }
            context.setScriptProvider(new IScriptProvider() {

                @Override
                public ScraperScript getScript(String scriptName) {
                    return CacheManager.getInstance().getCache(ScriptVersionedCache.class).getScriptByName(scriptName);
                }
            });
            //
            // For certain channels like ebay, the list script returns the order
            // details as well. So we cache the orders (XML elements) in a maps
            // and pass it to the get-order-detail script.
            //
            Map<String, Object> resultItems = new HashMap<>();
            context.addVariable("resultItems", resultItems);
            try {
                boolean traceLoggingEnabled = UserContext.current().isTraceLoggingEnabled();
                if (traceLoggingEnabled) {
                    LOG.info("SandboxParameters for sale order list script execution: {}", JsonUtils.objectToString(getSandboxParameters(channel)));
                }
                context.setTraceLoggingEnabled(traceLoggingEnabled);
                saleOrderListScript.execute();
                String saleOrderListXml = context.getScriptOutput();
                if (StringUtils.isNotBlank(saleOrderListXml)) {
                    List<Element> saleOrderElements = new ArrayList<>();
                    Element rootElement = XMLParser.parse(saleOrderListXml);

                    if (StringUtils.isNotBlank(rootElement.text(TOTAL_PAGES))) {
                        Integer totalPages = Integer.parseInt(rootElement.text(TOTAL_PAGES));
                        response.setHasMoreResults(totalPages > pageNumber);
                        response.setTotalPages(totalPages);
                    }

                    for (Element saleOrderElement : rootElement.list("SaleOrder")) {
                        saleOrderElements.add(saleOrderElement);
                    }
                    response.setSuccessful(true);
                    response.setSaleOrderElements(saleOrderElements);
                    response.setSaleOrderCodeToOrderElement(resultItems);
                } else {
                    response.setMessage("No sale order codes in list output");
                }
            } catch (Exception e) {
                response.setMessage(e.getMessage());
                LOG.error("Error executing order sync", e);
            } finally {
                ScriptExecutionContext.destroy();
            }
            return response;
        }

        private void syncChannelCatalogForProductIdentifiers(Channel channel, Set<WsChannelProductIdentifiers> channelProductIdentifiers) {
            SyncChannelCatalogRequest syncCatalogRequest = new SyncChannelCatalogRequest(channel.getCode(), new ArrayList<>(channelProductIdentifiers), true);
            channelCatalogSyncService.syncChannelCatalog(syncCatalogRequest);
        }
    }

    @Override
    @Locks({ @Lock(ns = Namespace.SALE_ORDER, key = "#{#args[0].saleOrderCode}") })
    @Transactional
    public RefreshSaleOrderResponse refreshSaleOrder(RefreshSaleOrderRequest request) {
        RefreshSaleOrderResponse response = new RefreshSaleOrderResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            SaleOrder saleOrder = saleOrderDao.getSaleOrderForUpdate(request.getSaleOrderCode());
            if (saleOrder == null) {
                context.addError(WsResponseCode.INVALID_SALE_ORDER_CODE);
            } else {
                Channel channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelById(saleOrder.getChannel().getId());
                Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(channel.getSourceCode());
                if (StringUtils.isNotBlank(CacheManager.getInstance().getCache(ChannelCache.class).getScriptName(channel.getCode(), Source.CHANNEL_REFRESH_SALE_ORDER_SCRIPT_NAME))
                        && source.isOrderRefreshEnabled()) {
                    ScraperScript scraperScript = CacheManager.getInstance().getCache(ChannelCache.class).getScriptByName(channel.getCode(),
                            Source.CHANNEL_REFRESH_SALE_ORDER_SCRIPT_NAME);
                    if (scraperScript != null) {
                        try {
                            executeRefreshSaleOrderScript(saleOrder, channel, scraperScript);
                        } catch (Exception e) {
                            LOG.error("Unable to refresh order " + saleOrder.getCode() + " :", e);
                            context.addError(WsResponseCode.INVALID_SALE_ORDER_STATE, "Unable to refresh order on " + source.getName() + ": " + e.getMessage());
                        }
                    } else {
                        context.addError(WsResponseCode.INVALID_SCRIPT, "Script is empty");
                    }
                }
            }
        }
        if (!context.hasErrors()) {
            response.setSuccessful(true);
        } else {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    private void executeRefreshSaleOrderScript(SaleOrder saleOrder, Channel channel, ScraperScript scraperScript) {
        final ScriptExecutionContext context = ScriptExecutionContext.current();
        SourceConfiguration sourceConfiguration = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class);
        Source source = sourceConfiguration.getSourceByCode(channel.getSourceCode());
        context.addVariable("source", source);
        context.addVariable("channel", channel);
        context.addVariable("saleOrder", saleOrder);
        for (ChannelConnector channelConnector : channel.getChannelConnectors()) {
            SourceConnector sourceConnector = sourceConfiguration.getSourceConnector(channel.getSourceCode(), channelConnector.getSourceConnectorName());
            if (sourceConnector.isRequiredInOrderSync()) {
                for (ChannelConnectorParameter channelConnectorParameter : channelConnector.getChannelConnectorParameters()) {
                    context.addVariable(channelConnectorParameter.getName(), channelConnectorParameter.getValue());
                }
            }
        }
        for (ChannelConfigurationParameter channelConfigurationParameter : channel.getChannelConfigurationParameters()) {
            context.addVariable(channelConfigurationParameter.getSourceConfigurationParameterName(), channelConfigurationParameter.getValue());
        }
        context.setScriptProvider(new IScriptProvider() {
            @Override
            public ScraperScript getScript(String scriptName) {
                return CacheManager.getInstance().getCache(ScriptVersionedCache.class).getScriptByName(scriptName);
            }
        });
        String resultItemsXml;
        try {
            context.setTraceLoggingEnabled(UserContext.current().isTraceLoggingEnabled());
            scraperScript.execute();
            resultItemsXml = ScriptExecutionContext.current().getScriptOutput();
            Element rootElement = XMLParser.parse(resultItemsXml);
            if (rootElement.get("AdditionalInfo") != null) {
                String additionalInfo = rootElement.get("AdditionalInfo").toString();
                saleOrder.setAdditionalInfo(additionalInfo);
            }
            if (rootElement.get("SaleOrderCode") != null) {
                String code = rootElement.get("SaleOrderCode").toString();
                saleOrder.setCode(code);
            }
        } finally {
            ScriptExecutionContext.destroy();
        }
    }
}
