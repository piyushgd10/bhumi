/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Apr 29, 2012
 *  @author singla
 */
package com.uniware.services.channel;

import com.uniware.core.api.channel.ChannelDetailDTO;
import com.uniware.core.api.channel.GetChannelInventorySyncStatusRequest;
import com.uniware.core.api.channel.GetChannelInventorySyncStatusResponse;
import com.uniware.core.api.channel.GetChannelInventoryUpdateSnapshotRequest;
import com.uniware.core.api.channel.GetChannelInventoryUpdateSnapshotResponse;
import com.uniware.core.api.channel.SyncChannelItemTypeInventoryRequest;
import com.uniware.core.api.channel.SyncChannelItemTypeInventoryResponse;
import com.uniware.core.api.channel.UpdateInventoryByChannelProductIdRequest;
import com.uniware.core.api.channel.UpdateInventoryByChannelProductIdResponse;
import com.uniware.core.api.inventory.CalculateChannelItemTypeInventoryResponse;
import com.uniware.core.api.inventory.SearchInventorySnapshotResponse.InventorySnapshotDTO;
import com.uniware.core.api.inventory.UpdateInventoryOnAllChannelsRequest;
import com.uniware.core.api.inventory.UpdateInventoryOnAllChannelsResponse;
import com.uniware.core.api.inventory.UpdateInventoryOnChannelRequest;
import com.uniware.core.api.inventory.UpdateInventoryOnChannelResponse;
import com.uniware.core.entity.ChannelItemType;
import com.uniware.core.entity.ItemType;
import com.uniware.core.vo.ChannelInventoryFormulaVO;
import com.uniware.services.messaging.jms.SyncChannelInventoryEvent;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Sunny
 */
public interface IChannelInventorySyncService {

    UpdateInventoryOnChannelResponse updateInventoryOnChannel(UpdateInventoryOnChannelRequest request);

    UpdateInventoryOnChannelResponse processChannelInventory(String channelCode, boolean processStockoutsOnly, boolean processPendingInventorySnapshots);

    void acknowledgePendingInventorySnapshots();

    void acknowledgeInventorySnapshot(InventorySnapshotDTO inventorySnapshot, Set<ChannelDetailDTO> channels, Date lastAcknowledgedTime,
            Map<Integer, Map<String, Long>> channelIdToUnprocessedOrderInventory);

    ChannelItemType markChannelItemTypeDirty(ChannelItemType channelItemType);

    UpdateInventoryOnChannelResponse updateAllInventoryOnChannel(UpdateInventoryOnChannelRequest request);

    GetChannelInventorySyncStatusResponse getChannelInventorySyncStatus(GetChannelInventorySyncStatusRequest request);

    CalculateChannelItemTypeInventoryResponse calculateChannelItemTypeInventory(ChannelDetailDTO channel, String formula, InventorySnapshotDTO inventorySnapshot,
            ItemType itemType, ChannelItemType channelItemType, Long unprocessedOrderInventory);

    GetChannelInventoryUpdateSnapshotResponse getInventoryUpdateSnapshotById(GetChannelInventoryUpdateSnapshotRequest request);

    void resetChannelInventorySyncStatusForAllTenants();

    SyncChannelItemTypeInventoryResponse syncChannelItemTypeInventory(SyncChannelItemTypeInventoryRequest request);

    Integer getAvailableBundleInventory(String skuCode);

    void resetChannelInventorySyncStatus();

    UpdateInventoryByChannelProductIdResponse updateInventoryByChannelProductId(UpdateInventoryByChannelProductIdRequest request);

    UpdateInventoryOnAllChannelsResponse updateInventoryOnAllChannels(UpdateInventoryOnAllChannelsRequest request);

    void create(ChannelInventoryFormulaVO channelInventoryFormula);

    ChannelInventoryFormulaVO getChannelInventoryFormulaByChecksum(String checksum);

    ChannelInventoryFormulaVO getChannelInventoryFormulaByCode(String code);

    InventorySnapshotDTO getItemTypeInventoryNotification(int itemTypeId, List<Integer> facilityIds);

    void doProcessChannelInventory(SyncChannelInventoryEvent syncChannelInventoryEvent);
}
