package com.uniware.services.pickup;

import com.uniware.core.api.o2o.GetAvailableFacilitiesRequest;
import com.uniware.core.api.o2o.GetAvailableFacilitiesResponse;
import com.uniware.core.api.o2o.GetFacilitiesSLARequest;
import com.uniware.core.api.o2o.GetFacilitiesSLAResponse;
import com.uniware.core.api.o2o.GetFacilityDetailsRequest;
import com.uniware.core.api.o2o.GetFacilityDetailsResponse;
import com.uniware.core.api.o2o.GetFacilitySLARequest;
import com.uniware.core.api.o2o.GetFacilitySLAResponse;

/**
 * Created by sunny on 20/05/15.
 */
public interface IStoreService {

    public GetAvailableFacilitiesResponse getAvailableFacilities(GetAvailableFacilitiesRequest request);

    GetFacilityDetailsResponse getFacilityDetails(GetFacilityDetailsRequest request);

    public GetFacilitiesSLAResponse getFacilitiesSLA(GetFacilitiesSLARequest request);

    GetFacilitySLAResponse getFacilitySLA(GetFacilitySLARequest request);
}
