package com.uniware.services.pickup.impl;

import com.unifier.core.jms.MessagingConstants;
import com.uniware.services.messaging.jms.ActiveMQConnector;
import com.uniware.services.pickup.AvailabilityChangeMessage;
import com.uniware.services.pickup.IStoreAvailabilityNotifyService;
import com.uniware.services.pickup.SellerRegistrationMessage;
import com.uniware.services.pickup.ServiceabilityChangeMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 * Created by piyush on 12/11/15.
 */
@Service("storeAvailabilityPublishService")
public class StoreAvailabilityNotifyImpl implements IStoreAvailabilityNotifyService {

    private static final Logger LOG = LoggerFactory.getLogger(StoreAvailabilityNotifyImpl.class);

    @Autowired
    @Qualifier("activeMQConnector1")
    private ActiveMQConnector activeMQConnector;

    @Override
    public void publishMessage(ServiceabilityChangeMessage message) {
        if (message != null) {
            LOG.info("Adding message {} to queue", message);
            activeMQConnector.produceMessage(MessagingConstants.Queue.SERVICABILITY_CHANGE, message);
        }
    }

    @Override
    public void publishMessage(AvailabilityChangeMessage message) {
        if (message != null) {
            LOG.info("Adding message {} to queue", message);
            activeMQConnector.produceMessage(MessagingConstants.Queue.AVAILABILITY_QUEUE, message);
        }
    }

    @Override
    public void publishMessage(SellerRegistrationMessage message) {
        if (message != null) {
            LOG.info("Adding message {} to queue", message);
            activeMQConnector.produceMessage(MessagingConstants.Queue.REGISTRATION_QUEUE, message);
        }
    }

}
