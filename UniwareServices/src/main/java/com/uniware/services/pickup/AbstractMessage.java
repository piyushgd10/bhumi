package com.uniware.services.pickup;

import java.io.Serializable;

/**
 * Created by piyush on 12/29/15.
 */
public class AbstractMessage implements Serializable {

    private String sellerCode;
    private String storeCode;

    public String getSellerCode() {
        return sellerCode;
    }

    public void setSellerCode(String sellerCode) {
        this.sellerCode = sellerCode;
    }

    public String getStoreCode() {
        return storeCode;
    }

    public void setStoreCode(String storeCode) {
        this.storeCode = storeCode;
    }

    @Override
    public String toString() {
        return "UnifierMessage{" + "sellerCode='" + sellerCode + '\'' + ", storeCode='" + storeCode + '\'' + '}';
    }
}
