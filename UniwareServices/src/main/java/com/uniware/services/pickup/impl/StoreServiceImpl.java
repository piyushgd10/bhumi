package com.uniware.services.pickup.impl;

import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.expressions.Expression;
import com.unifier.core.utils.CollectionUtils;
import com.unifier.core.utils.JibxUtils;
import com.unifier.core.utils.StringUtils;
import com.unifier.scraper.sl.runtime.ScraperScript;
import com.unifier.scraper.sl.runtime.ScriptExecutionContext;
import com.uniware.core.api.o2o.GetAvailableFacilitiesRequest;
import com.uniware.core.api.o2o.GetAvailableFacilitiesResponse;
import com.uniware.core.api.o2o.GetAvailableFacilitiesResponse.ShippingMethodDTO;
import com.uniware.core.api.o2o.GetFacilitiesSLARequest;
import com.uniware.core.api.o2o.GetFacilitiesSLAResponse;
import com.uniware.core.api.o2o.GetFacilitiesSLAResponse.ProductFacilitySLA;
import com.uniware.core.api.o2o.GetFacilitiesSLAResponse.ProductFacilitySLA.FacilitySLA;
import com.uniware.core.api.o2o.GetFacilityDetailsRequest;
import com.uniware.core.api.o2o.GetFacilityDetailsResponse;
import com.uniware.core.api.o2o.GetFacilityDetailsResponse.AddressDTO;
import com.uniware.core.api.o2o.GetFacilityDetailsResponse.DayTiming;
import com.uniware.core.api.o2o.GetFacilityDetailsResponse.DayTiming.DayOfWeek;
import com.uniware.core.api.o2o.GetFacilityDetailsResponse.FacilityDetailDTO;
import com.uniware.core.api.o2o.GetFacilityDetailsResponse.Location;
import com.uniware.core.api.o2o.GetFacilityDetailsResponse.StoreTiming;
import com.uniware.core.api.o2o.GetFacilitySLARequest;
import com.uniware.core.api.o2o.GetFacilitySLAResponse;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.cache.FacilityCache;
import com.uniware.core.entity.Facility;
import com.uniware.core.entity.FacilityProfile;
import com.uniware.core.entity.ItemType;
import com.uniware.core.entity.ItemTypeInventory;
import com.uniware.core.entity.ItemTypeInventorySnapshot;
import com.uniware.core.entity.PartyAddress;
import com.uniware.core.entity.PartyAddressType;
import com.uniware.core.entity.PartyContact;
import com.uniware.core.entity.PartyContactType;
import com.uniware.core.entity.ShippingMethod;
import com.uniware.core.utils.UserContext;
import com.uniware.dao.location.ILocationMao;
import com.uniware.services.cache.ScriptVersionedCache;
import com.uniware.services.cache.ServiceabilityCache;
import com.uniware.services.cache.ServiceabilityCache.PickupFacility;
import com.uniware.services.catalog.ICatalogService;
import com.uniware.services.configuration.ShippingConfiguration;
import com.uniware.services.configuration.SystemConfiguration;
import com.uniware.services.configuration.TenantSystemConfiguration;
import com.uniware.services.inventory.IInventoryService;
import com.uniware.services.pickup.IStoreService;
import com.uniware.services.shipping.IShippingProviderService;
import com.uniware.services.warehouse.IFacilityService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Service(value = "storeService")
public class StoreServiceImpl implements IStoreService {

    private static final Logger     LOG                                    = LoggerFactory.getLogger(StoreServiceImpl.class);

    private static final String     DEFAULT_FORMULA                        = "#{#inventorySnapshot.inventory - #inventorySnapshot.openSale - #inventorySnapshot.pendingInventoryAssessment}";

    private static final Expression CALCULATED_INVENTORY_EXPRESSION        = Expression.compile(DEFAULT_FORMULA);

    @Autowired
    private IFacilityService        facilityService;

    public enum Type {
        PICKUP,
        DELIVERY
    }

    public enum ShippingMethodName {
        STANDARD_COD("Standard-COD", Type.DELIVERY),
        STANDARD_PREPAID("Standard-Prepaid", Type.DELIVERY),
        PICKUP_COD("Pickup-COD", Type.PICKUP),
        PICKUP_PREPAID("Pickup-Prepaid", Type.PICKUP);
        private String name;
        private Type   type;

        ShippingMethodName(String name, Type type) {
            this.name = name;
            this.type = type;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Type getType() {
            return type;
        }

        public void setType(Type type) {
            this.type = type;
        }
    }

    @Autowired
    private ICatalogService          catalogService;

    @Autowired
    private IInventoryService        inventoryService;

    @Autowired
    private IShippingProviderService shippingProviderService;

    @Autowired
    private ILocationMao             locationMao;

    @Override
    public GetAvailableFacilitiesResponse getAvailableFacilities(GetAvailableFacilitiesRequest request) {
        GetAvailableFacilitiesResponse response = new GetAvailableFacilitiesResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            String fetchAvailableFacilitiesFromThirdPartyScript = ConfigurationManager.getInstance().getConfiguration(TenantSystemConfiguration.class).getAvailableFacilitiesFromThirdPartyScriptName();
            if (StringUtils.isNotBlank(fetchAvailableFacilitiesFromThirdPartyScript)) {
                ScraperScript fetchAvailableFacilitiesScript = CacheManager.getInstance().getCache(ScriptVersionedCache.class).getScriptByName(
                        fetchAvailableFacilitiesFromThirdPartyScript);
                if (fetchAvailableFacilitiesScript == null) {
                    response.setMessage("Fetch available facilities from third party not configured");
                    return response;
                } else {
                    try {
                        ScriptExecutionContext seContext = ScriptExecutionContext.current();
                        seContext.addVariable("request", request);
                        seContext.setTraceLoggingEnabled(UserContext.current().isTraceLoggingEnabled());
                        fetchAvailableFacilitiesScript.execute();
                        String fetchAvailableFacilitiesResponseXML = seContext.getScriptOutput();
                        if (LOG.isDebugEnabled()) {
                            LOG.debug("Fetch available facilities response Xml : {}", fetchAvailableFacilitiesResponseXML);
                        }
                        return JibxUtils.unmarshall("unicommerceO2OServicesBinding10", GetAvailableFacilitiesResponse.class, fetchAvailableFacilitiesResponseXML);
                    } catch (Exception e) {
                        LOG.error("Failed to fetch available facilities from third party", e);
                        response.setMessage(e.getMessage());
                        return response;
                    } finally {
                        ScriptExecutionContext.destroy();
                    }
                }
            } else {
                return getGetAvailableFacilitiesInternal(request, response, context);
            }
        }
        return response;
    }

    @Transactional(readOnly = true)
    private GetAvailableFacilitiesResponse getGetAvailableFacilitiesInternal(GetAvailableFacilitiesRequest request, GetAvailableFacilitiesResponse response,
            ValidationContext context) {
        long startTime = System.currentTimeMillis();
        if (request.getLocation() == null && StringUtils.isBlank(request.getPincode())) {
            context.addError(WsResponseCode.INVALID_API_REQUEST, "Either of location or pincode has to be specified");
        }
        List<String> invalidSKUs = new ArrayList<>();
        Map<String, ItemType> skuToItemType = new HashMap<>();
        for (GetAvailableFacilitiesRequest.WsProduct wsProduct : request.getProducts()) {
            ItemType itemType = catalogService.getItemTypeBySkuCode(wsProduct.getSellerSkuCode());
            if (itemType == null) {
                invalidSKUs.add(wsProduct.getSellerSkuCode());
            } else {
                skuToItemType.put(itemType.getSkuCode(), itemType);
            }
        }
        if (!invalidSKUs.isEmpty()) {
            context.addError(WsResponseCode.INVALID_ITEM_TYPE, "Invalid item types " + invalidSKUs.toString());
        }
        if (!context.hasErrors()) {
            List<GetAvailableFacilitiesResponse.ProductFacilityDTO> productFacilities = new ArrayList<>(request.getProducts().size());
            Set<String> requestedPickupMethods = new HashSet<>();
            Set<String> requestedDeliveryMethods = new HashSet<>();
            for (ShippingMethodName shippingMethodName : ShippingMethodName.values()) {
                if (request.getShippingMethods().isEmpty() || request.getShippingMethods().contains(shippingMethodName.getName())) {
                    if (Type.PICKUP.equals(shippingMethodName.getType())) {
                        requestedPickupMethods.add(shippingMethodName.getName());
                    } else if (Type.DELIVERY.equals(shippingMethodName.getType())) {
                        requestedDeliveryMethods.add(shippingMethodName.getName());
                    }
                }
            }

            Map<Integer, Set<ShippingMethodDTO>> facilityIdToShippingMethods = new HashMap<>();
            Map<Integer, PickupFacility> idToPickupFacility = new LinkedHashMap<>();
            double[] position = null;
            boolean searchByLocation = false;
            if (request.getLocation() != null) {
                position = new double[] { request.getLocation().getLatitude(), request.getLocation().getLongitude() };
                searchByLocation = true;
            }
            String pincode = request.getPincode();
            if (searchByLocation) {
                pincode = locationMao.getNearestPincode(position);
            }

            if (!requestedPickupMethods.isEmpty() && StringUtils.isNotBlank(pincode)) {
                List<PickupFacility> pickupServiceableFacilities = new ArrayList<>();
                ServiceabilityCache serviceabilityCache = CacheManager.getInstance().getCache(ServiceabilityCache.class);
                for (String method : requestedPickupMethods) {
                    if (searchByLocation) {
                        pickupServiceableFacilities.addAll(serviceabilityCache.getPickupFacilitiesWithinDistanceWithShippingMethod(position, request.getProximityInKms(), method));
                    } else {
                        pickupServiceableFacilities.addAll(serviceabilityCache.getPickupFacilitiesWithinDistanceWithShippingMethod(request.getPincode(),
                                request.getProximityInKms(), method));
                    }
                }
                // Checking the serviceability of available facilities
                Set<Integer> facilityIds = new HashSet<Integer>();
                for (ShippingMethodName shippingMethodName : ShippingMethodName.values()) {
                    if (requestedPickupMethods.contains(shippingMethodName.getName())) {
                        ShippingMethod shippingMethod = ConfigurationManager.getInstance().getConfiguration(ShippingConfiguration.class).getShippingMethod(
                                shippingMethodName.getName());
                        facilityIds = shippingProviderService.getServiceableByFacilities(shippingMethod.getId(), pincode);
                    }
                }
                for (PickupFacility pickupFacility : pickupServiceableFacilities) {
                    if(facilityIds.contains(pickupFacility.getId())){
                        idToPickupFacility.put(pickupFacility.getId(), pickupFacility);
                        for (ServiceabilityCache.ShippingMethodDTO s : pickupFacility.getShippingMethods()) {
                            if (requestedPickupMethods.contains(s.getShippingMethodName())) {
                                ShippingMethodDTO shippingMethodDTO = new ShippingMethodDTO(s.getShippingMethodCode(), s.getPaymentMethodCode(), s.getShippingMethodName());
                                addShippingMethodInFacility(pickupFacility.getId(), shippingMethodDTO, facilityIdToShippingMethods);
                            }
                        }
                    }
                }
            }
            if (!requestedDeliveryMethods.isEmpty() && StringUtils.isNotBlank(pincode)) {
                for (ShippingMethodName shippingMethodName : ShippingMethodName.values()) {
                    if (Type.DELIVERY.equals(shippingMethodName.getType()) && requestedDeliveryMethods.contains(shippingMethodName.getName())) {
                        ShippingMethod shippingMethod = ConfigurationManager.getInstance().getConfiguration(ShippingConfiguration.class).getShippingMethod(
                                shippingMethodName.getName());
                        Set<Integer> facilityIds = shippingProviderService.getServiceableByFacilities(shippingMethod.getId(), pincode);
                        for (Integer facilityId : facilityIds) {
                            ShippingMethodDTO shippingMethodDTO = new ShippingMethodDTO(shippingMethod.getCode(), shippingMethod.getPaymentMethod().getCode(),
                                    shippingMethod.getName());
                            addShippingMethodInFacility(facilityId, shippingMethodDTO, facilityIdToShippingMethods);
                        }
                    }
                }
            }

            for (GetAvailableFacilitiesRequest.WsProduct wsProduct : request.getProducts()) {
                ItemType itemType = skuToItemType.get(wsProduct.getSellerSkuCode());
                GetAvailableFacilitiesResponse.ProductFacilityDTO productFacilityDTO = new GetAvailableFacilitiesResponse.ProductFacilityDTO();
                List<GetAvailableFacilitiesResponse.FacilityDTO> pickupFacilities = new ArrayList<>(request.getNumberOfResults());
                productFacilityDTO.setSellerSkuCode(wsProduct.getSellerSkuCode());
                productFacilityDTO.setQuantity(wsProduct.getQuantity());
                Map<Integer, Integer> availableFacilitiesToInventory = new HashMap<>();
                if (!facilityIdToShippingMethods.isEmpty()) {
                    availableFacilitiesToInventory = getAvailableFacilitiesWithInventory(itemType, wsProduct.getQuantity(), facilityIdToShippingMethods.keySet());
                }
                Set<ShippingMethodDTO> allAvailableShippingMethods = new HashSet<>();
                Map<Integer, Integer> facilityIdToDispatchSLA = new HashMap<>();
                List<ItemTypeInventory> itemTypeInventories = new ArrayList<>();
                if (!availableFacilitiesToInventory.keySet().isEmpty()) {
                    itemTypeInventories = inventoryService.getItemTypeInventoryByItemType(itemType, new ArrayList<Integer>(availableFacilitiesToInventory.keySet()));
                }
                if (!itemTypeInventories.isEmpty()) {
                    for (ItemTypeInventory itemTypeInventory : itemTypeInventories) {
                        Integer facilityDispatchSLA = facilityIdToDispatchSLA.get(itemTypeInventory.getFacility().getId());
                        if (itemTypeInventory.getSla() != null && itemTypeInventory.getType().name().equals(ItemTypeInventory.Type.GOOD_INVENTORY.name())) {
                            facilityIdToDispatchSLA.put(itemTypeInventory.getFacility().getId(), itemTypeInventory.getSla());
                        } else if (itemTypeInventory.getSla() != null && facilityDispatchSLA == null
                                && itemTypeInventory.getType().name().equals(ItemTypeInventory.Type.VIRTUAL_INVENTORY.name())) {
                            facilityIdToDispatchSLA.put(itemTypeInventory.getFacility().getId(), itemTypeInventory.getSla());
                        }
                    }
                }
                for (Integer facilityId : availableFacilitiesToInventory.keySet()) {
                    Set<ShippingMethodDTO> shippingMethods = facilityIdToShippingMethods.get(facilityId);
                    allAvailableShippingMethods.addAll(shippingMethods);
                    Facility facility = CacheManager.getInstance().getCache(FacilityCache.class).getFacilityById(facilityId);
                    FacilityProfile facilityProfile = CacheManager.getInstance().getCache(FacilityCache.class).getFacilityProfileByCode(facility.getCode());
                    Integer dispatchSLA = null;
                    if (facilityIdToDispatchSLA.containsKey(facilityId)) {
                        dispatchSLA = facilityIdToDispatchSLA.get(facilityId);
                    } else {
                        dispatchSLA = facilityProfile.getDispatchSLA();
                    }
                    Integer deliverySLA = dispatchSLA + facilityProfile.getDeliverySLA();
                    if (productFacilityDTO.getDispatchSLA() == null || productFacilityDTO.getDispatchSLA() > dispatchSLA) {
                        productFacilityDTO.setDispatchSLA(dispatchSLA);
                    }
                    if (productFacilityDTO.getDeliverySLA() == null || productFacilityDTO.getDeliverySLA() > deliverySLA) {
                        productFacilityDTO.setDeliverySLA(deliverySLA);
                    }
                }
                for (Map.Entry<Integer, PickupFacility> e : idToPickupFacility.entrySet()) {
                    if (pickupFacilities.size() < request.getNumberOfResults()) {
                        if (availableFacilitiesToInventory.containsKey(e.getKey())) {
                            pickupFacilities.add(prepareFacilityDTO(e.getValue(), availableFacilitiesToInventory.get(e.getKey())));
                        }
                    } else {
                        break;
                    }
                }
                productFacilityDTO.setFacilities(pickupFacilities);
                productFacilityDTO.setShippingMethods(CollectionUtils.asList(allAvailableShippingMethods));
                productFacilities.add(productFacilityDTO);
            }
            if (!context.hasErrors()) {
                response.setProductFacilities(productFacilities);
            }
        }
        response.setSuccessful(!context.hasErrors());
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        LOG.info("Done executing get facility available in: {} ms or {} seconds.", System.currentTimeMillis() - startTime, (System.currentTimeMillis() - startTime) / 1000);
        return response;
    }

    private void addShippingMethodInFacility(Integer facilityId, ShippingMethodDTO shippingMethodDTO, Map<Integer, Set<ShippingMethodDTO>> facilityIdToShippingMethods) {
        Set<ShippingMethodDTO> shippingMethodDTOs = facilityIdToShippingMethods.get(facilityId);
        if (shippingMethodDTOs == null) {
            shippingMethodDTOs = new HashSet<>();
            facilityIdToShippingMethods.put(facilityId, shippingMethodDTOs);
        }
        shippingMethodDTOs.add(shippingMethodDTO);
    }

    private Map<Integer, Integer> getAvailableFacilitiesWithInventory(ItemType itemType, int quantity, Set<Integer> serviceableFacilities) {
        Map<Integer, Integer> availableFacilities = new HashMap<>(serviceableFacilities.size());
        List<ItemTypeInventorySnapshot> inventorySnapshots = inventoryService.getItemTypeInventorySnapshots(itemType.getId(), CollectionUtils.asList(serviceableFacilities));
        for (ItemTypeInventorySnapshot itis : inventorySnapshots) {
            Map<String, Object> contextParams = new HashMap<>();
            contextParams.put("itemType", itemType);
            contextParams.put("inventorySnapshot", itis);
            int calculatedInvetory = CALCULATED_INVENTORY_EXPRESSION.evaluate(contextParams, Integer.class);
            if (calculatedInvetory >= quantity) {
                availableFacilities.put(itis.getFacility().getId(), calculatedInvetory);
            }
        }
        return availableFacilities;
    }

    private GetAvailableFacilitiesResponse.FacilityDTO prepareFacilityDTO(ServiceabilityCache.PickupFacility pickupFacility, int calculatedInvetory) {
        GetAvailableFacilitiesResponse.FacilityDTO facilityDTO = new GetAvailableFacilitiesResponse.FacilityDTO();
        facilityDTO.setCode(pickupFacility.getCode());
        facilityDTO.setTitle(pickupFacility.getTitle());
        facilityDTO.setShortAddress(pickupFacility.getShortAddress());
        facilityDTO.setInventory(calculatedInvetory);
        facilityDTO.setDistance(pickupFacility.getDistance());
        facilityDTO.setLastUpdated(pickupFacility.getLastUpdated());
        facilityDTO.setLocation(new GetAvailableFacilitiesRequest.Location(pickupFacility.getPosition()[0], pickupFacility.getPosition()[1]));
        for (ServiceabilityCache.ShippingMethodDTO shippingMethod : pickupFacility.getShippingMethods()) {
            facilityDTO.getShippingMethods().add(
                    new ShippingMethodDTO(shippingMethod.getShippingMethodCode(), shippingMethod.getPaymentMethodCode(), shippingMethod.getShippingMethodName()));
        }
        return facilityDTO;
    }

    @Override
    @Transactional
    public GetFacilityDetailsResponse getFacilityDetails(GetFacilityDetailsRequest request) {
        GetFacilityDetailsResponse response = new GetFacilityDetailsResponse();
        ValidationContext context = request.validate();
        List<FacilityDetailDTO> facilities = new ArrayList<>();
        if (!context.hasErrors()) {
            for (String facilityCode : request.getFacilityCodes()) {
                Facility facility = facilityService.getAllFacilityByCode(facilityCode);
                if (facility == null) {
                    context.addError(WsResponseCode.INVALID_FACILITY_CODE, "Invalid facility code " + facilityCode);
                } else {
                    prepareFacilityDetailDTO(facility, facilities);
                }
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        } else {
            response.setFacilities(facilities);
        }
        response.setSuccessful(!context.hasErrors());
        return response;
    }

    private void prepareFacilityDetailDTO(Facility facility, List<FacilityDetailDTO> facilities) {
        FacilityDetailDTO facilityDetailDTO = new FacilityDetailDTO();
        FacilityProfile facilityProfile = CacheManager.getInstance().getCache(FacilityCache.class).getFacilityProfileByCode(facility.getCode());
        facilityDetailDTO.setCode(facility.getCode());
        facilityDetailDTO.setType(Facility.Type.valueOf(facility.getType()));
        facilityDetailDTO.setTitle(facility.getDisplayName());
        if (facilityProfile != null) {
            if (facilityProfile.getStoreTimings() != null) {
                StoreTiming storeTiming = new StoreTiming();
                List<DayTiming> dayTimings = new ArrayList<>();
                for (com.uniware.core.entity.FacilityProfile.DayTiming day : facilityProfile.getStoreTimings().getDayTimings()) {
                    DayTiming dayTiming = new DayTiming();
                    dayTiming.setOpeningTime(day.getOpeningTime());
                    dayTiming.setClosingTime(day.getClosingTime());
                    dayTiming.setDayOfWeek(DayOfWeek.valueOf(day.getDayOfWeek().name()));
                    dayTiming.setClosed(day.isClosed());
                    dayTimings.add(dayTiming);
                }
                storeTiming.setDayTimings(dayTimings);
                facilityDetailDTO.setStoreTiming(storeTiming);
            }
            if (facilityProfile.getPosition() != null) {
                Location location = new Location(facilityProfile.getPosition()[0], facilityProfile.getPosition()[1]);
                facilityDetailDTO.setLocation(location);
            }
            facilityDetailDTO.setEmail(facilityProfile.getContactEmail());
        }
        PartyAddress shippingAddress = facility.getPartyAddressByType(PartyAddressType.Code.SHIPPING.name());
        if (shippingAddress != null) {
            AddressDTO addressDTO = new AddressDTO(shippingAddress);
            facilityDetailDTO.setAddress(addressDTO);
            facilityDetailDTO.setShortAddress(shippingAddress.getAddressLine1());
            PartyContact partyContact = facility.getPartyContactByType(PartyContactType.Code.PRIMARY.name());
            if (partyContact != null) {
                facilityDetailDTO.setEmail(partyContact.getEmail());
            }
        }
        facilities.add(facilityDetailDTO);
    }

    @Override
    public GetFacilitiesSLAResponse getFacilitiesSLA(GetFacilitiesSLARequest request) {
        GetFacilitiesSLAResponse response = new GetFacilitiesSLAResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            List<String> invalidSKUs = new ArrayList<>();
            Map<String, ItemType> skuToItemType = new HashMap<>();
            for (com.uniware.core.api.o2o.GetFacilitiesSLARequest.WsProduct wsProduct : request.getProducts()) {
                ItemType itemType = catalogService.getItemTypeBySkuCode(wsProduct.getSellerSkuCode());
                if (itemType == null) {
                    invalidSKUs.add(wsProduct.getSellerSkuCode());
                } else {
                    skuToItemType.put(itemType.getSkuCode(), itemType);
                }
            }
            if (!invalidSKUs.isEmpty()) {
                context.addError(WsResponseCode.INVALID_ITEM_TYPE, "Invalid item types " + invalidSKUs.toString());
            }
            if (!context.hasErrors()) {
                List<ProductFacilitySLA> productFacilitySLAs = new ArrayList<>();
                for (com.uniware.core.api.o2o.GetFacilitiesSLARequest.WsProduct wsProduct : request.getProducts()) {
                    if (!context.hasErrors()) {
                        Map<Integer, FacilityProfile> facilityIdToFacilityProfile = new HashMap<>();
                        for (String facilityCode : wsProduct.getFacilityCodes()) {
                            Facility facility = CacheManager.getInstance().getCache(FacilityCache.class).getFacilityByCode(facilityCode);
                            if (facility == null) {
                                context.addError(WsResponseCode.INVALID_FACILITY_CODE, facilityCode);
                            } else {
                                FacilityProfile facilityProfile = CacheManager.getInstance().getCache(FacilityCache.class).getFacilityProfileByCode(facility.getCode());
                                if (facilityProfile.isStorePickupEnabled() || facilityProfile.isStoreDeliveryEnabled()) {
                                    facilityIdToFacilityProfile.put(facility.getId(), facilityProfile);
                                }
                            }
                        }
                        if (!context.hasErrors()) {
                            ProductFacilitySLA productFacilitySLA = new ProductFacilitySLA();
                            productFacilitySLA.setSellerSkuCode(wsProduct.getSellerSkuCode());
                            Map<Integer, Integer> facilityIdToDispatchSLA = new HashMap<>();
                            ItemType itemType = skuToItemType.get(wsProduct.getSellerSkuCode());
                            List<ItemTypeInventory> itemTypeInventories = inventoryService.getItemTypeInventoryByItemType(itemType, new ArrayList<Integer>(
                                    facilityIdToFacilityProfile.keySet()));
                            if (!itemTypeInventories.isEmpty()) {
                                // Virtual inventory SLA should not reflect in case good inventory is available and sla is undefined.
                                Map<Integer,Boolean> facilityIdToGoodInventoryAvailable  = new HashMap<>();
                                for (ItemTypeInventory itemTypeInventory : itemTypeInventories) {
                                    Integer facilityDispatchSLA = facilityIdToDispatchSLA.get(itemTypeInventory.getFacility().getId());
                                    if (itemTypeInventory.getQuantity() > 0 && itemTypeInventory.getType().name().equals(ItemTypeInventory.Type.GOOD_INVENTORY.name())) {
                                        if(facilityIdToGoodInventoryAvailable.get(itemTypeInventory.getFacility().getId()) != null && !facilityIdToGoodInventoryAvailable.get(itemTypeInventory.getFacility().getId())){
                                            facilityIdToDispatchSLA.remove(itemTypeInventory.getFacility().getId());
                                        }
                                        facilityIdToGoodInventoryAvailable.put(itemTypeInventory.getFacility().getId(), true);
                                        if(itemTypeInventory.getSla() != null){
                                            facilityIdToDispatchSLA.put(itemTypeInventory.getFacility().getId(), itemTypeInventory.getSla());
                                        }
                                    } else if (itemTypeInventory.getSla() != null && facilityDispatchSLA == null &&
                                            (facilityIdToGoodInventoryAvailable.get(itemTypeInventory.getFacility().getId()) == null || (facilityIdToGoodInventoryAvailable.get(itemTypeInventory.getFacility().getId()) != null && !facilityIdToGoodInventoryAvailable.get(itemTypeInventory.getFacility().getId())))
                                            && itemTypeInventory.getType().name().equals(ItemTypeInventory.Type.VIRTUAL_INVENTORY.name())) {
                                        facilityIdToGoodInventoryAvailable.put(itemTypeInventory.getFacility().getId(), false);
                                        facilityIdToDispatchSLA.put(itemTypeInventory.getFacility().getId(), itemTypeInventory.getSla());
                                    }
                                }
                            }
                            List<FacilitySLA> facilitySLAs = new ArrayList<>();
                            for (Map.Entry<Integer, FacilityProfile> facility : facilityIdToFacilityProfile.entrySet()) {
                                FacilityProfile facilityProfile = facility.getValue();
                                FacilitySLA facilitySLA = new FacilitySLA();
                                facilitySLA.setCode(facilityProfile.getFacilityCode());
                                Integer dispatchSLA = facilityIdToDispatchSLA.get(facility.getKey());
                                if (dispatchSLA == null) {
                                    dispatchSLA = facilityProfile.getDispatchSLA();
                                }
                                facilitySLA.setDispatchSLA(dispatchSLA);
                                if (facilityProfile.isStoreDeliveryEnabled()) {
                                    Integer deliverySLA = facilityProfile.getDeliverySLA() + dispatchSLA;
                                    facilitySLA.setDeliverySLA(deliverySLA);
                                }
                                facilitySLAs.add(facilitySLA);
                            }
                            productFacilitySLA.setFacilitySLAs(facilitySLAs);
                            productFacilitySLAs.add(productFacilitySLA);
                        }
                    } else {
                        break;
                    }
                }
                if (!context.hasErrors()) {
                    response.setProductFacilitySLAs(productFacilitySLAs);
                }
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        response.setSuccessful(!context.hasErrors());
        return response;
    }

    @Override
    @Transactional
    public GetFacilitySLAResponse getFacilitySLA(GetFacilitySLARequest request) {
        GetFacilitySLAResponse response = new GetFacilitySLAResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            FacilityCache facilityCache = CacheManager.getInstance().getCache(FacilityCache.class);
            Facility facility = facilityCache.getFacilityById(request.getFacilityId());
            if (facility == null) {
                context.addError(WsResponseCode.INVALID_FACILITY_CODE, "Invalid facility");
            } else {
                FacilityProfile facilityProfile = facilityCache.getFacilityProfileByCode(facility.getCode());
                response.setDispatchSLA(facilityProfile.getDispatchSLA());
                Integer deliverySLA = facilityProfile.getDispatchSLA() + facilityProfile.getDeliverySLA();
                response.setDeliverySLA(deliverySLA);
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        response.setSuccessful(!context.hasErrors());
        return response;
    }
}
