/*
 * Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 12/17/15 12:07 PM
 * @author harshpal
 */

package com.uniware.services.pickup;

import java.util.Set;

/**
 * Created by harshpal on 12/17/15.
 */
public class AvailabilityChangeMessage extends AbstractMessage{

    private Set<AvailabilityDetails> supcs;

    public Set<AvailabilityDetails> getSupcs() {
        return supcs;
    }

    public void setSupcs(Set<AvailabilityDetails> supcs) {
        this.supcs = supcs;
    }

    public static class AvailabilityDetails {

        private String  supc;
        private boolean available;
        private int     dispatchSla;
        private int deliverySla;

        public int getDeliverySla() {
            return deliverySla;
        }

        public void setDeliverySla(int deliverySla) {
            this.deliverySla = deliverySla;
        }

        public String getSupc() {
            return supc;
        }

        public void setSupc(String supc) {
            this.supc = supc;
        }

        public boolean isAvailable() {
            return available;
        }

        public void setAvailable(boolean available) {
            this.available = available;
        }

        public int getDispatchSla() {
            return dispatchSla;
        }

        public void setDispatchSla(int dispatchSla) {
            this.dispatchSla = dispatchSla;
        }
    }
}
