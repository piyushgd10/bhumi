/*
 * Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 12/24/15 11:49 AM
 * @author harshpal
 */

package com.uniware.services.pickup;

import java.util.Set;

public class ServiceabilityChangeMessage extends AbstractMessage {

    private Set<PincodeServiceabilityMethod> pincodes;

    public Set<PincodeServiceabilityMethod> getPincodes() {
        return pincodes;
    }

    public void setPincodes(Set<PincodeServiceabilityMethod> pincodes) {
        this.pincodes = pincodes;
    }

    public static class PincodeServiceabilityMethod {

        private static final int STD_COD        = 0;
        private static final int STD_PREPAID    = 1;
        private static final int PICKUP_COD     = 2;
        private static final int PICKUP_PREPAID = 3;

        private String pincode;
        private Boolean[] serviceableMethods;

        public String getPincode() {
            return pincode;
        }

        public void setPincode(String pincode) {
            this.pincode = pincode;
        }

        public Boolean[] getServiceableMethods() {
            return serviceableMethods;
        }

        public void setServiceableMethods(Boolean[] serviceableMethods) {
            this.serviceableMethods = serviceableMethods;
        }

        public Boolean isStdCODAvailable(){
            return this.serviceableMethods[STD_COD];
        }

        public void setStdCodAvailable(Boolean available){
            this.serviceableMethods[STD_COD] = available ;
        }

        public Boolean isStdPrepaidAvailable(){
            return this.serviceableMethods[STD_PREPAID];
        }

        public void setStdPrepaidAvailable(Boolean available){
            this.serviceableMethods[STD_PREPAID] = available;
        }

        public Boolean isPickupCodAvailable(){
            return this.serviceableMethods[PICKUP_COD];
        }

        public void setPickupCodAvailable(Boolean available){
            this.serviceableMethods[PICKUP_COD] = available;
        }

        public Boolean isPickupPrepaidAvailable(){
            return this.serviceableMethods[PICKUP_PREPAID];
        }

        public void setPickupPrepaidAvailable(Boolean available){
            this.serviceableMethods[PICKUP_PREPAID] = available;
        }
    }
}
