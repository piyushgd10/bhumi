/*
 * Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 12/24/15 11:50 AM
 * @author harshpal
 */

package com.uniware.services.pickup;

import com.unifier.core.utils.GeolocationUtils.Coordinate;

public class SellerRegistrationMessage extends AbstractMessage {

    private String endpoint;
    private Coordinate coordinate;

    public String getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    public Coordinate getCoordinate() {
        return coordinate;
    }

    public void setCoordinate(Coordinate coordinate) {
        this.coordinate = coordinate;
    }
}
