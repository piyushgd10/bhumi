package com.uniware.services.pickup;

/**
 * Created by piyush on 12/11/15.
 */
public interface IStoreAvailabilityNotifyService {

    void publishMessage(ServiceabilityChangeMessage message);

    void publishMessage(AvailabilityChangeMessage message);

    void publishMessage(SellerRegistrationMessage message);
}
