/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 15-May-2012
 *  @author vibhu
 */
package com.uniware.services.item;

import com.uniware.core.api.item.GetAllFacilityItemDetailRequest;
import com.uniware.core.api.item.GetAllFacilityItemDetailResponse;
import com.uniware.core.api.item.GetItemDetailRequest;
import com.uniware.core.api.item.GetItemDetailResponse;
import com.uniware.core.entity.Item;
import com.uniware.core.entity.ItemType;

public interface IItemService {

    GetItemDetailResponse getItemDetail(GetItemDetailRequest request);

    GetAllFacilityItemDetailResponse getItemDetailAcrossFacility(GetAllFacilityItemDetailRequest request);

    boolean isItemAboutToExpireForThisTolerance(Item item, ItemType itemType, int tolerancePercentage);
}
