/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 *  @version     1.0, 15-May-2012
 *  @author vibhu
 */
package com.uniware.services.item.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.JsonUtils;
import com.unifier.services.utils.CustomFieldUtils;
import com.uniware.core.api.item.GetAllFacilityItemDetailRequest;
import com.uniware.core.api.item.GetAllFacilityItemDetailResponse;
import com.uniware.core.api.item.GetAllFacilityItemDetailResponse.GatePassDTO;
import com.uniware.core.api.item.GetItemDetailRequest;
import com.uniware.core.api.item.GetItemDetailResponse;
import com.uniware.core.api.item.GetItemDetailResponse.ItemDTO;
import com.uniware.core.api.item.GetItemDetailResponse.SaleOrderItemDTO;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.entity.Item;
import com.uniware.core.entity.ItemType;
import com.uniware.core.entity.OutboundGatePass;
import com.uniware.core.entity.Picklist;
import com.uniware.core.entity.SaleOrder;
import com.uniware.core.entity.SaleOrderItem;
import com.uniware.dao.inventory.IInventoryDao;
import com.uniware.dao.material.IMaterialDao;
import com.uniware.services.catalog.ICatalogService;
import com.uniware.services.item.IItemService;
import com.uniware.services.picker.IPickerService;
import com.uniware.services.saleorder.ISaleOrderService;

@Service
public class ItemServiceImpl implements IItemService {

    @Autowired
    IInventoryDao             inventoryDao;

    @Autowired
    private ICatalogService   catalogService;

    @Autowired
    private ISaleOrderService saleOrderService;

    @Autowired
    private IPickerService    pickerService;

    @Autowired
    private IMaterialDao      materialDao;

    @Override
    @Transactional
    public GetItemDetailResponse getItemDetail(GetItemDetailRequest request) {
        GetItemDetailResponse response = new GetItemDetailResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            Item item = inventoryDao.getItemByCode(request.getItemCode());
            if (item == null) {
                context.addError(WsResponseCode.INVALID_ITEM_CODE, "Invalid Item Code");
            } else {
                ItemDTO itemDTO = new ItemDTO();
                itemDTO.setCode(item.getCode());
                itemDTO.setStatus(item.getStatusCode());
                itemDTO.setInventoryType(item.getInventoryType().name());
                itemDTO.setCreated(item.getCreated());
                itemDTO.setRejectionReason(item.getRejectionReason());
                if (item.getInflowReceiptItem() != null) {
                    itemDTO.setInflowReceiptCode(item.getInflowReceiptItem().getInflowReceipt().getCode());
                    itemDTO.setPurchaseOrderCode(item.getInflowReceiptItem().getPurchaseOrderItem().getPurchaseOrder().getCode());
                    itemDTO.setUnitPrice(item.getInflowReceiptItem().getPurchaseOrderItem().getUnitPrice());
                    itemDTO.setCreatedFacilityCode(item.getInflowReceiptItem().getPurchaseOrderItem().getPurchaseOrder().getFacility().getCode());
                }

                itemDTO.setVendorSkuCode(item.getVendorItemCode());
                if (item.getVendor() != null) {
                    itemDTO.setVendor(item.getVendor().getName());
                    itemDTO.setVendorCode(item.getVendor().getCode());
                }
                ItemType itemType = catalogService.getNonBundledItemTypeById(item.getItemType().getId());
                itemDTO.setItemSKU(itemType.getSkuCode());
                itemDTO.setMaxRetailPrice(itemType.getMaxRetailPrice());
                itemDTO.setColor(itemType.getColor());
                itemDTO.setSize(itemType.getSize());
                itemDTO.setBrand(itemType.getBrand());
                itemDTO.setItemTypeName(itemType.getName());
                itemDTO.setItemTypeImageUrl(itemType.getImageUrl());
                itemDTO.setItemTypePageUrl(itemType.getProductPageUrl());
                itemDTO.setItemTypeCustomFieldValues(CustomFieldUtils.getCustomFieldValuesDTO(itemType));
                itemDTO.setCurrentFacilityCode(item.getFacility().getCode());
                itemDTO.setUpdated(item.getUpdated());
                itemDTO.setAgeingStartDate(item.getAgeingStartDate());
                if (item.getPutaway() != null) {
                    itemDTO.setLastPutawayCode(item.getPutaway().getCode());
                }
                List<SaleOrderItem> saleOrderItems = saleOrderService.getSaleOrderItemsByItemId(item.getId());
                SaleOrder latestOrder = null;
                for (int i = 0; i < saleOrderItems.size(); i++) {
                    SaleOrderItem saleOrderItem = saleOrderItems.get(i);
                    SaleOrderItemDTO saleOrderItemDTO = new SaleOrderItemDTO();
                    saleOrderItemDTO.setId(saleOrderItem.getId());
                    saleOrderItemDTO.setCreated(saleOrderItem.getCreated());
                    saleOrderItemDTO.setSaleOrderCode(saleOrderItem.getSaleOrder().getCode());
                    saleOrderItemDTO.setItemName(saleOrderItem.getItemType().getName());
                    saleOrderItemDTO.setItemSku(saleOrderItem.getItemType().getSkuCode());
                    saleOrderItemDTO.setImageUrl(saleOrderItem.getItemType().getImageUrl());
                    saleOrderItemDTO.setPageUrl(saleOrderItem.getItemType().getProductPageUrl());
                    saleOrderItemDTO.setSaleOrderItemCode(saleOrderItem.getCode());
                    saleOrderItemDTO.setSaleOrderItemStatus(saleOrderItem.getStatusCode());
                    if (saleOrderItem.getShippingPackage() != null) {
                        saleOrderItemDTO.setShippingPackageCode(saleOrderItem.getShippingPackage().getCode());
                        saleOrderItemDTO.setShippingPackageStatus(saleOrderItem.getShippingPackage().getStatusCode());

                        Picklist picklist = pickerService.getPicklistForShippingPackage(saleOrderItem.getShippingPackage().getCode());
                        if (picklist != null) {
                            saleOrderItemDTO.setPicklistNumber(picklist.getCode());
                        }
                    }
                    itemDTO.addSaleOrderItemDTO(saleOrderItemDTO);
                    if (latestOrder == null || latestOrder.getCreated().before(saleOrderItem.getSaleOrder().getCreated())) {
                        latestOrder = saleOrderItem.getSaleOrder();
                    }
                }

                List<OutboundGatePass> outboundGatePasses = materialDao.getOutboundGatepassByItemId(item.getId());
                for (OutboundGatePass outboundGatePass : outboundGatePasses) {
                    com.uniware.core.api.item.GetItemDetailResponse.GatePassDTO gatePassDTO = new com.uniware.core.api.item.GetItemDetailResponse.GatePassDTO();
                    gatePassDTO.setCode(outboundGatePass.getCode());
                    gatePassDTO.setCreated(outboundGatePass.getCreated());
                    gatePassDTO.setPurpose(outboundGatePass.getPurpose());
                    gatePassDTO.setReference(outboundGatePass.getReferenceNumber());
                    gatePassDTO.setStatusCode(outboundGatePass.getStatusCode());
                    gatePassDTO.setType(outboundGatePass.getType());
                    itemDTO.addGatePassDTO(gatePassDTO);
                }

                if (latestOrder != null) {
                    itemDTO.setSaleOrderCustomFieldValues(CustomFieldUtils.getCustomFieldValuesDTO(latestOrder));
                }
                @SuppressWarnings("unchecked")
                HashMap<String, Object> itemCustomFields = JsonUtils.stringToJson(item.getItemDetails(), HashMap.class);
                itemDTO.setItemCustomFields(itemCustomFields);
                response.setItemDTO(itemDTO);
                response.setSuccessful(true);
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    public GetAllFacilityItemDetailResponse getItemDetailAcrossFacility(GetAllFacilityItemDetailRequest request) {
        GetAllFacilityItemDetailResponse response = new GetAllFacilityItemDetailResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            Item item = inventoryDao.getItemByCodeAcrossFacility(request.getItemCode());
            if (item == null) {
                context.addError(WsResponseCode.INVALID_ITEM_CODE, "Invalid Item Code");
            } else {
                com.uniware.core.api.item.GetAllFacilityItemDetailResponse.ItemDTO itemDTO = new com.uniware.core.api.item.GetAllFacilityItemDetailResponse.ItemDTO();
                itemDTO.setCode(item.getCode());
                itemDTO.setStatus(item.getStatusCode());
                itemDTO.setCreated(item.getCreated());
                if (item.getInflowReceiptItem() != null) {
                    itemDTO.setInflowReceiptCode(item.getInflowReceiptItem().getInflowReceipt().getCode());
                    itemDTO.setUnitPrice(item.getInflowReceiptItem().getPurchaseOrderItem().getUnitPrice());
                }

                itemDTO.setVendorSkuCode(item.getVendorItemCode());
                if (item.getVendor() != null) {
                    itemDTO.setVendor(item.getVendor().getName());
                }
                ItemType itemType = catalogService.getNonBundledItemTypeById(item.getItemType().getId());

                itemDTO.setItemSKU(itemType.getSkuCode());
                itemDTO.setMaxRetailPrice(itemType.getMaxRetailPrice());
                itemDTO.setColor(itemType.getColor());
                itemDTO.setSize(itemType.getSize());
                itemDTO.setBrand(itemType.getBrand());
                itemDTO.setItemTypeName(itemType.getName());
                itemDTO.setItemTypeImageUrl(itemType.getImageUrl());
                itemDTO.setItemTypePageUrl(itemType.getProductPageUrl());
                itemDTO.setItemTypeCustomFieldValues(CustomFieldUtils.getCustomFieldValuesDTO(itemType));

                if (item.getPutaway() != null) {
                    itemDTO.setLastPutawayCode(item.getPutaway().getCode());
                }
                List<SaleOrderItem> saleOrderItems = saleOrderService.getSaleOrderItemsByItemIdAcrossFacility(item.getId());
                SaleOrder latestOrder = null;
                for (int i = 0; i < saleOrderItems.size(); i++) {
                    SaleOrderItem saleOrderItem = saleOrderItems.get(i);
                    com.uniware.core.api.item.GetAllFacilityItemDetailResponse.SaleOrderItemDTO saleOrderItemDTO = new com.uniware.core.api.item.GetAllFacilityItemDetailResponse.SaleOrderItemDTO();
                    saleOrderItemDTO.setId(saleOrderItem.getId());
                    saleOrderItemDTO.setCreated(saleOrderItem.getCreated());
                    saleOrderItemDTO.setSaleOrderCode(saleOrderItem.getSaleOrder().getCode());
                    saleOrderItemDTO.setItemName(saleOrderItem.getItemType().getName());
                    saleOrderItemDTO.setItemSku(saleOrderItem.getItemType().getSkuCode());
                    saleOrderItemDTO.setSaleOrderItemCode(saleOrderItem.getCode());
                    saleOrderItemDTO.setSaleOrderItemStatus(saleOrderItem.getStatusCode());
                    saleOrderItemDTO.setFacilityName(saleOrderItem.getFacility().getName());
                    if (saleOrderItem.getShippingPackage() != null) {
                        saleOrderItemDTO.setShippingPackageCode(saleOrderItem.getShippingPackage().getCode());
                        saleOrderItemDTO.setShippingPackageStatus(saleOrderItem.getShippingPackage().getStatusCode());

                        Picklist picklist = pickerService.getPicklistForShippingPackage(saleOrderItem.getShippingPackage().getCode());
                        if (picklist != null) {
                            saleOrderItemDTO.setPicklistNumber(picklist.getCode());
                        }
                    }
                    itemDTO.addSaleOrderItemDTO(saleOrderItemDTO);
                    if (latestOrder == null || latestOrder.getCreated().before(saleOrderItem.getSaleOrder().getCreated())) {
                        latestOrder = saleOrderItem.getSaleOrder();
                    }
                }

                List<OutboundGatePass> outboundGatePasses = materialDao.getOutboundGatepassAcrossFacilityByItemId(item.getId());
                for (OutboundGatePass outboundGatePass : outboundGatePasses) {
                    GatePassDTO gatePassDTO = new GatePassDTO();
                    gatePassDTO.setCode(outboundGatePass.getCode());
                    gatePassDTO.setCreated(outboundGatePass.getCreated());
                    gatePassDTO.setPurpose(outboundGatePass.getPurpose());
                    gatePassDTO.setReference(outboundGatePass.getReferenceNumber());
                    gatePassDTO.setStatusCode(outboundGatePass.getStatusCode());
                    gatePassDTO.setType(outboundGatePass.getType());
                    gatePassDTO.setFacilityName(outboundGatePass.getFacility().getName());
                    itemDTO.addGatePassDTO(gatePassDTO);
                }
                if (latestOrder != null) {
                    itemDTO.setSaleOrderCustomFieldValues(CustomFieldUtils.getCustomFieldValuesDTO(latestOrder));
                }
                @SuppressWarnings("unchecked")
                HashMap<String, Object> itemCustomFields = JsonUtils.stringToJson(item.getItemDetails(), HashMap.class);
                itemDTO.setItemCustomFields(itemCustomFields);
                response.setItemDTO(itemDTO);
                response.setSuccessful(true);
            }

        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional(readOnly=true)
    public boolean isItemAboutToExpireForThisTolerance(Item item, ItemType itemType, int tolerancePercentage) {
        if (!catalogService.isItemTypeExpirable(itemType)) {
            return false;
        }
        int shelfLife = catalogService.getShelfLifeForItemType(itemType);
        int toleranceDays = (shelfLife * tolerancePercentage) / 100;
        Date manufacturingDate = item.getManufacturingDate();
        return manufacturingDate != null && !(DateUtils.addDaysToDate(manufacturingDate, shelfLife - toleranceDays).after(DateUtils.getCurrentTime()));
    }
}