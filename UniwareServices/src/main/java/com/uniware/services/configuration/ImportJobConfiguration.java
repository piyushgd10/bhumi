/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 21, 2012
 *  @author praveeng
 */

package com.uniware.services.configuration;

import com.unifier.core.annotation.Configuration;
import com.unifier.core.annotation.Level;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.configuration.IConfiguration;
import com.unifier.core.entity.ImportJobType;
import com.unifier.core.expressions.Expression;
import com.unifier.core.utils.StringUtils;
import com.unifier.core.utils.XMLParser;
import com.unifier.core.utils.XMLParser.Element;
import com.unifier.services.imports.IImportService;
import com.unifier.services.imports.ImportJobHandler;
import com.unifier.services.imports.ImportJobTypeColumnDTO;
import com.unifier.services.imports.ImportJobTypeDTO;
import com.uniware.services.configuration.CustomFieldsMetadataConfiguration.CustomFieldMetadataVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author praveeng
 */
@Configuration(name = "importJobConfiguration", level = Level.TENANT)
public class ImportJobConfiguration implements IConfiguration {

    private static final Logger              LOG            = LoggerFactory.getLogger(ImportJobConfiguration.class);
    private final        List<ImportJobType> importJobTypes = new ArrayList<ImportJobType>();

    private final Map<String, ImportJobType> nameToImportJobTypes = new ConcurrentHashMap<>();

    private final Map<String, ImportJobTypeDTO>  nameToImportJobTypeConfigs = new ConcurrentHashMap<>();
    private final Map<Integer, ImportJobTypeDTO> idToImportJobTypeConfigs   = new ConcurrentHashMap<>();

    @Autowired
    private transient IImportService importService;

    private void addImportJobType(ImportJobType jobType, ImportJobHandler importJobHandler) {
        importJobTypes.add(jobType);
        nameToImportJobTypes.put(jobType.getName(), jobType);
        ImportJobTypeDTO importJobTypeDTO = parseImportJobConfig(jobType);
        importJobTypeDTO.setImportJobHandler(importJobHandler);
        idToImportJobTypeConfigs.put(jobType.getId(), importJobTypeDTO);
        nameToImportJobTypeConfigs.put(jobType.getName(), importJobTypeDTO);
    }

    private ImportJobTypeDTO parseImportJobConfig(ImportJobType jobType) {
        ImportJobTypeDTO importJobTypeDTO = new ImportJobTypeDTO();
        for (String importOption : Arrays.asList(jobType.getImportOptions().split(","))) {
            importJobTypeDTO.getImportOptions().add(ImportJobType.ImportOptions.valueOf(importOption));
        }
        Element importConfigRoot = XMLParser.parse(jobType.getImportJobConfig());
        if (StringUtils.isNotBlank(importConfigRoot.attribute("delimiter"))) {
            importJobTypeDTO.setDelimiter(importConfigRoot.attribute("delimiter").charAt(0));
        }
        Element eColumns = importConfigRoot.get("columns");
        for (Element eColumn : eColumns.list("column")) {
            String name = eColumn.attribute("name");
            String source = eColumn.attribute("source");
            boolean required = StringUtils.parseBoolean(eColumn.attribute("required"));
            boolean requiredInUpdate = StringUtils.parseBoolean(eColumn.attribute("requiredInUpdate"));
            boolean groupBy = StringUtils.parseBoolean(eColumn.attribute("groupBy"));
            String description = eColumn.attribute("description");
            Expression valueExpression = StringUtils.isNotBlank(eColumn.attribute("valueExpression")) ? Expression.compile(eColumn.attribute("valueExpression")) : null;
            if (StringUtils.isBlank(source) && valueExpression == null) {
                throw new IllegalArgumentException("either of source or valueExpression is mandatory");
            } else {
                importJobTypeDTO.addColumn(new ImportJobTypeColumnDTO(name, source, required, requiredInUpdate, description, valueExpression, groupBy));
            }
        }
        CustomFieldsMetadataConfiguration configuration = ConfigurationManager.getInstance().getConfiguration(CustomFieldsMetadataConfiguration.class);
        for (Element eCustomFields : eColumns.list("custom-fields")) {
            List<CustomFieldMetadataVO> customFields = configuration.getCustomFieldsByEntity(eCustomFields.attribute("entity"));
            if (customFields != null) {
                for (CustomFieldMetadataVO customField : customFields) {
                    ImportJobTypeColumnDTO importJobTypeColumnDTO = new ImportJobTypeColumnDTO();
                    importJobTypeColumnDTO.setDescription(customField.getDisplayName());
                    importJobTypeColumnDTO.setGroupBy(false);
                    importJobTypeColumnDTO.setName(customField.getDisplayName());
                    importJobTypeColumnDTO.setSource(customField.getDisplayName());
                    importJobTypeColumnDTO.setRequired(customField.isRequired());
                    importJobTypeColumnDTO.setRequiredInUpdate(false);
                    importJobTypeDTO.addColumn(importJobTypeColumnDTO);
                }
            }
        }
        Element eFilters = importConfigRoot.get("filters");
        if (eFilters != null) {
            for (Element eFilter : eFilters.list("filter")) {
                importJobTypeDTO.addFilterExpression(Expression.compile(eFilter.attribute("condition")));
            }
        }
        if (StringUtils.isNotBlank(jobType.getLogExpressionText())) {
            importJobTypeDTO.setLogExpression(Expression.compile(jobType.getLogExpressionText()));
        }
        return importJobTypeDTO;
    }

    public ImportJobType getImportJobTypeByName(String name) {
        return nameToImportJobTypes.get(name);
    }

    public ImportJobTypeDTO getImportJobTypeConfigByName(String name) {
        return nameToImportJobTypeConfigs.get(name);
    }

    public ImportJobTypeDTO getImportJobTypeConfigById(Integer importJobTypeId) {
        return idToImportJobTypeConfigs.get(importJobTypeId);
    }

    /**
     * @return the importJobTypes
     */
    public List<ImportJobType> getImportJobTypes() {
        return importJobTypes;
    }

    public void freeze() {
        Collections.sort(importJobTypes, new Comparator<ImportJobType>() {
            @Override
            public int compare(ImportJobType ijt1, ImportJobType ijt2) {
                return ijt1.getName().compareTo(ijt2.getName());
            }
        });
    }

    @Override
    public void load() {
        List<ImportJobType> importJobTypes = importService.getImportJobTypes();
        for (ImportJobType jobType : importJobTypes) {
            try {
                ImportJobHandler importHandler = importService.constructImportJobHandler(jobType);
                addImportJobType(jobType, importHandler);
            } catch (Exception e) {
                LOG.error("unable to load import handler: " + jobType.getName(), e);
            }
        }
    }

}
