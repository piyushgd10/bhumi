/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 19, 2013
 *  @author karunsingla
 */
package com.uniware.services.configuration;

import com.unifier.core.annotation.Configuration;
import com.unifier.core.annotation.Level;
import com.unifier.core.configuration.IConfiguration;
import com.unifier.core.utils.ValidationProfileUtils;
import com.unifier.core.utils.ValidationProfileUtils.ValidationProfile;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author Sunny
 */
@Configuration(name = "validationProfileConfiguration", level = Level.GLOBAL, eager = true)
public class ValidationProfileConfiguration implements IConfiguration {

    private final Map<Class<?>, ValidationProfile> clazzToValidationProfile = new ConcurrentHashMap<>();

    public ValidationProfile getValidationProfile(Class<?> clazz) {
        ValidationProfile validationProfile;
        if (clazzToValidationProfile.containsKey(clazz)) {
            validationProfile = clazzToValidationProfile.get(clazz);
        } else {
            validationProfile = ValidationProfileUtils.getValidationProfile(clazz);
            clazzToValidationProfile.put(clazz, validationProfile);
        }
        return validationProfile;
    }

    @Override
    public void load() {
    }

}
