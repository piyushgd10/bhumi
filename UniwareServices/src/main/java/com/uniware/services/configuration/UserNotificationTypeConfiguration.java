/*
 *  Copyright 2013 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 08-Feb-2013
 *  @author praveeng
 */
package com.uniware.services.configuration;

import com.unifier.core.annotation.Configuration;
import com.unifier.core.annotation.Level;
import com.unifier.core.configuration.IConfiguration;
import com.unifier.services.user.notification.IUserNotificationService;
import com.uniware.core.vo.UserNotificationTypeVO;
import com.uniware.core.vo.UserNotificationTypeVO.NotificationType;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Configuration(name = "userNotificationTypeConfiguration", level = Level.TENANT, eager = true)
public class UserNotificationTypeConfiguration implements IConfiguration {

    private Map<NotificationType, UserNotificationTypeVO> typeToUserNotificationType = new ConcurrentHashMap<>();

    @Autowired
    private transient IUserNotificationService userNotificationService;

    public void addUserNotificationType(UserNotificationTypeVO userNotificationType) {
        typeToUserNotificationType.put(NotificationType.valueOf(userNotificationType.getType()), userNotificationType);
    }

    public UserNotificationTypeVO getUserNotificationTypeByType(NotificationType type) {
        return typeToUserNotificationType.get(type);
    }

    @Override
    public void load() {
        for (UserNotificationTypeVO userNotificationType : userNotificationService.getAllUserNotificationTypes()) {
            addUserNotificationType(userNotificationType);
        }
    }
}
