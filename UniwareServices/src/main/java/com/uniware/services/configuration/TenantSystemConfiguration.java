/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 18, 2012
 *  @author singla
 */
package com.uniware.services.configuration;

import com.unifier.core.annotation.Configuration;
import com.unifier.core.annotation.Level;
import com.unifier.core.configuration.IConfiguration;
import com.unifier.core.entity.SystemConfig;
import com.unifier.core.entity.User;
import com.unifier.core.utils.StringUtils;
import com.unifier.services.application.IApplicationSetupService;
import com.unifier.services.users.IUsersService;
import com.uniware.core.utils.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author Sunny
 */
@Configuration(name = "tenantSystemConfiguration", level = Level.TENANT, eager = true)
public class TenantSystemConfiguration implements IConfiguration {

    private static final String VALIDATE_POSTCODE                            = "validate.postcode";
    private static final String RECEIVE_PICKLIST_GROUPBY_SKU                 = "receive.picklist.groupby.sku";
    private static final String BASE_CURRENCY                                = "base.currency";
    private static final String REJECT_SALE_ORDER_IF_NON_SERVICEABLE         = "reject.sale.order.if.non.serviceable";
    private static final String AVAILABLE_FACILITIES_FROM_THIRD_PARTY_SCRIPT = "available.facilities.from.third.party.script.name";
    private static final String IMPORT_UPPER_LIMIT                           = "import.upper.limit";
    private static final String EXPORTS_DATA_RANGE_IN_MONTHS                 = "exports.data.range.in.months";
    private static final String DATATABLES_DATA_RANGE_IN_MONTHS              = "datatables.data.range.in.months";
    private static final String EXPORTS_DATA_SPAN_IN_DAYS                    = "exports.data.span.in.days";
    private static final String SELECTIVE_FACILITY_INVENTORY_SYNC_ENABLED    = "selective.facility.inventory.sync.enabled";
    private User systemUser;

    private final Map<String, String> properties = new ConcurrentHashMap<>();

    @Autowired
    private transient IApplicationSetupService applicationSetupService;

    @Autowired
    private transient IUsersService usersService;

    @Autowired
    private IConfigurationService     configurationService;

    private static final Logger LOG = LoggerFactory.getLogger(TenantSystemConfiguration.class);

    private void addConfiguration(SystemConfig systemConfig) {
        properties.put(systemConfig.getName(), systemConfig.getValue());
    }

    public String getProperty(String name) {
        return properties.get(name);
    }

    public boolean isValidatePostcode() {
        return getProperty(VALIDATE_POSTCODE) != null ? Boolean.parseBoolean(getProperty(VALIDATE_POSTCODE)) : true;
    }

    public boolean isReceivePicklistGroupbySku() {
        return getProperty(RECEIVE_PICKLIST_GROUPBY_SKU) != null ? Boolean.parseBoolean(getProperty(RECEIVE_PICKLIST_GROUPBY_SKU)) : false;
    }

    public boolean isRejectSaleOrderIfNonServiceable() {
        return getProperty(REJECT_SALE_ORDER_IF_NON_SERVICEABLE) != null ? Boolean.parseBoolean(getProperty(REJECT_SALE_ORDER_IF_NON_SERVICEABLE)) : false;
    }

    public boolean isSelectiveFacilityInventorySyncEnabled() {
        return getProperty(SELECTIVE_FACILITY_INVENTORY_SYNC_ENABLED) != null ? Boolean.parseBoolean(getProperty(SELECTIVE_FACILITY_INVENTORY_SYNC_ENABLED)) : false;
    }

    public long getUpperLimitForImport() {
        return getProperty(IMPORT_UPPER_LIMIT) != null ? Long.parseLong(getProperty(IMPORT_UPPER_LIMIT)) : 0;
    }

    public Integer getExportsDataRangeInMonths() {
        return getProperty(EXPORTS_DATA_RANGE_IN_MONTHS) != null ? Integer.parseInt(getProperty(EXPORTS_DATA_RANGE_IN_MONTHS)) : null;
    }

    public Integer getExportsDataSpanInDays() {
        return getProperty(EXPORTS_DATA_SPAN_IN_DAYS) != null ? Integer.parseInt(getProperty(EXPORTS_DATA_SPAN_IN_DAYS)) : null;
    }

    public Integer getDatatablesDataRangeInMonths() {
        return getProperty(DATATABLES_DATA_RANGE_IN_MONTHS) != null ? Integer.parseInt(getProperty(DATATABLES_DATA_RANGE_IN_MONTHS)) : null;
    }

    public String getAvailableFacilitiesFromThirdPartyScriptName() {
        return getProperty(AVAILABLE_FACILITIES_FROM_THIRD_PARTY_SCRIPT);
    }

    public String getBaseCurrency() {
        return getProperty(BASE_CURRENCY);
    }

    public User getSystemUser() {
        return systemUser;
    }

    public void setSystemUser(User systemUser) {
        this.systemUser = systemUser;
    }

    @Override
    public void load() {
        List<SystemConfig> systemConfigs = applicationSetupService.getTenantSystemConfigs();
        systemConfigs.forEach(this::addConfiguration);
        systemConfigs.forEach(config -> {
            if (StringUtils.isNotBlank(config.getConditionExpression())) {
                if (!configurationService.evaluateConditionExpression(config, properties)) {
                    throw new IllegalStateException("ConditionExpression evaluation failed. Dependent properties are not correct for: " + config.getName());
                }
            }
        });

        User systemUser = usersService.getUserByUsername(Constants.SYSTEM_USER_EMAIL);

        LOG.warn("Unable to find SYSTEM user");

        setSystemUser(systemUser);
    }
}
