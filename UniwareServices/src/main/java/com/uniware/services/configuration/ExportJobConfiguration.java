/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 18, 2012
 *  @author singla
 */
package com.uniware.services.configuration;

import com.unifier.core.annotation.Configuration;
import com.unifier.core.annotation.Level;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.configuration.IConfiguration;
import com.unifier.core.entity.ExportJobType;
import com.unifier.core.export.config.ExportColumn;
import com.unifier.core.export.config.ExportConfig;
import com.unifier.core.export.config.ExportFilter;
import com.unifier.core.export.config.ExportFilter.ValueType;
import com.unifier.core.expressions.Expression;
import com.unifier.core.table.config.DatatableColumn;
import com.unifier.core.table.config.DatatableConfig;
import com.unifier.core.table.config.DatatableFilter;
import com.unifier.core.ui.SelectItem;
import com.unifier.core.utils.JsonUtils;
import com.unifier.core.utils.StringUtils;
import com.unifier.core.utils.XMLParser;
import com.unifier.core.utils.XMLParser.Element;
import com.unifier.services.export.IExportService;
import com.uniware.core.cache.FacilityCache;
import com.uniware.core.utils.UserContext;
import com.uniware.services.configuration.CustomFieldsMetadataConfiguration.CustomFieldMetadataVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author singla
 */
@Configuration(name = "exportJobConfiguration", level = Level.TENANT)
public class ExportJobConfiguration implements IConfiguration {

    private static final Logger                     LOG                         = LoggerFactory.getLogger(ExportJobConfiguration.class);
    private              List<ExportConfig>         exportConfigs               = new ArrayList<ExportConfig>();
    private              List<DatatableConfig>      datatableConfigs            = new ArrayList<DatatableConfig>();
    private final        Map<String, ExportConfig>  nameToExportConfigs         = new ConcurrentHashMap<>();
    private final        Map<String, ExportJobType> nameToExportJobTypes        = new ConcurrentHashMap<>();
    private final        Map<Integer, ExportConfig> idToExportConfigs           = new ConcurrentHashMap<>();
    private final        Map<String, List<Integer>> viewNameToExportConfigIdMap = new ConcurrentHashMap<>();

    @Autowired
    private transient IExportService exportService;

    /**
     * @param exportConfig
     */
    private void addExportJobConfig(ExportJobType exportJobType, ExportConfig exportConfig) {
        if (ExportJobType.Type.EXPORT.name().equals(exportJobType.getType())) {
            exportConfigs.add(exportConfig);
        } else {
            datatableConfigs.add((DatatableConfig) exportConfig);
        }
        if (StringUtils.isNotEmpty(exportJobType.getViewName())) {
            if (viewNameToExportConfigIdMap.get(exportJobType.getViewName()) == null) {
                viewNameToExportConfigIdMap.put(exportJobType.getViewName(), new ArrayList<Integer>());
            }
            viewNameToExportConfigIdMap.get(exportJobType.getViewName()).add(exportJobType.getId());
        }
        nameToExportConfigs.put(exportConfig.getName(), exportConfig);
        nameToExportJobTypes.put(exportJobType.getName(), exportJobType);
        idToExportConfigs.put(exportJobType.getId(), exportConfig);
    }

    public List<ExportConfig> getExportConfigsByViewName(String viewName) {
        List<ExportConfig> exportConfigList = new ArrayList<>();
        for (Integer exportJobTypeId : viewNameToExportConfigIdMap.get(viewName)) {
            exportConfigList.add(getExportConfigByJobTypeId(exportJobTypeId));
        }
        return exportConfigList;
    }

    /**
     * @param exportJobTypeName
     * @return
     */
    public ExportConfig getExportConfigByName(String exportJobTypeName) {
        return nameToExportConfigs.get(exportJobTypeName);
    }

    public ExportJobType getExportJobTypeByName(String exportJobTypeName) {
        return nameToExportJobTypes.get(exportJobTypeName);
    }

    /**
     * @return the exportConfigs
     */
    public List<ExportConfig> getExportConfigs() {
        return exportConfigs;
    }

    /**
     * @param exportConfigs the exportConfigs to set
     */
    public void setExportConfigs(List<ExportConfig> exportConfigs) {
        this.exportConfigs = exportConfigs;
    }

    /**
     * @param id
     * @return
     */

    public ExportConfig getExportConfigByJobTypeId(Integer id) {
        return idToExportConfigs.get(id);
    }

    public void freeze() {
        Collections.sort(exportConfigs, new Comparator<ExportConfig>() {
            @Override
            public int compare(ExportConfig ec1, ExportConfig ec2) {
                return ec1.getName().compareTo(ec2.getName());
            }
        });
    }

    public List<DatatableConfig> getDatatableConfigs() {
        return datatableConfigs;
    }

    @Override
    public void load() {
        List<ExportJobType> exportJobTypes = exportService.getExportJobTypes();
        for (ExportJobType exportJobType : exportJobTypes) {
            try {
                ExportConfig exportConfig = parseExportConfig(exportJobType);
                if (exportJobType.isEnabled()) {
                    addExportJobConfig(exportJobType, exportConfig);
                }
            } catch (Exception e) {
                LOG.error("unable to load config for export job type: {}", exportJobType.getName(), e);
            }
        }
    }

    private ExportConfig parseExportConfig(ExportJobType exportJobType) throws ClassNotFoundException {
        Element exportConfigRoot = XMLParser.parse(exportJobType.getExportJobConfig().trim());
        ExportConfig exportConfig;
        if (ExportJobType.Type.EXPORT.name().equals(exportJobType.getType())) {
            exportConfig = prepareExportConfig(new ExportConfig(), exportConfigRoot);
        } else {
            exportConfig = prepareExportConfig(prepareDatatableConfig(new DatatableConfig(), exportConfigRoot), exportConfigRoot);
        }
        exportConfig.setNoSql(exportJobType.isNoSql());
        exportConfig.setName(exportJobType.getName());
        exportConfig.setDisplayName(exportJobType.getDisplayName());
        exportConfig.setAccessResourceName(exportJobType.getAccessResourceName());
        exportConfig.setType(exportJobType.getType());
        List<Element> eColumns = exportConfigRoot.get("columns", true).list("column");
        for (Element eColumn : eColumns) {
            ExportColumn exportColumn;
            if (ExportJobType.Type.EXPORT.name().equals(exportJobType.getType())) {
                exportColumn = prepareExportColumn(new ExportColumn(), eColumn);
            } else {
                exportColumn = prepareExportColumn(prepareDatatableColumn(new DatatableColumn(), eColumn), eColumn);
            }
            exportConfig.addExportColumn(exportColumn);
        }
        Element eCustomFields = exportConfigRoot.get("custom-fields");
        if (eCustomFields != null) {
            List<Element> eCustomFieldsList = eCustomFields.list("custom-field");
            if (eCustomFieldsList.size() > 0) {
                CustomFieldsMetadataConfiguration configuration = ConfigurationManager.getInstance().getConfiguration(CustomFieldsMetadataConfiguration.class);
                for (Element eCustomField : eCustomFieldsList) {
                    List<CustomFieldMetadataVO> customFieldMetadatas = configuration.getCustomFieldsByEntity(eCustomField.attribute("entity", true));
                    if (customFieldMetadatas != null && customFieldMetadatas.size() > 0) {
                        for (CustomFieldMetadataVO customFieldMetadata : customFieldMetadatas) {
                            ExportColumn exportColumn;
                            if (ExportJobType.Type.EXPORT.name().equals(exportJobType.getType())) {
                                exportColumn = new ExportColumn();
                            } else {
                                exportColumn = new DatatableColumn();
                                if (customFieldMetadata.getValueType().equalsIgnoreCase("DATE")) {
                                    ((DatatableColumn) exportColumn).setType("date");
                                }
                                exportColumn.setHidden(true);
                            }
                            exportColumn.setId(eCustomField.attribute("id", true) + "_" + customFieldMetadata.getFieldName());
                            exportColumn.setName(customFieldMetadata.getDisplayName());
                            exportColumn.setFieldName(customFieldMetadata.getFieldName());
                            String alias = eCustomField.attribute("alias", true);
                            if (customFieldMetadata.isSerializable()) {
                                exportColumn.setValue(alias + "." + customFieldMetadata.getFieldName());
                            } else {
                                exportColumn.setValue(alias + "cf" + "." + customFieldMetadata.getMappingFieldName());
                                exportColumn.setNonSerializableCustomField(true);
                            }
                            exportColumn.setEntity(eCustomField.attribute("entity", true));
                            exportColumn.setTableAlias(alias);
                            exportConfig.addExportColumn(exportColumn);
                            /*if(ExportJobType.Type.EXPORT.name().equals(exportJobType.getType())) { 
                                exportConfig.addExportFilter(prepareCustomFieldExportFilter(new ExportFilter(), customFieldMetadata, eCustomField)); 
                            } else { 
                                exportConfig.addExportFilter(prepareCustomFieldExportFilter(prepareCustomFieldDatatableFilter(new DatatableFilter(), eCustomField, customFieldMetadata), customFieldMetadata, eCustomField));
                            }*/
                        }
                    }
                }
            }
        }

        Element eFilters = exportConfigRoot.get("filters");
        if (eFilters != null) {
            List<Element> eFilterList = eFilters.list("filter");
            if (eFilterList.size() > 0) {
                for (Element eFilter : eFilterList) {
                    if (ExportJobType.Type.EXPORT.name().equals(exportJobType.getType())) {
                        exportConfig.addExportFilter(prepareExportFilter(new ExportFilter(), eFilter));
                    } else {
                        ExportFilter exportFilter = prepareExportFilter(prepareDatatableFilter(new DatatableFilter(), eFilter), eFilter);
                        if (exportFilter.isPrimary()) {
                            exportConfig.setPrimaryFilter(exportFilter);
                        }
                        exportConfig.addExportFilter(exportFilter);
                    }
                }
                if (StringUtils.isBlank(exportConfig.getGroupBy()) && exportConfig.getGroupFilters().size() > 0) {
                    throw new RuntimeException("Can not use group filters without using group by");
                }
            }
        }
        return exportConfig;
    }

    private DatatableFilter prepareCustomFieldDatatableFilter(DatatableFilter datatableFilter, Element eCustomField, CustomFieldMetadataVO customFieldMetadata) {
        datatableFilter.setAttachToColumn(
                (new StringBuilder(String.valueOf(eCustomField.attribute("id", true)))).append("_").append(customFieldMetadata.getFieldName()).toString());
        return datatableFilter;
    }

    private DatatableFilter prepareDatatableFilter(DatatableFilter datatableFilter, Element eFilter) {
        datatableFilter.setAttachToColumn(eFilter.attribute("attachToColumn", true));
        datatableFilter.setPrimary(StringUtils.parseBoolean(eFilter.attribute("primary")));
        return datatableFilter;
    }

    private DatatableColumn prepareDatatableColumn(DatatableColumn datatableColumn, Element eColumn) {
        datatableColumn.setAlign(eColumn.attribute("align"));
        datatableColumn.setWidth(Integer.parseInt(eColumn.attribute("width", true)));
        datatableColumn.setSortable(StringUtils.parseBoolean(eColumn.attribute("sortable")));
        datatableColumn.setType(eColumn.attribute("type"));
        String clientTemplate = eColumn.text("clientTemplate");
        if (StringUtils.isNotBlank(clientTemplate)) {
            datatableColumn.setClientTemplate(clientTemplate);
        }
        return datatableColumn;
    }

    private ExportConfig prepareExportConfig(ExportConfig exportConfig, Element exportConfigRoot) {
        exportConfig.setViewCollectionName(exportConfigRoot.attribute("view-collection"));
        exportConfig.setQueryString(exportConfigRoot.text("queryString", true));
        if (StringUtils.isNotBlank(exportConfigRoot.attribute("delimiter"))) {
            exportConfig.setDelimiter(exportConfigRoot.attribute("delimiter").charAt(0));
        }
        if (StringUtils.isNotBlank(exportConfigRoot.attribute("resultCountNotSupported"))) {
            exportConfig.setResultCountNotSupported(StringUtils.parseBoolean(exportConfigRoot.attribute("resultCountNotSupported")));
        }
        String groupBy = exportConfigRoot.text("groupBy");
        if (StringUtils.isNotBlank(groupBy)) {
            exportConfig.setGroupBy(groupBy);
            Pattern p;
            if (groupBy.toLowerCase().contains("having")) {
                p = Pattern.compile("group\\s+by\\s+(.+)having", Pattern.CASE_INSENSITIVE);
            } else {
                p = Pattern.compile("group\\s+by\\s+(.+)", Pattern.CASE_INSENSITIVE);
            }

            Matcher m = p.matcher(groupBy);
            if (m.find()) {
                exportConfig.setResultCountQuery("select count(distinct " + m.group(1) + ") as count" + exportConfig.getQueryString());
            } else {
                LOG.error("Invalid group by clause {} in export config {}", groupBy, exportConfig.getName());
            }
        } else {
            exportConfig.setResultCountQuery("select count(*) as count " + exportConfig.getQueryString());
        }
        return exportConfig;
    }

    private DatatableConfig prepareDatatableConfig(DatatableConfig datatableConfig, Element exportConfigRoot) {
        datatableConfig.setHeight(Integer.parseInt(exportConfigRoot.attribute("height", true)));
        datatableConfig.setDefaultSortBy(exportConfigRoot.text("defaultSortBy"));
        return datatableConfig;
    }

    private ExportColumn prepareExportColumn(ExportColumn exportColumn, Element eColumn) {
        exportColumn.setId(eColumn.attribute("id", true));
        exportColumn.setName(eColumn.attribute("name", true));
        exportColumn.setValue(eColumn.attribute("value", true));
        exportColumn.setCalculated(StringUtils.parseBoolean(eColumn.attribute("calculated")));
        if (exportColumn.isCalculated()) {
            exportColumn.setCalculationExpression(Expression.compile(exportColumn.getValue()));
        }
        exportColumn.setHidden(StringUtils.parseBoolean(eColumn.attribute("hidden")));
        String uiConfigParamJson = eColumn.attribute("ui-config-params");
        if (StringUtils.isNotEmpty(uiConfigParamJson)) {
            exportColumn.setUiConfigParams(JsonUtils.stringToJson(uiConfigParamJson, HashMap.class));
        }
        exportColumn.setEditable(StringUtils.parseBoolean(eColumn.attribute("editable")));
        if (StringUtils.isNotBlank(eColumn.attribute("exportable"))) {
            exportColumn.setExportable(StringUtils.parseBoolean(eColumn.attribute("exportable")));
        }
        return exportColumn;
    }

    private ExportFilter prepareExportFilter(ExportFilter exportFilter, Element eFilter) {
        exportFilter.setId(eFilter.attribute("id", true));
        exportFilter.setName(eFilter.attribute("name", true));
        exportFilter.setCondition(eFilter.attribute("condition", true));
        exportFilter.setConditionForView(eFilter.attribute("condition-for-view"));
        exportFilter.setHidden(StringUtils.parseBoolean(eFilter.attribute("hidden")));
        exportFilter.setHiddenValue(eFilter.attribute("hidden-value"));
        ValueType valueType = ValueType.valueOf(eFilter.attribute("type", true).toUpperCase());
        exportFilter.setType(valueType);
        if (valueType == ValueType.MULTISELECT || valueType == ValueType.SELECT) {
            String values = eFilter.attribute("values");
            if (StringUtils.isBlank(values)) {
                throw new RuntimeException("filter: " + exportFilter.getName() + ", values is mandatory with MULTISELECT/SELECT type");
            }
            if (values.toLowerCase().startsWith("query:")) {
                String query = values.substring("query:".length());
                List<SelectItem> items = new ArrayList<SelectItem>();
                Map<String, Object> parameters = new HashMap<String, Object>();
                parameters.put("tenantId", UserContext.current().getTenantId());
                parameters.put("facilities", CacheManager.getInstance().getCache(FacilityCache.class).getFacilityIds());
                for (Object[] object : exportService.executeAnonymousQuery(query, parameters)) {
                    SelectItem item = new SelectItem();
                    item.setValue(String.valueOf(object[0]));
                    item.setName(String.valueOf(object[1]));
                    items.add(item);
                }
                exportFilter.setSelectItems(items);
            } else if (values.toLowerCase().startsWith("enum:")) {
                String value = values.substring("enum:".length());
                List<SelectItem> items = new ArrayList<SelectItem>();
                for (String paramValue : value.split(",")) {
                    int index = paramValue.indexOf('|');
                    SelectItem item = new SelectItem();
                    if (index == -1) {
                        item.setName(paramValue);
                        item.setValue(paramValue);
                    } else {
                        item.setName(paramValue.substring(0, index));
                        item.setValue(paramValue.substring(index + 1));
                    }
                    items.add(item);
                }
                exportFilter.setSelectItems(items);
            } else if (values.toLowerCase().startsWith("cache:")) {
                String cacheExpression[] = values.substring("cache:".length()).split(":");
                String cacheName = cacheExpression[0].split("#")[0];
                String methodName = cacheExpression[0].split("#")[1];
                Object[] methodParameters = null;
                Class<?>[] parameterTypes = null;
                boolean validCacheMethod = false;
                if (cacheExpression.length > 1) {
                    methodParameters = cacheExpression[1].substring("params:".length()).split(",");
                }
                if (methodParameters != null) {
                    parameterTypes = (Class<?>[]) Array.newInstance(Class.class, methodParameters.length);
                    for (int i = 0; i < methodParameters.length; i++) {
                        parameterTypes[i] = String.class;
                    }
                }
                try {
                    @SuppressWarnings("unchecked")
                    List<SelectItem> selectItems = (List<SelectItem>) CacheManager.getInstance().getCache(cacheName).getClass().getMethod(methodName, parameterTypes).invoke(
                            CacheManager.getInstance().getCache(cacheName), methodParameters);
                    if (selectItems != null && !selectItems.isEmpty()) {
                        exportFilter.setSelectItems(selectItems);
                        validCacheMethod = true;
                    }
                } catch (Exception e) {
                    throw new RuntimeException("cache: " + cacheName + " ,method : " + methodName + e);
                }
                if (!validCacheMethod) {
                    throw new RuntimeException("cache: " + cacheName + " ,method : " + methodName + "is invalid or has no result");
                }
            }
        }
        exportFilter.setRequired(StringUtils.parseBoolean(eFilter.attribute("required")));
        exportFilter.setDefaultDateRangeRestrictionExempted(StringUtils.parseBoolean(eFilter.attribute("defaultDateRangeRestrictionExempted")));
        exportFilter.setGroupFilter(StringUtils.parseBoolean(eFilter.attribute("groupFilter")));
        if (StringUtils.isNotBlank(eFilter.attribute("valueExpression"))) {
            exportFilter.setValueExpression(Expression.compile(eFilter.attribute("valueExpression")));
        }
        if (exportFilter.isHidden() && StringUtils.isBlank(exportFilter.getHiddenValue()) && exportFilter.getValueExpression() == null) {
            throw new RuntimeException("Either hidden-value or valueExpression is mandatory for hidden filters");
        }
        return exportFilter;
    }

    // Fetching from view is *NOT* supported for this method.
    private ExportFilter prepareCustomFieldExportFilter(ExportFilter exportFilter, CustomFieldMetadataVO customFieldMetadata, Element eCustomField) {
        exportFilter.setId(eCustomField.attribute("id", true) + "_" + customFieldMetadata.getFieldName() + "Filter");
        exportFilter.setName(customFieldMetadata.getDisplayName());
        String alias = eCustomField.attribute("alias", true);
        if (customFieldMetadata.isSerializable()) {
            alias = alias + "." + customFieldMetadata.getFieldName();
        } else {
            alias = alias + "cf" + "." + customFieldMetadata.getMappingFieldName();
            exportFilter.setNonSerializableCustomField(true);
        }
        if (customFieldMetadata.getValueType().equalsIgnoreCase("SELECT")) {
            exportFilter.setType(ExportFilter.ValueType.MULTISELECT);
            List<SelectItem> items = new ArrayList<SelectItem>();
            for (String item : customFieldMetadata.getPossibleValues()) {
                SelectItem selectItem = new SelectItem();
                selectItem.setValue(item);
                selectItem.setName(item);
                items.add(selectItem);
            }
            exportFilter.setSelectItems(items);
            exportFilter.setCondition(alias + " in (:" + exportFilter.getId() + ")");
        } else if (customFieldMetadata.getValueType().equalsIgnoreCase("DATE")) {
            exportFilter.setType(com.unifier.core.export.config.ExportFilter.ValueType.DATERANGE);
            exportFilter.setCondition(alias + " > :" + exportFilter.getId() + "Start and " + alias + "< :" + exportFilter.getId() + "End");
        } else if (customFieldMetadata.getValueType().equalsIgnoreCase("CHECKBOX")) {
            exportFilter.setType(com.unifier.core.export.config.ExportFilter.ValueType.BOOLEAN);
            exportFilter.setCondition(alias + " = :" + exportFilter.getId());
        } else {
            exportFilter.setType(com.unifier.core.export.config.ExportFilter.ValueType.TEXT);
            exportFilter.setCondition(alias + " like :" + exportFilter.getId());
            exportFilter.setValueExpression(Expression.compile("%#{#" + exportFilter.getId() + "}%"));
        }
        return exportFilter;
    }

}
