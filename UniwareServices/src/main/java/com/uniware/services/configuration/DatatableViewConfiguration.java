/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 19, 2013
 *  @author karunsingla
 */
package com.uniware.services.configuration;

import com.unifier.core.annotation.Configuration;
import com.unifier.core.annotation.Level;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.IConfiguration;
import com.unifier.services.application.IApplicationSetupService;
import com.uniware.core.cache.FacilityCache;
import com.uniware.core.cache.RolesCache;
import com.uniware.core.entity.Facility;
import com.uniware.core.entity.UserDatatableView;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author karunsingla
 */
@Configuration(name = "datatableViewConfiguration", level = Level.TENANT, eager = true)
public class DatatableViewConfiguration implements IConfiguration {

    private final Map<String, List<UserDatatableView>>                     datatableTypeToDatatableViews               = new LinkedHashMap<>();
    private final Map<Facility.Type, Map<String, List<UserDatatableView>>> facilityTypeToDatatableTypeToDatatableViews = new ConcurrentHashMap<>();

    @Autowired
    private transient IApplicationSetupService applicationSetupService;

    public void addDatatableView(UserDatatableView userDatatableView) {
        doAddDatatableView(userDatatableView);
        for (Facility.Type facilityType : Facility.Type.values()) {
            if (CacheManager.getInstance().getCache(RolesCache.class).isAccessResourcePresentInFacilityType(userDatatableView.getAccessResourceName(), facilityType)) {
                doAddDatatableView(userDatatableView, facilityType);
            }
        }
    }

    private void doAddDatatableView(UserDatatableView userDatatableView, Facility.Type facilityType) {
        Map<String, List<UserDatatableView>> typeToDatatableViews = facilityTypeToDatatableTypeToDatatableViews.get(facilityType);
        if (typeToDatatableViews == null) {
            typeToDatatableViews = new LinkedHashMap<>();
            facilityTypeToDatatableTypeToDatatableViews.put(facilityType, typeToDatatableViews);
        }
        List<UserDatatableView> datatableViews = typeToDatatableViews.get(userDatatableView.getDatatableType());
        if (datatableViews == null) {
            datatableViews = new ArrayList<>();
            typeToDatatableViews.put(userDatatableView.getDatatableType(), datatableViews);
        }
        if (datatableViews.contains(userDatatableView)) {
            datatableViews.remove(userDatatableView);
        }
        datatableViews.add(userDatatableView);
        Collections.sort(datatableViews);
    }

    private void doAddDatatableView(UserDatatableView userDatatableView) {
        List<UserDatatableView> datatableViews = datatableTypeToDatatableViews.get(userDatatableView.getDatatableType());
        if (datatableViews == null) {
            datatableViews = new ArrayList<>();
            datatableTypeToDatatableViews.put(userDatatableView.getDatatableType(), datatableViews);
        }
        if (datatableViews.contains(userDatatableView)) {
            datatableViews.remove(userDatatableView);
        }
        datatableViews.add(userDatatableView);
        Collections.sort(datatableViews);
    }

    public List<UserDatatableView> getDatatableViewsByType(String datatableType) {
        Facility currentFacility = CacheManager.getInstance().getCache(FacilityCache.class).getCurrentFacility();
        if (currentFacility != null) {
            Facility.Type facilityType = Facility.Type.valueOf(currentFacility.getType());
            return facilityTypeToDatatableTypeToDatatableViews.get(facilityType).get(datatableType);
        }
        return datatableTypeToDatatableViews.get(datatableType);
    }

    @Override
    public void load() {
        List<UserDatatableView> datatableViews = applicationSetupService.getDatatableViews();
        for (UserDatatableView datatableView : datatableViews) {
            this.addDatatableView(datatableView);
        }
    }
}
