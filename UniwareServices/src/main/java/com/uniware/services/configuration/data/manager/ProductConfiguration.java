/*
 * Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 1/17/15 4:37 AM
 * @author amdalal
 */

package com.uniware.services.configuration.data.manager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.unifier.core.entity.ProductSource;
import com.uniware.core.entity.Source;
import org.springframework.beans.factory.annotation.Autowired;

import com.unifier.core.annotation.Configuration;
import com.unifier.core.annotation.Level;
import com.unifier.core.configuration.IConfiguration;
import com.unifier.core.entity.ProductFeature;
import com.unifier.core.entity.ProductRole;
import com.unifier.core.utils.StringUtils;
import com.unifier.services.application.IApplicationSetupService;
import com.uniware.core.entity.Feature;
import com.uniware.core.entity.Product;
import com.uniware.core.utils.UserContext;
import com.uniware.core.vo.ProductDefaultVO;

@Configuration(name = "productConfiguration", level = Level.GLOBAL, eager = true)
public class ProductConfiguration implements IConfiguration {

    private static final String                    COURIER_MANAGEMENT                       = "COURIER_MANAGEMENT";
    private static final String                    PRODUCT_MANAGEMENT                       = "PRODUCT_MANAGEMENT";
    private static final String                    INVENTORY_MANAGEMENT                     = "INVENTORY_MANAGEMENT";
    private static final String                    TOTAL_PERMITTED_CHANNEL_COUNT            = "total.permitted.channel.count";
    private static final String                    TOTAL_PERMITTED_SKU_COUNT                = "total.permitted.sku.count";
    private static final String                    TOTAL_PERMITTED_CHANNEL_COUNT_PER_SOURCE = "total.permitted.channel.count.per.source";
    private static final String                    MAXIMUM_EXPORT_LIMIT                     = "maximum.export.limit";
    private static final String                    MAXIMUM_IMPORT_LIMIT                     = "maximum.import.limit";

    private final List<Product>                    products                                 = new ArrayList<Product>();
    private final Map<String, List<Feature>>       productToFeatures                        = new HashMap<>();
    private final Map<String, List<Product>>       featureToProducts                        = new HashMap<>();
    private final Map<String, List<String>>        productToRoles                           = new HashMap<>();
    private final Map<String, Map<String, String>> productToProductDefaults                 = new HashMap<>();

    @Autowired
    private transient IApplicationSetupService               applicationSetupService;

    public void addProductFeature(ProductFeature productFeature) {
        List<Feature> features = productToFeatures.get(productFeature.getProduct().getCode());
        if (features == null) {
            features = new ArrayList<>();
            productToFeatures.put(productFeature.getProduct().getCode(), features);
        }
        features.add(productFeature.getFeature());

        List<Product> products = featureToProducts.get(productFeature.getFeature().getCode());
        if (products == null) {
            products = new ArrayList<>();
            featureToProducts.put(productFeature.getFeature().getCode(), products);
        }
        products.add(productFeature.getProduct());
    }

    public void addProductDefault(ProductDefaultVO productDefault) {
        Map<String, String> configuration = productToProductDefaults.get(productDefault.getProductType());
        if (configuration == null) {
            configuration = new HashMap<>();
            productToProductDefaults.put(productDefault.getProductType(), configuration);
        }
        configuration.put(productDefault.getName(), productDefault.getValue());
    }

    public void addProductRole(ProductRole productRole) {
        List<String> roles = productToRoles.get(productRole.getProduct().getCode());
        if (roles == null) {
            roles = new ArrayList<>();
            productToRoles.put(productRole.getProduct().getCode(), roles);
        }
        roles.add(productRole.getRole());

    }

    public List<Product> getProducts(String featureCode) {
        return featureToProducts.get(featureCode);
    }

    public boolean hasFeature(String featureCode) {
        String productName = UserContext.current().getTenant().getProduct().getCode();
        List<Feature> features = productToFeatures.get(productName);
        if (features != null) {
            for (Feature feature : features) {
                if (feature.getCode().equals(featureCode)) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean hasRole(String roleCode) {
        String productName = UserContext.current().getTenant().getProduct().getCode();
        List<String> roles = productToRoles.get(productName);
        if (roles != null) {
            for (String role : roles) {
                if (role.equals(roleCode)) {
                    return true;
                }
            }
        }
        return false;
    }

    public List<Feature> getAllFeatures() {
        String productCode = UserContext.current().getTenant().getProduct().getCode();
        return productToFeatures.get(productCode);
    }

    public String getProductDefault(String name) {
        String productCode = UserContext.current().getTenant().getProduct().getCode();
        Map<String, String> productDefaults = productToProductDefaults.get(productCode);
        if (productDefaults != null) {
            return productDefaults.get(name);
        }
        return null;
    }

    public Integer getTotalPermittedChannelsCount() {
        String value = getProductDefault(TOTAL_PERMITTED_CHANNEL_COUNT);
        if (value != null) {
            return Integer.parseInt(value);
        }
        return null;
    }
    public Integer getTotalPermittedSkusCount() {
        String value = getProductDefault(TOTAL_PERMITTED_SKU_COUNT);
        if (value != null) {
            return Integer.parseInt(value);
        }
        return null;
    }

    public Integer getTotalPermittedChannelsCountPerSource() {
        String value = getProductDefault(TOTAL_PERMITTED_CHANNEL_COUNT_PER_SOURCE);
        if (value != null) {
            return Integer.parseInt(value);
        }
        return null;
    }

    public Long getMaximumExportLimit() {
        String value = getProductDefault(MAXIMUM_EXPORT_LIMIT);
        if (StringUtils.isNotBlank(value)) {
            return Long.parseLong(value);
        }
        return 200000l;
    }

    public Long getMaximumImportLimit() {
        String value = getProductDefault(MAXIMUM_IMPORT_LIMIT);
        if (StringUtils.isNotBlank(value)) {
            return Long.parseLong(value);
        }
        return 100000l;
    }

    public boolean isCourierManagementSwitchedOff() {
        return !hasFeature(COURIER_MANAGEMENT);
    }

    public boolean isProductManagementSwitchedOff() {
        return !hasFeature(PRODUCT_MANAGEMENT);
    }

    public boolean isInventoryManagementSwitchedOff() {
        return !hasFeature(INVENTORY_MANAGEMENT);
    }

    public List<Product> getAllProducts() {
        return products;
    }

    @Override
    public void load() {
        for (Product product : applicationSetupService.getAllProducts()) {
            products.add(product);
        }

        for (ProductFeature productFeature : applicationSetupService.getProductFeatures()) {
            addProductFeature(productFeature);
        }
        for (ProductDefaultVO productDefault : applicationSetupService.getProductDefaults()) {
            addProductDefault(productDefault);
        }

        for (ProductRole productRole : applicationSetupService.getProductRoles()) {
            addProductRole(productRole);
        }
    }
}
