/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 28, 2011
 *  @author singla
 */
package com.uniware.services.configuration;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import com.uniware.services.channel.impl.ChannelServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;
import com.unifier.core.annotation.Configuration;
import com.unifier.core.annotation.Level;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.configuration.IConfiguration;
import com.unifier.services.application.IApplicationSetupService;
import com.unifier.services.shipping.service.ISourceShippingProviderService;
import com.unifier.services.vo.ShippingProviderVO;
import com.unifier.services.vo.SourceShippingProviderVO;
import com.uniware.core.api.saleorder.PaymentMethodDTO;
import com.uniware.core.api.shipping.ShipmentTrackingSyncStatusDTO;
import com.uniware.core.entity.PaymentMethod;
import com.uniware.core.entity.ShipmentTrackingStatus;
import com.uniware.core.entity.ShipmentTrackingStatusMapping;
import com.uniware.core.entity.ShippingMethod;
import com.uniware.core.entity.ShippingPackageStatus;
import com.uniware.core.entity.ShippingProvider;
import com.uniware.core.entity.ShippingProvider.ShippingServiceablity;
import com.uniware.core.entity.ShippingProvider.SyncStatus;
import com.uniware.core.entity.ShippingProviderSource;
import com.uniware.services.configuration.data.manager.ProductConfiguration;
import com.uniware.services.shipping.AbstractShipmentHandler;
import com.uniware.services.shipping.IShipmentTrackingService;
import com.uniware.services.shipping.IShippingAdminService;
import com.uniware.services.shipping.IShippingProviderService;
import com.uniware.services.shipping.IShippingService;

/**
 * @author singla
 */
@Configuration(name = "shippingConfiguration", level = Level.TENANT) public class ShippingConfiguration implements IConfiguration {

    private static final Logger LOG = LoggerFactory.getLogger(ShippingConfiguration.class);

    private final List<ShippingProvider>                     allShippingProviders                                     = new ArrayList<ShippingProvider>();
    private final List<ShippingProvider>                     shippingProviders                                        = new ArrayList<ShippingProvider>();
    private final Set<ShippingProvider>                      sourceShippingProviders                                  = new HashSet<ShippingProvider>();
    private final Set<String>                                trackingEnabledShippingProviderCodes                     = new HashSet<>();
    private final List<ShippingProvider>                     globalServiceabilityProviders                            = new ArrayList<ShippingProvider>();
    private final List<ShippingMethod>                       shippingMethods                                          = new ArrayList<ShippingMethod>();
    private final Map<Integer, ShippingMethod>               idToShippingMethods                                      = new ConcurrentHashMap<>();
    private final Map<String, ShippingMethod>                codeToShippingMethod                                     = new ConcurrentHashMap<>();
    private final List<PaymentMethod>                        paymentMethods                                           = new ArrayList<PaymentMethod>();
    private final Map<String, PaymentMethod>                 codeToPaymentMethods                                     = new ConcurrentHashMap<>();
    private final Map<Integer, ShippingProvider>             idToShippingProviders                                    = new ConcurrentHashMap<>();
    private final Map<String, ShippingProvider>              codeToShippingProviders                                  = new ConcurrentHashMap<>();
    private final Map<String, Map<String, ShippingProvider>> sourceCodeToSourceShippingProviderCodeToShippingProvider = new ConcurrentHashMap<>();
    private final Map<String, List<String>>                  shippingProviderToSources                                = new ConcurrentHashMap<>();
    private final Map<String, AbstractShipmentHandler>       codeToShipmentHandlers                                   = new ConcurrentHashMap<>();
    private final Map<String, ShipmentTrackingSyncStatusDTO> shippingCodeToTrackingSyncStatus                         = new ConcurrentHashMap<>();

    private final Map<String, ShipmentTrackingStatus>              codeToShipmentTrackingStatuses           = new ConcurrentHashMap<>();
    private final Map<Integer, ShippingPackageStatus>              idToShippingPackageStatuses              = new ConcurrentHashMap<>();
    private final Map<String, ShippingPackageStatus>               codeToShippingPackageStatuses            = new ConcurrentHashMap<>();
    private final List<String>                                     shippingPackageStatuses                  = new ArrayList<String>();
    private final Map<String, Map<String, ShipmentTrackingStatus>> providerToProviderStatusToTrackingStatus = new ConcurrentHashMap<>();
    private final List<String>                                     shipmentTrackingPollingStatuses          = new ArrayList<String>();
    private String shippingProvidersJson;
    private String shippingMethodsJson;
    private final List<ShipmentTrackingStatusVO> shipmentTrackingStatusVOs = new ArrayList<ShippingConfiguration.ShipmentTrackingStatusVO>();
    private String shipmentTrackingStatusJson;
    private final Map<String, Map<String, SourceShippingProviderVO>> sourceCodeToShippingProviderCodeToSourceShippingProvider = new ConcurrentHashMap<>();
    private String paymentMethodJson;
    private List<ShippingProviderVO> globalShippingProviders = new ArrayList<>();

    @Autowired private transient IShippingProviderService shippingProviderService;

    @Autowired private transient IShippingAdminService shippingAdminService;

    @Autowired private transient com.unifier.services.shipping.service.IShippingProviderService globalShippingProviderService;

    @Autowired private transient IShippingService shippingService;

    @Autowired private transient IShipmentTrackingService shipmentTrackingService;

    @Autowired private transient IApplicationSetupService applicationSetupService;

    @Autowired private transient ISourceShippingProviderService sourceShippingProviderService;

    private void addShippingProviderVO(ShippingProviderVO shippingProviderVO) {
        globalShippingProviders.add(shippingProviderVO);
        ShippingProvider shippingProvider = new ShippingProvider();
        shippingProvider.setCode(shippingProviderVO.getCode());
        shippingProvider.setName(shippingProviderVO.getName());
        codeToShippingProviders.put(shippingProvider.getCode().toLowerCase(), shippingProvider);
        // allShippingProviders.add(shippingProvider);
        // shippingProviders.add(shippingProvider);
    }

    private void addShippingProvider(ShippingProvider shippingProvider, AbstractShipmentHandler shipmentHandler) {
        allShippingProviders.add(shippingProvider);
        idToShippingProviders.put(shippingProvider.getId(), shippingProvider);
        codeToShippingProviders.put(shippingProvider.getCode().toLowerCase(), shippingProvider);
        codeToShipmentHandlers.put(shippingProvider.getCode(), shipmentHandler);

        if (shippingProvider.isEnabled()) {
            shippingProviders.add(shippingProvider);
            if (ShippingServiceablity.GLOBAL_SERVICEABLITY.equals(shippingProvider.getShippingServiceablity())) {
                globalServiceabilityProviders.add(shippingProvider);
            }
            if (SyncStatus.ON.equals(shippingProvider.getTrackingSyncStatus())) {
                trackingEnabledShippingProviderCodes.add(shippingProvider.getCode());
            }
            /*Map<String, State> stateNameToState = new HashMap<String, State>();
            for (ShippingProviderStateMapping providerStateMapping : shippingProvider.getShippingProviderStateMappings()) {
                stateNameToState.put(providerStateMapping.getText().toLowerCase(), providerStateMapping.getState());
            }
            providerToStateIdToStates.put(shippingProvider.getCode().toLowerCase(), stateNameToState);*/
        }
    }

    private void addShipmentTrackingStatus(ShipmentTrackingStatus shipmentTrackingStatus) {
        codeToShipmentTrackingStatuses.put(shipmentTrackingStatus.getCode(), shipmentTrackingStatus);
        shipmentTrackingStatusVOs.add(new ShipmentTrackingStatusVO(shipmentTrackingStatus));
        for (ShipmentTrackingStatusMapping mapping : shipmentTrackingStatus.getShipmentTrackingStatusMappings()) {
            ShippingProviderSource source = ConfigurationManager.getInstance().getConfiguration(ShippingSourceConfiguration.class).getShippingSourceByCode(
                    mapping.getShippingSourceCode());
            Map<String, ShipmentTrackingStatus> providerMappings = providerToProviderStatusToTrackingStatus.get(source.getId());
            if (providerMappings == null) {
                providerMappings = new HashMap<String, ShipmentTrackingStatus>();
                providerToProviderStatusToTrackingStatus.put(source.getId(), providerMappings);
            }
            providerMappings.put(mapping.getProviderStatus().toUpperCase(), shipmentTrackingStatus);
        }
        if (!shipmentTrackingStatus.isStopPolling()) {
            shipmentTrackingPollingStatuses.add(shipmentTrackingStatus.getCode());
        }
    }

    private void addSourceShippingProvider(SourceShippingProviderVO sourceShippingProviderVO) {
        Map<String, ShippingProvider> sourceShippingProviderCodeToShippingProviderMap = sourceCodeToSourceShippingProviderCodeToShippingProvider.get(
                sourceShippingProviderVO.getSourceCode());
        if (sourceShippingProviderCodeToShippingProviderMap == null) {
            sourceShippingProviderCodeToShippingProviderMap = new HashMap<String, ShippingProvider>();
            sourceCodeToSourceShippingProviderCodeToShippingProvider.put(sourceShippingProviderVO.getSourceCode(), sourceShippingProviderCodeToShippingProviderMap);
        }
        sourceShippingProviderCodeToShippingProviderMap.put(sourceShippingProviderVO.getSourceShippingProviderCode().toLowerCase(),
                getShippingProviderByCode(sourceShippingProviderVO.getShippingProviderCode()));
        List<String> sources = shippingProviderToSources.get(sourceShippingProviderVO.getShippingProviderCode().toLowerCase());
        if (sources == null) {
            sources = new ArrayList<String>();
            shippingProviderToSources.put(sourceShippingProviderVO.getShippingProviderCode().toLowerCase(), sources);
        }
        sources.add(sourceShippingProviderVO.getSourceCode());
        Map<String, SourceShippingProviderVO> codeToSourceShippingProvider = sourceCodeToShippingProviderCodeToSourceShippingProvider.get(sourceShippingProviderVO.getSourceCode());
        if (codeToSourceShippingProvider == null) {
            codeToSourceShippingProvider = new HashMap<String, SourceShippingProviderVO>();
            sourceCodeToShippingProviderCodeToSourceShippingProvider.put(sourceShippingProviderVO.getSourceCode(), codeToSourceShippingProvider);

        }
        codeToSourceShippingProvider.put(sourceShippingProviderVO.getShippingProviderCode(), sourceShippingProviderVO);
    }

    /**
     * @return the codeToShipmentTrackingStatuses
     */
    public Map<String, ShipmentTrackingStatus> getCodeToShipmentTrackingStatuses() {
        return codeToShipmentTrackingStatuses;
    }

    private void addShippingPackageStatus(ShippingPackageStatus shippingPackageStatus) {
        if (!idToShippingPackageStatuses.containsKey(shippingPackageStatus.getId())) {
            idToShippingPackageStatuses.put(shippingPackageStatus.getId(), shippingPackageStatus);
            codeToShippingPackageStatuses.put(shippingPackageStatus.getCode(), shippingPackageStatus);
            shippingPackageStatuses.add(shippingPackageStatus.getCode());
        }
    }

    public ShipmentTrackingStatus getMappedShipmentTrackingStatus(String shippingProviderSourceId, String providerStatus) {
        Map<String, ShipmentTrackingStatus> providerMappings = providerToProviderStatusToTrackingStatus.get(shippingProviderSourceId);
        if (providerMappings != null) {
            return providerMappings.get(providerStatus.toUpperCase());
        }
        return null;
    }

    private void addShippingMethod(ShippingMethod shippingMethod) {
        shippingMethods.add(shippingMethod);
        idToShippingMethods.put(shippingMethod.getId(), shippingMethod);
        codeToShippingMethod.put(shippingMethod.getName().toLowerCase(), shippingMethod);
    }

    private void addPaymentMethod(PaymentMethod paymentMethod) {
        paymentMethods.add(paymentMethod);
        codeToPaymentMethods.put(paymentMethod.getCode(), paymentMethod);
    }

    /*private void addShippingProviderRegion(ShippingProviderRegion providerRegion) {
        codeToShippingProviderRegion.put(providerRegion.getName(), providerRegion);
    }*/

    public List<ShippingProviderVO> getAllGlobalShippingProviders() {
        return globalShippingProviders;
    }

    public ShippingProvider getShippingProviderById(int shippingProviderId) {
        return idToShippingProviders.get(shippingProviderId);
    }

    public ShippingProvider getShippingProviderByCode(String shippingProviderCode) {
        return codeToShippingProviders.get(shippingProviderCode.toLowerCase());
    }

    public ShippingProvider getShippingProviderBySourceAndCode(String sourceCode, String shippingProviderCode) {
        ShippingProvider shippingProvider = null;
        Map<String, ShippingProvider> codeToShippingProvider = sourceCodeToSourceShippingProviderCodeToShippingProvider.get(sourceCode);
        if (codeToShippingProvider != null) {
            shippingProvider = codeToShippingProvider.get(shippingProviderCode.toLowerCase());
        }
        if (shippingProvider == null) {
            shippingProvider = getShippingProviderByCode(shippingProviderCode);
        }
        return shippingProvider;
    }

    public SourceShippingProviderVO getSourceShippingProviderBySourceAndShippingProviderCode(String sourceCode, String shippingProviderCode) {
        SourceShippingProviderVO sourceShippingProviderVo = null;
        Map<String, SourceShippingProviderVO> shippingProviderCodeToSourceShippingProvider = sourceCodeToShippingProviderCodeToSourceShippingProvider.get(sourceCode);
        if (shippingProviderCodeToSourceShippingProvider != null) {
            sourceShippingProviderVo = shippingProviderCodeToSourceShippingProvider.get(shippingProviderCode);
        }
        return sourceShippingProviderVo;
    }

    public List<String> getSourcesByShippingProvider(String shippingProviderCode) {
        return shippingProviderToSources.get(shippingProviderCode.toLowerCase());
    }

    public AbstractShipmentHandler getShipmentHandlerByProviderCode(String shippingProviderCode) {
        return codeToShipmentHandlers.get(shippingProviderCode);
    }

    public List<String> getShipmentTrackingPollingStatuses() {
        return shipmentTrackingPollingStatuses;
    }

    public ShipmentTrackingStatus getShipmentTrackingStatusByCode(String statusCode) {
        return codeToShipmentTrackingStatuses.get(statusCode);
    }

    public ShippingPackageStatus getShippingPackageStatusById(int statusId) {
        return idToShippingPackageStatuses.get(statusId);
    }

    public ShipmentTrackingSyncStatusDTO getShipmentTrackingSyncStatusDTOByCode(String shippingProviderCode) {
        return shippingCodeToTrackingSyncStatus.get(shippingProviderCode);
    }

    public ShipmentTrackingSyncStatusDTO updateShipmentTrackinfSyncStatusDTOByCode(ShipmentTrackingSyncStatusDTO statusDTO) {
        return shippingCodeToTrackingSyncStatus.put(statusDTO.getShippingProviderCode(), statusDTO);
    }

    /**
     * @param shippingMethodCode
     * @param cashOnDelivery
     */
    public ShippingMethod getShippingMethod(String shippingMethodName) {
        return codeToShippingMethod.get(shippingMethodName.toLowerCase());
    }

    /**
     * @param shippingMethodId
     * @return
     */
    public ShippingMethod getShippingMethodById(int shippingMethodId) {
        return idToShippingMethods.get(shippingMethodId);
    }

    public ShippingMethod getShippingMethodByCode(String shippingMethodCode) {
        return codeToShippingMethod.get(shippingMethodCode.toLowerCase());
    }

    public List<ShippingProvider> getAllShippingProviders() {
        return allShippingProviders;
    }

    public List<ShippingProvider> getShippingProviders() {
        return shippingProviders;
    }

    public List<ShippingProvider> getGlobalServiceabilityShippingProviders() {
        return globalServiceabilityProviders;
    }

    public Set<String> getTrackingEnabledShippingProviderCodes() {
        return trackingEnabledShippingProviderCodes;
    }

    public List<ShippingMethod> getShippingMethods() {
        return shippingMethods;
    }

    /*public ShippingProviderRegion getShippingProviderRegionByCode(String shippingProviderRegion) {
        return codeToShippingProviderRegion.get(shippingProviderRegion);
    }

    public State getStateCodeForProvider(String shippingProviderCode, String stateName) {
        Map<String, State> stateNameToState = providerToStateIdToStates.get(shippingProviderCode.toLowerCase());
        if (stateNameToState != null) {
            return stateNameToState.get(stateName.toLowerCase());
        }
        return null;
    }*/

    /**
     * @return the shippingPackageStatuses
     */
    public List<String> getShippingPackageStatuses() {
        return shippingPackageStatuses;
    }

    public ShippingPackageStatus getShippingPackageStatusByCode(String statusCode) {
        return codeToShippingPackageStatuses.get(statusCode);
    }

    /**
     * @return the shippingProvidersJson
     */
    public String getShippingProvidersJson() {
        return shippingProvidersJson;
    }

    /**
     * @return the shippingMethodsJson
     */
    public String getShippingMethodsJson() {
        return shippingMethodsJson;
    }

    /**
     * @return the shipmentTrackingStatusJson
     */
    public String getShipmentTrackingStatusJson() {
        return shipmentTrackingStatusJson;
    }

    /**
     * @param shipmentTrackingStatusJson the shipmentTrackingStatusJson to set
     */
    public void setShipmentTrackingStatusJson(String shipmentTrackingStatusJson) {
        this.shipmentTrackingStatusJson = shipmentTrackingStatusJson;
    }

    public PaymentMethod getPaymentMethodByCode(String paymentMethodCode) {
        return codeToPaymentMethods.get(paymentMethodCode);
    }

    public String getPaymentMethodJson() {
        return paymentMethodJson;
    }

    public void setPaymentMethodJson(String paymentMethodJson) {
        this.paymentMethodJson = paymentMethodJson;
    }

    public static class ShipmentTrackingStatusVO implements Serializable {

        private final String code;
        private final String description;

        public ShipmentTrackingStatusVO(ShipmentTrackingStatus status) {
            code = status.getCode();
            description = status.getDescription();
        }

        /**
         * @return the code
         */
        public String getCode() {
            return code;
        }

        /**
         * @return the description
         */
        public String getDescription() {
            return description;
        }

    }

    public void freeze() {
        Map<String, String> codeToProviderNameMap = new HashMap<String, String>();
        for (ShippingProvider provider : shippingProviders) {
            codeToProviderNameMap.put(provider.getCode(), provider.getName());
        }
        shippingProvidersJson = new Gson().toJson(codeToProviderNameMap);

        Map<String, String> codeToMethodNameMap = new HashMap<String, String>();
        for (ShippingMethod method : shippingMethods) {
            codeToMethodNameMap.put(method.getShippingMethodCode(), method.getName());
        }
        shippingMethodsJson = new Gson().toJson(codeToMethodNameMap);

        Map<String, String> codeToTrackingStatusMap = new HashMap<String, String>();
        for (ShipmentTrackingStatusVO shipmentTrackingStatusVo : shipmentTrackingStatusVOs) {
            codeToTrackingStatusMap.put(shipmentTrackingStatusVo.getCode(), shipmentTrackingStatusVo.getDescription());
        }
        shipmentTrackingStatusJson = new Gson().toJson(codeToTrackingStatusMap);

        sourceShippingProviders.addAll(shippingProviders);
        for (String shippingProviderCode : shippingProviderToSources.keySet()) {
            sourceShippingProviders.add(getShippingProviderByCode(shippingProviderCode));
        }

        paymentMethodJson = new Gson().toJson(getPaymentMethodDTOMap());

    }

    public List<PaymentMethod> getPaymentMethods() {
        return paymentMethods;
    }

    @Override @Transactional public void load() {
        try {
            if (ConfigurationManager.getInstance().getConfiguration(ProductConfiguration.class).isCourierManagementSwitchedOff()) {
                loadGlobalShippingProviders();
            } else {
                List<ShippingProvider> shippingProviders = shippingAdminService.getShippingProviders(true);
                for (ShippingProvider shippingProvider : shippingProviders) {
                    addShippingProvider(shippingProvider, shippingProviderService.constructShipmentHandler(shippingProvider));
                    addShipmentTrackingSyncStatuses(shippingProvider.getCode());
                }
            }
            loadShippingMethods();
            loadPaymentMethods();
            loadShippingPackageStatuses();
            loadSourceShippingProviders();
            loadShipmentTrackingStatuses();
        } catch (Exception e) {
            LOG.error("Error loading shipping configuration", e);
            throw e;
        }
    }

    private void addShipmentTrackingSyncStatuses(String shippingProviderCode) {
        ShipmentTrackingSyncStatusDTO syncStatusDTO = shipmentTrackingService.getTrackingSyncStatusByProviderFromDB(shippingProviderCode);
        if (syncStatusDTO == null) {
            syncStatusDTO = new ShipmentTrackingSyncStatusDTO(shippingProviderCode);
        }
        shippingCodeToTrackingSyncStatus.put(shippingProviderCode, syncStatusDTO);
    }

    private void loadGlobalShippingProviders() {
        List<ShippingProviderVO> shippingProviderVOs = globalShippingProviderService.getAllShippingProviders();
        for (ShippingProviderVO shippingProviderVO : shippingProviderVOs) {
            addShippingProviderVO(shippingProviderVO);
        }
    }

    private void loadShippingMethods() {
        List<ShippingMethod> methods = shippingService.getShippingMethods();
        for (ShippingMethod method : methods) {
            addShippingMethod(method);
        }
    }

    private void loadPaymentMethods() {
        List<PaymentMethod> paymentMethods = applicationSetupService.getPaymentMethods();
        for (PaymentMethod method : paymentMethods) {
            addPaymentMethod(method);
        }
    }

    private void loadSourceShippingProviders() {
        List<SourceShippingProviderVO> sourceShippingProvideVos = sourceShippingProviderService.getAllSourceShippingProviders();
        for (SourceShippingProviderVO sourceShippingProviderVO : sourceShippingProvideVos) {
            addSourceShippingProvider(sourceShippingProviderVO);
        }
    }

    /*private void loadShippingProviderRegions() {
        List<ShippingProviderRegion> regions = shippingProviderService.getShippingProviderRegions();
        for (ShippingProviderRegion region : regions) {
            addShippingProviderRegion(region);
        }
    }*/

    private void loadShippingPackageStatuses() {
        List<ShippingPackageStatus> shippingPackageStatuses = shippingService.getShippingPackageStatuses();
        for (ShippingPackageStatus shippingPackageStatus : shippingPackageStatuses) {
            addShippingPackageStatus(shippingPackageStatus);
        }
    }

    private void loadShipmentTrackingStatuses() {
        List<ShipmentTrackingStatus> shipmentTrackingStatuses = shipmentTrackingService.getShipmentTrackingStatuses();
        for (ShipmentTrackingStatus shipmentTrackingStatus : shipmentTrackingStatuses) {
            addShipmentTrackingStatus(shipmentTrackingStatus);
        }
    }

    private Map<String, PaymentMethodDTO> getPaymentMethodDTOMap() {
        Map<String, PaymentMethodDTO> paymentsMap = new HashMap<>();
        for (Entry<String, PaymentMethod> entry : codeToPaymentMethods.entrySet()) {
            paymentsMap.put(entry.getKey(), new PaymentMethodDTO(entry.getValue()));
        }
        return paymentsMap;
    }
}
