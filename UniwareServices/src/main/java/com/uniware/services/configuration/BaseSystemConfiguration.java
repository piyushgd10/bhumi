/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 18, 2012
 *  @author singla
 */
package com.uniware.services.configuration;

import com.unifier.core.annotation.Configuration;
import com.unifier.core.annotation.Level;
import com.unifier.core.configuration.IConfiguration;
import com.unifier.core.entity.SystemConfig;
import com.unifier.core.utils.StringUtils;
import com.unifier.services.application.IApplicationSetupService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author singla
 */
@Configuration(name = "baseSystemConfiguration", level = Level.FACILITY, eager = true)
public class BaseSystemConfiguration implements IConfiguration {

    private final Map<String, String> properties = new ConcurrentHashMap<>();

    @Autowired
    private transient IApplicationSetupService applicationSetupService;

    @Autowired
    private IConfigurationService     configurationService;

    private void addConfiguration(SystemConfig systemConfig) {
        properties.put(systemConfig.getName(), systemConfig.getValue());
    }

    public String getProperty(String name) {
        return properties.get(name);
    }

    public String getProperty(String name, String defaultValue) {
        String value = properties.get(name);
        return value != null ? value : defaultValue;
    }

    @Override
    public void load() {
        List<SystemConfig> systemConfigs = applicationSetupService.getSystemConfigs();
        systemConfigs.forEach(this::addConfiguration);
        systemConfigs.forEach(config -> {
            if (StringUtils.isNotBlank(config.getConditionExpression())) {
                if (!configurationService.evaluateConditionExpression(config, properties)) {
                    throw new IllegalStateException("ConditionExpression evaluation failed. Dependent properties are not correct for: " + config.getName());
                }
            }
        });
    }
}
