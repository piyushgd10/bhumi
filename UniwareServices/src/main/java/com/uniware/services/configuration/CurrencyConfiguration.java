/*
 *  Copyright 2013 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 08-Feb-2013
 *  @author karuns
 */
package com.uniware.services.configuration;

import com.google.gson.Gson;
import com.unifier.core.annotation.Configuration;
import com.unifier.core.annotation.Level;
import com.unifier.core.configuration.IConfiguration;
import com.uniware.core.entity.Currency;
import com.uniware.services.currency.ICurrencyService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Configuration(name = "currencyConfiguration", level = Level.GLOBAL, eager = true)
public class CurrencyConfiguration implements IConfiguration {

    private final List<Currency>         currencies     = new ArrayList<Currency>();
    private final Map<Integer, Currency> idToCurrency   = new ConcurrentHashMap<>();
    private final Map<String, Currency>  codeToCurrency = new ConcurrentHashMap<>();
    private String currenciesJson;

    @Autowired
    private transient ICurrencyService currencyService;

    private void addCurrency(Currency currency) {
        currencies.add(currency);
        idToCurrency.put(currency.getId(), currency);
        codeToCurrency.put(currency.getCode(), currency);
    }

    public Currency getCurrencyByCode(String code) {
        return codeToCurrency.get(code);
    }

    public Currency getCurrencyById(int currencyId) {
        return idToCurrency.get(currencyId);
    }

    public void freeze() {
        this.currenciesJson = new Gson().toJson(codeToCurrency);
    }

    public String getCurrenciesJson() {
        return currenciesJson;
    }

    public void setCurrenciesJson(String currenciesJson) {
        this.currenciesJson = currenciesJson;
    }

    @Override
    public void load() {
        List<Currency> currencies = currencyService.getAllCurrencies();
        for (Currency currency : currencies) {
            addCurrency(currency);
        }
    }

}
