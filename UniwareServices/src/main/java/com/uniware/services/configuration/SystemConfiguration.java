/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 18, 2012
 *  @author singla
 */
package com.uniware.services.configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unifier.core.annotation.Configuration;
import com.unifier.core.annotation.Level;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.configuration.IConfiguration;
import com.unifier.core.expressions.Expression;
import com.unifier.core.utils.StringUtils;
import com.uniware.services.configuration.data.manager.ProductConfiguration;

/**
 * @author singla
 */
@Configuration(name = "systemConfiguration", level = Level.FACILITY, eager = true)
public class SystemConfiguration implements IConfiguration {

    private static final Logger    LOG                                             = LoggerFactory.getLogger(SystemConfiguration.class);
    /**
     *
     */
    private static final String    SINGLE_ACTION_INVOICE_LABEL                     = "single.action.invoice.label";
    private static final String    BARCODING_BEFORE_QUALITY_CHECK                  = "barcoding.before.quality.check";
    private static final String    BARCODING_BEFORE_GRN                            = "barcoding.before.grn";
    private static final String    ITEM_DETAILING_MANDATORY_BEFORE_PUTAWAY         = "item.detailing.mandatory.before.putaway";
    private static final String    PURCHASE_ORDER_ATTACHMENT_NAME_EXPRESSION       = "po.attachment.name.expression";
    private static final String    REPRINT_ITEM_LABEL                              = "reprint.item.label";
    private static final String    RETURN_REPRINT_ITEM_LABEL_AUTOMATIC             = "return.reprint.item.label.automatic";
    private static final String    BARCODE_PRE_GENERATED                           = "barcode.pre.generated";
    private static final String    ALLOW_SINGLE_SKU_MULTI_BATCH_AT_GRN             = "allow.single.sku.multiple.batch.grn";
    private static final String    SHIPMENT_LABEL_CSV_ENABLED                      = "shipment.label.csv.enabled";
    private static final String    VENDOR_PRICES_TAX_EXCLUSIVE                     = "vendor.prices.tax.exclusive";
    private static final String    PREDEFINED_PACKAGES_FLEXIBLE                    = "predefined.packages.flexible";
    private static final String    AUTO_SPLIT_WAIT_HOURS                           = "auto.split.wait.hours";
    private static final String    SHIP_TOGETHER_RULE_EXPRESSION                   = "ship.together.rule.expression";
    private static final String    SEND_EMAIL_ON_PACKAGE_DISPATCH                  = "send.email.on.package.dispatch";
    private static final String    SEND_SMS_ON_PACKAGE_DISPATCH                    = "send.sms.on.package.dispatch";
    private static final String    SELECT_PACKAGE_TYPE_BEFORE_DISPATCH             = "select.package.type.before.dispatch";
    private static final String    ATTACH_INVOICE_PDF_TO_EMAIL_ON_PACKAGE_DISPATCH = "attach.invoice.pdf.to.email.on.package.dispatch";
    private static final String    INVOICE_PDF_NAME_EXPRESSION                     = "invoice.pdf.name.expression";
    private static final String    TRACEABILITY_LEVEL                              = "traceability.level";
    private static final String    INVENTORY_ACCURACY_LEVEL                        = "inventory.accuracy.level";
    private static final String    WAREHOUSE_STORAGE_BIN_MANAGEMENT                = "warehouse.storage.bin.management";
    private static final String    DEFAULT_COUNTRY_CODE                            = "default.country.code";
    private static final String    INVENTORY_MANAGEMENT_SWITCHED_OFF               = "inventory.management.switched.off";
    private static final String    ADD_SHIPPING_PACKAGES_ON_MANIFEST_CREATION      = "add.shipping.packages.on.manifest.creation";
    private static final String    SELLING_PRICES_TAX_EXCLUSIVE                    = "selling.prices.tax.exclusive";
    private static final String    RECONCILE_TOLERANCE_LEVEL                       = "reconcile.tolerance.level";
    private static final String    SOLR_INDEXING_ENABLED                           = "solr.indexing.enabled";
    private static final String    DOWNLOAD_CSV_ON_RECEIVE_PICKLIST                = "download.csv.on.receive.picklist";
    private static final String    SHIPPING_ZONE_EXCEPTION_RULES                   = "shipping.zone.exception.rules";
    private static final String    RESERVED_KEYWORDS_FOR_CHANNEL_CODE              = "reserved.keywords.for.channel.shortname";
    private static final String    SHIPMENT_TRACKING_DAYS_COUNT                    = "shipment.tracking.days.count";
    private static final String    VERIFY_POD_BEFORE_INVOICE                       = "verify.pod.before.invoice";
    private static final String    AVAILABLE_FACILITIES_FROM_THIRD_PARTY_SCRIPT    = "available.facilities.from.third.party.script.name";
    private static final String    RESTRICT_OTP_GENERATION                         = "restrict.otp.generation";
    private static final String    THIRD_PARTY_FETCH_OTP_SCRIPT_NAME               = "third.party.fetch.otp.script.name";
    private static final String    INVENTORY_ADJUSTMENT_VIA_FTP_SCRIPT_NAME        = "inventory.adjustment.via.ftp.script.name";
    private static final String    INVENTORY_AVAILABILITY_LEDGER_ACKNOWLEDGABLE    = "inventory.availability.ledger.acknowledgable";
    private static final String    POS_ENABLED                                     = "pos.enabled";
    private static final String    AUTO_DISABLE_STALE_CATALOG                      = "auto.disable.stale.catalog";
    private static final String    HANDHELD_ENABLED                                = "handheld.enabled";
    private static final String    GATEPASS_INVOICING_REQUIRED                     = "gatepass.invoicing.required";
    private static final String    ITEM_SEQUENCE_SUFFIX_LENGTH                     = "item.sequence.suffix.length";
    private static final String    MAX_ALLOWED_SHIPMENTS_IN_PICKLIST               = "max.allowed.shipments.in.picklist";
    private static final String    PICKING_VIA_HANDHELD_ENABLED                    = "picking.via.handheld.enabled";
    private static final String    AUTOMATIC_SUB_CYCLE_COUNT_ALLOWED               = "automatic.sub.cycle.count.allowed";
    private static final String    CYCLE_COUNT_SHELVES_FROM_ZONES_IN_PARALLEL      = "cycle.count.shelves.from.zones.in.parallel";
    private static final String    SUB_CYCLE_COUNT_MAX_BATCH_SIZE                  = "sub.cycle.count.max.batch.size";
    private static final String    SUB_CYCLE_COUNT_MIN_BATCH_SIZE                  = "sub.cycle.count.min.batch.size";
    private static final String    CYCLE_COUNT_ALLOWED_START_HOUR_OF_THE_DAY       = "cycle.count.allowed.start.hour";
    private static final String    CYCLE_COUNT_ALLOWED_END_HOUR_OF_THE_DAY         = "cycle.count.allowed.end.hour";
    private static final String    MAX_SHELF_BLOCKED_HOURS_FOR_CYCLE_COUNT         = "max.shelf.blocked.hours.for.cycle.count";
    private static final String    ALLOW_MULTIPLE_AGEING_DATE_ON_SHELF             = "allow.multiple.ageing.date.on.shelf";
    private static final String    ALLOW_CROSS_PICK_SET_SHIPMENTS                  = "allow.cross.pick.set.shipments";
    private static final String    ALLOW_TAX_ON_INTERNATIONAL_ORDER                = "allow.tax.on.international.order";
    private static final String    RETURN_INVOICE_OTHER_CHARGES_APPLICABLE         = "return.invoice.other.charges.applicable";
    private static final String    IS_AGEING_FOLLOWED_STRICTLY                     = "is.ageing.followed.strictly";
    private static final String    PACK_ITEMS_BRAND_WISE                           = "pack.items.brand.wise";

    /**
     * If set to true, system will not generate a unique code for each invoice. Shipment code will be used as invoice
     * code instead. This will remove dependency on {@link com.uniware.core.entity.Sequence} lock at the time of invoice
     * generation.
     */
    private static final String    INVOICE_MANAGEMENT_SWITCHED_OFF                 = "invoice.management.switched.off";

    private Expression             purchaseOrderAttachmentNameExpression;
    private Expression             invoicePdfNameExpression;

    private Expression             shipTogetherRuleExpression;
    private TraceabilityLevel      traceabilityLevel                               = TraceabilityLevel.ITEM;
    private InventoryAccuracyLevel inventoryAccuracyLevel                          = InventoryAccuracyLevel.ITEM_TYPE;

    public boolean isPickingViaHandheldEnabled() {
        String property = getProperty(PICKING_VIA_HANDHELD_ENABLED);
        return StringUtils.isNotEmpty(property) && Boolean.parseBoolean(property);
    }

    public String getProperty(String name) {
        BaseSystemConfiguration systemConfiguration = ConfigurationManager.getInstance().getConfiguration(BaseSystemConfiguration.class);
        String value = systemConfiguration.getProperty(name);
        if (value == null) {
            TenantSystemConfiguration tenantSystemConfiguration = ConfigurationManager.getInstance().getConfiguration(TenantSystemConfiguration.class);
            value = tenantSystemConfiguration.getProperty(name);
        }
        return value;
    }

    public String getProperty(String name, String defaultValue) {
        String value = getProperty(name);
        return value != null ? value : defaultValue;
    }

    public boolean isDispatchShippingPackageOnAddingToManifest() {
        return !isAddShippingPackagesOnManifestCreation();
    }

    public int getItemSequenceSuffixLength() {
        return getProperty(ITEM_SEQUENCE_SUFFIX_LENGTH) != null ? Integer.parseInt(getProperty(ITEM_SEQUENCE_SUFFIX_LENGTH)) : 0;
    }

    public boolean isAddShippingPackagesOnManifestCreation() {
        return getProperty(ADD_SHIPPING_PACKAGES_ON_MANIFEST_CREATION) != null && Boolean.parseBoolean(getProperty(ADD_SHIPPING_PACKAGES_ON_MANIFEST_CREATION));
    }

    public boolean isSingleActionInvoiceAndLabel() {
        return getProperty(SINGLE_ACTION_INVOICE_LABEL) != null && Boolean.parseBoolean(getProperty(SINGLE_ACTION_INVOICE_LABEL));
    }

    public boolean isPOSEnabled() {
        return getProperty(POS_ENABLED) != null && Boolean.parseBoolean(getProperty(POS_ENABLED));
    }

    public boolean isAutoDisableStaleCatalog() {
        return getProperty(AUTO_DISABLE_STALE_CATALOG) != null && Boolean.parseBoolean(getProperty(AUTO_DISABLE_STALE_CATALOG));
    }

    public boolean isVerifyPODBeforeInvoice() {
        return getProperty(VERIFY_POD_BEFORE_INVOICE) != null && Boolean.parseBoolean(getProperty(VERIFY_POD_BEFORE_INVOICE));
    }

    public String getAvailableFacilitiesFromThirdPartyScriptName() {
        return getProperty(AVAILABLE_FACILITIES_FROM_THIRD_PARTY_SCRIPT);
    }

    public String getOTPFromThirdPartyScriptName() {
        return getProperty(THIRD_PARTY_FETCH_OTP_SCRIPT_NAME);
    }

    public String getInventoryAdjustmentViaFTPScriptName() {
        return getProperty(INVENTORY_ADJUSTMENT_VIA_FTP_SCRIPT_NAME);
    }

    public boolean isBarcodePreGenerated() {
        return getProperty(BARCODE_PRE_GENERATED) != null && Boolean.parseBoolean(getProperty(BARCODE_PRE_GENERATED));
    }

    public boolean isBarcodingBeforeQualityCheck() {
        return getProperty(BARCODING_BEFORE_QUALITY_CHECK) != null && Boolean.parseBoolean(getProperty(BARCODING_BEFORE_QUALITY_CHECK));
    }

    public boolean isBarcodingBeforeGRN() {
        return getProperty(BARCODING_BEFORE_GRN) != null && Boolean.parseBoolean(getProperty(BARCODING_BEFORE_GRN));
    }

    public boolean isItemDetailingMandatoryBeforePutaway() {
        return getProperty(ITEM_DETAILING_MANDATORY_BEFORE_PUTAWAY) != null && Boolean.parseBoolean(getProperty(ITEM_DETAILING_MANDATORY_BEFORE_PUTAWAY));
    }

    public boolean isAllowSingleSkuMultiBatchAtGRN() {
        return getProperty(ALLOW_SINGLE_SKU_MULTI_BATCH_AT_GRN) != null && Boolean.parseBoolean(getProperty(ALLOW_SINGLE_SKU_MULTI_BATCH_AT_GRN));
    }

    public boolean isReturnInvoiceOtherChargesApplicable(){
        return getProperty(RETURN_INVOICE_OTHER_CHARGES_APPLICABLE) != null && Boolean.parseBoolean(getProperty(RETURN_INVOICE_OTHER_CHARGES_APPLICABLE));
    }

    public boolean isInventoryManagementSwitchedOff() {
        if (ConfigurationManager.getInstance().getConfiguration(ProductConfiguration.class).isInventoryManagementSwitchedOff()) {
            return true;
        }
        String value = getProperty(INVENTORY_MANAGEMENT_SWITCHED_OFF);
        if (value != null) {
            return Boolean.parseBoolean(value);
        }
        return false;
    }

    public boolean isInventoryAvailabilityLedgerAcknowledgable() {
        return getProperty(INVENTORY_AVAILABILITY_LEDGER_ACKNOWLEDGABLE) != null && Boolean.parseBoolean(getProperty(INVENTORY_AVAILABILITY_LEDGER_ACKNOWLEDGABLE));
    }

    /**
     * @return
     */
    public Expression getPurchaseOrderAttachmentNameExpression() {
        if (purchaseOrderAttachmentNameExpression == null) {
            String expressionText = getProperty(PURCHASE_ORDER_ATTACHMENT_NAME_EXPRESSION);
            if (StringUtils.isEmpty(expressionText)) {
                expressionText = "#{#purchaseOrder.code}";
            }
            purchaseOrderAttachmentNameExpression = Expression.compile(expressionText);
        }
        return purchaseOrderAttachmentNameExpression;
    }

    public boolean isReprintItemLabel() {
        return getProperty(REPRINT_ITEM_LABEL) != null && Boolean.parseBoolean(getProperty(REPRINT_ITEM_LABEL));
    }

    public boolean isReturnReprintItemLabelAutomatic() {
        return getProperty(RETURN_REPRINT_ITEM_LABEL_AUTOMATIC) != null && Boolean.parseBoolean(getProperty(RETURN_REPRINT_ITEM_LABEL_AUTOMATIC));
    }

    public boolean isShipmentLabelCSVEnabled() {
        return getProperty(SHIPMENT_LABEL_CSV_ENABLED) != null && Boolean.parseBoolean(getProperty(SHIPMENT_LABEL_CSV_ENABLED));
    }

    /**
     * @return
     */
    public boolean isVendorPricesTaxExclusive() {
        return getProperty(VENDOR_PRICES_TAX_EXCLUSIVE) != null && Boolean.parseBoolean(getProperty(VENDOR_PRICES_TAX_EXCLUSIVE));
    }

    public boolean isPredefinedPackagesFlexible() {
        return getProperty(PREDEFINED_PACKAGES_FLEXIBLE) != null && Boolean.parseBoolean(getProperty(PREDEFINED_PACKAGES_FLEXIBLE));
    }

    public int getAutoSplitWaitHours() {
        return getProperty(AUTO_SPLIT_WAIT_HOURS) != null ? Integer.parseInt(getProperty(AUTO_SPLIT_WAIT_HOURS).trim()) : 0;
    }

    public double getReconcileToleranceLevel() {
        return getProperty(RECONCILE_TOLERANCE_LEVEL) != null ? Double.parseDouble(getProperty(RECONCILE_TOLERANCE_LEVEL).trim()) : 0.00;
    }

    public Expression getShipTogetherRuleExpression() {
        return shipTogetherRuleExpression;

    }

    public boolean isSendEmailOnPackageDispatch() {
        return getProperty(SEND_EMAIL_ON_PACKAGE_DISPATCH) != null && Boolean.parseBoolean(getProperty(SEND_EMAIL_ON_PACKAGE_DISPATCH));
    }

    public boolean isSendSmsOnPackageDispatch() {
        return getProperty(SEND_SMS_ON_PACKAGE_DISPATCH) != null && Boolean.parseBoolean(getProperty(SEND_SMS_ON_PACKAGE_DISPATCH));
    }

    public boolean isSolrIndexingEnabled() {
        return getProperty(SOLR_INDEXING_ENABLED) != null && Boolean.parseBoolean(getProperty(SOLR_INDEXING_ENABLED));
    }

    public boolean isSelectPackageTypeBeforeDispatch() {
        return getProperty(SELECT_PACKAGE_TYPE_BEFORE_DISPATCH) != null && Boolean.parseBoolean(getProperty(SELECT_PACKAGE_TYPE_BEFORE_DISPATCH));
    }

    public boolean isAttachInvoicePdfToEmailOnPackageDispatch() {
        return getProperty(ATTACH_INVOICE_PDF_TO_EMAIL_ON_PACKAGE_DISPATCH) != null && Boolean.parseBoolean(getProperty(ATTACH_INVOICE_PDF_TO_EMAIL_ON_PACKAGE_DISPATCH));
    }

    public boolean isWarehouseStorageBinManagement() {
        return getProperty(WAREHOUSE_STORAGE_BIN_MANAGEMENT) == null || Boolean.parseBoolean(getProperty(WAREHOUSE_STORAGE_BIN_MANAGEMENT));
    }

    public boolean isRestrictOTPGeneration() {
        return getProperty(RESTRICT_OTP_GENERATION) != null && Boolean.parseBoolean(getProperty(RESTRICT_OTP_GENERATION));
    }

    public String getDeafultCountry() {
        return getProperty(DEFAULT_COUNTRY_CODE) != null ? getProperty(DEFAULT_COUNTRY_CODE) : "IN";
    }

    public String getShippingZoneExceptions() {
        return getProperty(SHIPPING_ZONE_EXCEPTION_RULES);
    }

    /**
     * @return
     */
    public Expression getInvoicePdfNameExpression() {
        if (invoicePdfNameExpression == null) {
            String expressionText = getProperty(INVOICE_PDF_NAME_EXPRESSION);
            if (StringUtils.isEmpty(expressionText)) {
                expressionText = "#{#invoice.code}";
            }
            invoicePdfNameExpression = Expression.compile(expressionText);
        }
        return invoicePdfNameExpression;
    }

    public TraceabilityLevel getTraceabilityLevel() {
        return traceabilityLevel;
    }

    public InventoryAccuracyLevel getInventoryAccuracyLevel() {
        return inventoryAccuracyLevel;
    }

    public boolean isSellingPricesTaxExclusive() {
        return getProperty(SELLING_PRICES_TAX_EXCLUSIVE) != null && Boolean.parseBoolean(getProperty(SELLING_PRICES_TAX_EXCLUSIVE));
    }

    public boolean downloadCsvOnReceivePicklist() {
        return getProperty(DOWNLOAD_CSV_ON_RECEIVE_PICKLIST) != null && Boolean.parseBoolean(getProperty(DOWNLOAD_CSV_ON_RECEIVE_PICKLIST));
    }

    public String getReservedKeywordsForChannelCode() {
        return getProperty(RESERVED_KEYWORDS_FOR_CHANNEL_CODE);
    }

    public int getShipmentTrackingDaysCount() {
        return getProperty(SHIPMENT_TRACKING_DAYS_COUNT) != null ? Integer.parseInt(getProperty(SHIPMENT_TRACKING_DAYS_COUNT)) : 30;
    }

    public boolean isHandheldEnabled() {
        return getProperty(HANDHELD_ENABLED) != null && Boolean.parseBoolean(getProperty(HANDHELD_ENABLED));
    }

    public boolean isGatepassInvoicingRequired() {
        String gatepassInvoicingRequired = getProperty(GATEPASS_INVOICING_REQUIRED);
        return StringUtils.isNotBlank(gatepassInvoicingRequired) && Boolean.parseBoolean(gatepassInvoicingRequired);
    }

    public boolean isCrossPickSetShipmentsAllowed() {
        String value = getProperty(ALLOW_CROSS_PICK_SET_SHIPMENTS);
        return StringUtils.isNotBlank(value) && Boolean.parseBoolean(value);
    }

    public void freeze() {
        String expressionText = getProperty(SHIP_TOGETHER_RULE_EXPRESSION);
        if (StringUtils.isNotBlank(expressionText)) {
            shipTogetherRuleExpression = Expression.compile(expressionText);
        }
        String strTraceabilityLevel = getProperty(TRACEABILITY_LEVEL);
        if (StringUtils.isNotBlank(strTraceabilityLevel)) {
            try {
                traceabilityLevel = TraceabilityLevel.valueOf(strTraceabilityLevel.toUpperCase());
            } catch (IllegalArgumentException e) {
                LOG.error("invalid value for traceability.level:" + strTraceabilityLevel);
            }
        }

        String strInventoryAccuracyLevel = getProperty(INVENTORY_ACCURACY_LEVEL);
        if (StringUtils.isNotBlank(strInventoryAccuracyLevel)) {
            try {
                inventoryAccuracyLevel = InventoryAccuracyLevel.valueOf(strInventoryAccuracyLevel.toUpperCase());
            } catch (IllegalArgumentException e) {
                LOG.error("invalid value for inventory.accuracy.level:" + strInventoryAccuracyLevel);
            }
        }
    }

    public int getMaxAllowedShipmentsInPicklist() {
        String maxAllowedShipmentsInPicklist = getProperty(MAX_ALLOWED_SHIPMENTS_IN_PICKLIST);
        return StringUtils.isNotBlank(maxAllowedShipmentsInPicklist) ? Integer.parseInt(maxAllowedShipmentsInPicklist) : 25;
    }

    /**
     * False by default.
     *
     * @see SystemConfiguration#INVOICE_MANAGEMENT_SWITCHED_OFF
     * @return
     */
    public boolean isInvoiceManagementSwitchedOff() {
        String invoiceManagmentSwitchedOff = getProperty(INVOICE_MANAGEMENT_SWITCHED_OFF);
        return StringUtils.isNotBlank(invoiceManagmentSwitchedOff) && Boolean.parseBoolean(invoiceManagmentSwitchedOff);
    }

    @Override
    public void load() {

    }

    public boolean isAutomaticSubCycleCountAllowed() {
        return getProperty(AUTOMATIC_SUB_CYCLE_COUNT_ALLOWED) == null || Boolean.parseBoolean(getProperty(AUTOMATIC_SUB_CYCLE_COUNT_ALLOWED));
    }

    public boolean isAddShelvesToSubCycleCountFromShelvesInParallel() {
        return getProperty(CYCLE_COUNT_SHELVES_FROM_ZONES_IN_PARALLEL) != null && Boolean.parseBoolean(getProperty(CYCLE_COUNT_SHELVES_FROM_ZONES_IN_PARALLEL));
    }

    public int getSubCycleCountMaxBatchSize() {
        return getProperty(SUB_CYCLE_COUNT_MAX_BATCH_SIZE) != null ? Integer.parseInt(getProperty(SUB_CYCLE_COUNT_MAX_BATCH_SIZE)) : 200;
    }

    public int getSubCycleCountMinBatchSize() {
        return getProperty(SUB_CYCLE_COUNT_MIN_BATCH_SIZE) != null ? Integer.parseInt(getProperty(SUB_CYCLE_COUNT_MIN_BATCH_SIZE)) : 50;
    }

    public int getAllowedStartHourForCycleCount() {
        return getProperty(CYCLE_COUNT_ALLOWED_START_HOUR_OF_THE_DAY) != null ? Integer.parseInt(getProperty(CYCLE_COUNT_ALLOWED_START_HOUR_OF_THE_DAY)) : 0;
    }

    public int getAllowedEndHourForCycleCount() {
        return getProperty(CYCLE_COUNT_ALLOWED_END_HOUR_OF_THE_DAY) != null ? Integer.parseInt(getProperty(CYCLE_COUNT_ALLOWED_END_HOUR_OF_THE_DAY)) : 0;
    }

    public Integer getMaxAllowedShelfBlockedHours() {
        return getProperty(MAX_SHELF_BLOCKED_HOURS_FOR_CYCLE_COUNT) != null ? Integer.parseInt(getProperty(MAX_SHELF_BLOCKED_HOURS_FOR_CYCLE_COUNT).trim()) : 2;
    }

    public boolean isAllowMultipleAgeingDateOnShelf() {
        return getProperty(ALLOW_MULTIPLE_AGEING_DATE_ON_SHELF) != null && Boolean.parseBoolean(getProperty(ALLOW_MULTIPLE_AGEING_DATE_ON_SHELF));
    }

    public boolean isTaxOnInternationalOrderAllowed() {
        String value = getProperty(ALLOW_TAX_ON_INTERNATIONAL_ORDER);
        return StringUtils.isNotBlank(value) && Boolean.parseBoolean(value);
    }

    public boolean isAgeingFollowedStrictly() {
        return getProperty(IS_AGEING_FOLLOWED_STRICTLY) != null && Boolean.parseBoolean(getProperty(IS_AGEING_FOLLOWED_STRICTLY));
    }

    public boolean isPackItemsBrandWiseEnabled() {
        return getProperty(PACK_ITEMS_BRAND_WISE) != null && Boolean.parseBoolean(getProperty(PACK_ITEMS_BRAND_WISE));
    }

    public enum TraceabilityLevel {
        NONE,
        ITEM_SKU,
        ITEM
    }

    public enum InventoryAccuracyLevel {
        BATCH,
        SHELF,
        ITEM_TYPE
    }

}
