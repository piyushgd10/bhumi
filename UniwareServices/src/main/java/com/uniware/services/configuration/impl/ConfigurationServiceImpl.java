/*
 * Copyright 2016 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 9/10/16 3:54 PM
 * @author amdalal
 */

package com.uniware.services.configuration.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.unifier.core.annotation.Level;
import com.unifier.core.api.application.EditSystemConfigurationRequest;
import com.unifier.core.api.application.EditSystemConfigurationResponse;
import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.entity.SystemConfig;
import com.unifier.core.expressions.Expression;
import com.unifier.core.utils.StringUtils;
import com.unifier.dao.application.IApplicationSetupDao;
import com.unifier.services.aspect.MarkDirty;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.services.configuration.BaseSystemConfiguration;
import com.uniware.services.configuration.IConfigurationService;
import com.uniware.services.configuration.SystemConfiguration;

@Service(value = "configurationService")
public class ConfigurationServiceImpl implements IConfigurationService {

    @Autowired
    private IApplicationSetupDao applicationSetupDao;

    @Override
    public boolean evaluateConditionExpression(SystemConfig systemConfig, Map<String, String> properties, String expectedOutput) {
        Expression expression = Expression.compile(systemConfig.getConditionExpression());
        Map<String, Object> props = new HashMap<>();
        props.put("properties", properties);
        return String.valueOf(expression.evaluate(props)).equals(expectedOutput);
    }

    @Override
    public boolean evaluateConditionExpression(SystemConfig systemConfig, Map<String, String> properties) {
        return evaluateConditionExpression(systemConfig, properties, "true");
    }
}
