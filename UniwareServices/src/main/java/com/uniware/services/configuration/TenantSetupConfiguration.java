/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 19-Feb-2014
 *  @author harsh
 */
package com.uniware.services.configuration;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ThreadLocalRandom;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.unifier.core.annotation.Configuration;
import com.unifier.core.api.myaccount.CreateAccountResponse;
import com.unifier.core.api.user.CreateUserRequest;
import com.unifier.core.api.user.CreateUserResponse;
import com.unifier.core.api.validation.ResponseCode;
import com.unifier.core.api.validation.WsWarning;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.cache.data.manager.ICacheDataManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.configuration.IConfiguration;
import com.unifier.core.entity.ApiUser;
import com.unifier.core.entity.Role.Code;
import com.unifier.core.entity.User;
import com.unifier.core.utils.DateUtils;
import com.unifier.services.aspect.RollbackOnFailure;
import com.unifier.services.job.IJobService;
import com.unifier.services.printing.IGlobalPrintConfigService;
import com.unifier.services.tenantprofile.service.ITenantProfileService;
import com.unifier.services.users.IUsersService;
import com.unifier.services.vo.TenantProfileVO;
import com.unifier.services.vo.TenantSetupPropertyVO;
import com.uniware.core.api.channel.AddChannelRequest;
import com.uniware.core.api.channel.AddChannelResponse;
import com.uniware.core.api.channel.WsChannel;
import com.uniware.core.api.facility.CreateFacilityRequest;
import com.uniware.core.api.facility.CreateFacilityResponse;
import com.uniware.core.api.model.WSShippingMethod;
import com.uniware.core.api.model.WsShippingProviderDetail;
import com.uniware.core.api.shipping.AddShippingProviderRequest;
import com.uniware.core.api.shipping.AddShippingProviderResponse;
import com.uniware.core.api.tax.CreateEditTaxTypesConfigurationRequest;
import com.uniware.core.api.tax.CreateTaxTypeConfigurationResponse;
import com.uniware.core.api.tax.WsTaxTypeConfiguration;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.cache.LocationCache;
import com.uniware.core.entity.Facility;
import com.uniware.core.entity.Product;
import com.uniware.core.entity.Sequence;
import com.uniware.core.entity.Sequence.Level;
import com.uniware.core.entity.Sequence.Name;
import com.uniware.core.entity.ShippingMethod;
import com.uniware.core.entity.ShippingProvider;
import com.uniware.core.entity.ShippingProviderMethod;
import com.uniware.core.entity.ShippingProviderSource;
import com.uniware.core.entity.Source;
import com.uniware.core.entity.State;
import com.uniware.core.entity.Tenant;
import com.uniware.core.entity.TenantSetupState;
import com.uniware.core.utils.UserContext;
import com.uniware.core.vo.PrintTemplateVO;
import com.uniware.core.vo.SamplePrintTemplateVO;
import com.uniware.core.vo.ScriptVersionVO;
import com.uniware.core.vo.UserProfileVO;
import com.uniware.dao.tenant.ITenantSetupDao;
import com.uniware.dao.tenant.ITenantSetupMao;
import com.uniware.dao.user.notification.IUserProfileMao;
import com.uniware.services.accounts.IAccountService;
import com.uniware.services.cache.ScriptVersionedCache;
import com.uniware.services.channel.IChannelService;
import com.uniware.services.common.ISequenceGenerator;
import com.uniware.services.reload.IReloadService;
import com.uniware.services.script.mao.IScriptMao;
import com.uniware.services.shipping.IShippingAdminService;
import com.uniware.services.tax.ITaxTypeService;
import com.uniware.services.tenant.ITenantSetupService;
import com.uniware.services.tenant.setup.AbstractTenantSetupAction;
import com.uniware.services.tenant.setup.ImpersonateTenant;
import com.uniware.services.tenant.setup.TenantSetupContext;
import com.uniware.services.warehouse.IFacilityService;

@Configuration(name = "tenantSetupConfiguration", level = com.unifier.core.annotation.Level.GLOBAL, eager = true)
public class TenantSetupConfiguration implements IConfiguration {

    private static final Logger                 LOG              = LoggerFactory.getLogger(TenantSetupConfiguration.class);

    @Autowired
    private transient ITenantSetupDao           tenantSetupDao;

    @Autowired
    private transient ISequenceGenerator        sequenceGenerator;

    @Autowired
    private transient ICacheDataManager         cacheDataManager;

    @Autowired
    private transient IUsersService             usersService;

    @Autowired
    private transient ITenantSetupMao           tenantSetupConfigurationMao;

    @Autowired
    private transient IFacilityService          facilityService;

    @Autowired
    private transient ITenantSetupMao           tenantSetupMao;

    @Autowired
    private transient ITenantProfileService     tenantProfileService;

    @Autowired
    private transient IAccountService           accountService;

    @Autowired
    private transient IChannelService           channelService;

    @Autowired
    private transient IShippingAdminService     shippingAdminService;

    @Autowired
    private transient ITenantSetupService       tenantSetupService;

    @Autowired
    private transient IScriptMao                scriptMao;

    @Autowired
    private transient IReloadService            reloadService;

    @Autowired
    private IUserProfileMao                     userProfileMao;

    @Autowired
    private ITaxTypeService                     taxTypeService;

    @Autowired
    private           IJobService               jobService;

    @Autowired
    private           IGlobalPrintConfigService globalPrintConfigService;

    private final Map<String, TenantSetupState> setupCodeToState = new ConcurrentHashMap<>();

    //list of tenant state codes in ascending order of their position
    private final List<String>                  setupCodeList    = new ArrayList<>();

    public enum TenantStates {
        DONE,
        INIT,
        TAX_TYPE,
        ROLE,
        ROLE_ACCESS_RESOURCE,
        SAMPLE_PRINT_TEMPLATE,
        PRINT_TEMPLATE,
        IMPORT_JOB_TYPE,
        EXPORT_JOB_TYPE,
        TASK,
        TASK_PARAMETER,
        CHANNEL,
        SHIPPING_SOURCE,
        EMAIL_TEMPLATE,
        SMS_TEMPLATE,
        SYSTEM_CONFIGURATION,
        SCRIPT_CONFIG,
        ITEM_DETAIL_FIELD,
        CATEGORY,
        ITEM_TYPE,
        USER_DATATABLE_VIEW,
        USER_NOTIFICATION_TYPE,
        CREATE_USERS,
        SEQUENCE_GENERATION,
        FACILITY_CREATION,
        ACCOUNT_CREATION,
        ACTIVATE_TENANT
    }

    @Override
    public void load() {
        for (TenantSetupState tss : cacheDataManager.getTenantSetupStateMappings()) {
            setupCodeToState.put(tss.getCode(), tss);
            setupCodeList.add(tss.getCode());
        }
    }

    @SuppressWarnings("serial")
    Map<TenantStates, AbstractTenantSetupAction> actionMappings = new HashMap<TenantStates, AbstractTenantSetupAction>() {
        {
            put(TenantStates.TAX_TYPE, new TaxConfigurationSetup());
            put(TenantStates.ROLE, new RolesConfigurationSetup());
            put(TenantStates.SAMPLE_PRINT_TEMPLATE, new SamplePrintTemplateSetup());
            put(TenantStates.PRINT_TEMPLATE, new PrintTemplateSetup());
            put(TenantStates.IMPORT_JOB_TYPE, new ImportJobTypeSetup());
            put(TenantStates.EXPORT_JOB_TYPE, new ExportJobTypeSetup());
            put(TenantStates.TASK, new TaskAction());
            put(TenantStates.CHANNEL, new ChannelAction());
            put(TenantStates.SHIPPING_SOURCE, new ShippingSourceAction());
            put(TenantStates.EMAIL_TEMPLATE, new EmailTemplateSetup());
            put(TenantStates.SMS_TEMPLATE, new SmsTemplateSetup());
            put(TenantStates.SYSTEM_CONFIGURATION, new SystemConfigurationSetup());
            put(TenantStates.SCRIPT_CONFIG, new ScriptConfigSetup());
            put(TenantStates.ITEM_DETAIL_FIELD, new ItemDetailFieldSetup());
            put(TenantStates.CATEGORY, new DefaultCategorySetup());
            put(TenantStates.ITEM_TYPE, new DefaultItemTypeSetup());
            put(TenantStates.USER_DATATABLE_VIEW, new UserDatatableViewSetup());
            put(TenantStates.CREATE_USERS, new UsersCreationAction());
            put(TenantStates.SEQUENCE_GENERATION, new SequenceGenerationSetup());
            put(TenantStates.FACILITY_CREATION, new FacilityCreationSetup());
            put(TenantStates.ACCOUNT_CREATION, new AccountCreationSetup());
            put(TenantStates.ACTIVATE_TENANT, new ActivateTenantAction());
            put(TenantStates.DONE, new CompletionAction());
        }
    };

    /**
     * Finds next action to be executed after action with given "statusCode"
     *
     * @param statusCode code for current state of tenant
     * @return action to be executed next, returns null if there is no further action to execute
     */
    public AbstractTenantSetupAction getNextAction(String statusCode) {
        TenantSetupState nextState = getNextStateByOrder(statusCode);
        if (null != nextState) {
            return actionMappings.get(TenantStates.valueOf(nextState.getCode()));
        } else {
            return null;
        }

    }

    public TenantSetupState getSetupStateByCode(String code) {
        return setupCodeToState.get(code);
    }

    /**
     * @param status tenant status
     * @return next state
     */
    public TenantSetupState getNextStateByOrder(String status) {
        int index = setupCodeList.indexOf(status);
        return (index == -1 || index == setupCodeList.size() - 1) ? null : getSetupStateByCode(setupCodeList.get(index + 1));

    }

    @ImpersonateTenant
    private class TaxConfigurationSetup extends AbstractTenantSetupAction {
        @Override
        @Transactional
        public void setup(TenantSetupContext context) {
            CreateEditTaxTypesConfigurationRequest taxTypeRequest = createTaxTypeRequest("DEFAULT", "Default");
            List<State> states = CacheManager.getInstance().getCache(LocationCache.class).getStatesByCountryCode(LocationCache.COUNTRY_CODE_INDIA);
            for (State state : states) {
                WsTaxTypeConfiguration wsTaxTypeConfiguration = new WsTaxTypeConfiguration(state.getIso2Code(), "DEFAULT");
                wsTaxTypeConfiguration.addTaxTypeConfigurationRange(new WsTaxTypeConfiguration.WSTaxTypeConfigurationRange(BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO,
                        BigDecimal.ZERO, BigDecimal.ZERO));
                taxTypeRequest.addTaxTypeConfiguration(wsTaxTypeConfiguration);
            }
            CreateTaxTypeConfigurationResponse response = taxTypeService.createTaxTypeConfiguration(taxTypeRequest);
            context.addErrors(response.getErrors());
            if (!context.hasErrors()) {
                CreateEditTaxTypesConfigurationRequest taxTypeRequestForGst = createTaxTypeRequest("GST_SERVICE_CHARGE", "GST_SERVICE_CHARGE");
                taxTypeRequestForGst.setGst(true);
                WsTaxTypeConfiguration wsTaxTypeConfiguration = new WsTaxTypeConfiguration(null, "GST_SERVICE_CHARGE");
                wsTaxTypeConfiguration.addTaxTypeConfigurationRange(
                        new WsTaxTypeConfiguration.WSTaxTypeConfigurationRange(BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.valueOf(-1), BigDecimal.valueOf(-1),
                                BigDecimal.valueOf(-1), BigDecimal.valueOf(-1), BigDecimal.valueOf(-1), BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.valueOf(999999999.99)));
                taxTypeRequestForGst.addTaxTypeConfiguration(wsTaxTypeConfiguration);
                CreateTaxTypeConfigurationResponse responseforGst = taxTypeService.createTaxTypeConfiguration(taxTypeRequestForGst);
                context.addErrors(responseforGst.getErrors());
            }

        }

        private CreateEditTaxTypesConfigurationRequest createTaxTypeRequest(String code, String name) {
            CreateEditTaxTypesConfigurationRequest taxTypeRequest = new CreateEditTaxTypesConfigurationRequest();
            taxTypeRequest.setEdit(false);
            taxTypeRequest.setTaxTypeCode(code);
            taxTypeRequest.setTaxTypeName(name);
            return taxTypeRequest;
        }
    }

    private class RolesConfigurationSetup extends AbstractTenantSetupAction {

        @Override
        @Transactional
        @RollbackOnFailure
        public void setup(TenantSetupContext context) {
            boolean status = tenantSetupDao.tenantSetupUpdate(TenantStates.ROLE.name(), context.getTenantToSetup().getId(), "code != \"" + Code.TENANT_MANAGER.name() + "\"")
                    && tenantSetupDao.tenantSetupUpdate(TenantStates.ROLE_ACCESS_RESOURCE.name(), "role", "code", context.getTenantToSetup().getId());
            if (!status) {
                context.addError(WsResponseCode.ERROR_SETTING_UP_TENANT, "Error setting up Roles configuration");
            }
        }

    }

    private class SamplePrintTemplateSetup extends AbstractTenantSetupAction {

        @Override
        public void setup(TenantSetupContext context) {
            List<SamplePrintTemplateVO> sampleTemplateList = tenantSetupMao.getSamplePrintTemplates();
            if (!sampleTemplateList.isEmpty()) {
                Tenant currTenant = UserContext.current().getTenant();
                try {
                    UserContext.current().setTenant(context.getTenantToSetup());
                    tenantSetupMao.removeSamplePrintTemplates(context.getTenantToSetup().getCode());
                    for (SamplePrintTemplateVO sampleTemplate : sampleTemplateList) {
                        sampleTemplate.setId(null);
                        sampleTemplate.setTenantCode(context.getTenantToSetup().getCode());
                        sampleTemplate.setCreated(DateUtils.getCurrentTime());
                        sampleTemplate.setUpdated(DateUtils.getCurrentTime());
                        tenantSetupMao.insertSamplePrintTemplate(sampleTemplate);
                    }
                } finally {
                    UserContext.current().setTenant(currTenant);
                }
            } else {
                context.addError(WsResponseCode.ERROR_SETTING_UP_TENANT, "No data found for updating collection : samplePrintTemplate");
            }
        }
    }

    private class PrintTemplateSetup extends AbstractTenantSetupAction {
        @Override
        public void setup(TenantSetupContext context) {
            tenantSetupMao.removePrintTemplates(context.getTenantToSetup().getCode());
            List<SamplePrintTemplateVO> sampleTemplateList = tenantSetupMao.getDefaultSamplePrintTemplates();
            if (!sampleTemplateList.isEmpty()) {
                Tenant currTenant = UserContext.current().getTenant();
                try {
                    UserContext.current().setTenant(context.getTenantToSetup());
                    for (SamplePrintTemplateVO sampleTemplate : sampleTemplateList) {
                        PrintTemplateVO printTemplate = new PrintTemplateVO();
                        printTemplate.setTenantCode(context.getTenantToSetup().getCode());
                        printTemplate.setType(sampleTemplate.getType());
                        printTemplate.setSamplePrintTemplateCode(sampleTemplate.getCode());
                        printTemplate.setEnabled(true);
                        printTemplate.setCreated(DateUtils.getCurrentTime());
                        printTemplate.setUpdated(DateUtils.getCurrentTime());
                        tenantSetupMao.insertPrintTemplate(printTemplate);
                    }
                } finally {
                    UserContext.current().setTenant(currTenant);
                }
            } else {
                context.addError(WsResponseCode.ERROR_SETTING_UP_TENANT, "No data found for updating collection : printTemplate");
            }

            List<SamplePrintTemplateVO> globalSamplePrintTemplates = tenantSetupMao.getDefaultGlobalSamplePrintTemplates();
            if(!globalSamplePrintTemplates.isEmpty()){
                Tenant currTenant = UserContext.current().getTenant();
                try {
                    UserContext.current().setTenant(context.getTenantToSetup());
                    for(SamplePrintTemplateVO globalSamplePrintTemplate : globalSamplePrintTemplates) {
                        PrintTemplateVO printTemplate = new PrintTemplateVO();
                        printTemplate.setType(globalSamplePrintTemplate.getType());
                        printTemplate.setSamplePrintTemplateCode(globalSamplePrintTemplate.getCode());
                        printTemplate.setEnabled(true);
                        printTemplate.setCreated(DateUtils.getCurrentTime());
                        printTemplate.setUpdated(DateUtils.getCurrentTime());
                        tenantSetupMao.insertPrintTemplate(printTemplate);
                    }
                } finally {
                    UserContext.current().setTenant(currTenant);
                }
            }
        }
    }

    private class ImportJobTypeSetup extends AbstractTenantSetupAction {
        @Override
        @Transactional
        public void setup(TenantSetupContext context) {
            String table = TenantStates.IMPORT_JOB_TYPE.name();
            boolean status = tenantSetupDao.tenantSetupUpdate(table, context.getTenantToSetup().getId(), "");
            if (!status) {
                context.addError(WsResponseCode.ERROR_SETTING_UP_TENANT, "Error executing updates in table : " + table);
            }
        }
    }

    private class ExportJobTypeSetup extends AbstractTenantSetupAction {
        @Override
        @Transactional
        public void setup(TenantSetupContext context) {
            String table = TenantStates.EXPORT_JOB_TYPE.name();
            boolean status = tenantSetupDao.tenantSetupUpdate(table, context.getTenantToSetup().getId(), "");
            if (!status) {
                context.addError(WsResponseCode.ERROR_SETTING_UP_TENANT, "Error executing updates in table : " + table);
            }
        }
    }

    private class TaskAction extends AbstractTenantSetupAction {

        @Override
        public void setup(TenantSetupContext context) {
            //            List<Job> baseTenantJobs = jobService.getRecurrentJobsForTenant(UserContext.current().getTenant().getCode());
            //            Tenant baseTenant = UserContext.current().getTenant();
            //            try {
            //                UserContext.current().setTenant(context.getTenantToSetup());
            //                jobService.removeAllJobsForTenant();
            //                for (Job job : baseTenantJobs) {
            //                    job.setId(null);
            //                    job.setCode(context.getTenantToSetup().getCode() + "-" + job.getName());
            //                    job.setTenantCode(context.getTenantToSetup().getCode());
            //                    job.setEndTime(null);
            //                    job.setLastExecResult(null);
            //                    job.setLastExecTime(null);
            //                    job.setCronExpression(randomizeCronExpression(job.getCronExpression()));
            //                    Date now = DateUtils.getCurrentTime();
            //                    job.setCreated(now);
            //                    job.setUpdated(now);
            //                    jobService.saveJob(job);
            //                }
            //            } finally {
            //                UserContext.current().setTenant(baseTenant);
            //            }
        }

        /**
         * Changes "XX *\/2 * * * ?" to "YY *\/2 * * * ?"
         *
         * @param fromCronExpression cron expression to randomize
         * @return
         */
        private String randomizeCronExpression(String fromCronExpression) {
            // set seconds as a random number between 0 and 59
            return String.valueOf(ThreadLocalRandom.current().nextInt(0, 60)) + fromCronExpression.substring(fromCronExpression.indexOf(" "));
        }
    }

    @ImpersonateTenant
    private class ChannelAction extends AbstractTenantSetupAction {
        @Override
        public void setup(TenantSetupContext context) {
            AddChannelRequest addChannelRequest = new AddChannelRequest();
            Source customSource = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(com.uniware.core.entity.Source.Code.CUSTOM.name());
            WsChannel channel = new WsChannel();
            channel.setChannelName(customSource.getName());
            channel.setSourceCode(com.uniware.core.entity.Source.Code.CUSTOM.name());
            channel.setEnabled(true);
            addChannelRequest.setWsChannel(channel);
            AddChannelResponse response = channelService.addChannel(addChannelRequest);
            if (!response.isSuccessful()) {
                context.addErrors(response.getErrors());
            }
        }
    }

    @ImpersonateTenant
    private class ShippingSourceAction extends AbstractTenantSetupAction {
        @Override
        public void setup(TenantSetupContext context) {
            AddShippingProviderRequest request = new AddShippingProviderRequest();
            ShippingProviderSource customShippingProviderSource = ConfigurationManager.getInstance().getConfiguration(ShippingSourceConfiguration.class).getShippingSourceByCode(ShippingProviderSource.Code.CUSTOM.name());
            WsShippingProviderDetail shippingProvider = new WsShippingProviderDetail();
            shippingProvider.setShippingSourceCode(customShippingProviderSource.getCode());
            shippingProvider.setName(ShippingProviderSource.Code.CUSTOM.name());
            shippingProvider.setCode(ShippingProviderSource.Code.CUSTOM.name());
            shippingProvider.setServiceability(ShippingProvider.ShippingServiceablity.GLOBAL_SERVICEABLITY.name());
            shippingProvider.setEnabled(true);
            shippingProvider.setTrackingSyncStatus(ShippingProvider.SyncStatus.OFF.name());
            List<WSShippingMethod> shippingMethods = new ArrayList<WSShippingMethod>();
            List<Facility> facilities = facilityService.getAllFacilities();
            if(facilities != null) {
                String facility = facilities.get(0).getCode();
                shippingMethods.add(new WSShippingMethod(ShippingMethod.Name.STANDARD_COD.getName(),true, ShippingProviderMethod.TrackingNumberGeneration.LIST.name(),facility));
                shippingMethods.add(new WSShippingMethod(ShippingMethod.Name.STANDARD_PREPAID.getName(),true,ShippingProviderMethod.TrackingNumberGeneration.LIST.name(),facility));
                shippingProvider.setShippingMethods(shippingMethods);
                request.setWsShippingProvider(shippingProvider);
                AddShippingProviderResponse response = shippingAdminService.addShippingProvider(request);
                if (!response.isSuccessful()) {
                    context.addErrors(response.getErrors());
                }
            }
            else {
                context.addError(WsResponseCode.INVALID_FACILITY_CODE, "No Valid Facility Found");
            }
        }
    }

    private class EmailTemplateSetup extends AbstractTenantSetupAction {
        @Override
        @Transactional
        public void setup(TenantSetupContext context) {
            String table = TenantStates.EMAIL_TEMPLATE.name();
            boolean status = tenantSetupDao.tenantSetupUpdate(table, context.getTenantToSetup().getId(), "");
            if (!status) {
                context.addError(WsResponseCode.ERROR_SETTING_UP_TENANT, "Error executing updates in table : " + table);
            }
        }
    }

    private class SmsTemplateSetup extends AbstractTenantSetupAction {
        @Override
        @Transactional
        public void setup(TenantSetupContext context) {
            String table = TenantStates.SMS_TEMPLATE.name();
            boolean status = tenantSetupDao.tenantSetupUpdate(table, context.getTenantToSetup().getId(), "");
            if (!status) {
                context.addError(WsResponseCode.ERROR_SETTING_UP_TENANT, "Error executing updates in table : " + table);
            }
        }
    }

    private class SystemConfigurationSetup extends AbstractTenantSetupAction {
        @Override
        @Transactional
        public void setup(TenantSetupContext context) {
            String table = TenantStates.SYSTEM_CONFIGURATION.name();
            boolean status = tenantSetupDao.tenantSetupUpdate(table, context.getTenantToSetup().getId(), "facility_id is null");
            if (!status) {
                context.addError(WsResponseCode.ERROR_SETTING_UP_TENANT, "Error executing updates in table : " + table);
            }
        }
    }

    private class ScriptConfigSetup extends AbstractTenantSetupAction {
        @Override
        public void setup(TenantSetupContext context) {
            List<ScriptVersionVO> scriptVersions = scriptMao.getScriptVersions();
            if (!scriptVersions.isEmpty()) {
                Tenant currTenant = UserContext.current().getTenant();
                try {
                    UserContext.current().setTenant(context.getTenantToSetup());
                    for (ScriptVersionVO scriptVersion : scriptVersions) {
                        scriptVersion.setId(null);
                        scriptVersion.setTenantCode(context.getTenantToSetup().getCode());
                        scriptVersion.setCreated(DateUtils.getCurrentTime());
                        scriptVersion.setUpdated(DateUtils.getCurrentTime());
                        tenantSetupMao.insertScriptVersion(scriptVersion);
                    }
                    CacheManager.getInstance().markCacheDirty(ScriptVersionedCache.class);
                } finally {
                    UserContext.current().setTenant(currTenant);
                }

            } else {
                context.addError(WsResponseCode.ERROR_SETTING_UP_TENANT, "No data found for updating collection : scriptVersion");
            }

        }
    }

    private class ItemDetailFieldSetup extends AbstractTenantSetupAction {
        @Override
        @Transactional
        public void setup(TenantSetupContext context) {
            String table = TenantStates.ITEM_DETAIL_FIELD.name();
            boolean status = tenantSetupDao.tenantSetupUpdate(table, context.getTenantToSetup().getId(), "");
            if (!status) {
                context.addError(WsResponseCode.ERROR_SETTING_UP_TENANT, "Error executing updates in table : " + table);
            }
        }

    }

    private class DefaultCategorySetup extends AbstractTenantSetupAction {
        @Override
        @Transactional
        public void setup(TenantSetupContext context) {
            String insert = "insert ignore into category (tenant_id, code, name, tax_type_id, traceable, item_detail_fields, created, updated) "
                    + "select :toTenant, code, name, tax_type_id, traceable, item_detail_fields, now(), now() from category where tenant_id = :fromTenant";
            String update = "update category c, tax_type tt1, tax_type tt2 set c.tax_type_id = tt2.id where c.tenant_id = :toTenant and c.tax_type_id = tt1.id and tt1.code = tt2.code and tt2.tenant_id = :toTenant";
            LOG.info("FromTenant: " + UserContext.current().getTenant().getId());
            LOG.info("ToTenant: " + context.getTenantToSetup().getId());
            boolean status = tenantSetupDao.executeUpdate(insert, context.getTenantToSetup().getId()) && tenantSetupDao.executeUpdate(update, context.getTenantToSetup().getId());
            if (!status) {
                context.addError(WsResponseCode.ERROR_SETTING_UP_TENANT, "Error executing updates in table : category");
            }
        }
    }

    private class DefaultItemTypeSetup extends AbstractTenantSetupAction {

        @Override
        @Transactional
        public void setup(TenantSetupContext context) {
            String table = TenantStates.ITEM_TYPE.name();
            boolean status = tenantSetupDao.tenantSetupUpdate(table, "category", "code", context.getTenantToSetup().getId());
            if (!status) {
                context.addError(WsResponseCode.ERROR_SETTING_UP_TENANT, "Error executing updates in table : itemType");
            }
        }
    }

    private class UserDatatableViewSetup extends AbstractTenantSetupAction {
        @Override
        @Transactional
        public void setup(TenantSetupContext context) {
            String table = TenantStates.USER_DATATABLE_VIEW.name();
            boolean status = tenantSetupDao.tenantSetupUpdate(table, context.getTenantToSetup().getId(), "user_id is null");
            if (!status) {
                context.addError(WsResponseCode.ERROR_SETTING_UP_TENANT, "Error executing updates in table : " + table);
            }
        }
    }

    private class SequenceGenerationSetup extends AbstractTenantSetupAction {
        @Override
        @Transactional
        public void setup(TenantSetupContext context) {
            for (Name name : Name.values()) {
                if (Level.TENANT.equals(name.level())) {
                    Sequence sequence = new Sequence(context.getTenantToSetup(), null, name.name(), Level.TENANT.name(), 0, 1, name.prefix(), name.prefixExpression(),
                            name.padToLength(), name.resetInterval(), DateUtils.getCurrentTime(), DateUtils.getCurrentTime());
                    sequenceGenerator.createSequence(sequence);
                }
            }
        }
    }

    @ImpersonateTenant
    private class UsersCreationAction extends AbstractTenantSetupAction {
        @Override
        @Transactional
        @RollbackOnFailure
        public void setup(TenantSetupContext context) {
            // Create the requested user.
            CreateUserRequest userRequest = new CreateUserRequest();
            userRequest.setUsername(context.getRequest().getUsername());
            userRequest.setName(context.getRequest().getName());
            userRequest.setPassword(context.getRequest().getPassword());
            userRequest.setConfirmPassword(context.getRequest().getConfirmPassword());
            userRequest.setEmail(context.getRequest().getEmail());
            userRequest.setMobile(context.getRequest().getMobile());
            userRequest.setVerifiedMobile(true);
            CreateUserResponse createUserResponse = usersService.createUser(userRequest);
            if (!createUserResponse.isSuccessful()) {
                context.addErrors(createUserResponse.getErrors());
                LOG.error("Error creating user with username: {}, errors: {}", context.getRequest().getUsername(), createUserResponse.getErrors());
            }

            TenantSetupPropertyVO config = tenantSetupConfigurationMao.getTenantSetupProperty();
            // Create the SUPER user.
            CreateUserRequest superUserRequest = new CreateUserRequest();
            superUserRequest.setUsername(config.getSuperUserUsername());
            superUserRequest.setName(config.getSuperUserName());
            String superUserPassword = UUID.randomUUID().toString().substring(0, 14);
            superUserRequest.setPassword(superUserPassword);
            superUserRequest.setConfirmPassword(superUserPassword);
            superUserRequest.setEmail(config.getSuperUserEmail());
            superUserRequest.setHidden(true);
            createUserResponse = usersService.createUser(superUserRequest);
            if (!createUserResponse.isSuccessful()) {
                context.addErrors(createUserResponse.getErrors());
                LOG.error("Error creating user with username: {}, errors: {}", superUserRequest.getUsername(), createUserResponse.getErrors());
            }

            // Create the Tech Support user/api user.
            if (!context.hasErrors()) {
                String password = UUID.randomUUID().toString().substring(0, 14);
                addPrivateUser(config.getTechSupportUserName(), config.getTechSupportUserEmail(), password, config.getTechSupportApiUsername(), password, context);
            }
            // Create the Customer Support user/api user.
            if (!context.hasErrors()) {
                String password = UUID.randomUUID().toString().substring(0, 14);
                addPrivateUser(config.getCustomerSupportUserName(), config.getCustomerSupportUserEmail(), password, config.getCustomerSupportApiUsername(), password, context);
            }
            // Create System API user
            if (!context.hasErrors()) {
                addPrivateUser(config.getSystemUserEmail(), config.getSystemUserEmail(), context.getRequest().getApiPassword(), context.getRequest().getApiUsername(),
                        context.getRequest().getApiPassword(), context);
            }

        }

        private void addPrivateUser(String username, String email, String password, String apiUsername, String apiPassword, TenantSetupContext context) {
            CreateUserRequest userRequest = new CreateUserRequest();
            userRequest.setUsername(username);
            userRequest.setName(username);
            userRequest.setPassword(password);
            userRequest.setConfirmPassword(password);
            userRequest.setEmail(email);
            userRequest.setHidden(true);
            CreateUserResponse createTechUserResponse = usersService.createUser(userRequest);
            if (createTechUserResponse.isSuccessful()) {
                User techSupportUser = usersService.getUserWithDetailsById(createTechUserResponse.getUserDTO().getUserId());
                ApiUser apiUser = new ApiUser();
                apiUser.setUser(techSupportUser);
                apiUser.setUsername(apiUsername);
                apiUser.setApiName(apiUsername);
                apiUser.setEnabled(true);
                apiUser.setCreated(DateUtils.getCurrentTime());
                apiUser.setPassword(apiPassword);
                usersService.addOrUpdateApiUser(apiUser);
            } else {
                context.addErrors(createTechUserResponse.getErrors());
                LOG.error("Error creating tech support user: {}", createTechUserResponse.getErrors());
            }
        }
    }

    @ImpersonateTenant
    private class FacilityCreationSetup extends AbstractTenantSetupAction {
        @Override
        public void setup(TenantSetupContext context) {
            CreateFacilityRequest facilityRequest = new CreateFacilityRequest();
            facilityRequest.setFacility(context.getRequest().getFacility());
            //TO-DO should ideally be request.getUsername
            facilityRequest.setUsername(context.getRequest().getEmail());
            CreateFacilityResponse response = facilityService.createFacility(facilityRequest);
            if (response.hasErrors()) {
                context.addErrors(response.getErrors());
            }
            if (response.hasWarnings()) {
                for (WsWarning warning : response.getWarnings()) {
                    context.addWarning(new ResponseCode(warning.getCode(), warning.getMessage()), warning.getDescription());
                }
            }
        }
    }

    private class AccountCreationSetup extends AbstractTenantSetupAction {

        @Override
        public void setup(TenantSetupContext context) {
            if (!context.getRequest().getTenantType().equals(Product.Type.WINGS.name())) {
                CreateAccountResponse response = accountService.createAccount(context.getRequest());
                if (response.isSuccessful()) {
                    context.getTenantToSetup().setAccountCode(response.getAccountCode());
                    tenantSetupDao.updateTenant(context.getTenantToSetup());
                } else {
                    context.addError(WsResponseCode.ERROR_SETTING_UP_TENANT, response.getErrors().get(0).getDescription());
                }
            } else {
                LOG.info("Skipping account creation for {} as it is WINGS product", context.getRequest().getCode());
            }
        }
    }

    @ImpersonateTenant
    private class ActivateTenantAction extends AbstractTenantSetupAction {
        @Override
        @Transactional
        public void setup(TenantSetupContext context) {
            TenantSetupPropertyVO config = tenantSetupMao.getTenantSetupProperty();
            ApiUser techSupport = tenantSetupDao.getApiUserForTenantByUsername(context.getTenantToSetup().getId(), config.getTechSupportApiUsername());
            ApiUser custSupport = tenantSetupDao.getApiUserForTenantByUsername(context.getTenantToSetup().getId(), config.getCustomerSupportApiUsername());
            TenantProfileVO tenantVO = tenantProfileService.getTenantProfileByCode(context.getTenantToSetup().getCode());
            tenantVO.setTechSupportUsername(techSupport.getUsername());
            tenantVO.setTechSupportPassword(techSupport.getPassword());
            tenantVO.setCustomerSupportUsername(custSupport.getUsername());
            tenantVO.setCustomerSupportPassword(custSupport.getPassword());
            // Verify our internal users
            UserProfileVO techSupportUserProfile = userProfileMao.getUserProfileByUsername(techSupport.getUser().getUsername());
            techSupportUserProfile.setVerified(true);
            userProfileMao.save(techSupportUserProfile);
            UserProfileVO custSupportUserProfile = userProfileMao.getUserProfileByUsername(custSupport.getUser().getUsername());
            custSupportUserProfile.setVerified(true);
            userProfileMao.save(custSupportUserProfile);
            User superUser = tenantSetupDao.getUserForTenantByUsername(context.getTenantToSetup().getId(), config.getSuperUserUsername());
            UserProfileVO superUserProfile = userProfileMao.getUserProfileByUsernameAndTenant(superUser.getUsername(), context.getTenantToSetup().getCode());
            superUserProfile.setVerified(true);
            userProfileMao.save(superUserProfile);
            User systemUser = tenantSetupDao.getUserForTenantByUsername(context.getTenantToSetup().getId(), config.getSystemUserEmail());
            UserProfileVO systemUserProfile = userProfileMao.getUserProfileByUsernameAndTenant(systemUser.getUsername(), context.getTenantToSetup().getCode());
            systemUserProfile.setVerified(true);
            userProfileMao.save(systemUserProfile);
            //            LOG.info("Loading tasks");
            //            ReloadTasksResponse response = reloadService.reloadTasksForTenant(context.getTenantToSetup().getCode());
            //            if (response.isSuccessful()) {
            //                LOG.info("Done Loading tasks");
            //
            //            } else {
            //                LOG.info("Error Loading tasks");
            //                context.addError(WsResponseCode.ERROR_SETTING_UP_TENANT, response.getErrors().get(0).getDescription());
            //            }
            tenantProfileService.activateTenant(tenantVO);
        }
    }

    @ImpersonateTenant
    private class CompletionAction extends AbstractTenantSetupAction {
        @Override
        public void setup(TenantSetupContext context) {
            reloadService.reloadAll();
            //            tenant.setSetupState(TenantStates.DONE.toString());
            //            tenantSetupDao.updateTenant(tenant);
        }
    }
}
