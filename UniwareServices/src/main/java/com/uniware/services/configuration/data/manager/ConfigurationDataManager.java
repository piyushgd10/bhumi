/*
 *  Copyright 2013 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 11-Nov-2013
 *  @author sunny
 */
package com.uniware.services.configuration.data.manager;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.unifier.core.configuration.data.manager.IConfigurationDataManager;
import com.uniware.core.entity.TaxType;
import com.uniware.services.tax.ITaxTypeService;

@Component("configurationDataManager")
@Transactional(readOnly = true)
public class ConfigurationDataManager implements IConfigurationDataManager {

    @Autowired
    private ITaxTypeService taxTypeService;

    @Override
    public List<TaxType> getAllTaxTypes() {
        return taxTypeService.getAllTaxTypes();
    }

}
