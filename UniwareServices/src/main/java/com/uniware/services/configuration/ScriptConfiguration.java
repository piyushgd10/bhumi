/*
 *  Copyright 2013 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 19, 2013
 *  @author karunsingla
 */
package com.uniware.services.configuration;

import com.unifier.core.annotation.Configuration;
import com.unifier.core.annotation.Level;
import com.unifier.core.configuration.IConfiguration;
import com.unifier.core.transport.http.HttpProxy;
import com.unifier.core.utils.StringUtils;
import com.unifier.scraper.sl.parser.ScraperScriptNode;
import com.unifier.scraper.sl.runtime.ScraperScript;
import com.uniware.core.vo.UniwareScriptVO;
import com.uniware.services.script.service.IScriptService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author Sunny
 */
@Configuration(name = "scriptConfiguration", level = Level.GLOBAL, eager = true)
public class ScriptConfiguration implements IConfiguration {

    private static final Logger LOG = LoggerFactory.getLogger(ScriptConfiguration.class);

    public Map<String, Map<String, ScraperScript>> nameToVersionToScript = new ConcurrentHashMap<>();

    @Autowired
    private transient IScriptService scriptService;

    public void addScript(UniwareScriptVO scriptConfig) {
        Map<String, ScraperScript> versionToScript = new HashMap<>();
        if (nameToVersionToScript.containsKey(scriptConfig.getName().toLowerCase())) {
            versionToScript = nameToVersionToScript.get(scriptConfig.getName().toLowerCase());
        }
        if (!versionToScript.containsKey(scriptConfig.getVersion())) {
            ScraperScriptNode scriptNode;
            try {
                if (StringUtils.isNotBlank(scriptConfig.getScript())) {
                    scriptNode = ScraperScriptNode.parse(scriptConfig.getScript());
                    scriptNode.validate();
                    ScraperScript scraperScript = scriptNode.compile();
                    if (StringUtils.isNotBlank(scriptConfig.getHttpProxyServer())) {
                        int colonIndex = scriptConfig.getHttpProxyServer().indexOf(':');
                        HttpProxy proxy = new HttpProxy();
                        proxy.setHost(scriptConfig.getHttpProxyServer().substring(0, colonIndex).trim());
                        proxy.setPort(Integer.parseInt(scriptConfig.getHttpProxyServer().substring(colonIndex + 1).trim()));
                        proxy.setAuthenticationRequired(false);
                        scraperScript.setHttpProxy(proxy);
                    }
                    if (scriptConfig.getHttpProxy() != null) {
                        scraperScript.setHttpProxy(scriptConfig.getHttpProxy());
                    }
                    versionToScript.put(scriptConfig.getVersion(), scraperScript);
                    nameToVersionToScript.put(scriptConfig.getName().toLowerCase(), versionToScript);
                }
            } catch (Exception e) {
                LOG.error("unable to load script:" + scriptConfig.getName() + " version :" + scriptConfig.getVersion(), e);
            }
        }

    }

    public ScraperScript getScriptByNameAndVersion(String scriptName, String version) {
        if (StringUtils.isNotBlank(scriptName) && StringUtils.isNotBlank(version)) {
            if (nameToVersionToScript.get(scriptName.toLowerCase()) == null || !nameToVersionToScript.get(scriptName.toLowerCase()).containsKey(version)) {
                UniwareScriptVO scriptVO = scriptService.getScriptVOByNameAndVersion(scriptName, version);
                if (scriptVO != null) {
                    addScript(scriptVO);
                } else {
                    Map<String, ScraperScript> versionToScript = nameToVersionToScript.get(scriptName.toLowerCase());
                    if (versionToScript == null) {
                        versionToScript = new HashMap<>();
                        nameToVersionToScript.put(scriptName.toLowerCase(), versionToScript);
                    }
                    versionToScript.put(version, null);
                }
            }
            return nameToVersionToScript.get(scriptName.toLowerCase()) != null ? nameToVersionToScript.get(scriptName.toLowerCase()).get(version) : null;
        }
        return null;
    }

    public ScraperScript getScriptByNameAndVersion(String scriptName, String version, boolean required) {
        ScraperScript script = getScriptByNameAndVersion(scriptName, version);
        if (script != null || !required) {
            return script;
        } else {
            throw new IllegalArgumentException("script not loaded: " + scriptName + " version " + version);
        }
    }

    @Override
    public void load() {
        for (UniwareScriptVO scriptVO : scriptService.getScriptsToLoadEagerly()) {
            addScript(scriptVO);
        }
    }
}
