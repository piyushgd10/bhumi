/*
 * Copyright 2016 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 9/10/16 3:54 PM
 * @author amdalal
 */

package com.uniware.services.configuration;

import java.util.Map;

import com.unifier.core.api.application.EditSystemConfigurationRequest;
import com.unifier.core.api.application.EditSystemConfigurationResponse;
import com.unifier.core.entity.SystemConfig;

public interface IConfigurationService {

    boolean evaluateConditionExpression(SystemConfig systemConfig, Map<String, String> properties, String expectedOutput);

    boolean evaluateConditionExpression(SystemConfig systemConfig, Map<String, String> properties);

}
