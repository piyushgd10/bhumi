/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 21, 2011
 *  @author singla
 */
package com.uniware.services.configuration;

import com.unifier.core.annotation.Configuration;
import com.unifier.core.annotation.Level;
import com.unifier.core.configuration.IConfiguration;
import com.unifier.core.exception.TemplateCompilationException;
import com.unifier.core.template.Template;
import com.unifier.services.sms.ISmsService;
import com.uniware.core.entity.SmsTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


/**
 * @author singla
 */
@Configuration(name = "smsConfiguration", level = Level.TENANT)
public class SmsConfiguration implements IConfiguration {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private final Map<String, SmsTemplateVO> nameToSmsTemplates = new ConcurrentHashMap<>();

    private static final Logger LOG = LoggerFactory.getLogger(SmsConfiguration.class);

    @Autowired
    private transient ISmsService smsService;

    private void addSmsTemplate(SmsTemplate template) {
        try {
            SmsTemplateVO templateVO = new SmsTemplateVO();
            templateVO.setName(template.getName());
            templateVO.setMessageTemplate(Template.compile(template.getName(), template.getTemplate()));
            templateVO.setEnabled(template.isEnabled());
            nameToSmsTemplates.put(template.getName(), templateVO);
        } catch (TemplateCompilationException ex) {
            LOG.error("Exception while adding template " + template.getName(), ex);
        }
    }

    /**
     * @param name
     * @return
     */
    public SmsTemplateVO getTemplateByName(String name) {
        return nameToSmsTemplates.get(name);
    }

    public static class SmsTemplateVO implements Serializable {
        private String   name;
        private Template messageTemplate;
        private boolean  enabled;

        /**
         * @return the name
         */
        public String getName() {
            return name;
        }

        /**
         * @param name the name to set
         */
        public void setName(String name) {
            this.name = name;
        }

        /**
         * @return the messageTemplate
         */
        public Template getMessageTemplate() {
            return messageTemplate;
        }

        /**
         * @param messageTemplate the messageTemplate to set
         */
        public void setMessageTemplate(Template messageTemplate) {
            this.messageTemplate = messageTemplate;
        }

        /**
         * @return the enabled
         */
        public boolean isEnabled() {
            return enabled;
        }

        /**
         * @param enabled the enabled to set
         */
        public void setEnabled(boolean enabled) {
            this.enabled = enabled;
        }

    }

    @Override
    public void load() {
        List<SmsTemplate> templates = smsService.getSmsTemplates();
        for (SmsTemplate template : templates) {
            addSmsTemplate(template);
        }
    }

}
