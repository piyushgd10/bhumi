/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 28, 2011
 *  @author singla
 */
package com.uniware.services.configuration;

import com.google.gson.Gson;
import com.unifier.core.annotation.Configuration;
import com.unifier.core.annotation.Level;
import com.unifier.core.configuration.IConfiguration;
import com.uniware.core.entity.ShippingMethod;
import com.uniware.core.entity.ShippingPackageType;
import com.uniware.core.entity.ShippingProvider;
import com.uniware.core.entity.ShippingProvider.ShippingServiceablity;
import com.uniware.core.entity.ShippingProviderAllocationRule;
import com.uniware.core.entity.ShippingProviderMethod;
import com.uniware.services.shipping.IShippingProviderService;
import com.uniware.services.shipping.IShippingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author singla
 */
@Configuration(name = "shippingFacilityLevelConfiguration", level = Level.FACILITY)
public class ShippingFacilityLevelConfiguration implements IConfiguration {
    private final List<ShippingPackageType>                          shippingPackageTypes                          = new ArrayList<ShippingPackageType>();
    private final Map<Integer, ShippingPackageType>                  idToShippingPackageTypes                      = new ConcurrentHashMap<>();
    private final Map<String, ShippingPackageType>                   codeToShippingPackageTypes                    = new ConcurrentHashMap<>();
    private final Map<String, Map<String, ShippingProviderMethod>>   providerToMethodToShippingProviderMethod      = new ConcurrentHashMap<>();
    private final Map<Integer, Map<Integer, ShippingProviderMethod>> providerIdToMethodIdToShippingProviderMethod  = new ConcurrentHashMap<>();
    private final Map<String, Map<String, String>>                   methodCodeToProviderCodeToShippingProviders   = new ConcurrentHashMap<>();
    private final Map<Integer, List<ShippingProvider>>               shippingMethodToGlobalServiceabilityProviders = new ConcurrentHashMap<>();
    private final Map<String, Map<String, String>>                   providerToMethodToshippingProviderMethodType  = new ConcurrentHashMap<>();
    private String shippingPackageTypesJson;
    private String methodCodeToProviderCodeToShippingProvidersJson;
    private       List<ShippingProviderAllocationRule> shippingProviderAllocationRules = new ArrayList<ShippingProviderAllocationRule>();
    private final Map<Integer, List<String>>           providerIdToMethods             = new ConcurrentHashMap<>();
    private String providerToMethodToShippingProviderMethodTypeJson;

    @Autowired
    private transient IShippingProviderService shippingProviderService;

    @Autowired
    private transient IShippingService shippingService;

    private void addShippingProviderMethod(ShippingProviderMethod shippingProviderMethod) {
        ShippingProvider shippingProvider = shippingProviderMethod.getShippingProvider();
        Map<String, ShippingProviderMethod> methodToProviderMethod = providerToMethodToShippingProviderMethod.get(shippingProvider.getCode().toLowerCase());
        if (methodToProviderMethod == null) {
            methodToProviderMethod = new HashMap<String, ShippingProviderMethod>();
            providerToMethodToShippingProviderMethod.put(shippingProvider.getCode().toLowerCase(), methodToProviderMethod);
        }
        ShippingMethod shippingMethod = shippingProviderMethod.getShippingMethod();
        String shippingMethodCode = shippingMethod.getShippingMethodCode();
        methodToProviderMethod.put(shippingMethodCode, shippingProviderMethod);

        Map<String, String> methodToProviderMethodType = providerToMethodToshippingProviderMethodType.get(shippingProvider.getCode().toLowerCase());
        if (methodToProviderMethodType == null) {
            methodToProviderMethodType = new HashMap<String, String>();
            providerToMethodToshippingProviderMethodType.put(shippingProvider.getCode().toLowerCase(), methodToProviderMethodType);
        }
        methodToProviderMethodType.put(shippingMethodCode, shippingProviderMethod.getTrackingNumberGeneration());

        Map<Integer, ShippingProviderMethod> shippingMethodToShippingProviderMethod = providerIdToMethodIdToShippingProviderMethod.get(shippingProvider.getId());
        if (shippingMethodToShippingProviderMethod == null) {
            shippingMethodToShippingProviderMethod = new HashMap<Integer, ShippingProviderMethod>();
            providerIdToMethodIdToShippingProviderMethod.put(shippingProvider.getId(), shippingMethodToShippingProviderMethod);
        }
        shippingMethodToShippingProviderMethod.put(shippingMethod.getId(), shippingProviderMethod);

        Map<String, String> codeToShippingProvider = methodCodeToProviderCodeToShippingProviders.get(shippingMethodCode);
        if (codeToShippingProvider == null) {
            codeToShippingProvider = new HashMap<String, String>();
            methodCodeToProviderCodeToShippingProviders.put(shippingMethodCode, codeToShippingProvider);
        }
        codeToShippingProvider.put(shippingProvider.getCode(), shippingProvider.getName());
        if (ShippingServiceablity.GLOBAL_SERVICEABLITY.equals(shippingProvider.getShippingServiceablity())) {
            List<ShippingProvider> shippingProviders = shippingMethodToGlobalServiceabilityProviders.get(shippingMethod.getId());
            if (shippingProviders == null) {
                shippingProviders = new ArrayList<ShippingProvider>();
                shippingMethodToGlobalServiceabilityProviders.put(shippingMethod.getId(), shippingProviders);
            }
            shippingProviders.add(shippingProvider);
        }
    }

    public Map<String, String> getShippingProvidersByShippingMethodCode(String methodCode) {
        return methodCodeToProviderCodeToShippingProviders.get(methodCode);
    }

    public List<ShippingProvider> getGlobalServicingShippingProviders(Integer shippingMethodId) {
        return shippingMethodToGlobalServiceabilityProviders.get(shippingMethodId);
    }

    private void addShippingProviderAllocationRule(ShippingProviderAllocationRule rule) {
        shippingProviderAllocationRules.add(rule);
    }

    public List<ShippingProviderAllocationRule> getShippingProviderAllocationRules() {
        return this.shippingProviderAllocationRules;
    }

    public ShippingProviderMethod getShippingProviderMethod(String shippingProviderCode, String shippingMethodCode, boolean cashOnDelivery) {
        Map<String, ShippingProviderMethod> methodToProviderMethod = providerToMethodToShippingProviderMethod.get(shippingProviderCode.toLowerCase());
        if (methodToProviderMethod != null) {
            return methodToProviderMethod.get((shippingMethodCode + "-" + cashOnDelivery).toLowerCase());
        }
        return null;
    }

    public ShippingProviderMethod getShippingProviderMethod(int shippingProviderId, int shippingMethodId) {
        Map<Integer, ShippingProviderMethod> methodToShippingProviders = providerIdToMethodIdToShippingProviderMethod.get(shippingProviderId);
        if (methodToShippingProviders != null) {
            return methodToShippingProviders.get(shippingMethodId);
        }
        return null;
    }

    /**
     * @return the methodCodeToProviderCodeToShippingProvidersJson
     */
    public String getMethodCodeToProviderCodeToShippingProvidersJson() {
        return methodCodeToProviderCodeToShippingProvidersJson;
    }

    /**
     * @param methodCodeToProviderCodeToShippingProvidersJson the methodCodeToProviderCodeToShippingProvidersJson to set
     */
    public void setMethodCodeToProviderCodeToShippingProvidersJson(String methodCodeToProviderCodeToShippingProvidersJson) {
        this.methodCodeToProviderCodeToShippingProvidersJson = methodCodeToProviderCodeToShippingProvidersJson;
    }

    /**
     * @return the providerToMethodToShippingProviderMethodTypeJson
     */
    public String getProviderToMethodToShippingProviderMethodTypeJson() {
        return providerToMethodToShippingProviderMethodTypeJson;
    }

    /**
     * @param providerToMethodToShippingProviderMethodTypeJson the providerToMethodToShippingProviderMethodTypeJson to
     *            set
     */
    public void setProviderToMethodToShippingProviderMethodTypeJson(String providerToMethodToShippingProviderMethodTypeJson) {
        this.providerToMethodToShippingProviderMethodTypeJson = providerToMethodToShippingProviderMethodTypeJson;
    }

    public void freeze() {
        setMethodCodeToProviderCodeToShippingProvidersJson(new Gson().toJson(methodCodeToProviderCodeToShippingProviders));
        Map<String, ShippingPackageTypeVO> shippingPackageTypeVOs = new HashMap<String, ShippingPackageTypeVO>();
        for (Map.Entry<Integer, ShippingPackageType> entry : idToShippingPackageTypes.entrySet()) {
            shippingPackageTypeVOs.put(entry.getValue().getCode(), new ShippingPackageTypeVO(entry.getValue()));
        }
        setShippingPackageTypesJson(new Gson().toJson(shippingPackageTypeVOs));
        shippingProviderAllocationRules = Collections.unmodifiableList(shippingProviderAllocationRules);

        setProviderToMethodToShippingProviderMethodTypeJson(new Gson().toJson(providerToMethodToshippingProviderMethodType));

        for (Entry<Integer, Map<Integer, ShippingProviderMethod>> entry : providerIdToMethodIdToShippingProviderMethod.entrySet()) {
            for (Entry<Integer, ShippingProviderMethod> entry2 : entry.getValue().entrySet()) {
                if (!providerIdToMethods.containsKey(entry.getKey())) {
                    providerIdToMethods.put(entry.getKey(), new ArrayList<String>());
                }
                providerIdToMethods.get(entry.getKey()).add(entry2.getValue().getShippingMethod().getShippingMethodCode());
            }
        }
    }

    private void addShippingPackageType(ShippingPackageType shippingPackageType) {
        idToShippingPackageTypes.put(shippingPackageType.getId(), shippingPackageType);
        codeToShippingPackageTypes.put(shippingPackageType.getCode().toLowerCase(), shippingPackageType);
        shippingPackageTypes.add(shippingPackageType);
    }

    /**
     * @return the shippingPackageTypesJson
     */
    public String getShippingPackageTypesJson() {
        return shippingPackageTypesJson;
    }

    /**
     * @param shippingPackageTypesJson the shippingPackageTypesJson to set
     */
    public void setShippingPackageTypesJson(String shippingPackageTypesJson) {
        this.shippingPackageTypesJson = shippingPackageTypesJson;
    }

    public ShippingPackageType getShippingPackageTypById(Integer shippingPackageTypeId) {
        return idToShippingPackageTypes.get(shippingPackageTypeId);
    }

    public ShippingPackageType getShippingPackageTypeByCode(String shippingPackageTypeCode) {
        return codeToShippingPackageTypes.get(shippingPackageTypeCode.toLowerCase());
    }

    public List<ShippingPackageType> getShippingPackageTypes() {
        return shippingPackageTypes;
    }

    public static class ShippingPackageTypeVO {
        private String  code;
        private int     boxLength;
        private int     boxWidth;
        private int     boxHeight;
        private int     boxWeight;
        private boolean enabled;
        private boolean editable;

        public ShippingPackageTypeVO(ShippingPackageType shippingPackageType) {
            this.code = shippingPackageType.getCode();
            this.boxLength = shippingPackageType.getBoxLength();
            this.boxWidth = shippingPackageType.getBoxWidth();
            this.boxHeight = shippingPackageType.getBoxHeight();
            this.boxWeight = shippingPackageType.getBoxWeight();
            this.enabled = shippingPackageType.isEnabled();
            this.editable = shippingPackageType.isEditable();
        }

        /**
         * @return the code
         */
        public String getCode() {
            return code;
        }

        /**
         * @param code the code to set
         */
        public void setCode(String code) {
            this.code = code;
        }

        /**
         * @return the boxLength
         */
        public int getBoxLength() {
            return boxLength;
        }

        /**
         * @param boxLength the boxLength to set
         */
        public void setBoxLength(int boxLength) {
            this.boxLength = boxLength;
        }

        /**
         * @return the boxWidth
         */
        public int getBoxWidth() {
            return boxWidth;
        }

        /**
         * @param boxWidth the boxWidth to set
         */
        public void setBoxWidth(int boxWidth) {
            this.boxWidth = boxWidth;
        }

        /**
         * @return the boxHeight
         */
        public int getBoxHeight() {
            return boxHeight;
        }

        /**
         * @param boxHeight the boxHeight to set
         */
        public void setBoxHeight(int boxHeight) {
            this.boxHeight = boxHeight;
        }

        /**
         * @return the boxWeight
         */
        public int getBoxWeight() {
            return boxWeight;
        }

        /**
         * @param boxWeight the boxWeight to set
         */
        public void setBoxWeight(int boxWeight) {
            this.boxWeight = boxWeight;
        }

        /**
         * @return the enabled
         */
        public boolean isEnabled() {
            return enabled;
        }

        /**
         * @param enabled the enabled to set
         */
        public void setEnabled(boolean enabled) {
            this.enabled = enabled;
        }

        /**
         * @return the editable
         */
        public boolean isEditable() {
            return editable;
        }

        /**
         * @param editable the editable to set
         */
        public void setEditable(boolean editable) {
            this.editable = editable;
        }

    }

    @Override
    @Transactional
    public void load() {
        List<ShippingProviderAllocationRule> allocationRules = shippingProviderService.getShippingProviderAllocationRules();
        for (ShippingProviderAllocationRule shippingProviderAllocationRule : allocationRules) {
            addShippingProviderAllocationRule(shippingProviderAllocationRule);
        }
        List<ShippingProviderMethod> shippingProviderMethods = shippingProviderService.getShippingProviderMethods();
        for (ShippingProviderMethod shippingProviderMethod : shippingProviderMethods) {
            addShippingProviderMethod(shippingProviderMethod);
        }
        loadShippingPackageTypes();
    }

    private void loadShippingPackageTypes() {
        List<ShippingPackageType> shippingPackageTypes = shippingService.getShippingPackageTypes();
        for (ShippingPackageType shippingPackageType : shippingPackageTypes) {
            addShippingPackageType(shippingPackageType);
        }
    }
}
