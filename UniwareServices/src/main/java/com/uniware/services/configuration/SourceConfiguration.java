/*
 *  Copyright 2013 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 08-Feb-2013
 *  @author praveeng
 */
package com.uniware.services.configuration;

import com.google.gson.Gson;
import com.unifier.core.annotation.Configuration;
import com.unifier.core.annotation.Level;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.configuration.IConfiguration;
import com.unifier.core.entity.ProductSource;
import com.unifier.services.application.IApplicationSetupService;
import com.uniware.core.api.channel.SourceDetailDTO;
import com.uniware.core.entity.Product;
import com.uniware.core.entity.Source;
import com.uniware.core.entity.SourceConfigurationParameter;
import com.uniware.core.entity.SourceConnector;
import com.uniware.core.entity.SourceProperty;
import com.uniware.core.utils.UserContext;
import com.uniware.services.channel.IChannelService;
import com.uniware.services.configuration.data.manager.ProductConfiguration;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Configuration(name = "sourceConfiguration", level = Level.GLOBAL, eager = true)
public class SourceConfiguration implements IConfiguration {

    private final List<Source> sources = new ArrayList<>();

    private final Map<String, Source>                                    codeToSource                              = new ConcurrentHashMap<>();
    private final Map<String, SourceDetailDTO>                           codeToSourceDetailDTO                     = new ConcurrentHashMap<>();
    private final Map<String, String>                                    codeToNameMap                             = new ConcurrentHashMap<>();
    private final Map<String, Map<String, SourceConnector>>              sourceToNameToSourceConnector             = new ConcurrentHashMap<>();
    private final Map<String, Map<String, SourceConfigurationParameter>> sourceToNameToSourceConfigurationParamter = new ConcurrentHashMap<>();
    private final Map<String, List<Source>>                              productCodeToSources                      = new ConcurrentHashMap<>();
    private final Map<String, String>                                    productCodeToSourcesJson                  = new ConcurrentHashMap<>();
    private final Map<String, Map<String, Object>>                       codeToSourceProperty                      = new ConcurrentHashMap<>();

    @Autowired
    private transient IChannelService channelService;

    @Autowired
    private transient IApplicationSetupService applicationSetupService;

    private void addSource(Source source) {
        sources.add(source);
        codeToNameMap.put(source.getCode(), source.getName());
        codeToSource.put(source.getCode().toLowerCase(), source);
        Map<String, SourceConnector> nameToSourceConnector = new HashMap<>();
        sourceToNameToSourceConnector.put(source.getCode(), nameToSourceConnector);
        for (SourceConnector connector : source.getSourceConnectors()) {
            nameToSourceConnector.put(connector.getName(), connector);
        }
        if (!codeToSourceProperty.containsKey(source.getCode())) {
            codeToSourceProperty.put(source.getCode(), new HashMap<>());
        }
        for (SourceProperty sourceProperty : source.getSourceProperties()) {
            Map<String, Object> property = codeToSourceProperty.get(source.getCode());
            property.put(sourceProperty.getName(), sourceProperty.getValue());
            codeToSourceProperty.put(source.getCode(), property);
        }
        Map<String, SourceConfigurationParameter> nameToSourceConfigurationParameter = new HashMap<>();
        sourceToNameToSourceConfigurationParamter.put(source.getCode(), nameToSourceConfigurationParameter);
        for (SourceConfigurationParameter param : source.getSourceConfigurationParameters()) {
            nameToSourceConfigurationParameter.put(param.getName(), param);
        }
    }

    private void addProductSources(String productCode, List<ProductSource> productSources) {
        List<Source> sources = new ArrayList<>();
        for (ProductSource ps : productSources) {
            Source source = getSourceByCode(ps.getSourceCode());
            if (source != null && source.isEnabled()) {
                sources.add(codeToSource.get(ps.getSourceCode().toLowerCase()));
            }
        }
        productCodeToSources.put(productCode, sources);
    }

    public Map<String, String> getCodeToNameMap() {
        return codeToNameMap;
    }

    public Map<String, Source> getCodeToSource() {
        return codeToSource;
    }

    public String getSourcesJson() {
        return productCodeToSourcesJson.get(UserContext.current().getTenant().getProduct().getCode());
    }

    public Source getSourceByCode(String code) {
        return codeToSource.get(code.toLowerCase());
    }

    public SourceDetailDTO getSourceDTOByCode(String code) {
        return codeToSourceDetailDTO.get(code.toLowerCase());
    }

    public Collection<Source> getAllSources() {
        return codeToSource.values();
    }

    public List<Source> getApplicableSources() {
        return productCodeToSources.get(UserContext.current().getTenant().getProduct().getCode());
    }

    public SourceConnector getSourceConnector(String sourceCode, String connectorName) {
        Map<String, SourceConnector> nameToSourceConnector = sourceToNameToSourceConnector.get(sourceCode);
        if (nameToSourceConnector != null) {
            return nameToSourceConnector.get(connectorName);
        }
        return null;
    }

    public SourceConfigurationParameter getSourceConfigurationParameter(String sourceCode, String paramName) {
        Map<String, SourceConfigurationParameter> nameToSourceConfigurationParameter = sourceToNameToSourceConfigurationParamter.get(sourceCode);
        if (nameToSourceConfigurationParameter != null) {
            return nameToSourceConfigurationParameter.get(paramName);
        }
        return null;
    }

    public String getScriptName(String sourceCode, String scriptName) {
        return (String) codeToSourceProperty.get(sourceCode).get(scriptName);
    }

    public void freeze() {
        for (String product : productCodeToSources.keySet()) {
            List<SourceDetailDTO> sourceDetailDTOs = new ArrayList<>();
            for (Source s : productCodeToSources.get(product)) {
                if (s.isEnabled()) {
                    SourceDetailDTO sourceDTO = codeToSourceDetailDTO.get(s.getCode());
                    if (sourceDTO == null) {
                        sourceDTO = prepareSourceDetailDTO(s);
                        codeToSourceDetailDTO.put(s.getCode().toLowerCase(), sourceDTO);
                    }
                    sourceDetailDTOs.add(sourceDTO);
                }
            }
            Collections.sort(sourceDetailDTOs, new Comparator<SourceDetailDTO>() {
                @Override
                public int compare(SourceDetailDTO o1, SourceDetailDTO o2) {
                    if (o1.getPriority() != o2.getPriority()) {
                        return o1.getPriority() - o2.getPriority();
                    } else {
                        return o1.getCode().compareTo(o2.getCode());
                    }
                }
            });
            productCodeToSourcesJson.put(product, new Gson().toJson(sourceDetailDTOs));
        }
    }

    public SourceDetailDTO prepareSourceDetailDTO(Source s) {
        SourceDetailDTO sourceDTO = new SourceDetailDTO(s);
        channelService.prepareSourceConfigurationParametersDTO(sourceDTO, s);
        return sourceDTO;
    }

    @Override
    public void load() {
        List<Source> sources = channelService.getAllSources();
        for (Source source : sources) {
            addSource(source);
        }

        for (Product product : ConfigurationManager.getInstance().getConfiguration(ProductConfiguration.class).getAllProducts()) {
            addProductSources(product.getCode(), applicationSetupService.getProductSources(product.getCode()));
        }
    }
}
