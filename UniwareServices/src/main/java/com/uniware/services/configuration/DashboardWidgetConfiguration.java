/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 10-Jun-2012
 *  @author praveeng
 */
package com.uniware.services.configuration;

import com.unifier.core.annotation.Configuration;
import com.unifier.core.annotation.Level;
import com.unifier.core.api.reports.WidgetDTO;
import com.unifier.core.configuration.IConfiguration;
import com.unifier.core.entity.DashboardWidget;
import com.unifier.services.report.IReportService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Configuration(name = "dashboardWidgetConfiguration", level = Level.GLOBAL, eager = true)
public class DashboardWidgetConfiguration implements IConfiguration {

    private static final Logger LOG = LoggerFactory.getLogger(DashboardWidgetConfiguration.class);

    private final List<DashboardWidget> dashboardWidgets = new ArrayList<DashboardWidget>();

    private final Map<String, DashboardWidget> nameToDashboardWidgets = new ConcurrentHashMap<String, DashboardWidget>();

    private final Map<String, DashboardWidget> codeToDashboardWidgets = new ConcurrentHashMap<String, DashboardWidget>();

    private final Map<String, List<WidgetDTO>> widgetsGroupByWidgetGroup = new ConcurrentHashMap<>();
    ;

    private final List<WidgetDTO> widgetDTOs = new ArrayList<WidgetDTO>();

    @Autowired
    private transient IReportService reportService;

    private void addDashboardWidget(DashboardWidget dashboardWidget) {
        dashboardWidgets.add(dashboardWidget);
        nameToDashboardWidgets.put(dashboardWidget.getName(), dashboardWidget);
        codeToDashboardWidgets.put(dashboardWidget.getCode(), dashboardWidget);

        List<WidgetDTO> widgets = widgetsGroupByWidgetGroup.get(dashboardWidget.getWidgetGroup());
        if (widgets == null) {
            widgets = new ArrayList<WidgetDTO>();
            widgetsGroupByWidgetGroup.put(dashboardWidget.getWidgetGroup(), widgets);
        }

        WidgetDTO widgetDTO = new WidgetDTO();
        widgetDTO.setCode(dashboardWidget.getCode());
        widgetDTO.setName(dashboardWidget.getName());
        widgets.add(widgetDTO);
        widgetDTOs.add(widgetDTO);
    }

    public DashboardWidget getDashboardWidgetByName(String name) {
        return nameToDashboardWidgets.get(name);
    }

    public DashboardWidget getDashboardWidgetByCode(String code) {
        return codeToDashboardWidgets.get(code);
    }

    public List<DashboardWidget> getDashboardWidgets() {
        return dashboardWidgets;
    }

    public Map<String, List<WidgetDTO>> getWidgetsGroupByWidgetGroup() {
        return widgetsGroupByWidgetGroup;
    }

    public List<WidgetDTO> getAllDashboardWidgets() {
        return widgetDTOs;
    }

    @Override
    public void load() {
        List<DashboardWidget> dashboardWidgets = reportService.getDashboardWidgets();
        for (DashboardWidget dashboardWidget : dashboardWidgets) {
            try {
                addDashboardWidget(dashboardWidget);
            } catch (Exception e) {
                LOG.error("unable to load config for widget: {}", dashboardWidget.getName(), e);
            }

        }
    }

}
