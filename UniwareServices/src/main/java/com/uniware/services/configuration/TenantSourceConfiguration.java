/*
 * Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *    
 * @version     1.0, 4/16/15 8:32 AM
 * @author amdalal
 */

package com.uniware.services.configuration;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import com.unifier.core.configuration.ConfigurationManager;
import com.uniware.core.api.channel.SourceDetailDTO;
import com.uniware.core.entity.Source;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.unifier.core.annotation.Configuration;
import com.unifier.core.annotation.Level;
import com.unifier.core.configuration.IConfiguration;
import com.unifier.core.entity.TenantSource;
import com.uniware.services.tenant.ITenantService;

@Configuration(name = "tenantSourceConfiguration", level = Level.TENANT, eager = true)
public class TenantSourceConfiguration implements IConfiguration {

    @Autowired
    private transient ITenantService tenantService;

    private List<String>          additionalSources = new ArrayList<>();
    private List<SourceDetailDTO> allowedSources    = new ArrayList<>();
    private List<SourceDetailDTO> disAllowedSources = new ArrayList<>();

    private void addTenantSource(TenantSource ts) {
        if (ts.isEnabled()) {
            additionalSources.add(ts.getSourceCode());
        }
    }

    public List<SourceDetailDTO> getDisallowedSources() {
        return disAllowedSources;
    }

    public List<SourceDetailDTO> getAllowedSources() {
        return allowedSources;
    }

    public List<String> getAdditionalSources() {
        return additionalSources;
    }

    @Override
    @Transactional(readOnly = true)
    public void load() {
        for (TenantSource ts : tenantService.getAllTenantSources()) {
            addTenantSource(ts);
        }
    }

    public void freeze() {
        SourceConfiguration sourceConfiguration = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class);
        Set<String> allowedSourceCodes = new HashSet<>();
        Set<SourceDetailDTO> allowedSources = new TreeSet<>(new Comparator<SourceDetailDTO>() {
            @Override
            public int compare(SourceDetailDTO o1, SourceDetailDTO o2) {
                if (o1.getPriority() != o2.getPriority()) {
                    return o1.getPriority() - o2.getPriority();
                } else {
                    return o1.getCode().compareTo(o2.getCode());
                }
            }
        });
        for (String additionalSource : additionalSources) {
            SourceDetailDTO s = sourceConfiguration.getSourceDTOByCode(additionalSource);
            if (s != null) {
                allowedSources.add(s);
                allowedSourceCodes.add(s.getCode());
            }
        }
        for (Source source : sourceConfiguration.getApplicableSources()) {
            SourceDetailDTO s = sourceConfiguration.getSourceDTOByCode(source.getCode());
            if (s != null) {
                allowedSources.add(s);
                allowedSourceCodes.add(s.getCode());
            }
        }
        this.allowedSources.addAll(allowedSources);
        for (Source source : sourceConfiguration.getAllSources()) {
            if (!allowedSourceCodes.contains(source.getCode())) {
                disAllowedSources.add(sourceConfiguration.getSourceDTOByCode(source.getCode()));
            }
        }
    }
}
