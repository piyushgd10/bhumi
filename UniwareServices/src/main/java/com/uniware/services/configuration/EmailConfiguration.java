/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 21, 2011
 *  @author singla
 */
package com.uniware.services.configuration;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.unifier.core.annotation.Configuration;
import com.unifier.core.annotation.Level;
import com.unifier.core.configuration.IConfiguration;
import com.unifier.core.entity.EmailTemplate;
import com.unifier.core.exception.TemplateCompilationException;
import com.unifier.core.expressions.Expression;
import com.unifier.core.template.Template;
import com.unifier.core.utils.StringUtils;
import com.unifier.services.email.IEmailService;

/**
 * @author singla
 */
@Configuration(name = "emailConfiguration", level = Level.TENANT, eager = true)
public class EmailConfiguration implements IConfiguration {
    private final Map<String, EmailTemplateVO> typeToEmailTemplates = new ConcurrentHashMap<String, EmailTemplateVO>();
    private final List<EmailTemplateVO>        emailTemplateList    = new ArrayList<EmailTemplateVO>();

    @Autowired
    private transient IEmailService emailService;

    /**
     * @param template
     */
    public void addEmailTemplate(EmailTemplate template) {
        EmailTemplateVO templateVO = new EmailTemplateVO();
        templateVO.setType(template.getType());
        if (StringUtils.isNotEmpty(template.getToEmail())) {
            templateVO.setTo(Expression.compile(template.getToEmail()));
        }
        if (StringUtils.isNotEmpty(template.getCcEmail())) {
            templateVO.setCc(Expression.compile(template.getCcEmail()));
        }
        if (StringUtils.isNotEmpty(template.getBccEmail())) {
            templateVO.setBcc(Expression.compile(template.getBccEmail()));
        }
        if (StringUtils.isNotEmpty(template.getFromEmail())) {
            templateVO.setFrom(Expression.compile(template.getFromEmail()));
        }
        if (StringUtils.isNotEmpty(template.getReplyToEmail())) {
            templateVO.setReplyTo(Expression.compile(template.getReplyToEmail()));
        }
        String bodyTemplateName = new StringBuilder("BODY_").append(template.getType()).toString();
        String subjectTemplateName = new StringBuilder("SUBJECT_").append(template.getType()).toString();

        try {
            templateVO.setBodyTemplate(Template.compile(bodyTemplateName, template.getBodyTemplate()));
            templateVO.setSubjectTemplate(Template.compile(subjectTemplateName, template.getSubjectTemplate()));
        } catch (TemplateCompilationException ex) {
        }
        templateVO.setEnabled(template.isEnabled());
        typeToEmailTemplates.put(template.getType(), templateVO);
        emailTemplateList.add(templateVO);
    }

    /**
     * @return
     */
    public List<EmailTemplateVO> getEmailTemplates() {
        return emailTemplateList;
    }

    /**
     * @param templateName
     * @return
     */
    public EmailTemplateVO getTemplateByType(String templateType) {
        return typeToEmailTemplates.get(templateType);
    }

    public static class EmailTemplateVO implements Serializable {
        private String     type;
        private Template   bodyTemplate;
        private Template   subjectTemplate;
        private Expression to;
        private Expression cc;
        private Expression bcc;
        private Expression from;
        private Expression replyTo;
        private int        emailChannelId;
        private boolean    enabled;

        public Template getBodyTemplate() {
            return bodyTemplate;
        }

        public void setBodyTemplate(Template bodyTemplate) {
            this.bodyTemplate = bodyTemplate;
        }

        public Template getSubjectTemplate() {
            return subjectTemplate;
        }

        public void setSubjectTemplate(Template subjectTemplate) {
            this.subjectTemplate = subjectTemplate;
        }

        public Expression getTo() {
            return to;
        }

        public void setTo(Expression to) {
            this.to = to;
        }

        public Expression getCc() {
            return cc;
        }

        public void setCc(Expression cc) {
            this.cc = cc;
        }

        public Expression getBcc() {
            return bcc;
        }

        public void setBcc(Expression bcc) {
            this.bcc = bcc;
        }

        public Expression getFrom() {
            return from;
        }

        public void setFrom(Expression from) {
            this.from = from;
        }

        public Expression getReplyTo() {
            return replyTo;
        }

        public void setReplyTo(Expression replyTo) {
            this.replyTo = replyTo;
        }

        public void setEmailChannelId(int emailChannelId) {
            this.emailChannelId = emailChannelId;
        }

        public int getEmailChannelId() {
            return emailChannelId;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getType() {
            return type;
        }

        /**
         * @return the enabled
         */
        public boolean isEnabled() {
            return enabled;
        }

        /**
         * @param enabled the enabled to set
         */
        public void setEnabled(boolean enabled) {
            this.enabled = enabled;
        }

    }

    @Override
    @Transactional
    public void load() {
        List<EmailTemplate> templates = emailService.getEmailTemplates();
        for (EmailTemplate template : templates) {
            addEmailTemplate(template);
        }
    }

}
