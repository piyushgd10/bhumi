/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 27, 2011
 *  @author singla
 */
package com.uniware.services.configuration;

import com.unifier.core.annotation.Configuration;
import com.unifier.core.annotation.Level;
import com.unifier.core.configuration.IConfiguration;
import com.uniware.core.entity.PickSet;
import com.uniware.core.entity.PickSet.PickSetType;
import com.uniware.core.entity.Section;
import com.uniware.services.admin.pickset.IPicksetService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author singla
 */
@Configuration(name = "pickConfiguration", level = Level.FACILITY)
public class PickConfiguration implements IConfiguration {

    private final Map<Integer, List<Integer>> pickSetToSectionsMap = new ConcurrentHashMap<>();
    private final Map<Integer, PickSet>       idToPickSets         = new ConcurrentHashMap<>();
    private final Map<String, PickSet>        nameToPickSets         = new ConcurrentHashMap<>();
    private final List<PickSet>               pickSets             = new ArrayList<>();
    private PickSet defaultPickSet;

    @Autowired
    private transient IPicksetService picksetService;

    private void addPickSet(PickSet pickSet) {
        pickSets.add(pickSet);
        if (PickSetType.DEFAULT.name().equals(pickSet.getName())) {
            defaultPickSet = pickSet;
        }
        idToPickSets.put(pickSet.getId(), pickSet);
        nameToPickSets.put(pickSet.getName().toUpperCase(), pickSet);
        List<Integer> pickSetToSections = new ArrayList<Integer>();
        pickSetToSectionsMap.put(pickSet.getId(), pickSetToSections);
        for (Section section : pickSet.getSections()) {
            pickSetToSections.add(section.getId());
        }
    }

    public PickSet getPickSetById(int pickSetId) {
        return idToPickSets.get(pickSetId);
    }

    public PickSet getPickSetByName(String pickSetName) {
        return nameToPickSets.get(pickSetName.toUpperCase());
    }

    public PickSet getDefaultPickSet() {
        return defaultPickSet;
    }

    public List<PickSet> getPickSets() {
        return pickSets;
    }

    public void freeze() {
        if (defaultPickSet == null) {
            throw new IllegalStateException("Unable to load pick configurations. No default pickset");
        }
    }

    @Override
    public void load() {
        List<PickSet> pickSets = picksetService.getAllPickSets();
        for (PickSet pickSet : pickSets) {
            addPickSet(pickSet);
        }
    }
}
