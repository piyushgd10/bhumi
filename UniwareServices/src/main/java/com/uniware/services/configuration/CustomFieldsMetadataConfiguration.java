/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jun 24, 2012
 *  @author singla
 */
package com.uniware.services.configuration;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.unifier.core.annotation.Configuration;
import com.unifier.core.annotation.Customizable;
import com.unifier.core.annotation.Level;
import com.unifier.core.configuration.IConfiguration;
import com.unifier.core.entity.CustomFieldMetadata;
import com.unifier.core.entity.CustomFieldValue;
import com.unifier.core.utils.ReflectionUtils;
import com.unifier.core.utils.StringUtils;
import com.unifier.services.customfields.ICustomFieldService;

/**
 * @author singla
 */
@Configuration(name = "customFieldsConfiguration", level = Level.TENANT, eager = true)
public class CustomFieldsMetadataConfiguration implements IConfiguration {

    private static final Logger LOG = LoggerFactory.getLogger(CustomFieldsMetadataConfiguration.class);

    private final Map<String, List<CustomFieldMetadataVO>>        clazzNameToCustomFieldMetadata            = new ConcurrentHashMap<>();
    private final Map<String, Map<String, CustomFieldMetadataVO>> clazzNameToFieldNameToCustomFieldMetadata = new ConcurrentHashMap<>();
    private final Map<String, Class<?>>                           customizableEntityNameToClassMap          = new ConcurrentHashMap<>();
    private String customizableEntityNamesJson;
    private              Map<String, Method>             entityToCustomFieldValueGetterMap = new ConcurrentHashMap<>();
    private              Map<String, Method>             entityToCustomFieldValueSetterMap = new ConcurrentHashMap<>();
    private              Map<String, Method>             entityToIdGetterMap               = new ConcurrentHashMap<>();
    private static final Map<String, PropertyDescriptor> customFieldToDescriptor           = new ConcurrentHashMap<>();

    @Autowired
    private transient ICustomFieldService customFieldService;

    /**
     * @param customFieldMetadata
     * @throws ClassNotFoundException
     * @throws NoSuchMethodException
     * @throws SecurityException
     * @throws IntrospectionException
     */
    private void addCustomFieldMetadata(CustomFieldMetadata customFieldMetadata) {
        List<CustomFieldMetadataVO> customFieldsMetadata = clazzNameToCustomFieldMetadata.get(customFieldMetadata.getEntity());
        if (customFieldsMetadata == null) {
            customFieldsMetadata = new ArrayList<>();
            clazzNameToCustomFieldMetadata.put(customFieldMetadata.getEntity(), customFieldsMetadata);
        }
        try {
            CustomFieldMetadataVO customFieldMetadataVO = new CustomFieldMetadataVO(customFieldMetadata);
            customFieldsMetadata.add(customFieldMetadataVO);
            Map<String, CustomFieldMetadataVO> fieldNameToCustomField = clazzNameToFieldNameToCustomFieldMetadata.get(customFieldMetadata.getEntity());
            if (fieldNameToCustomField == null) {
                fieldNameToCustomField = new HashMap<>();
                clazzNameToFieldNameToCustomFieldMetadata.put(customFieldMetadata.getEntity(), fieldNameToCustomField);
            }
            fieldNameToCustomField.put(customFieldMetadata.getName(), customFieldMetadataVO);
            if (!customFieldMetadataVO.isSerializable()) {
                entityToCustomFieldValueGetterMap.put(customFieldMetadataVO.getClazz().getName(),
                        new PropertyDescriptor("customFieldValue", customFieldMetadataVO.getClazz()).getReadMethod());
                entityToCustomFieldValueSetterMap.put(customFieldMetadataVO.getClazz().getName(),
                        new PropertyDescriptor("customFieldValue", customFieldMetadataVO.getClazz()).getWriteMethod());
                entityToIdGetterMap.put(customFieldMetadataVO.getClazz().getName(), new PropertyDescriptor("id", customFieldMetadataVO.getClazz()).getReadMethod());
            }
        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        }
    }

    public List<CustomFieldMetadataVO> getCustomFieldsByEntity(String entity) {
        int index = entity.indexOf('_');
        if (index != -1) {
            entity = entity.substring(0, index);
        }
        return clazzNameToCustomFieldMetadata.get(entity);
    }

//    public List<CustomFieldMetadataVO> getCustomFieldsByEntityAndType(String entity) {
//        int index = entity.indexOf('_');
//        if (index != -1) {
//            entity = entity.substring(0, index);
//        }
//        return clazzNameToCustomFieldMetadata.get(entity);
//    }

    public String getCustomFieldsByEntityJson(String entity) {
        return new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create().toJson(getCustomFieldsByEntity(entity));
    }

    public CustomFieldMetadataVO getCustomFieldByEntityByFieldName(String entityName, String fieldName) {
        return clazzNameToFieldNameToCustomFieldMetadata.containsKey(entityName) ? clazzNameToFieldNameToCustomFieldMetadata.get(entityName).get(fieldName) : null;
    }

    public String getCustomizableEntitiesJson() {
        return customizableEntityNamesJson;
    }

    public Class<?> getEntityByDisplayName(String displayName) {
        return customizableEntityNameToClassMap.get(displayName);
    }

    public Method getCustomFieldValueGetterByEntity(String name) {
        return entityToCustomFieldValueGetterMap.get(name);
    }

    public Method getCustomFieldValueSetterByEntity(String name) {
        return entityToCustomFieldValueSetterMap.get(name);
    }

    public Method getIdGetterByEntity(String name) {
        return entityToIdGetterMap.get(name);
    }

    public Method getCustomFieldReadMethod(String fieldName) {
        return customFieldToDescriptor.get(fieldName).getReadMethod();
    }

    public Method getCustomFieldWriteMethod(String fieldName) {
        return customFieldToDescriptor.get(fieldName).getWriteMethod();
    }

    public void freeze() {
        List<Class<?>> classes;
        try {
            List<String> customizableEntityNames = new ArrayList<>();
            classes = ReflectionUtils.getClassesAnnotatedWith(Customizable.class, "com.uniware.core.entity");
            for (Class<?> clazz : classes) {
                String displayName = clazz.getAnnotation(Customizable.class).displayName();
                customizableEntityNameToClassMap.put(displayName, clazz);
                customizableEntityNames.add(displayName);
            }
            this.customizableEntityNamesJson = new Gson().toJson(customizableEntityNames);
        } catch (Exception e) {
            LOG.error("Error occured while scanning customizble classes:" + e);
        }

        for (Field f : CustomFieldValue.class.getDeclaredFields()) {
            if (f.getName().contains("value")) {
                try {
                    customFieldToDescriptor.put(f.getName(), new PropertyDescriptor(f.getName(), CustomFieldValue.class));
                } catch (IntrospectionException e) {
                    LOG.error("Unable to load property descriptors for class CustomFieldMetadata", e);
                }
            }
        }
    }

    /**
     * @author singla
     */
    public static class CustomFieldMetadataVO implements Serializable {
        private Class<?>     clazz;
        @Expose
        private String       fieldName;
        private Method       getter;
        private Method       setter;
        private Class<?>     javaType;
        @Expose
        private String       displayName;
        @Expose
        private String       valueType;
        @Expose
        private boolean      required;
        @Expose
        private List<String> possibleValues;

        @Expose
        private String       defaultValue;
        private boolean      serializable;
        private String       mappingFieldName;

        public CustomFieldMetadataVO(CustomFieldMetadata customFieldMetadata) throws ClassNotFoundException, SecurityException, NoSuchMethodException, IntrospectionException {
            this.clazz = Class.forName(customFieldMetadata.getEntity());
            this.setFieldName(customFieldMetadata.getName());
            this.javaType = Class.forName(customFieldMetadata.getJavaType());
            this.serializable = customFieldMetadata.isSerializable();
            if (serializable) {
                this.getter = this.clazz.getMethod(new PropertyDescriptor(customFieldMetadata.getName(), clazz).getReadMethod().getName(), new Class<?>[0]); // this.clazz.getMethod(CustomFieldUtils.getGetterName(customFieldMetadata.getName()), new Class<?>[0]);
                this.setter = this.clazz.getMethod(new PropertyDescriptor(customFieldMetadata.getName(), clazz).getWriteMethod().getName(), new Class<?>[] { javaType }); // this.clazz.getMethod(CustomFieldUtils.getSetterName(customFieldMetadata.getName()), new Class<?>[] { javaType });
            }/* else {
                //
                // If the field is not serializable, then we fetch the values from table 
                // custom_field_metadata, for which getter and setter is instrumented.
                //
                this.getter = this.clazz.getMethod("lookupCustomValue", String.class);
                this.setter = this.clazz.getMethod("setCustomValue", String.class, String.class);
             }*/
            this.displayName = customFieldMetadata.getDisplayName();
            this.valueType = customFieldMetadata.getValueType();
            this.required = customFieldMetadata.isRequired();
            this.possibleValues = customFieldMetadata.getPossibleValues() != null ? StringUtils.split(customFieldMetadata.getPossibleValues()) : null;
            this.defaultValue = customFieldMetadata.getDefaultValue();
            this.mappingFieldName = customFieldMetadata.getMappingFieldName();
        }

        /**
         * @return the clazz
         */
        public Class<?> getClazz() {
            return clazz;
        }

        /**
         * @param clazz the clazz to set
         */
        public void setClazz(Class<?> clazz) {
            this.clazz = clazz;
        }

        /**
         * @return the getter
         */
        public Method getGetter() {
            return getter;
        }

        /**
         * @param getter the getter to set
         */
        public void setGetter(Method getter) {
            this.getter = getter;
        }

        /**
         * @return the setter
         */
        @JsonIgnore
        public Method getSetter() {
            return setter;
        }

        /**
         * @param setter the setter to set
         */
        public void setSetter(Method setter) {
            this.setter = setter;
        }

        /**
         * @return the javaType
         */
        @JsonIgnore
        public Class<?> getJavaType() {
            return javaType;
        }

        /**
         * @param javaType the javaType to set
         */
        public void setJavaType(Class<?> javaType) {
            this.javaType = javaType;
        }

        /**
         * @return the fieldName
         */
        public String getFieldName() {
            return fieldName;
        }

        /**
         * @param fieldName the fieldName to set
         */
        public void setFieldName(String fieldName) {
            this.fieldName = fieldName;
        }

        /**
         * @return the displayName
         */
        public String getDisplayName() {
            return displayName;
        }

        /**
         * @param displayName the displayName to set
         */
        public void setDisplayName(String displayName) {
            this.displayName = displayName;
        }

        /**
         * @return the valueType
         */
        public String getValueType() {
            return valueType;
        }

        /**
         * @param valueType the valueType to set
         */
        public void setValueType(String valueType) {
            this.valueType = valueType;
        }

        /**
         * @return the required
         */
        public boolean isRequired() {
            return required;
        }

        /**
         * @param required the required to set
         */
        public void setRequired(boolean required) {
            this.required = required;
        }

        /**
         * @return the possibleValues
         */
        public List<String> getPossibleValues() {
            return possibleValues;
        }

        /**
         * @param possibleValues the possibleValues to set
         */
        public void setPossibleValues(List<String> possibleValues) {
            this.possibleValues = possibleValues;
        }

        /**
         * @return the defaultValue
         */
        public String getDefaultValue() {
            return defaultValue;
        }

        /**
         * @param defaultValue the defaultValue to set
         */
        public void setDefaultValue(String defaultValue) {
            this.defaultValue = defaultValue;
        }

        /**
         * @return the serializable
         */
        public boolean isSerializable() {
            return serializable;
        }

        /**
         * @param serializable the serializable to set
         */
        public void setSerializable(boolean serializable) {
            this.serializable = serializable;
        }

        /**
         * @return the mappingFieldName
         */
        public String getMappingFieldName() {
            return mappingFieldName;
        }

        /**
         * @param mappingFieldName the mappingFieldName to set
         */
        public void setMappingFieldName(String mappingFieldName) {
            this.mappingFieldName = mappingFieldName;
        }

    }

    @Override
    public void load() {
        List<CustomFieldMetadata> customFieldsMetadata = customFieldService.getCustomFieldsMetadata();
        for (CustomFieldMetadata customFieldMetadata : customFieldsMetadata) {
            addCustomFieldMetadata(customFieldMetadata);
        }
    }
}
