/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 11-Feb-2015
 *  @author parijat
 */
package com.uniware.services.configuration;

import com.google.gson.Gson;
import com.unifier.core.annotation.Configuration;
import com.unifier.core.annotation.Level;
import com.unifier.core.configuration.IConfiguration;
import com.unifier.core.utils.StringUtils;
import com.uniware.core.api.shipping.ShippingSourceDetailDTO;
import com.uniware.core.entity.ShippingMethod;
import com.uniware.core.entity.ShippingProviderSource;
import com.uniware.core.entity.ShippingSourceConnector;
import com.uniware.services.shipping.IShippingAdminService;
import com.uniware.services.shipping.IShippingService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Configuration(name = "shippingSourceConfiguration", level = Level.GLOBAL)
public class ShippingSourceConfiguration implements IConfiguration {

    private final List<ShippingProviderSource> sources = new ArrayList<>();

    private final Map<String, ShippingProviderSource>  idToSource                    = new ConcurrentHashMap<>();
    private final Map<String, ShippingProviderSource>  codeToSource                  = new ConcurrentHashMap<>();
    private final Map<String, ShippingSourceDetailDTO> codeToShippingSourceDetailDTO = new ConcurrentHashMap<>();

    private final Map<String, String>                               codeToNameMap                 = new ConcurrentHashMap<>();
    private final Map<String, Map<String, ShippingSourceConnector>> sourceToNameToSourceConnector = new ConcurrentHashMap<>();
    private final StringBuilder                                     shippingMethodNamesCSV        = new StringBuilder();

    @Autowired
    private transient IShippingAdminService shippingAdminService;

    @Autowired
    private transient IShippingService shippingService;

    private void addSource(ShippingProviderSource source) {
        sources.add(source);
        idToSource.put(source.getId(), source);
        codeToNameMap.put(source.getCode(), source.getName());
        codeToSource.put(source.getCode(), source);
        Map<String, ShippingSourceConnector> nameToSourceConnector = new HashMap<String, ShippingSourceConnector>();
        sourceToNameToSourceConnector.put(source.getCode(), nameToSourceConnector);
        for (ShippingSourceConnector connector : source.getShippingSourceConnectors()) {
            nameToSourceConnector.put(connector.getName(), connector);
        }
    }

    public String getShippingSourcesJson() {
        return new Gson().toJson(codeToShippingSourceDetailDTO.values());
    }

    public ShippingSourceConnector getShippingSourceConnector(String shippingSourceCode, String connectorName) {
        return sourceToNameToSourceConnector.get(shippingSourceCode).get(connectorName);
    }

    public ShippingProviderSource getShippingSourceByCode(String code) {
        return codeToSource.get(code);
    }

    public ShippingProviderSource getById(Integer id) {
        return idToSource.get(id);
    }

    public List<ShippingProviderSource> getAllShippingSources() {
        return sources;
    }

    @Override
    public void load() {
        List<ShippingMethod> shippingMethods = shippingService.getShippingMethods();
        int index = 0;
        for (ShippingMethod shippingMethod : shippingMethods) {
            shippingMethodNamesCSV.append(shippingMethod.getName()).append((index < shippingMethods.size()) ? "," : StringUtils.EMPTY_STRING);
            index++;
        }
        List<ShippingProviderSource> shippingSources = shippingAdminService.getAllShippingProviderSources();
        for (ShippingProviderSource shippingSource : shippingSources) {
            if(shippingSource.isEnabled()) {
                codeToShippingSourceDetailDTO.put(shippingSource.getCode(), new ShippingSourceDetailDTO(shippingSource, shippingMethodNamesCSV.toString()));
            }
            addSource(shippingSource);
        }
    }

}
