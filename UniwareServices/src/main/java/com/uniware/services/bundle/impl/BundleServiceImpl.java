/*
 *  Copyright 2013 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 26-Jul-2013
 *  @author sunny
 */
package com.uniware.services.bundle.impl;

import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.NumberUtils;
import com.unifier.services.aspect.RollbackOnFailure;
import com.uniware.core.api.bundle.BundleDTO;
import com.uniware.core.api.bundle.ComponentItemTypeDTO;
import com.uniware.core.api.bundle.CreateBundleRequest;
import com.uniware.core.api.bundle.CreateBundleResponse;
import com.uniware.core.api.bundle.EditBundleRequest;
import com.uniware.core.api.bundle.EditBundleResponse;
import com.uniware.core.api.bundle.GetBundleByCodeRequest;
import com.uniware.core.api.bundle.GetBundleByCodeResponse;
import com.uniware.core.api.bundle.WsComponentItemType;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.entity.Bundle;
import com.uniware.core.entity.BundleItemType;
import com.uniware.core.entity.ItemType;
import com.uniware.core.entity.ItemType.Type;
import com.uniware.dao.bundle.IBundleDao;
import com.uniware.services.bundle.IBundleService;
import com.uniware.services.catalog.ICatalogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author Sunny
 */
@Service("bundleService")
public class BundleServiceImpl implements IBundleService {

    @Autowired
    private IBundleDao      bundleDao;

    @Autowired
    private ICatalogService catalogService;

    @Override
    @Transactional
    @RollbackOnFailure
    public CreateBundleResponse createBundle(CreateBundleRequest request) {
        CreateBundleResponse response = new CreateBundleResponse();
        ValidationContext context = request.validate();
        BigDecimal totalPrice = BigDecimal.ZERO;
        ItemType itemType = null;
        if (!context.hasErrors()) {
            itemType = catalogService.getAllItemTypeBySkuCode(request.getWsBundle().getItemSku());
            if (itemType == null || !Type.BUNDLE.equals(itemType.getType())) {
                context.addError(WsResponseCode.INVALID_ITEM_TYPE);
            } else {
                Bundle bundle = bundleDao.getBundleByCode(request.getWsBundle().getItemSku());
                if (bundle != null) {
                    context.addError(WsResponseCode.BUNDLE_ALREADY_EXISTS);
                } else {
                    for (WsComponentItemType wsComponentItemType : request.getWsBundle().getWsComponentItemTypes()) {
                        totalPrice = totalPrice.add(NumberUtils.multiply(wsComponentItemType.getPrice(), wsComponentItemType.getQuantity()));
                    }
                    if (NumberUtils.equals(BigDecimal.ZERO, totalPrice)) {
                        context.addError(WsResponseCode.INVALID_BUNDLE_PRICE);
                    }
                }
            }
        }

        if (!context.hasErrors()) {
            Bundle bundle = new Bundle();
            bundle.setCode(itemType.getSkuCode());
            bundle.setItemType(itemType);
            bundle.setCreated(DateUtils.getCurrentTime());
            BigDecimal totalRatio = BigDecimal.ZERO;
            for (WsComponentItemType wsComponentItemType : request.getWsBundle().getWsComponentItemTypes()) {
                ItemType componentItemType = catalogService.getAllItemTypeBySkuCode(wsComponentItemType.getItemSku());
                if (componentItemType == null) {
                    context.addError(WsResponseCode.INVALID_ITEM_TYPE, "Invalid item sku: " + wsComponentItemType.getItemSku());
                } else if (ItemType.Type.BUNDLE.equals(componentItemType.getType())) {
                    context.addError(WsResponseCode.INVALID_ITEM_TYPE, "Sku of type BUNDLE cannot be added to a bundle");
                } else {
                    BundleItemType bundleItemType = new BundleItemType();
                    BigDecimal ratio = NumberUtils.divide(wsComponentItemType.getPrice(), totalPrice, 5);
                    totalRatio = totalRatio.add(NumberUtils.multiply(ratio, wsComponentItemType.getQuantity(), 5));
                    bundleItemType.setItemType(componentItemType);
                    bundleItemType.setQuantity(wsComponentItemType.getQuantity());
                    bundleItemType.setPrice(wsComponentItemType.getPrice());
                    bundleItemType.setPriceRatio(ratio);
                    bundleItemType.setCreated(DateUtils.getCurrentTime());
                    bundleItemType.setBundle(bundle);
                    if (bundle.getBundleItemTypes().contains(bundleItemType)) {
                        context.addError(WsResponseCode.INVALID_ITEM_TYPE, "Duplicate component item type: " + componentItemType.getSkuCode());
                    } else {
                        bundle.getBundleItemTypes().add(bundleItemType);
                    }
                }
            }
            BigDecimal differenceRatio = BigDecimal.ONE.subtract(totalRatio);
            if (!NumberUtils.equals(BigDecimal.ZERO, differenceRatio)) {
                Iterator<BundleItemType> bundleItemTypeIterator = bundle.getBundleItemTypes().iterator();
                while(bundleItemTypeIterator.hasNext()){
                    BundleItemType bundleItemType = bundleItemTypeIterator.next();
                    if(NumberUtils.greaterThan(bundleItemType.getPriceRatio().add(differenceRatio),BigDecimal.ZERO)){
                        bundleItemType.setPriceRatio(bundleItemType.getPriceRatio().add(differenceRatio));
                        break;
                    }
                }
            }
            if (!context.hasErrors()) {
                bundle = bundleDao.create(bundle);
                response.setBundleDTO(new BundleDTO(bundle));
                response.setSuccessful(true);
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    @Override
    @Transactional
    @RollbackOnFailure
    public EditBundleResponse editBundle(EditBundleRequest request) {
        EditBundleResponse response = new EditBundleResponse();
        ValidationContext context = request.validate();
        BigDecimal totalPrice = BigDecimal.ZERO;
        Bundle bundle = null;
        if (!context.hasErrors()) {
            bundle = bundleDao.getBundleByCode(request.getWsBundle().getItemSku());
            if (bundle == null) {
                context.addError(WsResponseCode.INVALID_BUNDLE_CODE);
            } else {
                for (WsComponentItemType wsComponentItemType : request.getWsBundle().getWsComponentItemTypes()) {
                    totalPrice = totalPrice.add(NumberUtils.multiply(wsComponentItemType.getPrice(), wsComponentItemType.getQuantity()));
                }
                if (NumberUtils.equals(BigDecimal.ZERO, totalPrice)) {
                    context.addError(WsResponseCode.INVALID_BUNDLE_PRICE);
                }
            }
        }
        Map<String, BundleItemType> skuToBundleItemType = new HashMap<String, BundleItemType>(bundle.getBundleItemTypes().size());
        for (BundleItemType bundleItemType : bundle.getBundleItemTypes()) {
            skuToBundleItemType.put(bundleItemType.getItemType().getSkuCode(), bundleItemType);
        }

        if (!context.hasErrors()) {
            for (WsComponentItemType wsComponentItemType : request.getWsBundle().getWsComponentItemTypes()) {
                BundleItemType bundleItemType = skuToBundleItemType.get(wsComponentItemType.getItemSku());
                if (bundleItemType == null) {
                    context.addError(WsResponseCode.INVALID_BUNDLE_ITEM_TYPE, "Sku " + wsComponentItemType.getItemSku() + " does not exist in bundle");
                } else {
                    bundleItemType.setQuantity(wsComponentItemType.getQuantity());
                    bundleItemType.setPrice(wsComponentItemType.getPrice());
                    bundleItemType.setPriceRatio(NumberUtils.divide(wsComponentItemType.getPrice(), totalPrice, 5));
                    bundleItemType.setCreated(DateUtils.getCurrentTime());
                    bundleItemType.setBundle(bundle);
                }
            }
            if (!context.hasErrors()) {
                bundle = bundleDao.update(bundle);
                response.setBundleDTO(new BundleDTO(bundle));
                response.setSuccessful(true);
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    @Override
    @Transactional(readOnly = true)
    public Bundle getBundleByCode(String itemSkuCode) {
        return bundleDao.getBundleByCode(itemSkuCode);
    }


    @Override
    @Transactional(readOnly = true)
    public List<ComponentItemTypeDTO> getBundleComponents(String bundleSku) {
        Bundle bundle = getBundleByCode(bundleSku);
        List<ComponentItemTypeDTO> componentItemTypeDTOs = new ArrayList<ComponentItemTypeDTO>(bundle.getBundleItemTypes().size());
        for (BundleItemType bundleItemType : bundle.getBundleItemTypes()) {
            componentItemTypeDTOs.add(new ComponentItemTypeDTO(bundleItemType));
        }
        return componentItemTypeDTOs;
    }

    @Override
    @Transactional(readOnly = true)
    public Bundle getBundleBySkuCode(String bundleSkuCode) {
        return bundleDao.getBundleBySkuCode(bundleSkuCode);
    }

    @Override
    public GetBundleByCodeResponse getBundleByCode(GetBundleByCodeRequest request) {
        GetBundleByCodeResponse response = new GetBundleByCodeResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            Bundle bundle = getBundleByCode(request.getCode());
            if (bundle != null) {
                response.setBundleDTO(new BundleDTO(bundle));
                response.setSuccessful(true);
            } else {
                context.addError(WsResponseCode.INVALID_BUNDLE_CODE);
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    @Override
    @Transactional
    public List<Bundle> getBundlesByComponentItemType(String itemSku) {
        return bundleDao.getBundlesByComponentItemType(itemSku);
    }

}
