/*
 *  Copyright 2013 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 26-Jul-2013
 *  @author sunny
 */
package com.uniware.services.bundle;

import java.util.List;

import com.uniware.core.api.bundle.ComponentItemTypeDTO;
import com.uniware.core.api.bundle.CreateBundleRequest;
import com.uniware.core.api.bundle.CreateBundleResponse;
import com.uniware.core.api.bundle.EditBundleRequest;
import com.uniware.core.api.bundle.EditBundleResponse;
import com.uniware.core.api.bundle.GetBundleByCodeRequest;
import com.uniware.core.api.bundle.GetBundleByCodeResponse;
import com.uniware.core.entity.Bundle;

public interface IBundleService {

    CreateBundleResponse createBundle(CreateBundleRequest request);

    GetBundleByCodeResponse getBundleByCode(GetBundleByCodeRequest request);

    Bundle getBundleByCode(String itemSkuCode);

    EditBundleResponse editBundle(EditBundleRequest request);

    List<Bundle> getBundlesByComponentItemType(String itemSku);

    List<ComponentItemTypeDTO> getBundleComponents(String bundleSku);

    Bundle getBundleBySkuCode(String bundleSkuCode);
}