/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 23-Apr-2014
 *  @author parijat
 */
package com.uniware.services.audit;

import java.util.Map;

import com.unifier.core.api.audit.AuditItem;
import com.unifier.core.api.audit.FetchEntityAuditRequest;
import com.unifier.core.api.audit.FetchEntityAuditResponse;
import com.unifier.core.api.audit.GetEntitiesAuditLogsRequest;
import com.unifier.core.api.audit.GetEntitiesAuditLogsResponse;

/**
 * @author parijat
 *
 */
public interface IAuditService {
    
    void saveAuditLog(String methodName, String methodAuditLog, Map<String, AuditItem> entityAuditMap);

    FetchEntityAuditResponse getEntityAuditLog(FetchEntityAuditRequest request);

    GetEntitiesAuditLogsResponse getEntitiesAuditLog(GetEntitiesAuditLogsRequest request);

    //    Map<String, AuditItem> peristsAuditContext();
}
