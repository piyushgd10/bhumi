/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 23-Apr-2014
 *  @author parijat
 */
package com.uniware.services.audit.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unifier.core.api.audit.AuditItem;
import com.unifier.core.api.audit.EntityAuditDto;
import com.unifier.core.api.audit.FetchEntityAuditRequest;
import com.unifier.core.api.audit.FetchEntityAuditResponse;
import com.unifier.core.api.audit.GetEntitiesAuditLogsRequest;
import com.unifier.core.api.audit.GetEntitiesAuditLogsResponse;
import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.utils.DateUtils;
import com.unifier.dao.audit.IAuditMao;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.vo.EntityAuditLogVO;
import com.uniware.services.audit.IAuditService;

@Service("entityAuditService")
public class AuditServiceImpl implements IAuditService {

    @Autowired
    private IAuditMao           auditMao;

    private static final Logger LOG            = LoggerFactory.getLogger(AuditServiceImpl.class);

    @Override
    public void saveAuditLog(String methodName, String methodAuditLog, Map<String, AuditItem> entityAuditMap) {
        for (AuditItem auditItem : entityAuditMap.values()) {
            EntityAuditLogVO auditLogVO = new EntityAuditLogVO(auditItem);
            auditLogVO.setCreated(DateUtils.getCurrentTime());
            auditLogVO.setMethodName(methodName);
            auditLogVO.setAuditLog(methodAuditLog);
            auditMao.save(auditLogVO);
            LOG.info("Saved audit log for entity {} with method {}", auditLogVO.getEntityName(), methodName);
        }
    }


    @Override
    public FetchEntityAuditResponse getEntityAuditLog(FetchEntityAuditRequest request) {
        FetchEntityAuditResponse response = new FetchEntityAuditResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            List<EntityAuditLogVO> auditLogVOList = auditMao.getEntityAuditById(request.getEntityName(), request.getEntityIdentifier());
            if (auditLogVOList == null || auditLogVOList.size() == 0) {
                context.addError(WsResponseCode.INVALID_ENTITY_IDENTIFIER_NAME, "No audit entry found. Please provide valid entity name and identifier.");
                response.setSuccessful(false);
            } else {
                List<EntityAuditDto> auditDtos = new ArrayList<>();
                for (EntityAuditLogVO entityVO : auditLogVOList) {
                    //                    MethodAuditLogVO methodVO = auditMao.getMethodAuditVO(entityVO.getMethodReferenceId());
                    EntityAuditDto auditDto = new EntityAuditDto(entityVO);
                    auditDtos.add(auditDto);
                }
                response.setAuditDtos(auditDtos);
                response.setSuccessful(true);
            }
        } else {
            response.setSuccessful(false);
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    public GetEntitiesAuditLogsResponse getEntitiesAuditLog(GetEntitiesAuditLogsRequest request) {
        GetEntitiesAuditLogsResponse response = new GetEntitiesAuditLogsResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            List<EntityAuditLogVO> entitiesLogVOs = auditMao.getEntitiesAuditByTenant(request.isFilterByFacility());
            List<EntityAuditDto> auditDtos = new ArrayList<EntityAuditDto>();
            for (EntityAuditLogVO auditLogVO : entitiesLogVOs) {
                //                MethodAuditLogVO methodVO = auditMao.getMethodAuditVO(auditLogVO.getMethodReferenceId());
                auditDtos.add(new EntityAuditDto(auditLogVO));
            }
            response.setEntitiesAuditLogs(auditDtos);
            response.setSuccessful(true);
        } else {
            response.setSuccessful(false);
        }
        response.setErrors(context.getErrors());
        return response;
    }

}
