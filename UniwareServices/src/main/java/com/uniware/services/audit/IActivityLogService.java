/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 18-Jun-2014
 *  @author parijat
 */
package com.uniware.services.audit;

import com.unifier.core.api.audit.ActivityDto;
import com.unifier.core.api.audit.FetchEntityActivityRequest;
import com.unifier.core.api.audit.FetchEntityActivityResponse;

/**
 * @author parijat
 */
public interface IActivityLogService {

    void persistActivityLogs(ActivityDto dto);

    //    ActivityDto getActivityLogsbyEntityId(String identifier);

    FetchEntityActivityResponse getActivityLogsbyEntityId(FetchEntityActivityRequest request);

    FetchEntityActivityResponse getActivityLogsByIdentifier(FetchEntityActivityRequest request);

    int getActivityLogCountbyEntityId(String entityIdentifier, String entityName);
}
