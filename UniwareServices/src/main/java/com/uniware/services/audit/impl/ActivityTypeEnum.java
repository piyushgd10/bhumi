/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 08-Aug-2014
 *  @author parijat
 */
package com.uniware.services.audit.impl;

public enum ActivityTypeEnum {

    CREATION,
    EDIT,
    PROCESSING,
    PAUSED,
    COMPLETE,
    CANCEL,
    REPLACEMENT,
    RESHIPMENT,
    RETURN,
    COMMENT,
    CHANNEL_ADD,
    CONFIGURATION_PARAM_CHANGED,
    SYNC_ON_OFF,
    SYNC_TRIGGERED,
    CHANNEL_STATUS,
    CONNECTOR_PARAMS,
    REJECT,
    AMEND,
    APPROVE,
    QUALITY_CHECK,
    FAILED
}
