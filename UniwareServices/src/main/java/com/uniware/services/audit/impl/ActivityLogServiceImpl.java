/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 18-Jun-2014
 *  @author parijat
 */
package com.uniware.services.audit.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unifier.core.api.audit.ActivityDto;
import com.unifier.core.api.audit.FetchEntityActivityRequest;
import com.unifier.core.api.audit.FetchEntityActivityResponse;
import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.utils.StringUtils;
import com.unifier.dao.audit.IActivityLogMao;
import com.uniware.core.vo.ActivityMetaVO;
import com.uniware.services.audit.IActivityLogService;

/**
 * @author parijat
 */
@Service("activityLogService")
public class ActivityLogServiceImpl implements IActivityLogService {

    @Autowired
    private IActivityLogMao activityLogMao;

    /* (non-Javadoc)
     * @see com.uniware.services.audit.IActivityLogService#persistActivityLogs(com.unifier.core.api.audit.ActivityLogDto)
     */
    @Override
    public void persistActivityLogs(ActivityDto dto) {
        for (String log : dto.getActivityLogs()) {
            ActivityMetaVO vo = new ActivityMetaVO();
            vo.setEntityName(dto.getEntityName());
            vo.setIdentifier(dto.getIdentifier());
            if (StringUtils.isNotBlank(dto.getGroupIdentifier())) {
                vo.setGroupIdentifier(dto.getGroupIdentifier());
            } else {
                vo.setGroupIdentifier(dto.getIdentifier());
            }
            vo.setActivityType(dto.getActivityType());
            vo.setLog(log);
            activityLogMao.persistActivityMetaVO(vo);
            /*ActivityMetaLog metaLog = new ActivityMetaLog();
            metaLog.setActivityType(dto.getActivityType());
            metaLog.setEntityName(dto.getEntityName());
            metaLog.setIdentifier(dto.getIdentifier());
            metaLog.setGroupIdentifier(dto.getGroupIdentifier());
            metaLog.setLog(log);
            activityLogMao.saveActivityMetaLog(metaLog);*/
        }
    }

    /* (non-Javadoc)
     * @see com.uniware.services.audit.IActivityLogService#getActivityLogsbyEntityId(java.lang.String)
     */
    @Override
    public FetchEntityActivityResponse getActivityLogsbyEntityId(FetchEntityActivityRequest request) {
        FetchEntityActivityResponse response = new FetchEntityActivityResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            response.setEntityActivityLogs(activityLogMao.getActivitybyIdentifierOrGroupIdentifier(request.getEntityIdentifier(), request.getEntityName(), request.isSort()));
            response.setSuccessful(true);
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    public FetchEntityActivityResponse getActivityLogsByIdentifier(FetchEntityActivityRequest request) {
        FetchEntityActivityResponse response = new FetchEntityActivityResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            response.setEntityActivityLogs(activityLogMao.getActivitybyIdentifier(request.getEntityIdentifier(), request.getEntityName(), request.isSort()));
            response.setSuccessful(true);
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    public int getActivityLogCountbyEntityId(String entityIdentifier, String entityName){
        return activityLogMao.getActivityCountByIdentifier(entityIdentifier, entityName);
    }

}
