/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 24-Jun-2014
 *  @author parijat
 */
package com.uniware.services.audit.impl;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unifier.core.annotation.audit.AuditField;
import com.unifier.core.api.audit.ActivityDto;
import com.unifier.core.utils.CollectionUtils;
import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.DateUtils.Resolution;
import com.unifier.core.utils.StringUtils;
import com.uniware.core.utils.ActivityContext;

/**
 * @author parijat
 */
public class ActivityUtils {

    private static final Logger LOG = LoggerFactory.getLogger(ActivityUtils.class);

    public static void appendActivity(String identifier, String entityName, String grpIdentifier, List<String> activityLogs, String activityType) {
        if (!ActivityContext.current().getLoggingDtos().containsKey(identifier)) {
            ActivityDto dto = new ActivityDto(entityName, identifier, grpIdentifier, activityType);
            ActivityContext.current().getLoggingDtos().put(identifier, dto);
        }
        ActivityContext.current().getLoggingDtos().get(identifier).getActivityLogs().addAll(activityLogs);
    }

    public static String getAccessorNameForField(String name) {
        return new StringBuilder("is").append(name.substring(0, 1).toUpperCase()).append(name.substring(1)).toString();
    }

    public static void getObjectDiff(Object oldEntity, Object newEntity, Map<String, ArrayList<String>> channelMap) {
        HashSet<String> methodSet = new HashSet<String>();
        for (Method method : oldEntity.getClass().getMethods()) {
            methodSet.add(method.getName());
        }
        for (Field field : oldEntity.getClass().getDeclaredFields()) {
            if (field.isAnnotationPresent(AuditField.class)) {
                String getter = null;
                if (field.getType().equals(boolean.class)) {
                    getter = getAccessorNameForField(field.getName());
                } else {
                    getter = StringUtils.getAccessorNameForField(field.getName());
                }
                if (methodSet.contains(getter)) {
                    try {
                        Method method = oldEntity.getClass().getMethod(getter);
                        Object oldVal = method.invoke(oldEntity);
                        Object newVal = method.invoke(newEntity);
                        if ((oldVal instanceof Collection<?> && newVal instanceof Collection<?>) || (oldVal instanceof List<?> && newVal instanceof List<?>)) {
                            @SuppressWarnings("unchecked")
                            List<Object> oldEntityList = CollectionUtils.asList((Collection<Object>) oldVal);
                            @SuppressWarnings("unchecked")
                            List<Object> newEntityList = CollectionUtils.asList((Collection<Object>) newVal);
                            Map<String, Object> newEntityMap = new HashMap<>();
                            for (Object entity : newEntityList) {
                                String fieldName = StringUtils.EMPTY_STRING;
                                for (Field entityField : entity.getClass().getDeclaredFields()) {
                                    if (entityField.isAnnotationPresent(AuditField.class) && StringUtils.isNotBlank(entityField.getAnnotation(AuditField.class).fieldName())) {
                                        fieldName = entityField.getAnnotation(AuditField.class).fieldName();
                                        String methodName = StringUtils.getAccessorNameForField(fieldName);
                                        String name = StringUtils.convert(entity.getClass().getMethod(methodName).invoke(entity), String.class);
                                        newEntityMap.put(name, entity);
                                        break;
                                    }
                                }

                            }
                            for (Object o1 : oldEntityList) {
                                Object o2 = null;
                                String fieldName = StringUtils.EMPTY_STRING;
                                for (Field entityField : o1.getClass().getDeclaredFields()) {
                                    if (entityField.isAnnotationPresent(AuditField.class) && StringUtils.isNotBlank(entityField.getAnnotation(AuditField.class).fieldName())) {
                                        fieldName = entityField.getAnnotation(AuditField.class).fieldName();
                                        String methodName = StringUtils.getAccessorNameForField(fieldName);
                                        String name = StringUtils.convert(o1.getClass().getMethod(methodName).invoke(o1), String.class);
                                        o2 = newEntityMap.get(name);
                                        break;
                                    }
                                }
                                if (o2 != null) {
                                    getObjectDiff(o1, o2, channelMap);
                                }
                            }
                        } else if (oldVal instanceof Date && newVal instanceof Date) {
                            if (DateUtils.diff((Date) oldVal, (Date) newVal, Resolution.MILLISECOND) > 0) {
                                ArrayList<String> oldNewPairList = new ArrayList<String>();
                                oldNewPairList.add(DateUtils.dateToString((Date) oldVal, "yyyy-MM-dd HH:mm:ss"));
                                oldNewPairList.add(DateUtils.dateToString((Date) newVal, "yyyy-MM-dd HH:mm:ss"));
                                channelMap.put(field.getName(), oldNewPairList);
                            }
                        } else {
                            if (oldVal != null && newVal != null) {
                                String oldString = StringUtils.EMPTY_STRING;
                                String newString = StringUtils.EMPTY_STRING;
                                if (StringUtils.isNotBlank(field.getAnnotation(AuditField.class).fieldValue())) {
                                    String methodName = StringUtils.getAccessorNameForField(field.getAnnotation(AuditField.class).fieldValue());
                                    Method[] methods = field.getType().getDeclaredMethods();
                                    boolean methodContained = false;
                                    for (Method fieldMethod : methods) {
                                        if (fieldMethod.getName().contains(methodName)) {
                                            methodContained = true;
                                            break;
                                        }
                                    }
                                    if (methodContained) {
                                        oldString = StringUtils.convert(field.getType().getMethod(methodName).invoke(oldVal), String.class);
                                        newString = StringUtils.convert(field.getType().getMethod(methodName).invoke(newVal), String.class);
                                    } else {
                                        oldString = StringUtils.convert(field.getType().getSuperclass().getMethod(methodName).invoke(oldVal), String.class);
                                        newString = StringUtils.convert(field.getType().getSuperclass().getMethod(methodName).invoke(newVal), String.class);
                                    }

                                } else {
                                    oldString = StringUtils.convert(oldVal, String.class);
                                    newString = StringUtils.convert(newVal, String.class);
                                }
                                if (!oldString.equals(newString)) {
                                    ArrayList<String> oldNewPairList = new ArrayList<String>();
                                    oldNewPairList.add(oldString.toLowerCase());
                                    oldNewPairList.add(newString.toLowerCase());
                                    String fieldName = field.getName();
                                    if (StringUtils.isNotBlank(field.getAnnotation(AuditField.class).fieldName())) {
                                        fieldName = StringUtils.convert(
                                                oldEntity.getClass().getMethod(StringUtils.getAccessorNameForField(field.getAnnotation(AuditField.class).fieldName())).invoke(
                                                        oldEntity), String.class);
                                    }
                                    channelMap.put(fieldName, oldNewPairList);
                                }
                            } else {
                                if (oldVal == null && newVal != null) {
                                    String newString = StringUtils.EMPTY_STRING;
                                    if (StringUtils.isNotBlank(field.getAnnotation(AuditField.class).fieldValue())) {
                                        String methodName = StringUtils.getAccessorNameForField(field.getAnnotation(AuditField.class).fieldValue());
                                        Method[] methods = field.getType().getDeclaredMethods();
                                        boolean methodContained = false;
                                        for (Method fieldMethod : methods) {
                                            if (fieldMethod.getName().contains(methodName)) {
                                                methodContained = true;
                                                break;
                                            }
                                        }
                                        if (methodContained) {
                                            newString = StringUtils.convert(field.getType().getMethod(methodName).invoke(newVal), String.class);
                                        } else {
                                            newString = StringUtils.convert(field.getType().getSuperclass().getMethod(methodName).invoke(newVal), String.class);
                                        }

                                    } else {
                                        newString = StringUtils.convert(newVal, String.class);
                                    }
                                    ArrayList<String> oldNewPairList = new ArrayList<String>();
                                    oldNewPairList.add(newString.toLowerCase());
                                    String fieldName = field.getName();
                                    channelMap.put(fieldName, oldNewPairList);
                                } else if (oldVal != null && newVal == null) {
                                    String oldString = StringUtils.EMPTY_STRING;
                                    if (StringUtils.isNotBlank(field.getAnnotation(AuditField.class).fieldValue())) {
                                        String methodName = StringUtils.getAccessorNameForField(field.getAnnotation(AuditField.class).fieldValue());
                                        Method[] methods = field.getType().getDeclaredMethods();
                                        boolean methodContained = false;
                                        for (Method fieldMethod : methods) {
                                            if (fieldMethod.getName().contains(methodName)) {
                                                methodContained = true;
                                                break;
                                            }
                                        }
                                        if (methodContained) {
                                            oldString = StringUtils.convert(field.getType().getMethod(methodName).invoke(oldVal), String.class);
                                        } else {
                                            oldString = StringUtils.convert(field.getType().getSuperclass().getMethod(methodName).invoke(oldVal), String.class);
                                        }

                                    } else {
                                        oldString = StringUtils.convert(oldVal, String.class);
                                    }
                                    ArrayList<String> oldNewPairList = new ArrayList<String>();
                                    oldNewPairList.add(oldString.toLowerCase());
                                    oldNewPairList.add("null");
                                    String fieldName = field.getName();
                                    channelMap.put(fieldName, oldNewPairList);
                                }
                            }
                        }
                    } catch (Exception e) {
                        throw new RuntimeException(e);
                    }
                }
            }
        }
    }

    public static String getChannelSpecificActivityLogs(HashMap<String, ArrayList<String>> entityDifferenceMap) {
        ArrayList<String> channelActivitLogList = new ArrayList<String>();
        for (Entry<String, ArrayList<String>> logEntry : entityDifferenceMap.entrySet()) {
            String activityLogKey = logEntry.getKey();
            if (!org.apache.commons.lang.StringUtils.containsIgnoreCase(activityLogKey, "password")) {
                ArrayList<String> activityLogValue = logEntry.getValue();
                String oldActivityParam = null;
                String newActivityParam = null;
                if (activityLogValue.size() == 2) {
                    oldActivityParam = activityLogValue.get(0);
                    newActivityParam = activityLogValue.get(1);
                } else {
                    newActivityParam = activityLogValue.get(0);
                }
                if (activityLogKey.equals("inventorySyncStatus")) {
                    channelActivitLogList.add("Inventory Sync Was Put " + newActivityParam.toUpperCase());
                } else if (activityLogKey.equals("orderSyncStatus")) {
                    channelActivitLogList.add("Order Sync Was Put " + newActivityParam.toUpperCase());
                } else if (activityLogKey.equals("reconciliationSyncStatus")) {
                    channelActivitLogList.add("Reconciliation Sync Was Put " + newActivityParam.toUpperCase());
                } else if (activityLogKey.equals("inventorySyncStatus")) {
                    channelActivitLogList.add("Inventory Sync Was Put " + newActivityParam.toUpperCase());
                } else if (activityLogKey.equals("enabled")) {
                    channelActivitLogList.add("Channel Was " + (newActivityParam.equals("true") ? "enabled" : "disabled"));
                } else if (activityLogKey.equals("pricingSyncStatus")) {
                    channelActivitLogList.add("Pricing Sync Was Put " + newActivityParam.toUpperCase());
                } else {
                    if (StringUtils.isNotBlank(oldActivityParam)) {
                        channelActivitLogList.add("Parameter " + activityLogKey + " changed from " + oldActivityParam + " to " + newActivityParam);
                    } else {
                        channelActivitLogList.add("Parameter " + activityLogKey + " set to " + newActivityParam);
                    }
                }
            }
        }
        return StringUtils.join(channelActivitLogList);
    }
}
