/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 08-Aug-2014
 *  @author parijat
 */
package com.uniware.services.audit.impl;

public enum ActivityEntityEnum {

    SALE_ORDER("Sale Order"),
    SALE_ORDER_ITEM("Order Item"),
    SHIPPING_PACKAGE("Shipping Package"),
    PICKLIST("Pick list"),
    INVOICE("Invoice"),
    MANIFEST("Manifest"),
    RETURN_MANIFEST("Return Manifest"),
    RESHIPMENT_ORDER("Reshipment Order"),
    REPLACEMENT_ORDER("Replacement Order"),
    REVERSE_PICKUP("Reverse Pickup"),
    ITEM_TYPE("Item Type"),
    PUTWAWAY("Putaway"),
    CHANNEL("Channel"),
    PURCHASE_ORDER("Purchase Order"),
    PURCHASE_ORDER_ITEM("Purchase Order Item"),
    INFLOW_RECEIPT("Inflow Receipt"),
    PRICE("Price"),
    ITEM("Item");

    private String name;

    ActivityEntityEnum(String name) {
        this.name = name;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

}
