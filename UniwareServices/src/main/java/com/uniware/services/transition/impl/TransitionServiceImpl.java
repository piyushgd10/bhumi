/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 02-May-2012
 *  @author praveeng
 */
package com.uniware.services.transition.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.entity.User;
import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.StringUtils;
import com.unifier.services.aspect.RollbackOnFailure;
import com.unifier.services.users.IUsersService;
import com.uniware.core.api.currency.GetCurrencyConversionRateRequest;
import com.uniware.core.api.inflow.WsInflowReceiptItem;
import com.uniware.core.api.purchase.WsPurchaseOrderItem;
import com.uniware.core.api.transition.CreateInventoryRequest;
import com.uniware.core.api.transition.CreateInventoryResponse;
import com.uniware.core.api.transition.CreateItemsRequest;
import com.uniware.core.api.transition.CreateItemsResponse;
import com.uniware.core.api.transition.DiscardAllItemsRequest;
import com.uniware.core.api.transition.DiscardAllItemsResponse;
import com.uniware.core.api.transition.DiscardItemCodeRequest;
import com.uniware.core.api.transition.DiscardItemCodeResponse;
import com.uniware.core.api.transition.ItemTypeDTO;
import com.uniware.core.api.transition.ItemTypeDTO.VendorItemTypeDTO;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.cache.FacilityCache;
import com.uniware.core.entity.InflowReceipt;
import com.uniware.core.entity.InflowReceiptItem;
import com.uniware.core.entity.InventoryLedger;
import com.uniware.core.entity.Item;
import com.uniware.core.entity.ItemType;
import com.uniware.core.entity.ItemTypeInventory;
import com.uniware.core.entity.ItemTypeInventory.Type;
import com.uniware.core.entity.PartyAddress;
import com.uniware.core.entity.PartyAddressType;
import com.uniware.core.entity.PurchaseOrder;
import com.uniware.core.entity.PurchaseOrderItem;
import com.uniware.core.entity.Putaway;
import com.uniware.core.entity.PutawayItem;
import com.uniware.core.entity.Shelf;
import com.uniware.core.entity.Vendor;
import com.uniware.core.entity.VendorItemType;
import com.uniware.core.locking.Namespace;
import com.uniware.core.locking.annotation.Lock;
import com.uniware.core.locking.annotation.Locks;
import com.uniware.dao.transition.ITransitionDao;
import com.uniware.services.catalog.ICatalogService;
import com.uniware.services.configuration.TenantSystemConfiguration;
import com.uniware.services.currency.ICurrencyService;
import com.uniware.services.inflow.impl.InflowServiceImpl;
import com.uniware.services.inventory.IInventoryService;
import com.uniware.services.ledger.IInventoryLedgerService;
import com.uniware.services.purchase.IPurchaseService;
import com.uniware.services.transition.ITransitionService;
import com.uniware.services.vendor.IVendorService;
import com.uniware.services.warehouse.IShelfService;

@Service
public class TransitionServiceImpl implements ITransitionService {

    private static final Logger     LOG = LoggerFactory.getLogger(TransitionServiceImpl.class);

    @Autowired
    private ICatalogService         catalogService;

    @Autowired
    private IVendorService          vendorService;

    @Autowired
    private IInventoryService       inventoryService;

    @Autowired
    private IShelfService           shelfService;

    @Autowired
    private ITransitionDao          transitionDao;

    @Autowired
    private IUsersService           usersService;

    @Autowired
    private IPurchaseService        purchaseService;

    @Autowired
    private ICurrencyService        currencyService;

    @Autowired
    private InflowServiceImpl       inflowService;

    @Autowired
    private IInventoryLedgerService inventoryLedgerService;

    @Override
    @Locks({ @Lock(ns = Namespace.TRANSITION, key = "TRANSITION") })
    @Transactional
    @RollbackOnFailure
    public CreateItemsResponse createItems(CreateItemsRequest request) {
        CreateItemsResponse response = new CreateItemsResponse();
        ValidationContext context = request.validate();
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        } else {
            User user = usersService.getUserWithDetailsById(request.getUserId());
            ItemType itemType = catalogService.getItemTypeBySkuCode(request.getItemTypeSkuCode());
            if (itemType != null
                    && (StringUtils.isNotBlank(catalogService.getTaxTypeCode(itemType, true)) || StringUtils.isNotBlank(catalogService.getTaxTypeCode(itemType, false)))
                    && (!catalogService.isItemTypeExpirable(itemType) || request.getManufacturingDate() != null)) {
                Vendor vendor = vendorService.getVendorByCode(request.getVendorCode());
                boolean isITemTypeExpirable = catalogService.isItemTypeExpirable(itemType);
                if (vendor != null) {
                    Shelf shelf = shelfService.getShelfByCode(request.getShelfCode());
                    if (shelf != null) {
                        PurchaseOrder purchaseOrder = transitionDao.getPurchaseOrderByCode("TRANSIT-" + request.getVendorCode());
                        if (purchaseOrder == null) {
                            purchaseOrder = new PurchaseOrder();
                            purchaseOrder.setType(PurchaseOrder.Type.MANUAL.name());
                            purchaseOrder.setCode("TRANSIT-" + request.getVendorCode());
                            purchaseOrder.setVendor(vendor);
                            purchaseOrder.setUser(user);
                            purchaseOrder.setLastUpdatedByUser(user);
                            purchaseOrder.setStatusCode(PurchaseOrder.StatusCode.COMPLETE.name());
                            purchaseOrder.setFromParty(CacheManager.getInstance().getCache(FacilityCache.class).getCurrentFacility());
                            purchaseOrder.setCurrencyCode(ConfigurationManager.getInstance().getConfiguration(TenantSystemConfiguration.class).getBaseCurrency());
                            purchaseOrder.setCurrencyConversionRate(
                                    currencyService.getCurrencyConversionRate(new GetCurrencyConversionRateRequest(purchaseOrder.getCurrencyCode())).getConversionRate());
                            purchaseOrder.setCreated(DateUtils.getCurrentTime());
                            purchaseOrder.setUpdated(DateUtils.getCurrentTime());
                            purchaseOrder = transitionDao.addPurchaseOrder(purchaseOrder);
                        }
                        PartyAddress vendorShippingAddress = purchaseOrder.getVendor().getPartyAddressByType(PartyAddressType.Code.SHIPPING.name());
                        if (vendorShippingAddress != null) {
                            VendorItemType vendorItemType = catalogService.getVendorItemType(vendor.getId(), itemType.getId());
                            PurchaseOrderItem purchaseOrderItem = transitionDao.getPurchaseOrderItem(purchaseOrder.getCode(), request.getItemTypeSkuCode());
                            if (purchaseOrderItem == null) {
                                purchaseOrderItem = new PurchaseOrderItem();
                                WsPurchaseOrderItem wsPurchaseOrderItem = new WsPurchaseOrderItem();
                                wsPurchaseOrderItem.setQuantity(request.getQuantity());
                                wsPurchaseOrderItem.setUnitPrice(vendorItemType.getUnitPrice());
                                purchaseService.preparePurchaseOrderItem(purchaseOrderItem, purchaseOrder, vendor, itemType, vendorItemType.getVendorSkuCode(), null,
                                        wsPurchaseOrderItem);
                                purchaseOrderItem = transitionDao.addPurchaseOrderItem(purchaseOrderItem);
                            } else {
                                purchaseOrderItem.setQuantity(purchaseOrderItem.getQuantity() + request.getQuantity());
                                purchaseOrderItem = transitionDao.updatePurchaseOrderItem(purchaseOrderItem);
                            }

                            InflowReceipt inflowReceipt = null;
                            if (purchaseOrder.getInflowReceipts().size() == 0) {
                                inflowReceipt = new InflowReceipt(user, InflowReceipt.StatusCode.COMPLETE.name(), purchaseOrder, DateUtils.getCurrentTime(),
                                        DateUtils.getCurrentTime());
                                inflowReceipt.setVendorInvoiceNumber("TRANSITION");
                                inflowReceipt.setVendorInvoiceDate(DateUtils.getCurrentTime());
                                inflowReceipt.setCurrencyCode(ConfigurationManager.getInstance().getConfiguration(TenantSystemConfiguration.class).getBaseCurrency());
                                inflowReceipt.setCurrencyConversionRate(currencyService.getCurrencyConversionRate(
                                        new GetCurrencyConversionRateRequest(purchaseOrder.getCurrencyCode())).getConversionRate());
                                inflowReceipt = transitionDao.addInflowReceipt(inflowReceipt);
                            } else {
                                inflowReceipt = purchaseOrder.getInflowReceipts().iterator().next();
                            }

                            InflowReceiptItem inflowReceiptItem = transitionDao.getInflowReceiptItem(inflowReceipt.getId(), purchaseOrderItem.getId());
                            WsInflowReceiptItem wsInflowReceiptItem = new WsInflowReceiptItem();
                            if (inflowReceiptItem == null) {
                                inflowReceiptItem = new InflowReceiptItem();
                                wsInflowReceiptItem.setQuantity(request.getQuantity());
                                wsInflowReceiptItem.setManufacturingDate(isITemTypeExpirable ? request.getManufacturingDate() : null);
                                inflowService.prepareInflowReceiptItem(inflowReceiptItem, purchaseOrderItem, wsInflowReceiptItem);
                                inflowReceiptItem.setItemsLabelled(true);
                                inflowReceiptItem.setCreated(DateUtils.getCurrentTime());
                                inflowReceiptItem.setInflowReceipt(inflowReceipt);
                                inflowReceiptItem.setStatusCode(InflowReceiptItem.StatusCode.COMPLETE.name());
                                inflowReceiptItem = transitionDao.addInflowReceiptItem(inflowReceiptItem);
                            } else {
                                wsInflowReceiptItem.setQuantity(inflowReceiptItem.getQuantity() + request.getQuantity());
                                inflowService.prepareInflowReceiptItem(inflowReceiptItem, purchaseOrderItem, wsInflowReceiptItem);
                                inflowReceiptItem = transitionDao.updateInflowReceiptItem(inflowReceiptItem);
                            }

                            Putaway putaway = transitionDao.getPutawayByCode("TRANSITION");
                            if (putaway == null) {
                                putaway = new Putaway();
                                putaway.setCode("TRANSITION");
                                putaway.setUser(user);
                                putaway.setCreated(DateUtils.getCurrentTime());
                                putaway.setUpdated(DateUtils.getCurrentTime());
                                putaway.setType(Putaway.Type.PUTAWAY_GRN_ITEM.name());
                                putaway.setStatusCode(Putaway.StatusCode.COMPLETE.name());
                                putaway = transitionDao.addPutaway(putaway);
                            }
                            PutawayItem putawayItem = transitionDao.getPutawayItem(putaway.getId(), itemType.getId());
                            if (putawayItem == null) {
                                putawayItem = new PutawayItem(putaway, Type.GOOD_INVENTORY, inflowReceiptItem.getAgeingStartDate(), shelf, itemType, request.getQuantity(),
                                        request.getQuantity(), PutawayItem.StatusCode.COMPLETE.name(), DateUtils.getCurrentTime(), DateUtils.getCurrentTime());
                                putawayItem = transitionDao.addPutawayItem(putawayItem);
                            } else {
                                putawayItem.setQuantity(putawayItem.getQuantity() + request.getQuantity());
                                putawayItem.setPutawayQuantity(putawayItem.getPutawayQuantity() + request.getQuantity());
                                putawayItem = transitionDao.updatePutawayItem(putawayItem);
                            }

                            List<Item> items = inventoryService.addItems(itemType, vendor, request.getQuantity(), inflowReceiptItem,
                                    Item.StatusCode.valueOf(request.getStatusCode()));
                            for (Item item : items) {
                                item.setShelf(shelf);
                                item.setPutaway(putaway);
                                item.setAgeingStartDate(inflowReceiptItem.getAgeingStartDate());
                                inventoryService.updateItem(item);
                            }

                            StringBuilder itemCodes = new StringBuilder();
                            for (Item item : items) {
                                itemCodes.append(item.getCode()).append(",");
                            }
                            itemCodes.deleteCharAt(itemCodes.length() - 1);
                            addInventoryLedger(request, inflowReceipt.getCode(), putaway.getCode());
                            response.setItemCodes(itemCodes.toString());
                            response.setSuccessful(true);
                        } else {
                            context.addError(WsResponseCode.VENDOR_DETAILS_INCOMPLETE, "Vendor shipping address is not configured");
                        }
                    } else {
                        context.addError(WsResponseCode.INVALID_SHELF_CODE, "Invalid Shelf Code");
                    }
                } else {
                    context.addError(WsResponseCode.INVALID_VENDOR_ID, "Invalid Vendor");
                }
            } else {
                // handling each error condition in AND clause with different error message.
                if (itemType == null) {
                    context.addError(WsResponseCode.INVALID_ITEM_TYPE, "Invalid Item Type");
                } else if (StringUtils.isBlank(catalogService.getTaxTypeCode(itemType, true)) && StringUtils.isBlank(catalogService.getTaxTypeCode(itemType, false))) {
                    context.addError(WsResponseCode.TAX_TYPE_NOT_CONFIGURED, "Tax type is not configured.");
                } else if (catalogService.isItemTypeExpirable(itemType) && request.getManufacturingDate() == null) {
                    context.addError(WsResponseCode.INVALID_REQUEST, "Specify manufacturing date for expirable skus.");
                }
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    private void addInventoryLedger(CreateItemsRequest request, String inflowReceiptCode, String putawayCode) {
        InventoryLedger inventoryLedger = new InventoryLedger();
        inventoryLedger.setSkuCode(request.getItemTypeSkuCode());
        inventoryLedger.setTransactionIdentifier(inflowReceiptCode);
        inventoryLedger.setAdditionalInfo(putawayCode);
        inventoryLedger.setTransactionType(InventoryLedger.TransactionType.GRN);
        inventoryLedgerService.addInventoryLedgerEntry(inventoryLedger, request.getQuantity(), InventoryLedger.ChangeType.INCREASE);
    }

    @Override
    @Locks({ @Lock(ns = Namespace.TRANSITION, key = "TRANSITION") })
    @Transactional
    public CreateInventoryResponse createInventory(CreateInventoryRequest request) {
        CreateInventoryResponse response = new CreateInventoryResponse();
        ValidationContext context = request.validate();
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        } else {
            ItemType itemType = catalogService.getItemTypeBySkuCode(request.getItemTypeSkuCode());
            if (itemType == null) {
                context.addError(WsResponseCode.INVALID_ITEM_TYPE, "Invalid Item Type");
            } else {
                List<Object[]> objects = transitionDao.getItemCountGroupByShelfAndAgeing(itemType.getSkuCode());
                for (Object[] obj : objects) {
                    Integer quantity = ((Long) obj[0]).intValue();
                    Shelf shelf = (Shelf) obj[1];
                    Date ageingStartDate = (Date) obj[2];
                    ItemTypeInventory itemTypeInventory = inventoryService.addInventory(itemType, Type.GOOD_INVENTORY, ageingStartDate, shelf, quantity);
                    if (itemTypeInventory != null) {
                        transitionDao.updateItemsStatus(shelf.getId(), itemType.getId());
                    }
                }
                response.setSuccessful(true);
            }
        }
        response.addErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    public ItemTypeDTO getItemTypeDetails(Integer itemTypeId) {
        ItemTypeDTO itemTypeDTO = new ItemTypeDTO();
        List<VendorItemType> vendorItemTypes = transitionDao.getVendorItemTypeByItemTypeId(itemTypeId);
        ItemType itemType = catalogService.getNonBundledItemTypeById(itemTypeId);
        for (VendorItemType vendorItemType : vendorItemTypes) {
            VendorItemTypeDTO vendorItemTypeDTO = new VendorItemTypeDTO();
            Vendor vendor = vendorItemType.getVendor();
            vendorItemTypeDTO.setVendorEnabled(vendor.isEnabled());
            vendorItemTypeDTO.setVendorCode(vendor.getCode());
            vendorItemTypeDTO.setVendorName(vendor.getName());
            vendorItemTypeDTO.setVendorSkuCode(vendorItemType.getVendorSkuCode());
            itemTypeDTO.getVendorItemTypes().add(vendorItemTypeDTO);
        }
        if (itemTypeDTO.getVendorItemTypes().size() > 1) {
            Iterator<VendorItemTypeDTO> itr = itemTypeDTO.getVendorItemTypes().iterator();
            while (itr.hasNext()) {
                VendorItemTypeDTO vendorItemTypeDTO = itr.next();
                if (!vendorItemTypeDTO.isVendorEnabled()) {
                    itr.remove();
                }
            }
        }
        itemTypeDTO.setExpirable(catalogService.isItemTypeExpirable(itemType));
        itemTypeDTO.setItemSKU(itemType.getSkuCode());
        itemTypeDTO.setItemTypeName(itemType.getName());
        itemTypeDTO.setItemTypeImageUrl(itemType.getImageUrl());
        itemTypeDTO.setItemTypePageUrl(itemType.getProductPageUrl());
        itemTypeDTO.setCategory(itemType.getCategory().getName());
        itemTypeDTO.setGoodInventoryCount(transitionDao.getGoodInventoryCount(itemTypeId));
        itemTypeDTO.setInTransitionCount(transitionDao.getItemsInTransition(itemTypeId).size());

        List<Object[]> shelfToItemTypeCount = transitionDao.getShelfToItemTypeCount(itemTypeId);
        for (Object[] obj : shelfToItemTypeCount) {
            itemTypeDTO.getShelfToItemTypeCount().put((String) obj[0], (Integer) obj[1]);
        }
        return itemTypeDTO;
    }

    @Override
    @Transactional
    public DiscardItemCodeResponse discardItemCode(DiscardItemCodeRequest request) {
        DiscardItemCodeResponse response = new DiscardItemCodeResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            Item item = transitionDao.getItemByCode(request.getItemCode());
            if (item == null) {
                context.addError(WsResponseCode.INVALID_ITEM_CODE, "Invalid Item Code");
            } else if (!item.getItemType().getSkuCode().equals(request.getItemSKU())) {
                context.addError(WsResponseCode.INVALID_ITEM_TYPE, "Item does not belong to current item type");
            } else if (!Item.StatusCode.IN_TRANSITION.name().equals(item.getStatusCode())) {
                context.addError(WsResponseCode.INVALID_ITEM_STATE, "Items in Transition can be discarded only");
            } else {
                InflowReceiptItem inflowReceiptItem = item.getInflowReceiptItem();
                inflowReceiptItem.setQuantity(inflowReceiptItem.getQuantity() - 1);

                PurchaseOrderItem purchaseOrderItem = inflowReceiptItem.getPurchaseOrderItem();
                purchaseOrderItem.setQuantity(purchaseOrderItem.getQuantity() - 1);
                purchaseOrderItem.setReceivedQuantity(purchaseOrderItem.getReceivedQuantity() - 1);
                purchaseOrderItem.setSubtotal(purchaseOrderItem.getUnitPrice().multiply(new BigDecimal(purchaseOrderItem.getQuantity())).setScale(2, RoundingMode.HALF_EVEN));
                purchaseOrderItem.setTotal(purchaseOrderItem.getSubtotal().add(purchaseOrderItem.getCst()).add(purchaseOrderItem.getVat()));

                PutawayItem putawayItem = transitionDao.getPutawayItem(item.getPutaway().getId(), item.getItemType().getId());
                putawayItem.setQuantity(putawayItem.getQuantity() - 1);
                putawayItem.setPutawayQuantity(putawayItem.getPutawayQuantity() - 1);

                transitionDao.removeItem(request.getItemCode());
                response.setSuccessful(true);
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    public DiscardAllItemsResponse discardAllItems(DiscardAllItemsRequest request) {
        DiscardAllItemsResponse response = new DiscardAllItemsResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            ItemType itemType = catalogService.getItemTypeBySkuCode(request.getItemSKU());
            if (itemType == null) {
                context.addError(WsResponseCode.INVALID_ITEM_TYPE, "Invalid Item Type");
            } else {
                List<Item> items = getItemsInTransition(itemType.getId());
                for (Item item : items) {
                    DiscardItemCodeRequest discardItemCodeRequest = new DiscardItemCodeRequest();
                    discardItemCodeRequest.setItemSKU(itemType.getSkuCode());
                    discardItemCodeRequest.setItemCode(item.getCode());
                    DiscardItemCodeResponse discardItemCodeResponse = discardItemCode(discardItemCodeRequest);
                    if (!discardItemCodeResponse.isSuccessful()) {
                        context.getErrors().addAll(discardItemCodeResponse.getErrors());
                    }
                }
                if (!context.hasErrors()) {
                    response.setSuccessful(true);
                }
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    public List<Item> getItemsInTransition(int itemTypeId) {
        return transitionDao.getItemsInTransition(itemTypeId);
    }

}
