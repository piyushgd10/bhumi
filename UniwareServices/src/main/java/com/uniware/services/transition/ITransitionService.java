/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 02-May-2012
 *  @author praveeng
 */
package com.uniware.services.transition;

import com.uniware.core.api.transition.CreateInventoryRequest;
import com.uniware.core.api.transition.CreateInventoryResponse;
import com.uniware.core.api.transition.CreateItemsRequest;
import com.uniware.core.api.transition.CreateItemsResponse;
import com.uniware.core.api.transition.DiscardAllItemsRequest;
import com.uniware.core.api.transition.DiscardAllItemsResponse;
import com.uniware.core.api.transition.DiscardItemCodeRequest;
import com.uniware.core.api.transition.DiscardItemCodeResponse;
import com.uniware.core.api.transition.ItemTypeDTO;
import com.uniware.core.entity.Item;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface ITransitionService {

    /**
     * @param request
     * @return
     */
    CreateItemsResponse createItems(CreateItemsRequest request);

    /**
     * @param itemTypeId
     * @return
     */
    ItemTypeDTO getItemTypeDetails(Integer itemTypeId);

    /**
     * @param request
     * @return
     */
    DiscardItemCodeResponse discardItemCode(DiscardItemCodeRequest request);

    /**
     * @param request
     * @return
     */
    CreateInventoryResponse createInventory(CreateInventoryRequest request);

    /**
     * @param request
     * @return
     */
    DiscardAllItemsResponse discardAllItems(DiscardAllItemsRequest request);

    @Transactional List<Item> getItemsInTransition(int itemTypeId);
}
