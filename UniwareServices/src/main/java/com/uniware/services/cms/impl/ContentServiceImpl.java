/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 16/04/14
 *  @author amit
 */

package com.uniware.services.cms.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unifier.core.api.validation.ValidationContext;
import com.uniware.core.api.cms.GetContentForKeyRequest;
import com.uniware.core.api.cms.GetContentForKeyResponse;
import com.uniware.core.api.cms.GetContentForNamespaceRequest;
import com.uniware.core.api.cms.GetContentForNamespaceResponse;
import com.uniware.core.api.cms.GetContentForNamespaceResponse.NamespaceContentDTO;
import com.uniware.core.api.cms.GetVersionForNamespaceRequest;
import com.uniware.core.api.cms.GetVersionForNamespaceResponse;
import com.uniware.core.api.cms.SaveContentRequest;
import com.uniware.core.api.cms.SaveContentResponse;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.vo.ContentNamespaceVO;
import com.uniware.core.vo.ContentVO;
import com.uniware.core.vo.ContentVO.LanguageCode;
import com.uniware.dao.cms.IContentMao;
import com.uniware.services.cms.IContentService;
import com.uniware.services.exception.InvalidContentNamespaceException;

@Service(value = "contentService")
public class ContentServiceImpl implements IContentService {

    private static final String NS_KEY_DELIMITER = ":";

    @Autowired
    private IContentMao         contentMao;

    @Override
    public GetContentForNamespaceResponse getContentForNamespace(GetContentForNamespaceRequest request) {
        GetContentForNamespaceResponse response = new GetContentForNamespaceResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            String nsIdentifier = request.getNsIdentifier();
            Map<String, String> nsContentMap = new HashMap<String, String>();
            List<ContentVO> voList = contentMao.getContent(nsIdentifier, getLanguageCode(request.getLanguageCode()));
            if (voList != null) {
                for (ContentVO vo : voList) {
                    nsContentMap.put(vo.getKey(), vo.getContent());
                }
            }
            Integer version = getVersionForNamespace(nsIdentifier, getLanguageCode(request.getLanguageCode()));
            response.setNamespaceContentDTO(new NamespaceContentDTO(nsIdentifier, version, nsContentMap));
        }
        response.addErrors(context.getErrors());
        response.setSuccessful(!context.hasErrors());
        return response;
    }

    @Override
    public GetContentForKeyResponse getContentForKey(GetContentForKeyRequest request) {
        GetContentForKeyResponse response = new GetContentForKeyResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            NamespaceKeyDTO dto = null;
            try {
                dto = getNamespaceAndKey(request.getRequestString());
            } catch (StringIndexOutOfBoundsException e) {
                context.addError(WsResponseCode.INVALID_API_REQUEST, "Requested key format is incorrect");
            }
            if (!context.hasErrors()) {
                ContentVO vo = contentMao.getContent(dto.getNsIdentifier(), getLanguageCode(request.getLanguageCode()), dto.getKey());
                if (vo != null) {
                    response.setContent(vo.getContent());
                } else {
                    context.addError(WsResponseCode.INVALID_API_REQUEST, "No Content found for given namespace, key and language combination");
                }
            }
        }
        response.addErrors(context.getErrors());
        response.setSuccessful(!context.hasErrors());
        return response;
    }

    /**
     * Get {@code NamespaceKeyDTO} from request string (ns:key).
     * 
     * @param requestString
     * @return
     * @throws StringIndexOutOfBoundsException
     */
    private NamespaceKeyDTO getNamespaceAndKey(String requestString) throws StringIndexOutOfBoundsException {
        NamespaceKeyDTO dto = new NamespaceKeyDTO();
        dto.setNsIdentifier(requestString.substring(0, requestString.lastIndexOf(NS_KEY_DELIMITER)));
        dto.setKey(requestString.substring(requestString.lastIndexOf(NS_KEY_DELIMITER) + 1, requestString.length()));
        return dto;
    }

    /**
     * Namespace Name and Key String.
     */
    private class NamespaceKeyDTO {

        private String nsIdentifier;
        private String key;

        public String getNsIdentifier() {
            return nsIdentifier;
        }

        public void setNsIdentifier(String nsIdentifier) {
            this.nsIdentifier = nsIdentifier;
        }

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }
    }

    @Override
    public GetVersionForNamespaceResponse getVersionForNamespace(GetVersionForNamespaceRequest request) {
        GetVersionForNamespaceResponse response = new GetVersionForNamespaceResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            for (String nsIdentifier : request.getNsIdentifierSet()) {
                ContentNamespaceVO nsVO = contentMao.getContentNamespace(nsIdentifier);
                if (nsVO != null) {
                    response.getNsToVersionMap().put(nsIdentifier, getVersionForNamespace(nsIdentifier, request.getLanguageCode()));
                }
            }
        }
        response.addErrors(context.getErrors());
        response.setSuccessful(!context.hasErrors());
        return response;
    }

    @Override
    public Integer getVersionForNamespace(String nsIdentifier, LanguageCode languageCode) {
        ContentNamespaceVO nsVO = contentMao.getContentNamespace(nsIdentifier);
        if (nsVO != null) {
            return nsVO.getVersionMap().get(getLanguageCode(languageCode));
        }
        return null;
    }

    /**
     * Set {@code ContentVO.LanguageCode} to {@code ContentVO.LanguageCode.ENG} if it is not set.
     * 
     * @param langCode
     * @return
     */
    private ContentVO.LanguageCode getLanguageCode(ContentVO.LanguageCode langCode) {
        return langCode == null ? ContentVO.LanguageCode.ENG : langCode;
    }

    @Override
    public SaveContentResponse saveContent(SaveContentRequest request) {
        SaveContentResponse response = new SaveContentResponse();
        response.setNsIdentifier(request.getNsIdentifier());
        response.setKey(request.getKey());
        response.setContent(request.getContent());
        response.setLanguageCode(request.getLanguageCode());
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            try {
                contentMao.saveContent(request.getNsIdentifier(), getLanguageCode(request.getLanguageCode()), request.getKey(), request.getContent());
            } catch (InvalidContentNamespaceException e) {
                context.addError(WsResponseCode.INVALID_API_REQUEST, "No namespace found with identifier: " + request.getNsIdentifier());
            }
        }
        response.addErrors(context.getErrors());
        response.setSuccessful(!context.hasErrors());
        return response;
    }
}
