/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 16/04/14
 *  @author amit
 */

package com.uniware.services.cms;

import com.uniware.core.api.cms.GetContentForKeyRequest;
import com.uniware.core.api.cms.GetContentForKeyResponse;
import com.uniware.core.api.cms.GetContentForNamespaceRequest;
import com.uniware.core.api.cms.GetContentForNamespaceResponse;
import com.uniware.core.api.cms.GetVersionForNamespaceRequest;
import com.uniware.core.api.cms.GetVersionForNamespaceResponse;
import com.uniware.core.api.cms.SaveContentRequest;
import com.uniware.core.api.cms.SaveContentResponse;
import com.uniware.core.vo.ContentVO.LanguageCode;

public interface IContentService {

    GetContentForNamespaceResponse getContentForNamespace(GetContentForNamespaceRequest request);

    GetContentForKeyResponse getContentForKey(GetContentForKeyRequest request);

    GetVersionForNamespaceResponse getVersionForNamespace(GetVersionForNamespaceRequest request);

    SaveContentResponse saveContent(SaveContentRequest request);

    Integer getVersionForNamespace(String nsIdentifier, LanguageCode languageCode);

}
