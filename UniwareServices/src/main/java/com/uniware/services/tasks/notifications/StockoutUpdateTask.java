/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Nov 2, 2012
 *  @author praveeng
 */
package com.uniware.services.tasks.notifications;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.quartz.JobDataMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

import com.unifier.core.cache.CacheManager;
import com.unifier.core.entity.JobResult;
import com.unifier.services.job.IJobWorker;
import com.uniware.core.api.channel.ChannelDetailDTO;
import com.uniware.services.cache.ChannelCache;
import com.uniware.services.channel.IChannelInventorySyncService;

public class StockoutUpdateTask implements IJobWorker {

    private static final Logger LOG = LoggerFactory.getLogger(StockoutUpdateTask.class);

    private String getChannelString(Set<ChannelDetailDTO> channels) {
        String names = "[";
        int index = 0;
        for (Iterator<ChannelDetailDTO> iterator = channels.iterator(); iterator.hasNext();) {
            ChannelDetailDTO channelDTO = iterator.next();
            names += channelDTO.getName();
            if (index < channels.size() - 1)
                names += ",";
            index++;
        }
        return names + "]";
    }

    @Override public JobResult execute(ApplicationContext applicationContext, JobDataMap jobDataMap, JobResult jobResult) {
        IChannelInventorySyncService channelInventorySyncService = applicationContext.getBean(IChannelInventorySyncService.class);
        Set<ChannelDetailDTO> channelsToProcess = new HashSet<>();
        for (ChannelDetailDTO channelDetailDTO : CacheManager.getInstance().getCache(ChannelCache.class).getChannels()) {
            if (channelDetailDTO.isEnabled() && !channelDetailDTO.getSource().getCode().contains("ebay")) {
                channelsToProcess.add(channelDetailDTO);
            }
        }
        if (channelsToProcess.size() == 0) {
            jobResult.setMessage("No channel found for stockout inventory sync");
            return jobResult;
        }
        LOG.info("Stockout update sync scheduled for " + getChannelString(channelsToProcess));
        for (ChannelDetailDTO channel : channelsToProcess) {
            channelInventorySyncService.processChannelInventory(channel.getCode(), true, false);
        }
        jobResult.setMessage("Stockout update scheduled for " + getChannelString(channelsToProcess));
        return jobResult;
    }
}
