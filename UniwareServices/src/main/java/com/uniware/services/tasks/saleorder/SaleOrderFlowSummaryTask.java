/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 13, 2012
 *  @author praveeng
 */
package com.uniware.services.tasks.saleorder;

import org.quartz.JobDataMap;
import org.springframework.context.ApplicationContext;

import com.unifier.core.entity.JobResult;
import com.unifier.core.utils.DateUtils;
import com.unifier.services.job.IJobWorker;
import com.uniware.core.vo.SaleOrderFlowSummaryVO;
import com.uniware.services.saleorder.ISaleOrderService;

public class SaleOrderFlowSummaryTask implements IJobWorker {

    @Override
    public JobResult execute(ApplicationContext applicationContext, JobDataMap jobDataMap, JobResult jobResult) {
        final ISaleOrderService saleOrderService = applicationContext.getBean(ISaleOrderService.class);
        Long orderNotDispatched = saleOrderService.getItemCountNotDispatched();
        Long ordersDispatchedToday = saleOrderService.getItemCountDispatchedToday();
        SaleOrderFlowSummaryVO summary = new SaleOrderFlowSummaryVO();
        summary.setTotalNoOfOrders(orderNotDispatched + ordersDispatchedToday);
        summary.setOrdersDispatched(ordersDispatchedToday);
        summary.setCreated(DateUtils.getCurrentTime());
        saleOrderService.createSaleOrderFlowSummary(summary);
        jobResult.setMessage("Task completed successfully");
        return jobResult;
    }
}
