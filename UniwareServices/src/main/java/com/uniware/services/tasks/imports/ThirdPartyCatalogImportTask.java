/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, May 28, 2012
 *  @author singla
 */
package com.uniware.services.tasks.imports;

import com.uniware.core.entity.ChannelItemType;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.jibx.runtime.JiBXException;
import org.quartz.JobDataMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

import com.unifier.core.api.validation.WsError;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.entity.JobResult;
import com.unifier.core.utils.JibxUtils;
import com.unifier.core.utils.StringUtils;
import com.unifier.core.utils.XMLParser;
import com.unifier.core.utils.XMLParser.Element;
import com.unifier.scraper.sl.exception.ScriptCompilationException;
import com.unifier.scraper.sl.exception.ScriptExecutionException;
import com.unifier.scraper.sl.exception.ScriptParseException;
import com.unifier.scraper.sl.exception.ScriptValidationException;
import com.unifier.scraper.sl.runtime.ScraperScript;
import com.unifier.scraper.sl.runtime.ScriptExecutionContext;
import com.unifier.services.job.IJobWorker;
import com.uniware.core.api.catalog.BulkCreateOrEditItemTypeRequest;
import com.uniware.core.api.catalog.BulkCreateOrEditItemTypeResponse;
import com.uniware.core.api.catalog.CreateOrEditFacilityItemTypeRequest;
import com.uniware.core.api.catalog.CreateOrEditFacilityItemTypeResponse;
import com.uniware.core.api.channel.CreateChannelItemTypeRequest;
import com.uniware.core.api.channel.CreateChannelItemTypeResponse;
import com.uniware.core.entity.ItemType;
import com.uniware.core.utils.UserContext;
import com.uniware.services.cache.ScriptVersionedCache;
import com.uniware.services.catalog.ICatalogService;
import com.uniware.services.channel.IChannelCatalogService;

/**
 * @author singla
 */
public class ThirdPartyCatalogImportTask implements IJobWorker {

    private static final Logger LOG                                 = LoggerFactory.getLogger(ThirdPartyCatalogImportTask.class);

    // @Parameter(required = true, displayName = "Item Type List Script Name", value = "itemTypeListScriptXml")
    private static final String PARAM_ITEM_TYPE_LIST_SCRIPT_NAME    = "itemTypeListScriptName";

    // @Parameter(required = true, displayName = "Item Type Details Script Name", value = "itemTypeDetailsScriptXml")
    private static final String PARAM_ITEM_TYPE_DETAILS_SCRIPT_NAME = "itemTypeDetailsScriptName";

    // @Parameter(required = true, displayName = "Update Existing", value = "updateExisting")
    private static final String PARAM_UPDATE_EXISTING               = "updateExisting";

    @Override public JobResult execute(ApplicationContext applicationContext, JobDataMap jobDataMap, JobResult jobResult) {
        ICatalogService catalogService = applicationContext.getBean(ICatalogService.class);

        List<Element> itemTypeSkuCodes = getItemTypeList(jobDataMap.getString(PARAM_ITEM_TYPE_LIST_SCRIPT_NAME));
        List<WsError> errors = new ArrayList<>();
        for (Element itemTypeElement : itemTypeSkuCodes) {
            String itemTypeSkuCode = itemTypeElement.text();
            ItemType itemType = catalogService.getItemTypeBySkuCode(itemTypeSkuCode);
            if (jobDataMap.getBooleanFromString(PARAM_UPDATE_EXISTING) || itemType == null) {
                try {
                    ScraperScript itemTypeDetailsScript = CacheManager.getInstance().getCache(ScriptVersionedCache.class).getScriptByName(
                            jobDataMap.getString(PARAM_ITEM_TYPE_DETAILS_SCRIPT_NAME), true);
                    ScriptExecutionContext scriptExecutionContext = ScriptExecutionContext.current();
                    scriptExecutionContext.addVariable("itemSkuCode", itemTypeSkuCode);
                    scriptExecutionContext.addVariable("itemTypeElement", itemTypeElement);
                    scriptExecutionContext.setTraceLoggingEnabled(UserContext.current().isTraceLoggingEnabled());
                    itemTypeDetailsScript.execute();
                    createItemTypes(errors, itemTypeSkuCode, scriptExecutionContext.getScriptOutput(), applicationContext);
                    String facilityItemTypeXml = scriptExecutionContext.getScriptOutput("facilityItemTypes");
                    if (StringUtils.isNotBlank(facilityItemTypeXml)) {
                        createFacilityItemTypes(errors, itemTypeSkuCode, facilityItemTypeXml, applicationContext);
                    }
                    String channelItemTypesRequestXml = scriptExecutionContext.getScriptOutput("channelItemTypes");
                    if (StringUtils.isNotBlank(channelItemTypesRequestXml)) {
                        createChannelItemTypes(errors, itemTypeSkuCode, channelItemTypesRequestXml, applicationContext);
                    }
                } catch (Exception e) {
                    LOG.error("Exception while importing itemType: " + itemTypeSkuCode, e);
                } finally {
                    ScriptExecutionContext.destroy();
                }
            }
        }
        jobResult.setMessage("Task completed successfully");
        if (errors.size() > 0) {
            jobResult.addResultItem("errors", errors);
        }
        return jobResult;
    }

    private void createFacilityItemTypes(List<WsError> errors, String itemTypeSkuCode, String scriptOutput, ApplicationContext applicationContext) throws JiBXException,
            IOException {
        ICatalogService catalogService = applicationContext.getBean(ICatalogService.class);
        CreateOrEditFacilityItemTypeRequest request = JibxUtils.unmarshall("unicommerceServicesBinding14", CreateOrEditFacilityItemTypeRequest.class, scriptOutput);
        CreateOrEditFacilityItemTypeResponse response = catalogService.createOrEditFacilityItemType(request);
        if (response.isSuccessful()) {
            LOG.info("Facility Item type import successful for itemType:{} ", itemTypeSkuCode);
        } else {
            errors.addAll(response.getErrors());
            LOG.info("Facility Item type import failed for itemType:{}, errors: {} ", new Object[] { itemTypeSkuCode, response.getErrors() });
        }
    }

    private void createItemTypes(List<WsError> errors, String itemTypeSkuCode, String itemTypeRequestXml, ApplicationContext applicationContext) throws JiBXException, IOException {
        ICatalogService catalogService = applicationContext.getBean(ICatalogService.class);
        BulkCreateOrEditItemTypeRequest request = JibxUtils.unmarshall("unicommerceServicesBinding14", BulkCreateOrEditItemTypeRequest.class, itemTypeRequestXml);
        BulkCreateOrEditItemTypeResponse response = catalogService.bulkCreateOrEditItemTypeRequest(request);
        if (response.isSuccessful()) {
            LOG.info("Import successful for itemType:{} ", itemTypeSkuCode);
        } else {
            errors.addAll(response.getErrors());
            LOG.info("Import failed for itemType:{}, errors: {} ", new Object[] { itemTypeSkuCode, response.getErrors() });
        }
    }

    private void createChannelItemTypes(List<WsError> errors, String itemTypeSkuCode, String channelItemTypeRequestXml, ApplicationContext applicationContext)
            throws JiBXException, IOException {
        IChannelCatalogService channelCatalogService = applicationContext.getBean(IChannelCatalogService.class);
        CreateChannelItemTypeRequest request = JibxUtils.unmarshall("unicommerceServicesBinding15", CreateChannelItemTypeRequest.class, channelItemTypeRequestXml);
        request.setSyncId(ChannelItemType.SyncedBy.MANUAL.toString());
        CreateChannelItemTypeResponse response = channelCatalogService.createChannelItemType(request);
        if (response.isSuccessful()) {
            LOG.info("Import successful for channelitemType:{} ", itemTypeSkuCode);
        } else {
            errors.addAll(response.getErrors());
            LOG.info("Import failed for channelitemType:{}, errors: {} ", new Object[] { itemTypeSkuCode, response.getErrors() });
        }
    }

    /**
     * @return
     * @throws ScriptCompilationException
     * @throws ScriptParseException
     * @throws ScriptValidationException
     * @throws ScriptExecutionException
     */
    private List<Element> getItemTypeList(String itemTypeListScriptName) {
        ScraperScript itemTypeListScript = CacheManager.getInstance().getCache(ScriptVersionedCache.class).getScriptByName(itemTypeListScriptName, true);
        ScriptExecutionContext context = ScriptExecutionContext.current();
        try {
            context.setTraceLoggingEnabled(UserContext.current().isTraceLoggingEnabled());
            itemTypeListScript.execute();
            String itemTypeListXml = context.getScriptOutput();
            if (StringUtils.isNotBlank(itemTypeListXml)) {
                Element rootElement = XMLParser.parse(itemTypeListXml);
                return rootElement.list("ItemType");
            }
        } finally {
            ScriptExecutionContext.destroy();
        }
        return Collections.emptyList();
    }

}
