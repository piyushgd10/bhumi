package com.uniware.services.tasks.pricing;

import org.quartz.JobDataMap;
import org.springframework.context.ApplicationContext;

import com.unifier.core.entity.JobResult;
import com.unifier.services.job.IJobWorker;
import com.uniware.core.api.prices.PushAllPricesOnAllChannelsRequest;
import com.uniware.services.pricing.IPricingBridge;

/**
 * Created by shobhit on 03/11/15.
 */
public class PushPriceOnChannelRecurrentTask implements IJobWorker {

    @Override
    public JobResult execute(ApplicationContext applicationContext, JobDataMap jobDataMap, JobResult jobResult) {
        IPricingBridge pricingBridge = applicationContext.getBean(IPricingBridge.class);
        pricingBridge.pushAllPricesOnAllChannels(new PushAllPricesOnAllChannelsRequest());
        return jobResult;
    }
}
