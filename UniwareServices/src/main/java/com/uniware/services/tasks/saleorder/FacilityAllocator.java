/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 21, 2011
 *  @author singla
 */
package com.uniware.services.tasks.saleorder;

import java.util.List;

import org.quartz.JobDataMap;
import org.springframework.context.ApplicationContext;

import com.unifier.core.entity.JobResult;
import com.unifier.services.job.IJobWorker;
import com.uniware.services.saleorder.ISaleOrderProcessingService;
import com.uniware.services.saleorder.ISaleOrderService;

/**
 * @author singla
 */
public class FacilityAllocator implements IJobWorker {

    // @Parameter(required = false, value = "scriptName", displayName = "Script Name")
    private static final String PARAM_SCRIPT_NAME = "scriptName";

    @Override
    public JobResult execute(ApplicationContext applicationContext, JobDataMap jobDataMap, JobResult jobResult) {
        ISaleOrderProcessingService saleOrderProcessingService = applicationContext.getBean(ISaleOrderProcessingService.class);
        ISaleOrderService saleOrderService = applicationContext.getBean(ISaleOrderService.class);
        List<String> saleOrderCodes = saleOrderService.getSaleOrderCodesForFacilityAllocation();
        for (String saleOrderCode : saleOrderCodes) {
            saleOrderProcessingService.allocateFacility(saleOrderCode, jobDataMap.getString(PARAM_SCRIPT_NAME));
        }
        jobResult.setMessage("Task completed successfully");
        return jobResult;
    }
}
