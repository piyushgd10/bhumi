/*
 * Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 9/24/15 12:27 PM
 * @author harshpal
 */

package com.uniware.services.tasks.saleorder;

import org.quartz.JobDataMap;
import org.springframework.context.ApplicationContext;

import com.unifier.core.entity.JobResult;
import com.unifier.services.job.IJobWorker;
import com.uniware.services.saleorder.ISaleOrderService;

/**
 * Created by harshpal on 9/24/15.
 */
public class UnfulfilledPickupOrdersNotificationTask implements IJobWorker {

    @Override
    public JobResult execute(ApplicationContext applicationContext, JobDataMap jobDataMap, JobResult jobResult) {
        ISaleOrderService saleOrderService = applicationContext.getBean(ISaleOrderService.class);
        boolean success = saleOrderService.sendPendingPickupOrdersNotifications();
        if (success) {
            jobResult.setMessage("Pending Pickup Orders Notifications Task completed successfully");
        } else {
            jobResult.setMessage("Pending Pickup Orders Notifications Task failed");
        }
        return jobResult;
    }
}
