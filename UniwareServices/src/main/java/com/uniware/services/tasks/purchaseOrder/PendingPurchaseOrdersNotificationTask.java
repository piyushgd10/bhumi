/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Oct 10, 2012
 *  @author praveeng
 */
package com.uniware.services.tasks.purchaseOrder;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.quartz.JobDataMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

import com.unifier.core.email.EmailMessage;
import com.unifier.core.entity.JobResult;
import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.DateUtils.DateRange;
import com.unifier.core.utils.DateUtils.Interval;
import com.unifier.core.utils.StringUtils;
import com.unifier.services.email.IEmailService;
import com.unifier.services.job.IJobWorker;
import com.uniware.core.entity.InflowReceipt;
import com.uniware.core.entity.OutboundGatePass;
import com.uniware.core.entity.PartyContact;
import com.uniware.core.entity.PartyContactType;
import com.uniware.core.entity.PurchaseOrder;
import com.uniware.core.entity.Vendor;
import com.uniware.core.utils.Constants.EmailTemplateType;
import com.uniware.services.inflow.IInflowService;
import com.uniware.services.material.IMaterialService;
import com.uniware.services.purchase.IPurchaseService;
import com.uniware.services.vendor.IVendorService;

public class PendingPurchaseOrdersNotificationTask implements IJobWorker {

    private static final Logger LOG             = LoggerFactory.getLogger(PendingPurchaseOrdersNotificationTask.class);

    public static final String  PARAM_PAST_DAYS = "pastDays";

    @Override public JobResult execute(ApplicationContext applicationContext, JobDataMap jobDataMap, JobResult jobResult) {
        IVendorService vendorService = applicationContext.getBean(IVendorService.class);
        IMaterialService materialService = applicationContext.getBean(IMaterialService.class);
        IInflowService inflowService = applicationContext.getBean(IInflowService.class);
        IEmailService emailService = applicationContext.getBean(IEmailService.class);
        IPurchaseService purchaseService = applicationContext.getBean(IPurchaseService.class);

        DateRange dateRange = DateUtils.getPastInterval(DateUtils.getCurrentTime(),
                new Interval(TimeUnit.DAYS, jobDataMap.getIntegerFromString(PARAM_PAST_DAYS)));
        List<Vendor> vendors = vendorService.getVendors();
        for (Vendor vendor : vendors) {
            List<PurchaseOrder> purchaseOrders = purchaseService.getPurchaseOrderByStatus(PurchaseOrder.StatusCode.APPROVED.name(), vendor.getCode());
            List<InflowReceipt> grnSummary = inflowService.getPreviousDayGrnSummary(dateRange, vendor.getCode());
            List<OutboundGatePass> outboundGatePasses = materialService.getOutboundGatepassByRange(dateRange, vendor.getCode());
            Vendor vendorDetails = vendorService.getVendorByCode(vendor.getCode());
            PartyContact primaryContact = vendorDetails.getPartyContactByType(PartyContactType.Code.PRIMARY.name());
            if (!purchaseOrders.isEmpty() || !grnSummary.isEmpty() || !outboundGatePasses.isEmpty()) {
                if (primaryContact != null && StringUtils.isNotBlank(primaryContact.getEmail())) {
                    try {
                        List<String> recipients = new ArrayList<>();
                        recipients.add(primaryContact.getEmail());
                        EmailMessage message = new EmailMessage(recipients, EmailTemplateType.PENDING_PURCHASE_ORDERS.name());
                        message.addTemplateParam("purchaseOrders", purchaseOrders);
                        message.addTemplateParam("inflowReceipts", grnSummary);
                        message.addTemplateParam("outboundGatePasses", outboundGatePasses);
                        message.addTemplateParam("vendor", vendorDetails);
                        emailService.send(message);
                    } catch (Exception e) {
                        LOG.error("Error occurred while sending pending purchase orders mail to vendor: " + vendorDetails.getCode(), e);
                    }
                }
            }
        }
        jobResult.setMessage("Pending Purchase Order Task completed successfully");
        return jobResult;
    }
}
