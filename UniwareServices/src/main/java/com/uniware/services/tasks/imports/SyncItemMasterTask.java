/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jul 11, 2012
 *  @author singla
 */
package com.uniware.services.tasks.imports;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.quartz.JobDataMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

import com.unifier.core.api.customfields.WsCustomFieldValue;
import com.unifier.core.entity.JobResult;
import com.unifier.core.transport.http.HttpSender;
import com.unifier.core.utils.BatchProcessor;
import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.DateUtils.DateRange;
import com.unifier.core.utils.DateUtils.Interval;
import com.unifier.core.utils.JibxUtils;
import com.unifier.services.job.IJobWorker;
import com.unifier.services.utils.CustomFieldUtils;
import com.uniware.core.api.catalog.CreateOrEditItemTypeRequest;
import com.uniware.core.api.catalog.WsItemType;
import com.uniware.core.entity.ItemType;
import com.uniware.services.catalog.ICatalogService;

public class SyncItemMasterTask implements IJobWorker {

    private static final Logger LOG               = LoggerFactory.getLogger(SyncItemMasterTask.class);

    //    @Parameter(required = true, displayName = "Destination Service URL", value = "serviceUrl")
    private static final String PARAM_SERVICE_URL = "serviceUrl";

    // @Parameter(required = true, displayName = "Destination Service Username", value = "username")
    private static final String PARAM_USERNAME    = "username";

    // @Parameter(required = true, displayName = "Destination Service Password", value = "password")
    private static final String PARAM_PASSWORD    = "password";

    //@Parameter(required = true, displayName = "Batch Size", value = "batchSize")
    private static final String PARAM_BATCH_SIZE  = "batchSize";

    // @Parameter(required = true, displayName = "Update in Past (hours)", value = "pastHours")
    private static final String PARAM_PAST_HOURS  = "pastHours";

    @Override 
    public JobResult execute(ApplicationContext applicationContext, final JobDataMap jobDataMap, JobResult jobResult) {
        final ICatalogService catalogService = applicationContext.getBean(ICatalogService.class);
        final DateRange updatedIn = DateUtils.getPastInterval(DateUtils.getCurrentTime(), new Interval(TimeUnit.HOURS, Integer.valueOf(jobDataMap.getString(PARAM_PAST_HOURS))));
        final HttpSender sender = new HttpSender();
        BatchProcessor<ItemType> processor = new BatchProcessor<ItemType>(Integer.valueOf(jobDataMap.getString(PARAM_BATCH_SIZE))) {

            @Override
            protected List<ItemType> next(int start, int batchSize) {
                return catalogService.getItemTypes(start, batchSize, updatedIn);
            }

            @Override
            protected void process(List<ItemType> batchItems, int batchIndex) {
                for (ItemType itemType : batchItems) {
                    try {
                        CreateOrEditItemTypeRequest request = new CreateOrEditItemTypeRequest();
                        request.setItemType(new WsItemType(itemType));
                        Map<String, Object> customFieldValues = CustomFieldUtils.getCustomFieldValues(itemType);
                        if (customFieldValues != null && customFieldValues.size() > 0) {
                            List<WsCustomFieldValue> wsCustomFieldValues = new ArrayList<>(customFieldValues.size());
                            for (Map.Entry<String, Object> entry : customFieldValues.entrySet()) {
                                if (entry.getValue() != null) {
                                    wsCustomFieldValues.add(new WsCustomFieldValue(entry.getKey(), String.valueOf(entry.getValue())));
                                }
                            }
                            request.getItemType().setCustomFieldValues(wsCustomFieldValues);
                        }
                        String requestXml = JibxUtils.marshall("unicommerceServicesBinding13", request);
                        requestXml = requestXml.replaceFirst("<\\?xml version=\"1\\.0\" encoding=\"UTF-8\"\\?>", "");
                        String responseXml = sender.executePost(jobDataMap.getString(PARAM_SERVICE_URL), wrapInSoapHeader(requestXml, jobDataMap), null);
                        if (!responseXml.contains("<Successful>true</Successful>")) {
                            LOG.error("Unable to sync item: {}, response:{}", itemType.getSkuCode(), responseXml);
                        }
                    } catch (Exception e) {
                        LOG.error("Unable to sync item: {}" + itemType.getSkuCode(), e);
                    }
                }
            }

            private String wrapInSoapHeader(String requestXml, JobDataMap jobDataMap) {
                return "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">" + "<soapenv:Header>"
                        + "<wsse:Security soapenv:mustUnderstand=\"1\" xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\">"
                        + "<wsse:UsernameToken wsu:Id=\"UsernameToken-1\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\">"
                        + "<wsse:Username>" + jobDataMap.getString(PARAM_USERNAME) + "</wsse:Username>"
                        + "<wsse:Password Type=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText\">"
                        + jobDataMap.getString(PARAM_PASSWORD) + "</wsse:Password></wsse:UsernameToken></wsse:Security></soapenv:Header><soapenv:Body>" + requestXml
                        + "</soapenv:Body></soapenv:Envelope>";
            }

        };
        processor.process();
        jobResult.setMessage(processor.getTotalItems() + " Items synchronized to destination");
        return jobResult;
    }
}
