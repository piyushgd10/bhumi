/*
 *  Copyright 2013 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 05-Aug-2013
 *  @author karunsingla
 */
package com.uniware.services.tasks.common;

import java.util.HashMap;
import java.util.Map;

import org.quartz.JobDataMap;
import org.springframework.context.ApplicationContext;

import com.unifier.core.entity.JobResult;
import com.unifier.services.job.IJobWorker;
import com.uniware.services.common.ITaskScriptExecutorService;

public class ScriptExecutorTask implements IJobWorker {

    private static final String PARAM_SCRIPT_NAME = "scriptName";

    @Override public JobResult execute(ApplicationContext applicationContext, JobDataMap jobDataMap, JobResult jobResult) {
        ITaskScriptExecutorService taskScriptExecutorService = applicationContext.getBean(ITaskScriptExecutorService.class);
        Map<String, Object> scriptParameters = new HashMap<>();
        scriptParameters.put("taskResult", jobResult);
        taskScriptExecutorService.executeScript(jobDataMap.get(PARAM_SCRIPT_NAME).toString(), scriptParameters);
        return jobResult;
    }
}
