/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Apr 29, 2012
 *  @author singla
 */
package com.uniware.services.tasks.notifications;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.quartz.JobDataMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

import com.unifier.core.api.base.ServiceResponse;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.entity.JobResult;
import com.unifier.core.utils.BatchProcessor;
import com.unifier.core.utils.StringUtils;
import com.unifier.services.job.IJobWorker;
import com.unifier.services.job.JobConstants;
import com.uniware.core.api.channel.ChannelDetailDTO;
import com.uniware.core.api.notification.ProcessPendingNotificationsRequest;
import com.uniware.core.api.notification.ProcessPendingNotificationsResponse;
import com.uniware.core.cache.FacilityCache;
import com.uniware.core.entity.Notification;
import com.uniware.core.entity.Source;
import com.uniware.services.cache.ChannelCache;
import com.uniware.services.configuration.SourceConfiguration;
import com.uniware.services.notification.INotificationService;
import com.uniware.services.tasks.JobUtils;

/**
 * @author singla
 */
public class NotificationsProcessor implements IJobWorker {

    private static final Logger LOGGER = LoggerFactory.getLogger(NotificationsProcessor.class);

    private static final int DEFAULT_BATCH_SIZE = 1000;

    @Override
    public JobResult execute(ApplicationContext applicationContext, final JobDataMap jobDataMap, JobResult jobResult) {
        final INotificationService notificationService = applicationContext.getBean(INotificationService.class);
        final PendingNotificationStatus pendingNotificationStatus = notificationService.getPendingNotificationsStatus();
        LOGGER.info("Starting processing notifications. Pending notification count:{}, Max pending id:{} ", pendingNotificationStatus.getCount(), pendingNotificationStatus.getMaxId());
        if (CacheManager.getInstance().getCache(FacilityCache.class).getFacilities().size() > 0) {
            final Set<String> channelsToProcess = new HashSet<String>();
            for (ChannelDetailDTO channel : CacheManager.getInstance().getCache(ChannelCache.class).getChannels()) {
                Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(channel.getSource().getCode());
                if (StringUtils.isBlank(source.getNotificationUpdateCronExpression())
                        || JobUtils.isItAGoodTimeToRun(source.getNotificationUpdateCronExpression(), jobDataMap.getString(JobConstants.TRIGGER_CRON_EXPRESSION))) {
                    channelsToProcess.add(channel.getCode());
                }
            }
            final ServiceResponse response = new ServiceResponse();
            response.setSuccessful(true);
            BatchProcessor<Notification> processor = new BatchProcessor<Notification>(DEFAULT_BATCH_SIZE) {
                @Override
                protected void process(List<Notification> notifications, int batchIndex) {
                    LOGGER.info("Processing batch:{}, notificationsToBeProcessed: {}, totalPendingNotification:{}" ,batchIndex, notifications.size(),pendingNotificationStatus.getCount());
                    ProcessPendingNotificationsResponse bResponse = notificationService.processPendingNotifications(new ProcessPendingNotificationsRequest(notifications,
                            channelsToProcess));
                    response.setSuccessful(response.isSuccessful() && bResponse.isSuccessful());
                    response.addErrors(bResponse.getErrors());
                    pendingNotificationStatus.count = pendingNotificationStatus.getCount()-notifications.size();
                    LOGGER.info("Processed batch:{}, notificationsProcessedInBatch: {}, totalPendingNotification:{}" ,batchIndex, notifications.size(),pendingNotificationStatus.getCount());
                }

                @Override
                protected List<Notification> next(int start, int batchSize) {
                    return notificationService.getPendingNotifications(batchSize, pendingNotificationStatus.getMaxId());
                }
            };
            processor.process();

            if (response.isSuccessful()) {
                jobResult.setMessage("Task completed successfully");
            }
        }
        LOGGER.info("Finished processing notifications.");
        return jobResult;
    }

    public static class PendingNotificationStatus {
        private int maxId;
        private int count;

        public PendingNotificationStatus(int maxId, int count) {
            this.maxId = maxId;
            this.count = count;
        }

        public int getMaxId() {
            return maxId;
        }

        public int getCount() {
            return count;
        }

    }
}
