/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, May 28, 2012
 *  @author singla
 */
package com.uniware.services.tasks.imports;

import com.unifier.core.cache.CacheManager;
import com.unifier.core.entity.JobResult;
import com.unifier.services.job.IJobWorker;
import com.uniware.core.api.channel.ChannelDetailDTO;
import com.uniware.core.api.channel.SyncOrderStatusRequest;
import com.uniware.core.api.channel.SyncOrderStatusResponse;
import com.uniware.core.entity.Source;
import com.uniware.core.utils.UserContext;
import com.uniware.services.cache.ChannelCache;
import com.uniware.services.channel.saleorder.status.IChannelOrderStatusSyncService;
import org.quartz.JobDataMap;
import org.springframework.context.ApplicationContext;

/**
 * @author Sunny
 */
public class ThirdPartySaleOrderStatusSyncTask implements IJobWorker {


    // @Parameter(required = true, displayName = "Number of days", value = "numberOfDays")
    private static final String PARAM_NUMBER_OF_DAYS           = "numberOfDays";

    // @Parameter(required = false, displayName = "Only Sync Pending Orders", value = "syncPendingOrdersOnly")
    private static final String PARAM_SYNC_PENDING_ORDERS_ONLY = "syncPendingOrdersOnly";

    @Override
    public JobResult execute(ApplicationContext applicationContext, JobDataMap jobDataMap, JobResult jobResult) {
        if (UserContext.current().getTenant().isLive()) {
            IChannelOrderStatusSyncService channelOrderStatusSyncService = applicationContext.getBean(IChannelOrderStatusSyncService.class);
            ChannelCache channelCache = CacheManager.getInstance().getCache(ChannelCache.class);
            for (ChannelDetailDTO channel : channelCache.getChannels()) {
                if (channel.isEnabled() && channel.isOn()
                        && channelCache.getScriptName(channel.getCode(), Source.SALE_ORDER_STATUS_SYNC_SCRIPT_NAME) != null) {
                    SyncOrderStatusRequest syncOrderStatusRequest = new SyncOrderStatusRequest(channel.getCode(),
                            jobDataMap.getIntegerFromString(PARAM_NUMBER_OF_DAYS),
                            jobDataMap.getBooleanFromString(PARAM_SYNC_PENDING_ORDERS_ONLY) != null ? jobDataMap.getBooleanFromString(PARAM_SYNC_PENDING_ORDERS_ONLY) : false);
                    SyncOrderStatusResponse response = channelOrderStatusSyncService.syncSaleOrderStatus(syncOrderStatusRequest);
                    jobResult.addResultItem(channel.getCode(), response.getMessage());
                }
            }
        }
        jobResult.setMessage("Task completed successfully");
        return jobResult;
    }
}
