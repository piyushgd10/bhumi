/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, May 28, 2012
 *  @author singla
 */
package com.uniware.services.tasks.imports;

import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.entity.JobResult;
import com.unifier.core.utils.StringUtils;
import com.unifier.services.job.IJobWorker;
import com.uniware.core.api.channel.ChannelDetailDTO;
import com.uniware.core.api.channel.SyncChannelCatalogRequest;
import com.uniware.core.api.channel.SyncChannelCatalogResponse;
import com.uniware.core.api.channelWarehouse.SyncChannelWarehouseInventoryRequest;
import com.uniware.core.api.channelWarehouse.SyncChannelWarehouseInventoryResponse;
import com.uniware.core.entity.Channel;
import com.uniware.core.entity.Channel.SyncStatus;
import com.uniware.core.entity.Source;
import com.uniware.services.cache.ChannelCache;
import com.uniware.services.channel.IChannelCatalogSyncService;
import com.uniware.services.channelWarehouse.IChannelWarehouseInventorySyncService;
import com.uniware.services.configuration.SourceConfiguration;
import java.util.ArrayList;
import java.util.List;
import org.quartz.JobDataMap;
import org.springframework.context.ApplicationContext;

/**
 * @author Sunny
 */
public class ChannelCatalogSyncTask implements IJobWorker {

    @Override public JobResult execute(ApplicationContext applicationContext, JobDataMap jobDataMap, JobResult jobResult) {
        IChannelCatalogSyncService channelCatalogSyncService = applicationContext.getBean(IChannelCatalogSyncService.class);
        IChannelWarehouseInventorySyncService channelWarehouseInventorySyncService = applicationContext.getBean(IChannelWarehouseInventorySyncService.class);
        ChannelCache channelCache = CacheManager.getInstance().getCache(ChannelCache.class);
        List<String> channels = new ArrayList<String>();
        for (ChannelDetailDTO channelDetailDTO : channelCache.getChannels()) {
            Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(channelDetailDTO.getSource().getCode());
            if (!source.isPeriodicSyncDisabled() && channelDetailDTO.isEnabled() && SyncStatus.ON.name().equals(channelDetailDTO.getInventorySyncStatus())
                    && StringUtils.isNotBlank(channelCache.getScriptName(channelDetailDTO.getCode(), Source.CHANNEL_ITEM_TYPE_LIST_SCRIPT_NAME)) && StringUtils.isNotBlank(
                    channelCache.getScriptName(channelDetailDTO.getCode(), Source.CHANNEL_ITEM_TYPE_DETAILS_SCRIPT_NAME))) {
                SyncChannelCatalogResponse response = channelCatalogSyncService.syncChannelCatalog(new SyncChannelCatalogRequest(channelDetailDTO.getCode()));
                jobResult.addResultItem(channelDetailDTO.getCode(), response.getMessage());
            }
            if (!source.isPeriodicSyncDisabled() && channelDetailDTO.isEnabled() && (Channel.SyncStatus.ON.name().equals(channelDetailDTO.getInventorySyncStatus()))
                    && StringUtils.isNotBlank(channelCache.getScriptName(channelDetailDTO.getCode(), Source.CHANNEL_WAREHOUSE_INVENTORY_SCRIPT_NAME))
                    && StringUtils.isNotBlank(channelCache.getScriptName(channelDetailDTO.getCode(), Source.CHANNEL_WAREHOUSE_INVENTORY_SYNC_PREPROCESSOR_SCRIPT_NAME))) {
                SyncChannelWarehouseInventoryResponse response = channelWarehouseInventorySyncService.syncChannelWarehouseInventory(new SyncChannelWarehouseInventoryRequest(channelDetailDTO.getCode()));
                for (SyncChannelWarehouseInventoryResponse.ChannelWarehouseInventorySyncResponseDTO resultItem : response.getResultItems()) {
                    jobResult.addResultItem(resultItem.getChannelCode(), resultItem.getMessage());
                }
            }
        }
        jobResult.setMessage("Task completed successfully");
        return jobResult;
    }
}
