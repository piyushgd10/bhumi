/*
 * Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 3/22/15 10:08 PM
 * @author amdalal
 */

package com.uniware.services.tasks.saleorder;

import java.util.List;

import org.quartz.JobDataMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

import com.unifier.core.entity.JobResult;
import com.unifier.core.utils.BatchProcessor;
import com.unifier.services.job.IJobWorker;
import com.uniware.core.entity.SaleOrder;
import com.uniware.core.utils.UserContext;
import com.uniware.services.saleorder.ISaleOrderService;

public class ProcessDirtySaleOrderTask implements IJobWorker {

    private static final Logger LOG = LoggerFactory.getLogger(ProcessDirtySaleOrderTask.class);

    @Override
    public JobResult execute(ApplicationContext applicationContext, JobDataMap jobDataMap, JobResult jobResult) {
        final ISaleOrderService saleOrderService = applicationContext.getBean(ISaleOrderService.class);

        BatchProcessor<SaleOrder> batchProcessor = new BatchProcessor<SaleOrder>(1000) {
            @Override
            protected List<SaleOrder> next(int start, int batchSize) {
                LOG.info("Fetching {} dirty sale orders starting from {} for facility {}", new Object[] { batchSize, start, UserContext.current().getFacilityId() });
                // because previous batch was marked non-dirty by process(), we start from 0 again.
                return saleOrderService.getDirtySaleOrders(0, batchSize);
            }

            @Override
            protected void process(List<SaleOrder> batchItems, int batchIndex) {
                for (SaleOrder dirtySaleOrder : batchItems) {
                    try {
                        saleOrderService.processDirtySaleOrder(dirtySaleOrder);
                    } catch (Exception e) {
                        LOG.error("Failed to process dirty sale order " + dirtySaleOrder.getCode(), e);
                    }
                }
            }
        };
        batchProcessor.process();
        jobResult.setMessage("Task completed successfully");
        return jobResult;
    }

}
