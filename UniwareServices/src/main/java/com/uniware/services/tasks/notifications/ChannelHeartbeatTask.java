/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Nov 2, 2012
 *  @author praveeng
 */
package com.uniware.services.tasks.notifications;

import org.quartz.JobDataMap;
import org.springframework.context.ApplicationContext;

import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.entity.JobResult;
import com.unifier.services.job.IJobWorker;
import com.uniware.core.api.channel.ChannelDetailDTO;
import com.uniware.core.entity.Channel;
import com.uniware.core.entity.Channel.SyncStatus;
import com.uniware.core.entity.Source;
import com.uniware.services.cache.ChannelCache;
import com.uniware.services.channel.IChannelHeartbeatSyncService;
import com.uniware.services.configuration.SourceConfiguration;

public class ChannelHeartbeatTask implements IJobWorker {

    @Override
    public JobResult execute(ApplicationContext applicationContext, JobDataMap jobDataMap, JobResult jobResult) {
        IChannelHeartbeatSyncService channelHeartbeatSyncService = applicationContext.getBean(IChannelHeartbeatSyncService.class);
        boolean success = true;
        for (ChannelDetailDTO channelDTO : CacheManager.getInstance().getCache(ChannelCache.class).getChannels()) {
            Channel channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelByCode(channelDTO.getCode());
            Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(channel.getSourceCode());
            if (channel.isEnabled() && SyncStatus.ON.equals(channel.getOrderSyncStatus()) && source.isHeartbeatSyncEnabled()) {
                success = success && channelHeartbeatSyncService.syncChannelHeartbeat(channel.getCode());
            }
        }
        jobResult.setMessage("Task completed successfully");
        return jobResult;
    }
}
