/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Nov 2, 2012
 *  @author praveeng
 */
package com.uniware.services.tasks.notifications;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import com.unifier.core.utils.StringUtils;
import org.quartz.JobDataMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.entity.JobResult;
import com.unifier.services.job.IJobWorker;
import com.unifier.services.job.JobConstants;
import com.uniware.core.api.channel.ChannelDetailDTO;
import com.uniware.core.entity.Channel.SyncStatus;
import com.uniware.core.entity.Source;
import com.uniware.services.cache.ChannelCache;
import com.uniware.services.channel.IChannelInventorySyncService;
import com.uniware.services.configuration.SourceConfiguration;
import com.uniware.services.tasks.JobUtils;

public class UpdateInventoryTask implements IJobWorker {

    private static final Logger LOG = LoggerFactory.getLogger(UpdateInventoryTask.class);

    private String getChannelString(Set<ChannelDetailDTO> channels) {
        String names = "[";
        int index = 0;
        for (Iterator<ChannelDetailDTO> iterator = channels.iterator(); iterator.hasNext(); ) {
            ChannelDetailDTO channelDTO = iterator.next();
            names += channelDTO.getName();
            if (index < channels.size() - 1)
                names += ",";
            index++;
        }
        return names + "]";
    }

    @Override public JobResult execute(ApplicationContext applicationContext, JobDataMap jobDataMap, JobResult jobResult) {
        IChannelInventorySyncService channelInventorySyncService = applicationContext.getBean(IChannelInventorySyncService.class);

        Set<ChannelDetailDTO> stockoutChannelsToProcess = new HashSet<>();
        Set<ChannelDetailDTO> channelsToProcess = new HashSet<>();
        for (ChannelDetailDTO channelDetailDTO : CacheManager.getInstance().getCache(ChannelCache.class).getChannels()) {
            if (channelDetailDTO.isEnabled() && SyncStatus.ON.name().equals(channelDetailDTO.getInventorySyncStatus())) {
                boolean processAll = false;
                Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(channelDetailDTO.getSource().getCode());
                for (String cronExpression : StringUtils.split(jobDataMap.getString(JobConstants.TRIGGER_CRON_EXPRESSION), JobConstants.MULTI_TRIGGER_SEPARATOR)) {
                    if (JobUtils.isItABestTimeToRun(source.getInventoryUpdateCronExpression(), cronExpression)) {
                        processAll = true;
                    }
                }

                if (processAll) {
                    channelsToProcess.add(channelDetailDTO);
                } else {
                    stockoutChannelsToProcess.add(channelDetailDTO);
                }
            }
        }
        if (channelsToProcess.isEmpty() && stockoutChannelsToProcess.isEmpty()) {
            return jobResult;
        }

        String channelList = getChannelString(channelsToProcess);
        for (ChannelDetailDTO channel : channelsToProcess) {
            channelInventorySyncService.processChannelInventory(channel.getCode(), false, false);
        }
        LOG.info("Inventory sync scheduled for " + channelList);

        String stockoutChannelList = getChannelString(stockoutChannelsToProcess);
        for (ChannelDetailDTO channel : stockoutChannelsToProcess) {
            channelInventorySyncService.processChannelInventory(channel.getCode(), true, false);
        }
        LOG.info("Stockout inventory sync scheduled for " + stockoutChannelList);
        return jobResult;
    }
}
