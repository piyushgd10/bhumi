/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 09-May-2012
 *  @author praveeng
 */
package com.uniware.services.tasks.purchaseOrder;

import org.quartz.JobDataMap;
import org.springframework.context.ApplicationContext;

import com.unifier.core.entity.JobResult;
import com.unifier.services.job.IJobWorker;
import com.uniware.services.purchase.IPurchaseService;

public class AutoCompletePOTask implements IJobWorker {

    @Override
    public JobResult execute(ApplicationContext applicationContext, JobDataMap jobDataMap, JobResult jobResult) {
        IPurchaseService purchaseService = applicationContext.getBean(IPurchaseService.class);
        purchaseService.autoCompletePurchaseOrders();
        jobResult.setMessage("Auto Complete PO Task completed successfully");
        return jobResult;
    }

}
