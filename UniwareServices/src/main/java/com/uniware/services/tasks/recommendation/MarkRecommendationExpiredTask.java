/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 1, 2015
 *  @author akshay
 */
package com.uniware.services.tasks.recommendation;

import java.util.ArrayList;
import java.util.List;

import org.quartz.JobDataMap;
import org.springframework.context.ApplicationContext;

import com.unifier.core.entity.JobResult;
import com.unifier.core.utils.DateUtils;
import com.unifier.services.job.IJobWorker;
import com.uniware.core.api.recommendation.GetRecommendationsResponse;
import com.uniware.core.api.recommendation.ListRecommendationsRequest;
import com.uniware.core.api.recommendation.dto.RecommendationDTO;
import com.uniware.core.concurrent.ContextAwareExecutorFactory;
import com.uniware.core.vo.PriceRecommendationVO;
import com.uniware.mao.recommendation.IRecommendationMao;
import com.uniware.services.recommendation.IRecommendationManagementService;
import com.uniware.services.recommendation.impl.RepairRecommendationHandler;

public class MarkRecommendationExpiredTask implements IJobWorker {

    private static final String SERVICE_NAME = "recommendationManager";

    @Override public JobResult execute(ApplicationContext applicationContext, JobDataMap jobDataMap, JobResult jobResult) {
        IRecommendationManagementService recommendationManagementService = applicationContext.getBean(IRecommendationManagementService.class);
        IRecommendationMao recommendationMao = applicationContext.getBean(IRecommendationMao.class);
        ContextAwareExecutorFactory executorFactory = applicationContext.getBean(ContextAwareExecutorFactory.class);

        ListRecommendationsRequest request = new ListRecommendationsRequest();
        request.setRecommendationStatus(PriceRecommendationVO.RecommendationStatus.ACTIVE.name());
        request.setNoOfResults(recommendationMao.getActiveRecommendationsCount());
        GetRecommendationsResponse response = recommendationManagementService.listRecommendations(request);
        if (response.isSuccessful()) {
            if (!response.getRecommendations().isEmpty()) {
                List<RepairRecommendationHandler.RepairRecommendationJob> recommendationRepairJobs = new ArrayList<>();
                for (RecommendationDTO recommendation : response.getRecommendations()) {
                    if (recommendation.getExpiryTime().before(DateUtils.getCurrentTime())) {
                        RepairRecommendationHandler.RepairRecommendationJob repairJob = new RepairRecommendationHandler.RepairRecommendationJob(recommendation.getId(),
                                PriceRecommendationVO.RecommendationStatus.EXPIRED.name(), PriceRecommendationVO.RecommendationStatus.ACTIVE.name(),
                                PriceRecommendationVO.RecommendationStatus.EXPIRED.name());
                        recommendationRepairJobs.add(repairJob);
                    }
                }
                if (!recommendationRepairJobs.isEmpty()) {
                    submitRecommendationsToRepair(recommendationRepairJobs,recommendationMao,executorFactory );
                }
            }

            jobResult.setMessage("Task completed successfully");
        }
        return jobResult;
    }

    private void submitRecommendationsToRepair(List<RepairRecommendationHandler.RepairRecommendationJob> recommendationRepairJobs, IRecommendationMao recommendationMao,
            ContextAwareExecutorFactory executorFactory) {
        executorFactory.getExecutor(SERVICE_NAME).submit(new RepairRecommendationHandler(recommendationRepairJobs, recommendationMao));
    }

}
