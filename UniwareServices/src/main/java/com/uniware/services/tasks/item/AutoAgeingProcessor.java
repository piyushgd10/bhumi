/**
 * Copyright 2017 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version 1.0, 21/07/17
 * @author aditya
 */
package com.uniware.services.tasks.item;

import java.util.List;

import org.quartz.JobDataMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

import com.unifier.core.cache.CacheManager;
import com.unifier.core.entity.JobResult;
import com.unifier.core.utils.BatchProcessor;
import com.unifier.core.utils.DateUtils;
import com.unifier.services.job.IJobWorker;
import com.uniware.core.cache.FacilityCache;
import com.uniware.core.entity.Facility;
import com.uniware.core.entity.Item;
import com.uniware.core.entity.ItemType;
import com.uniware.core.utils.UserContext;
import com.uniware.services.catalog.ICatalogService;
import com.uniware.services.inventory.IInventoryService;

public class AutoAgeingProcessor implements IJobWorker {
    private static final Integer ITEM_TYPE_BATCH_SIZE = 100;
    private static final Integer ITEM_BATCH_SIZE      = 1000;
    private static final Logger  LOG                  = LoggerFactory.getLogger(AutoAgeingProcessor.class);

    @Override
    public JobResult execute(ApplicationContext applicationContext, JobDataMap jobDataMap, JobResult jobResult) {
        final IInventoryService inventoryService = applicationContext.getBean(IInventoryService.class);
        final ICatalogService catalogService = applicationContext.getBean(ICatalogService.class);
        final List<Facility> facilities = CacheManager.getInstance().getCache(FacilityCache.class).getFacilities();
        BatchProcessor<ItemType> itemTypeProcessor = new BatchProcessor<ItemType>(ITEM_TYPE_BATCH_SIZE) {

            @Override
            protected List<ItemType> next(int start, int batchSize) {
                LOG.info("ItemTypeBatch: Page: {}, BatchSize: {}", (start + 1) / batchSize, batchSize);
                return catalogService.getItemTypeByExpiryStatus(true, start, batchSize);
            }

            @Override
            protected void process(List<ItemType> expirableItemTypes, int batchIndex) {
                for (final ItemType itemType : expirableItemTypes) {
                    int shelfLife = catalogService.getShelfLifeForItemType(itemType);
                    facilities.forEach(facility -> {
                        try {
                            UserContext.current().setFacility(facility);
                            getItemBatchProcessor(itemType, shelfLife).process();
                        } finally {
                            UserContext.current().setFacility(null);
                        }
                    });
                }
            }

            private BatchProcessor<Item> getItemBatchProcessor(ItemType itemType, int shelfLife) {
                return new BatchProcessor<Item>(ITEM_BATCH_SIZE) {

                    private int idToProcessFrom = 0;

                    @Override
                    protected List<Item> next(int start, int batchSize) {
                        LOG.info("ItemBatch: Page: {}, BatchSize: {}", (start + 1) / batchSize, batchSize);
                        int allowedShelfLife = shelfLife - (itemType.getCategory().getDispatchExpiryTolerance() * shelfLife / 100);
                        return inventoryService.getItemsForAutoAging(itemType.getSkuCode(), idToProcessFrom, batchSize, allowedShelfLife);
                    }

                    @Override
                    protected void process(List<Item> batchItems, int batchIndex) {
                        int shelfLife = catalogService.getShelfLifeForItemType(itemType);
                        for (Item item : batchItems) {
                            inventoryService.markExpiredItemAsBadInventory(item, !DateUtils.addDaysToDate(item.getManufacturingDate(), shelfLife).after(DateUtils.getCurrentTime())
                                    ? Item.RejectionReason.EXPIRED_ITEM.name() : Item.RejectionReason.ABOUT_TO_EXPIRE.name());
                        }
                        if (!batchItems.isEmpty()) {
                            idToProcessFrom = batchItems.get(batchItems.size() - 1).getId();
                        }
                    }
                };
            }
        };
        itemTypeProcessor.process();
        return jobResult;
    }

}
