package com.uniware.services.tasks.notifications;

import java.util.List;

import org.quartz.JobDataMap;
import org.springframework.context.ApplicationContext;

import com.unifier.core.api.base.ServiceResponse;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.entity.JobResult;
import com.unifier.core.utils.BatchProcessor;
import com.unifier.services.job.IJobWorker;
import com.uniware.core.api.inventory.AcknowledgeStockoutInventoryRequest;
import com.uniware.core.api.inventory.AcknowledgeStockoutInventoryResponse;
import com.uniware.core.cache.FacilityCache;
import com.uniware.core.entity.Facility;
import com.uniware.core.entity.InventoryAvailabilityChangeLedger;
import com.uniware.services.inventory.IInventoryAvailabilityChangeService;

/**
 * @author piyush
 */
public class AcknowledgeInventoryLedgerTask implements IJobWorker {

    private static final int DEFAULT_BATCH_SIZE = 1000;

    @Override
    public JobResult execute(ApplicationContext applicationContext, final JobDataMap jobDataMap, JobResult jobResult) {
        final IInventoryAvailabilityChangeService inventoryAvailabilityChangeService = applicationContext.getBean(IInventoryAvailabilityChangeService.class);
        final UnacknowledgedInventoryLedgerStatus unacknowledgedInventoryLedgerStatus = inventoryAvailabilityChangeService.getUnacknowledgedInventoryLedgerStatus();
        final ServiceResponse response = new ServiceResponse();
        response.setSuccessful(true);
        for (final Facility facility : CacheManager.getInstance().getCache(FacilityCache.class).getFacilities()) {
            BatchProcessor<InventoryAvailabilityChangeLedger> processor = new BatchProcessor<InventoryAvailabilityChangeLedger>(DEFAULT_BATCH_SIZE) {
                @Override
                protected void process(List<InventoryAvailabilityChangeLedger> inventoryLedgers, int batchIndex) {
                    AcknowledgeStockoutInventoryResponse bResponse = inventoryAvailabilityChangeService.acknowledgeInventoryLedgers(new AcknowledgeStockoutInventoryRequest(
                            inventoryLedgers, facility.getCode()));
                    response.setSuccessful(response.isSuccessful() && bResponse.isSuccessful());
                    response.addErrors(bResponse.getErrors());
                }

                @Override
                protected List<InventoryAvailabilityChangeLedger> next(int start, int batchSize) {
                    return inventoryAvailabilityChangeService.getUnacknowledgedInventoryLedgersByFacilityId(batchSize, unacknowledgedInventoryLedgerStatus.getMaxId(),
                            facility.getId());
                }
            };
            processor.process();
        }
        if (response.isSuccessful()) {
            jobResult.setMessage("Task completed successfully");
        }
        return jobResult;
    }

    public static class UnacknowledgedInventoryLedgerStatus {
        private int maxId;
        private int count;

        public UnacknowledgedInventoryLedgerStatus(int maxId, int count) {
            this.maxId = maxId;
            this.count = count;
        }

        public int getMaxId() {
            return maxId;
        }

        public int getCount() {
            return count;
        }

    }
}
