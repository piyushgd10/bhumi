/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, May 28, 2012
 *  @author singla
 */
package com.uniware.services.tasks.imports;

import java.util.ArrayList;
import java.util.List;

import com.unifier.core.utils.StringUtils;
import org.quartz.JobDataMap;
import org.springframework.context.ApplicationContext;

import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.entity.JobResult;
import com.unifier.services.job.IJobWorker;
import com.unifier.services.job.JobConstants;
import com.uniware.core.api.channel.ChannelDetailDTO;
import com.uniware.core.api.channel.SyncChannelOrdersRequest;
import com.uniware.core.api.channel.SyncChannelOrdersResponse;
import com.uniware.core.entity.Channel.SyncStatus;
import com.uniware.core.entity.Source;
import com.uniware.services.cache.ChannelCache;
import com.uniware.services.channel.saleorder.IChannelOrderSyncService;
import com.uniware.services.configuration.SourceConfiguration;
import com.uniware.services.tasks.JobUtils;

/**
 * @author singla
 */
public class ThirdPartySaleOrderImportTask implements IJobWorker {

    @Override public JobResult execute(ApplicationContext applicationContext, JobDataMap jobDataMap, JobResult jobResult) {
        IChannelOrderSyncService channelOrderSyncService = applicationContext.getBean(IChannelOrderSyncService.class);
        List<String> channelCodesForNormalSync = new ArrayList<>();
        List<String> channelCodesForFrequentSync = new ArrayList<>();
        for (ChannelDetailDTO channel : CacheManager.getInstance().getCache(ChannelCache.class).getChannels()) {
            if (channel.isEnabled() && SyncStatus.ON.name().equals(channel.getOrderSyncStatus())) {
                Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(channel.getSource().getCode());
                if (source.isFrequentOrderSyncEnabled()) {
                    channelCodesForFrequentSync.add(channel.getCode());
                } else {
                    channelCodesForNormalSync.add(channel.getCode());
                }
            }
        }
        if (channelCodesForNormalSync.size() > 0) {
            SyncChannelOrdersResponse response = channelOrderSyncService.syncChannelOrders(new SyncChannelOrdersRequest(channelCodesForNormalSync, false));
            for (SyncChannelOrdersResponse.ChannelOrderSyncResponseDTO resultItem : response.getResultItems()) {
                jobResult.addResultItem(resultItem.getChannelCode(), resultItem.getMessage());
            }
        }
        if (channelCodesForFrequentSync.size() > 0) {
            SyncChannelOrdersResponse response = channelOrderSyncService.syncChannelOrders(new SyncChannelOrdersRequest(channelCodesForFrequentSync, true));
            for (SyncChannelOrdersResponse.ChannelOrderSyncResponseDTO resultItem : response.getResultItems()) {
                jobResult.addResultItem(resultItem.getChannelCode(), resultItem.getMessage());
            }
        }
        jobResult.setMessage("Task completed successfully");
        return jobResult;
    }
}
