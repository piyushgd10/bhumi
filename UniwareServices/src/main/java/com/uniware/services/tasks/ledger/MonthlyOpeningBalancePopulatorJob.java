/**
 * Copyright 2017 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version 1.0, 27/09/17
 * @author aditya
 */
package com.uniware.services.tasks.ledger;

import java.util.List;

import org.quartz.JobDataMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

import com.unifier.core.entity.JobResult;
import com.unifier.core.utils.BatchProcessor;
import com.unifier.services.job.IJobWorker;
import com.uniware.core.entity.ItemType;
import com.uniware.services.catalog.ICatalogService;
import com.uniware.services.ledger.IInventoryLedgerService;

public class MonthlyOpeningBalancePopulatorJob implements IJobWorker {

    private static final Logger LOG        = LoggerFactory.getLogger(MonthlyOpeningBalancePopulatorJob.class);

    private static final Integer BATCH_SIZE = 1000;

    @Override
    public JobResult execute(ApplicationContext applicationContext, JobDataMap jobDataMap, JobResult jobResult) {
        final IInventoryLedgerService ledgerService = applicationContext.getBean(IInventoryLedgerService.class);
        final ICatalogService catalogService = applicationContext.getBean(ICatalogService.class);
        BatchProcessor<ItemType> batchProcessor = new BatchProcessor<ItemType>(BATCH_SIZE) {
            @Override
            protected List<ItemType> next(int start, int batchSize) {
                LOG.info("Fetching {} item types starting from {}", new Object[] { batchSize, start });
                return catalogService.getAllItemTypes(start, batchSize);
            }

            @Override
            protected void process(List<ItemType> batchItems, int batchIndex) {
                ledgerService.addMonthlyOpeningBalanceForItemTypes(batchItems);
            }
        };
        batchProcessor.process();
        String message = "MonthlyOpeningBalancePopulatorJob completed. Processed :" + batchProcessor.getTotalItems();
        LOG.info(message);
        jobResult.setLastExecResult(message);
        return jobResult;    }
}
