/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 19-Feb-2014
 *  @author parijat
 */
package com.uniware.services.tasks.imports;

import com.unifier.core.cache.CacheManager;
import com.unifier.core.entity.JobResult;
import com.unifier.core.utils.BatchProcessor;
import com.unifier.services.job.IJobWorker;
import com.uniware.core.api.reconciliation.CreateUnreconciledSaleOrderRequest;
import com.uniware.core.api.reconciliation.PopulateInvoiceCodesForSaleOrderReconciliationRequest;
import com.uniware.core.entity.Channel;
import com.uniware.core.entity.SaleOrder;
import com.uniware.services.cache.ChannelCache;
import com.uniware.services.reconciliation.IReconciliationService;
import com.uniware.services.saleorder.ISaleOrderService;
import java.util.List;
import org.quartz.JobDataMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

/**
 * Task to create unreconciled row for recently fetched order
 *
 * @author parijat
 */
public class CreateOrderReconciliationDataTask implements IJobWorker {

    private static final Logger LOG = LoggerFactory.getLogger(CreateOrderReconciliationDataTask.class);

    @Override
    public JobResult execute(ApplicationContext applicationContext, JobDataMap jobDataMap, JobResult jobResult) {
        final ISaleOrderService saleOrderService = applicationContext.getBean(ISaleOrderService.class);
        final IReconciliationService reconciliationService = applicationContext.getBean(IReconciliationService.class);
        final ChannelCache channelCache = CacheManager.getInstance().getCache(ChannelCache.class);
        reconciliationService.populateInvoiceCodesForSaleOrderReconciliation(new PopulateInvoiceCodesForSaleOrderReconciliationRequest(30));
        BatchProcessor<SaleOrder> batchProcessor = new BatchProcessor<SaleOrder>(100) {
            @Override
            protected void process(List<SaleOrder> saleOrders, int batchIndex) {
                for (SaleOrder saleOrder : saleOrders) {
                    Channel channel = channelCache.getChannelById(saleOrder.getChannel().getId());
                    if (channel.isEnabled() && channel.getReconciliationSyncStatus().equals(Channel.SyncStatus.ON)) {
                        reconciliationService.createUnreconciliedSaleOrder(new CreateUnreconciledSaleOrderRequest(saleOrder.getCode()));
                    } else {
                        saleOrder.setReconciliationStatus(SaleOrder.ReconciliationStatus.IRRECONCILIABLE.name());
                        saleOrder.setReconciliationPending(false);
                        saleOrderService.updateSaleOrder(saleOrder);
                    }
                }
            }

            @Override
            protected List<SaleOrder> next(int start, int batchSize) {
                LOG.info("Fetching pending reconciliation sale orders, page: {}", start / batchSize);
                return saleOrderService.getReconciliationPendingOrders(0, batchSize);
            }
        };
        batchProcessor.process();
        jobResult.addResultItem("Processing Status", "Processed:[" + batchProcessor.getTotalItems() + "]");
        jobResult.setMessage("Task completed successfully.");
        return jobResult;
    }

}
