/*
 *  Copyright 2013 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 23-Jun-2013
 *  @author unicom
 */
package com.uniware.services.tasks.saleorder;

import java.util.ArrayList;
import java.util.List;

import org.quartz.JobDataMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

import com.unifier.core.cache.CacheManager;
import com.unifier.core.email.EmailMessage;
import com.unifier.core.entity.JobResult;
import com.unifier.core.utils.StringUtils;
import com.unifier.services.email.IEmailService;
import com.unifier.services.job.IJobWorker;
import com.uniware.core.cache.FacilityCache;
import com.uniware.core.entity.Facility;
import com.uniware.core.entity.PartyContact;
import com.uniware.core.entity.PartyContactType;
import com.uniware.core.entity.SaleOrderItem;
import com.uniware.core.utils.Constants.EmailTemplateType;
import com.uniware.services.saleorder.ISaleOrderService;
import com.uniware.services.warehouse.IFacilityService;

public class PendingSaleOrderItemsNotificationTask implements IJobWorker {

    private static final Logger LOG = LoggerFactory.getLogger(PendingSaleOrderItemsNotificationTask.class);

    @Override public JobResult execute(ApplicationContext applicationContext, JobDataMap jobDataMap, JobResult jobResult) {
        ISaleOrderService saleOrderService = applicationContext.getBean(ISaleOrderService.class);
        IFacilityService facilityService = applicationContext.getBean(IFacilityService.class);
        IEmailService emailService = applicationContext.getBean(IEmailService.class);

        List<Integer> facilityIds = facilityService.getFacilityIdsWithPendingSaleOrderItems();
        for (Integer facilityId : facilityIds) {
            Facility facility = CacheManager.getInstance().getCache(FacilityCache.class).getFacilityById(facilityId);
            PartyContact partyContact = facility.getPartyContactByType(PartyContactType.Code.PRIMARY.name());
            if (partyContact != null && StringUtils.isNotBlank(partyContact.getEmail())) {
                List<SaleOrderItem> saleOrderItems = saleOrderService.getPendingSaleOrderItemsByFacilityId(facilityId);
                try {
                    List<String> recipients = new ArrayList<>();
                    recipients.add(partyContact.getEmail());
                    EmailMessage message = new EmailMessage(recipients, EmailTemplateType.PENDING_SALE_ORDER_ITEMS.name());
                    message.addTemplateParam("saleOrderItems", saleOrderItems);
                    emailService.send(message);
                } catch (Exception e) {
                    LOG.error("Error occurred while sending pending sale order items mail to facility: " + partyContact, e);
                }
            }
        }
        jobResult.setMessage("Pending Sale Order Items Task completed successfully");
        return jobResult;
    }
}
