/*
 *  Copyright 2013 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *  @version     1.0, 03-Aug-2015
 *  @author akshaykochhar
 */
package com.uniware.services.tasks.saleorder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.quartz.JobDataMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.email.EmailMessage;
import com.unifier.core.entity.JobResult;
import com.unifier.core.sms.SmsMessage;
import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.StringUtils;
import com.unifier.services.email.IEmailService;
import com.unifier.services.job.IJobWorker;
import com.unifier.services.sms.ISmsService;
import com.unifier.services.tenantprofile.service.ITenantProfileService;
import com.unifier.services.vo.TenantProfileVO;
import com.uniware.core.cache.FacilityCache;
import com.uniware.core.entity.Facility;
import com.uniware.core.entity.FacilityProfile;
import com.uniware.core.entity.FacilityProfile.DayTiming;
import com.uniware.core.entity.SaleOrder;
import com.uniware.core.entity.SmsTemplate;
import com.uniware.core.utils.Constants;
import com.uniware.core.utils.UserContext;
import com.uniware.services.configuration.EmailConfiguration;
import com.uniware.services.configuration.SmsConfiguration;
import com.uniware.services.saleorder.ISaleOrderService;
import com.uniware.services.warehouse.IFacilityService;

/**
 * Created by akshaykochhar on 03/08/15.
 */
public class PendingSaleOrderNotificationsTask implements IJobWorker {

    private static final Logger LOG                          = LoggerFactory.getLogger(PendingSaleOrderNotificationsTask.class);

    public static final String  PARAM_PENDING_SINCE_IN_HOURS = "pendingSinceInHours";

    public static final String  PARAM_PENDING_STATUSES       = "pendingStatuses";

    @Override
    public JobResult execute(ApplicationContext applicationContext, JobDataMap jobDataMap, JobResult jobResult) {
        ISaleOrderService saleOrderService = applicationContext.getBean(ISaleOrderService.class);
        IEmailService emailService = applicationContext.getBean(IEmailService.class);
        IFacilityService facilityService = applicationContext.getBean(IFacilityService.class);
        ISmsService smsService = applicationContext.getBean(ISmsService.class);
        ITenantProfileService tenantProfileService = applicationContext.getBean(ITenantProfileService.class);

        List<String> pendingOrderStatuses = Arrays.asList(jobDataMap.getString(PARAM_PENDING_STATUSES).split(","));
        List<Integer> facilityIds = facilityService.getFacilityIdsWithPendingSaleOrders(jobDataMap.getIntegerFromString(PARAM_PENDING_SINCE_IN_HOURS), pendingOrderStatuses);
        boolean templatesAvailable = false;
        EmailConfiguration.EmailTemplateVO emailTemplateVO = ConfigurationManager.getInstance().getConfiguration(EmailConfiguration.class).getTemplateByType(
                Constants.EmailTemplateType.PENDING_SALE_ORDERS_EMAIL.name());
        SmsConfiguration.SmsTemplateVO smsTemplate = ConfigurationManager.getInstance().getConfiguration(SmsConfiguration.class).getTemplateByName(
                SmsTemplate.Name.PENDING_ORDERS_NOTIFICATION.name());
        if ((emailTemplateVO != null && emailTemplateVO.isEnabled() || (smsTemplate != null && smsTemplate.isEnabled()))) {
            templatesAvailable = true;
        }
        if (templatesAvailable) {
            TenantProfileVO tenantProfile = tenantProfileService.getTenantProfileByCode(UserContext.current().getTenant().getCode());
            for (Integer facilityId : facilityIds) {
                Facility facility = CacheManager.getInstance().getCache(FacilityCache.class).getFacilityById(facilityId);
                if (Facility.Type.STORE.name().equals(facility.getType())) {
                    FacilityProfile facilityProfile = CacheManager.getInstance().getCache(FacilityCache.class).getFacilityProfileByCode(facility.getCode());
                    com.uniware.core.entity.FacilityProfile.StoreTiming storeTiming = facilityProfile.getStoreTimings();
                    boolean storeClosed = false;
                    String currentDay = DateUtils.getWeekDayName(DateUtils.getCurrentDay());
                    for (DayTiming dayTiming : storeTiming.getDayTimings()) {
                        if (dayTiming.getDayOfWeek().name().equalsIgnoreCase(currentDay)) {
                            boolean closed = dayTiming.isClosed();
                            Date closingTime = DateUtils.stringToDate(dayTiming.getClosingTime(), "HH:mm");
                            if (closed || !DateUtils.isFutureTime(closingTime)) {
                                storeClosed = true;
                            }
                        }
                    }
                    if (!storeClosed) {
                        List<SaleOrder> saleOrders = saleOrderService.getPendingSaleOrders(jobDataMap.getIntegerFromString(PARAM_PENDING_SINCE_IN_HOURS), pendingOrderStatuses,
                                facilityId);
                        if (emailTemplateVO != null && emailTemplateVO.isEnabled() && StringUtils.isNotBlank(facilityProfile.getContactEmail())) {
                            try {
                                List<String> recipients = new ArrayList<String>();
                                recipients.add(facilityProfile.getContactEmail());
                                EmailMessage message = new EmailMessage(recipients, Constants.EmailTemplateType.PENDING_SALE_ORDERS_EMAIL.name());
                                message.addTemplateParam("saleOrders", saleOrders);
                                message.addTemplateParam("facility", facility);
                                message.addTemplateParam("facilityProfile", facilityProfile);
                                message.addTemplateParam("tenantProfile", tenantProfile);
                                emailService.send(message);
                            } catch (Exception e) {
                                LOG.error("Error occured while sending pending sale order mail to facility: " + facilityProfile.getContactEmail(), e);
                            }
                        }
                        if (smsTemplate != null && smsTemplate.isEnabled() && StringUtils.isNotBlank(facilityProfile.getMobile())) {
                            try {
                                SmsMessage message = new SmsMessage(facilityProfile.getMobile(), smsTemplate.getName());
                                message.addTemplateParam("saleOrders", saleOrders);
                                message.addTemplateParam("facility", facility);
                                message.addTemplateParam("facilityProfile", facilityProfile);
                                message.addTemplateParam("tenantProfile", tenantProfile);
                                smsService.send(message);
                            } catch (Exception e) {
                                LOG.error("Error occured while sending pending orderd sms:", e);
                            }
                        }
                    }
                }
            }
        } else {
            LOG.error("Error while running pending order notification task: No templates enabled");
        }
        jobResult.setMessage("Pending Sale Order Task completed successfully");
        return jobResult;
    }
}
