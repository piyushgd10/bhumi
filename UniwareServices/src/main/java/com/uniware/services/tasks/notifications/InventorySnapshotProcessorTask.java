/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Nov 2, 2012
 *  @author praveeng
 */
package com.uniware.services.tasks.notifications;

import org.quartz.JobDataMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

import com.unifier.core.entity.JobResult;
import com.unifier.services.job.IJobWorker;
import com.uniware.services.channel.IChannelInventorySyncService;

public class InventorySnapshotProcessorTask implements IJobWorker {

    private static final Logger LOG = LoggerFactory.getLogger(InventorySnapshotProcessorTask.class);

    @Override
    public JobResult execute(ApplicationContext applicationContext, JobDataMap jobDataMap, JobResult jobResult) {
        IChannelInventorySyncService channelInventorySyncService = applicationContext.getBean(IChannelInventorySyncService.class);
        channelInventorySyncService.acknowledgePendingInventorySnapshots();
        return jobResult;
    }
}
