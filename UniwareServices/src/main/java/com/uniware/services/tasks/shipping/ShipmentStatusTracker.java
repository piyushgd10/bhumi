/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 21, 2011
 *  @author singla
 */
package com.uniware.services.tasks.shipping;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.quartz.JobDataMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.entity.JobResult;
import com.unifier.core.utils.BatchProcessor;
import com.unifier.core.utils.DateUtils;
import com.unifier.services.job.IJobWorker;
import com.uniware.core.api.shipping.ShipmentTrackingSyncStatusDTO;
import com.uniware.core.api.shipping.UpdateTrackingStatusResponse;
import com.uniware.core.entity.ShipmentTracking;
import com.uniware.core.entity.ShippingProvider;
import com.uniware.services.configuration.ShippingConfiguration;
import com.uniware.services.configuration.ShippingSourceConfiguration;
import com.uniware.services.shipping.IShipmentTrackingService;

/**
 * @author singla
 */
public class ShipmentStatusTracker implements IJobWorker {

    private static final int    DEFAULT_BATCH_SIZE = 100;

    private static final Logger LOG                = LoggerFactory.getLogger(ShipmentStatusTracker.class);

    @Override public JobResult execute(ApplicationContext applicationContext, JobDataMap jobDataMap, final JobResult jobResult) {
        final IShipmentTrackingService shipmentTrackingService = applicationContext.getBean(IShipmentTrackingService.class);
        ShippingConfiguration shippingConfiguration = ConfigurationManager.getInstance().getConfiguration(ShippingConfiguration.class);
        ShippingSourceConfiguration sourceConfiguration = ConfigurationManager.getInstance().getConfiguration(ShippingSourceConfiguration.class);
        List<ShippingProvider> shippingProviders = new ArrayList<>();
        Set<String> ignoredShippingProvides = new HashSet<>();
        List<ShippingProvider> allShippingProviders = shippingConfiguration.getAllShippingProviders();
        for (ShippingProvider shippingProvider : allShippingProviders) {

            if (sourceConfiguration.getShippingSourceByCode(shippingProvider.getShippingProviderSourceCode()).isTrackingEnabled()) {
                shippingProviders.add(shippingProvider);
            } else {
                ignoredShippingProvides.add(shippingProvider.getCode());
            }
        }

        for (final ShippingProvider shippingProvider : shippingProviders) {
            final ShipmentTrackingSyncStatusDTO syncStatusDTO = ConfigurationManager.getInstance().getConfiguration(ShippingConfiguration.class).getShipmentTrackingSyncStatusDTOByCode(
                    shippingProvider.getCode());
            try {
                LOG.info("Updating Statuses for: {}", shippingProvider.getName());
                syncStatusDTO.reset();
                syncStatusDTO.setRunning(true);
                BatchProcessor<ShipmentTracking> processor = new BatchProcessor<ShipmentTracking>(DEFAULT_BATCH_SIZE) {
                    @Override
                    protected void process(List<ShipmentTracking> shipments, int batchIndex) {
                        UpdateTrackingStatusResponse response = shipmentTrackingService.updateTrackingStatuses(shippingProvider, shipments, syncStatusDTO);
                        jobResult.addResultItem(shippingProvider.getCode(), response);
                    }

                    @Override
                    protected List<ShipmentTracking> next(int start, int batchSize) {
                        LOG.info("Processing shipment tracking for start:{}, batch:{}", start, batchSize);
                        return shipmentTrackingService.getShipmentsForStatusPolling(shippingProvider.getId(), start, batchSize);
                    }
                };
                processor.process();
            } catch (Exception e) {
                syncStatusDTO.setLastSyncFailedNotificationTime(DateUtils.getCurrentTime());
                syncStatusDTO.setMessage(e.getMessage());
                LOG.error("Error processing tracking statuses for {}", shippingProvider.getCode());
            }
            syncStatusDTO.setRunning(false);
            ConfigurationManager.getInstance().getConfiguration(ShippingConfiguration.class).updateShipmentTrackinfSyncStatusDTOByCode(syncStatusDTO);
            shipmentTrackingService.saveShippingProviderSyncStatus(syncStatusDTO);
        }
        if (ignoredShippingProvides.size() > 0) {
            jobResult.addResultItem("Ignore Providers", ignoredShippingProvides);
        }
        return jobResult;
    }
}
