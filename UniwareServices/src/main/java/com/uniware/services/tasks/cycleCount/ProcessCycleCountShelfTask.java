/**
 * Copyright 2017 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version 1.0, 27/06/17
 * @author aditya
 */
package com.uniware.services.tasks.cycleCount;

import com.unifier.core.api.base.ServiceResponse;
import com.unifier.core.entity.JobResult;
import com.unifier.services.job.IJobWorker;
import com.uniware.services.cyclecount.ICycleCountService;
import org.quartz.JobDataMap;
import org.springframework.context.ApplicationContext;

public class ProcessCycleCountShelfTask implements IJobWorker {

    private ICycleCountService cycleCountService;

    @Override
    public JobResult execute(ApplicationContext applicationContext, JobDataMap jobDataMap, JobResult jobResult) {
        cycleCountService = applicationContext.getBean(ICycleCountService.class);
        ServiceResponse response = cycleCountService.preProcessShelvesForCycleCount();
        String message = "ProcessCycleCountShelfTask complete. Response message: " + response.getMessage() + ", Successful: " + response.isSuccessful();
        jobResult.setLastExecResult(message);
        return jobResult;
    }
}
