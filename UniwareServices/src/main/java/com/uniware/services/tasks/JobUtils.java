/*
 * Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *    
 * @version     1.0, 9/28/15 11:32 AM
 * @author amdalal 
 */

package com.uniware.services.tasks;

import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.StringUtils;
import com.unifier.services.job.JobConstants;
import org.quartz.CronExpression;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.support.CronSequenceGenerator;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

public class JobUtils {

    private static final Logger LOG = LoggerFactory.getLogger(JobUtils.class);

    private JobUtils() {

    }

    public static boolean isItAGoodTimeToRun(String sourceCronExpression, String jobCronExpression) {
        Date currentTime = DateUtils.getCurrentTime();
        Long nextRun = getNextRun(sourceCronExpression, currentTime).getTime();
        try {
            int timeRemainingForNextRun = DateUtils.diff(new CronExpression(jobCronExpression).getNextValidTimeAfter(currentTime), currentTime, DateUtils.Resolution.MILLISECOND);
            return (nextRun > currentTime.getTime()) && (nextRun <= (currentTime.getTime() + timeRemainingForNextRun));
        } catch (ParseException e) {
            LOG.error("Invalid cron expression " + jobCronExpression, e);
            throw new RuntimeException(e);
        }
    }

    /**
     * For any clarifications, contact : (We can't explain here) :P
     *
     * @author Piyush Arora
     * @author Rachit Sachdeva
     */

    public static boolean isItABestTimeToRun(String sourceCronExpression, String jobCronExpression) {
        Date currentTime = DateUtils.getCurrentTime();
        Date jobNextRun = getNextRun(jobCronExpression, currentTime);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(jobNextRun);
        int jobCronSeconds = calendar.get(Calendar.SECOND);
        Long sourceNextRun = getNextRun(sourceCronExpression, currentTime).getTime();
        int sourceCronInterval = getDiffBetweenRuns(sourceCronExpression, DateUtils.Resolution.MINUTE);
        int sourceCurrentDiff = DateUtils.diff(DateUtils.addToDate(new Date(sourceNextRun), Calendar.SECOND, jobCronSeconds), currentTime, DateUtils.Resolution.MINUTE);
        // Handling failure case for isItAGoodTimeToRun
        if (sourceCurrentDiff >= sourceCronInterval) {
            return true;
        }
        try {
            int timeRemainingForNextRun = DateUtils.diff(new CronExpression(jobCronExpression).getNextValidTimeAfter(currentTime), currentTime, DateUtils.Resolution.MILLISECOND);
            return (sourceNextRun > currentTime.getTime()) && (sourceNextRun <= (currentTime.getTime() + timeRemainingForNextRun));
        } catch (ParseException e) {
            LOG.error("Invalid cron expression " + jobCronExpression, e);
            throw new RuntimeException(e);
        }
    }

    public static Date getNextRun(String cronExpression, Date currentTime) {
        CronSequenceGenerator cronSequenceGenerator = new CronSequenceGenerator(cronExpression);
        return cronSequenceGenerator.next(currentTime);
    }

    public static int getDiffBetweenRuns(String cronExpression, DateUtils.Resolution resolution) {
        Date currentTime = DateUtils.getCurrentTime();
        CronSequenceGenerator cronSequenceGenerator = new CronSequenceGenerator(cronExpression);
        Date date1 = cronSequenceGenerator.next(currentTime);
        Date date2 = cronSequenceGenerator.next(date1);
        return DateUtils.diff(date1, date2, resolution);

    }

    public static boolean hasMultipleTriggers(String triggerCronExpression) {
        return StringUtils.split(triggerCronExpression, JobConstants.MULTI_TRIGGER_SEPARATOR).size() > 1;
    }

    public static void main(String[] args) {
        int seconds = 0;
        String sourceCronExpression = "0 0/10 * * * *";
        while (seconds < 60) {
            String jobCronExpression = Integer.toString(seconds) + " */2 * * * ?";
            int count = 0;
            int trueCount = 0;
            int falseCount = 0;
            Date currentTime = DateUtils.getCurrentTime();
            while (count < 10) {
                boolean result = isItABestTimeToRun(sourceCronExpression, jobCronExpression);
                if (result) {
                    trueCount += 1;
                } else {
                    falseCount += 1;
                }
                count += 1;
                currentTime = DateUtils.addToDate(currentTime, Calendar.MINUTE, 2);
                // System.out.println(time);
            }
            System.out.println(seconds + " :  " + trueCount + " " + falseCount);
            seconds += 1;

        }

    }

}
