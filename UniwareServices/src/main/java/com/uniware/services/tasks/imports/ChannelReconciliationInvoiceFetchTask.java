/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 29-Sep-2014
 *  @author parijat
 */
package com.uniware.services.tasks.imports;

import org.quartz.JobDataMap;
import org.springframework.context.ApplicationContext;

import com.unifier.core.cache.CacheManager;
import com.unifier.core.entity.JobResult;
import com.unifier.core.utils.StringUtils;
import com.unifier.services.job.IJobWorker;
import com.uniware.core.api.channel.ChannelDetailDTO;
import com.uniware.core.api.channel.SyncChannelReconciliationInvoiceRequest;
import com.uniware.core.api.channel.SyncChannelReconciliationInvoiceResponse;
import com.uniware.core.entity.Channel;
import com.uniware.core.entity.Source;
import com.uniware.services.cache.ChannelCache;
import com.uniware.services.channel.IChannelReconciliationInvoiceSyncService;

/**
 * The task will reconciliation invoices from different channels
 *
 * @author parijat
 */
public class ChannelReconciliationInvoiceFetchTask implements IJobWorker {

    @Override
    public JobResult execute(ApplicationContext applicationContext, JobDataMap jobDataMap, JobResult jobResult) {
        IChannelReconciliationInvoiceSyncService channelReconciliationInvoiceSyncService = applicationContext.getBean(IChannelReconciliationInvoiceSyncService.class);
        ChannelCache channelCache = CacheManager.getInstance().getCache(ChannelCache.class);
        for (ChannelDetailDTO channel : channelCache.getChannels()) {
            if (channel.isEnabled() && channel.getOrderReconciliationSyncStatus().equals(Channel.SyncStatus.ON.name())
                    && StringUtils.isNotBlank(channelCache.getScriptName(channel.getCode(), Source.CHANNEL_RECONCILIATION_INVOICE_LIST_SCRIPT_NAME))
                    && StringUtils.isNotBlank(channelCache.getScriptName(channel.getCode(), Source.CHANNEL_RECONCILIATION_INVOICE_DETAIL_SCRIPT_NAME))) {
                SyncChannelReconciliationInvoiceResponse response = channelReconciliationInvoiceSyncService.syncChannelReconciliationInvoices(new SyncChannelReconciliationInvoiceRequest(
                        channel.getCode()));
                jobResult.addResultItem(channel.getCode(), response.getMessage());
            }
        }
        jobResult.setMessage("Task completed successfully.");
        return jobResult;
    }
}
