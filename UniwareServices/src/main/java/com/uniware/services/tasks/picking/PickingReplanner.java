/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Apr 6, 2012
 *  @author singla
 */
package com.uniware.services.tasks.picking;

import java.util.List;

import org.quartz.JobDataMap;
import org.springframework.context.ApplicationContext;

import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.entity.JobResult;
import com.unifier.services.job.IJobWorker;
import com.uniware.core.cache.FacilityCache;
import com.uniware.core.entity.Facility;
import com.uniware.core.entity.PickSet;
import com.uniware.services.configuration.PickConfiguration;
import com.uniware.services.picker.IPickerService;

/**
 * @author singla
 */
public class PickingReplanner implements IJobWorker {

    @Override
    public JobResult execute(ApplicationContext applicationContext, JobDataMap jobDataMap, JobResult jobResult) {
        IPickerService pickerService = applicationContext.getBean(IPickerService.class);
        Facility facility = CacheManager.getInstance().getCache(FacilityCache.class).getCurrentFacility();
        if (Facility.Type.WAREHOUSE.name().equals(facility.getType())) {
            List<PickSet> pickSets = ConfigurationManager.getInstance().getConfiguration(PickConfiguration.class).getPickSets();
            for (PickSet pickSet : pickSets) {
                pickerService.replanPicking(pickSet.getId());
            }
            jobResult.setMessage("Pick Planning Successful");
        } else {
            jobResult.setMessage("Skipped for facility:" + facility.getCode());
        }
        return jobResult;
    }
}
