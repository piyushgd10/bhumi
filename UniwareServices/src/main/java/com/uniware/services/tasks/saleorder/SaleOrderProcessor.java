/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 21, 2011
 *  @author singla
 */
package com.uniware.services.tasks.saleorder;

import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.quartz.JobDataMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

import com.unifier.core.entity.JobResult;
import com.unifier.services.job.IJobWorker;
import com.uniware.core.api.saleorder.AllocateInventoryCreatePackageRequest;
import com.uniware.core.api.shipping.ReassessShippingPackageServiceabilityRequest;
import com.uniware.core.entity.ShippingPackage;
import com.uniware.services.inventory.IInventoryService;
import com.uniware.services.saleorder.ISaleOrderProcessingService;
import com.uniware.services.saleorder.ISaleOrderService;
import com.uniware.services.shipping.IShippingService;

/**
 * @author singla
 */
public class SaleOrderProcessor implements IJobWorker {

    private static final Logger LOG = LoggerFactory.getLogger(SaleOrderProcessor.class);

    public static class SaleOrderDTO implements Comparable<SaleOrderDTO> {
        private Integer id;
        private int     priority;
        private Date    fulfilmentTat;

        public SaleOrderDTO(Integer id, int priority, Date fulfilmentTat) {
            this.id = id;
            this.priority = priority;
            this.fulfilmentTat = fulfilmentTat;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public int getPriority() {
            return priority;
        }

        public void setPriority(int priority) {
            this.priority = priority;
        }

        public Date getFulfilmentTat() {
            return fulfilmentTat;
        }

        public void setFulfilmentTat(Date fulfilmentTat) {
            this.fulfilmentTat = fulfilmentTat;
        }

        @Override
        public int compareTo(SaleOrderDTO o) {
            return this.priority == o.getPriority() ? this.fulfilmentTat.equals(o.getFulfilmentTat()) ? this.id - o.getId() : this.fulfilmentTat.compareTo(o.getFulfilmentTat())
                    : o.getPriority() - this.priority;
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + ((id == null) ? 0 : id.hashCode());
            return result;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            SaleOrderDTO other = (SaleOrderDTO) obj;
            if (id == null) {
                if (other.id != null)
                    return false;
            } else if (!id.equals(other.id))
                return false;
            return true;
        }

        @Override
        public String toString() {
            return "SaleOrderDTO [id=" + id + ", priority=" + priority + ", fulfilmentTat=" + fulfilmentTat + "]";
        }
    }

    public static class ItemTypeDTO {
        private Integer id;
        private int     reassessVersion;

        public ItemTypeDTO(Integer id, int reassessVersion) {
            this.id = id;
            this.reassessVersion = reassessVersion;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public int getReassessVersion() {
            return reassessVersion;
        }

        public void setReassessVersion(int reassessVersion) {
            this.reassessVersion = reassessVersion;
        }
    }

    @Override
    public JobResult execute(ApplicationContext applicationContext, JobDataMap jobDataMap, JobResult jobResult) {
        Set<SaleOrderDTO> saleOrders = new TreeSet<>();
        IInventoryService inventoryService = applicationContext.getBean(IInventoryService.class);
        ISaleOrderService saleOrderService = applicationContext.getBean(ISaleOrderService.class);
        IShippingService shippingService =  applicationContext.getBean(IShippingService.class);
        ISaleOrderProcessingService saleOrderProcessingService = applicationContext.getBean(ISaleOrderProcessingService.class);
        
        List<ItemTypeDTO> itemTypes = inventoryService.getItemTypesToBeReassessed();
        for (SaleOrderProcessor.ItemTypeDTO itemType : itemTypes) {
            saleOrders.addAll(saleOrderService.getSaleOrdersForProcessing(itemType.getId()));
        }
        saleOrders.addAll(saleOrderService.getSaleOrdersForProcessing());
        for (SaleOrderProcessor.SaleOrderDTO saleOrder : saleOrders) {
            AllocateInventoryCreatePackageRequest request = new AllocateInventoryCreatePackageRequest();
            request.setSaleOrderId(saleOrder.getId());
            try {
                saleOrderProcessingService.allocateInventoryAndCreatePackage(request);
            } catch (Exception e) {
                LOG.error("Error while creating package for sale order " + saleOrder.getId(), e);
            }
        }
        for (SaleOrderProcessor.ItemTypeDTO itemType : itemTypes) {
            inventoryService.resetReassessVersionIfLatest(itemType.getId(), itemType.getReassessVersion());
        }

        List<ShippingPackage> shippingPackages = shippingService.getShippingPackagesByStatusCode(ShippingPackage.StatusCode.LOCATION_NOT_SERVICEABLE.name());
        for (ShippingPackage shippingPackage : shippingPackages) {
            ReassessShippingPackageServiceabilityRequest request = new ReassessShippingPackageServiceabilityRequest();
            request.setShippingPackageId(shippingPackage.getId());
            shippingService.reassessShippingPackageLocationServiceability(request);
        }
        String message = "SaleOrderProcessor complete, Processed SaleOrder: " + saleOrders.size() + ", ShippingPackage: " + shippingPackages.size(); 
        LOG.info(message);
        jobResult.setLastExecResult(message);
        return jobResult;
    }
}
