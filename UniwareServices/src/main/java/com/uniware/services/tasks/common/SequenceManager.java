/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Oct 9, 2012
 *  @author singla
 */
package com.uniware.services.tasks.common;

import org.quartz.JobDataMap;
import org.springframework.context.ApplicationContext;

import com.unifier.core.entity.JobResult;
import com.unifier.services.job.IJobWorker;
import com.uniware.services.common.ISequenceGenerator;

public class SequenceManager implements IJobWorker {

    @Override
    public JobResult execute(ApplicationContext applicationContext, JobDataMap jobDataMap, JobResult jobResult) {
        ISequenceGenerator sequenceGenerator = applicationContext.getBean(ISequenceGenerator.class);
        sequenceGenerator.updateSequencePrefixes();
        jobResult.setMessage("Task completed successfully");
        return jobResult;
    }
}
