/**
 * Copyright 2017 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version 1.0, 21/07/17
 * @author aditya
 */
package com.uniware.services.exception;

public class ShelfBlockedForCycleCountException extends Exception {
    public ShelfBlockedForCycleCountException(String message) {
        super(message);
    }
}
