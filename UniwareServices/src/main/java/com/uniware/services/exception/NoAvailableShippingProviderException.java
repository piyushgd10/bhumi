/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 22, 2011
 *  @author singla
 */
package com.uniware.services.exception;

/**
 * @author singla
 */
public class NoAvailableShippingProviderException extends ServiceException {

    /**
     * 
     */
    private static final long serialVersionUID = -7908328032002615871L;

    /**
     * @param message
     */
    public NoAvailableShippingProviderException(String message) {
        super(message);
    }

}
