/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 16/04/14
 *  @author amit
 */

package com.uniware.services.exception;

public class InvalidContentNamespaceException extends ServiceException {

    private static final long serialVersionUID = -7909928032002615871L;

    public InvalidContentNamespaceException(String message) {
        super(message);
    }
}
