/**
 * Copyright 2017 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version 1.0, 24/07/17
 * @author aditya
 */
package com.uniware.services.exception;

public class InventoryAccuracyException extends Exception {

    public InventoryAccuracyException(String message) {
        super(message);
    }
}
