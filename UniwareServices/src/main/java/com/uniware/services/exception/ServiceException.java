/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 22, 2011
 *  @author singla
 */
package com.uniware.services.exception;

/**
 * @author singla
 */
public class ServiceException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = 7796035268300917250L;

    /**
     * @param message
     */
    public ServiceException(String message) {
        super(message);
    }

}
