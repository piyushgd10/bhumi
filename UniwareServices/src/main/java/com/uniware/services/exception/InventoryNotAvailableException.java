/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 22, 2011
 *  @author singla
 */
package com.uniware.services.exception;

/**
 * @author singla
 */
public class InventoryNotAvailableException extends ServiceException {

    /**
     * @param message
     */
    public InventoryNotAvailableException(String message) {
        super(message);
    }

    /**
     * 
     */
    private static final long serialVersionUID = 3993759713126880128L;

}
