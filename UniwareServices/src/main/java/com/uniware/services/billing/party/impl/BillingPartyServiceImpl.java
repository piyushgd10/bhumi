/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *  @version     1.0, 15-Feb-2012
 *  @author vibhu
 */
package com.uniware.services.billing.party.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.unifier.core.api.validation.ResponseCode;
import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.StringUtils;
import com.unifier.services.aspect.RollbackOnFailure;
import com.uniware.core.api.party.CreateBillingPartyRequest;
import com.uniware.core.api.party.CreateBillingPartyResponse;
import com.uniware.core.api.party.EditBillingPartyRequest;
import com.uniware.core.api.party.EditBillingPartyResponse;
import com.uniware.core.api.party.GetBillingPartyRequest;
import com.uniware.core.api.party.GetBillingPartyResponse;
import com.uniware.core.api.party.WsBillingParty;
import com.uniware.core.api.party.WsPartyAddress;
import com.uniware.core.api.party.WsPartyContact;
import com.uniware.core.api.party.dto.BillingPartyDTO;
import com.uniware.core.api.party.dto.BillingPartySearchDTO;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.entity.BillingParty;
import com.uniware.core.entity.Facility;
import com.uniware.core.entity.Party;
import com.uniware.core.entity.PartyAddressType;
import com.uniware.core.entity.Sequence;
import com.uniware.core.entity.Sequence.Level;
import com.uniware.core.entity.Sequence.ResetInterval;
import com.uniware.core.utils.UserContext;
import com.uniware.dao.billing.party.IBillingPartyDao;
import com.uniware.events.BillingPartyDisableEvent;
import com.uniware.services.billing.party.IBillingPartyService;
import com.uniware.services.common.ISequenceGenerator;
import com.uniware.services.invoice.IInvoiceService;
import com.uniware.services.party.IGenericPartyService;
import com.uniware.services.warehouse.IFacilityService;

@Service
public class BillingPartyServiceImpl implements IBillingPartyService, ApplicationEventPublisherAware{

    @Autowired
    private IGenericPartyService genericPartyService;

    @Autowired
    private IBillingPartyDao     billingPartyDao;

    @Autowired
    private ISequenceGenerator   sequenceGenerator;

    @Autowired
    private IFacilityService     facilityService;

    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    @Autowired
    private IInvoiceService invoiceService;

    @Override
    @Transactional
    @RollbackOnFailure
    public CreateBillingPartyResponse createBillingParty(CreateBillingPartyRequest request) {
        CreateBillingPartyResponse response = new CreateBillingPartyResponse();
        //TODO temporary, should be removed when all Party creation/editing are moved to new services
        if (request.getBillingParty() != null) {
            updateRequestBeforeValidation(request.getBillingParty());
        }
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            BillingParty billingParty = billingPartyDao.getBillingPartyByCode(request.getBillingParty().getCode());
            if (billingParty != null) {
                context.addError(WsResponseCode.DUPLICATE_BILLING_PARTY_CODE, "Billing Party Code already exists");
            } else {
                billingParty = new BillingParty();
                genericPartyService.prepareParty(billingParty, request.getBillingParty(), context, true);
                if (!context.hasErrors()) {
                    addInvoiceSequences(billingParty);
                    billingParty = billingPartyDao.createBillingParty(billingParty);
                    Sequence retailInvoiceSequence = sequenceGenerator.getSequenceByName(billingParty.getRetailInvoiceSequence(), null);
                    Sequence taxInvoiceSequence = sequenceGenerator.getSequenceByName(billingParty.getTaxInvoiceSequence(), null);
                    Sequence saleInvoiceSequence = sequenceGenerator.getSequenceByName(billingParty.getSaleInvoiceSequence(), null);
                    Sequence saleReturnInvoiceSequence = sequenceGenerator.getSequenceByName(billingParty.getSaleReturnInvoiceSequence(), null);
                    response.setBillingParty(new BillingPartyDTO(billingParty, retailInvoiceSequence, taxInvoiceSequence, saleInvoiceSequence, saleReturnInvoiceSequence));
                }
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        } else {
            response.setSuccessful(true);
        }
        return response;
    }

    public void addInvoiceSequences(BillingParty billingParty) {
        String retailInvoicePrefix = "R" + billingParty.getCode().toUpperCase();
        String retailInvoiceSequenceName = new StringBuilder().append(billingParty.getCode()).append("-").append(Sequence.Name.INVOICE.name()).toString();
        Sequence retailInvoiceSequence = new Sequence(UserContext.current().getTenant(), null, retailInvoiceSequenceName, Level.TENANT.name(), 0, 1, retailInvoicePrefix,
                Sequence.Name.INVOICE.padToLength(), ResetInterval.NEVER, DateUtils.getCurrentTime(), DateUtils.getCurrentTime());
        sequenceGenerator.createSequence(retailInvoiceSequence);
        billingParty.setRetailInvoiceSequence(retailInvoiceSequence.getName());

        String taxInvoicePrefix = "T" + billingParty.getCode().toUpperCase();
        if (taxInvoicePrefix.length() > 3) {
            taxInvoicePrefix = taxInvoicePrefix.substring(0, 3);
        }
        String taxInvoiceSequenceName = new StringBuilder().append(billingParty.getCode()).append("-").append(Sequence.Name.TAX_INVOICE.name()).toString();
        Sequence taxInvoiceSequence = new Sequence(UserContext.current().getTenant(), null, taxInvoiceSequenceName, Level.TENANT.name(), 0, 1, taxInvoicePrefix,
                Sequence.Name.TAX_INVOICE.padToLength(), ResetInterval.NEVER, DateUtils.getCurrentTime(), DateUtils.getCurrentTime());
        sequenceGenerator.createSequence(taxInvoiceSequence);
        billingParty.setTaxInvoiceSequence(taxInvoiceSequence.getName());

        String saleInvoicePrefix = "S" + billingParty.getCode().toUpperCase();
        if (saleInvoicePrefix.length() > 3) {
            saleInvoicePrefix = saleInvoicePrefix.substring(0, 3);
        }
        String saleInvoiceSequenceName = new StringBuilder().append(billingParty.getCode()).append("-").append(Sequence.Name.SALE_INVOICE.name()).toString();
        Sequence saleInvoiceSequence = new Sequence(UserContext.current().getTenant(), null, saleInvoiceSequenceName, Level.TENANT.name(), 0, 1, saleInvoicePrefix,
                Sequence.Name.SALE_INVOICE.padToLength(), ResetInterval.NEVER, DateUtils.getCurrentTime(), DateUtils.getCurrentTime());
        sequenceGenerator.createSequence(saleInvoiceSequence);
        billingParty.setSaleInvoiceSequence(saleInvoiceSequence.getName());

        String saleReturnInvoicePrefix = "SR" + billingParty.getCode().toUpperCase();
        if (saleReturnInvoicePrefix.length() > 4) {
            saleReturnInvoicePrefix = saleReturnInvoicePrefix.substring(0, 4);
        }
        String saleReturnInvoiceSequenceName = new StringBuilder().append(billingParty.getCode()).append("-").append(Sequence.Name.SALE_RETURN_INVOICE.name()).toString();
        Sequence saleReturnInvoiceSequence = new Sequence(UserContext.current().getTenant(), null, saleReturnInvoiceSequenceName, Level.TENANT.name(), 0, 1, saleReturnInvoicePrefix,
                Sequence.Name.SALE_RETURN_INVOICE.padToLength(), ResetInterval.NEVER, DateUtils.getCurrentTime(), DateUtils.getCurrentTime());
        sequenceGenerator.createSequence(saleReturnInvoiceSequence);
        billingParty.setSaleReturnInvoiceSequence(saleReturnInvoiceSequence.getName());
    }

    @Override
    @Transactional
    @RollbackOnFailure
    public EditBillingPartyResponse editBillingParty(EditBillingPartyRequest request) {
        EditBillingPartyResponse response = new EditBillingPartyResponse();
        boolean hasBeenDisabled = false;
        //temporary, should be removed when all Party creation/editing are moved to new services
        if (request.getBillingParty() != null) {
            updateRequestBeforeValidation(request.getBillingParty());
        }
        ValidationContext context = request.validate(false);
        if (!context.hasErrors()) {
            BillingParty billingParty = billingPartyDao.getBillingPartyByCode(request.getBillingParty().getCode());
            if (billingParty == null) {
                context.addError(WsResponseCode.INVALID_BILLING_PARTY_CODE, "Invalid Billing Party Code");
            } else {
                //check if the edit operation is a disable event or not
                if (billingParty.isEnabled() && !request.getBillingParty().getEnabled()) {
                    hasBeenDisabled = true;
                }
                genericPartyService.prepareParty(billingParty, request.getBillingParty(), context);
                if (!context.hasErrors()) {
                    billingParty = billingPartyDao.updateBillingParty(billingParty);
                    if (hasBeenDisabled) {
                        //publish the event to all the listeners
                        this.applicationEventPublisher.publishEvent(new BillingPartyDisableEvent(billingParty, billingParty.getName() + " disabled"));
                    }
                    try {
                        Sequence retailInvoiceSequence = sequenceGenerator.getSequenceByName(billingParty.getRetailInvoiceSequence(), null);
                        Sequence taxInvoiceSequence = sequenceGenerator.getSequenceByName(billingParty.getTaxInvoiceSequence(), null);

                        if (request.getRetailInvoiceSequence() != null) {
                            String newRetailPrefix = request.getRetailInvoiceSequence().getPrefix();
                            String newRetailNextYearPrefix = request.getRetailInvoiceSequence().getNextYearPrefix();
                            if (StringUtils.isNotBlank(newRetailPrefix) && newRetailPrefix.equalsIgnoreCase(taxInvoiceSequence.getPrefix())) {
                                context.addError(WsResponseCode.DUPLICATE_SEQUENCE_PREFIX, "Retail Invoice and Tax Invoice sequence prefix can not be same");
                            } else if(StringUtils.isNotBlank(newRetailNextYearPrefix) && newRetailNextYearPrefix.equalsIgnoreCase(taxInvoiceSequence.getNextYearPrefix())) {
                                context.addError(WsResponseCode.DUPLICATE_SEQUENCE_PREFIX, "Retail Invoice and Tax Invoice sequence next year prefix can not be same");
                            } else {
                                Party party = null;
                                if(StringUtils.isNotBlank(request.getRetailInvoiceSequence().getPrefix())) {
                                    party = findPartyWithSameSequencePrefix(request.getRetailInvoiceSequence().getPrefix(), retailInvoiceSequence.getPrefix());
                                }
                                if (party == null) {
                                    facilityService.updateSequences(billingParty.getRetailInvoiceSequence(), retailInvoiceSequence, request.getRetailInvoiceSequence(), null, context);
                                } else {
                                    context.addError(WsResponseCode.DUPLICATE_SEQUENCE_PREFIX, "Sequence Prefix already exists for Facility or Billing Party : " + party.getName());
                                }
                            }
                        }
                        if (request.getTaxInvoiceSequence() != null) {
                            String newTaxPrefix = request.getTaxInvoiceSequence().getPrefix();
                            String newTaxNextYearPrefix = request.getTaxInvoiceSequence().getNextYearPrefix();
                            if (StringUtils.isNotBlank(newTaxPrefix) && newTaxPrefix.equalsIgnoreCase(retailInvoiceSequence.getPrefix())) {
                                context.addError(WsResponseCode.DUPLICATE_SEQUENCE_PREFIX, "Retail Invoice and Tax Invoice sequence prefix can not be same");
                            } else if(StringUtils.isNotBlank(newTaxNextYearPrefix) && newTaxNextYearPrefix.equalsIgnoreCase(retailInvoiceSequence.getNextYearPrefix())) {
                                context.addError(WsResponseCode.DUPLICATE_SEQUENCE_PREFIX, "Retail Invoice and Tax Invoice sequence next year prefix can not be same");
                            } else {
                                Party party = null;
                                if(StringUtils.isNotBlank(request.getTaxInvoiceSequence().getPrefix())) {
                                    party = findPartyWithSameSequencePrefix(request.getTaxInvoiceSequence().getPrefix(), taxInvoiceSequence.getPrefix());
                                }
                                if (party == null) {
                                    facilityService.updateSequences(billingParty.getTaxInvoiceSequence(), taxInvoiceSequence, request.getTaxInvoiceSequence(), null, context);
                                } else {
                                    context.addError(WsResponseCode.DUPLICATE_SEQUENCE_PREFIX, "Sequence Prefix already exists for Party : " + party.getName());
                                }
                            }
                        }
                        response.setBillingParty(new BillingPartyDTO(billingParty, retailInvoiceSequence, taxInvoiceSequence));

                    } catch (IllegalArgumentException e) {
                        context.addError(ResponseCode.INVALID_FORMAT, e.getMessage());
                    }
                }
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
            response.setSuccessful(false);
        } else {
            response.setSuccessful(true);
        }
        return response;
    }

    @Override
    @Transactional(readOnly = true)
    public GetBillingPartyResponse getBillingPartyByCode(GetBillingPartyRequest request) {
        GetBillingPartyResponse response = new GetBillingPartyResponse();
        ValidationContext context = request.validate();
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        } else {
            BillingParty billingParty = billingPartyDao.getBillingPartyByCode(request.getCode());
            if (billingParty == null) {
                context.addError(WsResponseCode.INVALID_BILLING_PARTY_CODE, "Invalid Billing Party Code");
            } else {
                Sequence retailInvoiceSequence = sequenceGenerator.getSequenceByName(billingParty.getRetailInvoiceSequence(), null);
                Sequence taxInvoiceSequence = sequenceGenerator.getSequenceByName(billingParty.getTaxInvoiceSequence(), null);
                response.setBillingParty(new BillingPartyDTO(billingParty, retailInvoiceSequence, taxInvoiceSequence));
                response.setSuccessful(true);
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    private Party findPartyWithSameSequencePrefix(String newPrefix, String skipThisPrefix) {
        List<Facility> facilities = facilityService.getAllFacilities();
        for (Facility fac : facilities) {
            Sequence rSequence = sequenceGenerator.getSequenceByName(Sequence.Name.INVOICE.name(), fac.getId());
            Sequence tSequence = sequenceGenerator.getSequenceByName(Sequence.Name.TAX_INVOICE.name(), fac.getId());
            if (newPrefix.equalsIgnoreCase(rSequence.getPrefix()) || newPrefix.equalsIgnoreCase(tSequence.getPrefix())) {
                return fac;
            }
        }
        for (BillingParty bp : getAllBillingParties()) {
            Sequence rSequence = sequenceGenerator.getSequenceByName(bp.getRetailInvoiceSequence(), null);
            Sequence tSequence = sequenceGenerator.getSequenceByName(bp.getTaxInvoiceSequence(), null);
            if (newPrefix.equalsIgnoreCase(rSequence.getPrefix()) || newPrefix.equalsIgnoreCase(tSequence.getPrefix())) {
                if (!newPrefix.equalsIgnoreCase(skipThisPrefix)) {
                    return bp;
                }
            }
        }
        return null;
    }

    @Override
    @Transactional
    public BillingParty getBillingPartyByCode(String code) {
        return billingPartyDao.getBillingPartyByCode(code);
    }

    @Override
    @Transactional
    public BillingParty getBillingPartyById(Integer id) {
        return billingPartyDao.getBillingPartyById(id);
    }

    @Override
    @Transactional
    public List<BillingPartySearchDTO> lookupBillingParty(String keyword) {
        List<BillingPartySearchDTO> searchDTOs = new ArrayList<BillingPartySearchDTO>();
        for (BillingParty party : billingPartyDao.searchBillingParty(keyword)) {
            BillingPartySearchDTO billingPartyDTO = new BillingPartySearchDTO();
            billingPartyDTO.setId(party.getId());
            billingPartyDTO.setCode(party.getCode());
            billingPartyDTO.setName(party.getName());
            searchDTOs.add(billingPartyDTO);
        }
        return searchDTOs;
    }

    /**
     * adds partyCode, addressType to addresses before validating request, temporary, should be removed when all Party
     * creation/editing are moved to new services
     *
     * @param billingParty
     */
    private void updateRequestBeforeValidation(WsBillingParty billingParty) {
        if (billingParty.getCode() != null) {
            WsPartyAddress bAddress = billingParty.getBillingAddress();
            WsPartyAddress sAddress = billingParty.getShippingAddress();
            if (bAddress != null) {
                bAddress.setPartyCode(billingParty.getCode());
                bAddress.setAddressType(PartyAddressType.Code.BILLING.name());
            }
            if (sAddress != null) {
                sAddress.setPartyCode(billingParty.getCode());
                sAddress.setAddressType(PartyAddressType.Code.SHIPPING.name());
            }
            if (billingParty.getPartyContacts() != null) {
                for (WsPartyContact contact : billingParty.getPartyContacts()) {
                    contact.setPartyCode(billingParty.getCode());
                }
            }
        }
    }

    @Override
    @Transactional
    public List<BillingParty> getAllBillingParties() {
        return billingPartyDao.getAllBillingParties();
    }

    @Override
    @Transactional
    public List<BillingParty> getAllBillingPartiesWithGstNumber() {
        return billingPartyDao.getAllBillingPartiesWithGstNumber();
    }

    @Override public void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
        this.applicationEventPublisher = applicationEventPublisher;
    }

    @Override
    public boolean checkIfValidBillingPartySequence(BillingParty billingParty, String sequenceName) {
        return StringUtils.isNotBlank(sequenceName) && StringUtils.equalsAny(sequenceName, billingParty.getRetailInvoiceSequence(), billingParty.getTaxInvoiceSequence(), billingParty.getSaleInvoiceSequence(), billingParty.getSaleReturnInvoiceSequence());
    }
}