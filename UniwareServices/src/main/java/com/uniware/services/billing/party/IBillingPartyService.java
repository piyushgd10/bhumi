/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 15-Feb-2012
 *  @author vibhu
 */
package com.uniware.services.billing.party;

import java.util.List;

import com.uniware.core.api.party.CreateBillingPartyRequest;
import com.uniware.core.api.party.CreateBillingPartyResponse;
import com.uniware.core.api.party.EditBillingPartyRequest;
import com.uniware.core.api.party.EditBillingPartyResponse;
import com.uniware.core.api.party.GetBillingPartyRequest;
import com.uniware.core.api.party.GetBillingPartyResponse;
import com.uniware.core.api.party.dto.BillingPartySearchDTO;
import com.uniware.core.entity.BillingParty;
import org.springframework.transaction.annotation.Transactional;

public interface IBillingPartyService {

    CreateBillingPartyResponse createBillingParty(CreateBillingPartyRequest request);

    EditBillingPartyResponse editBillingParty(EditBillingPartyRequest request);

    GetBillingPartyResponse getBillingPartyByCode(GetBillingPartyRequest request);

    BillingParty getBillingPartyByCode(String code);

    BillingParty getBillingPartyById(Integer id);

    List<BillingPartySearchDTO> lookupBillingParty(String keyword);

    List<BillingParty> getAllBillingParties();

    List<BillingParty> getAllBillingPartiesWithGstNumber();

    boolean checkIfValidBillingPartySequence(BillingParty billingParty, String name);
}
