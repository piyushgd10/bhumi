/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 11-May-2015
 *  @author parijat
 */
package com.uniware.services.location;

import com.uniware.core.api.location.SyncPincodeLatLongRequest;
import com.uniware.core.api.location.SyncPincodeLatLongResponse;

/**
 * @author parijat
 *
 */
public interface IPincodeLatLongSyncService {

    SyncPincodeLatLongResponse syncPincodesLatLong(SyncPincodeLatLongRequest request);

}
