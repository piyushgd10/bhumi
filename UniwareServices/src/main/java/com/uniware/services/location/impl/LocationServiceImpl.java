/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 11-May-2015
 *  @author parijat
 */
package com.uniware.services.location.impl;

import com.unifier.core.api.validation.ValidationContext;
import com.uniware.core.api.location.GetPincodesWithinRangeRequest;
import com.uniware.core.api.location.GetPincodesWithinRangeResponse;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.entity.Location;
import com.uniware.dao.location.ILocationMao;
import com.uniware.services.location.ILocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.geo.Distance;
import org.springframework.data.mongodb.core.geo.GeoResult;
import org.springframework.data.mongodb.core.geo.GeoResults;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author parijat
 */
@Service("locationService")
public class LocationServiceImpl implements ILocationService {

    @Autowired
    private ILocationMao locationMao;

    @Override
    public Location getLocationByPincode(String pincode) {
        return locationMao.getLocationbyPincode(pincode);
    }

    //    @Override
    //    public void saveLocation(Location location) {
    //        locationMao.save(location);
    //    }

    @Override
    public List<Location> getAllLocations() {
        return locationMao.getAllLocations();
    }

    @Override
    public List<Location> getLocations(int skipCount, int pageSize) {
        return locationMao.getLocations(skipCount, pageSize);
    }

    @Override
    public long getTotalCount() {
        return locationMao.getTotalCount();
    }

    @Override
    public GetPincodesWithinRangeResponse getAllPincodesWithinRange(GetPincodesWithinRangeRequest request) {
        GetPincodesWithinRangeResponse response = new GetPincodesWithinRangeResponse();
        ValidationContext context = request.validate();
        List<Location> nearByLocations = new ArrayList<>();
        if (!context.hasErrors()) {
            Location origin = getLocationByPincode(request.getPincode());
            if (origin == null) {
                context.addError(WsResponseCode.INVALID_ADDRESS_DETAIL, "Invalid pincode " + request.getPincode());
            } else {
                GeoResults<Location> nearByGeoLocations = locationMao.getAllPincodesWithinRange(origin, request.getDistance().doubleValue());
                Iterator<GeoResult<Location>> iter = nearByGeoLocations.iterator();
                while (iter.hasNext()) {
                    GeoResult<Location> geoLocation = iter.next();
                    nearByLocations.add(geoLocation.getContent());
                }
                response.setLocations(nearByLocations);
                response.setSuccessful(true);
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    public BigDecimal getDistanceBetweenPincodes(String originPincode, String destinationPincode) {
        Location location = getLocationByPincode(originPincode);
        if (location == null) {
            return BigDecimal.ZERO;
        } else {
            Distance distance = locationMao.getDistanceBetweenLocationAndPincode(location.getPosition(), destinationPincode);
            if (distance == null) {
                return BigDecimal.ZERO;
            }
            return new BigDecimal(distance.getValue()).setScale(3, RoundingMode.CEILING);
        }
    }

}
