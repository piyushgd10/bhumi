/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 11-May-2015
 *  @author parijat
 */
package com.uniware.services.location.impl;

import java.util.Iterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.transport.http.HttpSender;
import com.unifier.core.utils.JsonUtils;
import com.unifier.services.aspect.MarkDirty;
import com.uniware.core.api.location.SyncPincodeLatLongRequest;
import com.uniware.core.api.location.SyncPincodeLatLongResponse;
import com.uniware.core.cache.LocationCache;
import com.uniware.services.location.ILocationService;
import com.uniware.services.location.IPincodeLatLongSyncService;

/**
 * @author parijat
 */
@Service("pincodeLatLongSyncService")
public class PincodeLatLongSyncServiceImpl implements IPincodeLatLongSyncService {

    private static final Logger     LOG                      = LoggerFactory.getLogger(PincodeLatLongSyncServiceImpl.class);

    public static final String      WEB_GOOGLE_CLIENT_ID     = "702039528608-q6qe7t2oempfvvh0smjju9m0ih20m227.apps.googleusercontent.com";
    public static final String      GOOGLE_SECRET            = "9R-YSoP5cd4uBnbWoLkf9itY";
    public static final String      GOOGLE_API_KEY           = "AIzaSyB2u19S2a-gQS5dXAu0r3_HidUCmNGV1PA";
    public static final String      GOOGLE_MAP_API__BASE_URL = "https://maps.googleapis.com/maps/api";

    @Autowired
    private ILocationService        locationService;

    private static final HttpSender httpSender               = new HttpSender(true);

    @Override
    @MarkDirty(values = {LocationCache.class})
    public SyncPincodeLatLongResponse syncPincodesLatLong(SyncPincodeLatLongRequest request) {
        SyncPincodeLatLongResponse response = new SyncPincodeLatLongResponse();
        ValidationContext context = request.validate();
//        if (!context.hasErrors()) {
//            if (request.getLocations().size() > 0) {
//                response.setTotalCount(request.getLocations().size());
//                Map<String, String> errorMessages = new HashMap<>();
//                int successCount = 0;
//                int failCount = 0;
//                for (Location location : request.getLocations()) {
//                    Map<String, String> params = new HashMap<String, String>();
//                    params.put("components", "postal_code:" + location.getPincode() + "|country:IN");
//                    params.put("key", GOOGLE_API_KEY);
//                    //                    params.put("client", WEB_GOOGLE_CLIENT_ID);
//                    //                    params.put(
//                    //                            "signature",
//                    //                            EncryptionUtils.createBase64EncodedHMACSHA1(
//                    //                                    EncryptionUtils.base64UrlDecode("4e583ec9a2d41746955f3ef58f806d9f759b1113"),
//                    //                                    "/maps/api/geocode/json?components=postal_code%3A" + location.getPincode()
//                    //                                            + "%7Ccountry%3AIN&client=702039528608-q6qe7t2oempfvvh0smjju9m0ih20m227.apps.googleusercontent.com").replace("/", "_").replace("+", "_"));
//                    //                    //                    params.put("key", GOOGLE_API_KEY);
//                    try {
//                        String httpResponse = httpSender.executeGet(GOOGLE_MAP_API__BASE_URL + "/geocode/json", params);
//                        LOG.info(location.getPincode() + " http response :" + httpResponse);
//                        double[] position = getLatLongFromJson(httpResponse);
//                        if (position.length == 2) {
//                            location.setPosition(position);
//                        }
//                        locationService.saveLocation(location);
//                        response.setSuccessful(true);
//                        successCount++;
//                    } catch (HttpTransportException e) {
//                        response.setMessage("Error in calling goole api " + e.getMessage());
//                        LOG.error("Error in calling goole api {} for pincode {}", e.getMessage(), location.getPincode());
//                        failCount++;
//                        errorMessages.put(location.getPincode(), e.getMessage());
//                    } catch (InterruptedException e) {
//                        response.setMessage("Error in waiting for google API quota limit to expire " + e.getMessage());
//                        LOG.error("Error {} in waiting for google API quota limit to expire for pincode {}", e.getMessage(), location.getPincode());
//                        failCount++;
//                        errorMessages.put(location.getPincode(), e.getMessage());
//                    }
//                }
//                response.setSuccessCount(successCount);
//                response.setFailCount(failCount);
//                response.setErrorMessages(errorMessages);
//            }
//
//        }
        response.addErrors(context.getErrors());
        return response;
    }

    private double[] getLatLongFromJson(String httpResponse) throws InterruptedException {
        JsonElement json = (JsonElement) JsonUtils.stringToJson(httpResponse);
        double[] latLong = new double[2];
        if (!json.isJsonNull()) {
            JsonObject jObj = json.getAsJsonObject();
            if (jObj.get("status").getAsString().equalsIgnoreCase("OK")) {
                @SuppressWarnings("rawtypes")
                Iterator iter = jObj.get("results").getAsJsonArray().iterator();
                JsonObject jsonObj = (JsonObject) iter.next();
                JsonObject latlongJson = jsonObj.get("geometry").getAsJsonObject().get("location").getAsJsonObject();
                latLong[1] = latlongJson.get("lat").getAsDouble();
                latLong[0] = latlongJson.get("lng").getAsDouble();
            } else if (jObj.get("status").getAsString() == "OVER_QUERY_LIMIT") {
                Thread.sleep(1000);
            }
        }
        return latLong;
    }

    public static void main(String[] args) {
        String json = "{   \"results\" : [      {         \"address_components\" : [            {               \"long_name\" : \"201301\",               \"short_name\" : \"201301\",               \"types\" : [ \"postal_code\" ]            },            {               \"long_name\" : \"India\",               \"short_name\" : \"IN\",               \"types\" : [ \"country\", \"political\" ]            }         ],         \"formatted_address\" : \"201301, India\",         \"geometry\" : {            \"bounds\" : {               \"northeast\" : {                  \"lat\" : 28.6029008,                  \"lng\" : 77.373091               },               \"southwest\" : {                  \"lat\" : 28.5453081,                  \"lng\" : 77.2961997               }            },            \"location\" : {               \"lat\" : 28.5821195,               \"lng\" : 77.3266991            },            \"location_type\" : \"APPROXIMATE\",            \"viewport\" : {               \"northeast\" : {                  \"lat\" : 28.6029008,                  \"lng\" : 77.373091               },               \"southwest\" : {                  \"lat\" : 28.5453081,                  \"lng\" : 77.2961997               }            }         },         \"place_id\" : \"ChIJKQa0eU7kDDkRuol-quF6nFs\",         \"types\" : [ \"postal_code\" ]      }   ],   \"status\" : \"OK\"}";

        PincodeLatLongSyncServiceImpl impl = new PincodeLatLongSyncServiceImpl();
        try {
            impl.getLatLongFromJson(json);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
