/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 11-May-2015
 *  @author parijat
 */
package com.uniware.services.location;

import com.uniware.core.api.location.GetPincodesWithinRangeRequest;
import com.uniware.core.api.location.GetPincodesWithinRangeResponse;
import com.uniware.core.entity.Location;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author parijat
 *
 */
public interface ILocationService {

    Location getLocationByPincode(String pincode);
    
//    void saveLocation(Location location);

    List<Location> getAllLocations();

    List<Location> getLocations(int start, int pageSize);

    long getTotalCount();

    BigDecimal getDistanceBetweenPincodes(String originPincode, String destinationPincode);

    GetPincodesWithinRangeResponse getAllPincodesWithinRange(GetPincodesWithinRangeRequest request);

}
