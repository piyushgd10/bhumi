/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 04-Feb-2014
 *  @author karunsingla
 */
package com.uniware.services.customizations.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.uniware.core.entity.Item;
import com.uniware.core.entity.SaleOrderItem;

@SuppressWarnings("unchecked")
@Repository
public class LenskartDaoImpl {

    @Autowired
    private SessionFactory sessionFactory;

    public List<SaleOrderItem> getSaleOrderItemsToAllocate() {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select soi from SaleOrderItem soi where soi.statusCode = :statusCode and soi.item.id is null order by soi.created");
        query.setParameter("statusCode", SaleOrderItem.StatusCode.FULFILLABLE.name());
        return query.list();
    }

    public Item getItemForAllocation(Integer itemTypeId) {
        Query query = sessionFactory.getCurrentSession().createQuery("select i from Item i where i.statusCode = :statusCode and i.itemType.id = :itemTypeId");
        query.setParameter("statusCode", Item.StatusCode.GOOD_INVENTORY.name());
        query.setParameter("itemTypeId", itemTypeId);
        query.setMaxResults(1);
        return (Item) query.uniqueResult();
    }

}
