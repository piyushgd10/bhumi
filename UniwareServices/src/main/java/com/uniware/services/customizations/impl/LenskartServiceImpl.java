/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 03-Feb-2014
 *  @author karunsingla
 */
package com.uniware.services.customizations.impl;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.api.validation.WsError;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.utils.CollectionUtils;
import com.unifier.core.utils.StringUtils;
import com.unifier.services.aspect.RollbackOnFailure;
import com.unifier.services.utils.CustomFieldUtils;
import com.uniware.core.api.common.GenericServiceResponse;
import com.uniware.core.api.customizations.MarkItemDamagedOutboundQCRequest;
import com.uniware.core.api.customizations.MarkItemDamagedOutboundQCResponse;
import com.uniware.core.api.invoice.CreateShippingPackageInvoiceRequest;
import com.uniware.core.api.invoice.CreateShippingPackageInvoiceResponse;
import com.uniware.core.api.shipping.AllocateShippingProviderRequest;
import com.uniware.core.api.shipping.ReassessShippingPackageRequest;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.entity.InflowReceipt;
import com.uniware.core.entity.InflowReceiptItem;
import com.uniware.core.entity.Item;
import com.uniware.core.entity.ItemTypeInventory;
import com.uniware.core.entity.SaleOrder;
import com.uniware.core.entity.SaleOrderItem;
import com.uniware.core.entity.ShippingPackage;
import com.uniware.core.locking.Namespace;
import com.uniware.core.locking.annotation.Lock;
import com.uniware.core.locking.annotation.Locks;
import com.uniware.dao.saleorder.ISaleOrderDao;
import com.uniware.services.configuration.TenantSystemConfiguration;
import com.uniware.services.exception.InventoryNotAvailableException;
import com.uniware.services.inflow.IInflowService;
import com.uniware.services.inventory.IInventoryService;
import com.uniware.services.invoice.IInvoiceService;
import com.uniware.services.saleorder.ISaleOrderService;
import com.uniware.services.shipping.IShippingInvoiceService;
import com.uniware.services.shipping.IShippingProviderService;
import com.uniware.services.shipping.IShippingService;

@Service("lenskartService")
public class LenskartServiceImpl {

    @Autowired
    private LenskartDaoImpl          lenskartDao;

    @Autowired
    private ISaleOrderDao            saleOrderDao;

    @Autowired
    private ISaleOrderService        saleOrderService;

    @Autowired
    private IShippingService         shippingService;

    @Autowired
    private IShippingProviderService shippingProviderService;

    @Autowired
    private IInventoryService        inventoryService;

    @Autowired
    private IInflowService           inflowService;

    @Autowired
    private IInvoiceService          invoiceService;

    @Autowired
    private IShippingInvoiceService  iShippingInvoiceService;

    @Locks({ @Lock(ns = Namespace.SALE_ORDER, key = "#{#args[0].saleOrder.code}") })
    @Transactional
    public void allocateNonTraceableItem(SaleOrderItem saleOrderItem) {
        SaleOrder saleOrder = saleOrderDao.getSaleOrderForUpdate(saleOrderItem.getSaleOrder().getId());
        SaleOrderItem itemToAllocate = null;
        for (SaleOrderItem soi : saleOrder.getSaleOrderItems()) {
            if (soi.getId().equals(saleOrderItem.getId())) {
                itemToAllocate = soi;
            }
        }
        if (itemToAllocate != null && SaleOrderItem.StatusCode.FULFILLABLE.name().equals(itemToAllocate.getStatusCode()) && itemToAllocate.getItem() == null) {
            Item item = lenskartDao.getItemForAllocation(itemToAllocate.getItemType().getId());
            if (item != null) {
                itemToAllocate.setItem(item);
                item.setStatusCode(Item.StatusCode.ALLOCATED.name());
            }
        }
    }

    @Locks({ @Lock(ns = Namespace.SALE_ORDER, key = "#{#args[0].saleOrderCode}") })
    @Transactional
    public MarkItemDamagedOutboundQCResponse markItemDamagedOutboundQC(MarkItemDamagedOutboundQCRequest request) {
        MarkItemDamagedOutboundQCResponse response = new MarkItemDamagedOutboundQCResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            SaleOrder saleOrder = saleOrderService.getSaleOrderForUpdate(request.getSaleOrderCode());
            ShippingPackage shippingPackage = shippingService.getShippingPackageByCode(request.getShippingPackageCode());
            if (saleOrder == null) {
                context.addError(WsResponseCode.INVALID_SALE_ORDER_CODE, "Invalid sale order code");
            } else if (shippingPackage == null || !shippingPackage.getSaleOrder().getId().equals(saleOrder.getId())) {
                context.addError(WsResponseCode.INVALID_SHIPPING_PACKAGE_CODE, "Invalid shipping package code");
            } else if (!StringUtils.equalsAny(shippingPackage.getStatusCode(), ShippingPackage.StatusCode.PICKED.name(), ShippingPackage.StatusCode.CUSTOMIZATION_COMPLETE.name(),
                    ShippingPackage.StatusCode.PENDING_CUSTOMIZATION.name())) {
                context.addError(WsResponseCode.INVALID_SHIPPING_PACKAGE_CODE, "Invalid shipping package code");
            }
            if (!context.hasErrors()) {
                SaleOrderItem saleOrderItem = null;
                for (SaleOrderItem soi : shippingPackage.getSaleOrderItems()) {
                    if (soi.getItem() != null && soi.getItem().getCode().equals(request.getItemCode())) {
                        saleOrderItem = soi;
                        break;
                    }
                }
                if (saleOrderItem == null) {
                    context.addError(WsResponseCode.INVALID_ITEM_CODE, "Item code is invalid or not assigned to given package");
                } else if (!SaleOrderItem.StatusCode.FULFILLABLE.name().equals(saleOrderItem.getStatusCode())) {
                    context.addError(WsResponseCode.INVALID_SALE_ORDER_ITEM_STATE, "Sale order item not in FULFILLABLE state");
                } else {
                    Item item = saleOrderItem.getItem();
                    ItemTypeInventory markedItemTypeInventory = saleOrderItem.getItemTypeInventory();
                    if (request.isFixedPacket() || !shippingPackage.isSplittable()) {
                        try {
                            ItemTypeInventory itemTypeInventory = inventoryService.blockInventoryForLenskartAndJabongg(saleOrderItem.getItemType());
                            saleOrderItem.setItemTypeInventory(itemTypeInventory);
                        } catch (InventoryNotAvailableException e) {
                            Iterator<SaleOrderItem> iterator = shippingPackage.getSaleOrderItems().iterator();
                            while (iterator.hasNext()) {
                                SaleOrderItem soi = iterator.next();
                                if (soi.equals(saleOrderItem)) {
                                    saleOrderItem.setShippingPackage(null);
                                    saleOrderItem.setItemTypeInventory(null);
                                    saleOrderItem.setStatusCode(SaleOrderItem.StatusCode.CREATED.name());
                                } else {
                                    soi.setShippingPackage(null);
                                }
                                if (request.isFixedPacket()) {
                                    soi.setPacketNumber(1);
                                }
                                iterator.remove();
                            }
                        }
                    } else {
                        saleOrderItem.setShippingPackage(null);
                        saleOrderItem.setItemTypeInventory(null);
                        saleOrderItem.setStatusCode(SaleOrderItem.StatusCode.CREATED.name());
                        shippingPackage.getSaleOrderItems().remove(saleOrderItem);
                    }
                    if (!context.hasErrors()) {
                        inventoryService.commitBlockedInventory(markedItemTypeInventory.getId(), 1);
                        inventoryService.addInventory(saleOrderItem.getItemType(), ItemTypeInventory.Type.BAD_INVENTORY, item.getAgeingStartDate(), markedItemTypeInventory.getShelf(), 1);
                        item.setStatusCode(Item.StatusCode.BAD_INVENTORY.name());
                        item.setInventoryType(ItemTypeInventory.Type.BAD_INVENTORY);
                        item.setRejectionReason(request.getRejectionReason());
                        saleOrderItem.setItem(null);
                        if (request.getShippingPackageCustomFieldValues() != null) {
                            CustomFieldUtils.setCustomFieldValues(shippingPackage,
                                    CustomFieldUtils.getCustomFieldValues(context, ShippingPackage.class.getName(), request.getShippingPackageCustomFieldValues()), false);
                        }
                        if (request.getSaleOrderCustomFields() != null) {
                            CustomFieldUtils.setCustomFieldValues(saleOrder,
                                    CustomFieldUtils.getCustomFieldValues(context, SaleOrder.class.getName(), request.getSaleOrderCustomFields()), false);
                        }
                        ReassessShippingPackageRequest packageRequest = new ReassessShippingPackageRequest();
                        packageRequest.setShippingPackageCode(shippingPackage.getCode());
                        shippingPackage = shippingService.reassessShippingPackage(packageRequest).getShippingPackage();
                        if (!ShippingPackage.StatusCode.CANCELLED.name().equals(shippingPackage.getStatusCode())) {
                            shippingPackage.setStatusCode(ShippingPackage.StatusCode.PICKED.name());
                        }
                        response.setSuccessful(true);
                    }
                }
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    public GenericServiceResponse processSaleOrder(String inflowReceiptCode, String saleOrderCode) {
        if (StringUtils.isBlank(saleOrderCode)) {
            return updateInflowReceiptItemStatus(inflowReceiptCode);
        } else {
            return processSaleOrderInternal(inflowReceiptCode, saleOrderCode);
        }
    }

    @Locks({ @Lock(ns = Namespace.LENSKART_DUAL_COMPANY, key = "LENSKART_DUAL_COMPANY"), @Lock(ns = Namespace.SALE_ORDER, key = "#{#args[1]}") })
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @RollbackOnFailure
    public GenericServiceResponse processSaleOrderInternal(String inflowReceiptCode, String saleOrderCode) {
        GenericServiceResponse response = new GenericServiceResponse();
        SaleOrder saleOrder = saleOrderDao.getSaleOrderForUpdate(saleOrderCode);
        if (!SaleOrder.StatusCode.COMPLETE.name().equals(saleOrder.getStatusCode())) {
            InflowReceipt inflowReceipt = inflowService.getInflowReceiptByCode(inflowReceiptCode);
            Map<Integer, Queue<Item>> itemTypeIdToItemCodes = new HashMap<>();
            for (InflowReceiptItem inflowReceiptItem : inflowReceipt.getInflowReceiptItems()) {
                Queue<Item> itemCodes = new LinkedList<>();
                for (Item item : inflowService.getItemsByReceiptItemId(inflowReceiptItem.getId())) {
                    if (ItemTypeInventory.Type.GOOD_INVENTORY.equals(item.getInventoryType())) {
                        itemCodes.add(item);
                    } else {
                        item.setStatusCode(item.getInventoryType().name());
                    }
                }
                itemTypeIdToItemCodes.put(inflowReceiptItem.getItemType().getId(), itemCodes);
                inflowReceiptItem.setStatusCode(InflowReceiptItem.StatusCode.COMPLETE.name());
            }
            for (SaleOrderItem saleOrderItem : saleOrder.getSaleOrderItems()) {
                saleOrderItem.setStatusCode(SaleOrderItem.StatusCode.FULFILLABLE.name());
                Item item = itemTypeIdToItemCodes.get(saleOrderItem.getItemType().getId()).poll();
                if (item != null) {
                    saleOrderItem.setItem(item);
                    item.setStatusCode(Item.StatusCode.ALLOCATED.name());
                } else {
                    response.addError(new WsError("Item not available for allocation"));
                }
            }
            ShippingPackage shippingPackage = shippingService.createShippingPackage(CollectionUtils.asList(saleOrder.getSaleOrderItems()), false);
            shippingPackage.setRequiresCustomization(false);
            shippingPackage.setStatusCode(ShippingPackage.StatusCode.PICKED.name());
            CreateShippingPackageInvoiceRequest createShippingPackageInvoiceRequest = new CreateShippingPackageInvoiceRequest();
            createShippingPackageInvoiceRequest.setUserId(ConfigurationManager.getInstance().getConfiguration(TenantSystemConfiguration.class).getSystemUser().getId());
            createShippingPackageInvoiceRequest.setShippingPackageCode(shippingPackage.getCode());
            CreateShippingPackageInvoiceResponse createShippingPackageInvoiceResponse = iShippingInvoiceService.createShippingPackageInvoice(createShippingPackageInvoiceRequest);
            response.addErrors(createShippingPackageInvoiceResponse.getErrors());
            if (!response.hasErrors()) {
                AllocateShippingProviderRequest aspRequest = new AllocateShippingProviderRequest();
                aspRequest.setShippingPackageCode(shippingPackage.getCode());
                response.addErrors(shippingProviderService.allocateShippingProvider(aspRequest).getErrors());
                if (!response.hasErrors()) {
                    shippingPackage = shippingService.getShippingPackageByCode(shippingPackage.getCode());
                    shippingPackage.setStatusCode(ShippingPackage.StatusCode.DELIVERED.name());
                    for (SaleOrderItem saleOrderItem : saleOrder.getSaleOrderItems()) {
                        saleOrderItem.setStatusCode(SaleOrderItem.StatusCode.DELIVERED.name());
                    }
                    response.setSuccessful(true);
                }
            }
        } else {
            response.setSuccessful(true);
        }
        return response;
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @RollbackOnFailure
    private GenericServiceResponse updateInflowReceiptItemStatus(String inflowReceiptCode) {
        GenericServiceResponse response = new GenericServiceResponse();
        InflowReceipt inflowReceipt = inflowService.getInflowReceiptByCode(inflowReceiptCode);
        for (InflowReceiptItem inflowReceiptItem : inflowReceipt.getInflowReceiptItems()) {
            for (Item item : inflowService.getItemsByReceiptItemId(inflowReceiptItem.getId())) {
                item.setStatusCode(item.getInventoryType().name());
            }
        }
        return response;
    }

    @Locks({ @Lock(ns = Namespace.LENSKART_DUAL_COMPANY, key = "LENSKART_DUAL_COMPANY") })
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public GenericServiceResponse markInflowReceiptComplete(String inflowReceiptCode) {
        GenericServiceResponse response = new GenericServiceResponse();
        InflowReceipt inflowReceipt = inflowService.getInflowReceiptByCode(inflowReceiptCode);
        inflowReceipt.setStatusCode(InflowReceipt.StatusCode.COMPLETE.name());
        response.setSuccessful(true);
        return response;
    }
}
