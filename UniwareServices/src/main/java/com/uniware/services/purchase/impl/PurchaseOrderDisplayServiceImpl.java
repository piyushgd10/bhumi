/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, May 8, 2015
 *  @author harsh
 */
package com.uniware.services.purchase.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unifier.core.api.validation.ValidationContext;
import com.uniware.core.api.purchase.GetPurchaseOrderRequest;
import com.uniware.core.api.purchase.GetPurchaseOrderResponse;
import com.uniware.core.api.purchase.PurchaseOrderItemDTO;
import com.uniware.core.api.purchaseOrder.display.GetPurchaseOrderSummaryRequest;
import com.uniware.core.api.purchaseOrder.display.GetPurchaseOrderSummaryResponse;
import com.uniware.core.api.purchaseOrder.display.GetPurchaseOrderSummaryResponse.PurchaseOrderSummaryDTO;
import com.uniware.services.purchase.IPurchaseOrderDisplayService;
import com.uniware.services.purchase.IPurchaseService;

/**
 * @author harsh
 */
@Service("purchaseOrderDisplayService")
public class PurchaseOrderDisplayServiceImpl implements IPurchaseOrderDisplayService {

    @Autowired
    IPurchaseService purchaseService;

    @Override
    public GetPurchaseOrderSummaryResponse getPurchaseOrderSummary(GetPurchaseOrderSummaryRequest request) {
        GetPurchaseOrderSummaryResponse response = new GetPurchaseOrderSummaryResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            GetPurchaseOrderRequest purchaseOrderRequest = new GetPurchaseOrderRequest();
            purchaseOrderRequest.setPurchaseOrderCode(request.getPurchaseOrderCode());
            GetPurchaseOrderResponse purchaseOrderResponse = purchaseService.getPurchaseOrderDetail(purchaseOrderRequest);
            if (!purchaseOrderResponse.hasErrors()) {
                PurchaseOrderSummaryDTO summary = new PurchaseOrderSummaryDTO();
                summary.setPurchaseOrderCode(purchaseOrderResponse.getCode());
                summary.setFromParty(purchaseOrderResponse.getFromParty());
                summary.setAmendedFromPurchaseOrderCode(purchaseOrderResponse.getAmendedPurchaseOrderCode());
                summary.setCreated(purchaseOrderResponse.getCreated());
                summary.setCreatedBy(purchaseOrderResponse.getCreatedBy());
                summary.setExpiryDate(purchaseOrderResponse.getExpiryDate());
                summary.setDeliveryDate(purchaseOrderResponse.getDeliveryDate());
                summary.setVendorAgreement(purchaseOrderResponse.getVendorAgreementName());
                summary.setVendorName(purchaseOrderResponse.getVendorName());
                summary.setVendorCode(purchaseOrderResponse.getVendorCode());
                summary.setStatusCode(purchaseOrderResponse.getStatusCode());
                summary.setType(purchaseOrderResponse.getType());
                int totalQuantity = 0;
                int rejectedQuantity = 0;
                int pendingQuantity = 0;
                for (PurchaseOrderItemDTO poi : purchaseOrderResponse.getPurchaseOrderItems()) {
                    totalQuantity += poi.getQuantity();
                    rejectedQuantity += poi.getRejectedQuantity();
                    pendingQuantity += poi.getPendingQuantity();
                }
                summary.setTotalQuantity(totalQuantity);
                summary.setRejectedQuantity(rejectedQuantity);
                summary.setPendingQuantity(pendingQuantity);
                summary.setPartyAddress(purchaseOrderResponse.getPartyAddressDTO());
                summary.setCustomFieldValues(purchaseOrderResponse.getCustomFieldValues());
                response.setPurchaseOrderSummary(summary);
            } else {
                context.addErrors(purchaseOrderResponse.getErrors());
            }
        }
        response.addErrors(context.getErrors());
        response.setSuccessful(!context.hasErrors());
        return response;
    }
}
