/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 *  @version     1.0, Feb 29, 2012
 *  @author singla
 */
package com.uniware.services.purchase.impl;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.unifier.core.annotation.audit.LogActivity;
import com.unifier.core.api.audit.FetchEntityActivityRequest;
import com.unifier.core.api.audit.FetchEntityActivityResponse;
import com.unifier.core.api.comments.GetCommentsRequest;
import com.unifier.core.api.comments.GetCommentsResponse;
import com.unifier.core.api.comments.UserCommentDTO;
import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.api.validation.WsError;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.email.EmailMessage;
import com.unifier.core.entity.ReorderConfig;
import com.unifier.core.entity.User;
import com.unifier.core.expressions.Expression;
import com.unifier.core.fileparser.DelimitedFileParser;
import com.unifier.core.fileparser.Row;
import com.unifier.core.template.Template;
import com.unifier.core.transport.http.HttpSender;
import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.DateUtils.DateRange;
import com.unifier.core.utils.EncryptionUtils;
import com.unifier.core.utils.FileUtils;
import com.unifier.core.utils.NumberUtils;
import com.unifier.core.utils.ObjectUtils;
import com.unifier.core.utils.StringUtils;
import com.unifier.services.aspect.RollbackOnFailure;
import com.unifier.services.comments.ICommentsService;
import com.unifier.services.email.IEmailService;
import com.unifier.services.pdf.IPdfDocumentService;
import com.unifier.services.pdf.impl.PdfDocumentServiceImpl.PrintOptions;
import com.unifier.services.user.notification.IUserNotificationService;
import com.unifier.services.users.IUsersService;
import com.unifier.services.utils.CustomFieldUtils;
import com.uniware.core.api.currency.GetCurrencyConversionRateRequest;
import com.uniware.core.api.currency.GetCurrencyConversionRateResponse;
import com.uniware.core.api.email.SendEmailResponse;
import com.uniware.core.api.inflow.GetPOInflowReceiptsRequest;
import com.uniware.core.api.inflow.GetPOInflowReceiptsResponse;
import com.uniware.core.api.inflow.GetPOInflowReceiptsResponse.POInflowReceipt;
import com.uniware.core.api.model.WsInvoice;
import com.uniware.core.api.party.PartyAddressDTO;
import com.uniware.core.api.party.dto.VendorAgreementDTO;
import com.uniware.core.api.purchase.ASNItemDTO;
import com.uniware.core.api.purchase.AddItemsToPurchaseCartRequest;
import com.uniware.core.api.purchase.AddItemsToPurchaseCartRequest.WsPurchaseCartItem;
import com.uniware.core.api.purchase.AddItemsToPurchaseCartResponse;
import com.uniware.core.api.purchase.AddOrEditPurchaseOrderItemsRequest;
import com.uniware.core.api.purchase.AddOrEditPurchaseOrderItemsResponse;
import com.uniware.core.api.purchase.AddUpdateReorderConfigRequest;
import com.uniware.core.api.purchase.AddUpdateReorderConfigResponse;
import com.uniware.core.api.purchase.AdvanceShippingNoticeDTO;
import com.uniware.core.api.purchase.AmendPurchaseOrderRequest;
import com.uniware.core.api.purchase.AmendPurchaseOrderResponse;
import com.uniware.core.api.purchase.ApprovePurchaseOrderRequest;
import com.uniware.core.api.purchase.ApprovePurchaseOrderResponse;
import com.uniware.core.api.purchase.BulkApprovePurchaseOrderRequest;
import com.uniware.core.api.purchase.BulkApprovePurchaseOrderResponse;
import com.uniware.core.api.purchase.CancelPurchaseOrderRequest;
import com.uniware.core.api.purchase.CancelPurchaseOrderResponse;
import com.uniware.core.api.purchase.CheckoutPurchaseCartRequest;
import com.uniware.core.api.purchase.CheckoutPurchaseCartResponse;
import com.uniware.core.api.purchase.ClonePurchaseOrderRequest;
import com.uniware.core.api.purchase.ClonePurchaseOrderResponse;
import com.uniware.core.api.purchase.ClosePurchaseOrderRequest;
import com.uniware.core.api.purchase.ClosePurchaseOrderResponse;
import com.uniware.core.api.purchase.CreateASNRequest;
import com.uniware.core.api.purchase.CreateASNRequest.WsASNItem;
import com.uniware.core.api.purchase.CreateASNResponse;
import com.uniware.core.api.purchase.CreateApprovedPurchaseOrderRequest;
import com.uniware.core.api.purchase.CreateApprovedPurchaseOrderResponse;
import com.uniware.core.api.purchase.CreatePurchaseOrderRequest;
import com.uniware.core.api.purchase.CreatePurchaseOrderResponse;
import com.uniware.core.api.purchase.DeleteReorderConfigRequest;
import com.uniware.core.api.purchase.DeleteReorderConfigResponse;
import com.uniware.core.api.purchase.EditAdvanceShippingNoticeRequest;
import com.uniware.core.api.purchase.EditAdvanceShippingNoticeResponse;
import com.uniware.core.api.purchase.EmailPurchaseOrderRequest;
import com.uniware.core.api.purchase.EmailPurchaseOrderResponse;
import com.uniware.core.api.purchase.GetBackOrderItemsRequest;
import com.uniware.core.api.purchase.GetBackOrderItemsResponse;
import com.uniware.core.api.purchase.GetBackOrderItemsResponse.BackOrderItemDTO;
import com.uniware.core.api.purchase.GetBackOrderItemsResponse.BackOrderVendorItemTypeDTO;
import com.uniware.core.api.purchase.GetNextPurchaseOrderCodeResponse;
import com.uniware.core.api.purchase.GetPurchaseOrderActivitiesRequest;
import com.uniware.core.api.purchase.GetPurchaseOrderActivitiesResponse;
import com.uniware.core.api.purchase.GetPurchaseOrderRequest;
import com.uniware.core.api.purchase.GetPurchaseOrderResponse;
import com.uniware.core.api.purchase.GetPurchaseOrdersRequest;
import com.uniware.core.api.purchase.GetPurchaseOrdersResponse;
import com.uniware.core.api.purchase.GetReorderConfigByItemTypeRequest;
import com.uniware.core.api.purchase.GetReorderConfigByItemTypeResponse;
import com.uniware.core.api.purchase.GetReorderItemsRequest;
import com.uniware.core.api.purchase.GetReorderItemsResponse;
import com.uniware.core.api.purchase.GetReorderItemsResponse.ReorderItemDTO;
import com.uniware.core.api.purchase.GetReorderItemsResponse.ReorderVendorItemTypeDTO;
import com.uniware.core.api.purchase.GetVendorPurchaseOrderDetailRequest;
import com.uniware.core.api.purchase.GetVendorPurchaseOrderDetailResponse;
import com.uniware.core.api.purchase.GetVendorPurchaseOrdersRequest;
import com.uniware.core.api.purchase.GetVendorPurchaseOrdersResponse;
import com.uniware.core.api.purchase.LookupPurchaseOrderLineItemsRequest;
import com.uniware.core.api.purchase.LookupPurchaseOrderLineItemsResponse;
import com.uniware.core.api.purchase.PurchaseCartDTO;
import com.uniware.core.api.purchase.PurchaseCartDTO.PurchaseCartItemDTO;
import com.uniware.core.api.purchase.PurchaseCartDTO.PurchaseCartItemsDTO;
import com.uniware.core.api.purchase.PurchaseOrderDTO;
import com.uniware.core.api.purchase.PurchaseOrderDetailDTO;
import com.uniware.core.api.purchase.PurchaseOrderItemDTO;
import com.uniware.core.api.purchase.RejectPurchaseOrderRequest;
import com.uniware.core.api.purchase.RejectPurchaseOrderResponse;
import com.uniware.core.api.purchase.RemovePurchaseCartItemsRequest;
import com.uniware.core.api.purchase.RemovePurchaseCartItemsResponse;
import com.uniware.core.api.purchase.RemoveVendorItemsFromCartRequest;
import com.uniware.core.api.purchase.RemoveVendorItemsFromCartResponse;
import com.uniware.core.api.purchase.SearchASNRequest;
import com.uniware.core.api.purchase.SearchASNResponse;
import com.uniware.core.api.purchase.SearchASNResponse.WsASN;
import com.uniware.core.api.purchase.SearchInflowReceiptRequest;
import com.uniware.core.api.purchase.SearchInflowReceiptResponse;
import com.uniware.core.api.purchase.SearchInflowReceiptResponse.WsInflowReceipt;
import com.uniware.core.api.purchase.SearchItemTypeBackOrdersRequest;
import com.uniware.core.api.purchase.SearchItemTypeBackOrdersResponse;
import com.uniware.core.api.purchase.SearchItemTypeBackOrdersResponse.SaleOrderItemDTO;
import com.uniware.core.api.purchase.SearchItemTypeForCartRequest;
import com.uniware.core.api.purchase.SearchItemTypeForCartResponse;
import com.uniware.core.api.purchase.SearchItemTypeForCartResponse.ItemTypeCartDTO;
import com.uniware.core.api.purchase.SearchItemTypeForCartResponse.VendorItemTypeDTO;
import com.uniware.core.api.purchase.SearchPurchaseOrderRequest;
import com.uniware.core.api.purchase.SearchPurchaseOrderResponse;
import com.uniware.core.api.purchase.SendPurchaseOrderForApprovalRequest;
import com.uniware.core.api.purchase.SendPurchaseOrderForApprovalResponse;
import com.uniware.core.api.purchase.UpdatePurchaseOrderMetadataRequest;
import com.uniware.core.api.purchase.UpdatePurchaseOrderMetadataResponse;
import com.uniware.core.api.purchase.WsCheckoutPurchaseCartItem;
import com.uniware.core.api.purchase.WsCheckoutPurchaseCartVendorItem;
import com.uniware.core.api.purchase.WsPurchaseOrderItem;
import com.uniware.core.api.purchase.WsRemovePurchaseCartItem;
import com.uniware.core.api.purchase.WsReorderConfig;
import com.uniware.core.api.saleorder.display.GetSaleOrderActivitiesResponse.ActivityDTO;
import com.uniware.core.api.tax.GetTaxPercentagesRequest;
import com.uniware.core.api.tax.GetTaxPercentagesResponse;
import com.uniware.core.api.tax.TaxPercentageDTO;
import com.uniware.core.api.tax.WsTaxClass;
import com.uniware.core.api.usernotification.AddUserNotificationRequest;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.cache.EnvironmentPropertiesCache;
import com.uniware.core.cache.FacilityCache;
import com.uniware.core.configuration.TaxConfiguration;
import com.uniware.core.configuration.TaxConfiguration.TaxTypeVO;
import com.uniware.core.entity.AdvanceShippingNotice;
import com.uniware.core.entity.AdvanceShippingNoticeItem;
import com.uniware.core.entity.Facility;
import com.uniware.core.entity.InflowReceipt;
import com.uniware.core.entity.InflowReceiptItem;
import com.uniware.core.entity.ItemType;
import com.uniware.core.entity.Party;
import com.uniware.core.entity.PartyAddress;
import com.uniware.core.entity.PartyAddressType;
import com.uniware.core.entity.PartyContact;
import com.uniware.core.entity.PartyContactType;
import com.uniware.core.entity.PurchaseCart;
import com.uniware.core.entity.PurchaseCartItem;
import com.uniware.core.entity.PurchaseOrder;
import com.uniware.core.entity.PurchaseOrder.StatusCode;
import com.uniware.core.entity.PurchaseOrderItem;
import com.uniware.core.entity.PurchaseOrderStatus;
import com.uniware.core.entity.SaleOrderItem;
import com.uniware.core.entity.Sequence.Name;
import com.uniware.core.entity.TaxType;
import com.uniware.core.entity.Vendor;
import com.uniware.core.entity.VendorAgreement;
import com.uniware.core.entity.VendorItemType;
import com.uniware.core.utils.ActivityContext;
import com.uniware.core.utils.Constants.EmailTemplateType;
import com.uniware.core.utils.EnvironmentProperties;
import com.uniware.core.utils.UserContext;
import com.uniware.core.vo.ActivityMetaVO;
import com.uniware.core.vo.PrintTemplateVO;
import com.uniware.core.vo.SamplePrintTemplateVO;
import com.uniware.core.vo.UserNotificationTypeVO.NotificationType;
import com.uniware.core.vo.VendorInventoryLogVO;
import com.uniware.dao.inflow.IInflowDao;
import com.uniware.dao.purchase.IPurchaseDao;
import com.uniware.dao.reorder.IReorderConfigDAO;
import com.uniware.dao.vendor.IVendorDao;
import com.uniware.dao.vendor.IVendorInventoryLogMao;
import com.uniware.services.audit.IActivityLogService;
import com.uniware.services.audit.impl.ActivityEntityEnum;
import com.uniware.services.audit.impl.ActivityTypeEnum;
import com.uniware.services.audit.impl.ActivityUtils;
import com.uniware.services.cache.PrintTemplateCache;
import com.uniware.services.cache.SamplePrintTemplateCache;
import com.uniware.services.catalog.ICatalogService;
import com.uniware.services.common.ISequenceGenerator;
import com.uniware.services.configuration.SystemConfiguration;
import com.uniware.services.configuration.TenantSystemConfiguration;
import com.uniware.services.currency.ICurrencyService;
import com.uniware.services.party.IPartyService;
import com.uniware.services.purchase.IPurchaseService;
import com.uniware.services.tax.ITaxService;
import com.uniware.services.vendor.IVendorService;

/**
 * @author singla
 */
@Service
public class PurchaseServiceImpl implements IPurchaseService {

    private static final Logger      LOG = LoggerFactory.getLogger(PurchaseServiceImpl.class);

    @Autowired
    private IVendorService           vendorService;

    @Autowired
    private ICatalogService          catalogService;

    @Autowired
    private IPartyService            partyService;

    @Autowired
    private IPurchaseDao             purchaseDao;

    @Autowired
    private IVendorDao               vendorDao;

    @Autowired
    private IInflowDao               inflowDao;

    @Autowired
    private IUsersService            usersService;

    @Autowired
    private IEmailService            emailService;

    @Autowired
    private IPdfDocumentService      pdfDocumentService;

    @Autowired
    private IUserNotificationService userNotificationService;

    @Autowired
    private IReorderConfigDAO        reorderConfigDao;

    @Autowired
    private ICurrencyService         currencyService;

    @Autowired
    private IVendorInventoryLogMao   vendorInventoryLogMao;

    @Autowired
    private ISequenceGenerator       sequenceGenerator;

    @Autowired
    private IActivityLogService      activityLogService;

    @Autowired
    private ICommentsService         commentsService;

    @Autowired
    private ITaxService              taxService;

    @Override
    public PurchaseOrder getPurchaseOrderById(Integer purchaseOrderId, boolean lock) {
        return purchaseDao.getPurchaseOrderById(purchaseOrderId, lock);
    }

    @Override
    public PurchaseOrder lockOnPurchaseOrder(Integer purchaseOrderId) {
        return purchaseDao.lockOnPurchaseOrder(purchaseOrderId);
    }

    @Override
    public GetNextPurchaseOrderCodeResponse getNextPurchaseOrderCode() {
        GetNextPurchaseOrderCodeResponse response = new GetNextPurchaseOrderCodeResponse();
        String purchaseOrderCode = sequenceGenerator.generateNext(Name.PURCHASE_ORDER);
        response.setNextPurchaseOrderCode(purchaseOrderCode);
        response.setSuccessful(true);
        return response;
    };

    /* (non-Javadoc)
     * @see com.uniware.services.purchase.IPurchaseService#createPurchaseOrder(com.uniware.core.api.purchase.CreatePurchaseOrderRequest)
     */
    @Override
    @Transactional
    @LogActivity
    public CreatePurchaseOrderResponse createPurchaseOrder(CreatePurchaseOrderRequest request) {
        CreatePurchaseOrderResponse response = new CreatePurchaseOrderResponse();
        ValidationContext context = request.validate();
        Vendor vendor = null;
        VendorAgreement vendorAgreement = null;
        PurchaseOrder purchaseOrder = null;
        if (!context.hasErrors()) {
            if (request.getExpiryDate() != null && DateUtils.isPastTime(request.getExpiryDate())) {
                context.addError(WsResponseCode.INVALID_DATE, "Expiry date should be a future date");
            }
            if (request.getDeliveryDate() != null && DateUtils.isPastTime(request.getDeliveryDate())) {
                context.addError(WsResponseCode.INVALID_DATE, "Delivery date should be a future date");
            }
            if (StringUtils.isNotBlank(request.getPurchaseOrderCode())) {
                purchaseOrder = purchaseDao.getPurchaseOrderByCode(request.getPurchaseOrderCode());
                if (purchaseOrder != null) {
                    context.addError(WsResponseCode.DUPLICATE_CODE, "Duplicate Purchase Order Code");
                }
            }

            if (!context.hasErrors()) {
                vendor = vendorService.getVendorByCode(request.getVendorCode());
                if (vendor == null) {
                    context.addError(WsResponseCode.INVALID_VENDOR_CODE, "Invalid Vendor");
                } else if (!vendor.isConfigured()) {
                    context.addError(WsResponseCode.VENDOR_DETAILS_INCOMPLETE, "Vendor details incomplete: " + vendor.getName());
                }

                if (!context.hasErrors() && StringUtils.isNotBlank(request.getVendorAgreementName())) {
                    vendorAgreement = vendorService.getVendorAgreement(vendor.getId(), request.getVendorAgreementName());
                    if (vendorAgreement == null) {
                        context.addError(WsResponseCode.INVALID_VENDOR_AGREEMENT, "Invalid vendor agreement name");
                    } else if (!VendorAgreement.StatusCode.ACTIVE.name().equals(vendorAgreement.getStatusCode()) || DateUtils.isFutureTime(vendorAgreement.getStartTime())) {
                        context.addError(WsResponseCode.VENDOR_AGREEMENT_NOT_ACTIVE, "Vendor agreement not active");
                    } else if (DateUtils.isPastTime(vendorAgreement.getEndTime())) {
                        context.addError(WsResponseCode.VENDOR_AGREEMENT_NOT_ACTIVE, "Vendor agreement has expired");
                    }
                }
            }
        }
        String currencyCode = StringUtils.isNotBlank(request.getCurrencyCode()) ? request.getCurrencyCode()
                : ConfigurationManager.getInstance().getConfiguration(TenantSystemConfiguration.class).getBaseCurrency();
        GetCurrencyConversionRateResponse conversionRateResponse = currencyService.getCurrencyConversionRate(new GetCurrencyConversionRateRequest(currencyCode));
        if (!conversionRateResponse.isSuccessful()) {
            context.addErrors(conversionRateResponse.getErrors());
        }
        if (!context.hasErrors()) {
            purchaseOrder = new PurchaseOrder();
            if (StringUtils.isNotBlank(request.getPurchaseOrderCode())) {
                purchaseOrder.setCode(request.getPurchaseOrderCode());
            }
            purchaseOrder.setType(request.getType());
            purchaseOrder.setVendor(vendor);
            purchaseOrder.setVendorAgreement(vendorAgreement);
            purchaseOrder.setStatusCode(PurchaseOrder.StatusCode.CREATED.name());
            purchaseOrder.setFromParty(CacheManager.getInstance().getCache(FacilityCache.class).getCurrentFacility());
            User user = usersService.getUserWithDetailsById(request.getUserId());
            purchaseOrder.setUser(user);
            purchaseOrder.setLastUpdatedByUser(user);
            if (request.getExpiryDate() != null) {
                purchaseOrder.setExpiryDate(request.getExpiryDate());
            } else if (vendor.getPurchaseExpiryPeriod() != null) {
                purchaseOrder.setExpiryDate(DateUtils.addToDate(DateUtils.getCurrentTime(), Calendar.DATE, vendor.getPurchaseExpiryPeriod()));
            }
            purchaseOrder.setDeliveryDate(request.getDeliveryDate());
            purchaseOrder.setCurrencyCode(currencyCode);
            purchaseOrder.setCurrencyConversionRate(conversionRateResponse.getConversionRate());

            if (request.getPurchaseOrderItems() != null) {
                addOrEditPurchaseOrderItemsInternal(context, purchaseOrder, request.getPurchaseOrderItems(), request.getLogisticCharges(),
                        request.getLogisticChargesDivisionMethod());
            }
            purchaseOrder.setCreated(DateUtils.getCurrentTime());
            CustomFieldUtils.setCustomFieldValues(purchaseOrder, CustomFieldUtils.getCustomFieldValues(context, PurchaseOrder.class.getName(), request.getCustomFieldValues()));
            if (!context.hasErrors()) {
                purchaseOrder = purchaseDao.addPurchaseOrder(purchaseOrder);
                response.setPurchaseOrderCode(purchaseOrder.getCode());
                response.setVendorName(vendor.getName());
                response.setSuccessful(true);
                if (ActivityContext.current().isEnable()) {
                    ActivityUtils.appendActivity(purchaseOrder.getCode(), ActivityEntityEnum.PURCHASE_ORDER.getName(), null,
                            Arrays.asList(new String[] { "Purchase Order {" + ActivityEntityEnum.PURCHASE_ORDER + ":" + purchaseOrder.getCode() + "} created" }),
                            ActivityTypeEnum.CREATION.name());
                }
            }
        }

        response.setErrors(context.getErrors());
        return response;
    }

    @RollbackOnFailure
    @Override
    @Transactional
    public ClonePurchaseOrderResponse clonePurchaseOrder(ClonePurchaseOrderRequest request) {
        ClonePurchaseOrderResponse response = new ClonePurchaseOrderResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            PurchaseOrder purchaseOrder = purchaseDao.getPurchaseOrderByCode(request.getPurchaseOrderCode(), true);
            if (purchaseOrder == null) {
                context.addError(WsResponseCode.INVALID_PURCHASE_ORDER_CODE, "Invalid Purchase Order ");
            }
            Vendor vendor = null;
            if (!context.hasErrors()) {
                vendor = vendorService.getVendorByCode(request.getVendorCode());
                if (vendor == null) {
                    context.addError(WsResponseCode.INVALID_VENDOR_CODE, "Invalid vendor");
                }
                if (!context.hasErrors() && !vendor.isConfigured()) {
                    context.addError(WsResponseCode.VENDOR_DETAILS_INCOMPLETE, "Vendor details not configured");
                }
            }
            if (!context.hasErrors()) {
                PurchaseOrder clonedPO = new PurchaseOrder();
                clonedPO.setApproveTime(DateUtils.getCurrentTime());
                clonedPO.setCreated(DateUtils.getCurrentTime());
                clonedPO.setType(PurchaseOrder.Type.MANUAL.name());
                clonedPO.setDeliveryDate(purchaseOrder.getDeliveryDate());
                clonedPO.setExpiryDate(purchaseOrder.getExpiryDate());
                clonedPO.setFromParty(purchaseOrder.getFromParty());
                User user = usersService.getUserWithDetailsById(request.getUserId());
                clonedPO.setUser(user);
                clonedPO.setLastUpdatedByUser(user);
                clonedPO.setStatusCode(PurchaseOrder.StatusCode.CREATED.name());
                clonedPO.setVendor(vendor);
                clonedPO.setVendorAgreement(purchaseOrder.getVendorAgreement());
                clonedPO.setCurrencyCode(purchaseOrder.getCurrencyCode());
                clonedPO.setCurrencyConversionRate(currencyService.getCurrencyConversionRate(new GetCurrencyConversionRateRequest(clonedPO.getCurrencyCode())).getConversionRate());

                addOrEditPurchaseOrderItemsInternal(context, clonedPO, request.getPurchaseOrderItems(), request.getLogisticCharges(), request.getLogisticChargesDivisionMethod());
                if (!context.hasErrors()) {
                    clonedPO = purchaseDao.addPurchaseOrder(clonedPO);
                    clonedPO.setExpiryDate(request.getPurchaseOrderMetadata().getExpiryDate());
                    clonedPO.setDeliveryDate(request.getPurchaseOrderMetadata().getDeliveryDate());
                    if (request.getPurchaseOrderMetadata().getCustomFieldValues() != null) {
                        CustomFieldUtils.setCustomFieldValues(clonedPO,
                                CustomFieldUtils.getCustomFieldValues(context, PurchaseOrder.class.getName(), request.getPurchaseOrderMetadata().getCustomFieldValues()));
                    }
                }
                if (!context.hasErrors()) {
                    clonedPO.setStatusCode(PurchaseOrder.StatusCode.WAITING_FOR_APPROVAL.name());
                    ChangeVendorInventoryResponse inventoryLogResponse = blockVendorInventory(clonedPO);
                    //save logs for changes in inventory
                    saveVendorInventoryLogs(inventoryLogResponse.getLogs(), "ClonePO-" + request.getPurchaseOrderCode(), request.getUserId());
                    response.setSuccessful(true);
                    response.setPurchaseOrderCode(clonedPO.getCode());
                }
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    private ChangeVendorInventoryResponse blockVendorInventory(PurchaseOrder purchaseOrder) {
        ChangeVendorInventoryResponse response = new ChangeVendorInventoryResponse();

        Map<Integer, Integer> itemTypeToQuantity = new HashMap<>();
        purchaseOrder.getPurchaseOrderItems().stream().forEach(purchaseOrderItem -> itemTypeToQuantity.compute(purchaseOrderItem.getItemType().getId(),
                (k, v) -> (v == null ? purchaseOrderItem.getQuantity() : v + purchaseOrderItem.getQuantity())));

        List<VendorItemType> vendorItemTypeList = vendorDao.getVendorItemTypes(purchaseOrder.getVendor().getId(), itemTypeToQuantity.keySet(), true);
        for (VendorItemType vendorItemType : vendorItemTypeList) {
            int inventory = vendorItemType.getVendorInventory() - itemTypeToQuantity.get(vendorItemType.getItemType().getId());
            VendorInventoryLogVO logVO = new VendorInventoryLogVO();
            logVO.setOldValue(vendorItemType.getVendorInventory());
            vendorItemType.setVendorInventory(inventory > 0 ? inventory : 0);
            logVO.setNewValue(vendorItemType.getVendorInventory());
            logVO.setItemSku(vendorItemType.getItemType().getSkuCode());
            response.getLogs().add(logVO);
        }
        return response;
    }

    private ChangeVendorInventoryResponse releaseVendorInventory(PurchaseOrder purchaseOrder) {
        ChangeVendorInventoryResponse response = new ChangeVendorInventoryResponse();
        for (PurchaseOrderItem purchaseOrderItem : purchaseOrder.getPurchaseOrderItems()) {
            VendorItemType vendorItemType = vendorDao.getVendorItemType(purchaseOrder.getVendor().getId(), purchaseOrderItem.getItemType().getId(), true);
            VendorInventoryLogVO logVO = new VendorInventoryLogVO();
            logVO.setOldValue(vendorItemType.getVendorInventory());
            vendorItemType.setVendorInventory(vendorItemType.getVendorInventory() + purchaseOrderItem.getQuantity());
            logVO.setNewValue(vendorItemType.getVendorInventory());
            logVO.setItemSku(vendorItemType.getItemType().getSkuCode());
            response.getLogs().add(logVO);
        }
        return response;
    }

    private PurchaseOrderItemDTO preparePurchaseOrderItemDTO(PurchaseOrderItem purchaseOrderItem) {
        PurchaseOrderItemDTO purchaseOrderItemDTO = new PurchaseOrderItemDTO();
        if (purchaseOrderItem.getId() != null) {
            purchaseOrderItemDTO.setId(purchaseOrderItem.getId());
        }

        ItemType itemType = purchaseOrderItem.getItemType();
        purchaseOrderItemDTO.setItemTypeId(itemType.getId());
        purchaseOrderItemDTO.setItemSKU(itemType.getSkuCode());
        purchaseOrderItemDTO.setItemTypeName(itemType.getName());
        purchaseOrderItemDTO.setItemTypeImageUrl(itemType.getImageUrl());
        purchaseOrderItemDTO.setItemTypePageUrl(itemType.getProductPageUrl());
        purchaseOrderItemDTO.setVendorSkuCode(purchaseOrderItem.getVendorSkuCode());
        purchaseOrderItemDTO.setQuantity(purchaseOrderItem.getQuantity());
        purchaseOrderItemDTO.setPendingQuantity(purchaseOrderItem.getQuantity() - purchaseOrderItem.getReceivedQuantity());
        purchaseOrderItemDTO.setRejectedQuantity(purchaseOrderItem.getRejectedQuantity());
        purchaseOrderItemDTO.setBrand(itemType.getBrand());
        purchaseOrderItemDTO.setColor(itemType.getColor());
        purchaseOrderItemDTO.setSize(itemType.getSize());
        if (ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).isVendorPricesTaxExclusive()) {
            purchaseOrderItemDTO.setUnitPrice(purchaseOrderItem.getUnitPrice());
            purchaseOrderItemDTO.setDiscount(purchaseOrderItem.getDiscount());
        } else {
            BigDecimal unitPriceWithTax = NumberUtils.divide(purchaseOrderItem.getTotal(), purchaseOrderItem.getQuantity());
            BigDecimal percentage = NumberUtils.getPercentageFromBaseAndPercentageAmount(purchaseOrderItem.getSubtotal(),
                    purchaseOrderItem.getVat().add(purchaseOrderItem.getCst()));
            BigDecimal discountWithTax = purchaseOrderItem.getDiscount().add(NumberUtils.getPercentageFromBase(purchaseOrderItem.getDiscount(), percentage));
            purchaseOrderItemDTO.setDiscount(discountWithTax);
            purchaseOrderItemDTO.setUnitPrice(unitPriceWithTax.add(discountWithTax));
        }
        purchaseOrderItemDTO.setMaxRetailPrice(purchaseOrderItem.getMaxRetailPrice());
        // discount percentage = (discount / (total for one purchase order item + discount)) * 100
        purchaseOrderItemDTO.setDiscountPercentage(NumberUtils.getPercentageFromBaseAndPercentageAmount(
                purchaseOrderItem.getTotal().divide(BigDecimal.valueOf(purchaseOrderItem.getQuantity()), 2, RoundingMode.HALF_EVEN).add(purchaseOrderItem.getDiscount()),
                purchaseOrderItem.getDiscount()));
        // subtotal for presentation layer = subtotal + discount
        purchaseOrderItemDTO.setSubtotal(purchaseOrderItem.getSubtotal().add(purchaseOrderItem.getDiscount()));
        purchaseOrderItemDTO.setTotal(purchaseOrderItem.getTotal());
        purchaseOrderItemDTO.setTaxPercentage(
                purchaseOrderItem.getTaxPercentage().add(purchaseOrderItem.getCentralGstPercentage()).add(purchaseOrderItem.getStateGstPercentage()).add(
                        purchaseOrderItem.getUnionTerritoryGstPercentage()).add(purchaseOrderItem.getIntegratedGstPercentage()).add(
                                purchaseOrderItem.getCompensationCessPercentage()));
        purchaseOrderItemDTO.setTaxType(purchaseOrderItem.getTaxTypeCode());
        purchaseOrderItemDTO.setShelfLife(catalogService.getShelfLifeForItemType(itemType));
        purchaseOrderItemDTO.setExpirable(catalogService.isItemTypeExpirable(itemType));
        purchaseOrderItemDTO.setGrnExpiryTolerance(itemType.getCategory().getGrnExpiryTolerance());

        purchaseOrderItemDTO.setTax(purchaseOrderItem.getTotal().subtract(purchaseOrderItem.getSubtotal()));
        purchaseOrderItemDTO.setLogisticCharges(purchaseOrderItem.getLogisticCharges());

        return purchaseOrderItemDTO;
    }

    @Override
    public void preparePurchaseOrderItem(PurchaseOrderItem purchaseOrderItem, PurchaseOrder purchaseOrder, Vendor vendor, ItemType itemType, String vendorSkuCode, TaxType taxType,
            WsPurchaseOrderItem wsPurchaseOrderItem) {
        purchaseOrderItem.setPurchaseOrder(purchaseOrder);
        purchaseOrderItem.setItemType(itemType);
        purchaseOrderItem.setQuantity(wsPurchaseOrderItem.getQuantity());
        purchaseOrderItem.setVendorSkuCode(vendorSkuCode);

        BigDecimal discount;
        if (wsPurchaseOrderItem.getDiscount() == null) {
            BigDecimal discountPercentage = wsPurchaseOrderItem.getDiscountPercentage() == null ? BigDecimal.ZERO : wsPurchaseOrderItem.getDiscountPercentage();
            discount = NumberUtils.getPercentageFromBase(wsPurchaseOrderItem.getUnitPrice(), discountPercentage);
        } else {
            discount = wsPurchaseOrderItem.getDiscount();
        }

        if (NumberUtils.greaterThan(discount, wsPurchaseOrderItem.getUnitPrice())) {
            throw new IllegalStateException("discount for " + itemType.getSkuCode() + " cannot be greater than unit price");
        }

        PartyAddress vendorShippingAddress = vendor.getPartyAddressByType(PartyAddressType.Code.SHIPPING.name());
        PartyAddress warehouseShippingAddress = purchaseOrder.getFromParty().getPartyAddressByType(PartyAddressType.Code.SHIPPING.name());

        Map<String, WsTaxClass> taxClassMap = new HashMap<>();
        WsTaxClass taxClass;
        if (taxType == null) {
            String taxTypeCode = catalogService.getTaxTypeCode(itemType, false);
            String gstTaxTypeCode = catalogService.getTaxTypeCode(itemType, true);
            taxClass = new WsTaxClass(taxTypeCode, gstTaxTypeCode, wsPurchaseOrderItem.getUnitPrice().subtract(NumberUtils.divide(discount, wsPurchaseOrderItem.getQuantity())));
            taxClassMap.put(wsPurchaseOrderItem.getItemSKU(), taxClass);
        } else {
            taxClass = new WsTaxClass(taxType.getCode(), taxType.getCode(),
                    wsPurchaseOrderItem.getUnitPrice().subtract(NumberUtils.divide(discount, wsPurchaseOrderItem.getQuantity())));
            taxClassMap.put(wsPurchaseOrderItem.getItemSKU(), taxClass);
        }

        if (vendorShippingAddress.isIndiaAddress() && !vendor.isTaxExempted()) {
            GetTaxPercentagesRequest getTaxPercentagesRequest = new GetTaxPercentagesRequest();
            getTaxPercentagesRequest.setSourceState(vendorShippingAddress.getStateCode());
            getTaxPercentagesRequest.setSourceCountry(vendorShippingAddress.getCountryCode());
            getTaxPercentagesRequest.setDestinationState(warehouseShippingAddress.getStateCode());
            getTaxPercentagesRequest.setDestinationCountry(warehouseShippingAddress.getCountryCode());
            getTaxPercentagesRequest.setProvidesCForm(vendor.isAcceptsCForm());
            getTaxPercentagesRequest.setTaxClassMap(taxClassMap);
            getTaxPercentagesRequest.setSource(WsInvoice.Source.PURCHASE_ORDER);
            GetTaxPercentagesResponse getTaxPercentagesResponse = taxService.getTaxPercentages(getTaxPercentagesRequest);
            TaxPercentageDTO taxPercentageDTO = null;
            if (getTaxPercentagesResponse.isSuccessful()) {
                taxPercentageDTO = getTaxPercentagesResponse.getIdentifierToTaxPercentages().get(wsPurchaseOrderItem.getItemSKU());
            } else {
                LOG.error(WsResponseCode.TAX_TYPE_NOT_CONFIGURED.message(),
                        "Exception while evaluating tax for " + itemType.getSkuCode() + " : " + getTaxPercentagesResponse.getErrors().get(0).getDescription());
                throw new IllegalStateException(
                        "Exception while evaluating tax for " + itemType.getSkuCode() + " : " + getTaxPercentagesResponse.getErrors().get(0).getDescription());
            }

            if (taxType == null) {
                TaxConfiguration taxConfiguration = ConfigurationManager.getInstance().getConfiguration(TaxConfiguration.class);
                taxType = taxConfiguration.getTaxTypeByCode(taxPercentageDTO.getAppliedTaxTypeCode());
            }
            if (ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).isVendorPricesTaxExclusive()) {
                purchaseOrderItem.setUnitPrice(wsPurchaseOrderItem.getUnitPrice());
                purchaseOrderItem.setDiscount(discount);
                BigDecimal subTotalAfterSubtractingDiscount = wsPurchaseOrderItem.getUnitPrice().subtract(discount).multiply(
                        new BigDecimal(wsPurchaseOrderItem.getQuantity())).setScale(2, RoundingMode.HALF_EVEN);
                purchaseOrderItem.setSubtotal(subTotalAfterSubtractingDiscount);
                if (taxPercentageDTO.getVat() != null && !taxPercentageDTO.getVat().equals(BigDecimal.ZERO)) {
                    purchaseOrderItem.setVat(NumberUtils.getPercentageFromBase(subTotalAfterSubtractingDiscount, taxPercentageDTO.getVat()));
                    purchaseOrderItem.setAdditionalTax(NumberUtils.getPercentageFromBase(purchaseOrderItem.getVat(), taxPercentageDTO.getAdditionalTax()));
                    purchaseOrderItem.setTaxPercentage(taxPercentageDTO.getVat());
                    purchaseOrderItem.setAdditionalTaxPercentage(taxPercentageDTO.getAdditionalTax());
                } else {
                    if (vendor.isAcceptsCForm() && taxPercentageDTO.getCstFormc() != null && !taxPercentageDTO.getCstFormc().equals(BigDecimal.ZERO)) {
                        purchaseOrderItem.setCst(NumberUtils.getPercentageFromBase(subTotalAfterSubtractingDiscount, taxPercentageDTO.getCstFormc()));
                        purchaseOrderItem.setTaxPercentage(taxPercentageDTO.getCstFormc());
                    } else {
                        purchaseOrderItem.setCst(NumberUtils.getPercentageFromBase(subTotalAfterSubtractingDiscount, taxPercentageDTO.getCst()));
                        purchaseOrderItem.setTaxPercentage(taxPercentageDTO.getCst());
                    }
                }
                purchaseOrderItem.setCentralGst(NumberUtils.getPercentageFromBase(subTotalAfterSubtractingDiscount, taxPercentageDTO.getCentralGst()));
                purchaseOrderItem.setStateGst(NumberUtils.getPercentageFromBase(subTotalAfterSubtractingDiscount, taxPercentageDTO.getStateGst()));
                purchaseOrderItem.setUnionTerritoryGst(NumberUtils.getPercentageFromBase(subTotalAfterSubtractingDiscount, taxPercentageDTO.getUnionTerritoryGst()));
                purchaseOrderItem.setIntegratedGst(NumberUtils.getPercentageFromBase(subTotalAfterSubtractingDiscount, taxPercentageDTO.getIntegratedGst()));
                purchaseOrderItem.setCompensationCess(NumberUtils.getPercentageFromBase(subTotalAfterSubtractingDiscount, taxPercentageDTO.getCompensationCess()));
                BigDecimal totalTax = purchaseOrderItem.getCompensationCess().add(purchaseOrderItem.getIntegratedGst()).add(purchaseOrderItem.getStateGst()).add(
                        purchaseOrderItem.getCentralGst()).add(purchaseOrderItem.getVat()).add(purchaseOrderItem.getCst()).add(purchaseOrderItem.getUnionTerritoryGst()).add(
                                purchaseOrderItem.getAdditionalTax());
                purchaseOrderItem.setTotal(subTotalAfterSubtractingDiscount.add(totalTax));
            } else {
                purchaseOrderItem.setTotal(
                        wsPurchaseOrderItem.getUnitPrice().subtract(discount).multiply(new BigDecimal(wsPurchaseOrderItem.getQuantity())).setScale(2, RoundingMode.HALF_EVEN));
                if (taxPercentageDTO.getVat() != null && !taxPercentageDTO.getVat().equals(BigDecimal.ZERO)) {
                    BigDecimal vatWithAdditionalTax = NumberUtils.getPercentageFromTotal(purchaseOrderItem.getTotal(),
                            taxPercentageDTO.getVat().multiply(new BigDecimal(100).add(taxPercentageDTO.getAdditionalTax()).divide(new BigDecimal(100))));
                    purchaseOrderItem.setAdditionalTax(NumberUtils.getPercentageFromTotal(vatWithAdditionalTax, taxPercentageDTO.getAdditionalTax()));
                    purchaseOrderItem.setVat(vatWithAdditionalTax.subtract(purchaseOrderItem.getAdditionalTax()));
                    purchaseOrderItem.setTaxPercentage(taxPercentageDTO.getVat());
                    purchaseOrderItem.setAdditionalTaxPercentage(taxPercentageDTO.getAdditionalTax());
                } else {
                    if (vendor.isAcceptsCForm() && taxPercentageDTO.getCstFormc() != null && !taxPercentageDTO.getCstFormc().equals(BigDecimal.ZERO)) {
                        purchaseOrderItem.setCst(NumberUtils.getPercentageFromTotal(purchaseOrderItem.getTotal(), taxPercentageDTO.getCstFormc()));
                        purchaseOrderItem.setTaxPercentage(taxPercentageDTO.getCstFormc());
                    } else {
                        purchaseOrderItem.setCst(NumberUtils.getPercentageFromTotal(purchaseOrderItem.getTotal(), taxPercentageDTO.getCst()));
                        purchaseOrderItem.setTaxPercentage(taxPercentageDTO.getCst());
                    }
                }

                BigDecimal totalGstTaxPercentage = taxPercentageDTO.getTotalGstTaxPercentage();
                BigDecimal totalGstTax = NumberUtils.getPercentageFromTotal(purchaseOrderItem.getTotal(), totalGstTaxPercentage);
                if (totalGstTaxPercentage.compareTo(BigDecimal.ZERO) > 0) {
                    purchaseOrderItem.setCentralGst(
                            taxPercentageDTO.getCentralGst().multiply(totalGstTax).divide(totalGstTaxPercentage, 2, RoundingMode.HALF_EVEN).setScale(2, RoundingMode.HALF_EVEN));
                    purchaseOrderItem.setStateGst(
                            taxPercentageDTO.getStateGst().multiply(totalGstTax).divide(totalGstTaxPercentage, 2, RoundingMode.HALF_EVEN).setScale(2, RoundingMode.HALF_EVEN));
                    purchaseOrderItem.setUnionTerritoryGst(
                            taxPercentageDTO.getUnionTerritoryGst().multiply(totalGstTax).divide(totalGstTaxPercentage, 2, RoundingMode.HALF_EVEN).setScale(2,
                                    RoundingMode.HALF_EVEN));
                    purchaseOrderItem.setIntegratedGst(
                            taxPercentageDTO.getIntegratedGst().multiply(totalGstTax).divide(totalGstTaxPercentage, 2, RoundingMode.HALF_EVEN).setScale(2, RoundingMode.HALF_EVEN));
                    purchaseOrderItem.setCompensationCess(
                            taxPercentageDTO.getCompensationCess().multiply(totalGstTax).divide(totalGstTaxPercentage, 2, RoundingMode.HALF_EVEN).setScale(2,
                                    RoundingMode.HALF_EVEN));
                }
                BigDecimal totalTax = totalGstTax.add(purchaseOrderItem.getVat()).add(purchaseOrderItem.getCst()).add(purchaseOrderItem.getAdditionalTax());
                purchaseOrderItem.setDiscount(discount);
                purchaseOrderItem.setSubtotal(purchaseOrderItem.getTotal().subtract(totalTax));
                BigDecimal unitPriceWithoutDiscountAndTax = purchaseOrderItem.getSubtotal().divide(new BigDecimal(purchaseOrderItem.getQuantity()), 2, RoundingMode.HALF_EVEN);
                purchaseOrderItem.setUnitPrice(unitPriceWithoutDiscountAndTax);
            }
            purchaseOrderItem.setCentralGstPercentage(taxPercentageDTO.getCentralGst());
            purchaseOrderItem.setStateGstPercentage(taxPercentageDTO.getStateGst());
            purchaseOrderItem.setUnionTerritoryGstPercentage(taxPercentageDTO.getUnionTerritoryGst());
            purchaseOrderItem.setIntegratedGstPercentage(taxPercentageDTO.getIntegratedGst());
            purchaseOrderItem.setCompensationCessPercentage(taxPercentageDTO.getCompensationCess());
        } else {
            if (taxType == null) {
                String taxTypeCode = catalogService.getTaxTypeCode(itemType, taxService.isGSTEnabled());
                taxType = ConfigurationManager.getInstance().getConfiguration(TaxConfiguration.class).getTaxTypeByCode(taxTypeCode);
            }
            purchaseOrderItem.setDiscount(discount);
            purchaseOrderItem.setUnitPrice(wsPurchaseOrderItem.getUnitPrice());
            purchaseOrderItem.setSubtotal(
                    wsPurchaseOrderItem.getUnitPrice().subtract(purchaseOrderItem.getDiscount()).multiply(new BigDecimal(wsPurchaseOrderItem.getQuantity())).setScale(2,
                            RoundingMode.HALF_EVEN));
            purchaseOrderItem.setTotal(purchaseOrderItem.getSubtotal());
        }
        purchaseOrderItem.setTaxTypeCode(taxType.getCode());
        if (wsPurchaseOrderItem.getMaxRetailPrice() != null) {
            purchaseOrderItem.setMaxRetailPrice(wsPurchaseOrderItem.getMaxRetailPrice());
        } else {
            purchaseOrderItem.setMaxRetailPrice(itemType.getMaxRetailPrice() == null ? BigDecimal.ZERO : itemType.getMaxRetailPrice());
        }
        purchaseOrderItem.setCreated(DateUtils.getCurrentTime());
        purchaseOrder.getPurchaseOrderItems().add(purchaseOrderItem);
    }

    /* (non-Javadoc)
     * @see com.uniware.services.purchase.IPurchaseService#approvePurchaseOrder(com.uniware.core.api.purchase.ApprovePurchaseOrderRequest)
     */
    @Override
    @Transactional
    @LogActivity
    public ApprovePurchaseOrderResponse approvePurchaseOrder(ApprovePurchaseOrderRequest request) {
        ApprovePurchaseOrderResponse response = new ApprovePurchaseOrderResponse();
        ValidationContext context = request.validate();
        PurchaseOrder purchaseOrder = null;
        if (!context.hasErrors()) {
            purchaseOrder = purchaseDao.getPurchaseOrderByCode(request.getPurchaseOrderCode(), true);
            if (purchaseOrder == null) {
                context.addError(WsResponseCode.INVALID_PURCHASE_ORDER_CODE, "Invalid Purchase Order Code");
            } else if (!PurchaseOrder.StatusCode.WAITING_FOR_APPROVAL.name().equals(purchaseOrder.getStatusCode())) {
                context.addError(WsResponseCode.INVALID_PURCHASE_ORDER_STATE, "Invalid purchase order state");
            } else {
                if (purchaseOrder.getPurchaseOrderItems().size() == 0) {
                    context.addError(WsResponseCode.NO_ITEM_ADDED_TO_PURCHASE_ORDER, "No item added to purchase order yet.");
                } else {
                    Date deliveryDate = purchaseOrder.getDeliveryDate();
                    Date expiryDate = purchaseOrder.getExpiryDate();
                    if (expiryDate != null && expiryDate.before(purchaseOrder.getCreated())) {
                        context.addError(WsResponseCode.INVALID_DATE, "Expiry date should come after PO created date");
                    } else if (deliveryDate != null && deliveryDate.before(DateUtils.clearTime(purchaseOrder.getCreated()))) {
                        context.addError(WsResponseCode.INVALID_DATE, "Delivery date should come after PO created date");
                    } else if (deliveryDate != null && expiryDate != null && deliveryDate.after(expiryDate)) {
                        context.addError(WsResponseCode.INVALID_DATE, "Delivery date should be before expiry date");
                    } else {
                        purchaseOrder.setApproveTime(DateUtils.getCurrentTime());
                        purchaseOrder.setStatusCode(PurchaseOrder.StatusCode.APPROVED.name());
                        purchaseOrder.setLastUpdatedByUser(usersService.getUserWithDetailsById(request.getUserId()));
                        purchaseOrder = purchaseDao.updatePuchaseOrder(purchaseOrder);
                        sendPurchaseOrderApprovalMail(purchaseOrder);
                        response.setSuccessful(true);
                        if (ActivityContext.current().isEnable()) {
                            ActivityUtils.appendActivity(purchaseOrder.getCode(), ActivityEntityEnum.PURCHASE_ORDER.getName(), null,
                                    Arrays.asList(new String[] { "Purchase Order {" + ActivityEntityEnum.PURCHASE_ORDER + ":" + purchaseOrder.getCode() + "} approved" }),
                                    ActivityTypeEnum.APPROVE.name());
                        }
                    }
                }
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    public EmailPurchaseOrderResponse emailPurchaseOrder(EmailPurchaseOrderRequest request) {
        EmailPurchaseOrderResponse response = new EmailPurchaseOrderResponse();
        ValidationContext context = request.validate();
        PurchaseOrder purchaseOrder = null;
        if (!context.hasErrors()) {
            purchaseOrder = purchaseDao.getPurchaseOrderByCode(request.getPurchaseOrderCode());
            if (purchaseOrder == null) {
                context.addError(WsResponseCode.INVALID_PURCHASE_ORDER_CODE, "Invalid Purchase Order Code");
            } else {
                if (purchaseOrder.getPurchaseOrderItems().size() == 0) {
                    context.addError(WsResponseCode.NO_ITEM_ADDED_TO_PURCHASE_ORDER, "No item added to purchase order yet.");
                } else {
                    sendPurchaseOrderApprovalMail(purchaseOrder);
                    response.setSuccessful(true);
                }
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    private void sendPurchaseOrderApprovalMail(PurchaseOrder purchaseOrder) {
        PartyContact vendorPrimaryContact = purchaseOrder.getVendor().getPartyContactByType(PartyContactType.Code.PRIMARY.name());
        String purchaseOrderCreater = purchaseOrder.getUser().getEmail();
        if (StringUtils.isNotBlank(purchaseOrderCreater) || (vendorPrimaryContact != null && StringUtils.isNotBlank(vendorPrimaryContact.getEmail()))) {
            try {
                List<String> recipients = new ArrayList<String>();
                if (vendorPrimaryContact != null) {
                    recipients.add(vendorPrimaryContact.getEmail());
                }
                if (StringUtils.isNotBlank(purchaseOrderCreater)) {
                    recipients.add(purchaseOrderCreater);
                }
                EmailMessage message = new EmailMessage(recipients, EmailTemplateType.PURCHASE_ORDER_APPROVAL_EMAIL.name());
                message.addTemplateParam("purchaseOrder", purchaseOrder);

                Vendor vendor = purchaseOrder.getVendor();
                if (vendor.getUser() == null) {
                    Map<String, String> urlParams = new HashMap<String, String>();
                    urlParams.put("vendorId", String.valueOf(vendor.getId()));
                    String timestamp = String.valueOf(DateUtils.getCurrentTime().getTime());
                    urlParams.put("timestamp", timestamp);
                    urlParams.put("checksum", EncryptionUtils.md5Encode(String.valueOf(purchaseOrder.getVendor().getId()) + timestamp, "unicom"));
                    message.addTemplateParam("createUserLink",
                            HttpSender.createURL(CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class).getAppContextPath() + "/signup/vendor", urlParams));
                }
                message.addTemplateParam("createASNLink",
                        CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class).getAppContextPath() + "/vendor/createASN?poId=" + purchaseOrder.getCode());
                String directory = EnvironmentProperties.getApprovedPurchaseOrdersDirectoryPath();
                Expression purchaseOrderAttachmentNameExpression = ConfigurationManager.getInstance().getConfiguration(
                        SystemConfiguration.class).getPurchaseOrderAttachmentNameExpression();
                Map<String, Object> contextParams = new HashMap<String, Object>();
                contextParams.put("purchaseOrder", purchaseOrder);

                String attachmentName = purchaseOrderAttachmentNameExpression.evaluate(contextParams, String.class);
                attachmentName = StringUtils.underscorify(attachmentName);

                attachPurchaseOrderPdf(purchaseOrder, message, directory, attachmentName);
                attachPurchaseOrderCsv(purchaseOrder, message, directory, attachmentName);
                SendEmailResponse response = emailService.send(message);
                if (!response.isSuccessful()) {
                    for (String email : response.getInvalidEmails()) {
                        userNotificationService.addNotification(new AddUserNotificationRequest(email + NotificationType.INVALID_VENDOR_EMAIL, NotificationType.INVALID_VENDOR_EMAIL,
                                "Invalid email: " + email, null, "Invalid Email: " + email));
                    }
                }
            } catch (Exception e) {
                LOG.error("Error occured while sending po approval mail to vendor:", e);
            }
        }
    }

    private void attachPurchaseOrderCsv(PurchaseOrder purchaseOrder, EmailMessage message, String directory, String attachmentName) throws FileNotFoundException, IOException {
        Template template = CacheManager.getInstance().getCache(PrintTemplateCache.class).getTemplateByType(PrintTemplateVO.Type.PURCHASE_ORDER_CSV.name());
        PrintTemplateVO purchaseOrderCsvTemplate = CacheManager.getInstance().getCache(PrintTemplateCache.class).getPrintTemplateByType(PrintTemplateVO.Type.PURCHASE_ORDER_CSV);
        if (purchaseOrderCsvTemplate != null) {
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("purchaseOrder", purchaseOrder);
            String purchaseOrderCsvText = template.evaluate(params);
            File purchaseOrderCsv = new File((directory.endsWith("/") ? directory : directory + "/" + attachmentName) + ".csv");
            FileOutputStream fileOutputStream = null;
            try {
                fileOutputStream = new FileOutputStream(purchaseOrderCsv);
                FileUtils.write(purchaseOrderCsvText.getBytes(), fileOutputStream);
                message.getAttachments().add(purchaseOrderCsv);
            } finally {
                FileUtils.safelyCloseOutputStream(fileOutputStream);
            }
        }
    }

    private void attachPurchaseOrderPdf(PurchaseOrder purchaseOrder, EmailMessage message, String directory, String attachmentName) throws FileNotFoundException {
        Template template = CacheManager.getInstance().getCache(PrintTemplateCache.class).getTemplateByType(PrintTemplateVO.Type.PURCHASE_ORDER.name());
        PrintTemplateVO globalPOTemplate = CacheManager.getInstance().getCache(PrintTemplateCache.class).getPrintTemplateByType(PrintTemplateVO.Type.PURCHASE_ORDER);
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("purchaseOrder", purchaseOrder);
        String purchaseOrderHtml = template.evaluate(params);

        File file = new File((directory.endsWith("/") ? directory : directory + "/" + attachmentName) + ".pdf");
        FileOutputStream fout = null;
        try {
            fout = new FileOutputStream(file);
            SamplePrintTemplateVO samplePrintTemplate = CacheManager.getInstance().getCache(SamplePrintTemplateCache.class).getSamplePrintTemplate(
                    globalPOTemplate.getSamplePrintTemplateCode());
            pdfDocumentService.writeHtmlToPdf(fout, purchaseOrderHtml, new PrintOptions(samplePrintTemplate));
            message.getAttachments().add(file);
        } finally {
            FileUtils.safelyCloseOutputStream(fout);
        }

    }

    @Override
    @Transactional(readOnly = true)
    public PurchaseOrder getPurchaseOrderByCode(String purchaseOrderCode) {
        return purchaseDao.getPurchaseOrderByCode(purchaseOrderCode);
    }

    /* (non-Javadoc)
     * @see com.uniware.services.purchase.IPurchaseService#getVendorPurchaseOrders(java.lang.Integer)
     */
    @Override
    @Transactional(readOnly = true)
    public List<PurchaseOrderDTO> getVendorPurchaseOrders(String vendorCode) {
        List<PurchaseOrderDTO> purchaseOrders = new ArrayList<PurchaseOrderDTO>();
        for (PurchaseOrder order : purchaseDao.getPurchaseOrdersByVendorCode(vendorCode)) {
            PurchaseOrderDTO purchaseOrderDTO = new PurchaseOrderDTO(order);
            purchaseOrderDTO.setCustomFieldValues(CustomFieldUtils.getCustomFieldValuesDTO(order));
            purchaseOrders.add(purchaseOrderDTO);
        }
        return purchaseOrders;
    }

    /* (non-Javadoc)
     * @see com.uniware.services.purchase.IPurchaseService#suggestPurchaseOrders(com.uniware.core.api.purchase.SuggestPurchaseOrdersRequest)
     */
    @Override
    @Transactional(readOnly = true)
    public GetBackOrderItemsResponse getBackOrderItems(GetBackOrderItemsRequest request) {
        GetBackOrderItemsResponse response = new GetBackOrderItemsResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            List<BackOrderItemDTO> backOrderItemDTOs = new ArrayList<BackOrderItemDTO>();
            for (Object[] backOrderItemType : purchaseDao.getBackOrders(request)) {
                ItemType itemType = catalogService.getNonBundledItemTypeById((Integer) backOrderItemType[0]);
                BackOrderItemDTO backOrderItemDTO = new BackOrderItemDTO(itemType, (Integer) backOrderItemType[1]);
                List<VendorItemType> vendorItemTypes = catalogService.getVendorItemTypeByItemTypeId(itemType.getId());
                for (VendorItemType vendorItemType : vendorItemTypes) {
                    BackOrderVendorItemTypeDTO vendorItemTypeDTO = new BackOrderVendorItemTypeDTO();
                    vendorItemTypeDTO.setUnitPrice(vendorItemType.getUnitPrice());
                    vendorItemTypeDTO.setVendorId(vendorItemType.getVendor().getId());
                    vendorItemTypeDTO.setVendorName(vendorItemType.getVendor().getName());
                    vendorItemTypeDTO.setVendorCode(vendorItemType.getVendor().getCode());
                    vendorItemTypeDTO.setVendorSkuCode(vendorItemType.getVendorSkuCode());
                    backOrderItemDTO.getVendorItemTypes().add(vendorItemTypeDTO);
                }
                if (request.getNoVendors() != null && request.getNoVendors() == Boolean.TRUE) {
                    if (backOrderItemDTO.getVendorItemTypes().size() == 0) {
                        backOrderItemDTOs.add(backOrderItemDTO);
                    }
                } else {
                    backOrderItemDTOs.add(backOrderItemDTO);
                }

            }
            Collections.sort(backOrderItemDTOs, new Comparator<BackOrderItemDTO>() {
                @Override
                public int compare(BackOrderItemDTO o1, BackOrderItemDTO o2) {
                    if (o1.getVendorItemTypes().size() == 0 || o2.getVendorItemTypes().size() == 0) {
                        return o1.getVendorItemTypes().size() - o2.getVendorItemTypes().size();
                    } else {
                        return o1.getWaitingQuantity() == o2.getWaitingQuantity() ? 0 : (o1.getWaitingQuantity() < o2.getWaitingQuantity() ? 1 : -1);
                    }
                }
            });
            if (request.getNoVendors() != null) {
                response.setTotalRecords(Long.valueOf(backOrderItemDTOs.size()));
            } else if (request.getSearchOptions() != null && request.getSearchOptions().isGetCount()) {
                Long count = purchaseDao.getBackOrdersCount(request);
                response.setTotalRecords(count);
            }
            response.setElements(backOrderItemDTOs);
            response.setSuccessful(true);
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    public List<PurchaseOrderDTO> getPurchaseOrdersWaitingForApproval() {
        List<PurchaseOrderDTO> purchaseOrders = new ArrayList<PurchaseOrderDTO>();
        for (PurchaseOrder order : purchaseDao.getPurchaseOrdersWaitingForApproval()) {
            PurchaseOrderDTO purchaseOrderDTO = new PurchaseOrderDTO(order);
            purchaseOrderDTO.setCustomFieldValues(CustomFieldUtils.getCustomFieldValuesDTO(order));
            purchaseOrders.add(purchaseOrderDTO);
        }
        return purchaseOrders;
    }

    @Override
    @Transactional
    public List<PurchaseOrderDTO> getActivePurchaseOrders() {
        List<PurchaseOrderDTO> purchaseOrders = new ArrayList<PurchaseOrderDTO>();
        for (PurchaseOrder order : purchaseDao.getActivePurchaseOrders()) {
            PurchaseOrderDTO purchaseOrderDTO = new PurchaseOrderDTO(order);
            purchaseOrderDTO.setCustomFieldValues(CustomFieldUtils.getCustomFieldValuesDTO(order));
            purchaseOrders.add(purchaseOrderDTO);
        }
        return purchaseOrders;
    }

    @Override
    @Transactional
    public PurchaseOrder getPurchaseOrderDetailedByCode(String purchaseOrderCode) {
        PurchaseOrder purchaseOrder = purchaseDao.getPurchaseOrderByCode(purchaseOrderCode);
        Hibernate.initialize(purchaseOrder.getUser());
        Vendor vendor = purchaseOrder.getVendor();
        Hibernate.initialize(vendor);
        Hibernate.initialize(vendor.getPartyContactByType(PartyContactType.Code.PRIMARY.name()));
        Hibernate.initialize(vendor.getPartyAddressByType(PartyAddressType.Code.SHIPPING.name()));
        Hibernate.initialize(vendor.getPartyAddressByType(PartyAddressType.Code.BILLING.name()));

        Party fromParty = purchaseOrder.getFromParty();
        Hibernate.initialize(fromParty);
        Hibernate.initialize(fromParty.getPartyContactByType(PartyContactType.Code.PRIMARY.name()));
        Hibernate.initialize(fromParty.getPartyAddressByType(PartyAddressType.Code.SHIPPING.name()));
        Hibernate.initialize(fromParty.getPartyAddressByType(PartyAddressType.Code.BILLING.name()));
        Hibernate.initialize(purchaseOrder.getVendorAgreement());
        Hibernate.initialize(purchaseOrder.getPurchaseOrderItems());
        for (PurchaseOrderItem purchaseOrderItem : purchaseOrder.getPurchaseOrderItems()) {
            Hibernate.initialize(purchaseOrderItem.getItemType());
            Hibernate.initialize(purchaseOrderItem.getItemType().getCategory());
        }
        Hibernate.initialize(purchaseOrder.getVendorAgreement());
        return purchaseOrder;
    }

    @Override
    @Transactional
    public ClosePurchaseOrderResponse closePurchaseOrder(ClosePurchaseOrderRequest request) {
        ClosePurchaseOrderResponse response = new ClosePurchaseOrderResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            PurchaseOrder purchaseOrder = purchaseDao.getPurchaseOrderByCode(request.getPurchaseOrderCode(), true);
            if (purchaseOrder == null) {
                context.addError(WsResponseCode.INVALID_PURCHASE_ORDER_CODE, "Invalid Purchase Order Code");
            } else if (!PurchaseOrder.StatusCode.APPROVED.name().equals(purchaseOrder.getStatusCode())) {
                context.addError(WsResponseCode.INVALID_PURCHASE_ORDER_STATE, "Invalid purchase order state");
            } else {
                for (PurchaseOrderItem purchaseOrderItem : purchaseOrder.getPurchaseOrderItems()) {
                    purchaseOrderItem.setCancelledQuantity(purchaseOrderItem.getQuantity() - purchaseOrderItem.getReceivedQuantity());
                    purchaseDao.updatePuchaseOrderItem(purchaseOrderItem);
                }
                purchaseOrder.setLastUpdatedByUser(usersService.getUserWithDetailsById(request.getUserId()));
                purchaseOrder.setStatusCode(PurchaseOrder.StatusCode.COMPLETE.name());
                purchaseOrder = purchaseDao.updatePuchaseOrder(purchaseOrder);
                PurchaseOrderDTO purchaseOrderDTO = new PurchaseOrderDTO(purchaseOrder);
                purchaseOrderDTO.setCustomFieldValues(CustomFieldUtils.getCustomFieldValuesDTO(purchaseOrder));
                response.setPurchaseOrder(purchaseOrderDTO);
                sendPurchaseOrderClosureMail(purchaseOrder);
                response.setSuccessful(true);
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional(readOnly = true)
    public LookupPurchaseOrderLineItemsResponse lookupPurchaseOrderLineItems(LookupPurchaseOrderLineItemsRequest request) {
        LookupPurchaseOrderLineItemsResponse response = new LookupPurchaseOrderLineItemsResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            BufferedWriter writer = null;
            File importFile = null;
            try {
                InputStreamReader isr = new InputStreamReader(request.getInputStream(), "UTF-8");
                BufferedReader reader = new BufferedReader(isr);
                String importFileRelativePath = new StringBuilder().append("import-").append(String.valueOf(System.nanoTime())).append(".csv").toString();
                String importDirectory = CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class).getImportDirectoryPath();
                importFile = new File(FileUtils.normalizeFilePath(importDirectory, importFileRelativePath));
                importFile.createNewFile();
                writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(importFile), "UTF-8"));
                String line;
                while ((line = reader.readLine()) != null) {
                    writer.newLine();
                    writer.append(line);
                }
                writer.close();
            } catch (Exception e) {
                response.addError(new WsError(WsResponseCode.UNKNOWN_ERROR.code(), e.getMessage()));
                return response;
            } finally {
                if (writer != null) {
                    try {
                        writer.close();
                    } catch (Exception e) {
                    }
                }
            }
            DelimitedFileParser parser = new DelimitedFileParser(importFile.getAbsolutePath());
            DelimitedFileParser.RowIterator rowIterator = parser.parse();
            List<LookupPurchaseOrderLineItemsResponse.PurchaseOrderLineItem> lineItems = new ArrayList<>();
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                String skuCode = row.getColumnValue("Item SKU Code");
                VendorItemType vit = vendorService.getVendorItemType(request.getVendorCode(), skuCode);
                if (vit == null) {
                    context.addWarning(WsResponseCode.INVALID_ITEM_TYPE, skuCode);
                } else {
                    ItemType it = vit.getItemType();
                    Integer quantity = null;
                    Double sellingPrice = null;
                    Double mrp = null;
                    Double discount = null;
                    if (it.getBasePrice() != null) {
                        sellingPrice = it.getBasePrice().doubleValue();
                    }
                    if (it.getMaxRetailPrice() != null) {
                        mrp = it.getMaxRetailPrice().doubleValue();
                    }
                    TaxConfiguration taxConfiguration = ConfigurationManager.getInstance().getConfiguration(TaxConfiguration.class);
                    try {
                        quantity = Integer.parseInt(row.getColumnValue("Units"));
                        sellingPrice = Double.parseDouble(row.getColumnValue("Selling Price"));
                        mrp = Double.parseDouble(row.getColumnValue("MRP"));
                        discount = StringUtils.isNotBlank(row.getColumnValue("Discount")) ? Double.parseDouble(row.getColumnValue("Discount")) : 0;
                        if (discount > 100) {
                            discount = null;
                        }
                    } catch (Exception e) {
                        // Ignore
                        continue;
                    }
                    LookupPurchaseOrderLineItemsResponse.PurchaseOrderLineItem lineItem = new LookupPurchaseOrderLineItemsResponse.PurchaseOrderLineItem();
                    lineItem.setDiscount(discount);
                    lineItem.setMrp(mrp);
                    lineItem.setQuantity(quantity);
                    lineItem.setVendorSkuCode(vit.getVendorSkuCode());
                    lineItem.setSellingPrice(sellingPrice);
                    lineItem.setItemSKU(it.getSkuCode());

                    String rowTaxTypeCode = row.getColumnValue("Tax Type Code");
                    if (StringUtils.isNotBlank(rowTaxTypeCode) && taxConfiguration.getTaxTypeByCode(rowTaxTypeCode) != null) {
                        lineItem.setTaxType(new TaxTypeVO(taxConfiguration.getTaxTypeByCode(rowTaxTypeCode)));
                    } else if (it.getGstTaxType() != null) {
                        lineItem.setTaxType(new TaxTypeVO(it.getGstTaxType()));
                    } else if (it.getCategory().getGstTaxType() != null) {
                        lineItem.setTaxType(new TaxTypeVO(it.getCategory().getGstTaxType()));
                    }
                    lineItems.add(lineItem);
                }
            }
            response.setLineItems(lineItems);
            response.setSuccessful(true);
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        if (context.hasWarnings()) {
            response.setWarnings(context.getWarnings());
        }
        return response;
    }

    private void sendPurchaseOrderClosureMail(PurchaseOrder purchaseOrder) {
        PartyContact vendorPrimaryContact = purchaseOrder.getVendor().getPartyContactByType(PartyContactType.Code.PRIMARY.name());
        String purchaseOrderCreater = purchaseOrder.getUser().getEmail();
        if (StringUtils.isNotBlank(purchaseOrderCreater) || (vendorPrimaryContact != null && StringUtils.isNotBlank(vendorPrimaryContact.getEmail()))) {
            List<String> recipients = new ArrayList<String>();
            if (vendorPrimaryContact != null) {
                recipients.add(vendorPrimaryContact.getEmail());
            }
            if (StringUtils.isNotBlank(purchaseOrderCreater)) {
                recipients.add(purchaseOrderCreater);
            }
            EmailMessage message = new EmailMessage(recipients, EmailTemplateType.PURCHASE_ORDER_CLOSURE_EMAIL.name());
            message.addTemplateParam("purchaseOrder", purchaseOrder);
            SendEmailResponse response = emailService.send(message);
            if (!response.isSuccessful()) {
                for (String email : response.getInvalidEmails()) {
                    userNotificationService.addNotification(new AddUserNotificationRequest(email + NotificationType.INVALID_VENDOR_EMAIL, NotificationType.INVALID_VENDOR_EMAIL,
                            "Invalid email: " + email, null, "Invalid Email: " + email));
                }
            }
        }
    }

    @Override
    @Transactional
    public CancelPurchaseOrderResponse cancelPurchaseOrder(CancelPurchaseOrderRequest request) {
        CancelPurchaseOrderResponse response = new CancelPurchaseOrderResponse();
        ValidationContext context = request.validate();
        PurchaseOrder purchaseOrder = null;
        if (!context.hasErrors()) {
            purchaseOrder = purchaseDao.getPurchaseOrderByCode(request.getPurchaseOrderCode(), true);
            if (purchaseOrder == null) {
                context.addError(WsResponseCode.INVALID_PURCHASE_ORDER_CODE, "Invalid Purchase Order Code");
            } else if (!PurchaseOrder.StatusCode.CREATED.name().equals(purchaseOrder.getStatusCode())) {
                context.addError(WsResponseCode.INVALID_PURCHASE_ORDER_STATE, "Invalid purchase order state");
            } else {
                purchaseOrder.setLastUpdatedByUser(usersService.getUserWithDetailsById(request.getUserId()));
                purchaseOrder.setStatusCode(PurchaseOrder.StatusCode.CANCELLED.name());
                PurchaseOrderDTO purchaseOrderDTO = new PurchaseOrderDTO(purchaseOrder);
                purchaseOrderDTO.setCustomFieldValues(CustomFieldUtils.getCustomFieldValuesDTO(purchaseOrder));
                response.setPurchaseOrder(purchaseOrderDTO);
                response.setSuccessful(true);
                if (ActivityContext.current().isEnable()) {
                    ActivityUtils.appendActivity(purchaseOrder.getCode(), ActivityEntityEnum.PURCHASE_ORDER.getName(), null,
                            Arrays.asList(new String[] { "Purchase Order {" + ActivityEntityEnum.PURCHASE_ORDER + ":" + purchaseOrder.getCode() + "} cancelled" }),
                            ActivityTypeEnum.CANCEL.name());
                }
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    @LogActivity
    public RejectPurchaseOrderResponse rejectPurchaseOrder(RejectPurchaseOrderRequest request) {
        RejectPurchaseOrderResponse response = new RejectPurchaseOrderResponse();
        ValidationContext context = request.validate();
        PurchaseOrder purchaseOrder = null;
        if (!context.hasErrors()) {
            purchaseOrder = purchaseDao.getPurchaseOrderByCode(request.getPurchaseOrderCode(), true);
            if (purchaseOrder == null) {
                context.addError(WsResponseCode.INVALID_PURCHASE_ORDER_CODE, "Invalid Purchase Order Code");
            } else if (!PurchaseOrder.StatusCode.WAITING_FOR_APPROVAL.name().equals(purchaseOrder.getStatusCode())) {
                context.addError(WsResponseCode.INVALID_PURCHASE_ORDER_STATE, "Invalid purchase order state");
            } else {
                purchaseOrder.setLastUpdatedByUser(usersService.getUserWithDetailsById(request.getUserId()));
                purchaseOrder.setStatusCode(PurchaseOrder.StatusCode.REJECTED.name());
                ChangeVendorInventoryResponse inventoryLogResponse = releaseVendorInventory(purchaseOrder);
                //save logs for changes in inventory
                saveVendorInventoryLogs(inventoryLogResponse.getLogs(), "RejectPO-" + request.getPurchaseOrderCode(), request.getUserId());
                response.setSuccessful(true);
                if (ActivityContext.current().isEnable()) {
                    ActivityUtils.appendActivity(purchaseOrder.getCode(), ActivityEntityEnum.PURCHASE_ORDER.getName(), null,
                            Arrays.asList(new String[] { "Purchase Order {" + ActivityEntityEnum.PURCHASE_ORDER + ":" + purchaseOrder.getCode() + "} rejected" }),
                            ActivityTypeEnum.REJECT.name());
                }
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    /* (non-Javadoc)
     * @see com.uniware.services.purchase.IPurchaseService#addItemToPurchaseCart(com.uniware.core.api.purchase.AddItemsToPurchaseCartRequest)
     */
    @Override
    @Transactional
    public AddItemsToPurchaseCartResponse addItemsToPurchaseCart(AddItemsToPurchaseCartRequest request) {
        AddItemsToPurchaseCartResponse response = new AddItemsToPurchaseCartResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            Map<String, PurchaseCartItem> vendorItemTypeToPurchaseCartItems = new HashMap<String, PurchaseCartItem>();
            for (WsPurchaseCartItem cartItem : request.getPurchaseCartItems()) {
                ItemType itemType = catalogService.getItemTypeBySkuCode(cartItem.getItemSku());
                if (itemType == null) {
                    context.addError(WsResponseCode.INVALID_ITEM_TYPE, "Invalid Item Type");
                } else if (!itemType.isEnabled()) {
                    context.addError(WsResponseCode.INVALID_ITEM_TYPE, "Item Type is Disabled");
                } else {
                    VendorItemType vendorItemType = catalogService.getVendorItemType(cartItem.getVendorCode(), itemType.getId());
                    if (vendorItemType == null) {
                        context.addError(WsResponseCode.ITEM_NOT_CONFIGURED_TO_PURCHASE_FROM_VENDOR, "Item not configured to purchase from vendor sku:" + cartItem.getItemSku());
                    } else {
                        PurchaseCartItem purchaseCartItem = new PurchaseCartItem();
                        purchaseCartItem.setItemType(itemType);
                        purchaseCartItem.setVendor(vendorItemType.getVendor());
                        purchaseCartItem.setQuantity(cartItem.getQuantity());
                        purchaseCartItem.setUnitPrice(cartItem.getUnitPrice() != null ? cartItem.getUnitPrice() : vendorItemType.getUnitPrice());
                        purchaseCartItem.setCreated(DateUtils.getCurrentTime());
                        purchaseCartItem.setUpdated(DateUtils.getCurrentTime());
                        vendorItemTypeToPurchaseCartItems.put(purchaseCartItem.getVendor().getId() + "|" + purchaseCartItem.getItemType().getId(), purchaseCartItem);
                    }
                }
            }
            if (!context.hasErrors()) {
                PurchaseCart purchaseCart = purchaseDao.getPurchaseCartByUserId(request.getUserId());
                if (purchaseCart == null) {
                    purchaseCart = new PurchaseCart(usersService.getUserWithDetailsById(request.getUserId()), DateUtils.getCurrentTime(), DateUtils.getCurrentTime());
                    purchaseCart = purchaseDao.addPurchaseCart(purchaseCart);
                }

                for (PurchaseCartItem purchaseCartItem : purchaseCart.getPurchaseCartItems()) {
                    PurchaseCartItem cartItem = vendorItemTypeToPurchaseCartItems.get(purchaseCartItem.getVendor().getId() + "|" + purchaseCartItem.getItemType().getId());
                    if (cartItem != null) {
                        purchaseCartItem.setQuantity(purchaseCartItem.getQuantity() + cartItem.getQuantity());
                        purchaseDao.updatePurchaseCartItem(purchaseCartItem);
                        vendorItemTypeToPurchaseCartItems.remove(purchaseCartItem.getVendor().getId() + "|" + purchaseCartItem.getItemType().getId());
                    }
                }
                for (PurchaseCartItem purchaseCartItem : vendorItemTypeToPurchaseCartItems.values()) {
                    purchaseCartItem.setPurchaseCart(purchaseCart);
                    purchaseCartItem = purchaseDao.addPurchaseCartItem(purchaseCartItem);
                }
                response.setSuccessful(true);
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    /* (non-Javadoc)
     * @see com.uniware.services.purchase.IPurchaseService#removePurchaseCartItem(com.uniware.core.api.purchase.RemovePurchaseCartItemRequest)
     */
    @Override
    @Transactional
    @RollbackOnFailure
    public RemovePurchaseCartItemsResponse removePurchaseCartItem(RemovePurchaseCartItemsRequest request) {
        RemovePurchaseCartItemsResponse response = new RemovePurchaseCartItemsResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            for (WsRemovePurchaseCartItem removePurchaseCartItem : request.getRemovePurchaseCartItems()) {
                ItemType itemType = catalogService.getItemTypeBySkuCode(removePurchaseCartItem.getItemSku());
                Vendor vendor = vendorService.getVendorByCode(removePurchaseCartItem.getVendorCode());
                if (itemType == null) {
                    context.addError(WsResponseCode.INVALID_ITEM_TYPE, "Invalid Item Type");
                    break;
                } else if (vendor == null) {
                    context.addError(WsResponseCode.INVALID_VENDOR_CODE, "Invalid Vendor");
                    break;
                } else {
                    PurchaseCartItem purchaseCartItem = purchaseDao.getPurchaseCartItem(request.getUserId(), vendor.getId(), itemType.getId());
                    if (purchaseCartItem == null) {
                        context.addError(WsResponseCode.NO_SUCH_ITEM_IN_PURCHASE_CART, "Purchase cart doesn't contains requested item");
                    } else {
                        if (purchaseCartItem.getQuantity() > removePurchaseCartItem.getQuantity()) {
                            purchaseCartItem.setQuantity(purchaseCartItem.getQuantity() - removePurchaseCartItem.getQuantity());
                            purchaseDao.updatePurchaseCartItem(purchaseCartItem);
                        } else {
                            purchaseDao.removePurchaseCartItem(purchaseCartItem);
                        }
                    }
                }
            }
            if (!context.hasErrors()) {
                response.setSuccessful(true);
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Transactional
    @Override
    @RollbackOnFailure
    public CheckoutPurchaseCartResponse checkoutPurchaseCart(CheckoutPurchaseCartRequest request) {
        CheckoutPurchaseCartResponse response = new CheckoutPurchaseCartResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            PurchaseCart purchaseCart = purchaseDao.getPurchaseCartByUserId(request.getUserId());
            if (purchaseCart == null) {
                context.addError(WsResponseCode.PURCHASE_CART_NOT_INITIALIZED, "Purchase cart not initialized");
            } else {
                for (WsCheckoutPurchaseCartItem purchaseCartItem : request.getCheckoutPurchaseCartItems()) {
                    if (!context.hasErrors()) {
                        createPurchaseOrdersForVendor(purchaseCartItem, request, response, context);
                    }
                }
                if (!context.hasErrors()) {
                    response.setSuccessful(true);
                }
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    private void createPurchaseOrdersForVendor(WsCheckoutPurchaseCartItem purchaseCartItem, CheckoutPurchaseCartRequest request, CheckoutPurchaseCartResponse response,
            ValidationContext context) {
        CreatePurchaseOrderRequest purchaseOrderRequest = new CreatePurchaseOrderRequest();
        purchaseOrderRequest.setType(PurchaseOrder.Type.CART.name());
        purchaseOrderRequest.setUserId(request.getUserId());
        purchaseOrderRequest.setVendorAgreementName(purchaseCartItem.getVendorAgreementName());
        purchaseOrderRequest.setVendorCode(purchaseCartItem.getVendorCode());
        CreatePurchaseOrderResponse cpoResponse = createPurchaseOrder(purchaseOrderRequest);
        if (cpoResponse.hasErrors()) {
            context.getErrors().addAll(cpoResponse.getErrors());
        }
        if (!context.hasErrors()) {
            AddOrEditPurchaseOrderItemsRequest apRequest = new AddOrEditPurchaseOrderItemsRequest();
            apRequest.setPurchaseOrderCode(cpoResponse.getPurchaseOrderCode());
            List<WsPurchaseOrderItem> wsPurchaseOrderItems = new ArrayList<WsPurchaseOrderItem>();
            apRequest.setPurchaseOrderItems(wsPurchaseOrderItems);

            for (WsCheckoutPurchaseCartVendorItem purchaseCartVendorItem : purchaseCartItem.getCheckoutPurchaseCartVendorItems()) {
                WsPurchaseOrderItem wsPurchaseOrderItem = new WsPurchaseOrderItem();
                wsPurchaseOrderItem.setQuantity(purchaseCartVendorItem.getQuantity());
                wsPurchaseOrderItem.setUnitPrice(purchaseCartVendorItem.getUnitPrice());
                wsPurchaseOrderItem.setItemSKU(purchaseCartVendorItem.getItemSku());
                wsPurchaseOrderItems.add(wsPurchaseOrderItem);
            }

            AddOrEditPurchaseOrderItemsResponse apResponse = addOrEditPurchaseOrderItems(apRequest);
            if (apResponse.hasErrors()) {
                context.getErrors().addAll(apResponse.getErrors());
            }
        }

        if (!context.hasErrors()) {
            RemovePurchaseCartItemsRequest rpcRequest = new RemovePurchaseCartItemsRequest();
            rpcRequest.setUserId(request.getUserId());
            List<WsRemovePurchaseCartItem> removePurchaseCartItems = new ArrayList<WsRemovePurchaseCartItem>();
            rpcRequest.setRemovePurchaseCartItems(removePurchaseCartItems);
            for (WsCheckoutPurchaseCartVendorItem purchaseCartVendorItem : purchaseCartItem.getCheckoutPurchaseCartVendorItems()) {
                WsRemovePurchaseCartItem removePurchaseCartItem = new WsRemovePurchaseCartItem();
                removePurchaseCartItem.setItemSku(purchaseCartVendorItem.getItemSku());
                removePurchaseCartItem.setQuantity(purchaseCartVendorItem.getQuantity());
                removePurchaseCartItem.setVendorCode(purchaseCartItem.getVendorCode());
                removePurchaseCartItems.add(removePurchaseCartItem);
            }
            RemovePurchaseCartItemsResponse rpcResponse = removePurchaseCartItem(rpcRequest);
            if (rpcResponse.hasErrors()) {
                context.getErrors().addAll(rpcResponse.getErrors());
            }
        }

        if (!context.hasErrors() && purchaseCartItem.isSendForApproval()) {
            SendPurchaseOrderForApprovalRequest sendForApprovalRequest = new SendPurchaseOrderForApprovalRequest();
            sendForApprovalRequest.setPurchaseOrderCode(cpoResponse.getPurchaseOrderCode());
            sendForApprovalRequest.setUserId(request.getUserId());
            SendPurchaseOrderForApprovalResponse sendForApprovalResponse = sendPurchaseOrderForApproval(sendForApprovalRequest);
            if (sendForApprovalResponse.hasErrors()) {
                context.getErrors().addAll(sendForApprovalResponse.getErrors());
            }
        }
        response.getPurchaseOrders().add(new CheckoutPurchaseCartResponse.PurchaseOrderDTO(cpoResponse.getPurchaseOrderCode(), cpoResponse.getVendorName()));
    }

    @Override
    @Transactional
    @LogActivity
    public SendPurchaseOrderForApprovalResponse sendPurchaseOrderForApproval(SendPurchaseOrderForApprovalRequest request) {
        SendPurchaseOrderForApprovalResponse response = new SendPurchaseOrderForApprovalResponse();
        ValidationContext context = request.validate();
        ChangeVendorInventoryResponse inventoryLogResponse = new ChangeVendorInventoryResponse();
        if (!context.hasErrors()) {
            PurchaseOrder purchaseOrder = purchaseDao.getPurchaseOrderByCode(request.getPurchaseOrderCode(), true);
            if (purchaseOrder == null) {
                context.addError(WsResponseCode.INVALID_PURCHASE_ORDER_CODE, "Invalid Purchase Order Code");
            } else if (!PurchaseOrder.StatusCode.CREATED.name().equals(purchaseOrder.getStatusCode())) {
                context.addError(WsResponseCode.INVALID_PURCHASE_ORDER_STATE, "Invalid purchase order state");
            } else if (purchaseOrder.getPurchaseOrderItems().size() == 0) {
                context.addError(WsResponseCode.NO_ITEM_ADDED_TO_PURCHASE_ORDER, "No item added to purchase order yet.");
            } else {
                inventoryLogResponse = blockVendorInventory(purchaseOrder);
                purchaseOrder.setStatusCode(PurchaseOrder.StatusCode.WAITING_FOR_APPROVAL.name());
                response.setSuccessful(true);
                if (ActivityContext.current().isEnable()) {
                    ActivityUtils.appendActivity(purchaseOrder.getCode(), ActivityEntityEnum.PURCHASE_ORDER.getName(), null,
                            Arrays.asList(new String[] { "Purchase Order {" + ActivityEntityEnum.PURCHASE_ORDER + ":" + purchaseOrder.getCode() + "} sent for approval" }),
                            ActivityTypeEnum.APPROVE.name());
                }
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        } else {
            //save logs for changes in inventory
            saveVendorInventoryLogs(inventoryLogResponse.getLogs(), "PO-" + request.getPurchaseOrderCode(), request.getUserId());
        }
        return response;
    }

    private void saveVendorInventoryLogs(List<VendorInventoryLogVO> logs, String action, int userId) {
        User user = usersService.getUserWithDetailsById(userId);
        Date currentTime = DateUtils.getCurrentTime();
        for (VendorInventoryLogVO vo : logs) {
            vo.setAction(action);
            vo.setCreated(currentTime);
            vo.setUsername(user.getUsername());
        }
        vendorInventoryLogMao.createNew(logs);
    }

    @Override
    @Transactional(readOnly = true)
    public SearchItemTypeForCartResponse searchItemType(SearchItemTypeForCartRequest request) {
        SearchItemTypeForCartResponse response = new SearchItemTypeForCartResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {

            List<Object> itemTypeIds = purchaseDao.searchItemType(request);
            if (itemTypeIds.size() > 0) {
                List<ItemType> itemTypes = purchaseDao.getItemTypes(itemTypeIds);
                for (ItemType itemType : itemTypes) {
                    ItemTypeCartDTO dto = new ItemTypeCartDTO();
                    dto.setName(itemType.getName());
                    dto.setSkuCode(itemType.getSkuCode());
                    dto.setItemTypeImageUrl(itemType.getImageUrl());
                    dto.setItemTypePageUrl(itemType.getProductPageUrl());

                    List<VendorItemType> vendorItemTypes = catalogService.getVendorItemTypeByItemTypeId(itemType.getId());
                    for (VendorItemType vendorItemType : vendorItemTypes) {
                        VendorItemTypeDTO vendorItemTypeDTO = new VendorItemTypeDTO();
                        vendorItemTypeDTO.setUnitPrice(vendorItemType.getUnitPrice());
                        vendorItemTypeDTO.setVendorId(vendorItemType.getVendor().getId());
                        vendorItemTypeDTO.setVendorCode(vendorItemType.getVendor().getCode());
                        vendorItemTypeDTO.setVendorName(vendorItemType.getVendor().getName());
                        vendorItemTypeDTO.setVendorSkuCode(vendorItemType.getVendorSkuCode());
                        dto.getVendorItemTypes().add(vendorItemTypeDTO);
                    }
                    response.getItemTypes().add(dto);
                }
            }
            response.setTotalRecords(purchaseDao.getItemTypeCount(request));
            response.setSuccessful(true);
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    public PurchaseCartDTO getPurchaseCart(Integer userId) {
        PurchaseCart purchaseCart = purchaseDao.getPurchaseCartByUserId(userId);
        if (purchaseCart != null) {
            PurchaseCartDTO purchaseCartDTO = new PurchaseCartDTO();
            purchaseCartDTO.setId(purchaseCart.getId());
            for (PurchaseCartItem cartItem : purchaseCart.getPurchaseCartItems()) {
                PurchaseCartItemDTO cartItemDTO = new PurchaseCartItemDTO();
                cartItemDTO.setId(cartItem.getId());
                cartItemDTO.setItemTypeName(cartItem.getItemType().getName());
                cartItemDTO.setItemSku(cartItem.getItemType().getSkuCode());
                cartItemDTO.setBrand(cartItem.getItemType().getBrand());
                cartItemDTO.setQuantity(cartItem.getQuantity());
                cartItemDTO.setUnitPrice(cartItem.getUnitPrice());
                Integer vendorId = cartItem.getVendor().getId();
                PurchaseCartItemsDTO vendorPurchaseCartItemsDTO = purchaseCartDTO.getVendorToPurchaseCartItems().get(vendorId);
                if (vendorPurchaseCartItemsDTO == null) {
                    vendorPurchaseCartItemsDTO = new PurchaseCartItemsDTO();
                    vendorPurchaseCartItemsDTO.setVendorId(vendorId);
                    vendorPurchaseCartItemsDTO.setVendorCode(cartItem.getVendor().getCode());
                    vendorPurchaseCartItemsDTO.setVendorName(cartItem.getVendor().getName());
                    purchaseCartDTO.getVendorToPurchaseCartItems().put(vendorId, vendorPurchaseCartItemsDTO);
                }
                vendorPurchaseCartItemsDTO.getPurchaseCartItems().add(cartItemDTO);
            }

            for (Entry<Integer, PurchaseCartItemsDTO> entry : purchaseCartDTO.getVendorToPurchaseCartItems().entrySet()) {
                Integer vendorId = entry.getKey();
                Vendor vendor = vendorService.getVendorById(vendorId);
                PurchaseCartItemsDTO cartItemsDTO = entry.getValue();
                for (VendorAgreement vendorAgreement : vendor.getVendorAgreements()) {
                    if (vendorAgreement.isValid()) {
                        VendorAgreementDTO agreementDTO = new VendorAgreementDTO();
                        agreementDTO.setName(vendorAgreement.getName());
                        agreementDTO.setId(vendorAgreement.getId());
                        cartItemsDTO.getVendorAgreements().add(agreementDTO);
                    }
                }
            }

            return purchaseCartDTO;
        }
        return new PurchaseCartDTO();
    }

    @Override
    @Transactional(readOnly = true)
    public GetVendorPurchaseOrdersResponse getVendorPurchaseOrders(GetVendorPurchaseOrdersRequest request) {
        GetVendorPurchaseOrdersResponse response = new GetVendorPurchaseOrdersResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            response.setPurchaseOrderCodes(purchaseDao.getPurchaseOrderCodes(StatusCode.APPROVED.name(), request.getVendorIds(), request.getApprovedBetween()));
            response.setSuccessful(true);
        }
        return response;
    }

    @Override
    @Transactional(readOnly = true)
    public SearchPurchaseOrderResponse searchPurchaseOrders(SearchPurchaseOrderRequest request) {
        SearchPurchaseOrderResponse response = new SearchPurchaseOrderResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            List<Object> ids = purchaseDao.searchPurchaseOrders(request);
            if (request.getSearchOptions() != null && request.getSearchOptions().isGetCount()) {
                Long count = purchaseDao.getPurchaseOrderCount(request);
                response.setTotalRecords(count);
            }

            if (ids.size() > 0) {
                List<PurchaseOrder> purchaseOrders = purchaseDao.getPurchaseOrders(ids);
                for (PurchaseOrder purchaseOrder : purchaseOrders) {
                    PurchaseOrderDTO dto = new PurchaseOrderDTO();
                    dto.setFromParty(purchaseOrder.getFromParty().getName());
                    dto.setCode(purchaseOrder.getCode());
                    dto.setCreated(purchaseOrder.getCreated());
                    dto.setApproved(purchaseOrder.getApproveTime());
                    dto.setStatusCode(purchaseOrder.getStatusCode());
                    if (purchaseOrder.getVendorAgreement() != null) {
                        dto.setVendorAgreement(purchaseOrder.getVendorAgreement().getName());
                    }
                    dto.setVendorCode(purchaseOrder.getVendor().getCode());
                    dto.setVendorName(purchaseOrder.getVendor().getName());
                    response.getElements().add(dto);
                }
            }
            response.setSuccessful(true);
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional(readOnly = true)
    public GetPOInflowReceiptsResponse getInflowReceipts(GetPOInflowReceiptsRequest request) {
        GetPOInflowReceiptsResponse response = new GetPOInflowReceiptsResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            for (InflowReceipt receipt : inflowDao.getPOInflowReceipts(request.getPurchaseOrderCode())) {
                POInflowReceipt dto = new POInflowReceipt();
                dto.setCreated(receipt.getCreated());
                dto.setCreatedBy(receipt.getUser().getUsername());
                dto.setCode(receipt.getCode());
                dto.setStatusCode(receipt.getStatusCode());
                dto.setVendorInvoiceDate(receipt.getVendorInvoiceDate());
                dto.setVendorInvoiceNumber(receipt.getVendorInvoiceNumber());
                dto.setCustomFieldValues(CustomFieldUtils.getCustomFieldValuesDTO(receipt));
                for (InflowReceiptItem item : receipt.getInflowReceiptItems()) {
                    dto.setTotalQuantity(dto.getTotalQuantity() + item.getQuantity());
                    dto.setTotalRejectedQuantity(dto.getTotalRejectedQuantity() + item.getRejectedQuantity());
                    dto.setTotalReceivedAmount(dto.getTotalReceivedAmount().add(item.getTotal()));
                    if (item.getQuantity() > 0) {
                        dto.setTotalRejectedAmount(
                                dto.getTotalRejectedAmount().add(NumberUtils.multiply(NumberUtils.divide(item.getTotal(), item.getQuantity()), item.getRejectedQuantity())));
                    }
                }
                response.getInflowReceipts().add(dto);
            }
            response.setSuccessful(true);
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    public void autoCompletePurchaseOrders() {
        List<PurchaseOrder> purchaseOrders = purchaseDao.getExpiredPurchaseOrders();
        for (PurchaseOrder purchaseOrder : purchaseOrders) {
            ClosePurchaseOrderRequest request = new ClosePurchaseOrderRequest();
            request.setPurchaseOrderCode(purchaseOrder.getCode());
            request.setUserId(ConfigurationManager.getInstance().getConfiguration(TenantSystemConfiguration.class).getSystemUser().getId());
            ClosePurchaseOrderResponse response = closePurchaseOrder(request);
            if (!response.isSuccessful()) {
                LOG.error(response.getErrors().toString());
            }
        }
    }

    @Override
    @Transactional
    public CreateASNResponse createASN(CreateASNRequest request) {
        CreateASNResponse response = new CreateASNResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            PurchaseOrder purchaseOrder = purchaseDao.getPurchaseOrderByCode(request.getPurchaseOrderCode());
            if (purchaseOrder == null) {
                context.addError(WsResponseCode.INVALID_PURCHASE_ORDER_CODE, "Invalid Purchase Order Code");
            } else if (!purchaseOrder.getVendor().getCode().equals(request.getVendorCode())) {
                context.addError(WsResponseCode.INVALID_PURCHASE_ORDER_CODE, "Invalid Purchase Order Code");
            } else {
                if (request.getExpectedDeliveryDate().before(DateUtils.addToDate(purchaseOrder.getApproveTime(), Calendar.DATE, -1))) {
                    context.addError(WsResponseCode.INVALID_DATE, "expected delivery date should come after purchase order approval date");
                } else {
                    AdvanceShippingNotice asn = new AdvanceShippingNotice();
                    asn.setPurchaseOrder(purchaseOrder);
                    asn.setCreated(DateUtils.getCurrentTime());
                    asn.setExpectedDeliveryDate(request.getExpectedDeliveryDate());
                    asn.setStatusCode(AdvanceShippingNotice.StatusCode.CREATED.name());
                    for (WsASNItem wsAsnItem : request.getAsnItems()) {
                        PurchaseOrderItem purchaseOrderItem = purchaseDao.getPurchaseOrderItemBySkuCode(purchaseOrder.getId(), wsAsnItem.getSkuCode());
                        if (wsAsnItem.getQuantity() > (purchaseOrderItem.getQuantity() - purchaseOrderItem.getReceivedQuantity())) {
                            context.addError(WsResponseCode.RECEIVED_QUANTITY_CANNOT_EXCEED_PENDING_QUANTITY, "Quantity cannot be greater than pending quantity");
                            break;
                        }
                        AdvanceShippingNoticeItem asnItem = new AdvanceShippingNoticeItem();
                        asnItem.setAdvanceShippingNotice(asn);
                        asnItem.setCreated(DateUtils.getCurrentTime());
                        asnItem.setQuantity(wsAsnItem.getQuantity());
                        asnItem.setPurchaseOrderItem(purchaseOrderItem);
                        asn.getAdvanceShippingNoticeItems().add(asnItem);
                    }
                    if (!context.hasErrors()) {
                        Map<String, Object> customFieldValues = CustomFieldUtils.getCustomFieldValues(context, AdvanceShippingNotice.class.getName(),
                                request.getCustomFieldValues());
                        CustomFieldUtils.setCustomFieldValues(asn, customFieldValues);
                        purchaseDao.addASN(asn);
                        response.setAsnCode(asn.getCode());
                        sendCreateASNEmail(purchaseOrder, asn);
                        response.setSuccessful(true);
                    }
                }
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    public void sendCreateASNEmail(PurchaseOrder purchaseOrder, AdvanceShippingNotice advanceShippingNotice) {
        Vendor vendor = purchaseOrder.getVendor();
        PartyContact vendorPrimaryContact = vendor.getPartyContactByType(PartyContactType.Code.PRIMARY.name());
        if (vendorPrimaryContact != null && StringUtils.isNotBlank(vendorPrimaryContact.getEmail())) {
            List<String> recipients = new ArrayList<String>();
            if (vendorPrimaryContact != null) {
                recipients.add(vendorPrimaryContact.getEmail());
            }
            EmailMessage message = new EmailMessage(recipients, EmailTemplateType.CREATE_ASN_NOTIFICATION.name());
            message.addTemplateParam("purchaseOrder", purchaseOrder);
            message.addTemplateParam("vendor", vendor);
            message.addTemplateParam("asn", advanceShippingNotice);
            emailService.send(message);
        }
    }

    @Override
    @Transactional(readOnly = true)
    public SearchASNResponse searchASN(SearchASNRequest request) {
        SearchASNResponse response = new SearchASNResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            PurchaseOrder purchaseOrder = purchaseDao.getPurchaseOrderByCode(request.getPurchaseOrderCode());
            if (purchaseOrder == null) {
                context.addError(WsResponseCode.INVALID_PURCHASE_ORDER_CODE, "Invalid Purchase Order Code");
            } else if (!purchaseOrder.getVendor().getId().equals(request.getVendorId())) {
                context.addError(WsResponseCode.INVALID_PURCHASE_ORDER_CODE, "Invalid Purchase Order Code");
            } else {
                List<AdvanceShippingNotice> asnList = purchaseDao.getAdvanceShippingNotices(request.getPurchaseOrderCode());
                for (AdvanceShippingNotice asn : asnList) {
                    WsASN wsASN = new WsASN();
                    wsASN.setCreated(asn.getCreated());
                    wsASN.setExpectedDeliveryDate(asn.getExpectedDeliveryDate());
                    wsASN.setId(asn.getId());
                    response.getAsnList().add(wsASN);
                }
                response.setSuccessful(true);
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional(readOnly = true)
    public AdvanceShippingNoticeDTO getASN(String asnCode) {
        AdvanceShippingNoticeDTO asndto = new AdvanceShippingNoticeDTO();
        AdvanceShippingNotice shippingNotice = purchaseDao.getASNByCode(asnCode);
        asndto.setId(shippingNotice.getId());
        asndto.setVendorId(shippingNotice.getPurchaseOrder().getVendor().getId());
        asndto.setCode(shippingNotice.getCode());
        asndto.setPurchaseOrderCode(shippingNotice.getPurchaseOrder().getCode());
        asndto.setCreated(shippingNotice.getCreated());
        asndto.setExpectedDeliveryDate(shippingNotice.getExpectedDeliveryDate());
        asndto.setCustomFieldValues(CustomFieldUtils.getCustomFieldValuesDTO(shippingNotice));
        for (AdvanceShippingNoticeItem asnItem : shippingNotice.getAdvanceShippingNoticeItems()) {
            ASNItemDTO item = new ASNItemDTO();
            item.setId(asnItem.getId());
            item.setItemSKU(asnItem.getPurchaseOrderItem().getItemType().getSkuCode());
            item.setItemTypeName(asnItem.getPurchaseOrderItem().getItemType().getName());
            item.setVendorItemSKU(asnItem.getPurchaseOrderItem().getVendorSkuCode());
            item.setQuantity(asnItem.getQuantity());
            asndto.getAsnItems().add(item);
        }
        return asndto;
    }

    @Override
    @Transactional(readOnly = true)
    public SearchInflowReceiptResponse searchInflowReceipt(SearchInflowReceiptRequest request) {
        SearchInflowReceiptResponse response = new SearchInflowReceiptResponse();
        ValidationContext context = request.validate();
        if (context.hasErrors()) {
            response.setSuccessful(false);
            response.setErrors(context.getErrors());
        } else {
            List<Object> ids = purchaseDao.searchInflowReceipt(request);
            if (request.getSearchOptions() != null && request.getSearchOptions().isGetCount()) {
                Long count = purchaseDao.getInflowReceiptCount(request);
                response.setTotalRecords(count);
            }
            if (ids.size() > 0) {
                List<InflowReceipt> inflowReceipts = purchaseDao.getInflowReceipts(ids);
                for (InflowReceipt receipt : inflowReceipts) {
                    WsInflowReceipt inflowReceipt = new WsInflowReceipt();
                    inflowReceipt.setCode(receipt.getCode());
                    inflowReceipt.setVendorInvoiceNumber(receipt.getVendorInvoiceNumber());
                    inflowReceipt.setVendorInvoiceDate(receipt.getVendorInvoiceDate());
                    inflowReceipt.setCreated(receipt.getCreated());
                    inflowReceipt.setStatusCode(receipt.getStatusCode());
                    PurchaseOrder purchaseOrder = receipt.getPurchaseOrder();
                    inflowReceipt.setVendorName(purchaseOrder.getVendor().getName());
                    inflowReceipt.setPurchaseOrderCode(purchaseOrder.getCode());
                    response.getElements().add(inflowReceipt);
                }
            }
            response.setSuccessful(true);
        }
        return response;
    }

    @Override
    @Transactional
    public BulkApprovePurchaseOrderResponse bulkApprovePurchaseOrders(BulkApprovePurchaseOrderRequest request) {
        BulkApprovePurchaseOrderResponse response = new BulkApprovePurchaseOrderResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            ApprovePurchaseOrderRequest apRequest = new ApprovePurchaseOrderRequest();
            apRequest.setUserId(request.getUserId());
            for (String purchaseOrderCode : request.getPurchaseOrderCodes()) {
                apRequest.setPurchaseOrderCode(purchaseOrderCode);
                ApprovePurchaseOrderResponse apResponse = approvePurchaseOrder(apRequest);
                if (!apResponse.isSuccessful()) {
                    context.getErrors().addAll(apResponse.getErrors());
                    break;
                }
            }
            response.setSuccessful(true);
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    @RollbackOnFailure
    @LogActivity
    public AmendPurchaseOrderResponse amendPurchaseOrder(AmendPurchaseOrderRequest request) {
        AmendPurchaseOrderResponse response = new AmendPurchaseOrderResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            PurchaseOrder purchaseOrder = purchaseDao.getPurchaseOrderByCode(request.getPurchaseOrderCode(), true);
            if (purchaseOrder == null) {
                context.addError(WsResponseCode.INVALID_PURCHASE_ORDER_CODE, "Invalid Purchase Order Code");
            } else if (!PurchaseOrder.StatusCode.APPROVED.name().equals(purchaseOrder.getStatusCode())) {
                context.addError(WsResponseCode.INVALID_PURCHASE_ORDER_STATE, "Invalid Purchase Order State");
            } else if (purchaseOrder.getInflowReceipts().size() > 0) {
                context.addError(WsResponseCode.PURCHASE_ORDER_AMENDMENT_NOT_ALLOWED_AFTER_GRN, "PO amendment only allowed before any GRN is created");
            } else {
                PurchaseOrder amendment = new PurchaseOrder();
                amendment.setType(PurchaseOrder.Type.MANUAL.name());
                amendment.setCreated(DateUtils.getCurrentTime());
                amendment.setDeliveryDate(purchaseOrder.getDeliveryDate());
                amendment.setExpiryDate(purchaseOrder.getExpiryDate());
                amendment.setFromParty(purchaseOrder.getFromParty());
                User user = usersService.getUserWithDetailsById(request.getUserId());
                amendment.setUser(user);
                amendment.setLastUpdatedByUser(user);
                amendment.setStatusCode(PurchaseOrder.StatusCode.CREATED.name());
                amendment.setVendor(purchaseOrder.getVendor());
                amendment.setVendorAgreement(purchaseOrder.getVendorAgreement());
                amendment.setAmendedPurchaseOrder(purchaseOrder);
                amendment.setCurrencyCode(purchaseOrder.getCurrencyCode());
                amendment.setCurrencyConversionRate(
                        currencyService.getCurrencyConversionRate(new GetCurrencyConversionRateRequest(amendment.getCurrencyCode())).getConversionRate());

                CustomFieldUtils.setCustomFieldValues(amendment, CustomFieldUtils.getCustomFieldValues(purchaseOrder));
                addOrEditPurchaseOrderItemsInternal(context, amendment, request.getPurchaseOrderItems(), request.getLogisticCharges(), request.getLogisticChargesDivisionMethod());
                if (!context.hasErrors()) {
                    amendment = purchaseDao.addPurchaseOrder(amendment);
                    purchaseOrder.setLastUpdatedByUser(user);
                    purchaseOrder.setStatusCode(PurchaseOrder.StatusCode.AMENDED.name());
                    amendment.setStatusCode(PurchaseOrder.StatusCode.WAITING_FOR_APPROVAL.name());
                    sendPurchaseOrderApprovalMail(amendment);
                    response.setSuccessful(true);
                    response.setPurchaseOrderCode(amendment.getCode());
                    if (ActivityContext.current().isEnable()) {
                        ActivityUtils.appendActivity(purchaseOrder.getCode(), ActivityEntityEnum.PURCHASE_ORDER.getName(), null, Arrays.asList(new String[] {
                                "Purchase Order {" + ActivityEntityEnum.PURCHASE_ORDER + ":" + purchaseOrder.getCode() + "} amended and " + amendment.getCode() + " is created" }),
                                ActivityTypeEnum.AMEND.name());
                    }
                }
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    @LogActivity
    public UpdatePurchaseOrderMetadataResponse updatePurchaseOrderMetadata(UpdatePurchaseOrderMetadataRequest request) {
        UpdatePurchaseOrderMetadataResponse response = new UpdatePurchaseOrderMetadataResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {

            PurchaseOrder purchaseOrder = purchaseDao.getPurchaseOrderByCode(request.getPurchaseOrderCode(), true);
            if (purchaseOrder == null) {
                context.addError(WsResponseCode.INVALID_PURCHASE_ORDER_CODE, "Invalid Purchase Order Code");
            } else if (!StringUtils.equalsAny(purchaseOrder.getStatusCode(), PurchaseOrder.StatusCode.CREATED.name(), PurchaseOrder.StatusCode.WAITING_FOR_APPROVAL.name(),
                    PurchaseOrder.StatusCode.APPROVED.name())) {
                context.addError(WsResponseCode.INVALID_PURCHASE_ORDER_STATE, "Invalid Purchase Order State");
            } else {
                try {
                    PurchaseOrder clone = ObjectUtils.clone(purchaseOrder);
                    Date deliveryDate = request.getPurchaseOrderMetadata().getDeliveryDate();
                    Date expiryDate = request.getPurchaseOrderMetadata().getExpiryDate();
                    if (expiryDate != null && expiryDate.before(purchaseOrder.getCreated())) {
                        context.addError(WsResponseCode.INVALID_DATE, "Expiry date should come after PO created date");
                    } else if (deliveryDate != null && deliveryDate.before(DateUtils.clearTime(purchaseOrder.getCreated()))) {
                        context.addError(WsResponseCode.INVALID_DATE, "Delivery date should come on or after PO created date");
                    } else if (deliveryDate != null && expiryDate != null && deliveryDate.after(expiryDate)) {
                        context.addError(WsResponseCode.INVALID_DATE, "Delivery date should be before expiry date");
                    } else {
                        purchaseOrder.setExpiryDate(expiryDate);
                        purchaseOrder.setDeliveryDate(deliveryDate);
                        purchaseOrder.setType(request.getPurchaseOrderMetadata().getType());
                        if (request.getPurchaseOrderMetadata().getCustomFieldValues() != null) {
                            CustomFieldUtils.setCustomFieldValues(purchaseOrder,
                                    CustomFieldUtils.getCustomFieldValues(context, PurchaseOrder.class.getName(), request.getPurchaseOrderMetadata().getCustomFieldValues()));
                        }
                        if (!context.hasErrors()) {
                            purchaseOrder = purchaseDao.updatePuchaseOrder(purchaseOrder);
                            response.setSuccessful(true);
                            if (ActivityContext.current().isEnable()) {
                                HashMap<String, ArrayList<String>> entityDifferenceMap = new HashMap<String, ArrayList<String>>();
                                ActivityUtils.getObjectDiff(clone, purchaseOrder, entityDifferenceMap);
                                StringBuffer sb = new StringBuffer();
                                for (Map.Entry<String, ArrayList<String>> entry : entityDifferenceMap.entrySet()) {
                                    sb.append(entry.getKey() + " changed from "
                                            + (entry.getValue().size() == 2 ? entry.getValue().get(0) + " to " + entry.getValue().get(1) : "null to " + entry.getValue().get(0))
                                            + ",");
                                }
                                String activityLog = sb.toString();
                                if (StringUtils.isNotBlank(activityLog)) {
                                    ActivityUtils.appendActivity(purchaseOrder.getCode(), ActivityEntityEnum.PURCHASE_ORDER.getName(), purchaseOrder.getCode(),
                                            Arrays.asList(new String[] { activityLog.substring(0, activityLog.length() - 2) }), ActivityTypeEnum.EDIT.name());
                                }
                            }
                        }
                    }
                } catch (Exception e) {
                    LOG.error("Error updating purchase order {}", e);
                    response.setSuccessful(false);
                }
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional(readOnly = true)
    public SearchItemTypeBackOrdersResponse getItemTypeBackOrders(SearchItemTypeBackOrdersRequest request) {
        SearchItemTypeBackOrdersResponse response = new SearchItemTypeBackOrdersResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            int backOrderQuantity = purchaseDao.getItemTypeBackOrderQuantity(request.getItemSKU());
            for (SaleOrderItem saleOrderItem : purchaseDao.getUnfulfillableSaleOrderItems(request.getItemSKU(), backOrderQuantity)) {
                SaleOrderItemDTO saleOrderItemDTO = new SaleOrderItemDTO();
                ItemType itemType = saleOrderItem.getItemType();
                saleOrderItemDTO.setImageUrl(itemType.getImageUrl());
                saleOrderItemDTO.setItemName(itemType.getName());
                saleOrderItemDTO.setItemSku(itemType.getSkuCode());
                saleOrderItemDTO.setSellingPrice(saleOrderItem.getSellingPrice());
                saleOrderItemDTO.setSaleOrderCode(saleOrderItem.getSaleOrder().getCode());
                saleOrderItemDTO.setSaleOrderItemId(saleOrderItem.getId());
                saleOrderItemDTO.setSaleOrderItemCode(saleOrderItem.getCode());
                saleOrderItemDTO.setStatus(saleOrderItem.getStatusCode());
                response.getSaleOrderItems().add(saleOrderItemDTO);
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional(readOnly = true)
    public List<PurchaseOrder> getPurchaseOrderByStatusInRange(String statusCode, DateRange dateRange, String vendorCode) {
        return purchaseDao.getPurchaseOrderByStatusInRange(statusCode, dateRange, vendorCode);
    }

    @Override
    @Transactional(readOnly = true)
    public List<PurchaseOrder> getPurchaseOrderByStatus(String statusCode, String vendorCode) {
        return purchaseDao.getPurchaseOrderByStatus(statusCode, vendorCode);
    }

    @Override
    @Transactional
    @RollbackOnFailure
    public CreateApprovedPurchaseOrderResponse createApprovedPurchaseOrder(CreateApprovedPurchaseOrderRequest request) {
        CreateApprovedPurchaseOrderResponse response = new CreateApprovedPurchaseOrderResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            CreatePurchaseOrderRequest purchaseOrderRequest = new CreatePurchaseOrderRequest();
            purchaseOrderRequest.setUserId(request.getUserId());
            purchaseOrderRequest.setPurchaseOrderCode(request.getPurchaseOrderCode());
            purchaseOrderRequest.setCurrencyCode(request.getCurrencyCode());
            purchaseOrderRequest.setVendorCode(request.getVendorCode());
            purchaseOrderRequest.setVendorAgreementName(request.getVendorAgreementName());
            purchaseOrderRequest.setExpiryDate(request.getExpiryDate());
            purchaseOrderRequest.setDeliveryDate(request.getDeliveryDate());
            purchaseOrderRequest.setCustomFieldValues(request.getCustomFieldValues());
            CreatePurchaseOrderResponse purchaseOrderResponse = createPurchaseOrder(purchaseOrderRequest);
            String purchaseOrderCode = purchaseOrderResponse.getPurchaseOrderCode();
            if (!purchaseOrderResponse.hasErrors()) {
                AddOrEditPurchaseOrderItemsRequest purchaseOrderItemsRequest = new AddOrEditPurchaseOrderItemsRequest();
                purchaseOrderItemsRequest.setPurchaseOrderCode(purchaseOrderResponse.getPurchaseOrderCode());
                purchaseOrderItemsRequest.setPurchaseOrderItems(request.getPurchaseOrderItems());
                AddOrEditPurchaseOrderItemsResponse purchaseOrderItemsResponse = addOrEditPurchaseOrderItems(purchaseOrderItemsRequest);
                if (!purchaseOrderItemsResponse.hasErrors()) {
                    SendPurchaseOrderForApprovalRequest approvalRequest = new SendPurchaseOrderForApprovalRequest();
                    approvalRequest.setPurchaseOrderCode(purchaseOrderCode);
                    approvalRequest.setUserId(request.getUserId());
                    SendPurchaseOrderForApprovalResponse approvalResponse = sendPurchaseOrderForApproval(approvalRequest);
                    if (!approvalResponse.hasErrors()) {
                        ApprovePurchaseOrderRequest approvePurchaseOrderRequest = new ApprovePurchaseOrderRequest();
                        approvePurchaseOrderRequest.setPurchaseOrderCode(purchaseOrderCode);
                        approvePurchaseOrderRequest.setUserId(request.getUserId());
                        ApprovePurchaseOrderResponse approvePurchaseOrderResponse = approvePurchaseOrder(approvePurchaseOrderRequest);
                        if (!approvePurchaseOrderResponse.hasErrors()) {
                            response.setPurchaseOrderCode(purchaseOrderResponse.getPurchaseOrderCode());
                            response.setSuccessful(true);
                        } else {
                            context.getErrors().addAll(approvePurchaseOrderResponse.getErrors());
                        }
                    } else {
                        context.getErrors().addAll(approvalResponse.getErrors());
                    }
                } else {
                    context.getErrors().addAll(purchaseOrderItemsResponse.getErrors());
                }
            } else {
                context.getErrors().addAll(purchaseOrderResponse.getErrors());
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    public GetReorderItemsResponse getReorderItemsNew(GetReorderItemsRequest request) {
        long startTime = System.currentTimeMillis();
        GetReorderItemsResponse response = new GetReorderItemsResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            List<ReorderItemDTO> reorderItemDTOs = new ArrayList<ReorderItemDTO>();
            for (Object[] reorders : purchaseDao.getReordersNew(request)) {
                //                ItemType itemType = catalogService.getNonBundledItemTypeById((Integer) reorders[0]);
                if ((Integer) reorders[5] <= 0) {
                    continue;
                }
                ReorderItemDTO reorderItemDTO = new ReorderItemDTO((String) reorders[3], (String) reorders[2], (Integer) reorders[4], (Integer) reorders[5], (Integer) reorders[0]);
                List<VendorItemType> vendorItemTypes = catalogService.getVendorItemTypeByItemTypeId((Integer) reorders[1]);
                for (VendorItemType vendorItemType : vendorItemTypes) {
                    ReorderVendorItemTypeDTO vendorItemTypeDTO = new ReorderVendorItemTypeDTO();
                    vendorItemTypeDTO.setUnitPrice(vendorItemType.getUnitPrice());
                    vendorItemTypeDTO.setVendorId(vendorItemType.getVendor().getId());
                    vendorItemTypeDTO.setVendorName(vendorItemType.getVendor().getName());
                    vendorItemTypeDTO.setVendorCode(vendorItemType.getVendor().getCode());
                    reorderItemDTO.getVendorItemTypes().add(vendorItemTypeDTO);
                }
                reorderItemDTOs.add(reorderItemDTO);
            }
            Collections.sort(reorderItemDTOs, new Comparator<ReorderItemDTO>() {
                @Override
                public int compare(ReorderItemDTO o1, ReorderItemDTO o2) {
                    if (o1.getVendorItemTypes().size() == 0 && o2.getVendorItemTypes().size() > 0) {
                        return -1;
                    }
                    if (o2.getVendorItemTypes().size() == 0 && o1.getVendorItemTypes().size() > 0) {
                        return 1;
                    }
                    return Integer.compare(o1.getWaitingQuantity(), o2.getWaitingQuantity());
                }
            });
            response.setItemTypes(reorderItemDTOs);
            response.setSuccessful(true);
        }
        response.setErrors(context.getErrors());
        System.out.println("Time to complete API " + (System.currentTimeMillis() - startTime));
        return response;
    }

    @Override
    @Transactional
    public GetReorderItemsResponse getReorderItems(GetReorderItemsRequest request) {
        GetReorderItemsResponse response = new GetReorderItemsResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            List<ReorderItemDTO> reorderItemDTOs = new ArrayList<ReorderItemDTO>();
            for (Object[] reorderItemType : purchaseDao.getReordersNew(request)) {
                ItemType itemType = catalogService.getNonBundledItemTypeById((Integer) reorderItemType[0]);
                ReorderItemDTO reorderItemDTO = new ReorderItemDTO(itemType, (Integer) reorderItemType[1]);
                List<VendorItemType> vendorItemTypes = catalogService.getVendorItemTypeByItemTypeId(itemType.getId());
                for (VendorItemType vendorItemType : vendorItemTypes) {
                    ReorderVendorItemTypeDTO vendorItemTypeDTO = new ReorderVendorItemTypeDTO();
                    vendorItemTypeDTO.setUnitPrice(vendorItemType.getUnitPrice());
                    vendorItemTypeDTO.setVendorId(vendorItemType.getVendor().getId());
                    vendorItemTypeDTO.setVendorName(vendorItemType.getVendor().getName());
                    vendorItemTypeDTO.setVendorCode(vendorItemType.getVendor().getCode());
                    reorderItemDTO.getVendorItemTypes().add(vendorItemTypeDTO);
                }
                reorderItemDTOs.add(reorderItemDTO);
            }
            Collections.sort(reorderItemDTOs, new Comparator<ReorderItemDTO>() {
                @Override
                public int compare(ReorderItemDTO o1, ReorderItemDTO o2) {
                    if (o1.getVendorItemTypes().size() == 0 && o2.getVendorItemTypes().size() > 0) {
                        return -1;
                    }
                    if (o2.getVendorItemTypes().size() == 0 && o1.getVendorItemTypes().size() > 0) {
                        return 1;
                    }
                    return Integer.compare(o1.getWaitingQuantity(), o2.getWaitingQuantity());
                }
            });
            response.setItemTypes(reorderItemDTOs);
            response.setSuccessful(true);
        }
        response.setErrors(context.getErrors());
        return response;
    }

    /* (non-Javadoc)
     * @see com.uniware.services.purchase.IPurchaseService#removeVendorItemsFromCart(com.uniware.core.api.purchase.RemoveVendorCartItemsRequest)
     */
    @Override
    @Transactional
    public RemoveVendorItemsFromCartResponse removeVendorItemsFromCart(RemoveVendorItemsFromCartRequest request) {
        RemoveVendorItemsFromCartResponse response = new RemoveVendorItemsFromCartResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            PurchaseCart purchaseCart = purchaseDao.getPurchaseCartByUserId(request.getUserId());
            if (purchaseCart == null) {
                context.addError(WsResponseCode.PURCHASE_CART_NOT_INITIALIZED, "Purchase cart not initialized");
            } else {
                Vendor vendor = vendorService.getVendorByCode(request.getVendorCode());
                if (vendor == null) {
                    context.addError(WsResponseCode.INVALID_VENDOR_CODE, "Invalid Vendor");
                } else {
                    purchaseDao.removeVendorItemsFromCart(purchaseCart, vendor);
                    response.setSuccessful(true);
                }
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional(readOnly = true)
    public GetPurchaseOrderResponse getPurchaseOrderDetail(GetPurchaseOrderRequest request) {
        GetPurchaseOrderResponse response = new GetPurchaseOrderResponse();
        ValidationContext context = request.validate();
        PurchaseOrder purchaseOrder = null;
        if (!context.hasErrors()) {
            purchaseOrder = purchaseDao.getPurchaseOrderByCode(request.getPurchaseOrderCode());
            if (purchaseOrder == null) {
                context.addError(WsResponseCode.INVALID_PURCHASE_ORDER_CODE, "Invalid purchase order code");
            } else {
                Party fromParty = partyService.getPartyById(purchaseOrder.getFromParty().getId());
                purchaseOrder.setFromParty(fromParty);

                List<PurchaseOrderItem> purchaseOrderItems = purchaseDao.getPurchaseOrderItems(purchaseOrder.getId());
                purchaseOrder.getPurchaseOrderItems().addAll(purchaseOrderItems);

                Vendor vendor = vendorService.getVendorById(purchaseOrder.getVendor().getId());
                purchaseOrder.setVendor(vendor);

                if (!(purchaseOrder.getVendorAgreement() == null)) {
                    VendorAgreement vendorAgreement = vendorService.getVendorAgreementById(purchaseOrder.getVendorAgreement().getId());
                    purchaseOrder.setVendorAgreement(vendorAgreement);
                }

                response.setCreatedBy(purchaseOrder.getUser().getUsername());
                response.setFromParty(purchaseOrder.getFromParty().getName());
                response.setPartyAddressDTO(new PartyAddressDTO(purchaseOrder.getFromParty().getPartyAddressByType(PartyAddressType.Code.SHIPPING.name())));
                if (purchaseOrder.getAmendedPurchaseOrder() != null) {
                    response.setAmendedPurchaseOrderCode(purchaseOrder.getAmendedPurchaseOrder().getCode());
                }
                if (purchaseOrder.getAmendment() != null) {
                    response.setAmendmentPurchaseOrderCode(purchaseOrder.getAmendment().getCode());
                }
                response.setStatusCode(purchaseOrder.getStatusCode());
                response.setId(purchaseOrder.getId());
                response.setCode(purchaseOrder.getCode());
                response.setType(purchaseOrder.getType());
                response.setCreated(purchaseOrder.getCreated());
                response.setExpiryDate(purchaseOrder.getExpiryDate());
                response.setDeliveryDate(purchaseOrder.getDeliveryDate());
                response.setVendorCode(purchaseOrder.getVendor().getCode());
                response.setVendorId(purchaseOrder.getVendor().getId());
                response.setVendorName(purchaseOrder.getVendor().getName());
                if (!(purchaseOrder.getVendorAgreement() == null)) {
                    response.setVendorAgreementName(purchaseOrder.getVendorAgreement().getName());
                }
                response.setCustomFieldValues(CustomFieldUtils.getCustomFieldValuesDTO(purchaseOrder));
                response.setInflowReceiptsCount(purchaseOrder.getInflowReceipts().size());
                GetPurchaseOrderResponse.PurchaseOrderPriceSummary purchaseOrderPriceSummary = new GetPurchaseOrderResponse.PurchaseOrderPriceSummary();
                for (PurchaseOrderItem item : purchaseOrder.getPurchaseOrderItems()) {
                    PurchaseOrderItemDTO purchaseOrderItemDTO = preparePurchaseOrderItemDTO(item);
                    purchaseOrderPriceSummary.setDiscount(purchaseOrderPriceSummary.getDiscount().add(purchaseOrderItemDTO.getDiscount()));
                    purchaseOrderPriceSummary.setTaxOnSales(purchaseOrderPriceSummary.getTaxOnSales().add(purchaseOrderItemDTO.getTax()));
                    purchaseOrderPriceSummary.setSubTotalBeforeTaxesAndDiscount(purchaseOrderPriceSummary.getSubTotalBeforeTaxesAndDiscount().add(
                            purchaseOrderItemDTO.getTotal().min(purchaseOrderItemDTO.getTax()).add(purchaseOrderItemDTO.getDiscount())));
                    purchaseOrderPriceSummary.setTotalAmount(purchaseOrderPriceSummary.getTotalAmount().add(purchaseOrderItemDTO.getTotal()));
                    purchaseOrderPriceSummary.setTotalItems(purchaseOrderPriceSummary.getTotalItems() + 1);
                    if (request.isFetchPurchaseOrderItemDetail()) {
                        response.getPurchaseOrderItems().add(purchaseOrderItemDTO);
                    }
                }
                purchaseOrderPriceSummary.setLogisticCharges(purchaseOrder.getLogisticCharges());
                response.setPurchaseOrderPriceSummary(purchaseOrderPriceSummary);
                response.setLogisticCharges(purchaseOrder.getLogisticCharges());
                response.setLogisticChargesDivisionMethod(purchaseOrder.getLogisticChargesDivisionMethod());
                response.setSuccessful(true);
            }
        }
        response.setErrors(context.getErrors());
        return response;

    }

    private PurchaseOrderDetailDTO preparePurchaseOrderDetailDTO(PurchaseOrder purchaseOrder) {
        PurchaseOrderDetailDTO purchaseOrderDetailDTO = new PurchaseOrderDetailDTO();
        Party fromParty = partyService.getPartyById(purchaseOrder.getFromParty().getId());
        purchaseOrder.setFromParty(fromParty);

        List<PurchaseOrderItem> purchaseOrderItems = purchaseDao.getPurchaseOrderItems(purchaseOrder.getId());
        purchaseOrder.getPurchaseOrderItems().addAll(purchaseOrderItems);

        Vendor vendor = vendorService.getVendorById(purchaseOrder.getVendor().getId());
        purchaseOrder.setVendor(vendor);

        if (!(purchaseOrder.getVendorAgreement() == null)) {
            VendorAgreement vendorAgreement = vendorService.getVendorAgreementById(purchaseOrder.getVendorAgreement().getId());
            purchaseOrder.setVendorAgreement(vendorAgreement);
        }

        purchaseOrderDetailDTO.setFromParty(purchaseOrder.getFromParty().getName());
        purchaseOrderDetailDTO.setPartyAddressDTO(new PartyAddressDTO(purchaseOrder.getFromParty().getPartyAddressByType(PartyAddressType.Code.SHIPPING.name())));
        if (purchaseOrder.getAmendedPurchaseOrder() != null) {
            purchaseOrderDetailDTO.setAmendedPurchaseOrderCode(purchaseOrder.getAmendedPurchaseOrder().getCode());
        }
        if (purchaseOrder.getAmendment() != null) {
            purchaseOrderDetailDTO.setAmendmentPurchaseOrderCode(purchaseOrder.getAmendment().getCode());
        }
        purchaseOrderDetailDTO.setStatusCode(purchaseOrder.getStatusCode());
        purchaseOrderDetailDTO.setId(purchaseOrder.getId());
        purchaseOrderDetailDTO.setCode(purchaseOrder.getCode());
        purchaseOrderDetailDTO.setType(purchaseOrder.getType());
        purchaseOrderDetailDTO.setCreated(purchaseOrder.getCreated());
        purchaseOrderDetailDTO.setExpiryDate(purchaseOrder.getExpiryDate());
        purchaseOrderDetailDTO.setDeliveryDate(purchaseOrder.getDeliveryDate());
        purchaseOrderDetailDTO.setVendorCode(purchaseOrder.getVendor().getCode());
        purchaseOrderDetailDTO.setVendorName(purchaseOrder.getVendor().getName());
        if (!(purchaseOrder.getVendorAgreement() == null)) {
            purchaseOrderDetailDTO.setVendorAgreementName(purchaseOrder.getVendorAgreement().getName());
        }
        purchaseOrderDetailDTO.setCustomFieldValues(CustomFieldUtils.getCustomFieldValuesDTO(purchaseOrder));
        purchaseOrderDetailDTO.setInflowReceiptsCount(purchaseOrder.getInflowReceipts().size());
        for (PurchaseOrderItem item : purchaseOrder.getPurchaseOrderItems()) {
            purchaseOrderDetailDTO.getPurchaseOrderItems().add(preparePurchaseOrderItemDTO(item));
        }
        return purchaseOrderDetailDTO;
    }

    @Override
    @Transactional(readOnly = true)
    public GetVendorPurchaseOrderDetailResponse getVendorPurchaseOrderDetail(GetVendorPurchaseOrderDetailRequest request) {
        GetVendorPurchaseOrderDetailResponse response = new GetVendorPurchaseOrderDetailResponse();
        ValidationContext context = request.validate();
        PurchaseOrder purchaseOrder = null;
        if (!context.hasErrors()) {
            purchaseOrder = purchaseDao.getVendorPurchaseOrderByCode(request.getPurchaseOrderCode(), request.getVendorIds(), false);
            if (purchaseOrder == null) {
                context.addError(WsResponseCode.INVALID_PURCHASE_ORDER_CODE, "Invalid purchase order code");
            } else {
                response.setPurchaseOrderDetailDTO(preparePurchaseOrderDetailDTO(purchaseOrder));
                response.setSuccessful(true);
            }
        }
        return response;
    }

    @Override
    @Transactional(readOnly = true)
    public AddOrEditPurchaseOrderItemsResponse addOrEditPurchaseOrderItemsReadOnly(AddOrEditPurchaseOrderItemsRequest request) {
        AddOrEditPurchaseOrderItemsResponse response = new AddOrEditPurchaseOrderItemsResponse();
        ValidationContext context = request.validate();
        PurchaseOrder purchaseOrder = null;
        if (!context.hasErrors()) {
            if (StringUtils.isNotBlank(request.getPurchaseOrderCode())) {
                purchaseOrder = purchaseDao.getPurchaseOrderByCode(request.getPurchaseOrderCode(), true);
                if (purchaseOrder == null) {
                    context.addError(WsResponseCode.INVALID_PURCHASE_ORDER_CODE, "Invalid Purchase Order Code");
                }
            } else {
                Vendor vendor = vendorService.getVendorByCode(request.getVendorCode());
                if (vendor == null) {
                    context.addError(WsResponseCode.INVALID_VENDOR_CODE, "Invalid Vendor");
                } else {
                    String currencyCode = StringUtils.isNotBlank(request.getCurrencyCode()) ? request.getCurrencyCode()
                            : ConfigurationManager.getInstance().getConfiguration(TenantSystemConfiguration.class).getBaseCurrency();
                    GetCurrencyConversionRateResponse conversionRateResponse = currencyService.getCurrencyConversionRate(new GetCurrencyConversionRateRequest(currencyCode));
                    if (!conversionRateResponse.isSuccessful()) {
                        context.addErrors(conversionRateResponse.getErrors());
                    } else {
                        purchaseOrder = new PurchaseOrder();
                        purchaseOrder.setCurrencyCode(currencyCode);
                        purchaseOrder.setCurrencyConversionRate(conversionRateResponse.getConversionRate());
                        purchaseOrder.setFromParty(CacheManager.getInstance().getCache(FacilityCache.class).getCurrentFacility());
                        purchaseOrder.setStatusCode(PurchaseOrder.StatusCode.CREATED.name());
                        purchaseOrder.setVendor(vendor);
                    }
                }
            }

            if (!context.hasErrors()) {
                addOrEditPurchaseOrderItemsInternal(context, purchaseOrder, request.getPurchaseOrderItems(), request.getLogisticCharges(),
                        request.getLogisticChargesDivisionMethod());
                if (!context.hasErrors()) {
                    for (PurchaseOrderItem purchaseOrderItem : purchaseOrder.getPurchaseOrderItems()) {
                        response.getPurchaseOrderItems().add(preparePurchaseOrderItemDTO(purchaseOrderItem));
                    }
                    response.setLogisticCharges(purchaseOrder.getLogisticCharges());
                    response.setLogisticChargesDivisionMethod(purchaseOrder.getLogisticChargesDivisionMethod());
                    response.setSuccessful(true);
                }
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    public AddOrEditPurchaseOrderItemsResponse addOrEditPurchaseOrderItems(AddOrEditPurchaseOrderItemsRequest request) {
        AddOrEditPurchaseOrderItemsResponse response = new AddOrEditPurchaseOrderItemsResponse();
        ValidationContext context = request.validate();
        PurchaseOrder purchaseOrder = null;
        if (!context.hasErrors()) {
            if (StringUtils.isBlank(request.getPurchaseOrderCode())) {
                context.addError(WsResponseCode.INVALID_PURCHASE_ORDER_CODE, "Purchase Order Code may not be empty");
            } else {
                purchaseOrder = purchaseDao.getPurchaseOrderByCode(request.getPurchaseOrderCode(), true);
                if (purchaseOrder == null) {
                    context.addError(WsResponseCode.INVALID_PURCHASE_ORDER_CODE, "Invalid Purchase Order Code");
                } else if (!PurchaseOrder.StatusCode.CREATED.name().equals(purchaseOrder.getStatusCode())) {
                    context.addError(WsResponseCode.INVALID_PURCHASE_ORDER_STATE, "Invalid Purchase Order State");
                } else {
                    addOrEditPurchaseOrderItemsInternal(context, purchaseOrder, request.getPurchaseOrderItems(), request.getLogisticCharges(),
                            request.getLogisticChargesDivisionMethod());
                    if (!context.hasErrors()) {
                        purchaseOrder = purchaseDao.updatePuchaseOrder(purchaseOrder);
                        for (PurchaseOrderItem purchaseOrderItem : purchaseOrder.getPurchaseOrderItems()) {
                            response.getPurchaseOrderItems().add(preparePurchaseOrderItemDTO(purchaseOrderItem));
                        }
                        response.setLogisticCharges(purchaseOrder.getLogisticCharges());
                        response.setLogisticChargesDivisionMethod(purchaseOrder.getLogisticChargesDivisionMethod());
                        response.setSuccessful(true);
                    }
                }
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    private void addOrEditPurchaseOrderItemsInternal(ValidationContext context, PurchaseOrder purchaseOrder, List<WsPurchaseOrderItem> purchaseOrderItems,
            BigDecimal logisticCharges, String logisticChargesDivisionMethod) {
        Map<Integer, PurchaseOrderItem> itemTypeIdToPurchaseOrderItem = purchaseOrder.getPurchaseOrderItems().stream().collect(
                Collectors.toMap(poi -> poi.getItemType().getId(), Function.identity()));

        Set<Integer> existingItemTypeIds = purchaseOrder.getPurchaseOrderItems().stream().map(poi -> poi.getItemType().getId()).collect(Collectors.toSet());
        ItemType itemType;
        VendorItemType vendorItemType;
        Map<String, WsPurchaseOrderItem> itemTypeToWsPurchaseOrderItems = new HashMap<>();
        Vendor vendor = vendorService.getVendorById(purchaseOrder.getVendor().getId());
        for (WsPurchaseOrderItem wsPurchaseOrderItem : purchaseOrderItems) {
            itemType = catalogService.getItemTypeBySkuCode(wsPurchaseOrderItem.getItemSKU());
            if (itemType == null) {
                context.addError(WsResponseCode.INVALID_ITEM_TYPE, "Invalid Item Type:" + wsPurchaseOrderItem.getItemSKU());
            } else {
                existingItemTypeIds.remove(itemType.getId());
                TaxType taxType = null;
                if (StringUtils.isNotBlank(wsPurchaseOrderItem.getTaxTypeCode())) {
                    taxType = ConfigurationManager.getInstance().getConfiguration(TaxConfiguration.class).getTaxTypeByCode(wsPurchaseOrderItem.getTaxTypeCode());
                    if (taxType == null) {
                        context.addError(WsResponseCode.INVALID_TAX_TYPE_CODE, "Invalid Tax Type:" + wsPurchaseOrderItem.getTaxTypeCode());
                        break;
                    }
                }
                if (itemTypeToWsPurchaseOrderItems.containsKey(wsPurchaseOrderItem.getItemSKU())) {
                    context.addError(WsResponseCode.ITEM_TYPE_ALREADY_ADDED_TO_PURCHASE_ORDER, "Item type " + itemType.getSkuCode() + " already added to purchase order");
                    break;
                } else {
                    purchaseOrder.setLogisticCharges(logisticCharges);
                    purchaseOrder.setLogisticChargesDivisionMethod(logisticChargesDivisionMethod);
                    if (itemTypeIdToPurchaseOrderItem.containsKey(itemType.getId())) {
                        PurchaseOrderItem purchaseOrderItem = itemTypeIdToPurchaseOrderItem.get(itemType.getId());
                        if (purchaseOrderItem.getReceivedQuantity() > wsPurchaseOrderItem.getQuantity()) {
                            context.addError(WsResponseCode.ITEM_QUANTITY_CANNOT_BE_LESS_THAN_RECEIVED, "Item quantity cannot be less");
                        } else {
                            try {
                                preparePurchaseOrderItem(purchaseOrderItem, purchaseOrder, vendor, itemType, purchaseOrderItem.getVendorSkuCode(), taxType, wsPurchaseOrderItem);
                            } catch (IllegalStateException e) {
                                context.addError(WsResponseCode.INVALID_REQUEST, e.getMessage());
                            }
                        }
                    } else {
                        vendorItemType = catalogService.getVendorItemType(purchaseOrder.getVendor().getId(), itemType.getId());
                        if (vendorItemType == null) {
                            context.addError(WsResponseCode.ITEM_NOT_CONFIGURED_TO_PURCHASE_FROM_VENDOR,
                                    "Item not configured to purchase from vendor sku:" + wsPurchaseOrderItem.getItemSKU());
                        } else {
                            PurchaseOrderItem purchaseOrderItem = new PurchaseOrderItem();
                            try {
                                preparePurchaseOrderItem(purchaseOrderItem, purchaseOrder, vendor, itemType, vendorItemType.getVendorSkuCode(), taxType, wsPurchaseOrderItem);
                            } catch (IllegalStateException e) {
                                context.addError(WsResponseCode.INVALID_REQUEST, e.getMessage());
                            }
                        }
                    }
                }
            }
            itemTypeToWsPurchaseOrderItems.put(wsPurchaseOrderItem.getItemSKU(), wsPurchaseOrderItem);
        }

        // See if any existing purchase order item has been removed
        if (!context.hasErrors() && !existingItemTypeIds.isEmpty()) {
            for (Integer itemTypeId : existingItemTypeIds) {
                PurchaseOrderItem purchaseOrderItem = itemTypeIdToPurchaseOrderItem.get(itemTypeId);
                purchaseOrder.getPurchaseOrderItems().remove(purchaseOrderItem);
                purchaseDao.removePurchaseOrderItem(purchaseOrderItem);
            }
        }
        if (!context.hasErrors()) {
            BigDecimal totalQuantity = new BigDecimal(0);
            BigDecimal totalPrice = new BigDecimal(0);
            for (PurchaseOrderItem purchaseOrderItem : purchaseOrder.getPurchaseOrderItems()) {
                totalQuantity = totalQuantity.add(new BigDecimal(purchaseOrderItem.getQuantity()));
                totalPrice = totalPrice.add(purchaseOrderItem.getTotal());
            }

            for (PurchaseOrderItem poItem : purchaseOrder.getPurchaseOrderItems()) {
                if (PurchaseOrder.LogisticChargesDivisionMethod.EQUALLY.name().equals(purchaseOrder.getLogisticChargesDivisionMethod())) {
                    poItem.setLogisticCharges(NumberUtils.divide(purchaseOrder.getLogisticCharges(), purchaseOrder.getPurchaseOrderItems().size()));
                } else if (PurchaseOrder.LogisticChargesDivisionMethod.QUANTITY.name().equals(purchaseOrder.getLogisticChargesDivisionMethod())) {
                    poItem.setLogisticCharges(NumberUtils.divide(NumberUtils.multiply(purchaseOrder.getLogisticCharges(), new BigDecimal(poItem.getQuantity())), totalQuantity));
                } else if (PurchaseOrder.LogisticChargesDivisionMethod.ROW_TOTAL.name().equals(purchaseOrder.getLogisticChargesDivisionMethod())) {
                    poItem.setLogisticCharges(NumberUtils.divide(NumberUtils.multiply(purchaseOrder.getLogisticCharges(), poItem.getTotal()), totalPrice));
                }
            }
        }

    }

    @Override
    @Transactional
    public List<PurchaseOrderStatus> getPurchaseOrderStatuses() {
        return purchaseDao.getPurchaseOrderStatuses();
    }

    @Override
    public List<PurchaseOrder> searchPurchaseOrder(String keyword) {
        return purchaseDao.searchPurchaseOrder(keyword);
    }

    @Override
    @Transactional
    public AddUpdateReorderConfigResponse persistReorderConfig(AddUpdateReorderConfigRequest request) {
        AddUpdateReorderConfigResponse response = new AddUpdateReorderConfigResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            ItemType itemType = catalogService.getItemTypeBySkuCode(request.getItemSkuCode());
            if (itemType == null) {
                context.addError(WsResponseCode.INVALID_ITEM_TYPE, "No item type for skuCode [" + request.getItemSkuCode() + "]");
                response.setSuccessful(false);
            } else {
                ReorderConfig reorderConfig = reorderConfigDao.getReorderConfigForItemID(itemType.getId());
                if (reorderConfig != null) {
                    reorderConfig.setReorderQuantity(request.getReorderQuantity());
                    reorderConfig.setReorderThreshold(request.getReorderThreshold());
                    reorderConfig.setUpdated(DateUtils.getCurrentTime());
                    reorderConfig = reorderConfigDao.update(reorderConfig);
                } else {
                    reorderConfig = new ReorderConfig();
                    reorderConfig.setFacility(CacheManager.getInstance().getCache(FacilityCache.class).getCurrentFacility());
                    reorderConfig.setItemType(itemType);
                    reorderConfig.setReorderQuantity(request.getReorderQuantity());
                    reorderConfig.setReorderThreshold(request.getReorderThreshold());
                    reorderConfig.setCreated(DateUtils.getCurrentTime());
                    reorderConfig.setUpdated(DateUtils.getCurrentTime());
                    reorderConfig = reorderConfigDao.create(reorderConfig);
                    WsReorderConfig wsReorderConfig = new WsReorderConfig(reorderConfig.getItemType().getId(), reorderConfig.getItemType().getName(),
                            reorderConfig.getFacility().getDisplayName(), reorderConfig.getReorderQuantity(), reorderConfig.getReorderThreshold());
                    response.setWsReorderConfig(wsReorderConfig);
                }
                response.setSuccessful(true);
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    public void createReorderConfigForItem(int itemTypeId) {
        ItemType itemType = catalogService.getNonBundledItemTypeById(itemTypeId);
        ReorderConfig reorderConfig = new ReorderConfig();
        reorderConfig.setItemType(itemType);
        reorderConfig.setReorderQuantity(0);
        reorderConfig.setReorderThreshold(0);
        reorderConfig.setCreated(DateUtils.getCurrentTime());
        reorderConfig = reorderConfigDao.create(reorderConfig);
    }

    @Override
    @Transactional
    public DeleteReorderConfigResponse deteleReorderConfig(DeleteReorderConfigRequest request) {
        DeleteReorderConfigResponse response = new DeleteReorderConfigResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            ItemType itemType = catalogService.getItemTypeBySkuCode(request.getItemSkuCode());
            ReorderConfig config = reorderConfigDao.getReorderConfigForItemID(itemType.getId());
            if (config == null) {
                context.addError(WsResponseCode.INVALID_ITEM_ID, "No reorder config for provided item [" + request.getItemSkuCode() + "]");
                response.setSuccessful(false);
            } else {
                reorderConfigDao.delete(config);
                response.setSuccessful(true);
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    public List<WsReorderConfig> getAllReorderConfig() {
        List<WsReorderConfig> reorderConfigs = new ArrayList<WsReorderConfig>();
        for (ReorderConfig reorderConfig : reorderConfigDao.getAllReorderConfigs()) {
            WsReorderConfig wsReorderConfig = new WsReorderConfig(reorderConfig.getItemType().getId(), reorderConfig.getItemType().getName(),
                    reorderConfig.getFacility().getDisplayName(), reorderConfig.getReorderQuantity(), reorderConfig.getReorderThreshold());
            reorderConfigs.add(wsReorderConfig);
        }
        return reorderConfigs;
    }

    @Override
    public GetReorderConfigByItemTypeResponse getReorderConfigByItemType(GetReorderConfigByItemTypeRequest request) {
        GetReorderConfigByItemTypeResponse response = new GetReorderConfigByItemTypeResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            ItemType itemType = catalogService.getItemTypeBySkuCode(request.getItemSkuCode());
            ReorderConfig reorderConfig = reorderConfigDao.getReorderConfigForItemID(itemType.getId());
            if (reorderConfig == null) {
                context.addError(WsResponseCode.INVALID_ITEM_ID, "No Reorderconfig for item id [" + request.getItemSkuCode() + "]");
                response.setSuccessful(false);
            } else {
                WsReorderConfig wsReorderConfig = new WsReorderConfig(reorderConfig.getItemType().getId(), reorderConfig.getItemType().getName(),
                        reorderConfig.getFacility().getDisplayName(), reorderConfig.getReorderQuantity(), reorderConfig.getReorderThreshold());
                response.setWsReorderConfig(wsReorderConfig);
                response.setSuccessful(true);
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    public GetPurchaseOrdersResponse getPurchaseOrders(GetPurchaseOrdersRequest request) {
        GetPurchaseOrdersResponse response = new GetPurchaseOrdersResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            List<String> purchaseOrderCodes = new ArrayList<String>();
            for (PurchaseOrder purchaseOrder : purchaseDao.getPurchaseOrderCodes(request)) {
                purchaseOrderCodes.add(purchaseOrder.getCode());
            }
            response.setPurchaseOrderCodes(purchaseOrderCodes);
            response.setSuccessful(true);
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    public EditAdvanceShippingNoticeResponse editAdvanceShippingNoticeMetadata(EditAdvanceShippingNoticeRequest request) {
        EditAdvanceShippingNoticeResponse response = new EditAdvanceShippingNoticeResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            AdvanceShippingNotice asn = purchaseDao.getASNByCode(request.getAdvanceShippingNoticeCode());
            if (asn == null) {
                context.addError(WsResponseCode.INVALID_CODE, "Invalid Advance Shipping Notice Code:" + request.getAdvanceShippingNoticeCode());
            } else {
                if (request.getCustomFieldValues() != null) {
                    CustomFieldUtils.setCustomFieldValues(asn,
                            CustomFieldUtils.getCustomFieldValues(context, AdvanceShippingNotice.class.getName(), request.getCustomFieldValues()), false);
                }
                response.setSuccessful(true);
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional(readOnly = true)
    public GetPurchaseOrderActivitiesResponse getPurchaseOrderActivities(GetPurchaseOrderActivitiesRequest request) {
        GetPurchaseOrderActivitiesResponse response = new GetPurchaseOrderActivitiesResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            PurchaseOrder purchaseOrder = purchaseDao.getPurchaseOrderByCode(request.getPurchaseOrderCode());
            if (purchaseOrder == null) {
                context.addError(WsResponseCode.INVALID_PURCHASE_ORDER_CODE);
            } else {
                List<ActivityDTO> activities = new ArrayList<>();
                FetchEntityActivityRequest fetchEntityActivityRequest = new FetchEntityActivityRequest();
                fetchEntityActivityRequest.setEntityName(ActivityEntityEnum.PURCHASE_ORDER.getName());
                fetchEntityActivityRequest.setEntityIdentifier(purchaseOrder.getCode());
                fetchEntityActivityRequest.setSort(false);
                FetchEntityActivityResponse fetchEntityActivityResponse = activityLogService.getActivityLogsbyEntityId(fetchEntityActivityRequest);
                if (fetchEntityActivityResponse.isSuccessful()) {
                    for (ActivityMetaVO activityMetaVO : fetchEntityActivityResponse.getEntityActivityLogs()) {
                        ActivityDTO activityDTO = new ActivityDTO();
                        prepareActivityDTO(activityDTO, activityMetaVO);
                        activities.add(activityDTO);
                    }
                }
                GetCommentsRequest getCommentsRequest = new GetCommentsRequest();
                Facility currfacility = CacheManager.getInstance().getCache(FacilityCache.class).getFacilityById(UserContext.current().getFacilityId());
                getCommentsRequest.setReferenceIdentifier("PO-" + purchaseOrder.getCode() + "-" + currfacility.getCode());
                GetCommentsResponse getCommentsResponse = commentsService.getComments(getCommentsRequest);
                if (getCommentsResponse.isSuccessful()) {
                    for (UserCommentDTO userCommentDTO : getCommentsResponse.getUserCommentDTOs()) {
                        ActivityDTO activityDTO = new ActivityDTO();
                        prepareActivityDTO(activityDTO, userCommentDTO);
                        activities.add(activityDTO);
                    }
                }
                Collections.sort(activities);
                response.setActivities(activities);
                response.setSuccessful(true);
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    private void prepareActivityDTO(ActivityDTO activityDTO, UserCommentDTO userCommentDTO) {
        activityDTO.setActivityType(ActivityTypeEnum.COMMENT.name());
        activityDTO.setLog(userCommentDTO.getComment());
        activityDTO.setCreated(userCommentDTO.getCreated());
        if (userCommentDTO.isSystemGenerated()) {
            activityDTO.setUsername("System");
        } else {
            activityDTO.setUsername(userCommentDTO.getUsername());
        }
    }

    private void prepareActivityDTO(ActivityDTO activityDTO, ActivityMetaVO activityMetaVO) {
        activityDTO.setActivityType(activityMetaVO.getActivityType());
        activityDTO.setLog(activityMetaVO.getLog());
        activityDTO.setCreated(activityMetaVO.getCreated());
        if (StringUtils.isNotBlank(activityMetaVO.getUsername())) {
            activityDTO.setUsername(activityMetaVO.getUsername());
        } else if (StringUtils.isNotBlank(activityMetaVO.getApiUsername())) {
            activityDTO.setUsername(activityMetaVO.getApiUsername());
        } else {
            activityDTO.setUsername("System");
        }
    }

    private class ChangeVendorInventoryResponse {
        List<VendorInventoryLogVO> logs = new ArrayList<>();

        public List<VendorInventoryLogVO> getLogs() {
            return logs;
        }

        @SuppressWarnings("unused")
        public void setLogs(List<VendorInventoryLogVO> logs) {
            this.logs = logs;
        }

    }
}
