/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 29, 2012
 *  @author singla
 */
package com.uniware.services.purchase;

import com.uniware.core.api.purchase.WsPurchaseOrderItem;
import com.uniware.core.entity.ItemType;
import com.uniware.core.entity.PurchaseOrderItem;
import com.uniware.core.entity.TaxType;
import com.uniware.core.entity.Vendor;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import com.unifier.core.utils.DateUtils.DateRange;
import com.uniware.core.api.inflow.GetPOInflowReceiptsRequest;
import com.uniware.core.api.inflow.GetPOInflowReceiptsResponse;
import com.uniware.core.api.purchase.AddItemsToPurchaseCartRequest;
import com.uniware.core.api.purchase.AddItemsToPurchaseCartResponse;
import com.uniware.core.api.purchase.AddOrEditPurchaseOrderItemsRequest;
import com.uniware.core.api.purchase.AddOrEditPurchaseOrderItemsResponse;
import com.uniware.core.api.purchase.AddUpdateReorderConfigRequest;
import com.uniware.core.api.purchase.AddUpdateReorderConfigResponse;
import com.uniware.core.api.purchase.AdvanceShippingNoticeDTO;
import com.uniware.core.api.purchase.AmendPurchaseOrderRequest;
import com.uniware.core.api.purchase.AmendPurchaseOrderResponse;
import com.uniware.core.api.purchase.ApprovePurchaseOrderRequest;
import com.uniware.core.api.purchase.ApprovePurchaseOrderResponse;
import com.uniware.core.api.purchase.BulkApprovePurchaseOrderRequest;
import com.uniware.core.api.purchase.BulkApprovePurchaseOrderResponse;
import com.uniware.core.api.purchase.CancelPurchaseOrderRequest;
import com.uniware.core.api.purchase.CancelPurchaseOrderResponse;
import com.uniware.core.api.purchase.CheckoutPurchaseCartRequest;
import com.uniware.core.api.purchase.CheckoutPurchaseCartResponse;
import com.uniware.core.api.purchase.ClonePurchaseOrderRequest;
import com.uniware.core.api.purchase.ClonePurchaseOrderResponse;
import com.uniware.core.api.purchase.ClosePurchaseOrderRequest;
import com.uniware.core.api.purchase.ClosePurchaseOrderResponse;
import com.uniware.core.api.purchase.CreateASNRequest;
import com.uniware.core.api.purchase.CreateASNResponse;
import com.uniware.core.api.purchase.CreateApprovedPurchaseOrderRequest;
import com.uniware.core.api.purchase.CreateApprovedPurchaseOrderResponse;
import com.uniware.core.api.purchase.CreatePurchaseOrderRequest;
import com.uniware.core.api.purchase.CreatePurchaseOrderResponse;
import com.uniware.core.api.purchase.DeleteReorderConfigRequest;
import com.uniware.core.api.purchase.DeleteReorderConfigResponse;
import com.uniware.core.api.purchase.EditAdvanceShippingNoticeRequest;
import com.uniware.core.api.purchase.EditAdvanceShippingNoticeResponse;
import com.uniware.core.api.purchase.EmailPurchaseOrderRequest;
import com.uniware.core.api.purchase.EmailPurchaseOrderResponse;
import com.uniware.core.api.purchase.GetBackOrderItemsRequest;
import com.uniware.core.api.purchase.GetBackOrderItemsResponse;
import com.uniware.core.api.purchase.GetNextPurchaseOrderCodeResponse;
import com.uniware.core.api.purchase.GetPurchaseOrderActivitiesRequest;
import com.uniware.core.api.purchase.GetPurchaseOrderActivitiesResponse;
import com.uniware.core.api.purchase.GetPurchaseOrderRequest;
import com.uniware.core.api.purchase.GetPurchaseOrderResponse;
import com.uniware.core.api.purchase.GetPurchaseOrdersRequest;
import com.uniware.core.api.purchase.GetPurchaseOrdersResponse;
import com.uniware.core.api.purchase.GetReorderConfigByItemTypeRequest;
import com.uniware.core.api.purchase.GetReorderConfigByItemTypeResponse;
import com.uniware.core.api.purchase.GetReorderItemsRequest;
import com.uniware.core.api.purchase.GetReorderItemsResponse;
import com.uniware.core.api.purchase.GetVendorPurchaseOrderDetailRequest;
import com.uniware.core.api.purchase.GetVendorPurchaseOrderDetailResponse;
import com.uniware.core.api.purchase.GetVendorPurchaseOrdersRequest;
import com.uniware.core.api.purchase.GetVendorPurchaseOrdersResponse;
import com.uniware.core.api.purchase.LookupPurchaseOrderLineItemsRequest;
import com.uniware.core.api.purchase.LookupPurchaseOrderLineItemsResponse;
import com.uniware.core.api.purchase.PurchaseCartDTO;
import com.uniware.core.api.purchase.PurchaseOrderDTO;
import com.uniware.core.api.purchase.RejectPurchaseOrderRequest;
import com.uniware.core.api.purchase.RejectPurchaseOrderResponse;
import com.uniware.core.api.purchase.RemovePurchaseCartItemsRequest;
import com.uniware.core.api.purchase.RemovePurchaseCartItemsResponse;
import com.uniware.core.api.purchase.RemoveVendorItemsFromCartRequest;
import com.uniware.core.api.purchase.RemoveVendorItemsFromCartResponse;
import com.uniware.core.api.purchase.SearchASNRequest;
import com.uniware.core.api.purchase.SearchASNResponse;
import com.uniware.core.api.purchase.SearchInflowReceiptRequest;
import com.uniware.core.api.purchase.SearchInflowReceiptResponse;
import com.uniware.core.api.purchase.SearchItemTypeBackOrdersRequest;
import com.uniware.core.api.purchase.SearchItemTypeBackOrdersResponse;
import com.uniware.core.api.purchase.SearchItemTypeForCartRequest;
import com.uniware.core.api.purchase.SearchItemTypeForCartResponse;
import com.uniware.core.api.purchase.SearchPurchaseOrderRequest;
import com.uniware.core.api.purchase.SearchPurchaseOrderResponse;
import com.uniware.core.api.purchase.SendPurchaseOrderForApprovalRequest;
import com.uniware.core.api.purchase.SendPurchaseOrderForApprovalResponse;
import com.uniware.core.api.purchase.UpdatePurchaseOrderMetadataRequest;
import com.uniware.core.api.purchase.UpdatePurchaseOrderMetadataResponse;
import com.uniware.core.api.purchase.WsReorderConfig;
import com.uniware.core.entity.PurchaseOrder;
import com.uniware.core.entity.PurchaseOrderStatus;

/**
 * @author singla
 */
/**
 * @author unicom
 *
 */
/**
 * @author unicom
 */
public interface IPurchaseService {

    /**
     * @param request
     * @return
     */
    CreatePurchaseOrderResponse createPurchaseOrder(CreatePurchaseOrderRequest request);

    void preparePurchaseOrderItem(PurchaseOrderItem purchaseOrderItem, PurchaseOrder purchaseOrder, Vendor vendor,
            ItemType itemType, String vendorSkuCode, TaxType taxType, WsPurchaseOrderItem wsPurchaseOrderItem);

    /**
     * @param request
     * @return
     * @throws FileNotFoundException
     * @throws IOException
     */
    ApprovePurchaseOrderResponse approvePurchaseOrder(ApprovePurchaseOrderRequest request);

    /**
     * @param purchaseOrderCode
     * @return
     */
    PurchaseOrder getPurchaseOrderByCode(String purchaseOrderCode);

    GetPurchaseOrderResponse getPurchaseOrderDetail(GetPurchaseOrderRequest request);

    /**
     * @param vendorId
     * @return
     */
    List<PurchaseOrderDTO> getVendorPurchaseOrders(String vendorCode);

    List<PurchaseOrderDTO> getPurchaseOrdersWaitingForApproval();

    List<PurchaseOrderDTO> getActivePurchaseOrders();

    /**
     * @param purchaseOrderCode
     * @return
     */
    PurchaseOrder getPurchaseOrderDetailedByCode(String purchaseOrderCode);

    /**
     * @param request
     * @return
     */
    ClosePurchaseOrderResponse closePurchaseOrder(ClosePurchaseOrderRequest request);

    /**
     * @param request
     * @return
     */
    AddItemsToPurchaseCartResponse addItemsToPurchaseCart(AddItemsToPurchaseCartRequest request);

    /**
     * @param request
     * @return
     */
    RemovePurchaseCartItemsResponse removePurchaseCartItem(RemovePurchaseCartItemsRequest request);

    /**
     * @param request
     * @return
     */
    CheckoutPurchaseCartResponse checkoutPurchaseCart(CheckoutPurchaseCartRequest request);

    /**
     * @param request
     * @return
     */
    SearchItemTypeForCartResponse searchItemType(SearchItemTypeForCartRequest request);

    /**
     * @param userId
     * @return
     */
    PurchaseCartDTO getPurchaseCart(Integer userId);

    /**
     * @param getBackOrderItemsRequest
     * @return
     */
    GetBackOrderItemsResponse getBackOrderItems(GetBackOrderItemsRequest getBackOrderItemsRequest);

    /**
     * @param request
     * @return
     */
    SearchPurchaseOrderResponse searchPurchaseOrders(SearchPurchaseOrderRequest request);

    /**
     * @param request
     * @return
     */
    GetPOInflowReceiptsResponse getInflowReceipts(GetPOInflowReceiptsRequest request);

    /**
     * 
     */
    void autoCompletePurchaseOrders();

    /**
     * @param request
     * @return
     */
    CreateASNResponse createASN(CreateASNRequest request);

    /**
     * @param request
     * @return
     */
    SearchASNResponse searchASN(SearchASNRequest request);

    /**
     * @param asnCode
     * @return
     */
    AdvanceShippingNoticeDTO getASN(String asnCode);

    /**
     * @param request
     * @return
     */
    CancelPurchaseOrderResponse cancelPurchaseOrder(CancelPurchaseOrderRequest request);

    /**
     * @param request
     * @return
     */
    SearchInflowReceiptResponse searchInflowReceipt(SearchInflowReceiptRequest request);

    /**
     * @param request
     * @return
     */
    SendPurchaseOrderForApprovalResponse sendPurchaseOrderForApproval(SendPurchaseOrderForApprovalRequest request);

    /**
     * @param request
     * @return
     */
    RejectPurchaseOrderResponse rejectPurchaseOrder(RejectPurchaseOrderRequest request);

    BulkApprovePurchaseOrderResponse bulkApprovePurchaseOrders(BulkApprovePurchaseOrderRequest request);

    /**
     * @param request
     * @return
     */
    AmendPurchaseOrderResponse amendPurchaseOrder(AmendPurchaseOrderRequest request);

    /**
     * @param request
     * @return
     */
    UpdatePurchaseOrderMetadataResponse updatePurchaseOrderMetadata(UpdatePurchaseOrderMetadataRequest request);

    /**
     * @param request
     * @return
     */
    ClonePurchaseOrderResponse clonePurchaseOrder(ClonePurchaseOrderRequest request);

    /**
     * @param request
     * @return
     */
    SearchItemTypeBackOrdersResponse getItemTypeBackOrders(SearchItemTypeBackOrdersRequest request);

    /**
     * @param request
     * @return
     */
    EmailPurchaseOrderResponse emailPurchaseOrder(EmailPurchaseOrderRequest request);

    /**
     * @param statusCode
     * @param dateRange
     * @param vendorCode
     * @return
     */
    List<PurchaseOrder> getPurchaseOrderByStatusInRange(String statusCode, DateRange dateRange, String vendorCode);

    /**
     * @param request
     * @return
     */
    CreateApprovedPurchaseOrderResponse createApprovedPurchaseOrder(CreateApprovedPurchaseOrderRequest request);

    /**
     * @param request
     * @return
     */
    GetReorderItemsResponse getReorderItems(GetReorderItemsRequest request);

    /**
     * @param request
     * @return
     */
    RemoveVendorItemsFromCartResponse removeVendorItemsFromCart(RemoveVendorItemsFromCartRequest request);

    GetVendorPurchaseOrdersResponse getVendorPurchaseOrders(GetVendorPurchaseOrdersRequest request);

    GetVendorPurchaseOrderDetailResponse getVendorPurchaseOrderDetail(GetVendorPurchaseOrderDetailRequest request);

    AddOrEditPurchaseOrderItemsResponse addOrEditPurchaseOrderItems(AddOrEditPurchaseOrderItemsRequest request);

    AddOrEditPurchaseOrderItemsResponse addOrEditPurchaseOrderItemsReadOnly(AddOrEditPurchaseOrderItemsRequest request);

    List<PurchaseOrderStatus> getPurchaseOrderStatuses();

    List<PurchaseOrder> searchPurchaseOrder(String keyword);

    /**
     * Add or update a reorder config
     * 
     * @param request
     * @return response contains error list or persisted object
     */
    AddUpdateReorderConfigResponse persistReorderConfig(AddUpdateReorderConfigRequest request);

    /**
     * delete reorder config for a item
     * 
     * @param request
     * @return response contains error if any
     */
    DeleteReorderConfigResponse deteleReorderConfig(DeleteReorderConfigRequest request);

    List<WsReorderConfig> getAllReorderConfig();

    /**
     * get reorder configs for a item
     * 
     * @param request
     * @return resposne contain list of reorder config or errors if any
     */
    GetReorderConfigByItemTypeResponse getReorderConfigByItemType(GetReorderConfigByItemTypeRequest request);

    void createReorderConfigForItem(int itemTypeId);

    GetReorderItemsResponse getReorderItemsNew(GetReorderItemsRequest request);

    GetPurchaseOrdersResponse getPurchaseOrders(GetPurchaseOrdersRequest request);

    List<PurchaseOrder> getPurchaseOrderByStatus(String statusCode, String vendorCode);

    EditAdvanceShippingNoticeResponse editAdvanceShippingNoticeMetadata(EditAdvanceShippingNoticeRequest request);

    PurchaseOrder getPurchaseOrderById(Integer purchaseOrderId, boolean lock);

    PurchaseOrder lockOnPurchaseOrder(Integer purchaseOrderId);

    GetNextPurchaseOrderCodeResponse getNextPurchaseOrderCode();

    LookupPurchaseOrderLineItemsResponse lookupPurchaseOrderLineItems(LookupPurchaseOrderLineItemsRequest request);

    GetPurchaseOrderActivitiesResponse getPurchaseOrderActivities(GetPurchaseOrderActivitiesRequest request);

}
