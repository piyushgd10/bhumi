/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, May 8, 2015
 *  @author harsh
 */
package com.uniware.services.purchase;

import com.uniware.core.api.purchaseOrder.display.GetPurchaseOrderSummaryRequest;
import com.uniware.core.api.purchaseOrder.display.GetPurchaseOrderSummaryResponse;

/**
 * @author harsh
 */
public interface IPurchaseOrderDisplayService {

    GetPurchaseOrderSummaryResponse getPurchaseOrderSummary(GetPurchaseOrderSummaryRequest request);

}
