/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *  @version     1.0, Jan 20, 2012
 *  @author singla
 */
package com.uniware.services.invoice.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import com.uniware.core.entity.BundleItemType;
import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.unifier.core.annotation.audit.LogActivity;
import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.utils.CollectionUtils;
import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.NumberUtils;
import com.unifier.core.utils.StringUtils;
import com.unifier.core.utils.ValidatorUtils;
import com.unifier.services.users.IUsersService;
import com.unifier.services.utils.CustomFieldUtils;
import com.uniware.core.api.invoice.GenerateOrUpdateInvoiceRequest;
import com.uniware.core.api.invoice.GenerateOrUpdateInvoiceResponse;
import com.uniware.core.api.invoice.GetTaxDetailsRequest;
import com.uniware.core.api.invoice.GetTaxDetailsResponse;
import com.uniware.core.api.invoice.InvoiceDTO;
import com.uniware.core.api.invoice.InvoiceItemDTO;
import com.uniware.core.api.invoice.TaxDetailDTO;
import com.uniware.core.api.model.WsInvoice;
import com.uniware.core.api.model.WsInvoiceItem;
import com.uniware.core.api.model.WsTaxPercentageDetail;
import com.uniware.core.api.tax.GetTaxPercentagesRequest;
import com.uniware.core.api.tax.GetTaxPercentagesResponse;
import com.uniware.core.api.tax.TaxPercentageDTO;
import com.uniware.core.api.tax.WsTaxClass;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.cache.FacilityCache;
import com.uniware.core.cache.LocationCache;
import com.uniware.core.entity.Bundle;
import com.uniware.core.entity.Channel;
import com.uniware.core.entity.Customer;
import com.uniware.core.entity.Facility;
import com.uniware.core.entity.Invoice;
import com.uniware.core.entity.InvoiceItem;
import com.uniware.core.entity.InvoiceItemTax;
import com.uniware.core.entity.ItemType;
import com.uniware.core.entity.Party;
import com.uniware.core.entity.PartyAddress;
import com.uniware.core.entity.PartyAddressType;
import com.uniware.core.entity.SaleOrderItem;
import com.uniware.core.entity.Sequence;
import com.uniware.core.entity.TaxType;
import com.uniware.core.utils.Constants;
import com.uniware.dao.invoice.IInvoiceDao;
import com.uniware.services.billing.party.IBillingPartyService;
import com.uniware.services.bundle.IBundleService;
import com.uniware.services.cache.ChannelCache;
import com.uniware.services.catalog.ICatalogService;
import com.uniware.services.channel.IChannelService;
import com.uniware.services.common.ISequenceGenerator;
import com.uniware.services.configuration.SystemConfiguration;
import com.uniware.services.customer.ICustomerService;
import com.uniware.services.invoice.IInvoiceService;
import com.uniware.services.party.IPartyService;
import com.uniware.services.tax.ITaxService;
import com.uniware.services.tax.ITaxTypeService;

import edu.umd.cs.findbugs.annotations.Nullable;

/**
 * @author singla
 */

@Service("invoiceService")
public class InvoiceServiceImpl implements IInvoiceService {

    public static final String   DEFAULT_TAX_TYPE_CODE = "DEFAULT";
    private static final Logger  LOG                   = LoggerFactory.getLogger(InvoiceServiceImpl.class);
    @Autowired
    private ICatalogService      catalogService;

    @Autowired
    private IInvoiceDao          invoiceDao;

    @Autowired
    private IUsersService        usersService;

    @Autowired
    private ICustomerService     customerService;

    @Autowired
    private IPartyService        partyService;

    @Autowired
    private ISequenceGenerator   iSequenceGenerator;

    @Autowired
    private ITaxService          taxService;

    @Autowired
    private ITaxTypeService      taxTypeService;

    @Autowired
    private IBillingPartyService billingPartyService;

    @Autowired
    private IChannelService      channelService;

    @Autowired
    private IBundleService       bundleService;

    @Override
    public GenerateOrUpdateInvoiceResponse createInvoice(GenerateOrUpdateInvoiceRequest request) {
        GenerateOrUpdateInvoiceResponse response = new GenerateOrUpdateInvoiceResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            WsInvoice wsInvoice = request.getInvoice();
            Party fromParty = partyService.getPartyByCode(wsInvoice.getFromPartyCode());
            Party toParty = StringUtils.isNotBlank(wsInvoice.getToPartyCode()) ? partyService.getPartyByCode(wsInvoice.getToPartyCode()) : null;
            Facility facility = CacheManager.getInstance().getCache(FacilityCache.class).getCurrentFacility();
            if (fromParty == null) {
                context.addError(WsResponseCode.INVALID_PARTY_CODE, "Invalid from party code : " + wsInvoice.getFromPartyCode());
            } else if (StringUtils.isNotBlank(wsInvoice.getToPartyCode()) && toParty == null) {
                context.addError(WsResponseCode.INVALID_PARTY_CODE, "Invalid to party code : " + wsInvoice.getToPartyCode());
            } else {
                Invoice invoice = new Invoice();
                invoice.setFromParty(fromParty);
                invoice.setToParty(toParty);
                invoice.setUser(usersService.getUserById(wsInvoice.getUserId()));
                Map<String, TaxPercentageDTO> skuToTaxPercentageDTOForSellingPrice = computeTaxPercentageMapForInvoiceItems(context, wsInvoice, fromParty, toParty,
                        request.getInvoice().getSource(), true);
                Map<String, TaxPercentageDTO> skuToTaxPercentageDTOForOtherCharges = computeTaxPercentageMapForInvoiceItems(context, wsInvoice, fromParty, toParty,
                        request.getInvoice().getSource(), false);

                if (!context.hasErrors()) {
                    PartyAddress warehouseShippingAddress = facility.getPartyAddressByType(PartyAddressType.Code.SHIPPING.name());
                    if (warehouseShippingAddress != null) {
                        if (taxService.isGSTEnabled() && warehouseShippingAddress.isIndiaAddress() && fromParty.getSignatureBinaryObject() == null) {
                            context.addError(WsResponseCode.SIGNATURE_NOT_UPLOADED);
                        } else {
                            for (WsInvoiceItem wsInvoiceItem : wsInvoice.getInvoiceItems()) {
                                if (!context.hasErrors()) {
                                    BigDecimal perUnitCharges = NumberUtils.divide(wsInvoiceItem.getTotal(), wsInvoiceItem.getQuantity());
                                    String invoiceItemIdentifier = WsInvoiceItem.generateInvoiceItemIdentifier(wsInvoice.getSource(), wsInvoiceItem.getSkuCode(),
                                            wsInvoiceItem.getBundleSkuCode(), perUnitCharges);
                                    InvoiceItem invoiceItem = prepareInvoiceItem(wsInvoice, wsInvoiceItem, fromParty,
                                            skuToTaxPercentageDTOForSellingPrice.get(invoiceItemIdentifier), skuToTaxPercentageDTOForOtherCharges.get(invoiceItemIdentifier),
                                            context);
                                    invoiceItem.setInvoice(invoice);
                                    invoice.getInvoiceItems().add(invoiceItem);
                                } else {
                                    break;
                                }
                            }
                            if (!context.hasErrors()) {
                                if (StringUtils.isNotBlank(wsInvoice.getCode())) {
                                    invoice.setCode(wsInvoice.getCode());
                                } else {
                                    invoice.setCode(generateInvoiceCodeFromSequence(wsInvoice.getType()));
                                }
                                invoice.setDisplayCode(StringUtils.isNotBlank(wsInvoice.getDisplayCode()) ? wsInvoice.getDisplayCode() : invoice.getCode());
                                invoice.setChannelCreated(wsInvoice.getChannelCreated());
                                invoice.setCreated(DateUtils.getCurrentTime());
                                invoice.setType(wsInvoice.getType());
                                invoiceDao.addInvoice(invoice);
                                response.setInvoiceCode(invoice.getCode());
                                response.setInvoiceDisplayCode(invoice.getDisplayCode());
                            } else {
                                LOG.debug("Errors added in context during preparation of invoice items: {}", context.getErrors());
                            }
                        }
                    } else {
                        context.addError(WsResponseCode.INVALID_WAREHOUSE_CONFIGURATION);
                        return null;
                    }

                }
            }
        }
        if (context.hasErrors()) {
            response.addErrors(context.getErrors());
            response.addWarnings(context.getWarnings());
        } else {
            response.setSuccessful(true);
        }
        return response;
    }

    /**
     * @param wsInvoice
     * @param wsInvoiceItem
     * @param fromParty
     * @param taxPercentageDTOForSellingPrice
     * @param taxPercentageDTOForOtherCharges
     * @param context
     * @return
     */
    private InvoiceItem prepareInvoiceItem(WsInvoice wsInvoice, WsInvoiceItem wsInvoiceItem, Party fromParty, TaxPercentageDTO taxPercentageDTOForSellingPrice,
            TaxPercentageDTO taxPercentageDTOForOtherCharges, ValidationContext context) {
        InvoiceItem invoiceItem = new InvoiceItem();
        ItemType itemType = catalogService.getItemTypeBySkuCode(wsInvoiceItem.getSkuCode());
        if (StringUtils.isNotBlank(wsInvoiceItem.getBundleSkuCode())) {
            Bundle bundle = bundleService.getBundleBySkuCode(wsInvoiceItem.getBundleSkuCode());
            invoiceItem.setBundle(bundle);
        }
        invoiceItem.setProduct(itemType.getSkuCode());
        invoiceItem.setDescription(itemType.getName());
        invoiceItem.setQuantity(wsInvoiceItem.getQuantity());
        invoiceItem.setSellerSkuCode(itemType.getSkuCode());
        invoiceItem.setItemType(itemType);
        invoiceItem.setChannelProductId(wsInvoiceItem.getChannelProductId());
        invoiceItem.setShippingCharges(ValidatorUtils.getOrDefaultValue(wsInvoiceItem.getShippingCharges(), BigDecimal.ZERO));
        invoiceItem.setShippingMethodCharges(ValidatorUtils.getOrDefaultValue(wsInvoiceItem.getShippingMethodCharges(), BigDecimal.ZERO));
        invoiceItem.setGiftWrapCharges(ValidatorUtils.getOrDefaultValue(wsInvoiceItem.getGiftWrapCharges(), BigDecimal.ZERO));
        invoiceItem.setCashOnDeliveryCharges(ValidatorUtils.getOrDefaultValue(wsInvoiceItem.getCashOnDeliveryCharges(), BigDecimal.ZERO));
        invoiceItem.setTotal(wsInvoiceItem.getTotal());
        TaxDetailDTO taxDetailDTOForSellingPrice = getTaxDetails(itemType, invoiceItem.getTotal(), fromParty, wsInvoice.getDestinationStateCode(),
                wsInvoice.getDestinationCountryCode(), wsInvoice.getTaxExempted(), wsInvoice.getCformProvided(), taxPercentageDTOForSellingPrice, wsInvoice.getSource());
        LOG.debug("Evaluated tax details for sku {} : {}", itemType.getSkuCode(), taxDetailDTOForSellingPrice);
        invoiceItem.setTaxTypeCode(taxDetailDTOForSellingPrice.getTaxTypeCode());
        InvoiceItemTax invoiceItemTaxForSellingPrice = prepareInvoiceItemTax(InvoiceItemTax.CostHead.SELLING_PRICE, taxPercentageDTOForSellingPrice, taxDetailDTOForSellingPrice);
        invoiceItem.getInvoiceItemTaxes().add(invoiceItemTaxForSellingPrice);
        invoiceItemTaxForSellingPrice.setInvoiceItem(invoiceItem);

        if (invoiceItem.getShippingCharges().compareTo(BigDecimal.ZERO) == 1) {
            TaxDetailDTO taxDetailDTO = getTaxDetails(itemType, invoiceItem.getShippingCharges(), fromParty, wsInvoice.getDestinationStateCode(),
                    wsInvoice.getDestinationCountryCode(), wsInvoice.getTaxExempted(), wsInvoice.getCformProvided(), taxPercentageDTOForOtherCharges, wsInvoice.getSource());
            InvoiceItemTax taxOnShippingCharges = prepareInvoiceItemTax(InvoiceItemTax.CostHead.SHIPPING_CHARGES, taxPercentageDTOForOtherCharges, taxDetailDTO);
            invoiceItem.setShippingCharges(invoiceItem.getShippingCharges().subtract(taxOnShippingCharges.getTotalTaxAmount()));
            invoiceItem.getInvoiceItemTaxes().add(taxOnShippingCharges);
            taxOnShippingCharges.setInvoiceItem(invoiceItem);
        }
        if (invoiceItem.getGiftWrapCharges().compareTo(BigDecimal.ZERO) == 1) {
            TaxDetailDTO taxDetailDTO = getTaxDetails(itemType, invoiceItem.getGiftWrapCharges(), fromParty, wsInvoice.getDestinationStateCode(),
                    wsInvoice.getDestinationCountryCode(), wsInvoice.getTaxExempted(), wsInvoice.getCformProvided(), taxPercentageDTOForOtherCharges, wsInvoice.getSource());
            InvoiceItemTax taxOnGiftWrapCharges = prepareInvoiceItemTax(InvoiceItemTax.CostHead.GIFT_WRAP_CHARGES, taxPercentageDTOForOtherCharges, taxDetailDTO);
            invoiceItem.setGiftWrapCharges(invoiceItem.getGiftWrapCharges().subtract(taxOnGiftWrapCharges.getTotalTaxAmount()));
            invoiceItem.getInvoiceItemTaxes().add(taxOnGiftWrapCharges);
            taxOnGiftWrapCharges.setInvoiceItem(invoiceItem);
        }
        if (invoiceItem.getCashOnDeliveryCharges().compareTo(BigDecimal.ZERO) == 1) {
            TaxDetailDTO taxDetailDTO = getTaxDetails(itemType, invoiceItem.getCashOnDeliveryCharges(), fromParty, wsInvoice.getDestinationStateCode(),
                    wsInvoice.getDestinationCountryCode(), wsInvoice.getTaxExempted(), wsInvoice.getCformProvided(), taxPercentageDTOForOtherCharges, wsInvoice.getSource());
            InvoiceItemTax taxOnCashOnDeliveryCharges = prepareInvoiceItemTax(InvoiceItemTax.CostHead.CASH_ON_DELIVERY_CHARGES, taxPercentageDTOForOtherCharges, taxDetailDTO);
            invoiceItem.setCashOnDeliveryCharges(invoiceItem.getCashOnDeliveryCharges().subtract(taxOnCashOnDeliveryCharges.getTotalTaxAmount()));
            invoiceItem.getInvoiceItemTaxes().add(taxOnCashOnDeliveryCharges);
            taxOnCashOnDeliveryCharges.setInvoiceItem(invoiceItem);
        }

        BigDecimal taxAmount = invoiceItemTaxForSellingPrice.getTotalTaxAmount();
        if (ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).isSellingPricesTaxExclusive()) {
            invoiceItem.setTotal(invoiceItem.getTotal().add(taxAmount).add(invoiceItem.getShippingCharges()));
        }
        invoiceItemTaxForSellingPrice.setServiceTax(ValidatorUtils.getOrDefaultValue(wsInvoiceItem.getServiceTax(), BigDecimal.ZERO));
        invoiceItem.setSubtotal(invoiceItem.getTotal().subtract(taxAmount));
        invoiceItem.setUnitPrice(invoiceItem.getSubtotal().divide(new BigDecimal(invoiceItem.getQuantity()), 2, RoundingMode.HALF_EVEN));
        invoiceItem.setDiscount(ValidatorUtils.getOrDefaultValue(wsInvoiceItem.getDiscount(), BigDecimal.ZERO));
        invoiceItem.setPrepaidAmount(ValidatorUtils.getOrDefaultValue(wsInvoiceItem.getPrepaidAmount(), BigDecimal.ZERO));
        invoiceItem.setVoucherValue(ValidatorUtils.getOrDefaultValue(wsInvoiceItem.getVoucherValue(), BigDecimal.ZERO));
        invoiceItem.setStoreCredit(ValidatorUtils.getOrDefaultValue(wsInvoiceItem.getStoreCredit(), BigDecimal.ZERO));
        invoiceItem.setAdditionalInfo(wsInvoiceItem.getAdditionalInfo());
        invoiceItem.setItemDetails(StringUtils.isNotBlank(wsInvoiceItem.getItemDetails()) ? wsInvoiceItem.getItemDetails() : computeItemDetails(wsInvoiceItem.getSaleOrderItems()));
        if (wsInvoiceItem.getCustomFieldValues() != null) {
            Map<String, Object> customFieldValues = CustomFieldUtils.getCustomFieldValues(context, InvoiceItem.class.getName(), wsInvoiceItem.getCustomFieldValues());
            CustomFieldUtils.setCustomFieldValues(invoiceItem, customFieldValues);
        }
        return invoiceItem;
    }

    private InvoiceItemTax prepareInvoiceItemTax(InvoiceItemTax.CostHead costHead, TaxPercentageDTO taxPercentageDTO, TaxDetailDTO taxDetailDTO) {
        InvoiceItemTax invoiceItemTax = new InvoiceItemTax();
        invoiceItemTax.setCostHead(costHead);
        invoiceItemTax.setTaxTypeCode(taxDetailDTO.getTaxTypeCode());
        invoiceItemTax.setVat(taxDetailDTO.getVat());
        invoiceItemTax.setAdditionalTax(taxDetailDTO.getAdditionalTax());
        invoiceItemTax.setTaxPercentage(taxDetailDTO.getTaxPercentage());
        invoiceItemTax.setAdditionalTaxPercentage(taxDetailDTO.getAdditionalTaxPercentage());
        invoiceItemTax.setCst(taxDetailDTO.getCst());
        invoiceItemTax.setCentralGst(taxDetailDTO.getCentralGst());
        invoiceItemTax.setCentralGstPercentage(taxPercentageDTO.getCentralGst());
        invoiceItemTax.setStateGst(taxDetailDTO.getStateGst());
        invoiceItemTax.setStateGstPercentage(taxPercentageDTO.getStateGst());
        invoiceItemTax.setUnionTerritoryGst(taxDetailDTO.getUnionTerritoryGst());
        invoiceItemTax.setUnionTerritoryGstPercentage(taxPercentageDTO.getUnionTerritoryGst());
        invoiceItemTax.setIntegratedGst(taxDetailDTO.getIntegratedGst());
        invoiceItemTax.setIntegratedGstPercentage(taxPercentageDTO.getIntegratedGst());
        invoiceItemTax.setCompensationCess(taxDetailDTO.getCompensationCess());
        invoiceItemTax.setCompensationCessPercentage(taxPercentageDTO.getCompensationCess());
        return invoiceItemTax;
    }

    // TODO : compute item detail json as per already running format
    private String computeItemDetails(List<WsInvoiceItem.WsSaleOrderItem> saleOrderItems) {
        if (!CollectionUtils.isEmpty(saleOrderItems)) {
            saleOrderItems.forEach(wsSaleOrderItem -> {
            });
        }
        return null;
    }

    private TaxDetailDTO getTaxDetails(ItemType itemType, BigDecimal sellingPrice, Party fromParty, String shippingState, String shippingCountry, Boolean taxExempted,
            Boolean providesCForm, TaxPercentageDTO taxPercentageDTO, WsInvoice.Source source) {
        TaxDetailDTO taxDetailDTO = new TaxDetailDTO();
        taxDetailDTO.setItemSku(itemType.getSkuCode());
        Facility facility = CacheManager.getInstance().getCache(FacilityCache.class).getCurrentFacility();
        PartyAddress warehouseShippingAddress = facility.getPartyAddressByType(PartyAddressType.Code.SHIPPING.name());
        SystemConfiguration configuration = ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class);
        if (warehouseShippingAddress.isIndiaAddress() && (LocationCache.COUNTRY_CODE_INDIA.equals(shippingCountry) || configuration.isTaxOnInternationalOrderAllowed())
                && !taxExempted && !fromParty.isTaxExempted()) {
            if (WsInvoice.Source.SHIPPING_PACKAGE.equals(source) && ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).isSellingPricesTaxExclusive()) {
                if (LocationCache.COUNTRY_CODE_INDIA.equalsIgnoreCase(shippingCountry) || configuration.isTaxOnInternationalOrderAllowed()) {
                    if (warehouseShippingAddress.getStateCode().equals(shippingState)) {
                        taxDetailDTO.setVat(NumberUtils.getPercentageFromBase(sellingPrice, taxPercentageDTO.getVat()));
                        taxDetailDTO.setAdditionalTax(NumberUtils.getPercentageFromBase(taxDetailDTO.getVat(), taxPercentageDTO.getAdditionalTax()));
                        taxDetailDTO.setTaxPercentage(taxPercentageDTO.getVat());
                        taxDetailDTO.setAdditionalTaxPercentage(taxPercentageDTO.getAdditionalTax());
                    } else if (providesCForm) {
                        taxDetailDTO.setCst(NumberUtils.getPercentageFromBase(sellingPrice, taxPercentageDTO.getCstFormc()));
                        taxDetailDTO.setTaxPercentage(taxPercentageDTO.getCstFormc());
                    } else {
                        taxDetailDTO.setCst(NumberUtils.getPercentageFromBase(sellingPrice, taxPercentageDTO.getCst()));
                        taxDetailDTO.setAdditionalTax(NumberUtils.getPercentageFromBase(taxDetailDTO.getCst(), taxPercentageDTO.getAdditionalTax()));
                        taxDetailDTO.setTaxPercentage(taxPercentageDTO.getCst());
                        taxDetailDTO.setAdditionalTaxPercentage(taxPercentageDTO.getAdditionalTax());
                    }
                }
                //gst
                taxDetailDTO.setCentralGst(NumberUtils.getPercentageFromBase(sellingPrice, taxPercentageDTO.getCentralGst()));
                taxDetailDTO.setStateGst(NumberUtils.getPercentageFromBase(sellingPrice, taxPercentageDTO.getStateGst()));
                taxDetailDTO.setUnionTerritoryGst(NumberUtils.getPercentageFromBase(sellingPrice, taxPercentageDTO.getUnionTerritoryGst()));
                taxDetailDTO.setIntegratedGst(NumberUtils.getPercentageFromBase(sellingPrice, taxPercentageDTO.getIntegratedGst()));
                taxDetailDTO.setCompensationCess(NumberUtils.getPercentageFromBase(sellingPrice, taxPercentageDTO.getCompensationCess()));
            } else {
                if (LocationCache.COUNTRY_CODE_INDIA.equalsIgnoreCase(shippingCountry) || configuration.isTaxOnInternationalOrderAllowed()) {
                    if (warehouseShippingAddress.getStateCode().equals(shippingState)) {
                        BigDecimal vatWithAdditionalTax = NumberUtils.getPercentageFromTotal(sellingPrice,
                                taxPercentageDTO.getVat().multiply(new BigDecimal(100).add(taxPercentageDTO.getAdditionalTax()).divide(new BigDecimal(100))));
                        taxDetailDTO.setAdditionalTax(NumberUtils.getPercentageFromTotal(vatWithAdditionalTax, taxPercentageDTO.getAdditionalTax()));
                        taxDetailDTO.setVat(vatWithAdditionalTax.subtract(taxDetailDTO.getAdditionalTax()));
                        taxDetailDTO.setTaxPercentage(taxPercentageDTO.getVat());
                        taxDetailDTO.setAdditionalTaxPercentage(taxPercentageDTO.getAdditionalTax());
                    } else if (providesCForm) {
                        taxDetailDTO.setCst(NumberUtils.getPercentageFromTotal(sellingPrice, taxPercentageDTO.getCstFormc()));
                        taxDetailDTO.setTaxPercentage(taxPercentageDTO.getCstFormc());
                    } else {
                        BigDecimal cstWithAdditionalTax = NumberUtils.getPercentageFromTotal(sellingPrice,
                                taxPercentageDTO.getCst().multiply(new BigDecimal(100).add(taxPercentageDTO.getAdditionalTax()).divide(new BigDecimal(100))));
                        taxDetailDTO.setAdditionalTax(NumberUtils.getPercentageFromTotal(cstWithAdditionalTax, taxPercentageDTO.getAdditionalTax()));
                        taxDetailDTO.setCst(cstWithAdditionalTax.subtract(taxDetailDTO.getAdditionalTax()));
                        taxDetailDTO.setTaxPercentage(taxPercentageDTO.getCst());
                        taxDetailDTO.setAdditionalTaxPercentage(taxPercentageDTO.getAdditionalTax());
                    }
                }
                //gst
                BigDecimal totalGstTaxPercentage = taxPercentageDTO.getTotalGstTaxPercentage();
                BigDecimal totalTax = NumberUtils.getPercentageFromTotal(sellingPrice, totalGstTaxPercentage);
                if (totalGstTaxPercentage.compareTo(BigDecimal.ZERO) > 0) {
                    taxDetailDTO.setCentralGst(
                            taxPercentageDTO.getCentralGst().multiply(totalTax).divide(totalGstTaxPercentage, 2, RoundingMode.HALF_EVEN).setScale(2, RoundingMode.HALF_EVEN));
                    taxDetailDTO.setStateGst(
                            taxPercentageDTO.getStateGst().multiply(totalTax).divide(totalGstTaxPercentage, 2, RoundingMode.HALF_EVEN).setScale(2, RoundingMode.HALF_EVEN));
                    taxDetailDTO.setUnionTerritoryGst(taxPercentageDTO.getUnionTerritoryGst().multiply(totalTax).divide(totalGstTaxPercentage, 2, RoundingMode.HALF_EVEN).setScale(
                            2, RoundingMode.HALF_EVEN));
                    taxDetailDTO.setIntegratedGst(
                            taxPercentageDTO.getIntegratedGst().multiply(totalTax).divide(totalGstTaxPercentage, 2, RoundingMode.HALF_EVEN).setScale(2, RoundingMode.HALF_EVEN));
                    taxDetailDTO.setCompensationCess(
                            taxPercentageDTO.getCompensationCess().multiply(totalTax).divide(totalGstTaxPercentage, 2, RoundingMode.HALF_EVEN).setScale(2, RoundingMode.HALF_EVEN));
                }
            }
        }
        taxDetailDTO.setTaxTypeCode(taxPercentageDTO.getAppliedTaxTypeCode());
        return taxDetailDTO;
    }

    @Override
    @LogActivity
    public GenerateOrUpdateInvoiceResponse updateInvoice(GenerateOrUpdateInvoiceRequest request) {
        GenerateOrUpdateInvoiceResponse response = new GenerateOrUpdateInvoiceResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            if (StringUtils.isBlank(request.getInvoice().getCode())) {
                context.addError(WsResponseCode.INVALID_REQUEST, "Invoice code cannot be empty");
            } else {
                Invoice invoice = getInvoiceByCode(request.getInvoice().getCode());
                if (invoice == null) {
                    context.addError(WsResponseCode.INVALID_REQUEST, "No Invoice exist with given code");
                } else {
                    LOG.debug("Updating invoice : {}", invoice.getCode());
                    updateInvoiceInternal(request, response, context, invoice);
                    LOG.info("Updated invoice : {}", invoice.getCode());
                }
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        } else {
            response.setSuccessful(true);
        }
        return response;
    }

    private GenerateOrUpdateInvoiceResponse updateInvoiceInternal(GenerateOrUpdateInvoiceRequest request, GenerateOrUpdateInvoiceResponse response, ValidationContext context,
            Invoice invoice) {
        Iterator<InvoiceItem> iterator = invoice.getInvoiceItems().iterator();
        while (iterator.hasNext()) {
            InvoiceItem invoiceItem = iterator.next();
            invoiceDao.removeInvoiceItem(invoiceItem);
            iterator.remove();
        }
        LOG.debug("Computing tax percentage map for invoice : {} with from party : {}, to party : {}", invoice.getCode(), invoice.getFromParty().getCode(),
                invoice.getToParty() != null ? invoice.getToParty().getCode() : null);
        Map<String, TaxPercentageDTO> skuToTaxPercentageDTOForSellingPrice = computeTaxPercentageMapForInvoiceItems(context, request.getInvoice(), invoice.getFromParty(),
                invoice.getToParty(), request.getInvoice().getSource(), true);
        Map<String, TaxPercentageDTO> skuToTaxPercentageDTOForOtherCharges = computeTaxPercentageMapForInvoiceItems(context, request.getInvoice(), invoice.getFromParty(),
                invoice.getToParty(), request.getInvoice().getSource(), false);
        if (!context.hasErrors()) {
            PartyAddress warehouseShippingAddress = invoice.getFromParty().getPartyAddressByType(PartyAddressType.Code.SHIPPING.name());
            if (taxService.isGSTEnabled() && warehouseShippingAddress.isIndiaAddress() && invoice.getFromParty().getSignatureBinaryObject() == null) {
                context.addError(WsResponseCode.SIGNATURE_NOT_UPLOADED);
            } else {
                if (warehouseShippingAddress != null) {
                    for (WsInvoiceItem wsInvoiceItem : request.getInvoice().getInvoiceItems()) {
                        if (!context.hasErrors()) {
                            BigDecimal perUnitCharges = NumberUtils.divide(wsInvoiceItem.getTotal(), wsInvoiceItem.getQuantity());
                            String invoiceItemIdentifier = WsInvoiceItem.generateInvoiceItemIdentifier(request.getInvoice().getSource(), wsInvoiceItem.getSkuCode(),
                                    wsInvoiceItem.getBundleSkuCode(), perUnitCharges);
                            InvoiceItem invoiceItem = prepareInvoiceItem(request.getInvoice(), wsInvoiceItem, invoice.getFromParty(),
                                    skuToTaxPercentageDTOForSellingPrice.get(invoiceItemIdentifier), skuToTaxPercentageDTOForOtherCharges.get(invoiceItemIdentifier), context);
                            invoiceItem.setInvoice(invoice);
                            invoice.getInvoiceItems().add(invoiceItem);
                        } else {
                            break;
                        }
                    }
                    if (!context.hasErrors()) {
                        invoiceDao.updateInvoice(invoice);
                        response.setInvoiceCode(invoice.getCode());
                        response.setInvoiceDisplayCode(invoice.getDisplayCode());
                    } else {
                        LOG.debug("Errors added in context while preparing invoice item during update invoice: {}", context.getErrors());
                    }
                } else {
                    context.addError(WsResponseCode.INVALID_WAREHOUSE_CONFIGURATION);
                }
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        } else {
            response.setSuccessful(true);
        }
        return response;
    }

    private String generateInvoiceCodeFromSequence(Invoice.Type type) {
        String invoiceCode = null;
        if (taxService.isGSTEnabled()) {
            String sequenceName;
            switch (type) {
                case SALE:
                    sequenceName = Sequence.Name.SALE_INVOICE.name();
                    break;
                case PURCHASE:
                    sequenceName = Sequence.Name.PURCHASE_INVOICE.name();
                    break;
                case GATEPASS:
                    sequenceName = Sequence.Name.GATEPASS_INVOICE.name();
                    break;
                case SALE_RETURN:
                    sequenceName = Sequence.Name.SALE_RETURN_INVOICE.name();
                    break;
                case PURCHASE_RETURN:
                    sequenceName = Sequence.Name.PURCHASE_RETURN_INVOICE.name();
                    break;
                case GATEPASS_RETURN:
                    sequenceName = Sequence.Name.GATEPASS_RETURN_INVOICE.name();
                    break;
                case DELIVERY_CHALLAN:
                    sequenceName = Sequence.Name.DELIVERY_CHALLAN.name();
                    break;
                default:
                    sequenceName = Sequence.Name.SALE_INVOICE.name();
            }
            invoiceCode = iSequenceGenerator.generateNextInSameTransaction(sequenceName);
        }
        return invoiceCode;
    }

    @Override
    @Transactional(readOnly = true)
    public GetTaxDetailsResponse getTaxDetails(GetTaxDetailsRequest request) {
        GetTaxDetailsResponse response = new GetTaxDetailsResponse();
        ValidationContext context = request.validate();
        Channel channel = null;
        Customer customer = null;
        if (!context.hasErrors()) {
            channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelByCode(request.getChannelCode());
            if (channel == null) {
                context.addError(WsResponseCode.INVALID_CHANNEL_CODE);
            } else if (StringUtils.isNotBlank(request.getCustomerCode())) {
                customer = customerService.getCustomerByCode(request.getCustomerCode());
                if (customer == null) {
                    context.addError(WsResponseCode.INVALID_CUSTOMER_CODE);
                }
            }
        }
        if (!context.hasErrors()) {
            Facility currentFacility = CacheManager.getInstance().getCache(FacilityCache.class).getCurrentFacility();
            PartyAddress warehouseShippingAddress = currentFacility.getPartyAddressByType(PartyAddressType.Code.SHIPPING.name());
            Party fromParty = (channel.getBillingParty() != null) ? billingPartyService.getBillingPartyById(channel.getBillingParty().getId()) : currentFacility;
            PartyAddress toPartyAddress = null;
            if (StringUtils.isNotBlank(request.getShippingState()) && StringUtils.isNotBlank(request.getShippingCountry())) {
                toPartyAddress = new PartyAddress();
                toPartyAddress.setCountryCode(request.getShippingCountry());
                toPartyAddress.setStateCode(request.getShippingState());
            } else {
                toPartyAddress = warehouseShippingAddress;
            }
            boolean taxExempted = (customer != null) && customer.isTaxExempted();
            boolean providesCForm = (customer != null) && customer.isProvidesCform();
            List<GetTaxDetailsResponse.TaxDTO> taxDTOs = new ArrayList<>();
            if (request.getItemTypes() != null) {
                Map<String, BigDecimal> skuToSellingPrice = request.getItemTypes().stream().collect(
                        Collectors.toMap(GetTaxDetailsRequest.WsItemType::getItemSku, GetTaxDetailsRequest.WsItemType::getSellingPrice, BigDecimal::max));
                Map<String, TaxPercentageDTO> identifierToPercentageDTOMap = prepareSkuToTaxPercentageDTO(skuToSellingPrice, context,
                        fromParty.getPartyAddressByType(PartyAddressType.Code.SHIPPING.name()), toPartyAddress, providesCForm, request.getSource());
                if (!context.hasErrors()) {
                    for (GetTaxDetailsRequest.WsItemType wsItemType : request.getItemTypes()) {
                        ItemType itemType = catalogService.getItemTypeBySkuCode(wsItemType.getItemSku());
                        if (itemType == null) {
                            context.addError(WsResponseCode.INVALID_ITEM_TYPE, WsResponseCode.INVALID_ITEM_TYPE.message() + wsItemType.getItemSku());
                            break;
                        } else {
                            TaxDetailDTO taxDetailDTO = getTaxDetails(itemType, wsItemType.getSellingPrice(), fromParty, toPartyAddress.getStateCode(),
                                    toPartyAddress.getCountryCode(), taxExempted, providesCForm, identifierToPercentageDTOMap.get(itemType.getSkuCode()), request.getSource());
                            if (!context.hasErrors()) {
                                BigDecimal totalTax = taxDetailDTO.getCompensationCess().add(taxDetailDTO.getIntegratedGst()).add(taxDetailDTO.getStateGst()).add(
                                        taxDetailDTO.getCentralGst()).add(taxDetailDTO.getVat()).add(taxDetailDTO.getCst()).add(taxDetailDTO.getUnionTerritoryGst());
                                taxDTOs.add(new GetTaxDetailsResponse.TaxDTO(taxDetailDTO.getItemSku(), taxDetailDTO.getTaxTypeCode(),
                                        totalTax.multiply(new BigDecimal(wsItemType.getQuantity())),
                                        taxDetailDTO.getAdditionalTax().multiply(new BigDecimal(wsItemType.getQuantity()))));
                            } else {
                                break;
                            }
                        }
                    }

                }
            } else if (request.getChannelItemTypes() != null) {
                ItemType itemType = catalogService.getItemTypeBySkuCode(ItemType.UNKNOWN_SKU);
                for (GetTaxDetailsRequest.WsChannelItemType wsChannelItemType : request.getChannelItemTypes()) {
                    TaxDetailDTO taxDetailDTO = getTaxDetails(itemType, wsChannelItemType.getChannelProductId(), wsChannelItemType.getSellingPrice(), toPartyAddress.getStateCode(),
                            toPartyAddress.getCountryCode(), wsChannelItemType.getTaxPercentage());
                    BigDecimal totalTax = taxDetailDTO.getCompensationCess().add(taxDetailDTO.getIntegratedGst()).add(taxDetailDTO.getStateGst()).add(
                            taxDetailDTO.getCentralGst()).add(taxDetailDTO.getVat()).add(taxDetailDTO.getCst());
                    taxDTOs.add(new GetTaxDetailsResponse.TaxDTO(taxDetailDTO.getItemSku(), taxDetailDTO.getTaxTypeCode(),
                            totalTax.multiply(new BigDecimal(wsChannelItemType.getQuantity())),
                            taxDetailDTO.getAdditionalTax().multiply(new BigDecimal(wsChannelItemType.getQuantity()))));
                }
            } else {
                context.addError(WsResponseCode.INVALID_REQUEST, "Either of itemTypes or channelItemTypes has to be specified");
            }
            if (!context.hasErrors()) {
                response.setTaxDetails(taxDTOs);
                response.setSuccessful(true);
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    /**
     * Prepares a map of SKU to {@link TaxPercentageDTO}
     * 
     * @param context
     * @param wsInvoice
     * @param fromParty
     * @param toParty
     * @param source
     * @param computeForSellingPrice
     * @return
     */
    private Map<String, TaxPercentageDTO> computeTaxPercentageMapForInvoiceItems(ValidationContext context, WsInvoice wsInvoice, Party fromParty, Party toParty,
            WsInvoice.Source source, boolean computeForSellingPrice) {
        Map<String, WsTaxClass> taxClassMap = new HashMap<>();
        Map<String, TaxPercentageDTO> skuToTaxPercentageDTO = new HashMap<>(wsInvoice.getInvoiceItems().size());
        for (WsInvoiceItem wsInvoiceItem : wsInvoice.getInvoiceItems()) {
            BigDecimal perUnitSellingPrice = NumberUtils.divide(wsInvoiceItem.getTotal(), wsInvoiceItem.getQuantity());
            if (computeForSellingPrice) {
                prepareTaxClassMapForTaxForPrice(skuToTaxPercentageDTO, wsInvoiceItem, perUnitSellingPrice, taxClassMap, wsInvoice.getSource());
            } else {
                prepareTaxClassMapForTaxOnOtherCharges(wsInvoice.getChannelCode(), skuToTaxPercentageDTO, wsInvoiceItem, perUnitSellingPrice, taxClassMap, wsInvoice.getSource());
            }
        }
        if (taxClassMap.size() > 0) {
            GetTaxPercentagesResponse getTaxPercentagesResponse = getGetTaxPercentagesFromTaxService(wsInvoice, fromParty, toParty, source, taxClassMap);
            if (getTaxPercentagesResponse.hasErrors()) {
                context.addErrors(getTaxPercentagesResponse.getErrors());
            } else {
                skuToTaxPercentageDTO.putAll(getTaxPercentagesResponse.getIdentifierToTaxPercentages());
            }
        }
        LOG.debug("Calculated skuToTaxPercentageDTO {}", skuToTaxPercentageDTO.toString());
        return skuToTaxPercentageDTO;
    }

    /**
     * Prepare SKU to TaxClass map for given invoice item for all cost heads other than
     * {@link com.uniware.core.entity.InvoiceItemTax.CostHead#SELLING_PRICE}.
     * <p>
     * The value of other charges is immaterial for tax rate computation as tax rate for all "other charges" would be
     * fixed irrespective its value. Hence, it is fixed at 1.
     * </p>
     * 
     * @param channelCode
     * @param skuToTaxPercentageDTO
     * @param wsInvoiceItem
     * @param perUnitSellingPrice perUnitSellingPrice of invoice item. It is </b>not</b> the per unit value of
     *            respective {@link InvoiceItemTax.CostHead} because if fixed rate of taxes on other charges is not
     *            defined, then the tax rate on other charges should be same as one used for
     * @param source
     */
    private void prepareTaxClassMapForTaxOnOtherCharges(String channelCode, Map<String, TaxPercentageDTO> skuToTaxPercentageDTO, WsInvoiceItem wsInvoiceItem,
            BigDecimal perUnitSellingPrice, Map<String, WsTaxClass> taxClassMap, WsInvoice.Source source) {
        if (StringUtils.isNotBlank(channelCode) && channelService.isFixedGSTOnOtherChargesApplicable(channelCode)) {
            // 1. Because gstTaxType is pre-defined (irrespective of prices), the value of sellingPrice in WsTaxClass is immaterial
            // 2. Setting taxTypeCode as null assuming that GST will never be rolled back
            TaxType configuredTaxTypeForOtherCharges = taxTypeService.getTaxTypeByCode(Constants.GST_SERVICE_CHARGE_TAX_CLASS_CODE);
            WsTaxClass taxClass = new WsTaxClass(null, configuredTaxTypeForOtherCharges.getCode(), BigDecimal.ONE);
            taxClassMap.put(WsInvoiceItem.generateInvoiceItemIdentifier(source, wsInvoiceItem.getSkuCode(), wsInvoiceItem.getBundleSkuCode(), perUnitSellingPrice), taxClass);
        } else {
            // if not defined on source, then divide proportionately
            prepareTaxClassMapForTaxForPrice(skuToTaxPercentageDTO, wsInvoiceItem, perUnitSellingPrice, taxClassMap, source);
        }
    }

    /**
     * Prepare SKU to TaxClass map for given invoice item and price. If taxes were pre-defined on {@link WsInvoiceItem},
     * populate them into skuToTaxPercentageDTO.
     * 
     * @param skuToTaxPercentageDTO the DTO that we're trying to populate
     * @param taxClassMap the map in which a tax class is generated and put if tax percentage detail for
     *            {@code wsInvoiceItem} is null.
     * @param source
     */
    private void prepareTaxClassMapForTaxForPrice(Map<String, TaxPercentageDTO> skuToTaxPercentageDTO, WsInvoiceItem wsInvoiceItem, BigDecimal price,
            Map<String, WsTaxClass> taxClassMap, WsInvoice.Source source) {
        LOG.info("prepareTaxClassMapForTaxForPrice called for invoice item sku: {} and invoice item tax detail: {}", wsInvoiceItem.getSkuCode(),
                wsInvoiceItem.getTaxPercentageDetail());
        String invoiceItemIdentifier = WsInvoiceItem.generateInvoiceItemIdentifier(source, wsInvoiceItem.getSkuCode(), wsInvoiceItem.getBundleSkuCode(), price);
        if (wsInvoiceItem.getTaxPercentageDetail() != null) {
            skuToTaxPercentageDTO.computeIfAbsent(invoiceItemIdentifier, k -> prepareTaxPercentageDTO(wsInvoiceItem.getTaxPercentageDetail()));
        } else if (taxClassMap.get(invoiceItemIdentifier) == null) {
            ItemType itemType = null;
            if(StringUtils.isNotBlank(wsInvoiceItem.getBundleSkuCode())){
                itemType = catalogService.getAllItemTypeBySkuCode(wsInvoiceItem.getBundleSkuCode());
                Bundle bundle = bundleService.getBundleBySkuCode(itemType.getSkuCode());
                Optional<BundleItemType> bundleItemTypeOptional = bundle.getBundleItemTypes().stream().filter(bit -> bit.getItemType().getSkuCode().equals(wsInvoiceItem.getSkuCode())).findFirst();
                if(bundleItemTypeOptional.isPresent()){
                    BundleItemType bundleItemType = bundleItemTypeOptional.get();
                    price = NumberUtils.multiply(NumberUtils.divide(price, bundleItemType.getPriceRatio()), bundleItemType.getQuantity());
                }
            } else {
                itemType = catalogService.getAllItemTypeBySkuCode(wsInvoiceItem.getSkuCode());
            }
            String taxTypeCode = catalogService.getTaxTypeCode(itemType, false);
            String gstTaxTypeCode = catalogService.getTaxTypeCode(itemType, true);
            WsTaxClass taxClass = new WsTaxClass(taxTypeCode, gstTaxTypeCode, price);
            taxClassMap.put(invoiceItemIdentifier, taxClass);
        }
    }

    /**
     * Fetch applicable tax percentages from tax service.
     * 
     * @param wsInvoice
     * @param fromParty origin address of invoice
     * @param toParty destination address of invoice
     * @param source
     * @param taxClassMap tax classes which are being moved from origin address to destination address
     * @return
     */
    private GetTaxPercentagesResponse getGetTaxPercentagesFromTaxService(WsInvoice wsInvoice, Party fromParty, @Nullable Party toParty, WsInvoice.Source source,
            Map<String, WsTaxClass> taxClassMap) {
        GetTaxPercentagesRequest getTaxPercentagesRequest = new GetTaxPercentagesRequest();
        getTaxPercentagesRequest.setSource(source);
        PartyAddress fromPartyAddress = fromParty.getPartyAddressByType(PartyAddressType.Code.SHIPPING.name());
        getTaxPercentagesRequest.setSourceState(fromPartyAddress.getStateCode());
        getTaxPercentagesRequest.setSourceCountry(fromPartyAddress.getCountryCode());
        PartyAddress toPartyAddress = toParty != null ? toParty.getPartyAddressByType(PartyAddressType.Code.SHIPPING.name()) : null;
        getTaxPercentagesRequest.setDestinationState(toPartyAddress != null ? toPartyAddress.getStateCode() : wsInvoice.getDestinationStateCode());
        getTaxPercentagesRequest.setDestinationCountry(toPartyAddress != null ? toPartyAddress.getCountryCode() : wsInvoice.getDestinationCountryCode());
        getTaxPercentagesRequest.setProvidesCForm(wsInvoice.getCformProvided());
        getTaxPercentagesRequest.setTaxClassMap(taxClassMap);
        LOG.debug("Calling taxService with request : {}", getTaxPercentagesRequest);
        return taxService.getTaxPercentages(getTaxPercentagesRequest);
    }

    private TaxPercentageDTO prepareTaxPercentageDTO(WsTaxPercentageDetail taxPercentageDetail) {
        TaxPercentageDTO taxPercentageDTO = new TaxPercentageDTO();
        taxPercentageDTO.setAppliedTaxTypeCode(StringUtils.isBlank(taxPercentageDetail.getTaxTypeCode()) ? DEFAULT_TAX_TYPE_CODE : taxPercentageDetail.getTaxTypeCode());
        taxPercentageDTO.setVat(taxPercentageDetail.getVat());
        taxPercentageDTO.setCst(taxPercentageDetail.getCst());
        taxPercentageDTO.setCstFormc(taxPercentageDetail.getCstFormc());
        taxPercentageDTO.setServiceTax(taxPercentageDetail.getServiceTax());
        taxPercentageDTO.setTaxPercentage(taxPercentageDetail.getTaxPercentage());
        taxPercentageDTO.setAdditionalTax(taxPercentageDetail.getAdditionalTax());
        taxPercentageDTO.setCentralGst(taxPercentageDetail.getCentralGst());
        taxPercentageDTO.setStateGst(taxPercentageDetail.getStateGst());
        taxPercentageDTO.setUnionTerritoryGst(taxPercentageDetail.getUnionTerritoryGst());
        taxPercentageDTO.setIntegratedGst(taxPercentageDetail.getIntegratedGst());
        taxPercentageDTO.setCompensationCess(taxPercentageDetail.getCompensationCess());
        return taxPercentageDTO;
    }

    private Map<String, TaxPercentageDTO> prepareSkuToTaxPercentageDTO(Map<String, BigDecimal> skuToSellingPrice, ValidationContext context, PartyAddress fromPartyAddress,
            PartyAddress toPartyAddress, boolean providesCForm, WsInvoice.Source source) {
        GetTaxPercentagesResponse getTaxPercentagesResponse = new GetTaxPercentagesResponse();
        Map<String, WsTaxClass> taxClassMap = new HashMap<>(skuToSellingPrice.size());
        for (Map.Entry<String, BigDecimal> entry : skuToSellingPrice.entrySet()) {
            if (taxClassMap.get(entry.getKey()) == null) {
                ItemType itemType = catalogService.getItemTypeBySkuCode(entry.getKey());
                if (itemType == null) {
                    context.addError(WsResponseCode.INVALID_ITEM_TYPE, WsResponseCode.INVALID_ITEM_TYPE.message() + entry.getKey());
                    break;
                } else {
                    String taxTypeCode = catalogService.getTaxTypeCode(itemType, false);
                    String gstTaxTypeCode = catalogService.getTaxTypeCode(itemType, true);
                    WsTaxClass taxClass = new WsTaxClass(taxTypeCode, gstTaxTypeCode, entry.getValue());
                    taxClassMap.put(entry.getKey(), taxClass);
                }
            }
        }

        if (!context.hasErrors()) {
            GetTaxPercentagesRequest getTaxPercentagesRequest = new GetTaxPercentagesRequest();
            getTaxPercentagesRequest.setSourceState(fromPartyAddress.getStateCode());
            getTaxPercentagesRequest.setDestinationCountry(fromPartyAddress.getCountryCode());
            getTaxPercentagesRequest.setDestinationState(toPartyAddress.getStateCode());
            getTaxPercentagesRequest.setDestinationCountry(toPartyAddress.getCountryCode());
            getTaxPercentagesRequest.setProvidesCForm(providesCForm);
            getTaxPercentagesRequest.setTaxClassMap(taxClassMap);
            getTaxPercentagesRequest.setSource(source);
            getTaxPercentagesResponse = taxService.getTaxPercentages(getTaxPercentagesRequest);
            if (getTaxPercentagesResponse.hasErrors()) {
                context.addErrors(getTaxPercentagesResponse.getErrors());
            } else if (getTaxPercentagesResponse.getIdentifierToTaxPercentages().size() != taxClassMap.size()) {
                context.addError(WsResponseCode.TAX_TYPE_NOT_CONFIGURED, "tax type not configured for some of the skus");
            }
        }
        return getTaxPercentagesResponse.getIdentifierToTaxPercentages();
    }

    private TaxDetailDTO getTaxDetails(ItemType itemType, String channelProductId, BigDecimal sellingPrice, String shippingState, String shippingCountry,
            BigDecimal taxPercentage) {
        Facility facility = CacheManager.getInstance().getCache(FacilityCache.class).getCurrentFacility();
        PartyAddress warehouseShippingAddress = facility.getPartyAddressByType(PartyAddressType.Code.SHIPPING.name());
        TaxDetailDTO taxDetailDTO = new TaxDetailDTO();
        taxDetailDTO.setItemSku(itemType.getSkuCode());
        taxDetailDTO.setChannelProductId(channelProductId);
        if (LocationCache.COUNTRY_CODE_INDIA.equalsIgnoreCase(shippingCountry) && warehouseShippingAddress.isIndiaAddress()) {
            if (warehouseShippingAddress.getStateCode().equals(shippingState)) {
                BigDecimal vat = NumberUtils.getPercentageFromTotal(sellingPrice, taxPercentage.multiply(new BigDecimal(100)).divide(new BigDecimal(100)));
                taxDetailDTO.setVat(vat);
                taxDetailDTO.setTaxPercentage(taxPercentage);
            } else {
                BigDecimal cst = NumberUtils.getPercentageFromTotal(sellingPrice, taxPercentage.multiply(new BigDecimal(100)).divide(new BigDecimal(100)));
                taxDetailDTO.setCst(cst);
                taxDetailDTO.setTaxPercentage(taxPercentage);
            }
        }
        taxDetailDTO.setTaxTypeCode(DEFAULT_TAX_TYPE_CODE);
        return taxDetailDTO;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Invoice> searchInvoice(String keyword) {
        return invoiceDao.searchInvoice(keyword);
    }

    @Override
    @Transactional(readOnly = true)
    public Invoice getInvoiceDetailedById(int invoiceId) {
        Invoice invoice = invoiceDao.getInvoiceById(invoiceId);
        Party fromParty = invoice.getFromParty();
        Hibernate.initialize(fromParty);
        Hibernate.initialize(fromParty.getPartyAddressByType(PartyAddressType.Code.SHIPPING.name()));
        Hibernate.initialize(fromParty.getPartyAddressByType(PartyAddressType.Code.BILLING.name()));
        for (InvoiceItem invoiceItem : invoice.getInvoiceItems()) {
            Hibernate.initialize(invoiceItem);
            Hibernate.initialize(invoiceItem.getItemType());
            Hibernate.initialize(invoiceItem.getItemType().getCategory());
        }
        return invoice;
    }

    @Override
    @Transactional(readOnly = true)
    public boolean isNextInvoicePrefixValid(String prefix) {
        return invoiceDao.searchInvoice(prefix + "%").size() == 0;
    }

    @Override
    public InvoiceDTO prepareInvoiceDTO(Invoice invoice) {
        InvoiceDTO invoiceDTO = new InvoiceDTO();
        invoiceDTO.setCode(invoice.getCode());
        invoiceDTO.setDisplayCode(invoice.getDisplayCode());
        invoiceDTO.setChannelCreated(invoice.getChannelCreated());
        invoiceDTO.setCreated(invoice.getCreated());
        invoiceDTO.setReturn(invoice.getType().isReturn());
        List<InvoiceItemDTO> invoiceItemDTOs = new ArrayList<>(invoice.getInvoiceItems().size());
        invoiceDTO.setInvoiceItems(invoiceItemDTOs);
        for (InvoiceItem invoiceItem : invoice.getInvoiceItems()) {
            InvoiceItemDTO invoiceItemDTO = new InvoiceItemDTO();
            prepareInvoiceItemDTO(invoiceItemDTO, invoiceItem);
            invoiceItemDTOs.add(invoiceItemDTO);
            invoiceDTO.setTotalQuantity(invoiceDTO.getTotalQuantity() + invoiceItemDTO.getQuantity());
            invoiceDTO.setSubtotal(invoiceDTO.getSubtotal().add(invoiceItemDTO.getSubtotal()));
            invoiceDTO.setTotal(invoiceDTO.getTotal().add(getTotalChargeOfInvoiceItemDTO(invoiceItemDTO)));
            invoiceDTO.setVat(invoiceDTO.getVat().add(invoiceItemDTO.getVat()));
            invoiceDTO.setCst(invoiceDTO.getCst().add(invoiceItemDTO.getCst()));
            invoiceDTO.setCentralGst(invoiceDTO.getCentralGst().add(invoiceItemDTO.getCentralGst()));
            invoiceDTO.setStateGst(invoiceDTO.getStateGst().add(invoiceItemDTO.getStateGst()));
            invoiceDTO.setUnionTerritoryGst(invoiceDTO.getUnionTerritoryGst().add(invoiceItemDTO.getUnionTerritoryGst()));
            invoiceDTO.setIntegratedGst(invoiceDTO.getIntegratedGst().add(invoiceItemDTO.getIntegratedGst()));
            invoiceDTO.setCompensationCess(invoiceDTO.getCompensationCess().add(invoiceItemDTO.getCompensationCess()));
            invoiceDTO.setAdditionalTax(invoiceDTO.getAdditionalTax().add(invoiceItemDTO.getAdditionalTax()));
            invoiceDTO.setPrepaidAmount(invoiceDTO.getPrepaidAmount().add(invoiceItemDTO.getPrepaidAmount()));
            invoiceDTO.setStoreCredit(invoiceDTO.getStoreCredit().add(invoiceItemDTO.getStoreCredit()));
            invoiceDTO.setDiscount(invoiceDTO.getDiscount().add(invoiceItemDTO.getDiscount()));
            invoiceDTO.setShippingCharges(invoiceDTO.getShippingCharges().add(invoiceItemDTO.getShippingCharges()));
            invoiceDTO.setCashOnDeliveryCharges(invoiceDTO.getCashOnDeliveryCharges().add(invoiceItemDTO.getCashOnDeliveryCharges()));
            invoiceDTO.setShippingMethodCharges(invoiceDTO.getShippingMethodCharges().add(invoiceItemDTO.getShippingMethodCharges()));
            invoiceDTO.setGiftWrapCharges(invoiceDTO.getGiftWrapCharges().add(invoiceItemDTO.getGiftWrapCharges()));
        }
        invoiceDTO.setSellingPrice(invoiceDTO.getSubtotal());
        return invoiceDTO;
    }

    /**
     * @return the sum of subTotal, shipping charges, cod charges, shipping method charges and taxes of an
     *         {@code invoiceItemDTO}
     */
    private BigDecimal getTotalChargeOfInvoiceItemDTO(InvoiceItemDTO invoiceItemDTO) {
        return invoiceItemDTO.getSubtotal().add(invoiceItemDTO.getShippingCharges()).add(invoiceItemDTO.getCashOnDeliveryCharges()).add(invoiceItemDTO.getGiftWrapCharges()).add(
                invoiceItemDTO.getShippingMethodCharges()).add(invoiceItemDTO.getTotalTax());
    }

    private void prepareInvoiceItemDTO(InvoiceItemDTO iiDTO, InvoiceItem invoiceItem) {
        iiDTO.setItemSku(ItemType.UNKNOWN_SKU.equals(invoiceItem.getItemType().getSkuCode()) ? invoiceItem.getChannelSkuCode() : invoiceItem.getItemType().getSkuCode());
        iiDTO.setChannelSku(invoiceItem.getChannelProductId());
        iiDTO.setQuantity(invoiceItem.getQuantity());
        iiDTO.setUnitPrice(invoiceItem.getUnitPrice());
        iiDTO.setShippingCharges(invoiceItem.getShippingCharges());
        iiDTO.setShippingMethodCharges(iiDTO.getShippingMethodCharges());
        iiDTO.setCashOnDeliveryCharges(invoiceItem.getCashOnDeliveryCharges());
        iiDTO.setGiftWrapCharges(invoiceItem.getGiftWrapCharges());
        iiDTO.setPrepaidAmount(invoiceItem.getPrepaidAmount());
        iiDTO.setVoucherValue(invoiceItem.getVoucherValue());
        iiDTO.setStoreCredit(invoiceItem.getStoreCredit());
        iiDTO.setSubtotal(invoiceItem.getSubtotal());
        iiDTO.setTotal(invoiceItem.getTotal());
        iiDTO.setDiscount(invoiceItem.getDiscount());
        iiDTO.setSellingPrice(invoiceItem.getSubtotal());
        // taxes
        iiDTO.setTaxPercentage(invoiceItem.getInvoiceItemTaxes().stream().map(InvoiceItemTax::getTaxPercentage).reduce(BigDecimal.ZERO, BigDecimal::add));
        BigDecimal totalCstOnInvoiceItem = invoiceItem.getInvoiceItemTaxes().stream().map(InvoiceItemTax::getCst).reduce(BigDecimal.ZERO, BigDecimal::add);
        BigDecimal totalVatOnInvoiceItem = invoiceItem.getInvoiceItemTaxes().stream().map(InvoiceItemTax::getVat).reduce(BigDecimal.ZERO, BigDecimal::add);
        iiDTO.setVat(totalVatOnInvoiceItem);
        iiDTO.setVatPercentage(NumberUtils.getPercentageFromBaseAndPercentageAmount(invoiceItem.getSubtotal(), totalVatOnInvoiceItem));
        iiDTO.setCst(totalCstOnInvoiceItem);
        iiDTO.setCstPercentage(NumberUtils.getPercentageFromBaseAndPercentageAmount(invoiceItem.getSubtotal(), totalCstOnInvoiceItem));
        iiDTO.setCompensationCess(invoiceItem.getInvoiceItemTaxes().stream().map(InvoiceItemTax::getCompensationCess).reduce(BigDecimal.ZERO, BigDecimal::add));
        iiDTO.setCompensationCessPercentage(invoiceItem.getSellingPriceInvoiceItemTax().getCompensationCessPercentage());
        iiDTO.setCentralGst(invoiceItem.getInvoiceItemTaxes().stream().map(InvoiceItemTax::getCentralGst).reduce(BigDecimal.ZERO, BigDecimal::add));
        iiDTO.setCentralGstPercentage(invoiceItem.getSellingPriceInvoiceItemTax().getCentralGstPercentage());
        iiDTO.setStateGst(invoiceItem.getInvoiceItemTaxes().stream().map(InvoiceItemTax::getStateGst).reduce(BigDecimal.ZERO, BigDecimal::add));
        iiDTO.setStateGstPercentage(invoiceItem.getSellingPriceInvoiceItemTax().getStateGstPercentage());
        iiDTO.setUnionTerritoryGst(invoiceItem.getInvoiceItemTaxes().stream().map(InvoiceItemTax::getUnionTerritoryGst).reduce(BigDecimal.ZERO, BigDecimal::add));
        iiDTO.setUnionTerritoryGstPercentage(invoiceItem.getSellingPriceInvoiceItemTax().getUnionTerritoryGstPercentage());
        iiDTO.setIntegratedGst(invoiceItem.getInvoiceItemTaxes().stream().map(InvoiceItemTax::getIntegratedGst).reduce(BigDecimal.ZERO, BigDecimal::add));
        iiDTO.setIntegratedGstPercentage(invoiceItem.getSellingPriceInvoiceItemTax().getIntegratedGstPercentage());
        iiDTO.setAdditionalTax(invoiceItem.getInvoiceItemTaxes().stream().map(InvoiceItemTax::getAdditionalTax).reduce(BigDecimal.ZERO, BigDecimal::add));
        iiDTO.setAdditionalTaxPercentage(invoiceItem.getInvoiceItemTaxes().stream().map(InvoiceItemTax::getAdditionalTaxPercentage).reduce(BigDecimal.ZERO, BigDecimal::add));
        iiDTO.setTotalTax(iiDTO.getVat().add(iiDTO.getCst()).add(iiDTO.getAdditionalTax()).add(iiDTO.getCompensationCess()).add(iiDTO.getCentralGst()).add(iiDTO.getStateGst()).add(
                iiDTO.getUnionTerritoryGst()).add(iiDTO.getIntegratedGst()));
    }

    @Override
    public Invoice updateInvoice(Invoice invoice) {
        return invoiceDao.updateInvoice(invoice);
    }

    @Override
    @Transactional(readOnly = true)
    public Invoice getInvoiceByCode(String code) {
        return invoiceDao.getInvoiceByCode(code);
    }

    public static class Combination {
        private String              combinationItemType;
        private BigDecimal          sellingPrice;
        private List<SaleOrderItem> saleOrderItems;
        private String              description;

        public Combination(List<SaleOrderItem> saleOrderItems) {
            Collections.sort(saleOrderItems, (arg0, arg1) -> arg0.getItemType().getId().compareTo(arg1.getItemType().getId()));
            StringBuilder builder = new StringBuilder();
            BigDecimal sellingPrice = BigDecimal.ZERO;
            for (SaleOrderItem saleOrderItem : saleOrderItems) {
                builder.append(saleOrderItem.getItemType().getId()).append("-");
                sellingPrice.add(saleOrderItem.getSellingPrice());
            }
            this.combinationItemType = builder.toString();
            this.sellingPrice = sellingPrice;
            this.saleOrderItems = saleOrderItems;
            this.description = saleOrderItems.get(0).getCombinationDescription();
        }

        /**
         * @return the combinationItemType
         */
        public String getCombinationItemType() {
            return combinationItemType;
        }

        /**
         * @return the sellingPrice
         */
        public BigDecimal getSellingPrice() {
            return sellingPrice;
        }

        /**
         * @return the saleOrderItems
         */
        public List<SaleOrderItem> getSaleOrderItems() {
            return saleOrderItems;
        }

        /**
         * @return the description
         */
        public String getDescription() {
            return description;
        }
    }
}
