/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jan 20, 2012
 *  @author singla
 */
package com.uniware.services.invoice;

import com.uniware.core.api.invoice.InvoiceDTO;
import java.util.List;

import com.uniware.core.api.invoice.GenerateOrUpdateInvoiceRequest;
import com.uniware.core.api.invoice.GenerateOrUpdateInvoiceResponse;
import com.uniware.core.api.invoice.GetTaxDetailsRequest;
import com.uniware.core.api.invoice.GetTaxDetailsResponse;
import com.uniware.core.entity.Invoice;

/**
 * @author singla
 */
public interface IInvoiceService {

    /**
     * @param request
     * @return
     */
    GenerateOrUpdateInvoiceResponse createInvoice(GenerateOrUpdateInvoiceRequest request);

    /**
     * @param invoiceId
     * @return
     */
    Invoice getInvoiceDetailedById(int invoiceId);

    GenerateOrUpdateInvoiceResponse updateInvoice(GenerateOrUpdateInvoiceRequest request);

    GetTaxDetailsResponse getTaxDetails(GetTaxDetailsRequest request);

    List<Invoice> searchInvoice(String keyword);

    boolean isNextInvoicePrefixValid(String prefix);

    InvoiceDTO prepareInvoiceDTO(Invoice invoice);

    Invoice updateInvoice(Invoice invoice);

    Invoice getInvoiceByCode(String code);
}
