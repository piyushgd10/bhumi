package com.uniware.services.invoice.impl;

import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.jms.MessagingConstants;
import com.unifier.core.utils.StringUtils;
import com.uniware.core.cache.FacilityCache;
import com.uniware.core.entity.*;
import com.uniware.core.utils.UserContext;
import com.uniware.services.cache.ChannelCache;
import com.uniware.services.configuration.SourceConfiguration;
import com.uniware.services.invoice.IInvoicePublisherService;
import com.uniware.services.invoice.InvoiceMessage;
import com.uniware.services.messaging.jms.ActiveMQConnector;
import com.uniware.services.shipping.IShippingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

/**
 * Created by sunny on 18/02/15.
 */
@Service("invoicePublisherService")
public class InvoicePublisherServiceImpl implements IInvoicePublisherService {

    public static final Logger  LOG          = LoggerFactory.getLogger(InvoicePublisherServiceImpl.class);

    private static final String SERVICE_NAME = "invoicePublisherService";

    @Autowired
    private IShippingService    shippingService;

    @Autowired
    @Qualifier("activeMQConnector1")
    private ActiveMQConnector   activeMQConnector;

    //@Autowired
    //private ContextAwareExecutorFactory executorFactory;

    @Override
    @Transactional
    public void publish(ShippingPackage shippingPackage) {
        Tenant tenant = UserContext.current().getTenant();
        if (StringUtils.isNotBlank(tenant.getAccountCode())) {
            shippingPackage = shippingService.getShippingPackageById(shippingPackage.getId());
            Invoice invoice = shippingPackage.getInvoice();
            InvoiceMessage message = new InvoiceMessage();
            BigDecimal invoiceValue = BigDecimal.ZERO;
            int numberOfItems = 0;
            for (InvoiceItem invoiceItem : invoice.getInvoiceItems()) {
                invoiceValue = invoiceValue.add(invoiceItem.getTotal());
                numberOfItems += invoiceItem.getQuantity();
            }
            message.setNumberOfItems(numberOfItems);
            Facility facility = CacheManager.getInstance().getCache(FacilityCache.class).getFacilityById(shippingPackage.getFacility().getId());
            message.setFacilityCode(facility.getCode());
            message.setInvoiceValue(invoiceValue);
            message.setInvoiceCode(invoice.getCode());
            message.setAccountCode(tenant.getAccountCode());
            message.setIdentifier(tenant.getCode() + "-" + UserContext.current().getFacilityId() + "-" + invoice.getCode() + "-" + invoice.getId());
            Channel channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelById(shippingPackage.getSaleOrder().getChannel().getId());
            Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(channel.getSourceCode());
            message.setFulfilledByChannel(source.isFulfillmentByChannel());
            message.setChannelCode(shippingPackage.getSaleOrder().getChannel().getCode());
            message.setCreated(invoice.getCreated());
            LOG.info("Enqueueing invoice message {}", message);
            activeMQConnector.produceMessage(MessagingConstants.Queue.INVOICE_QUEUE, message);
        }
    }
}
