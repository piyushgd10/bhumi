package com.uniware.services.invoice;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by sunny on 18/02/15.
 */
public class InvoiceMessage {

    private String     invoiceCode;
    private String     accountCode;
    private String     channelCode;
    private int        numberOfItems;
    private BigDecimal invoiceValue;
    private String     identifier;
    private Date       created;
    private String     facilityCode;
    private boolean    fulfilledByChannel;

    public String getFacilityCode() {
        return facilityCode;
    }

    public void setFacilityCode(String facilityCode) {
        this.facilityCode = facilityCode;
    }

    public String getInvoiceCode() {
        return invoiceCode;
    }

    public void setInvoiceCode(String invoiceCode) {
        this.invoiceCode = invoiceCode;
    }

    public String getAccountCode() {
        return accountCode;
    }

    public void setAccountCode(String accountCode) {
        this.accountCode = accountCode;
    }

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    public int getNumberOfItems() {
        return numberOfItems;
    }

    public void setNumberOfItems(int numberOfItems) {
        this.numberOfItems = numberOfItems;
    }

    public BigDecimal getInvoiceValue() {
        return invoiceValue;
    }

    public void setInvoiceValue(BigDecimal invoiceValue) {
        this.invoiceValue = invoiceValue;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public boolean isFulfilledByChannel() {
        return fulfilledByChannel;
    }

    public void setFulfilledByChannel(boolean fulfilledByChannel) {
        this.fulfilledByChannel = fulfilledByChannel;
    }

    @Override
    public String toString() {
        return "InvoiceMessage{" + "invoiceCode='" + invoiceCode + '\'' + ", accountCode='" + accountCode + '\'' + ", channelCode='" + channelCode + '\'' + ", numberOfItems="
                + numberOfItems + ", invoiceValue=" + invoiceValue + ", identifier='" + identifier + '\'' + ", created=" + created + ", facilityCode='" + facilityCode + '\''
                + ", fulfilledByChannel=" + fulfilledByChannel + '}';
    }
}
