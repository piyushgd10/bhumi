package com.uniware.services.invoice;

import com.uniware.core.entity.ShippingPackage;

/**
 * Created by sunny on 18/02/15.
 */
public interface IInvoicePublisherService {

    public void publish(ShippingPackage shippingPackage);
}
