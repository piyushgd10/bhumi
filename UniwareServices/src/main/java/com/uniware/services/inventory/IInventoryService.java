/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 22, 2011
 *  @author singla
 */
package com.uniware.services.inventory;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.transaction.annotation.Transactional;

import com.unifier.core.api.validation.ValidationContext;
import com.uniware.core.api.inflow.CreateItemLabelsRequest;
import com.uniware.core.api.inflow.CreateItemLabelsResponse;
import com.uniware.core.api.inventory.AddItemLabelsRequest;
import com.uniware.core.api.inventory.AddItemLabelsResponse;
import com.uniware.core.api.inventory.AddOrEditItemLabelsRequest;
import com.uniware.core.api.inventory.AddOrEditItemLabelsResponse;
import com.uniware.core.api.inventory.EditReorderItemLevelRequest;
import com.uniware.core.api.inventory.EditReorderItemLevelResponse;
import com.uniware.core.api.inventory.GenerateItemLabelsRequest;
import com.uniware.core.api.inventory.GenerateItemLabelsResponse;
import com.uniware.core.api.inventory.GetInventorySnapshotRequest;
import com.uniware.core.api.inventory.GetInventorySnapshotResponse;
import com.uniware.core.api.inventory.GetInventorySyncRequest;
import com.uniware.core.api.inventory.GetInventorySyncResponse;
import com.uniware.core.api.inventory.InventoryAdjustmentRequest;
import com.uniware.core.api.inventory.InventoryAdjustmentResponse;
import com.uniware.core.api.inventory.MarkItemTypeInventoryNotFoundRequest;
import com.uniware.core.api.inventory.MarkItemTypeInventoryNotFoundResponse;
import com.uniware.core.api.inventory.MarkItemTypeQuantityFoundRequest;
import com.uniware.core.api.inventory.MarkItemTypeQuantityFoundResponse;
import com.uniware.core.api.inventory.MarkItemTypeQuantityNotFoundRequest;
import com.uniware.core.api.inventory.MarkItemtypeQuantityNotFoundResponse;
import com.uniware.core.api.inventory.MarkTraceableInventoryDamagedRequest;
import com.uniware.core.api.inventory.MarkTraceableInventoryDamagedResponse;
import com.uniware.core.api.inventory.ReassessInventoryForItemTypeRequest;
import com.uniware.core.api.inventory.ReassessInventoryForItemTypeResponse;
import com.uniware.core.api.inventory.SearchInventorySnapshotResponse.InventorySnapshotDTO;
import com.uniware.core.api.item.UpdateItemRequest;
import com.uniware.core.api.item.UpdateItemResponse;
import com.uniware.core.entity.Bundle;
import com.uniware.core.entity.InflowReceiptItem;
import com.uniware.core.entity.Item;
import com.uniware.core.entity.Item.StatusCode;
import com.uniware.core.entity.ItemType;
import com.uniware.core.entity.ItemTypeInventory;
import com.uniware.core.entity.ItemTypeInventory.Type;
import com.uniware.core.entity.ItemTypeInventorySnapshot;
import com.uniware.core.entity.Shelf;
import com.uniware.core.entity.Vendor;
import com.uniware.core.entity.VendorItemType;
import com.uniware.services.exception.InventoryAccuracyException;
import com.uniware.services.exception.InventoryNotAvailableException;
import com.uniware.services.exception.ShelfBlockedForCycleCountException;
import com.uniware.services.imports.AutoGrnItemsImportJobHandler;
import com.uniware.services.tasks.saleorder.SaleOrderProcessor.ItemTypeDTO;

/**
 * @author singla
 */
public interface IInventoryService {

    ItemTypeInventory blockInventoryForLenskartAndJabongg(ItemType itemType) throws InventoryNotAvailableException;

    /**
     * @param itemTypeInventoryId
     * @return
     */
    ItemTypeInventory getItemTypeInventoryById(int itemTypeInventoryId);

    Map<ItemTypeInventory, Integer> blockAvailableInventory(ItemType itemType, int channelId, int quantity);

    /**
     * @param itemType
     * @param shelf
     * @param quantity
     * @return
     */
    ItemTypeInventory addInventory(ItemType itemType, Type inventoryType, Shelf shelf, int quantity);

    /**
     * @param itemTypeId
     */
    void markInventoryNotFound(int itemTypeId);

    @Transactional
    void markInventoryNotFound(int itemTypeInventoryId, int qty);

    /**
     * @param itemTypeInventoryId
     */
    void releaseInventory(int itemTypeInventoryId, int channelId, int quantity);

    /**
     * @param request
     * @return
     */
    InventoryAdjustmentResponse adjustInventory(InventoryAdjustmentRequest request);

    /**
     * @param itemType
     * @param shelf
     * @param quantity
     * @return
     * @throws InventoryNotAvailableException
     */
    ItemTypeInventory removeInventory(ItemType itemType, Type inventoryType, Shelf shelf, int quantity) throws InventoryNotAvailableException, ShelfBlockedForCycleCountException;

    /**
     * @param itemId
     * @return
     */
    Item getItemById(int itemId);

    /**
     * @param itemCode
     * @return
     */
    Item getItemByCode(String itemCode);

    /**
     * @param itemType
     * @param vendor
     * @param quantity
     * @param inflowReceiptItem
     * @param statusCode
     * @return
     */
    List<Item> addItems(ItemType itemType, Vendor vendor, int quantity, InflowReceiptItem inflowReceiptItem, StatusCode statusCode);

    List<Item> addItemsWithCodeAndDetails(ItemType itemType, Vendor vendor, List<AutoGrnItemsImportJobHandler.ItemDTO> itemDTOs, InflowReceiptItem inflowReceiptItem,
            StatusCode statusCode, ValidationContext context);

    /**
     * @param item
     * @return
     */
    Item updateItem(Item item);

    /**
     * @param itemCodes
     * @return
     */
    List<Item> getItems(List<String> itemCodes);

    @Transactional(readOnly = true)
    List<Item> getItemsByCodes(List<String> itemCodes);

    ItemTypeInventorySnapshot getItemTypeInventorySnapshot(Integer itemTypeId);

    CreateItemLabelsResponse createItemLabels(CreateItemLabelsRequest request);

    /**
     * @param request
     * @return
     */
    AddItemLabelsResponse addItemLabels(AddItemLabelsRequest request);

    /**
     * @param request
     * @return
     */
    AddOrEditItemLabelsResponse addOrEditItemLabels(AddOrEditItemLabelsRequest request);

    List<Item> getItemsByItemTypeByStatus(String skuCode, String name, Integer offset, Integer limit);

    List<ItemTypeInventorySnapshot> getItemTypeInventorySnapshotByTenant(Integer itemTypeId);

    MarkTraceableInventoryDamagedResponse markTraceableInventoryDamaged(MarkTraceableInventoryDamagedRequest request);

    /**
     * @return
     * @param start
     * @param pageSize
     */
    List<InventorySnapshotDTO> getPendingNonBundleItemTypeInventoryNotifications();

    @Transactional
    List<ItemTypeInventorySnapshot> getItemTypeInventorySnapshots(Integer itemTypeId, List<Integer> facilityIds);

    /**
     * @param request
     * @return
     */
    GetInventorySnapshotResponse getInventorySnapshot(GetInventorySnapshotRequest request);

    /**
     * @param editReoderLevelRequest
     * @return
     */
    EditReorderItemLevelResponse editReorderItemLevel(EditReorderItemLevelRequest editReoderLevelRequest);

    /**
     * @param itemCodes
     * @return
     */
    String getItemLabelHtml(List<String> itemCodes);

    List<ItemTypeInventory> getItemTypeInventoryByItemType(ItemType itemType);

    void commitBlockedInventory(int itemTypeInventoryId, int quantity);

    ItemTypeInventory blockInventory(Item item) throws InventoryNotAvailableException, ShelfBlockedForCycleCountException, InventoryAccuracyException;

    ItemTypeInventory removeAgeAgnosticInventory(ItemType itemType, Type inventoryType) throws InventoryNotAvailableException, ShelfBlockedForCycleCountException;

    ItemTypeInventory removeInventory(ItemType itemType, Type inventoryType, Date ageingStartDate, Shelf shelf, int quantity, Integer sla)
            throws InventoryNotAvailableException, ShelfBlockedForCycleCountException;

    MarkItemTypeQuantityFoundResponse markItemTypeQuantityFound(MarkItemTypeQuantityFoundRequest request);

    Item getItemByCode(String itemCode, boolean lock);

    void reSyncAllItemTypes();

    ItemTypeInventory addInventory(ItemType itemType, Type inventoryType, Date ageingStartDate, Shelf shelf, int quantity, Integer sla);

    ItemTypeInventory replaceInventory(ItemType itemType, Type inventoryType, Shelf shelf, int quantity) throws InventoryNotAvailableException;

    InventorySnapshotDTO getItemTypeInventoryNotification(int itemTypeId);

    void markBundlesUnacknowledged(List<Bundle> bundles);

    ItemTypeInventorySnapshot createItemTypeInventorySnapshot(ItemType itemType);

    List<InventorySnapshotDTO> getPendingBundleItemTypeInventoryNotifications();

    @Transactional
    MarkItemTypeInventoryNotFoundResponse markItemTypeInventoryNotFound(MarkItemTypeInventoryNotFoundRequest request);

    MarkItemtypeQuantityNotFoundResponse markItemTypeQuantityNotFound(MarkItemTypeQuantityNotFoundRequest request);

    /**
     * For a given {@code ItemType} and quantity, generates given number of {@code Item}(s) for given {@code Vendor}.
     * This would be called from vendors on B2B clients, (eg. Jade, Myntra) for generating client friendly labels.
     *
     * @param request
     * @return
     */
    GenerateItemLabelsResponse generateItemLabels(GenerateItemLabelsRequest request);

    boolean markInventorySnapshotDirty(Integer itemTypeId);

    List<Item> addItems(ItemType itemType, Vendor vendor, int quantity, StatusCode statusCode, VendorItemType vendorItemtype);

    ReassessInventoryForItemTypeResponse reassessInventoryForItemType(ReassessInventoryForItemTypeRequest request);

    GetInventorySyncResponse getInventorySync(GetInventorySyncRequest request);

    List<ItemTypeDTO> getItemTypesToBeReassessed();

    boolean resetReassessVersionIfLatest(Integer id, int reassessVersion);

    List<ItemTypeInventory> getItemTypeInventoryByItemTypeId(Integer itemTypeId);

    List<ItemTypeInventory> getItemTypeInventoryByItemType(ItemType itemType, List<Integer> facilityIds);

    ItemTypeInventory removeInventory(ItemType itemType, Type inventoryType, Date ageingStartDate, Shelf shelf, int quantity)
            throws InventoryNotAvailableException, ShelfBlockedForCycleCountException;

    ItemTypeInventory removeInventory(ItemType itemType, Type inventoryType, Shelf shelf, int quantity, Integer sla)
            throws InventoryNotAvailableException, ShelfBlockedForCycleCountException;

    ItemTypeInventory addInventory(ItemType itemType, Type inventoryType, Date ageingStartDate, Shelf shelf, int quantity);

    ItemTypeInventory addInventory(ItemType itemType, Type inventoryType, Shelf shelf, int quantity, Integer sla);

    ItemTypeInventory replaceInventory(ItemType itemType, Type inventoryType, Date ageingStartDate, Shelf shelf, int quantity) throws InventoryNotAvailableException;

    ItemTypeInventory replaceInventory(ItemType itemType, Type inventoryType, Shelf shelf, int quantity, Integer sla) throws InventoryNotAvailableException;

    ItemTypeInventory replaceInventory(ItemType itemType, Type inventoryType, Date ageingStartDate, Shelf shelf, int quantity, Integer sla) throws InventoryNotAvailableException;

    List<Item> getItemsExpectedOnShelf(int shelfId);

    Item updateItemOptimistic(Item item, Integer shelfId, Integer cycleCountId);

    List<ItemTypeInventory> getAllItemTypeInventoryForShelf(Shelf shelf, boolean lock);

    ItemTypeInventory addInventory(String skuCode, Type inventoryType, Date ageingStartDate, Shelf shelf, int adjustedQuantity);

    ItemTypeInventory getItemTypeInventory(String skuCode, Type inventoryType, Shelf shelf, boolean quantityGreaterThanZero);

    ItemTypeInventory updateItemTypeInventory(ItemTypeInventory itemTypeInventory);

    List<Item> getItemsForAutoAging(String skuCode, int idToProcessFrom, int batchSize, int allowedShelfLife);

    void markExpiredItemAsBadInventory(Item item, String rejectionReason);

    ItemTypeInventory getItemTypeInventory(String skuCode, Type inventoryType, Shelf shelf, Date ageingStartDate);

    boolean isItemPresentWithDifferentAgeingOnShelf(String skuCode, Date ageingStartDate, Type type, int shelfId);

    void decrementNotFoundInventory(Item item, ValidationContext context);

    List<Item> getItemsByItemTypeAndShelfAndStatus(Integer itemTypeId, Integer shelfId, String status);

    UpdateItemResponse updateItemForFacility(UpdateItemRequest updateItemRequest);

    Long getInventoryCountByShelfId(Integer shelfId);

    Long getNumberOfItemsWithoutManufacturingDateForCategory(String categoryCode);

    List<Integer> getPendingNonBundleItemTypesForInventoryNotifications(int start, int batchSize);

    List<ItemTypeInventorySnapshot> getItemTypeInventorySnapshots(List<Integer> itemTypeIds);
}