package com.uniware.services.inventory;

import com.uniware.core.api.inventory.AcknowledgeStockoutInventoryRequest;
import com.uniware.core.api.inventory.AcknowledgeStockoutInventoryResponse;
import com.uniware.core.entity.InventoryAvailabilityChangeLedger;
import com.uniware.services.tasks.notifications.AcknowledgeInventoryLedgerTask;

import java.util.List;

/**
 * Created by piyush on 1/6/16.
 */
public interface IInventoryAvailabilityChangeService {

    AcknowledgeInventoryLedgerTask.UnacknowledgedInventoryLedgerStatus getUnacknowledgedInventoryLedgerStatus();

    AcknowledgeStockoutInventoryResponse acknowledgeInventoryLedgers(AcknowledgeStockoutInventoryRequest acknowledgeStockoutInventoryRequest);

    List<InventoryAvailabilityChangeLedger> getUnacknowledgedInventoryLedgers(int batchSize, int maxId);

    List<InventoryAvailabilityChangeLedger> getUnacknowledgedInventoryLedgersByFacilityId(int batchSize, int maxId, int facilityId);
}
