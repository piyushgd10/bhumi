/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 22, 2011
 *  @author singla
 */
package com.uniware.services.inventory.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import com.unifier.core.utils.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;
import com.unifier.core.annotation.Level;
import com.unifier.core.annotation.audit.LogActivity;
import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.api.validation.WsError;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.entity.User;
import com.unifier.core.template.Template;
import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.DateUtils.DateRange;
import com.unifier.core.utils.DateUtils.Interval;
import com.unifier.core.utils.JsonUtils;
import com.unifier.core.utils.StringUtils;
import com.unifier.services.users.IUsersService;
import com.uniware.core.api.channel.ChannelInventoryStatusDTO;
import com.uniware.core.api.channel.ChannelInventorySyncStatusDTO;
import com.uniware.core.api.channel.ChannelSyncDTO;
import com.uniware.core.api.inflow.CreateItemLabelsRequest;
import com.uniware.core.api.inflow.CreateItemLabelsResponse;
import com.uniware.core.api.inventory.AddItemLabelsRequest;
import com.uniware.core.api.inventory.AddItemLabelsResponse;
import com.uniware.core.api.inventory.AddOrEditItemLabelsRequest;
import com.uniware.core.api.inventory.AddOrEditItemLabelsResponse;
import com.uniware.core.api.inventory.EditReorderItemLevelRequest;
import com.uniware.core.api.inventory.EditReorderItemLevelResponse;
import com.uniware.core.api.inventory.GenerateItemLabelsRequest;
import com.uniware.core.api.inventory.GenerateItemLabelsResponse;
import com.uniware.core.api.inventory.GetInventorySnapshotRequest;
import com.uniware.core.api.inventory.GetInventorySnapshotResponse;
import com.uniware.core.api.inventory.GetInventorySyncRequest;
import com.uniware.core.api.inventory.GetInventorySyncResponse;
import com.uniware.core.api.inventory.InventoryAdjustmentRequest;
import com.uniware.core.api.inventory.InventoryAdjustmentResponse;
import com.uniware.core.api.inventory.ItemLabelDTO;
import com.uniware.core.api.inventory.ItemTypeInventoryDTO;
import com.uniware.core.api.inventory.MarkItemTypeInventoryNotFoundRequest;
import com.uniware.core.api.inventory.MarkItemTypeInventoryNotFoundResponse;
import com.uniware.core.api.inventory.MarkItemTypeQuantityFoundRequest;
import com.uniware.core.api.inventory.MarkItemTypeQuantityFoundResponse;
import com.uniware.core.api.inventory.MarkItemTypeQuantityNotFoundRequest;
import com.uniware.core.api.inventory.MarkItemtypeQuantityNotFoundResponse;
import com.uniware.core.api.inventory.MarkTraceableInventoryDamagedRequest;
import com.uniware.core.api.inventory.MarkTraceableInventoryDamagedResponse;
import com.uniware.core.api.inventory.ReassessInventoryForItemTypeRequest;
import com.uniware.core.api.inventory.ReassessInventoryForItemTypeResponse;
import com.uniware.core.api.inventory.SearchInventorySnapshotResponse.InventorySnapshotDTO;
import com.uniware.core.api.inventory.WsInventoryAdjustment;
import com.uniware.core.api.inventory.WsItemLabel;
import com.uniware.core.api.inventory.WsItemTypeInventorySnapshot;
import com.uniware.core.api.item.ItemDetailFieldDTO;
import com.uniware.core.api.item.ItemTypeQuantityNotFoundDTO;
import com.uniware.core.api.item.UpdateItemRequest;
import com.uniware.core.api.item.UpdateItemResponse;
import com.uniware.core.api.saleorder.UnblockSaleOrderItemsInventoryRequest;
import com.uniware.core.api.saleorder.UnblockSaleOrderItemsInventoryResponse;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.cache.FacilityCache;
import com.uniware.core.entity.Bundle;
import com.uniware.core.entity.Channel;
import com.uniware.core.entity.Facility;
import com.uniware.core.entity.InflowReceiptItem;
import com.uniware.core.entity.InventoryAdjustment;
import com.uniware.core.entity.InventoryAdjustment.AdjustmentType;
import com.uniware.core.entity.InventoryLedger;
import com.uniware.core.entity.InventoryLedger.ChangeType;
import com.uniware.core.entity.Item;
import com.uniware.core.entity.Item.StatusCode;
import com.uniware.core.entity.ItemType;
import com.uniware.core.entity.ItemTypeInventory;
import com.uniware.core.entity.ItemTypeInventory.Type;
import com.uniware.core.entity.ItemTypeInventorySnapshot;
import com.uniware.core.entity.SaleOrderItem;
import com.uniware.core.entity.Shelf;
import com.uniware.core.entity.Source;
import com.uniware.core.entity.Vendor;
import com.uniware.core.entity.VendorItemType;
import com.uniware.core.locking.Namespace;
import com.uniware.core.locking.annotation.Lock;
import com.uniware.core.locking.annotation.Locks;
import com.uniware.core.utils.ActivityContext;
import com.uniware.core.utils.Constants;
import com.uniware.core.utils.UserContext;
import com.uniware.core.vo.PrintTemplateVO;
import com.uniware.dao.inventory.IInventoryDao;
import com.uniware.services.audit.impl.ActivityEntityEnum;
import com.uniware.services.audit.impl.ActivityUtils;
import com.uniware.services.cache.ChannelCache;
import com.uniware.services.cache.ItemTypeDetailCache;
import com.uniware.services.cache.PrintTemplateCache;
import com.uniware.services.catalog.ICatalogService;
import com.uniware.services.channel.IChannelService;
import com.uniware.services.configuration.SourceConfiguration;
import com.uniware.services.configuration.SystemConfiguration;
import com.uniware.services.configuration.SystemConfiguration.InventoryAccuracyLevel;
import com.uniware.services.configuration.SystemConfiguration.TraceabilityLevel;
import com.uniware.services.exception.InventoryAccuracyException;
import com.uniware.services.exception.InventoryNotAvailableException;
import com.uniware.services.exception.ShelfBlockedForCycleCountException;
import com.uniware.services.imports.AutoGrnItemsImportJobHandler;
import com.uniware.services.inventory.IInventoryService;
import com.uniware.services.ledger.IInventoryLedgerService;
import com.uniware.services.saleorder.ISaleOrderService;
import com.uniware.services.tasks.saleorder.SaleOrderProcessor.ItemTypeDTO;
import com.uniware.services.warehouse.IFacilityService;
import com.uniware.services.warehouse.IShelfService;

/**
 * @author singla
 */
@Service
public class InventoryServiceImpl implements IInventoryService {

    private static final Logger     LOG                = LoggerFactory.getLogger(InventoryServiceImpl.class);
    private static final int        MIN_QUANTITY_FOUND = 0;
    @Autowired
    private IInventoryDao           inventoryDao;
    @Autowired
    private ICatalogService         catalogService;
    @Autowired
    private IShelfService           shelfService;
    @Autowired
    private IUsersService           usersService;
    @Autowired
    private IFacilityService        facilityService;
    @Autowired
    private IChannelService         channelService;
    @Autowired
    private ISaleOrderService       saleOrderService;

    @Autowired
    private IInventoryLedgerService inventoryLedgerService;

    /* (non-Javadoc)
     * @see com.uniware.services.inventory.IInventoryService#blockInventory(com.uniware.core.entity.ItemType)
     */
    @Override
    @Transactional
    public ItemTypeInventory blockInventoryForLenskartAndJabongg(ItemType itemType) throws InventoryNotAvailableException {
        // TODO: 21/07/17 @ADITYA @INVENTORY_AGEING_DATE these need to be removed when they are not used anymore
        ItemTypeInventory itemTypeInventory = inventoryDao.getItemTypeInventory(itemType, Type.GOOD_INVENTORY);
        if (itemTypeInventory == null) {
            throw new InventoryNotAvailableException("Inventory not available for itemType:" + itemType.getId());
        }
        itemTypeInventory.setQuantity(itemTypeInventory.getQuantity() - 1);
        itemTypeInventory.setQuantityBlocked(itemTypeInventory.getQuantityBlocked() + 1);
        itemTypeInventory = inventoryDao.updateItemTypeInventory(itemTypeInventory);
        return itemTypeInventory;
    }

    @Override
    @Transactional
    public Map<ItemTypeInventory, Integer> blockAvailableInventory(ItemType itemType, int channelId, int quantity) {
        int quantityToBlock = quantity;
        Map<ItemTypeInventory, Integer> blockedInventories = new LinkedHashMap<ItemTypeInventory, Integer>();
        while (quantityToBlock > 0) {
            ItemTypeInventory itemTypeInventory = inventoryDao.getItemTypeInventory(itemType, Type.GOOD_INVENTORY);
            if (itemTypeInventory == null) {
                break;
            } else {
                int quantityAvailable = Math.min(itemTypeInventory.getQuantity(), quantityToBlock);
                itemTypeInventory.setQuantity(itemTypeInventory.getQuantity() - quantityAvailable);
                itemTypeInventory.setQuantityBlocked(itemTypeInventory.getQuantityBlocked() + quantityAvailable);
                blockedInventories.put(itemTypeInventory, quantityAvailable);
                quantityToBlock -= quantityAvailable;
            }
        }

        return blockedInventories;
    }

    /* (non-Javadoc)
     * @see com.uniware.services.inventory.IInventoryService#addInventory(com.uniware.core.entity.ItemType, com.uniware.core.entity.Shelf, int)
     */
    @Override
    @Transactional
    public ItemTypeInventory addInventory(ItemType itemType, Type inventoryType, Shelf shelf, int quantity) {
        if (ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).getTraceabilityLevel() == TraceabilityLevel.ITEM) {
            throw new RuntimeException("This method should not be called on ITEM traceability.");
        } else {
            return addInventory(itemType, inventoryType, Constants.ITEM_SKU_OR_NONE_TRACEABILITY_AGEING_DATE, shelf, quantity);
        }
    }

    @Override
    @Transactional
    public ItemTypeInventory addInventory(ItemType itemType, Type inventoryType, Date ageingStartDate, Shelf shelf, int quantity) {
        ItemTypeInventory itemTypeInventory = inventoryDao.getInventory(itemType, inventoryType, shelf, ageingStartDate);
        if (itemTypeInventory == null) {
            itemTypeInventory = new ItemTypeInventory(shelf, itemType, inventoryType, ageingStartDate, quantity, 0, DateUtils.getCurrentTime(), DateUtils.getCurrentTime());
            return inventoryDao.addItemTypeInventory(itemTypeInventory);
        } else {
            LOG.info("Updating item type inventory {}, quantity {}", itemTypeInventory, quantity);
            itemTypeInventory.setQuantity(itemTypeInventory.getQuantity() + quantity);
            return inventoryDao.updateItemTypeInventory(itemTypeInventory);
        }
    }

    @Override
    @Transactional
    public ItemTypeInventory addInventory(ItemType itemType, Type inventoryType, Shelf shelf, int quantity, Integer sla) {
        if (ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).getTraceabilityLevel() == TraceabilityLevel.ITEM) {
            throw new RuntimeException("This method should not be called on ITEM traceability.");
        } else {
            return addInventory(itemType, inventoryType, Constants.ITEM_SKU_OR_NONE_TRACEABILITY_AGEING_DATE, shelf, quantity, sla);
        }
    }

    @Override
    @Transactional
    public ItemTypeInventory addInventory(ItemType itemType, Type inventoryType, Date ageingStartDate, Shelf shelf, int quantity, Integer sla) {
        ItemTypeInventory itemTypeInventory = inventoryDao.getInventory(itemType, inventoryType, shelf, ageingStartDate);
        if (itemTypeInventory == null) {
            itemTypeInventory = new ItemTypeInventory(shelf, itemType, inventoryType, ageingStartDate, quantity, 0, sla, DateUtils.getCurrentTime(), DateUtils.getCurrentTime());
            return inventoryDao.addItemTypeInventory(itemTypeInventory);
        } else {
            itemTypeInventory.setQuantity(itemTypeInventory.getQuantity() + quantity);
            itemTypeInventory.setSla(sla);
            return inventoryDao.updateItemTypeInventory(itemTypeInventory);
        }
    }

    @Override
    @Transactional
    public ItemTypeInventory replaceInventory(ItemType itemType, Type inventoryType, Shelf shelf, int quantity) throws InventoryNotAvailableException {
        if (ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).getTraceabilityLevel() == TraceabilityLevel.ITEM) {
            throw new RuntimeException("This method should not be called on ITEM traceability.");
        } else {
            return replaceInventory(itemType, inventoryType, Constants.ITEM_SKU_OR_NONE_TRACEABILITY_AGEING_DATE, shelf, quantity);
        }
    }

    @Override
    @Transactional
    public ItemTypeInventory replaceInventory(ItemType itemType, Type inventoryType, Date ageingStartDate, Shelf shelf, int quantity) throws InventoryNotAvailableException {
        ItemTypeInventory itemTypeInventory = inventoryDao.getInventory(itemType, inventoryType, shelf, ageingStartDate);
        if (itemTypeInventory == null) {
            itemTypeInventory = new ItemTypeInventory(shelf, itemType, inventoryType, ageingStartDate, quantity, 0, DateUtils.getCurrentTime(), DateUtils.getCurrentTime());
            return inventoryDao.addItemTypeInventory(itemTypeInventory);
        } else {
            if (quantity < itemTypeInventory.getQuantityBlocked()) {
                throw new InventoryNotAvailableException("Inventory already blocked for orders, Shelf:" + shelf.getCode() + ", Type:" + inventoryType + ", Item SKU:"
                        + itemType.getSkuCode() + ", Blocked Quantity:" + itemTypeInventory.getQuantityBlocked());
            } else {
                itemTypeInventory.setQuantity(quantity - itemTypeInventory.getQuantityBlocked());
                return inventoryDao.updateItemTypeInventory(itemTypeInventory);
            }
        }
    }

    @Override
    @Transactional
    public ItemTypeInventory replaceInventory(ItemType itemType, Type inventoryType, Shelf shelf, int quantity, Integer sla) throws InventoryNotAvailableException {
        if (ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).getTraceabilityLevel() == TraceabilityLevel.ITEM) {
            throw new RuntimeException("This method should not be called on ITEM traceability.");
        } else {
            return replaceInventory(itemType, inventoryType, Constants.ITEM_SKU_OR_NONE_TRACEABILITY_AGEING_DATE, shelf, quantity, sla);
        }
    }

    @Override
    @Transactional
    public ItemTypeInventory replaceInventory(ItemType itemType, Type inventoryType, Date ageingStartDate, Shelf shelf, int quantity, Integer sla)
            throws InventoryNotAvailableException {
        ItemTypeInventory itemTypeInventory = inventoryDao.getInventory(itemType, inventoryType, shelf, ageingStartDate);
        if (itemTypeInventory == null) {
            itemTypeInventory = new ItemTypeInventory(shelf, itemType, inventoryType, ageingStartDate, quantity, 0, sla, DateUtils.getCurrentTime(), DateUtils.getCurrentTime());
            return inventoryDao.addItemTypeInventory(itemTypeInventory);
        } else {
            if (quantity < itemTypeInventory.getQuantityBlocked()) {
                throw new InventoryNotAvailableException("Inventory already blocked for orders, Shelf:" + shelf.getCode() + ", Type:" + inventoryType + ", Item SKU:"
                        + itemType.getSkuCode() + ", Blocked Quantity:" + itemTypeInventory.getQuantityBlocked());
            } else {
                itemTypeInventory.setQuantity(quantity - itemTypeInventory.getQuantityBlocked());
                if (sla != null) {
                    itemTypeInventory.setSla(sla);
                }
                return inventoryDao.updateItemTypeInventory(itemTypeInventory);
            }
        }
    }

    @Transactional
    @Override
    public List<Item> getItemsExpectedOnShelf(int shelfId) {
        return inventoryDao.getItemsByShelfAndStatus(shelfId, Arrays.asList(StatusCode.GOOD_INVENTORY, StatusCode.BAD_INVENTORY, StatusCode.QC_REJECTED));
    }

    @Transactional
    @Override
    public Item updateItemOptimistic(Item item, Integer shelfId, Integer cycleCountId) {
        return inventoryDao.updateItemOptimistic(item, shelfId, cycleCountId);
    }

    /**
     * @param shelf shelf for which item type inventories are fetched.
     * @param lock if true, then pessimistic locking is used. See
     *            <a href="https://docs.jboss.org/hibernate/orm/4.0/devguide/en-US/html/ch05.html">jboss doc</a>.
     * @return List of ItemTypeInventory
     */
    @Transactional(readOnly = true)
    @Override
    public List<ItemTypeInventory> getAllItemTypeInventoryForShelf(Shelf shelf, boolean lock) {
        Integer shelfId = shelf.getId();
        return inventoryDao.getAllItemTypeInventoryForShelf(shelfId, lock);
    }

    /**
     * Adds {@code quantity} inventory in system, of which item type is identified by following criteria:
     * {@code skuCode, type, ageing, shelf}
     */
    @Override
    @Transactional
    public ItemTypeInventory addInventory(String skuCode, Type inventoryType, Date ageingStartDate, Shelf shelf, int adjustedQuantity) {
        ItemTypeInventory itemTypeInventory = inventoryDao.getInventory(skuCode, inventoryType, shelf, ageingStartDate);
        if (itemTypeInventory == null) {
            ItemType itemType = catalogService.getItemTypeBySkuCode(skuCode);
            Date now = DateUtils.getCurrentTime();
            itemTypeInventory = new ItemTypeInventory(shelf, itemType, inventoryType, ageingStartDate, adjustedQuantity, 0, now, now);
            return inventoryDao.addItemTypeInventory(itemTypeInventory);
        } else {
            itemTypeInventory.setQuantity(itemTypeInventory.getQuantity() + adjustedQuantity);
            itemTypeInventory.setPriority(0);
            return inventoryDao.updateItemTypeInventory(itemTypeInventory);
        }
    }

    /**
     * Gets item type inventory as uniquely identified by sku, type and shelf.
     * 
     * @param quantityGreaterThanZero true if we require item type inventory to have at least 1 quantity.
     * @return Object of {@link ItemTypeInventory}
     */
    @Override
    @Transactional(readOnly = true)
    public ItemTypeInventory getItemTypeInventory(String skuCode, Type inventoryType, Shelf shelf, boolean quantityGreaterThanZero) {
        return inventoryDao.getInventoryBySkuCodeAndShelf(skuCode, inventoryType, shelf, quantityGreaterThanZero);
    }

    @Override
    @Transactional
    public ItemTypeInventory updateItemTypeInventory(ItemTypeInventory itemTypeInventory) {
        return inventoryDao.updateItemTypeInventory(itemTypeInventory);
    }

    @Transactional
    @Override
    public void commitBlockedInventory(int itemTypeInventoryId, int quantity) {
        inventoryDao.commitBlockedInventory(itemTypeInventoryId, quantity);
    }

    @Override
    @Transactional
    public ItemTypeInventory removeInventory(ItemType itemType, Type inventoryType, Shelf shelf, int quantity)
            throws InventoryNotAvailableException, ShelfBlockedForCycleCountException {
        if (ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).getTraceabilityLevel() == TraceabilityLevel.ITEM) {
            throw new RuntimeException("This method should not be called on ITEM traceability.");
        } else {
            return removeInventory(itemType, inventoryType, Constants.ITEM_SKU_OR_NONE_TRACEABILITY_AGEING_DATE, shelf, quantity);
        }
    }

    @Override
    @Transactional
    public ItemTypeInventory removeInventory(ItemType itemType, Type inventoryType, Shelf shelf, int quantity, Integer sla)
            throws InventoryNotAvailableException, ShelfBlockedForCycleCountException {
        if (ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).getTraceabilityLevel() == TraceabilityLevel.ITEM) {
            throw new RuntimeException("This method should not be called on ITEM traceability.");
        } else {
            return removeInventory(itemType, inventoryType, Constants.ITEM_SKU_OR_NONE_TRACEABILITY_AGEING_DATE, shelf, quantity, sla);
        }
    }

    @Override
    @Transactional
    public ItemTypeInventory removeInventory(ItemType itemType, Type inventoryType, Date ageingStartDate, Shelf shelf, int quantity, Integer sla)
            throws InventoryNotAvailableException, ShelfBlockedForCycleCountException {
        ItemTypeInventory itemTypeInventory = inventoryDao.getInventory(itemType, inventoryType, shelf, ageingStartDate);
        if (inventoryType.equals(Type.GOOD_INVENTORY) && shelf.isBlockedForCycleCount()) {
            throw new ShelfBlockedForCycleCountException("Shelf is blocked for Cycle Count");
        } else if (itemTypeInventory == null || itemTypeInventory.getQuantity() < quantity) {
            throw new InventoryNotAvailableException("Inventory at Shelf:" + shelf.getCode() + ", Type:" + inventoryType + ", Item SKU:" + itemType.getSkuCode() + ", Expected:"
                    + quantity + ", Found:" + (itemTypeInventory == null ? 0 : itemTypeInventory.getQuantity()));
        } else {
            itemTypeInventory.setQuantity(itemTypeInventory.getQuantity() - quantity);
            itemTypeInventory.setSla(sla);
            return inventoryDao.updateItemTypeInventory(itemTypeInventory);
        }
    }

    @Override
    @Transactional
    public ItemTypeInventory removeAgeAgnosticInventory(ItemType itemType, Type inventoryType) throws InventoryNotAvailableException, ShelfBlockedForCycleCountException {
        ItemTypeInventory itemTypeInventory;
        if (inventoryType.equals(Type.GOOD_INVENTORY)) {
            itemTypeInventory = inventoryDao.getItemTypeInventoryFromActiveShelf(itemType.getSkuCode(), inventoryType);
        } else {
            itemTypeInventory = inventoryDao.getItemTypeInventory(itemType, inventoryType);
        }
        if (itemTypeInventory == null) {
            if (inventoryType.equals(Type.GOOD_INVENTORY)) {
                itemTypeInventory = inventoryDao.getItemTypeInventory(itemType, inventoryType);
            }
            if (itemTypeInventory == null) {
                throw new InventoryNotAvailableException("No additional inventory available for this item type" + itemType.getSkuCode());
            } else {
                throw new ShelfBlockedForCycleCountException("All the shelves with available inventory for sku " + itemType.getSkuCode() + " are blocked for cycle count");
            }
        } else {
            itemTypeInventory.setQuantity(itemTypeInventory.getQuantity() - 1);
            return inventoryDao.updateItemTypeInventory(itemTypeInventory);
        }
    }

    @Transactional
    @Override
    public ItemTypeInventory blockInventory(Item item) throws InventoryNotAvailableException, ShelfBlockedForCycleCountException, InventoryAccuracyException {
        if (item.getInventoryType().equals(Type.GOOD_INVENTORY) && item.getShelf() != null && item.getShelf().isBlockedForCycleCount()) {
            throw new ShelfBlockedForCycleCountException("Shelf is blocked for cycle count");
        }
        InventoryAccuracyLevel inventoryAccuracyLevel = ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).getInventoryAccuracyLevel();
        if (inventoryAccuracyLevel == InventoryAccuracyLevel.SHELF && item.getInventoryType().equals(Type.GOOD_INVENTORY)) {
            if (item.getShelf() != null) {
                return blockInventory(item, item.getShelf());
            } else {
                throw new InventoryAccuracyException("Invalid inventory state, item should be assigned to a shelf in configuration InventoryAccuracyLevel.SHELF");
            }
        } else {
            if (item.getShelf() != null) {
                try {
                    return blockInventory(item, item.getShelf());
                } catch (InventoryNotAvailableException e) {
                    //just to let it get through to next statement
                }
            }
            return removeAgeAgnosticInventory(item.getItemType(), item.getInventoryType());
        }
    }

    /* (non-Javadoc)
     * @see com.uniware.services.inventory.IInventoryService#getItemTypeInventoryById(int)
     */
    @Override
    @Transactional
    public ItemTypeInventory getItemTypeInventoryById(int itemTypeInventoryId) {
        return inventoryDao.getItemTypeInventoryById(itemTypeInventoryId);
    }

    /* (non-Javadoc)
     * @see com.uniware.services.inventory.IInventoryService#markInventoryNotFound(int)
     */
    @Override
    @Transactional
    public void markInventoryNotFound(int itemTypeInventoryId) {
        inventoryDao.markInventoryNotFound(itemTypeInventoryId);
    }

    @Override
    @Transactional
    public void markInventoryNotFound(int itemTypeInventoryId, int qty) {
        inventoryDao.markInventoryNotFound(itemTypeInventoryId, qty);
    }

    /* (non-Javadoc)
     * @see com.uniware.services.inventory.IInventoryService#releaseInventory(int)
     */
    @Override
    @Transactional
    public void releaseInventory(int itemTypeInventoryId, int channelId, int quantity) {
        inventoryDao.releaseInventory(itemTypeInventoryId, quantity);
    }

    @Override
    public InventoryAdjustmentResponse adjustInventory(InventoryAdjustmentRequest request) {
        InventoryAdjustmentResponse response = new InventoryAdjustmentResponse();
        ValidationContext context = request.validate();
        WsInventoryAdjustment adjustment = request.getInventoryAdjustment();
        ItemType itemType = null;
        Shelf shelf = null, transferToShelf = null;
        adjustment.setInventoryType(adjustment.getInventoryType() == null ? Type.GOOD_INVENTORY : adjustment.getInventoryType());
        if (!context.hasErrors()) {
            if (ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).getTraceabilityLevel() == TraceabilityLevel.ITEM
                    && !(adjustment.getAdjustmentType().equals(AdjustmentType.TRANSFER)
                            && ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).getInventoryAccuracyLevel().equals(
                                    InventoryAccuracyLevel.ITEM_TYPE))) {
                context.addError(WsResponseCode.FEATURE_NOT_SUPPORTED, "This feature is not supported with configuration TraceabilityLevel.ITEM");
            } else if (adjustment.getAdjustmentType().equals(AdjustmentType.TRANSFER) && StringUtils.isEmpty(adjustment.getTransferToShelfCode())) {
                context.addError(WsResponseCode.INVALID_REQUEST, "transferToShelfCode is required with AdjustmentType:TRANSFER");
            } else {
                itemType = catalogService.getItemTypeBySkuCode(adjustment.getItemSKU());
                if (itemType == null) {
                    context.addError(WsResponseCode.INVALID_ITEM_TYPE);
                } else {
                    shelf = shelfService.getShelfByCode(adjustment.getShelfCode());
                    if (shelf == null) {
                        context.addError(WsResponseCode.INVALID_SHELF_CODE, "request.shelfCode is Invalid");
                    } else if (adjustment.getAdjustmentType().equals(AdjustmentType.TRANSFER)) {
                        transferToShelf = shelfService.getShelfByCode(adjustment.getTransferToShelfCode());
                        if (transferToShelf == null) {
                            context.addError(WsResponseCode.INVALID_SHELF_CODE, "request.transferToShelfCode is Invalid");
                        }
                    }
                }
            }

        }
        if (!context.hasErrors()) {
            try {
                User user = usersService.getUserWithDetailsById(request.getUserId());
                adjustInventory(itemType, adjustment.getInventoryType(), shelf, transferToShelf, adjustment.getQuantity(), adjustment.getAdjustmentType(), adjustment.getRemarks(),
                        user, adjustment.getSla());
                response.setSuccessful(true);
            } catch (InventoryNotAvailableException e) {
                context.addError(WsResponseCode.INVENTORY_NOT_FOUND_AT_EXPECTED_LOCATION, e.getMessage());
            } catch (ShelfBlockedForCycleCountException e) {
                context.addError(WsResponseCode.SHELF_BLOCKED_FOR_CYCLE_COUNT);
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Transactional
    public InventoryAdjustment adjustInventory(ItemType itemType, Type inventoryType, Shelf shelf, Shelf transferToShelf, int quantity, AdjustmentType adjustmentType,
            String remarks, User user, Integer sla) throws InventoryNotAvailableException, ShelfBlockedForCycleCountException {
        int oldQuantity = 0;
        ItemTypeInventory itemTypeInventory = getInventory(itemType, inventoryType, shelf);
        if (itemTypeInventory != null) {
            oldQuantity = itemTypeInventory.getQuantity() + itemTypeInventory.getQuantityBlocked();
        }
        if (AdjustmentType.ADD.equals(adjustmentType)) {
            addInventory(itemType, inventoryType, shelf, quantity, sla);
        } else if (AdjustmentType.REMOVE.equals(adjustmentType)) {
            removeInventory(itemType, inventoryType, shelf, quantity, sla);
        } else if (AdjustmentType.REPLACE.equals(adjustmentType)) {
            replaceInventory(itemType, inventoryType, shelf, quantity, sla);
        } else if (AdjustmentType.TRANSFER.equals(adjustmentType)) {
            removeInventory(itemType, inventoryType, shelf, quantity, sla);
            addInventory(itemType, inventoryType, transferToShelf, quantity, sla);
        }
        InventoryAdjustment inventoryAdjustment;
        if (transferToShelf != null) {
            inventoryAdjustment = new InventoryAdjustment(shelf.getCode(), transferToShelf.getCode(), itemType, adjustmentType, inventoryType, quantity, DateUtils.getCurrentTime(),
                    DateUtils.getCurrentTime());
        } else {
            inventoryAdjustment = new InventoryAdjustment(shelf.getCode(), itemType, adjustmentType, inventoryType, quantity, DateUtils.getCurrentTime(),
                    DateUtils.getCurrentTime());
        }
        if (AdjustmentType.ADD.equals(adjustmentType) || AdjustmentType.REMOVE.equals(adjustmentType) || AdjustmentType.REPLACE.equals(adjustmentType)) {
            inventoryAdjustment.setOldQuantity(oldQuantity);
        }
        inventoryAdjustment.setUsername(user.getUsername());
        inventoryAdjustment.setRemarks(remarks);
        InventoryAdjustment adjustment = inventoryDao.addInventoryAdjustment(inventoryAdjustment);
        if (inventoryAdjustment.getAdjustmentType() != AdjustmentType.TRANSFER) {
            addInventoryLedgerSummary(itemType, adjustment);
        }
        return adjustment;
    }

    /**
     * Creates ledger entries in case of inventory adjustment. Only works for adjustment types:
     * {@code ADD, REMOVE and REPLACE}. Does not work for type {@code TRANSFER}, and {@code REPLACE} is handled by
     * {@code ADD} or {@code REMOVE}. This method works only for ITEM_SKU or NONE traceability, so item codes are not
     * set.
     *
     * @see IInventoryLedgerService#addInventoryLedgerEntry(InventoryLedger, int, ChangeType)
     */
    private void addInventoryLedgerSummary(ItemType itemType, InventoryAdjustment inventoryAdjustment) {
        InventoryLedger inventoryLedger = new InventoryLedger();
        inventoryLedger.setSkuCode(itemType.getSkuCode());
        inventoryLedger.setTransactionIdentifier(inventoryAdjustment.getId().toString());
        inventoryLedger.setTransactionType(InventoryLedger.TransactionType.INVENTORY_ADJUSTMENT);
        inventoryLedger.setAdditionalInfo(inventoryAdjustment.getAdjustmentType().name());
        AdjustmentDetails adjustmentDetails = getAdjustmentDetails(inventoryAdjustment);
        inventoryLedgerService.addInventoryLedgerEntry(inventoryLedger, adjustmentDetails.getAdjustmentQuantity(), adjustmentDetails.getChangeType());

    }

    private AdjustmentDetails getAdjustmentDetails(InventoryAdjustment inventoryAdjustment) {
        ChangeType changeType;
        Integer quantity;
        switch (inventoryAdjustment.getAdjustmentType()) {
            case ADD:
                quantity = inventoryAdjustment.getQuantity();
                changeType = ChangeType.INCREASE;
                break;
            case REMOVE:
                quantity = inventoryAdjustment.getQuantity();
                changeType = ChangeType.DECREASE;
                break;
            case REPLACE:
                quantity = Math.abs(inventoryAdjustment.getOldQuantity() - inventoryAdjustment.getQuantity());
                changeType = inventoryAdjustment.getQuantity() > inventoryAdjustment.getOldQuantity() ? ChangeType.INCREASE : ChangeType.DECREASE;
                break;
            default:
                throw new RuntimeException("Invalid adjustment type: " + inventoryAdjustment.getAdjustmentType().name());
        }
        return new AdjustmentDetails(changeType, quantity);
    }

    /* (non-Javadoc)
     * @see com.uniware.services.inventory.IInventoryService#getItemById(int)
     */
    @Override
    public Item getItemById(int itemId) {
        return inventoryDao.getItemById(itemId);
    }

    /* (non-Javadoc)
     * @see com.uniware.services.inventory.IInventoryService#getItemByCode(java.lang.String)
     */
    @Override
    @Transactional
    public Item getItemByCode(String itemCode) {
        return getItemByCode(itemCode, false);
    }

    @Override
    @Transactional
    public Item getItemByCode(String itemCode, boolean fetchSaleOrderItems) {
        return inventoryDao.getItemByCode(itemCode, fetchSaleOrderItems);
    }

    /* (non-Javadoc)
     * @see com.uniware.services.inventory.IInventoryService#addItems(com.uniware.core.entity.ItemType, com.uniware.core.entity.Vendor, int)
     */
    @Override
    @Transactional
    @LogActivity
    public List<Item> addItems(ItemType itemType, Vendor vendor, int quantity, InflowReceiptItem inflowReceiptItem, StatusCode statusCode) {
        List<Item> items = new ArrayList<>();
        for (int i = 0; i < quantity; i++) {
            Item item = new Item(inflowReceiptItem, vendor, itemType, DateUtils.getCurrentTime(), DateUtils.getCurrentTime());
            item.setStatusCode(statusCode.name());
            item = inventoryDao.addItem(item);
            items.add(item);
            if (ActivityContext.current().isEnable()) {
                ActivityUtils.appendActivity(item.getCode(), ActivityEntityEnum.ITEM.getName(), null,
                        Collections.singletonList("Item {" + ActivityEntityEnum.ITEM + ":" + item.getCode() + "} created."), Item.ActivityType.CREATED.name());
            }
        }
        return items;
    }

    @Override
    @Transactional
    public List<Item> addItems(ItemType itemType, Vendor vendor, int quantity, StatusCode statusCode, VendorItemType vendorItemtype) {
        List<Item> items = new ArrayList<>();
        for (int i = 0; i < quantity; i++) {
            Item item = new Item(vendor, itemType, vendorItemtype.getVendorSkuCode(), DateUtils.getCurrentTime(), DateUtils.getCurrentTime());
            item.setStatusCode(statusCode.name());
            items.add(item);
        }
        items = inventoryDao.addItems(items);
        return items;
    }

    @Override
    @Transactional
    @LogActivity
    public List<Item> addItemsWithCodeAndDetails(ItemType itemType, Vendor vendor, List<AutoGrnItemsImportJobHandler.ItemDTO> itemDTOs, InflowReceiptItem inflowReceiptItem,
            StatusCode statusCode, ValidationContext context) {
        List<Item> items = new ArrayList<>();
        List<ItemDetailFieldDTO> itemDetailFields = CacheManager.getInstance().getCache(ItemTypeDetailCache.class).getItemDetailFields(itemType);
        itemDTOs.forEach(itemDTO -> {
            Item item = new Item(inflowReceiptItem, vendor, itemType, DateUtils.getCurrentTime(), DateUtils.getCurrentTime());
            item.setCode(itemDTO.getCode());
            item.setStatusCode(statusCode.name());
            Map<String, String> itemFieldValues = new HashMap<>();
            if (StringUtils.isNotBlank(itemDTO.getItemDetails()) && !CollectionUtils.isEmpty(itemDetailFields)) {
                HashMap<String, String> itemCustomFields = JsonUtils.stringToJson(itemDTO.getItemDetails(), HashMap.class);
                itemDetailFields.stream().filter(i -> !context.hasErrors()).forEach(itemDetail -> {
                    String value = itemCustomFields.get(itemDetail.getName());
                    if (StringUtils.isNotBlank(itemDetail.getValidatorPattern()) && value != null && !value.matches(itemDetail.getValidatorPattern())) {
                        context.addError(WsResponseCode.ITEM_DETAILS_INVALID_FORMAT, "Invalid item detail value for :" + itemDetail.getName());
                    } else {
                        itemFieldValues.put(itemDetail.getName(), value);
                    }
                });
            }
            if(!CollectionUtils.isEmpty(itemDetailFields)){
                item.setItemDetails(new Gson().toJson(itemFieldValues));
            }
            item = inventoryDao.addItem(item, false);
            items.add(item);
            if (ActivityContext.current().isEnable()) {
                ActivityUtils.appendActivity(item.getCode(), ActivityEntityEnum.ITEM.getName(), null,
                        Collections.singletonList("Item {" + ActivityEntityEnum.ITEM + ":" + item.getCode() + "} created from auto grn."), Item.ActivityType.CREATED.name());
            }
        });
        return items;
    }

    @Override
    @Transactional
    public Item updateItem(Item item) {
        return inventoryDao.updateItem(item);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Item> getItems(List<String> itemCodes) {
        return inventoryDao.getItems(itemCodes);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Item> getItemsByCodes(List<String> itemCodes) {
        return inventoryDao.getItemsByCodes(itemCodes);
    }

    @Override
    @Transactional
    public ItemTypeInventorySnapshot getItemTypeInventorySnapshot(Integer itemTypeId) {
        return inventoryDao.getItemTypeInventorySnapshot(itemTypeId);
    }

    @Override
    @Transactional
    public List<ItemTypeInventorySnapshot> getItemTypeInventorySnapshots(Integer itemTypeId, List<Integer> facilityIds) {
        return inventoryDao.getItemTypeInventorySnapshots(itemTypeId, facilityIds);
    }

    @Override
    @Transactional(readOnly = true)
    public GetInventorySnapshotResponse getInventorySnapshot(GetInventorySnapshotRequest request) {
        GetInventorySnapshotResponse response = new GetInventorySnapshotResponse();
        ValidationContext context = request.validate();
        List<ItemTypeInventorySnapshot> inventorySnapshots = new ArrayList<>();
        if (!context.hasErrors()) {
            if (request.getUpdatedSinceInMinutes() != null && request.getItemTypeSKUs() != null) {
                if (request.getUpdatedSinceInMinutes() <= DateUtils.MINUTES_IN_A_DAY) {
                    DateRange dateRange = DateUtils.getPastInterval(DateUtils.getCurrentTime(), new Interval(TimeUnit.MINUTES, request.getUpdatedSinceInMinutes()));
                    inventorySnapshots = inventoryDao.getItemTypeInventorySnapshotsBySKUsInDateRange(dateRange, request.getItemTypeSKUs());
                } else {
                    context.addError(WsResponseCode.INVALID_REQUEST, "You can query for only one day snapshots");
                }
            } else if (request.getUpdatedSinceInMinutes() != null) {
                if (request.getUpdatedSinceInMinutes() <= DateUtils.MINUTES_IN_A_DAY) {
                    DateRange dateRange = DateUtils.getPastInterval(DateUtils.getCurrentTime(), new Interval(TimeUnit.MINUTES, request.getUpdatedSinceInMinutes()));
                    inventorySnapshots = inventoryDao.getItemTypeInventorySnapshotsInDateRange(dateRange);
                } else {
                    context.addError(WsResponseCode.INVALID_REQUEST, "You can query for only one day snapshots");
                }
            } else if (request.getItemTypeSKUs() != null) {
                inventorySnapshots = inventoryDao.getItemTypeInventorySnapshots(request.getItemTypeSKUs());
            } else {
                context.addError(WsResponseCode.INVALID_REQUEST, "Either Item Sku or Updated Since required");
            }
        }
        List<String> validSKUs = new ArrayList<>();
        if (!context.hasErrors()) {
            if (inventorySnapshots.size() > 0) {
                for (ItemTypeInventorySnapshot inventorySnapshot : inventorySnapshots) {
                    response.addInventorySnapshots(prepareWsItemTypeInventorySnapshot(inventorySnapshot));
                    validSKUs.add(inventorySnapshot.getItemType().getSkuCode());
                }
                response.setSuccessful(true);
            } else {
                response.setSuccessful(false);
                context.addError(WsResponseCode.INVENTORY_NOT_AVAILABLE, "Could not find any any items");
            }
        }

        if (request.getItemTypeSKUs() != null && request.getUpdatedSinceInMinutes() == null) {
            request.getItemTypeSKUs().removeAll(validSKUs);
            if (!request.getItemTypeSKUs().isEmpty()) {
                context.addWarning(WsResponseCode.INVALID_ITEM_TYPE, "Invalid item type SKUs:[" + StringUtils.join(',', request.getItemTypeSKUs()) + "]");
            }
        }
        response.setWarnings(context.getWarnings());
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    public GetInventorySyncResponse getInventorySync(GetInventorySyncRequest request) {
        GetInventorySyncResponse response = new GetInventorySyncResponse();
        ValidationContext context = request.validate();
        List<ChannelInventoryStatusDTO> channelInventory = new ArrayList<>();
        if (!context.hasErrors()) {
            List<ChannelInventorySyncStatusDTO> lastInventoryUpdates = channelService.getChannelInventorySyncStatuses();
            for (ChannelInventorySyncStatusDTO inventorySyncStatusDTO : lastInventoryUpdates) {
                Channel channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelByCode(inventorySyncStatusDTO.getChannelCode());
                Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(channel.getSourceCode());
                ChannelInventoryStatusDTO inventoryStatusDTO = new ChannelInventoryStatusDTO(new ChannelSyncDTO(channel, source));
                inventoryStatusDTO.setLastInventorySyncResult(inventorySyncStatusDTO);
                channelInventory.add(inventoryStatusDTO);
            }
            response.setLastInventorySyncResults(channelInventory);
            response.setSuccessful(true);
        }
        response.setErrors(context.getErrors());
        return response;
    }

    private WsItemTypeInventorySnapshot prepareWsItemTypeInventorySnapshot(ItemTypeInventorySnapshot inventorySnapshot) {
        WsItemTypeInventorySnapshot snapshot = new WsItemTypeInventorySnapshot();
        snapshot.setInventory(inventorySnapshot.getInventory());
        snapshot.setItemTypeSKU(inventorySnapshot.getItemType().getSkuCode());
        snapshot.setOpenSale(inventorySnapshot.getOpenSale());
        snapshot.setOpenPurchase(inventorySnapshot.getOpenPurchase());
        snapshot.setPutawayPending(inventorySnapshot.getPutawayPending());
        snapshot.setInventoryBlocked(inventorySnapshot.getInventoryBlocked());
        snapshot.setPendingStockTransfer(inventorySnapshot.getPendingStockTransfer());
        snapshot.setVendorInventory(inventorySnapshot.getVendorInventory());
        snapshot.setVirtualInventory(inventorySnapshot.getVirtualInventory());
        return snapshot;
    }

    @Override
    @Transactional
    public CreateItemLabelsResponse createItemLabels(CreateItemLabelsRequest request) {
        CreateItemLabelsResponse response = new CreateItemLabelsResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            if (TraceabilityLevel.ITEM != ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).getTraceabilityLevel()) {
                context.addError(WsResponseCode.INVALID_API_REQUEST, "Operation only valid for item level traceability");
            } else {
                ItemType itemType = catalogService.getItemTypeBySkuCode(request.getItemSkuCode());
                if (itemType == null) {
                    context.addError(WsResponseCode.INVALID_ITEM_TYPE, "INVALID_ITEM_TYPE");
                } else {
                    boolean expirable = catalogService.isItemTypeExpirable(itemType);
                    List<Item> items = inventoryDao.createItemLabels(itemType.getId(), request.getVendorSkuCode(), request.getVendorId(), request.getQuantity(),
                            request.getManufacturingDate(), expirable);
                    for (Item item : items) {
                        response.addItemCode(item.getCode());
                    }
                    response.setSuccessful(true);
                }
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Transactional
    public AddItemLabelsResponse addItemLabelsInternal(AddItemLabelsRequest request) {
        AddItemLabelsResponse response = new AddItemLabelsResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            ItemType itemType = catalogService.getItemTypeBySkuCode(request.getItemSkuCode());
            if (itemType == null) {
                context.addError(WsResponseCode.INVALID_ITEM_TYPE, "INVALID_ITEM_TYPE");
            } else if (catalogService.isItemTypeExpirable(itemType) && request.getManufacturingDate() == null) {
                context.addError(WsResponseCode.INVALID_API_REQUEST, "Manufacturing Date cant be null for expirable items.");
            } else {
                createItemLabels(itemType.getSkuCode(), request.getItemCodes(), request.getManufacturingDate(), context);
                response.setSuccessful(true);
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    public AddItemLabelsResponse addItemLabels(AddItemLabelsRequest request) {
        try {
            return addItemLabelsInternal(request);
        } catch (DataIntegrityViolationException e) {
            return (AddItemLabelsResponse) new AddItemLabelsResponse().addError(
                    new WsError(WsResponseCode.DUPLICATE_ITEM_CODE.code(), WsResponseCode.DUPLICATE_ITEM_CODE.message(), "Duplicate Item Code"));
        }
    }

    @Override
    @Transactional
    public AddOrEditItemLabelsResponse addOrEditItemLabels(AddOrEditItemLabelsRequest request) {
        AddOrEditItemLabelsResponse response = new AddOrEditItemLabelsResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            ItemType itemType = catalogService.getItemTypeBySkuCode(request.getItemSkuCode());
            if (itemType == null) {
                context.addError(WsResponseCode.INVALID_ITEM_TYPE, "INVALID_ITEM_TYPE");
            } else {
                boolean expirable = catalogService.isItemTypeExpirable(itemType);
                List<String> itemCodesToCreate = new ArrayList<>();
                for (String itemCode : request.getItemCodes()) {
                    Item item = inventoryDao.getItemByCode(itemCode);
                    if (item == null) {
                        itemCodesToCreate.add(itemCode);
                    } else if (StatusCode.LIQUIDATED.name().equals(item.getStatusCode())) {
                        item.setItemType(itemType);
                        item.setInventoryType(Type.GOOD_INVENTORY);
                        item.setStatusCode(StatusCode.UNPROCESSED.name());
                        if (ActivityContext.current().isEnable()) {
                            ActivityUtils.appendActivity(item.getCode(), ActivityEntityEnum.ITEM.getName(), null,
                                    Collections.singletonList("Item {" + ActivityEntityEnum.ITEM.name() + ":" + item.getCode() + "} moved to unprocessed."),
                                    Item.ActivityType.UNPROCESSED.name());
                        }
                    } else {
                        context.addError(WsResponseCode.INVALID_ITEM_STATE, "Item with the same code exists in warehouse");
                    }
                }
                if (!context.hasErrors() && itemCodesToCreate.size() > 0) {
                    if (expirable && request.getManufacturingDate() == null) {
                        context.addError(WsResponseCode.INVALID_ITEM_STATE, "Manufacturing date cant be null for expirable items");
                    } else if (expirable && request.getManufacturingDate().after(DateUtils.getCurrentTime())) {
                        context.addError(WsResponseCode.INVALID_API_REQUEST, "Manufacturing Date cant be future");
                    } else {
                        createItemLabels(itemType.getSkuCode(), itemCodesToCreate, request.getManufacturingDate(), context);
                    }
                }
            }
        }
        response.setSuccessful(!context.hasErrors());
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Item> getItemsByItemTypeByStatus(String skuCode, String status, Integer offset, Integer limit) {
        return inventoryDao.getItemsByItemTypeByStatus(skuCode, status, offset, limit);
    }

    @Override
    @Transactional(readOnly = true)
    public List<ItemTypeInventorySnapshot> getItemTypeInventorySnapshotByTenant(Integer itemTypeId) {
        return inventoryDao.getItemTypeInventorySnapshotByTenant(itemTypeId);
    }

    @Override
    @Locks({ @Lock(ns = Namespace.ITEM, key = "#{#args[0].itemCode}", level = Level.FACILITY) })
    @Transactional
    public MarkTraceableInventoryDamagedResponse markTraceableInventoryDamaged(MarkTraceableInventoryDamagedRequest request) {
        MarkTraceableInventoryDamagedResponse response = new MarkTraceableInventoryDamagedResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            Item item = inventoryDao.getItemByCode(request.getItemCode());
            if (item == null) {
                context.addError(WsResponseCode.INVALID_ITEM_CODE, "Invalid Item Code");
            } else if (!StatusCode.GOOD_INVENTORY.name().equals(item.getStatusCode())) {
                context.addError(WsResponseCode.INVALID_ITEM_STATE, "Item not in GOOD_INVENTORY state");
            } else {
                try {
                    ItemTypeInventory itemTypeInventory = blockInventory(item);
                    addInventory(item.getItemType(), Type.BAD_INVENTORY, item.getAgeingStartDate(), itemTypeInventory.getShelf(), 1);
                } catch (InventoryNotAvailableException e) {
                    context.addError(WsResponseCode.INVENTORY_NOT_AVAILABLE, "Item inventory has already been assigned");
                } catch (ShelfBlockedForCycleCountException e) {
                    context.addError(WsResponseCode.SHELF_BLOCKED_FOR_CYCLE_COUNT, e.getMessage());
                } catch (InventoryAccuracyException e) {
                    context.addError(WsResponseCode.INVALID_ITEM_CODE, e.getMessage());
                }
                if (!context.hasErrors()) {
                    item.setInventoryType(Type.BAD_INVENTORY);
                    item.setStatusCode(StatusCode.BAD_INVENTORY.name());
                    item.setRejectionReason(request.getRejectionReason());
                    response.setSuccessful(true);
                }
            }
        }
        response.addErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Integer> getPendingNonBundleItemTypesForInventoryNotifications(int start, int pageSize) {
        List<Integer> facilityIds = CacheManager.getInstance().getCache(FacilityCache.class).getFacilitiesForChannelInventorySync();
        return inventoryDao.getPendingNonBundleItemTypesForInventoryNotifications(facilityIds, start, pageSize);
    }

    @Override
    @Transactional(readOnly = true)
    public List<ItemTypeInventorySnapshot> getItemTypeInventorySnapshots(List<Integer> itemTypeIds) {
        return inventoryDao.getItemTypeInventorySnapshotsForItemTypes(itemTypeIds);
    }

    @Override
    @Transactional(readOnly = true)
    public List<InventorySnapshotDTO> getPendingNonBundleItemTypeInventoryNotifications() {
        List<Integer> facilityIds = CacheManager.getInstance().getCache(FacilityCache.class).getFacilitiesForChannelInventorySync();
        return inventoryDao.getPendingNonBundleItemTypeInventoryNotifications(facilityIds);
    }

    @Override
    @Transactional(readOnly = true)
    public List<InventorySnapshotDTO> getPendingBundleItemTypeInventoryNotifications() {
        List<Integer> facilityIds = CacheManager.getInstance().getCache(FacilityCache.class).getFacilitiesForChannelInventorySync();
        return inventoryDao.getPendingBundleItemTypeInventoryNotifications(facilityIds);
    }

    @Override
    @Transactional(readOnly = true)
    public InventorySnapshotDTO getItemTypeInventoryNotification(int itemTypeId) {
        List<Integer> facilityIds = CacheManager.getInstance().getCache(FacilityCache.class).getFacilitiesForChannelInventorySync();
        return inventoryDao.getItemTypeInventoryNotification(itemTypeId, facilityIds);
    }

    @Override
    @Transactional
    public EditReorderItemLevelResponse editReorderItemLevel(EditReorderItemLevelRequest request) {
        EditReorderItemLevelResponse response = new EditReorderItemLevelResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            ItemTypeInventorySnapshot inventorySnapshot = inventoryDao.getItemTypeInventorySnapshot(request.getSkuCode());
            if (inventorySnapshot == null) {
                context.addError(WsResponseCode.INVALID_ITEM_TYPE, "Invalid Item Type");
            } else {
                inventorySnapshot.setMinBinQuantity(request.getMinBinSize());
                response.setSuccessful(true);
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    public String getItemLabelHtml(List<String> itemCodes) {
        List<Item> items = getItems(itemCodes);
        if (items != null && items.size() > 0) {
            Template template = CacheManager.getInstance().getCache(PrintTemplateCache.class).getTemplateByType(PrintTemplateVO.Type.ITEM_LABEL.name());
            Map<String, Object> params = new HashMap<>();
            params.put("items", items);
            return template.evaluate(params);
        }
        return null;
    }

    @Override
    @Transactional
    public List<ItemTypeInventory> getItemTypeInventoryByItemType(ItemType itemType) {
        return inventoryDao.getItemTypeInventoryByItemType(itemType.getId());
    }

    @Override
    @Transactional
    public List<ItemTypeInventory> getItemTypeInventoryByItemTypeId(Integer itemTypeId) {
        return inventoryDao.getItemTypeInventoryByItemType(itemTypeId);
    }

    @Override
    @Transactional
    public List<ItemTypeInventory> getItemTypeInventoryByItemType(ItemType itemType, List<Integer> facilityIds) {
        return inventoryDao.getItemTypeInventoryByItemType(itemType.getId(), facilityIds);
    }

    @Override
    public ItemTypeInventory removeInventory(ItemType itemType, Type inventoryType, Date ageingStartDate, Shelf shelf, int quantity)
            throws InventoryNotAvailableException, ShelfBlockedForCycleCountException {
        ItemTypeInventory itemTypeInventory = inventoryDao.getInventory(itemType, inventoryType, shelf, ageingStartDate);
        if (inventoryType.equals(Type.GOOD_INVENTORY) && shelf.isBlockedForCycleCount()) {
            throw new ShelfBlockedForCycleCountException("Shelf is blocked for Cycle Count");
        } else if (itemTypeInventory == null || itemTypeInventory.getQuantity() < quantity) {
            throw new InventoryNotAvailableException("Inventory at Shelf:" + shelf.getCode() + ", Type:" + inventoryType + ", Item SKU:" + itemType.getSkuCode() + ", Expected:"
                    + quantity + ", Found:" + (itemTypeInventory == null ? 0 : itemTypeInventory.getQuantity()));
        } else {
            itemTypeInventory.setQuantity(itemTypeInventory.getQuantity() - quantity);
            return inventoryDao.updateItemTypeInventory(itemTypeInventory);
        }
    }

    @Override
    @Transactional
    public MarkItemTypeQuantityFoundResponse markItemTypeQuantityFound(MarkItemTypeQuantityFoundRequest request) {
        MarkItemTypeQuantityFoundResponse response = new MarkItemTypeQuantityFoundResponse();
        ValidationContext context = request.validate();
        Shelf shelf = null;
        ItemType itemType = null;
        if (!context.hasErrors()) {
            shelf = shelfService.getShelfByCode(request.getShelfCode());
            itemType = catalogService.getItemTypeBySkuCode(request.getItemSku());
            validateShelfAndItemType(shelf, itemType, request, context);
        }
        if (!context.hasErrors()) {
            ItemTypeQuantityNotFoundDTO itemTypeQuantityNotFoundDTO = new ItemTypeQuantityNotFoundDTO();
            itemTypeQuantityNotFoundDTO.setItemSku(request.getItemSku());
            itemTypeQuantityNotFoundDTO.setShelfCode(request.getShelfCode());
            List<ItemTypeInventory> itemTypeInventories = getItemTypeInventories(request, shelf, itemType);
            doMarkItemTypeQuantityFoundInternal(itemTypeInventories, itemTypeQuantityNotFoundDTO, request, context);
            response.getItemQuantityNotFoundDTO().add(itemTypeQuantityNotFoundDTO);
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        } else {
            response.setSuccessful(true);
        }
        return response;
    }

    private void validateShelfAndItemType(Shelf shelf, ItemType itemType, MarkItemTypeQuantityFoundRequest request, ValidationContext context) {
        if (ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).isAgeingFollowedStrictly() && request.getAgeingStartDate() == null) {
            context.addError(WsResponseCode.INVALID_REQUEST, "Ageing date is required.");
            return;
        }
        validateShelf(shelf, context);
        validateItemType(itemType, context);
    }

    private void validateItemType(ItemType itemType, ValidationContext context) {
        if (itemType == null) {
            context.addError(WsResponseCode.INVALID_ITEM_CODE, "Item code is invalid");
        }
    }

    private void validateShelf(Shelf shelf, ValidationContext context) {
        if (shelf == null) {
            context.addError(WsResponseCode.INVALID_SHELF_CODE, "Shelf code is not valid");
        } else if (shelf.isBlockedForCycleCount()) {
            context.addError(WsResponseCode.SHELF_BLOCKED_FOR_CYCLE_COUNT);
        }
    }

    private List<ItemTypeInventory> getItemTypeInventories(MarkItemTypeQuantityFoundRequest request, Shelf shelf, ItemType itemType) {
        if (ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).isAgeingFollowedStrictly()) {
            ItemTypeInventory itemTypeInventory = getItemTypeInventory(itemType.getSkuCode(), Type.GOOD_INVENTORY, shelf, request.getAgeingStartDate());
            return Collections.singletonList(itemTypeInventory);
        } else {
            return inventoryDao.getNotFoundInventories(itemType.getSkuCode(), Type.GOOD_INVENTORY, shelf);
        }
    }

    private void doMarkItemTypeQuantityFoundInternal(List<ItemTypeInventory> itemTypeInventories, ItemTypeQuantityNotFoundDTO itemTypeQuantityNotFoundDTO,
            MarkItemTypeQuantityFoundRequest request, ValidationContext context) {
        if (itemTypeInventories.isEmpty()) {
            context.addError(WsResponseCode.INVALID_REQUEST, "Could not find item type inventory.");
            return;
        }
        Integer totalQuantityNotFound = itemTypeInventories.stream().mapToInt(ItemTypeInventory::getQuantityNotFound).sum();
        if (request.getQuantityFound() > totalQuantityNotFound) {
            context.addError(WsResponseCode.INVALID_QUANTITY_NOT_FOUND_VALUE, "Quantity found cannot be greater than not found quantity");
            return;
        }
        itemTypeQuantityNotFoundDTO.setNotFoundQuantity(totalQuantityNotFound - request.getQuantityFound());
        Integer requestQuantityFound = request.getQuantityFound();
        for (ItemTypeInventory itemTypeInventory : itemTypeInventories) {
            if (requestQuantityFound == 0) {
                break;
            }
            Integer maxInventoryThatCanBeFound = Integer.min(requestQuantityFound, itemTypeInventory.getQuantityNotFound());
            itemTypeInventory.setQuantityNotFound(itemTypeInventory.getQuantityNotFound() - maxInventoryThatCanBeFound);
            itemTypeInventory.setQuantity(itemTypeInventory.getQuantity() + maxInventoryThatCanBeFound);
            requestQuantityFound -= maxInventoryThatCanBeFound;
        }
    }

    @Override
    @Transactional
    public MarkItemTypeInventoryNotFoundResponse markItemTypeInventoryNotFound(MarkItemTypeInventoryNotFoundRequest request) {
        MarkItemTypeInventoryNotFoundResponse response = new MarkItemTypeInventoryNotFoundResponse();
        ValidationContext context = request.validate();
        Shelf shelf = shelfService.getShelfByCode(request.getShelfCode());
        if (shelf == null) {
            context.addError(WsResponseCode.INVALID_SHELF_CODE, "Shelf code is not valid");
        } else if (shelf.isBlockedForCycleCount()) {
            context.addError(WsResponseCode.INVALID_SHELF_CODE, "Shelf blocked for Cycle Count");
        }
        ItemType itemType = catalogService.getItemTypeBySkuCode(request.getItemSku());
        if (itemType == null) {
            context.addError(WsResponseCode.INVALID_ITEM_CODE, "Item code is invalid");
        }
        if (!context.hasErrors()) {
            List<ItemTypeInventory> itemTypeInventories = inventoryDao.getItemTypeInventoryByIdAndShelf(itemType.getId(), shelf.getId(), Type.GOOD_INVENTORY, true);
            itemTypeInventories.forEach(itemTypeInventory -> {
                itemTypeInventory.setQuantityNotFound(itemTypeInventory.getQuantityNotFound() + itemTypeInventory.getQuantity());
                itemTypeInventory.setQuantity(0);
                response.getInventoryDTOs().add(new ItemTypeInventoryDTO(itemTypeInventory));
            });
        }
        response.setErrors(context.getErrors());
        response.setSuccessful(!context.hasErrors());
        return response;
    }

    @Override
    @Transactional
    public MarkItemtypeQuantityNotFoundResponse markItemTypeQuantityNotFound(MarkItemTypeQuantityNotFoundRequest request) {
        MarkItemtypeQuantityNotFoundResponse response = new MarkItemtypeQuantityNotFoundResponse();
        ValidationContext context = request.validate();
        Shelf shelf = shelfService.getShelfByCode(request.getShelfCode());
        if (shelf == null) {
            context.addError(WsResponseCode.INVALID_SHELF_CODE, "Shelf code is not valid");
        }
        ItemType itemType = catalogService.getItemTypeBySkuCode(request.getItemSku());
        if (itemType == null) {
            context.addError(WsResponseCode.INVALID_ITEM_CODE, "Item code is invalid");
        }
        if (!context.hasErrors()) {
            ItemTypeInventory itemTypeInventory = inventoryDao.getInventory(itemType, Type.GOOD_INVENTORY, shelf);
            if (itemTypeInventory != null) {
                if (request.getQuantityNotFound() > itemTypeInventory.getQuantity()) {
                    context.addError(WsResponseCode.INVALID_QUANTITY_NOT_FOUND_VALUE, "Quantity not found cannot be greater then quantity");
                } else {
                    itemTypeInventory.setQuantityNotFound(itemTypeInventory.getQuantityNotFound() + request.getQuantityNotFound());
                    itemTypeInventory.setQuantity(itemTypeInventory.getQuantity() - request.getQuantityNotFound());
                    ItemTypeInventoryDTO itemTypeInventoryDTO = new ItemTypeInventoryDTO(itemTypeInventory);
                    response.getInventoryDTOs().add(itemTypeInventoryDTO);
                    response.setSuccessful(true);
                }
            } else {
                context.addError(WsResponseCode.INVALID_API_REQUEST, "No inventory found on shelf " + request.getShelfCode());
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    public void reSyncAllItemTypes() {
        inventoryDao.reSyncAllItemTypes();
    }

    @Override
    @Transactional
    public ReassessInventoryForItemTypeResponse reassessInventoryForItemType(ReassessInventoryForItemTypeRequest request) {
        ReassessInventoryForItemTypeResponse response = new ReassessInventoryForItemTypeResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            ItemType itemType = catalogService.getItemTypeBySkuCode(request.getItemSku());
            if (itemType == null) {
                context.addError(WsResponseCode.INVALID_ITEM_TYPE);
            } else {
                List<ItemTypeInventorySnapshot> inventorySnapshots = inventoryDao.getAllItemTypeInventorySnapshots(itemType.getId());
                if (inventorySnapshots.isEmpty()) {
                    createItemTypeInventorySnapshot(itemType);
                } else {
                    ItemTypeInventorySnapshot inventorySnapshot = inventorySnapshots.get(0);
                    inventorySnapshot.setAcknowledged(false);
                    inventoryDao.update(inventorySnapshot);
                }
                response.setSuccessful(true);
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    @Override
    @Transactional
    public void markBundlesUnacknowledged(List<Bundle> bundles) {
        for (Bundle bundle : bundles) {
            List<ItemTypeInventorySnapshot> inventorySnapshots = inventoryDao.getAllItemTypeInventorySnapshots(bundle.getItemType().getId());
            if (inventorySnapshots.isEmpty()) {
                createItemTypeInventorySnapshot(bundle.getItemType());
            } else {
                ItemTypeInventorySnapshot inventorySnapshot = inventorySnapshots.get(0);
                inventorySnapshot.setAcknowledged(false);
                inventoryDao.update(inventorySnapshot);
            }
        }
    }

    @Override
    @Transactional
    public ItemTypeInventorySnapshot createItemTypeInventorySnapshot(ItemType itemType) {
        ItemTypeInventorySnapshot itis = new ItemTypeInventorySnapshot();
        itis.setItemType(itemType);
        itis.setAcknowledged(false);
        itis.setCreated(DateUtils.getCurrentTime());
        List<Facility> facilities = facilityService.getAllEnabledFacilities();
        itis.setFacility(facilities.get(0));
        inventoryDao.persist(itis);
        return itis;
    }

    /**
     * For a given {@code ItemType} and quantity, generates given number of {@code Item}(s) for given {@code Vendor}.
     * This would be called from vendors on B2B clients, (eg. Jade, Myntra) for generating client friendly labels.
     *
     * @param request
     * @return
     */
    @Override
    @Transactional
    public GenerateItemLabelsResponse generateItemLabels(GenerateItemLabelsRequest request) {
        GenerateItemLabelsResponse response = new GenerateItemLabelsResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            for (WsItemLabel wsItemLabel : request.getItemLabelList()) {
                CreateItemLabelsRequest createLabelsRequest = new CreateItemLabelsRequest();
                createLabelsRequest.setItemSkuCode(wsItemLabel.getItemSkuCode());
                createLabelsRequest.setQuantity(wsItemLabel.getQuantity());
                createLabelsRequest.setVendorSkuCode(wsItemLabel.getVendorSkuCode());
                createLabelsRequest.setVendorId(request.getVendorId());
                createLabelsRequest.setManufacturingDate(request.getManufacturingDate());
                CreateItemLabelsResponse createLabelsResponse = createItemLabels(createLabelsRequest);
                if (createLabelsResponse.hasErrors()) {
                    context.addErrors(createLabelsResponse.getErrors());
                    break;
                } else {
                    ItemType itemType = catalogService.getItemTypeBySkuCode(wsItemLabel.getItemSkuCode());
                    ItemLabelDTO itemLabelDTO = new ItemLabelDTO(itemType);
                    itemLabelDTO.setItemCodeList(createLabelsResponse.getItemCodes());
                    response.getItemLabelDTOList().add(itemLabelDTO);
                }
            }
        }
        response.addErrors(context.getErrors());
        response.setSuccessful(!context.hasErrors());
        return response;
    }

    @Override
    @Transactional
    public boolean markInventorySnapshotDirty(Integer itemTypeId) {
        return inventoryDao.markInventorySnapshotDirty(itemTypeId);
    }

    @Override
    @Transactional
    public List<ItemTypeDTO> getItemTypesToBeReassessed() {
        return inventoryDao.getItemTypesToBeReassessed();
    }

    @Override
    @Transactional
    public boolean resetReassessVersionIfLatest(Integer itemTypeId, int reassessVersion) {
        return inventoryDao.resetReassessVersionIfLatest(itemTypeId, reassessVersion);
    }

    @Override
    @Transactional
    public List<Item> getItemsForAutoAging(String skuCode, int idToProcessFrom, int batchSize, int allowedShelfLife) {
        return inventoryDao.getItemsForAutoAging(skuCode, idToProcessFrom, batchSize, allowedShelfLife);

    }

    /**
     * <p>
     * Marks item expired. Item's <b>status</b> must be either GOOD_INVENTORY or BAD_INVENTORY. If item's status was
     * GOOD_INVENTORY, then corresponding changes are made in item type inventory of which the item was a part of. In
     * addition, if item has been blocked for picking, then its inventory must first be ublocked, and only then it can
     * be marked expired.<br>
     * No such changes in item type inventory are required if the item status was already BAD_INVENTORY, cause then the
     * only change is in item's rejection reason. (No change in item type inventory.)
     *
     * @param item item which is to be marked expired
     * @param rejectionReason reason for marking it expired
     */
    @Override
    @Locks({
            @Lock(ns = Namespace.ITEM, key = "#{#args[0].code}", level = Level.FACILITY),
            @Lock(ns = Namespace.SHELF, key = "#{T(java.lang.String).valueOf(#args[0].shelf?.code)}", level = Level.FACILITY) })
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @LogActivity
    public void markExpiredItemAsBadInventory(Item item, String rejectionReason) {
        item = inventoryDao.getItemByCode(item.getCode());
        String itemCode = item.getCode();
        if (item.getShelf() == null) {
            LOG.info("Item {} auto ageing skipped as it doesn't have any shelf", item.getCode());
            if (ActivityContext.current().isEnable()) {
                ActivityUtils.appendActivity(itemCode, ActivityEntityEnum.ITEM.getName(), null,
                        Collections.singletonList("Item {" + ActivityEntityEnum.ITEM + ":" + itemCode + "} auto ageing skipped, Reason : item doesn't have any shelf allocated"),
                        Item.ActivityType.BAD_INVENTORY.name());

            }
            return;
        }
        if (item.isGoodInventoryByStatus() || (item.isBadInventoryByStatus() && item.isRejectionReasonAboutToExpire())) {
            if (item.isGoodInventoryByStatus()) {
                try {
                    LOG.info("[Auto Ageing] - Removing good and adding bad inventory against item {}", itemCode);
                    ItemTypeInventory itemTypeInventory;
                    itemTypeInventory = removeInventoryWithAgeing(item.getItemType().getSkuCode(), item.getInventoryType(), item.getAgeingStartDate(), item.getShelf());
                    addInventory(item.getItemType().getSkuCode(), ItemTypeInventory.Type.BAD_INVENTORY, item.getAgeingStartDate(), itemTypeInventory.getShelf(), 1);
                    LOG.info("[Auto Ageing] - Done removing good and adding bad inventory against item {}", itemCode);
                } catch (InventoryNotAvailableException e) {
                    LOG.info("[Auto Ageing] - Finding sale order item to unblock against item {}", itemCode);
                    SaleOrderItem saleOrderItem = saleOrderService.getSaleOrderItemForDeallocation(item);
                    if (saleOrderItem != null) {
                        Integer itemTypeInventoryId = saleOrderItem.getItemTypeInventory().getId();
                        UnblockSaleOrderItemsInventoryRequest unblockSaleOrderItemsInventoryRequest = new UnblockSaleOrderItemsInventoryRequest();
                        unblockSaleOrderItemsInventoryRequest.setSaleOrderCode(saleOrderItem.getSaleOrder().getCode());
                        // Don't use singleton list here, as it creates an immutable list. unblockSaleOrderItemsInventory requires mutable lists
                        List<String> saleOrderItemCodes = new ArrayList<>();
                        saleOrderItemCodes.add(saleOrderItem.getCode());
                        unblockSaleOrderItemsInventoryRequest.setSaleOrderItemCodes(saleOrderItemCodes);
                        UnblockSaleOrderItemsInventoryResponse unblockSaleOrderItemsInventoryResponse = saleOrderService.unblockSaleOrderItemsInventory(
                                unblockSaleOrderItemsInventoryRequest);
                        if (!unblockSaleOrderItemsInventoryResponse.hasErrors()) {
                            try {
                                LOG.info("[Auto Ageing] - Successfully unblocked sale order item against item {}", itemCode);
                                ItemTypeInventory itemTypeInventory = removeInventoryByItemTypeInventoryId(itemTypeInventoryId);
                                addInventory(item.getItemType().getSkuCode(), ItemTypeInventory.Type.BAD_INVENTORY, item.getAgeingStartDate(), itemTypeInventory.getShelf(), 1);
                            } catch (InventoryNotAvailableException | ShelfBlockedForCycleCountException e1) {
                                LOG.info("Error marking expired item as bad inventory : {}", e1.getMessage());
                            }
                        }
                    } else {
                        LOG.info("Could not mark item {} as expired as no sale order item found to unblock", itemCode);
                        return;
                    }
                } catch (ShelfBlockedForCycleCountException e) {
                    LOG.info("Error marking expired item as bad inventory : {}", e.getMessage());
                    return;
                }
                item.setInventoryType(ItemTypeInventory.Type.BAD_INVENTORY);
                item.setStatusCode(Item.StatusCode.BAD_INVENTORY.name());
                item.setBadInventoryMarkedTime(DateUtils.getCurrentTime());
            }
            item.setRejectionReason(rejectionReason);
            updateItem(item);
            LOG.info("[Auto Ageing] - Successfully marked bad inventory for item {}", itemCode);
            if (ActivityContext.current().isEnable()) {
                ActivityUtils.appendActivity(item.getCode(), ActivityEntityEnum.ITEM.getName(), null,
                        Collections.singletonList(
                                "Item {" + ActivityEntityEnum.ITEM.name() + ":" + item.getCode() + "} marked BAD_INVENTORY, Rejection Reason: " + rejectionReason),
                        Item.ActivityType.BAD_INVENTORY.name());
            }
        }
    }

    /**
     * <p>
     * Removes 1 quantity of itemTypeInventory identified by unique itemTypeInventoryId
     *
     * @see #removeInventoryWithAgeing(String, Type, Date, Shelf)
     * @throws ShelfBlockedForCycleCountException
     * @throws InventoryNotAvailableException
     */
    @Transactional
    private ItemTypeInventory removeInventoryByItemTypeInventoryId(Integer itemTypeInventoryId) throws InventoryNotAvailableException, ShelfBlockedForCycleCountException {
        ItemTypeInventory itemTypeInventory = getItemTypeInventoryById(itemTypeInventoryId);
        if (itemTypeInventory == null || itemTypeInventory.getQuantity() == 0) {
            throw new InventoryNotAvailableException("Inventory not available at Shelf");
        } else if (itemTypeInventory.getShelf().isBlockedForCycleCount()) {
            throw new ShelfBlockedForCycleCountException("Shelf is blocked for cycle count");
        } else {
            itemTypeInventory.setQuantity(itemTypeInventory.getQuantity() - 1);
            return inventoryDao.updateItemTypeInventory(itemTypeInventory);
        }
    }

    /**
     * <p>
     * Gets item type inventory as uniquely identified by combination of
     * {@code skuCode, inventoryType, ageingStartDate, shelf}.
     *
     * @return
     * @throws ShelfBlockedForCycleCountException if shelf was in cycle count.
     */
    private ItemTypeInventory getItemTypeInventoryForUpdate(String skuCode, Type inventoryType, Shelf shelf, Date ageingStartDate) throws ShelfBlockedForCycleCountException {
        if (inventoryType.equals(Type.GOOD_INVENTORY) && shelf.isBlockedForCycleCount()) {
            throw new ShelfBlockedForCycleCountException("Shelf is blocked for Cycle Count");
        }
        return getItemTypeInventory(skuCode, inventoryType, shelf, ageingStartDate);

    }

    private ItemTypeInventory getItemTypeInventoryForUpdate(String skuCode, Type inventoryType, Shelf shelf) throws ShelfBlockedForCycleCountException {
        if (inventoryType.equals(Type.GOOD_INVENTORY) && shelf.isBlockedForCycleCount()) {
            throw new ShelfBlockedForCycleCountException("Shelf is blocked for Cycle Count");
        }
        return getItemTypeInventory(skuCode, inventoryType, shelf, true);
    }

    @Override
    @Transactional
    public ItemTypeInventory getItemTypeInventory(String skuCode, Type inventoryType, Shelf shelf, Date ageingStartDate) {
        return inventoryDao.getInventory(skuCode, inventoryType, shelf, ageingStartDate);
    }

    @Override
    public boolean isItemPresentWithDifferentAgeingOnShelf(String skuCode, Date ageingStartDate, Type type, int shelfId) {
        return inventoryDao.isItemPresentWithDifferentAgeingOnShelf(skuCode, ageingStartDate, type, shelfId);
    }

    /**
     * @param request
     * @return
     */
    @Override
    public UpdateItemResponse updateItemForFacility(UpdateItemRequest request) {
        UpdateItemResponse response = new UpdateItemResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            Item item = getItemByCodeAndFacility(request.getItemCode(), request.getFacilityCode());
            if (item != null) {
                if (!StringUtils.equalsAny(item.getStatusCode(), Item.StatusCode.GOOD_INVENTORY.name(), Item.StatusCode.BAD_INVENTORY.name(), Item.StatusCode.ALLOCATED.name())) {
                    context.addError(WsResponseCode.INVALID_ITEM_CODE, "Item should have status as either GOOD_INVENTORY or BAD_INVENTORY for updating manufacturing date");
                } else {
                    InflowReceiptItem inflowReceiptItem = item.getInflowReceiptItem();
                    if (inflowReceiptItem != null) {
                        updateItemForFacilityInternal(request);
                    } else {
                        context.addError(WsResponseCode.INVALID_ITEM_CODE, "This item doesn't belong to any Inflow Receipt");
                    }
                }
            } else {
                context.addError(WsResponseCode.INVALID_ITEM_CODE, "No such item in given facility");
            }
        }
        response.setErrors(context.getErrors());
        response.setSuccessful(!context.hasErrors());
        return response;
    }

    @Transactional(readOnly = true)
    @Override
    public Long getInventoryCountByShelfId(Integer shelfId) {
        return inventoryDao.getInventoryCountByShelfId(shelfId);
    }

    @Override
    @Transactional(readOnly = true)
    public Long getNumberOfItemsWithoutManufacturingDateForCategory(String categoryCode) {
        return inventoryDao.getNumberOfItemsWithoutManufacturingDateForCategory(categoryCode);
    }

    @Transactional
    @Locks({ @Lock(ns = Namespace.ITEM, key = "#{#args[0].itemCode}") })
    @LogActivity
    private void updateItemForFacilityInternal(UpdateItemRequest request) {
        Item item = getItemByCodeAndFacility(request.getItemCode(), request.getFacilityCode());
        item.setManufacturingDate(request.getManufacturingDate());
        if (request.getManufacturingDate() != null) {
            item.setAgeingStartDate(request.getManufacturingDate());
        }
        updateItemForFacility(item);
        if (ActivityContext.current().isEnable()) {
            ActivityUtils.appendActivity(item.getCode(), ActivityEntityEnum.ITEM.getName(), null, Collections.singletonList(
                    "Item {" + ActivityEntityEnum.ITEM.name() + ":" + item.getCode() + "} manufacturing date changed to " + request.getManufacturingDate() + " by GRN item update"),
                    Item.ActivityType.UPDATED.name());
        }
    }

    @Transactional
    private Item updateItemForFacility(Item item) {
        return inventoryDao.updateItemForFacility(item);
    }

    @Transactional(readOnly = true)
    private Item getItemByCodeAndFacility(String itemCode, String facilityCode) {
        return inventoryDao.getItemByCodeAndFacility(itemCode, facilityCode);
    }

    @Transactional
    private ItemTypeInventory blockInventory(Item item, Shelf shelf) throws ShelfBlockedForCycleCountException, InventoryNotAvailableException {
        try {
            return removeInventoryWithAgeing(item.getItemType().getSkuCode(), item.getInventoryType(), item.getAgeingStartDate(), shelf);
        } catch (InventoryNotAvailableException e) {
            return removeInventoryWithoutAgeing(item.getItemType().getSkuCode(), item.getInventoryType(), shelf);
        }
    }

    private ItemTypeInventory removeInventoryWithoutAgeing(String skuCode, Type inventoryType, Shelf shelf)
            throws InventoryNotAvailableException, ShelfBlockedForCycleCountException {
        ItemTypeInventory itemTypeInventory = getItemTypeInventoryForUpdate(skuCode, inventoryType, shelf);
        return doRemoveInventory(skuCode, inventoryType, shelf, itemTypeInventory);
    }

    /**
     * <p>
     * Removes 1 quantity of inventory as identified by combination of
     * {@code skuCode, inventoryType, ageingStartDate, shelf}. Throws 2 exceptions:
     *
     * @throws ShelfBlockedForCycleCountException
     * @throws InventoryNotAvailableException
     */
    @Transactional
    private ItemTypeInventory removeInventoryWithAgeing(String skuCode, Type inventoryType, Date ageingStartDate, Shelf shelf)
            throws ShelfBlockedForCycleCountException, InventoryNotAvailableException {
        ItemTypeInventory itemTypeInventory = getItemTypeInventoryForUpdate(skuCode, inventoryType, shelf, ageingStartDate);
        return doRemoveInventory(skuCode, inventoryType, shelf, itemTypeInventory);
    }

    private ItemTypeInventory doRemoveInventory(String skuCode, Type inventoryType, Shelf shelf, ItemTypeInventory itemTypeInventory)
            throws InventoryNotAvailableException, ShelfBlockedForCycleCountException {
        if (itemTypeInventory == null || itemTypeInventory.getQuantity() == 0) {
            throw new InventoryNotAvailableException("Inventory not available at Shelf:" + shelf.getCode() + ", Type:" + inventoryType + ", Item SKU:" + skuCode);
        } else if (itemTypeInventory.getShelf().isBlockedForCycleCount()) {
            throw new ShelfBlockedForCycleCountException("Shelf is blocked for cycle count");
        } else {
            itemTypeInventory.setQuantity(itemTypeInventory.getQuantity() - 1);
            return inventoryDao.updateItemTypeInventory(itemTypeInventory);
        }
    }

    @Override
    @Transactional
    public void decrementNotFoundInventory(Item item, ValidationContext context) {
        ItemTypeInventory itemTypeInventory = null;
        try {
            itemTypeInventory = getNotFoundItemTypeInventoryForUpdate(item.getItemType().getId(), item.getInventoryType(), item.getShelf());
        } catch (ShelfBlockedForCycleCountException e) {
            context.addError(WsResponseCode.SHELF_BLOCKED_FOR_CYCLE_COUNT, "Shelf code : {} blocked for cycle count", item.getShelf().getCode());
        }
        if (!context.hasErrors()) {
            if (itemTypeInventory == null) {
                context.addError(WsResponseCode.INVALID_REQUEST, "No not-found inventory found");
            } else if (itemTypeInventory.getQuantityNotFound() <= 0) {
                context.addError(WsResponseCode.INVALID_REQUEST, "not-found inventory should be greater than 1");
            } else {
                itemTypeInventory.setQuantityNotFound(itemTypeInventory.getQuantityNotFound() - 1);
            }
        }
    }

    private ItemTypeInventory getNotFoundItemTypeInventoryForUpdate(Integer itemTypeId, Type inventoryType, Shelf shelf) throws ShelfBlockedForCycleCountException {
        if (inventoryType.equals(Type.GOOD_INVENTORY) && shelf.isBlockedForCycleCount()) {
            throw new ShelfBlockedForCycleCountException("Shelf is blocked for Cycle Count");
        }
        return inventoryDao.getNotFoundInventory(itemTypeId, inventoryType, shelf);
    }

    @LogActivity
    private List<Item> createItemLabels(String skuCode, List<String> itemCodes, Date manufacturingDate, ValidationContext context) {
        List<Item> items = new ArrayList<>();
        ItemType itemType = catalogService.getItemTypeBySkuCode(skuCode);
        for (String itemCode : itemCodes) {
            Item item = new Item();
            item.setItemType(itemType);
            item.setCode(itemCode);
            item.setStatusCode(Item.StatusCode.UNPROCESSED.name());
            item.setManufacturingDate(manufacturingDate);
            if (manufacturingDate != null) {
                item.setAgeingStartDate(manufacturingDate);
            } else {
                // It will be over written at the time of GRN
                item.setAgeingStartDate(DateUtils.getCurrentDate());
            }
            item.setFacility(UserContext.current().getFacility());
            item.setCreated(DateUtils.getCurrentTime());
            item.setUpdated(DateUtils.getCurrentTime());
            items.add(inventoryDao.addItem(item, false));
            if (ActivityContext.current().isEnable()) {
                ActivityUtils.appendActivity(item.getCode(), ActivityEntityEnum.ITEM.getName(), null,
                        Collections.singletonList("Item {" + ActivityEntityEnum.ITEM.name() + ":" + item.getCode() + "} moved to unprocessed."),
                        Item.ActivityType.UNPROCESSED.name());
            }
        }
        return items;
    }

    @Override
    @Transactional
    public List<Item> getItemsByItemTypeAndShelfAndStatus(Integer itemTypeId, Integer shelfId, String status) {
        return inventoryDao.getItemsByItemTypeAndShelfAndStatus(itemTypeId, shelfId, status);
    }

    private ItemTypeInventory getInventory(ItemType itemType, Type inventoryType, Shelf shelf) {
        if (ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).getTraceabilityLevel() == TraceabilityLevel.ITEM) {
            throw new RuntimeException("This method should not be called on ITEM traceability.");
        } else {
            return getInventory(itemType, inventoryType, shelf, Constants.ITEM_SKU_OR_NONE_TRACEABILITY_AGEING_DATE);
        }
    }

    private ItemTypeInventory getInventory(ItemType itemType, Type inventoryType, Shelf shelf, Date ageingDate) {
        return inventoryDao.getInventory(itemType, inventoryType, shelf, ageingDate);
    }

    private class AdjustmentDetails {
        ChangeType changeType;
        Integer    adjustmentQuantity;

        AdjustmentDetails(ChangeType changeType, Integer adjustmentQuantity) {
            this.changeType = changeType;
            this.adjustmentQuantity = adjustmentQuantity;
        }

        ChangeType getChangeType() {
            return changeType;
        }

        Integer getAdjustmentQuantity() {
            return adjustmentQuantity;
        }

    }
}
