package com.uniware.services.inventory.impl;

import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.utils.StringUtils;
import com.uniware.core.api.inventory.AcknowledgeStockoutInventoryRequest;
import com.uniware.core.api.inventory.AcknowledgeStockoutInventoryResponse;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.cache.FacilityCache;
import com.uniware.core.entity.Facility;
import com.uniware.core.entity.FacilityProfile;
import com.uniware.core.entity.InventoryAvailabilityChangeLedger;
import com.uniware.core.entity.ItemTypeInventory;
import com.uniware.core.utils.UserContext;
import com.uniware.dao.inventory.IInventoryAvailabilityChangeDao;
import com.uniware.services.configuration.SystemConfiguration;
import com.uniware.services.inventory.IInventoryAvailabilityChangeService;
import com.uniware.services.inventory.IInventoryService;
import com.uniware.services.pickup.AvailabilityChangeMessage;
import com.uniware.services.pickup.IStoreAvailabilityNotifyService;
import com.uniware.services.tasks.notifications.AcknowledgeInventoryLedgerTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by piyush on 1/6/16.
 */
@Service("inventoryAvailabilityChangeService")
public class InventoryAvailabilityChangeServiceImpl implements IInventoryAvailabilityChangeService {

    private static final Logger LOG = LoggerFactory.getLogger(InventoryAvailabilityChangeServiceImpl.class);

    @Autowired
    private IInventoryAvailabilityChangeDao inventoryAvailabilityChangeDao;

    @Autowired
    private IStoreAvailabilityNotifyService storeAvailabilityNotifyService;

    @Autowired
    private IInventoryService inventoryService;

    @Override
    @Transactional
    public AcknowledgeInventoryLedgerTask.UnacknowledgedInventoryLedgerStatus getUnacknowledgedInventoryLedgerStatus() {
        return inventoryAvailabilityChangeDao.getUnacknowledgedInventoryLedgerStatus();
    }

    @Override
    public AcknowledgeStockoutInventoryResponse acknowledgeInventoryLedgers(AcknowledgeStockoutInventoryRequest request) {
        AcknowledgeStockoutInventoryResponse response = new AcknowledgeStockoutInventoryResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            Facility facility = CacheManager.getInstance().getCache(FacilityCache.class).getFacilityByCode(request.getFacilityCode());
            if (facility == null) {
                context.addError(WsResponseCode.INVALID_FACILITY_CODE, "no facility found");
            } else {
                Facility currentFacility = UserContext.current().getFacility();
                try {
                    UserContext.current().setFacility(facility);
                    if (!ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).isInventoryAvailabilityLedgerAcknowledgable()
                            || pushAvailabilityChangeNotification(facility, request.getPendingInventoryLedgers())) {
                        int affectedRows = markStockoutInventoryLedgersAcknowledged(request.getPendingInventoryLedgers());
                        if (affectedRows == 0) {
                            LOG.info("unknown error while marking inventory legder acknowledged for facility : {}", facility.getCode());
                            context.addError(WsResponseCode.UNKNOWN_ERROR, "unknown error while marking inventory legder acknowledged");
                        }
                    } else {
                        LOG.info("Not Acknowledging Inventory Availability Ledger for facility : {}", facility.getCode());
                    }
                } catch (Exception e){
                    LOG.info("unknown error while acknowledging inventory legder for facility : {}", facility.getCode());
                    context.addError(WsResponseCode.UNKNOWN_ERROR,"unknown error while acknowledging inventory legder");
                } finally {
                    if(currentFacility != null){
                        UserContext.current().setFacility(currentFacility);
                    } else {
                        UserContext.current().setFacility(null);
                    }
                }
            }
        }
        if(context.hasErrors()){
            response.addErrors(context.getErrors());
        } else {
            response.setSuccessful(true);
        }
        return response;
    }

    private boolean pushAvailabilityChangeNotification(Facility facility, List<InventoryAvailabilityChangeLedger> inventoryLedgers) {
        AvailabilityChangeMessage message = prepareAvailabilityChangeMessage(facility, inventoryLedgers);
        if(message != null){
            storeAvailabilityNotifyService.publishMessage(message);
            return true;
        }
        return false;
    }

    private AvailabilityChangeMessage prepareAvailabilityChangeMessage(Facility facility, List<InventoryAvailabilityChangeLedger> inventoryLedgers) {
        FacilityProfile facilityProfile = CacheManager.getInstance().getCache(FacilityCache.class).getFacilityProfileByCode(facility.getCode());
        if(facilityProfile != null && StringUtils.isNotBlank(facilityProfile.getSellerCode())){
            AvailabilityChangeMessage message = new AvailabilityChangeMessage();
            Set<AvailabilityChangeMessage.AvailabilityDetails> supcs = new HashSet<>();
            for(InventoryAvailabilityChangeLedger inventoryChangeLedger : inventoryLedgers){
                Integer facilityDispatchSLA = null;
                Integer facilityDeliverySLA = facilityProfile.getDeliverySLA();
                for(ItemTypeInventory itemTypeInventory : inventoryService.getItemTypeInventoryByItemTypeId(inventoryChangeLedger.getItemTypeId())){
                    if (itemTypeInventory.getSla() != null && itemTypeInventory.getType().name().equals(ItemTypeInventory.Type.GOOD_INVENTORY.name())) {
                        facilityDispatchSLA =  itemTypeInventory.getSla();
                    } else if (itemTypeInventory.getSla() != null && facilityDispatchSLA == null
                            && itemTypeInventory.getType().name().equals(ItemTypeInventory.Type.VIRTUAL_INVENTORY.name())) {
                        facilityDispatchSLA =  itemTypeInventory.getSla();
                    }
                }
                if(facilityDispatchSLA == null){
                    facilityDispatchSLA = facilityProfile.getDispatchSLA();
                }
                AvailabilityChangeMessage.AvailabilityDetails supcDetails = new AvailabilityChangeMessage.AvailabilityDetails();
                supcDetails.setSupc(inventoryChangeLedger.getSkuCode());
                supcDetails.setAvailable(inventoryChangeLedger.isOutOfStock());
                supcDetails.setDispatchSla(facilityDispatchSLA);
                supcDetails.setDeliverySla(facilityDeliverySLA);
                supcs.add(supcDetails);
            }
            message.setStoreCode(facility.getCode());
            message.setSellerCode(facilityProfile.getSellerCode());
            message.setSupcs(supcs);
            return message;
        }
        return null;
    }

    @Transactional
    private int markStockoutInventoryLedgersAcknowledged(List<InventoryAvailabilityChangeLedger> processedInventoryLedgers) {
        List<Integer> processedInventoryLedgerIds = new ArrayList<>(processedInventoryLedgers.size());
        for(InventoryAvailabilityChangeLedger stockoutInventoryLedger : processedInventoryLedgers){
            processedInventoryLedgerIds.add(stockoutInventoryLedger.getId());
        }
        return inventoryAvailabilityChangeDao.markStockoutInventoryLedgersAcknowledged(processedInventoryLedgerIds);
    }

    @Override
    @Transactional
    public List<InventoryAvailabilityChangeLedger> getUnacknowledgedInventoryLedgers(int batchSize, int maxId) {
        return inventoryAvailabilityChangeDao.getUnacknowledgedInventoryLedgers(batchSize,maxId);
    }

    @Override
    @Transactional
    public List<InventoryAvailabilityChangeLedger> getUnacknowledgedInventoryLedgersByFacilityId(int batchSize, int maxId, int facilityId) {
        return inventoryAvailabilityChangeDao.getUnacknowledgedInventoryLedgersByFacilityId(batchSize, maxId, facilityId);
    }
}
