/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jun 20, 2012
 *  @author singla
 */
package com.uniware.services.dual.company;

import com.uniware.core.api.dual.company.AutoCompletePutawayRequest;
import com.uniware.core.api.dual.company.AutoCompletePutawayResponse;
import com.uniware.core.api.dual.company.ReceiveReturnFromRetailRequest;
import com.uniware.core.api.dual.company.ReceiveReturnFromRetailResponse;
import com.uniware.core.api.dual.company.ReceiveReversePickupFromRetailRequest;
import com.uniware.core.api.dual.company.ReceiveReversePickupFromRetailResponse;
import com.uniware.core.api.dual.company.ReceiveShippingPackageFromWholesaleRequest;
import com.uniware.core.api.dual.company.ReceiveShippingPackageFromWholesaleResponse;

/**
 * @author singla
 */
public interface IDualCompanyService {

    /**
     * @param request
     * @return
     */
    AutoCompletePutawayResponse autoCompletePutaway(AutoCompletePutawayRequest request);

    ReceiveShippingPackageFromWholesaleResponse receiveShippingPackageFromWholesale(ReceiveShippingPackageFromWholesaleRequest request);

    ReceiveReturnFromRetailResponse receiveReturnFromRetail(ReceiveReturnFromRetailRequest request);

    ReceiveReversePickupFromRetailResponse receiveReversePickupFromRetail(ReceiveReversePickupFromRetailRequest request);

}
