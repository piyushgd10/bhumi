/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 *  @version     1.0, Jun 20, 2012
 *  @author singla
 */
package com.uniware.services.dual.company.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.api.validation.WsError;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.entity.User;
import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.NumberUtils;
import com.unifier.core.utils.StringUtils;
import com.unifier.services.aspect.RollbackOnFailure;
import com.uniware.core.api.currency.GetCurrencyConversionRateRequest;
import com.uniware.core.api.dual.company.AutoCompletePutawayRequest;
import com.uniware.core.api.dual.company.AutoCompletePutawayResponse;
import com.uniware.core.api.dual.company.ReceiveReturnFromRetailRequest;
import com.uniware.core.api.dual.company.ReceiveReturnFromRetailResponse;
import com.uniware.core.api.dual.company.ReceiveReversePickupFromRetailRequest;
import com.uniware.core.api.dual.company.ReceiveReversePickupFromRetailRequest.WsRetailReversePickupItem;
import com.uniware.core.api.dual.company.ReceiveReversePickupFromRetailResponse;
import com.uniware.core.api.dual.company.ReceiveShippingPackageFromWholesaleRequest;
import com.uniware.core.api.dual.company.ReceiveShippingPackageFromWholesaleRequest.WsWholesaleSaleOrderItem;
import com.uniware.core.api.dual.company.ReceiveShippingPackageFromWholesaleResponse;
import com.uniware.core.api.dual.company.WsInflowItemType;
import com.uniware.core.api.inflow.AddItemToInflowReceiptRequest;
import com.uniware.core.api.inflow.AddItemToInflowReceiptResponse;
import com.uniware.core.api.inflow.CompleteInflowReceiptItemsQCRequest;
import com.uniware.core.api.inflow.CompleteInflowReceiptItemsQCResponse;
import com.uniware.core.api.inflow.CreateInflowReceiptRequest;
import com.uniware.core.api.inflow.CreateInflowReceiptResponse;
import com.uniware.core.api.inflow.GetInflowReceiptRequest;
import com.uniware.core.api.inflow.InflowReceiptDTO;
import com.uniware.core.api.inflow.InflowReceiptItemDTO;
import com.uniware.core.api.inflow.SendInflowReceiptForQCRequest;
import com.uniware.core.api.inflow.SendInflowReceiptForQCResponse;
import com.uniware.core.api.inflow.WsGRN;
import com.uniware.core.api.inventory.AddOrEditItemLabelsRequest;
import com.uniware.core.api.inventory.AddOrEditItemLabelsResponse;
import com.uniware.core.api.invoice.CreateShippingPackageInvoiceRequest;
import com.uniware.core.api.invoice.CreateShippingPackageInvoiceResponse;
import com.uniware.core.api.purchase.WsPurchaseOrderItem;
import com.uniware.core.api.putaway.AddInflowReceiptItemsToPutawayRequest;
import com.uniware.core.api.putaway.AddInflowReceiptItemsToPutawayResponse;
import com.uniware.core.api.putaway.CompletePutawayRequest;
import com.uniware.core.api.putaway.CompletePutawayResponse;
import com.uniware.core.api.putaway.CreatePutawayListRequest;
import com.uniware.core.api.putaway.CreatePutawayListResponse;
import com.uniware.core.api.putaway.CreatePutawayRequest;
import com.uniware.core.api.putaway.CreatePutawayResponse;
import com.uniware.core.api.putaway.DiscardPutawayRequest;
import com.uniware.core.api.putaway.EditPutawayItemRequest;
import com.uniware.core.api.returns.AddDirectReturnedSaleOrderItemsToPutawayRequest;
import com.uniware.core.api.returns.AddShippingPackageToReturnManifestRequest;
import com.uniware.core.api.returns.AddShippingPackageToReturnManifestResponse;
import com.uniware.core.api.returns.CreateReturnManifestRequest;
import com.uniware.core.api.reversepickup.AddReversePickupSaleOrderItemsToPutawayRequest.WsSaleOrderItem;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.cache.FacilityCache;
import com.uniware.core.entity.InflowReceipt;
import com.uniware.core.entity.InflowReceiptItem;
import com.uniware.core.entity.Item;
import com.uniware.core.entity.ItemType;
import com.uniware.core.entity.ItemTypeInventory;
import com.uniware.core.entity.PurchaseOrder;
import com.uniware.core.entity.PurchaseOrderItem;
import com.uniware.core.entity.Putaway;
import com.uniware.core.entity.PutawayItem;
import com.uniware.core.entity.ReturnManifest;
import com.uniware.core.entity.SaleOrder;
import com.uniware.core.entity.SaleOrderItem;
import com.uniware.core.entity.Shelf;
import com.uniware.core.entity.ShippingPackage;
import com.uniware.core.entity.Vendor;
import com.uniware.core.locking.Namespace;
import com.uniware.core.locking.annotation.Lock;
import com.uniware.core.locking.annotation.Locks;
import com.uniware.core.utils.UserContext;
import com.uniware.dao.inflow.IInflowDao;
import com.uniware.dao.inventory.IInventoryDao;
import com.uniware.dao.purchase.IPurchaseDao;
import com.uniware.services.configuration.TenantSystemConfiguration;
import com.uniware.services.currency.ICurrencyService;
import com.uniware.services.dual.company.IDualCompanyService;
import com.uniware.services.inflow.IInflowService;
import com.uniware.services.inventory.IInventoryService;
import com.uniware.services.purchase.IPurchaseService;
import com.uniware.services.putaway.IPutawayService;
import com.uniware.services.returns.IReturnsService;
import com.uniware.services.saleorder.ISaleOrderService;
import com.uniware.services.shipping.IShippingInvoiceService;
import com.uniware.services.shipping.IShippingService;
import com.uniware.services.vendor.IVendorService;

/**
 * @author singla
 */
@Service("dualCompanyService")
public class DualCompanyServiceImpl implements IDualCompanyService {

    private static final String WHOLESALE_VENDOR_CODE = "WHOLESALE";

    @Autowired
    private IInflowService      inflowService;

    @Autowired
    private IPurchaseService    purchaseService;

    @Autowired
    private IInventoryService   inventoryService;

    @Autowired
    private IPutawayService     putawayService;

    @Autowired
    private ISaleOrderService   saleOrderService;

    @Autowired
    private IShippingService    shippingService;

    @Autowired
    private IVendorService      vendorService;

    @Autowired
    private IPurchaseDao        purchaseDao;

    @Autowired
    private IInflowDao          inflowDao;

    @Autowired
    private IInventoryDao       inventoryDao;

    @Autowired
    private IReturnsService     returnsService;

    @Autowired
    private ICurrencyService    currencyService;

    @Autowired
    private IShippingInvoiceService iShippingInvoiceService;

    @Override
    @Transactional
    @RollbackOnFailure
    public AutoCompletePutawayResponse autoCompletePutaway(AutoCompletePutawayRequest request) {
        AutoCompletePutawayResponse response = new AutoCompletePutawayResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            PurchaseOrder purchaseOrder = purchaseDao.getPurchaseOrderByCode(request.getPurchaseOrderCode(), true);
            if (purchaseOrder == null) {
                context.addError(WsResponseCode.INVALID_PURCHASE_ORDER_CODE);
            } else if (StringUtils.isBlank(request.getInflowReceiptCode()) && PurchaseOrder.StatusCode.COMPLETE.name().equals(purchaseOrder.getStatusCode())) {
                response.setSuccessful(true);
            } else {
                String inflowReceiptCode = request.getInflowReceiptCode();
                if (StringUtils.isBlank(inflowReceiptCode)) {
                    CreateInflowReceiptResponse cirResponse = createInflowReceipt(request, context, purchaseOrder);
                    for (WsInflowItemType inflowItemType : request.getInflowItemTypes()) {
                        if (!context.hasErrors()) {
                            addItemLabels(inflowItemType, context);
                            for (String itemCode : inflowItemType.getItemCodes()) {
                                if (!context.hasErrors()) {
                                    addItemToInflowReceipt(itemCode, cirResponse.getInflowReceiptCode(), context);
                                }
                            }
                        }
                    }
                    if (!context.hasErrors()) {
                        sendInflowReceiptForQC(cirResponse.getInflowReceiptCode(), context);
                        inflowReceiptCode = cirResponse.getInflowReceiptCode();
                    }
                }
                if (!context.hasErrors()) {
                    InflowReceiptDTO inflowReceipt = inflowService.getInflowReceipt(new GetInflowReceiptRequest(inflowReceiptCode)).getInflowReceipt();
                    if (InflowReceipt.StatusCode.QC_PENDING.name().equals(inflowReceipt.getStatusCode())) {
                        List<Integer> inflowReceiptItemIds = new ArrayList<Integer>(inflowReceipt.getInflowReceiptItems().size());
                        for (InflowReceiptItemDTO inflowReceiptItem : inflowReceipt.getInflowReceiptItems()) {
                            inflowReceiptItemIds.add(inflowReceiptItem.getId());
                        }
                        completeInflowReceiptQC(inflowReceiptCode, inflowReceiptItemIds, context);

                        if (!context.hasErrors()) {
                            if (!request.isOnlyPrepareGRN()) {
                                CreatePutawayResponse cpResponse = createPutaway(context);
                                if (!context.hasErrors()) {
                                    addInflowReceiptToPutaway(inflowReceipt.getCode(), inflowReceiptItemIds, cpResponse.getPutawayDTO().getCode(), context);
                                    if (!context.hasErrors()) {
                                        createPutawayList(cpResponse.getPutawayDTO().getCode(), context);
                                    }
                                    if (!context.hasErrors()) {
                                        completePutaway(cpResponse.getPutawayDTO().getCode(), context);
                                        if (!context.hasErrors()) {
                                            response.setSuccessful(true);
                                        }
                                    }
                                }
                            } else {
                                response.setSuccessful(true);
                            }
                        }
                    } else {
                        context.addError(WsResponseCode.INVALID_INFLOW_RECEIPT_STATE, "Invalid inflow receipt state");
                    }
                }
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    /**
     * @param putawayCode
     * @param context
     * @return
     */
    private CreatePutawayListResponse createPutawayList(String putawayCode, ValidationContext context) {
        CreatePutawayListRequest request = new CreatePutawayListRequest();
        request.setPutawayCode(putawayCode);
        request.setUserId(ConfigurationManager.getInstance().getConfiguration(TenantSystemConfiguration.class).getSystemUser().getId());
        CreatePutawayListResponse response = putawayService.createPutawayList(request);
        if (response.hasErrors()) {
            context.getErrors().addAll(response.getErrors());
        }
        return response;
    }

    /**
     * @param inflowReceiptCode
     * @param inflowReceiptItemIds
     * @param putawayCode
     * @param context
     * @return
     */
    private AddInflowReceiptItemsToPutawayResponse addInflowReceiptToPutaway(String inflowReceiptCode, List<Integer> inflowReceiptItemIds, String putawayCode,
            ValidationContext context) {
        AddInflowReceiptItemsToPutawayRequest request = new AddInflowReceiptItemsToPutawayRequest();
        request.setUserId(ConfigurationManager.getInstance().getConfiguration(TenantSystemConfiguration.class).getSystemUser().getId());
        request.setPutawayCode(putawayCode);
        request.setInflowReceiptCode(inflowReceiptCode);
        request.setInflowReceiptItemIds(inflowReceiptItemIds);
        AddInflowReceiptItemsToPutawayResponse response = putawayService.addInflowReceiptItemsToPutaway(request);
        if (response.hasErrors()) {
            context.getErrors().addAll(response.getErrors());
        }
        return response;
    }

    private CompletePutawayResponse completePutaway(String putawayCode, ValidationContext context) {
        CompletePutawayRequest request = new CompletePutawayRequest();
        request.setPutawayCode(putawayCode);
        CompletePutawayResponse response = putawayService.completePutaway(request);
        if (response.hasErrors()) {
            context.getErrors().addAll(response.getErrors());
        }
        return response;
    }

    private CreatePutawayResponse createPutaway(ValidationContext context) {
        CreatePutawayRequest request = new CreatePutawayRequest();
        request.setUserId(ConfigurationManager.getInstance().getConfiguration(TenantSystemConfiguration.class).getSystemUser().getId());
        request.setType(Putaway.Type.PUTAWAY_GRN_ITEM.name());
        CreatePutawayResponse response = putawayService.createPutaway(request);
        if (response.hasErrors()) {
            context.getErrors().addAll(response.getErrors());
        }
        return response;
    }

    /**
     * @param inflowReceiptCode
     * @param context
     * @return
     */
    private SendInflowReceiptForQCResponse sendInflowReceiptForQC(String inflowReceiptCode, ValidationContext context) {
        SendInflowReceiptForQCRequest sirfQCRequest = new SendInflowReceiptForQCRequest();
        sirfQCRequest.setInflowReceiptCode(inflowReceiptCode);
        SendInflowReceiptForQCResponse response = inflowService.sendInflowReceiptForQC(sirfQCRequest);
        if (response.hasErrors()) {
            context.getErrors().addAll(response.getErrors());
        }
        return response;
    }

    /**
     * @param itemCode
     * @param inflowReceiptCode
     * @param context
     * @return
     */

    private AddItemToInflowReceiptResponse addItemToInflowReceipt(String itemCode, String inflowReceiptCode, ValidationContext context) {
        AddItemToInflowReceiptRequest request = new AddItemToInflowReceiptRequest();
        request.setInflowReceiptCode(inflowReceiptCode);
        request.setItemCode(itemCode);
        AddItemToInflowReceiptResponse response = inflowService.addItemToInflowReceipt(request);
        if (response.hasErrors()) {
            context.getErrors().addAll(response.getErrors());
        }
        return response;
    }

    private AddOrEditItemLabelsResponse addItemLabels(WsInflowItemType inflowItemType, ValidationContext context) {
        AddOrEditItemLabelsRequest request = new AddOrEditItemLabelsRequest();
        request.setItemCodes(inflowItemType.getItemCodes());
        request.setItemSkuCode(inflowItemType.getItemSkuCode());
        AddOrEditItemLabelsResponse response = inventoryService.addOrEditItemLabels(request);
        if (response.hasErrors()) {
            context.getErrors().addAll(response.getErrors());
        }
        return response;
    }

    /**
     * @param inflowReceiptCode
     * @param inflowReceiptItemIds
     * @param context
     * @return
     */
    private CompleteInflowReceiptItemsQCResponse completeInflowReceiptQC(String inflowReceiptCode, List<Integer> inflowReceiptItemIds, ValidationContext context) {
        CompleteInflowReceiptItemsQCRequest request = new CompleteInflowReceiptItemsQCRequest();
        request.setInflowReceiptItemIds(inflowReceiptItemIds);
        request.setInflowReceiptCode(inflowReceiptCode);
        CompleteInflowReceiptItemsQCResponse response = inflowService.completeInflowReceiptItemsQC(request);
        if (response.hasErrors()) {
            context.getErrors().addAll(response.getErrors());
        }
        return response;
    }

    /**
     * @param acpRequest
     * @param context
     * @param purchaseOrder
     * @return
     */
    private CreateInflowReceiptResponse createInflowReceipt(AutoCompletePutawayRequest acpRequest, ValidationContext context, PurchaseOrder purchaseOrder) {
        CreateInflowReceiptRequest request = new CreateInflowReceiptRequest();
        request.setPurchaseOrderCode(purchaseOrder.getCode());
        WsGRN wsGRN = new WsGRN();
        request.setWsGRN(wsGRN);
        wsGRN.setUserId(ConfigurationManager.getInstance().getConfiguration(TenantSystemConfiguration.class).getSystemUser().getId());
        wsGRN.setVendorInvoiceNumber(acpRequest.getVendorInvoiceNumber());
        wsGRN.setVendorInvoiceDate(acpRequest.getVendorInvoiceDate());
        CreateInflowReceiptResponse response = inflowService.createInflowReceipt(request);
        if (response.hasErrors()) {
            context.getErrors().addAll(response.getErrors());
        }
        return response;
    }

    @Override
    @Locks({ @Lock(ns = Namespace.SALE_ORDER, key = "#{#args[0].saleOrderCode}") })
    @Transactional
    @RollbackOnFailure
    public ReceiveShippingPackageFromWholesaleResponse receiveShippingPackageFromWholesale(ReceiveShippingPackageFromWholesaleRequest request) {
        ReceiveShippingPackageFromWholesaleResponse response = new ReceiveShippingPackageFromWholesaleResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            SaleOrder saleOrder = saleOrderService.getSaleOrderForUpdate(request.getSaleOrderCode());
            if (saleOrder == null) {
                context.addError(WsResponseCode.INVALID_SALE_ORDER_CODE);
            } else {
                Map<String, WsWholesaleSaleOrderItem> saleOrderItemCodes = getSaleOrderItemCodes(request.getSaleOrderItems());
                Map<String, WsWholesaleSaleOrderItem> saleOrderItemCodeToSaleOrderItems = getSaleOrderItemCodes(request.getSaleOrderItems());
                List<SaleOrderItem> itemsToPackage = new ArrayList<SaleOrderItem>();
                Map<String, Item> saleOrderItemToItems = new HashMap<String, Item>();
                Map<String, WsWholesaleSaleOrderItem> itemsToCreate = new HashMap<String, WsWholesaleSaleOrderItem>();
                for (SaleOrderItem saleOrderItem : saleOrder.getSaleOrderItems()) {
                    WsWholesaleSaleOrderItem wsSaleOrderItem = saleOrderItemCodes.remove(saleOrderItem.getCode());
                    if (wsSaleOrderItem != null) {
                        if (!SaleOrderItem.StatusCode.UNFULFILLABLE.name().equals(saleOrderItem.getStatusCode())) {
                            context.addError(WsResponseCode.INVALID_SALE_ORDER_ITEM_STATE, "SaleOrderItem[" + saleOrderItem.getCode() + "] not in UNFULFILLABLE state");
                        } else {
                            Item item = inventoryService.getItemByCode(wsSaleOrderItem.getItemCode());
                            if (item != null && !Item.StatusCode.LIQUIDATED.name().equals(item.getStatusCode())) {
                                context.addError(WsResponseCode.DUPLICATE_ITEM_CODE);
                            } else {
                                itemsToPackage.add(saleOrderItem);
                                if (item == null) {
                                    itemsToCreate.put(saleOrderItem.getCode(), wsSaleOrderItem);
                                } else {
                                    saleOrderItemToItems.put(saleOrderItem.getCode(), item);
                                }
                            }
                        }
                    }
                }
                if (saleOrderItemCodes.size() > 0) {
                    context.addError(WsResponseCode.INVALID_SALE_ORDER_ITEM_CODE, "Invalid SaleOrderItem codes :[" + StringUtils.join('|', saleOrderItemCodes.keySet()) + "]");
                }
                if (!context.hasErrors()) {
                    for (SaleOrderItem saleOrderItem : itemsToPackage) {
                        Item item = saleOrderItemToItems.get(saleOrderItem.getCode());
                        if (item == null) {
                            item = addPurchaseDetails(saleOrderItem.getItemType(), item, itemsToCreate.get(saleOrderItem.getCode()), response);
                        }
                        if (!context.hasErrors()) {
                            saleOrderItem.setFacility(UserContext.current().getFacility());
                            saleOrderItem.setStatusCode(SaleOrderItem.StatusCode.FULFILLABLE.name());
                            item.setStatusCode(Item.StatusCode.ALLOCATED.name());
                            item.setItemDetails(saleOrderItemCodeToSaleOrderItems.get(saleOrderItem.getCode()).getItemCustomFieldValues());
                            saleOrderItem.setItemDetails(item.getItemDetails());
                            saleOrderItem.setItem(item);
                        }
                    }
                    if (!context.hasErrors()) {
                        ShippingPackage shippingPackage = shippingService.createShippingPackage(itemsToPackage, false, request.getShippingPackageCode());
                        shippingPackage.setStatusCode(ShippingPackage.StatusCode.PICKED.name());

                        CreateShippingPackageInvoiceRequest createShippingPackageInvoiceRequest = new CreateShippingPackageInvoiceRequest();
                        createShippingPackageInvoiceRequest.setUserId(ConfigurationManager.getInstance().getConfiguration(TenantSystemConfiguration.class).getSystemUser().getId());
                        createShippingPackageInvoiceRequest.setShippingPackageCode(shippingPackage.getCode());
                        CreateShippingPackageInvoiceResponse createShippingPackageInvoiceResponse = iShippingInvoiceService.createShippingPackageInvoice(
                                createShippingPackageInvoiceRequest);
                        response.addErrors(createShippingPackageInvoiceResponse.getErrors());
                        if (!response.hasErrors()) {
                            response.setSuccessful(true);
                        }
                    }
                }
            }
        }
        response.addErrors(context.getErrors());
        return response;
    }

    private Item addPurchaseDetails(ItemType itemType, Item item, WsWholesaleSaleOrderItem wsWholesaleSaleOrderItem, ReceiveShippingPackageFromWholesaleResponse response) {
        //get today's purchase order - pattern POyyyyMMdd
        PurchaseOrder purchaseOrder = purchaseService.getPurchaseOrderByCode(getPurchaseOrderCode());
        if (purchaseOrder == null) {
            purchaseOrder = createPurchaseOrder(response);
        }
        if (!response.hasErrors()) {
            PurchaseOrderItem purchaseOrderItem = null;
            for (PurchaseOrderItem orderItem : purchaseOrder.getPurchaseOrderItems()) {
                if (itemType.getId().equals(orderItem.getItemType().getId())) {
                    purchaseOrderItem = orderItem;
                }
            }
            if (purchaseOrderItem == null) {
                purchaseOrderItem = createPurchaseOrderItem(itemType, purchaseOrder, response, wsWholesaleSaleOrderItem.getSellingPrice());
            } else {
                purchaseOrderItem.setQuantity(purchaseOrderItem.getQuantity() + 1);
            }
            if (!response.hasErrors()) {
                purchaseOrderItem.setReceivedQuantity(purchaseOrderItem.getReceivedQuantity() + 1);
                InflowReceipt inflowReceipt;
                if (purchaseOrder.getInflowReceipts().size() > 0) {
                    inflowReceipt = purchaseOrder.getInflowReceipts().iterator().next();
                } else {
                    inflowReceipt = createInflowReceipt(purchaseOrder);
                }
                InflowReceiptItem inflowReceiptItem = null;
                for (InflowReceiptItem receiptItem : inflowReceipt.getInflowReceiptItems()) {
                    if (itemType.getId().equals(receiptItem.getItemType().getId())) {
                        inflowReceiptItem = receiptItem;
                    }
                }
                if (inflowReceiptItem == null) {
                    inflowReceiptItem = createInflowReceiptItem(inflowReceipt, purchaseOrderItem, itemType);
                    inflowReceiptItem.setStatusCode(InflowReceiptItem.StatusCode.COMPLETE.name());
                }
                inflowReceiptItem.setQuantity(inflowReceiptItem.getQuantity() + 1);
                inflowReceiptItem.setSubtotal(NumberUtils.multiply(inflowReceiptItem.getUnitPrice(), inflowReceiptItem.getQuantity()));
                inflowService.prepareinflowReceiptItemTaxation(inflowReceiptItem, purchaseOrderItem);
                if (item == null) {
                    item = createItem(inflowReceiptItem, purchaseOrder.getVendor(), itemType, wsWholesaleSaleOrderItem.getItemCode());
                }
                return item;
            }
        }
        return null;
    }

    private Item createItem(InflowReceiptItem inflowReceiptItem, Vendor vendor, ItemType itemType, String itemCode) {
        Item item = new Item(inflowReceiptItem, vendor, itemType, DateUtils.getCurrentTime(), DateUtils.getCurrentTime());
        item.setCode(itemCode);
        item.setStatusCode(Item.StatusCode.ALLOCATED.name());
        return inventoryDao.addItem(item, false);
    }

    private InflowReceiptItem createInflowReceiptItem(InflowReceipt inflowReceipt, PurchaseOrderItem purchaseOrderItem, ItemType itemType) {
        InflowReceiptItem inflowReceiptItem = new InflowReceiptItem(purchaseOrderItem, inflowReceipt, 0, purchaseOrderItem.getUnitPrice(), DateUtils.getCurrentTime(),
                DateUtils.getCurrentTime());
        inflowReceiptItem.setStatusCode(InflowReceiptItem.StatusCode.COMPLETE.name());
        inflowReceiptItem.setItemType(purchaseOrderItem.getItemType());
        inflowReceiptItem.setUnitPrice(purchaseOrderItem.getUnitPrice());
        inflowReceiptItem.setSubtotal(BigDecimal.ZERO);
        inflowReceiptItem.setTotal(BigDecimal.ZERO);
        inflowReceiptItem.setMaxRetailPrice(purchaseOrderItem.getMaxRetailPrice());
        inflowReceiptItem = inflowDao.addInflowReceiptItem(inflowReceiptItem);
        inflowReceipt.getInflowReceiptItems().add(inflowReceiptItem);
        return inflowReceiptItem;
    }

    private InflowReceipt createInflowReceipt(PurchaseOrder purchaseOrder) {
        InflowReceipt inflowReceipt = new InflowReceipt();
        inflowReceipt.setPurchaseOrder(purchaseOrder);
        inflowReceipt.setStatusCode(InflowReceipt.StatusCode.COMPLETE.name());
        inflowReceipt.setVendorInvoiceNumber(purchaseOrder.getCode());
        inflowReceipt.setVendorInvoiceDate(DateUtils.getCurrentTime());
        inflowReceipt.setUser(ConfigurationManager.getInstance().getConfiguration(TenantSystemConfiguration.class).getSystemUser());
        inflowReceipt.setCurrencyCode(purchaseOrder.getCurrencyCode());
        inflowReceipt.setCurrencyConversionRate(currencyService.getCurrencyConversionRate(new GetCurrencyConversionRateRequest(inflowReceipt.getCurrencyCode())).getConversionRate());
        inflowReceipt.setCreated(DateUtils.getCurrentTime());
        inflowReceipt.setUpdated(DateUtils.getCurrentTime());
        inflowReceipt = inflowDao.addInflowReceipt(inflowReceipt);
        purchaseOrder.getInflowReceipts().add(inflowReceipt);
        return inflowReceipt;
    }

    private PurchaseOrderItem createPurchaseOrderItem(ItemType itemType, PurchaseOrder purchaseOrder, ReceiveShippingPackageFromWholesaleResponse response, BigDecimal sellingPrice) {
        PurchaseOrderItem purchaseOrderItem = new PurchaseOrderItem();
        WsPurchaseOrderItem wsPurchaseOrderItem = new WsPurchaseOrderItem();
        wsPurchaseOrderItem.setUnitPrice(sellingPrice);
        wsPurchaseOrderItem.setQuantity(1);
        Vendor vendor = vendorService.getVendorById(purchaseOrder.getVendor().getId());
        purchaseService.preparePurchaseOrderItem(purchaseOrderItem, purchaseOrder, vendor, itemType, itemType.getSkuCode(), null, wsPurchaseOrderItem);
        purchaseOrderItem = purchaseDao.addPuchaseOrderItem(purchaseOrderItem);
        purchaseOrder.getPurchaseOrderItems().add(purchaseOrderItem);
        return purchaseOrderItem;
    }

    private PurchaseOrder createPurchaseOrder(ReceiveShippingPackageFromWholesaleResponse response) {
        Vendor vendor = vendorService.getVendorByCode(WHOLESALE_VENDOR_CODE);
        if (vendor == null || vendor.getVendorAgreements().size() == 0) {
            response.addError(new WsError(10000, "INVALID_CONFIGURATION"));
            return null;
        }
        PurchaseOrder purchaseOrder = new PurchaseOrder();
        purchaseOrder.setType(PurchaseOrder.Type.MANUAL.name());
        purchaseOrder.setCode(getPurchaseOrderCode());
        purchaseOrder.setVendor(vendor);
        purchaseOrder.setVendorAgreement(vendor.getVendorAgreements().iterator().next());
        purchaseOrder.setStatusCode(PurchaseOrder.StatusCode.COMPLETE.name());
        purchaseOrder.setFromParty(CacheManager.getInstance().getCache(FacilityCache.class).getCurrentFacility());
        purchaseOrder.setCurrencyCode(ConfigurationManager.getInstance().getConfiguration(TenantSystemConfiguration.class).getBaseCurrency());
        purchaseOrder.setCurrencyConversionRate(currencyService.getCurrencyConversionRate(new GetCurrencyConversionRateRequest(purchaseOrder.getCurrencyCode())).getConversionRate());

        User user = ConfigurationManager.getInstance().getConfiguration(TenantSystemConfiguration.class).getSystemUser();
        purchaseOrder.setUser(user);
        purchaseOrder.setLastUpdatedByUser(user);
        purchaseOrder.setCreated(DateUtils.getCurrentTime());
        purchaseOrder.setUpdated(DateUtils.getCurrentTime());
        purchaseOrder = purchaseDao.addPurchaseOrder(purchaseOrder);
        return purchaseOrder;
    }

    private String getPurchaseOrderCode() {
        return new StringBuilder().append("PO").append(DateUtils.dateToString(DateUtils.getCurrentTime(), "yyyyMMddHH")).toString();
    }

    private Map<String, WsWholesaleSaleOrderItem> getSaleOrderItemCodes(List<WsWholesaleSaleOrderItem> saleOrderItems) {
        Map<String, WsWholesaleSaleOrderItem> saleOrderItemCodes = new HashMap<String, WsWholesaleSaleOrderItem>();
        for (WsWholesaleSaleOrderItem wholesaleSaleOrderItem : saleOrderItems) {
            saleOrderItemCodes.put(wholesaleSaleOrderItem.getSaleOrderItemCode(), wholesaleSaleOrderItem);
        }
        return saleOrderItemCodes;
    }

    @Override
    public ReceiveReturnFromRetailResponse receiveReturnFromRetail(ReceiveReturnFromRetailRequest request) {
        ReceiveReturnFromRetailResponse response = new ReceiveReturnFromRetailResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            SaleOrder saleOrder = saleOrderService.getSaleOrderByCode(request.getSaleOrderCode());
            if (saleOrder == null) {
                context.addError(WsResponseCode.INVALID_SALE_ORDER_CODE, "Invalid saleOrderCode:" + request.getSaleOrderCode());
            } else {
                ShippingPackage shippingPackage = shippingService.getShippingPackageByCode(request.getShippingPackageCode());
                if (shippingPackage == null || !shippingPackage.getSaleOrder().getId().equals(saleOrder.getId())) {
                    context.addError(WsResponseCode.INVALID_SHIPPING_PACKAGE_CODE, "Invalid shippingPackageCode:" + request.getShippingPackageCode());
                } else {
                    String returnManifestCode = getReturnManifestCode(shippingPackage.getShippingProviderCode());
                    ReturnManifest returnManifest = returnsService.getReturnManifestByCode(returnManifestCode);
                    if (returnManifest == null) {
                        createReturnManifest(returnManifestCode, shippingPackage.getShippingProviderCode());
                    }
                    AddShippingPackageToReturnManifestResponse addPackageResponse = addShippingPackageToReturnManifest(shippingPackage.getCode(), returnManifestCode);
                    if (addPackageResponse.isSuccessful()) {
                        response.setSuccessful(true);
                    } else {
                        response.addErrors(addPackageResponse.getErrors());
                    }
                }
            }
        }
        response.addErrors(context.getErrors());
        return response;
    }

    @Transactional
    public AddShippingPackageToReturnManifestResponse addShippingPackageToReturnManifest(String code, String returnManifestCode) {
        AddShippingPackageToReturnManifestRequest request = new AddShippingPackageToReturnManifestRequest();
        request.setAwbOrShipmentCode(code);
        request.setReturnManifestCode(returnManifestCode);
        return returnsService.addShippingPackageToReturnManifest(request);
    }

    @Transactional
    public void createReturnManifest(String returnManifestCode, String code) {
        CreateReturnManifestRequest request = new CreateReturnManifestRequest();
        request.setReturnManifestCode(returnManifestCode);
        request.setShippingProviderCode(code);
        request.setUserId(ConfigurationManager.getInstance().getConfiguration(TenantSystemConfiguration.class).getSystemUser().getId());
        returnsService.createReturnManifest(request);
    }

    private String getReturnManifestCode(String shippingProviderCode) {
        return new StringBuilder().append("RM-").append(shippingProviderCode).append('-').append(DateUtils.dateToString(DateUtils.getCurrentTime(), "yyyyMMdd")).toString();
    }

    @Override
    public ReceiveReversePickupFromRetailResponse receiveReversePickupFromRetail(ReceiveReversePickupFromRetailRequest request) {
        ReceiveReversePickupFromRetailResponse response = new ReceiveReversePickupFromRetailResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            CreatePutawayRequest cpRequest = new CreatePutawayRequest();
            cpRequest.setType(Putaway.Type.PUTAWAY_RECEIVED_RETURNS.name());
            cpRequest.setUserId(request.getUserId());
            CreatePutawayResponse cpResponse = putawayService.createPutaway(cpRequest);
            if (!cpResponse.hasErrors()) {
                for (WsRetailReversePickupItem reversePickupItem : request.getReversePickupItems()) {
                    AddDirectReturnedSaleOrderItemsToPutawayRequest adrRequest = new AddDirectReturnedSaleOrderItemsToPutawayRequest();
                    adrRequest.setPutawayCode(cpResponse.getPutawayDTO().getCode());
                    adrRequest.setSaleOrderCode(reversePickupItem.getSaleOrderCode());
                    adrRequest.setReason("Returned By Retail");
                    adrRequest.setUserId(request.getUserId());
                    adrRequest.setCustomFieldValues(request.getCustomFieldValues());
                    List<WsSaleOrderItem> returnedSaleOrderItems = new ArrayList<WsSaleOrderItem>();
                    for (String itemCode : reversePickupItem.getItemCodes()) {
                        SaleOrderItem saleOrderItem = saleOrderService.getSaleOrderItemByItemCode(reversePickupItem.getSaleOrderCode(), itemCode);
                        WsSaleOrderItem wsSaleOrderItem = new WsSaleOrderItem();
                        wsSaleOrderItem.setCode(saleOrderItem.getCode());
                        wsSaleOrderItem.setStatus(ItemTypeInventory.Type.BAD_INVENTORY.name());
                        returnedSaleOrderItems.add(wsSaleOrderItem);
                    }
                    adrRequest.setReturnedSaleOrderItems(returnedSaleOrderItems);
                    response.addErrors(putawayService.addDirectReturnsSaleOrderItemsToPutaway(adrRequest).getErrors());
                }
            }
            Putaway putaway = putawayService.getPutawayByCode(cpResponse.getPutawayDTO().getCode());
            if (putaway.getPutawayItems().size() > 0) {
                CreatePutawayListRequest createPutawayListRequest = new CreatePutawayListRequest();
                createPutawayListRequest.setPutawayCode(cpResponse.getPutawayDTO().getCode());
                createPutawayListRequest.setUserId(request.getUserId());
                if (!putawayService.createPutawayList(createPutawayListRequest).hasErrors()) {
                    for (PutawayItem pi : putaway.getPutawayItems()) {
                        EditPutawayItemRequest editPutawayItemRequest = new EditPutawayItemRequest();
                        editPutawayItemRequest.setPutawayItemId(pi.getId());
                        editPutawayItemRequest.setShelfCode(Shelf.ShelfCode.DEFAULT.name());
                        editPutawayItemRequest.setQuantity(pi.getQuantity());
                        putawayService.editPutawayItem(editPutawayItemRequest);
                    }
                    CompletePutawayRequest completePutawayRequest = new CompletePutawayRequest();
                    completePutawayRequest.setPutawayCode(cpResponse.getPutawayDTO().getCode());
                    response.addErrors(putawayService.completePutaway(completePutawayRequest).getErrors());
                }
            } else {
                DiscardPutawayRequest discardPutawayRequest = new DiscardPutawayRequest();
                discardPutawayRequest.setPutawayCode(cpResponse.getPutawayDTO().getCode());
                discardPutawayRequest.setUserId(request.getUserId());
                putawayService.discardPutaway(discardPutawayRequest);
            }
            if (!response.hasErrors()) {
                response.setSuccessful(true);
            }
        }
        response.addErrors(context.getErrors());
        return response;
    }
}
