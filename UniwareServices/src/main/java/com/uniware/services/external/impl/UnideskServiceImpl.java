/*
 * Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *    
 * @version     1.0, 8/4/15 5:01 PM
 * @author amdalal
 */

package com.uniware.services.external.impl;

import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.unifier.core.api.external.GetUnideskTokenRequest;
import com.unifier.core.api.external.GetUnideskTokenResponse;
import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.utils.EncryptionUtils;
import com.uniware.core.cache.EnvironmentPropertiesCache;
import com.uniware.core.utils.UserContext;
import com.uniware.services.external.IUnideskService;

@Service(value = "unideskService")
public class UnideskServiceImpl implements IUnideskService {

    private static final Logger LOG = LoggerFactory.getLogger(UnideskServiceImpl.class);

    @Override
    public GetUnideskTokenResponse getUnideskToken(GetUnideskTokenRequest request) {
        GetUnideskTokenResponse response = new GetUnideskTokenResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            String customerCode = UserContext.current().getTenant().getCode();
            long timestamp = DateUtils.getCurrentTime().getTime();
            try {
                String[] props = CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class).getUnideskProperties().split("\\|");
                StringBuilder tokenBuilder = new StringBuilder(props[0]).append(":").append(customerCode).append(":").append(request.getUsername()).append(":").append(
                        request.getName()).append(":").append(timestamp).append(":");
                tokenBuilder.append(EncryptionUtils.md5Encode(request.getName() + request.getUsername() + customerCode + props[0] + props[1], String.valueOf(timestamp)));
                response.setToken(EncryptionUtils.hexEncode(tokenBuilder.toString()));
                if (StringUtils.isNotBlank(request.getMobile())) {
                    response.setMobile(request.getMobile());
                }
                if (StringUtils.isNotBlank(request.getProductType())) {
                    response.setProductType(request.getProductType());
                }
                response.setSuccessful(true);
            } catch (Exception e) {
                LOG.error("Failed to parse unidesk properties. Should be defined as ACCOUNT_CODE|SECRET_KEY");
            }
        }
        return response;
    }
}
