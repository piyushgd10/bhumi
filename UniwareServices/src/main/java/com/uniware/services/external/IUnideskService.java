/*
 * Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *    
 * @version     1.0, 8/4/15 5:00 PM
 * @author amdalal
 */

package com.uniware.services.external;

import com.unifier.core.api.external.GetUnideskTokenRequest;
import com.unifier.core.api.external.GetUnideskTokenResponse;

public interface IUnideskService {

    GetUnideskTokenResponse getUnideskToken(GetUnideskTokenRequest request);
}
