/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 17-Feb-2012
 *  @author vibhu
 */
package com.uniware.services.admin.pickset;

import java.util.List;

import com.uniware.core.api.admin.pickset.CreatePickAreaRequest;
import com.uniware.core.api.admin.pickset.CreatePickAreaResponse;
import com.uniware.core.api.admin.pickset.CreatePicksetRequest;
import com.uniware.core.api.admin.pickset.CreatePicksetResponse;
import com.uniware.core.api.admin.pickset.EditPicksetRequest;
import com.uniware.core.api.admin.pickset.EditPicksetResponse;
import com.uniware.core.api.admin.pickset.GetPickSetsByTypeRequest;
import com.uniware.core.api.admin.pickset.GetPickSetsByTypeResponse;
import com.uniware.core.api.admin.pickset.UpdatePicksetSectionsRequest;
import com.uniware.core.api.admin.pickset.UpdatePicksetSectionsResponse;
import com.uniware.core.entity.Facility;
import com.uniware.core.entity.PickArea;
import com.uniware.core.entity.PickSet;
import com.uniware.core.entity.Section;

public interface IPicksetService {

    CreatePickAreaResponse createPickArea(CreatePickAreaRequest request);

    CreatePicksetResponse createPickset(CreatePicksetRequest request);

    EditPicksetResponse editPickset(EditPicksetRequest request);

    List<PickSet> getPicksets();

    List<PickSet> getActivePicksets();

    UpdatePicksetSectionsResponse updatePicksetSections(UpdatePicksetSectionsRequest request);

    Section getDefaultSection(Facility facility);

    List<PickSet> getPickSetsByType(PickSet.Type type);

    List<PickSet> getAllPickSets();

    PickSet getPicksetById(Integer picksetId);

    PickSet getPickSetByName(String pickSetName);

    PickSet getActivePickSetByNameAndType(String zoneName);

    List<PickArea> getPickAreas();

    GetPickSetsByTypeResponse getPickSetDTOsByType(GetPickSetsByTypeRequest request);
}
