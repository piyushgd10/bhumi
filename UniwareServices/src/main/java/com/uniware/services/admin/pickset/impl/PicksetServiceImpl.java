/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 17-Feb-2012
 *  @author vibhu
 */
package com.uniware.services.admin.pickset.impl;

import java.util.ArrayList;
import java.util.List;

import com.uniware.core.api.admin.pickset.CreatePickAreaRequest;
import com.uniware.core.api.admin.pickset.CreatePickAreaResponse;
import com.uniware.core.api.admin.pickset.GetPickSetsByTypeRequest;
import com.uniware.core.api.admin.pickset.GetPickSetsByTypeResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.utils.DateUtils;
import com.unifier.services.aspect.MarkDirty;
import com.uniware.core.api.admin.pickset.CreatePicksetRequest;
import com.uniware.core.api.admin.pickset.CreatePicksetResponse;
import com.uniware.core.api.admin.pickset.EditPicksetRequest;
import com.uniware.core.api.admin.pickset.EditPicksetResponse;
import com.uniware.core.api.admin.pickset.PicksetDTO;
import com.uniware.core.api.admin.pickset.PicksetSectionDTO;
import com.uniware.core.api.admin.pickset.UpdatePicksetSectionsRequest;
import com.uniware.core.api.admin.pickset.UpdatePicksetSectionsResponse;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.entity.Facility;
import com.uniware.core.entity.PickArea;
import com.uniware.core.entity.PickSet;
import com.uniware.core.entity.Section;
import com.uniware.dao.admin.pickset.IPicksetDao;
import com.uniware.dao.warehouse.ISectionDao;
import com.uniware.services.admin.pickset.IPicksetService;
import com.uniware.services.cache.FacilityLayoutCache;

@Service
@Transactional
public class PicksetServiceImpl implements IPicksetService {

    @Autowired
    private IPicksetDao picksetDao;

    @Autowired
    private ISectionDao sectionDao;

    @Override
    public CreatePickAreaResponse createPickArea(CreatePickAreaRequest request) {
        CreatePickAreaResponse response = new CreatePickAreaResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            PickArea pickArea = picksetDao.getPickAreaByName(request.getName());
            if (pickArea != null) {
                context.addError(WsResponseCode.INVALID_REQUEST, "ZoneGroup exists with name \"" + request.getName() + "\"");
            } else {
                pickArea = new PickArea();
                pickArea.setName(request.getName());
                pickArea.setCreated(DateUtils.getCurrentTime());
                picksetDao.createPickArea(pickArea);
                response.setPickAreaName(request.getName());
                response.setSuccessful(true);
            }
        }
        if (context.hasErrors()) {
            response.addErrors(context.getErrors());
            response.addWarnings(context.getWarnings());
        } else {
            response.setSuccessful(true);
        }
        return response;
    }

    @Override
    @MarkDirty(values = { FacilityLayoutCache.class })
    public CreatePicksetResponse createPickset(CreatePicksetRequest request) {
        CreatePicksetResponse response = new CreatePicksetResponse();
        ValidationContext context = request.validate();
        if (context.hasErrors()) {
            response.setSuccessful(false);
            response.setErrors(context.getErrors());
        } else {
            PickArea pickArea = picksetDao.getPickAreaByName(request.getPickAreaName());
            if (picksetDao.getPicksetByName(request.getName()) != null) {
                context.addError(WsResponseCode.DUPLICATE_CODE, "Zone exists with name \"" + request.getName() + "\"");
                response.setSuccessful(false);
                response.setErrors(context.getErrors());
            } else if (pickArea == null) {
                context.addError(WsResponseCode.INVALID_REQUEST, "No ZoneGroup exists with name \"" + request.getPickAreaName() + "\"");
                response.addErrors(context.getErrors());
            } else if (PickSet.Type.STAGING.equals(request.getPickSetType())
                    && picksetDao.getPicksetByTypeInPickArea(pickArea.getName(), PickSet.Type.STAGING).size() >= 1) {
                context.addError(WsResponseCode.INVALID_REQUEST, "There can be only one STAGING zone in  \"" + request.getPickAreaName() + "\"");
                response.addErrors(context.getErrors());
            } else {
                PickSet pickset = new PickSet();
                pickset.setName(request.getName().toUpperCase());
                pickset.setCapacity(request.getCapacity());
                pickset.setActive(true);
                pickset.setType(request.getPickSetType());
                pickset.setPickArea(pickArea);
                pickset.setEditable(!request.isUseAsDefault());
                pickset.setCreated(DateUtils.getCurrentTime());
                pickset = picksetDao.createPickset(pickset);
                response.setSuccessful(true);
                response.setPicksetDTO(new PicksetDTO(pickset));
            }
        }
        return response;
    }

    @Override
    @MarkDirty(values = { FacilityLayoutCache.class })
    public EditPicksetResponse editPickset(EditPicksetRequest request) {
        EditPicksetResponse response = new EditPicksetResponse();
        ValidationContext context = request.validate();
        if (context.hasErrors()) {
            response.setSuccessful(false);
            response.setErrors(context.getErrors());
        } else {
            PickSet picksetById = picksetDao.getPicksetById(request.getId());
            if (picksetById == null) {
                context.addError(WsResponseCode.INVALID_CODE, "No Zone exists with Id \"" + request.getId() + "\"");
                response.setSuccessful(false);
                response.setErrors(context.getErrors());
            } else {
                PickSet picksetByName = picksetDao.getPicksetByName(request.getName());
                if (picksetById.getId() != picksetByName.getId()) {
                    context.addError(WsResponseCode.DUPLICATE_CODE, "Zone exists with name \"" + request.getName() + "\"");
                    response.setSuccessful(false);
                    response.setErrors(context.getErrors());
                } else {
                    picksetById.setName(request.getName());
                    picksetById.setCapacity(request.getCapacity());
                    picksetById.setActive(request.isEnabled());
                    picksetById = picksetDao.editPickset(picksetById);
                    response.setSuccessful(true);
                    response.setPicksetDTO(new PicksetDTO(picksetById));
                }
            }
        }
        return response;
    }

    @Override
    public List<PickSet> getPicksets() {
        return picksetDao.getPicksets();
    }

    @Override
    @MarkDirty(values = { FacilityLayoutCache.class })
    public UpdatePicksetSectionsResponse updatePicksetSections(UpdatePicksetSectionsRequest request) {
        UpdatePicksetSectionsResponse response = new UpdatePicksetSectionsResponse();
        ValidationContext context = request.validate();
        if (context.hasErrors()) {
            response.setSuccessful(false);
            response.setErrors(context.getErrors());
        } else {

            for (PicksetSectionDTO picksetSectionDTO : request.getPicksetSectionDTOs()) {
                Section section = sectionDao.getSectionById(picksetSectionDTO.getSectionId());
                PickSet pickSet = picksetDao.getPicksetById(picksetSectionDTO.getPicksetId());
                section.setPickSet(pickSet);
                response.addPicksetSection(section);

            }
            response.setSuccessful(true);
        }
        return response;
    }

    @Override
    public Section getDefaultSection(Facility facility) {
        Section section = picksetDao.getDefaultSection(facility);
        return section;
    }

    @Override
    public List<PickSet> getActivePicksets() {
        List<PickSet> picksets = picksetDao.getPicksets();
        PickSet pickset;
        for (int i = 0; i < picksets.size(); i++) {
            pickset = picksets.get(i);
            if (!pickset.isActive()) {
                picksets.remove(i);
            }
        }
        return picksets;
    }

    @Override
    @Transactional(readOnly = true)
    public GetPickSetsByTypeResponse getPickSetDTOsByType(GetPickSetsByTypeRequest request) {
        GetPickSetsByTypeResponse response = new GetPickSetsByTypeResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            List<PickSet> picksets;
            if (PickSet.Type.STAGING.equals(request.getType())) {
                picksets = getPickSetsByType(PickSet.Type.STAGING);
            } else if (PickSet.Type.STOCKING.equals(request.getType())) {
                picksets = getPickSetsByType(PickSet.Type.STOCKING);
            } else {
                picksets = getAllPickSets();
            }
            List<PicksetDTO> picksetDTOs = new ArrayList<>();
            for (PickSet pickSet : picksets) {
                picksetDTOs.add(new PicksetDTO(pickSet));
            }
            response.setPicksets(picksetDTOs);
        }
        if (!context.hasErrors()) {
            response.setSuccessful(true);
        } else {
            response.addErrors(context.getErrors());
            response.addWarnings(context.getWarnings());
        }
        return response;
    }

    @Override
    public List<PickSet> getPickSetsByType(PickSet.Type type) {
        return picksetDao.getPickSetsByType(type);
    }

    @Override
    @Transactional
    public List<PickSet> getAllPickSets() {
        return picksetDao.getAllPickSets();
    }

    @Override
    @Transactional
    public PickSet getPicksetById(Integer picksetId) {
        return picksetDao.getPicksetById(picksetId);
    }

    @Override
    @Transactional(readOnly = true)
    public PickSet getPickSetByName(String pickSetName) {
        return picksetDao.getPicksetByName(pickSetName);
    }

    @Override
    @Transactional
    public PickSet getActivePickSetByNameAndType(String zoneName) {
        return picksetDao.getActivePickSetByNameAndType(zoneName);
    }

    @Override
    public List<PickArea> getPickAreas() {
        return picksetDao.getPickAreas();
    }
}
