/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 5, 2012
 *  @author singla
 */
package com.uniware.services.putaway.impl;

import com.uniware.core.api.returns.MarkSaleOrderReturnedRequest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.unifier.core.annotation.Level;
import com.unifier.core.annotation.audit.LogActivity;
import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.entity.User;
import com.unifier.core.utils.CollectionUtils;
import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.StringUtils;
import com.unifier.scraper.sl.runtime.IScriptProvider;
import com.unifier.scraper.sl.runtime.ScraperScript;
import com.unifier.scraper.sl.runtime.ScriptExecutionContext;
import com.unifier.services.aspect.RollbackOnFailure;
import com.unifier.services.users.IUsersService;
import com.uniware.core.api.gatepass.AddGatePassItemsToPutawayRequest;
import com.uniware.core.api.gatepass.AddGatePassItemsToPutawayRequest.WsGatePassItem;
import com.uniware.core.api.gatepass.AddGatePassItemsToPutawayResponse;
import com.uniware.core.api.invoice.CreateShippingPackageReverseInvoiceRequest;
import com.uniware.core.api.invoice.CreateShippingPackageReverseInvoiceResponse;
import com.uniware.core.api.invoice.UpdateInvoiceRequest;
import com.uniware.core.api.invoice.UpdateInvoiceResponse;
import com.uniware.core.api.item.AddInspectedNotBadItemToPutawayRequest;
import com.uniware.core.api.item.AddInspectedNotBadItemToPutawayResponse;
import com.uniware.core.api.item.AddNonTraceablePicklistItemsToPutawayRequest;
import com.uniware.core.api.item.AddNonTraceablePicklistItemsToPutawayResponse;
import com.uniware.core.api.item.AddPutbackPendingItemToPutawayRequest;
import com.uniware.core.api.item.AddPutbackPendingItemToPutawayResponse;
import com.uniware.core.api.item.GetItemDetailRequest;
import com.uniware.core.api.item.GetItemDetailResponse;
import com.uniware.core.api.putaway.AddInflowReceiptItemsToPutawayRequest;
import com.uniware.core.api.putaway.AddInflowReceiptItemsToPutawayResponse;
import com.uniware.core.api.putaway.AddNonTraceableItemToTransferPutawayRequest;
import com.uniware.core.api.putaway.AddNonTraceableItemToTransferPutawayResponse;
import com.uniware.core.api.putaway.AddTraceableItemToTransferPutawayRequest;
import com.uniware.core.api.putaway.AddTraceableItemToTransferPutawayResponse;
import com.uniware.core.api.putaway.CheckItemForPutawayRequest;
import com.uniware.core.api.putaway.CheckItemForPutawayResponse;
import com.uniware.core.api.putaway.CompleteItemPutawayRequest;
import com.uniware.core.api.putaway.CompleteItemPutawayResponse;
import com.uniware.core.api.putaway.CompletePutawayItemsRequest;
import com.uniware.core.api.putaway.CompletePutawayItemsResponse;
import com.uniware.core.api.putaway.CompletePutawayRequest;
import com.uniware.core.api.putaway.CompletePutawayResponse;
import com.uniware.core.api.putaway.CreatePutawayListRequest;
import com.uniware.core.api.putaway.CreatePutawayListResponse;
import com.uniware.core.api.putaway.CreatePutawayRequest;
import com.uniware.core.api.putaway.CreatePutawayResponse;
import com.uniware.core.api.putaway.DiscardPutawayRequest;
import com.uniware.core.api.putaway.DiscardPutawayResponse;
import com.uniware.core.api.putaway.EditPutawayItemRequest;
import com.uniware.core.api.putaway.EditPutawayItemResponse;
import com.uniware.core.api.putaway.GetPutawayRequest;
import com.uniware.core.api.putaway.GetPutawayResponse;
import com.uniware.core.api.putaway.GetPutawayTypesRequest;
import com.uniware.core.api.putaway.GetPutawayTypesResponse;
import com.uniware.core.api.putaway.GetShelfRequest;
import com.uniware.core.api.putaway.GetShelfResponse;
import com.uniware.core.api.putaway.PutawayDTO;
import com.uniware.core.api.putaway.PutawayItemDTO;
import com.uniware.core.api.putaway.ReleaseInventoryAndRepackageRequest;
import com.uniware.core.api.putaway.ReleaseInventoryAndRepackageResponse;
import com.uniware.core.api.putaway.ReleaseInventoryAndRepackageResponse.FailedShippingPackageDTO;
import com.uniware.core.api.putaway.SearchDirectReturnsRequest;
import com.uniware.core.api.putaway.SearchDirectReturnsResponse;
import com.uniware.core.api.putaway.SearchDirectReturnsResponse.DirectSaleOrderItemDTO;
import com.uniware.core.api.putaway.SplitPutawayItemRequest;
import com.uniware.core.api.putaway.SplitPutawayItemRequest.PutawayItemToSplit;
import com.uniware.core.api.putaway.SplitPutawayItemResponse;
import com.uniware.core.api.returns.AddDirectReturnedSaleOrderItemsToPutawayRequest;
import com.uniware.core.api.returns.AddReturnedSaleOrderItemsToPutawayResponse;
import com.uniware.core.api.returns.AddReturnsSaleOrderItemsToPutawayRequest;
import com.uniware.core.api.returns.AddReturnsSaleOrderItemsToPutawayRequest.SaleOrderItemDTO;
import com.uniware.core.api.returns.AddReturnsSaleOrderItemsToPutawayResponse;
import com.uniware.core.api.returns.CreateReversePickupRequest;
import com.uniware.core.api.returns.CreateReversePickupResponse;
import com.uniware.core.api.returns.WsReversePickupItem;
import com.uniware.core.api.reversepickup.AddReversePickupSaleOrderItemsToPutawayRequest;
import com.uniware.core.api.reversepickup.AddReversePickupSaleOrderItemsToPutawayRequest.WsSaleOrderItem;
import com.uniware.core.api.reversepickup.AddReversePickupSaleOrderItemsToPutawayResponse;
import com.uniware.core.api.shipping.AddCancelledSaleOrderItemsToPutawayRequest;
import com.uniware.core.api.shipping.AddCancelledSaleOrderItemsToPutawayResponse;
import com.uniware.core.api.shipping.ReassessShippingPackageRequest;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.entity.Channel;
import com.uniware.core.entity.ChannelConfigurationParameter;
import com.uniware.core.entity.ChannelConnector;
import com.uniware.core.entity.ChannelConnectorParameter;
import com.uniware.core.entity.InflowReceipt;
import com.uniware.core.entity.InflowReceiptItem;
import com.uniware.core.entity.InventoryLedger;
import com.uniware.core.entity.Item;
import com.uniware.core.entity.Item.RejectionReason;
import com.uniware.core.entity.ItemType;
import com.uniware.core.entity.ItemTypeInventory;
import com.uniware.core.entity.ItemTypeInventory.Type;
import com.uniware.core.entity.OutboundGatePass;
import com.uniware.core.entity.OutboundGatePassItem;
import com.uniware.core.entity.PicklistItem;
import com.uniware.core.entity.Putaway;
import com.uniware.core.entity.PutawayItem;
import com.uniware.core.entity.PutawayStatus;
import com.uniware.core.entity.ReversePickup;
import com.uniware.core.entity.SaleOrder;
import com.uniware.core.entity.SaleOrderItem;
import com.uniware.core.entity.Shelf;
import com.uniware.core.entity.ShipmentTrackingStatus;
import com.uniware.core.entity.ShippingPackage;
import com.uniware.core.entity.Source;
import com.uniware.core.locking.Namespace;
import com.uniware.core.locking.annotation.Lock;
import com.uniware.core.locking.annotation.Locks;
import com.uniware.core.utils.ActivityContext;
import com.uniware.core.utils.Constants;
import com.uniware.core.utils.UserContext;
import com.uniware.dao.inflow.IInflowDao;
import com.uniware.dao.putaway.IPutawayDao;
import com.uniware.services.audit.impl.ActivityEntityEnum;
import com.uniware.services.audit.impl.ActivityTypeEnum;
import com.uniware.services.audit.impl.ActivityUtils;
import com.uniware.services.cache.ChannelCache;
import com.uniware.services.cache.ScriptVersionedCache;
import com.uniware.services.catalog.ICatalogService;
import com.uniware.services.configuration.ShippingConfiguration;
import com.uniware.services.configuration.SystemConfiguration;
import com.uniware.services.configuration.SystemConfiguration.InventoryAccuracyLevel;
import com.uniware.services.configuration.SystemConfiguration.TraceabilityLevel;
import com.uniware.services.exception.InventoryAccuracyException;
import com.uniware.services.exception.InventoryNotAvailableException;
import com.uniware.services.exception.ShelfBlockedForCycleCountException;
import com.uniware.services.inflow.IInflowService;
import com.uniware.services.inventory.IInventoryService;
import com.uniware.services.invoice.IInvoiceService;
import com.uniware.services.item.IItemService;
import com.uniware.services.ledger.IInventoryLedgerService;
import com.uniware.services.material.IMaterialService;
import com.uniware.services.material.impl.MaterialServiceImpl;
import com.uniware.services.picker.IPickerService;
import com.uniware.services.putaway.IPutawayService;
import com.uniware.services.returns.IReturnsService;
import com.uniware.services.saleorder.ISaleOrderService;
import com.uniware.services.shipping.IShippingInvoiceService;
import com.uniware.services.shipping.IShippingService;
import com.uniware.services.tax.ITaxService;
import com.uniware.services.warehouse.IShelfService;

/**
 * @author singla
 */
@Service("putawayService")
public class PutawayServiceImpl implements IPutawayService {

    private static final Logger     LOG = LoggerFactory.getLogger(PutawayServiceImpl.class);
    @Autowired
    private IPutawayDao             putawayDao;

    @Autowired
    private ISaleOrderService       saleOrderService;

    @Autowired
    private ICatalogService         catalogService;

    @Autowired
    private IUsersService           usersService;

    @Autowired
    private IShelfService           shelfService;

    @Autowired
    private IInflowDao              inflowDao;

    @Autowired
    private IInventoryService       inventoryService;

    @Autowired
    private IShippingService        shippingService;

    @Autowired
    private IReturnsService         returnsService;

    @Autowired
    private IMaterialService        materialService;

    @Autowired
    private IInvoiceService         invoiceService;

    @Autowired
    private IItemService            itemService;

    @Autowired
    private IInflowService          inflowService;

    @Autowired
    private IShippingInvoiceService shippingInvoiceService;

    @Autowired
    private ITaxService             taxService;

    @Autowired
    private IPickerService          pickerService;

    @Autowired
    private IInventoryLedgerService inventoryLedgerService;

    @Override
    @Transactional
    public CreatePutawayResponse createPutaway(CreatePutawayRequest request) {
        CreatePutawayResponse response = new CreatePutawayResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            Putaway putaway = new Putaway();
            putaway.setUser(usersService.getUserWithDetailsById(request.getUserId()));
            putaway.setStatusCode(Putaway.StatusCode.CREATED.name());
            putaway.setType(Putaway.Type.valueOf(request.getType()).name());
            putaway.setCreated(DateUtils.getCurrentTime());
            putaway.setUpdated(DateUtils.getCurrentTime());
            putaway = putawayDao.addPutaway(putaway);
            response.setPutawayDTO(preparePutawayDTO(putaway));
            response.setSuccessful(true);
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Locks({ @Lock(ns = Namespace.PUTAWAY, key = "#{#args[0].putawayCode}", level = Level.FACILITY) })
    @Transactional
    public DiscardPutawayResponse discardPutaway(DiscardPutawayRequest request) {
        DiscardPutawayResponse response = new DiscardPutawayResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            Putaway putaway = getPutawayByCode(request.getPutawayCode());
            if (putaway == null) {
                context.addError(WsResponseCode.INVALID_PUTAWAY_ID, "Invalid putaway code");
            } else if (!Putaway.StatusCode.CREATED.name().equals(putaway.getStatusCode())) {
                context.addError(WsResponseCode.INVALID_PUTAWAY_STATE, "Putaway not in CREATED state");
            } else if (putaway.getPutawayItems().size() > 0) {
                context.addError(WsResponseCode.INVALID_PUTAWAY_STATE, "Items already added to putaway");
            } else if (!putaway.getUser().getId().equals(request.getUserId())) {
                context.addError(WsResponseCode.INVALID_USER_ID, "putaway created by another user");
            } else {
                putaway.setStatusCode(Putaway.StatusCode.DISCARDED.name());
                response.setSuccessful(true);
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    public EditPutawayItemResponse editPutawayItem(EditPutawayItemRequest request) {
        EditPutawayItemResponse response = new EditPutawayItemResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            PutawayItem putawayItem = putawayDao.getPutawayItemById(request.getPutawayItemId(), true);
            if (putawayItem == null) {
                context.addError(WsResponseCode.INVALID_PUTAWAY_ITEM_ID, "Invalid inflow putaway item id");
            } else if (!Putaway.StatusCode.PENDING.name().equals(putawayItem.getPutaway().getStatusCode())) {
                context.addError(WsResponseCode.INVALID_PUTAWAY_STATE, "Shelf can be modified only in PENDING state");
            } else {
                Shelf shelf = shelfService.getShelfByCode(request.getShelfCode());
                if (shelf == null) {
                    context.addError(WsResponseCode.INVALID_SHELF_CODE, "Invalid Shelf Code");
                } else if (shelf.isBlockedForCycleCount()) {
                    context.addError(WsResponseCode.SHELF_BLOCKED_FOR_CYCLE_COUNT, "Shelf blocked for cycle count");
                } else if (shelf.isInactive()) {
                    context.addError(WsResponseCode.INACTIVE_SHELF, "Shelf is inactive.");
                } else {
                    putawayItem.setShelf(shelf);
                    response.setSuccessful(true);
                }
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    /**
     * @param putaway
     * @return
     */
    private PutawayDTO preparePutawayDTO(Putaway putaway) {
        PutawayDTO putawayDTO = new PutawayDTO();
        putawayDTO.setCode(putaway.getCode());
        putawayDTO.setType(putaway.getType());
        putawayDTO.setId(putaway.getId());
        putawayDTO.setStatus(putaway.getStatusCode());
        putawayDTO.setCreated(putaway.getCreated());
        putawayDTO.setUsername(putaway.getUser().getUsername());
        return putawayDTO;
    }

    /**
     * Performs validation and then forwards request to
     * {@link #addInflowReceiptItemsToPutaway(String, int, List, AddInflowReceiptItemsToPutawayResponse, ValidationContext)}<br/>
     * Used when items are inwarded in facility after {@link InflowReceipt} aka GRN
     * <ol>
     * Following conditions should be met while adding items:
     * <li>The putaway should be in {@link Putaway.StatusCode#CREATED} state.</li>
     * <li>Putaway must be of type {@link Putaway.Type#PUTAWAY_GRN_ITEM}</li>
     * <li>Only the user which created the putaway can add items in it.</li>
     * </ol>
     */
    @Override
    @Locks({ @Lock(ns = Namespace.PUTAWAY, key = "#{#args[0].putawayCode}", level = Level.FACILITY) })
    public AddInflowReceiptItemsToPutawayResponse addInflowReceiptItemsToPutaway(AddInflowReceiptItemsToPutawayRequest request) {
        AddInflowReceiptItemsToPutawayResponse response = new AddInflowReceiptItemsToPutawayResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            InflowReceipt inflowReceipt = inflowService.getInflowReceiptByCode(request.getInflowReceiptCode());
            if (inflowReceipt == null) {
                context.addError(WsResponseCode.INVALID_INFLOW_RECEIPT_ID, "No inflow receipt found");
            } else {
                Putaway putaway = getPutawayByCode(request.getPutawayCode());
                if (putaway == null) {
                    context.addError(WsResponseCode.INVALID_PUTAWAY_ID, "Invalid putaway id");
                } else if (!Putaway.StatusCode.CREATED.name().equals(putaway.getStatusCode())) {
                    context.addError(WsResponseCode.INVALID_PUTAWAY_STATE, "Items can be added only in CREATED state");
                } else if (!Putaway.Type.PUTAWAY_GRN_ITEM.name().equals(putaway.getType())) {
                    context.addError(WsResponseCode.INVALID_PUTAWAY_CODE, "Putaway should be of GRN type");
                } else if (!putaway.getUser().getId().equals(request.getUserId())) {
                    context.addError(WsResponseCode.INVALID_USER_ID, "putaway created by another user");
                } else {
                    return addInflowReceiptItemsToPutaway(request.getPutawayCode(), inflowReceipt.getId(), request.getInflowReceiptItemIds(), response, context);
                }
            }
        }
        if (context.hasErrors()) {
            response.addErrors(context.getErrors());
        }
        return response;
    }

    /**
     * Adds GRN items to {@link Putaway}.
     * <p>
     * First, it fetches {@link InflowReceiptItem}s aka <b>iri</b> of the {@link InflowReceipt} aka GRN. Then, checks if
     * they all are {@code QC_COMPLETE}. Then, fetches all the items of each iri, and checks if they are not expired.
     * Then, we create a map of all putaway items of the given putaway, with mapping of Type to Sku to Ageing to Putaway
     * item: {@code typeToSkuCodeToAgeingDateToPutawayItemMap}
     * <ol>
     * For each inflow receipt item, there are two possibilities:
     * <li>It has a putaway item corresponding to it in the map. In that case, we update the quantity of that putaway
     * item.</li>
     * <li>It does not have a putaway item in the map. In that case, we create a new putawa item corresponding to it,
     * and add it to the putaway.</li>
     * </ol>
     * This is done for both the {@code GOOD_INVENTORY} and {@code QC_REJECTED} inventory types. Finally, if the GRN is
     * complete, then its status is updated acordingly.
     * <p>
     * <b>Note</b>: This api only works on {@link TraceabilityLevel#ITEM}
     * </p>
     */
    @Transactional
    @Locks({ @Lock(ns = Namespace.PURCHASE_ORDER, key = "#{#args[1]}", level = Level.FACILITY) })
    @LogActivity
    @RollbackOnFailure
    private AddInflowReceiptItemsToPutawayResponse addInflowReceiptItemsToPutaway(String putawayCode, int inflowReceiptId, List<Integer> inflowReceiptItemIds,
            AddInflowReceiptItemsToPutawayResponse response, ValidationContext context) {
        Putaway putaway = getPutawayByCode(putawayCode);
        InflowReceipt inflowReceipt = inflowService.getInflowReceiptById(inflowReceiptId);
        Map<Integer, InflowReceiptItem> idToInflowReceiptItems = new HashMap<>();
        for (InflowReceiptItem inflowReceiptItem : inflowReceipt.getInflowReceiptItems()) {
            idToInflowReceiptItems.put(inflowReceiptItem.getId(), inflowReceiptItem);
        }
        List<InflowReceiptItem> inflowReceiptItems = new ArrayList<>();
        for (Integer inflowReceiptItemId : inflowReceiptItemIds) {
            InflowReceiptItem inflowReceiptItem = idToInflowReceiptItems.get(inflowReceiptItemId);
            if (inflowReceiptItem == null) {
                context.addError(WsResponseCode.INVALID_INFLOW_RECEIPT_ITEM_ID, "Invalid inflow receipt item id:" + inflowReceiptItemId);
                break;
            } else if (!InflowReceiptItem.StatusCode.QC_COMPLETE.name().equals(inflowReceiptItem.getStatusCode())) {
                context.addError(WsResponseCode.INVALID_INFLOW_RECEIPT_STATE, "Only QC_COMPLETE inflow receipts can be added to putaway");
            } else {
                inflowReceiptItems.add(inflowReceiptItem);
            }
        }
        if (!context.hasErrors()) {
            Map<Type, Map<String, Map<Date, PutawayItem>>> typeToSkuCodeToAgeingDateToPutawayItemMap = getPutawayItemsMap(putaway);
            for (InflowReceiptItem inflowReceiptItem : inflowReceiptItems) {
                List<Item> items = inflowDao.getItemsByReceiptItemId(inflowReceiptItem.getId());
                ItemType itemType = catalogService.getNonBundledItemTypeById(inflowReceiptItem.getItemType().getId());
                if (catalogService.isItemTypeExpirable(itemType)) {
                    items.stream().filter(item -> item.getManufacturingDate() == null).forEach(item -> {
                        context.addError(WsResponseCode.INVALID_ITEM_CODE,
                                "Cant add expirable item to putaway before adding manufacturing date, Item : " + item.getCode() + " SKU : " + item.getItemType().getSkuCode());
                    });
                }
                if (!context.hasErrors()) {
                    Date ageingStartDate = Constants.ITEM_SKU_OR_NONE_TRACEABILITY_AGEING_DATE;
                    if (TraceabilityLevel.ITEM.equals(ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).getTraceabilityLevel())) {
                        ageingStartDate = inflowReceiptItem.getAgeingStartDate();
                    }
                    // Adding Good Inventory items to Putaway with type GOOD_INVENTORY
                    if (inflowReceiptItem.getQuantity() - inflowReceiptItem.getRejectedQuantity() > 0) {
                        PutawayItem putawayItem = getPutawayItem(typeToSkuCodeToAgeingDateToPutawayItemMap, Type.GOOD_INVENTORY,
                                inflowReceiptItem.getPurchaseOrderItem().getItemType().getSkuCode(), ageingStartDate);
                        if (putawayItem == null) {
                            putawayItem = new PutawayItem(putaway, Type.GOOD_INVENTORY, ageingStartDate, inflowReceiptItem.getPurchaseOrderItem().getItemType(),
                                    inflowReceiptItem.getQuantity() - inflowReceiptItem.getRejectedQuantity(), 0, PutawayItem.StatusCode.CREATED.name(), DateUtils.getCurrentTime(),
                                    DateUtils.getCurrentTime());
                            putaway.getPutawayItems().add(putawayItem);
                            putawayDao.addPutawayItem(putawayItem);
                        } else {
                            putawayItem.setQuantity(putawayItem.getQuantity() + inflowReceiptItem.getQuantity() - inflowReceiptItem.getRejectedQuantity());
                            putawayDao.updatePutawayItem(putawayItem);
                        }
                    }

                    // Adding QC Rejected items to Putaway with type QC_REJECTED
                    if (inflowReceiptItem.getRejectedQuantity() > 0) {
                        PutawayItem qcRejectedPutawayItem = getPutawayItem(typeToSkuCodeToAgeingDateToPutawayItemMap, Type.QC_REJECTED,
                                inflowReceiptItem.getPurchaseOrderItem().getItemType().getSkuCode(), ageingStartDate);
                        if (qcRejectedPutawayItem == null) {
                            qcRejectedPutawayItem = new PutawayItem(putaway, Type.QC_REJECTED, ageingStartDate, inflowReceiptItem.getPurchaseOrderItem().getItemType(),
                                    inflowReceiptItem.getRejectedQuantity(), 0, PutawayItem.StatusCode.CREATED.name(), DateUtils.getCurrentTime(), DateUtils.getCurrentTime());
                            putaway.getPutawayItems().add(qcRejectedPutawayItem);
                            putawayDao.addPutawayItem(qcRejectedPutawayItem);
                        } else {
                            qcRejectedPutawayItem.setQuantity(qcRejectedPutawayItem.getQuantity() + inflowReceiptItem.getRejectedQuantity());
                        }
                    }

                    inflowReceiptItem.setStatusCode(InflowReceiptItem.StatusCode.COMPLETE.name());
                    addInventoryLedgerEntryAtGrn(inflowReceiptItem, putawayCode);
                    putawayDao.addReceiptItemsToPutaway(inflowReceiptItem.getId(), putaway);
                }
            }
            if (!context.hasErrors()) {
                if (isInflowReceiptComplete(inflowReceipt)) {
                    inflowReceipt.setStatusCode(InflowReceipt.StatusCode.COMPLETE.name());
                    inflowDao.updateInflowReceipt(inflowReceipt);
                }
                response.setSuccessful(true);
                if (ActivityContext.current().isEnable()) {
                    ActivityUtils.appendActivity(inflowReceipt.getCode(), ActivityEntityEnum.PURCHASE_ORDER.getName(), inflowReceipt.getPurchaseOrder().getCode(), Arrays.asList(
                            new String[] { "GRN " + inflowReceipt.getCode() + " for {" + ActivityEntityEnum.PURCHASE_ORDER + ":" + "} added to putaway" + putaway.getCode() }),
                            ActivityTypeEnum.CREATION.name());
                }
            }
        }

        response.setErrors(context.getErrors());
        return response;
    }

    /**
     * Adds inventory ledger when adding inflow receipt item to putaway
     *
     * @param inflowReceiptItem
     */
    private void addInventoryLedgerEntryAtGrn(InflowReceiptItem inflowReceiptItem, String putawayCode) {
        InventoryLedger ledger = new InventoryLedger();
        ledger.setTransactionType(InventoryLedger.TransactionType.GRN);
        ledger.setSkuCode(inflowReceiptItem.getItemType().getSkuCode());
        ledger.setTransactionIdentifier(inflowReceiptItem.getInflowReceipt().getCode());
        ledger.setAdditionalInfo(putawayCode);
        inventoryLedgerService.addInventoryLedgerEntry(ledger, inflowReceiptItem.getQuantity(), InventoryLedger.ChangeType.INCREASE);
    }

    /**
     * @return putaway item from putaway item map (mapping from inventory type to skuCode to ageing start date).
     */
    private PutawayItem getPutawayItem(Map<Type, Map<String, Map<Date, PutawayItem>>> putawayItems, Type inventoryType, String skuCode, Date ageingStartDate) {
        return Optional.ofNullable(putawayItems).map(c -> c.get(inventoryType)).map(c -> c.get(skuCode)).map(c -> c.get(ageingStartDate)).orElse(null);
    }

    private PutawayItem getPutawayItemWithShelfCode(Map<Type, Map<String, Map<String,Map<Date, PutawayItem>>>> putawayItems, Type inventoryType, String skuCode,String shelfCode, Date ageingStartDate) {
        return Optional.ofNullable(putawayItems).map(c -> c.get(inventoryType)).map(c -> c.get(skuCode)).map(c->c.get(shelfCode)).map(c -> c.get(ageingStartDate)).orElse(null);
    }

    /**
     * @return a map of putaway items of the putaway, with mapping from inventory type to sku to ageing to putaway item
     */
    private Map<Type, Map<String, Map<Date, PutawayItem>>> getPutawayItemsMap(Putaway putaway) {
        Map<Type, Map<String, Map<Date, PutawayItem>>> putawayItems = new HashMap<>();
        for (PutawayItem putawayItem : putaway.getPutawayItems()) {
            putawayItems.computeIfAbsent(putawayItem.getType(), k -> new HashMap<>()).computeIfAbsent(putawayItem.getItemType().getSkuCode(), k -> new HashMap<>()).put(
                    putawayItem.getAgeingStartDate(), putawayItem);
        }
        return putawayItems;
    }

    private Map<Type, Map<String, Map<String,Map<Date, PutawayItem>>>> getPutawayItemsMapWithShelfCode(Putaway putaway) {
        Map<Type, Map<String, Map<String,Map<Date, PutawayItem>>>> putawayItems = new HashMap<>();
        for (PutawayItem putawayItem : putaway.getPutawayItems()) {
            String shelfCode = putawayItem.getShelf()==null? null: putawayItem.getShelf().getCode();
            putawayItems.computeIfAbsent(putawayItem.getType(), k -> new HashMap<>()).computeIfAbsent(putawayItem.getItemType().getSkuCode(), k -> new HashMap<>()).
                    computeIfAbsent(shelfCode, k -> new HashMap<>()).put(
                    putawayItem.getAgeingStartDate(), putawayItem);
        }
        return putawayItems;
    }



    @Override
    @Locks({ @Lock(ns = Namespace.PUTAWAY, key = "#{#args[0].putawayCode}", level = Level.FACILITY) })
    @Transactional
    @RollbackOnFailure
    public CreatePutawayListResponse createPutawayList(CreatePutawayListRequest request) {
        CreatePutawayListResponse response = new CreatePutawayListResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            Putaway putaway = getPutawayByCode(request.getPutawayCode());
            if (putaway == null) {
                context.addError(WsResponseCode.INVALID_PUTAWAY_ID, "Invalid putaway code");
            } else if (!Putaway.StatusCode.CREATED.name().equals(putaway.getStatusCode())) {
                context.addError(WsResponseCode.INVALID_PUTAWAY_STATE, "Putaway not in CREATED state");
            } else if (!putaway.getUser().getId().equals(request.getUserId())) {
                context.addError(WsResponseCode.INVALID_USER_ID, "putaway created by another user");
            } else {
                for (PutawayItem putawayItem : putaway.getPutawayItems()) {
                    if(putawayItem.getShelf()==null){
                        Shelf putawayToShelf = null;
                        if (Type.GOOD_INVENTORY.equals(putawayItem.getType())) {
                            putawayToShelf = putawayDao.getLastShelfForPutaway(putawayItem.getItemType().getId());
                            if (putawayToShelf == null) {
                                putawayToShelf = shelfService.getShelfByCode(Shelf.ShelfCode.DEFAULT.name());
                            }
                        } else if (Type.BAD_INVENTORY.equals(putawayItem.getType())
                                || !ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).isWarehouseStorageBinManagement()) {
                            putawayToShelf = shelfService.getShelfByCode(Shelf.ShelfCode.DEFAULT.name());
                        }
                        if (putawayToShelf != null) {
                            putawayItem.setShelf(putawayToShelf);
                        }
                    }
                    putawayItem.setStatusCode(PutawayItem.StatusCode.PENDING.name());
                }
                putaway.setStatusCode(Putaway.StatusCode.PENDING.name());
                putawayDao.updatePutaway(putaway);
                if (!ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).isWarehouseStorageBinManagement()) {
                    CompletePutawayRequest cpRequest = new CompletePutawayRequest();
                    cpRequest.setPutawayCode(putaway.getCode());
                    CompletePutawayResponse cpResponse = completePutaway(cpRequest);
                    if (cpResponse.isSuccessful()) {
                        response.setSuccessful(true);
                    } else {
                        response.setErrors(cpResponse.getErrors());
                    }
                } else {
                    response.setSuccessful(true);
                }
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    /**
     * @param inflowReceipt
     * @return
     */
    private boolean isInflowReceiptComplete(InflowReceipt inflowReceipt) {
        if (InflowReceipt.StatusCode.CREATED.name().equals(inflowReceipt.getStatusCode())) {
            return false;
        } else {
            for (InflowReceiptItem inflowReceiptItem : inflowReceipt.getInflowReceiptItems()) {
                if (!InflowReceiptItem.StatusCode.COMPLETE.name().equals(inflowReceiptItem.getStatusCode())) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * @param putaway
     * @param statusCode
     * @param shelf
     */
    private PutawayItem preparePutawayItem(Putaway putaway, String statusCode, Shelf shelf, ItemType itemType, Type inventoryType, int quantity, Date ageingStartDate) {
        PutawayItem putawayItem = new PutawayItem();
        putawayItem.setPutaway(putaway);
        putawayItem.setQuantity(quantity);
        putawayItem.setStatusCode(statusCode);
        putawayItem.setShelf(shelf);
        putawayItem.setType(inventoryType);
        putawayItem.setCreated(DateUtils.getCurrentTime());
        putawayItem.setUpdated(DateUtils.getCurrentTime());
        putawayItem.setItemType(itemType);
        putawayItem.setAgeingStartDate(ageingStartDate);
        putawayItem = putawayDao.addPutawayItem(putawayItem);
        putaway.getPutawayItems().add(putawayItem);
        return putawayItem;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.uniware.services.inflow.IInflowService#completeInflowPutaway(com.
     * uniware.core.api.inflow.CloseInflowPutawayRequest)
     */
    @Override
    @Locks({ @Lock(ns = Namespace.PUTAWAY, key = "#{#args[0].putawayCode}", level = Level.FACILITY) })
    @Transactional
    public CompletePutawayResponse completePutaway(CompletePutawayRequest request) {
        CompletePutawayResponse response = new CompletePutawayResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            Putaway putaway = getPutawayByCode(request.getPutawayCode());
            TraceabilityLevel traceabilityLevel = ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).getTraceabilityLevel();
            if (putaway == null) {
                context.addError(WsResponseCode.INVALID_PUTAWAY_ID, "Invalid putaway code");
            } else if (!Putaway.StatusCode.PENDING.name().equals(putaway.getStatusCode())) {
                context.addError(WsResponseCode.INVALID_PUTAWAY_STATE, "Putaway not in PENDING state");
            } else if (ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).getInventoryAccuracyLevel() != InventoryAccuracyLevel.ITEM_TYPE) {
                context.addError(WsResponseCode.FEATURE_NOT_SUPPORTED, "This feature is only support with configuration InventoryAccuracyLevel.ITEM_TYPE");
            } else if (ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).isHandheldEnabled()) {
                context.addError(WsResponseCode.WEB_PUTAWAY_BLOCKED, "Web Putaway is not allowed for this facility");
            } else {
                for (PutawayItem putawayItem : putaway.getPutawayItems()) {
                    if (putawayItem.getShelf() == null) {
                        context.addError(WsResponseCode.INFLOW_RECEIPT_SHELFS_ARE_NOT_ASSIGNED_TO_ALL_ITEMS, "Shelfs must be assigned to all items before putaway complete");
                        break;
                    } else if (putawayItem.getShelf().isBlockedForCycleCount()) {
                        context.addError(WsResponseCode.SHELF_BLOCKED_FOR_CYCLE_COUNT, "Cycle Count in progress for one of the shelves");
                    } else if (putawayItem.getShelf().isInactive()) {
                        context.addError(WsResponseCode.INACTIVE_SHELF, "Shelf:[" + putawayItem.getShelf().getCode() + "] is inactive");
                    }
                }
                if (!context.hasErrors()) {
                    if (TraceabilityLevel.ITEM.equals(traceabilityLevel)) {
                        Map<String, Map<Type, Map<Integer, Date>>> skuToInventoryTypeToShelfToAgeing = getMapOfSkuToInventoryTypeToShelfToAgeing(context,
                                putaway.getPutawayItems());
                        if (!context.hasErrors()) {
                            putaway.getPutawayItems().stream().filter(putawayItem -> !PutawayItem.StatusCode.COMPLETE.name().equals(putawayItem.getStatusCode())).forEach(
                                    putawayItem -> {
                                        String skuCode = putawayItem.getItemType().getSkuCode();
                                        inventoryService.addInventory(skuCode, putawayItem.getType(), putawayItem.getAgeingStartDate(), putawayItem.getShelf(),
                                                putawayItem.getQuantity() - putawayItem.getPutawayQuantity());
                                        if (skuToInventoryTypeToShelfToAgeing.get(skuCode).get(putawayItem.getType()).size() == 1) {
                                            putawayDao.updateItemShelf(putaway.getId(), putawayItem.getItemType().getId(), putawayItem.getShelf().getId(), putawayItem.getType());
                                        }
                                        putawayItem.setStatusCode(PutawayItem.StatusCode.COMPLETE.name());
                                        User user = usersService.getUserByUsername(UserContext.current().getUniwareUserName());
                                        putawayItem.setCompletedBy(user);
                                        putawayItem.setPutawayQuantity(putawayItem.getQuantity());
                                        putawayDao.updatePutawayItem(putawayItem);
                                    });
                            for (Item item : getItemsByPutawayId(putaway.getId())) {
                                if (ActivityContext.current().isEnable()) {
                                    ActivityUtils.appendActivity(item.getCode(), ActivityEntityEnum.ITEM.getName(), null,
                                            Collections.singletonList(
                                                    "Item {" + ActivityEntityEnum.ITEM.name() + ":" + item.getCode() + "} moved to good inventory, in: " + putaway.getCode()),
                                            Item.ActivityType.GOOD_INVENTORY.name());
                                }
                            }
                            putawayDao.addItemsToInventory(putaway.getId());
                            putaway.setStatusCode(InflowReceipt.StatusCode.COMPLETE.name());
                            putawayDao.updatePutaway(putaway);
                        }
                    } else {
                        for (PutawayItem putawayItem : putaway.getPutawayItems()) {
                            if (!PutawayItem.StatusCode.COMPLETE.name().equals(putawayItem.getStatusCode())) {
                                inventoryService.addInventory(putawayItem.getItemType(), putawayItem.getType(), putawayItem.getAgeingStartDate(), putawayItem.getShelf(),
                                        putawayItem.getQuantity() - putawayItem.getPutawayQuantity());
                                putawayItem.setStatusCode(PutawayItem.StatusCode.COMPLETE.name());
                                putawayItem.setPutawayQuantity(putawayItem.getQuantity());
                                putawayDao.updatePutawayItem(putawayItem);
                            }
                        }
                        if (ActivityContext.current().isEnable()) {
                            ActivityUtils.appendActivity(putaway.getCode(), ActivityEntityEnum.PUTWAWAY.getName(), null,
                                    Collections.singletonList("Putaway {" + ActivityEntityEnum.PUTWAWAY.getName() + "} complete."), Putaway.ActivityType.COMPLETE.name());
                        }
                        putawayDao.addItemsToInventory(putaway.getId());
                        putaway.setStatusCode(InflowReceipt.StatusCode.COMPLETE.name());
                        putawayDao.updatePutaway(putaway);

                    }
                }
            }
        }
        response.setSuccessful(!context.hasErrors());
        response.setErrors(context.getErrors());
        return response;
    }

    private Map<String, Map<Type, Map<Integer, Date>>> getMapOfSkuToInventoryTypeToShelfToAgeing(ValidationContext context, Set<PutawayItem> putawayItems) {
        Map<String, Map<Type, Map<Integer, Date>>> skuToInventoryTypeToShelfToAgeing = new HashMap<>();
        putawayItems.stream().filter(putawayItem -> !PutawayItem.StatusCode.COMPLETE.name().equals(putawayItem.getStatusCode())).forEach(putawayItem -> {
            String skuCode = putawayItem.getItemType().getSkuCode();
            int shelfId = putawayItem.getShelf().getId();
            Map<Integer, Date> shelfToAgeing = skuToInventoryTypeToShelfToAgeing.computeIfAbsent(skuCode, k -> new HashMap<>()).computeIfAbsent(putawayItem.getType(),
                    k -> new HashMap<>());
            if (Type.GOOD_INVENTORY.equals(putawayItem.getType())
                    && !ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).isAllowMultipleAgeingDateOnShelf()) {
                if (!shelfToAgeing.containsKey(shelfId)) {
                    if (inventoryService.isItemPresentWithDifferentAgeingOnShelf(skuCode, putawayItem.getAgeingStartDate(), putawayItem.getType(), shelfId)) {
                        context.addError(WsResponseCode.INVALID_PUTAWAY_STATE,
                                "Items of same sku with different ageing are already present Sku : " + skuCode + " Shelf : " + putawayItem.getShelf().getCode());
                    } else {
                        shelfToAgeing.put(shelfId, putawayItem.getAgeingStartDate());
                    }
                } else if (!shelfToAgeing.get(shelfId).equals(putawayItem.getAgeingStartDate())) {
                    context.addError(WsResponseCode.INVALID_PUTAWAY_STATE, "Items of same SKU with different ageing start dates cannot be placed together. SKU:" + skuCode);
                }
            } else {
                shelfToAgeing.put(shelfId, putawayItem.getAgeingStartDate());
            }
        });
        return skuToInventoryTypeToShelfToAgeing;
    }

    @Override
    @Transactional
    @RollbackOnFailure
    @LogActivity
    @Locks({ @Lock(ns = Namespace.PUTAWAY, key = "#{#args[0].putawayCode}", level = Level.FACILITY) })
    public CompletePutawayItemsResponse completePutawayItems(CompletePutawayItemsRequest request) {
        CompletePutawayItemsResponse response = new CompletePutawayItemsResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            TraceabilityLevel traceabilityLevel = ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).getTraceabilityLevel();
            request.getShelfItemsList().stream().filter(k -> !context.hasErrors()).forEach(shelfItems -> {
                CompleteItemPutawayRequest completeItemPutawayRequest = new CompleteItemPutawayRequest();
                completeItemPutawayRequest.setPutawayCode(request.getPutawayCode());
                completeItemPutawayRequest.setShelfCode(shelfItems.getShelfCode());
                if (TraceabilityLevel.ITEM.equals(traceabilityLevel)) {
                    if (CollectionUtils.isEmpty(shelfItems.getItemCodes())) {
                        context.addError(WsResponseCode.ACTION_APPLICABLE_FOR_TRACEABLE_ITEMS, "Item Codes required for ITEM level traceability");
                    } else {
                        completeItemPutawayRequest.setItemCodes(shelfItems.getItemCodes());
                    }
                } else if (TraceabilityLevel.ITEM_SKU.equals(traceabilityLevel)) {
                    if (CollectionUtils.isEmpty(shelfItems.getPutawayItems())) {
                        context.addError(WsResponseCode.ACTION_NOT_APPLICABLE_FOR_TRACEABLE_ITEMS, "Putaway Items required for ITEM_SKU level traceability");
                    } else if (shelfItems.getPutawayItems().stream().anyMatch(items -> StringUtils.isBlank(items.getSkuCode()))) {
                        context.addError(WsResponseCode.ACTION_NOT_APPLICABLE_FOR_TRACEABLE_ITEMS, "Putaway Items required for ITEM_SKU level traceability");
                    } else {
                        completeItemPutawayRequest.setPutawayItems(shelfItems.getPutawayItems());
                    }
                }
                if (!context.hasErrors()) {
                    CompleteItemPutawayResponse completeItemPutawayResponse = completeItemPutaway(completeItemPutawayRequest);
                    if (!completeItemPutawayResponse.isSuccessful()) {
                        context.addErrors(completeItemPutawayResponse.getErrors());
                    }
                }
            });
        }
        if (!context.hasErrors()) {
            response.setSuccessful(true);
        } else {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    @Override
    @Transactional
    public CompleteItemPutawayResponse completeItemPutaway(CompleteItemPutawayRequest request) {
        CompleteItemPutawayResponse response = new CompleteItemPutawayResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            Shelf shelf = shelfService.getShelfByCode(request.getShelfCode());
            if (shelf == null) {
                context.addError(WsResponseCode.INVALID_SHELF_CODE, "Invalid shelf code");
            } else {
                Putaway putaway = getPutawayByCode(request.getPutawayCode());
                TraceabilityLevel traceabilityLevel = ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).getTraceabilityLevel();
                if (putaway == null) {
                    context.addError(WsResponseCode.INVALID_PUTAWAY_ID, "Invalid Putaway code");
                } else if (!Putaway.StatusCode.PENDING.name().equals(putaway.getStatusCode())) {
                    context.addError(WsResponseCode.INVALID_PUTAWAY_STATE, "Putaway not in PENDING state");
                } else if (TraceabilityLevel.ITEM.equals(traceabilityLevel) && CollectionUtils.isEmpty(request.getItemCodes())) {
                    context.addError(WsResponseCode.INVALID_FACILITY_CONFIGURATION, "Item Codes required for ITEM level traceability");
                } else if (TraceabilityLevel.ITEM_SKU.equals(traceabilityLevel) && CollectionUtils.isEmpty(request.getPutawayItems())) {
                    context.addError(WsResponseCode.INVALID_FACILITY_CONFIGURATION, "Putaway Items required for ITEM_SKU level traceability");
                } else {
                    Map<Type, Map<String, Map<Date, List<PutawayItem>>>> typeToSkuCodeToAgeingDateToPutawayItemsMap = new HashMap<>();
                    putaway.getPutawayItems().stream().filter(putawayItem -> !PutawayItem.StatusCode.COMPLETE.name().equals(putawayItem.getStatusCode())).forEach(putawayItem -> {
                        // We need to add putaway quantity for putaway item
                        // which has same shelf
                        List<PutawayItem> putawayItems = typeToSkuCodeToAgeingDateToPutawayItemsMap.computeIfAbsent(putawayItem.getType(), k -> new HashMap<>()).computeIfAbsent(
                                putawayItem.getItemType().getSkuCode(), k -> new HashMap<>()).computeIfAbsent(putawayItem.getAgeingStartDate(), k -> new ArrayList<>());
                        if (putawayItem.getShelf() != null && putawayItem.getShelf().getId().equals(shelf.getId())) {
                            putawayItems.add(0, putawayItem);
                        } else {
                            putawayItems.add(putawayItem);
                        }
                    });

                    if (TraceabilityLevel.ITEM.equals(traceabilityLevel)) {
                        HashMap<String, Date> ageingStartDates = new HashMap<>();
                        List<Item> items = new ArrayList<>(request.getItemCodes().size());
                        Set<String> itemCodes = new HashSet<>(request.getItemCodes().size());
                        itemCodes.addAll(request.getItemCodes());
                        itemCodes.stream().filter(itemCode -> !context.hasErrors()).forEach(itemCode -> {
                            Item item = inventoryService.getItemByCode(itemCode);
                            if (item == null) {
                                context.addError(WsResponseCode.INVALID_ITEM_CODE, itemCode + " Invalid item code");
                            } else if (isItemValidForPutaway(putaway.getCode(), item, context)) {
                                items.add(item);
                                if (Type.GOOD_INVENTORY.equals(item.getInventoryType())
                                        && !ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).isAllowMultipleAgeingDateOnShelf()) {
                                    String skuCode = item.getItemType().getSkuCode();
                                    if (!ageingStartDates.containsKey(skuCode)) {
                                        if (!inventoryService.isItemPresentWithDifferentAgeingOnShelf(skuCode, item.getAgeingStartDate(), item.getInventoryType(), shelf.getId())) {
                                            ageingStartDates.put(skuCode, item.getAgeingStartDate());
                                        } else {
                                            context.addError(WsResponseCode.INVALID_PUTAWAY_STATE,
                                                    "Items of same sku with different ageing are already present Sku : " + skuCode + " Shelf : " + shelf.getCode());
                                        }
                                    } else if (!ageingStartDates.get(skuCode).equals(item.getAgeingStartDate())) {
                                        context.addError(WsResponseCode.INVALID_PUTAWAY_STATE,
                                                "Items of same SKU with different ageing start dates cannot be placed together. SKU:" + skuCode);
                                    }
                                }
                            }
                        });

                        if (!context.hasErrors()) {
                            Map<Type, Map<String, Map<Date, List<Item>>>> typeToSkuToAgeingToItems = generateItemMap(items);
                            typeToSkuToAgeingToItems.forEach(
                                    (inventoryType, skuMap) -> skuMap.forEach((sku, ageingDateMap) -> ageingDateMap.forEach((ageingStartDate, itemList) -> {
                                        itemList.forEach(item -> {
                                            item.setShelf(shelf);
                                            item.setStatusCode(inventoryType.name());
                                            if (ActivityContext.current().isEnable()) {
                                                ActivityUtils.appendActivity(item.getCode(), ActivityEntityEnum.ITEM.getName(), null,
                                                        Collections.singletonList(
                                                                "Item {" + ActivityEntityEnum.ITEM.name() + ":" + item.getCode() + "} moved to status: " + item.getStatusCode()),
                                                        Item.ActivityType.STATUS_CHANGED.name());
                                            }
                                        });
                                        inventoryService.addInventory(sku, inventoryType, ageingStartDate, shelf, itemList.size());
                                        List<PutawayItem> putawayItems = Optional.of(typeToSkuCodeToAgeingDateToPutawayItemsMap).map(c -> c.get(inventoryType)).map(
                                                c -> c.get(sku)).map(c -> c.get(ageingStartDate)).orElse(null);
                                        int putawayQuantity = itemList.size();
                                        putawayQuantity = doCompleteItemPutaway(context, shelf, putawayItems, putawayQuantity);
                                        if (putawayQuantity > 0) {
                                            throw new IllegalStateException("putaway quantity exceeds putaway pending, putaway:" + putaway.getId() + ", Item codes:"
                                                    + StringUtils.join(',', request.getItemCodes()));
                                        }
                                    })));
                        }
                    } else if (TraceabilityLevel.ITEM_SKU.equals(traceabilityLevel)) {
                        Map<Type, Map<String, Integer>> typeToItemTypeToQuantity = new HashMap<>();
                        request.getPutawayItems().stream().filter(k -> !context.hasErrors()).forEach(wsPutawayItem -> {
                            ItemType itemType = catalogService.getItemTypeBySkuCode(wsPutawayItem.getSkuCode());
                            if (itemType == null) {
                                context.addError(WsResponseCode.INVALID_ITEM_SKU_CODE, "Invalid sku code : " + wsPutawayItem.getSkuCode());
                            } else {
                                typeToItemTypeToQuantity.computeIfAbsent(Type.valueOf(wsPutawayItem.getType()), k -> new HashMap<>()).compute(itemType.getSkuCode(),
                                        (itemTypeId, quantity) -> quantity == null ? wsPutawayItem.getQuantity() : quantity + wsPutawayItem.getQuantity());
                            }
                        });

                        if (!context.hasErrors()) {
                            typeToItemTypeToQuantity.forEach((type, itemTypeToQuantity) -> {
                                itemTypeToQuantity.forEach((itemTypeId, quantity) -> {
                                    List<PutawayItem> putawayItems = new ArrayList<>();
                                    if (typeToSkuCodeToAgeingDateToPutawayItemsMap.containsKey(type)) {
                                        typeToSkuCodeToAgeingDateToPutawayItemsMap.get(type).forEach(
                                                (sku, ageingMap) -> ageingMap.forEach((ageing, putawayItem) -> putawayItems.addAll(putawayItem)));
                                    }

                                    if (putawayItems.size() > 0) {
                                        inventoryService.addInventory(putawayItems.get(0).getItemType(), type, putawayItems.get(0).getAgeingStartDate(), shelf, quantity);
                                        quantity = doCompleteItemPutaway(context, shelf, putawayItems, quantity);
                                        if (quantity > 0) {
                                            throw new IllegalStateException("putaway quantity exceeds putaway pending, putaway:" + putaway.getId() + ", SKU:"
                                                    + putawayItems.get(0).getItemType().getSkuCode());
                                        }
                                    }
                                });
                            });
                        }
                    }
                    if (isPutawayComplete(putaway)) {
                        putaway.setStatusCode(Putaway.StatusCode.COMPLETE.name());
                    }
                }
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        } else {
            response.setSuccessful(true);
        }
        return response;

    }

    private Map<Type, Map<String, Map<Date, List<Item>>>> generateItemMap(List<Item> items) {
        Map<Type, Map<String, Map<Date, List<Item>>>> typeToSkuToAgeingToItems = new HashMap<>();
        items.forEach(item -> typeToSkuToAgeingToItems.computeIfAbsent(item.getInventoryType(), k -> new HashMap<>()).computeIfAbsent(item.getItemType().getSkuCode(),
                k -> new HashMap<>()).computeIfAbsent(item.getAgeingStartDate(), k -> new ArrayList<>()).add(item));
        return typeToSkuToAgeingToItems;
    }

    private int doCompleteItemPutaway(ValidationContext context, Shelf shelf, List<PutawayItem> putawayItems, int putawayQuantity) {
        int index = 0;
        User user = usersService.getUserByUsername(UserContext.current().getUniwareUserName());
        if (putawayItems != null) {
            while (putawayQuantity > 0 && index < putawayItems.size()) {
                PutawayItem putawayItem = putawayItems.get(index);
                int putawayPending = putawayItem.getQuantity() - putawayItem.getPutawayQuantity();
                // This is the case where putaway item needs to be splitted, one complete, other pending
                if (putawayPending > putawayQuantity) {
                    SplitPutawayItemRequest splitPutawayItemRequest = new SplitPutawayItemRequest();
                    splitPutawayItemRequest.setPutawayItemId(putawayItem.getId());
                    PutawayItemToSplit completedPutawayItemPart = new PutawayItemToSplit();
                    completedPutawayItemPart.setShelfCode(shelf.getCode());
                    completedPutawayItemPart.setQuantity(putawayItem.getPutawayQuantity() + putawayQuantity);

                    PutawayItemToSplit pendingPutawayItemPart = new PutawayItemToSplit();
                    if (putawayItem.getShelf() != null) {
                        pendingPutawayItemPart.setShelfCode(putawayItem.getShelf().getCode());
                    } else {
                        // just a place holder
                        pendingPutawayItemPart.setShelfCode(shelf.getCode());
                    }
                    pendingPutawayItemPart.setQuantity(putawayItem.getQuantity() - completedPutawayItemPart.getQuantity());

                    List<PutawayItemToSplit> putawayItemToSplits = new ArrayList<>();
                    putawayItemToSplits.add(completedPutawayItemPart);
                    putawayItemToSplits.add(pendingPutawayItemPart);
                    splitPutawayItemRequest.setPutawayItems(putawayItemToSplits);
                    SplitPutawayItemResponse splitPutawayItemResponse = splitPutawayItem(splitPutawayItemRequest);
                    if (splitPutawayItemResponse.isSuccessful()) {
                        PutawayItemDTO completedPutawayItemDTO = splitPutawayItemResponse.getPutawayItemDTOs().get(0);
                        PutawayItem completedPutawayItem = putawayDao.getPutawayItemById(completedPutawayItemDTO.getId());
                        completedPutawayItem.setPutawayQuantity(completedPutawayItem.getQuantity());
                        completedPutawayItem.setStatusCode(PutawayItem.StatusCode.COMPLETE.name());
                        completedPutawayItem.setCompletedBy(user);
                        PutawayItemDTO pendingPutawayItemDTO = splitPutawayItemResponse.getPutawayItemDTOs().get(1);
                        PutawayItem pendingPutawayItem = putawayDao.getPutawayItemById(pendingPutawayItemDTO.getId());
                        pendingPutawayItem.setPutawayQuantity(0);
                    } else {
                        context.addErrors(splitPutawayItemResponse.getErrors());
                        break;
                    }
                    putawayQuantity = 0;
                } else {
                    putawayItem.setShelf(shelf);
                    putawayItem.setCompletedBy(user);
                    putawayItem.setPutawayQuantity(putawayItem.getQuantity());
                    putawayItem.setStatusCode(PutawayItem.StatusCode.COMPLETE.name());
                    putawayQuantity -= putawayPending;
                    index++;
                }
            }
        }
        return putawayQuantity;
    }

    private boolean isPutawayComplete(Putaway putaway) {
        for (PutawayItem putawayItem : putaway.getPutawayItems()) {
            if (!PutawayItem.StatusCode.COMPLETE.name().equals(putawayItem.getStatusCode())) {
                return false;
            }
        }
        return true;
    }

    @Override
    @Locks({ @Lock(ns = Namespace.PUTAWAY, key = "#{#args[0].putawayCode}", level = Level.FACILITY) })
    public AddReturnsSaleOrderItemsToPutawayResponse addReturnsSaleOrderItemsToPutaway(AddReturnsSaleOrderItemsToPutawayRequest request) {
        AddReturnsSaleOrderItemsToPutawayResponse response = new AddReturnsSaleOrderItemsToPutawayResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            ShippingPackage shippingPackage = shippingService.getShippingPackageByCode(request.getShipmentCode());
            if (shippingPackage == null) {
                context.addError(WsResponseCode.INVALID_SHIPPING_PACKAGE_CODE, "Invalid Shipping Package Code");
            } else {
                Putaway putaway = getPutawayByCode(request.getPutawayCode());
                if (putaway == null) {
                    context.addError(WsResponseCode.INVALID_PUTAWAY_ID, "Invalid putaway code");
                } else if (!Putaway.StatusCode.CREATED.name().equals(putaway.getStatusCode())) {
                    context.addError(WsResponseCode.INVALID_PUTAWAY_STATE, "Items can be added only in CREATED state");
                } else if (!putaway.getUser().getId().equals(request.getUserId())) {
                    context.addError(WsResponseCode.INVALID_USER_ID, "putaway created by another user");
                } else {
                    addReturnsSaleOrderItemsToPutaway(request, response, context, shippingPackage, putaway.getId());
                }
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    /**
     *
     */
    @Locks({ @Lock(ns = Namespace.SALE_ORDER, key = "#{#args[3].saleOrder.code}") })
    @Transactional
    @LogActivity
    @RollbackOnFailure
    public AddReturnsSaleOrderItemsToPutawayResponse addReturnsSaleOrderItemsToPutaway(AddReturnsSaleOrderItemsToPutawayRequest request,
            AddReturnsSaleOrderItemsToPutawayResponse response, ValidationContext context, ShippingPackage shippingPackage, Integer putawayId) {
        Putaway putaway = putawayDao.getPutawayById(putawayId);
        SaleOrder saleOrder = saleOrderService.getSaleOrderForUpdate(shippingPackage.getSaleOrder().getId());
        shippingPackage = shippingService.getShippingPackageById(shippingPackage.getId(), true);
        TraceabilityLevel traceabilityLevel = ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).getTraceabilityLevel();
        if (!ShippingPackage.StatusCode.RETURN_ACKNOWLEDGED.name().equals(shippingPackage.getStatusCode())) {
            context.addError(WsResponseCode.INVALID_STATE, "Invalid Shipping Package State. putaway can be done for packages added to return manifest");
        } else {
            Map<Type, Map<String, Map<Date, List<SaleOrderItem>>>> typeToSkuCodeToAgeingToSaleOrderItems = new HashMap<>();
            Map<String, List<SaleOrderItem>> typeToSaleOrderItemDetails = new HashMap<>();
            Map<String, SaleOrderItem> saleOrderItemsMap = saleOrder.getSaleOrderItemsMap();
            Map<String, SaleOrderItemDTO> saleOrderItemDTOMap = new HashMap<>();
            for (SaleOrderItemDTO saleOrderItemDTO : request.getSaleOrderItems()) {
                saleOrderItemDTOMap.put(saleOrderItemDTO.getCode(), saleOrderItemDTO);
                SaleOrderItem saleOrderItem = saleOrderItemsMap.get(saleOrderItemDTO.getCode());
                if (saleOrderItem == null) {
                    context.addError(WsResponseCode.INVALID_SALE_ORDER_ITEM_ID, "Invalid Sale Order Item Id");
                } else if (StringUtils.equalsAny(saleOrderItemDTO.getStatus(), Item.StatusCode.LIQUIDATED.name())) {
                    if (saleOrderItem.getItem() != null) {
                        saleOrderItem.getItem().setStatusCode(Item.StatusCode.LIQUIDATED.name());
                        if (ActivityContext.current().isEnable()) {
                            ActivityUtils.appendActivity(saleOrderItem.getItem().getCode(), ActivityEntityEnum.ITEM.getName(), null,
                                    Collections.singletonList("Item {" + ActivityEntityEnum.ITEM.name() + ":" + saleOrderItem.getItem().getCode() + "} moved to LIQUIDATED"),
                                    Item.ActivityType.LIQUIDATED.name());
                        }
                    }
                } else if (StringUtils.equalsAny(saleOrderItemDTO.getStatus(), Item.StatusCode.GOOD_INVENTORY.name(), Item.StatusCode.BAD_INVENTORY.name())) {
                    if (TraceabilityLevel.ITEM.equals(traceabilityLevel)) {
                        Item item = saleOrderItem.getItem();
                        ItemType itemType = catalogService.getItemTypeBySkuCode(item.getItemType().getSkuCode());
                        boolean isItemAboutToExpire = isItemAboutToExpire(itemType, item.getManufacturingDate(), item.getCode());
                        if (item.getManufacturingDate() == null && catalogService.isItemTypeExpirable(itemType)) {
                            context.addError(WsResponseCode.INVALID_ITEM_CODE,
                                    "Cant add expirable item to putaway before adding manufacturing date, Item : " + item.getCode() + " SKU : " + item.getItemType().getSkuCode());
                        } else if (isItemAboutToExpire && !saleOrderItemDTO.isExpiredReturn()) {
                            context.addError(WsResponseCode.INVALID_REQUEST, "Item is about to expire, mark it as bad");
                        } else if (isItemAboutToExpire) {
                            item.setInventoryType(Type.BAD_INVENTORY);
                            item.setBadInventoryMarkedTime(DateUtils.getCurrentTime());
                            int shelfLife = catalogService.getShelfLifeForItemType(itemType);
                            if (!DateUtils.addDaysToDate(item.getManufacturingDate(), shelfLife).after(DateUtils.getCurrentTime())) {
                                item.setRejectionReason(RejectionReason.EXPIRED_ITEM.name());
                            } else {
                                item.setRejectionReason(RejectionReason.ABOUT_TO_EXPIRE.name());
                            }
                            LOG.info("marking status for expired returns as bad inventory:" + saleOrderItemDTO.getCode());
                            saleOrderItemDTO.setStatus(Item.StatusCode.BAD_INVENTORY.name());
                        }
                    }
                    typeToSkuCodeToAgeingToSaleOrderItems.computeIfAbsent(Type.valueOf(saleOrderItemDTO.getStatus()), k -> new HashMap<>()).computeIfAbsent(
                            saleOrderItem.getItemType().getSkuCode(), k -> new HashMap<>()).computeIfAbsent(
                                    saleOrderItem.getItem() != null ? saleOrderItem.getItem().getAgeingStartDate() : Constants.ITEM_SKU_OR_NONE_TRACEABILITY_AGEING_DATE,
                                    k -> new ArrayList<>()).add(saleOrderItem);
                }
            }

            if (!context.hasErrors()) {
                Map<Type, Map<String, Map<Date, PutawayItem>>> typeToSkuCodeToAgeingDateToPutawayItemMap = getPutawayItemsMap(putaway);
                typeToSkuCodeToAgeingToSaleOrderItems.forEach((type, skuMap) -> skuMap.forEach((skuCode, ageingDateMap) -> ageingDateMap.forEach((ageingDate, saleOrderItems) -> {
                    PutawayItem putawayItem = getPutawayItem(typeToSkuCodeToAgeingDateToPutawayItemMap, type, skuCode, ageingDate);
                    if (putawayItem == null) {
                        putawayItem = new PutawayItem(putaway, type, ageingDate, saleOrderItems.get(0).getItemType(), saleOrderItems.size(), 0,
                                PutawayItem.StatusCode.CREATED.name(), DateUtils.getCurrentTime(), DateUtils.getCurrentTime());
                        putawayItem = putawayDao.addPutawayItem(putawayItem);
                        putaway.getPutawayItems().add(putawayItem);
                    } else {
                        putawayItem.setQuantity(putawayItem.getQuantity() + saleOrderItems.size());
                    }

                    for (SaleOrderItem saleOrderItem : saleOrderItems) {
                        Item item = saleOrderItem.getItem();
                        if (item != null) {
                            item.setPutaway(putaway);
                            item.setStatusCode(Item.StatusCode.PUTAWAY_PENDING.name());
                            item.setInventoryType(type);
                            if (Type.BAD_INVENTORY == type && saleOrderItemDTOMap.containsKey(saleOrderItem.getCode())) {
                                item.setBadInventoryMarkedTime(DateUtils.getCurrentTime());
                            }
                        }
                        addInventoryLedgerEntryOnReturn(saleOrderItem, putaway.getType(), putaway.getCode());
                    }
                    response.getPutawayItems().add(preparePutawayItemDTO(putawayItem));

                })));
                putawayDao.updatePutaway(putaway);

                if (shippingPackage.getReshipment() != null) {
                    returnsService.completeReshipment(shippingPackage.getReshipment().getId());
                }
                if (!context.hasErrors() && typeToSaleOrderItemDetails.size() > 0) {
                    if (StringUtils.isNotBlank(CacheManager.getInstance().getCache(ChannelCache.class).getScriptName(shippingPackage.getSaleOrder().getChannel().getCode(),
                            Source.MARK_SALE_ORDER_RETURNED_SCRIPT_NAME))) {
                        ScraperScript markSaleOrderReturnedScript = CacheManager.getInstance().getCache(ChannelCache.class).getScriptByName(
                                shippingPackage.getSaleOrder().getChannel().getCode(), Source.MARK_SALE_ORDER_RETURNED_SCRIPT_NAME);
                        if (markSaleOrderReturnedScript != null) {
                            markSaleOrderReturnedOnChannel(context, shippingPackage.getSaleOrder().getChannel(), typeToSaleOrderItemDetails, markSaleOrderReturnedScript);
                        }
                    }
                }
                // prepare credit note for returned shipment
                if (!context.hasErrors() && taxService.isGSTEnabled()) {
                    CreateShippingPackageReverseInvoiceRequest createReverseInvoiceRequest = new CreateShippingPackageReverseInvoiceRequest();
                    createReverseInvoiceRequest.setShippingPackageCode(shippingPackage.getCode());
                    createReverseInvoiceRequest.setUserId(request.getUserId());
                    CreateShippingPackageReverseInvoiceResponse createReverseInvoiceResponse = shippingInvoiceService.createShippingPackageReverseInvoice(
                            createReverseInvoiceRequest);
                    if (context.hasErrors()) {
                        context.addErrors(createReverseInvoiceResponse.getErrors());
                    } else {
                        shippingPackage.setReturnInvoice(invoiceService.getInvoiceByCode(createReverseInvoiceResponse.getInvoiceCode()));
                    }
                }
                if (!context.hasErrors()) {
                    shippingPackage.setStatusCode(ShippingPackage.StatusCode.RETURNED.name());
                    shippingService.updateShippingPackage(shippingPackage);
                }
            }
        }
        if (!context.hasErrors()) {
            response.setSuccessful(true);
        } else {
            response.addErrors(context.getErrors());
            response.addWarnings(context.getWarnings());
        }
        return response;
    }

    /**
     * @param itemType
     * @param manufacturingDate
     * @param itemCode
     * @return
     */
    private boolean isItemAboutToExpire(ItemType itemType, Date manufacturingDate, String itemCode) {
        if (catalogService.isItemTypeExpirable(itemType) && manufacturingDate != null) {
            int shelfLife = catalogService.getShelfLifeForItemType(itemType);
            if (!DateUtils.addDaysToDate(manufacturingDate, shelfLife - (itemType.getCategory().getReturnExpiryTolerance() * shelfLife / 100)).after(DateUtils.getCurrentTime())) {
                LOG.info("Manufacturing Date :" + manufacturingDate + ",Shelf life: " + shelfLife + ",Return expiry tolerance:" + itemType.getCategory().getReturnExpiryTolerance()
                        + ":Expired item for sale order:" + itemCode);
                return true;
            }
        }
        return false;
    }

    @Override
    public void markSaleOrderReturnedOnChannel(ValidationContext context, Channel channel, Map<String, List<SaleOrderItem>> typeTosaleOrderItemDetails,
            ScraperScript markSaleOrderReturnedScript) {
        ScriptExecutionContext seContext = ScriptExecutionContext.current();
        for (ChannelConnector channelConnector : channel.getChannelConnectors()) {
            for (ChannelConnectorParameter channelConnectorParameter : channelConnector.getChannelConnectorParameters()) {
                seContext.addVariable(channelConnectorParameter.getName(), channelConnectorParameter.getValue());
            }
        }
        for (ChannelConfigurationParameter channelConfigurationParameter : channel.getChannelConfigurationParameters()) {
            seContext.addVariable(channelConfigurationParameter.getSourceConfigurationParameterName(), channelConfigurationParameter.getValue());
        }
        seContext.setScriptProvider(new IScriptProvider() {

            @Override
            public ScraperScript getScript(String scriptName) {
                return CacheManager.getInstance().getCache(ScriptVersionedCache.class).getScriptByName(scriptName);
            }
        });
        seContext.addVariable("typeTosaleOrderItemDetails", typeTosaleOrderItemDetails);
        seContext.addVariable("channel", channel);
        try {
            seContext.setTraceLoggingEnabled(UserContext.current().isTraceLoggingEnabled());
            markSaleOrderReturnedScript.execute();
        } catch (Exception e) {
            LOG.error("Unable to mark order returned on channel", e);
            context.addError(WsResponseCode.UNABLE_TO_MARK_RETURNED_ON_CHANNEL, "Unable to mark returned on channel");
        } finally {
            ScriptExecutionContext.destroy();
        }
    }

    private PutawayItemDTO preparePutawayItemDTO(PutawayItem putawayItem) {
        PutawayItemDTO putawayItemDTO = new PutawayItemDTO();
        if (putawayItem.getShelf() != null) {
            putawayItemDTO.setShelfCode(putawayItem.getShelf().getCode());
        } else {
            Shelf putawayToShelf = putawayDao.getLastShelfForPutaway(putawayItem.getItemType().getId());
            putawayItemDTO.setShelfCode(putawayToShelf != null ? putawayToShelf.getCode() : null);
        }
        putawayItemDTO.setId(putawayItem.getId());
        ItemType itemType = putawayItem.getItemType();
        putawayItemDTO.setItemTypeName(itemType.getName());
        putawayItemDTO.setItemTypeSkuCode(itemType.getSkuCode());
        putawayItemDTO.setInventoryType(putawayItem.getType().name());
        putawayItemDTO.setQuantity(putawayItem.getQuantity());
        putawayItemDTO.setPutawayQuantity(putawayItem.getPutawayQuantity());
        putawayItemDTO.setStatusCode(putawayItem.getStatusCode());
        return putawayItemDTO;
    }

    private List<PutawayItemDTO> preparePutawayItemDTOList(Set<PutawayItem> putawayItems) {
        List<PutawayItemDTO> putawayItemDTOList = new ArrayList<>(putawayItems.size());
        Map<Integer, String> itemTypeToShelfSuggestionCache = new HashMap<>();
        for (PutawayItem putawayItem : putawayItems) {
            PutawayItemDTO putawayItemDTO = new PutawayItemDTO();
            if (putawayItem.getShelf() != null) {
                putawayItemDTO.setShelfCode(putawayItem.getShelf().getCode());
            } else {
                String putawayToShelfCode = null;
                if (itemTypeToShelfSuggestionCache.containsKey(putawayItem.getItemType().getId())) {
                    putawayToShelfCode = itemTypeToShelfSuggestionCache.get(putawayItem.getItemType().getId());
                } else {
                    Shelf putawayToShelf = putawayDao.getLastShelfForPutaway(putawayItem.getItemType().getId());
                    putawayToShelfCode = putawayToShelf == null ? null : putawayToShelf.getCode();
                    itemTypeToShelfSuggestionCache.put(putawayItem.getItemType().getId(), putawayToShelfCode);
                }
                putawayItemDTO.setShelfCode(putawayToShelfCode);
            }
            putawayItemDTO.setId(putawayItem.getId());
            ItemType itemType = putawayItem.getItemType();
            putawayItemDTO.setItemTypeName(itemType.getName());
            putawayItemDTO.setItemTypeSkuCode(itemType.getSkuCode());
            putawayItemDTO.setInventoryType(putawayItem.getType().name());
            putawayItemDTO.setQuantity(putawayItem.getQuantity());
            putawayItemDTO.setPutawayQuantity(putawayItem.getPutawayQuantity());
            putawayItemDTO.setStatusCode(putawayItem.getStatusCode());
            putawayItemDTOList.add(putawayItemDTO);
        }
        return putawayItemDTOList;
    }

    /**
     * Adds list of saleOrderItems to putaway. This is used when {@link SaleOrderItem} is cancelled after it's
     * {@link ShippingPackage} was either in {@link ShippingPackage.StatusCode#PACKED} or in
     * {@link ShippingPackage.StatusCode#PICKING}.
     *
     * @param request
     * @return
     */
    @Override
    @Locks({ @Lock(ns = Namespace.PUTAWAY, key = "#{#args[0].putawayCode}", level = Level.FACILITY) })
    public AddCancelledSaleOrderItemsToPutawayResponse addCancelledSaleOrderItemsToPutaway(AddCancelledSaleOrderItemsToPutawayRequest request) {
        AddCancelledSaleOrderItemsToPutawayResponse response = new AddCancelledSaleOrderItemsToPutawayResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            ShippingPackage shippingPackage = shippingService.getShippingPackageByCode(request.getShipmentCode());
            if (shippingPackage == null) {
                context.addError(WsResponseCode.INVALID_SHIPPING_PACKAGE_CODE);
            } else {
                Putaway putaway = getPutawayByCode(request.getPutawayCode());
                if (putaway == null) {
                    context.addError(WsResponseCode.INVALID_PUTAWAY_ID, "Invalid putaway id");
                } else if (!Putaway.StatusCode.CREATED.name().equals(putaway.getStatusCode())) {
                    context.addError(WsResponseCode.INVALID_PUTAWAY_STATE, "Items can be added only in CREATED state");
                } else if (!putaway.getUser().getId().equals(request.getUserId())) {
                    context.addError(WsResponseCode.INVALID_USER_ID, "putaway created by another user");
                }
                if (!context.hasErrors()) {
                    doAddCancelledSaleOrderItemsToPutaway(request, response, context, shippingPackage, putaway.getId());
                }
            }
        }
        response.addErrors(context.getErrors());
        if (!response.hasErrors()) {
            response.setSuccessful(true);
        }
        return response;
    }

    /**
     * @param response
     * @param context
     * @param shippingPackage
     * @param putawayId
     */
    @Locks({ @Lock(ns = Namespace.SALE_ORDER, key = "#{#args[3].saleOrder.code}") })
    @Transactional
    public void doAddCancelledSaleOrderItemsToPutaway(AddCancelledSaleOrderItemsToPutawayRequest request, AddCancelledSaleOrderItemsToPutawayResponse response,
            ValidationContext context, ShippingPackage shippingPackage, Integer putawayId) {
        Putaway putaway = putawayDao.getPutawayById(putawayId);
        shippingPackage = shippingService.getShippingPackageById(shippingPackage.getId(), true);
        TraceabilityLevel traceabilityLevel = ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).getTraceabilityLevel();
        if (!isShippingPackageInValidStateForCancelledPutaway(shippingPackage, traceabilityLevel)) {
            if (TraceabilityLevel.ITEM.equals(traceabilityLevel)) {
                context.addError(WsResponseCode.INVALID_STATE,
                        "Invalid Shipping Package State. 'PACKED or READY TO SHIP or CUSTOMIZATION COMPLETE or PENDING_CUSTOMIZATION' expected");
            } else {
                context.addError(WsResponseCode.INVALID_STATE, "Invalid Shipping Package State. 'PICKED' expected.");
            }
        } else {
            Map<String, AddCancelledSaleOrderItemsToPutawayRequest.WsSaleOrderItem> saleOrderItemCodeToStatus = new HashMap<>();
            if (request.getCancelledSaleOrderItems() != null) {
                for (AddCancelledSaleOrderItemsToPutawayRequest.WsSaleOrderItem wsSaleOrderItem : request.getCancelledSaleOrderItems()) {
                    saleOrderItemCodeToStatus.put(wsSaleOrderItem.getCode(), wsSaleOrderItem);
                }
            }
            List<SaleOrderItem> cancelledItems = new ArrayList<>();
            Map<Type, Map<String, Map<Date, List<SaleOrderItem>>>> typeToItemTypeSkuToAgeingToSaleOrderItems = new HashMap<>();
            for (SaleOrderItem saleOrderItem : shippingPackage.getSaleOrderItems()) {
                if (SaleOrderItem.StatusCode.CANCELLED.name().equals(saleOrderItem.getStatusCode())) {
                    cancelledItems.add(saleOrderItem);
                    String skuCode = saleOrderItem.getItemType().getSkuCode();
                    Type inventoryType = Type.GOOD_INVENTORY;
                    if (saleOrderItemCodeToStatus.containsKey(saleOrderItem.getCode())) {
                        inventoryType = Type.valueOf(saleOrderItemCodeToStatus.get(saleOrderItem.getCode()).getStatus().name());
                    }
                    if (TraceabilityLevel.ITEM.equals(traceabilityLevel)) {
                        Item item = saleOrderItem.getItem();
                        if (item.getManufacturingDate() == null && catalogService.isItemTypeExpirable(skuCode)) {
                            context.addError(WsResponseCode.INVALID_ITEM_CODE,
                                    "Cant add expirable item to putaway before adding manufacturing date, Item : " + item.getCode() + " SKU : " + skuCode);
                        }
                    }

                    typeToItemTypeSkuToAgeingToSaleOrderItems.computeIfAbsent(inventoryType, k -> new HashMap<>()).computeIfAbsent(skuCode, k -> new HashMap<>()).computeIfAbsent(
                            saleOrderItem.getItem() != null ? saleOrderItem.getItem().getAgeingStartDate() : Constants.ITEM_SKU_OR_NONE_TRACEABILITY_AGEING_DATE,
                            k -> new ArrayList<>()).add(saleOrderItem);
                }
            }

            if (!context.hasErrors()) {
                Map<Type, Map<String, Map<Date, PutawayItem>>> typeToSkuCodeToAgeingDateToPutawayItemMap = getPutawayItemsMap(putaway);
                typeToItemTypeSkuToAgeingToSaleOrderItems.forEach((type, skuMap) -> skuMap.forEach((skuCode, ageingMap) -> ageingMap.forEach((ageingDate, saleOrderItems) -> {
                    PutawayItem putawayItem = getPutawayItem(typeToSkuCodeToAgeingDateToPutawayItemMap, type, skuCode, ageingDate);
                    if (putawayItem == null) {
                        putawayItem = new PutawayItem(putaway, type, ageingDate, saleOrderItems.get(0).getItemType(), saleOrderItems.size(), 0,
                                PutawayItem.StatusCode.CREATED.name(), DateUtils.getCurrentTime(), DateUtils.getCurrentTime());
                        putawayItem = putawayDao.addPutawayItem(putawayItem);
                        putaway.getPutawayItems().add(putawayItem);
                    } else {
                        putawayItem.setQuantity(putawayItem.getQuantity() + saleOrderItems.size());
                        putawayItem = putawayDao.updatePutawayItem(putawayItem);
                    }
                    for (SaleOrderItem saleOrderItem : saleOrderItems) {
                        if (saleOrderItem.getItem() != null) {
                            saleOrderItem.getItem().setPutaway(putaway);
                            saleOrderItem.getItem().setStatusCode(Item.StatusCode.PUTAWAY_PENDING.name());
                            saleOrderItem.getItem().setInventoryType(type);
                        }
                    }
                    response.getPutawayItems().add(preparePutawayItemDTO(putawayItem));
                })));

                // remove shipping package and invoice item of saleOrderItem
                for (SaleOrderItem saleOrderItem : cancelledItems) {
                    saleOrderItem.setShippingPackage(null);
                    saleOrderItem.setInvoiceItem(null);
                    shippingPackage.getSaleOrderItems().remove(saleOrderItem);
                    saleOrderService.updateSaleOrderItem(saleOrderItem);
                    if (StringUtils.equalsAny(shippingPackage.getStatusCode(), ShippingPackage.StatusCode.PICKED.name(), ShippingPackage.StatusCode.PENDING_CUSTOMIZATION.name(),
                            ShippingPackage.StatusCode.CUSTOMIZATION_COMPLETE.name())) {
                        inventoryService.commitBlockedInventory(saleOrderItem.getItemTypeInventory().getId(), 1);
                    }
                }
                shippingPackage.setPutawayPending(false);
                ReassessShippingPackageRequest packageRequest = new ReassessShippingPackageRequest();
                packageRequest.setShippingPackageCode(shippingPackage.getCode());

                shippingService.reassessShippingPackage(packageRequest);
                if (shippingPackage.getNoOfItems() > 0) {
                    UpdateInvoiceRequest invoiceRequest = new UpdateInvoiceRequest();
                    invoiceRequest.setShippingPackageCode(shippingPackage.getCode());
                    UpdateInvoiceResponse invoiceResponse = shippingInvoiceService.updateInvoice(invoiceRequest);
                    if (!invoiceResponse.isSuccessful()) {
                        response.addErrors(invoiceResponse.getErrors());
                    }
                }
            }
        }

    }

    /**
     * if traceability level is ITEM, shipping package status should be in (PENDING_CUSTOMIZATION,
     * CUSTOMIZATION_COMPLETE, PACKED, READY_TO_SHIP). Otherwise, it should be PICKED.
     */
    private boolean isShippingPackageInValidStateForCancelledPutaway(ShippingPackage shippingPackage, TraceabilityLevel traceabilityLevel) {
        switch (traceabilityLevel) {
            case ITEM:
                return StringUtils.equalsAny(shippingPackage.getStatusCode(), ShippingPackage.StatusCode.PENDING_CUSTOMIZATION.name(),
                        ShippingPackage.StatusCode.CUSTOMIZATION_COMPLETE.name(), ShippingPackage.StatusCode.PACKED.name(), ShippingPackage.StatusCode.READY_TO_SHIP.name());
            case NONE:
            case ITEM_SKU:
                return StringUtils.equalsAny(shippingPackage.getStatusCode(), ShippingPackage.StatusCode.PENDING_CUSTOMIZATION.name(),
                        ShippingPackage.StatusCode.CUSTOMIZATION_COMPLETE.name(), ShippingPackage.StatusCode.PACKED.name(), ShippingPackage.StatusCode.READY_TO_SHIP.name(),
                        ShippingPackage.StatusCode.PICKED.name());
            default:
                throw new RuntimeException("Unknown traceability level.");
        }
    }

    private void addSaleOrderItemToItemTypeMap(SaleOrderItem saleOrderItem, Integer itemTypeId, Map<Integer, List<SaleOrderItem>> itemTypeIdToSaleOrderItems) {
        if (itemTypeIdToSaleOrderItems.containsKey(itemTypeId)) {
            itemTypeIdToSaleOrderItems.get(itemTypeId).add(saleOrderItem);
        } else {
            List<SaleOrderItem> orderItems = new ArrayList<>();
            orderItems.add(saleOrderItem);
            itemTypeIdToSaleOrderItems.put(itemTypeId, orderItems);
        }
    }

    @Override
    @Transactional
    public GetPutawayResponse getPutaway(GetPutawayRequest request) {
        GetPutawayResponse response = new GetPutawayResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            Putaway putaway = putawayDao.getPutawayByCode(request.getCode());
            //for mobile code can be putawayCode as well as itemCode.
            if (putaway == null) {
                Item item = inventoryService.getItemByCode(request.getCode());
                if (item != null) {
                    if (item.getPutaway() == null) {
                        context.addError(WsResponseCode.INVALID_ITEM_CODE, "Item is not assigned to any putaway");
                    } else {
                        putaway = item.getPutaway();
                    }
                }
            }
            if (putaway == null) {
                context.addError(WsResponseCode.INVALID_PUTAWAY_CODE, "Invalid putaway number");
            } else {
                PutawayDTO putawayDTO = preparePutawayDTO(putaway);
                Map<String, Map<String, Set<String>>> skuCodeToTypeToItemCodes = new HashMap<>();
                List<Item> items = getPendingItemsByPutawayId(putaway.getId());
                items.forEach(item -> skuCodeToTypeToItemCodes.computeIfAbsent(item.getItemType().getSkuCode(), skuCode -> new HashMap<>()).computeIfAbsent(
                        item.getInventoryType().name(), type -> new HashSet<>()).add(item.getCode()));

                List<PutawayItemDTO> putawayItemDTOList = preparePutawayItemDTOList(putaway.getPutawayItems());
                for (PutawayItemDTO putawayItemDTO : putawayItemDTOList) {
                    putawayItemDTO.setPendingItemCodes(skuCodeToTypeToItemCodes.computeIfAbsent(putawayItemDTO.getItemTypeSkuCode(), skuCode -> new HashMap<>()).computeIfAbsent(
                            putawayItemDTO.getInventoryType(), type -> new HashSet<>()));
                    putawayDTO.addPutawayItemDTO(putawayItemDTO);
                }
                Collections.sort(putawayDTO.getPutawayItemDTOs(), new Comparator<PutawayItemDTO>() {
                    @Override
                    public int compare(PutawayItemDTO putawayItem1, PutawayItemDTO putawayItem2) {
                        int typeCompare = putawayItem1.getInventoryType().compareTo(putawayItem2.getInventoryType());
                        return typeCompare == 0 ? putawayItem1.getItemTypeSkuCode().compareTo(putawayItem2.getItemTypeSkuCode()) : typeCompare;
                    }
                });

                response.setPutawayDTO(putawayDTO);
                response.setSuccessful(true);
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Item> getPendingItemsByPutawayId(int putawayId) {
        return putawayDao.getPendingItemsByPutawayId(putawayId);
    }

    @Override
    @Transactional
    public GetShelfResponse getShelf(GetShelfRequest request) {
        GetShelfResponse response = new GetShelfResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            Shelf shelf = shelfService.getShelfByCode(request.getCode());
            if (shelf != null) {
                response.setSuccessful(true);
                response.setShelfCode(shelf.getCode());
            } else {
                context.addError(WsResponseCode.INVALID_SHELF_CODE, "Invalid Shelf Code");
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    private boolean isItemValidForPutaway(String putawayCode, Item item, ValidationContext context) {
        if (!(item.getPutaway() != null && putawayCode.equals(item.getPutaway().getCode()))) {
            context.addError(WsResponseCode.ITEM_NOT_IN_GIVEN_PUTAWAY, item.getCode() + " Item doesn't belong to this putaway");
            return false;
        } else if (!Item.StatusCode.PUTAWAY_PENDING.name().equals(item.getStatusCode())) {
            context.addError(WsResponseCode.INVALID_ITEM_STATE, item.getCode() + " Item not in PUTAWAY_PENDING state");
            return false;
        }
        return true;
    }

    @Override
    @Transactional
    public CheckItemForPutawayResponse checkItemForPutaway(CheckItemForPutawayRequest request) {
        ValidationContext context = request.validate();
        CheckItemForPutawayResponse response = new CheckItemForPutawayResponse();
        Item item = inventoryService.getItemByCode(request.getItemCode());
        if (item == null) {
            context.addError(WsResponseCode.INVALID_ITEM_CODE, request.getItemCode() + " Invalid item code");
        } else if (isItemValidForPutaway(request.getPutawayCode(), item, context)) {
            response.setSuccessful(true);
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Transactional
    @Locks({ @Lock(ns = Namespace.SALE_ORDER, key = "#{#args[3].saleOrder.code}") })
    @RollbackOnFailure
    public AddReversePickupSaleOrderItemsToPutawayResponse doAddReversePickupSaleOrderItemsToPutaway(AddReversePickupSaleOrderItemsToPutawayRequest request,
            AddReversePickupSaleOrderItemsToPutawayResponse response, ValidationContext context, ReversePickup reversePickup, String putawayCode) {
        SaleOrder saleOrder = saleOrderService.getSaleOrderForUpdate(reversePickup.getSaleOrder().getId());
        Putaway putaway = getPutawayByCode(putawayCode);
        TraceabilityLevel traceabilityLevel = ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).getTraceabilityLevel();
        reversePickup = returnsService.getReversePickupByCode(request.getReversePickupCode());
        if (!ReversePickup.StatusCode.CREATED.name().equals(reversePickup.getStatusCode())) {
            context.addError(WsResponseCode.INVALID_REVERSE_PICKUP_STATE);
        } else {
            Set<SaleOrderItem> itemsToReturnToCustomer = new HashSet<>();
            Set<SaleOrderItem> itemsToReplaceOrCancel = new HashSet<>();
            Map<Type, Map<String, Map<String,Map<Date, List<SaleOrderItem>>>>> typeToSkuCodeToShelfCodeToAgeingDateToSaleOrderItems = new HashMap<>();
            Map<String, SaleOrderItem> saleOrderItemsMap = saleOrder.getSaleOrderItemsMap();
            Map<String, AddReversePickupSaleOrderItemsToPutawayRequest.WsSaleOrderItem> wsSaleOrderItemMap = new HashMap<>();
            Map<String,Shelf> saleOrderItemCodeToShelfMap=new HashMap<>();
            Map<String, List<SaleOrderItem>> typeTosaleOrderItemDetails = new HashMap<>();
            for (WsSaleOrderItem wsSaleOrderItem : request.getSaleOrderItems()) {
                wsSaleOrderItemMap.put(wsSaleOrderItem.getCode(), wsSaleOrderItem);
                SaleOrderItem saleOrderItem = saleOrderItemsMap.get(wsSaleOrderItem.getCode());
                if (saleOrderItem == null) {
                    context.addError(WsResponseCode.INVALID_SALE_ORDER_ITEM_CODE, "Invalid Sale Order Item Code");
                } else {
                    if (!StringUtils.equalsAny(wsSaleOrderItem.getStatus(), WsSaleOrderItem.StatusCode.GOOD_INVENTORY.name(), WsSaleOrderItem.StatusCode.BAD_INVENTORY.name(),
                            WsSaleOrderItem.StatusCode.RETURN_TO_CUSTOMER.name(), WsSaleOrderItem.StatusCode.LIQUIDATED.name(), WsSaleOrderItem.StatusCode.DEAD_INVENTORY.name())) {
                        context.addError(WsResponseCode.INVALID_REVERSE_PICKUP_STATE, "invalid status code: " + wsSaleOrderItem.getStatus());
                    }
                    if(wsSaleOrderItem.getReturnReason()!=null && !wsSaleOrderItem.getReturnReason().isEmpty()){
                        saleOrderItem.setReversePickupReason(wsSaleOrderItem.getReturnReason());
                    }
                    if (StringUtils.equalsAny(wsSaleOrderItem.getStatus(), WsSaleOrderItem.StatusCode.LIQUIDATED.name())) {
                        if (saleOrderItem.getItem() != null) {
                            saleOrderItem.getItem().setStatusCode(Item.StatusCode.LIQUIDATED.name());
                        }
                        itemsToReplaceOrCancel.add(saleOrderItem);
                    } else if (StringUtils.equalsAny(wsSaleOrderItem.getStatus(), Item.StatusCode.GOOD_INVENTORY.name(), Item.StatusCode.BAD_INVENTORY.name())) {
                        if (TraceabilityLevel.ITEM.equals(traceabilityLevel)) {
                            Item item = saleOrderItem.getItem();
                            ItemType itemType = item.getItemType();
                            boolean isItemAboutToExpire = isItemAboutToExpire(itemType, item.getManufacturingDate(), item.getCode());
                            if (item.getManufacturingDate() == null && catalogService.isItemTypeExpirable(itemType)) {
                                context.addError(WsResponseCode.INVALID_ITEM_CODE, "Cant add expirable item to putaway before adding manufacturing date, Item : " + item.getCode()
                                        + " SKU : " + item.getItemType().getSkuCode());
                            } else if (isItemAboutToExpire && (!wsSaleOrderItem.isExpiredReturn() && !wsSaleOrderItem.getStatus().equals(Item.StatusCode.BAD_INVENTORY.name()))) {
                                context.addError(WsResponseCode.INVALID_REQUEST, "Item is about to expire, mark it as bad");
                            } else if (isItemAboutToExpire) {
                                item.setInventoryType(Type.BAD_INVENTORY);
                                item.setStatusCode(Item.StatusCode.BAD_INVENTORY.name());
                                item.setBadInventoryMarkedTime(DateUtils.getCurrentTime());
                                int shelfLife = catalogService.getShelfLifeForItemType(itemType);
                                if (!DateUtils.addDaysToDate(item.getManufacturingDate(), shelfLife).after(DateUtils.getCurrentTime())) {
                                    item.setRejectionReason(Item.RejectionReason.EXPIRED_ITEM.name());
                                } else {
                                    item.setRejectionReason(Item.RejectionReason.ABOUT_TO_EXPIRE.name());
                                }
                                LOG.info("marking status for expired returns as bad inventory:" + wsSaleOrderItem.getCode());
                                wsSaleOrderItem.setStatus(Item.StatusCode.BAD_INVENTORY.name());
                            }
                        }
                        //todo: check here Ankit
                        if(StringUtils.isNotBlank(wsSaleOrderItem.getShelfCode())){
                            Shelf putawayToShelf = shelfService.getShelfByCode(wsSaleOrderItem.getShelfCode());
                            if(putawayToShelf==null){
                                context.addError(WsResponseCode.INVALID_SHELF_CODE, "invalid shelf code: " + wsSaleOrderItem.getShelfCode());
                            }
                            saleOrderItemCodeToShelfMap.put(saleOrderItem.getCode(),putawayToShelf);
                        }
                        typeToSkuCodeToShelfCodeToAgeingDateToSaleOrderItems.computeIfAbsent(Type.valueOf(wsSaleOrderItem.getStatus()), k -> new HashMap<>()).computeIfAbsent(
                                saleOrderItem.getItemType().getSkuCode(), k -> new HashMap<>()).computeIfAbsent(wsSaleOrderItem.getShelfCode(),k -> new HashMap<>()).computeIfAbsent(
                                        saleOrderItem.getItem() != null ? saleOrderItem.getItem().getAgeingStartDate() : Constants.ITEM_SKU_OR_NONE_TRACEABILITY_AGEING_DATE,
                                        k -> new ArrayList<>()).add(saleOrderItem);
                        itemsToReplaceOrCancel.add(saleOrderItem);
                    } else if (WsSaleOrderItem.StatusCode.RETURN_TO_CUSTOMER.name().equals(wsSaleOrderItem.getStatus())) {
                        if (reversePickup.getReversePickupAction().equals(ReversePickup.Action.REPLACE_IMMEDIATELY_DONT_EXPECT_RETURN.code())
                                || reversePickup.getReversePickupAction().equals(ReversePickup.Action.REPLACE_IMMEDIATELY_EXPECT_RETURN.code())) {
                            context.addError(WsResponseCode.INVALID_REVERSE_PICKUP_STATE, "items cannot be returned to customer for reverse pickups with type REPLACE_IMMEDIATELY");
                        } else {
                            itemsToReturnToCustomer.add(saleOrderItem);
                        }
                    }
                    if (!WsSaleOrderItem.StatusCode.RETURN_TO_CUSTOMER.name().equals(wsSaleOrderItem.getStatus())) {
                        List<SaleOrderItem> itemsToUpdateOnChannel = typeTosaleOrderItemDetails.computeIfAbsent(wsSaleOrderItem.getStatus(), k -> new ArrayList<>());
                        itemsToUpdateOnChannel.add(saleOrderItem);
                    }
                }
            }
            if (!context.hasErrors()) {
                 Map<Type, Map<String, Map<String,Map<Date, PutawayItem>>>> typeToSkuCodeToShelfCodeToAgeingDateToPutawayItemMap = getPutawayItemsMapWithShelfCode(putaway);   //REmoving becoz there is confirmation of no putawayItems in given putaway

                typeToSkuCodeToShelfCodeToAgeingDateToSaleOrderItems.forEach((type, skuMap) -> skuMap.forEach((sku, shelfMap) ->shelfMap.forEach((shelf,ageingDateMap) -> ageingDateMap.forEach((ageingDate, saleOrderItems) -> {
                    PutawayItem putawayItem = getPutawayItemWithShelfCode(typeToSkuCodeToShelfCodeToAgeingDateToPutawayItemMap, type, sku,shelf, ageingDate);
                    Shelf putawayToShelf = saleOrderItemCodeToShelfMap.get(saleOrderItems.get(0).getCode());
                    if (putawayItem == null) {
                        putawayItem = new PutawayItem(putaway, type, ageingDate, saleOrderItems.get(0).getItemType(), saleOrderItems.size(), 0,
                                PutawayItem.StatusCode.CREATED.name(), DateUtils.getCurrentTime(), DateUtils.getCurrentTime());
                        putawayItem.setShelf(putawayToShelf);
                        putawayItem = putawayDao.addPutawayItem(putawayItem);
                    } else {
                        putawayItem.setQuantity(putawayItem.getQuantity() + saleOrderItems.size());
                        putawayItem.setShelf(putawayToShelf);
                        putawayItem = putawayDao.updatePutawayItem(putawayItem);
                    }

                    for (SaleOrderItem orderItem : saleOrderItems) {
                        if (orderItem.getItem() != null) {
                            orderItem.getItem().setPutaway(putaway);
                            orderItem.getItem().setFacility(UserContext.current().getFacility());
                            orderItem.getItem().setStatusCode(Item.StatusCode.PUTAWAY_PENDING.name());
                            orderItem.getItem().setInventoryType(type);
                        }
                        addInventoryLedgerEntryOnReturn(orderItem, putaway.getType(), putawayCode);
                    }
                    response.getPutawayItems().add(preparePutawayItemDTO(putawayItem));
                    putaway.getPutawayItems().add(putawayItem);
                }))));

                //Do not cancel/replace items which are to be returned to customer
                if(reversePickup.getPendingActionableSaleOrderItems().size() == 0 || reversePickup.getPendingActionableSaleOrderItems().size() == itemsToReturnToCustomer.size() + itemsToReplaceOrCancel.size()){
                    if (itemsToReturnToCustomer.size() > 0) {
                        reversePickup.setStatusCode(ReversePickup.StatusCode.REDISPATCH_PENDING.name());
                    } else {
                        reversePickup.setStatusCode(ReversePickup.StatusCode.COMPLETE.name());
                        // prepare credit note for returned shipment
                        if (taxService.isGSTEnabled()) {
                            Set<String> saleOrderItemCodes = reversePickup.getSaleOrderItems().stream().map(saleOrderItem -> saleOrderItem.getCode()).collect(Collectors.toSet());
                            CreateShippingPackageReverseInvoiceRequest createReverseInvoiceRequest = new CreateShippingPackageReverseInvoiceRequest();
                            createReverseInvoiceRequest.setSaleOrderCode(reversePickup.getSaleOrder().getCode());
                            createReverseInvoiceRequest.setSaleOrderItemCodes(saleOrderItemCodes);
                            createReverseInvoiceRequest.setUserId(request.getUserId());
                            CreateShippingPackageReverseInvoiceResponse createReverseInvoiceResponse = shippingInvoiceService.createShippingPackageReverseInvoice(
                                    createReverseInvoiceRequest);
                            if (createReverseInvoiceResponse.hasErrors()) {
                                context.addErrors(createReverseInvoiceResponse.getErrors());
                            } else {
                                reversePickup.setReturnInvoice(invoiceService.getInvoiceByCode(createReverseInvoiceResponse.getInvoiceCode()));
                            }
                        }
                    }
                }

                // Update reverse pickups
                if (itemsToReplaceOrCancel.size() > 0) {
                    if (reversePickup.getReversePickupAction().equals(ReversePickup.Action.WAIT_FOR_RETURN_AND_REPLACE.code())) {
                        reversePickup.setReplacementSaleOrder(returnsService.createReplacement(reversePickup, itemsToReplaceOrCancel));
                        for (SaleOrderItem saleOrderItem : itemsToReplaceOrCancel) {
                            saleOrderItem.setStatusCode(SaleOrderItem.StatusCode.REPLACED.name());
                        }

                    } else if (reversePickup.getReversePickupAction().equals(ReversePickup.Action.WAIT_FOR_RETURN_AND_CANCEL.code())) {
                        for (SaleOrderItem saleOrderItem : itemsToReplaceOrCancel) {
                            saleOrderItem.setStatusCode(SaleOrderItem.StatusCode.CANCELLED.name());
                            saleOrderItem.setCancellationTime(DateUtils.getCurrentTime());
                        }
                    }
                }
                if (reversePickup.getShipmentTracking() != null) {
                    ShipmentTrackingStatus trackingStatus = ConfigurationManager.getInstance().getConfiguration(ShippingConfiguration.class).getShipmentTrackingStatusByCode(
                            reversePickup.getShipmentTracking().getStatusCode());
                    if (!trackingStatus.isStopPolling()) {
                        reversePickup.getShipmentTracking().setDisableStatusUpdate(true);
                    }
                }
                if (!context.hasErrors() && typeTosaleOrderItemDetails.size() > 0) {
                    if (StringUtils.isNotBlank(
                            CacheManager.getInstance().getCache(ChannelCache.class).getScriptName(saleOrder.getChannel().getCode(), Source.MARK_SALE_ORDER_RETURNED_SCRIPT_NAME))) {
                        ScraperScript markSaleOrderReturnedScript = CacheManager.getInstance().getCache(ChannelCache.class).getScriptByName(saleOrder.getChannel().getCode(),
                                Source.MARK_SALE_ORDER_RETURNED_SCRIPT_NAME);
                        if (markSaleOrderReturnedScript != null) {
                            markSaleOrderReturnedOnChannel(context, saleOrder.getChannel(), typeTosaleOrderItemDetails, markSaleOrderReturnedScript);
                        }
                    }
                }
            }

        }
        response.setSuccessful(!context.hasErrors());
        response.setErrors(context.getErrors());
        return response;
    }

    private void addInventoryLedgerEntryOnReturn(SaleOrderItem saleOrderItem, String putawayType, String putawayCode) {
        InventoryLedger inventoryLedger = new InventoryLedger();
        inventoryLedger.setSkuCode(saleOrderItem.getItemType().getSkuCode());
        inventoryLedger.setTransactionIdentifier(saleOrderItem.getCode());
        inventoryLedger.setAdditionalInfo(putawayCode);
        inventoryLedger.setTransactionType(InventoryLedger.TransactionType.RETURN);
        inventoryLedgerService.addInventoryLedgerEntry(inventoryLedger, 1, InventoryLedger.ChangeType.INCREASE);
    }

    private AddReversePickupSaleOrderItemsToPutawayResponse addReversePickupSaleOrderItemsToPutaway(String reversePickupCode, String putawayCode,
            List<WsSaleOrderItem> returnedSaleOrderItems, Integer userId) {
        AddReversePickupSaleOrderItemsToPutawayRequest addReversePickupToPutawayRequest = new AddReversePickupSaleOrderItemsToPutawayRequest();
        addReversePickupToPutawayRequest.setPutawayCode(putawayCode);
        addReversePickupToPutawayRequest.setReversePickupCode(reversePickupCode);
        addReversePickupToPutawayRequest.setSaleOrderItems(returnedSaleOrderItems);
        addReversePickupToPutawayRequest.setUserId(userId);
        return addReversePickupSaleOrderItemsToPutaway(addReversePickupToPutawayRequest);
    }

    @Override
    @Locks({ @Lock(ns = Namespace.PUTAWAY, key = "#{#args[0].putawayCode}", level = Level.FACILITY) })
    public AddReversePickupSaleOrderItemsToPutawayResponse addReversePickupSaleOrderItemsToPutaway(AddReversePickupSaleOrderItemsToPutawayRequest request) {
        AddReversePickupSaleOrderItemsToPutawayResponse response = new AddReversePickupSaleOrderItemsToPutawayResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            ReversePickup reversePickup = returnsService.getReversePickupByCode(request.getReversePickupCode());
            if (reversePickup == null) {
                context.addError(WsResponseCode.INVALID_REVERSE_PICKUP_CODE, "Invalid Reverse Pickup Code");
            } else {
                Putaway putaway = getPutawayByCode(request.getPutawayCode());
                if (putaway == null) {
                    context.addError(WsResponseCode.INVALID_PUTAWAY_ID, "Invalid putaway code");
                } else if (!Putaway.StatusCode.CREATED.name().equals(putaway.getStatusCode())) {
                    context.addError(WsResponseCode.INVALID_PUTAWAY_STATE, "Items can be added only in CREATED state");
                } else if (!putaway.getUser().getId().equals(request.getUserId())) {
                    context.addError(WsResponseCode.INVALID_USER_ID, "putaway created by another user");
                } else {
                    doAddReversePickupSaleOrderItemsToPutaway(request, response, context, reversePickup, request.getPutawayCode());
                }
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Transactional
    @Override
    public Putaway getPutawayByCode(String putawayCode) {
        return putawayDao.getPutawayByCode(putawayCode);
    }

    @Transactional
    @Override
    public Putaway getPutawayById(Integer putawayId) {
        return putawayDao.getPutawayById(putawayId);
    }

    /**
     * Uses {@link com.uniware.services.returns.impl.ReturnsServiceImpl#createReversePickup(CreateReversePickupRequest)}
     * to first create reverse pickup of returned sale order items, and then adds them to putaway using
     * {@link PutawayServiceImpl#addReversePickupSaleOrderItemsToPutaway(AddReversePickupSaleOrderItemsToPutawayRequest)}.
     * <p>
     * <b>Essentially, combines creating reverse pickup and adding reverse pickup items to putaway</b>
     * </p>
     *
     * @param request {@link AddDirectReturnedSaleOrderItemsToPutawayRequest}
     */
    @Override
    @Locks({ @Lock(ns = Namespace.PUTAWAY, key = "#{#args[0].putawayCode}", level = Level.FACILITY), @Lock(ns = Namespace.SALE_ORDER, key = "#{#args[0].saleOrderCode}") })
    @Transactional
    @RollbackOnFailure
    public AddReturnedSaleOrderItemsToPutawayResponse addDirectReturnsSaleOrderItemsToPutaway(AddDirectReturnedSaleOrderItemsToPutawayRequest request) {
        AddReturnedSaleOrderItemsToPutawayResponse response = new AddReturnedSaleOrderItemsToPutawayResponse();
        ValidationContext context = request.validate();
        Putaway putaway = getPutawayByCode(request.getPutawayCode());
        if (putaway == null || !Putaway.Type.PUTAWAY_RECEIVED_RETURNS.name().equals(putaway.getType())) {
            context.addError(WsResponseCode.INVALID_PUTAWAY_CODE);
        }
        if (!context.hasErrors()) {
            SaleOrder saleOrder = saleOrderService.getSaleOrderForUpdate(request.getSaleOrderCode());
            Map<String, WsSaleOrderItem> saleOrderItemToReversePickupItems = new HashMap<>();
            Map<String, List<WsSaleOrderItem>> reversePickupCodeToSaleOrderItems = new HashMap<>();
            List<WsSaleOrderItem> validSaleOrderItemsForReversePickup = new ArrayList<>();
            request.getReturnedSaleOrderItems().forEach(returnedSaleOrderItem -> saleOrderItemToReversePickupItems.put(returnedSaleOrderItem.getCode(), returnedSaleOrderItem));
            saleOrder.getSaleOrderItems().stream().filter(saleOrderItem -> saleOrderItemToReversePickupItems.get(saleOrderItem.getCode()) != null).forEach(saleOrderItem -> {
                WsSaleOrderItem wsSaleOrderItem = saleOrderItemToReversePickupItems.get(saleOrderItem.getCode());
                if (saleOrderItem.getReversePickup() != null) {
                    reversePickupCodeToSaleOrderItems.computeIfAbsent(saleOrderItem.getReversePickup().getCode(), k -> new ArrayList<>()).add(wsSaleOrderItem);
                } else {
                    validSaleOrderItemsForReversePickup.add(wsSaleOrderItem);
                }
            });

            if (!CollectionUtils.isEmpty(validSaleOrderItemsForReversePickup)) {
                CreateReversePickupRequest createReversePickupRequest = new CreateReversePickupRequest();
                createReversePickupRequest.setActionCode(ReversePickup.Action.WAIT_FOR_RETURN_AND_CANCEL.code());
                createReversePickupRequest.setSaleOrderCode(request.getSaleOrderCode());
                createReversePickupRequest.setCustomFieldValues(request.getCustomFieldValues());
                List<WsReversePickupItem> reversePickItems = new ArrayList<>(validSaleOrderItemsForReversePickup.size());
                validSaleOrderItemsForReversePickup.forEach(returnedSaleOrderItem -> {
                    WsReversePickupItem wsReversePickItem = new WsReversePickupItem();
                    wsReversePickItem.setReason(request.getReason());
                    wsReversePickItem.setSaleOrderItemCode(returnedSaleOrderItem.getCode());
                    reversePickItems.add(wsReversePickItem);
                });
                createReversePickupRequest.setReversePickItems(reversePickItems);
                CreateReversePickupResponse createReversePickupResponse = returnsService.createReversePickup(createReversePickupRequest);
                if (!createReversePickupResponse.isSuccessful()) {
                    context.addErrors(createReversePickupResponse.getErrors());
                } else {
                    reversePickupCodeToSaleOrderItems.put(createReversePickupResponse.getReversePickupCode(), validSaleOrderItemsForReversePickup);
                }
            }

            if (!context.hasErrors()) {
                reversePickupCodeToSaleOrderItems.forEach((reversePickupCode, wsSaleOrderItems) -> {
                    AddReversePickupSaleOrderItemsToPutawayResponse addReversePickupToPutawayResponse = addReversePickupSaleOrderItemsToPutaway(reversePickupCode,
                            request.getPutawayCode(), wsSaleOrderItems, request.getUserId());
                    if (!addReversePickupToPutawayResponse.isSuccessful()) {
                        context.addErrors(addReversePickupToPutawayResponse.getErrors());
                    }
                });
            }
        }
        if (context.hasErrors()) {
            response.addErrors(context.getErrors());
        } else {
            response.setSuccessful(true);
        }
        return response;
    }

    /**
     * Performs validation and then forwards request to
     * {@link #addGatePassItemsToPutaway(AddGatePassItemsToPutawayRequest, AddGatePassItemsToPutawayResponse, ValidationContext, OutboundGatePass, Putaway)}<br/>
     * Used when an {@link OutboundGatePass} of type in [{@link OutboundGatePass.Type#STOCK_TRANSFER},
     * {@link OutboundGatePass.Type#RETURNABLE}]is received in facility.
     * <ol>
     * Following conditions should be met while adding items:
     * <li>The putaway should be in {@link Putaway.StatusCode#CREATED} state.</li>
     * <li>Putaway must be of type {@link Putaway.Type#PUTAWAY_GATEPASS_ITEM}</li>
     * <li>Gatepass status must be {@link OutboundGatePass.StatusCode#RETURN_AWAITED}</li>
     * <li>Only the user which created the putaway can add items in it.</li>
     * </ol>
     */
    @Override
    @Locks({ @Lock(ns = Namespace.PUTAWAY, key = "#{#args[0].putawayCode}", level = Level.FACILITY) })
    @Transactional
    public AddGatePassItemsToPutawayResponse addGatePassItemsToPutaway(AddGatePassItemsToPutawayRequest request) {
        AddGatePassItemsToPutawayResponse response = new AddGatePassItemsToPutawayResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            Putaway putaway = getPutawayByCode(request.getPutawayCode());
            if (putaway == null) {
                context.addError(WsResponseCode.INVALID_PUTAWAY_ID, "Invalid putaway id");
            } else if (!Putaway.StatusCode.CREATED.name().equals(putaway.getStatusCode())) {
                context.addError(WsResponseCode.INVALID_PUTAWAY_STATE, "Items can be added only in CREATED state");
            } else if (!Putaway.Type.PUTAWAY_GATEPASS_ITEM.name().equals(putaway.getType())) {
                context.addError(WsResponseCode.INVALID_PUTAWAY_CODE, "Putaway is not of correct type.");
            } else if (!putaway.getUser().getId().equals(request.getUserId())) {
                context.addError(WsResponseCode.INVALID_USER_ID, "putaway created by another user");
            } else {
                OutboundGatePass outboundGatePass = materialService.getGatePassByCode(request.getGatePassCode(), true);
                if (outboundGatePass == null) {
                    context.addError(WsResponseCode.INVALID_OUTBOUND_GATE_PASS_CODE, "Invalid Outbound GatePass Code");
                } else if (OutboundGatePass.Type.STOCK_TRANSFER.name().equals(outboundGatePass.getType())
                        && !outboundGatePass.getToParty().getId().equals(UserContext.current().getFacilityId())) {
                    context.addError(WsResponseCode.INVALID_OUTBOUND_GATE_PASS_CODE, "The Stock Transfer does not belongs to current facility");
                } else if (!OutboundGatePass.StatusCode.RETURN_AWAITED.name().equals(outboundGatePass.getStatusCode())) {
                    context.addError(WsResponseCode.INVALID_STATE, "Invalid GatePass State. putaway can be done for gatepass in return awaited state");
                } else {
                    addGatePassItemsToPutaway(request, response, context, outboundGatePass, putaway);
                }
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    /**
     * <p>
     * Called by {@link PutawayServiceImpl#addGatePassItemsToPutaway(AddGatePassItemsToPutawayRequest)}. This method
     * adds {@link OutboundGatePassItem}s of {@link OutboundGatePass} to putaway.
     * <ol>
     * First, a map of {@code inventory type, sku code of item, ageing start date of item} to putaway item is created,
     * with name: <b>typeToSkuCodeToAgeingDateToPutawayItemMap</b> Then, while iterating on a
     * {@code typeToOutboundGatePassItems} map, depending on traceability level, different workflow is followed:
     * <li><b>ITEM</b>: First, putaway item is fetched from the {@code typeToSkuCodeToAgeingDateToPutawayItemMap}. If
     * its found to be null, then a new putaway item is created. Else, the quantity in the fetched putaway Item is
     * updated.<br/>
     * Then, corresponding updates are made on {@code outbound gate pass item} and {@code item}. The received quantity
     * in {@code outbound gate pass item} is set to be 1, as in case of this traceability, items are scanned one by
     * one.</li>
     * <li><b>ITEM_SKU, NONE</b>:Same as in previous case, except for the fact that there is no change in item table,
     * and since items are added in 1 go, the received quantity is not equal to 1</li>
     * <p>
     * Finally, the mthod checks whether all gate pass items were added, and if so, the gate pass is set to be
     * {@code CLOSED}
     * </p>
     * </p>
     * </ol>
     */
    private void addGatePassItemsToPutaway(AddGatePassItemsToPutawayRequest request, AddGatePassItemsToPutawayResponse response, ValidationContext context,
            OutboundGatePass outboundGatePass, Putaway putaway) {
        Map<Type, List<OutboundGatePassItem>> typeToOutboundGatePassItems = new HashMap<>();
        Map<Integer, WsGatePassItem> idToWsGatePassItems = new HashMap<>();
        Map<Integer, OutboundGatePassItem> idToGatePassItems = getIdToOutboundGatePassItemMap(outboundGatePass);
        TraceabilityLevel traceabilityLevel = ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).getTraceabilityLevel();
        for (WsGatePassItem wsGatePassItem : request.getGatePassItems()) {
            OutboundGatePassItem gatePassItem = idToGatePassItems.get(wsGatePassItem.getId());
            if (gatePassItem == null) {
                context.addError(WsResponseCode.INVALID_OUTBOUND_GATE_PASS_ITEM_ID, "Invalid gate pass item id:" + wsGatePassItem.getId());
                break;
            } else if (!OutboundGatePassItem.StatusCode.RETURN_AWAITED.name().equals(gatePassItem.getStatusCode())) {
                context.addError(WsResponseCode.INVALID_OUTBOUND_GATE_PASS_STATE, "Only RETURN_AWAITED gate pass items can be added to putaway");
                break;
            } else if (ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).getTraceabilityLevel() != TraceabilityLevel.ITEM
                    && wsGatePassItem.getQuantity() > gatePassItem.getQuantity() - gatePassItem.getReceivedQuantity()) {
                context.addError(WsResponseCode.INVALID_OUTBOUND_GATE_PASS_STATE,
                        "Only " + (gatePassItem.getQuantity() - gatePassItem.getReceivedQuantity()) + " are pending to be received");
                break;
            } else if (traceabilityLevel == TraceabilityLevel.ITEM && gatePassItem.getItem().getManufacturingDate() == null
                    && catalogService.isItemTypeExpirable(gatePassItem.getItemType().getSkuCode())) {
                context.addError(WsResponseCode.INVALID_ITEM_CODE, "Cant add expirable item to putaway before adding manufacturing date, Item : " + gatePassItem.getItem().getCode()
                        + " SKU : " + gatePassItem.getItemType().getSkuCode());
                break;
            } else {
                typeToOutboundGatePassItems.computeIfAbsent(wsGatePassItem.getStatus(), k -> new ArrayList<>()).add(gatePassItem);
            }
            idToWsGatePassItems.put(wsGatePassItem.getId(), wsGatePassItem);
        }
        if (!context.hasErrors()) {
            Map<Type, Map<String, Map<Date, PutawayItem>>> typeToSkuCodeToAgeingDateToPutawayItemMap = getPutawayItemsMap(putaway);
            List<OutboundGatePassItem> outboundGatePassItems = new ArrayList<>();
            typeToOutboundGatePassItems.forEach((inventoryType, ogpItems) -> ogpItems.forEach(outboundGatePassItem -> {
                outboundGatePassItems.add(outboundGatePassItem);
                if (traceabilityLevel == TraceabilityLevel.ITEM) {
                    PutawayItem putawayItem = getPutawayItem(typeToSkuCodeToAgeingDateToPutawayItemMap, inventoryType, outboundGatePassItem.getItemType().getSkuCode(),
                            outboundGatePassItem.getItem().getAgeingStartDate());
                    if (putawayItem == null) {
                        putawayItem = new PutawayItem(putaway, inventoryType, outboundGatePassItem.getItem().getAgeingStartDate(), outboundGatePassItem.getItem().getItemType(), 1,
                                0, PutawayItem.StatusCode.CREATED.name(), DateUtils.getCurrentTime(), DateUtils.getCurrentTime());
                        putaway.getPutawayItems().add(putawayItem);
                        putawayDao.addPutawayItem(putawayItem);
                        typeToSkuCodeToAgeingDateToPutawayItemMap.computeIfAbsent(putawayItem.getType(), k -> new HashMap<>()).computeIfAbsent(
                                putawayItem.getItemType().getSkuCode(), k -> new HashMap<>()).put(putawayItem.getAgeingStartDate(), putawayItem);
                    } else {
                        putawayItem.setQuantity(putawayItem.getQuantity() + 1);
                        putawayDao.updatePutawayItem(putawayItem);
                    }
                    outboundGatePassItem.setStatusCode(OutboundGatePassItem.StatusCode.CLOSED.name());
                    outboundGatePassItem.setReceivedQuantity(1);
                    Item item = outboundGatePassItem.getItem();
                    item.setFacility(UserContext.current().getFacility());
                    item.setStatusCode(Item.StatusCode.PUTAWAY_PENDING.name());
                    item.setInventoryType(inventoryType);
                    item.setPutaway(putaway);
                } else {
                    PutawayItem putawayItem = getPutawayItem(typeToSkuCodeToAgeingDateToPutawayItemMap, inventoryType, outboundGatePassItem.getItemType().getSkuCode(),
                            Constants.ITEM_SKU_OR_NONE_TRACEABILITY_AGEING_DATE);
                    if (putawayItem == null) {
                        putawayItem = new PutawayItem(putaway, inventoryType, Constants.ITEM_SKU_OR_NONE_TRACEABILITY_AGEING_DATE, outboundGatePassItem.getItemType(),
                                idToWsGatePassItems.get(outboundGatePassItem.getId()).getQuantity(), 0, PutawayItem.StatusCode.CREATED.name(), DateUtils.getCurrentTime(),
                                DateUtils.getCurrentTime());
                        putaway.getPutawayItems().add(putawayItem);
                        putawayDao.addPutawayItem(putawayItem);
                        typeToSkuCodeToAgeingDateToPutawayItemMap.computeIfAbsent(putawayItem.getType(), k -> new HashMap<>()).computeIfAbsent(
                                putawayItem.getItemType().getSkuCode(), k -> new HashMap<>()).put(putawayItem.getAgeingStartDate(), putawayItem);
                    } else {
                        putawayItem.setQuantity(putawayItem.getQuantity() + idToWsGatePassItems.get(outboundGatePassItem.getId()).getQuantity());
                        putawayDao.updatePutawayItem(putawayItem);
                    }
                    outboundGatePassItem.setReceivedQuantity(outboundGatePassItem.getReceivedQuantity() + idToWsGatePassItems.get(outboundGatePassItem.getId()).getQuantity());
                    if (outboundGatePassItem.getQuantity() == outboundGatePassItem.getReceivedQuantity()) {
                        outboundGatePassItem.setStatusCode(OutboundGatePassItem.StatusCode.CLOSED.name());
                    }
                }
            }));
            addInventoryLedgerEntryOnGatepassReturn(outboundGatePass.getCode(), outboundGatePassItems, putaway.getCode());
            if (isGatePassClosed(outboundGatePass)) {
                if (ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).isGatepassInvoicingRequired() && taxService.isGSTEnabled()
                        && materialService.isReturnInvoiceNeeded(outboundGatePass)) {
                    materialService.createGatepassInvoice(outboundGatePass, context, request.getUserId());
                }
                if (!context.hasErrors()) {
                    outboundGatePass.setStatusCode(OutboundGatePass.StatusCode.CLOSED.name());
                }
            }
            response.setSuccessful(true);
        }
    }

    private Map<Integer, OutboundGatePassItem> getIdToOutboundGatePassItemMap(OutboundGatePass outboundGatePass) {
        Map<Integer, OutboundGatePassItem> idToGatePassItems = new HashMap<>();
        for (OutboundGatePassItem outboundGatePassItem : outboundGatePass.getOutboundGatePassItems()) {
            idToGatePassItems.put(outboundGatePassItem.getId(), outboundGatePassItem);
        }
        return idToGatePassItems;
    }

    private void addInventoryLedgerEntryOnGatepassReturn(String outboundGatePassCode, List<OutboundGatePassItem> outboundGatePassItems, String putawayCode) {
        Map<String, MaterialServiceImpl.GatepassItemDetails> skuCodeToGatepassItemDetailsMap = materialService.populateSkuToGatepassItemDetailsMap(outboundGatePassItems);
        skuCodeToGatepassItemDetailsMap.forEach((skuCode, gatepassItemDetails) -> {
            InventoryLedger inventoryLedger = new InventoryLedger();
            inventoryLedger.setTransactionType(InventoryLedger.TransactionType.OUTBOUND_GATE_PASS_RETURN);
            inventoryLedger.setTransactionIdentifier(outboundGatePassCode);
            inventoryLedger.setSkuCode(skuCode);
            inventoryLedger.setAdditionalInfo(putawayCode);
            inventoryLedgerService.addInventoryLedgerEntry(inventoryLedger, gatepassItemDetails.getQuantity(), InventoryLedger.ChangeType.INCREASE);

        });
    }

    /**
     * Validates and forwards requests to {@link #doAddTraceableItemToTransferPutaway}. Used when doing shelf transfer
     * of {@link Item}.
     * <ol>
     * Following conditions should be met while adding items:
     * <li>The putaway should be in {@link Putaway.StatusCode#CREATED} state.</li>
     * <li>Putaway must be of type {@link Putaway.Type#PUTAWAY_SHELF_TRANSFER}</li>
     * <li>Only the user which created the putaway can add items in it.</li>
     * </ol>
     * <p>
     * <b>Note</b>: Only works on {@link TraceabilityLevel#ITEM}
     * </p>
     */
    @Override
    @Locks({ @Lock(ns = Namespace.PUTAWAY, key = "#{#args[0].putawayCode}", level = Level.FACILITY) })
    public AddTraceableItemToTransferPutawayResponse addTraceableItemToTransferPutaway(AddTraceableItemToTransferPutawayRequest request) {
        AddTraceableItemToTransferPutawayResponse response = new AddTraceableItemToTransferPutawayResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            Putaway putaway = getPutawayByCode(request.getPutawayCode());
            if (ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).getTraceabilityLevel() != TraceabilityLevel.ITEM) {
                context.addError(WsResponseCode.FEATURE_NOT_SUPPORTED, "This feature is only support with configuration TraceabilityLevel.ITEM");
            } else if (putaway == null) {
                context.addError(WsResponseCode.INVALID_PUTAWAY_CODE, "Invalid putaway code");
            } else if (!putaway.getUser().getId().equals(request.getUserId())) {
                context.addError(WsResponseCode.INVALID_USER_ID, "putaway created by another user");
            } else if (!Putaway.StatusCode.CREATED.name().equals(putaway.getStatusCode())) {
                context.addError(WsResponseCode.INVALID_PUTAWAY_STATE, "Items can be added only in CREATED state");
            } else if (!Putaway.Type.PUTAWAY_SHELF_TRANSFER.name().equals(putaway.getType())) {
                context.addError(WsResponseCode.INVALID_PUTAWAY_CODE, "putaway not of type PUTAWAY_TRANSFER");
            } else {
                doAddTraceableItemToTransferPutaway(request, response, context, putaway);
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    /**
     * Adds {@link Item} to {@link Putaway} of type {@link Putaway.Type#PUTAWAY_SHELF_TRANSFER}.
     * <ol>
     * <li>Can only add item if it's status code is in [{@link Item.StatusCode#GOOD_INVENTORY},
     * {@link Item.StatusCode#BAD_INVENTORY}, {@link Item.StatusCode#QC_REJECTED}]</li>
     * <li>Accuracy level must be {@link InventoryAccuracyLevel#SHELF}</li>
     * </ol>
     */
    @Locks({ @Lock(ns = Namespace.ITEM, key = "#{#args[0].itemCode}", level = Level.FACILITY) })
    @Transactional
    @RollbackOnFailure
    private AddTraceableItemToTransferPutawayResponse doAddTraceableItemToTransferPutaway(AddTraceableItemToTransferPutawayRequest request,
            AddTraceableItemToTransferPutawayResponse response, ValidationContext context, Putaway putaway) {
        putaway = putawayDao.getPutawayById(putaway.getId());
        Item item = inventoryService.getItemByCode(request.getItemCode(), false);
        if (item == null) {
            context.addError(WsResponseCode.INVALID_ITEM_CODE, "Invalid Item Code");
        } else if (!StringUtils.equalsAny(item.getStatusCode(), Item.StatusCode.GOOD_INVENTORY.name(), Item.StatusCode.BAD_INVENTORY.name(), Item.StatusCode.QC_REJECTED.name())) {
            context.addError(WsResponseCode.INVALID_ITEM_STATE, "Invalid Item State: only GOOD_INVENTORY/BAD_INVENTORY/QC_REJECTED items can be transferred");
        } else {
            if (StringUtils.isNotBlank(request.getShelfCode())) {
                Shelf shelf = shelfService.getShelfByCode(request.getShelfCode());
                if (shelf == null) {
                    context.addError(WsResponseCode.INVALID_SHELF_CODE, "Invalid Shelf Code");
                } else if (ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).getInventoryAccuracyLevel().equals(InventoryAccuracyLevel.SHELF)
                        && !shelf.getCode().equals(item.getShelf().getCode())) {
                    context.addError(WsResponseCode.INVALID_SHELF_CODE, "Item shelf should be same as fromShelf for accuracy level: SHELF");
                }
            }
            if (!context.hasErrors()) {
                try {
                    inventoryService.blockInventory(item);
                } catch (InventoryNotAvailableException e) {
                    context.addError(WsResponseCode.INVENTORY_NOT_AVAILABLE, "Item inventory has already been assigned");
                } catch (InventoryAccuracyException e) {
                    context.addError(WsResponseCode.INVENTORY_NOT_AVAILABLE, "Inventory not available");
                } catch (ShelfBlockedForCycleCountException e) {
                    context.addError(WsResponseCode.SHELF_BLOCKED_FOR_CYCLE_COUNT, "Shelf blocked for cycle count. Try later.");
                }
                if (!context.hasErrors()) {
                    addItemToPutaway(item, putaway);
                    response.setSuccessful(true);
                } else {
                    response.setErrors(context.getErrors());
                }
            }
        }
        return response;
    }

    @Override
    @Locks({ @Lock(ns = Namespace.PUTAWAY, key = "#{#args[0].putawayCode}", level = Level.FACILITY) })
    @Transactional
    @RollbackOnFailure
    public AddNonTraceableItemToTransferPutawayResponse addNonTraceableItemToTransferPutaway(AddNonTraceableItemToTransferPutawayRequest request) {
        AddNonTraceableItemToTransferPutawayResponse response = new AddNonTraceableItemToTransferPutawayResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            Putaway putaway = getPutawayByCode(request.getPutawayCode());
            if (ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).getTraceabilityLevel() == TraceabilityLevel.ITEM) {
                context.addError(WsResponseCode.FEATURE_NOT_SUPPORTED, "This feature is not supported with configuration TraceabilityLevel.ITEM");
            } else if (putaway == null) {
                context.addError(WsResponseCode.INVALID_PUTAWAY_CODE, "Invalid putaway code");
            } else if (!putaway.getUser().getId().equals(request.getUserId())) {
                context.addError(WsResponseCode.INVALID_USER_ID, "putaway created by another user");
            } else if (!Putaway.StatusCode.CREATED.name().equals(putaway.getStatusCode())) {
                context.addError(WsResponseCode.INVALID_PUTAWAY_STATE, "Items can be added only in CREATED state");
            } else if (!Putaway.Type.PUTAWAY_SHELF_TRANSFER.name().equals(putaway.getType())) {
                context.addError(WsResponseCode.INVALID_PUTAWAY_CODE, "putaway not of type PUTAWAY_TRANSFER");
            } else {
                ItemType itemType = catalogService.getItemTypeBySkuCode(request.getItemSKU());
                if (itemType == null) {
                    context.addError(WsResponseCode.INVALID_ITEM_TYPE, "Invalid item sku code");
                } else {
                    Type inventoryType = Type.GOOD_INVENTORY;
                    if (StringUtils.isNotBlank(request.getInventoryType())) {
                        try {
                            inventoryType = Type.valueOf(request.getInventoryType());
                        } catch (IllegalArgumentException e) {
                            inventoryType = Type.GOOD_INVENTORY;
                            LOG.warn("Not a valid value for inventory type " + request.getInventoryType());
                        }
                    }
                    if (StringUtils.isNotBlank(request.getShelfCode())) {
                        Shelf shelf = shelfService.getShelfByCode(request.getShelfCode());
                        if (shelf == null) {
                            context.addError(WsResponseCode.INVALID_SHELF_CODE, "No shelf found with code: " + request.getShelfCode());
                        } else {
                            try {
                                inventoryService.removeInventory(itemType, inventoryType, shelf, request.getQuantity());
                            } catch (InventoryNotAvailableException e) {
                                context.addError(WsResponseCode.INVENTORY_NOT_AVAILABLE, "Item inventory has already been assigned");
                            } catch (ShelfBlockedForCycleCountException e) {
                                context.addError(WsResponseCode.SHELF_BLOCKED_FOR_CYCLE_COUNT);
                            }
                        }
                    } else {
                        for (int i = 0; i < request.getQuantity(); i++) {
                            try {
                                inventoryService.removeAgeAgnosticInventory(itemType, inventoryType);
                            } catch (InventoryNotAvailableException e) {
                                context.addError(WsResponseCode.INVENTORY_NOT_AVAILABLE, "Item inventory has already been assigned");
                                break;
                            } catch (ShelfBlockedForCycleCountException e) {
                                context.addError(WsResponseCode.SHELF_BLOCKED_FOR_CYCLE_COUNT);
                                break;
                            }
                        }
                    }

                    if (!context.hasErrors()) {
                        PutawayItem putawayItem = null;
                        for (PutawayItem pi : putaway.getPutawayItems()) {
                            if (pi.getItemType().getId().equals(itemType.getId())) {
                                putawayItem = pi;
                                break;
                            }
                        }
                        if (putawayItem == null) {
                            putawayItem = new PutawayItem(putaway, inventoryType, Constants.ITEM_SKU_OR_NONE_TRACEABILITY_AGEING_DATE, itemType, request.getQuantity(), 0,
                                    PutawayItem.StatusCode.CREATED.name(), DateUtils.getCurrentTime(), DateUtils.getCurrentTime());
                            putaway.getPutawayItems().add(putawayItem);
                            putawayDao.addPutawayItem(putawayItem);
                        } else {
                            putawayItem.setQuantity(putawayItem.getQuantity() + request.getQuantity());
                            putawayDao.updatePutawayItem(putawayItem);
                        }
                        response.setSuccessful(true);
                    }
                }
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    private boolean isGatePassClosed(OutboundGatePass outboundGatePass) {
        if (OutboundGatePass.StatusCode.CLOSED.name().equals(outboundGatePass.getStatusCode())) {
            return false;
        } else {
            for (OutboundGatePassItem outboundGatePassItem : outboundGatePass.getOutboundGatePassItems()) {
                if (!OutboundGatePassItem.StatusCode.CLOSED.name().equals(outboundGatePassItem.getStatusCode())) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Performs validation and then forwards request to
     * {@link #doAddInspectedNonBadItemToPutawayInternal(AddInspectedNotBadItemToPutawayRequest, AddInspectedNotBadItemToPutawayResponse, ValidationContext, Putaway)}<br/>
     * Used when {@link Item} of status {@link Item.StatusCode#BAD_INVENTORY} is to be marked
     * {@link Item.StatusCode#GOOD_INVENTORY}. This is done by first adding Item in {@link Putaway} of type
     * {@link Putaway.Type#PUTAWAY_INSPECTED_NOT_BAD_ITEM}, and then completeing the putaway.
     * <ol>
     * Following conditions should be met while adding items:
     * <li>The putaway should be in {@link Putaway.StatusCode#CREATED} state.</li>
     * <li>Only the user which created the putaway can add items in it.</li>
     * </ol>
     * <p>
     * <b>Note</b>: Only works on {@link TraceabilityLevel#ITEM}
     * </p>
     */
    @Override
    @Locks({ @Lock(ns = Namespace.PUTAWAY, key = "#{#args[0].putawayCode}", level = Level.FACILITY) })
    public AddInspectedNotBadItemToPutawayResponse addInspectedNotBadItemToPutaway(AddInspectedNotBadItemToPutawayRequest request) {
        AddInspectedNotBadItemToPutawayResponse response = new AddInspectedNotBadItemToPutawayResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            Putaway putaway = getPutawayByCode(request.getPutawayCode());
            if (putaway == null) {
                context.addError(WsResponseCode.INVALID_PUTAWAY_ID, "Invalid putaway id");
            } else if (!Putaway.StatusCode.CREATED.name().equals(putaway.getStatusCode())) {
                context.addError(WsResponseCode.INVALID_PUTAWAY_STATE, "Items can be added only in CREATED state");
            } else if (!putaway.getUser().getId().equals(request.getUserId())) {
                context.addError(WsResponseCode.INVALID_USER_ID, "putaway created by another user");
            } else if (ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).getTraceabilityLevel() != TraceabilityLevel.ITEM) {
                context.addError(WsResponseCode.FEATURE_NOT_SUPPORTED, "This feature is only support with configuration TraceabilityLevel.ITEM");
            } else {
                doAddInspectedNonBadItemToPutawayInternal(request, response, context, putaway);
            }
        }
        response.addErrors(context.getErrors());
        return response;
    }

    /**
     * Fetches {@link Item} while taking lock and forwards request to
     * {@link #addInspectedNotBadItemToPutaway(AddInspectedNotBadItemToPutawayRequest, AddInspectedNotBadItemToPutawayResponse, ValidationContext, Item, Putaway)}<br/>
     */
    @Locks({ @Lock(ns = Namespace.ITEM, key = "#{#args[0].itemCode}", level = Level.FACILITY) })
    @Transactional
    private void doAddInspectedNonBadItemToPutawayInternal(AddInspectedNotBadItemToPutawayRequest request, AddInspectedNotBadItemToPutawayResponse response,
            ValidationContext context, Putaway putaway) {
        putaway = getPutawayById(putaway.getId());
        Item item = inventoryService.getItemByCode(request.getItemCode(), false);
        if (item == null) {
            context.addError(WsResponseCode.INVALID_ITEM_CODE, "Invalid Item Code");
        } else {
            addInspectedNotBadItemToPutaway(request, response, context, item, putaway);
        }
    }

    /**
     * Adds {@link Item} to {@link Putaway}. {@code Item} must be in state of
     * {@link Item.StatusCode#BAD_INVENTORY}.<br/>
     * This method calls {@link IInventoryService#blockInventory(Item)} to block inventory,
     */
    private void addInspectedNotBadItemToPutaway(AddInspectedNotBadItemToPutawayRequest request, AddInspectedNotBadItemToPutawayResponse response, ValidationContext context,
            Item item, Putaway putaway) {
        if (!Item.StatusCode.BAD_INVENTORY.name().equals(item.getStatusCode())) {
            context.addError(WsResponseCode.INVALID_STATE, "Invalid Item State. putaway can be done for item in BAD_INVENTORY state");
        } else {
            try {
                inventoryService.blockInventory(item);
            } catch (InventoryNotAvailableException e) {
                // Ignoring this exception if BAD inventory is added to gatepass
            } catch (InventoryAccuracyException e) {
                context.addError(WsResponseCode.INVENTORY_NOT_AVAILABLE, "Inventory not available");
            } catch (ShelfBlockedForCycleCountException e) {
                context.addError(WsResponseCode.SHELF_BLOCKED_FOR_CYCLE_COUNT, "Shelf blocked for cycle count. Try later.");
            }
            if (!context.hasErrors()) {
                item.setInventoryType(Type.GOOD_INVENTORY);
                addItemToPutaway(item, putaway);
                response.setSuccessful(true);
            }
        }
    }

    @Override
    @Transactional(readOnly = true)
    public List<Item> getItemsByPutawayId(int putawayId) {
        return putawayDao.getItemsByPutawayId(putawayId);
    }

    private void addItemToPutaway(Item item, Putaway putaway) {
        PutawayItem putawayItem = null;
        for (PutawayItem pi : putaway.getPutawayItems()) {
            if (item.getInventoryType().equals(pi.getType()) && pi.getItemType().getSkuCode().equals(item.getItemType().getSkuCode())
                    && pi.getAgeingStartDate().equals(item.getAgeingStartDate())) {
                putawayItem = pi;
                break;
            }
        }
        if (putawayItem == null) {
            putawayItem = new PutawayItem(putaway, item.getInventoryType(), item.getAgeingStartDate(), item.getItemType(), 1, 0, PutawayItem.StatusCode.CREATED.name(),
                    DateUtils.getCurrentTime(), DateUtils.getCurrentTime());
            putaway.getPutawayItems().add(putawayItem);
            putawayDao.addPutawayItem(putawayItem);
        } else {
            putawayItem.setQuantity(putawayItem.getQuantity() + 1);
            putawayDao.updatePutawayItem(putawayItem);
        }
        item.setStatusCode(Item.StatusCode.PUTAWAY_PENDING.name());
        item.setPutaway(putaway);
    }

    @Override
    @Transactional
    public List<PutawayStatus> getPutawayStatuses() {
        return putawayDao.getPutawayStatuses();
    }

    @Override
    @Transactional
    public SearchDirectReturnsResponse searchDirectReturns(SearchDirectReturnsRequest request) {
        SearchDirectReturnsResponse response = new SearchDirectReturnsResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            if (ScanType.ITEM.name().equalsIgnoreCase(request.getType())) {
                GetItemDetailRequest itemDetailRequest = new GetItemDetailRequest();
                itemDetailRequest.setItemCode(request.getCode());
                GetItemDetailResponse itemDetailResponse = itemService.getItemDetail(itemDetailRequest);
                if (itemDetailResponse.isSuccessful()) {
                    DirectSaleOrderItemDTO latestSaleOrderItemDTO = null;
                    com.uniware.core.api.item.GetItemDetailResponse.SaleOrderItemDTO lastSaleOrderItemDTO = null;
                    for (com.uniware.core.api.item.GetItemDetailResponse.SaleOrderItemDTO saleOrderItemDTO : itemDetailResponse.getItemDTO().getSaleOrderItemDTOs()) {
                        DirectSaleOrderItemDTO directSaleOrderItemDTO = new DirectSaleOrderItemDTO();
                        directSaleOrderItemDTO.setCode(saleOrderItemDTO.getSaleOrderItemCode());
                        directSaleOrderItemDTO.setSaleOrderCode(saleOrderItemDTO.getSaleOrderCode());
                        directSaleOrderItemDTO.setItemName(saleOrderItemDTO.getItemName());
                        directSaleOrderItemDTO.setItemSku(saleOrderItemDTO.getItemSku());
                        SaleOrderItem saleOrderItem = saleOrderService.getSaleOrderItemByCode(saleOrderItemDTO.getSaleOrderCode(), saleOrderItemDTO.getSaleOrderItemCode());
                        if (saleOrderItem != null && saleOrderItem.getShippingPackage() != null) {
                            directSaleOrderItemDTO.setTrackingNumber(saleOrderItem.getShippingPackage().getTrackingNumber());
                        }
                        ItemType itemType = catalogService.getItemTypeBySkuCode(saleOrderItemDTO.getItemSku());
                        directSaleOrderItemDTO.setColor(itemType.getColor());
                        directSaleOrderItemDTO.setSize(itemType.getSize());
                        directSaleOrderItemDTO.setBrand(itemType.getBrand());
                        directSaleOrderItemDTO.setId(saleOrderItemDTO.getId());
                        directSaleOrderItemDTO.setSaleOrderItemStatus(saleOrderItemDTO.getSaleOrderItemStatus());
                        directSaleOrderItemDTO.setImageUrl(saleOrderItemDTO.getImageUrl());
                        directSaleOrderItemDTO.setPageUrl(saleOrderItemDTO.getPageUrl());
                        directSaleOrderItemDTO.setItemCode(request.getCode());
                        if (lastSaleOrderItemDTO == null || lastSaleOrderItemDTO.getCreated().before(saleOrderItemDTO.getCreated())) {
                            latestSaleOrderItemDTO = directSaleOrderItemDTO;
                            lastSaleOrderItemDTO = saleOrderItemDTO;
                        }
                    }
                    response.addDirectSaleOrderItemDTO(latestSaleOrderItemDTO);
                    response.setSuccessful(true);
                } else {
                    context.addErrors(itemDetailResponse.getErrors());
                }
            } else if (ScanType.PACKAGE_CODE.name().equalsIgnoreCase(request.getType())) {
                ShippingPackage shippingPackage = shippingService.getShippingPackageByCode(request.getCode());
                if (shippingPackage != null) {
                    if (ShippingPackage.StatusCode.DISPATCHED.name().equals(shippingPackage.getStatusCode())
                            || ShippingPackage.StatusCode.DELIVERED.name().equals(shippingPackage.getStatusCode())) {
                        response.getSaleOrderItems().addAll(prepareApplicableSaleOrderItemDTOs(shippingPackage.getSaleOrderItems()));
                        response.setSuccessful(true);
                    } else {
                        context.addError(WsResponseCode.INVALID_SHIPPING_PACKAGE_STATE, "Invalid package state" + shippingPackage.getStatusCode());
                    }
                } else {
                    context.addError(WsResponseCode.INVALID_SHIPPING_PACKAGE_CODE, "Invalid Package Code");
                }
            } else if (ScanType.SALE_ORDER_CODE.name().equals(request.getType())) {
                SaleOrder saleOrder = saleOrderService.getSaleOrderByCode(request.getCode());
                if (saleOrder != null) {
                    response.getSaleOrderItems().addAll(prepareApplicableSaleOrderItemDTOs(saleOrder.getSaleOrderItems()));
                    response.setSuccessful(true);
                } else {
                    context.addError(WsResponseCode.INVALID_SALE_ORDER_CODE, "Invalid Order Code");
                }
            } else if (ScanType.TRACKING_NUMBER.name().equals(request.getType())) {
                List<ShippingPackage> shippingPackages = shippingService.getShippingPackagesByTrackingNumber(request.getCode());
                if (!shippingPackages.isEmpty()) {
                    ShippingPackage shippingPackage = shippingPackages.get(0);
                    if (ShippingPackage.StatusCode.DISPATCHED.name().equals(shippingPackage.getStatusCode())
                            || ShippingPackage.StatusCode.DELIVERED.name().equals(shippingPackage.getStatusCode())) {
                        response.getSaleOrderItems().addAll(prepareApplicableSaleOrderItemDTOs(shippingPackage.getSaleOrderItems()));
                        response.setSuccessful(true);
                    } else {
                        context.addError(WsResponseCode.INVALID_SHIPPING_PACKAGE_STATE, "Invalid package state" + shippingPackage.getStatusCode());
                    }
                } else {
                    context.addError(WsResponseCode.INVALID_TRACKING_NUMBER, "Invalid tracking number");
                }
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    private List<DirectSaleOrderItemDTO> prepareApplicableSaleOrderItemDTOs(Set<SaleOrderItem> saleOrderItems) {
        List<DirectSaleOrderItemDTO> saleOrderItemDTOs = new ArrayList<>(saleOrderItems.size());
        for (SaleOrderItem saleOrderItem : saleOrderItems) {
            if (StringUtils.equalsAny(saleOrderItem.getStatusCode(), SaleOrderItem.StatusCode.DISPATCHED.name(), SaleOrderItem.StatusCode.DELIVERED.name())) {
                DirectSaleOrderItemDTO directSaleOrderItemDTO = new DirectSaleOrderItemDTO();
                directSaleOrderItemDTO.setCode(saleOrderItem.getCode());
                directSaleOrderItemDTO.setSaleOrderCode(saleOrderItem.getSaleOrder().getCode());
                directSaleOrderItemDTO.setItemName(saleOrderItem.getItemType().getName());
                directSaleOrderItemDTO.setItemSku(saleOrderItem.getItemType().getSkuCode());
                directSaleOrderItemDTO.setId(saleOrderItem.getId());
                directSaleOrderItemDTO.setSaleOrderItemStatus(saleOrderItem.getStatusCode());
                directSaleOrderItemDTO.setImageUrl(saleOrderItem.getItemType().getImageUrl());
                directSaleOrderItemDTO.setPageUrl(saleOrderItem.getItemType().getProductPageUrl());
                directSaleOrderItemDTO.setColor(saleOrderItem.getItemType().getColor());
                directSaleOrderItemDTO.setSize(saleOrderItem.getItemType().getSize());
                directSaleOrderItemDTO.setBrand(saleOrderItem.getItemType().getBrand());
                if (saleOrderItem.getShippingPackage() != null) {
                    directSaleOrderItemDTO.setTrackingNumber(saleOrderItem.getShippingPackage().getTrackingNumber());
                }
                if (saleOrderItem.getItem() != null) {
                    directSaleOrderItemDTO.setItemCode(saleOrderItem.getItem().getCode());
                }
                saleOrderItemDTOs.add(directSaleOrderItemDTO);
            }
        }
        return saleOrderItemDTOs;
    }

    @RollbackOnFailure
    @Transactional
    @Override
    public ReleaseInventoryAndRepackageResponse releaseInventoryAndRepackage(ReleaseInventoryAndRepackageRequest request) {
        ReleaseInventoryAndRepackageResponse response = new ReleaseInventoryAndRepackageResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            CreatePutawayRequest createPutawayRequest = new CreatePutawayRequest();
            createPutawayRequest.setType(Putaway.Type.PUTAWAY_CANCELLED_ITEM.name());
            createPutawayRequest.setUserId(request.getUserId());
            CreatePutawayResponse createPutawayResponse = createPutaway(createPutawayRequest);
            if (!createPutawayResponse.isSuccessful()) {
                context.getErrors().addAll(createPutawayResponse.getErrors());
            } else {
                for (String packageCode : request.getShippingPackageCodes()) {
                    ShippingPackage shippingPackage = shippingService.getShippingPackageByCode(packageCode);
                    if (shippingPackage == null) {
                        response.getFailedShippingPackages().put(packageCode, new FailedShippingPackageDTO(packageCode, WsResponseCode.INVALID_SHIPPING_PACKAGE_CODE.message()));
                    } else {
                        boolean hasCancelledItems = false;
                        for (SaleOrderItem saleOreItem : shippingPackage.getSaleOrderItems()) {
                            if (SaleOrderItem.StatusCode.CANCELLED.name().equals(saleOreItem.getStatusCode())) {
                                hasCancelledItems = true;
                                break;
                            }
                        }
                        if (hasCancelledItems) {
                            AddCancelledSaleOrderItemsToPutawayRequest addCancelledSoiToPutawayRequest = new AddCancelledSaleOrderItemsToPutawayRequest();
                            addCancelledSoiToPutawayRequest.setPutawayCode(createPutawayResponse.getPutawayDTO().getCode());
                            addCancelledSoiToPutawayRequest.setShipmentCode(packageCode);
                            addCancelledSoiToPutawayRequest.setUserId(request.getUserId());
                            AddCancelledSaleOrderItemsToPutawayResponse addCancelledSoiToPutawayResponse = addCancelledSaleOrderItemsToPutaway(addCancelledSoiToPutawayRequest);
                            if (!addCancelledSoiToPutawayResponse.isSuccessful()) {
                                response.getFailedShippingPackages().put(packageCode,
                                        new FailedShippingPackageDTO(packageCode, addCancelledSoiToPutawayResponse.getErrors().get(0).getMessage()));
                            } else {
                                shippingPackage.setPutawayPending(false);
                                shippingService.updateShippingPackage(shippingPackage);
                            }
                        } else {
                            response.getFailedShippingPackages().put(packageCode, new FailedShippingPackageDTO(packageCode, "Package does not have any cancelled items"));
                        }
                    }
                }
                CreatePutawayListRequest createPutawayListRequest = new CreatePutawayListRequest();
                createPutawayListRequest.setPutawayCode(createPutawayResponse.getPutawayDTO().getCode());
                createPutawayListRequest.setUserId(request.getUserId());
                CreatePutawayListResponse createPutawayListResponse = createPutawayList(createPutawayListRequest);
                if (!createPutawayListResponse.isSuccessful()) {
                    context.getErrors().addAll(createPutawayListResponse.getErrors());
                }
                if (!context.hasErrors()) {
                    Putaway putaway = putawayDao.getPutawayByCode(createPutawayResponse.getPutawayDTO().getCode());
                    //in case of warehouse bin management true
                    if (!Putaway.StatusCode.COMPLETE.name().equals(putaway.getStatusCode())) {
                        CompletePutawayRequest completePutawayRequest = new CompletePutawayRequest();
                        completePutawayRequest.setPutawayCode(createPutawayResponse.getPutawayDTO().getCode());
                        CompletePutawayResponse completePutawayResponse = completePutaway(completePutawayRequest);
                        if (!completePutawayResponse.isSuccessful()) {
                            context.getErrors().addAll(completePutawayResponse.getErrors());
                        }
                    }
                }
                if (!context.hasErrors()) {
                    response.setSuccessful(true);
                }
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    @RollbackOnFailure
    public SplitPutawayItemResponse splitPutawayItem(SplitPutawayItemRequest request) {
        SplitPutawayItemResponse response = new SplitPutawayItemResponse();
        ValidationContext context = request.validate();
        PutawayItem putawayItem = null;
        if (!context.hasErrors()) {
            putawayItem = putawayDao.getPutawayItemById(request.getPutawayItemId(), true);
            if (putawayItem == null) {
                context.addError(WsResponseCode.INVALID_PUTAWAY_ITEM_ID, "Invalid inflow putaway item id");
            } else if (!Putaway.StatusCode.PENDING.name().equals(putawayItem.getPutaway().getStatusCode())) {
                context.addError(WsResponseCode.INVALID_PUTAWAY_STATE, "Shelf can be modified only in PENDING state");
            }
        }
        if (!context.hasErrors()) {
            Integer quantity = 0;
            for (PutawayItemToSplit item : request.getPutawayItems()) {
                quantity += item.getQuantity();
            }
            if (quantity != putawayItem.getQuantity()) {
                context.addError(WsResponseCode.INFLOW_RECEIPT_NEW_PUTAWAY_QTY_CANNOT_EXCEED_CURRENT, "Putaway Quantity mismatch with existing quantity");
            } else {
                Integer splitCount = 0;
                List<String> shelfCodes = getPendingShelvesForPutaway(putawayItem.getPutaway().getCode());
                for (PutawayItemToSplit item : request.getPutawayItems()) {
                    splitCount++;
                    Shelf shelf = shelfService.getShelfByCode(item.getShelfCode());
                    if (shelf == null) {
                        context.addError(WsResponseCode.INVALID_SHELF_CODE, "Invalid Shelf Code");
                        break;
                    } else if (shelf.isBlockedForCycleCount() && !shelfCodes.contains(shelf.getCode())) {
                        context.addError(WsResponseCode.SHELF_BLOCKED_FOR_CYCLE_COUNT, "Shelf blocked in cycle count");
                        break;
                    } else if (shelf.isInactive()) {
                        context.addError(WsResponseCode.INACTIVE_SHELF, "Shelf is inactive.");
                    } else {
                        if (splitCount == 1) {
                            putawayItem.setShelf(shelf);
                            putawayItem.setQuantity(item.getQuantity());
                            response.getPutawayItemDTOs().add(new PutawayItemDTO(putawayItem));
                        } else {
                            PutawayItem newPutawayItem = preparePutawayItem(putawayItem.getPutaway(), putawayItem.getStatusCode(), shelf, putawayItem.getItemType(),
                                    putawayItem.getType(), item.getQuantity(), putawayItem.getAgeingStartDate());
                            response.getPutawayItemDTOs().add(new PutawayItemDTO(newPutawayItem));
                        }
                    }
                }
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        } else {
            response.setSuccessful(true);
        }
        return response;
    }

    @Transactional(readOnly = true)
    private List<String> getPendingShelvesForPutaway(String putawayCode) {
        return putawayDao.getPendingShelvesForPutaway(putawayCode);
    }

    @Override
    public GetPutawayTypesResponse getPutawayTypes(GetPutawayTypesRequest request) {
        GetPutawayTypesResponse response = new GetPutawayTypesResponse();
        List<String> types = new ArrayList<>();
        for (Putaway.Type type : Putaway.Type.values()) {
            if (request.isWebOnly()) {
                types.add(type.name());
            } else if (!type.isWebOnly()) {
                types.add(type.name());
            }
        }
        response.setTypes(types);
        response.setSuccessful(true);
        return response;
    }

    @Override
    @Locks({ @Lock(ns = Namespace.PUTAWAY, key = "#{#args[0].putawayCode}", level = Level.FACILITY) })
    public AddPutbackPendingItemToPutawayResponse addPutbackPendingItemToPutaway(AddPutbackPendingItemToPutawayRequest request) {
        AddPutbackPendingItemToPutawayResponse response = new AddPutbackPendingItemToPutawayResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            Putaway putaway = getPutawayByCode(request.getPutawayCode());
            Item item = inventoryService.getItemByCode(request.getItemCode());
            if (putaway == null) {
                context.addError(WsResponseCode.INVALID_PUTAWAY_CODE, "Invalid putaway code");
            } else if (!SystemConfiguration.TraceabilityLevel.ITEM.equals(ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).getTraceabilityLevel())) {
                context.addError(WsResponseCode.INVALID_REQUEST, "Operation only supported for ITEM traceability");
            } else if (!putaway.getUser().getId().equals(request.getUserId())) {
                context.addError(WsResponseCode.INVALID_USER_ID, "putaway created by another user");
            } else if (!Putaway.StatusCode.CREATED.name().equals(putaway.getStatusCode())) {
                context.addError(WsResponseCode.INVALID_PUTAWAY_STATE, "Items can be added only in CREATED state");
            } else if (!Putaway.Type.PUTAWAY_PICKLIST_ITEM.name().equals(putaway.getType())) {
                context.addError(WsResponseCode.INVALID_PUTAWAY_CODE, "putaway not of type PUTAWAY_PICKED_ITEMS");
            } else if (item == null) {
                context.addError(WsResponseCode.INVALID_ITEM_CODE, "Invalid Item Code");
            } else {
                PicklistItem picklistItem = pickerService.getPicklistItemForPutbackByItemCode(item.getCode());
                if (picklistItem == null) {
                    context.addError(WsResponseCode.INVALID_ITEM_CODE, "No putback item found");
                } else {
                    if (picklistItem.isPutbackAcknowledged()) {
                        doAddPutbackPendingItemToPutaway(request, response, context, putaway, picklistItem, false);
                    } else if (PicklistItem.StatusCode.INVENTORY_NOT_FOUND.equals(picklistItem.getStatusCode())) {
                        doAddPutbackPendingItemToPutaway(request, response, context, putaway, picklistItem, picklistItem.getShelfCode(), true);
                    } else {
                        context.addError(WsResponseCode.INVALID_ITEM_CODE, "Invalid item scanned");
                    }
                }
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @SuppressWarnings("unused")
    @Locks({ @Lock(ns = Namespace.ITEM, key = "#{#args[5]}", level = Level.FACILITY) })
    private void doAddPutbackPendingItemToPutaway(AddPutbackPendingItemToPutawayRequest request, AddPutbackPendingItemToPutawayResponse response, ValidationContext context,
            Putaway putaway, PicklistItem picklistItem, String shelfCode, boolean isNotFound) {
        doAddPutbackPendingItemToPutaway(request, response, context, putaway, picklistItem, isNotFound);
    }

    @Locks({
            @Lock(ns = Namespace.ITEM, key = "#{#args[0].itemCode}", level = Level.FACILITY),
            @Lock(ns = Namespace.PICKLIST, key = "#{#args[4].picklist.code}", level = Level.FACILITY) })
    @Transactional
    @RollbackOnFailure
    private void doAddPutbackPendingItemToPutaway(AddPutbackPendingItemToPutawayRequest request, AddPutbackPendingItemToPutawayResponse response, ValidationContext context,
            Putaway putaway, PicklistItem picklistItem, boolean isNotFound) {
        putaway = putawayDao.getPutawayById(putaway.getId());
        Item item = inventoryService.getItemByCode(request.getItemCode(), false);
        picklistItem = pickerService.getPicklistItemById(picklistItem.getId());
        if (!Item.StatusCode.PUTBACK_PENDING.name().equals(item.getStatusCode()) && !(isNotFound && Item.StatusCode.PICKED.name().equals(item.getStatusCode()))) {
            context.addError(WsResponseCode.INVALID_ITEM_STATE, "Invalid Item State: only PUTBACK_PENDING items can be added for putback");
        } else if (picklistItem == null) {
            context.addError(WsResponseCode.INVALID_ITEM_CODE, "No putback item found");
        } else {
            if (picklistItem.isPutbackAcknowledged()) {
                picklistItem.setStatusCode(PicklistItem.StatusCode.PUTBACK_COMPLETE);
            } else if (PicklistItem.StatusCode.INVENTORY_NOT_FOUND.equals(picklistItem.getStatusCode())) {
                // Decrement count of not found Inventory
                inventoryService.decrementNotFoundInventory(item, context);
            } else {
                context.addError(WsResponseCode.INVALID_ITEM_CODE, "Invalid item scanned");
            }
            if (!context.hasErrors()) {
                addItemToPutaway(item, putaway);
                response.setSuccessful(true);
            }
        }
    }

    @Override
    @Locks({ @Lock(ns = Namespace.PUTAWAY, key = "#{#args[0].putawayCode}", level = Level.FACILITY) })
    public AddNonTraceablePicklistItemsToPutawayResponse addNonTraceablePicklistItemsToPutaway(AddNonTraceablePicklistItemsToPutawayRequest request) {
        AddNonTraceablePicklistItemsToPutawayResponse response = new AddNonTraceablePicklistItemsToPutawayResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            Putaway putaway = getPutawayByCode(request.getPutawayCode());
            if (putaway == null) {
                context.addError(WsResponseCode.INVALID_PUTAWAY_CODE, "Invalid putaway code");
            } else if (SystemConfiguration.TraceabilityLevel.ITEM.equals(ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).getTraceabilityLevel())) {
                context.addError(WsResponseCode.INVALID_REQUEST, "Operation not supported for ITEM traceability");
            } else if (!putaway.getUser().getId().equals(request.getUserId())) {
                context.addError(WsResponseCode.INVALID_USER_ID, "putaway created by another user");
            } else if (!Putaway.StatusCode.CREATED.name().equals(putaway.getStatusCode())) {
                context.addError(WsResponseCode.INVALID_PUTAWAY_STATE, "Items can be added only in CREATED state");
            } else if (!Putaway.Type.PUTAWAY_PICKLIST_ITEM.name().equals(putaway.getType())) {
                context.addError(WsResponseCode.INVALID_PUTAWAY_CODE, "putaway not of type PUTAWAY_PICKED_ITEMS");
            } else {
                doAddNonTraceablePicklistItemsToPutaway(request, response, context, putaway.getId());
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        } else {
            response.setSuccessful(true);
        }
        return response;
    }

    @Transactional
    private void doAddNonTraceablePicklistItemsToPutaway(AddNonTraceablePicklistItemsToPutawayRequest request, AddNonTraceablePicklistItemsToPutawayResponse response,
            ValidationContext context, Integer putawayId) {

        Putaway putaway = putawayDao.getPutawayById(putawayId);
        List<PicklistItem> picklistItemsAddedInPutaway = new ArrayList<>();
        Map<String, ItemType> skuToItemType = new HashMap<>();
        Map<ItemTypeInventory.Type, Map<String, Integer>> typeToSkuToQuantity = new HashMap<>();
        request.getPutbackItems().stream().filter(putbackItem -> !context.hasErrors()).forEach(wsPutbackItem -> {
            PicklistItem.QCStatus qcStatus = Type.BAD_INVENTORY.equals(wsPutbackItem.getInventoryType()) ? PicklistItem.QCStatus.REJECTED : PicklistItem.QCStatus.ACCEPTED;
            skuToItemType.computeIfAbsent(wsPutbackItem.getSkuCode(), k -> catalogService.getItemTypeBySkuCode(wsPutbackItem.getSkuCode()));
            List<PicklistItem> picklistItems = pickerService.getPutbackAcceptedItemsBySkuCodeAndQCStatus(wsPutbackItem.getSkuCode(), qcStatus, wsPutbackItem.getQuantity());
            if (skuToItemType.get(wsPutbackItem.getSkuCode()) == null) {
                context.addError(WsResponseCode.INVALID_ITEM_SKU_CODE, "Invalid sku code : " + wsPutbackItem.getSkuCode());
            } else if (picklistItems.size() != wsPutbackItem.getQuantity()) {
                context.addError(WsResponseCode.INVALID_ITEM_SKU_CODE, "Quantity Mismatch of Putback Accepted Items for sku code : " + wsPutbackItem.getSkuCode()
                        + " inventory type : " + wsPutbackItem.getInventoryType().name());
            } else {
                typeToSkuToQuantity.computeIfAbsent(wsPutbackItem.getInventoryType(), k -> new HashMap<>()).put(wsPutbackItem.getSkuCode(), wsPutbackItem.getQuantity());
                picklistItemsAddedInPutaway.addAll(picklistItems);
            }
        });
        if (!context.hasErrors()) {
            Map<Type, Map<String, Map<Date, PutawayItem>>> typeToSkuCodeToAgeingDateToPutawayItemMap = getPutawayItemsMap(putaway);
            Date ageingDate = Constants.ITEM_SKU_OR_NONE_TRACEABILITY_AGEING_DATE;
            typeToSkuToQuantity.forEach((type, skuMap) -> skuMap.forEach((skuCode, quantity) -> {
                PutawayItem putawayItem = getPutawayItem(typeToSkuCodeToAgeingDateToPutawayItemMap, type, skuCode, ageingDate);
                if (putawayItem == null) {
                    putawayItem = new PutawayItem(putaway, type, ageingDate, skuToItemType.get(skuCode), quantity, 0, PutawayItem.StatusCode.CREATED.name(),
                            DateUtils.getCurrentTime(), DateUtils.getCurrentTime());
                    putawayItem = putawayDao.addPutawayItem(putawayItem);
                    putaway.getPutawayItems().add(putawayItem);
                } else {
                    putawayItem.setQuantity(putawayItem.getQuantity() + quantity);
                    putawayItem = putawayDao.updatePutawayItem(putawayItem);
                }
                response.getPutawayItems().add(preparePutawayItemDTO(putawayItem));
                response.setSuccessful(true);
            }));
            picklistItemsAddedInPutaway.forEach(picklistItem -> picklistItem.setStatusCode(PicklistItem.StatusCode.PUTBACK_COMPLETE));
        }

    }

    @Override
    @Transactional(readOnly = true)
    public Long getPutawaysPendingForShelfCount(int shelfId) {
        return putawayDao.getPutawaysPendingForShelfCount(shelfId);
    }

    public enum ScanType {
        ITEM,
        PACKAGE_CODE,
        SALE_ORDER_CODE,
        TRACKING_NUMBER
    }

}
