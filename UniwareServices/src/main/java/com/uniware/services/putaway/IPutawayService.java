/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 5, 2012
 *  @author singla
 */
package com.uniware.services.putaway;

import java.util.List;
import java.util.Map;

import com.unifier.core.api.validation.ValidationContext;
import com.unifier.scraper.sl.runtime.ScraperScript;
import com.uniware.core.api.gatepass.AddGatePassItemsToPutawayRequest;
import com.uniware.core.api.gatepass.AddGatePassItemsToPutawayResponse;
import com.uniware.core.api.item.AddInspectedNotBadItemToPutawayRequest;
import com.uniware.core.api.item.AddInspectedNotBadItemToPutawayResponse;
import com.uniware.core.api.item.AddNonTraceablePicklistItemsToPutawayRequest;
import com.uniware.core.api.item.AddNonTraceablePicklistItemsToPutawayResponse;
import com.uniware.core.api.item.AddPutbackPendingItemToPutawayRequest;
import com.uniware.core.api.item.AddPutbackPendingItemToPutawayResponse;
import com.uniware.core.api.putaway.AddInflowReceiptItemsToPutawayRequest;
import com.uniware.core.api.putaway.AddInflowReceiptItemsToPutawayResponse;
import com.uniware.core.api.putaway.AddNonTraceableItemToTransferPutawayRequest;
import com.uniware.core.api.putaway.AddNonTraceableItemToTransferPutawayResponse;
import com.uniware.core.api.putaway.AddTraceableItemToTransferPutawayRequest;
import com.uniware.core.api.putaway.AddTraceableItemToTransferPutawayResponse;
import com.uniware.core.api.putaway.CheckItemForPutawayRequest;
import com.uniware.core.api.putaway.CheckItemForPutawayResponse;
import com.uniware.core.api.putaway.CompleteItemPutawayRequest;
import com.uniware.core.api.putaway.CompleteItemPutawayResponse;
import com.uniware.core.api.putaway.CompletePutawayItemsRequest;
import com.uniware.core.api.putaway.CompletePutawayItemsResponse;
import com.uniware.core.api.putaway.CompletePutawayRequest;
import com.uniware.core.api.putaway.CompletePutawayResponse;
import com.uniware.core.api.putaway.CreatePutawayListRequest;
import com.uniware.core.api.putaway.CreatePutawayListResponse;
import com.uniware.core.api.putaway.CreatePutawayRequest;
import com.uniware.core.api.putaway.CreatePutawayResponse;
import com.uniware.core.api.putaway.DiscardPutawayRequest;
import com.uniware.core.api.putaway.DiscardPutawayResponse;
import com.uniware.core.api.putaway.EditPutawayItemRequest;
import com.uniware.core.api.putaway.EditPutawayItemResponse;
import com.uniware.core.api.putaway.GetPutawayRequest;
import com.uniware.core.api.putaway.GetPutawayResponse;
import com.uniware.core.api.putaway.GetPutawayTypesRequest;
import com.uniware.core.api.putaway.GetPutawayTypesResponse;
import com.uniware.core.api.putaway.GetShelfRequest;
import com.uniware.core.api.putaway.GetShelfResponse;
import com.uniware.core.api.putaway.ReleaseInventoryAndRepackageRequest;
import com.uniware.core.api.putaway.ReleaseInventoryAndRepackageResponse;
import com.uniware.core.api.putaway.SearchDirectReturnsRequest;
import com.uniware.core.api.putaway.SearchDirectReturnsResponse;
import com.uniware.core.api.putaway.SplitPutawayItemRequest;
import com.uniware.core.api.putaway.SplitPutawayItemResponse;
import com.uniware.core.api.returns.AddDirectReturnedSaleOrderItemsToPutawayRequest;
import com.uniware.core.api.returns.AddReturnedSaleOrderItemsToPutawayResponse;
import com.uniware.core.api.returns.AddReturnsSaleOrderItemsToPutawayRequest;
import com.uniware.core.api.returns.AddReturnsSaleOrderItemsToPutawayResponse;
import com.uniware.core.api.reversepickup.AddReversePickupSaleOrderItemsToPutawayRequest;
import com.uniware.core.api.reversepickup.AddReversePickupSaleOrderItemsToPutawayResponse;
import com.uniware.core.api.shipping.AddCancelledSaleOrderItemsToPutawayRequest;
import com.uniware.core.api.shipping.AddCancelledSaleOrderItemsToPutawayResponse;
import com.uniware.core.entity.Channel;
import com.uniware.core.entity.Item;
import com.uniware.core.entity.Putaway;
import com.uniware.core.entity.PutawayStatus;
import com.uniware.core.entity.SaleOrderItem;

/**
 * @author singla
 */
public interface IPutawayService {

    /**
     * @param request
     * @return
     */
    CreatePutawayResponse createPutaway(CreatePutawayRequest request);

    /**
     * @param request
     * @return
     */
    EditPutawayItemResponse editPutawayItem(EditPutawayItemRequest request);

    /**
     * @param request
     * @return
     */
    AddInflowReceiptItemsToPutawayResponse addInflowReceiptItemsToPutaway(AddInflowReceiptItemsToPutawayRequest request);

    /**
     * @param request
     * @return
     */
    CompletePutawayResponse completePutaway(CompletePutawayRequest request);

    /**
     * @param request
     * @return
     */
    CreatePutawayListResponse createPutawayList(CreatePutawayListRequest request);

    /**
     * @param request
     * @return
     */
    AddReturnsSaleOrderItemsToPutawayResponse addReturnsSaleOrderItemsToPutaway(AddReturnsSaleOrderItemsToPutawayRequest request);

    void markSaleOrderReturnedOnChannel(ValidationContext context, Channel channel, Map<String, List<SaleOrderItem>> typeTosaleOrderItemDetails,
            ScraperScript markSaleOrderReturnedScript);

    /**
     * @param request
     * @return
     */
    AddCancelledSaleOrderItemsToPutawayResponse addCancelledSaleOrderItemsToPutaway(AddCancelledSaleOrderItemsToPutawayRequest request);

    /**
     * @param request
     * @return
     */
    GetPutawayResponse getPutaway(GetPutawayRequest request);

    CompletePutawayItemsResponse completePutawayItems(CompletePutawayItemsRequest request);

    /**
     * @param request
     * @return
     */
    CompleteItemPutawayResponse completeItemPutaway(CompleteItemPutawayRequest request);

    /**
     * @param request
     * @return
     */
    DiscardPutawayResponse discardPutaway(DiscardPutawayRequest request);

    List<Item> getPendingItemsByPutawayId(int putawayId);

    GetShelfResponse getShelf(GetShelfRequest request);

    CheckItemForPutawayResponse checkItemForPutaway(CheckItemForPutawayRequest request);

    /**
     * @param request
     * @return
     */
    AddReversePickupSaleOrderItemsToPutawayResponse addReversePickupSaleOrderItemsToPutaway(AddReversePickupSaleOrderItemsToPutawayRequest request);

    /**
     * @param request
     * @return
     */
    AddGatePassItemsToPutawayResponse addGatePassItemsToPutaway(AddGatePassItemsToPutawayRequest request);

    /**
     * @param request
     * @return
     */
    AddInspectedNotBadItemToPutawayResponse addInspectedNotBadItemToPutaway(AddInspectedNotBadItemToPutawayRequest request);

    AddTraceableItemToTransferPutawayResponse addTraceableItemToTransferPutaway(AddTraceableItemToTransferPutawayRequest request);

    AddNonTraceableItemToTransferPutawayResponse addNonTraceableItemToTransferPutaway(AddNonTraceableItemToTransferPutawayRequest request);

    List<PutawayStatus> getPutawayStatuses();

    AddReturnedSaleOrderItemsToPutawayResponse addDirectReturnsSaleOrderItemsToPutaway(AddDirectReturnedSaleOrderItemsToPutawayRequest request);

    SearchDirectReturnsResponse searchDirectReturns(SearchDirectReturnsRequest request);

    Putaway getPutawayByCode(String putawayCode);

    Putaway getPutawayById(Integer putawayId);

    List<Item> getItemsByPutawayId(int putawayId);

    ReleaseInventoryAndRepackageResponse releaseInventoryAndRepackage(ReleaseInventoryAndRepackageRequest request);

    SplitPutawayItemResponse splitPutawayItem(SplitPutawayItemRequest request);

    GetPutawayTypesResponse getPutawayTypes(GetPutawayTypesRequest request);

    AddPutbackPendingItemToPutawayResponse addPutbackPendingItemToPutaway(AddPutbackPendingItemToPutawayRequest request);

    Long getPutawaysPendingForShelfCount(int id);

    AddNonTraceablePicklistItemsToPutawayResponse addNonTraceablePicklistItemsToPutaway(AddNonTraceablePicklistItemsToPutawayRequest request);
}
