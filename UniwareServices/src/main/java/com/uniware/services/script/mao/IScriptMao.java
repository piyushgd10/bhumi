/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 07-Apr-2014
 *  @author amit
 */
package com.uniware.services.script.mao;

import java.util.List;

import com.uniware.core.vo.UniwareScriptVO;
import com.uniware.core.vo.ScriptVersionVO;

public interface IScriptMao {

    List<String> lookupScripts(String keyword);

    List<UniwareScriptVO> getScriptsToLoadEagerly();

    List<ScriptVersionVO> getScriptVersions();

    UniwareScriptVO getScriptByNameAndVersion(String name, String version);

}
