/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 07-Apr-2014
 *  @author amit
 */
package com.uniware.services.script.service;

import java.util.List;

import com.unifier.core.api.tasks.EditScriptConfigRequest;
import com.unifier.core.api.tasks.EditScriptConfigResponse;
import com.unifier.core.api.tasks.ScriptConfigDTO;
import com.uniware.core.vo.UniwareScriptVO;
import com.uniware.core.vo.ScriptVersionVO;

public interface IScriptService {

    EditScriptConfigResponse editScriptConfig(EditScriptConfigRequest request);

    List<String> lookupScripts(String keyword);

	List<UniwareScriptVO> getScriptsToLoadEagerly();

    List<ScriptVersionVO> getSciptVersions();

    UniwareScriptVO getScriptVOByNameAndVersion(String name, String version);

    ScriptConfigDTO getScriptByName(String name);
}
