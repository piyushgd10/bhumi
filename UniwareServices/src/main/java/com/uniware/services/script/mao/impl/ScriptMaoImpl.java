/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 07-Apr-2014
 *  @author amit
 */
package com.uniware.services.script.mao.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.uniware.core.utils.UserContext;
import com.uniware.core.vo.UniwareScriptVO;
import com.uniware.core.vo.ScriptVersionVO;
import com.uniware.services.script.mao.IScriptMao;

@Repository
public class ScriptMaoImpl implements IScriptMao {

    @Autowired
	@Qualifier(value = "commonMongo")
    private MongoOperations mongoOperations;
    
    @Override
    public UniwareScriptVO getScriptByNameAndVersion(String name, String version) {
        return mongoOperations.findOne(new Query(Criteria.where("name").is(name).and("version").is(version)), UniwareScriptVO.class);
    }

    @Override
    public List<String> lookupScripts(String keyword) {
        Query q = new Query(Criteria.where("name").regex(keyword, "i"));
        q.fields().include("name");
        List<UniwareScriptVO> scripts = mongoOperations.find(q, UniwareScriptVO.class);
        List<String> names = new ArrayList<String>(scripts.size());
        for (UniwareScriptVO script : scripts) {
            names.add(script.getName());
        }
        return names;
    }

    @Override
    public List<UniwareScriptVO> getScriptsToLoadEagerly() {
        Query q = new Query(Criteria.where("loadEagerly").is(true));
        return mongoOperations.find(q, UniwareScriptVO.class);
    }

    @Override
    public List<ScriptVersionVO> getScriptVersions() {
        Query q = new Query(Criteria.where("tenantCode").is(UserContext.current().getTenant().getCode()));
        return mongoOperations.find(q, ScriptVersionVO.class);
    }

}
