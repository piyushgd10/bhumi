/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 07-Apr-2014
 *  @author amit
 */
package com.uniware.services.script.service.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unifier.core.api.tasks.EditScriptConfigRequest;
import com.unifier.core.api.tasks.EditScriptConfigResponse;
import com.unifier.core.api.tasks.ScriptConfigDTO;
import com.unifier.core.cache.CacheManager;
import com.unifier.scraper.sl.runtime.ScraperScript;
import com.unifier.services.aspect.MarkDirty;
import com.uniware.core.vo.ScriptVersionVO;
import com.uniware.core.vo.UniwareScriptVO;
import com.uniware.services.cache.ScriptVersionedCache;
import com.uniware.services.configuration.ScriptConfiguration;
import com.uniware.services.script.mao.IScriptMao;
import com.uniware.services.script.service.IScriptService;

@Service("scriptService")
public class ScriptServiceImpl implements IScriptService {

    @Autowired
    private IScriptMao scriptMao;

    @Override
    public UniwareScriptVO getScriptVOByNameAndVersion(String name, String version) {
        return scriptMao.getScriptByNameAndVersion(name, version);
    }

    @Override
    public ScriptConfigDTO getScriptByName(String name) {
        ScriptConfigDTO scriptConfigDto = null;
        String version = CacheManager.getInstance().getCache(ScriptVersionedCache.class).getScriptVersion(name);
        if (version != null) {
            UniwareScriptVO scriptConfig = scriptMao.getScriptByNameAndVersion(name, version);
                scriptConfigDto = new ScriptConfigDTO(scriptConfig);
        }
        return scriptConfigDto;
    }

    @Override
    @MarkDirty(values = { ScriptConfiguration.class })
    public EditScriptConfigResponse editScriptConfig(EditScriptConfigRequest request) {
        EditScriptConfigResponse response = new EditScriptConfigResponse();
//        ValidationContext context = request.validate();
//        if (!context.hasErrors()) {
//            ScriptVO scriptConfig = scriptMao.getScriptByName(request.getName());
//            if (scriptConfig == null) {
//                context.addError(WsResponseCode.INVALID_SCRIPT, "Invalid script name");
//            }
//            if (!context.hasErrors()) {
//                ScraperScriptNode scriptNode;
//                try {
//                    if (StringUtils.isNotBlank(request.getScript())) {
//                        scriptNode = ScraperScriptNode.parse(request.getScript());
//                        scriptNode.validate();
//                        scriptNode.compile();
//                    }
//                    scriptConfig.setScript(request.getScript());
//                    scriptMao.editScript(scriptConfig);
//                    response.setSuccessful(true);
//                } catch (Exception e) {
//                    context.addError(WsResponseCode.INVALID_SCRIPT, "unable to load script:" + scriptConfig.getName() + ", " + e);
//                }
//            }
//        }
//        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    public List<String> lookupScripts(String keyword) {
        List<String> scripts = scriptMao.lookupScripts(keyword);
        Set<String> names = new HashSet<String>();
        names.addAll(scripts);
        scripts = new ArrayList<String>(names);
        return scripts;
    }

    @Override
    public List<UniwareScriptVO> getScriptsToLoadEagerly() {
        return scriptMao.getScriptsToLoadEagerly();
    }
    
    @Override
    public List<ScriptVersionVO> getSciptVersions() {
        return scriptMao.getScriptVersions();
    }

}
