/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Oct 13, 2015
 *  @author      bhupi
 */
package com.uniware.services.pricing;

import java.util.Date;

import com.uniware.core.api.prices.dto.PriceDTO;

/**
 * Listens to price push milestones.
 */
public interface IPricingPushListener {
    
    void onPushStart(final String channelCode, final String channelProductId);
    
    void onPushSuccess(final String channelCode, final String channelProductId, final Date originalVersion, final PriceDTO prices, final String successMessage);
    
    void onPushFailure(final String channelCode, final String channelProductId, final Date originalVersion, final String failureMessage);
    
}
