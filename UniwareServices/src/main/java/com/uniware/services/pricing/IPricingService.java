/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Oct 13, 2015
 *  @author akshay
 */
package com.uniware.services.pricing;

import java.util.List;

import com.unifier.core.api.validation.WsError;
import com.uniware.core.api.prices.CheckChannelItemTypePriceRequest;
import com.uniware.core.api.prices.CheckChannelItemTypePriceResponse;
import com.uniware.core.api.prices.EditBulkChannelItemPriceRequest;
import com.uniware.core.api.prices.EditBulkChannelItemPriceResponse;
import com.uniware.core.api.prices.EditChannelItemTypePriceRequest;
import com.uniware.core.api.prices.EditChannelItemTypePriceResponse;
import com.uniware.core.api.prices.GetChannelItemTypePriceRequest;
import com.uniware.core.api.prices.GetChannelItemTypePriceResponse;
import com.uniware.core.api.prices.GetChannelItemTypePricesRequest;
import com.uniware.core.api.prices.GetChannelItemTypePricesResponse;
import com.uniware.core.api.prices.GetHistoricalChannelItemTypePriceRequest;
import com.uniware.core.api.prices.dto.PriceDTO;

/**
 * 
 * A service that manages item and channel-item level prices. It also supports time series prices, i.e. user can fetch 
 * historical prices by time.
 * <ul>
 * The responsibilities include:
 * <li>Fetching current prices for a given channel item.
 * <li>Fetching historical prices for a given channel item.
 * <li>Checking prices for a given channel item.
 * <li>Editing prices for a given channel item.
 * </ul>
 */
public interface IPricingService {

    /** Get channel item prices from uniware. */
    GetChannelItemTypePriceResponse getChannelItemTypePrice(final GetChannelItemTypePriceRequest request);
    
    /** Get historical channel item prices from uniware. */
    GetChannelItemTypePriceResponse getHistoricalChannelItemTypePrice(final GetHistoricalChannelItemTypePriceRequest request);
    
    /** Validate prices in the request and recompute derived ones. */
    CheckChannelItemTypePriceResponse checkChannelItemTypePrice(final CheckChannelItemTypePriceRequest request);

    /** Edit channel item prices in uniware. */
    EditChannelItemTypePriceResponse editChannelItemTypePrice(final EditChannelItemTypePriceRequest request);

    /** Edit bulk channel item prices in uniware. */
    EditBulkChannelItemPriceResponse editChannelItemTypePrice(final EditBulkChannelItemPriceRequest request);
    
    /** Edit channel item prices in uniware for seller sku */
    GetChannelItemTypePricesResponse getChannelItemTypePrices(final GetChannelItemTypePricesRequest request);

    /** Validate channel prices */
    List<WsError> validatePrices(PriceDTO oldPrices, PriceDTO newPrices, String channelCode);
}
