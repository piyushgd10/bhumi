/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Oct 13, 2015
 *  @author akshay
 */
package com.uniware.services.pricing.impl;

import com.codahale.metrics.annotation.Timed;
import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.api.validation.WsError;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.ValidatorUtils;
import com.uniware.core.api.channel.ChannelDetailDTO;
import com.uniware.core.api.prices.CheckChannelItemTypePriceRequest;
import com.uniware.core.api.prices.CheckChannelItemTypePriceResponse;
import com.uniware.core.api.prices.EditBulkChannelItemPriceRequest;
import com.uniware.core.api.prices.EditBulkChannelItemPriceResponse;
import com.uniware.core.api.prices.EditChannelItemTypePriceRequest;
import com.uniware.core.api.prices.EditChannelItemTypePriceResponse;
import com.uniware.core.api.prices.GetChannelItemTypePriceRequest;
import com.uniware.core.api.prices.GetChannelItemTypePriceResponse;
import com.uniware.core.api.prices.GetChannelItemTypePricesRequest;
import com.uniware.core.api.prices.GetChannelItemTypePricesResponse;
import com.uniware.core.api.prices.GetHistoricalChannelItemTypePriceRequest;
import com.uniware.core.api.prices.MergeChannelItemTypePriceRequest;
import com.uniware.core.api.prices.dto.ListingPricesDTO;
import com.uniware.core.api.prices.dto.PriceDTO;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.entity.Channel;
import com.uniware.core.entity.ChannelItemType;
import com.uniware.core.entity.ChannelItemTypePrice;
import com.uniware.core.entity.ItemType;
import com.uniware.core.utils.UserContext;
import com.uniware.core.vo.ChannelPriceUpdateVO;
import com.uniware.dao.pricing.IPricingDao;
import com.uniware.dao.pricing.IPricingMao;
import com.uniware.services.cache.ChannelCache;
import com.uniware.services.catalog.ICatalogService;
import com.uniware.services.channel.IChannelCatalogService;
import com.uniware.services.configuration.data.manager.ProductConfiguration;
import com.uniware.services.pricing.IPricingPushListener;
import com.uniware.services.pricing.IPricingService;
import com.uniware.services.pricing.PricingSourceConfiguration;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * An implementation of pricing service that treats uniware pricing store as authoritative.
 * <p>
 * Currently item level price management is not implemented. Instead sellers can manage prices at a channel-item level
 * only.
 */
@Service("uniwarePricingService")
//@Metrics(registry = "service-registry")
public class PricingServiceImpl implements IPricingService, IPricingPushListener {

    private static final Logger     LOG               = LoggerFactory.getLogger(PricingServiceImpl.class);

    private static final int        MAX_RETRY_COUNT   = 5;
    //private static final String     SERVICE_METRICS   = "service-metrics";
    private static final String     MSP               = "MSP";
    private static final String     MRP               = "MRP";
    private static final String     SELLING_PRICE     = "SELLING_PRICE";
    private static final BigDecimal MAX_PRICE_ALLOWED = BigDecimal.valueOf(1000000000);

    //private final MetricRegistry    serviceMetrics    = SharedMetricRegistries.getOrCreate(SERVICE_METRICS);

    @Autowired
    private IChannelCatalogService  channelCatalogService;

    @Autowired
    private IPricingDao             pricingDao;

    @Autowired
    private IPricingMao             pricingMao;

    @Autowired
    private ICatalogService         catalogService;

    @Autowired
    private IChannelCatalogService  ChannelCatalogService;

    /**
     * {@inheritDoc}
     */
    @Override
    @Timed
    @Transactional
    public GetChannelItemTypePriceResponse getChannelItemTypePrice(final GetChannelItemTypePriceRequest request) {
        GetChannelItemTypePriceResponse response = new GetChannelItemTypePriceResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            Channel channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelByCode(request.getChannelCode());
            if (channel == null) {
                context.addError(WsResponseCode.INVALID_CHANNEL_CODE, "Invalid channel code " + request.getChannelCode());
            } else {
                ChannelItemTypePrice channelItemTypePrice = getChannelItemTypePriceByChannelAndChannelProductId(channel.getId(), request.getChannelProductId());
                if (channelItemTypePrice == null) {
                    context.addError(WsResponseCode.INVALID_CHANNEL_ITEM_TYPE, "No prices available for channel product id " + request.getChannelProductId());
                } else {
                    PriceDTO priceDto = new PriceDTO(channelItemTypePrice);
                    response.setChannelItemTypePrice(priceDto);
                    response.setDirty(channelItemTypePrice.isDirty());
                    response.setVersion(channelItemTypePrice.getUpdated());
                }
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        response.setSuccessful(!context.hasErrors());
        return response;
    }

    /**
     * Unsupported.
     */
    @Override
    public CheckChannelItemTypePriceResponse checkChannelItemTypePrice(final CheckChannelItemTypePriceRequest request) {
        throw new UnsupportedOperationException();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Timed
    @Transactional
    public EditChannelItemTypePriceResponse editChannelItemTypePrice(final EditChannelItemTypePriceRequest request) {
        EditChannelItemTypePriceResponse response = new EditChannelItemTypePriceResponse();
        ValidationContext context = request.validate();
        try {
            if (!context.hasErrors()) {
                Channel channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelByCode(request.getChannelCode());
                if (!channel.isEnabled()) {
                    LOG.info("Channel disabled: {}", channel.getCode());
                    response.setMessage("Channel disabled");
                    context.addError(WsResponseCode.BAD_CHANNEL_STATE, "Channel disabled");
                } else if (!Channel.SyncStatus.ON.equals(channel.getPricingSyncStatus())) {
                    LOG.info("Channel pricing sync disabled: {}", channel.getCode());
                    response.setMessage("Pricing Sync disabled");
                    context.addError(WsResponseCode.PRICE_SYNC_NOT_CONFIGURED, "Pricing Sync disabled");
                } else {
                    ChannelItemTypePrice channelItemTypePrice = getChannelItemTypePriceByChannelAndChannelProductId(channel.getId(), request.getChannelProductId());
                    if (channelItemTypePrice == null) {
                        ChannelItemType channelItemType = getChannelItemType(channel.getId(), request.getChannelProductId());
                        if (channelItemType == null) {
                            context.addError(WsResponseCode.INVALID_CHANNEL_ITEM_TYPE, "No channel item type exists with " + request.getChannelProductId());
                            response.setErrors(context.getErrors());
                            return response;
                        } else {
                            channelItemTypePrice = new ChannelItemTypePrice();
                            channelItemTypePrice.setChannel(channel);
                            channelItemTypePrice.setChannelItemType(channelItemType);
                            channelItemTypePrice.setChannelProductId(request.getChannelProductId());
                            channelItemTypePrice.setCreated(DateUtils.getCurrentTime());
                            channelItemTypePrice.setCurrencyCode(request.getCurrencyCode());
                            LOG.info("Creating new channel item type price for " + request.getChannelProductId() + " on " + request.getChannelCode());
                        }
                    }

                    if (channelItemTypePrice.isDirty() && !request.isForceEdit()) {
                        LOG.info("Skipping update since old updates are still pending for " + request.getChannelProductId() + " on " + request.getChannelCode());
                        context.addError(WsResponseCode.PENDING_PRICE_UPDATES, "Price updates are already pending for " + request.getChannelProductId());
                    } else if (hasChanged(request, channelItemTypePrice)) {
                        if (request instanceof MergeChannelItemTypePriceRequest) {
                            response.setChannelItemTypePrice(doMergeChannelItemTypePrice((MergeChannelItemTypePriceRequest) request, channelItemTypePrice, context));
                        } else {
                            // We allow manual edit of prices, only if it is eligible for inventory sync.
                            if (!isEligibleForInventorySync(channelItemTypePrice)) {
                                context.addError(WsResponseCode.INVALID_CHANNEL_ITEM_TYPE, "Prices can be edited only for sync enabled channel item types. Please fix "
                                        + "your inventory sync issues.");
                            } else {
                                response.setChannelItemTypePrice(doEditChannelItemTypePrice(request, channelItemTypePrice, context));
                            }
                        }
                    } else if (request.isForceMarkDirty()) {
                        // no changes b/w requested prices and current prices
                        // but were asked to mark as dirty, nonetheless; oblige.
                        LOG.info("Marking {} prices as dirty and clearing disable-due-to-push-errors flag", request.getChannelProductId());
                        channelItemTypePrice.setDirty(true);
                        channelItemTypePrice.setDisabledDueToPushErrors(false);
                    } else {
                        // No diff in request and the original value
                        // Success but let the user know we're skipping.
                        response.setNoChangeDetected(true);
                        LOG.info("Skipping update since no changes detected in the request for " + request.getChannelProductId() + " on " + request.getChannelCode());
                        response.setMessage("Skipping update since no changes detected in the request");
                    }
                }
            }
        } catch (Exception e) {
            LOG.error("Error editing prices in uniware for channelProductId: " + request.getChannelProductId(), e);
            context.addError(WsResponseCode.UNABLE_TO_UPDATE_PRICE, e.getMessage());
            response.setMessage(e.getMessage());
        }
        response.setSuccessful(!context.hasErrors());
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    /**
     * Unsupported.
     */
    @Override
    public EditBulkChannelItemPriceResponse editChannelItemTypePrice(EditBulkChannelItemPriceRequest request) {
        throw new UnsupportedOperationException("Uniware price mangement doesn't support bulk update yet");
    }

    /**
     * Returns true if the channel item type is eligible for inventory sync. Once edited, we always try to push them to
     * channel, unless they were de-linked or manually disabled.
     */
    private boolean isEligibleForInventorySync(final ChannelItemTypePrice channelItemTypePrice) {
        if (null == channelItemTypePrice) {
            return false;
        }
        ChannelItemType channelItemType = channelItemTypePrice.getChannelItemType();
        return (channelItemType.getStatusCode() == ChannelItemType.Status.LINKED || ConfigurationManager.getInstance().getConfiguration(ProductConfiguration.class).isProductManagementSwitchedOff())
                && (channelItemType.getListingStatus() == ChannelItemType.ListingStatus.ACTIVE || channelItemType.getListingStatus() == ChannelItemType.ListingStatus.INACTIVE)
                && !channelItemType.isDisabled() && !channelItemType.isDisabledDueToErrors() && channelItemType.isVerified();
    }

    /**
     * Returns true only if there are price changes that we'd want to propagate
     */
    private boolean hasChanged(final EditChannelItemTypePriceRequest request, final ChannelItemTypePrice channelItemTypePrice) {
        if (!channelItemTypePrice.getCurrencyCode().equals(request.getCurrencyCode())) {
            return true;
        }
        if (!isSamePrice(request.getSellingPrice(), channelItemTypePrice.getSellingPrice()) || !isSamePrice(request.getMsp(), channelItemTypePrice.getMsp())
                || !isSamePrice(request.getMrp(), channelItemTypePrice.getMrp())) {
            return true;
        }
        if (request instanceof MergeChannelItemTypePriceRequest) {
            MergeChannelItemTypePriceRequest mergeRequest = (MergeChannelItemTypePriceRequest) request;
            return !isSamePrice(mergeRequest.getCompetitivePrice(), channelItemTypePrice.getCompetitivePrice())
                    || !isSamePrice(mergeRequest.getTransferPrice(), channelItemTypePrice.getTransferPrice());
        }
        // Nothing has changed.
        return false;
    }

    private boolean isSamePrice(final BigDecimal requestPrice, final BigDecimal oldPrice) {
        if (requestPrice == null) {
            // null request price indicates no change.
            return true;
        }
        if (oldPrice == null) {
            // oldPrice is null whereas requested price is not
            return false;
        }
        // Do a scale agnostic comparison
        return requestPrice.compareTo(oldPrice) == 0;
    }

    @Transactional
    private PriceDTO doEditChannelItemTypePrice(final EditChannelItemTypePriceRequest updateRequest, final ChannelItemTypePrice original, final ValidationContext context) {
        PriceDTO pricesToValidate = new PriceDTO(original);
        // Apply edits over copy of original (pricesToValidate)
        pricesToValidate.setCurrencyCode(updateRequest.getCurrencyCode());
        pricesToValidate.setSellingPrice(ValidatorUtils.getOrDefaultValue(updateRequest.getSellingPrice(), null));
        pricesToValidate.setMrp(ValidatorUtils.getOrDefaultValue(updateRequest.getMrp(), null));
        pricesToValidate.setMsp(ValidatorUtils.getOrDefaultValue(updateRequest.getMsp(), null));

        // validate this copy
        PriceDTO oldPrices = new PriceDTO(original.getMsp(), original.getMrp(), original.getSellingPrice(), null, null, original.getCurrencyCode());
        context.addErrors(validatePrices(oldPrices, pricesToValidate, original.getChannel().getCode()));

        if (context.hasErrors()) {
            return null;
        }

        // fail-safe counters
        updateFailSafeMetricCounters(updateRequest, original);

        if (!isSamePrice(updateRequest.getSellingPrice(), original.getSellingPrice())) {
            // For SP -> TP, CP are derived fields. We mark them as null, to indicate that they are
            // going to be recomputed.
            original.setCompetitivePrice(null);
            original.setTransferPrice(null);
        }
        original.setSellingPrice(ValidatorUtils.getOrDefaultValue(pricesToValidate.getSellingPrice(), original.getSellingPrice()));
        original.setMrp(ValidatorUtils.getOrDefaultValue(pricesToValidate.getMrp(), original.getMrp()));
        original.setMsp(ValidatorUtils.getOrDefaultValue(pricesToValidate.getMsp(), original.getMsp()));
        original.setCurrencyCode(pricesToValidate.getCurrencyCode());

        // Clear disabled due to errors flag since we have a new edit/merge
        original.setDisabledDueToPushErrors(false);
        original.setDirty(true);

        update(original);
        savePriceSnpapshot(original, ChannelPriceUpdateVO.Op.LOCAL_EDIT, ChannelPriceUpdateVO.Status.SUCCESS);
        return new PriceDTO(original);
    }

    /**
     * Update few fail-safe metric counters that we could alarm on.
     */
    private void updateFailSafeMetricCounters(final EditChannelItemTypePriceRequest updateRequest, final ChannelItemTypePrice original) {
        if (!isSamePrice(updateRequest.getSellingPrice(), original.getSellingPrice())) {
            String tenant = UserContext.current().getTenant().getCode();
            //serviceMetrics.counter(MetricRegistry.name(PricingServiceImpl.class, "sp-edited")).inc();
            //serviceMetrics.counter(MetricRegistry.name(PricingServiceImpl.class, "sp-edited", tenant)).inc();
            if (original.getSellingPrice()==null || (updateRequest.getSellingPrice().doubleValue() < 0.50 * original.getSellingPrice().doubleValue())) {
              //  serviceMetrics.counter(MetricRegistry.name(PricingServiceImpl.class, "sp-drastic-reduction")).inc();
               // serviceMetrics.counter(MetricRegistry.name(PricingServiceImpl.class, "sp-drastic-reduction", tenant)).inc();
                LOG.warn("Selling price has been reduced by a huge margin {} -> {} for {}", new Object[] {
                        original.getSellingPrice(),
                        updateRequest.getSellingPrice(),
                        updateRequest.getChannelProductId() });
            }
        }
        if (!isSamePrice(updateRequest.getMrp(), original.getMrp())) {
            //serviceMetrics.counter(MetricRegistry.name(PricingServiceImpl.class, "mrp-edited")).inc();
            String tenant = UserContext.current().getTenant().getCode();
            //serviceMetrics.counter(MetricRegistry.name(PricingServiceImpl.class, "mrp-edited", tenant)).inc();
        }
        if (!isSamePrice(updateRequest.getMsp(), original.getMsp())) {
            //serviceMetrics.counter(MetricRegistry.name(PricingServiceImpl.class, "msp-edited")).inc();
            String tenant = UserContext.current().getTenant().getCode();
            //serviceMetrics.counter(MetricRegistry.name(PricingServiceImpl.class, "msp-edited", tenant)).inc();
        }
    }

    @Transactional
    private PriceDTO doMergeChannelItemTypePrice(final MergeChannelItemTypePriceRequest mergeRequest, final ChannelItemTypePrice original, final ValidationContext context) {
        original.setSellingPrice(mergeRequest.getSellingPrice());
        original.setMrp(mergeRequest.getMrp());
        original.setMsp(mergeRequest.getMsp());
        original.setCompetitivePrice(mergeRequest.getCompetitivePrice());
        original.setTransferPrice(mergeRequest.getTransferPrice());
        original.setCurrencyCode(mergeRequest.getCurrencyCode());

        original.setChannelCompetitivePrice(mergeRequest.getCompetitivePrice());
        original.setChannelTransferPrice(mergeRequest.getTransferPrice());
        original.setChannelMsp(mergeRequest.getMsp());
        original.setChannelSellingPrice(mergeRequest.getSellingPrice());
        original.setChannelMrp(mergeRequest.getMrp());
        original.setChannelCurrencyCode(mergeRequest.getCurrencyCode());
        // Clear disabled due to errors flag since we have a new merge
        original.setDisabledDueToPushErrors(false);
        original.setLastPullAt(DateUtils.getCurrentTime());
        original.setDirty(false);

        update(original);
        savePriceSnpapshot(original, ChannelPriceUpdateVO.Op.CHANNEL_PULL, ChannelPriceUpdateVO.Status.SUCCESS);
        return new PriceDTO(original);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Timed
    public GetChannelItemTypePriceResponse getHistoricalChannelItemTypePrice(final GetHistoricalChannelItemTypePriceRequest request) {
        throw new UnsupportedOperationException("Uniware price mangement doesn't support historical prices yet");
    }

    private ChannelItemType getChannelItemType(final int channelCode, final String channelProductId) {
        return channelCatalogService.getChannelItemTypeByChannelAndChannelProductId(channelCode, channelProductId);
    }

    @Transactional
    private ChannelItemTypePrice getChannelItemTypePriceByChannelAndChannelProductId(final int channelId, final String channelProductId) {
        return pricingDao.getChannelItemTypePriceByChannelAndChannelProductId(channelId, channelProductId);
    }

    @Transactional
    private ChannelItemTypePrice update(ChannelItemTypePrice channelItemTypePrice) {
        return pricingDao.update(channelItemTypePrice);
    }

    @Transactional
    private boolean updateConditional(final ChannelItemTypePrice channelItemTypePrice, final Date maxVersion) {
        return pricingDao.updateChannelItemTypePriceIfFresh(channelItemTypePrice, maxVersion);
    }

    private void savePriceSnpapshot(final ChannelItemTypePrice prices, final ChannelPriceUpdateVO.Op operation, final ChannelPriceUpdateVO.Status status) {
        ChannelPriceUpdateVO channelPriceUpdateVO = new ChannelPriceUpdateVO();
        Channel channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelById(prices.getChannel().getId());
        channelPriceUpdateVO.setChannelCode(channel.getCode());
        channelPriceUpdateVO.setChannelProductId(prices.getChannelProductId());
        channelPriceUpdateVO.setSellerSkuCode(prices.getChannelItemType().getSellerSkuCode());
        if (prices.getChannelItemType().getItemType() != null) {
            ItemType itemType = catalogService.getAllItemTypeById(prices.getChannelItemType().getItemType().getId());
            channelPriceUpdateVO.setUniwareSkuCode(itemType.getSkuCode());
        }
        channelPriceUpdateVO.setSellingPrice(prices.getSellingPrice());
        channelPriceUpdateVO.setTransferPrice(prices.getTransferPrice());
        channelPriceUpdateVO.setCompetitivePrice(prices.getCompetitivePrice());
        channelPriceUpdateVO.setMsp(prices.getMsp());
        channelPriceUpdateVO.setMrp(prices.getMrp());
        channelPriceUpdateVO.setCurrencyCode(prices.getCurrencyCode());

        channelPriceUpdateVO.setOperation(operation.toString());
        channelPriceUpdateVO.setStatusCode(status.toString());
        if (ChannelPriceUpdateVO.Op.CHANNEL_PUSH == operation) {
            channelPriceUpdateVO.setMessage(prices.getLastPushMessage());
        }

        pricingMao.createPriceSnapshot(channelPriceUpdateVO);
    }

    @Override
    public void onPushStart(final String channelCode, final String channelProductId) {
    }

    @Override
    public void onPushSuccess(final String channelCode, final String channelProductId, final Date originalVersion, final PriceDTO prices, final String successMessage) {
        LOG.warn("Pushing prices from uniware -> channel for {} sucessful: {}", channelProductId, successMessage);
        // updating push status on channel to local sink
        Channel channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelByCode(channelCode);
        ChannelItemTypePrice channelItemTypePrice = getChannelItemTypePriceByChannelAndChannelProductId(channel.getId(), channelProductId);
        channelItemTypePrice.setLastPushMessage(successMessage);
        channelItemTypePrice.setLastPushStatus(ChannelPriceUpdateVO.Status.SUCCESS.name());
        channelItemTypePrice.setLastUpdateAttemptAt(DateUtils.getCurrentTime());
        channelItemTypePrice.setPushFailedCount(0);
        channelItemTypePrice.setDisabledDueToPushErrors(false);
        if (null != prices) {
            channelItemTypePrice.setSellingPrice(prices.getSellingPrice());
            channelItemTypePrice.setTransferPrice(prices.getTransferPrice());
            channelItemTypePrice.setMsp(prices.getMsp());
            channelItemTypePrice.setMrp(prices.getMrp());
            channelItemTypePrice.setCompetitivePrice(prices.getCompetitivePrice());
            channelItemTypePrice.setCurrencyCode(prices.getCurrencyCode());
            channelItemTypePrice.setChannelCompetitivePrice(prices.getCompetitivePrice());
            channelItemTypePrice.setChannelTransferPrice(prices.getTransferPrice());
            channelItemTypePrice.setChannelMsp(prices.getMsp());
            channelItemTypePrice.setChannelSellingPrice(prices.getSellingPrice());
            channelItemTypePrice.setChannelMrp(prices.getMrp());
            channelItemTypePrice.setChannelCurrencyCode(prices.getCurrencyCode());
            channelItemTypePrice.setDirty(false);
            LOG.info("Pushing resulting derived prices from channel -> uniware for " + channelProductId);
        }
        if (!updateConditional(channelItemTypePrice, originalVersion)) {
            LOG.warn("Skipping channelProductId update for : {}, since it has some newer updates", channelProductId);
        } else if (null != prices) {
            // We're not ignoring the push request and there are price changes to record
            savePriceSnpapshot(channelItemTypePrice, ChannelPriceUpdateVO.Op.CHANNEL_PUSH, ChannelPriceUpdateVO.Status.SUCCESS);
        }
    }

    @Override
    public void onPushFailure(final String channelCode, final String channelProductId, final Date originalVersion, final String failureMessage) {
        LOG.warn("Skipping channelProductId price sync for : {}, error : {}", channelProductId, failureMessage);
        Channel channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelByCode(channelCode);
        ChannelItemTypePrice channelItemTypePrice = getChannelItemTypePriceByChannelAndChannelProductId(channel.getId(), channelProductId);
        if (null == channelItemTypePrice) {
            LOG.warn("channelItemTypePrice not found for channelProductId {}. Skipping the failureCount update", channelProductId);
            return;
        }
        if (channelItemTypePrice.getPushFailedCount() >= MAX_RETRY_COUNT) {
            channelItemTypePrice.setDisabledDueToPushErrors(true);
        } else {
            channelItemTypePrice.setPushFailedCount(channelItemTypePrice.getPushFailedCount() + 1);
        }
        channelItemTypePrice.setLastPushMessage(failureMessage);
        channelItemTypePrice.setLastPushStatus(ChannelPriceUpdateVO.Status.FAILED.name());
        channelItemTypePrice.setLastUpdateAttemptAt(DateUtils.getCurrentTime());
        if (!updateConditional(channelItemTypePrice, originalVersion)) {
            LOG.warn("Skipping channelProductId update for : {}, since it has some newer updates", channelProductId);
        } else {
            // We're not ignoring the push request
            savePriceSnpapshot(channelItemTypePrice, ChannelPriceUpdateVO.Op.CHANNEL_PUSH, ChannelPriceUpdateVO.Status.FAILED);
        }
    }

    @Override
    @Transactional
    public GetChannelItemTypePricesResponse getChannelItemTypePrices(final GetChannelItemTypePricesRequest request) {
        GetChannelItemTypePricesResponse response = new GetChannelItemTypePricesResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            ChannelCache channelCache = CacheManager.getInstance().getCache(ChannelCache.class);
            Map<String, Boolean> channelCodeToPriceSyncEnabled = new HashMap<>();
            for (ChannelDetailDTO channel : channelCache.getChannels()) {
                channelCodeToPriceSyncEnabled.put(channel.getCode(), 
                        Channel.SyncStatus.ON.name().equals(channel.getPricingSyncStatus()));
            }
            List<ListingPricesDTO> listingPrices = new ArrayList<>();
            for (String sellerSkuCode : request.getSellerSkuCodes()) {
                List<ChannelItemType> channelItemTypes = channelCatalogService.getChannelItemTypeBySellerSkuCode(sellerSkuCode);
                if (!channelItemTypes.isEmpty()) {
                    for (ChannelItemType channelItemType : channelItemTypes) {
                        ListingPricesDTO listingPrice = new ListingPricesDTO();
                        listingPrice.setChannelCode(channelItemType.getChannel().getCode());
                        listingPrice.setChannelProductId(channelItemType.getChannelProductId());
                        if (ConfigurationManager.getInstance().getConfiguration(ProductConfiguration.class).isProductManagementSwitchedOff()) {
                            listingPrice.setProductName(channelItemType.getProductName());
                        } else if (channelItemType.getItemType() != null) {
                            listingPrice.setProductName(channelItemType.getItemType().getName());
                            listingPrice.setImageUrl(channelItemType.getItemType().getImageUrl());
                        }
                        listingPrice.setSellerSkuCode(channelItemType.getSellerSkuCode());
                        if (channelCodeToPriceSyncEnabled.get(channelItemType.getChannel().getCode()) 
                                && channelItemType.getChannelItemTypePrice() != null) {
                            listingPrice.setPrices(new PriceDTO(channelItemType.getChannelItemTypePrice()));
                        }
                        listingPrices.add(listingPrice);
                    }
                }
            }
            response.setListingPrices(listingPrices);
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        response.setSuccessful(!context.hasErrors());
        return response;
    }

    @Transactional(readOnly = true)
    public List<ChannelItemTypePrice> getDirtyChannelItemTypePricesByChannel(final int channelId, final int start, final int batchSize, final Date lastUpdated) {
        if (ConfigurationManager.getInstance().getConfiguration(ProductConfiguration.class).isProductManagementSwitchedOff()) {
            return pricingDao.getDirtyChannelItemTypePrices(channelId, start, batchSize, lastUpdated, ChannelItemType.Status.UNLINKED);
        } else {
            return pricingDao.getDirtyChannelItemTypePrices(channelId, start, batchSize, lastUpdated, ChannelItemType.Status.LINKED);
        }
    }

    @Transactional
    public long getDirtyChannelItemTypePriceCount(int channelId, Date lastUpdated) {
        if (ConfigurationManager.getInstance().getConfiguration(ProductConfiguration.class).isProductManagementSwitchedOff()) {
            return pricingDao.getDirtyChannelItemTypePriceCount(channelId, lastUpdated, ChannelItemType.Status.UNLINKED);
        } else {
            return pricingDao.getDirtyChannelItemTypePriceCount(channelId, lastUpdated, ChannelItemType.Status.LINKED);
        }
    }

    @Transactional
    public long getChannelItemTypePriceCount(int channelId, String lastPriceSyncStatus) {
        return pricingDao.getChannelItemTypePriceCount(channelId, lastPriceSyncStatus);
    }

    @Transactional
    public long getChannelItemTypePriceCount(int channelId, final String lastPriceUpdateStatus, final DateUtils.DateRange updateRange) {
        return pricingDao.getChannelItemTypePriceCount(channelId, lastPriceUpdateStatus, updateRange);
    }

    public List<WsError> validatePrices(final PriceDTO oldPrices, final PriceDTO newPrices, final String channelCode) {
        Channel channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelByCode(channelCode);
        if (channel == null) {
            return Collections.singletonList(new WsError(WsResponseCode.INVALID_CHANNEL_CODE.code(), WsResponseCode.INVALID_CHANNEL_CODE.message(), "Invalid channel code "));
        }
        if (newPrices.getMsp() == null && newPrices.getMrp() == null && newPrices.getSellingPrice() == null) {
            return Collections.singletonList(new WsError(WsResponseCode.INVALID_REQUEST.code(), WsResponseCode.INVALID_REQUEST.message(),
                    "Either of msp | msp | selling price is required to update"));
        }

        PricingSourceConfiguration pricingConfig = new PricingSourceConfiguration(channel.getSourceCode());
        List<WsError> errors = new ArrayList<WsError>();

        if (!pricingConfig.isCurrencyCodeSupported(newPrices.getCurrencyCode())) {
            errors.add(new WsError(WsResponseCode.INVALID_CURRENCY_CODE.code(), WsResponseCode.INVALID_CURRENCY_CODE.message(), "Currency code invalid for source "
                    + pricingConfig.getSourceCode()));
        }
        if (!newPrices.getCurrencyCode().equals(oldPrices.getCurrencyCode())) {
            // Currency code change is not supported currently.
            errors.add(new WsError(WsResponseCode.INVALID_REQUEST.code(), WsResponseCode.INVALID_REQUEST.message(), "Currency code change is not supported for "
                    + pricingConfig.getSourceCode()));
        }

        if (!pricingConfig.isFractionalPriceSupported()) {
            if (!isNullOrPositiveInt(newPrices.getSellingPrice()) || !isNullOrPositiveInt(newPrices.getMrp()) || !isNullOrPositiveInt(newPrices.getMsp())) {
                errors.add(new WsError(WsResponseCode.INVALID_REQUEST.code(), WsResponseCode.INVALID_REQUEST.message(), pricingConfig.getSourceCode()
                        + " supports only +ve integer values for prices"));
            }
        }

        BigDecimal sellingPrice = newPrices.getSellingPrice();
        BigDecimal msp = newPrices.getMsp();
        BigDecimal mrp = newPrices.getMrp();

        if (sellingPrice != null && !pricingConfig.isPriceFieldEditable(SELLING_PRICE)) {
            errors.add(new WsError(WsResponseCode.INVALID_REQUEST.code(), WsResponseCode.INVALID_REQUEST.message(), "Selling price is not editable for source "
                    + pricingConfig.getSourceCode()));
        }
        if (msp != null && !pricingConfig.isPriceFieldEditable((MSP))) {
            errors.add(new WsError(WsResponseCode.INVALID_REQUEST.code(), WsResponseCode.INVALID_REQUEST.message(), "MSP price is not editable for source "
                    + pricingConfig.getSourceCode()));
        }
        if (mrp != null && !pricingConfig.isPriceFieldEditable(MRP)) {
            errors.add(new WsError(WsResponseCode.INVALID_REQUEST.code(), WsResponseCode.INVALID_REQUEST.message(), "MRP price is not editable for source "
                    + pricingConfig.getSourceCode()));
        }
        if (errors.isEmpty()) {
            msp = msp != null ? msp : sellingPrice != null ? sellingPrice : mrp != null ? mrp : BigDecimal.ZERO;
            sellingPrice = sellingPrice != null ? sellingPrice : msp != null ? msp : mrp != null ? mrp : BigDecimal.ZERO;
            mrp = mrp != null ? mrp : sellingPrice != null ? sellingPrice : msp != null ? msp : BigDecimal.ZERO;
            if (sellingPrice.compareTo(msp) == -1 || mrp.compareTo(sellingPrice) == -1) {
                errors.add(new WsError(WsResponseCode.INVALID_REQUEST.code(), WsResponseCode.INVALID_REQUEST.message(),
                        "Selling price should lie between minimum selling price and mrp"));
            }
        }
        if (errors.isEmpty()) {
            if (msp.compareTo(MAX_PRICE_ALLOWED) > -1 && mrp.compareTo(MAX_PRICE_ALLOWED) > -1 && sellingPrice.compareTo(MAX_PRICE_ALLOWED) > -1) {
                errors.add(new WsError(WsResponseCode.INVALID_PRICES.code(), WsResponseCode.INVALID_PRICES.message(), "All prices must be less than " + MAX_PRICE_ALLOWED));
            }
        }
        return errors;
    }

    public boolean isNullOrPositiveInt(final BigDecimal bd) {
        return null == bd || // is it null
                ((bd.scale() <= 0 || bd.stripTrailingZeros().scale() <= 0) && bd.signum() >= 0); // or +ve int
    }
}
