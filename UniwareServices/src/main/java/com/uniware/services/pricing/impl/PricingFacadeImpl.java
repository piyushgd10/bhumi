/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Oct 13, 2015
 *  @author      bhupi
 */
package com.uniware.services.pricing.impl;

import java.util.List;
import java.util.concurrent.ExecutionException;

import com.uniware.services.channel.IChannelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.unifier.core.api.validation.WsError;
import com.unifier.core.cache.CacheManager;
import com.uniware.core.api.channel.ChannelPricingPushStatusDTO;
import com.uniware.core.api.prices.CheckChannelItemTypePriceRequest;
import com.uniware.core.api.prices.CheckChannelItemTypePriceResponse;
import com.uniware.core.api.prices.EditChannelItemTypePriceRequest;
import com.uniware.core.api.prices.EditChannelItemTypePriceResponse;
import com.uniware.core.api.prices.GetChannelItemTypePriceRequest;
import com.uniware.core.api.prices.GetChannelItemTypePriceResponse;
import com.uniware.core.api.prices.GetChannelItemTypePricesRequest;
import com.uniware.core.api.prices.GetChannelItemTypePricesResponse;
import com.uniware.core.api.prices.GetChannelPricingSyncStatusRequest;
import com.uniware.core.api.prices.GetChannelPricingSyncStatusResponse;
import com.uniware.core.api.prices.GetHistoricalChannelItemTypePriceRequest;
import com.uniware.core.api.prices.GetUIPricePropertiesByChannelRequest;
import com.uniware.core.api.prices.GetUIPricePropertiesByChannelResponse;
import com.uniware.core.api.prices.PullChannelItemTypePriceRequest;
import com.uniware.core.api.prices.PullChannelItemTypePriceResponse;
import com.uniware.core.api.prices.PushAllPricesOnChannelRequest;
import com.uniware.core.api.prices.PushAllPricesOnChannelResponse;
import com.uniware.core.api.prices.PushChannelItemTypePriceRequest;
import com.uniware.core.api.prices.PushChannelItemTypePriceResponse;
import com.uniware.core.api.prices.dto.PriceDTO;
import com.uniware.core.entity.Channel;
import com.uniware.services.cache.ChannelCache;
import com.uniware.services.pricing.IPricingBridge;
import com.uniware.services.pricing.IPricingFacade;
import com.uniware.services.pricing.IPricingService;
import com.uniware.services.pricing.PricingSourceConfiguration;

/**
 * A facade for use by the presentation layers (desktop, mobile, api endpoints). Hides the complexities of
 * local and remove price management.
 */
@Service("pricingFacade")
public class PricingFacadeImpl implements IPricingFacade {
    
    @Autowired
    @Qualifier("uniwarePricingService")
    private IPricingService localPricingService;
    
    @Autowired
    @Qualifier("channelPricingService")
    private IPricingService channelPricingService;
    
    @Autowired
    private IPricingBridge pricingBridge;

    @Autowired
    private IChannelService channelService;
    
    /**
     * {@inheritDoc}
     */
    @Override
    public GetChannelItemTypePriceResponse getChannelItemTypePrice(GetChannelItemTypePriceRequest request) {
        return localPricingService.getChannelItemTypePrice(request);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public GetChannelItemTypePriceResponse getHistoricalChannelItemTypePrice(
            final GetHistoricalChannelItemTypePriceRequest request) 
    {
        return localPricingService.getHistoricalChannelItemTypePrice(request);
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public EditChannelItemTypePriceResponse editChannelItemType(final EditChannelItemTypePriceRequest request) {
        request.setIsForceEdit(true);
        // Edit locally
        EditChannelItemTypePriceResponse editResponse = localPricingService.editChannelItemTypePrice(request);
        if (editResponse.isSuccessful() && !editResponse.isNoChangeDetected()) {
            // .. and push changes to the channel, but don't wait for the results.
            PushChannelItemTypePriceRequest pushRequest = new PushChannelItemTypePriceRequest();
            pushRequest.setChannelCode(request.getChannelCode());
            pushRequest.setChannelProductId(request.getChannelProductId());
            pushRequest.setForcePush(true);
            pricingBridge.pushChannelItemTypePrice(pushRequest);
        }
        return editResponse;
    }
    
    @Override
    public EditChannelItemTypePriceResponse localEditChannelItemType(final EditChannelItemTypePriceRequest request) {
        request.setIsForceEdit(true);
        return localPricingService.editChannelItemTypePrice(request);
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public CheckChannelItemTypePriceResponse checkChannelItemTypePrice(CheckChannelItemTypePriceRequest request) {
        return channelPricingService.checkChannelItemTypePrice(request);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PushChannelItemTypePriceResponse pushPricesToChannel(final PushChannelItemTypePriceRequest request) {
        try {
            return pricingBridge.pushChannelItemTypePrice(request).sync();
        } catch (InterruptedException | ExecutionException e) {
            throw new RuntimeException(e);
        }
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public PullChannelItemTypePriceResponse pullPricesFromChannel(final PullChannelItemTypePriceRequest request) {
        try {
            return pricingBridge.pullChannelItemTypePrice(request).sync();
        } catch (InterruptedException | ExecutionException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public GetChannelPricingSyncStatusResponse getChannelPricingSyncStatus(final GetChannelPricingSyncStatusRequest request) {
        GetChannelPricingSyncStatusResponse response = new GetChannelPricingSyncStatusResponse();
        ChannelPricingPushStatusDTO pricingPushStatusDTO = channelService.getChannelPricingSyncStatus(request.getChannelCode());
        if (pricingPushStatusDTO != null) {
            response.setPriceSyncStatus(pricingPushStatusDTO);
            response.setSuccessful(true);
        } else {
            response.setSuccessful(false);
        }
        return response;

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PushAllPricesOnChannelResponse pushAllPricesOnChannel(final PushAllPricesOnChannelRequest request) {
        return pricingBridge.pushAllPricesOnChannel(request);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public GetUIPricePropertiesByChannelResponse getUIPricePropertiesByChannel(final GetUIPricePropertiesByChannelRequest request) {
        ChannelCache channelCache = CacheManager.getInstance().getCache(ChannelCache.class);
        Channel channel = channelCache.getChannelByCode(request.getChannelCode());
        PricingSourceConfiguration pricingConfig = new PricingSourceConfiguration(channel.getSourceCode());
        GetUIPricePropertiesByChannelResponse response = new GetUIPricePropertiesByChannelResponse();
        response.setEditableFields(pricingConfig.getEditablePriceFields());
        response.setAvailableFields(pricingConfig.getAvailablePriceFields());
        response.setPriceComputationSupported(pricingConfig.isPriceComputationSupported());
        response.setSuccessful(true);
        return response;
    }
    
    @Override
    public GetChannelItemTypePricesResponse getChannelItemTypePrices(final GetChannelItemTypePricesRequest request) {
        return localPricingService.getChannelItemTypePrices(request);
    }

    @Override
    public List<WsError> validatePrices(PriceDTO oldPrices, PriceDTO newPrices, String channelCode) {
        return localPricingService.validatePrices(oldPrices, newPrices, channelCode);
    }
}
