/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Oct 13, 2015
 *  @author      bhupi
 */

package com.uniware.services.pricing;

import com.unifier.core.configuration.ConfigurationManager;
import com.uniware.core.entity.Source;
import com.uniware.services.configuration.SourceConfiguration;

import java.util.List;

/**
 * Pricing configuration for channel source.
 */
public class PricingSourceConfiguration {
    
    private final Source source;

    public PricingSourceConfiguration(final String sourceCode) {
        this.source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class)
                                          .getSourceByCode(sourceCode);
    }

    /**
     * Returns whether the given currency is supported in the channel source.
     */
    public boolean isCurrencyCodeSupported(final String currencyCode) {
        List<String> supportedCurrencies = source.getSupportedCurrencies();
        return supportedCurrencies.contains(currencyCode);
    }

    /**
     * Returns true if the price field is editable for the source.
     */
    public boolean isPriceFieldEditable(final String priceFieldName) {
        return getEditablePriceFields().contains(priceFieldName);
    }

    /**
     * Returns true if the price computation is supported for the source
     */
    public boolean isPriceComputationSupported() {
        return source.isPriceComputationSupported();
    }

    /**
     *
     * Return all the editable price fields for the source
     */
    public List<String> getEditablePriceFields() {
        return source.getEditablePriceFields();
    }

    /**
     * Return all the price fields available for the source
     */
    public List<String> getAvailablePriceFields() {
        return source.getAvailablePriceFields();
    }

    /**
     * Does the channel source support fractional (non-integer price) points.
     */
    public boolean isFractionalPriceSupported() {
        return source.isFractionalPriceSupported();
    }
    
    /**
     * Return channel source code.
     */
    public String getSourceCode() {
        return source.getCode();
    }
}
