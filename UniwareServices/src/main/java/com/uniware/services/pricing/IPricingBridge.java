/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Oct 13, 2015
 *  @author      bhupi
 */
package com.uniware.services.pricing;

import com.uniware.core.api.prices.PullAllPricesFromChannelAsyncResponse;
import com.uniware.core.api.prices.PullAllPricesFromChannelRequest;
import com.uniware.core.api.prices.PullChannelItemTypePriceAsyncResponse;
import com.uniware.core.api.prices.PullChannelItemTypePriceRequest;
import com.uniware.core.api.prices.PushAllPricesOnAllChannelsRequest;
import com.uniware.core.api.prices.PushAllPricesOnChannelRequest;
import com.uniware.core.api.prices.PushAllPricesOnChannelResponse;
import com.uniware.core.api.prices.PushAllPricesOnChannelsAsyncResponse;
import com.uniware.core.api.prices.PushChannelItemTypePriceAsyncResponse;
import com.uniware.core.api.prices.PushChannelItemTypePriceRequest;

/**
 * A service that bridges interactions between the uniware price management and channel specific price management
 * services.
 */
public interface IPricingBridge {
    
    /** Pull price changes from channel into uniware. */
    PullChannelItemTypePriceAsyncResponse pullChannelItemTypePrice(final PullChannelItemTypePriceRequest request);
    
    /** Pull all prices from the given channels into uniware. */
    PullAllPricesFromChannelAsyncResponse pullAllPricesFromChannel(final PullAllPricesFromChannelRequest request);

    /** Push local (in uniware) price edits to the channel. */
    PushChannelItemTypePriceAsyncResponse pushChannelItemTypePrice(final PushChannelItemTypePriceRequest request);
    
    /** Push all local (in uniware) price edits to the given channel */
    PushAllPricesOnChannelResponse pushAllPricesOnChannel(final PushAllPricesOnChannelRequest request);

    /** Push all local (in uniware) to all channels in relevant state */
    PushAllPricesOnChannelsAsyncResponse pushAllPricesOnAllChannels(final PushAllPricesOnAllChannelsRequest request);

}