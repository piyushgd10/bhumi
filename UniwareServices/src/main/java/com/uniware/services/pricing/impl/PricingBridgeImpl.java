/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version 1.0, Oct 29, 2015
 *  @author bhupi
 */
package com.uniware.services.pricing.impl;

import com.codahale.metrics.annotation.Timed;
import com.unifier.core.api.base.ServiceResponse;
import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.api.validation.WsError;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.jms.MessagingConstants;
import com.uniware.core.api.channel.ChannelDetailDTO;
import com.uniware.core.api.channel.ChannelPricingPushStatusDTO;
import com.uniware.core.api.prices.PullAllPricesFromChannelAsyncResponse;
import com.uniware.core.api.prices.PullAllPricesFromChannelRequest;
import com.uniware.core.api.prices.PullAllPricesFromChannelResponse;
import com.uniware.core.api.prices.PullChannelItemTypePriceAsyncResponse;
import com.uniware.core.api.prices.PullChannelItemTypePriceRequest;
import com.uniware.core.api.prices.PullChannelItemTypePriceResponse;
import com.uniware.core.api.prices.PushAllPricesOnAllChannelsRequest;
import com.uniware.core.api.prices.PushAllPricesOnChannelRequest;
import com.uniware.core.api.prices.PushAllPricesOnChannelResponse;
import com.uniware.core.api.prices.PushAllPricesOnChannelsAsyncResponse;
import com.uniware.core.api.prices.PushChannelItemTypePriceAsyncResponse;
import com.uniware.core.api.prices.PushChannelItemTypePriceRequest;
import com.uniware.core.api.prices.PushChannelItemTypePriceResponse;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.cache.EnvironmentPropertiesCache;
import com.uniware.core.concurrent.ContextAwareExecutorFactory;
import com.uniware.core.entity.Channel;
import com.uniware.core.entity.Source;
import com.uniware.core.locking.Namespace;
import com.uniware.core.locking.annotation.Lock;
import com.uniware.core.locking.annotation.Locks;
import com.uniware.services.cache.ChannelCache;
import com.uniware.services.channel.IChannelService;
import com.uniware.services.channel.impl.ChannelServiceImpl;
import com.uniware.services.configuration.SourceConfiguration;
import com.uniware.core.locking.ILockingService;
import com.uniware.services.messaging.jms.ActiveMQConnector;
import com.uniware.services.pricing.IPricingBridge;
import com.uniware.services.pricing.IPricingPushListener;
import com.uniware.services.pricing.IPricingService;
import com.uniware.services.pricing.impl.PricingServicePipe.PipeResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

@Service("pricingServiceBride")
// @Metrics(registry = "service-registry")
public class PricingBridgeImpl implements IPricingBridge {

    private static final Logger         LOG          = LoggerFactory.getLogger(PricingBridgeImpl.class);
    private static final String         SERVICE_NAME = "asyncPricingBridge";

    @Autowired
    @Qualifier("uniwarePricingService")
    private IPricingService             uniwarePricing;

    @Autowired
    @Qualifier("channelPricingService")
    private IPricingService             channelPricing;

    @Autowired
    private IPricingPushListener        pushListener;

    @Autowired
    private ContextAwareExecutorFactory executorFactory;

    @Autowired
    private IChannelService             channelService;

    @Autowired
    private ILockingService             lockingService;

    @Autowired
    @Qualifier("activeMQConnector1")
    private ActiveMQConnector           activeMQConnector;

    /**
     * {@inheritDoc}
     */
    @Override
    @Timed
    /** Pull price changes from channel into uniware. */
    public PullChannelItemTypePriceAsyncResponse pullChannelItemTypePrice(final PullChannelItemTypePriceRequest request) {
        PullChannelItemTypePriceAsyncResponse response = new PullChannelItemTypePriceAsyncResponse();
        response.setChannelCode(request.getChannelCode());
        response.setAsyncResult(getExecutor().submit(new Callable<PullChannelItemTypePriceResponse>() {
            @Override
            public PullChannelItemTypePriceResponse call() throws Exception {
                return pullChannelItemTypePriceSync(request);
            }
        }));
        response.setSuccessful(true);
        return response;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Timed
    public PushChannelItemTypePriceAsyncResponse pushChannelItemTypePrice(final PushChannelItemTypePriceRequest request) {
        PushChannelItemTypePriceAsyncResponse response = new PushChannelItemTypePriceAsyncResponse();
        response.setChannelCode(request.getChannelCode());
        response.setAsyncResult(getExecutor().submit(new Callable<PushChannelItemTypePriceResponse>() {
            @Override
            public PushChannelItemTypePriceResponse call() throws Exception {
                return pushChannelItemTypePriceSync(request);
            }
        }));
        response.setSuccessful(true);
        return response;

    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Timed
    public PullAllPricesFromChannelAsyncResponse pullAllPricesFromChannel(final PullAllPricesFromChannelRequest request) {
        PullAllPricesFromChannelAsyncResponse response = new PullAllPricesFromChannelAsyncResponse();
        response.setChannelCode(request.getChannelCode());
        response.setAsyncTaskResult(getExecutor().submit(new Callable<PullAllPricesFromChannelResponse>() {
            @Override
            public PullAllPricesFromChannelResponse call() throws Exception {
                return pullAllPricesFromChannelSync(request);
            }
        }));
        response.setSuccessful(true);
        return response;
    }

    @Override
    @Timed
    public PushAllPricesOnChannelsAsyncResponse pushAllPricesOnAllChannels(final PushAllPricesOnAllChannelsRequest request) {
        PushAllPricesOnChannelsAsyncResponse response = new PushAllPricesOnChannelsAsyncResponse();
        Set<ChannelDetailDTO> channelsToProcess = new HashSet<ChannelDetailDTO>();
        for (ChannelDetailDTO channelDetailDTO : CacheManager.getInstance().getCache(ChannelCache.class).getChannels()) {
            if (channelDetailDTO.isEnabled() && Channel.SyncStatus.ON.name().equals(channelDetailDTO.getPricingSyncStatus())) {
                channelsToProcess.add(channelDetailDTO);
            }
        }
        if (channelsToProcess.isEmpty()) {
            response.addError(new WsError("No channels to process"));
            response.setSuccessful(true);
            return response;
        }
        for (ChannelDetailDTO channel : channelsToProcess) {
            pushAllPricesOnChannel(new PushAllPricesOnChannelRequest(channel.getCode()));
        }
        response.setSuccessful(true);
        return response;

    }

    /**
     * {@inheritDoc}
     */

    @Override
    @Timed
    public PushAllPricesOnChannelResponse pushAllPricesOnChannel(final PushAllPricesOnChannelRequest request) {
        PushAllPricesOnChannelResponse response = new PushAllPricesOnChannelResponse();
        final Channel channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelByCode(request.getChannelCode());
        final ChannelPricingPushStatusDTO pricingSyncStatusDTO = getChannelPricingSyncStatus(request.getChannelCode());
        if (channel == null) {
            response.setMessage("Invalid channel");
            response.addError(new WsError(WsResponseCode.INVALID_CHANNEL_CODE.code(), WsResponseCode.INVALID_CHANNEL_CODE.message(), "Invalid channel"));
        } else if (!channel.isEnabled()) {
            LOG.info("Channel disabled: {}", channel.getCode());
            response.setMessage("Channel disabled");
            response.addError(new WsError(WsResponseCode.BAD_CHANNEL_STATE.code(), WsResponseCode.BAD_CHANNEL_STATE.message(), "Channel disabled"));
        } else if (!Channel.SyncStatus.ON.equals(channel.getPricingSyncStatus())) {
            LOG.info("Channel pricing sync disabled: {}", channel.getCode());
            response.setMessage("Pricing Sync disabled");
            response.addError(new WsError(WsResponseCode.PRICE_SYNC_NOT_CONFIGURED.code(), WsResponseCode.PRICE_SYNC_NOT_CONFIGURED.message(), "Pricing Sync disabled"));
        } else {
            java.util.concurrent.locks.Lock pricingSyncLock = lockingService.getLock(Namespace.CHANNEL_PROCESS_PRICE, channel.getCode());
            boolean lockAcquired = false;
            try {
                long defaultTimeout = CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class).getLockWaitTimeoutInSeconds();
                if (request.isLocalExecution()) {
                    lockAcquired = pricingSyncLock.tryLock(defaultTimeout, TimeUnit.SECONDS);
                } else {
                    lockAcquired = pricingSyncLock.tryLock();
                }
                if (lockAcquired) {
                    if (!request.isLocalExecution()) {
                        activeMQConnector.produceContextAwareMessage(MessagingConstants.Queue.PRICE_SYNC_QUEUE, request);
                        pricingSyncStatusDTO.setMessage("Request queued");
                        pricingSyncStatusDTO.setSyncExecutionStatus(Channel.SyncExecutionStatus.QUEUED);
                        updateChannelPricingSyncStatus(pricingSyncStatusDTO);
                        LOG.info("Queued PushAllPricesOnChannelRequest for channel: {}", channel.getCode());
                    } else {
                        LOG.info("Verifying price update connectors for channel {}", channel.getName());
                        pricingSyncStatusDTO.setMessage("Verifying connectors");
                        final ChannelServiceImpl.ChannelGoodToSyncResponse channelPriceSyncReady = isChannelPriceSyncReady(channel, null);
                        if (!channelPriceSyncReady.isGoodToSync()) {
                            pricingSyncStatusDTO.reset();
                            pricingSyncStatusDTO.setMessage("Connector is broken");
                            pricingSyncStatusDTO.setSyncExecutionStatus(Channel.SyncExecutionStatus.IDLE);
                            updateChannelPricingSyncStatus(pricingSyncStatusDTO);
                            LOG.warn("Connector broken for channel {}", channel.getName());
                        } else {
                            pricingSyncStatusDTO.reset();
                            pricingSyncStatusDTO.setMessage("Request enqued");
                            pricingSyncStatusDTO.setSyncExecutionStatus(Channel.SyncExecutionStatus.RUNNING);
                            LOG.info("Channel PRICING sync scheduled: {}", channel.getCode());
                            updateChannelPricingSyncStatus(pricingSyncStatusDTO);
                            PushAllPricesOnChannelTask pushTask = new PushAllPricesOnChannelTask(channel, channelPriceSyncReady.getParams(), pricingSyncStatusDTO,
                                    (PricingServiceImpl) uniwarePricing, (ChannelPricingServiceImpl) channelPricing,(ChannelServiceImpl) channelService);
                            // delegate the push listener
                            pushTask.setPushListener(pushListener);
                            // Register a callback to update the channel's price push status mao
                            pushTask.setComplectionCallback(new PushAllPricesOnChannelTask.OnComplete() {
                                @Override
                                public void call(final ChannelPricingPushStatusDTO pushStatus) {
                                    updateChannelPricingSyncStatus(pushStatus);
                                }
                            });
                            getExecutor().submit(pushTask);
                        }
                    }
                } else {
                    LOG.info("Failed to obtain {} lock for channel {}", Namespace.CHANNEL_PROCESS_PRICE.name(), channel.getCode());
                    response.addError(new WsError(WsResponseCode.ALREADY_RUNNING.code(), "Pricing Sync already running for channel " + channel.getCode()));
                }
            } catch (InterruptedException e) {
                LOG.error("InterruptedException while waiting for lock", e);
            } finally {
                if (lockAcquired) {
                    pricingSyncLock.unlock();
                    updateChannelPricingSyncStatus(pricingSyncStatusDTO);
                }
            }
        }
        if (!response.hasErrors()) {
            response.setSuccessful(true);
        }
        return response;
    }

    private PullChannelItemTypePriceResponse pullChannelItemTypePriceSync(final PullChannelItemTypePriceRequest request) {
        PricingServicePipe pipe = new PricingServicePipe(channelPricing, uniwarePricing);
        ServiceResponse pipeResponse = pipe.transfer(request.getChannelCode(), request.getChannelProductId(), request.isForcePull());

        PullChannelItemTypePriceResponse response = new PullChannelItemTypePriceResponse();
        response.setChannelCode(request.getChannelCode());
        response.setResponse(pipeResponse);
        if (!response.isSuccessful()) {
            LOG.warn("Pulling prices from channel --> uniware for {} unsucessful: {}", request.getChannelProductId(), response.getMessage());
        }
        return response;
    }

    private PullAllPricesFromChannelResponse pullAllPricesFromChannelSync(final PullAllPricesFromChannelRequest request) {
        throw new UnsupportedOperationException();
    }

    @Locks({ @Lock(ns = Namespace.CHANNEL_PROCESS_PRICE, key = "#{#args[0].channelCode}/#{#args[0].channelProductId}") })
    private PushChannelItemTypePriceResponse pushChannelItemTypePriceSync(final PushChannelItemTypePriceRequest request) {
        String channelCode = request.getChannelCode();
        String channelProductId = request.getChannelProductId();
        PushChannelItemTypePriceResponse response = new PushChannelItemTypePriceResponse();
        final Channel channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelByCode(request.getChannelCode());
        if (!channel.isEnabled()) {
            LOG.info("Channel disabled: {}", channel.getCode());
            response.setMessage("Channel disabled");
            response.addError(new WsError(WsResponseCode.BAD_CHANNEL_STATE.code(), WsResponseCode.BAD_CHANNEL_STATE.message(), "Channel disabled"));
        } else if (!Channel.SyncStatus.ON.equals(channel.getPricingSyncStatus())) {
            LOG.info("Channel pricing sync disabled: {}", channel.getCode());
            response.setMessage("Pricing Sync disabled");
            response.addError(new WsError(WsResponseCode.PRICE_SYNC_NOT_CONFIGURED.code(), WsResponseCode.PRICE_SYNC_NOT_CONFIGURED.message(), "Pricing Sync disabled"));
        } else {
            PricingServicePipe pipe = new PricingServicePipe(uniwarePricing, channelPricing);
            LOG.info("Pushing prices from uniware -> channel for " + channelProductId);
            PipeResponse pipeResponse = pipe.transfer(channelCode, channelProductId, request.isForcePush());
            response.setResponse(pipeResponse);
            response.setChannelCode(channelCode);
            if (response.isSuccessful()) {
                if (pushListener != null) {
                    pushListener.onPushSuccess(channelCode, channelProductId, pipeResponse.getSourceDataVersion(), pipeResponse.getTransferredData(), response.getMessage());
                }
            } else {
                pushListener.onPushFailure(channelCode, channelProductId, pipeResponse.getSourceDataVersion(), response.getMessage());
            }
        }
        return response;
    }

    private ExecutorService getExecutor() {
        return executorFactory.getExecutor(SERVICE_NAME);
    }

    private ChannelPricingPushStatusDTO getChannelPricingSyncStatus(final String channelCode) {
        return channelService.getChannelPricingSyncStatus(channelCode);
    }

    private void updateChannelPricingSyncStatus(final ChannelPricingPushStatusDTO pricingSyncStatusDTO) {
        try {
            LOG.info("Updating pricing sync status for channel: {} to {status:{}, message:{}}",
                    new Object[] { pricingSyncStatusDTO.getChannelCode(), pricingSyncStatusDTO.getSyncExecutionStatus().name(), pricingSyncStatusDTO.getMessage() });
            channelService.updateChannelPricingSyncStatus(pricingSyncStatusDTO);
        } catch (Throwable t) {
            LOG.error("UpdatePricingStatus threw exception", t);
        }
    }

    private ChannelServiceImpl.ChannelGoodToSyncResponse isChannelPriceSyncReady(final Channel channel, final ValidationContext context) {
        Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(channel.getSourceCode());
        ChannelServiceImpl.ChannelGoodToSyncResponse response = new ChannelServiceImpl.ChannelGoodToSyncResponse();
        if (!source.isPricingSyncConfigured()) {
            response.setGoodToSync(false);
            if (context != null) {
                context.addError(WsResponseCode.PRICE_SYNC_NOT_CONFIGURED, "Price sync not available for " + source.getCode());
            }
        } else if (!Channel.SyncStatus.ON.equals(channel.getPricingSyncStatus())) {
            response.setGoodToSync(false);
            if (context != null) {
                context.addError(WsResponseCode.PRICE_SYNC_NOT_CONFIGURED, "Price sync not configured for channel" + channel.getSourceCode());
            }
        } else {
            response = channelService.isChannelGoodToSync(channel.getCode(), ChannelServiceImpl.Sync.CATALOG);
            if (!response.isGoodToSync()) {
                LOG.warn("Connector broken for channel {}", channel.getName());
            }
        }
        return response;
    }
}
