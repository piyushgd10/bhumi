/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 01, 2015
 *  @author      akshay
 */
package com.uniware.services.pricing.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.unifier.core.api.validation.WsError;
import com.unifier.core.cache.CacheManager;
import com.uniware.core.api.prices.dto.PriceDTO;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.entity.Channel;
import com.uniware.services.cache.ChannelCache;
import com.uniware.services.pricing.PricingSourceConfiguration;

public class PricingUtils {
    
    private static final String     MSP               = "MSP";
    private static final String     MRP               = "MRP";
    private static final String     SELLING_PRICE     = "SELLING_PRICE";
    private static final BigDecimal MAX_PRICE_ALLOWED = BigDecimal.valueOf(1000000000);
    
    public static List<WsError> validatePrices(final PriceDTO oldPrices, final PriceDTO newPrices, final String channelCode) {
        Channel channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelByCode(channelCode);
        if (channel == null) {
            return Collections.singletonList(new WsError(WsResponseCode.INVALID_CHANNEL_CODE.code(), WsResponseCode.INVALID_CHANNEL_CODE.message(), "Invalid channel code "));
        }
        if (newPrices.getMsp() == null && newPrices.getMrp() == null && newPrices.getSellingPrice() == null) {
            return Collections.singletonList(new WsError(WsResponseCode.INVALID_REQUEST.code(), WsResponseCode.INVALID_REQUEST.message(),
                    "Either of msp | msp | selling price is required to update"));
        }

        PricingSourceConfiguration pricingConfig = new PricingSourceConfiguration(channel.getSourceCode());
        List<WsError> errors = new ArrayList<WsError>();

        if (!pricingConfig.isCurrencyCodeSupported(newPrices.getCurrencyCode())) {
            errors.add(new WsError(WsResponseCode.INVALID_CURRENCY_CODE.code(), WsResponseCode.INVALID_CURRENCY_CODE.message(), "Currency code invalid for source "
                    + pricingConfig.getSourceCode()));
        }
        if (!newPrices.getCurrencyCode().equals(oldPrices.getCurrencyCode())) {
            // Currency code change is not supported currently.
            errors.add(new WsError(WsResponseCode.INVALID_REQUEST.code(), WsResponseCode.INVALID_REQUEST.message(), "Currency code change is not supported for "
                    + pricingConfig.getSourceCode()));
        }

        if (!pricingConfig.isFractionalPriceSupported()) {
            if (!isNullOrPositiveInt(oldPrices.getSellingPrice()) || !isNullOrPositiveInt(oldPrices.getMrp()) || !isNullOrPositiveInt(oldPrices.getMsp())) {
                errors.add(new WsError(WsResponseCode.INVALID_REQUEST.code(), WsResponseCode.INVALID_REQUEST.message(), pricingConfig.getSourceCode()
                        + " supports only +ve integer values for prices"));
            }
        }

        BigDecimal sellingPrice = newPrices.getSellingPrice();
        BigDecimal msp = newPrices.getMsp();
        BigDecimal mrp = newPrices.getMrp();

        if (sellingPrice != null && !pricingConfig.isPriceFieldEditable(SELLING_PRICE)) {
            errors.add(new WsError(WsResponseCode.INVALID_REQUEST.code(), WsResponseCode.INVALID_REQUEST.message(), "Selling price is not editable for source "
                    + pricingConfig.getSourceCode()));
        }
        if (msp != null && !pricingConfig.isPriceFieldEditable((MSP))) {
            errors.add(new WsError(WsResponseCode.INVALID_REQUEST.code(), WsResponseCode.INVALID_REQUEST.message(), "MSP price is not editable for source "
                    + pricingConfig.getSourceCode()));
        }
        if (mrp != null && !pricingConfig.isPriceFieldEditable(MRP)) {
            errors.add(new WsError(WsResponseCode.INVALID_REQUEST.code(), WsResponseCode.INVALID_REQUEST.message(), "MRP price is not editable for source "
                    + pricingConfig.getSourceCode()));
        }
        if (errors.isEmpty()) {
            msp = msp != null ? msp : sellingPrice != null ? sellingPrice : mrp != null ? mrp : BigDecimal.ZERO;
            sellingPrice = sellingPrice != null ? sellingPrice : msp != null ? msp : mrp != null ? mrp : BigDecimal.ZERO;
            mrp = mrp != null ? mrp : sellingPrice != null ? sellingPrice : msp != null ? msp : BigDecimal.ZERO;
            if (sellingPrice.compareTo(msp) == -1 || mrp.compareTo(sellingPrice) == -1) {
                errors.add(new WsError(WsResponseCode.INVALID_REQUEST.code(), WsResponseCode.INVALID_REQUEST.message(),
                        "Selling price should lie between minimum selling price and mrp"));
            }
        }
        if (errors.isEmpty()) {
            if (msp.compareTo(MAX_PRICE_ALLOWED) > -1 && mrp.compareTo(MAX_PRICE_ALLOWED) > -1 && sellingPrice.compareTo(MAX_PRICE_ALLOWED) > -1) {
                errors.add(new WsError(WsResponseCode.INVALID_PRICES.code(), WsResponseCode.INVALID_PRICES.message(), "All prices must be less than " + MAX_PRICE_ALLOWED));
            }
        }
        return errors;
    }

    public static boolean isNullOrPositiveInt(final BigDecimal bd) {
        return null == bd || // is it null
                ((bd.scale() <= 0 || bd.stripTrailingZeros().scale() <= 0) && bd.signum() >= 0); // or +ve int
    }

}
