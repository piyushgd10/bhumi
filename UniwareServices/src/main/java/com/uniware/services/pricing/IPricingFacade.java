/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Oct 13, 2015
 *  @author      bhupi
 */
package com.uniware.services.pricing;

import java.util.List;

import com.unifier.core.api.validation.WsError;
import com.uniware.core.api.prices.CheckChannelItemTypePriceRequest;
import com.uniware.core.api.prices.CheckChannelItemTypePriceResponse;
import com.uniware.core.api.prices.EditChannelItemTypePriceRequest;
import com.uniware.core.api.prices.EditChannelItemTypePriceResponse;
import com.uniware.core.api.prices.GetChannelItemTypePriceRequest;
import com.uniware.core.api.prices.GetChannelItemTypePriceResponse;
import com.uniware.core.api.prices.GetChannelItemTypePricesRequest;
import com.uniware.core.api.prices.GetChannelItemTypePricesResponse;
import com.uniware.core.api.prices.GetChannelPricingSyncStatusRequest;
import com.uniware.core.api.prices.GetChannelPricingSyncStatusResponse;
import com.uniware.core.api.prices.GetHistoricalChannelItemTypePriceRequest;
import com.uniware.core.api.prices.GetUIPricePropertiesByChannelRequest;
import com.uniware.core.api.prices.GetUIPricePropertiesByChannelResponse;
import com.uniware.core.api.prices.PullChannelItemTypePriceRequest;
import com.uniware.core.api.prices.PullChannelItemTypePriceResponse;
import com.uniware.core.api.prices.PushAllPricesOnChannelRequest;
import com.uniware.core.api.prices.PushAllPricesOnChannelResponse;
import com.uniware.core.api.prices.PushChannelItemTypePriceRequest;
import com.uniware.core.api.prices.PushChannelItemTypePriceResponse;
import com.uniware.core.api.prices.dto.PriceDTO;

/**
 * A facade for use by the presentation layers (desktop, mobile, api endpoints).
 * Hides the complexities of local and remote price management and aims to be an all
 * encompassing one-stop shop for price management in uniware.
 */
public interface IPricingFacade {

    /** Get channel item prices from uniware. */
    public GetChannelItemTypePriceResponse getChannelItemTypePrice(final GetChannelItemTypePriceRequest request);
    
    /** Get historical channel item prices from uniware. */
    GetChannelItemTypePriceResponse getHistoricalChannelItemTypePrice(final GetHistoricalChannelItemTypePriceRequest request);
    
    /** Validate prices in the request and recompute derived ones. */
    public CheckChannelItemTypePriceResponse checkChannelItemTypePrice(final CheckChannelItemTypePriceRequest request);

    /** Edit channel item prices locally in uniware and issue an async task for push on the remote channel. */
    public EditChannelItemTypePriceResponse editChannelItemType(final EditChannelItemTypePriceRequest request);

    /** Edit channel item prices locally in uniware only. */
    public EditChannelItemTypePriceResponse localEditChannelItemType(final EditChannelItemTypePriceRequest request);

    /** Push local (in uniware) price edits to the channel. */
    public PushChannelItemTypePriceResponse pushPricesToChannel(final PushChannelItemTypePriceRequest request);

    /** Pull price edits from the given channel into uniware. */
    public PullChannelItemTypePriceResponse pullPricesFromChannel(final PullChannelItemTypePriceRequest request);

    /** Get channel price sync status for the given channel */
    public GetChannelPricingSyncStatusResponse getChannelPricingSyncStatus(final GetChannelPricingSyncStatusRequest request);

    /** Push all prices from uniware to channel */
    public PushAllPricesOnChannelResponse pushAllPricesOnChannel(final PushAllPricesOnChannelRequest request);

    /** Get UI price properties of the given channel */
    public GetUIPricePropertiesByChannelResponse getUIPricePropertiesByChannel(final GetUIPricePropertiesByChannelRequest request);

    /** Get prices for a seller sku code */
    public GetChannelItemTypePricesResponse getChannelItemTypePrices(GetChannelItemTypePricesRequest request);
    
    /** Validate uniware prices */
    List<WsError> validatePrices(PriceDTO oldPrices, PriceDTO newPrices, String channelCode);
    
}
