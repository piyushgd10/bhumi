/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version 1.0, Nov 03, 2015
 *  @author bhupi
 */
package com.uniware.services.pricing.impl;

import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.SharedMetricRegistries;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.utils.BatchProcessor;
import com.unifier.core.utils.CollectionUtils;
import com.unifier.core.utils.DateUtils;
import com.uniware.core.api.channel.ChannelPricingPushStatusDTO;
import com.uniware.core.api.prices.EditBulkChannelItemPriceRequest;
import com.uniware.core.api.prices.EditBulkChannelItemPriceResponse;
import com.uniware.core.api.prices.EditOneChannelItemTypePriceRequest;
import com.uniware.core.api.prices.PushAllPricesOnChannelResponse;
import com.uniware.core.api.prices.dto.PriceDTO;
import com.uniware.core.entity.Channel;
import com.uniware.core.entity.ChannelItemTypePrice;
import com.uniware.core.entity.Source;
import com.uniware.core.locking.Namespace;
import com.uniware.core.locking.annotation.Lock;
import com.uniware.core.locking.annotation.Locks;
import com.uniware.core.utils.UserContext;
import com.uniware.core.vo.ChannelPriceUpdateVO;
import com.uniware.services.cache.ChannelCache;
import com.uniware.services.channel.impl.ChannelServiceImpl;
import com.uniware.services.configuration.SourceConfiguration;
import com.uniware.services.pricing.IPricingPushListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.concurrent.Callable;

/**
 * A callable that assumes the responsibility to push prices on the given channel.
 */
class PushAllPricesOnChannelTask implements Callable<PushAllPricesOnChannelResponse> {
    
    private static final Logger         LOG                 = LoggerFactory.getLogger(PushAllPricesOnChannelTask.class);
    private static final int            DEFAULT_BATCH_SIZE  = 500;
    private static final String         SERVER_DOWN         = "SERVER_DOWN";
    private static final String         SERVICE_METRICS     = "service-metrics";
    
    private final MetricRegistry serviceMetrics = SharedMetricRegistries.getOrCreate(SERVICE_METRICS);
    
    private final Channel channel;
    private final Map<String, String> channelParams;
    private final ChannelPricingPushStatusDTO pricingSyncStatusDTO;
    
    // Guys used to carry out the heavy lifting of pushing the prices
    private final PricingServiceImpl uniwarePricing;
    private final ChannelPricingServiceImpl channelPricing;

    private final ChannelServiceImpl channelService;
    
    // Lets folks interested know about the progress
    private IPricingPushListener pushListener;
    private OnComplete completionCallback;
    
    public interface OnComplete {
        void call(ChannelPricingPushStatusDTO pushStatus);
    }
    
    public PushAllPricesOnChannelTask(
        final Channel channel, final Map<String, String> channelparams, 
        final ChannelPricingPushStatusDTO pricingSyncStatusDTO, 
        final PricingServiceImpl uniwarePricing, final ChannelPricingServiceImpl channelPricing, final ChannelServiceImpl channelService)
    {
        this.channel = channel;
        this.channelParams = channelparams;
        this.pricingSyncStatusDTO = pricingSyncStatusDTO;
        this.uniwarePricing = uniwarePricing;
        this.channelPricing = channelPricing;
        this.channelService = channelService;
    }

    @Override
    public PushAllPricesOnChannelResponse call() throws Exception {
        try {
            return pushAllPricesOnChannelSync(channel, channelParams);
        }
        finally {
            if (null != completionCallback) {
                fillupPriceSyncSuccessStatus(pricingSyncStatusDTO);
                completionCallback.call(pricingSyncStatusDTO);
            }
        }
    }
    
    public void setPushListener(final IPricingPushListener pushListener) {
        this.pushListener = pushListener;
    }
    
    public void setComplectionCallback(final OnComplete callable) {
        completionCallback = callable;
    }
    
    @Locks({ @Lock(ns = Namespace.CHANNEL_PROCESS_PRICE, key = "#{#args[0].code}") })
    private PushAllPricesOnChannelResponse pushAllPricesOnChannelSync(
        final Channel channel, final Map<String, String> channelParams)
    {
        PushAllPricesOnChannelResponse response = new PushAllPricesOnChannelResponse();
        response.setChannelCode(channel.getCode());
        try {
            final Date lastUpdated = DateUtils.getCurrentTime();
            long totalCount = uniwarePricing.getDirtyChannelItemTypePriceCount(channel.getId(), lastUpdated);
            if (totalCount == 0) {
                LOG.warn("No pending price edits for channel {}", channel.getName());
                pricingSyncStatusDTO.setMessage("No pending price edits for channel " + channel.getName());
                response.setMessage("No pending price edits for channel " + channel.getName());
                response.setSuccessful(true);
                return response;
            }
            
            serviceMetrics.histogram(
                MetricRegistry.name(PushAllPricesOnChannelTask.class, "dirtyCount")).update(totalCount);
            String tenant = UserContext.current().getTenant().getCode();
            serviceMetrics.histogram(
                MetricRegistry.name(PushAllPricesOnChannelTask.class, "dirtyCount", tenant)).update(totalCount);

            pricingSyncStatusDTO.setTotalMileStones(totalCount);
            LOG.info("Channel: {}, Number of pending price edits: {}", channel.getName(), totalCount);
            // notification count available on channel kept at source level
            final int processPriceNotificationCount = 
                CacheManager.getInstance().getCache(ChannelCache.class).getProcessPriceCount(channel.getCode());
            int batchSize = DEFAULT_BATCH_SIZE;
            if (processPriceNotificationCount > DEFAULT_BATCH_SIZE) {
                batchSize = processPriceNotificationCount;
            }

            channelService.updateChannelPricingSyncStatus(pricingSyncStatusDTO);
            BatchProcessor<ChannelItemTypePrice> processor = new BatchProcessor<ChannelItemTypePrice>(batchSize) {
                @Override
                protected void process(List<ChannelItemTypePrice> channelItemTypePrices, int batchIndex) {

                    Iterator<List<ChannelItemTypePrice>> iterator = 
                        CollectionUtils.sublistIterator(channelItemTypePrices, processPriceNotificationCount);
                    while (iterator.hasNext()) {
                        List<ChannelItemTypePrice> channelQuanta = iterator.next();
                        pricingSyncStatusDTO.setMessage("Updating prices for batch containing " + channelQuanta.size() + " items");
                        // Do meaty sh**
                        pushPriceOnChannel(channelQuanta);
                        channelService.updateChannelPricingSyncStatus(pricingSyncStatusDTO);
                    }
                }

                @Override
                protected List<ChannelItemTypePrice> next(int start, int batchSize) {
                    LOG.info("Page: {}, BatchSize: {}", start/batchSize, batchSize);
                    // batch start index is zero, since by the time this is called again, dirty items would have been
                    // flushed.
                    return uniwarePricing.getDirtyChannelItemTypePricesByChannel(channel.getId(), 0, batchSize, lastUpdated);
                }
            };
            processor.process();
            LOG.info("Completed SyncPriceOnChannelTask for channel: {}", channel.getName());
            response.setSuccessful(true);
        } catch (Throwable e) {
            if (SERVER_DOWN.equals(e.getMessage())) {
                pricingSyncStatusDTO.setMessage(e.getMessage());
            }
            LOG.error("Error while updating price on channel: {}", channel.getCode());
            LOG.error("Stack trace: ", e);
            response.setSuccessful(false);
        }
        return response;
    }
    
    private void fillupPriceSyncSuccessStatus(final ChannelPricingPushStatusDTO pricingSyncStatusDTO) {
        if (pricingSyncStatusDTO.getLastFailedPushCount() > 0) {
            pricingSyncStatusDTO.setLastPushFailedTime(DateUtils.getCurrentTime());
        }
        if (pricingSyncStatusDTO.getLastSuccessfullPushCount() > 0) {
            pricingSyncStatusDTO.setLastSuccessfullPriceSyncTime(DateUtils.getCurrentTime());
            pricingSyncStatusDTO.setLastPushSuccessful(true);
        } else if (pricingSyncStatusDTO.getCurrentMileStone() == 0) {
            pricingSyncStatusDTO.setLastPushSuccessful(true);
        }
        pricingSyncStatusDTO.setFailedCount(
            uniwarePricing.getChannelItemTypePriceCount(channel.getId(), ChannelPriceUpdateVO.Status.FAILED.name()));
        pricingSyncStatusDTO.setTodaysSuccessfullPushCount(
            uniwarePricing.getChannelItemTypePriceCount(
                channel.getId(), ChannelPriceUpdateVO.Status.SUCCESS.name(),
                new DateUtils.DateRange(DateUtils.getCurrentDate(), DateUtils.getCurrentTime())));
        pricingSyncStatusDTO.setLastPushTime(DateUtils.getCurrentTime());
        pricingSyncStatusDTO.setSyncExecutionStatus(Channel.SyncExecutionStatus.IDLE);
    }
    
    // @Transactional
    private void pushPriceOnChannel(final List<ChannelItemTypePrice> channelItemTypePrices) {
        Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class)
                .getSourceByCode(channel.getSourceCode());
        pricingSyncStatusDTO.setMileStone("Updating price on channel " + source.getName(), channelItemTypePrices.size());
        EditBulkChannelItemPriceRequest bulkChannelItemPriceRequest = new EditBulkChannelItemPriceRequest();
        List<EditOneChannelItemTypePriceRequest> channelItemTypePriceDTOs = new ArrayList<>();
        for (ChannelItemTypePrice itemTypePrice : channelItemTypePrices) {
            EditOneChannelItemTypePriceRequest channelItemTypePriceDTO = new EditOneChannelItemTypePriceRequest();
            channelItemTypePriceDTO.setSellingPrice(itemTypePrice.getSellingPrice());
            channelItemTypePriceDTO.setMsp(itemTypePrice.getMsp());
            channelItemTypePriceDTO.setChannelProductId(itemTypePrice.getChannelProductId());
            channelItemTypePriceDTO.setMrp(itemTypePrice.getMrp());
            channelItemTypePriceDTO.setCurrencyCode(itemTypePrice.getCurrencyCode());
            channelItemTypePriceDTO.setSellerSkuCode(itemTypePrice.getChannelItemType().getSellerSkuCode());
            LOG.info("Channel: {}, SellerSkuCode: {}", channel.getName(), itemTypePrice.getChannelItemType().getSellerSkuCode());
            channelItemTypePriceDTOs.add(channelItemTypePriceDTO);
        }
        bulkChannelItemPriceRequest.setChannelCode(channel.getCode());
        bulkChannelItemPriceRequest.setChannelItemTypePriceRequests(channelItemTypePriceDTOs);

        EditBulkChannelItemPriceResponse bulkCITPriceResponse =
                channelPricing.editChannelItemTypePrice(bulkChannelItemPriceRequest);

        if (!bulkCITPriceResponse.isSuccessful()) {
            if (SERVER_DOWN.equals(bulkCITPriceResponse.getMessage())) {
                throw new RuntimeException("Server down for channel: " + source.getName() + ", will retry in next run.");
            }
        } else {
            for (ChannelItemTypePrice itemTypePrice : channelItemTypePrices) {
                String channelProductId = itemTypePrice.getChannelProductId();
                if (bulkCITPriceResponse.getFailedChannelProductIdToErrorMessage() != null && bulkCITPriceResponse.getFailedChannelProductIdToErrorMessage().containsKey(channelProductId)) {
                    pricingSyncStatusDTO.incrementFailedPushCount(1);
                    if (pushListener != null) {
                        pushListener.onPushFailure(
                                channel.getCode(), itemTypePrice.getChannelProductId(), itemTypePrice.getUpdated(),
                                bulkCITPriceResponse.getFailedChannelProductIdToErrorMessage().get(channelProductId));
                    }
                } else {
                    PriceDTO itemTypePriceDTO = bulkCITPriceResponse.getChannelProductIdToPriceDTO().get(channelProductId);
                    pricingSyncStatusDTO.incrementSuccessfullPushCount(1);
                    if (pushListener != null) {
                        pushListener.onPushSuccess(
                                channel.getCode(), itemTypePrice.getChannelProductId(), itemTypePrice.getUpdated(),
                                itemTypePriceDTO, bulkCITPriceResponse.getMessage());
                    }
                }
            }
        }
    }
}
