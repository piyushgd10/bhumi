/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Oct 13, 2015
 *  @author bhupi
 */
package com.uniware.services.pricing.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.codahale.metrics.annotation.Timed;
import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.api.validation.WsError;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.utils.JibxUtils;
import com.unifier.scraper.sl.exception.ScriptExecutionException;
import com.unifier.scraper.sl.runtime.IScriptProvider;
import com.unifier.scraper.sl.runtime.ScraperScript;
import com.unifier.scraper.sl.runtime.ScriptExecutionContext;
import com.uniware.core.api.prices.CheckChannelItemTypePriceRequest;
import com.uniware.core.api.prices.CheckChannelItemTypePriceResponse;
import com.uniware.core.api.prices.EditBulkChannelItemPriceRequest;
import com.uniware.core.api.prices.EditBulkChannelItemPriceResponse;
import com.uniware.core.api.prices.EditChannelItemTypePriceRequest;
import com.uniware.core.api.prices.EditChannelItemTypePriceResponse;
import com.uniware.core.api.prices.GetChannelItemTypePriceRequest;
import com.uniware.core.api.prices.GetChannelItemTypePriceResponse;
import com.uniware.core.api.prices.GetChannelItemTypePricesRequest;
import com.uniware.core.api.prices.GetChannelItemTypePricesResponse;
import com.uniware.core.api.prices.GetHistoricalChannelItemTypePriceRequest;
import com.uniware.core.api.prices.WsChannelItemTypePrice;
import com.uniware.core.api.prices.WsChannelItemTypePrices;
import com.uniware.core.api.prices.dto.PriceDTO;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.entity.Channel;
import com.uniware.core.entity.Channel.SyncStatus;
import com.uniware.core.entity.ChannelItemType;
import com.uniware.core.entity.Source;
import com.uniware.core.script.error.ChannelScriptError;
import com.uniware.core.utils.UserContext;
import com.uniware.services.cache.ChannelCache;
import com.uniware.services.cache.ScriptVersionedCache;
import com.uniware.services.channel.IChannelCatalogService;
import com.uniware.services.channel.IChannelService;
import com.uniware.services.channel.impl.ChannelServiceImpl;
import com.uniware.services.channel.impl.ChannelServiceImpl.ChannelGoodToSyncResponse;
import com.uniware.services.configuration.SourceConfiguration;
import com.uniware.services.pricing.IPricingService;
import com.uniware.services.pricing.PricingSourceConfiguration;

/**
 * An implementation of pricing service that treats the channel pricing store as authoritative. i.e. its gets 
 * and puts prices directly in the channel's store. Basically acts as a proxy for CRUD operations on the
 * channels pricing panels.
 */
@Service("channelPricingService")
//@Metrics(registry = "service-registry")
public class ChannelPricingServiceImpl implements IPricingService {

    private static final Logger LOG = LoggerFactory.getLogger(ChannelPricingServiceImpl.class);

    @Autowired
    private IChannelService channelService;

    @Autowired
    private IChannelCatalogService channelCatalogService;

    /**
     * {@inheritDoc}
     */
    @Override
    @Timed
    public GetChannelItemTypePriceResponse getChannelItemTypePrice(final GetChannelItemTypePriceRequest request) {
        GetChannelItemTypePriceResponse response = new GetChannelItemTypePriceResponse();
        ValidationContext context = request.validate();
        try {
            if (!context.hasErrors()) {
                ChannelCache channelCache = CacheManager.getInstance().getCache(ChannelCache.class);
                Channel channel = channelCache.getChannelByCode(request.getChannelCode());
                if (channel == null) {
                    context.addError(WsResponseCode.INVALID_CHANNEL_CODE,
                            "Invalid channel code " + request.getChannelCode());
                } else {
                    ChannelGoodToSyncResponse channelPriceSyncReady = isChannelPriceSyncReady(channel, context);
                    if (channelPriceSyncReady.isGoodToSync()) {
                        ScraperScript priceUpdateScript =
                                channelCache.getScriptByName(channel.getCode(), Source.CHECK_PRICE_SCRIPT_NAME, true);
                        PriceDTO price = getPriceFromChannel(channel, request.getChannelProductId(),
                                priceUpdateScript, channelPriceSyncReady.getParams(), context);
                        response.setChannelItemTypePrice(price);
                        // We assume that prices on channel are dirty at all times.
                        response.setDirty(true);
                    }
                }
            }
        } catch (Exception e) {
            LOG.error("Error fetching prices for channelProductId: " + request.getChannelProductId(), e);
            context.addError(WsResponseCode.UNABLE_TO_CHECK_PRICE, e.getMessage());
            response.setMessage(e.getMessage());
        } finally {

        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        response.setSuccessful(!context.hasErrors());
        return response;
    }

    /**
     * Actual heavy lifting of pulling prices from the channel.
     */
    private PriceDTO getPriceFromChannel(
            final Channel channel, final String channelProductId,
            final ScraperScript priceUpdateScript, final Map<String, String> params,
            final ValidationContext context) throws Exception {
        ScriptExecutionContext seContext = ScriptExecutionContext.current();
        for (Entry<String, String> e : params.entrySet()) {
            seContext.addVariable(e.getKey(), e.getValue());
        }
        Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class)
                .getSourceByCode(channel.getSourceCode());
        seContext.addVariable("source", source);
        seContext.addVariable("channel", channel);
        seContext.addVariable("channelProductId", channelProductId);
        seContext.addVariable("fetchOnly", true);
        seContext.setScriptProvider(new IScriptProvider() {
            @Override
            public ScraperScript getScript(String scriptName) {
                return CacheManager.getInstance().getCache(ScriptVersionedCache.class).getScriptByName(scriptName);
            }
        });
        
        try {
            seContext.setTraceLoggingEnabled(UserContext.current().isTraceLoggingEnabled());
            priceUpdateScript.execute();
            String priceDetailXml = seContext.getScriptOutput();
            return JibxUtils.unmarshall("unicommerceServicesBinding19", PriceDTO.class, priceDetailXml);
        } catch (ScriptExecutionException se) {
            LOG.info("Unable to check price on channel " + se);
            context.addError(WsResponseCode.UNABLE_TO_PULL_PRICE, "Unable to pull price from channel. Code:" + se.getReasonCode());
            throw se;
        } finally {
            ScriptExecutionContext.destroy();
        }
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    @Timed
    public CheckChannelItemTypePriceResponse checkChannelItemTypePrice(final CheckChannelItemTypePriceRequest request) {
        ValidationContext context = request.validate();
        ChannelCache channelCache = CacheManager.getInstance().getCache(ChannelCache.class);
        Channel channel = channelCache.getChannelByCode(request.getChannelCode());
        if (channel == null) {
            context.addError(WsResponseCode.INVALID_CHANNEL_CODE, request.getChannelCode());
        } else {
            PricingSourceConfiguration pricingConfig = new PricingSourceConfiguration(channel.getSourceCode());
            if(!pricingConfig.isPriceComputationSupported()) {
                context.addError(WsResponseCode.INVALID_REQUEST, "Price computation is not allowed for channel " + request.getChannelCode());
            }
        }
        CheckChannelItemTypePriceResponse response = new CheckChannelItemTypePriceResponse();
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
            response.setSuccessful(false);
            return response;
        }
        
        // TODO add validation
        // - supported currency
        // - only SP price field present for snapdeal channel.

        try {
            ChannelGoodToSyncResponse channelPriceSyncReady = isChannelPriceSyncReady(channel, context);
            if (channelPriceSyncReady.isGoodToSync()) {
                ScraperScript checkPriceScript = channelCache.getScriptByName(
                    channel.getCode(), Source.CHECK_PRICE_SCRIPT_NAME, true);
                PriceDTO priceDTO = checkPriceOnChannel(
                    channel, channelPriceSyncReady.getParams(), request, checkPriceScript, context);
                response.setChannelItemTypePrice(priceDTO);
                response.setSuccessful(true);
            }
            else {
                context.addError(WsResponseCode.UNABLE_TO_CHECK_PRICE, "Cannot do price validation on channel");
            }
        } catch (Exception e) {
            LOG.error("Error checking prices for channelProductId: " + request.getChannelProductId(), e);
            context.addError(WsResponseCode.UNABLE_TO_CHECK_PRICE, e.getMessage());
            response.setMessage(e.getMessage());
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    private PriceDTO checkPriceOnChannel(Channel channel, Map<String, String> channelParams, 
            CheckChannelItemTypePriceRequest channelItemTypePrice,
            ScraperScript checkPriceScript, ValidationContext context) throws Exception
    {
        ScriptExecutionContext seContext = ScriptExecutionContext.current();
        for (Entry<String, String> e : channelParams.entrySet()) {
            seContext.addVariable(e.getKey(), e.getValue());
        }
        seContext.addVariable("channel", channel);
        seContext.addVariable("channelProductId", channelItemTypePrice.getChannelProductId());
        seContext.addVariable("priceRequest", channelItemTypePrice);
        seContext.addVariable("fetchOnly", false);
        seContext.setScriptProvider(new IScriptProvider() {
            @Override
            public ScraperScript getScript(String scriptName) {
                return CacheManager.getInstance().getCache(ScriptVersionedCache.class).getScriptByName(scriptName);
            }
        });
        try {
            seContext.setTraceLoggingEnabled(UserContext.current().isTraceLoggingEnabled());
            checkPriceScript.execute();
            String priceDetailXml = seContext.getScriptOutput();
            return JibxUtils.unmarshall("unicommerceServicesBinding19", PriceDTO.class, priceDetailXml);
        } catch (ScriptExecutionException se) {
            LOG.info("Unable to check price on channel " + se);
            context.addError(WsResponseCode.UNABLE_TO_CHECK_PRICE, "Unable to check price on channel " + se.getMessage());
            throw se;
        } finally {
            ScriptExecutionContext.destroy();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Timed
    public EditChannelItemTypePriceResponse editChannelItemTypePrice(
        final EditChannelItemTypePriceRequest request)
    {
        ValidationContext context = request.validate();
        ChannelCache channelCache = CacheManager.getInstance().getCache(ChannelCache.class);
        Channel channel = channelCache.getChannelByCode(request.getChannelCode());
        if (channel == null) {
            context.addError(WsResponseCode.INVALID_CHANNEL_CODE, request.getChannelCode());
        }
        
        EditChannelItemTypePriceResponse response = new EditChannelItemTypePriceResponse();
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
            response.setSuccessful(false);
            return response;
        }
        PriceDTO priceDTO = new PriceDTO(request.getMsp(), request.getMrp(), request.getSellingPrice(), 
                null, null, request.getChannelCode());
        try {
            ChannelGoodToSyncResponse channelPriceSyncReady = isChannelPriceSyncReady(channel, context);
            if (channelPriceSyncReady.isGoodToSync()) {
                ScraperScript priceUpdateScript = 
                    channelCache.getScriptByName(channel.getCode(), Source.UPDATE_PRICE_SCRIPT_NAME, true);
                priceDTO = pushPriceOnChannel(
                    channel, channelPriceSyncReady.getParams(), request, priceUpdateScript, context);
                if(!context.hasErrors()) {
                    response.setChannelItemTypePrice(priceDTO);
                    response.setSuccessful(true);
                }
            } else {
                context.addError(WsResponseCode.BAD_CHANNEL_STATE, channelPriceSyncReady.getMessage());
                response.setMessage(channelPriceSyncReady.getMessage());
            }
        } catch (Exception e) {
            String channelPricePushMessage = e.getMessage(); 
            LOG.error("Error updating prices for channelProductId: " + request.getChannelProductId(), e);
            context.addError(WsResponseCode.UNABLE_TO_PUSH_PRICE, e.getMessage());
            response.setMessage(channelPricePushMessage);
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Timed
    public EditBulkChannelItemPriceResponse editChannelItemTypePrice(
            final EditBulkChannelItemPriceRequest request)
    {
        ValidationContext context = request.validate();
        ChannelCache channelCache = CacheManager.getInstance().getCache(ChannelCache.class);
        Channel channel = channelCache.getChannelByCode(request.getChannelCode());
        if (channel == null) {
            context.addError(WsResponseCode.INVALID_CHANNEL_CODE, request.getChannelCode());
        }

        EditBulkChannelItemPriceResponse response = new EditBulkChannelItemPriceResponse();

        try {
            ChannelGoodToSyncResponse channelPriceSyncReady = isChannelPriceSyncReady(channel, context);
            if (channelPriceSyncReady.isGoodToSync()) {
                ScraperScript priceUpdateScript =
                        channelCache.getScriptByName(channel.getCode(), Source.UPDATE_PRICE_SCRIPT_NAME, true);
                 response= pushPriceOnChannel(
                        channel, channelPriceSyncReady.getParams(), request, priceUpdateScript, context);
                if(!context.hasErrors()) {
                    response.setSuccessful(true);
                }
                else {
                    response.setErrors(context.getErrors());
                    response.setSuccessful(false);
                }
            } else {
                context.addError(WsResponseCode.BAD_CHANNEL_STATE, channelPriceSyncReady.getMessage());
                response.setMessage(channelPriceSyncReady.getMessage());
            }
        } catch (Exception e) {
            String channelPricePushMessage = e.getMessage();
            LOG.error("Error updating bulk prices ", e);
            context.addError(WsResponseCode.UNABLE_TO_PUSH_PRICE, e.getMessage());
            response.setMessage(channelPricePushMessage);
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        return response;
    }
    
    /**
     * Update prices given by the {@code channelPriceSyncDTO} to the given channel.
     */
    private PriceDTO  pushPriceOnChannel(
        final Channel channel, final Map<String, String> channelParams, 
        final EditChannelItemTypePriceRequest request,
        final ScraperScript updatePriceScript, final ValidationContext context) throws Exception
    {
        ScriptExecutionContext seContext = ScriptExecutionContext.current();
        for (Entry<String, String> e : channelParams.entrySet()) {
            seContext.addVariable(e.getKey(), e.getValue());
        }
        Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class)
                                                          .getSourceByCode(channel.getSourceCode());
        if(source.getCode().contains("AMAZON")) {
            ChannelItemType channelItemType = channelCatalogService.getChannelItemTypeByChannelAndChannelProductId(request.getChannelCode()
                    , request.getChannelProductId());
            seContext.addVariable("sellerSkuCode", channelItemType.getSellerSkuCode());
        }
        seContext.addVariable("source", source);
        seContext.addVariable("channel", channel);
        seContext.addVariable("channelProductId", request.getChannelProductId());
        seContext.addVariable("priceRequest", request);
        seContext.addVariable("isBulkEditpriceRequest", "false");
        seContext.setScriptProvider(new IScriptProvider() {
            @Override
            public ScraperScript getScript(String scriptName) {
                return CacheManager.getInstance().getCache(ScriptVersionedCache.class).getScriptByName(scriptName);
            }
        });
        try {
            seContext.setTraceLoggingEnabled(UserContext.current().isTraceLoggingEnabled());
            updatePriceScript.execute();
            String channelItemTypePricesXml = seContext.getScriptOutput();
            WsChannelItemTypePrices channelItemTypeUpdatedPrices = 
                JibxUtils.unmarshall("unicommerceServicesBinding19", WsChannelItemTypePrices.class, channelItemTypePricesXml);
            List<WsChannelItemTypePrice> channelItemTypePriceList = channelItemTypeUpdatedPrices.getChannelItemTypePrices();
            if (channelItemTypePriceList.isEmpty()) {
                context.addError(WsResponseCode.INVALID_SCRIPT_RESPONSE,"Empty response from script");
                return null;
            }
            WsChannelItemTypePrice price = channelItemTypePriceList.get(0);
            return price.getPrices();
        } catch (ScriptExecutionException se) {
            if (ChannelScriptError.ScriptErrorCodes.SERVER_DOWN.name().equals(se.getMessage())) {
                context.addError(WsResponseCode.UNABLE_TO_PUSH_PRICE, ChannelScriptError.ScriptErrorCodes.SERVER_DOWN.name());
            } else if (ChannelScriptError.ScriptErrorCodes.CHANNEL_ERROR.name().equals(se.getMessage()) ) {
                context.addError(WsResponseCode.UNABLE_TO_PUSH_PRICE,ChannelScriptError.ScriptErrorCodes.CHANNEL_ERROR.name());
            } else {
                context.addError(WsResponseCode.UNABLE_TO_PUSH_PRICE, se.getMessage());
            }
            LOG.info("Unable to push price on channel " + se);
            throw se;
        } finally {
            ScriptExecutionContext.destroy();
        }
    }

    /**
     * Update prices given by the {@code channelPriceSyncDTO} to the given channel.
     */
    private EditBulkChannelItemPriceResponse pushPriceOnChannel(
            final Channel channel, final Map<String, String> channelParams,
            final EditBulkChannelItemPriceRequest request,
            final ScraperScript updatePriceScript, final ValidationContext context) throws Exception
    {
        ScriptExecutionContext seContext = ScriptExecutionContext.current();
        for (Entry<String, String> e : channelParams.entrySet()) {
            seContext.addVariable(e.getKey(), e.getValue());
        }
        EditBulkChannelItemPriceResponse response = new EditBulkChannelItemPriceResponse();
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
            response.setSuccessful(false);
            return response;
        }
        Map<String, String> failedPriceSyncCITs = new HashMap(request.getChannelItemTypePriceRequests().size());
        Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class)
                .getSourceByCode(channel.getSourceCode());
        seContext.addVariable("source", source);
        seContext.addVariable("channel", channel);
        seContext.addVariable("priceRequest", request);
        seContext.addVariable("failedPriceSyncCITs", failedPriceSyncCITs);
        seContext.addVariable("isBulkEditpriceRequest", "true");
        seContext.setScriptProvider(new IScriptProvider() {
            @Override
            public ScraperScript getScript(String scriptName) {
                return CacheManager.getInstance().getCache(ScriptVersionedCache.class).getScriptByName(scriptName);
            }
        });
        try {
            seContext.setTraceLoggingEnabled(UserContext.current().isTraceLoggingEnabled());
            updatePriceScript.execute();
            String channelItemTypePricesXml = seContext.getScriptOutput();
            response.setFailedChannelProductIdToErrorMessage(failedPriceSyncCITs);
            WsChannelItemTypePrices channelItemTypeUpdatedPrices =
                    JibxUtils.unmarshall("unicommerceServicesBinding19", WsChannelItemTypePrices.class, channelItemTypePricesXml);
            List<WsChannelItemTypePrice> channelItemTypePriceList = channelItemTypeUpdatedPrices.getChannelItemTypePrices();
            if (channelItemTypePriceList.isEmpty()) {
                context.addError(WsResponseCode.INVALID_SCRIPT_RESPONSE,"Empty response from script");
                return null;
            }
            else {
                for(WsChannelItemTypePrice channelItemTypePrice : channelItemTypePriceList) {
                    response.getChannelProductIdToPriceDTO().put(channelItemTypePrice.getChannelProductId(),channelItemTypePrice.getPrices());
                }
            }

        } catch (ScriptExecutionException se) {
            if (ChannelScriptError.ScriptErrorCodes.SERVER_DOWN.name().equals(se.getMessage())) {
                context.addError(WsResponseCode.UNABLE_TO_PUSH_PRICE, ChannelScriptError.ScriptErrorCodes.SERVER_DOWN.name());
            } else if (ChannelScriptError.ScriptErrorCodes.CHANNEL_ERROR.name().equals(se.getMessage()) ) {
                context.addError(WsResponseCode.UNABLE_TO_PUSH_PRICE,ChannelScriptError.ScriptErrorCodes.CHANNEL_ERROR.name());
            } else {
                context.addError(WsResponseCode.UNABLE_TO_PUSH_PRICE, se.getMessage());
            }
            LOG.info("Unable to push price on channel " + se);
            throw se;
        } finally {
            ScriptExecutionContext.destroy();
        }
        return response;
    }

    /**
     * Unsupported.
     * <p>
     * {@inheritDoc}
     */
    @Override
    public GetChannelItemTypePriceResponse getHistoricalChannelItemTypePrice(
        final GetHistoricalChannelItemTypePriceRequest request) 
    {
        throw new UnsupportedOperationException("Channel price mangement doesn't support historical prices yet");
    }
    
    /**
     * Determines if the given channel is ready for price sync.
     * <li>Checks if the channel source supports price sync
     * <li>Checks if the price sync is on for the channel
     * <li>Checks the connector status
     */
    @Timed
    private ChannelGoodToSyncResponse isChannelPriceSyncReady(
        final Channel channel, final ValidationContext context) 
    {
        Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class)
                        .getSourceByCode(channel.getSourceCode());
        ChannelGoodToSyncResponse response = new ChannelGoodToSyncResponse();
        if (!channel.isEnabled()) {
            response.setGoodToSync(false);
            if (context != null) {
                context.addError(WsResponseCode.BAD_CHANNEL_STATE, "Channel is disabled " + channel.getCode());
            }
        }
        else if (!source.isPricingSyncConfigured()) {
            response.setGoodToSync(false);
            if (context != null) {
                context.addError(WsResponseCode.PRICE_SYNC_NOT_CONFIGURED, "Price sync not available for " 
                               + source.getCode());
                response.setMessage("Price sync not available for");
            }
        } 
        else if (!SyncStatus.ON.equals(channel.getPricingSyncStatus())) {
            response.setGoodToSync(false);
            if (context != null) {
                context.addError(WsResponseCode.PRICE_SYNC_NOT_CONFIGURED, "Price sync not configured for channel " 
                               + channel.getSourceCode());
                response.setMessage("Price sync not configured");
            }
        }
        else {
            response = channelService.isChannelGoodToSync(channel.getCode(), ChannelServiceImpl.Sync.CATALOG);
            if (!response.isGoodToSync()) {
                LOG.warn("Connector broken for channel {}", channel.getName());
                response.setMessage("Connector broken");
            }
        }
        return response;
    }

    @Override
    public GetChannelItemTypePricesResponse getChannelItemTypePrices(GetChannelItemTypePricesRequest request) {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Channel price mangement doesn't support get prices by seller sku yet");
    }

    @Override
    public List<WsError> validatePrices(PriceDTO oldPrices, PriceDTO newPrices, String channelCode) {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Operation not supported");
    }
    
    
}
