/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version 1.0, Oct 29, 2015
 *  @author bhupi
 */
package com.uniware.services.pricing.impl;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unifier.core.api.base.ServiceResponse;
import com.uniware.core.api.prices.EditChannelItemTypePriceResponse;
import com.uniware.core.api.prices.GetChannelItemTypePriceRequest;
import com.uniware.core.api.prices.GetChannelItemTypePriceResponse;
import com.uniware.core.api.prices.MergeChannelItemTypePriceRequest;
import com.uniware.core.api.prices.dto.PriceDTO;
import com.uniware.services.pricing.IPricingService;

/**
 * A pipe that transfers {@code PriceDTO} objects between a source and a sink {@code IPricingService}. 
 */
class PricingServicePipe {
    
    private static final Logger LOG = LoggerFactory.getLogger(PricingServicePipe.class);
    
    private final IPricingService source;
    private final IPricingService sink;
    
    @SuppressWarnings("serial")
    public static class PipeResponse extends ServiceResponse {
        
        private PriceDTO transferredData;
        private Date sourceDataVersion;

        public Date getSourceDataVersion() {
            return sourceDataVersion;
        }

        public void setSourceDataVersion(final Date sourceDataVersion) {
            this.sourceDataVersion = sourceDataVersion;
        }

        public PriceDTO getTransferredData() {
            return transferredData;
        }

        public void setTransferredData(final PriceDTO transferredPrices) {
            this.transferredData = transferredPrices;
        }
    }

    public PricingServicePipe(final IPricingService source, final IPricingService sink) {
        this.source = source;
        this.sink = sink;
    }
    
    /**
     * Fetch price from source {@code IPricingService} and transfer to the sink {@code IPricingService}.
     * <p>
     * If the source is not dirty (i.e. source does not have any pending edits) and {@code forceTransfer} 
     * is not specified the price transfer is aborted.
     * <p>
     * If the sink is dirty (i.e. sink has pending price edits) and {@code forceTransfer} is not specified,
     * the price transfer is aborted.
     */
    public PipeResponse transfer(
        final String channelCode, final String channelProductId, final boolean forceTransfer) 
    {
        PipeResponse response = new PipeResponse();
        
        GetChannelItemTypePriceRequest getPricesFromSourceRequest = new GetChannelItemTypePriceRequest();
        getPricesFromSourceRequest.setChannelCode(channelCode);
        getPricesFromSourceRequest.setChannelProductId(channelProductId);
        LOG.info("Getting prices from source for {}:{}", channelCode, channelProductId);
        GetChannelItemTypePriceResponse pricesFromSourceResponse = 
            source.getChannelItemTypePrice(getPricesFromSourceRequest);
        response.setResponse(pricesFromSourceResponse);
        response.setSourceDataVersion(pricesFromSourceResponse.getVersion());
        if (! response.isSuccessful()) {
            return response;
        }
        if (pricesFromSourceResponse.isDirty() || forceTransfer) {
            MergeChannelItemTypePriceRequest editPricesInSinkRequest = 
                createMergePriceRequest(
                    channelCode, channelProductId, pricesFromSourceResponse.getChannelItemTypePrice());
            editPricesInSinkRequest.setIsForceEdit(forceTransfer);
            LOG.info("Transferring prices to sink for {}:{}", channelCode, channelProductId);
            EditChannelItemTypePriceResponse editPricesInSinkResponse = 
                sink.editChannelItemTypePrice(editPricesInSinkRequest);
            response.setResponse(editPricesInSinkResponse);
            response.setTransferredData(editPricesInSinkResponse.getChannelItemTypePrice());
        }
        else {
            LOG.info("Prices from source are not dirty and forceTransfer is not specified, so skipping price"
                    + " transfer for {}:{}", channelCode, channelProductId);
            response.setMessage("Skipping transfer of non-dirty prices");
        }
        return response;
    }
    
    private MergeChannelItemTypePriceRequest createMergePriceRequest(
        final String channelCode, final String channelProductId, final PriceDTO prices) 
    {
        MergeChannelItemTypePriceRequest editPricesInUniwareRequest = new MergeChannelItemTypePriceRequest();
        editPricesInUniwareRequest.setChannelCode(channelCode);
        editPricesInUniwareRequest.setChannelProductId(channelProductId);
        editPricesInUniwareRequest.setSellingPrice(prices.getSellingPrice());
        editPricesInUniwareRequest.setTransferPrice(prices.getTransferPrice());
        editPricesInUniwareRequest.setMsp(prices.getMsp());
        editPricesInUniwareRequest.setMrp(prices.getMrp());
        editPricesInUniwareRequest.setCompetitivePrice(prices.getCompetitivePrice());
        editPricesInUniwareRequest.setCurrencyCode(prices.getCurrencyCode());
        return editPricesInUniwareRequest;
    }
}
