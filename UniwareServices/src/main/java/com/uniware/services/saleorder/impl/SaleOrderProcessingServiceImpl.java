/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 26, 2011
 *  @author singla
 */
package com.uniware.services.saleorder.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import com.uniware.core.entity.PickSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.unifier.core.annotation.audit.LogActivity;
import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.expressions.Expression;
import com.unifier.core.utils.CollectionUtils;
import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.NumberUtils;
import com.unifier.core.utils.StringUtils;
import com.unifier.scraper.sl.runtime.ScraperScript;
import com.unifier.scraper.sl.runtime.ScriptExecutionContext;
import com.uniware.core.api.inflow.AddSaleOrderItemDetailRequest;
import com.uniware.core.api.inflow.AddSaleOrderItemDetailResponse;
import com.uniware.core.api.saleorder.AllocateInventoryAndGenerateInvoiceRequest;
import com.uniware.core.api.saleorder.AllocateInventoryAndGenerateInvoiceResponse;
import com.uniware.core.api.saleorder.AllocateInventoryCreatePackageRequest;
import com.uniware.core.api.saleorder.AllocateInventoryCreatePackageResponse;
import com.uniware.core.api.saleorder.SaleOrderItemForDetail;
import com.uniware.core.api.shipping.ForceDispatchShippingPackageRequest;
import com.uniware.core.api.shipping.ForceDispatchShippingPackageResponse;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.cache.FacilityCache;
import com.uniware.core.entity.Channel;
import com.uniware.core.entity.ChannelConfigurationParameter;
import com.uniware.core.entity.Facility;
import com.uniware.core.entity.FacilityAllocationRule;
import com.uniware.core.entity.FacilityItemType;
import com.uniware.core.entity.ItemType;
import com.uniware.core.entity.ItemTypeInventory;
import com.uniware.core.entity.ItemTypeInventorySnapshot;
import com.uniware.core.entity.SaleOrder;
import com.uniware.core.entity.SaleOrderItem;
import com.uniware.core.entity.ShippingMethod;
import com.uniware.core.entity.ShippingPackage;
import com.uniware.core.entity.Source;
import com.uniware.core.entity.VendorItemType;
import com.uniware.core.locking.Namespace;
import com.uniware.core.locking.annotation.Lock;
import com.uniware.core.locking.annotation.Locks;
import com.uniware.core.utils.ActivityContext;
import com.uniware.core.utils.UserContext;
import com.uniware.core.vo.FacilityAllocationVO;
import com.uniware.dao.catalog.ICatalogDao;
import com.uniware.dao.saleorder.ISaleOrderDao;
import com.uniware.dao.saleorder.ISaleOrderMao;
import com.uniware.dao.shipping.IShippingDao;
import com.uniware.services.audit.impl.ActivityEntityEnum;
import com.uniware.services.audit.impl.ActivityTypeEnum;
import com.uniware.services.audit.impl.ActivityUtils;
import com.uniware.services.cache.ChannelCache;
import com.uniware.services.cache.ScriptVersionedCache;
import com.uniware.services.catalog.ICatalogService;
import com.uniware.services.configuration.SourceConfiguration;
import com.uniware.services.configuration.SystemConfiguration;
import com.uniware.services.configuration.data.manager.ProductConfiguration;
import com.uniware.services.exception.NoAvailableShippingProviderException;
import com.uniware.services.inflow.IInflowService;
import com.uniware.services.inventory.IInventoryService;
import com.uniware.services.saleorder.ISaleOrderProcessingService;
import com.uniware.services.saleorder.ISaleOrderService;
import com.uniware.services.shipping.IDispatchService;
import com.uniware.services.shipping.IShippingProviderService;
import com.uniware.services.shipping.IShippingService;

/**
 * @author singla
 */
@Service
public class SaleOrderProcessingServiceImpl implements ISaleOrderProcessingService {

    private static final Logger      LOG = LoggerFactory.getLogger(SaleOrderProcessingServiceImpl.class);

    @Autowired
    private IInventoryService        inventoryService;

    @Autowired
    private ISaleOrderDao            saleOrderDao;

    @Autowired
    private IShippingService         shippingService;

    @Autowired
    private ICatalogService          catalogService;

    @Autowired
    private ICatalogDao              catalogDao;

    @Autowired
    private IShippingDao             shippingDao;

    @Autowired
    private ISaleOrderService        saleOrderService;

    @Autowired
    private IShippingProviderService shippingProviderService;

    @Autowired
    private ISaleOrderMao            saleOrderMao;

    @Autowired
    private IDispatchService         dispatchService;

    @Autowired
    private IInflowService           inflowService;

    @Override
    @LogActivity
    public AllocateInventoryCreatePackageResponse allocateInventoryAndCreatePackage(AllocateInventoryCreatePackageRequest request) {
        AllocateInventoryCreatePackageResponse response = new AllocateInventoryCreatePackageResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            SaleOrder so = saleOrderService.getSaleOrderById(request.getSaleOrderId());
            LOG.info("Processing sale order:{}", so.getCode());
            response = doAllocateInventoryAndCreatePackage(so.getCode(), response);
            LOG.info("Done processing sale order:{}", so.getCode());
        } else {
            response.addErrors(context.getErrors());
        }
        return response;
    }

    @Override
    @LogActivity
    @Locks({ @Lock(ns = Namespace.SALE_ORDER, key = "#{#args[0].saleOrderCode}") })
    @Transactional
    public AllocateInventoryAndGenerateInvoiceResponse allocateInventoryAndGenerateInvoice(AllocateInventoryAndGenerateInvoiceRequest request) {
        AllocateInventoryAndGenerateInvoiceResponse response = new AllocateInventoryAndGenerateInvoiceResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            SaleOrder saleOrder = saleOrderService.getSaleOrderByCode(request.getSaleOrderCode());
            if (saleOrder == null) {
                context.addError(WsResponseCode.INVALID_SALE_ORDER_CODE, "Sale Order not found");
            } else if (!SaleOrder.StatusCode.CREATED.name().equals(saleOrder.getStatusCode())) {
                context.addError(WsResponseCode.INVALID_SALE_ORDER_STATE, "Sale Order not in created state");
            } else if (saleOrder.isProductManagementSwitchedOff()) {
                context.addError(WsResponseCode.INVALID_SALE_ORDER_STATE, "Action not applicable for this sale order");
            } else {
                AllocateInventoryCreatePackageRequest allocateInventoryCreatePackageRequest = new AllocateInventoryCreatePackageRequest();
                allocateInventoryCreatePackageRequest.setSaleOrderId(saleOrder.getId());
                AllocateInventoryCreatePackageResponse allocateInventoryCreatePackageResponse = allocateInventoryAndCreatePackage(allocateInventoryCreatePackageRequest);
                if (!allocateInventoryCreatePackageResponse.isSuccessful()) {
                    context.addErrors(allocateInventoryCreatePackageResponse.getErrors());
                } else if (CollectionUtils.isEmpty(allocateInventoryCreatePackageResponse.getShippingPackageCodes())) {
                    context.addError(WsResponseCode.INVALID_SALE_ORDER_STATE, "Order not fulfillable");
                } else {
                    LOG.info("SaleOrder: {}, shipping package codes: {}", saleOrder.getCode(), allocateInventoryCreatePackageResponse.getShippingPackageCodes());
                    if (request.getSaleOrderItemForDetailList() != null) {
                        for (SaleOrderItemForDetail saleOrderItemForDetail : request.getSaleOrderItemForDetailList()) {
                            SaleOrderItem saleOrderItem = saleOrder.getSaleOrderItemsMap().get(saleOrderItemForDetail.getSaleOrderItemCode());
                            if (saleOrderItem != null) {
                                if (saleOrderItem.getItemDetailingStatus() == SaleOrderItem.ItemDetailingStatus.PENDING
                                        && StringUtils.isNotBlank(saleOrderItem.getItemDetailFields())) {
                                    AddSaleOrderItemDetailRequest addSaleOrderItemDetailRequest = new AddSaleOrderItemDetailRequest();
                                    addSaleOrderItemDetailRequest.setSaleOrderCode(saleOrder.getCode());
                                    addSaleOrderItemDetailRequest.setSaleOrderItemCode(saleOrderItem.getCode());
                                    addSaleOrderItemDetailRequest.setItemDetails(saleOrderItemForDetail.getItemDetails());
                                    AddSaleOrderItemDetailResponse addSaleOrderItemDetailResponse = inflowService.addSaleOrderItemDetails(addSaleOrderItemDetailRequest);
                                    if (addSaleOrderItemDetailResponse.hasErrors()) {
                                        context.addErrors(addSaleOrderItemDetailResponse.getErrors());
                                        break;
                                    }
                                }
                            } else {
                                context.addError(WsResponseCode.INVALID_ITEM_CODE, "sale Order Item not found");
                                break;
                            }
                        }
                    }
                    List<String> invoiceCodes = new ArrayList<>();
                    for (String shippingPackageCode : allocateInventoryCreatePackageResponse.getShippingPackageCodes()) {
                        ForceDispatchShippingPackageRequest forceDispatchShippingPackageRequest = new ForceDispatchShippingPackageRequest();
                        forceDispatchShippingPackageRequest.setShippingPackageCode(shippingPackageCode);
                        forceDispatchShippingPackageRequest.setUserId(request.getUserId());
                        forceDispatchShippingPackageRequest.setSkipDetailing(false);
                        ForceDispatchShippingPackageResponse forceDispatchShippingPackageResponse = dispatchService.forceDispatchShippingPackage(
                                forceDispatchShippingPackageRequest);
                        if (forceDispatchShippingPackageResponse.hasErrors()) {
                            context.addErrors(forceDispatchShippingPackageResponse.getErrors());
                            break;
                        }
                        ShippingPackage shippingPackage = shippingService.getShippingPackageByCode(shippingPackageCode);
                        invoiceCodes.add(shippingPackage.getInvoice().getCode());
                    }
                    if (!context.hasErrors()) {
                        response.setInvoiceCodes(invoiceCodes);
                        response.setSuccessful(true);
                    }
                }
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
            LOG.info("Context Error for allocateInventoryAndGenerateInvoice : {}", context.getErrors().toString());
        }
        return response;
    }

    @Locks({ @Lock(ns = Namespace.SALE_ORDER, key = "#{#args[0]}") })
    @Transactional
    private AllocateInventoryCreatePackageResponse doAllocateInventoryAndCreatePackage(String saleOrderCode, AllocateInventoryCreatePackageResponse response) {
        SaleOrder saleOrder = saleOrderDao.getSaleOrderForUpdate(saleOrderCode);
        Channel channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelById(saleOrder.getChannel().getId());
        List<String> activityLogs = new ArrayList<>();
        boolean activityEnabled = ActivityContext.current().isEnable();
        if (SaleOrder.StatusCode.CREATED.name().equals(saleOrder.getStatusCode())) {
            saleOrder.setStatusCode(SaleOrder.StatusCode.PROCESSING.name());
            LOG.info("SaleOrderCode " + saleOrderCode + " status moved to Processing");
            if (activityEnabled) {
                activityLogs.add("Order status moved to Processing");
            }
        }
        Map<ItemType, Set<SaleOrderItem>> reassessInventoryItems = new HashMap<ItemType, Set<SaleOrderItem>>();
        Map<String, List<String>> statusCodeToSaleOrderItems = new HashMap<>();
        for (SaleOrderItem saleOrderItem : saleOrder.getSaleOrderItems()) {
            if (StringUtils.equalsAny(saleOrderItem.getStatusCode(), SaleOrderItem.StatusCode.CREATED.name(), SaleOrderItem.StatusCode.UNFULFILLABLE.name(),
                    SaleOrderItem.StatusCode.LOCATION_NOT_SERVICEABLE.name()) && !saleOrderItem.isOnHold() && saleOrderItem.getFacility() != null
                    && saleOrderItem.getFacility().getId().equals(UserContext.current().getFacilityId())) {
                String oldStatus = saleOrderItem.getStatusCode();
                if (StringUtils.equalsAny(saleOrderItem.getStatusCode(), SaleOrderItem.StatusCode.CREATED.name(), SaleOrderItem.StatusCode.LOCATION_NOT_SERVICEABLE.name())
                        && saleOrderItem.getShippingProvider() == null) {
                    if (saleOrder.getChannel().isThirdPartyShipping()) {
                        saleOrderItem.setStatusCode(SaleOrderItem.StatusCode.CREATED.name());
                    } else if (!ConfigurationManager.getInstance().getConfiguration(ProductConfiguration.class).isCourierManagementSwitchedOff()
                            && !ShippingMethod.Code.PKP.name().equals(saleOrderItem.getShippingMethod().getCode())) {
                        try {
                            shippingProviderService.getAvailableShippingProviders(saleOrderItem.getShippingMethod().getId(), saleOrderItem.getShippingAddress().getPincode(), null);
                            saleOrderItem.setStatusCode(SaleOrderItem.StatusCode.CREATED.name());
                        } catch (NoAvailableShippingProviderException e) {
                            LOG.info("SaleOrderCode: {}, SaleOrderItemCode {} status changed to LOCATION_NOT_SERVICEABLE", saleOrderCode, saleOrderItem.getCode());
                            saleOrderItem.setStatusCode(SaleOrderItem.StatusCode.LOCATION_NOT_SERVICEABLE.name());
                        }
                    }
                }
                if (StringUtils.equalsAny(saleOrderItem.getStatusCode(), SaleOrderItem.StatusCode.CREATED.name(), SaleOrderItem.StatusCode.UNFULFILLABLE.name())) {
                    if (ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).isInventoryManagementSwitchedOff()) {
                        saleOrderItem.setStatusCode(SaleOrderItem.StatusCode.FULFILLABLE.name());
                    } else {
                        Set<SaleOrderItem> saleOrderItems = reassessInventoryItems.get(saleOrderItem.getItemType());
                        if (saleOrderItems == null) {
                            saleOrderItems = new LinkedHashSet<SaleOrderItem>();
                            reassessInventoryItems.put(saleOrderItem.getItemType(), saleOrderItems);
                        }
                        saleOrderItems.add(saleOrderItem);
                    }
                }
                if (activityEnabled && !oldStatus.equalsIgnoreCase(saleOrderItem.getStatusCode())) {
                    if (!statusCodeToSaleOrderItems.containsKey(saleOrderItem.getStatusCode())) {
                        statusCodeToSaleOrderItems.put(saleOrderItem.getStatusCode(), new ArrayList<String>());
                    }
                    statusCodeToSaleOrderItems.get(saleOrderItem.getStatusCode()).add(saleOrderItem.getCode());
                }
            }
            LOG.info("SaleOrderCode: {}, SaleOrderItemCode: {} status: {}", saleOrderCode, saleOrderItem.getCode(), saleOrderItem.getStatusCode());
        }
        for (Map.Entry<ItemType, Set<SaleOrderItem>> entry : reassessInventoryItems.entrySet()) {
            Map<ItemTypeInventory, Integer> itemTypeInventories = inventoryService.blockAvailableInventory(entry.getKey(), channel.getId(), entry.getValue().size());
            Iterator<SaleOrderItem> iterator = entry.getValue().iterator();
            for (Map.Entry<ItemTypeInventory, Integer> inventoryEntry : itemTypeInventories.entrySet()) {
                for (int i = 0; i < inventoryEntry.getValue(); i++) {
                    SaleOrderItem saleOrderItem = iterator.next();
                    saleOrderItem.setItemTypeInventory(inventoryEntry.getKey());
                    String oldStatus = saleOrderItem.getStatusCode();
                    saleOrderItem.setStatusCode(SaleOrderItem.StatusCode.FULFILLABLE.name());
                    if (activityEnabled && !oldStatus.equalsIgnoreCase(saleOrderItem.getStatusCode())) {
                        if (!statusCodeToSaleOrderItems.containsKey(saleOrderItem.getStatusCode())) {
                            statusCodeToSaleOrderItems.put(saleOrderItem.getStatusCode(), new ArrayList<String>());
                        }
                        statusCodeToSaleOrderItems.get(saleOrderItem.getStatusCode()).add(saleOrderItem.getCode());
                    }
                }
            }
            while (iterator.hasNext()) {
                SaleOrderItem saleOrderItem = iterator.next();
                String oldStatus = saleOrderItem.getStatusCode();
                saleOrderItem.setStatusCode(SaleOrderItem.StatusCode.UNFULFILLABLE.name());
                if (activityEnabled && !oldStatus.equalsIgnoreCase(saleOrderItem.getStatusCode())) {
                    if (!statusCodeToSaleOrderItems.containsKey(saleOrderItem.getStatusCode())) {
                        statusCodeToSaleOrderItems.put(saleOrderItem.getStatusCode(), new ArrayList<String>());
                    }
                    statusCodeToSaleOrderItems.get(saleOrderItem.getStatusCode()).add(saleOrderItem.getCode());
                }
            }
        }
        Map<Integer, List<SaleOrderItem>> predefinedPacketItems = new HashMap<>();
        for (SaleOrderItem saleOrderItem : saleOrder.getSaleOrderItems()) {
            if (saleOrderItem.getFacility() != null && saleOrderItem.getFacility().getId().equals(UserContext.current().getFacilityId())
                    && canProcessSaleOrderItem(saleOrderItem)) {
                List<SaleOrderItem> packetItems = predefinedPacketItems.get(saleOrderItem.getPacketNumber());
                if (packetItems == null) {
                    packetItems = new ArrayList<>();
                    predefinedPacketItems.put(saleOrderItem.getPacketNumber(), packetItems);
                }
                packetItems.add(saleOrderItem);
            }
        }

        SaleOrderItem.PackageType packageType;
        if (ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).isPredefinedPackagesFlexible()) {
            packageType = SaleOrderItem.PackageType.FLEXIBLE;
        } else {
            ChannelConfigurationParameter ccp = channel.getParameter(Channel.PACKAGE_TYPE);
            packageType = ccp != null ? SaleOrderItem.PackageType.valueOf(ccp.getValue()) : null;
        }

        List<String> shippingPackageCodes = new ArrayList<>();
        for (Map.Entry<Integer, List<SaleOrderItem>> entry : predefinedPacketItems.entrySet()) {
            if (entry.getKey() != 0 || (packageType != null && packageType.equals(SaleOrderItem.PackageType.FIXED))) {
                shippingPackageCodes.addAll(createFixedPackages(entry.getValue()));
            } else {
                shippingPackageCodes.addAll(createFlexiblePackages(entry.getValue()));
            }
        }

        if (activityEnabled) {
            for (String key : statusCodeToSaleOrderItems.keySet()) {
                activityLogs.add("OrderItem {" + ActivityEntityEnum.SALE_ORDER_ITEM + ":" + statusCodeToSaleOrderItems.get(key) + "} has been marked " + key);
            }
        }
        saleOrder.setReassessInventory(false);
        saleOrderDao.updateSaleOrder(saleOrder);
        if (activityEnabled && activityLogs.size() > 0) {
            ActivityUtils.appendActivity(saleOrder.getCode(), ActivityEntityEnum.SALE_ORDER.getName(), null, activityLogs, ActivityTypeEnum.PROCESSING.name());
        }
        response.setShippingPackageCodes(shippingPackageCodes);
        response.setSuccessful(true);
        return response;
    }

    private List<String> createFlexiblePackages(List<SaleOrderItem> saleOrderItems) {
        List<String> shippingPackageCodes = new ArrayList<>();
        Set<List<SaleOrderItem>> generatedPackageList = new HashSet<>();
        Map<String, List<SaleOrderItem>> combinations = new HashMap<>();
        for (SaleOrderItem saleOrderItem : saleOrderItems) {
            if (StringUtils.isBlank(saleOrderItem.getCombinationIdentifier())) {
                addToSuitablePackageList(saleOrderItem, generatedPackageList);
            } else {
                List<SaleOrderItem> combinationItems = combinations.get(saleOrderItem.getCombinationIdentifier());
                if (combinationItems == null) {
                    combinationItems = new ArrayList<>();
                    combinations.put(saleOrderItem.getCombinationIdentifier(), combinationItems);
                }
                combinationItems.add(saleOrderItem);
            }
        }
        for (Map.Entry<String, List<SaleOrderItem>> entry : combinations.entrySet()) {
            List<SaleOrderItem> suggestedPackage = addToSuitablePackageList(entry.getValue().get(0), generatedPackageList);
            for (int i = 1; i < entry.getValue().size(); i++) {
                suggestedPackage.add(entry.getValue().get(i));
            }
        }
        for (List<SaleOrderItem> generatedPackage : generatedPackageList) {
            if (isPackageFulfillable(generatedPackage)) {
                ShippingPackage shippingPackage = shippingService.createShippingPackage(generatedPackage, true);
                shippingPackageCodes.add(shippingPackage.getCode());
            } else {
                Set<String> unfulfillableCombinations = new HashSet<String>();
                Iterator<SaleOrderItem> iterator = generatedPackage.iterator();
                Date oldestFulfillableItem = DateUtils.getCurrentTime();
                while (iterator.hasNext()) {
                    SaleOrderItem saleOrderItem = iterator.next();
                    if (!SaleOrderItem.StatusCode.FULFILLABLE.name().equals(saleOrderItem.getStatusCode())) {
                        if (StringUtils.isBlank(saleOrderItem.getCombinationIdentifier())) {
                            iterator.remove();
                        } else {
                            unfulfillableCombinations.add(saleOrderItem.getCombinationIdentifier());
                        }
                    } else if (saleOrderItem.getUpdated().before(oldestFulfillableItem)) {
                        oldestFulfillableItem = saleOrderItem.getUpdated();
                    }
                }
                if (unfulfillableCombinations.size() > 0) {
                    removeUnfulfillableCombinations(generatedPackage, unfulfillableCombinations);
                }
                int autoSplitWaitHours = ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).getAutoSplitWaitHours();
                if (generatedPackage.size() > 0
                        && !(autoSplitWaitHours > 0 && oldestFulfillableItem.after(DateUtils.addToDate(DateUtils.getCurrentTime(), Calendar.HOUR, -autoSplitWaitHours)))) {
                    ShippingPackage shippingPackage = shippingService.createShippingPackage(generatedPackage, true);
                    shippingPackageCodes.add(shippingPackage.getCode());
                }
            }
        }
        return shippingPackageCodes;
    }

    private void removeUnfulfillableCombinations(List<SaleOrderItem> generatedPackage, Set<String> unfulfillableCombinations) {
        Iterator<SaleOrderItem> iterator = generatedPackage.iterator();
        while (iterator.hasNext()) {
            SaleOrderItem saleOrderItem = iterator.next();
            if (StringUtils.isNotBlank(saleOrderItem.getCombinationIdentifier()) && unfulfillableCombinations.contains(saleOrderItem.getCombinationIdentifier())) {
                iterator.remove();
            }
        }
    }

    private List<String> createFixedPackages(List<SaleOrderItem> saleOrderItems) {
        if (isPackageFulfillable(saleOrderItems)) {
            ShippingPackage shippingPackage = shippingService.createShippingPackage(saleOrderItems, false);
            return Arrays.asList(shippingPackage.getCode());
        }
        return Collections.emptyList();
    }

    private boolean isPackageFulfillable(List<SaleOrderItem> saleOrderItems) {
        for (SaleOrderItem saleOrderItem : saleOrderItems) {
            if (SaleOrderItem.StatusCode.UNFULFILLABLE.name().equals(saleOrderItem.getStatusCode())
                    || SaleOrderItem.StatusCode.ALTERNATE_SUGGESTED.name().equals(saleOrderItem.getStatusCode())) {
                return false;
            }
        }
        return true;
    }

    private boolean canProcessSaleOrderItem(SaleOrderItem saleOrderItem) {
        boolean can = saleOrderItem.getShippingPackage() == null;
        can &= StringUtils.equalsAny(saleOrderItem.getStatusCode(), SaleOrderItem.StatusCode.FULFILLABLE.name(), SaleOrderItem.StatusCode.UNFULFILLABLE.name(),
                SaleOrderItem.StatusCode.ALTERNATE_SUGGESTED.name());
        return can;
    }

    /**
     * @param saleOrderItem1
     * @param generatedPackageList
     * @return
     */
    private List<SaleOrderItem> addToSuitablePackageList(SaleOrderItem saleOrderItem1, Set<List<SaleOrderItem>> generatedPackageList) {
        List<SaleOrderItem> suitablePackageList = null;
        ItemType itemType1 = saleOrderItem1.getItemType();
        SystemConfiguration configuration = ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class);
        for (List<SaleOrderItem> packageList : generatedPackageList) {
            SaleOrderItem saleOrderItem2 = packageList.stream().filter(soi -> soi.getStatusCode().equals(SaleOrderItem.StatusCode.FULFILLABLE.name())).findFirst().orElse(
                    packageList.get(0));
            ItemType itemType2 = saleOrderItem2.getItemType();
            boolean bothAreGiftWrap = saleOrderItem1.isGiftWrap() == saleOrderItem2.isGiftWrap();
            boolean shippingMethodIsSame = saleOrderItem1.getShippingMethod().getId().equals(saleOrderItem2.getShippingMethod().getId());
            boolean shippingAddressIsSame = saleOrderItem1.getShippingAddress().getId().equals(saleOrderItem2.getShippingAddress().getId());
            boolean itemTypeInventoryIsNull = saleOrderItem2.getItemTypeInventory() == null || saleOrderItem1.getItemTypeInventory() == null;
            boolean bothBelongToSamePickSet = false;
            boolean bothBelongToSamePickArea = false;
            boolean bothBelongToSameBrand = false;
            if (configuration.isPackItemsBrandWiseEnabled()) {
                String brand1 = saleOrderItem1.getItemType().getBrand();
                String brand2 = saleOrderItem2.getItemType().getBrand();
                if (StringUtils.isBlank(brand1) && StringUtils.isBlank(brand2)) {
                    bothBelongToSameBrand = true;
                } else if (StringUtils.isNotBlank(brand1) && StringUtils.isNotBlank(brand2)) {
                    bothBelongToSameBrand = brand1.replaceAll("\\s+", "").equalsIgnoreCase(brand2.replaceAll("\\s+", ""));
                }
            }
            if (!itemTypeInventoryIsNull) {
                PickSet pickSet2 = saleOrderItem2.getItemTypeInventory().getShelf().getSection().getPickSet();
                PickSet pickSet1 = saleOrderItem1.getItemTypeInventory().getShelf().getSection().getPickSet();
                bothBelongToSamePickSet = pickSet2.getId().equals(pickSet1.getId());
                bothBelongToSamePickArea = pickSet2.getPickArea().getId().equals(pickSet1.getPickArea().getId());
            }
            LOG.debug("Adding sale order item : " + saleOrderItem1.getCode() + " to package, saleOrderItem2 : " + saleOrderItem2.getCode() + " bothAreGiftWrap : " + bothAreGiftWrap
                    + ", shippingMethodIsSame : " + shippingMethodIsSame + ", shippingAddressIsSame : " + shippingAddressIsSame + ", inventory management switched off "
                    + configuration.isInventoryManagementSwitchedOff() + ", itemTypeInventoryIsNull : " + itemTypeInventoryIsNull + ", bothBelongToSamePickSet : "
                    + bothBelongToSamePickSet + ", cross zone shipment allowed : " + configuration.isCrossPickSetShipmentsAllowed() + ", bothBelongToSamePickArea : "
                    + bothBelongToSamePickArea + ", canShipTogether : " + canShipTogetherRules(itemType1, itemType2));

            if ((!configuration.isPackItemsBrandWiseEnabled() || (configuration.isPackItemsBrandWiseEnabled() && bothBelongToSameBrand))
                && bothAreGiftWrap && shippingMethodIsSame && shippingAddressIsSame && (configuration.isInventoryManagementSwitchedOff() || itemTypeInventoryIsNull
                || bothBelongToSamePickSet || (configuration.isCrossPickSetShipmentsAllowed() && bothBelongToSamePickArea)) && canShipTogetherRules(itemType1, itemType2)) {
                suitablePackageList = packageList;
                break;
            }
        }

        if (suitablePackageList == null) {
            suitablePackageList = new ArrayList<>();
            generatedPackageList.add(suitablePackageList);
        }
        suitablePackageList.add(saleOrderItem1);
        return suitablePackageList;
    }

    private boolean canShipTogetherRules(ItemType itemType1, ItemType itemType2) {
        Expression expression = ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).getShipTogetherRuleExpression();
        if (expression != null) {
            Map<String, Object> contextParams = new HashMap<String, Object>();
            contextParams.put("itemType1", itemType1);
            contextParams.put("itemType2", itemType2);
            return expression.evaluate(contextParams, Boolean.class);
        } else {
            return true;
        }
    }

    @Override
    public void allocateFacility(String saleOrderCode, String scriptName) {
        doAllocateFacility(saleOrderCode, scriptName);
    }

    /*
    *
    *    For Fulfillment by channel type
    *
    *
    */

    @Override
    @LogActivity
    public ShippingPackage createPackageForCompleteOrder(SaleOrder saleOrder, int packetNumber) {
        ShippingPackage shippingPackage = new ShippingPackage();
        List<SaleOrderItem> saleOrderItems = new ArrayList<SaleOrderItem>(saleOrder.getSaleOrderItems().size());
        List<String> activityLogs = new ArrayList<>();
        boolean activityEnabled = ActivityContext.current().isEnable();
        if (SaleOrder.StatusCode.CREATED.name().equals(saleOrder.getStatusCode())) {
            saleOrder.setStatusCode(SaleOrder.StatusCode.PROCESSING.name());
            if (activityEnabled) {
                activityLogs.add("Order status moved to Processing");
            }
        }
        Channel channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelById(saleOrder.getChannel().getId());
        Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(channel.getSourceCode());
        Map<String, List<String>> statusCodeToSaleOrderItems = new HashMap<>();
        for (SaleOrderItem saleOrderItem : saleOrder.getSaleOrderItems()) {
            String oldStatus = saleOrderItem.getStatusCode();
            if (StringUtils.equalsAny(saleOrderItem.getStatusCode(), SaleOrderItem.StatusCode.CREATED.name(), SaleOrderItem.StatusCode.UNFULFILLABLE.name(),
                    SaleOrderItem.StatusCode.LOCATION_NOT_SERVICEABLE.name()) && !saleOrderItem.isOnHold() && saleOrderItem.getFacility() != null
                    && source.isFulfillmentByChannel()) {
                saleOrderItem.setStatusCode(SaleOrderItem.StatusCode.FULFILLABLE.name());
            }
            if (activityEnabled && !oldStatus.equalsIgnoreCase(saleOrderItem.getStatusCode())) {
                if (!statusCodeToSaleOrderItems.containsKey(saleOrderItem.getStatusCode())) {
                    statusCodeToSaleOrderItems.put(saleOrderItem.getStatusCode(), new ArrayList<String>());
                }
                statusCodeToSaleOrderItems.get(saleOrderItem.getStatusCode()).add(saleOrderItem.getCode());
            }
            if (saleOrderItem.getPacketNumber() == packetNumber) {
                saleOrderItems.add(saleOrderItem);
            }
        }

        shippingPackage.setCode(null);
        shippingPackage.getSaleOrderItems().addAll(saleOrderItems);
        shippingPackage.setSaleOrder(saleOrderItems.get(0).getSaleOrder());
        shippingPackage.setShippingManager(saleOrderItems.get(0).getSaleOrder().isThirdPartyShipping() ? ShippingPackage.ShippingManager.CHANNEL
                : ConfigurationManager.getInstance().getConfiguration(ProductConfiguration.class).isCourierManagementSwitchedOff() ? ShippingPackage.ShippingManager.NONE
                        : ShippingPackage.ShippingManager.UNIWARE);
        shippingPackage.setShipmentLabelFormat(Source.ShipmentLabelFormat.valueOf(saleOrderItems.get(0).getSaleOrder().getChannel().getShipmentLabelFormat()));
        shippingPackage = shippingService.prepareShippingPackageForCompleteOrder(shippingPackage, false);

        if (activityEnabled) {
            for (String key : statusCodeToSaleOrderItems.keySet()) {
                activityLogs.add("OrderItem {" + ActivityEntityEnum.SALE_ORDER_ITEM + ":" + statusCodeToSaleOrderItems.get(key) + "} has been marked " + key);
            }
        }
        saleOrder.setReassessInventory(false);
        if (activityEnabled && activityLogs.size() > 0) {
            ActivityUtils.appendActivity(saleOrder.getCode(), ActivityEntityEnum.SALE_ORDER.getName(), null, activityLogs, ActivityTypeEnum.PROCESSING.name());
        }
        return shippingPackage;
    }

    @Locks({ @Lock(ns = Namespace.SALE_ORDER, key = "#{#args[0]}") })
    @Transactional
    private void doAllocateFacility(String saleOrderCode, String scriptName) {
        SaleOrder saleOrder = saleOrderDao.getSaleOrderForUpdate(saleOrderCode);
        List<String> activityLogs = new ArrayList<>();
        boolean activityEnabled = ActivityContext.current().isEnable();
        FacilityCache cache = CacheManager.getInstance().getCache(FacilityCache.class);
        // check with facilities which are not Type THIRD_PARTY_WAREHOUSE
        if (cache.getNonThirdPartyFacilities().size() == 1) {
            Facility facility = cache.getNonThirdPartyFacilities().get(0);
            for (SaleOrderItem saleOrderItem : saleOrder.getSaleOrderItems()) {
                if (saleOrderItem.getFacility() == null) {
                    saleOrderItem.setFacility(facility);
                }
            }
        } else {
            Map<Integer, SaleOrderItem> saleOrderItems = getSaleOrderItemsForAllocation(saleOrder);
            FacilityAllocationData facilityAllocationData = prepareFacilityAllocationData(saleOrderItems);
            boolean facilityAllocationAttempted = false;

            List<FacilityAllocationRule> facilityAllocationRules = cache.getFacilityAllocationRules();
            if (!facilityAllocationRules.isEmpty()) {
                facilityAllocationAttempted = true;
                applyFacilityAllocationRules(saleOrder, saleOrderItems, facilityAllocationRules, facilityAllocationData);
            }
            ScraperScript allocationScript = CacheManager.getInstance().getCache(ScriptVersionedCache.class).getScriptByName(scriptName);
            if (!saleOrderItems.isEmpty() && allocationScript != null) {
                facilityAllocationAttempted = true;
                applyFacilityAllocationScript(saleOrder, saleOrderItems, facilityAllocationData, allocationScript);
            }
            if (!saleOrderItems.isEmpty() && CacheManager.getInstance().getCache(FacilityCache.class).hasDropshipFacilities()) {
                facilityAllocationAttempted = true;
                allocateDropshipFacilities(saleOrder, saleOrderItems);
            }
            if (!facilityAllocationAttempted) {
                applyDefaultFacilityAllocation(saleOrder, saleOrderItems, facilityAllocationData);
            }
        }
        if (activityEnabled) {
            ActivityUtils.appendActivity(saleOrder.getCode(), ActivityEntityEnum.SALE_ORDER.getName(), null, activityLogs, ActivityTypeEnum.PROCESSING.name());
        }
    }

    private void applyFacilityAllocationScript(SaleOrder saleOrder, Map<Integer, SaleOrderItem> saleOrderItems, FacilityAllocationData facilityAllocationData,
            ScraperScript allocationScript) {
        LOG.info("SaleOrder:{} applying facility allocation script, pending items for allocation : {}", saleOrder.getCode(), saleOrderItems.size());
        ScriptExecutionContext seContext = ScriptExecutionContext.current();
        seContext.addVariable("saleOrder", saleOrder);
        seContext.addVariable("allocationCriteria", facilityAllocationData);
        seContext.addVariable("saleOrderItems", saleOrderItems);
        try {
            seContext.setTraceLoggingEnabled(UserContext.current().isTraceLoggingEnabled());
            allocationScript.execute();
            for (Map.Entry<Integer, SaleOrderItem> entry : saleOrderItems.entrySet()) {
                if (entry.getValue().getFacility() != null) {
                    saleOrderItems.remove(entry.getValue().getId());
                }
            }
            LOG.info("SaleOrder:{} completed facility allocation script, pending items for allocation : {}", saleOrder.getCode(), saleOrderItems.size());
        } catch (Throwable e) {
            LOG.error("Unable to execute facility allocation script", e);
        } finally {
            ScriptExecutionContext.destroy();
        }
    }

    private void allocateDropshipFacilities(SaleOrder saleOrder, Map<Integer, SaleOrderItem> saleOrderItems) {
        LOG.info("SaleOrder:{} allocating dropship facilities, pending items for allocation : {}", saleOrder.getCode(), saleOrderItems.size());
        Map<Integer, List<FacilityItemType>> itemTypeIdToFacilityItemTypes = new HashMap<Integer, List<FacilityItemType>>();
        for (Map.Entry<Integer, SaleOrderItem> entry : saleOrderItems.entrySet()) {
            SaleOrderItem saleOrderItem = entry.getValue();
            List<FacilityItemType> facilityItemTypes;
            if (itemTypeIdToFacilityItemTypes.containsKey(saleOrderItem.getItemType().getId())) {
                facilityItemTypes = itemTypeIdToFacilityItemTypes.get(saleOrderItem.getItemType().getId());
            } else {
                facilityItemTypes = catalogDao.getFacilityItemTypesByItemTypeId(saleOrderItem.getItemType().getId());
                itemTypeIdToFacilityItemTypes.put(saleOrderItem.getItemType().getId(), facilityItemTypes);
            }
            if (facilityItemTypes.size() == 1) {
                LOG.info("SaleOrder:{} only one facility item mapping found with inventory, allocating to facility : {}", saleOrder.getCode(),
                        facilityItemTypes.get(0).getFacility().getCode());
                allocateToFacility(saleOrderItems, saleOrderItem, facilityItemTypes, facilityItemTypes.get(0));
            } else if (facilityItemTypes.size() > 1) {
                Set<Integer> serviceableByFacilities = shippingDao.getServiceableByFacilities(entry.getValue().getShippingMethod().getId(),
                        entry.getValue().getShippingAddress().getPincode());
                for (FacilityItemType facilityItemType : facilityItemTypes) {
                    if (serviceableByFacilities.contains(facilityItemType.getFacility().getId())) {
                        allocateToFacility(saleOrderItems, saleOrderItem, facilityItemTypes, facilityItemType);
                        break;
                    }
                }
            }
        }
        LOG.info("SaleOrder:{} completed dropship facilities allocation, pending items for allocation : {}", saleOrder.getCode(), saleOrderItems.size());

    }

    private void allocateToFacility(Map<Integer, SaleOrderItem> saleOrderItems, SaleOrderItem saleOrderItem, List<FacilityItemType> facilityItemTypes,
            FacilityItemType facilityItemType) {
        saleOrderItem.setFacility(facilityItemType.getFacility());
        saleOrderItem.setDropshipCommission(NumberUtils.getPercentageFromBase(
                saleOrderItem.getSellingPrice().add(saleOrderItem.getDiscount()).add(saleOrderItem.getVoucherValue()).add(saleOrderItem.getStoreCredit()),
                facilityItemType.getCommission()));
        saleOrderItem.setDropshipShippingCharges(facilityItemType.getShippingCharges());
        if (facilityItemType.getInventory() != -1) {
            facilityItemType.setInventory(facilityItemType.getInventory() - 1);
            if (facilityItemType.getInventory() == 0) {
                facilityItemTypes.remove(facilityItemType);
            }
        }
        saleOrderItems.remove(saleOrderItem.getId());
    }

    private FacilityAllocationData prepareFacilityAllocationData(Map<Integer, SaleOrderItem> saleOrderItems) {
        LOG.info("Preparing facility allocation data");
        long startTime = System.currentTimeMillis();
        FacilityAllocationData allocationData = new FacilityAllocationData();
        Map<String, Set<Integer>> pincodeToServiceableFacilities = new HashMap<String, Set<Integer>>();
        Map<Integer, Map<Integer, ItemTypeInventorySnapshotVO>> itemTypeToInventorySnapshots = new HashMap<Integer, Map<Integer, ItemTypeInventorySnapshotVO>>();
        Map<Integer, Map<Integer, VendorItemTypeVO>> itemTypeToVendorItemTypes = new HashMap<Integer, Map<Integer, VendorItemTypeVO>>();
        for (Map.Entry<Integer, SaleOrderItem> entry : saleOrderItems.entrySet()) {
            Set<Integer> serviceableFacilities;
            if (pincodeToServiceableFacilities.containsKey(entry.getValue().getShippingMethod().getId() + entry.getValue().getShippingAddress().getPincode())) {
                serviceableFacilities = pincodeToServiceableFacilities.get(entry.getValue().getShippingMethod().getId() + entry.getValue().getShippingAddress().getPincode());
            } else {
                serviceableFacilities = shippingDao.getServiceableByFacilities(entry.getValue().getShippingMethod().getId(), entry.getValue().getShippingAddress().getPincode());
                pincodeToServiceableFacilities.put(entry.getValue().getShippingMethod().getId() + entry.getValue().getShippingAddress().getPincode(), serviceableFacilities);
            }
            Map<Integer, ItemTypeInventorySnapshotVO> facilityToInventorySnapshots;
            if (itemTypeToInventorySnapshots.containsKey(entry.getValue().getItemType().getId())) {
                facilityToInventorySnapshots = itemTypeToInventorySnapshots.get(entry.getValue().getItemType().getId());
            } else {
                List<ItemTypeInventorySnapshot> inventorySnapshots = inventoryService.getItemTypeInventorySnapshotByTenant(entry.getValue().getItemType().getId());
                facilityToInventorySnapshots = getInventorySnapshotsByFacility(inventorySnapshots);
                itemTypeToInventorySnapshots.put(entry.getValue().getItemType().getId(), facilityToInventorySnapshots);
            }
            Map<Integer, VendorItemTypeVO> facilityToVendorItemTypes;
            if (itemTypeToVendorItemTypes.containsKey(entry.getValue().getItemType().getId())) {
                facilityToVendorItemTypes = itemTypeToVendorItemTypes.get(entry.getValue().getItemType().getId());
            } else {
                List<VendorItemType> vendorItemTypes = catalogService.getVendorItemTypeByItemTypeIdByTenant(entry.getValue().getItemType().getId());
                facilityToVendorItemTypes = getVendorItemTypesByFacility(vendorItemTypes);
                itemTypeToVendorItemTypes.put(entry.getValue().getItemType().getId(), facilityToVendorItemTypes);
            }
        }
        allocationData.setPincodeToServiceableFacities(pincodeToServiceableFacilities);
        allocationData.setItemTypeToInventorySnapshots(itemTypeToInventorySnapshots);
        allocationData.setItemTypeToVendorItemTypes(itemTypeToVendorItemTypes);
        LOG.info("Facility allocation data prepared in {} ms", System.currentTimeMillis() - startTime);
        return allocationData;
    }

    private Map<Integer, VendorItemTypeVO> getVendorItemTypesByFacility(List<VendorItemType> vendorItemTypes) {
        Map<Integer, VendorItemTypeVO> facilityToVendorItemTypes = new HashMap<Integer, VendorItemTypeVO>();
        for (VendorItemType vit : vendorItemTypes) {
            if (!facilityToVendorItemTypes.containsKey(vit.getVendor().getFacility().getId())) {
                facilityToVendorItemTypes.put(vit.getVendor().getFacility().getId(), new VendorItemTypeVO(vit));
            }
        }
        return facilityToVendorItemTypes;
    }

    private Map<Integer, ItemTypeInventorySnapshotVO> getInventorySnapshotsByFacility(List<ItemTypeInventorySnapshot> inventorySnapshots) {
        Map<Integer, ItemTypeInventorySnapshotVO> facilityToInventorySnapshots = new LinkedHashMap<Integer, ItemTypeInventorySnapshotVO>();
        for (ItemTypeInventorySnapshot snapshot : inventorySnapshots) {
            if (snapshot.getInventory() + snapshot.getPutawayPending() - snapshot.getOpenSale() > 0) {
                facilityToInventorySnapshots.put(snapshot.getFacility().getId(), new ItemTypeInventorySnapshotVO(snapshot));
            }
        }
        return facilityToInventorySnapshots;
    }

    private Map<Integer, SaleOrderItem> getSaleOrderItemsForAllocation(SaleOrder saleOrder) {
        Map<Integer, SaleOrderItem> saleOrderItems = new ConcurrentHashMap<Integer, SaleOrderItem>();
        for (SaleOrderItem saleOrderItem : saleOrder.getSaleOrderItems()) {
            if (saleOrderItem.getFacility() == null) {
                saleOrderItems.put(saleOrderItem.getId(), saleOrderItem);
            }
        }
        return saleOrderItems;
    }

    private void applyFacilityAllocationRules(SaleOrder saleOrder, Map<Integer, SaleOrderItem> saleOrderItems, List<FacilityAllocationRule> facilityAllocationRules,
            FacilityAllocationData facilityAllocationData) {
        LOG.info("SaleOrder:{} applying facility allocation rules, pending items for allocation : {}", saleOrder.getCode(), saleOrderItems.size());
        Map<String, Object> contextParams = new HashMap<String, Object>();
        contextParams.put("saleOrder", saleOrder);
        contextParams.put("allocationCriteria", facilityAllocationData);
        Map<String, Facility> combinationsFacility = new HashMap<String, Facility>();
        for (Map.Entry<Integer, SaleOrderItem> entry : saleOrderItems.entrySet()) {
            SaleOrderItem saleOrderItem = entry.getValue();
            if (saleOrderItem.getFacility() == null
                    && (StringUtils.isBlank(saleOrderItem.getCombinationIdentifier()) || !combinationsFacility.containsKey(saleOrderItem.getCombinationIdentifier()))) {
                contextParams.put("saleOrderItem", saleOrderItem);
                for (FacilityAllocationRule facilityAllocationRule : facilityAllocationRules) {
                    facilityAllocationData.setCurrentFacility(facilityAllocationRule.getFacility());
                    facilityAllocationData.setCurrentItemTypeId(saleOrderItem.getItemType().getId());
                    facilityAllocationData.setCurrentPincode(saleOrderItem.getShippingMethod().getId() + saleOrderItem.getShippingAddress().getPincode());
                    if (facilityAllocationRule.getConditionExpression().evaluate(contextParams, Boolean.class)) {
                        LOG.info("SaleOrder: {}, SaleOrderItem:{}, allocated to facility: {}, by rule: {}",
                                new Object[] { saleOrder.getCode(), saleOrderItem.getCode(), facilityAllocationRule.getFacility().getCode(), facilityAllocationRule.getName() });
                        saleOrderItem.setFacility(facilityAllocationRule.getFacility());
                        saleOrderItems.remove(saleOrderItem.getId());
                        ItemTypeInventorySnapshotVO inventorySnapshotVO = facilityAllocationData.getItemTypeToInventorySnapshots().get(saleOrderItem.getItemType().getId()).get(
                                facilityAllocationRule.getFacility().getId());
                        if (inventorySnapshotVO != null) {
                            inventorySnapshotVO.setFulfillableInventory(inventorySnapshotVO.getFulfillableInventory() - 1);
                        }
                        break;
                    }
                }
                if (saleOrderItem.getFacility() != null && StringUtils.isNotBlank(saleOrderItem.getCombinationIdentifier())) {
                    combinationsFacility.put(saleOrderItem.getCombinationIdentifier(), saleOrderItem.getFacility());
                }
            }
        }
        allocateItemsWithCombinationFacilities(saleOrder, saleOrderItems, facilityAllocationData, combinationsFacility);
    }

    private void allocateItemsWithCombinationFacilities(SaleOrder saleOrder, Map<Integer, SaleOrderItem> saleOrderItems, FacilityAllocationData facilityAllocationData,
            Map<String, Facility> combinationsFacility) {
        if (combinationsFacility.size() > 0) {
            for (Map.Entry<Integer, SaleOrderItem> entry : saleOrderItems.entrySet()) {
                SaleOrderItem saleOrderItem = entry.getValue();
                if (saleOrderItem.getFacility() == null && StringUtils.isNotBlank(saleOrderItem.getCombinationIdentifier())
                        && combinationsFacility.containsKey(saleOrderItem.getCombinationIdentifier())) {
                    Facility facility = combinationsFacility.get(saleOrderItem.getCombinationIdentifier());
                    LOG.info("SaleOrder: {}, allocating same facility to all items in combination:{}, facility: {}",
                            new Object[] { saleOrder.getCode(), saleOrderItem.getCombinationIdentifier(), facility.getCode() });
                    saleOrderItem.setFacility(facility);
                    saleOrderItems.remove(saleOrderItem.getId());
                    ItemTypeInventorySnapshotVO inventorySnapshotVO = facilityAllocationData.getItemTypeToInventorySnapshots().get(saleOrderItem.getItemType().getId()).get(
                            facility.getId());
                    if (inventorySnapshotVO != null) {
                        inventorySnapshotVO.setFulfillableInventory(inventorySnapshotVO.getFulfillableInventory() - 1);
                    }
                }
            }
        }
    }

    private void applyDefaultFacilityAllocation(SaleOrder saleOrder, Map<Integer, SaleOrderItem> saleOrderItems, FacilityAllocationData facilityAllocationData) {
        Map<String, Facility> combinationsFacility = new HashMap<String, Facility>();
        for (Map.Entry<Integer, SaleOrderItem> entry : saleOrderItems.entrySet()) {
            SaleOrderItem saleOrderItem = entry.getValue();
            if (saleOrderItem.getFacility() == null
                    && (StringUtils.isBlank(saleOrderItem.getCombinationIdentifier()) || !combinationsFacility.containsKey(saleOrderItem.getCombinationIdentifier()))) {
                Set<Integer> serviceableFacilityIds = facilityAllocationData.getPincodeToServiceableFacities().get(
                        saleOrderItem.getShippingMethod().getId() + saleOrderItem.getShippingAddress().getPincode());
                FacilityAllocationVO allocatedFacilities = saleOrderMao.getFacilityAllocation(saleOrderItem.getCode(), saleOrder.getCode());
                for (Integer facilityId : serviceableFacilityIds) {
                    Facility facility = CacheManager.getInstance().getCache(FacilityCache.class).getFacilityById(facilityId);
                    boolean isAssignedBefore = allocatedFacilities != null && allocatedFacilities.getFacilityCodes().contains(facility.getCode());
                    if (!isAssignedBefore) {
                        ItemTypeInventorySnapshotVO inventorySnapshotVO = facilityAllocationData.getItemTypeToInventorySnapshots().get(saleOrderItem.getItemType().getId()).get(
                                facilityId);
                        if (inventorySnapshotVO != null && inventorySnapshotVO.getFulfillableInventory() > 0) {
                            saleOrderItem.setFacility(facility);
                            inventorySnapshotVO.setFulfillableInventory(inventorySnapshotVO.getFulfillableInventory() - 1);
                            break;
                        }
                    }
                }
                if (saleOrderItem.getFacility() != null && StringUtils.isNotBlank(saleOrderItem.getCombinationIdentifier())) {
                    combinationsFacility.put(saleOrderItem.getCombinationIdentifier(), saleOrderItem.getFacility());
                }
            }
        }
        allocateItemsWithCombinationFacilities(saleOrder, saleOrderItems, facilityAllocationData, combinationsFacility);
    }

    public static class ItemTypeInventorySnapshotVO {
        private int fulfillableInventory;

        public ItemTypeInventorySnapshotVO(ItemTypeInventorySnapshot snapshot) {
            this.fulfillableInventory = snapshot.getInventory() + snapshot.getPutawayPending() + snapshot.getPendingStockTransfer() - snapshot.getOpenSale()
                    - snapshot.getPendingInventoryAssessment();
        }

        /**
         * @return the fulfillableInventory
         */
        public int getFulfillableInventory() {
            return fulfillableInventory;
        }

        /**
         * @param fulfillableInventory the fulfillableInventory to set
         */
        public void setFulfillableInventory(int fulfillableInventory) {
            this.fulfillableInventory = fulfillableInventory;
        }

    }

    public static class VendorItemTypeVO {
        private BigDecimal unitPrice;
        private Integer    leadTime;

        public VendorItemTypeVO(VendorItemType vit) {
            this.unitPrice = vit.getUnitPrice();
            this.leadTime = vit.getLeadTime() != null ? vit.getLeadTime() : vit.getVendor().getLeadTime();
        }

        /**
         * @return the unitPrice
         */
        public BigDecimal getUnitPrice() {
            return unitPrice;
        }

        /**
         * @param unitPrice the unitPrice to set
         */
        public void setUnitPrice(BigDecimal unitPrice) {
            this.unitPrice = unitPrice;
        }

        /**
         * @return the leadTime
         */
        public Integer getLeadTime() {
            return leadTime;
        }

        /**
         * @param leadTime the leadTime to set
         */
        public void setLeadTime(Integer leadTime) {
            this.leadTime = leadTime;
        }

    }

    public static class FacilityAllocationData {
        private Map<String, Set<Integer>>                               pincodeToServiceableFacities;
        private Map<Integer, Map<Integer, ItemTypeInventorySnapshotVO>> itemTypeToInventorySnapshots;
        private Map<Integer, Map<Integer, VendorItemTypeVO>>            itemTypeToVendorItemTypes;
        private Facility                                                currentFacility;
        private Integer                                                 currentItemTypeId;
        private String                                                  currentPincode;

        /**
         * @return the pincodeToServiceableFacities
         */
        public Map<String, Set<Integer>> getPincodeToServiceableFacities() {
            return pincodeToServiceableFacities;
        }

        /**
         * @param pincodeToServiceableFacities the pincodeToServiceableFacities to set
         */
        public void setPincodeToServiceableFacities(Map<String, Set<Integer>> pincodeToServiceableFacities) {
            this.pincodeToServiceableFacities = pincodeToServiceableFacities;
        }

        /**
         * @return the itemTypeToInventorySnapshots
         */
        public Map<Integer, Map<Integer, ItemTypeInventorySnapshotVO>> getItemTypeToInventorySnapshots() {
            return itemTypeToInventorySnapshots;
        }

        /**
         * @param itemTypeToInventorySnapshots the itemTypeToInventorySnapshots to set
         */
        public void setItemTypeToInventorySnapshots(Map<Integer, Map<Integer, ItemTypeInventorySnapshotVO>> itemTypeToInventorySnapshots) {
            this.itemTypeToInventorySnapshots = itemTypeToInventorySnapshots;
        }

        /**
         * @return the itemTypeToVendorItemTypes
         */
        public Map<Integer, Map<Integer, VendorItemTypeVO>> getItemTypeToVendorItemTypes() {
            return itemTypeToVendorItemTypes;
        }

        /**
         * @param itemTypeToVendorItemTypes the itemTypeToVendorItemTypes to set
         */
        public void setItemTypeToVendorItemTypes(Map<Integer, Map<Integer, VendorItemTypeVO>> itemTypeToVendorItemTypes) {
            this.itemTypeToVendorItemTypes = itemTypeToVendorItemTypes;
        }

        public boolean isServiceable() {
            return pincodeToServiceableFacities.get(currentPincode).contains(currentFacility.getId());
        }

        public boolean hasVendor() {
            return itemTypeToVendorItemTypes.get(currentItemTypeId).containsKey(currentFacility.getId());
        }

        public boolean hasInventory() {
            return itemTypeToInventorySnapshots.get(currentItemTypeId).containsKey(currentFacility.getId());
        }

        /**
         * @return the currentFacility
         */
        public Facility getCurrentFacility() {
            return currentFacility;
        }

        /**
         * @param currentFacility the currentFacility to set
         */
        public void setCurrentFacility(Facility currentFacility) {
            this.currentFacility = currentFacility;
        }

        /**
         * @return the currentItemTypeId
         */
        public Integer getCurrentItemTypeId() {
            return currentItemTypeId;
        }

        /**
         * @param currentItemTypeId the currentItemTypeId to set
         */
        public void setCurrentItemTypeId(Integer currentItemTypeId) {
            this.currentItemTypeId = currentItemTypeId;
        }

        /**
         * @return the currentPincode
         */
        public String getCurrentPincode() {
            return currentPincode;
        }

        /**
         * @param currentPincode the currentPincode to set
         */
        public void setCurrentPincode(String currentPincode) {
            this.currentPincode = currentPincode;
        }
    }
}
