package com.uniware.services.saleorder;

import com.uniware.core.api.saleorder.DispatchSaleOrderRequest;
import com.uniware.core.api.saleorder.DispatchSaleOrderResponse;

/**
 * Created by akshayag on 9/30/15.
 */


public interface IDispatchSaleOrderService {

    public DispatchSaleOrderResponse dispatchSaleOrder(DispatchSaleOrderRequest request);
}
