package com.uniware.services.saleorder.impl;

import com.unifier.core.api.validation.ValidationContext;
import com.uniware.core.api.saleorder.DispatchSaleOrderRequest;
import com.uniware.core.api.saleorder.DispatchSaleOrderResponse;
import com.uniware.core.api.shipping.AddShippingPackagesToManifestRequest;
import com.uniware.core.api.shipping.AddShippingPackagesToManifestResponse;
import com.uniware.core.api.shipping.CloseShippingManifestRequest;
import com.uniware.core.api.shipping.CloseShippingManifestResponse;
import com.uniware.core.api.shipping.CreateShippingManifestRequest;
import com.uniware.core.api.shipping.CreateShippingManifestResponse;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.entity.ShippingPackage;
import com.uniware.dao.shipping.IShippingDao;
import com.uniware.services.saleorder.IDispatchSaleOrderService;
import com.uniware.services.shipping.IDispatchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by akshayag on 9/30/15.
 */

@Service("dispatchSaleOrderService")
public class DispatchSaleOrderServiceImpl implements IDispatchSaleOrderService {

    @Autowired
    private IShippingDao     shippingDao;

    @Autowired
    private IDispatchService dispatchService;


    @Override
    @Transactional
    public DispatchSaleOrderResponse dispatchSaleOrder(DispatchSaleOrderRequest request) {
        DispatchSaleOrderResponse response = new DispatchSaleOrderResponse();
        ValidationContext context = request.validate();
        if(!context.hasErrors()) {
            List<String> readyToShipShippingPackages = new ArrayList<>();
            Set<ShippingPackage> failedShippingPackages = new HashSet<>();
            CreateShippingManifestRequest createManifestRequest = new CreateShippingManifestRequest();
            CreateShippingManifestResponse createManifestResponse = new CreateShippingManifestResponse();
            CloseShippingManifestRequest closeManifestRequest = new CloseShippingManifestRequest();
            CloseShippingManifestResponse closeManifestResponse = new CloseShippingManifestResponse();
            AddShippingPackagesToManifestRequest addShippingPackagesToManifestRequest = new AddShippingPackagesToManifestRequest();
            AddShippingPackagesToManifestResponse addShippingPackagesToManifestResponse = new AddShippingPackagesToManifestResponse();
            for (String saleOrderCode : request.getSaleOrderCodes()) {
                List<ShippingPackage> shippingPackages = new ArrayList<>();
                shippingPackages = shippingDao.getAllShippingPackagesBySaleOrder(saleOrderCode);
                for (ShippingPackage shippingPackage : shippingPackages) {
                    if (ShippingPackage.StatusCode.READY_TO_SHIP.name().equals(shippingPackage.getStatusCode())) {
                        readyToShipShippingPackages.add(shippingPackage.getCode());
                    } else {
                        context.addError(WsResponseCode.INVALID_SHIPPING_PACKAGE_STATE,"Invalid Shipping Package State");
                    }
                }
                createManifestRequest.setChannel(request.getChannelCode());
                createManifestRequest.setShippingProviderCode(request.getShippingProviderCode());
                createManifestRequest.setShippingProviderName(request.getShippingProviderName());
                createManifestResponse = dispatchService.createShippingManifest(createManifestRequest);
            }
            addShippingPackagesToManifestRequest.setShippingManifestCode(createManifestResponse.getShippingManifestCode());
            addShippingPackagesToManifestRequest.setShippingPackageCodes(readyToShipShippingPackages);
            addShippingPackagesToManifestResponse = dispatchService.addShippingPackagesToManifest(addShippingPackagesToManifestRequest);
            closeManifestRequest.setShippingManifestCode(createManifestResponse.getShippingManifestCode());
            closeManifestResponse = dispatchService.closeShippingManifest(closeManifestRequest);
            response.setShippingManifestCode(createManifestResponse.getShippingManifestCode());
//            response.setShippingManifestStatus(closeManifestResponse.getShippingManifestStatus());
        }
        response.addErrors(context.getErrors());
        return response;
    }
}