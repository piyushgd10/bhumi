/*
 * Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 3/22/15 4:56 PM
 * @author amdalal
 */

package com.uniware.services.saleorder.impl;

import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.support.TransactionSynchronizationAdapter;

import com.unifier.dao.export.IExportMao;
import com.uniware.core.utils.ViewContext;

@Component(value = "viewDirtyPostCommitHandler")
public class ViewDirtyPostCommitHandler extends TransactionSynchronizationAdapter {

    private static final Logger LOG = LoggerFactory.getLogger(ViewDirtyPostCommitHandler.class);

    @Autowired
    private IExportMao          exportMao;

    @Override
    public void afterCommit() {
        for (Map.Entry<String, Set<String>> e : ViewContext.current().getDirtySaleOrderCodes().entrySet()) {
            try {
                LOG.info("Marking sale order: {} dirty in {}", e.getValue(), e.getKey());
                exportMao.markViewDirty(e.getValue(), e.getKey());
                ViewContext.current().removeSaleOrderCodes(e.getValue());
            } catch (Exception ex) {
                LOG.error("Failed to mark saleOrders: " + e.getValue() + "dirty", ex);
                // if this mongodb update fails, we choose to ignore it.
            }
        }
    }
}