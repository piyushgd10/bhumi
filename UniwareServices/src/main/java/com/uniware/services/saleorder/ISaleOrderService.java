/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 15, 2011
 *  @author singla
 */
package com.uniware.services.saleorder;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.transaction.annotation.Transactional;

import com.uniware.core.api.catalog.LookupSaleOrderLineItemsRequest;
import com.uniware.core.api.catalog.LookupSaleOrderLineItemsResponse;
import com.uniware.core.api.saleorder.AcceptSaleOrderAlternateRequest;
import com.uniware.core.api.saleorder.AcceptSaleOrderAlternateResponse;
import com.uniware.core.api.saleorder.AcceptSaleOrderItemAlternateRequest;
import com.uniware.core.api.saleorder.AcceptSaleOrderItemAlternateResponse;
import com.uniware.core.api.saleorder.AddOrEditSaleOrderItemsRequest;
import com.uniware.core.api.saleorder.AddOrEditSaleOrderItemsResponse;
import com.uniware.core.api.saleorder.CancelSaleOrderRequest;
import com.uniware.core.api.saleorder.CancelSaleOrderResponse;
import com.uniware.core.api.saleorder.CreateSaleOrderAlternateRequest;
import com.uniware.core.api.saleorder.CreateSaleOrderAlternateResponse;
import com.uniware.core.api.saleorder.CreateSaleOrderItemAlternateRequest;
import com.uniware.core.api.saleorder.CreateSaleOrderItemAlternateResponse;
import com.uniware.core.api.saleorder.CreateSaleOrderRequest;
import com.uniware.core.api.saleorder.CreateSaleOrderResponse;
import com.uniware.core.api.saleorder.DeleteFailedSaleOrdersRequest;
import com.uniware.core.api.saleorder.DeleteFailedSaleOrdersResponse;
import com.uniware.core.api.saleorder.DeleteSaleOrderRequest;
import com.uniware.core.api.saleorder.DeleteSaleOrderResponse;
import com.uniware.core.api.saleorder.EditSaleOrderAddressRequest;
import com.uniware.core.api.saleorder.EditSaleOrderAddressResponse;
import com.uniware.core.api.saleorder.EditSaleOrderAddressesRequest;
import com.uniware.core.api.saleorder.EditSaleOrderAddressesResponse;
import com.uniware.core.api.saleorder.EditSaleOrderItemMetadataRequest;
import com.uniware.core.api.saleorder.EditSaleOrderItemMetadataResponse;
import com.uniware.core.api.saleorder.EditSaleOrderItemRequest;
import com.uniware.core.api.saleorder.EditSaleOrderItemResponse;
import com.uniware.core.api.saleorder.EditSaleOrderMetadataRequest;
import com.uniware.core.api.saleorder.EditSaleOrderMetadataResponse;
import com.uniware.core.api.saleorder.FixAddressErrorInFailedOrderRequest;
import com.uniware.core.api.saleorder.FixAddressErrorInFailedOrderResponse;
import com.uniware.core.api.saleorder.GenerateSaleOrderNextSequenceRequest;
import com.uniware.core.api.saleorder.GenerateSaleOrderNextSequenceResponse;
import com.uniware.core.api.saleorder.GetCancellationReasonsRequest;
import com.uniware.core.api.saleorder.GetCancellationReasonsResponse;
import com.uniware.core.api.saleorder.GetErrorsForFailedOrdersRequest;
import com.uniware.core.api.saleorder.GetErrorsForFailedOrdersResponse;
import com.uniware.core.api.saleorder.GetFailedSaleOrdersRequest;
import com.uniware.core.api.saleorder.GetFailedSaleOrdersResponse;
import com.uniware.core.api.saleorder.GetOrderSyncRequest;
import com.uniware.core.api.saleorder.GetOrderSyncResponse;
import com.uniware.core.api.saleorder.GetReturnReasonsRequest;
import com.uniware.core.api.saleorder.GetReturnReasonsResponse;
import com.uniware.core.api.saleorder.GetSaleOrderAlternateRequest;
import com.uniware.core.api.saleorder.GetSaleOrderAlternateResponse;
import com.uniware.core.api.saleorder.GetSaleOrderItemAlternateRequest;
import com.uniware.core.api.saleorder.GetSaleOrderItemAlternateResponse;
import com.uniware.core.api.saleorder.GetSaleOrderRequest;
import com.uniware.core.api.saleorder.GetSaleOrderResponse;
import com.uniware.core.api.saleorder.GetShippingPackagesByStatusCodeRequest;
import com.uniware.core.api.saleorder.GetShippingPackagesByStatusCodeResponse;
import com.uniware.core.api.saleorder.GetUnverifiedSaleOrdersRequest;
import com.uniware.core.api.saleorder.GetUnverifiedSaleOrdersResponse;
import com.uniware.core.api.saleorder.HoldSaleOrderItemsRequest;
import com.uniware.core.api.saleorder.HoldSaleOrderItemsResponse;
import com.uniware.core.api.saleorder.HoldSaleOrderRequest;
import com.uniware.core.api.saleorder.HoldSaleOrderResponse;
import com.uniware.core.api.saleorder.MarkSaleOrderItemsCompleteRequest;
import com.uniware.core.api.saleorder.MarkSaleOrderItemsCompleteResponse;
import com.uniware.core.api.saleorder.MarkSaleOrderItemsUnfulfillableRequest;
import com.uniware.core.api.saleorder.MarkSaleOrderItemsUnfulfillableResponse;
import com.uniware.core.api.saleorder.ModifyPacketSaleOrderRequest;
import com.uniware.core.api.saleorder.ModifyPacketSaleOrderResponse;
import com.uniware.core.api.saleorder.RemoveMappingErrorRetrySaleOrderRequest;
import com.uniware.core.api.saleorder.RemoveMappingErrorRetrySaleOrderResponse;
import com.uniware.core.api.saleorder.SearchSaleOrderItemRequest;
import com.uniware.core.api.saleorder.SearchSaleOrderItemResponse;
import com.uniware.core.api.saleorder.SearchSaleOrderRequest;
import com.uniware.core.api.saleorder.SearchSaleOrderResponse;
import com.uniware.core.api.saleorder.SetSaleOrderPriorityRequest;
import com.uniware.core.api.saleorder.SetSaleOrderPriorityResponse;
import com.uniware.core.api.saleorder.SwitchSaleOrderItemFacilityRequest;
import com.uniware.core.api.saleorder.SwitchSaleOrderItemFacilityResponse;
import com.uniware.core.api.saleorder.UnblockSaleOrderItemsInventoryRequest;
import com.uniware.core.api.saleorder.UnblockSaleOrderItemsInventoryResponse;
import com.uniware.core.api.saleorder.UnholdSaleOrderItemsRequest;
import com.uniware.core.api.saleorder.UnholdSaleOrderItemsResponse;
import com.uniware.core.api.saleorder.UnholdSaleOrderRequest;
import com.uniware.core.api.saleorder.UnholdSaleOrderResponse;
import com.uniware.core.api.saleorder.VerifySaleOrderRequest;
import com.uniware.core.api.saleorder.VerifySaleOrderResponse;
import com.uniware.core.api.saleorder.VerifySaleOrdersRequest;
import com.uniware.core.api.saleorder.VerifySaleOrdersResponse;
import com.uniware.core.entity.AddressDetail;
import com.uniware.core.entity.ChannelItemType;
import com.uniware.core.entity.Item;
import com.uniware.core.entity.SaleOrder;
import com.uniware.core.entity.SaleOrderItem;
import com.uniware.core.entity.SaleOrderItemStatus;
import com.uniware.core.entity.SaleOrderStatus;
import com.uniware.core.vo.SaleOrderFlowSummaryVO;
import com.uniware.core.vo.SaleOrderVO;
import com.uniware.services.tasks.saleorder.SaleOrderProcessor.SaleOrderDTO;

/**
 * @author singla
 */
public interface ISaleOrderService {

    CreateSaleOrderResponse createSaleOrder(CreateSaleOrderRequest request);

    boolean isChannelItemTypeValidForSaleOrderCreation(ChannelItemType channelItemType, boolean useVerifiedListings);

    List<SaleOrder> getSaleOrdersByStatus(String statusCode);

    /**
     * @param saleOrderItemId
     * @return
     */
    SaleOrderItem getSaleOrderItemById(int saleOrderItemId, boolean refresh);

    /**
     * @param saleOrderItemId
     * @return
     */
    SaleOrderItem getSaleOrderItemById(int saleOrderItemId);

    SaleOrderItem updateSaleOrderItem(SaleOrderItem saleOrderItem);

    /**
     * Get and lock sale order for update. By saleOrderId.
     * 
     * @param saleOrderId
     */
    SaleOrder getSaleOrderForUpdate(Integer saleOrderId);

    /**
     * @param itemId
     * @return
     */
    SaleOrderItem getSaleOrderItemByAllocatedItem(int itemId);

    SaleOrderItem getSaleOrderItemForAllocation(String shippingPackageCode, Item item);

    SearchSaleOrderResponse searchSaleOrders(SearchSaleOrderRequest req);

    GetSaleOrderResponse getSaleOrder(GetSaleOrderRequest getOrderRequest);

    /**
     * @param saleOrderId
     * @return
     */
    SaleOrder getSaleOrderById(int saleOrderId);

    List<SaleOrderItem> getSaleOrderItemsByStatus(String statusCode);

    /**
     * @param request
     * @return
     */
    CancelSaleOrderResponse cancelSaleOrder(CancelSaleOrderRequest request);

    Long getItemCountNotDispatched();

    Long getItemCountDispatchedToday();

    SaleOrderFlowSummaryVO createSaleOrderFlowSummary(SaleOrderFlowSummaryVO summary);

    /**
     * @param request
     * @return
     */
    HoldSaleOrderResponse holdSaleOrder(HoldSaleOrderRequest request);

    UnholdSaleOrderResponse unholdSaleOrder(UnholdSaleOrderRequest request);

    List<SaleOrderItem> getSaleOrderItemByItemTypeByStatus(int itemTypeId, String statusCode);

    /**
     * @param saleOrder
     * @return
     */
    SaleOrder addSaleOrder(SaleOrder saleOrder);

    /**
     * @param addressDetail
     * @return
     */
    AddressDetail addAddressDetail(AddressDetail addressDetail);

    /**
     * @return
     */
    List<SaleOrderDTO> getSaleOrdersForProcessing();

    SearchSaleOrderItemResponse searchSaleOrderItems(SearchSaleOrderItemRequest request);

    ModifyPacketSaleOrderResponse modifySaleOrderItems(ModifyPacketSaleOrderRequest request);

    /**
     * @param picklistCode
     * @param item
     * @return
     */
    SaleOrderItem getSaleOrderItemForAllocationInPicklist(String picklistCode, Item item);

    List<SaleOrderItem> getSaleOrderItemsByItemId(Integer itemId);

    EditSaleOrderAddressesResponse editSaleOrderAddresses(EditSaleOrderAddressesRequest request);

    EditSaleOrderMetadataResponse editSaleOrderMetadata(EditSaleOrderMetadataRequest request);

    HoldSaleOrderItemsResponse holdSaleOrderItems(HoldSaleOrderItemsRequest request);

    UnholdSaleOrderItemsResponse unholdSaleOrderItems(UnholdSaleOrderItemsRequest request);

    CreateSaleOrderItemAlternateResponse createSaleOrderItemAlternate(CreateSaleOrderItemAlternateRequest request);

    CreateSaleOrderAlternateResponse createSaleOrderAlternate(CreateSaleOrderAlternateRequest request);

    AcceptSaleOrderItemAlternateResponse acceptSaleOrderItemAlternate(AcceptSaleOrderItemAlternateRequest request);

    AcceptSaleOrderAlternateResponse acceptSaleOrderAlternate(AcceptSaleOrderAlternateRequest request);

    EditSaleOrderItemResponse editSaleOrderItem(EditSaleOrderItemRequest request);

    GetSaleOrderItemAlternateResponse getSaleOrderItemAlternateRequest(GetSaleOrderItemAlternateRequest request);

    GetSaleOrderAlternateResponse getSaleOrderAlternate(GetSaleOrderAlternateRequest request);

    SetSaleOrderPriorityResponse setSaleOrderPriority(SetSaleOrderPriorityRequest request);

    List<Integer> getSaleOrdersForFacilityAllocation();

    List<String> getSaleOrderCodesForFacilityAllocation();

    SwitchSaleOrderItemFacilityResponse switchSaleOrderItemFacility(SwitchSaleOrderItemFacilityRequest request);

    UnblockSaleOrderItemsInventoryResponse unblockSaleOrderItemsInventory(UnblockSaleOrderItemsInventoryRequest request);

    EditSaleOrderAddressResponse editSaleOrderAddress(EditSaleOrderAddressRequest request);

    SaleOrder updateSaleOrder(SaleOrder saleOrder);

    CreateSaleOrderResponse createSaleOrderInternal(CreateSaleOrderRequest request);

    List<SaleOrderStatus> getOrderStatuses();

    List<SaleOrderItemStatus> getOrderItemStatuses();

    List<SaleOrderItem> getPendingSaleOrderItemsByFacilityId(Integer facilityId);

    VerifySaleOrdersResponse verifySaleOrders(VerifySaleOrdersRequest request);

    VerifySaleOrderResponse verifySaleOrder(VerifySaleOrderRequest request);

    SaleOrderItem getSaleOrderItemByCode(String saleOrderCode, String saleOrderItemCode);

    void createOrUpdateFailedSaleOrder(SaleOrderVO saleOrderVO);

    SaleOrderVO getFailedSaleOrderByCode(String saleOrderCode);

    List<SaleOrderVO> getAllSaleOrderVOs();

    @Transactional List<SaleOrderItem> getSaleOrderItemByCodeList(String saleOrderCode, List<String> saleOrderItemCodeList);

    List<SaleOrder> searchSaleOrder(String keyword);

    List<SaleOrderVO> searchFailedOrPartialOrder(String keyword);

    SaleOrder getSaleOrderByAdditionalInfo(String additionalInfo);

    AddOrEditSaleOrderItemsResponse divideSOItemsLogisticCharges(AddOrEditSaleOrderItemsRequest request);

    EditSaleOrderItemMetadataResponse editSaleOrderItemMetadata(EditSaleOrderItemMetadataRequest request);

    DeleteFailedSaleOrdersResponse removeAllFailedSaleOrders(DeleteFailedSaleOrdersRequest request);

    List<SaleOrderItem> getSaleOrderItemsByItemIdAcrossFacility(Integer itemId);

    /**
     * @param code
     * @return
     */
    SaleOrder getSaleOrderByCode(String code);

    /**
     * Get and lock sale order for update.
     *
     * @param code
     * @return
     */
    SaleOrder getSaleOrderForUpdate(String code);

    SaleOrder getSaleOrderForUpdate(String code, boolean markViewDirty);

    List<SaleOrder> getSaleOrderByDisplayOrderCode(String code, Integer channelId);

    List<SaleOrderItem> getSaleOrderItemsByChannelOrderAndItemCode(String channelSaleOrderCode, String channelSaleOrderItemCode, Integer channelId);

    SaleOrderItem getSaleOrderItemByItemCode(String saleOrderCode, String itemCode);

    List<SaleOrder> getSaleOrdersForStatusSync(Integer channelId, boolean pendingOnly, int numOfDays, int maxId, int batchSize);

    Long getSaleOrderCountForStatusSync(Integer channelId, boolean pendingOnly, int numOfDays);

    /**
     * Delete {@code SaleOrder} for the tenant. Used when tenant wants the order to be re-synced from {@code Channel}.
     * For administrative users only.
     * 
     * @param request
     * @return
     */
    DeleteSaleOrderResponse deleteSaleOrder(DeleteSaleOrderRequest request);

    List<SaleOrderVO> getFailedSaleOrdersByUnmappedChannelProductId(String skuCode);

    RemoveMappingErrorRetrySaleOrderResponse removeMappingErrorFromSaleOrderVOs(RemoveMappingErrorRetrySaleOrderRequest request);

    GetErrorsForFailedOrdersResponse getErrorsFromSaleOrderVOs(GetErrorsForFailedOrdersRequest request);

    FixAddressErrorInFailedOrderResponse fixAddressError(FixAddressErrorInFailedOrderRequest request);

    GenerateSaleOrderNextSequenceResponse generateNext(GenerateSaleOrderNextSequenceRequest request);

    LookupSaleOrderLineItemsResponse lookupSaleOrderLineItems(LookupSaleOrderLineItemsRequest request) throws IOException;

    SaleOrder getSaleOrderByCode(String code, Integer channelId);

    List<SaleOrder> getReconciliationPendingOrders(int start, int pageSize);

    GetOrderSyncResponse getOrderSyncStatuses(GetOrderSyncRequest request);

    List<SaleOrderDTO> getSaleOrdersForProcessing(Integer itemTypeId);

    List<SaleOrder> searchSaleOrderByDisplayOrderCode(String keyword);

    List<SaleOrder> searchSaleOrderByOrderCode(String keyword);

    void processDirtySaleOrder(SaleOrder dirtySaleOrder);

    List<SaleOrder> getDirtySaleOrders(int start, int pageSize);

    MarkSaleOrderItemsCompleteResponse markSaleOrderItemsComplete(MarkSaleOrderItemsCompleteRequest request);

    MarkSaleOrderItemsUnfulfillableResponse markSaleOrderItemsUnfulfillable(MarkSaleOrderItemsUnfulfillableRequest request);

    List<SaleOrder> getPendingSaleOrders(Integer pendingSinceInHours, List<String> pendingOrderStatuses, Integer facilityId);

    GetCancellationReasonsResponse getCancellationReasons(GetCancellationReasonsRequest request);

    GetShippingPackagesByStatusCodeResponse getShippingPackageByStatusCode(GetShippingPackagesByStatusCodeRequest request);

    GetReturnReasonsResponse getReturnReasons(GetReturnReasonsRequest request);

    GetUnverifiedSaleOrdersResponse getUnverifiedSaleOrders(GetUnverifiedSaleOrdersRequest request);

    GetFailedSaleOrdersResponse getFailedSaleOrders(GetFailedSaleOrdersRequest request);

    boolean sendPendingPickupOrdersNotifications();

    List<String> getSaleOrderItemCodes(String saleOrderCode);

    Long getUnprocessedSaleOrderItemCountByChannelProductId(int channelId, String channelProductId);

    Map<String, Long> getUnprocessedSaleOrderItemsCount(int channelId);

    boolean splitsBundleItems(String saleOrderCode, List<String> saleOrderItemCodes, List<String> picklistSaleorderItemCodes);

    SaleOrderItem getSaleOrderItemForDeallocationInPicklist(String picklistCode, Item item);

    Long getUnserviceableSaleOrderItemsCountByChannelProductId(String channelProductId);

    List<SaleOrderItem> getFulfillableSaleOrderItemsForItemTypeInventory(int itemTypeInventoryId, int numberOfItemsToRelease);

    SaleOrderItem getSaleOrderItemForDeallocation(Item item);

    SaleOrderItem getSaleOrderItemForAllocationInPicklist(String picklistCode, Item item, String shippingPackageCode);

    void updateSaleOrderItemStatusForPicklistCreation(Set<Integer> saleOrderItemIds, String statusCode);
}
