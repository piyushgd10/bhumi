/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 15, 2011
 *  @author singla
 */
package com.uniware.services.saleorder;

import com.uniware.core.api.channel.GetShippingPackageTaxSummaryRequest;
import com.uniware.core.api.channel.GetShippingPackageTaxSummaryResponse;
import com.uniware.core.api.saleorder.display.GetSaleOrderActivitiesRequest;
import com.uniware.core.api.saleorder.display.GetSaleOrderActivitiesResponse;
import com.uniware.core.api.saleorder.display.GetSaleOrderInvoicesRequest;
import com.uniware.core.api.saleorder.display.GetSaleOrderInvoicesResponse;
import com.uniware.core.api.saleorder.display.GetSaleOrderLineItemDetailsRequest;
import com.uniware.core.api.saleorder.display.GetSaleOrderLineItemDetailsResponse;
import com.uniware.core.api.saleorder.display.GetSaleOrderLineItemsRequest;
import com.uniware.core.api.saleorder.display.GetSaleOrderLineItemsResponse;
import com.uniware.core.api.saleorder.display.GetSaleOrderPriceSummaryRequest;
import com.uniware.core.api.saleorder.display.GetSaleOrderPriceSummaryResponse;
import com.uniware.core.api.saleorder.display.GetSaleOrderReturnsRequest;
import com.uniware.core.api.saleorder.display.GetSaleOrderReturnsResponse;
import com.uniware.core.api.saleorder.display.GetSaleOrderShippingPackagesRequest;
import com.uniware.core.api.saleorder.display.GetSaleOrderShippingPackagesResponse;
import com.uniware.core.api.saleorder.display.GetSaleOrderSummaryRequest;
import com.uniware.core.api.saleorder.display.GetSaleOrderSummaryResponse;

/**
 * @author Sunny
 */
public interface ISaleOrderDisplayService {

    GetSaleOrderSummaryResponse getSaleOrderSummary(GetSaleOrderSummaryRequest request);

    GetSaleOrderLineItemsResponse getSaleOrderLineItems(GetSaleOrderLineItemsRequest request);

    GetSaleOrderLineItemDetailsResponse getSaleOrderLineItemDetails(GetSaleOrderLineItemDetailsRequest request);

    GetSaleOrderInvoicesResponse getSaleOrderInvoices(GetSaleOrderInvoicesRequest request);

    GetSaleOrderShippingPackagesResponse getSaleOrderShippingPackages(GetSaleOrderShippingPackagesRequest request);

    GetSaleOrderPriceSummaryResponse getSaleOrderPriceSummary(GetSaleOrderPriceSummaryRequest request);

    GetSaleOrderReturnsResponse getSaleOrderReturns(GetSaleOrderReturnsRequest request);

    GetSaleOrderActivitiesResponse getSaleOrderActivities(GetSaleOrderActivitiesRequest request);

    GetShippingPackageTaxSummaryResponse getShippingPackageTaxSummary(GetShippingPackageTaxSummaryRequest request);
}
