/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 26, 2011
 *  @author singla
 */
package com.uniware.services.saleorder;

import com.unifier.core.annotation.audit.LogActivity;
import com.uniware.core.api.saleorder.AllocateInventoryAndGenerateInvoiceRequest;
import com.uniware.core.api.saleorder.AllocateInventoryAndGenerateInvoiceResponse;
import com.uniware.core.api.saleorder.AllocateInventoryCreatePackageRequest;
import com.uniware.core.api.saleorder.AllocateInventoryCreatePackageResponse;


import com.uniware.core.entity.SaleOrder;
import com.uniware.core.entity.ShippingPackage;
import com.uniware.core.locking.Namespace;
import com.uniware.core.locking.annotation.Lock;
import com.uniware.core.locking.annotation.Locks;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author singla
 */
public interface ISaleOrderProcessingService {

    AllocateInventoryCreatePackageResponse allocateInventoryAndCreatePackage(AllocateInventoryCreatePackageRequest request);

    AllocateInventoryAndGenerateInvoiceResponse allocateInventoryAndGenerateInvoice(AllocateInventoryAndGenerateInvoiceRequest request);

    void allocateFacility(String saleOrderId, String scriptName);

    ShippingPackage createPackageForCompleteOrder(SaleOrder saleOrder, int packetNumber);

}
