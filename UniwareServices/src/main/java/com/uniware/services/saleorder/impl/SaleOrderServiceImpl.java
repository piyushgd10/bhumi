/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *  @version     1.0, Dec 15, 2011
 *  @author singla
 */
package com.uniware.services.saleorder.impl;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import org.hibernate.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.unifier.core.annotation.audit.LogActivity;
import com.unifier.core.api.datatable.GetDatatableResultRequest;
import com.unifier.core.api.export.WsExportFilter;
import com.unifier.core.api.validation.ResponseCode;
import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.api.validation.WsError;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.email.EmailMessage;
import com.unifier.core.entity.User;
import com.unifier.core.export.config.ExportColumn;
import com.unifier.core.export.config.ExportConfig;
import com.unifier.core.fileparser.DelimitedFileParser;
import com.unifier.core.fileparser.Row;
import com.unifier.core.sms.SmsMessage;
import com.unifier.core.ui.SelectItem;
import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.FileUtils;
import com.unifier.core.utils.NumberUtils;
import com.unifier.core.utils.StringUtils;
import com.unifier.core.utils.ValidatorUtils;
import com.unifier.dao.export.IExportDao;
import com.unifier.dao.export.IExportMao;
import com.unifier.scraper.sl.runtime.IScriptProvider;
import com.unifier.scraper.sl.runtime.ScraperScript;
import com.unifier.scraper.sl.runtime.ScriptExecutionContext;
import com.unifier.services.aspect.RollbackOnFailure;
import com.unifier.services.email.IEmailService;
import com.unifier.services.export.IExportService;
import com.unifier.services.sms.ISmsService;
import com.unifier.services.users.IUsersService;
import com.unifier.services.utils.CustomFieldUtils;
import com.unifier.services.utils.ExceptionUtils;
import com.uniware.core.api.catalog.LookupSaleOrderLineItemsRequest;
import com.uniware.core.api.catalog.LookupSaleOrderLineItemsResponse;
import com.uniware.core.api.catalog.dto.ItemTypeLookupDTO;
import com.uniware.core.api.channel.ChannelOrderSyncDTO;
import com.uniware.core.api.channel.ChannelOrderSyncStatusDTO;
import com.uniware.core.api.channel.ChannelSyncDTO;
import com.uniware.core.api.channel.CreateChannelItemTypeRequest;
import com.uniware.core.api.channel.WsChannelItemType;
import com.uniware.core.api.currency.GetCurrencyConversionRateRequest;
import com.uniware.core.api.currency.GetCurrencyConversionRateResponse;
import com.uniware.core.api.invoice.CreateInvoiceWithDetailsRequest;
import com.uniware.core.api.invoice.CreateInvoiceWithDetailsResponse;
import com.uniware.core.api.item.ItemDetailFieldDTO;
import com.uniware.core.api.model.WSSaleOrderItemLight;
import com.uniware.core.api.model.WsAddressDetail;
import com.uniware.core.api.model.WsAddressRef;
import com.uniware.core.api.model.WsInvoice;
import com.uniware.core.api.model.WsSaleOrder;
import com.uniware.core.api.model.WsSaleOrderItem;
import com.uniware.core.api.model.WsSaleOrderItem.WsSaleOrderItemPricing;
import com.uniware.core.api.model.WsSaleOrderItemCombination;
import com.uniware.core.api.model.WsShippingProvider;
import com.uniware.core.api.saleorder.AcceptSaleOrderAlternateRequest;
import com.uniware.core.api.saleorder.AcceptSaleOrderAlternateResponse;
import com.uniware.core.api.saleorder.AcceptSaleOrderItemAlternateRequest;
import com.uniware.core.api.saleorder.AcceptSaleOrderItemAlternateResponse;
import com.uniware.core.api.saleorder.AddOrEditSaleOrderItemsRequest;
import com.uniware.core.api.saleorder.AddOrEditSaleOrderItemsResponse;
import com.uniware.core.api.saleorder.AddressDetailErrorDTO;
import com.uniware.core.api.saleorder.AllocateInventoryCreatePackageRequest;
import com.uniware.core.api.saleorder.CancelSaleOrderRequest;
import com.uniware.core.api.saleorder.CancelSaleOrderResponse;
import com.uniware.core.api.saleorder.CreateSaleOrderAlternateRequest;
import com.uniware.core.api.saleorder.CreateSaleOrderAlternateResponse;
import com.uniware.core.api.saleorder.CreateSaleOrderItemAlternateRequest;
import com.uniware.core.api.saleorder.CreateSaleOrderItemAlternateResponse;
import com.uniware.core.api.saleorder.CreateSaleOrderRequest;
import com.uniware.core.api.saleorder.CreateSaleOrderResponse;
import com.uniware.core.api.saleorder.CreateSaleOrderResponse.ChannelItemTypeDTO;
import com.uniware.core.api.saleorder.CreateSaleOrderResponse.ChannelItemTypeDTO.Status;
import com.uniware.core.api.saleorder.DeleteFailedSaleOrdersRequest;
import com.uniware.core.api.saleorder.DeleteFailedSaleOrdersResponse;
import com.uniware.core.api.saleorder.DeleteSaleOrderRequest;
import com.uniware.core.api.saleorder.DeleteSaleOrderResponse;
import com.uniware.core.api.saleorder.EditSaleOrderAddressRequest;
import com.uniware.core.api.saleorder.EditSaleOrderAddressRequest.WsSaleOrderAddress;
import com.uniware.core.api.saleorder.EditSaleOrderAddressRequest.WsSaleOrderAddressItem;
import com.uniware.core.api.saleorder.EditSaleOrderAddressResponse;
import com.uniware.core.api.saleorder.EditSaleOrderAddressesRequest;
import com.uniware.core.api.saleorder.EditSaleOrderAddressesRequest.SaleOrderAddressDTO;
import com.uniware.core.api.saleorder.EditSaleOrderAddressesResponse;
import com.uniware.core.api.saleorder.EditSaleOrderItemMetadataRequest;
import com.uniware.core.api.saleorder.EditSaleOrderItemMetadataResponse;
import com.uniware.core.api.saleorder.EditSaleOrderItemRequest;
import com.uniware.core.api.saleorder.EditSaleOrderItemResponse;
import com.uniware.core.api.saleorder.EditSaleOrderMetadataRequest;
import com.uniware.core.api.saleorder.EditSaleOrderMetadataResponse;
import com.uniware.core.api.saleorder.FixAddressErrorInFailedOrderRequest;
import com.uniware.core.api.saleorder.FixAddressErrorInFailedOrderResponse;
import com.uniware.core.api.saleorder.GenerateSaleOrderNextSequenceRequest;
import com.uniware.core.api.saleorder.GenerateSaleOrderNextSequenceResponse;
import com.uniware.core.api.saleorder.GetCancellationReasonsRequest;
import com.uniware.core.api.saleorder.GetCancellationReasonsResponse;
import com.uniware.core.api.saleorder.GetErrorsForFailedOrdersRequest;
import com.uniware.core.api.saleorder.GetErrorsForFailedOrdersResponse;
import com.uniware.core.api.saleorder.GetFailedSaleOrdersRequest;
import com.uniware.core.api.saleorder.GetFailedSaleOrdersResponse;
import com.uniware.core.api.saleorder.GetOrderSyncRequest;
import com.uniware.core.api.saleorder.GetOrderSyncResponse;
import com.uniware.core.api.saleorder.GetReturnReasonsRequest;
import com.uniware.core.api.saleorder.GetReturnReasonsResponse;
import com.uniware.core.api.saleorder.GetSaleOrderAlternateRequest;
import com.uniware.core.api.saleorder.GetSaleOrderAlternateResponse;
import com.uniware.core.api.saleorder.GetSaleOrderItemAlternateRequest;
import com.uniware.core.api.saleorder.GetSaleOrderItemAlternateResponse;
import com.uniware.core.api.saleorder.GetSaleOrderItemAlternateResponse.SaleOrderItemAlternateDTO;
import com.uniware.core.api.saleorder.GetSaleOrderItemAlternateResponse.SaleOrderItemAlternateSuggestionDTO;
import com.uniware.core.api.saleorder.GetSaleOrderRequest;
import com.uniware.core.api.saleorder.GetSaleOrderResponse;
import com.uniware.core.api.saleorder.GetShippingPackagesByStatusCodeRequest;
import com.uniware.core.api.saleorder.GetShippingPackagesByStatusCodeResponse;
import com.uniware.core.api.saleorder.GetUnverifiedSaleOrdersRequest;
import com.uniware.core.api.saleorder.GetUnverifiedSaleOrdersResponse;
import com.uniware.core.api.saleorder.HoldSaleOrderItemsRequest;
import com.uniware.core.api.saleorder.HoldSaleOrderItemsResponse;
import com.uniware.core.api.saleorder.HoldSaleOrderRequest;
import com.uniware.core.api.saleorder.HoldSaleOrderResponse;
import com.uniware.core.api.saleorder.MarkSaleOrderItemsCompleteRequest;
import com.uniware.core.api.saleorder.MarkSaleOrderItemsCompleteResponse;
import com.uniware.core.api.saleorder.MarkSaleOrderItemsUnfulfillableRequest;
import com.uniware.core.api.saleorder.MarkSaleOrderItemsUnfulfillableResponse;
import com.uniware.core.api.saleorder.ModifyPacketSaleOrderRequest;
import com.uniware.core.api.saleorder.ModifyPacketSaleOrderResponse;
import com.uniware.core.api.saleorder.RemoveMappingErrorRetrySaleOrderRequest;
import com.uniware.core.api.saleorder.RemoveMappingErrorRetrySaleOrderResponse;
import com.uniware.core.api.saleorder.SaleOrderDetailDTO;
import com.uniware.core.api.saleorder.SaleOrderItemDTO;
import com.uniware.core.api.saleorder.SaleOrderItemLightDTO;
import com.uniware.core.api.saleorder.SearchSaleOrderItemRequest;
import com.uniware.core.api.saleorder.SearchSaleOrderItemResponse;
import com.uniware.core.api.saleorder.SearchSaleOrderItemResponse.SearchSaleOrderItemDTO;
import com.uniware.core.api.saleorder.SearchSaleOrderRequest;
import com.uniware.core.api.saleorder.SearchSaleOrderResponse;
import com.uniware.core.api.saleorder.SetSaleOrderPriorityRequest;
import com.uniware.core.api.saleorder.SetSaleOrderPriorityResponse;
import com.uniware.core.api.saleorder.ShippingPackageDTO;
import com.uniware.core.api.saleorder.SkuMappingErrorDTO;
import com.uniware.core.api.saleorder.SwitchSaleOrderItemFacilityRequest;
import com.uniware.core.api.saleorder.SwitchSaleOrderItemFacilityResponse;
import com.uniware.core.api.saleorder.UnblockSaleOrderItemsInventoryRequest;
import com.uniware.core.api.saleorder.UnblockSaleOrderItemsInventoryResponse;
import com.uniware.core.api.saleorder.UnholdSaleOrderItemsRequest;
import com.uniware.core.api.saleorder.UnholdSaleOrderItemsResponse;
import com.uniware.core.api.saleorder.UnholdSaleOrderRequest;
import com.uniware.core.api.saleorder.UnholdSaleOrderResponse;
import com.uniware.core.api.saleorder.VerifySaleOrderRequest;
import com.uniware.core.api.saleorder.VerifySaleOrderResponse;
import com.uniware.core.api.saleorder.VerifySaleOrdersRequest;
import com.uniware.core.api.saleorder.VerifySaleOrdersResponse;
import com.uniware.core.api.saleorder.WsSaleOrderItemAlternate;
import com.uniware.core.api.shipping.ForceDispatchShippingPackageRequest;
import com.uniware.core.api.shipping.ForceDispatchShippingPackageResponse;
import com.uniware.core.api.shipping.ReassessShippingPackageRequest;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.cache.EnvironmentPropertiesCache;
import com.uniware.core.cache.FacilityCache;
import com.uniware.core.cache.LocationCache;
import com.uniware.core.entity.AddressDetail;
import com.uniware.core.entity.Bundle;
import com.uniware.core.entity.BundleItemType;
import com.uniware.core.entity.Channel;
import com.uniware.core.entity.Channel.SyncStatus;
import com.uniware.core.entity.ChannelConfigurationParameter;
import com.uniware.core.entity.ChannelConnector;
import com.uniware.core.entity.ChannelConnectorParameter;
import com.uniware.core.entity.ChannelItemType;
import com.uniware.core.entity.Country;
import com.uniware.core.entity.Currency;
import com.uniware.core.entity.Customer;
import com.uniware.core.entity.Facility;
import com.uniware.core.entity.Item;
import com.uniware.core.entity.ItemType;
import com.uniware.core.entity.ItemTypeInventory;
import com.uniware.core.entity.Party;
import com.uniware.core.entity.PartyAddress;
import com.uniware.core.entity.PartyAddressType;
import com.uniware.core.entity.PaymentMethod;
import com.uniware.core.entity.PurchaseOrder;
import com.uniware.core.entity.ReversePickup;
import com.uniware.core.entity.SaleOrder;
import com.uniware.core.entity.SaleOrderItem;
import com.uniware.core.entity.SaleOrderItem.ItemDetailingStatus;
import com.uniware.core.entity.SaleOrderItemAlternate;
import com.uniware.core.entity.SaleOrderItemAlternateSuggestion;
import com.uniware.core.entity.SaleOrderItemStatus;
import com.uniware.core.entity.SaleOrderStatus;
import com.uniware.core.entity.Sequence.Name;
import com.uniware.core.entity.Shelf;
import com.uniware.core.entity.ShippingMethod;
import com.uniware.core.entity.ShippingPackage;
import com.uniware.core.entity.ShippingPackage.StatusCode;
import com.uniware.core.entity.ShippingProvider;
import com.uniware.core.entity.SmsTemplate;
import com.uniware.core.entity.Source;
import com.uniware.core.entity.SourceConnector;
import com.uniware.core.entity.State;
import com.uniware.core.locking.Namespace;
import com.uniware.core.locking.annotation.Lock;
import com.uniware.core.locking.annotation.Locks;
import com.uniware.core.utils.ActivityContext;
import com.uniware.core.utils.Constants;
import com.uniware.core.utils.UserContext;
import com.uniware.core.view.View;
import com.uniware.core.vo.ChannelItemTypeVO;
import com.uniware.core.vo.FacilityAllocationVO;
import com.uniware.core.vo.SaleOrderFlowSummaryVO;
import com.uniware.core.vo.SaleOrderVO;
import com.uniware.dao.reconciliation.IReconciliationMao;
import com.uniware.dao.returns.IReturnsDao;
import com.uniware.dao.saleorder.ISaleOrderDao;
import com.uniware.dao.saleorder.ISaleOrderMao;
import com.uniware.services.audit.impl.ActivityEntityEnum;
import com.uniware.services.audit.impl.ActivityTypeEnum;
import com.uniware.services.audit.impl.ActivityUtils;
import com.uniware.services.bundle.IBundleService;
import com.uniware.services.cache.ChannelCache;
import com.uniware.services.cache.ItemTypeDetailCache;
import com.uniware.services.cache.ScriptVersionedCache;
import com.uniware.services.cache.UICustomizationsCache;
import com.uniware.services.catalog.ICatalogService;
import com.uniware.services.channel.IChannelCatalogService;
import com.uniware.services.channel.IChannelService;
import com.uniware.services.common.ISequenceGenerator;
import com.uniware.services.configuration.CurrencyConfiguration;
import com.uniware.services.configuration.EmailConfiguration;
import com.uniware.services.configuration.ExportJobConfiguration;
import com.uniware.services.configuration.ShippingConfiguration;
import com.uniware.services.configuration.SmsConfiguration;
import com.uniware.services.configuration.SourceConfiguration;
import com.uniware.services.configuration.SystemConfiguration;
import com.uniware.services.configuration.TenantSystemConfiguration;
import com.uniware.services.configuration.data.manager.ProductConfiguration;
import com.uniware.services.currency.ICurrencyService;
import com.uniware.services.customer.ICustomerService;
import com.uniware.services.inventory.IInventoryService;
import com.uniware.services.picker.IPickerService;
import com.uniware.services.saleorder.ISaleOrderProcessingService;
import com.uniware.services.saleorder.ISaleOrderService;
import com.uniware.services.shipping.IDispatchService;
import com.uniware.services.shipping.IShippingInvoiceService;
import com.uniware.services.shipping.IShippingProviderService;
import com.uniware.services.shipping.IShippingService;
import com.uniware.services.warehouse.IFacilityService;
import com.uniware.services.warehouse.IShelfService;

/**
 * @author singla
 */
@Service("saleOrderService")
public class SaleOrderServiceImpl implements ISaleOrderService {

    private static final Logger         LOG                    = LoggerFactory.getLogger(SaleOrderServiceImpl.class);

    private static final String         DEFAULT_PINCODE        = "0";

    private static final Date           orderDeletionTolerence = DateUtils.createDate(2016, 8, 1);

    @Autowired
    private ISaleOrderMao               saleOrderMao;

    @Autowired
    private ISaleOrderDao               saleOrderDao;

    @Autowired
    private IReturnsDao                 returnsDao;

    @Autowired
    private ICatalogService             catalogService;

    @Autowired
    private IShippingService            shippingService;

    @Autowired
    private IInventoryService           inventoryService;

    @Autowired
    private IShippingProviderService    shippingProviderService;

    @Autowired
    private IPickerService              pickerService;

    @Autowired
    private ICustomerService            customerService;

    @Autowired
    private IChannelCatalogService      channelCatalogService;

    @Autowired
    private ISequenceGenerator          sequenceGenerator;

    @Autowired
    private ICurrencyService            currencyService;

    @Autowired
    private IBundleService              bundleService;

    @Autowired
    private ISaleOrderProcessingService saleOrderProcessingService;

    @Autowired
    private IExportMao                  exportMao;

    @Autowired
    private IUsersService               usersService;

    @Autowired
    private IExportDao                  exportDao;

    @Autowired
    private IExportService              exportService;

    @Autowired
    private IShelfService               shelfService;

    @Autowired
    private IDispatchService            dispatchService;

    @Autowired
    private IEmailService               emailService;

    @Autowired
    private ISmsService                 smsService;

    @Autowired
    private IChannelService             channelService;

    @Autowired
    private IFacilityService            facilityService;

    @Autowired
    private IShippingInvoiceService     shippingInvoiceService;

    @Autowired
    private IReconciliationMao          reconciliationMao;

    @Override
    public void createOrUpdateFailedSaleOrder(SaleOrderVO saleOrderVO) {
        saleOrderVO.setFixable(true);
        if (saleOrderVO.getResponse().getErrors() != null) {
            for (WsError error : saleOrderVO.getResponse().getErrors()) {
                if (error.getCode() != WsResponseCode.INVALID_ADDRESS_DETAIL.code() && error.getCode() != WsResponseCode.SKU_NOT_MAPPED.code()
                        && !(error.getCode() == ResponseCode.MISSING_REQUIRED_PARAMETERS.code() && error.getDescription().contains("saleOrder.addresses"))) {
                    saleOrderVO.setFixable(false);
                }
            }
        }
        saleOrderVO.setUpdated(DateUtils.getCurrentTime());
        saleOrderMao.save(saleOrderVO);
    }

    @Override
    public DeleteFailedSaleOrdersResponse removeAllFailedSaleOrders(DeleteFailedSaleOrdersRequest request) {
        DeleteFailedSaleOrdersResponse response = new DeleteFailedSaleOrdersResponse();
        channelCatalogService.resetFailedOrderInventory();
        saleOrderMao.removeAllSaleOrders();
        response.setSuccessful(true);
        return response;
    }

    @Override
    public SaleOrderVO getFailedSaleOrderByCode(String saleOrderCode) {
        return saleOrderMao.getSaleOrderVOByCode(saleOrderCode);
    }

    @Override
    public List<SaleOrderVO> getFailedSaleOrdersByUnmappedChannelProductId(String channelProductId) {
        return saleOrderMao.getFailedOrdersByChannelProductId(channelProductId);
    }

    @Override
    public GetErrorsForFailedOrdersResponse getErrorsFromSaleOrderVOs(GetErrorsForFailedOrdersRequest request) {
        GetErrorsForFailedOrdersResponse response = new GetErrorsForFailedOrdersResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            List<SaleOrderVO> failedOrders = saleOrderMao.getFailedOrdersForSaleOrderCodes(request.getSaleOrderCodes());
            Map<String, SkuMappingErrorDTO> mappingErrors = new HashMap<>();
            Map<String, AddressDetailErrorDTO> addressErrors = new HashMap<>();
            for (SaleOrderVO vo : failedOrders) {
                for (WsError error : vo.getResponse().getErrors()) {
                    if (error.getCode() == WsResponseCode.SKU_NOT_MAPPED.code()) {
                        SkuMappingErrorDTO mappingError = new SkuMappingErrorDTO();
                        mappingError.setSaleOrderCode(vo.getCode());
                        ChannelItemTypeDTO channelItemTypeDTO = (ChannelItemTypeDTO) error.getErrorParams().get("channelItemType");
                        mappingError.setChannelProductId(channelItemTypeDTO.getChannelProductId());
                        Channel channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelByCode(vo.getRequest().getSaleOrder().getChannel());
                        mappingError.setChannelCode(channel.getCode());
                        mappingErrors.put(channelItemTypeDTO.getChannelProductId(), mappingError);

                    } else if (error.getCode() == WsResponseCode.INVALID_ADDRESS_DETAIL.code()) {
                        AddressDetailErrorDTO addressError = new AddressDetailErrorDTO();
                        if (addressErrors.get(vo.getCode()) != null) {
                            addressError = addressErrors.get(vo.getCode());
                        } else {
                            addressErrors.put(vo.getCode(), addressError);
                            response.getAddressErrors().add(addressError);
                            addressError.setSaleOrder(vo.getRequest().getSaleOrder());
                        }
                        addressError.getErrors().add(error);

                    } else if (error.getCode() == ResponseCode.MISSING_REQUIRED_PARAMETERS.code()) {
                        if (error.getDescription().contains("saleOrder.addresses")) {
                            AddressDetailErrorDTO addressError = new AddressDetailErrorDTO();
                            if (addressErrors.get(vo.getCode()) != null) {
                                addressError = addressErrors.get(vo.getCode());
                            } else {
                                addressErrors.put(vo.getCode(), addressError);
                                addressError.setSaleOrder(vo.getRequest().getSaleOrder());
                                response.getAddressErrors().add(addressError);
                            }
                            addressError.getErrors().add(error);
                        }
                    }
                }
                response.setSkuErrors(new ArrayList<SkuMappingErrorDTO>(mappingErrors.values()));
                if (mappingErrors.size() < 1 && response.getAddressErrors().size() < 1) {
                    response.setMessage("No fixable errors found in the selected orders");
                }
                response.setSuccessful(true);
            }
        } else {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    @Override
    public RemoveMappingErrorRetrySaleOrderResponse removeMappingErrorFromSaleOrderVOs(RemoveMappingErrorRetrySaleOrderRequest request) {
        ValidationContext context = request.validate();
        RemoveMappingErrorRetrySaleOrderResponse response = new RemoveMappingErrorRetrySaleOrderResponse();
        if (!context.hasErrors()) {
            String channelProductId = request.getChannelProductId();
            int count = 0;
            int successfulOrderCount = 0;
            List<SaleOrderVO> list = getFailedSaleOrdersByUnmappedChannelProductId(channelProductId);
            for (SaleOrderVO failedOrder : list) {
                int mappingErrorCount = 0;
                for (Iterator<WsError> errorIter = failedOrder.getResponse().getErrors().listIterator(); errorIter.hasNext();) {
                    WsError error = errorIter.next();
                    if (error.getErrorParams() != null) {
                        ChannelItemTypeDTO channelItemTypeDTO = (ChannelItemTypeDTO) error.getErrorParams().get("channelItemType");
                        if (channelItemTypeDTO != null) {
                            if (channelProductId.equals(channelItemTypeDTO.getChannelProductId())) {
                                errorIter.remove();
                                mappingErrorCount++;
                            }
                        }
                    }
                }
                if (failedOrder.getResponse().getErrors().size() == 0) {
                    CreateSaleOrderResponse resp = createSaleOrder(failedOrder.getRequest());
                    if (resp.isSuccessful()) {
                        successfulOrderCount++;
                    } else {
                        failedOrder.setResponse(resp);
                        createOrUpdateFailedSaleOrder(failedOrder);
                    }
                } else if (mappingErrorCount > 0) {
                    count++;
                    createOrUpdateFailedSaleOrder(failedOrder);
                }
            }
            response.setSuccessful(true);
            response.setNumOfOrdersUpdated(count);
            response.setSuccessfulOrderCount(successfulOrderCount);
        } else {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    @Override
    public FixAddressErrorInFailedOrderResponse fixAddressError(FixAddressErrorInFailedOrderRequest request) {
        FixAddressErrorInFailedOrderResponse response = new FixAddressErrorInFailedOrderResponse();
        ValidationContext context = request.validate();
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        } else {
            SaleOrderVO failedOrder = saleOrderMao.getSaleOrderVOByCode(request.getSaleOrderCode());
            CreateSaleOrderRequest retryRequest = failedOrder.getRequest();
            List<WsAddressDetail> addresses = retryRequest.getSaleOrder().getAddresses();
            for (int i = 0; i < addresses.size(); i++) {
                if (addresses.get(i).getId().equals(request.getAddress().getId())) {
                    addresses.set(i, request.getAddress());
                }
            }
            CreateSaleOrderResponse retryResponse = createSaleOrder(retryRequest);
            if (retryResponse.isSuccessful()) {
                response.setSuccessful(true);
            } else {
                response.setErrors(retryResponse.getErrors());
                failedOrder.setResponse(retryResponse);
                createOrUpdateFailedSaleOrder(failedOrder);
            }
        }
        return response;
    }

    @Override
    public List<SaleOrderVO> getAllSaleOrderVOs() {
        return saleOrderMao.getAllSaleOrders();
    }

    @Override
    public CreateSaleOrderResponse createSaleOrder(CreateSaleOrderRequest request) {
        CreateSaleOrderResponse response = new CreateSaleOrderResponse();
        boolean internalError = false;
        try {
            response = createSaleOrderInternal(request);
        } catch (DataIntegrityViolationException dke) {
            if (ExceptionUtils.isDuplicateKeyException(dke)) {
                response.addError(new WsError(WsResponseCode.DUPLICATE_SALE_ORDER.code(), WsResponseCode.DUPLICATE_SALE_ORDER.message(),
                        "Duplicate request for saleOrder[@code=" + request.getSaleOrder().getCode() + "]"));
            } else {
                internalError = true;
            }
            LOG.error("Error creating saleOrder " + request.getSaleOrder().getCode() + ", Stack trace: ", dke);
        } catch (Exception e) {
            internalError = true;
            LOG.error("Internal error for saleOrder " + request.getSaleOrder().getCode() + ", Stack trace: ", e);
        }

        if (!response.isSuccessful()) {
            handleFailure(request, response, internalError);
        } else {
            handleSuccess(request);
        }
        return response;
    }

    private void handleSuccess(CreateSaleOrderRequest request) {
        SaleOrderVO failedOrder = getFailedSaleOrderByCode(request.getSaleOrder().getCode());
        Channel channel = channelService.getChannelByCode(request.getSaleOrder().getChannel());
        Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(channel.getSourceCode());
        if (failedOrder != null) {
            saleOrderMao.remove(request.getSaleOrder().getCode());
            if (!source.isFulfillmentByChannel()) {
                channelCatalogService.updateFailedOrderInventory(failedOrder.getRequest(), false);
            }
        }

        if (StringUtils.isNotBlank(request.getSaleOrder().getChannel())) {
            // Update blocked inventories for this channel
            if (source.isFulfillmentByChannel()) {
                for (WsSaleOrderItem saleOrderItem : request.getSaleOrder().getSaleOrderItems()) {
                    String channelSku = StringUtils.isNotBlank(saleOrderItem.getChannelProductId()) ? saleOrderItem.getChannelProductId() : saleOrderItem.getItemSku();
                    ChannelItemType cit = channelCatalogService.blockChannelItemTypeInventory(request.getSaleOrder().getChannel(), channelSku, 1);
                    if (cit != null && ConfigurationManager.getInstance().getConfiguration(ProductConfiguration.class).isProductManagementSwitchedOff()) {
                        channelCatalogService.decrementNextInventoryUpdate(cit, 1);
                    }
                }
            }
        }
        if ((ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).isInventoryManagementSwitchedOff())) {
            SaleOrder saleOrder = getSaleOrderByCode(request.getSaleOrder().getCode());
            if (!saleOrder.getStatusCode().equals(SaleOrder.StatusCode.PENDING_VERIFICATION.name())) {
                AllocateInventoryCreatePackageRequest createPackageRequest = new AllocateInventoryCreatePackageRequest();
                createPackageRequest.setSaleOrderId(saleOrder.getId());
                saleOrderProcessingService.allocateInventoryAndCreatePackage(createPackageRequest);
            }
        }
    }

    private void handleFailure(CreateSaleOrderRequest request, CreateSaleOrderResponse response, boolean unknownError) {
        updateFailedSaleOrderIfExists(request, response);
        if (unknownError) {
            response.addError(new WsError(WsResponseCode.SALE_ORDER_INTERNAL_ERROR.code(), WsResponseCode.SALE_ORDER_INTERNAL_ERROR.message(),
                    "Internal error for saleOrder[@code=" + request.getSaleOrder().getCode() + "]"));
        } else {
            for (WsError error : response.getErrors()) {
                if (WsResponseCode.SKU_NOT_MAPPED.code() == error.getCode()) {
                    ChannelItemTypeDTO channelItemTypeDTO = (ChannelItemTypeDTO) error.getErrorParams().get("channelItemType");
                    if (Status.MISSING.equals(channelItemTypeDTO.getStatusCode())) {
                        CreateChannelItemTypeRequest createUnmappedItemTypeRequest = new CreateChannelItemTypeRequest();
                        WsChannelItemType channelItemType = new WsChannelItemType();
                        channelItemType.setChannelProductId(channelItemTypeDTO.getChannelProductId());
                        channelItemType.setSellerSkuCode(channelItemTypeDTO.getSellerSkuCode());
                        channelItemType.setProductName(channelItemTypeDTO.getProductName());
                        Channel channel;
                        if (StringUtils.isBlank(request.getSaleOrder().getChannel())) {
                            channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelByName(Source.Code.CUSTOM.name());
                        } else {
                            channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelByCode(request.getSaleOrder().getChannel());
                        }
                        channelItemType.setChannelCode(channel.getCode());
                        createUnmappedItemTypeRequest.setChannelItemType(channelItemType);
                        createUnmappedItemTypeRequest.setSyncId(ChannelItemType.SyncedBy.SYSTEM.toString());
                        channelCatalogService.createChannelItemType(createUnmappedItemTypeRequest);
                    }
                }
            }
        }
    }

    @Override
    @Transactional
    @RollbackOnFailure
    @LogActivity
    public CreateSaleOrderResponse createSaleOrderInternal(CreateSaleOrderRequest request) {
        CreateSaleOrderResponse response = new CreateSaleOrderResponse();
        ValidationContext context = validateRequest(request);
        if (!context.hasErrors()) {
            SaleOrder saleOrder = prepareSaleOrder(request, context);
            Channel channel = null;
            if (!context.hasErrors()) {
                saleOrder = saleOrderDao.addSaleOrder(saleOrder);
                channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelById(saleOrder.getChannel().getId());
                if (StringUtils.isNotBlank(CacheManager.getInstance().getCache(ChannelCache.class).getScriptName(channel.getCode(),
                        com.uniware.core.entity.Source.SALE_ORDER_ACKNOWLEDGEMENT_SCRIPT_NAME))) {
                    ScraperScript scraperScript = CacheManager.getInstance().getCache(ChannelCache.class).getScriptByName(channel.getCode(),
                            com.uniware.core.entity.Source.SALE_ORDER_ACKNOWLEDGEMENT_SCRIPT_NAME);
                    if (scraperScript != null) {
                        try {
                            ScriptExecutionContext scriptContext = ScriptExecutionContext.current();
                            scriptContext.addVariable("channel", channel);
                            scriptContext.addVariable("saleOrder", saleOrder);
                            for (ChannelConnector channelConnector : channel.getChannelConnectors()) {
                                for (ChannelConnectorParameter channelConnectorParameter : channelConnector.getChannelConnectorParameters()) {
                                    scriptContext.addVariable(channelConnectorParameter.getName(), channelConnectorParameter.getValue());
                                }
                            }
                            for (ChannelConfigurationParameter channelConfigurationParameter : channel.getChannelConfigurationParameters()) {
                                scriptContext.addVariable(channelConfigurationParameter.getSourceConfigurationParameterName(), channelConfigurationParameter.getValue());
                            }
                            scriptContext.setScriptProvider(new IScriptProvider() {

                                @Override
                                public ScraperScript getScript(String scriptName) {
                                    return CacheManager.getInstance().getCache(ScriptVersionedCache.class).getScriptByName(scriptName);
                                }
                            });
                            scriptContext.setTraceLoggingEnabled(UserContext.current().isTraceLoggingEnabled());
                            scraperScript.execute();
                        } catch (Exception e) {
                            LOG.error("unable to acknowledge saleOrder:" + saleOrder.getCode(), e);
                            context.addError(WsResponseCode.INVALID_STATE, e.getMessage());
                        } finally {
                            ScriptExecutionContext.destroy();
                        }
                    }
                }
                if (ActivityContext.current().isEnable()) {
                    ActivityUtils.appendActivity(saleOrder.getCode(), ActivityEntityEnum.SALE_ORDER.getName(), null,
                            Arrays.asList(new String[] { "Order {" + ActivityEntityEnum.SALE_ORDER + ":" + saleOrder.getCode() + "} created" }), ActivityTypeEnum.CREATION.name());
                }
                if (saleOrder != null) {
                    SaleOrderDetailDTO saleOrderDetailDTO = new SaleOrderDetailDTO(saleOrder);
                    for (SaleOrderItem saleOrderItem : saleOrder.getSaleOrderItems()) {
                        SaleOrderItemDTO saleOrderItemDTO = new SaleOrderItemDTO(saleOrderItem);
                        // Add Item Detailing SOI_DTO
                        List<ItemDetailFieldDTO> itemDetailFieldDTOList = new ArrayList<>();
                        String itemDetailFieldText = saleOrderItem.getItemDetailFields();
                        if (StringUtils.isNotBlank(itemDetailFieldText)) {
                            for (String itemDetailField : StringUtils.split(itemDetailFieldText)) {
                                ItemDetailFieldDTO detailFieldDTO = CacheManager.getInstance().getCache(ItemTypeDetailCache.class).getItemDetailFieldByName(itemDetailField);
                                itemDetailFieldDTOList.add(detailFieldDTO);
                            }
                        }
                        saleOrderItemDTO.setItemDetailFieldDTOList(itemDetailFieldDTOList);
                        saleOrderDetailDTO.addSaleOrderItem(saleOrderItemDTO);
                    }
                    response.setSaleOrderDetailDTO(saleOrderDetailDTO);
                }
            }
            if (!context.hasErrors() && channel != null) {
                Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(channel.getSourceCode());
                if (source.isFetchCompleteOrders()) {
                    Facility currentFacility = UserContext.current().getFacility();
                    try {
                        Facility associatedFacility = facilityService.getAllFacilityByCode(saleOrder.getChannel().getCode());
                        UserContext.current().setFacility(associatedFacility);
                        /*
                         * Syste Create shipping packages for sale order
                         */
                        AllocateInventoryCreatePackageRequest createPackageRequest = new AllocateInventoryCreatePackageRequest();
                        createPackageRequest.setSaleOrderId(saleOrder.getId());
                        saleOrderProcessingService.allocateInventoryAndCreatePackage(createPackageRequest);
                        Set<String> shippingPackageCodes = new HashSet<>(request.getInvoices().size());
                        /*
                         * Check if sale order items are alloted packages and
                         * create invoice
                         */
                        for (SaleOrderItem saleOrderItem : saleOrder.getSaleOrderItems()) {
                            if (saleOrderItem.getShippingPackage() == null) {
                                LOG.error("Package not alloted to " + saleOrderItem.getCode());
                                context.addError(WsResponseCode.INVALID_REQUEST, "Package not alloted to " + saleOrderItem.getCode());
                            } else {
                                shippingPackageCodes.add(saleOrderItem.getShippingPackage().getCode());
                            }
                        }
                        User user = usersService.getUserByUsername(Constants.SYSTEM_USER_EMAIL);
                        for (WsInvoice wsInvoice : request.getInvoices()) {
                            CreateInvoiceWithDetailsRequest createInvoiceWithDetailsRequest = new CreateInvoiceWithDetailsRequest();
                            createInvoiceWithDetailsRequest.setSaleOrderCode(saleOrder.getCode());
                            createInvoiceWithDetailsRequest.setInvoice(wsInvoice);
                            createInvoiceWithDetailsRequest.setUserId(user.getId());
                            LOG.info("Creating invoice for " + wsInvoice.getCode());
                            CreateInvoiceWithDetailsResponse createInvoiceWithDetailsResponse = shippingInvoiceService.createInvoiceWithDetails(createInvoiceWithDetailsRequest);
                            LOG.info("Created invoice for " + wsInvoice.getCode());
                            if (!createInvoiceWithDetailsResponse.isSuccessful()) {
                                LOG.error("Error while creating invoice " + wsInvoice.getCode() + createInvoiceWithDetailsResponse.getErrors().get(0).getDescription());
                                context.addErrors(createInvoiceWithDetailsResponse.getErrors());
                            }
                        }
                        /*
                         * mark packages dispatched
                         */
                        if (!context.hasErrors()) {
                            for (String shippingPackageCode : shippingPackageCodes) {
                                ForceDispatchShippingPackageRequest forceDispatchShippingPackageRequest = new ForceDispatchShippingPackageRequest();
                                forceDispatchShippingPackageRequest.setShippingPackageCode(shippingPackageCode);
                                forceDispatchShippingPackageRequest.setUserId(user.getId());
                                ForceDispatchShippingPackageResponse forceDispatchShippingPackageResponse = dispatchService.forceDispatchShippingPackage(
                                        forceDispatchShippingPackageRequest);
                                if (!forceDispatchShippingPackageResponse.isSuccessful()) {
                                    context.addErrors(forceDispatchShippingPackageResponse.getErrors());
                                }
                            }
                        }
                    } catch (Exception e) {
                        LOG.error("error while adding facility : ", e);
                        context.addError(WsResponseCode.INVALID_REQUEST, "Unable to Configure facility for channel");
                    } finally {
                        if (currentFacility != null) {
                            UserContext.current().setFacility(currentFacility);
                        } else {
                            UserContext.current().setFacility(null);
                        }
                    }
                }
            }
        }
        if (!context.hasErrors()) {
            response.setSuccessful(true);
        } else {
            response.setErrors(context.getErrors());
        }
        if (context.hasWarnings()) {
            response.setWarnings(context.getWarnings());
        }
        return response;
    }

    private void updateFailedSaleOrderIfExists(CreateSaleOrderRequest request, CreateSaleOrderResponse response) {
        SaleOrderVO vo = saleOrderMao.getSaleOrderVOByCode(request.getSaleOrder().getCode());
        if (vo != null) {
            vo.setRequest(request);
            vo.setResponse(response);
            createOrUpdateFailedSaleOrder(vo);
        }
    }

    @Override
    public GetOrderSyncResponse getOrderSyncStatuses(GetOrderSyncRequest request) {
        GetOrderSyncResponse response = new GetOrderSyncResponse();
        ValidationContext context = request.validate();
        List<ChannelOrderSyncDTO> orderSyncDTOs = new ArrayList<>();
        if (!context.hasErrors()) {
            List<ChannelOrderSyncStatusDTO> orderSyncStatuses = channelService.getChannelOrderSyncStatuses();
            for (ChannelOrderSyncStatusDTO orderSyncStatusDTO : orderSyncStatuses) {
                Channel channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelByCode(orderSyncStatusDTO.getChannelCode());
                if (channel != null) {
                    Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(channel.getSourceCode());
                    if (source != null) {
                        ChannelOrderSyncDTO orderSyncDTO = new ChannelOrderSyncDTO(new ChannelSyncDTO(channel, source));
                        orderSyncDTO.setLastOrderSyncResult(orderSyncStatusDTO);
                        orderSyncDTOs.add(orderSyncDTO);
                    }
                }
            }
            response.setLastOrderSyncResults(orderSyncDTOs);
            response.setSuccessful(true);
        }
        response.setErrors(context.getErrors());
        return response;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.uniware.services.saleorder.ISaleOrderService#addSaleOrder(com.uniware
     * .core.entity.SaleOrder)
     */
    @Override
    @Transactional
    public SaleOrder addSaleOrder(SaleOrder saleOrder) {
        return saleOrderDao.addSaleOrder(saleOrder);
    }

    /**
     * @param request
     * @param context
     * @return
     */
    private SaleOrder prepareSaleOrder(CreateSaleOrderRequest request, ValidationContext context) {
        SaleOrder saleOrder = new SaleOrder();
        Map<String, Object> customFieldValues = CustomFieldUtils.getCustomFieldValues(context, SaleOrder.class.getName(), request.getSaleOrder().getCustomFieldValues());
        CustomFieldUtils.setCustomFieldValues(saleOrder, customFieldValues);
        Channel channel = null;
        if (StringUtils.isNotBlank(request.getSaleOrder().getChannel())) {
            channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelByCode(request.getSaleOrder().getChannel());
            if (channel == null) {
                context.addError(WsResponseCode.INVALID_CHANNEL_NAME);
                return null;
            } else {
                saleOrder.setChannel(channel);
            }
        } else {
            saleOrder.setChannel(CacheManager.getInstance().getCache(ChannelCache.class).getChannelByName(com.uniware.core.entity.Source.Code.CUSTOM.name()));
        }

        Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(saleOrder.getChannel().getSourceCode());
        if (source.isDisplayOrderCodeUnique() && request.getSaleOrder().getDisplayOrderCode() != null
                && !getSaleOrderByDisplayOrderCode(request.getSaleOrder().getDisplayOrderCode(), saleOrder.getChannel().getId()).isEmpty()) {
            context.addError(WsResponseCode.DUPLICATE_SALE_ORDER, "Duplicate display order code");
            return null;
        }

        if (source.isDisplayOrderCodeToSaleOrderCodeUnique() && request.getSaleOrder().getDisplayOrderCode() != null
                && getSaleOrderByCode(request.getSaleOrder().getDisplayOrderCode(), saleOrder.getChannel().getId()) != null) {
            context.addError(WsResponseCode.DUPLICATE_SALE_ORDER, "Duplicate sale order " + request.getSaleOrder().getDisplayOrderCode());
            return null;
        }

        if (StringUtils.isNotBlank(request.getSaleOrder().getCustomerCode())) {
            saleOrder.setCustomer(customerService.getCustomerByCode(request.getSaleOrder().getCustomerCode()));
        } else if (saleOrder.getChannel().getCustomer() != null) {
            saleOrder.setCustomer(saleOrder.getChannel().getCustomer());
        }

        if (request.getSaleOrder().getTaxExempted() != null) {
            saleOrder.setTaxExempted(request.getSaleOrder().getTaxExempted());
        } else if (saleOrder.getCustomer() != null) {
            saleOrder.setTaxExempted(saleOrder.getCustomer().isTaxExempted());
        }

        if (request.getSaleOrder().getCformProvided() != null) {
            saleOrder.setCformProvided(request.getSaleOrder().getCformProvided());
        } else if (saleOrder.getCustomer() != null) {
            saleOrder.setCformProvided(saleOrder.getCustomer().isProvidesCform());
        }

        if (saleOrder.getChannel().getReconciliationSyncStatus().equals(SyncStatus.ON)) {
            saleOrder.setReconciliationPending(true);
            saleOrder.setReconciliationStatus(SaleOrder.ReconciliationStatus.AWAITING_PAYMENT.name());
        } else {
            saleOrder.setReconciliationPending(false);
            //saleOrder.setReconciliationStatus(SaleOrder.ReconciliationStatus.IRRECONCILIABLE.name());
        }

        if (request.getSaleOrder().getPriority() != null) {
            saleOrder.setPriority(request.getSaleOrder().getPriority());
        }
        saleOrder.setCreated(DateUtils.getCurrentTime());
        saleOrder.setDisplayOrderDateTime(request.getSaleOrder().getDisplayOrderDateTime() == null ? saleOrder.getCreated() : request.getSaleOrder().getDisplayOrderDateTime());
        if (request.getSaleOrder().getFulfillmentTat() == null && source.isTatAvailableFromChannel()) {
            context.addError(WsResponseCode.MISSING_REQUIRED_FIELD_TAT, "Unable to fetch tat from channel");
        } else if (request.getSaleOrder().getFulfillmentTat() != null) {
            saleOrder.setFulfillmentTat(request.getSaleOrder().getFulfillmentTat());
        } else {
            saleOrder.setFulfillmentTat(
                    DateUtils.addToDate(saleOrder.getDisplayOrderDateTime(), Calendar.HOUR, (null == saleOrder.getChannel().getTat() ? 0 : saleOrder.getChannel().getTat())));
        }

        if (context.hasErrors()) {
            return null;
        }

        Map<String, ItemType> channelProductIdToItemType = validateItemTypeSKUs(request, saleOrder.getChannel(), context);
        if (context.hasErrors()) {
            return null;
        }

        Map<String, AddressDetail> addressMap = new HashMap<String, AddressDetail>();
        for (WsAddressDetail address : request.getSaleOrder().getAddresses()) {
            addressMap.put(address.getId(), prepareAddress(address, context));
        }

        AddressDetail shippingAddress = addressMap.get(request.getSaleOrder().getShippingAddress().getReferenceId());
        shippingAddress = saleOrderDao.addAddressDetail(shippingAddress);
        addressMap.put(request.getSaleOrder().getShippingAddress().getReferenceId(), shippingAddress);
        AddressDetail billingAddress;
        if (request.getSaleOrder().getBillingAddress() != null) {
            billingAddress = addressMap.get(request.getSaleOrder().getBillingAddress().getReferenceId());
            billingAddress = saleOrderDao.addAddressDetail(billingAddress);
            addressMap.put(request.getSaleOrder().getBillingAddress().getReferenceId(), billingAddress);
        } else {
            billingAddress = shippingAddress;
        }
        saleOrder.setBillingAddress(billingAddress);
        saleOrder.setNotificationEmail(request.getSaleOrder().getNotificationEmail());
        saleOrder.setNotificationMobile(request.getSaleOrder().getNotificationMobile());
        saleOrder.setCode(request.getSaleOrder().getCode());
        saleOrder.setProductManagementSwitchedOff(ConfigurationManager.getInstance().getConfiguration(ProductConfiguration.class).isProductManagementSwitchedOff());
        PaymentMethod paymentMethod = null;
        if (request.getSaleOrder().getCashOnDelivery()) {
            paymentMethod = ConfigurationManager.getInstance().getConfiguration(ShippingConfiguration.class).getPaymentMethodByCode(PaymentMethod.Code.COD.name());
        } else {
            paymentMethod = ConfigurationManager.getInstance().getConfiguration(ShippingConfiguration.class).getPaymentMethodByCode(PaymentMethod.Code.PREPAID.name());
        }
        saleOrder.setPaymentMethod(paymentMethod);
        saleOrder.setPaymentInstrument(request.getSaleOrder().getPaymentInstrument());
        if (request.getSaleOrder().getVerificationRequired() != null) {
            saleOrder.setStatusCode(request.getSaleOrder().getVerificationRequired() ? SaleOrder.StatusCode.PENDING_VERIFICATION.name() : SaleOrder.StatusCode.CREATED.name());
        } else {
            saleOrder.setStatusCode(saleOrder.getChannel().isAutoVerifyOrders(saleOrder.getPaymentMethod()) ? SaleOrder.StatusCode.CREATED.name() : SaleOrder.StatusCode.PENDING_VERIFICATION.name());
        }
        if (request.getSaleOrder().getThirdPartyShipping() != null) {
            saleOrder.setThirdPartyShipping(request.getSaleOrder().getThirdPartyShipping());
        } else {
            saleOrder.setThirdPartyShipping(saleOrder.getChannel().isThirdPartyShipping());
        }
        saleOrder.setAdditionalInfo(request.getSaleOrder().getAdditionalInfo());
        saleOrder.setDisplayOrderCode(StringUtils.isEmpty(request.getSaleOrder().getDisplayOrderCode()) ? saleOrder.getCode() : request.getSaleOrder().getDisplayOrderCode());
        if (StringUtils.isNotBlank(request.getSaleOrder().getCurrencyCode())) {
            saleOrder.setCurrencyCode(request.getSaleOrder().getCurrencyCode());
        } else {
            saleOrder.setCurrencyCode(ConfigurationManager.getInstance().getConfiguration(TenantSystemConfiguration.class).getBaseCurrency());
        }
        GetCurrencyConversionRateResponse conversionRateResponse = currencyService.getCurrencyConversionRate(new GetCurrencyConversionRateRequest(saleOrder.getCurrencyCode()));
        if (conversionRateResponse.isSuccessful()) {
            saleOrder.setCurrencyConversionRate(conversionRateResponse.getConversionRate());
        } else {
            context.addErrors(conversionRateResponse.getErrors());
            return null;
        }
        Map<Integer, ShippingProvider> shippingProviders = new HashMap<Integer, ShippingProvider>();
        Map<Integer, String> trackingNumbers = new HashMap<Integer, String>();
        if (request.getSaleOrder().getShippingProviders() != null) {
            for (WsShippingProvider shippingProvider : request.getSaleOrder().getShippingProviders()) {
                shippingProviders.put(shippingProvider.getPacketNumber(),
                        ConfigurationManager.getInstance().getConfiguration(ShippingConfiguration.class).getShippingProviderByCode(shippingProvider.getCode()));
                if (StringUtils.isNotEmpty(shippingProvider.getTrackingNumber())) {
                    trackingNumbers.put(shippingProvider.getPacketNumber(), shippingProvider.getTrackingNumber());
                }
            }
        }
        Map<String, WsSaleOrderItemCombination> combinations = null;
        if (request.getSaleOrder().getSaleOrderItemCombinations() != null) {
            combinations = new HashMap<>();
            for (WsSaleOrderItemCombination combination : request.getSaleOrder().getSaleOrderItemCombinations()) {
                combinations.put(combination.getCombinationIdentifier(), combination);
            }
        }

        // Divide totalDiscount, totalShippingCharges, totalCODCharges
        if (request.getSaleOrder().getTotalShippingCharges() != null || request.getSaleOrder().getTotalDiscount() != null
                || request.getSaleOrder().getTotalCashOnDeliveryCharges() != null || request.getSaleOrder().getTotalGiftWrapCharges() != null) {
            BigDecimal totalSellingPrice = BigDecimal.ZERO;
            for (WsSaleOrderItem wsSaleOrderItem : request.getSaleOrder().getSaleOrderItems()) {
                totalSellingPrice = totalSellingPrice.add(wsSaleOrderItem.getSellingPrice());
            }
            if (NumberUtils.greaterThan(totalSellingPrice, BigDecimal.ZERO)) {
                setOtherChargesAndAdjustRoundingDelta(request, totalSellingPrice);
            } else {
                for (WsSaleOrderItem wsSaleOrderItem : request.getSaleOrder().getSaleOrderItems()) {
                    if (request.getSaleOrder().getTotalShippingCharges() != null) {
                        wsSaleOrderItem.setShippingCharges(NumberUtils.divide(request.getSaleOrder().getTotalShippingCharges(), request.getSaleOrder().getSaleOrderItems().size()));
                    }
                    if (request.getSaleOrder().getTotalCashOnDeliveryCharges() != null) {
                        wsSaleOrderItem.setCashOnDeliveryCharges(
                                NumberUtils.divide(request.getSaleOrder().getTotalCashOnDeliveryCharges(), request.getSaleOrder().getSaleOrderItems().size()));
                    }
                    if (request.getSaleOrder().getTotalGiftWrapCharges() != null) {
                        wsSaleOrderItem.setGiftWrapCharges(NumberUtils.divide(request.getSaleOrder().getTotalGiftWrapCharges(), request.getSaleOrder().getSaleOrderItems().size()));
                    }
                    wsSaleOrderItem.setTotalPrice(wsSaleOrderItem.getSellingPrice().add(wsSaleOrderItem.getShippingCharges()).add(wsSaleOrderItem.getCashOnDeliveryCharges()).add(
                            wsSaleOrderItem.getShippingMethodCharges()).add(wsSaleOrderItem.getGiftWrapCharges()));
                }
            }
        }

        // Divide totalStoreCredit
        if (request.getSaleOrder().getTotalStoreCredit() != null) {
            BigDecimal totalSellingPrice = BigDecimal.ZERO;
            for (WsSaleOrderItem wsSaleOrderItem : request.getSaleOrder().getSaleOrderItems()) {
                totalSellingPrice = totalSellingPrice.add(wsSaleOrderItem.getSellingPrice());
            }
            if (NumberUtils.greaterThan(totalSellingPrice, BigDecimal.ZERO)) {
                for (WsSaleOrderItem wsSaleOrderItem : request.getSaleOrder().getSaleOrderItems()) {
                    BigDecimal storeCredit = wsSaleOrderItem.getSellingPrice().multiply(request.getSaleOrder().getTotalStoreCredit()).divide(totalSellingPrice, 2,
                            RoundingMode.HALF_UP);
                    if (NumberUtils.greaterThan(storeCredit, wsSaleOrderItem.getSellingPrice())) {
                        wsSaleOrderItem.setStoreCredit(wsSaleOrderItem.getSellingPrice());
                    } else {
                        wsSaleOrderItem.setStoreCredit(storeCredit);
                    }
                    wsSaleOrderItem.setSellingPrice(wsSaleOrderItem.getSellingPrice().subtract(wsSaleOrderItem.getStoreCredit()));
                    wsSaleOrderItem.setTotalPrice(wsSaleOrderItem.getTotalPrice().subtract(wsSaleOrderItem.getStoreCredit()));
                }
            }
        }

        // Divide Total Prepaid Amount
        if (request.getSaleOrder().getTotalPrepaidAmount() != null) {
            BigDecimal totalPrice = BigDecimal.ZERO;
            for (WsSaleOrderItem wsSaleOrderItem : request.getSaleOrder().getSaleOrderItems()) {
                totalPrice = totalPrice.add(wsSaleOrderItem.getTotalPrice());
            }
            if (NumberUtils.greaterThan(totalPrice, BigDecimal.ZERO)) {
                for (WsSaleOrderItem wsSaleOrderItem : request.getSaleOrder().getSaleOrderItems()) {
                    BigDecimal prepaidAmount = wsSaleOrderItem.getTotalPrice().multiply(request.getSaleOrder().getTotalPrepaidAmount()).divide(totalPrice, 2, RoundingMode.HALF_UP);
                    if (NumberUtils.greaterThan(prepaidAmount, wsSaleOrderItem.getTotalPrice())) {
                        wsSaleOrderItem.setPrepaidAmount(wsSaleOrderItem.getTotalPrice());
                    } else {
                        wsSaleOrderItem.setPrepaidAmount(prepaidAmount);
                    }
                    if (source.isFulfillmentByChannel()) {
                        wsSaleOrderItem.setFacilityCode(channel.getCode());
                    }
                }
            }
        }

        // Prepare saleOrderItems
        if (!context.hasErrors()) {
            for (WsSaleOrderItem wsSaleOrderItem : request.getSaleOrder().getSaleOrderItems()) {
                List<SaleOrderItem> saleOrderItems = prepareSaleOrderItems(wsSaleOrderItem, saleOrder, shippingAddress, addressMap, shippingProviders, trackingNumbers,
                        combinations, channelProductIdToItemType, context);
                if (context.hasErrors()) {
                    return null;
                } else {
                    saleOrder.getSaleOrderItems().addAll(saleOrderItems);
                }
            }
        }

        if (!context.hasErrors()) {
            if (StringUtils.isNotBlank(request.getSaleOrder().getCustomerName())) {
                saleOrder.setCustomerName(request.getSaleOrder().getCustomerName());
            } else {
                saleOrder.setCustomerName(saleOrder.getBillingAddress().getName());
            }
        }
        return saleOrder;
    }

    /**
     * @param request
     * @param totalSellingPrice
     */
    private void setOtherChargesAndAdjustRoundingDelta(CreateSaleOrderRequest request, BigDecimal totalSellingPrice) {
        BigDecimal totalDiscountAmountAfterRounding = BigDecimal.ZERO;
        BigDecimal totalShippingChargesAfterRounding = BigDecimal.ZERO;
        BigDecimal totalGiftWrapChargesAfterRounding = BigDecimal.ZERO;
        BigDecimal totalCashOnDeliveryChargesAfterRounding = BigDecimal.ZERO;
        for (WsSaleOrderItem wsSaleOrderItem : request.getSaleOrder().getSaleOrderItems()) {
            if (request.getSaleOrder().getTotalShippingCharges() != null) {
                BigDecimal shippingChargesOnSaleOrderItem = wsSaleOrderItem.getSellingPrice().multiply(request.getSaleOrder().getTotalShippingCharges()).divide(totalSellingPrice,
                        2, RoundingMode.HALF_UP);
                wsSaleOrderItem.setShippingCharges(shippingChargesOnSaleOrderItem);
                totalShippingChargesAfterRounding = totalShippingChargesAfterRounding.add(shippingChargesOnSaleOrderItem);
            }

            if (request.getSaleOrder().getTotalCashOnDeliveryCharges() != null) {
                BigDecimal cashOnDeliveryChargesOnSaleOrderItem = wsSaleOrderItem.getSellingPrice().multiply(request.getSaleOrder().getTotalCashOnDeliveryCharges()).divide(
                        totalSellingPrice, 2, RoundingMode.HALF_UP);
                wsSaleOrderItem.setCashOnDeliveryCharges(cashOnDeliveryChargesOnSaleOrderItem);
                totalCashOnDeliveryChargesAfterRounding = totalCashOnDeliveryChargesAfterRounding.add(cashOnDeliveryChargesOnSaleOrderItem);
            }
            if (request.getSaleOrder().getTotalGiftWrapCharges() != null) {
                BigDecimal giftWrapChargesOnSaleOrderItem = wsSaleOrderItem.getSellingPrice().multiply(request.getSaleOrder().getTotalGiftWrapCharges()).divide(totalSellingPrice,
                        2, RoundingMode.HALF_UP);
                wsSaleOrderItem.setGiftWrapCharges(giftWrapChargesOnSaleOrderItem);
                totalGiftWrapChargesAfterRounding = totalGiftWrapChargesAfterRounding.add(giftWrapChargesOnSaleOrderItem);
            }
            if (request.getSaleOrder().getTotalDiscount() != null) {
                BigDecimal discountAmount = wsSaleOrderItem.getSellingPrice().multiply(request.getSaleOrder().getTotalDiscount()).divide(totalSellingPrice, 2,
                        RoundingMode.HALF_UP);
                totalDiscountAmountAfterRounding = totalDiscountAmountAfterRounding.add(discountAmount);
                if (NumberUtils.greaterThan(discountAmount, wsSaleOrderItem.getSellingPrice())) {
                    wsSaleOrderItem.setDiscount(wsSaleOrderItem.getSellingPrice());
                } else {
                    wsSaleOrderItem.setDiscount(discountAmount);
                }
                wsSaleOrderItem.setSellingPrice(wsSaleOrderItem.getSellingPrice().subtract(wsSaleOrderItem.getDiscount()));
            }
            wsSaleOrderItem.setTotalPrice(wsSaleOrderItem.getSellingPrice().add(wsSaleOrderItem.getShippingCharges()).add(wsSaleOrderItem.getCashOnDeliveryCharges()).add(
                    wsSaleOrderItem.getShippingMethodCharges()).add(wsSaleOrderItem.getGiftWrapCharges()));
        }
        // Get rounding deltas for different charges.
        BigDecimal shippingChargesRoundingDelta = BigDecimal.ZERO;
        BigDecimal cashOnDeliveryChargesRoundingDelta = BigDecimal.ZERO;
        BigDecimal giftWrapChargesRoundingDelta = BigDecimal.ZERO;
        BigDecimal discountRoundingDelta = BigDecimal.ZERO;
        if (request.getSaleOrder().getTotalShippingCharges() != null) {
            shippingChargesRoundingDelta = request.getSaleOrder().getTotalShippingCharges().subtract(totalShippingChargesAfterRounding);
        }
        if (request.getSaleOrder().getTotalCashOnDeliveryCharges() != null) {
            cashOnDeliveryChargesRoundingDelta = request.getSaleOrder().getTotalCashOnDeliveryCharges().subtract(totalCashOnDeliveryChargesAfterRounding);
        }
        if (request.getSaleOrder().getTotalGiftWrapCharges() != null) {
            giftWrapChargesRoundingDelta = request.getSaleOrder().getTotalGiftWrapCharges().subtract(totalGiftWrapChargesAfterRounding);
        }
        if (request.getSaleOrder().getTotalDiscount() != null) {
            discountRoundingDelta = request.getSaleOrder().getTotalDiscount().subtract(totalDiscountAmountAfterRounding);
        }
        adjustRoundingDeltas(request.getSaleOrder(), shippingChargesRoundingDelta, cashOnDeliveryChargesRoundingDelta, giftWrapChargesRoundingDelta, discountRoundingDelta);
    }

    /**
     * This method is used to correct deltas that accumulate due to rounding errors. This is done by iterating through
     * all {@code saleOrderItems}, and adding the minimum between the delta and its corresponding charge. Actually, this
     * is only needed while setting selling price, cause that is the only charge that can go in negative due to the fact
     * that <b>discount may be subtracted</b> from it. And that too, happens in extreme cases.
     * <p>
     * For example, say a person is selling 5 items with mrp of 1000 Rs each. Suppose he offers a total discount of
     * 4999.90 This implies that actual selling price is (5*1000 - 4999.90) = 0.10 Rs, i.e., 10 paise. <br>
     * Now, suppose the total {@code discountRoundingDelta} is 5 paise, i.e., corresponding to 4999.85. This depicts a
     * 15 paise discount, which must be changed to 10 paise. At this stage, each sale order item is having a selling
     * price of 4 paise.<br>
     * <table border=1>
     * <tr>
     * <td><b>Item id</b></td>
     * <td><b>Current price</b>(which is wrong, it should be 2 paise per item)</td>
     * </tr>
     * <tr>
     * <td>1</td>
     * <td>3</td>
     * </tr>
     * <tr>
     * <td>2</td>
     * <td>3</td>
     * </tr>
     * <tr>
     * <td>3</td>
     * <td>3</td>
     * </tr>
     * <tr>
     * <td>4</td>
     * <td>3</td>
     * </tr>
     * <tr>
     * <td>5</td>
     * <td>3</td>
     * </tr>
     * </table>
     * To adjust the 5 paise discount from only one item, it will make that item's selling price go to negative, and
     * will throw an error. Instead, a better approach is to iterate through all items, and subtract whatever rounding
     * delta can be subtracted, which will be the minimum of its selling price and {@code discountRoundingDelta}. This
     * way,
     * <table border=1>
     * <tr>
     * <td><b>Item id</b></td>
     * <td><b>Correct price</b></td>
     * </tr>
     * <tr>
     * <td>1</td>
     * <td>0 (subtracted minimimum of 3 and 5, afterwards, discountRoundingDelta is 2)</td>
     * </tr>
     * <tr>
     * <td>2</td>
     * <td>1 (subtracted minimimum of 3 and 2, afterwards, discountRoundingDelta is 0)</td>
     * </tr>
     * <tr>
     * <td>3</td>
     * <td>3</td>
     * </tr>
     * <tr>
     * <td>4</td>
     * <td>3</td>
     * </tr>
     * <tr>
     * <td>5</td>
     * <td>3</td>
     * </tr>
     * </table>
     * </p>
     *
     * @param wsSaleOrder {@link WsSaleOrder}
     * @param shippingChargesRoundingDelta wsSaleOrder.TotalShippingCharges - totalShippingChargesAfterRounding (Actual
     *            - rounding ones)
     * @param cashOnDeliveryChargesRoundingDelta wsSaleOrder.TotalCashOnDeliveryCharges -
     *            totalCashOnDeliveryChargesAfterRounding (Actual - rounding ones)
     * @param giftWrapChargesRoundingDelta wsSaleOrder.TotalGiftWrapCharges - totalGiftWrapChargesAfterRounding (Actual
     *            - rounding ones)
     * @param discountRoundingDelta wsSaleOrder.TotalDiscount - totalDiscountAmountAfterRounding (Actual - rounding
     *            ones)
     */
    private void adjustRoundingDeltas(WsSaleOrder wsSaleOrder, BigDecimal shippingChargesRoundingDelta, BigDecimal cashOnDeliveryChargesRoundingDelta,
            BigDecimal giftWrapChargesRoundingDelta, BigDecimal discountRoundingDelta) {
        List<WsSaleOrderItem> wsSaleOrderItemList = wsSaleOrder.getSaleOrderItems();
        wsSaleOrderItemList.sort(Comparator.comparing(WsSaleOrderItem::getSellingPrice));
        for (WsSaleOrderItem wsSaleOrderItem : wsSaleOrderItemList) {

            if (wsSaleOrder.getTotalShippingCharges() != null && !BigDecimal.ZERO.equals(shippingChargesRoundingDelta)) {
                BigDecimal minimumDelta = wsSaleOrderItem.getShippingCharges().min(shippingChargesRoundingDelta);
                wsSaleOrderItem.setShippingCharges(wsSaleOrderItem.getShippingCharges().add(minimumDelta));
                wsSaleOrderItem.setTotalPrice(wsSaleOrderItem.getTotalPrice().add(minimumDelta));
                shippingChargesRoundingDelta = shippingChargesRoundingDelta.subtract(minimumDelta);
            }
            if (wsSaleOrder.getTotalCashOnDeliveryCharges() != null && !BigDecimal.ZERO.equals(cashOnDeliveryChargesRoundingDelta)) {
                BigDecimal minimumDelta = wsSaleOrderItem.getCashOnDeliveryCharges().min(cashOnDeliveryChargesRoundingDelta);
                wsSaleOrderItem.setShippingCharges(wsSaleOrderItem.getCashOnDeliveryCharges().add(minimumDelta));
                wsSaleOrderItem.setTotalPrice(wsSaleOrderItem.getTotalPrice().add(minimumDelta));
                cashOnDeliveryChargesRoundingDelta = cashOnDeliveryChargesRoundingDelta.subtract(minimumDelta);
            }
            if (wsSaleOrder.getTotalGiftWrapCharges() != null && !BigDecimal.ZERO.equals(giftWrapChargesRoundingDelta)) {
                BigDecimal minimumDelta = wsSaleOrderItem.getGiftWrapCharges().min(giftWrapChargesRoundingDelta);
                wsSaleOrderItem.setGiftWrapCharges(wsSaleOrderItem.getGiftWrapCharges().add(minimumDelta));
                wsSaleOrderItem.setTotalPrice(wsSaleOrderItem.getTotalPrice().add(minimumDelta));
                giftWrapChargesRoundingDelta = giftWrapChargesRoundingDelta.subtract(minimumDelta);
            }
            if (wsSaleOrder.getTotalDiscount() != null && !BigDecimal.ZERO.equals(discountRoundingDelta)) {
                BigDecimal minimumDelta = wsSaleOrderItem.getSellingPrice().min(discountRoundingDelta);
                wsSaleOrderItem.setSellingPrice(wsSaleOrderItem.getSellingPrice().subtract(minimumDelta));
                wsSaleOrderItem.setDiscount(wsSaleOrderItem.getDiscount().add(minimumDelta));
                wsSaleOrderItem.setTotalPrice(wsSaleOrderItem.getTotalPrice().subtract(minimumDelta));
                discountRoundingDelta = discountRoundingDelta.subtract(minimumDelta);
            }
            if (shippingChargesRoundingDelta.equals(BigDecimal.ZERO) && cashOnDeliveryChargesRoundingDelta.equals(BigDecimal.ZERO)
                    && giftWrapChargesRoundingDelta.equals(BigDecimal.ZERO) && discountRoundingDelta.equals(BigDecimal.ZERO)) {
                break;
            }

        }
    }

    private Map<String, ItemType> validateItemTypeSKUs(CreateSaleOrderRequest request, Channel channel, ValidationContext context) {
        Map<String, ItemType> channelProductIdToItemTypes = new HashMap<>();
        boolean productManagementSwitchedOff = ConfigurationManager.getInstance().getConfiguration(ProductConfiguration.class).isProductManagementSwitchedOff();
        ItemType unknownItemType = catalogService.getItemTypeBySkuCode(ItemType.UNKNOWN_SKU);
        for (WsSaleOrderItem wsSaleOrderItem : request.getSaleOrder().getSaleOrderItems()) {
            if (productManagementSwitchedOff) {
                // Every channelProductId is mapped to a single UNKNOWN sku.
                channelProductIdToItemTypes.put(wsSaleOrderItem.getChannelProductId(), unknownItemType);
            } else if ("CUSTOM".equals(channel.getSourceCode()) && StringUtils.isBlank(wsSaleOrderItem.getChannelProductId())) {
                ItemType itemType = catalogService.getAllEnabledItemTypeBySkuCode(wsSaleOrderItem.getItemSku());
                if (itemType == null) {
                    context.addError(WsResponseCode.INVALID_ITEM_TYPE,
                            "invalid item sku at saleOrder.saleOrderItems[" + wsSaleOrderItem.getCode() + "].itemSku " + wsSaleOrderItem.getItemSku());
                } else {
                    // For custom channels default channelProductId to itemSKU
                    // if not provided.
                    wsSaleOrderItem.setChannelProductId(wsSaleOrderItem.getItemSku());
                    channelProductIdToItemTypes.put(wsSaleOrderItem.getChannelProductId(), itemType);
                }
            }

            if (!channelProductIdToItemTypes.containsKey(wsSaleOrderItem.getChannelProductId())) {
                ItemType itemType = null;
                ChannelItemType channelItemType = null;
                if (StringUtils.isBlank(wsSaleOrderItem.getChannelProductId())) {
                    context.addError(WsResponseCode.INVALID_REQUEST, "channel product id not found at saleOrder.saleOrderItems[" + wsSaleOrderItem.getCode() + "]");
                } else {
                    channelItemType = channelCatalogService.lookupChannelItemType(channel.getCode(), wsSaleOrderItem.getChannelProductId(), wsSaleOrderItem.getItemSku(),
                            wsSaleOrderItem.getItemName());
                    if (isChannelItemTypeValidForSaleOrderCreation(channelItemType, request.getSaleOrder().isUseVerifiedListings())) {
                        itemType = channelItemType.getItemType();
                    } else {
                        ChannelItemTypeDTO channelItemTypeDTO = new ChannelItemTypeDTO(wsSaleOrderItem.getChannelProductId(), wsSaleOrderItem.getItemSku(),
                                wsSaleOrderItem.getItemName());
                        Map<String, Object> errorParams = new HashMap<String, Object>();
                        errorParams.put("channelItemType", channelItemTypeDTO);
                        if (channelItemType == null) {
                            channelItemTypeDTO.setStatusCode(Status.MISSING);
                            context.addError(WsResponseCode.SKU_NOT_MAPPED, "missing channelProductId at saleOrder.saleOrderItems[" + wsSaleOrderItem.getCode()
                                    + "].channelProductId " + wsSaleOrderItem.getChannelProductId(), errorParams);
                        } else if (ChannelItemType.Status.IGNORED.equals(channelItemType.getStatusCode())) {
                            channelItemTypeDTO.setStatusCode(Status.IGNORED);
                            context.addError(WsResponseCode.SKU_NOT_MAPPED, "channelProductId marked ignored at saleOrder.saleOrderItems[" + wsSaleOrderItem.getCode()
                                    + "].channelProductId " + wsSaleOrderItem.getChannelProductId(), errorParams);
                        } else if (ChannelItemType.Status.UNLINKED.equals(channelItemType.getStatusCode())) {
                            channelItemTypeDTO.setStatusCode(Status.UNLINKED);
                            context.addError(WsResponseCode.SKU_NOT_MAPPED, "unmapped channelProductId at saleOrder.saleOrderItems[" + wsSaleOrderItem.getCode()
                                    + "].channelProductId " + wsSaleOrderItem.getChannelProductId(), errorParams);
                        } else if (!channelItemType.isVerified()) {
                            channelItemTypeDTO.setStatusCode(Status.UNVERIFIED);
                            context.addError(WsResponseCode.SKU_NOT_MAPPED, "unverified channelProductId at saleOrder.saleOrderItems[" + wsSaleOrderItem.getCode()
                                    + "].channelProductId " + wsSaleOrderItem.getChannelProductId(), errorParams);
                        }
                    }
                }
                channelProductIdToItemTypes.put(wsSaleOrderItem.getChannelProductId(), itemType);
            }
        }
        return channelProductIdToItemTypes;
    }

    @Override
    public boolean isChannelItemTypeValidForSaleOrderCreation(ChannelItemType channelItemType, boolean useVerifiedListings) {
        return channelItemType != null && ChannelItemType.Status.LINKED.equals(channelItemType.getStatusCode()) && (channelItemType.isVerified() || !useVerifiedListings);
    }

    /*
     * See if the item type in saleOrderItem is of type bundle, if yes, create
     * saleOrderItem corresponding to each quantity of bundle item type
     */
    private List<SaleOrderItem> prepareSaleOrderItems(WsSaleOrderItem wsSaleOrderItem, SaleOrder saleOrder, AddressDetail shippingAddress, Map<String, AddressDetail> addressMap,
            Map<Integer, ShippingProvider> shippingProviders, Map<Integer, String> trackingNumbers, Map<String, WsSaleOrderItemCombination> combinations,
            Map<String, ItemType> channelProductIdToItemTypes, ValidationContext context) {
        List<SaleOrderItem> saleOrderItems = new ArrayList<SaleOrderItem>();
        ShippingMethod shippingMethod = null;
        if (!context.hasErrors()) {
            shippingMethod = shippingService.getShippingMethodByCode(wsSaleOrderItem.getShippingMethodCode(), saleOrder.isCashOnDelivery());
            if (shippingMethod == null) {
                context.addError(WsResponseCode.INVALID_SHIPPING_METHOD,
                        "invalid shipping method at saleOrder.saleOrderItems[" + wsSaleOrderItem.getCode() + "].shippingMethodCode");
            }
        }

        if (!context.hasErrors()) {
            Map<String, Object> customFieldValues = CustomFieldUtils.getCustomFieldValues(context, SaleOrderItem.class.getName(), wsSaleOrderItem.getCustomFieldValues());
            ItemType itemType = channelProductIdToItemTypes.get(wsSaleOrderItem.getChannelProductId());
            BigDecimal transferPrice = null;
            wsSaleOrderItem.setTransferPrice(transferPrice);
            if (!ItemType.Type.BUNDLE.equals(itemType.getType())) {
                WsSaleOrderItemPricing pricing = new WsSaleOrderItemPricing(wsSaleOrderItem);
                SaleOrderItem saleOrderItem = prepareSaleOrderItem(wsSaleOrderItem.getCode(), wsSaleOrderItem, saleOrder, shippingAddress, addressMap, shippingProviders,
                        trackingNumbers, combinations, shippingMethod, customFieldValues, itemType, pricing, context);
                if (!context.hasErrors()) {
                    saleOrderItems.add(saleOrderItem);
                }
            } else {
                Bundle bundle = bundleService.getBundleByCode(itemType.getSkuCode());
                if (bundle == null) {
                    context.addError(WsResponseCode.INVALID_BUNDLE_ITEM_TYPE);
                } else {
                    wsSaleOrderItem.setCombinationIdentifier(wsSaleOrderItem.getCode());
                    WsSaleOrderItemCombination wsSaleOrderItemCombination = new WsSaleOrderItemCombination(wsSaleOrderItem.getCode(),
                            StringUtils.isNotBlank(wsSaleOrderItem.getItemName()) ? wsSaleOrderItem.getItemName() : itemType.getName());
                    if (combinations == null) {
                        combinations = new HashMap<>();
                    }
                    combinations.put(wsSaleOrderItemCombination.getCombinationIdentifier(), wsSaleOrderItemCombination);
                    List<BundleItemType> bundleItemTypes = new ArrayList<>();
                    for (BundleItemType bundleItemType : bundle.getBundleItemTypes()) {
                        for (int i = 0; i < bundleItemType.getQuantity(); i++) {
                            bundleItemTypes.add(bundleItemType);
                        }
                    }
                    WsSaleOrderItemPricing saleOrderItemPricing = new WsSaleOrderItemPricing(wsSaleOrderItem, bundleItemTypes);
                    while (saleOrderItemPricing.hasNext()) {
                        WsSaleOrderItemPricing pricing = saleOrderItemPricing.next();
                        SaleOrderItem saleOrderItem = prepareSaleOrderItem(pricing.getCode(), wsSaleOrderItem, saleOrder, shippingAddress, addressMap, shippingProviders,
                                trackingNumbers, combinations, shippingMethod, customFieldValues, pricing.getBundleItemType().getItemType(), pricing, context);
                        saleOrderItem.setBundleSkuCode(bundle.getItemType().getSkuCode());
                        if (!context.hasErrors()) {
                            saleOrderItems.add(saleOrderItem);
                        }
                    }
                }
            }
        }
        return saleOrderItems;
    }

    private SaleOrderItem prepareSaleOrderItem(String code, WsSaleOrderItem wsSaleOrderItem, SaleOrder saleOrder, AddressDetail shippingAddress,
            Map<String, AddressDetail> addressMap, Map<Integer, ShippingProvider> shippingProviders, Map<Integer, String> trackingNumbers,
            Map<String, WsSaleOrderItemCombination> combinations, ShippingMethod shippingMethod, Map<String, Object> customFieldValues, ItemType itemType,
            WsSaleOrderItemPricing pricing, ValidationContext context) {
        SaleOrderItem saleOrderItem = new SaleOrderItem();
        CustomFieldUtils.setCustomFieldValues(saleOrderItem, customFieldValues);
        saleOrderItem.setCode(code);
        saleOrderItem.setChannelSaleOrderItemCode(
                wsSaleOrderItem.getChannelSaleOrderItemCode() != null ? wsSaleOrderItem.getChannelSaleOrderItemCode() : wsSaleOrderItem.getCode());
        saleOrderItem.setChannelProductName(wsSaleOrderItem.getItemName() != null ? wsSaleOrderItem.getItemName() : itemType.getName());
        saleOrderItem.setGiftWrap(wsSaleOrderItem.isGiftWrap());
        saleOrderItem.setGiftMessage(wsSaleOrderItem.getGiftMessage());
        saleOrderItem.setVoucherCode(wsSaleOrderItem.getVoucherCode());

        saleOrderItem.setItemType(itemType);
        Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(saleOrder.getChannel().getSourceCode());
        Set<String> itemDetailFieldNames = CacheManager.getInstance().getCache(ItemTypeDetailCache.class).getItemDetailFieldNames(itemType, wsSaleOrderItem.getItemDetailFields());
        if (itemDetailFieldNames != null && !source.isFetchCompleteOrders()) {
            saleOrderItem.setItemDetailFields(StringUtils.join(itemDetailFieldNames));
            saleOrderItem.setItemDetailingStatus(ItemDetailingStatus.PENDING);
        } else {
            saleOrderItem.setItemDetailingStatus(ItemDetailingStatus.NOT_REQUIRED);
        }
        saleOrderItem.setChannelProductId(StringUtils.isNotBlank(wsSaleOrderItem.getChannelProductId()) ? wsSaleOrderItem.getChannelProductId() : wsSaleOrderItem.getItemSku());
        saleOrderItem.setSellerSkuCode(wsSaleOrderItem.getItemSku());
        saleOrderItem.setPacketNumber(wsSaleOrderItem.getPacketNumber());
        saleOrderItem.setSaleOrder(saleOrder);

        // Set reconciliation fields if present.
        ChannelItemType channelItemType = channelCatalogService.getChannelItemTypeByChannelAndChannelProductId(saleOrder.getChannel().getId(), saleOrderItem.getChannelProductId());
        if (channelItemType != null) {
            saleOrderItem.setCommissionPercentage(channelItemType.getCommissionPercentage());
            saleOrderItem.setPaymentGatewayCharge(channelItemType.getPaymentGatewayCharge());
            saleOrderItem.setLogisticsCost(channelItemType.getLogisticsCost());
        }

        // Pricing fields
        saleOrderItem.setCostPrice(itemType.getCostPrice());
        saleOrderItem.setSellingPrice(pricing.getSellingPrice());
        saleOrderItem.setChannelTransferPrice(pricing.getChannelTransferPrice());
        saleOrderItem.setDiscount(pricing.getDiscount());
        saleOrderItem.setTransferPrice(pricing.getTransferPrice());
        saleOrderItem.setShippingCharges(pricing.getShippingCharges());
        saleOrderItem.setShippingMethodCharges(pricing.getShippingMethodCharges());
        saleOrderItem.setTotalPrice(pricing.getTotalPrice());
        saleOrderItem.setVoucherValue(pricing.getVoucherValue());
        saleOrderItem.setPrepaidAmount(pricing.getPrepaidAmount());
        saleOrderItem.setStoreCredit(pricing.getStoreCredit());
        saleOrderItem.setGiftWrapCharges(pricing.getGiftWrapCharges());
        saleOrderItem.setCashOnDeliveryCharges(pricing.getCashOnDeliveryCharges());

        saleOrderItem.setOnHold(wsSaleOrderItem.isOnHold());
        if (wsSaleOrderItem.getShippingAddress() != null) {
            shippingAddress = addressMap.get(wsSaleOrderItem.getShippingAddress().getReferenceId());
            shippingAddress = saleOrderDao.addAddressDetail(shippingAddress);
            addressMap.put(wsSaleOrderItem.getShippingAddress().getReferenceId(), shippingAddress);
        }
        saleOrderItem.setShippingAddress(shippingAddress);
        saleOrderItem.setShippingMethod(shippingMethod);
        saleOrderItem.setRequiresCustomization(ValidatorUtils.getOrDefaultValue(wsSaleOrderItem.getRequiresCustomization(), false));
        if (shippingProviders.containsKey(saleOrderItem.getPacketNumber())) {
            ShippingProvider shippingProvider = shippingProviders.get(saleOrderItem.getPacketNumber());
            saleOrderItem.setShippingProvider(shippingProvider);
        } else if (!saleOrder.getChannel().isThirdPartyShipping()
                && ConfigurationManager.getInstance().getConfiguration(TenantSystemConfiguration.class).isRejectSaleOrderIfNonServiceable()) {
            if (shippingProviderService.getServiceableByFacilities(saleOrderItem.getShippingMethod().getId(), shippingAddress.getPincode()).size() == 0) {
                context.addError(WsResponseCode.LOCATION_NOT_SERVICEABLE_WITH_GIVEN_PROVIDER_METHOD, "No Available Shipping providers at given location");
                return null;
            }
        }
        if (StringUtils.isNotBlank(wsSaleOrderItem.getCombinationIdentifier())) {
            WsSaleOrderItemCombination combination = combinations.get(wsSaleOrderItem.getCombinationIdentifier());
            saleOrderItem.setCombinationIdentifier(combination.getCombinationIdentifier());
            saleOrderItem.setCombinationDescription(combination.getCombinationDescription());
        }
        FacilityCache cache = CacheManager.getInstance().getCache(FacilityCache.class);
        if (StringUtils.isNotBlank(wsSaleOrderItem.getFacilityCode())) {
            Facility facility = cache.getFacilityByCode(wsSaleOrderItem.getFacilityCode());
            if (facility == null) {
                context.addError(WsResponseCode.INVALID_FACILITY_CODE, "Invalid faciity code");
                return null;
            } else {
                saleOrderItem.setFacility(facility);
            }
        } else if (saleOrder.getChannel().getAssociatedFacility() != null) {
            Facility facility = cache.getFacilityByCode(saleOrder.getChannel().getAssociatedFacility().getCode());
            if (facility == null) {
                context.addError(WsResponseCode.INVALID_FACILITY_CODE, "Invalid faciity code");
                return null;
            } else {
                saleOrderItem.setFacility(facility);
            }
        } else if (cache.getFacilities().size() == 1 && Facility.Type.WAREHOUSE.name().equals(cache.getFacilities().get(0).getType())) {
            saleOrderItem.setFacility(cache.getFacilities().get(0));
        }
        if (trackingNumbers.containsKey(saleOrderItem.getPacketNumber())) {
            saleOrderItem.setTrackingNumber(trackingNumbers.get(saleOrderItem.getPacketNumber()));
        }
        saleOrderItem.setStatusCode(SaleOrderItem.StatusCode.CREATED.name());
        Date currentTime = DateUtils.getCurrentTime();
        saleOrderItem.setUpdated(currentTime);
        saleOrderItem.setCreated(currentTime);
        return saleOrderItem;
    }

    /**
     * @param address
     * @param context
     * @return
     */
    private AddressDetail prepareAddress(WsAddressDetail address, ValidationContext context) {
        return new AddressDetail(address.getName(), address.getAddressLine1(), address.getAddressLine2(), address.getCity(), address.getState(), address.getCountry(),
                address.getPincode(), address.getPhone(), address.getEmail(), DateUtils.getCurrentTime(), DateUtils.getCurrentTime());
    }

    /**
     * @param request
     * @return
     */
    private ValidationContext validateRequest(CreateSaleOrderRequest request) {
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            if (StringUtils.isBlank(request.getSaleOrder().getCode())) {
                context.addError(WsResponseCode.INVALID_SALE_ORDER_CODE, "Sale order code cannot be empty");
            }
            if (request.getSaleOrder().getSaleOrderItems() == null || request.getSaleOrder().getSaleOrderItems().isEmpty()) {
                context.addError(WsResponseCode.MISSING_SALE_ORDER_ITEMS, "Sale order items required to create a valid order.");
            }
            if (StringUtils.isNotBlank(request.getSaleOrder().getChannel())) {
                Channel channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelByName(request.getSaleOrder().getChannel());
                if (channel == null) {
                    context.addError(WsResponseCode.INVALID_CHANNEL_NAME);
                }
            } else {
                request.getSaleOrder().setChannel(com.uniware.core.entity.Source.Code.CUSTOM.name());
            }
        }
        if (!context.hasErrors()) {
            if (request.getSaleOrder().getAddresses() != null && request.getSaleOrder().getAddresses().size() > 0 && request.getSaleOrder().getShippingAddress() != null) {
                Map<String, WsAddressDetail> addresses = new HashMap<String, WsAddressDetail>();
                int index = 0;
                Channel channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelByName(request.getSaleOrder().getChannel());
                for (WsAddressDetail address : request.getSaleOrder().getAddresses()) {
                    validateAndNormalizeAddress(context, address, channel.isThirdPartyShipping(), index++);
                    addresses.put(address.getId(), address);
                }
                if (!addresses.containsKey(request.getSaleOrder().getShippingAddress().getReferenceId())) {
                    context.addError(WsResponseCode.INVALID_ADDRESS_REFERENCE,
                            "invalid address reference at saleOrder.shippingAddress[@ref=" + request.getSaleOrder().getShippingAddress().getReferenceId() + "]");
                }

                if (request.getSaleOrder().getBillingAddress() != null && !addresses.containsKey(request.getSaleOrder().getBillingAddress().getReferenceId())) {
                    context.addError(WsResponseCode.INVALID_ADDRESS_REFERENCE,
                            "invalid address reference at saleOrder.billingAddress[@ref=" + request.getSaleOrder().getBillingAddress().getReferenceId() + "]");
                }

                for (WsSaleOrderItem saleOrderItem : request.getSaleOrder().getSaleOrderItems()) {
                    if (saleOrderItem.getShippingAddress() != null && !addresses.containsKey(saleOrderItem.getShippingAddress().getReferenceId())) {
                        context.addError(WsResponseCode.INVALID_ADDRESS_REFERENCE, "invalid address reference at saleOrder.saleOrderItems[" + saleOrderItem.getCode()
                                + "].shippingAddress[@ref=" + saleOrderItem.getShippingAddress().getReferenceId() + "]");
                    }
                }
            } else {
                if (StringUtils.isNotBlank(request.getSaleOrder().getCustomerCode())) {
                    Customer customer = customerService.getCustomerByCode(request.getSaleOrder().getCustomerCode());
                    if (customer == null) {
                        context.addError(WsResponseCode.INVALID_CUSTOMER_CODE);
                    } else if (customer.getPartyAddressByType(PartyAddressType.Code.SHIPPING.name()) == null
                            || customer.getPartyAddressByType(PartyAddressType.Code.BILLING.name()) == null) {
                        context.addError(WsResponseCode.INVALID_CUSTOMER_CODE, "Customer does not have Billing/Shipping address");
                    } else {
                        addCustomerAddressToSaleOrder(request, customer);
                    }
                } else {
                    context.addError(ResponseCode.MISSING_REQUIRED_PARAMETERS, "either of CustomerCode or ShippingAddress is required");
                }
            }

            Set<String> saleOrderItemCodes = new HashSet<String>();
            for (WsSaleOrderItem saleOrderItem : request.getSaleOrder().getSaleOrderItems()) {
                if (saleOrderItemCodes.contains(saleOrderItem.getCode())) {
                    context.addError(WsResponseCode.DUPLICATE_SALE_ORDER_ITEM, "duplicate sale order item code:" + saleOrderItem.getCode());
                } else {
                    saleOrderItemCodes.add(saleOrderItem.getCode());
                }
            }

            if (StringUtils.isNotBlank(request.getSaleOrder().getCurrencyCode())) {
                Currency currency = ConfigurationManager.getInstance().getConfiguration(CurrencyConfiguration.class).getCurrencyByCode(request.getSaleOrder().getCurrencyCode());
                if (currency == null) {
                    context.addError(WsResponseCode.INVALID_CURRENCY_CODE, "invalid currency code:" + request.getSaleOrder().getCurrencyCode());
                }
            }
        }

        if (!context.hasErrors()) {
            if (request.getSaleOrder().getTotalDiscount() != null) {
                for (WsSaleOrderItem wsSaleOrderItem : request.getSaleOrder().getSaleOrderItems()) {
                    if (!wsSaleOrderItem.getDiscount().equals(BigDecimal.ZERO)) {
                        context.addError(WsResponseCode.INVALID_SALE_ORDER_ITEM_STATE, "only one of order level/order item level discounts can be specified");
                        break;
                    }
                }
            }
        }

        if (!context.hasErrors()) {
            if (request.getSaleOrder().getTotalShippingCharges() != null) {
                for (WsSaleOrderItem wsSaleOrderItem : request.getSaleOrder().getSaleOrderItems()) {
                    if (!wsSaleOrderItem.getShippingCharges().equals(BigDecimal.ZERO)) {
                        context.addError(WsResponseCode.INVALID_SALE_ORDER_ITEM_STATE, "only one of order level/order item level shipping charges can be specified");
                        break;
                    }
                }
            }
        }

        if (!context.hasErrors()) {
            validatePackageConfiguration(request, context);
        }
        if (!context.hasErrors() && request.getSaleOrder().getShippingProviders() != null && request.getSaleOrder().getShippingProviders().size() > 0) {
            validateShippingProviders(request, context);
        }
        if (!context.hasErrors()) {
            validateSaleOrderItemCombinations(request, context);
        }
        validateNotificationEmailAndMobile(request, context);
        return context;
    }

    private void validateAndNormalizeAddress(ValidationContext context, WsAddressDetail address, boolean thirdPartyShipping, int index) {
        LocationCache locationCache = CacheManager.getInstance().getCache(LocationCache.class);
        Country country = locationCache.getCountryByCode(address.getCountry());
        if (country == null) {
            Map<String, Object> errorParams = new HashMap<>();
            errorParams.put("fieldName", "Country");
            errorParams.put("fieldValue", address.getCountry());
            context.addError(WsResponseCode.INVALID_ADDRESS_DETAIL, "saleOrder.addresses[" + index + "].country", "saleOrder.addresses[" + index + "].country has invalid code",
                    errorParams);
        } else {
            address.setCountry(country.getCode());
            if (country.isDefinedStateCodes()) {
                State state = locationCache.getStateByCode(address.getState(), country.getCode());
                if (state == null) {
                    Map<String, Object> errorParams = new HashMap<>();
                    errorParams.put("fieldName", "State");
                    errorParams.put("fieldValue", address.getState());
                    context.addError(WsResponseCode.INVALID_ADDRESS_DETAIL, "saleOrder.addresses[" + index + "].state", "saleOrder.addresses[" + index + "].state has invalid code",
                            errorParams);
                } else {
                    address.setState(state.getIso2Code());
                }
            }
            if (!ConfigurationManager.getInstance().getConfiguration(ProductConfiguration.class).isCourierManagementSwitchedOff()) {
                if (StringUtils.isNotBlank(country.getPostcodePattern())
                        && ConfigurationManager.getInstance().getConfiguration(TenantSystemConfiguration.class).isValidatePostcode()) {
                    String pincode = address.getPincode().toUpperCase();
                    if (country.getPostcodeNormalizationExpression() != null) {
                        Map<String, Object> contextParams = new HashMap<>();
                        contextParams.put("value", pincode);
                        pincode = country.getPostcodeNormalizationExpression().evaluate(contextParams, String.class);
                    }
                    if (!Pattern.matches(country.getPostcodePattern(), pincode)) {
                        Map<String, Object> errorParams = new HashMap<>();
                        errorParams.put("fieldName", "Pincode");
                        errorParams.put("fieldValue", pincode);
                        context.addError(WsResponseCode.INVALID_ADDRESS_DETAIL, "saleOrder.addresses[" + index + "].pincode",
                                "saleOrder.addresses[" + index + "].pincode has invalid format", errorParams);
                    }
                    address.setPincode(pincode);
                }
            } else {
                String pincode = StringUtils.isNotBlank(address.getPincode()) ? address.getPincode() : DEFAULT_PINCODE;
                address.setPincode(pincode);
            }
        }
    }

    private void validateSaleOrderItemCombinations(CreateSaleOrderRequest request, ValidationContext context) {
        Set<String> combinationIdentifiers = new HashSet<String>();
        if (request.getSaleOrder().getSaleOrderItemCombinations() != null && request.getSaleOrder().getSaleOrderItemCombinations().size() > 0) {
            for (WsSaleOrderItemCombination combination : request.getSaleOrder().getSaleOrderItemCombinations()) {
                combinationIdentifiers.add(combination.getCombinationIdentifier());
            }
        }
        for (WsSaleOrderItem saleOrderItem : request.getSaleOrder().getSaleOrderItems()) {
            if (StringUtils.isNotBlank(saleOrderItem.getCombinationIdentifier()) && !combinationIdentifiers.contains(saleOrderItem.getCombinationIdentifier())) {
                context.addError(WsResponseCode.INVALID_COMBINATION_IDENTIFIER,
                        "Invalid combination identifier at saleOrder.saleOrderItems[" + saleOrderItem.getCode() + "].combinationIdentifier");
            }
        }

    }

    private void addCustomerAddressToSaleOrder(CreateSaleOrderRequest request, Customer customer) {
        List<WsAddressDetail> addresses = new ArrayList<WsAddressDetail>();
        WsAddressDetail shippingAddress = prepareWsAddressFromPartyAddress(customer, customer.getPartyAddressByType(PartyAddressType.Code.SHIPPING.name()));
        addresses.add(shippingAddress);
        WsAddressDetail billingAddress = prepareWsAddressFromPartyAddress(customer, customer.getPartyAddressByType(PartyAddressType.Code.BILLING.name()));
        addresses.add(billingAddress);
        request.getSaleOrder().setShippingAddress(new WsAddressRef(shippingAddress.getId()));
        request.getSaleOrder().setBillingAddress(new WsAddressRef(billingAddress.getId()));
        request.getSaleOrder().setAddresses(addresses);
        for (WsSaleOrderItem wsSaleOrderItem : request.getSaleOrder().getSaleOrderItems()) {
            wsSaleOrderItem.setShippingAddress(null);
        }
    }

    private WsAddressDetail prepareWsAddressFromPartyAddress(Party party, PartyAddress partyAddressByType) {
        WsAddressDetail addressDetail = new WsAddressDetail();
        addressDetail.setId(String.valueOf(partyAddressByType.getId()));
        addressDetail.setAddressLine1(partyAddressByType.getAddressLine1());
        addressDetail.setAddressLine2(partyAddressByType.getAddressLine2());
        addressDetail.setCity(partyAddressByType.getCity());
        addressDetail.setName(party.getName());
        addressDetail.setPhone(partyAddressByType.getPhone());
        addressDetail.setPincode(partyAddressByType.getPincode());
        addressDetail.setState(partyAddressByType.getStateCode());
        addressDetail.setCountry(partyAddressByType.getCountryCode());
        return addressDetail;
    }

    /**
     * @param request
     * @param context
     */
    private void validateShippingProviders(CreateSaleOrderRequest request, ValidationContext context) {
        Set<Integer> packetNumbers = new HashSet<Integer>();
        for (WsSaleOrderItem saleOrderItem : request.getSaleOrder().getSaleOrderItems()) {
            packetNumbers.add(saleOrderItem.getPacketNumber());
        }

        for (WsShippingProvider wsShippingProvider : request.getSaleOrder().getShippingProviders()) {
            if (!packetNumbers.contains(wsShippingProvider.getPacketNumber())) {
                context.addError(WsResponseCode.INVALID_PACKAGE_SPECIFICATIONS,
                        "invalid packet number at saleOrder.shippingProviders[" + wsShippingProvider.getCode() + "].packetNumber");
            } else {
                ShippingProvider shippingProvider = ConfigurationManager.getInstance().getConfiguration(ShippingConfiguration.class).getShippingProviderByCode(
                        wsShippingProvider.getCode());
                if (shippingProvider == null) {
                    context.addError(WsResponseCode.INVALID_SHIPPING_PROVIDER_CODE,
                            "invalid shipping provider code at saleOrder.shippingProviders[" + wsShippingProvider.getCode() + "].code");
                }
            }
        }

    }

    /**
     * @param request
     * @param context
     */
    private void validatePackageConfiguration(CreateSaleOrderRequest request, ValidationContext context) {
        Map<Integer, WsSaleOrderItem> packConfigurations = new HashMap<Integer, WsSaleOrderItem>();
        for (WsSaleOrderItem saleOrderItem : request.getSaleOrder().getSaleOrderItems()) {
            if (saleOrderItem.getPacketNumber() != 0) {
                if (!packConfigurations.containsKey(saleOrderItem.getPacketNumber())) {
                    packConfigurations.put(saleOrderItem.getPacketNumber(), saleOrderItem);
                } else {
                    WsSaleOrderItem previousSaleOrderItem = packConfigurations.get(saleOrderItem.getPacketNumber());
                    String previousShippingAddress = previousSaleOrderItem.getShippingAddress() != null ? previousSaleOrderItem.getShippingAddress().getReferenceId()
                            : request.getSaleOrder().getShippingAddress().getReferenceId();
                    String currenShippingAddress = saleOrderItem.getShippingAddress() != null ? saleOrderItem.getShippingAddress().getReferenceId()
                            : request.getSaleOrder().getShippingAddress().getReferenceId();
                    if (!saleOrderItem.getShippingMethodCode().equalsIgnoreCase(previousSaleOrderItem.getShippingMethodCode())
                            || saleOrderItem.isGiftWrap() != previousSaleOrderItem.isGiftWrap() || !currenShippingAddress.equalsIgnoreCase(previousShippingAddress)) {
                        context.addError(WsResponseCode.INVALID_PACKAGE_SPECIFICATIONS,
                                "Item1:" + saleOrderItem.getCode() + ", Item2:" + previousSaleOrderItem.getCode() + " cannot be shipped together");
                    }
                }
            }
        }
    }

    /**
     * @param request
     * @param context
     */
    private void validateNotificationEmailAndMobile(CreateSaleOrderRequest request, ValidationContext context) {
        if (StringUtils.isNotEmpty(request.getSaleOrder().getNotificationMobile())) {
            String trackingMobile = ValidatorUtils.getValidMobileOrNull(request.getSaleOrder().getNotificationMobile());
            if (trackingMobile != null) {
                request.getSaleOrder().setNotificationMobile(trackingMobile);
            } else {
                context.addWarning(ResponseCode.INVALID_FORMAT, "Invalid mobile: mobile notifications will not be enabled for this order");
            }
        }

        if (StringUtils.isNotEmpty(request.getSaleOrder().getNotificationEmail())) {
            if (!ValidatorUtils.isEmailValid(request.getSaleOrder().getNotificationEmail())) {
                context.addWarning(ResponseCode.INVALID_FORMAT, "Invalid email: email notifications will not be enabled for this order");
            }
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.uniware.services.saleorder.ISaleOrderService#getSaleOrdersByStatus
     * (java.lang.String)
     */
    @Override
    @Transactional(readOnly = true)
    public List<SaleOrder> getSaleOrdersByStatus(String statusCode) {
        return saleOrderDao.getSaleOrdersByStatus(statusCode);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.uniware.services.saleorder.ISaleOrderService#updateSaleOrderItem(
     * com.uniware.core.entity.SaleOrderItem)
     */
    @Override
    @Transactional
    public SaleOrderItem updateSaleOrderItem(SaleOrderItem saleOrderItem) {
        return saleOrderDao.updateSaleOrderItem(saleOrderItem);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.uniware.services.saleorder.ISaleOrderService#getSaleOrderForUpdate(
     * java .lang.Integer)
     */
    @Override
    @Transactional
    public SaleOrder getSaleOrderForUpdate(Integer saleOrderId) {
        return saleOrderDao.getSaleOrderForUpdate(saleOrderId);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.uniware.services.saleorder.ISaleOrderService#getSaleOrderItemById
     * (int)
     */
    @Override
    @Transactional
    public SaleOrderItem getSaleOrderItemById(int saleOrderItemId, boolean refresh) {
        return saleOrderDao.getSaleOrderItemById(saleOrderItemId, refresh);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.uniware.services.saleorder.ISaleOrderService#
     * getSaleOrderItemByAllocatedItem(int)
     */
    @Override
    @Transactional
    public SaleOrderItem getSaleOrderItemByAllocatedItem(int itemId) {
        return saleOrderDao.getSaleOrderItemByAllocatedItem(itemId);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.uniware.services.saleorder.ISaleOrderService#getSaleOrderItemById
     * (int)
     */
    @Override
    @Transactional
    public SaleOrderItem getSaleOrderItemById(int saleOrderItemId) {
        return saleOrderDao.getSaleOrderItemById(saleOrderItemId);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.uniware.services.saleorder.ISaleOrderService#
     * getSaleOrderItemForAllocation(int, int)
     */
    @Override
    @Transactional
    public SaleOrderItem getSaleOrderItemForAllocation(String shippingPackageCode, Item item) {
        return saleOrderDao.getSaleOrderItemForAllocation(shippingPackageCode, item);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.uniware.services.saleorder.ISaleOrderService#searchSaleOrders(com
     * .uniware.core.api.saleorder.SearchSaleOrderRequest)
     */
    @Override
    @Transactional(readOnly = true)
    public SearchSaleOrderResponse searchSaleOrders(SearchSaleOrderRequest request) {
        SearchSaleOrderResponse response = new SearchSaleOrderResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            List<SaleOrder> saleOrders = saleOrderDao.searchSaleOrders(request);
            for (SaleOrder saleOrder : saleOrders) {
                response.getElements().add(new com.uniware.core.api.saleorder.SaleOrderDTO(saleOrder));
            }
            if (request.getSearchOptions() != null && request.getSearchOptions().isGetCount()) {
                Long count = saleOrderDao.getSaleOrderCount(request);
                response.setTotalRecords(count);
            }
            response.setSuccessful(true);
        }
        response.setErrors(context.getErrors());
        return response;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.uniware.services.saleorder.ISaleOrderService#getSaleOrder(com.uniware
     * .core.api.saleorder.GetSaleOrderRequest)
     */
    @Override
    @Transactional
    public GetSaleOrderResponse getSaleOrder(GetSaleOrderRequest request) {
        GetSaleOrderResponse response = new GetSaleOrderResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            SaleOrder saleOrder = saleOrderDao.getSaleOrderByCode(request.getCode());
            if (saleOrder == null) {
                context.addError(WsResponseCode.INVALID_SALE_ORDER_CODE);
            } else {
                Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(saleOrder.getChannel().getSourceCode());
                SaleOrderDetailDTO saleOrderDTO = prepareSaleOrderDTO(saleOrder, request.getFacilityCodes());
                if (saleOrderDTO.getSaleOrderItems().isEmpty()) {
                    context.addError(WsResponseCode.INVALID_SALE_ORDER_CODE);
                } else {
                    response.setSuccessful(true);
                    response.setSaleOrderDTO(saleOrderDTO);
                    response.setRefreshEnabled(source.isOrderRefreshEnabled());
                }
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    public AddOrEditSaleOrderItemsResponse divideSOItemsLogisticCharges(AddOrEditSaleOrderItemsRequest request) {
        AddOrEditSaleOrderItemsResponse response = new AddOrEditSaleOrderItemsResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            BigDecimal totalQuantity = new BigDecimal(0);
            BigDecimal totalPrice = new BigDecimal(0.00);
            for (WSSaleOrderItemLight soItem : request.getSaleOrderItems()) {
                totalQuantity = totalQuantity.add(soItem.getQuantity());
                totalPrice = totalPrice.add(soItem.getTotalPrice());
            }
            List<SaleOrderItemLightDTO> saleOrderItemLightDtos = new ArrayList<SaleOrderItemLightDTO>();
            for (WSSaleOrderItemLight soItem : request.getSaleOrderItems()) {
                SaleOrderItemLightDTO itemLightDTO = new SaleOrderItemLightDTO(soItem.getItemName(), soItem.getItemSku(), soItem.getBasePrice(), soItem.getTotalPrice(),
                        soItem.getSellingPrice(), soItem.getQuantity(), soItem.getDiscount(), soItem.getDiscountPercentage(), soItem.getMaxRetailPrice());
                if (PurchaseOrder.LogisticChargesDivisionMethod.EQUALLY.name().equals(request.getLogisticChargesDivisionMethod())) {
                    itemLightDTO.setShippingCharges(NumberUtils.divide(request.getLogisticCharges(), request.getSaleOrderItems().size()));
                } else if (PurchaseOrder.LogisticChargesDivisionMethod.QUANTITY.name().equals(request.getLogisticChargesDivisionMethod())) {
                    itemLightDTO.setShippingCharges(NumberUtils.divide(NumberUtils.multiply(request.getLogisticCharges(), soItem.getQuantity()), totalQuantity));
                } else if (PurchaseOrder.LogisticChargesDivisionMethod.ROW_TOTAL.name().equals(request.getLogisticChargesDivisionMethod())) {
                    itemLightDTO.setShippingCharges(NumberUtils.divide(NumberUtils.multiply(request.getLogisticCharges(), soItem.getTotalPrice()), totalPrice));
                }
                saleOrderItemLightDtos.add(itemLightDTO);
            }
            response.setSuccessful(true);
            response.setSaleOrderItemLightDTOs(saleOrderItemLightDtos);
        }
        response.setErrors(context.getErrors());
        return response;
    }

    /**
     * @param saleOrder
     * @return
     */
    private SaleOrderDetailDTO prepareSaleOrderDTO(SaleOrder saleOrder, List<String> facilityCodes) {
        SaleOrderDetailDTO saleOrderDTO = new SaleOrderDetailDTO(saleOrder);
        saleOrderDTO.setCustomFieldValues(CustomFieldUtils.getCustomFieldValuesDTO(saleOrder));
        Map<Integer, WsAddressDetail> addresses = new HashMap<Integer, WsAddressDetail>();
        Set<Integer> shippingPackageIds = new HashSet<Integer>();
        WsAddressDetail billingAddress = new WsAddressDetail(saleOrder.getBillingAddress());
        saleOrderDTO.setBillingAddress(billingAddress);
        saleOrderDTO.getAddresses().add(billingAddress);
        addresses.put(saleOrder.getBillingAddress().getId(), billingAddress);
        for (SaleOrderItem saleOrderItem : saleOrder.getSaleOrderItems()) {
            if (facilityCodes == null || facilityCodes.isEmpty() || (saleOrderItem.getFacility() != null && facilityCodes.contains(saleOrderItem.getFacility().getCode()))) {
                SaleOrderItemDTO saleOrderItemDTO = new SaleOrderItemDTO(saleOrderItem);
                if (saleOrderItem.getFacility() != null) {
                    saleOrderItemDTO.setFacilityCode(saleOrderItem.getFacility().getCode());
                    saleOrderItemDTO.setFacilityName(saleOrderItem.getFacility().getDisplayName());
                    saleOrderItemDTO.setAlternateFacilityCode(saleOrderItem.getFacility().getAlternateCode());
                }
                ItemType itemType = catalogService.getNonBundledItemTypeById(saleOrderItem.getItemType().getId());
                saleOrderItemDTO.setItemName(itemType.getName());
                saleOrderItemDTO.setItemSku(itemType.getSkuCode());
                saleOrderItemDTO.setImageUrl(itemType.getImageUrl());
                saleOrderItemDTO.setPageUrl(itemType.getProductPageUrl());
                saleOrderItemDTO.setCustomFieldValues(CustomFieldUtils.getCustomFieldValuesDTO(saleOrderItem));
                if (saleOrderItem.getShippingPackage() != null) {
                    if (!shippingPackageIds.contains(saleOrderItem.getShippingPackage().getId())) {
                        ShippingPackage shippingPackage = shippingService.getShippingPackageById(saleOrderItem.getShippingPackage().getId());
                        ShippingPackageDTO shippingPackageDTO = new ShippingPackageDTO(shippingPackage);
                        shippingPackageDTO.setCustomFieldValues(CustomFieldUtils.getCustomFieldValuesDTO(shippingPackage));
                        shippingPackageDTO.setDispatched(shippingPackage.getDispatchTime());
                        shippingPackageDTO.setDelivered(shippingPackage.getDeliveryTime());
                        if (StringUtils.isNotBlank(shippingPackage.getPodCode())) {
                            shippingPackageDTO.setPodCode(shippingPackage.getPodCode());
                        }
                        saleOrderDTO.addShippingPackage(shippingPackageDTO);
                        shippingPackageIds.add(saleOrderItem.getShippingPackage().getId());
                    }
                }
                if (saleOrderItem.getItemTypeInventory() != null) {
                    saleOrderItemDTO.setShelfCode(saleOrderItem.getItemTypeInventory().getShelf().getCode());
                }
                if (saleOrderItem.getReversePickup() != null) {
                    saleOrderItemDTO.setReversePickupCode(returnsDao.getReversePickupById(saleOrderItem.getReversePickup().getId()).getCode());
                }
                if (!addresses.containsKey(saleOrderItem.getShippingAddress().getId())) {
                    WsAddressDetail wsAddressDetail = new WsAddressDetail(saleOrderItem.getShippingAddress());
                    saleOrderDTO.getAddresses().add(wsAddressDetail);
                    addresses.put(saleOrderItem.getShippingAddress().getId(), wsAddressDetail);
                }
                if (saleOrderItem.getItem() != null) {
                    saleOrderItemDTO.setItem(saleOrderItem.getItem().getCode());
                    if (saleOrderItem.getItem().getShelf() != null) {
                        saleOrderItemDTO.setShelfCode(saleOrderItem.getItem().getShelf().getCode());
                    }
                }
                if (StringUtils.equalsAny(saleOrderItem.getStatusCode(), SaleOrderItem.StatusCode.CREATED.name(), SaleOrderItem.StatusCode.FULFILLABLE.name(),
                        SaleOrderItem.StatusCode.UNFULFILLABLE.name(), SaleOrderItem.StatusCode.LOCATION_NOT_SERVICEABLE.name())) {
                    saleOrderItemDTO.setEditAddress(true);
                }
                if (saleOrderItem.getSaleOrderItemAlternate() != null) {
                    saleOrderItemDTO.setSaleOrderItemAlternateId(saleOrderItem.getSaleOrderItemAlternate().getId());
                }
                if (saleOrderItem.getInvoiceItem() != null) {
                    saleOrderItemDTO.setTaxPercentage(saleOrderItem.getInvoiceItem().getSellingPriceInvoiceItemTax().getTaxPercentage());
                }
                ReversePickup reversePickup = saleOrderItem.getReversePickup();
                if (reversePickup != null && reversePickup.getReplacementSaleOrder() != null) {
                    saleOrderItemDTO.setReplacementSaleOrderCode(reversePickup.getReplacementSaleOrder().getCode());
                }
                saleOrderDTO.addSaleOrderItem(saleOrderItemDTO);
            }
        }
        return saleOrderDTO;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.uniware.services.saleorder.ISaleOrderService#getSaleOrderById(int)
     */
    @Override
    @Transactional
    public SaleOrder getSaleOrderById(int saleOrderId) {
        return saleOrderDao.getSaleOrderById(saleOrderId);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.uniware.services.saleorder.ISaleOrderService#cancelSaleOrder(com.
     * uniware.core.api.saleorder.CancelSaleOrderRequest)
     */
    @Override
    @Locks({ @Lock(ns = Namespace.SALE_ORDER, key = "#{#args[0].saleOrderCode}") })
    @Transactional
    @RollbackOnFailure
    @LogActivity
    public CancelSaleOrderResponse cancelSaleOrder(CancelSaleOrderRequest request) {
        CancelSaleOrderResponse response = new CancelSaleOrderResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            SaleOrder saleOrder = saleOrderDao.getSaleOrderForUpdate(request.getSaleOrderCode());
            List<SaleOrderItem> itemsToCancel = new ArrayList<>();
            if (saleOrder == null) {
                context.addError(WsResponseCode.INVALID_SALE_ORDER_CODE, "Sale Order does not exist");
            } else {
                int saleOrderItemsToCancelCount = request.getSaleOrderItemCodes() != null ? request.getSaleOrderItemCodes().size() : 0;
                for (SaleOrderItem saleOrderItem : saleOrder.getSaleOrderItems()) {
                    if (saleOrderItemsToCancelCount == 0 || request.getSaleOrderItemCodes().remove(saleOrderItem.getCode())) {
                        if (!saleOrderItem.isCancellable()) {
                            context.addError(WsResponseCode.INVALID_SALE_ORDER_ITEM_STATE,
                                    "SaleOrderItem[" + saleOrderItem.getCode() + "] can't be cancelled in this state : " + saleOrderItem.getStatusCode());
                        } else {
                            itemsToCancel.add(saleOrderItem);
                        }
                    }
                }
                if (saleOrderItemsToCancelCount > 0 && request.getSaleOrderItemCodes().size() > 0) {
                    context.addError(WsResponseCode.INVALID_SALE_ORDER_ITEM_CODE, "Invalid SaleOrderItem codes :[" + StringUtils.join('|', request.getSaleOrderItemCodes()) + "]");
                }
            }
            if (!context.hasErrors()) {
                Channel channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelById(saleOrder.getChannel().getId());
                List<ShippingPackage> affectedPackages = new ArrayList<>();
                Map<Integer, Integer> releaseInventoryMap = new HashMap<>();
                for (SaleOrderItem saleOrderItem : itemsToCancel) {
                    cancelSaleOrderItem(saleOrderItem, affectedPackages, releaseInventoryMap, request.getCancellationReason());
                    saleOrderItem.setOnHold(false);
                }
                for (Map.Entry<Integer, Integer> entry : releaseInventoryMap.entrySet()) {
                    inventoryService.releaseInventory(entry.getKey(), channel.getId(), entry.getValue());
                }
                for (ShippingPackage shippingPackage : affectedPackages) {
                    shippingService.reassessShippingPackage(shippingPackage);
                }
                for (SaleOrderItem saleOrderItem : saleOrder.getSaleOrderItems()) {
                    if (saleOrderItem.getShippingPackage() == null && saleOrderItem.getStatusCode().equals(SaleOrderItem.StatusCode.FULFILLABLE.name())) {
                        saleOrder.setReassessInventory(true);
                        updateSaleOrderItem(saleOrderItem);
                    }
                }

                Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(channel.getSourceCode());
                if (request.isCancelOnChannel() && StringUtils.isNotBlank(
                        CacheManager.getInstance().getCache(ChannelCache.class).getScriptName(channel.getCode(), Source.SALE_ORDER_CANCELLATION_SCRIPT_NAME))) {
                    ScraperScript scraperScript = CacheManager.getInstance().getCache(ChannelCache.class).getScriptByName(channel.getCode(),
                            Source.SALE_ORDER_CANCELLATION_SCRIPT_NAME);
                    if (scraperScript != null) {
                        try {
                            executeSaleOrderCancellationScript(saleOrder, channel, itemsToCancel, scraperScript);
                            if (ActivityContext.current().isEnable()) {
                                ActivityUtils.appendActivity(saleOrder.getCode(), ActivityEntityEnum.SALE_ORDER.getName(), null,
                                        Arrays.asList(new String[] { "Cancellation done on channel " + channel.getCode() + ". Reason [" + request.getCancellationReason() + "]" }),
                                        ActivityTypeEnum.CANCEL.name());
                            }
                        } catch (Exception e) {
                            context.addError(WsResponseCode.INVALID_SALE_ORDER_ITEM_STATE, "Unable to cancel order on " + source.getName() + ": " + e.getMessage());
                        }
                    }
                }
                if (!context.hasErrors() && StringUtils.isNotBlank(CacheManager.getInstance().getCache(ChannelCache.class).getScriptName(channel.getCode(),
                        com.uniware.core.entity.Source.CANCEL_SALE_ORDER_ACKNOWLEDGEMENT_SCRIPT_NAME))) {
                    ScraperScript scraperScript = CacheManager.getInstance().getCache(ChannelCache.class).getScriptByName(channel.getCode(),
                            com.uniware.core.entity.Source.CANCEL_SALE_ORDER_ACKNOWLEDGEMENT_SCRIPT_NAME);
                    if (scraperScript != null) {
                        try {
                            ScriptExecutionContext scriptContext = ScriptExecutionContext.current();
                            scriptContext.addVariable("channel", channel);
                            scriptContext.addVariable("saleOrder", saleOrder);
                            scriptContext.addVariable("saleOrderItems", itemsToCancel);
                            for (ChannelConnector channelConnector : channel.getChannelConnectors()) {
                                for (ChannelConnectorParameter channelConnectorParameter : channelConnector.getChannelConnectorParameters()) {
                                    scriptContext.addVariable(channelConnectorParameter.getName(), channelConnectorParameter.getValue());
                                }
                            }
                            for (ChannelConfigurationParameter channelConfigurationParameter : channel.getChannelConfigurationParameters()) {
                                scriptContext.addVariable(channelConfigurationParameter.getSourceConfigurationParameterName(), channelConfigurationParameter.getValue());
                            }
                            scriptContext.setScriptProvider(new IScriptProvider() {

                                @Override
                                public ScraperScript getScript(String scriptName) {
                                    return CacheManager.getInstance().getCache(ScriptVersionedCache.class).getScriptByName(scriptName);
                                }
                            });
                            scriptContext.setTraceLoggingEnabled(UserContext.current().isTraceLoggingEnabled());
                            scraperScript.execute();
                        } catch (Exception e) {
                            LOG.error("unable to acknowledge cancellation for saleOrder:" + saleOrder.getCode(), e);
                            context.addError(WsResponseCode.INVALID_STATE, "unable to acknowledge cancellation for saleOrder from the panel" + e.getMessage());
                        } finally {
                            ScriptExecutionContext.destroy();
                        }
                    }
                }
            }
        }
        if (!context.hasErrors()) {
            response.setSuccessful(true);
        } else {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    private void executeSaleOrderCancellationScript(SaleOrder saleOrder, Channel channel, List<SaleOrderItem> saleOrderItems, ScraperScript scraperScript) {
        final ScriptExecutionContext context = ScriptExecutionContext.current();
        context.addVariable("source", ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(channel.getSourceCode()));
        context.addVariable("channel", channel);
        context.addVariable("saleOrder", saleOrder);
        context.addVariable("saleOrderItems", saleOrderItems);
        SourceConfiguration sourceConfiguration = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class);
        for (ChannelConnector channelConnector : channel.getChannelConnectors()) {
            SourceConnector sourceConnector = sourceConfiguration.getSourceConnector(channel.getSourceCode(), channelConnector.getSourceConnectorName());
            if (sourceConnector.isRequiredInOrderSync()) {
                for (ChannelConnectorParameter channelConnectorParameter : channelConnector.getChannelConnectorParameters()) {
                    context.addVariable(channelConnectorParameter.getName(), channelConnectorParameter.getValue());
                }
            }
        }
        for (ChannelConfigurationParameter channelConfigurationParameter : channel.getChannelConfigurationParameters()) {
            context.addVariable(channelConfigurationParameter.getSourceConfigurationParameterName(), channelConfigurationParameter.getValue());
        }
        context.setScriptProvider(new IScriptProvider() {
            @Override
            public ScraperScript getScript(String scriptName) {
                return CacheManager.getInstance().getCache(ScriptVersionedCache.class).getScriptByName(scriptName);
            }
        });
        try {
            context.setTraceLoggingEnabled(UserContext.current().isTraceLoggingEnabled());
            scraperScript.execute();
        } finally {
            ScriptExecutionContext.destroy();
        }
    }

    private void cancelSaleOrderItem(SaleOrderItem saleOrderItem, List<ShippingPackage> affectedPackages, Map<Integer, Integer> releaseInventoryMap, String cancellationReason) {
        boolean activityEnabled = ActivityContext.current().isEnable();
        List<String> activityLogs = new ArrayList<>();
        if (StringUtils.equalsAny(saleOrderItem.getStatusCode(), SaleOrderItem.StatusCode.CREATED.name(), SaleOrderItem.StatusCode.UNFULFILLABLE.name(),
                SaleOrderItem.StatusCode.ALTERNATE_SUGGESTED.name(), SaleOrderItem.StatusCode.LOCATION_NOT_SERVICEABLE.name())) {
            // Nothing has been done for SaleOrderItem, so it can be cancelled.
            if (activityEnabled) {
                activityLogs.add("OrderItem {" + ActivityEntityEnum.SALE_ORDER_ITEM + ":" + saleOrderItem.getCode() + "} moved from " + saleOrderItem.getStatusCode()
                        + " to CANCELLED." + (StringUtils.isNotBlank(cancellationReason) ? " Reason " + cancellationReason : ""));
            }
            saleOrderItem.setStatusCode(SaleOrderItem.StatusCode.CANCELLED.name());
            saleOrderItem.setCancellationTime(DateUtils.getCurrentTime());
            saleOrderDao.updateSaleOrderItem(saleOrderItem);
        } else if (StringUtils.equalsAny(saleOrderItem.getStatusCode(), SaleOrderItem.StatusCode.FULFILLABLE.name(), SaleOrderItem.StatusCode.PICKING_FOR_INVOICING.name(),
                SaleOrderItem.StatusCode.PICKING_FOR_STAGING.name(), SaleOrderItem.StatusCode.STAGED.name())) {
            // Inventory is already blocked for saleOrderItem.
            if (saleOrderItem.getShippingPackage() == null) {
                // Shipping Package has not been created yet, so items can be
                // cancelled and inventory can be release for the item.
                if (activityEnabled) {
                    activityLogs.add("OrderItem {" + ActivityEntityEnum.SALE_ORDER_ITEM + ":" + saleOrderItem.getCode() + "} moved from " + saleOrderItem.getStatusCode()
                            + " to CANCELLED. Released Inventory." + (StringUtils.isNotBlank(cancellationReason) ? " Reason " + cancellationReason : ""));
                }
                saleOrderItem.setStatusCode(SaleOrderItem.StatusCode.CANCELLED.name());
                saleOrderItem.setCancellationTime(DateUtils.getCurrentTime());
                saleOrderDao.updateSaleOrderItem(saleOrderItem);
                if (saleOrderItem.getItemTypeInventory() != null) {
                    if (!releaseInventoryMap.containsKey(saleOrderItem.getItemTypeInventory().getId())) {
                        releaseInventoryMap.put(saleOrderItem.getItemTypeInventory().getId(), 0);
                    }
                    releaseInventoryMap.put(saleOrderItem.getItemTypeInventory().getId(), releaseInventoryMap.get(saleOrderItem.getItemTypeInventory().getId()) + 1);
                    if (saleOrderItem.getItem() != null) {
                        saleOrderItem.getItem().setStatusCode(Item.StatusCode.GOOD_INVENTORY.name());
                        saleOrderItem.setItem(null);
                    }
                }
            } else {
                // Shipping Package has been created yet, item might have been
                // planned or already picked
                ShippingPackage shippingPackage;
                int index = affectedPackages.indexOf(saleOrderItem.getShippingPackage());
                if (index != -1) {
                    shippingPackage = affectedPackages.get(index);
                } else {
                    shippingPackage = shippingService.getShippingPackageById(saleOrderItem.getShippingPackage().getId());
                }
                for (SaleOrderItem soi : shippingPackage.getSaleOrderItems()) {
                    if (soi.getId().equals(saleOrderItem.getId())) {
                        saleOrderItem = soi;
                        break;
                    }
                }
                if (saleOrderItem.getItemTypeInventory() == null) {
                    saleOrderItem.setStatusCode(SaleOrderItem.StatusCode.CANCELLED.name());
                    saleOrderItem.setCancellationTime(DateUtils.getCurrentTime());
                    saleOrderItem.setShippingPackage(null);
                    saleOrderItem.setInvoiceItem(null);
                    shippingPackage.getSaleOrderItems().remove(saleOrderItem);
                    affectedPackages.add(shippingPackage);
                } else {
                    if (ShippingPackage.StatusCode.CREATED.name().equals(shippingPackage.getStatusCode())
                            || ShippingPackage.StatusCode.LOCATION_NOT_SERVICEABLE.name().equals(shippingPackage.getStatusCode())) {
                        // Item not yet in picking update the package, cancel
                        // sale
                        // order item and release inventory
                        saleOrderItem.setStatusCode(SaleOrderItem.StatusCode.CANCELLED.name());
                        saleOrderItem.setCancellationTime(DateUtils.getCurrentTime());
                        saleOrderItem.setShippingPackage(null);
                        saleOrderDao.updateSaleOrderItem(saleOrderItem);
                        /*
                         * activityLogs.add("OrderItem {" +
                         * ActivityEntityEnum.SALE_ORDER_ITEM + ":" +
                         * saleOrderItem.getCode() + "} moved from " +
                         * saleOrderItem.getStatusCode() +
                         * " to CANCELLED. Released Inventory. Removed from shipping package {"
                         * + ActivityEntityEnum.SHIPPING_PACKAGE + ":" +
                         * shippingPackage.getCode() + "}" +
                         * (StringUtils.isNotBlank(cancellationReason) ?
                         * " Reason " + cancellationReason : ""));
                         */
                        if (!releaseInventoryMap.containsKey(saleOrderItem.getItemTypeInventory().getId())) {
                            releaseInventoryMap.put(saleOrderItem.getItemTypeInventory().getId(), 0);
                        }
                        releaseInventoryMap.put(saleOrderItem.getItemTypeInventory().getId(), releaseInventoryMap.get(saleOrderItem.getItemTypeInventory().getId()) + 1);
                        if (saleOrderItem.getItem() != null) {
                            saleOrderItem.getItem().setStatusCode(Item.StatusCode.GOOD_INVENTORY.name());
                            saleOrderItem.setItem(null);
                        }
                        shippingPackage.getSaleOrderItems().remove(saleOrderItem);
                        affectedPackages.add(shippingPackage);
                    } else if (StringUtils.equalsAny(shippingPackage.getStatusCode(), ShippingPackage.StatusCode.PICKED.name(), ShippingPackage.StatusCode.PICKING.name())) {
                        boolean picklistItemAssessmentPending = pickerService.handlePicklistItemCancellation(saleOrderItem);
                        saleOrderItem.setStatusCode(SaleOrderItem.StatusCode.CANCELLED.name());
                        saleOrderItem.setCancellationTime(DateUtils.getCurrentTime());
                        if (picklistItemAssessmentPending) {
                            saleOrderItem.setShippingPackage(null);
                            shippingPackage.getSaleOrderItems().remove(saleOrderItem);
                            affectedPackages.add(shippingPackage);
                        } else {
                            // item was scanned already
                            saleOrderItem.setInvoiceItem(null);
                            shippingPackage.setPutawayPending(true);
                            shippingService.updateShippingPackage(shippingPackage);
                        }
                        saleOrderDao.updateSaleOrderItem(saleOrderItem);
                    } else if (StringUtils.equalsAny(shippingPackage.getStatusCode(), ShippingPackage.StatusCode.PACKED.name(),
                            ShippingPackage.StatusCode.CUSTOMIZATION_COMPLETE.name(), ShippingPackage.StatusCode.PENDING_CUSTOMIZATION.name(),
                            ShippingPackage.StatusCode.READY_TO_SHIP.name())) {
                        saleOrderItem.setStatusCode(SaleOrderItem.StatusCode.CANCELLED.name());
                        saleOrderItem.setCancellationTime(DateUtils.getCurrentTime());
                        saleOrderItem.setInvoiceItem(null);
                        saleOrderDao.updateSaleOrderItem(saleOrderItem);
                        shippingPackage.setPutawayPending(true);
                    }
                    if (activityEnabled) {
                        activityLogs.add(
                                "OrderItem {" + ActivityEntityEnum.SALE_ORDER_ITEM + ":" + saleOrderItem.getCode() + "} moved from " + SaleOrderItem.StatusCode.FULFILLABLE.name()
                                        + " to CANCELLED. Released Inventory. Removed from shipping package {" + ActivityEntityEnum.SHIPPING_PACKAGE + ":"
                                        + shippingPackage.getCode() + "}" + (StringUtils.isNotBlank(cancellationReason) ? " Reason " + cancellationReason : ""));
                    }
                }
                shippingService.updateShippingPackage(shippingPackage);
            }
        }
        if (activityEnabled) {
            ActivityUtils.appendActivity(saleOrderItem.getCode(), ActivityEntityEnum.SALE_ORDER_ITEM.getName(), saleOrderItem.getSaleOrder().getCode(), activityLogs,
                    ActivityTypeEnum.CANCEL.name());
        }
        saleOrderItem.setCancellationReason(cancellationReason);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.uniware.services.saleorder.ISaleOrderService#
     * getItemCountNotDispatched ()
     */
    @Override
    @Transactional(readOnly = true)
    public Long getItemCountNotDispatched() {
        return saleOrderDao.getItemCountNotDispatched();
    }

    /*
     * (non-Javadoc)
     *
     * @see com.uniware.services.saleorder.ISaleOrderService#
     * getItemCountDispatchedToday ()
     */
    @Override
    @Transactional(readOnly = true)
    public Long getItemCountDispatchedToday() {
        return saleOrderDao.getItemCountDispatchedToday();
    }

    /*
     * (non-Javadoc)
     *
     * @see com.uniware.services.saleorder.ISaleOrderService#
     * createSaleOrderFlowSummary (com.uniware.core.vo.SaleOrderFlowSummaryVO)
     */
    @Override
    @Transactional
    public SaleOrderFlowSummaryVO createSaleOrderFlowSummary(SaleOrderFlowSummaryVO summary) {
        return saleOrderMao.createSaleOrderFlowSummary(summary);

    }

    /*
     * (non-Javadoc)
     *
     * @see com.uniware.services.saleorder.ISaleOrderService#
     * getSaleOrderItemsByStatus (java.lang.String)
     */
    @Override
    @Transactional(readOnly = true)
    public List<SaleOrderItem> getSaleOrderItemsByStatus(String statusCode) {
        return saleOrderDao.getSaleOrderItemsByStatus(statusCode);
    }

    @Override
    @Transactional(readOnly = true)
    public List<com.uniware.services.tasks.saleorder.SaleOrderProcessor.SaleOrderDTO> getSaleOrdersForProcessing() {
        return saleOrderDao.getSaleOrdersForProcessing();
    }

    @Override
    @Transactional(readOnly = true)
    public List<com.uniware.services.tasks.saleorder.SaleOrderProcessor.SaleOrderDTO> getSaleOrdersForProcessing(Integer itemTypeId) {
        return saleOrderDao.getSaleOrdersForProcessing(itemTypeId);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.uniware.services.saleorder.ISaleOrderService#holdSaleOrder(com.
     * uniware .core.api.saleorder.HoldSaleOrderRequest)
     */
    @Override
    @Locks({ @Lock(ns = Namespace.SALE_ORDER, key = "#{#args[0].saleOrderCode}") })
    @Transactional
    @LogActivity
    public HoldSaleOrderResponse holdSaleOrder(HoldSaleOrderRequest request) {
        HoldSaleOrderResponse response = new HoldSaleOrderResponse();
        ValidationContext context = request.validate();
        response.setErrors(context.getErrors());
        if (!context.hasErrors()) {
            SaleOrder saleOrder = saleOrderDao.getSaleOrderForUpdate(request.getSaleOrderCode());
            if (saleOrder == null) {
                context.addError(WsResponseCode.INVALID_SALE_ORDER_CODE);
            } else {
                HoldSaleOrderItemsRequest holdSaleOrderItemsRequest = new HoldSaleOrderItemsRequest();
                holdSaleOrderItemsRequest.setSaleOrderCode(saleOrder.getCode());
                List<String> saleOrderItemCodes = new ArrayList<String>();
                for (SaleOrderItem saleOrderItem : saleOrder.getSaleOrderItems()) {
                    saleOrderItemCodes.add(saleOrderItem.getCode());
                }
                holdSaleOrderItemsRequest.setSaleOrderItemCodes(saleOrderItemCodes);
                HoldSaleOrderItemsResponse holdSaleOrderItemsResponse = holdSaleOrderItems(holdSaleOrderItemsRequest);
                if (holdSaleOrderItemsResponse.isSuccessful()) {
                    response.setSuccessful(true);
                } else {
                    response.addErrors(holdSaleOrderItemsResponse.getErrors());
                }
                if (ActivityContext.current().isEnable()) {
                    ActivityUtils.appendActivity(saleOrder.getCode(), ActivityEntityEnum.SALE_ORDER.getName(), null,
                            Arrays.asList(new String[] { "Order {" + ActivityEntityEnum.SALE_ORDER + ":" + saleOrder.getCode() + "} put on hold." }),
                            ActivityTypeEnum.PAUSED.name());
                }
            }
        }
        return response;
    }

    @Override
    @Locks({ @Lock(ns = Namespace.SALE_ORDER, key = "#{#args[0].saleOrderCode}") })
    @Transactional
    public SetSaleOrderPriorityResponse setSaleOrderPriority(SetSaleOrderPriorityRequest request) {
        SetSaleOrderPriorityResponse response = new SetSaleOrderPriorityResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            SaleOrder saleOrder = saleOrderDao.getSaleOrderForUpdate(request.getSaleOrderCode());
            if (saleOrder == null) {
                context.addError(WsResponseCode.INVALID_SALE_ORDER_CODE);
            } else {
                saleOrder.setPriority(request.getPriority());
                for (SaleOrderItem saleOrderItem : saleOrder.getSaleOrderItems()) {
                    if (saleOrderItem.getShippingPackage() != null && ShippingPackage.StatusCode.CREATED.name().equals(saleOrderItem.getShippingPackage().getStatusCode())) {
                        saleOrderItem.getShippingPackage().setPriority(request.getPriority());
                    }
                }
                response.setSuccessful(true);
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.uniware.services.saleorder.ISaleOrderService#holdSaleOrder(com.
     * uniware .core.api.saleorder.HoldSaleOrderRequest)
     */
    @Override
    @Locks({ @Lock(ns = Namespace.SALE_ORDER, key = "#{#args[0].saleOrderCode}") })
    @Transactional
    @LogActivity
    public HoldSaleOrderItemsResponse holdSaleOrderItems(HoldSaleOrderItemsRequest request) {
        HoldSaleOrderItemsResponse response = new HoldSaleOrderItemsResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            SaleOrder saleOrder = saleOrderDao.getSaleOrderForUpdate(request.getSaleOrderCode());
            if (saleOrder == null) {
                context.addError(WsResponseCode.INVALID_SALE_ORDER_CODE);
            } else {
                List<SaleOrderItem> itemsToHold = new ArrayList<SaleOrderItem>();
                for (SaleOrderItem saleOrderItem : saleOrder.getSaleOrderItems()) {
                    if (request.getSaleOrderItemCodes().remove(saleOrderItem.getCode())) {
                        if (!saleOrderItem.isCancellable()) {
                            context.addError(WsResponseCode.INVALID_SALE_ORDER_ITEM_STATE, "SaleOrderItem[" + saleOrderItem.getCode() + "] can't be hold in this state");
                        } else {
                            itemsToHold.add(saleOrderItem);
                        }
                    }
                }
                if (request.getSaleOrderItemCodes().size() > 0) {
                    context.addError(WsResponseCode.INVALID_SALE_ORDER_ITEM_CODE, "Invalid SaleOrderItem codes :[" + StringUtils.join('|', request.getSaleOrderItemCodes()) + "]");
                }
                if (!context.hasErrors()) {
                    List<String> soiCodes = new ArrayList<>();
                    for (SaleOrderItem saleOrderItem : itemsToHold) {
                        if (SaleOrderItem.StatusCode.UNFULFILLABLE.name().equals(saleOrderItem.getStatusCode())) {
                            saleOrderItem.setStatusCode(SaleOrderItem.StatusCode.CREATED.name());
                        }
                        saleOrderItem.setOnHold(true);
                        soiCodes.add(saleOrderItem.getCode());
                    }
                    if (ActivityContext.current().isEnable()) {
                        ActivityUtils.appendActivity(saleOrder.getCode(), ActivityEntityEnum.SALE_ORDER_ITEM.getName(), saleOrder.getCode(),
                                Arrays.asList(new String[] { "Order Item {" + ActivityEntityEnum.SALE_ORDER_ITEM + ":" + soiCodes + "} has been put on hold." }),
                                ActivityTypeEnum.PAUSED.name());
                    }
                    response.setSuccessful(true);
                }
            }
        }
        response.setWarnings(context.getWarnings());
        response.setErrors(context.getErrors());
        return response;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.uniware.services.saleorder.ISaleOrderService#unholdSaleOrder(com.
     * uniware.core.api.saleorder.UnholdSaleOrderRequest)
     */
    @Override
    @Locks({ @Lock(ns = Namespace.SALE_ORDER, key = "#{#args[0].saleOrderCode}") })
    @Transactional
    @LogActivity
    public UnholdSaleOrderResponse unholdSaleOrder(UnholdSaleOrderRequest request) {
        UnholdSaleOrderResponse response = new UnholdSaleOrderResponse();
        ValidationContext context = request.validate();
        response.setErrors(context.getErrors());
        if (!context.hasErrors()) {
            SaleOrder saleOrder = saleOrderDao.getSaleOrderForUpdate(request.getSaleOrderCode());
            if (saleOrder == null) {
                context.addError(WsResponseCode.INVALID_SALE_ORDER_CODE);
            } else {
                UnholdSaleOrderItemsRequest unholdSaleOrderItemsRequest = new UnholdSaleOrderItemsRequest();
                unholdSaleOrderItemsRequest.setSaleOrderCode(saleOrder.getCode());
                List<String> saleOrderItemCodes = new ArrayList<>();
                for (SaleOrderItem saleOrderItem : saleOrder.getSaleOrderItems()) {
                    saleOrderItemCodes.add(saleOrderItem.getCode());
                }
                unholdSaleOrderItemsRequest.setSaleOrderItemCodes(saleOrderItemCodes);
                UnholdSaleOrderItemsResponse unholdSaleOrderItemsResponse = unholdSaleOrderItems(unholdSaleOrderItemsRequest);
                if (unholdSaleOrderItemsResponse.isSuccessful()) {
                    saleOrder.setReassessInventory(true);
                    response.setSuccessful(true);
                    if (ActivityContext.current().isEnable()) {
                        ActivityUtils.appendActivity(saleOrder.getCode(), ActivityEntityEnum.SALE_ORDER_ITEM.getName(), saleOrder.getCode(),
                                Arrays.asList(new String[] { "Order {" + ActivityEntityEnum.SALE_ORDER + ":" + saleOrder.getCode() + "} has been marked for Unhold" }),
                                ActivityTypeEnum.PROCESSING.name());
                    }
                } else {
                    response.addErrors(unholdSaleOrderItemsResponse.getErrors());
                }
            }
        }
        return response;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.uniware.services.saleorder.ISaleOrderService#unholdSaleOrder(com.
     * uniware.core.api.saleorder.UnholdSaleOrderRequest)
     */
    @Override
    @Locks({ @Lock(ns = Namespace.SALE_ORDER, key = "#{#args[0].saleOrderCode}") })
    @Transactional
    @LogActivity
    public UnholdSaleOrderItemsResponse unholdSaleOrderItems(UnholdSaleOrderItemsRequest request) {
        UnholdSaleOrderItemsResponse response = new UnholdSaleOrderItemsResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            SaleOrder saleOrder = saleOrderDao.getSaleOrderForUpdate(request.getSaleOrderCode());
            if (saleOrder == null) {
                context.addError(WsResponseCode.INVALID_SALE_ORDER_CODE);
            } else {
                List<SaleOrderItem> itemsToUnhold = new ArrayList<SaleOrderItem>();
                for (SaleOrderItem saleOrderItem : saleOrder.getSaleOrderItems()) {
                    if (request.getSaleOrderItemCodes().remove(saleOrderItem.getCode())) {
                        itemsToUnhold.add(saleOrderItem);
                    }
                }
                if (request.getSaleOrderItemCodes().size() > 0) {
                    context.addError(WsResponseCode.INVALID_SALE_ORDER_ITEM_CODE, "Invalid SaleOrderItem codes :[" + StringUtils.join('|', request.getSaleOrderItemCodes()) + "]");
                }
                if (!context.hasErrors()) {
                    List<String> soiCodes = new ArrayList<>();
                    for (SaleOrderItem saleOrderItem : itemsToUnhold) {
                        saleOrderItem.setOnHold(false);
                        soiCodes.add(saleOrderItem.getCode());
                    }
                    if (ActivityContext.current().isEnable()) {
                        ActivityUtils.appendActivity(saleOrder.getCode(), ActivityEntityEnum.SALE_ORDER_ITEM.getName(), saleOrder.getCode(),
                                Arrays.asList(new String[] { "Order Item {" + ActivityEntityEnum.SALE_ORDER_ITEM + ":" + soiCodes + "} has been marked for Unhold" }),
                                ActivityTypeEnum.PROCESSING.name());
                    }
                    response.setSuccessful(true);
                }
            }
        }
        response.setWarnings(context.getWarnings());
        response.setErrors(context.getErrors());
        return response;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.uniware.services.saleorder.ISaleOrderService#
     * getSaleOrderItemByItemTypeByStatus(int, java.lang.String)
     */
    @Override
    @Transactional
    public List<SaleOrderItem> getSaleOrderItemByItemTypeByStatus(int itemTypeId, String statusCode) {
        return saleOrderDao.getSaleOrderItemByItemTypeByStatus(itemTypeId, statusCode);
    }

    @Override
    @Transactional
    public SaleOrder getSaleOrderByCode(String code) {
        return saleOrderDao.getSaleOrderByCode(code);
    }

    @Override
    @Transactional
    public SaleOrder getSaleOrderForUpdate(String code) {
        return getSaleOrderForUpdate(code, true);
    }

    @Override
    @Transactional
    public SaleOrder getSaleOrderForUpdate(String code, boolean markViewDirty) {
        return saleOrderDao.getSaleOrderForUpdate(code, markViewDirty);
    }

    @Override
    @Transactional
    public List<SaleOrder> getSaleOrderByDisplayOrderCode(String code, Integer channelId) {
        return saleOrderDao.getSaleOrderByDisplayOrderCode(code, channelId);
    }

    @Override
    @Transactional
    public List<SaleOrderItem> getSaleOrderItemsByChannelOrderAndItemCode(String channelSaleOrderCode, String channelSaleOrderItemCode, Integer channelId) {
        return saleOrderDao.getSaleOrderItemsByChannelOrderAndItemCode(channelSaleOrderCode, channelSaleOrderItemCode, channelId);
    }

    @Override
    @Transactional
    public SaleOrder getSaleOrderByCode(String code, Integer channelId) {
        return saleOrderDao.getSaleOrderByCode(code, channelId);
    }

    @Override
    @Transactional(readOnly = true)
    public SaleOrder getSaleOrderByAdditionalInfo(String additionalInfo) {
        return saleOrderDao.getSaleOrderByAdditionalInfo(additionalInfo);
    }

    @Override
    @Transactional
    public AddressDetail addAddressDetail(AddressDetail addressDetail) {
        return saleOrderDao.addAddressDetail(addressDetail);
    }

    @Override
    @Transactional
    public SearchSaleOrderItemResponse searchSaleOrderItems(SearchSaleOrderItemRequest request) {
        SearchSaleOrderItemResponse response = new SearchSaleOrderItemResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            for (SaleOrderItem saleOrderItem : saleOrderDao.searchSaleOrderItems(request)) {
                SearchSaleOrderItemDTO searchSaleOrderItemDTO = new SearchSaleOrderItemDTO();
                ItemType itemType = saleOrderItem.getItemType();
                searchSaleOrderItemDTO.setSaleOrderItemId(saleOrderItem.getId());
                searchSaleOrderItemDTO.setItemName(itemType.getName());
                searchSaleOrderItemDTO.setItemSku(itemType.getSkuCode());
                searchSaleOrderItemDTO.setImageUrl(itemType.getImageUrl());
                searchSaleOrderItemDTO.setCreated(saleOrderItem.getCreated());

                searchSaleOrderItemDTO.setStatus(saleOrderItem.getStatusCode());
                searchSaleOrderItemDTO.setSaleOrderCode(saleOrderItem.getSaleOrder().getCode());
                response.add(searchSaleOrderItemDTO);
            }
            if (request.getSearchOptions() != null && request.getSearchOptions().isGetCount()) {
                Long count = saleOrderDao.getSearchSaleOrderItemRequestCount(request);
                response.setTotalRecords(count);
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Locks({ @Lock(ns = Namespace.SALE_ORDER, key = "#{#args[0].saleOrderCode}") })
    @Transactional
    @LogActivity
    public ModifyPacketSaleOrderResponse modifySaleOrderItems(ModifyPacketSaleOrderRequest request) {
        ModifyPacketSaleOrderResponse response = new ModifyPacketSaleOrderResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            SaleOrder saleOrder = saleOrderDao.getSaleOrderForUpdate(request.getSaleOrderCode());
            List<SaleOrderItem> itemsToModify = new ArrayList<SaleOrderItem>();
            if (saleOrder == null) {
                context.addError(WsResponseCode.INVALID_SALE_ORDER_CODE);
            } else {
                for (SaleOrderItem saleOrderItem : saleOrder.getSaleOrderItems()) {
                    if (request.getSaleOrderItemCodes().remove(saleOrderItem.getCode())) {
                        if (saleOrderItem.isPacketConfigurable()) {
                            itemsToModify.add(saleOrderItem);
                        } else {
                            context.addError(WsResponseCode.INVALID_SALE_ORDER_ITEM_STATE, "SaleOrderItem[" + saleOrderItem.getCode() + "] packet can't be changed in this state");
                        }
                    }
                }
                if (request.getSaleOrderItemCodes().size() > 0) {
                    context.addError(WsResponseCode.INVALID_SALE_ORDER_ITEM_CODE, "Invalid SaleOrderItem codes :[" + StringUtils.join('|', request.getSaleOrderItemCodes()) + "]");
                }
            }
            if (!context.hasErrors()) {
                List<String> saleOrderItemCodes = new ArrayList<>();
                for (SaleOrderItem saleOrderItem : itemsToModify) {
                    saleOrderItem.setPacketNumber(0);
                    saleOrder.setReassessInventory(true);
                    saleOrderDao.updateSaleOrderItem(saleOrderItem);
                    if (ActivityContext.current().isEnable()) {
                        saleOrderItemCodes.add(saleOrderItem.getCode());
                    }
                }
                if (ActivityContext.current().isEnable()) {
                    ActivityUtils.appendActivity(saleOrder.getCode(), ActivityEntityEnum.SALE_ORDER.getName(), null,
                            Arrays.asList(new String[] { "Order items {" + ActivityEntityEnum.SALE_ORDER_ITEM + ":" + saleOrderItemCodes + "} moved to different packet." }),
                            ActivityTypeEnum.EDIT.name());
                }
                response.setSuccessful(true);
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    public VerifySaleOrdersResponse verifySaleOrders(VerifySaleOrdersRequest request) {
        VerifySaleOrdersResponse response = new VerifySaleOrdersResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            for (String saleOrderCode : request.getSaleOrderCodes()) {
                VerifySaleOrderRequest verifySaleOrderRequest = new VerifySaleOrderRequest();
                verifySaleOrderRequest.setSaleOrderCode(saleOrderCode);
                VerifySaleOrderResponse verifySaleOrderResponse = verifySaleOrder(verifySaleOrderRequest);
                if (verifySaleOrderResponse.hasErrors()) {
                    context.addErrors(verifySaleOrderResponse.getErrors());
                }
            }
        }

        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        } else {
            response.setSuccessful(true);
        }
        return response;
    }

    @Override
    @Locks({ @Lock(ns = Namespace.SALE_ORDER, key = "#{#args[0].saleOrderCode}") })
    @Transactional
    @LogActivity
    public VerifySaleOrderResponse verifySaleOrder(VerifySaleOrderRequest request) {
        VerifySaleOrderResponse response = new VerifySaleOrderResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            SaleOrder saleOrder = getSaleOrderForUpdate(request.getSaleOrderCode());
            if (saleOrder == null) {
                context.addError(WsResponseCode.INVALID_SALE_ORDER_CODE, WsResponseCode.INVALID_SALE_ORDER_CODE.message() + ": " + request.getSaleOrderCode());
            } else if (!SaleOrder.StatusCode.PENDING_VERIFICATION.name().equals(saleOrder.getStatusCode())) {
                context.addError(WsResponseCode.INVALID_SALE_ORDER_STATE,
                        WsResponseCode.INVALID_SALE_ORDER_STATE.message() + ": " + request.getSaleOrderCode() + ": Current Status :" + saleOrder.getStatusCode());
            } else {
                String verifyOrderScriptName = Source.VERIFY_ORDER_SCRIPT_NAME;
                ScraperScript verifyOrderScript = CacheManager.getInstance().getCache(ChannelCache.class).getScriptByName(saleOrder.getChannel().getCode(), verifyOrderScriptName);
                if (verifyOrderScript != null) {
                    verifyOrder(context, saleOrder, verifyOrderScript);
                }
                if (!context.hasErrors()) {
                    saleOrder.setStatusCode(SaleOrder.StatusCode.CREATED.name());
                    saleOrder = updateSaleOrder(saleOrder);
                    if (ActivityContext.current().isEnable()) {
                        ActivityUtils.appendActivity(saleOrder.getCode(), ActivityEntityEnum.SALE_ORDER.getName(), null,
                                Arrays.asList(new String[] { "Sale order {" + ActivityEntityEnum.SALE_ORDER + ":" + saleOrder.getCode() + "} verified." }),
                                ActivityTypeEnum.PROCESSING.name());
                    }
                    // Create Shipping Package
                    if ((ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).isInventoryManagementSwitchedOff())) {
                        AllocateInventoryCreatePackageRequest createPackageRequest = new AllocateInventoryCreatePackageRequest();
                        createPackageRequest.setSaleOrderId(saleOrder.getId());
                        saleOrderProcessingService.allocateInventoryAndCreatePackage(createPackageRequest);
                    }
                    response.setSuccessful(true);
                }
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    public void verifyOrder(ValidationContext context, SaleOrder saleOrder, ScraperScript verifyOrderScript) {
        ScriptExecutionContext seContext = ScriptExecutionContext.current();
        for (ChannelConnector channelConnector : saleOrder.getChannel().getChannelConnectors()) {
            for (ChannelConnectorParameter channelConnectorParameter : channelConnector.getChannelConnectorParameters()) {
                seContext.addVariable(channelConnectorParameter.getName(), channelConnectorParameter.getValue());
            }
        }
        for (ChannelConfigurationParameter channelConfigurationParameter : saleOrder.getChannel().getChannelConfigurationParameters()) {
            seContext.addVariable(channelConfigurationParameter.getSourceConfigurationParameterName(), channelConfigurationParameter.getValue());
        }
        seContext.addVariable("channel", saleOrder.getChannel());
        seContext.addVariable("saleOrder", saleOrder);
        seContext.setScriptProvider(new IScriptProvider() {
            @Override
            public ScraperScript getScript(String scriptName) {
                return CacheManager.getInstance().getCache(ScriptVersionedCache.class).getScriptByName(scriptName);
            }
        });
        try {
            seContext.setTraceLoggingEnabled(UserContext.current().isTraceLoggingEnabled());
            verifyOrderScript.execute();
            String verificationStatus = seContext.getScriptOutput();
            if (StringUtils.isNotBlank(verificationStatus)) {
                LOG.error("Unable to verify order", saleOrder.getCode());
                context.addError(WsResponseCode.UNABLE_TO_VERIFY_ORDER, "Unable to verify sale order");
            }
        } catch (Exception e) {
            LOG.error("Unable to verify order", e);
            context.addError(WsResponseCode.UNABLE_TO_VERIFY_ORDER, "Unable to verify sale order " + e.getMessage());
        } finally {
            ScriptExecutionContext.destroy();
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see com.uniware.services.saleorder.ISaleOrderService#
     * getSaleOrderItemForAllocationInPicklist(java.lang.Integer,
     * java.lang.Integer)
     */
    @Override
    @Transactional
    public SaleOrderItem getSaleOrderItemForAllocationInPicklist(String picklistCode, Item item) {
        SaleOrderItem saleOrderItem = saleOrderDao.getSaleOrderItemForAllocationInPicklist(picklistCode, item);
        if (saleOrderItem == null) {
            saleOrderItem = saleOrderDao.getSaleOrderItemForAllocationInPicklist(picklistCode, item, true);
        }
        return saleOrderItem;
    }

    @Override
    public List<SaleOrderItem> getSaleOrderItemsByItemId(Integer itemId) {
        return saleOrderDao.getSaleOrderItemsByItemId(itemId);
    }

    @Override
    @Transactional
    public EditSaleOrderAddressesResponse editSaleOrderAddresses(EditSaleOrderAddressesRequest request) {
        EditSaleOrderAddressesResponse response = new EditSaleOrderAddressesResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            for (SaleOrderAddressDTO addressDTO : request.getAddresses()) {
                AddressDetail address = saleOrderDao.getAddressDetail(addressDTO.getAddressId());
                if (address == null) {
                    context.addError(WsResponseCode.INVALID_ADDRESS_REFERENCE, "Invalid Address Id");
                    break;
                } else {
                    address.setName(addressDTO.getName());
                    address.setAddressLine1(addressDTO.getAddressLine1());
                    address.setAddressLine2(addressDTO.getAddressLine2());
                    address.setCity(addressDTO.getCity());
                    address.setStateCode(addressDTO.getStateCode());
                    address.setPincode(addressDTO.getPincode());
                    address.setPhone(addressDTO.getPhone());
                    address.setCountryCode(addressDTO.getCountryCode());

                    address = saleOrderDao.updateAddressDetail(address);
                    response.getAddresses().add(new WsAddressDetail(address));
                }
            }
            response.setSuccessful(true);
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Locks({ @Lock(ns = Namespace.SALE_ORDER, key = "#{#args[0].saleOrderCode}") })
    @Transactional
    public EditSaleOrderItemResponse editSaleOrderItem(EditSaleOrderItemRequest request) {
        EditSaleOrderItemResponse response = new EditSaleOrderItemResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            SaleOrder saleOrder = saleOrderDao.getSaleOrderForUpdate(request.getSaleOrderCode());
            if (saleOrder == null) {
                context.addError(WsResponseCode.INVALID_SALE_ORDER_CODE, "Invalid sale order code");
            } else {
                SaleOrderItem itemToEdit = null;
                for (SaleOrderItem saleOrderItem : saleOrder.getSaleOrderItems()) {
                    if (saleOrderItem.getCode().equals(request.getSaleOrderItemCode())) {
                        itemToEdit = saleOrderItem;
                        break;
                    }
                }
                if (itemToEdit == null) {
                    context.addError(WsResponseCode.INVALID_SALE_ORDER_CODE, "Invalid Sale Order Item code");
                } else if (!StringUtils.equalsAny(itemToEdit.getStatusCode(), SaleOrderItem.StatusCode.CREATED.name(), SaleOrderItem.StatusCode.UNFULFILLABLE.name(),
                        SaleOrderItem.StatusCode.FULFILLABLE.name())) {
                    context.addError(WsResponseCode.INVALID_SALE_ORDER_ITEM_STATE, "Invalid sale order item state");
                } else if (itemToEdit.getShippingPackage() != null && !StringUtils.equalsAny(itemToEdit.getShippingPackage().getStatusCode(),
                        ShippingPackage.StatusCode.CREATED.name(), ShippingPackage.StatusCode.PICKING.name(), ShippingPackage.StatusCode.PICKED.name())) {
                    context.addError(WsResponseCode.INVALID_SALE_ORDER_ITEM_STATE, "Sale order item is already invoiced");
                } else {
                    itemToEdit.setCashOnDeliveryCharges(request.getCashOnDeliveryCharges());
                    itemToEdit.setGiftWrapCharges(request.getGiftWrapCharges());
                    itemToEdit.setVoucherCode(request.getVoucherCode());
                    itemToEdit.setVoucherValue(request.getVoucherValue());
                    itemToEdit.setStoreCredit(request.getStoreCredit());
                    itemToEdit.setSellingPrice(request.getSellingPrice());
                    itemToEdit.setDiscount(request.getDiscount());
                    itemToEdit.setShippingCharges(request.getShippingCharges());
                    itemToEdit.setShippingMethodCharges(request.getShippingMethodCharges());
                    itemToEdit.setTotalPrice(request.getTotalPrice());
                    if (itemToEdit.getShippingPackage() != null) {
                        ReassessShippingPackageRequest reassessRequest = new ReassessShippingPackageRequest();
                        reassessRequest.setShippingPackageCode(itemToEdit.getShippingPackage().getCode());
                        shippingService.reassessShippingPackage(reassessRequest);
                    }
                    response.setSuccessful(true);
                }
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Locks({ @Lock(ns = Namespace.SALE_ORDER, key = "#{#args[0].saleOrderCode}") })
    @Transactional
    public EditSaleOrderMetadataResponse editSaleOrderMetadata(EditSaleOrderMetadataRequest request) {
        EditSaleOrderMetadataResponse response = new EditSaleOrderMetadataResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            SaleOrder saleOrder = saleOrderDao.getSaleOrderForUpdate(request.getSaleOrderCode());
            if (saleOrder == null) {
                context.addError(WsResponseCode.INVALID_SALE_ORDER_CODE);
            } else {
                if (request.getCustomFieldValues() != null) {
                    CustomFieldUtils.setCustomFieldValues(saleOrder, CustomFieldUtils.getCustomFieldValues(context, SaleOrder.class.getName(), request.getCustomFieldValues()),
                            false);
                }
                if (!context.hasErrors()) {
                    if (request.getPriority() != null) {
                        saleOrder.setPriority(request.getPriority());
                        for (SaleOrderItem saleOrderItem : saleOrder.getSaleOrderItems()) {
                            if (saleOrderItem.getShippingPackage() != null
                                    && ShippingPackage.StatusCode.CREATED.name().equals(saleOrderItem.getShippingPackage().getStatusCode())) {
                                saleOrderItem.getShippingPackage().setPriority(request.getPriority());
                            }
                        }
                    }
                    saleOrderDao.updateSaleOrder(saleOrder);
                    response.setSuccessful(true);
                }
            }
        }
        response.setWarnings(context.getWarnings());
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    public EditSaleOrderItemMetadataResponse editSaleOrderItemMetadata(EditSaleOrderItemMetadataRequest request) {
        EditSaleOrderItemMetadataResponse response = new EditSaleOrderItemMetadataResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            SaleOrderItem saleOrderItem = saleOrderDao.getSaleOrderItemByCode(request.getSaleOrderCode(), request.getSaleOrderItemCode());
            if (saleOrderItem == null) {
                context.addError(WsResponseCode.INVALID_SALE_ORDER_ITEM_CODE, "Invalid sale order item/sale order code");
            } else {
                if (request.getCustomFieldValues() != null) {
                    CustomFieldUtils.setCustomFieldValues(saleOrderItem,
                            CustomFieldUtils.getCustomFieldValues(context, SaleOrderItem.class.getName(), request.getCustomFieldValues()), false);
                }
                if (!context.hasErrors()) {
                    saleOrderDao.updateSaleOrderItem(saleOrderItem);
                    response.setSuccessful(true);
                }
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @RollbackOnFailure
    @Override
    @Transactional
    public CreateSaleOrderAlternateResponse createSaleOrderAlternate(CreateSaleOrderAlternateRequest request) {
        CreateSaleOrderAlternateResponse response = new CreateSaleOrderAlternateResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            for (CreateSaleOrderAlternateRequest.WsSOItemAlternate wsSOItemAlternate : request.getSaleOrderItemAlternateList()) {
                CreateSaleOrderItemAlternateRequest createSaleOrderItemAlternateRequest = new CreateSaleOrderItemAlternateRequest();
                createSaleOrderItemAlternateRequest.setUserId(request.getUserId());

                com.uniware.core.api.saleorder.CreateSaleOrderItemAlternateRequest.WsSaleOrderItem wsSaleOrderItem = new CreateSaleOrderItemAlternateRequest.WsSaleOrderItem();
                wsSaleOrderItem.setSaleOrderCode(request.getSaleOrderCode());
                wsSaleOrderItem.setSaleOrderItemCode(wsSOItemAlternate.getSaleOrderItemCode());
                List<com.uniware.core.api.saleorder.CreateSaleOrderItemAlternateRequest.WsSaleOrderItem> wsSaleOrderItems = new ArrayList<>();
                wsSaleOrderItems.add(wsSaleOrderItem);
                createSaleOrderItemAlternateRequest.setSaleOrderItems(wsSaleOrderItems);

                List<WsSaleOrderItemAlternate> wsSaleOrderItemAlternateList = wsSOItemAlternate.getSaleOrderItemAlternates();
                createSaleOrderItemAlternateRequest.setSaleOrderItemAlternates(wsSaleOrderItemAlternateList);

                CreateSaleOrderItemAlternateResponse createSaleOrderItemAlternateResponse = createSaleOrderItemAlternate(createSaleOrderItemAlternateRequest);

                if (createSaleOrderItemAlternateResponse.hasErrors()) {
                    context.addErrors(createSaleOrderItemAlternateResponse.getErrors());
                } else {
                    response.getSuccessfulSaleOrderItemCodes().add(wsSOItemAlternate.getSaleOrderItemCode());
                }
            }
        }
        if (!context.hasErrors()) {
            response.setSuccessful(true);
        } else {
            response.addErrors(context.getErrors());
            response.getSuccessfulSaleOrderItemCodes().clear();
        }
        return response;
    }

    @Override
    @Transactional
    public CreateSaleOrderItemAlternateResponse createSaleOrderItemAlternate(CreateSaleOrderItemAlternateRequest request) {
        CreateSaleOrderItemAlternateResponse response = new CreateSaleOrderItemAlternateResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            SaleOrderItemAlternate saleOrderItemAlternate = createSaleOrderItemAlternate(new User(request.getUserId()), request, context);
            if (!context.hasErrors()) {
                for (com.uniware.core.api.saleorder.CreateSaleOrderItemAlternateRequest.WsSaleOrderItem wsSaleOrderItem : request.getSaleOrderItems()) {
                    SaleOrderItem saleOrderItem = saleOrderDao.getSaleOrderItemByCode(wsSaleOrderItem.getSaleOrderCode(), wsSaleOrderItem.getSaleOrderItemCode());
                    if (saleOrderItem == null) {
                        context.addError(WsResponseCode.INVALID_SALE_ORDER_ITEM_CODE);
                    } else {
                        updateSaleOrderItemAlternate(response, saleOrderItem, context, saleOrderItemAlternate);
                    }
                }
            }
            if (!context.hasErrors()) {
                response.setSuccessful(true);
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public SaleOrderItemAlternate createSaleOrderItemAlternate(User user, CreateSaleOrderItemAlternateRequest request, ValidationContext context) {
        SaleOrderItemAlternate saleOrderItemAlternate = new SaleOrderItemAlternate();
        saleOrderItemAlternate.setStatusCode(SaleOrderItemAlternate.StatusCode.CREATED.name());
        saleOrderItemAlternate.setCreated(DateUtils.getCurrentTime());
        saleOrderItemAlternate.setUser(user);
        for (WsSaleOrderItemAlternate wsSaleOrderItemAlternate : request.getSaleOrderItemAlternates()) {
            ItemType itemType = catalogService.getItemTypeBySkuCode(wsSaleOrderItemAlternate.getItemSku());
            if (itemType == null) {
                context.addError(WsResponseCode.INVALID_ITEM_TYPE, "Invalid item type selected");
            } else {
                SaleOrderItemAlternateSuggestion alternateSuggestion = new SaleOrderItemAlternateSuggestion();
                alternateSuggestion.setAmountDifference(wsSaleOrderItemAlternate.getAmountDifference());
                alternateSuggestion.setItemType(itemType);
                alternateSuggestion.setSaleOrderItemAlternate(saleOrderItemAlternate);
                alternateSuggestion.setCreated(DateUtils.getCurrentTime());
                saleOrderItemAlternate.getSaleOrderItemAlternateSuggestions().add(alternateSuggestion);
            }
        }
        if (!context.hasErrors()) {
            saleOrderItemAlternate = saleOrderDao.addSaleOrderItemAlternate(saleOrderItemAlternate);
        }
        return saleOrderItemAlternate;
    }

    private void updateSaleOrderItemAlternate(CreateSaleOrderItemAlternateResponse response, SaleOrderItem saleOrderItem, ValidationContext context,
            SaleOrderItemAlternate saleOrderItemAlternate) {
        SaleOrder so = getSaleOrderById(saleOrderItem.getSaleOrder().getId());
        doUpdateSaleOrderItemAlternate(response, saleOrderItem, context, saleOrderItemAlternate, so.getCode());
    }

    @Locks({ @Lock(ns = Namespace.SALE_ORDER, key = "#{#args[4]}") })
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    private void doUpdateSaleOrderItemAlternate(CreateSaleOrderItemAlternateResponse response, SaleOrderItem saleOrderItem, ValidationContext context,
            SaleOrderItemAlternate saleOrderItemAlternate, String saleOrderCode) {
        saleOrderDao.getSaleOrderForUpdate(saleOrderItem.getSaleOrder().getId());
        saleOrderItem = saleOrderDao.getSaleOrderItemById(saleOrderItem.getId(), true);
        if (!SaleOrderItem.StatusCode.UNFULFILLABLE.name().equals(saleOrderItem.getStatusCode())) {
            context.addError(WsResponseCode.INVALID_SALE_ORDER_ITEM_STATE, "SaleOrderItem:" + saleOrderItem.getCode() + " not in UNFULFILLABLE state");
        } else if (StringUtils.isNotBlank(saleOrderItem.getCombinationIdentifier())) {
            context.addError(WsResponseCode.INVALID_SALE_ORDER_ITEM_STATE, "SaleOrderItem:" + saleOrderItem.getCode() + " is part of a COMBO");
        } else {
            saleOrderItem.setStatusCode(SaleOrderItem.StatusCode.ALTERNATE_SUGGESTED.name());
            saleOrderItem.setSaleOrderItemAlternate(saleOrderItemAlternate);
            response.getSuccessfulSaleOrderItemCodes().add(saleOrderItem.getCode());
        }
    }

    @RollbackOnFailure
    @Override
    @Transactional
    public AcceptSaleOrderAlternateResponse acceptSaleOrderAlternate(AcceptSaleOrderAlternateRequest request) {
        AcceptSaleOrderAlternateResponse response = new AcceptSaleOrderAlternateResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            for (AcceptSaleOrderAlternateRequest.WsSaleOrderItemAlternatePair wsSaleOrderItemAlternatePair : request.getSaleOrderItemAlternatePairs()) {
                AcceptSaleOrderItemAlternateRequest acceptSaleOrderItemAlternateRequest = new AcceptSaleOrderItemAlternateRequest();

                acceptSaleOrderItemAlternateRequest.setSaleOrderCode(request.getSaleOrderCode());

                List<String> saleOrderItemCodes = new ArrayList<>();
                saleOrderItemCodes.add(wsSaleOrderItemAlternatePair.getSaleOrderItemCode());
                acceptSaleOrderItemAlternateRequest.setSaleOrderItemCodes(saleOrderItemCodes);

                acceptSaleOrderItemAlternateRequest.setSelectedAlternateItemSku(wsSaleOrderItemAlternatePair.getSelectedAlternateItemSku());

                AcceptSaleOrderItemAlternateResponse acceptSaleOrderItemAlternateResponse = acceptSaleOrderItemAlternate(acceptSaleOrderItemAlternateRequest);

                if (acceptSaleOrderItemAlternateResponse.hasErrors()) {
                    context.addErrors(acceptSaleOrderItemAlternateResponse.getErrors());
                } else {
                    response.getSuccessfulSaleOrderItemCodes().add(wsSaleOrderItemAlternatePair.getSaleOrderItemCode());
                }
            }
        }
        if (!context.hasErrors()) {
            response.setSuccessful(true);
        } else {
            response.addErrors(context.getErrors());
            response.getSuccessfulSaleOrderItemCodes().clear();
        }
        return response;
    }

    @Override
    @Locks({ @Lock(ns = Namespace.SALE_ORDER, key = "#{#args[0].saleOrderCode}") })
    @Transactional
    public AcceptSaleOrderItemAlternateResponse acceptSaleOrderItemAlternate(AcceptSaleOrderItemAlternateRequest request) {
        AcceptSaleOrderItemAlternateResponse response = new AcceptSaleOrderItemAlternateResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            SaleOrder saleOrder = saleOrderDao.getSaleOrderForUpdate(request.getSaleOrderCode());
            ItemType itemType = catalogService.getItemTypeBySkuCode(request.getSelectedAlternateItemSku());
            if (saleOrder == null) {
                context.addError(WsResponseCode.INVALID_SALE_ORDER_CODE);
            } else if (itemType == null) {
                context.addError(WsResponseCode.INVALID_ITEM_TYPE, "Invalid item type sku");
            } else {
                List<SaleOrderItem> itemsToAlter = new ArrayList<SaleOrderItem>();
                Integer saleOrderItemAlternateId = null;
                for (SaleOrderItem saleOrderItem : saleOrder.getSaleOrderItems()) {
                    if (request.getSaleOrderItemCodes().remove(saleOrderItem.getCode())) {
                        if (saleOrderItem.getSaleOrderItemAlternate() == null
                                || (saleOrderItemAlternateId != null && !saleOrderItem.getSaleOrderItemAlternate().getId().equals(saleOrderItemAlternateId))
                                || !SaleOrderItem.StatusCode.ALTERNATE_SUGGESTED.name().equals(saleOrderItem.getStatusCode())) {
                            context.addError(WsResponseCode.INVALID_SALE_ORDER_ITEM_ALTERNATE_SELECTION,
                                    "Invalid sale order item alternate selection - state : " + saleOrderItem.getStatusCode());
                        } else {
                            saleOrderItemAlternateId = saleOrderItem.getSaleOrderItemAlternate().getId();
                            itemsToAlter.add(saleOrderItem);
                        }
                    }
                }
                if (request.getSaleOrderItemCodes().size() > 0) {
                    context.addError(WsResponseCode.INVALID_SALE_ORDER_ITEM_CODE, "Invalid SaleOrderItem codes :[" + StringUtils.join('|', request.getSaleOrderItemCodes()) + "]");
                }
                if (!context.hasErrors()) {
                    SaleOrderItemAlternate saleOrderItemAlternate = saleOrderDao.getSaleOrderItemAlternateById(saleOrderItemAlternateId);
                    SaleOrderItemAlternateSuggestion selectedAlternate = null;
                    for (SaleOrderItemAlternateSuggestion suggestion : saleOrderItemAlternate.getSaleOrderItemAlternateSuggestions()) {
                        if (suggestion.getItemType().getId().equals(itemType.getId())) {
                            selectedAlternate = suggestion;
                            break;
                        }
                    }
                    if (selectedAlternate == null) {
                        context.addError(WsResponseCode.INVALID_SALE_ORDER_ITEM_ALTERNATE_SELECTION, "Invalid sale order item alternate selection");
                    } else {
                        for (SaleOrderItem saleOrderItem : itemsToAlter) {
                            saleOrderItem.setStatusCode(SaleOrderItem.StatusCode.ALTERNATE_ACCEPTED.name());
                            SaleOrderItem alternateSaleOrderItem = prepareAlternateSaleOrderItem(saleOrderItem, selectedAlternate);
                            alternateSaleOrderItem.setAlteredSaleOrderItem(saleOrderItem);
                            alternateSaleOrderItem = saleOrderDao.addSaleOrderItem(alternateSaleOrderItem);
                            saleOrder.getSaleOrderItems().add(alternateSaleOrderItem);
                        }
                        response.setSuccessful(true);
                    }
                }
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    private SaleOrderItem prepareAlternateSaleOrderItem(SaleOrderItem saleOrderItem, SaleOrderItemAlternateSuggestion selectedAlternate) {
        SaleOrderItem alternateSaleOrderItem = new SaleOrderItem(saleOrderItem);
        alternateSaleOrderItem.setSellingPrice(alternateSaleOrderItem.getSellingPrice().add(selectedAlternate.getAmountDifference()));
        alternateSaleOrderItem.setTotalPrice(alternateSaleOrderItem.getTotalPrice().add(selectedAlternate.getAmountDifference()));
        alternateSaleOrderItem.setTransferPrice(null);
        alternateSaleOrderItem.setItemType(selectedAlternate.getItemType());
        Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(saleOrderItem.getSaleOrder().getChannel().getSourceCode());
        Set<String> itemDetailFieldNames = CacheManager.getInstance().getCache(ItemTypeDetailCache.class).getItemDetailFieldNames(selectedAlternate.getItemType());
        if (itemDetailFieldNames != null && !source.isFetchCompleteOrders()) {
            alternateSaleOrderItem.setItemDetailFields(StringUtils.join(itemDetailFieldNames));
            alternateSaleOrderItem.setItemDetailingStatus(ItemDetailingStatus.PENDING);
        } else {
            alternateSaleOrderItem.setItemDetailingStatus(ItemDetailingStatus.NOT_REQUIRED);
        }
        alternateSaleOrderItem.setSaleOrder(saleOrderItem.getSaleOrder());
        alternateSaleOrderItem.setCode(saleOrderItem.getCode() + "-A");
        return alternateSaleOrderItem;
    }

    @Override
    @Transactional
    public GetSaleOrderAlternateResponse getSaleOrderAlternate(GetSaleOrderAlternateRequest request) {
        GetSaleOrderAlternateResponse response = new GetSaleOrderAlternateResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            for (Integer saleOrderItemAlternateId : request.getSaleOrderItemAlternateList()) {
                GetSaleOrderItemAlternateRequest getSaleOrderItemAlternateRequest = new GetSaleOrderItemAlternateRequest();
                getSaleOrderItemAlternateRequest.setSaleOrderItemAlternateId(saleOrderItemAlternateId);

                GetSaleOrderItemAlternateResponse getSaleOrderItemAlternateResponse = getSaleOrderItemAlternateRequest(getSaleOrderItemAlternateRequest);

                if (!response.hasErrors()) {
                    response.getSaleOrderItemAlternateDTOList().add(
                            new GetSaleOrderAlternateResponse.WsSOIAlternate(saleOrderItemAlternateId, getSaleOrderItemAlternateResponse.getSaleOrderItemAlternateDTO()));
                } else {
                    context.addError(WsResponseCode.UNKNOWN_ERROR);
                }
            }
        }
        if (context.hasErrors()) {
            response.addErrors(context.getErrors());
        } else {
            response.setSuccessful(true);
        }
        return response;
    }

    @Override
    @Transactional
    public GetSaleOrderItemAlternateResponse getSaleOrderItemAlternateRequest(GetSaleOrderItemAlternateRequest request) {
        GetSaleOrderItemAlternateResponse response = new GetSaleOrderItemAlternateResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            SaleOrderItemAlternate saleOrderItemAlternate = saleOrderDao.getSaleOrderItemAlternateById(request.getSaleOrderItemAlternateId());
            if (saleOrderItemAlternate == null) {
                context.addError(WsResponseCode.INVALID_REQUEST);
            } else {
                SaleOrderItemAlternateDTO saleOrderItemAlternateDTO = new SaleOrderItemAlternateDTO();
                saleOrderItemAlternateDTO.setStatusCode(saleOrderItemAlternate.getStatusCode());
                for (SaleOrderItemAlternateSuggestion suggestion : saleOrderItemAlternate.getSaleOrderItemAlternateSuggestions()) {
                    SaleOrderItemAlternateSuggestionDTO suggestionDTO = new SaleOrderItemAlternateSuggestionDTO();
                    suggestionDTO.setAmountDifference(suggestion.getAmountDifference());
                    suggestionDTO.setItemTypeId(suggestion.getItemType().getId());
                    suggestionDTO.setItemTypeName(suggestion.getItemType().getName());
                    suggestionDTO.setItemTypeSKU(suggestion.getItemType().getSkuCode());
                    saleOrderItemAlternateDTO.addSaleOrderItemAlternateSuggestionDTO(suggestionDTO);
                }
                response.setSaleOrderItemAlternateDTO(saleOrderItemAlternateDTO);
                response.setSuccessful(true);
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    public List<Integer> getSaleOrdersForFacilityAllocation() {
        return saleOrderDao.getSaleOrdersForFacilityAllocation();
    }

    @Override
    @Transactional
    public List<String> getSaleOrderCodesForFacilityAllocation() {
        return saleOrderDao.getSaleOrderCodesForFacilityAllocation();
    }

    @Override
    @Transactional
    @LogActivity
    public SwitchSaleOrderItemFacilityResponse switchSaleOrderItemFacility(SwitchSaleOrderItemFacilityRequest request) {
        SwitchSaleOrderItemFacilityResponse response = new SwitchSaleOrderItemFacilityResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            Facility facility = CacheManager.getInstance().getCache(FacilityCache.class).getFacilityByCode(request.getFacilityCode());
            if (facility == null) {
                context.addError(WsResponseCode.INVALID_FACILITY_CODE, "Invalid facility code");
            }
            List<Integer> saleOrderItemIds = new ArrayList<Integer>();
            for (String saleOrderItemCode : request.getSaleOrderItemCodes()) {
                SaleOrderItem saleOrderItem = saleOrderDao.getSaleOrderItemByCode(request.getSaleOrderCode(), saleOrderItemCode);
                saleOrderItemIds.add(saleOrderItem.getId());
            }
            Map<Integer, Set<Integer>> saleOrderItemsToSwitch = new HashMap<Integer, Set<Integer>>();
            for (Integer saleOrderItemId : saleOrderItemIds) {
                SaleOrderItem saleOrderItem = saleOrderDao.getSaleOrderItemById(saleOrderItemId);
                if (saleOrderItem == null) {
                    context.addError(WsResponseCode.INVALID_SALE_ORDER_ITEM_ID);
                } else {
                    if (saleOrderItem.getFacility() == null || !saleOrderItem.getFacility().getId().equals(facility.getId())) {
                        Set<Integer> saleOrderItems = saleOrderItemsToSwitch.get(saleOrderItem.getSaleOrder().getId());
                        if (saleOrderItems == null) {
                            saleOrderItems = new HashSet<Integer>();
                            saleOrderItemsToSwitch.put(saleOrderItem.getSaleOrder().getId(), saleOrderItems);
                        }
                        saleOrderItems.add(saleOrderItem.getId());
                    }
                }
            }
            if (!context.hasErrors()) {
                for (Map.Entry<Integer, Set<Integer>> entry : saleOrderItemsToSwitch.entrySet()) {
                    switchSaleOrderItemFacility(response, entry.getKey(), entry.getValue(), context, facility);
                }
            }
            if (!context.hasErrors()) {
                response.setSuccessful(true);
            }

        }
        response.setErrors(context.getErrors());
        return response;
    }

    public void switchSaleOrderItemFacility(SwitchSaleOrderItemFacilityResponse response, Integer saleOrderId, Set<Integer> saleOrderItems, ValidationContext context,
            Facility facility) {
        SaleOrder saleOrder = getSaleOrderById(saleOrderId);
        doSwitchSaleOrderItemFacility(response, saleOrder.getCode(), saleOrderItems, context, facility);
    }

    @Locks({ @Lock(ns = Namespace.SALE_ORDER, key = "#{#args[1]}") })
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    private void doSwitchSaleOrderItemFacility(SwitchSaleOrderItemFacilityResponse response, String saleOrderCode, Set<Integer> saleOrderItems, ValidationContext context,
            Facility facility) {
        SaleOrder saleOrder = saleOrderDao.getSaleOrderForUpdate(saleOrderCode);
        List<SaleOrderItem> saleOrderItemsToSwitch = new ArrayList<>();
        List<String> saleOrderItemsSwitched = new ArrayList<String>();
        for (SaleOrderItem saleOrderItem : saleOrder.getSaleOrderItems()) {
            if (saleOrderItems.contains(saleOrderItem.getId())) {
                if (!(StringUtils.equalsAny(saleOrderItem.getStatusCode(), SaleOrderItem.StatusCode.UNFULFILLABLE.name(), SaleOrderItem.StatusCode.LOCATION_NOT_SERVICEABLE.name(),
                        SaleOrderItem.StatusCode.CREATED.name())
                        || (SaleOrderItem.StatusCode.FULFILLABLE.name().equals(saleOrderItem.getStatusCode())
                                && (saleOrderItem.getShippingPackage() == null || StringUtils.equalsAny(saleOrderItem.getShippingPackage().getStatusCode(),
                                        ShippingPackage.StatusCode.CREATED.name(), ShippingPackage.StatusCode.LOCATION_NOT_SERVICEABLE.name()))))) {
                    context.addError(WsResponseCode.INVALID_SALE_ORDER_ITEM_STATE,
                            "SaleOrderItem:" + saleOrderItem.getCode() + " not in CREATED/UNFULFILLABLE/LOCATION_NOT_SERVICEABLE state");
                } else {
                    saleOrderItemsToSwitch.add(saleOrderItem);
                }
            }
        }
        if (!context.hasErrors()) {
            Map<Integer, Integer> releaseInventoryMap = new HashMap<>();
            Map<String, List<String>> shippingPackageToSOI = new HashMap<>();
            for (SaleOrderItem saleOrderItem : saleOrderItemsToSwitch) {
                if (SaleOrderItem.StatusCode.FULFILLABLE.name().equals(saleOrderItem.getStatusCode())) {
                    if (saleOrderItem.getItemTypeInventory() != null) {
                        if (!releaseInventoryMap.containsKey(saleOrderItem.getItemTypeInventory().getId())) {
                            releaseInventoryMap.put(saleOrderItem.getItemTypeInventory().getId(), 0);
                        }
                        releaseInventoryMap.put(saleOrderItem.getItemTypeInventory().getId(), releaseInventoryMap.get(saleOrderItem.getItemTypeInventory().getId()) + 1);
                        saleOrderItem.setItemTypeInventory(null);
                        if (saleOrderItem.getItem() != null) {
                            saleOrderItem.getItem().setStatusCode(Item.StatusCode.GOOD_INVENTORY.name());
                            saleOrderItem.setItem(null);
                        }
                    }
                    if (saleOrderItem.getShippingPackage() != null) {
                        ShippingPackage shippingPackage = shippingService.getShippingPackageById(saleOrderItem.getShippingPackage().getId());
                        shippingPackage.setStatusCode(ShippingPackage.StatusCode.CANCELLED.name());
                        shippingPackage.setCancellationTime(DateUtils.getCurrentTime());
                        if (ActivityContext.current().isEnable()) {
                            if (!shippingPackageToSOI.containsKey(shippingPackage.getCode())) {
                                shippingPackageToSOI.put(shippingPackage.getCode(), new ArrayList<String>());
                            }
                            shippingPackageToSOI.get(shippingPackage.getCode()).add(saleOrderItem.getCode());
                        }
                        for (SaleOrderItem orderItem : saleOrder.getSaleOrderItems()) {
                            if (orderItem.getShippingPackage() != null && shippingPackage.getId().equals(orderItem.getShippingPackage().getId())) {
                                orderItem.setShippingPackage(null);
                            }
                        }
                    } else {
                        if (!shippingPackageToSOI.containsKey(StringUtils.EMPTY_STRING)) {
                            shippingPackageToSOI.put(StringUtils.EMPTY_STRING, new ArrayList<String>());
                        }
                        shippingPackageToSOI.get(StringUtils.EMPTY_STRING).add(saleOrderItem.getCode());
                    }
                }
                saleOrder.setReassessInventory(true);
                saleOrderItem.setStatusCode(SaleOrderItem.StatusCode.CREATED.name());
                saleOrderItem.setFacility(facility);
                saleOrderItemsSwitched.add(saleOrderItem.getCode());

            }
            if (ActivityContext.current().isEnable()) {
                ActivityUtils.appendActivity(saleOrder.getCode(), ActivityEntityEnum.SALE_ORDER_ITEM.getName(), saleOrder.getCode(),
                        Arrays.asList(new String[] {
                                "Sale order item {" + ActivityEntityEnum.SALE_ORDER_ITEM + ":" + shippingPackageToSOI.values() + "} facility switched to " + facility.getCode()
                                        + ". Shipping packages {" + ActivityEntityEnum.SHIPPING_PACKAGE + ":" + shippingPackageToSOI.keySet() + "} marked cancelled." }),
                        ActivityTypeEnum.EDIT.name());
            }
            for (Map.Entry<Integer, Integer> entry : releaseInventoryMap.entrySet()) {
                inventoryService.releaseInventory(entry.getKey(), saleOrder.getChannel().getId(), entry.getValue());
            }
            response.setSuccessfulSaleOrderItemCodes(saleOrderItemsSwitched);
        }
    }

    @Override
    @Locks({ @Lock(ns = Namespace.SALE_ORDER, key = "#{#args[0].saleOrderCode}") })
    @Transactional
    @LogActivity
    public UnblockSaleOrderItemsInventoryResponse unblockSaleOrderItemsInventory(UnblockSaleOrderItemsInventoryRequest request) {
        UnblockSaleOrderItemsInventoryResponse response = new UnblockSaleOrderItemsInventoryResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            SaleOrder saleOrder = saleOrderDao.getSaleOrderForUpdate(request.getSaleOrderCode());
            if (saleOrder == null) {
                context.addError(WsResponseCode.INVALID_SALE_ORDER_CODE);
            } else {
                List<SaleOrderItem> itemsToUnblock = new ArrayList<SaleOrderItem>();
                for (SaleOrderItem saleOrderItem : saleOrder.getSaleOrderItems()) {
                    if (request.getSaleOrderItemCodes().remove(saleOrderItem.getCode())) {
                        if (!(SaleOrderItem.StatusCode.FULFILLABLE.name().equals(saleOrderItem.getStatusCode())
                                && (saleOrderItem.getShippingPackage() == null || StringUtils.equalsAny(saleOrderItem.getShippingPackage().getStatusCode(),
                                        ShippingPackage.StatusCode.CREATED.name(), ShippingPackage.StatusCode.LOCATION_NOT_SERVICEABLE.name())))) {
                            context.addError(WsResponseCode.INVALID_SALE_ORDER_ITEM_STATE,
                                    "Inventory for SaleOrderItem[" + saleOrderItem.getCode() + "] can't be unblocked in this state");
                        } else {
                            itemsToUnblock.add(saleOrderItem);
                        }
                    }
                }
                if (request.getSaleOrderItemCodes().size() > 0) {
                    context.addError(WsResponseCode.INVALID_SALE_ORDER_ITEM_CODE, "Invalid SaleOrderItem codes :[" + StringUtils.join('|', request.getSaleOrderItemCodes()) + "]");
                }
                if (!context.hasErrors()) {
                    Set<Integer> affectedPackages = new HashSet<Integer>();
                    Map<Integer, Integer> releaseInventoryMap = new HashMap<Integer, Integer>();
                    List<String> soiCodes = new ArrayList<>();
                    for (SaleOrderItem saleOrderItem : itemsToUnblock) {
                        if (!releaseInventoryMap.containsKey(saleOrderItem.getItemTypeInventory().getId())) {
                            releaseInventoryMap.put(saleOrderItem.getItemTypeInventory().getId(), 0);
                        }
                        releaseInventoryMap.put(saleOrderItem.getItemTypeInventory().getId(), releaseInventoryMap.get(saleOrderItem.getItemTypeInventory().getId()) + 1);

                        saleOrderItem.setItemTypeInventory(null);
                        saleOrderItem.setStatusCode(SaleOrderItem.StatusCode.CREATED.name());
                        if (saleOrderItem.getShippingPackage() != null) {
                            affectedPackages.add(saleOrderItem.getShippingPackage().getId());
                            saleOrderItem.setShippingPackage(null);
                        }
                        if (saleOrderItem.getItem() != null) {
                            saleOrderItem.getItem().setStatusCode(Item.StatusCode.GOOD_INVENTORY.name());
                            saleOrderItem.setItem(null);
                        }
                        soiCodes.add(saleOrderItem.getCode());
                    }
                    if (ActivityContext.current().isEnable() && itemsToUnblock.size() > 0) {
                        ActivityUtils.appendActivity(saleOrder.getCode(), ActivityEntityEnum.SALE_ORDER_ITEM.getName(), saleOrder.getCode(),
                                Arrays.asList(
                                        new String[] { "Released inventory and unassigned package for Order Items {" + ActivityEntityEnum.SALE_ORDER_ITEM + ":" + soiCodes + "}" }),
                                ActivityTypeEnum.PROCESSING.name());
                    }
                    for (Map.Entry<Integer, Integer> entry : releaseInventoryMap.entrySet()) {
                        inventoryService.releaseInventory(entry.getKey(), saleOrder.getChannel().getId(), entry.getValue());
                    }
                    for (SaleOrderItem saleOrderItem : saleOrder.getSaleOrderItems()) {
                        if (saleOrderItem.getShippingPackage() != null && affectedPackages.contains(saleOrderItem.getShippingPackage().getId())) {
                            saleOrderItem.setShippingPackage(null);
                        }
                    }
                    for (Integer shippingPackageId : affectedPackages) {
                        ShippingPackage shippingPackage = shippingService.getShippingPackageById(shippingPackageId);
                        shippingPackage.setStatusCode(ShippingPackage.StatusCode.CANCELLED.name());
                        shippingPackage.setCancellationTime(DateUtils.getCurrentTime());
                        if (ActivityContext.current().isEnable()) {
                            ActivityUtils.appendActivity(shippingPackage.getCode(), ActivityEntityEnum.SHIPPING_PACKAGE.getName(), shippingPackage.getSaleOrder().getCode(),
                                    Arrays.asList(new String[] { "Shipping package {" + ActivityEntityEnum.SHIPPING_PACKAGE + ":" + shippingPackage.getCode() + "} CANCELLED" }),
                                    ActivityTypeEnum.CANCEL.name());
                        }
                    }
                    response.setSuccessful(true);
                }
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Locks({ @Lock(ns = Namespace.SALE_ORDER, key = "#{#args[0].saleOrderAddress.saleOrderCode}") })
    @Transactional
    @RollbackOnFailure
    public EditSaleOrderAddressResponse editSaleOrderAddress(EditSaleOrderAddressRequest request) {
        EditSaleOrderAddressResponse response = new EditSaleOrderAddressResponse();
        ValidationContext context = request.validate();
        SaleOrder saleOrder = saleOrderDao.getSaleOrderForUpdate(request.getSaleOrderAddress().getSaleOrderCode());
        if (saleOrder == null) {
            context.addError(WsResponseCode.INVALID_SALE_ORDER_CODE, "Invalid Sale Order Code");
        } else {
            Channel channel = saleOrder.getChannel();
            context = validateEditSaleOrderAddressRequest(request, context, channel.isThirdPartyShipping());
            if (!context.hasErrors()) {
                WsSaleOrderAddress wsSaleOrderAddress = request.getSaleOrderAddress();
                Map<String, SaleOrderItem> codeToSaleOrderItem = new HashMap<String, SaleOrderItem>();
                if (request.getSaleOrderAddress().getSaleOrderAddressItems() != null) {
                    Map<String, WsSaleOrderAddressItem> codeToWsSaleOrderAddressItems = new HashMap<String, WsSaleOrderAddressItem>();
                    Map<String, List<WsSaleOrderAddressItem>> packageCodeToSaleOrderItems = new HashMap<String, List<WsSaleOrderAddressItem>>();
                    for (WsSaleOrderAddressItem saleOrderAddressItem : request.getSaleOrderAddress().getSaleOrderAddressItems()) {
                        codeToWsSaleOrderAddressItems.put(saleOrderAddressItem.getSaleOrderItemCode(), saleOrderAddressItem);
                    }
                    for (SaleOrderItem saleOrderItem : saleOrder.getSaleOrderItems()) {
                        if (codeToWsSaleOrderAddressItems.get(saleOrderItem.getCode()) != null) {
                            if (saleOrderItem.getShippingPackage() != null) {
                                List<WsSaleOrderAddressItem> packageItems = packageCodeToSaleOrderItems.computeIfAbsent(saleOrderItem.getShippingPackage().getCode(),
                                        k -> new ArrayList<WsSaleOrderAddressItem>());
                                packageItems.add(codeToWsSaleOrderAddressItems.get(saleOrderItem.getCode()));
                            }
                            codeToWsSaleOrderAddressItems.remove(saleOrderItem.getCode());
                        }
                        codeToSaleOrderItem.put(saleOrderItem.getCode(), saleOrderItem);
                    }
                    if (codeToWsSaleOrderAddressItems.size() > 0) {
                        context.addError(WsResponseCode.INVALID_SALE_ORDER_ITEM_CODE,
                                "Invalid SaleOrderItem codes :[" + StringUtils.join('|', codeToWsSaleOrderAddressItems.keySet()) + "]");
                    }

                    if (!context.hasErrors()) {
                        for (Map.Entry<String, List<WsSaleOrderAddressItem>> entry : packageCodeToSaleOrderItems.entrySet()) {
                            ShippingPackage shippingPackage = shippingService.getShippingPackageByCode(entry.getKey());
                            List<WsSaleOrderAddressItem> wsSaleOrderAddressItems = entry.getValue();
                            if (wsSaleOrderAddressItems.size() < shippingPackage.getSaleOrderItems().size()) {
                                context.addError(WsResponseCode.INVALID_REQUEST,
                                        "Address can only be changed for all items in same package or none. Please add/remove all items of package: " + entry.getKey());
                            } else {
                                String referenceId = null;
                                for (WsSaleOrderAddressItem wsSaleOrderAddressItem : wsSaleOrderAddressItems) {
                                    String currentReferenceId = wsSaleOrderAddressItem.getShippingAddress().getReferenceId();
                                    if (referenceId != null && !referenceId.equals(currentReferenceId)) {
                                        context.addError(WsResponseCode.INVALID_ADDRESS_REFERENCE, "SaleOrderItem: " + wsSaleOrderAddressItem.getSaleOrderItemCode()
                                                + " should have same address reference as of other items in same package:" + entry.getKey());
                                        break;
                                    }
                                    referenceId = currentReferenceId;
                                }
                            }
                        }
                    }
                }

                if (!context.hasErrors()) {
                    Map<String, AddressDetail> addressMap = new HashMap<String, AddressDetail>();
                    for (WsAddressDetail address : request.getSaleOrderAddress().getAddresses()) {
                        addressMap.put(address.getId(), prepareAddress(address, context));
                    }

                    if (wsSaleOrderAddress.getBillingAddress() != null) {
                        AddressDetail billingAddress = addressMap.get(wsSaleOrderAddress.getBillingAddress().getReferenceId());
                        billingAddress = saleOrderDao.addAddressDetail(billingAddress);
                        saleOrder.setBillingAddress(billingAddress);
                    }

                    AddressDetail shippingAddress = addressMap.get(request.getSaleOrderAddress().getShippingAddress().getReferenceId());
                    if (shippingAddress != null) {
                        if (request.getSaleOrderAddress().getSaleOrderAddressItems() != null) {
                            for (WsSaleOrderAddressItem wsSaleOrderAddressItem : request.getSaleOrderAddress().getSaleOrderAddressItems()) {
                                SaleOrderItem saleOrderItem = codeToSaleOrderItem.get(wsSaleOrderAddressItem.getSaleOrderItemCode());
                                editShippingAddress(context, addressMap, wsSaleOrderAddressItem.getShippingAddress().getReferenceId(), saleOrderItem);
                            }
                        } else {
                            for (SaleOrderItem saleOrderItem : saleOrder.getSaleOrderItems()) {
                                editShippingAddress(context, addressMap, wsSaleOrderAddress.getShippingAddress().getReferenceId(), saleOrderItem);
                            }
                        }
                    }

                    if (!context.hasErrors()) {
                        response.setSuccessful(true);
                    }
                }
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    private void editShippingAddress(ValidationContext context, Map<String, AddressDetail> addressMap, String referenceId, SaleOrderItem saleOrderItem) {

        if (!StringUtils.equalsAny(saleOrderItem.getStatusCode(), SaleOrderItem.StatusCode.CREATED.name(), SaleOrderItem.StatusCode.ALTERNATE_SUGGESTED.name(),
                SaleOrderItem.StatusCode.FULFILLABLE.name(), SaleOrderItem.StatusCode.UNFULFILLABLE.name())) {
            context.addError(WsResponseCode.INVALID_SALE_ORDER_ITEM_STATE, "Invalid Sale Order Item State: " + saleOrderItem.getCode());
        } else {
            AddressDetail shippingAddress = addressMap.get(referenceId);
            ShippingPackage shippingPackage = saleOrderItem.getShippingPackage();
            if (!shippingAddress.getPincode().equals(saleOrderItem.getShippingAddress().getPincode()) && shippingPackage != null
                    && !StringUtils.equalsAny(shippingPackage.getStatusCode(), ShippingPackage.StatusCode.CREATED.name(), ShippingPackage.StatusCode.PICKING.name(),
                            ShippingPackage.StatusCode.LOCATION_NOT_SERVICEABLE.name(), ShippingPackage.StatusCode.PICKED.name())) {
                context.addError(WsResponseCode.INVALID_SHIPPING_PACKAGE_STATE,
                        "Changing Pincode is allowed before invoice generation only. Check sale order item:" + saleOrderItem.getCode());
            }

            if (!context.hasErrors()) {
                shippingAddress = saleOrderDao.addAddressDetail(shippingAddress);
                addressMap.put(referenceId, shippingAddress);
                saleOrderItem.setShippingAddress(shippingAddress);
                if (shippingPackage != null) {
                    shippingPackage.setShippingAddress(shippingAddress);
                }
            }
        }
    }

    /**
     * @param request
     * @param context
     * @param thirdPartyShipping
     * @return
     */
    private ValidationContext validateEditSaleOrderAddressRequest(EditSaleOrderAddressRequest request, ValidationContext context, boolean thirdPartyShipping) {
        Map<String, WsAddressDetail> addresses = new HashMap<String, WsAddressDetail>();
        int index = 0;
        for (WsAddressDetail address : request.getSaleOrderAddress().getAddresses()) {
            validateAndNormalizeAddress(context, address, thirdPartyShipping, index++);
            addresses.put(address.getId(), address);
        }

        if (request.getSaleOrderAddress().getShippingAddress() != null && !addresses.containsKey(request.getSaleOrderAddress().getShippingAddress().getReferenceId())) {
            context.addError(WsResponseCode.INVALID_ADDRESS_REFERENCE,
                    "invalid address reference at saleOrder.shippingAddress[@ref=" + request.getSaleOrderAddress().getShippingAddress().getReferenceId() + "]");
        }

        if (request.getSaleOrderAddress().getBillingAddress() != null && !addresses.containsKey(request.getSaleOrderAddress().getBillingAddress().getReferenceId())) {
            context.addError(WsResponseCode.INVALID_ADDRESS_REFERENCE,
                    "invalid address reference at saleOrder.billingAddress[@ref=" + request.getSaleOrderAddress().getBillingAddress().getReferenceId() + "]");
        }

        if (request.getSaleOrderAddress().getSaleOrderAddressItems() != null) {
            for (WsSaleOrderAddressItem saleOrderItem : request.getSaleOrderAddress().getSaleOrderAddressItems()) {
                if (saleOrderItem.getShippingAddress() != null && !addresses.containsKey(saleOrderItem.getShippingAddress().getReferenceId())) {
                    context.addError(WsResponseCode.INVALID_ADDRESS_REFERENCE, "invalid address reference at saleOrder.saleOrderItems[" + saleOrderItem.getSaleOrderItemCode()
                            + "].shippingAddress[@ref=" + saleOrderItem.getShippingAddress().getReferenceId() + "]");
                }
            }
        }
        return context;
    }

    @Override
    @Transactional
    public SaleOrder updateSaleOrder(SaleOrder saleOrder) {
        return saleOrderDao.updateSaleOrder(saleOrder);
    }

    @Override
    @Transactional
    public List<SaleOrderStatus> getOrderStatuses() {
        return saleOrderDao.getOrderStatuses();
    }

    @Override
    @Transactional
    public List<SaleOrderItemStatus> getOrderItemStatuses() {
        return saleOrderDao.getOrderItemStatuses();
    }

    @Override
    @Transactional
    public List<SaleOrderItem> getPendingSaleOrderItemsByFacilityId(Integer facilityId) {
        return saleOrderDao.getPendingSaleOrderItemsByFacilityId(facilityId);
    }

    @Override
    @Transactional
    public SaleOrderItem getSaleOrderItemByCode(String saleOrderCode, String saleOrderItemCode) {
        return saleOrderDao.getSaleOrderItemByCode(saleOrderCode, saleOrderItemCode);
    }

    @Override
    @Transactional
    public List<SaleOrderItem> getSaleOrderItemByCodeList(String saleOrderCode, List<String> saleOrderItemCodeList) {
        return saleOrderDao.getSaleOrderItemByCodeList(saleOrderCode, saleOrderItemCodeList);
    }

    @Override
    @Transactional
    public List<SaleOrder> searchSaleOrder(String keyword) {
        return saleOrderDao.searchSaleOrder(keyword);
    }

    @Override
    @Transactional
    public List<SaleOrderVO> searchFailedOrPartialOrder(String keyword) {
        return saleOrderMao.searchFailedOrPartialOrders(keyword);
    }

    @Override
    @Transactional
    public List<SaleOrderItem> getSaleOrderItemsByItemIdAcrossFacility(Integer itemId) {
        return saleOrderDao.getSaleOrderItemsByItemIdForTenant(itemId);
    }

    @Override
    @Transactional
    public SaleOrderItem getSaleOrderItemByItemCode(String saleOrderCode, String itemCode) {
        return saleOrderDao.getSaleOrderItemByItemCode(saleOrderCode, itemCode);
    }

    @Override
    @Transactional
    public List<SaleOrder> getReconciliationPendingOrders(int start, int pageSize) {
        return saleOrderDao.getReconciliationPendingOrders(start, pageSize);
    }

    @Override
    @Transactional(readOnly = true)
    public List<SaleOrder> getSaleOrdersForStatusSync(Integer channelId, boolean pendingOnly, int numOfDays, int maxId, int batchSize) {
        List<String> statusCodes = new ArrayList<String>();
        statusCodes.add(SaleOrderItem.StatusCode.CREATED.name());
        statusCodes.add(SaleOrderItem.StatusCode.FULFILLABLE.name());
        statusCodes.add(SaleOrderItem.StatusCode.UNFULFILLABLE.name());
        statusCodes.add(SaleOrderItem.StatusCode.MANIFESTED.name());
        if (!pendingOnly) {
            statusCodes.add(SaleOrderItem.StatusCode.DISPATCHED.name());
            statusCodes.add(SaleOrderItem.StatusCode.DELIVERED.name());
        }
        return saleOrderDao.getSaleOrdersForStatusSync(statusCodes, numOfDays, channelId, maxId, batchSize);
    }

    @Override
    @Transactional(readOnly = true)
    public Long getSaleOrderCountForStatusSync(Integer channelId, boolean pendingOnly, int numOfDays) {
        List<String> statusCodes = new ArrayList<String>();
        statusCodes.add(SaleOrderItem.StatusCode.CREATED.name());
        statusCodes.add(SaleOrderItem.StatusCode.FULFILLABLE.name());
        statusCodes.add(SaleOrderItem.StatusCode.UNFULFILLABLE.name());
        statusCodes.add(SaleOrderItem.StatusCode.MANIFESTED.name());
        if (!pendingOnly) {
            statusCodes.add(SaleOrderItem.StatusCode.DISPATCHED.name());
            statusCodes.add(SaleOrderItem.StatusCode.DELIVERED.name());
        }
        return saleOrderDao.getSaleOrderCountForStatusSync(statusCodes, numOfDays, channelId);
    }

    /**
     * Delete {@link com.uniware.core.entity.SaleOrder} for the tenant. Used when tenant wants the order to be re-synced
     * from {@link com.uniware.core.entity.Channel}. For administrative users only.
     *
     * @param request
     * @return
     */
    @Override
    @Locks({ @Lock(ns = Namespace.SALE_ORDER, key = "#{#args[0].saleOrderCode}") })
    @Transactional
    @RollbackOnFailure
    public DeleteSaleOrderResponse deleteSaleOrder(DeleteSaleOrderRequest request) {
        LOG.info("Received deleteSaleOrder request for: {}", request.getSaleOrderCode());
        DeleteSaleOrderResponse response = new DeleteSaleOrderResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            SaleOrder saleOrder = getSaleOrderForUpdate(request.getSaleOrderCode());
            if (saleOrder == null) {
                context.addError(WsResponseCode.INVALID_SALE_ORDER_CODE);
            } else if (saleOrder.getCreated().before(orderDeletionTolerence)) {
                context.addError(WsResponseCode.INVALID_SALE_ORDER_CODE, "Sale order prior to Aug 1 cannot be deleted");
            } else {
                for (SaleOrderItem soi : saleOrder.getSaleOrderItems()) {
                    // soi should be cancellable or cancelled already
                    if (soi.isCancellable() || SaleOrderItem.StatusCode.CANCELLED.name().equals(soi.getStatusCode())) {
                        // for packaged SOIs, status should be CREATED *only*
                        if (soi.getShippingPackage() != null && !ShippingPackage.StatusCode.CREATED.name().equals(soi.getShippingPackage().getStatusCode())) {
                            context.addError(WsResponseCode.INVALID_SHIPPING_PACKAGE_STATE,
                                    "SaleOrderItem[" + soi.getCode() + "] cannot be deleted because corresponding package is: " + soi.getShippingPackage().getStatusCode());
                            break;
                        }
                    } else {
                        context.addError(WsResponseCode.INVALID_SALE_ORDER_ITEM_STATE, "SaleOrderItem[" + soi.getCode() + "] cannot be deleted in state: " + soi.getStatusCode());
                        break;
                    }
                }
                // in case any of the soi was cancelled, shippingPackage of
                // saleOrderItem would be null.
                if (!context.hasErrors()) {
                    for (ShippingPackage sp : shippingService.getShippingPackagesBySaleOrder(request.getSaleOrderCode())) {
                        if (sp.getInvoice() != null) {
                            context.addError(WsResponseCode.INVALID_SHIPPING_PACKAGE_STATE, "ShippingPackage[" + sp.getCode() + "] is already invoiced");
                            break;
                        }
                    }
                }
                if (!context.hasErrors()) {
                    // cancel if it is not already cancelled
                    if (!SaleOrder.StatusCode.CANCELLED.name().equals(saleOrder.getStatusCode())) {
                        CancelSaleOrderRequest cancelRequest = new CancelSaleOrderRequest();
                        cancelRequest.setSaleOrderCode(request.getSaleOrderCode());
                        cancelRequest.setCancellationReason("Order Deleted");
                        cancelRequest.setCancelOnChannel(false);
                        CancelSaleOrderResponse cancelResponse = cancelSaleOrder(cancelRequest);
                        if (!cancelResponse.isSuccessful()) {
                            context.addErrors(cancelResponse.getErrors());
                        }
                    }
                    if (!context.hasErrors()) {
                        LOG.info("SaleOrder: {} is CANCELLED. Deleting it now.", request.getSaleOrderCode());
                        saleOrderDao.deleteSaleOrder(saleOrder);
                        LOG.info("Deleting SaleOrderReconciliation for Order: {}", request.getSaleOrderCode());
                        reconciliationMao.removeSaleOrderReconciliationsBySaleOrderCode(saleOrder.getCode(), saleOrder.getChannel().getCode());
                    }
                }
            }
        }
        response.setErrors(context.getErrors());
        response.setSuccessful(!context.hasErrors());
        return response;
    }

    @Override
    @Transactional
    public GenerateSaleOrderNextSequenceResponse generateNext(GenerateSaleOrderNextSequenceRequest request) {
        GenerateSaleOrderNextSequenceResponse response = new GenerateSaleOrderNextSequenceResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            response.setNextSequence(sequenceGenerator.generateNext(Name.SALE_ORDER));
            response.setSuccessful(true);
        }
        return response;
    }

    @Override
    public LookupSaleOrderLineItemsResponse lookupSaleOrderLineItems(LookupSaleOrderLineItemsRequest request) throws IOException {
        LookupSaleOrderLineItemsResponse response = new LookupSaleOrderLineItemsResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            BufferedWriter writer = null;
            File importFile = null;
            try {
                InputStreamReader isr = new InputStreamReader(request.getInputStream(), "UTF-8");
                BufferedReader reader = new BufferedReader(isr);
                String importFileRelativePath = new StringBuilder().append("import-").append(String.valueOf(System.nanoTime())).append(".csv").toString();
                String importDirectory = CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class).getImportDirectoryPath();
                importFile = new File(FileUtils.normalizeFilePath(importDirectory, importFileRelativePath));
                importFile.createNewFile();
                writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(importFile), "UTF-8"));
                String line;
                while ((line = reader.readLine()) != null) {
                    writer.newLine();
                    writer.append(line);
                }
                writer.close();
            } finally {
                if (writer != null) {
                    try {
                        writer.close();
                    } catch (Exception e) {
                    }
                }
            }
            DelimitedFileParser parser = new DelimitedFileParser(importFile.getAbsolutePath());
            DelimitedFileParser.RowIterator rowIterator = parser.parse();
            List<LookupSaleOrderLineItemsResponse.LineItem> lineItems = new ArrayList<>();
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                String skuCode = row.getColumnValue("Item SKU");
                ItemType itemType = catalogService.getAllItemTypeBySkuCode(skuCode);
                if (itemType == null) {
                    context.addWarning(WsResponseCode.INVALID_ITEM_TYPE, skuCode);
                } else {
                    ItemTypeLookupDTO itemTypeLookupDTO = catalogService.prepareItemTypeLookupDTO(itemType);
                    Integer quantity = null;
                    Double sellingPrice = null;
                    if (itemTypeLookupDTO.getBasePrice() != null) {
                        sellingPrice = itemTypeLookupDTO.getBasePrice().doubleValue();
                    }
                    try {
                        quantity = Integer.parseInt(row.getColumnValue("Quantity"));
                        sellingPrice = Double.parseDouble(row.getColumnValue("Selling Price"));
                    } catch (Exception e) {
                        // Ignore
                    }
                    LookupSaleOrderLineItemsResponse.LineItem lineItem = new LookupSaleOrderLineItemsResponse.LineItem();
                    lineItem.setItemType(itemTypeLookupDTO);
                    lineItem.setQuantity(quantity);
                    lineItem.setSellingPrice(sellingPrice);
                    lineItems.add(lineItem);
                }
            }
            response.setLineItems(lineItems);
            response.setSuccessful(true);
        }

        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        if (context.hasWarnings()) {
            response.setWarnings(context.getWarnings());
        }
        return response;
    }

    @Override
    public List<SaleOrder> searchSaleOrderByDisplayOrderCode(String keyword) {
        return saleOrderDao.searchSaleOrderByDisplayOrderCode(keyword);
    }

    @Override
    public List<SaleOrder> searchSaleOrderByOrderCode(String keyword) {
        return saleOrderDao.searchSaleOrderByOrderCode(keyword);
    }

    @Override
    public void processDirtySaleOrder(SaleOrder dirtySaleOrder) {
        LOG.info("Processing dirty sale order: {}", dirtySaleOrder.getCode());
        updateViewForDirtySaleOrder(dirtySaleOrder);
        LOG.info("Done updating view for dirty sale order: {}", dirtySaleOrder.getCode());
        markSaleOrderNonDirty(dirtySaleOrder);
        LOG.info("Marked sale order: {} non-dirty", dirtySaleOrder.getCode());
    }

    /**
     * Mark sale order non dirty in MySQL.
     *
     * @param dirtySaleOrder
     */
    @Locks({ @Lock(ns = Namespace.SALE_ORDER, key = "#{#args[0].code}") })
    @Transactional
    private void markSaleOrderNonDirty(SaleOrder dirtySaleOrder) {
        SaleOrder so = getSaleOrderForUpdate(dirtySaleOrder.getCode(), false);
        // check if someone else updated the same sale order while we were busy
        if (!so.getUpdated().after(dirtySaleOrder.getUpdated())) {
            so.setViewDirty(false);
        } else {
            LOG.info("Skipped marking sale order: {} non-dirty", so.getCode());
        }
    }

    /**
     * Mark sale order non dirty in view.
     *
     * @param dirtySaleOrder
     */
    private void updateViewForDirtySaleOrder(SaleOrder dirtySaleOrder) {
        Map<View, List<Map<String, Object>>> viewToSaleOrderDataMap = new HashMap<>(View.values().length);
        ExportJobConfiguration configuration = ConfigurationManager.getInstance().getConfiguration(ExportJobConfiguration.class);
        for (View v : View.values()) {
            List<ExportConfig> exportConfigList = configuration.getExportConfigsByViewName(v.getCollectionName());
            for (ExportConfig exportConfig : exportConfigList) {
                List<Map<String, Object>> dataForSaleOrder = getSaleOrderData(exportConfig, dirtySaleOrder);
                viewToSaleOrderDataMap.put(v, dataForSaleOrder);
            }
        }
        exportMao.updateViews(viewToSaleOrderDataMap);
    }

    private List<Map<String, Object>> getSaleOrderData(ExportConfig exportConfig, SaleOrder dirtySO) {
        if (CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class).isDatatableViaReplicationEnabled()) {
            return getSaleOrderDataFromSecondaryDS(exportConfig, dirtySO);
        } else {
            return getSaleOrderDataFromPrimaryDS(exportConfig, dirtySO);
        }
    }

    @Transactional(readOnly = true)
    private List<Map<String, Object>> getSaleOrderDataFromPrimaryDS(ExportConfig exportConfig, SaleOrder dirtySO) {
        return getSaleOrderDataInternal(exportConfig, dirtySO);
    }

    @Transactional(readOnly = true, value = "txManagerReplication")
    private List<Map<String, Object>> getSaleOrderDataFromSecondaryDS(ExportConfig exportConfig, SaleOrder dirtySO) {
        return getSaleOrderDataInternal(exportConfig, dirtySO);
    }

    private List<Map<String, Object>> getSaleOrderDataInternal(ExportConfig exportConfig, SaleOrder dirtySO) {
        GetDatatableResultRequest request = new GetDatatableResultRequest();
        User user = usersService.getUserByUsername(Constants.SYSTEM_USER_EMAIL);
        // set filter
        WsExportFilter filter = new WsExportFilter();
        filter.setText(dirtySO.getCode());
        filter.setId(exportConfig.getPrimaryFilter().getId());
        List<WsExportFilter> filterList = new ArrayList<>(1);
        filterList.add(filter);
        request.setFilters(filterList);
        // get filter params
        Map<String, Object> filterValues = exportService.getFilterParameters(user.getVendor(), exportConfig, request.getFilters());
        // set export columns
        List<String> exportColumnList = new ArrayList<>();
        for (ExportColumn exportColumn : exportConfig.getExportColumns()) {
            exportColumnList.add(exportColumn.getId());
        }
        for (ExportColumn hiddenColumn : exportConfig.getHiddenColumns()) {
            exportColumnList.add(hiddenColumn.getId());
        }
        request.setColumns(exportColumnList);
        Query query = exportDao.prepareQuery(exportConfig, request.getColumns(), request.getFilters(), filterValues, request.getSortColumns(), true);
        /**
         * query.setFirstResult(request.getStart()); query.setMaxResults(request.getNoOfResults());
         **/
        return exportDao.executeAnonymousQuery(query);
    }

    @Transactional(readOnly = true)
    @Override
    public List<SaleOrder> getDirtySaleOrders(int start, int pageSize) {
        return saleOrderDao.getDirtySaleOrders(start, pageSize);
    }

    @Override
    @Locks({ @Lock(ns = Namespace.SALE_ORDER, key = "#{#args[0].saleOrderCode}") })
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @RollbackOnFailure
    public MarkSaleOrderItemsCompleteResponse markSaleOrderItemsComplete(MarkSaleOrderItemsCompleteRequest request) {
        MarkSaleOrderItemsCompleteResponse response = new MarkSaleOrderItemsCompleteResponse();
        ValidationContext context = request.validate();
        SaleOrder saleOrder = null;
        Channel channel = null;
        if (!context.hasErrors()) {
            saleOrder = getSaleOrderForUpdate(request.getSaleOrderCode());
            if (saleOrder == null) {
                context.addError(WsResponseCode.INVALID_SALE_ORDER_CODE);
            } else if (SaleOrder.StatusCode.CANCELLED.equals(saleOrder.getStatusCode())) {
                context.addError(WsResponseCode.INVALID_SALE_ORDER_CODE, "Sale Order is already cancelled");
            } else if (SaleOrder.StatusCode.COMPLETE.equals(saleOrder.getStatusCode())) {
                context.addError(WsResponseCode.INVALID_SALE_ORDER_CODE, "Sale Order is already complete");
            } else if (SystemConfiguration.TraceabilityLevel.ITEM.equals(ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).getTraceabilityLevel())) {
                context.addError(WsResponseCode.INVALID_SALE_ORDER_CODE, "Action not applicable for ITEM level traceability");
            }
        }
        if (!context.hasErrors()) {
            LOG.info("Marking saleOrder {} complete", saleOrder.getCode());
            channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelById(saleOrder.getChannel().getId());

            if (SaleOrder.StatusCode.PENDING_VERIFICATION.equals(saleOrder.getStatusCode())) {
                saleOrder.setStatusCode(SaleOrder.StatusCode.CREATED.name());
            }

            Map<String, SaleOrderItem> codeToSaleOrderItem = new HashMap<>(saleOrder.getSaleOrderItems().size());
            for (SaleOrderItem soi : saleOrder.getSaleOrderItems()) {
                codeToSaleOrderItem.put(soi.getCode(), soi);
            }

            Map<String, List<SaleOrderItem>> itemSkuToUnfulfilledSaleOrderItems = new HashMap<>();
            Set<String> shippingPackageCodes = new HashSet<>();
            for (String soiCode : request.getSaleOrderItemCodes()) {
                SaleOrderItem soi = codeToSaleOrderItem.get(soiCode);
                if (soi.getShippingPackage() != null) {
                    if (StringUtils.equalsAny(soi.getShippingPackage().getStatusCode(), StatusCode.CREATED.name(), StatusCode.PACKED.name(), StatusCode.READY_TO_SHIP.name(),
                            StatusCode.MANIFESTED.name(), StatusCode.PICKED.name(), StatusCode.PICKING.name())) {
                        LOG.debug("Adding shipping package code {}", soi.getShippingPackage().getCode());
                        shippingPackageCodes.add(soi.getShippingPackage().getCode());
                    }
                } else {
                    List<SaleOrderItem> saleOrderItems = itemSkuToUnfulfilledSaleOrderItems.get(soi.getItemType().getSkuCode());
                    if (saleOrderItems == null) {
                        saleOrderItems = new ArrayList<>();
                        itemSkuToUnfulfilledSaleOrderItems.put(soi.getItemType().getSkuCode(), saleOrderItems);
                    }
                    saleOrderItems.add(soi);
                }
            }
            for (Map.Entry<String, List<SaleOrderItem>> entry : itemSkuToUnfulfilledSaleOrderItems.entrySet()) {
                LOG.info("SaleOrder {}, allocating inventory and creating shipping package", saleOrder.getCode());
                if (ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).isInventoryManagementSwitchedOff()) {
                    for (SaleOrderItem saleOrderItem : entry.getValue()) {
                        saleOrderItem.setStatusCode(SaleOrderItem.StatusCode.FULFILLABLE.name());
                    }
                } else {
                    ItemType itemType = catalogService.getItemTypeBySkuCode(entry.getKey());
                    Shelf shelf = shelfService.getShelfByCode("DEFAULT");
                    inventoryService.addInventory(itemType, ItemTypeInventory.Type.GOOD_INVENTORY, shelf, entry.getValue().size());
                    Map<ItemTypeInventory, Integer> itemTypeInventories = inventoryService.blockAvailableInventory(itemType, channel.getId(), entry.getValue().size());
                    Iterator<SaleOrderItem> iterator = entry.getValue().iterator();
                    for (Map.Entry<ItemTypeInventory, Integer> inventoryEntry : itemTypeInventories.entrySet()) {
                        for (int i = 0; i < inventoryEntry.getValue(); i++) {
                            SaleOrderItem saleOrderItem = iterator.next();
                            saleOrderItem.setItemTypeInventory(inventoryEntry.getKey());
                            saleOrderItem.setStatusCode(SaleOrderItem.StatusCode.FULFILLABLE.name());
                        }
                    }
                }
                ShippingPackage shippingPackage = shippingService.createShippingPackage(entry.getValue(), false);
                shippingPackageCodes.add(shippingPackage.getCode());
            }
            for (String shippingPackageCode : shippingPackageCodes) {
                LOG.info("Force dispatch shipping package {}", shippingPackageCode);
                ForceDispatchShippingPackageRequest forceDispatchShippingPackageRequest = new ForceDispatchShippingPackageRequest();
                forceDispatchShippingPackageRequest.setShippingPackageCode(shippingPackageCode);
                forceDispatchShippingPackageRequest.setUserId(request.getUserId());
                ForceDispatchShippingPackageResponse forceDispatchShippingPackageResponse = dispatchService.forceDispatchShippingPackage(forceDispatchShippingPackageRequest);
                if (forceDispatchShippingPackageResponse.hasErrors()) {
                    context.addErrors(forceDispatchShippingPackageResponse.getErrors());
                }
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        } else {
            LOG.info("Done marking saleOrder {} complete", saleOrder.getCode());
            response.setSuccessful(true);
        }
        return response;
    }

    @Override
    @LogActivity
    public MarkSaleOrderItemsUnfulfillableResponse markSaleOrderItemsUnfulfillable(MarkSaleOrderItemsUnfulfillableRequest request) {
        MarkSaleOrderItemsUnfulfillableResponse response = new MarkSaleOrderItemsUnfulfillableResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            SaleOrder saleOrder = getSaleOrderByCode(request.getSaleOrderCode());
            if (saleOrder == null) {
                context.addError(WsResponseCode.INVALID_SALE_ORDER_CODE, "Invalid sale order code");
            }
            if (!context.hasErrors()) {
                Set<Integer> saleOrderItemsToMarkUnfulfillable = new HashSet<>();
                Set<String> invalidOrderItems = new HashSet<>();
                for (String saleOrderItemCode : request.getSaleOrderItemCodes()) {
                    SaleOrderItem saleOrderItem = getSaleOrderItemByCode(request.getSaleOrderCode(), saleOrderItemCode);
                    if (saleOrderItem != null && saleOrderItem.getFacility() != null && saleOrderItem.getFacility().getId().equals(UserContext.current().getFacilityId())) {
                        saleOrderItemsToMarkUnfulfillable.add(saleOrderItem.getId());
                    } else {
                        invalidOrderItems.add(saleOrderItemCode);
                    }
                }
                if (!invalidOrderItems.isEmpty()) {
                    context.addError(WsResponseCode.INVALID_SALE_ORDER_ITEM_CODE, "Invalid sale order item codes " + invalidOrderItems);
                }
                if (!context.hasErrors()) {
                    markSaleOrderItemUnfulfillable(response, saleOrder.getCode(), saleOrderItemsToMarkUnfulfillable, context);
                }
                if (!context.hasErrors()) {
                    response.setSuccessful(true);
                }
            }

        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    public List<SaleOrder> getPendingSaleOrders(Integer pendingSinceInHours, List<String> pendingOrderStatuses, Integer facilityId) {
        return saleOrderDao.getPendingSaleOrders(pendingSinceInHours, pendingOrderStatuses, facilityId);
    }

    @Locks({ @Lock(ns = Namespace.SALE_ORDER, key = "#{#args[1]}") })
    @Transactional
    private void markSaleOrderItemUnfulfillable(MarkSaleOrderItemsUnfulfillableResponse response, String saleOrderCode, Set<Integer> saleOrderItems, ValidationContext context) {
        SaleOrder saleOrder = saleOrderDao.getSaleOrderForUpdate(saleOrderCode);
        List<SaleOrderItem> saleOrderItemsToMarkUnfulfillable = new ArrayList<SaleOrderItem>();
        List<String> saleOrderItemsMarkedUnfulfillable = new ArrayList<String>();
        for (SaleOrderItem saleOrderItem : saleOrder.getSaleOrderItems()) {
            if (saleOrderItems.contains(saleOrderItem.getId())) {
                if (!(StringUtils.equalsAny(saleOrderItem.getStatusCode(), SaleOrderItem.StatusCode.UNFULFILLABLE.name(), SaleOrderItem.StatusCode.LOCATION_NOT_SERVICEABLE.name(),
                        SaleOrderItem.StatusCode.CREATED.name()) || (SaleOrderItem.StatusCode.FULFILLABLE.name().equals(saleOrderItem.getStatusCode())))) {
                    context.addError(WsResponseCode.INVALID_SALE_ORDER_ITEM_STATE,
                            "SaleOrderItem:" + saleOrderItem.getCode() + " not in CREATED/FULFILLABLE/UNFULFILLABLE/LOCATION_NOT_SERVICEABLE state");
                } else if (!(saleOrderItem.getShippingPackage() == null
                        || StringUtils.equalsAny(saleOrderItem.getShippingPackage().getStatusCode(), ShippingPackage.StatusCode.CREATED.name(),
                                ShippingPackage.StatusCode.LOCATION_NOT_SERVICEABLE.name(), ShippingPackage.StatusCode.PICKING.name(), ShippingPackage.StatusCode.PICKED.name()))) {
                    context.addError(WsResponseCode.INVALID_SHIPPING_PACKAGE_STATE,
                            "SaleOrderItem:" + saleOrderItem.getCode() + " package not in CREATED/PICKING/PICKED/LOCATION_NOT_SERVICEABLE state");
                } else {
                    saleOrderItemsToMarkUnfulfillable.add(saleOrderItem);
                }
            }
        }
        if (!context.hasErrors()) {
            Map<Integer, Integer> releaseInventoryMap = new HashMap<Integer, Integer>();
            Map<String, List<String>> shippingPackageToSOI = new HashMap<>();
            for (SaleOrderItem saleOrderItem : saleOrderItemsToMarkUnfulfillable) {
                if (SaleOrderItem.StatusCode.FULFILLABLE.name().equals(saleOrderItem.getStatusCode())) {
                    if (saleOrderItem.getItemTypeInventory() != null) {
                        if (!releaseInventoryMap.containsKey(saleOrderItem.getItemTypeInventory().getId())) {
                            releaseInventoryMap.put(saleOrderItem.getItemTypeInventory().getId(), 0);
                        }
                        releaseInventoryMap.put(saleOrderItem.getItemTypeInventory().getId(), releaseInventoryMap.get(saleOrderItem.getItemTypeInventory().getId()) + 1);
                        saleOrderItem.setItemTypeInventory(null);
                        if (saleOrderItem.getItem() != null) {
                            saleOrderItem.getItem().setStatusCode(Item.StatusCode.GOOD_INVENTORY.name());
                            saleOrderItem.setItem(null);
                        }
                    }
                    if (saleOrderItem.getShippingPackage() != null) {
                        ShippingPackage shippingPackage = shippingService.getShippingPackageById(saleOrderItem.getShippingPackage().getId());
                        shippingPackage.setStatusCode(ShippingPackage.StatusCode.CANCELLED.name());
                        shippingPackage.setCancellationTime(DateUtils.getCurrentTime());
                        if (ActivityContext.current().isEnable()) {
                            if (!shippingPackageToSOI.containsKey(shippingPackage.getCode())) {
                                shippingPackageToSOI.put(shippingPackage.getCode(), new ArrayList<String>());
                            }
                            shippingPackageToSOI.get(shippingPackage.getCode()).add(saleOrderItem.getCode());
                        }
                        for (SaleOrderItem orderItem : saleOrder.getSaleOrderItems()) {
                            if (orderItem.getShippingPackage() != null && shippingPackage.getId().equals(orderItem.getShippingPackage().getId())) {
                                orderItem.setShippingPackage(null);
                            }
                        }
                    } else {
                        if (!shippingPackageToSOI.containsKey(StringUtils.EMPTY_STRING)) {
                            shippingPackageToSOI.put(StringUtils.EMPTY_STRING, new ArrayList<String>());
                        }
                        shippingPackageToSOI.get(StringUtils.EMPTY_STRING).add(saleOrderItem.getCode());
                    }
                }
                saleOrder.setReassessInventory(true);
                saleOrderItem.setStatusCode(SaleOrderItem.StatusCode.CREATED.name());
                saleOrderItem.setFacility(null);
                saleOrderItemsMarkedUnfulfillable.add(saleOrderItem.getCode());

            }
            if (ActivityContext.current().isEnable()) {
                ActivityUtils.appendActivity(saleOrder.getCode(), ActivityEntityEnum.SALE_ORDER_ITEM.getName(), saleOrder.getCode(),
                        Arrays.asList(new String[] {
                                "Sale order item {" + ActivityEntityEnum.SALE_ORDER_ITEM + ":" + shippingPackageToSOI.values() + "} marked unfulfillable " + ". Shipping packages {"
                                        + ActivityEntityEnum.SHIPPING_PACKAGE + ":" + shippingPackageToSOI.keySet() + "} marked cancelled." }),
                        ActivityTypeEnum.EDIT.name());
            }
            for (Map.Entry<Integer, Integer> entry : releaseInventoryMap.entrySet()) {
                inventoryService.releaseInventory(entry.getKey(), saleOrder.getChannel().getId(), entry.getValue());
            }
            for (String soiCode : saleOrderItemsMarkedUnfulfillable) {
                FacilityAllocationVO facilityAllocation = saleOrderMao.getFacilityAllocation(soiCode, saleOrder.getCode());
                if (facilityAllocation != null) {
                    facilityAllocation.getFacilityCodes().add(CacheManager.getInstance().getCache(FacilityCache.class).getCurrentFacility().getCode());
                } else {
                    facilityAllocation = new FacilityAllocationVO();
                    List<String> facilityCodes = new ArrayList<>();
                    facilityCodes.add(CacheManager.getInstance().getCache(FacilityCache.class).getCurrentFacility().getCode());
                    facilityAllocation.setFacilityCodes(facilityCodes);
                    facilityAllocation.setSaleOrderItemCode(soiCode);
                    facilityAllocation.setSaleOrderCode(saleOrder.getCode());
                }
                saleOrderMao.save(facilityAllocation);
            }
            response.setSuccessfulSaleOrderItemCodes(saleOrderItemsMarkedUnfulfillable);
        }
    }

    @Override
    @Transactional(readOnly = true)
    public GetCancellationReasonsResponse getCancellationReasons(GetCancellationReasonsRequest request) {
        GetCancellationReasonsResponse response = new GetCancellationReasonsResponse();
        ValidationContext context = request.validate();
        List<SelectItem> cancellationReasons;
        if (!context.hasErrors()) {
            cancellationReasons = CacheManager.getInstance().getCache(UICustomizationsCache.class).getUICustomListOptions("editSaleOrder:cancelReason");
            if (cancellationReasons == null) {
                Channel channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelByName(request.getChannelName());
                if (channel == null) {
                    context.addError(WsResponseCode.INVALID_CHANNEL_NAME, "Invalid Channel Name : " + request.getChannelName());
                    LOG.error("Invalid Channel Name : " + request.getChannelName());
                } else {
                    Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(channel.getSourceCode());
                    List<String> reasons = source.getSaleOrderCancellationReasons();
                    LOG.info("Fetching Cancellation Reasons for Channel : " + request.getChannelName() + ", No of reasons :" + reasons.size());
                    cancellationReasons = new ArrayList<>(reasons.size());
                    for (String reason : reasons) {
                        cancellationReasons.add(new SelectItem(reason, reason));
                    }
                }
            }
            response.setCancellationReasons(cancellationReasons != null ? cancellationReasons : Collections.EMPTY_LIST);
        }
        if (context.hasErrors()) {
            response.addErrors(context.getErrors());
        }
        response.setSuccessful(!context.hasErrors());
        return response;
    }

    @Override
    @Transactional(readOnly = true)
    public GetReturnReasonsResponse getReturnReasons(GetReturnReasonsRequest request) {
        GetReturnReasonsResponse response = new GetReturnReasonsResponse();
        ValidationContext context = request.validate();
        List<SelectItem> returnReasons;
        if (!context.hasErrors()) {
            returnReasons = CacheManager.getInstance().getCache(UICustomizationsCache.class).getUICustomListOptions("editSaleOrder:returnReason");
            if (returnReasons == null) {
                Channel channel = CacheManager.getInstance().getCache(ChannelCache.class).getChannelByName(request.getChannelName());
                if (channel == null) {
                    context.addError(WsResponseCode.INVALID_CHANNEL_NAME, "Invalid Channel Name : " + request.getChannelName());
                    LOG.error("Invalid Channel Name : " + request.getChannelName());
                } else {
                    Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(channel.getSourceCode());
                    List<String> reasons = source.getSaleOrderReturnReasons();
                    LOG.info("Fetching Return Reasons for Channel : " + request.getChannelName() + ", No of reasons :" + reasons.size());
                    returnReasons = new ArrayList<>(reasons.size());
                    for (String reason : reasons) {
                        returnReasons.add(new SelectItem(reason, reason));
                    }
                }
            }
            response.setReturnReasons(returnReasons != null ? returnReasons : Collections.EMPTY_LIST);
        }
        if (context.hasErrors()) {
            response.addErrors(context.getErrors());
        }
        response.setSuccessful(!context.hasErrors());
        return response;
    }

    @Override
    @Transactional(readOnly = true)
    public GetShippingPackagesByStatusCodeResponse getShippingPackageByStatusCode(GetShippingPackagesByStatusCodeRequest request) {
        GetShippingPackagesByStatusCodeResponse response = new GetShippingPackagesByStatusCodeResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            List<ShippingPackage> shippingPackages = shippingService.getPaginatedShippingPackagesByStatusCode(request.getStatusCode(), request.getPageRequest());
            List<GetShippingPackagesByStatusCodeResponse.ShippingPackageDTO> shippingPackageDTOs = new ArrayList<>(shippingPackages.size());
            for (ShippingPackage shippingPackage : shippingPackages) {
                List<GetShippingPackagesByStatusCodeResponse.SaleOrderItemDTO> saleOrderItemDTOs = new ArrayList<>(shippingPackage.getSaleOrderItems().size());
                SaleOrder saleOrder = shippingPackage.getSaleOrder();
                for (SaleOrderItem saleOrderItem : shippingPackage.getSaleOrderItems()) {
                    String imageUrl = null;
                    ChannelItemTypeVO channelItemTypeVO = channelCatalogService.getChannelItemTypeVOByChannelAndChannelProductId(saleOrder.getChannel().getCode(),
                            saleOrderItem.getChannelProductId());
                    if (channelItemTypeVO != null) {
                        Set<String> imageUrls = channelCatalogService.getChannelItemTypeVOByChannelAndChannelProductId(saleOrder.getChannel().getCode(),
                                saleOrderItem.getChannelProductId()).getImageUrls();
                        if (imageUrls != null && !imageUrls.isEmpty()) {
                            imageUrl = imageUrls.iterator().next();
                        }
                    }
                    saleOrderItemDTOs.add(new GetShippingPackagesByStatusCodeResponse.SaleOrderItemDTO(saleOrderItem, imageUrl));
                }
                shippingPackageDTOs.add(new GetShippingPackagesByStatusCodeResponse.ShippingPackageDTO(shippingPackage, saleOrderItemDTOs));
            }
            response.setShippingPackages(shippingPackageDTOs);
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        } else {
            response.setSuccessful(true);
        }
        return response;
    }

    @Override
    @Transactional(readOnly = true)
    public GetFailedSaleOrdersResponse getFailedSaleOrders(GetFailedSaleOrdersRequest request) {
        GetFailedSaleOrdersResponse response = new GetFailedSaleOrdersResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            List<SaleOrderVO> saleOrderVOs = saleOrderMao.getAllSaleOrders();
            List<GetFailedSaleOrdersResponse.SaleOrderDTO> failedSaleOrderDTOs = new ArrayList<>(saleOrderVOs.size());
            for (SaleOrderVO saleOrderVO : saleOrderVOs) {
                GetFailedSaleOrdersResponse.SaleOrderDTO failedSaleOrderDTO = new GetFailedSaleOrdersResponse.SaleOrderDTO(saleOrderVO);
                failedSaleOrderDTOs.add(failedSaleOrderDTO);
            }
            response.setSaleOrders(failedSaleOrderDTOs);
            response.setSuccessful(true);
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    @Override
    @Transactional(readOnly = true)
    public GetUnverifiedSaleOrdersResponse getUnverifiedSaleOrders(GetUnverifiedSaleOrdersRequest request) {
        GetUnverifiedSaleOrdersResponse response = new GetUnverifiedSaleOrdersResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            List<SaleOrder> saleOrders = saleOrderDao.getPaginatedSaleOrdersByStatus(Arrays.asList(SaleOrder.StatusCode.PENDING_VERIFICATION.name()), request.getPageRequest());
            List<GetUnverifiedSaleOrdersResponse.SaleOrderDTO> unverifiedSaleOrders = new ArrayList<>(saleOrders.size());
            for (SaleOrder saleOrder : saleOrders) {
                List<GetUnverifiedSaleOrdersResponse.SaleOrderItemDTO> saleOrderItems = new ArrayList<>(saleOrder.getSaleOrderItems().size());
                for (SaleOrderItem saleOrderItem : saleOrder.getSaleOrderItems()) {
                    String imageUrl = null;
                    ChannelItemTypeVO channelItemTypeVO = channelCatalogService.getChannelItemTypeVOByChannelAndChannelProductId(saleOrder.getChannel().getCode(),
                            saleOrderItem.getChannelProductId());
                    if (channelItemTypeVO != null) {
                        Set<String> imageUrls = channelItemTypeVO.getImageUrls();
                        if (imageUrls != null && !imageUrls.isEmpty()) {
                            imageUrl = imageUrls.iterator().next();
                        }
                    }
                    GetUnverifiedSaleOrdersResponse.SaleOrderItemDTO saleOrderItemDTO = new GetUnverifiedSaleOrdersResponse.SaleOrderItemDTO(saleOrderItem, imageUrl);
                    saleOrderItems.add(saleOrderItemDTO);
                }
                unverifiedSaleOrders.add(new GetUnverifiedSaleOrdersResponse.SaleOrderDTO(saleOrder.getCode(), saleOrder.getDisplayOrderCode(), saleOrderItems,
                        saleOrder.getFulfillmentTat(), saleOrder.getDisplayOrderDateTime()));
            }
            response.setSaleOrders(unverifiedSaleOrders);
            response.setSuccessful(true);
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional(readOnly = true)
    public boolean sendPendingPickupOrdersNotifications() {
        List<ShippingPackage> packages = shippingService.getShippingPackagesByStatusCode(ShippingPackage.StatusCode.CREATED.name(), null,
                DateUtils.addToDate(DateUtils.getCurrentTime(), Calendar.DAY_OF_MONTH, -1));
        boolean templatesAvailable = false;
        EmailConfiguration.EmailTemplateVO emailTemplateVO = ConfigurationManager.getInstance().getConfiguration(EmailConfiguration.class).getTemplateByType(
                Constants.EmailTemplateType.PENDING_PICKUP_ORDERS_EMAIL.name());
        SmsConfiguration.SmsTemplateVO smsTemplate = ConfigurationManager.getInstance().getConfiguration(SmsConfiguration.class).getTemplateByName(
                SmsTemplate.Name.PICKUP_ORDERS_NOTIFICATION.name());
        if ((emailTemplateVO != null && emailTemplateVO.isEnabled() || (smsTemplate != null && smsTemplate.isEnabled()))) {
            templatesAvailable = true;
        }
        if (templatesAvailable) {
            for (ShippingPackage shippingPackage : packages) {
                if (shippingPackage.getShippingMethod().equals(ShippingMethod.Code.PKP)) {
                    AddressDetail addressDetail = shippingPackage.getShippingAddress();
                    if (StringUtils.isNotBlank(addressDetail.getEmail()) && emailTemplateVO != null && emailTemplateVO.isEnabled()) {
                        try {
                            List<String> recipients = new ArrayList<String>();
                            recipients.add(shippingPackage.getShippingAddress().getEmail());
                            EmailMessage message = new EmailMessage(recipients, Constants.EmailTemplateType.PENDING_PICKUP_ORDERS_EMAIL.name());
                            message.addTemplateParam("shippingPackage", shippingPackage);
                            emailService.send(message);
                        } catch (Exception e) {
                            LOG.error("Error occured while sending pending pickup order mail to customer: " + shippingPackage.getShippingAddress().getName(), e);
                        }
                    }
                    if (smsTemplate != null && smsTemplate.isEnabled() && StringUtils.isNotBlank(addressDetail.getPhone())) {
                        try {
                            SmsMessage message = new SmsMessage(addressDetail.getPhone(), smsTemplate.getName());
                            message.addTemplateParam("shippingPackage", shippingPackage);
                            smsService.send(message, shippingPackage.getSaleOrder().getChannel().getSourceCode());
                        } catch (Exception e) {
                            LOG.error("Error occured while sending pending pickup orders sms:", e);
                        }
                    }
                }
            }
            return true;
        } else {
            LOG.error("Error while running pending pickup orders notification task: No templates enabled");
            return false;
        }
    }

    @Transactional
    @Override
    public List<String> getSaleOrderItemCodes(String saleOrderCode) {
        SaleOrder saleOrder = saleOrderDao.getSaleOrderByCode(saleOrderCode);
        if (saleOrder != null) {
            List<String> saleOrderItemCodes = new ArrayList<>();
            for (SaleOrderItem soi : saleOrder.getSaleOrderItems()) {
                saleOrderItemCodes.add(soi.getCode());
            }
            return saleOrderItemCodes;
        }
        return null;
    }

    @Transactional
    @Override
    public Long getUnprocessedSaleOrderItemCountByChannelProductId(int channelId, String channelProductId) {
        return saleOrderDao.getUnprocessedSaleOrderItemCount(channelId, channelProductId);
    }

    @Transactional
    @Override
    public Map<String, Long> getUnprocessedSaleOrderItemsCount(int channelId) {
        return saleOrderDao.getUnprocessedSaleOrderItemsCount(channelId);
    }

    @Override
    public boolean splitsBundleItems(String saleOrderCode, List<String> saleOrderItemCodes, List<String> picklistSaleorderItemCodes) {
        for (String saleOrderItemCode : saleOrderItemCodes) {
            List<String> bundleItemCodes = saleOrderDao.getBundleItemsByCode(saleOrderCode, saleOrderItemCode);
            for (String bundleItemCode : bundleItemCodes) {
                if (!saleOrderItemCodes.contains(bundleItemCode) && picklistSaleorderItemCodes.contains(bundleItemCode))
                    return true;
            }
        }
        return false;
    }

    /*
     *  Return cancelled saleOrderItem in picklist if any otherwise return least priority saleOrderItem
     */
    @Override
    @Transactional
    public SaleOrderItem getSaleOrderItemForDeallocationInPicklist(String picklistCode, Item item) {
        SaleOrderItem saleOrderItem = saleOrderDao.getSoftAllocatedPicklistItemForDeallocationInPicklist(picklistCode, item);
        // If PickingViaHandledEnabled then there must be a soft allocated picklist item
        if (!ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).isPickingViaHandheldEnabled()) {
            if (saleOrderItem == null) {
                saleOrderItem = saleOrderDao.getCancelledSaleOrderItemForDeallocationInPicklist(picklistCode, item);
            }
            if (saleOrderItem == null) {
                saleOrderItem = saleOrderDao.getSaleOrderItemForDeallocationInPicklist(picklistCode, item);
            }
        }
        return saleOrderItem;
    }

    @Override
    public List<SaleOrderItem> getFulfillableSaleOrderItemsForItemTypeInventory(int itemTypeInventoryId, int numberOfItems) {
        return saleOrderDao.getFulfillableSaleOrderItemsForItemTypeInventory(itemTypeInventoryId, numberOfItems);
    }

    @Override
    @Transactional
    public SaleOrderItem getSaleOrderItemForDeallocation(Item item) {
        return saleOrderDao.getSaleOrderItemForDeallocation(item);
    }

    @Override
    @Transactional(readOnly = true)
    public SaleOrderItem getSaleOrderItemForAllocationInPicklist(String picklistCode, Item item, String shippingPackageCode) {
        SaleOrderItem saleOrderItem = saleOrderDao.getSaleOrderItemForAllocationInPicklist(picklistCode, item, shippingPackageCode);
        if (saleOrderItem == null) {
            saleOrderItem = saleOrderDao.getSaleOrderItemForAllocationInPicklist(picklistCode, item, shippingPackageCode, true);
        }
        return saleOrderItem;
    }

    @Override
    @Transactional(readOnly = true)
    public Long getUnserviceableSaleOrderItemsCountByChannelProductId(String channelProductId) {
        return saleOrderDao.getSaleOrderItemsCountByChannelProductIdAndStatus(channelProductId,
                Collections.singletonList(SaleOrderItem.StatusCode.LOCATION_NOT_SERVICEABLE.name()));
    }

    @Override
    public void updateSaleOrderItemStatusForPicklistCreation(Set<Integer> saleOrderItemIds, String statusCode) {
        saleOrderDao.updateSaleOrderItemStatusForPicklistCreation(saleOrderItemIds, statusCode);
    }
}
