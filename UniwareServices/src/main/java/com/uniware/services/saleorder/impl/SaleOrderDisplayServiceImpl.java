/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *  @version     1.0, Dec 15, 2011
 *  @author singla
 */
package com.uniware.services.saleorder.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import com.uniware.core.entity.PicklistItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.unifier.core.api.audit.FetchEntityActivityRequest;
import com.unifier.core.api.audit.FetchEntityActivityResponse;
import com.unifier.core.api.comments.GetCommentsRequest;
import com.unifier.core.api.comments.GetCommentsResponse;
import com.unifier.core.api.comments.UserCommentDTO;
import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.utils.StringUtils;
import com.unifier.services.comments.ICommentsService;
import com.unifier.services.utils.CustomFieldUtils;
import com.uniware.core.api.channel.GetShippingPackageTaxSummaryRequest;
import com.uniware.core.api.channel.GetShippingPackageTaxSummaryResponse;
import com.uniware.core.api.channel.GetShippingPackageTaxSummaryResponse.ShippingPackageTaxDTO;
import com.uniware.core.api.invoice.GetAllInvoicesDetailsRequest;
import com.uniware.core.api.invoice.GetAllInvoicesDetailsResponse;
import com.uniware.core.api.invoice.GetTaxDetailsRequest;
import com.uniware.core.api.invoice.GetTaxDetailsRequest.WsChannelItemType;
import com.uniware.core.api.invoice.GetTaxDetailsRequest.WsItemType;
import com.uniware.core.api.invoice.GetTaxDetailsResponse;
import com.uniware.core.api.invoice.GetTaxDetailsResponse.TaxDTO;
import com.uniware.core.api.invoice.InvoiceDTO;
import com.uniware.core.api.model.WsAddressDetail;
import com.uniware.core.api.packer.RegulatoryFormDTO;
import com.uniware.core.api.returns.GetReversePickupInvoiceRequest;
import com.uniware.core.api.returns.GetReversePickupInvoiceResponse;
import com.uniware.core.api.returns.ReversePickupDTO;
import com.uniware.core.api.returns.ReversePickupDTO.ReplacedSaleOrderItemDTO;
import com.uniware.core.api.saleorder.AddressDTO;
import com.uniware.core.api.saleorder.display.GetSaleOrderActivitiesRequest;
import com.uniware.core.api.saleorder.display.GetSaleOrderActivitiesResponse;
import com.uniware.core.api.saleorder.display.GetSaleOrderActivitiesResponse.ActivityDTO;
import com.uniware.core.api.saleorder.display.GetSaleOrderInvoicesRequest;
import com.uniware.core.api.saleorder.display.GetSaleOrderInvoicesResponse;
import com.uniware.core.api.saleorder.display.GetSaleOrderLineItemDetailsRequest;
import com.uniware.core.api.saleorder.display.GetSaleOrderLineItemDetailsResponse;
import com.uniware.core.api.saleorder.display.GetSaleOrderLineItemDetailsResponse.LineItemDetail;
import com.uniware.core.api.saleorder.display.GetSaleOrderLineItemDetailsResponse.SaleOrderItemDTO;
import com.uniware.core.api.saleorder.display.GetSaleOrderLineItemsRequest;
import com.uniware.core.api.saleorder.display.GetSaleOrderLineItemsResponse;
import com.uniware.core.api.saleorder.display.GetSaleOrderLineItemsResponse.SaleOrderLineItemDTO;
import com.uniware.core.api.saleorder.display.GetSaleOrderPriceSummaryRequest;
import com.uniware.core.api.saleorder.display.GetSaleOrderPriceSummaryResponse;
import com.uniware.core.api.saleorder.display.GetSaleOrderPriceSummaryResponse.SaleOrderPriceSummaryDTO;
import com.uniware.core.api.saleorder.display.GetSaleOrderReturnsRequest;
import com.uniware.core.api.saleorder.display.GetSaleOrderReturnsResponse;
import com.uniware.core.api.saleorder.display.GetSaleOrderReturnsResponse.SaleOrderReturnDTO;
import com.uniware.core.api.saleorder.display.GetSaleOrderShippingPackagesRequest;
import com.uniware.core.api.saleorder.display.GetSaleOrderShippingPackagesResponse;
import com.uniware.core.api.saleorder.display.GetSaleOrderShippingPackagesResponse.ShippingPackageFullDTO;
import com.uniware.core.api.saleorder.display.GetSaleOrderShippingPackagesResponse.ShippingPackageLineItem;
import com.uniware.core.api.saleorder.display.GetSaleOrderShippingPackagesResponse.StateRegulatoryForm;
import com.uniware.core.api.saleorder.display.GetSaleOrderSummaryRequest;
import com.uniware.core.api.saleorder.display.GetSaleOrderSummaryResponse;
import com.uniware.core.api.saleorder.display.GetSaleOrderSummaryResponse.ItemStatusDTO;
import com.uniware.core.api.saleorder.display.GetSaleOrderSummaryResponse.SaleOrderSummaryDTO;
import com.uniware.core.api.shipping.GetApplicableRegulatoryFormsRequest;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.cache.FacilityCache;
import com.uniware.core.cache.LocationCache;
import com.uniware.core.entity.AddressDetail;
import com.uniware.core.entity.Facility;
import com.uniware.core.entity.InvoiceItem;
import com.uniware.core.entity.InvoiceItemTax;
import com.uniware.core.entity.ItemType;
import com.uniware.core.entity.ItemTypeInventory;
import com.uniware.core.entity.PartyAddress;
import com.uniware.core.entity.PartyAddressType;
import com.uniware.core.entity.Picklist;
import com.uniware.core.entity.ReturnManifestItem;
import com.uniware.core.entity.ReversePickup;
import com.uniware.core.entity.SaleOrder;
import com.uniware.core.entity.SaleOrderItem;
import com.uniware.core.entity.ShippingPackage;
import com.uniware.core.entity.Source;
import com.uniware.core.entity.TaxType;
import com.uniware.core.vo.ActivityMetaVO;
import com.uniware.core.vo.ChannelItemTypeTaxVO;
import com.uniware.dao.saleorder.ISaleOrderDao;
import com.uniware.services.audit.IActivityLogService;
import com.uniware.services.audit.impl.ActivityEntityEnum;
import com.uniware.services.audit.impl.ActivityTypeEnum;
import com.uniware.services.cache.ReturnsCache;
import com.uniware.services.channel.IChannelCatalogService;
import com.uniware.services.configuration.SourceConfiguration;
import com.uniware.services.configuration.SystemConfiguration;
import com.uniware.services.inventory.IInventoryService;
import com.uniware.services.invoice.IInvoiceService;
import com.uniware.services.picker.IPickerService;
import com.uniware.services.returns.IReturnsService;
import com.uniware.services.saleorder.ISaleOrderDisplayService;
import com.uniware.services.shipping.IRegulatoryService;
import com.uniware.services.shipping.IShippingInvoiceService;
import com.uniware.services.shipping.IShippingService;

/**
 * @author singla
 */
@Service("saleOrderDisplayService")
public class SaleOrderDisplayServiceImpl implements ISaleOrderDisplayService {

    @Autowired
    private IShippingService        shippingService;

    @Autowired
    private IInvoiceService         invoiceService;

    @Autowired
    private ISaleOrderDao           saleOrderDao;

    @Autowired
    private IReturnsService         returnsService;

    @Autowired
    private IPickerService          pickerService;

    @Autowired
    private IRegulatoryService      regulatoryService;

    @Autowired
    private IActivityLogService     activityLogService;

    @Autowired
    private ICommentsService        commentsService;

    @Autowired
    private IChannelCatalogService  channelCatalogService;

    @Autowired
    private IInventoryService       inventoryService;

    @Autowired
    private IShippingInvoiceService shippingInvoiceService;

    @Override
    @Transactional(readOnly = true)
    public GetSaleOrderSummaryResponse getSaleOrderSummary(GetSaleOrderSummaryRequest request) {
        GetSaleOrderSummaryResponse response = new GetSaleOrderSummaryResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            SaleOrder saleOrder = saleOrderDao.getSaleOrderByCode(request.getCode());
            if (saleOrder == null) {
                context.addError(WsResponseCode.INVALID_SALE_ORDER_CODE);
            } else {
                int activityCount = 0;
                activityCount += activityLogService.getActivityLogCountbyEntityId(saleOrder.getCode(), ActivityEntityEnum.SALE_ORDER.getName());
                activityCount += commentsService.getCommentsCount("SO-" + saleOrder.getCode());

                int returnsCount = 0;
                returnsCount += shippingService.getReturnsCountBySaleOrder(saleOrder.getCode());
                returnsCount += saleOrder.getReversePickups().size();

                int invoiceCount = 0;
                invoiceCount += shippingService.getInvoiceCountBySaleOrder(saleOrder.getCode());

                int shipmentCount = 0;
                shipmentCount += shippingService.getShipmentCountBySaleOrder(saleOrder.getCode());

                int saleOrderItemCount = 0;
                Facility currentFacility = CacheManager.getInstance().getCache(FacilityCache.class).getCurrentFacility();
                saleOrderItemCount += getSaleOrderLineItemsCount(currentFacility, saleOrder);

                prepareSaleOrderSummary(response, saleOrder, activityCount, returnsCount, invoiceCount, shipmentCount, saleOrderItemCount);
                response.setSuccessful(true);
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    private int getSaleOrderLineItemsCount(Facility currentFacility, SaleOrder saleOrder) {
        int orderCount = 0;
        for (SaleOrderItem soi : saleOrder.getSaleOrderItems()) {
            if (currentFacility != null && Facility.Type.DROPSHIP.name().equals(currentFacility.getType())) {
                if (soi.getFacility() != null && currentFacility.getCode().equals(soi.getFacility().getCode())) {
                    orderCount++;
                }
            } else {
                orderCount++;
            }
        }
        return orderCount;
    }

    @Override
    @Transactional(readOnly = true)
    public GetSaleOrderPriceSummaryResponse getSaleOrderPriceSummary(GetSaleOrderPriceSummaryRequest request) {
        GetSaleOrderPriceSummaryResponse response = new GetSaleOrderPriceSummaryResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            SaleOrder saleOrder = saleOrderDao.getSaleOrderByCode(request.getCode());
            if (saleOrder == null) {
                context.addError(WsResponseCode.INVALID_SALE_ORDER_CODE);
            } else {
                SaleOrderPriceSummaryDTO saleOrderPriceSummary = new SaleOrderPriceSummaryDTO();
                prepareSaleOrderPriceSummary(saleOrderPriceSummary, saleOrder, context);
                if (!context.hasErrors()) {
                    response.setSaleOrderPriceSummary(saleOrderPriceSummary);
                    response.setSuccessful(true);
                } else {
                    response.setErrors(context.getErrors());
                }
            }
        }
        return response;
    }

    @Override
    @Transactional(readOnly = true)
    public GetSaleOrderLineItemsResponse getSaleOrderLineItems(GetSaleOrderLineItemsRequest request) {
        GetSaleOrderLineItemsResponse response = new GetSaleOrderLineItemsResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            SaleOrder saleOrder = saleOrderDao.getSaleOrderByCode(request.getCode());
            if (saleOrder == null) {
                context.addError(WsResponseCode.INVALID_SALE_ORDER_CODE);
            } else {
                prepareSaleOrderLineItems(response, saleOrder);
                response.setSuccessful(true);
            }
        }
        return response;
    }

    @Override
    @Transactional(readOnly = true)
    public GetSaleOrderLineItemDetailsResponse getSaleOrderLineItemDetails(GetSaleOrderLineItemDetailsRequest request) {
        GetSaleOrderLineItemDetailsResponse response = new GetSaleOrderLineItemDetailsResponse();
        ValidationContext context = request.validate();
        Map<String, LineItem> lineItemsMap = new HashMap<>();
        if (!context.hasErrors()) {
            SaleOrder saleOrder = saleOrderDao.getSaleOrderByCode(request.getCode());
            if (saleOrder == null) {
                context.addError(WsResponseCode.INVALID_SALE_ORDER_CODE);
            } else {
                Facility currentFacility = CacheManager.getInstance().getCache(FacilityCache.class).getCurrentFacility();
                FacilitySaleOrderItems facilitySaleOrderItems = getFacilitySaleOrderItems(saleOrder, currentFacility);
                if (currentFacility != null && Facility.Type.DROPSHIP.name().equals(currentFacility.getType())) {
                    if (facilitySaleOrderItems.getCurrentFacilitySaleOrderItems().isEmpty()) {
                        response.setInvalidFacility(true);
                        response.setValidFacilities(facilitySaleOrderItems.getValidFacilities());
                    } else {
                        lineItemsMap = getSaleOrderLineItems(facilitySaleOrderItems.getCurrentFacilitySaleOrderItems());
                    }
                } else {
                    lineItemsMap = getSaleOrderLineItems(facilitySaleOrderItems.getAllSaleOrderItems());
                }
            }
        }
        if (!context.hasErrors()) {
            List<LineItemDetail> lineItemDetails = new ArrayList<>();
            for (String lineItemIdentifier : request.getLineItemIdentifiers()) {
                if (!lineItemsMap.containsKey(lineItemIdentifier)) {
                    context.addError(WsResponseCode.INVALID_SALE_ORDER_LINE_ITEM, "Invalid lineItem: " + lineItemIdentifier);
                    break;
                } else {
                    List<SaleOrderItem> saleOrderItems = lineItemsMap.get(lineItemIdentifier).getSaleOrderItems();
                    LineItemDetail lineItemDetail = new LineItemDetail();
                    lineItemDetail.setLineItemIdentifier(lineItemIdentifier);
                    List<GetSaleOrderLineItemDetailsResponse.SaleOrderItemDTO> saleOrderItemDTOs = new ArrayList<>(saleOrderItems.size());
                    for (SaleOrderItem soi : saleOrderItems) {
                        SaleOrderItemDTO saleOrderItemDTO = new SaleOrderItemDTO();
                        prepareSaleOrderItem(saleOrderItemDTO, soi);
                        saleOrderItemDTOs.add(saleOrderItemDTO);
                    }
                    lineItemDetail.setSaleOrderItems(saleOrderItemDTOs);
                    lineItemDetails.add(lineItemDetail);
                }
            }
            response.setLineItemDetails(lineItemDetails);
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        } else {
            response.setSuccessful(true);
        }
        return response;
    }

    @Override
    @Transactional(readOnly = true)
    public GetSaleOrderInvoicesResponse getSaleOrderInvoices(GetSaleOrderInvoicesRequest request) {
        GetSaleOrderInvoicesResponse response = new GetSaleOrderInvoicesResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            SaleOrder saleOrder = saleOrderDao.getSaleOrderByCode(request.getSaleOrderCode());
            if (saleOrder == null) {
                context.addError(WsResponseCode.INVALID_SALE_ORDER_CODE);
            } else {
                List<ShippingPackage> shippingPackageList = shippingService.getShippingPackagesBySaleOrder(saleOrder.getCode());
                List<InvoiceDTO> invoices = new ArrayList<>(2 * shippingPackageList.size());
                for (ShippingPackage shippingPackage : shippingPackageList) {
                    GetAllInvoicesDetailsRequest invoiceDetailsRequest = new GetAllInvoicesDetailsRequest();
                    invoiceDetailsRequest.setShippingPackageCode(shippingPackage.getCode());
                    GetAllInvoicesDetailsResponse invoiceDetailsResponse = shippingInvoiceService.getAllShippingPackageInvoicesDetails(invoiceDetailsRequest);
                    if (invoiceDetailsResponse.isSuccessful()) {
                        invoices.addAll(invoiceDetailsResponse.getInvoices());
                    }
                }
                Set<ReversePickup> reversePickups = saleOrder.getSaleOrderItems().stream().filter(soi -> soi.getReversePickup() != null).map(
                        SaleOrderItem::getReversePickup).collect(Collectors.toSet());
                for (ReversePickup reversePickup : reversePickups) {
                    GetReversePickupInvoiceRequest reversePickupInvoiceRequest = new GetReversePickupInvoiceRequest();
                    reversePickupInvoiceRequest.setReversePickupCode(reversePickup.getCode());
                    GetReversePickupInvoiceResponse reversePickupInvoicesDetails = returnsService.getReversePickupInvoicesDetails(reversePickupInvoiceRequest);
                    if (reversePickupInvoicesDetails.isSuccessful()) {
                        invoices.add(reversePickupInvoicesDetails.getInvoice());
                    }
                }
                response.setInvoices(invoices);
                response.setSuccessful(true);
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    @Override
    @Transactional(readOnly = true)
    public GetSaleOrderShippingPackagesResponse getSaleOrderShippingPackages(GetSaleOrderShippingPackagesRequest request) {
        GetSaleOrderShippingPackagesResponse response = new GetSaleOrderShippingPackagesResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            SaleOrder saleOrder = saleOrderDao.getSaleOrderByCode(request.getSaleOrderCode());
            if (saleOrder == null) {
                context.addError(WsResponseCode.INVALID_SALE_ORDER_CODE);
            } else {
                List<ShippingPackage> shippingPackageList = shippingService.getShippingPackagesBySaleOrder(saleOrder.getCode());
                List<ShippingPackageFullDTO> shippingPackages = new ArrayList<>(shippingPackageList.size());
                for (ShippingPackage sp : shippingPackageList) {
                    ShippingPackage shippingPackage = shippingService.getShippingPackageByCode(sp.getCode());
                    if (shippingPackage != null) {
                        ShippingPackageFullDTO shippingPackageFullDTO = new ShippingPackageFullDTO();
                        prepareShippingPackageFullDTO(shippingPackageFullDTO, shippingPackage);
                        shippingPackages.add(shippingPackageFullDTO);
                    }
                }
                response.setShippingPackages(shippingPackages);
                response.setSuccessful(true);
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    @Override
    @Transactional(readOnly = true)
    public GetShippingPackageTaxSummaryResponse getShippingPackageTaxSummary(GetShippingPackageTaxSummaryRequest request) {
        GetShippingPackageTaxSummaryResponse response = new GetShippingPackageTaxSummaryResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            List<ShippingPackageTaxDTO> shippingPackageTaxes = new ArrayList<>(request.getShippingPackageCodes().size());
            for (String shippingPackageCode : request.getShippingPackageCodes()) {
                ShippingPackage shippingPackage = shippingService.getShippingPackageByCode(shippingPackageCode);
                if (shippingPackage == null) {
                    context.addError(WsResponseCode.INVALID_SHIPPING_PACKAGE_CODE, "Invalid shipping package code: " + shippingPackageCode);
                    break;
                } else {
                    ShippingPackageTaxDTO shippingPackageTaxDTO = new ShippingPackageTaxDTO();
                    shippingPackageTaxDTO.setCode(shippingPackageCode);
                    shippingPackageTaxDTO.setChannelCode(shippingPackage.getSaleOrder().getChannel().getCode());
                    Map<String, List<SaleOrderItem>> lineItemToSaleOrderItems = new HashMap<>();
                    for (SaleOrderItem soi : shippingPackage.getSaleOrderItems()) {
                        String lineItemIdentifier = soi.getChannelProductId();
                        List<SaleOrderItem> saleOrderItems = lineItemToSaleOrderItems.computeIfAbsent(lineItemIdentifier, k -> new ArrayList<>());
                        saleOrderItems.add(soi);
                    }

                    Facility facility = CacheManager.getInstance().getCache(FacilityCache.class).getCurrentFacility();
                    if (facility.getTaxPercentage() != null) {
                        response.setGlobalTaxPercentage(facility.getTaxPercentage());
                    }
                    List<ShippingPackageTaxDTO.ShippingPackageLineItem> packageLineItems = new ArrayList<>(lineItemToSaleOrderItems.size());
                    for (Entry<String, List<SaleOrderItem>> lineItem : lineItemToSaleOrderItems.entrySet()) {
                        ShippingPackageTaxDTO.ShippingPackageLineItem shippingPackageLineItem = new ShippingPackageTaxDTO.ShippingPackageLineItem();
                        SaleOrderItem soi = lineItem.getValue().iterator().next();
                        shippingPackageLineItem.setLineItemIdentifier(lineItem.getKey());
                        shippingPackageLineItem.setSellerSkuCode(lineItem.getValue().get(0).getSellerSkuCode());
                        shippingPackageLineItem.setItemName(soi.getChannelProductName());
                        shippingPackageLineItem.setQuantity(lineItem.getValue().size());
                        ChannelItemTypeTaxVO channelItemTypeTax = channelCatalogService.getChannelItemTypeTax(shippingPackage.getSaleOrder().getChannel().getCode(),
                                lineItem.getKey());
                        shippingPackageLineItem.setCstPercentage(channelItemTypeTax != null ? channelItemTypeTax.getCstPercentage() : null);
                        shippingPackageLineItem.setVatPercentage(channelItemTypeTax != null ? channelItemTypeTax.getVatPercentage() : null);
                        shippingPackageLineItem.setHsnCode(channelItemTypeTax != null ? channelItemTypeTax.getHsnCode() : null);
                        shippingPackageLineItem.setCentralGstPercentage(channelItemTypeTax != null ? channelItemTypeTax.getCentralGstPercentage() : null);
                        shippingPackageLineItem.setStateGstPercentage(channelItemTypeTax != null ? channelItemTypeTax.getStateGstPercentage() : null);
                        shippingPackageLineItem.setUnionTerritoryGstPercentage(channelItemTypeTax != null ? channelItemTypeTax.getUnionTerritoryGstPercentage() : null);
                        shippingPackageLineItem.setIntegratedGstPercentage(channelItemTypeTax != null ? channelItemTypeTax.getIntegratedGstPercentage() : null);
                        shippingPackageLineItem.setCompensationCessPercentage(channelItemTypeTax != null ? channelItemTypeTax.getCompensationCessPercentage() : null);
                        packageLineItems.add(shippingPackageLineItem);
                    }
                    shippingPackageTaxDTO.setLineItems(packageLineItems);
                    shippingPackageTaxes.add(shippingPackageTaxDTO);
                }
            }
            if (!context.hasErrors()) {
                response.setShippingPackageTaxes(shippingPackageTaxes);
                response.setSuccessful(true);
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    @Override
    @Transactional(readOnly = true)
    public GetSaleOrderReturnsResponse getSaleOrderReturns(GetSaleOrderReturnsRequest request) {
        GetSaleOrderReturnsResponse response = new GetSaleOrderReturnsResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            SaleOrder saleOrder = saleOrderDao.getSaleOrderByCode(request.getSaleOrderCode());
            if (saleOrder == null) {
                context.addError(WsResponseCode.INVALID_SALE_ORDER_CODE);
            } else {
                List<SaleOrderReturnDTO> saleOrderReturns = new ArrayList<>();
                for (ReversePickup reversePickup : saleOrder.getReversePickups()) {
                    ReversePickupDTO reversePickupDTO = returnsService.prepareReversePickupDTO(reversePickup);
                    saleOrderReturns.add(prepareSaleOrderReturnDTO(reversePickupDTO));
                }
                for (ShippingPackage shippingPackage : shippingService.getShippingPackagesBySaleOrder(saleOrder.getCode())) {
                    if (StringUtils.equalsAny(shippingPackage.getStatusCode(), ShippingPackage.StatusCode.RETURN_EXPECTED.name(),
                            ShippingPackage.StatusCode.RETURN_ACKNOWLEDGED.name(), ShippingPackage.StatusCode.RETURNED.name())) {
                        saleOrderReturns.add(prepareSaleOrderReturnDTO(shippingPackage));
                    }
                }
                response.setSaleOrderReturns(saleOrderReturns);
                response.setSuccessful(true);
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    private SaleOrderReturnDTO prepareSaleOrderReturnDTO(ShippingPackage shippingPackage) {
        SaleOrderReturnDTO saleOrderReturnDTO = new SaleOrderReturnDTO();
        saleOrderReturnDTO.setCode(shippingPackage.getCode());
        saleOrderReturnDTO.setType(SaleOrderReturnDTO.Type.COURIER_RETURNED.getName());
        saleOrderReturnDTO.setCreated(shippingPackage.getCreated());
        saleOrderReturnDTO.setStatusCode(shippingPackage.getStatusCode());
        if (shippingPackage.getReshipment() != null) {
            if (shippingPackage.getReshipment().getReshipmentSaleOrder() != null) {
                saleOrderReturnDTO.setReshipmentSaleOrderCode(shippingPackage.getReshipment().getReshipmentSaleOrder().getCode());
            }
            saleOrderReturnDTO.setActionCode(
                    CacheManager.getInstance().getCache(ReturnsCache.class).getReshipmentActionByCode(shippingPackage.getReshipment().getReshipmentAction()).getDescription());
        }
        ReturnManifestItem returnManifestItem = shippingService.getReturnManifestItemForShippingPackage(shippingPackage);
        if (returnManifestItem != null) {
            saleOrderReturnDTO.setReturnReason(returnManifestItem.getReturnReason());
        }
        List<SaleOrderReturnDTO.SaleOrderItemDTO> saleOrderItems = new ArrayList<>();
        for (SaleOrderItem saleOrderItem : shippingPackage.getSaleOrderItems()) {
            SaleOrderReturnDTO.SaleOrderItemDTO saleOrderItemDTO = new SaleOrderReturnDTO.SaleOrderItemDTO();
            saleOrderItemDTO.setCode(saleOrderItem.getCode());
            saleOrderItemDTO.setProductName(
                    saleOrderItem.getSaleOrder().isProductManagementSwitchedOff() ? saleOrderItem.getChannelProductName() : saleOrderItem.getItemType().getName());
            saleOrderItemDTO.setItemSku(saleOrderItem.getItemType().getSkuCode());
            if (saleOrderItem.getItemTypeInventory() != null) {
                ItemTypeInventory inventory = inventoryService.getItemTypeInventoryById(saleOrderItem.getItemTypeInventory().getId());
                saleOrderItemDTO.setShelfCode(inventory.getShelf().getCode());
            }
            saleOrderItemDTO.setStatusCode(saleOrderItem.getStatusCode());
            saleOrderItems.add(saleOrderItemDTO);
        }
        saleOrderReturnDTO.setSaleOrderItems(saleOrderItems);
        return saleOrderReturnDTO;
    }

    private SaleOrderReturnDTO prepareSaleOrderReturnDTO(ReversePickupDTO reversePickupDTO) {
        SaleOrderReturnDTO saleOrderReturnDTO = new SaleOrderReturnDTO();
        saleOrderReturnDTO.setCode(reversePickupDTO.getCode());
        saleOrderReturnDTO.setType(SaleOrderReturnDTO.Type.CUSTOMER_RETURNED.getName());
        saleOrderReturnDTO.setCreated(reversePickupDTO.getCreated());
        saleOrderReturnDTO.setStatusCode(reversePickupDTO.getStatusCode());
        saleOrderReturnDTO.setActionCode(reversePickupDTO.getActionCode());
        saleOrderReturnDTO.setReturnReason(reversePickupDTO.getReason());
        saleOrderReturnDTO.setReplacementSaleOrderCode(reversePickupDTO.getReplacementSaleOrderCode());
        saleOrderReturnDTO.setTrackingNumber(reversePickupDTO.getTrackingNumber());
        saleOrderReturnDTO.setShippingProvider(reversePickupDTO.getShippingProvider());
        saleOrderReturnDTO.setRedispatchManifestCode(reversePickupDTO.getRedispatchManifestCode());
        saleOrderReturnDTO.setRedispatchReason(reversePickupDTO.getRedispatchReason());
        saleOrderReturnDTO.setRedispatchShippingProvider(reversePickupDTO.getRedispatchShippingProvider());
        saleOrderReturnDTO.setRedispatchTrackingNumber(reversePickupDTO.getRedispatchTrackingNumber());
        saleOrderReturnDTO.setReversePickupCustomFieldValues(reversePickupDTO.getCustomFieldValues());
        List<SaleOrderReturnDTO.SaleOrderItemDTO> saleOrderItems = new ArrayList<>();
        for (com.uniware.core.api.packer.SaleOrderItemDTO saleOrderItemDTO : reversePickupDTO.getSaleOrderItems()) {
            SaleOrderReturnDTO.SaleOrderItemDTO saleOrderItem = new SaleOrderReturnDTO.SaleOrderItemDTO();
            saleOrderItem.setCode(saleOrderItemDTO.getCode());
            saleOrderItem.setProductName(saleOrderItemDTO.getItemName());
            saleOrderItem.setItemSku(saleOrderItemDTO.getItemSku());
            saleOrderItem.setShelfCode(saleOrderItemDTO.getShelfCode());
            saleOrderItem.setStatusCode(saleOrderItemDTO.getStatusCode());
            saleOrderItems.add(saleOrderItem);
        }
        saleOrderReturnDTO.setSaleOrderItems(saleOrderItems);
        for (ReplacedSaleOrderItemDTO replacedSaleOrderItem : reversePickupDTO.getReplacedSaleOrderItems()) {
            SaleOrderReturnDTO.ReplacedSaleOrderItemDTO replacedSOI = new SaleOrderReturnDTO.ReplacedSaleOrderItemDTO();
            replacedSOI.setItemName(replacedSaleOrderItem.getItemName());
            replacedSOI.setItemSku(replacedSaleOrderItem.getItemSku());
            replacedSOI.setSaleOrderItemCode(replacedSaleOrderItem.getSaleOrderItemCode());
            replacedSOI.setSaleOrderItemId(replacedSaleOrderItem.getSaleOrderItemId());
            saleOrderReturnDTO.getReplacedSaleOrderItems().add(replacedSOI);
        }
        return saleOrderReturnDTO;
    }

    @Override
    @Transactional(readOnly = true)
    public GetSaleOrderActivitiesResponse getSaleOrderActivities(GetSaleOrderActivitiesRequest request) {
        GetSaleOrderActivitiesResponse response = new GetSaleOrderActivitiesResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            SaleOrder saleOrder = saleOrderDao.getSaleOrderByCode(request.getSaleOrderCode());
            if (saleOrder == null) {
                context.addError(WsResponseCode.INVALID_SALE_ORDER_CODE);
            } else {
                List<ActivityDTO> activities = new ArrayList<>();
                FetchEntityActivityRequest fetchEntityActivityRequest = new FetchEntityActivityRequest();
                fetchEntityActivityRequest.setEntityName(ActivityEntityEnum.SALE_ORDER.getName());
                fetchEntityActivityRequest.setEntityIdentifier(saleOrder.getCode());
                fetchEntityActivityRequest.setSort(false);
                FetchEntityActivityResponse fetchEntityActivityResponse = activityLogService.getActivityLogsbyEntityId(fetchEntityActivityRequest);
                if (fetchEntityActivityResponse.isSuccessful()) {
                    for (ActivityMetaVO activityMetaVO : fetchEntityActivityResponse.getEntityActivityLogs()) {
                        ActivityDTO activityDTO = new ActivityDTO();
                        prepareActivityDTO(activityDTO, activityMetaVO);
                        activities.add(activityDTO);
                    }
                }
                GetCommentsRequest getCommentsRequest = new GetCommentsRequest();
                getCommentsRequest.setReferenceIdentifier("SO-" + saleOrder.getCode());
                GetCommentsResponse getCommentsResponse = commentsService.getComments(getCommentsRequest);
                if (getCommentsResponse.isSuccessful()) {
                    for (UserCommentDTO userCommentDTO : getCommentsResponse.getUserCommentDTOs()) {
                        ActivityDTO activityDTO = new ActivityDTO();
                        prepareActivityDTO(activityDTO, userCommentDTO);
                        activities.add(activityDTO);
                    }
                }
                Collections.sort(activities);
                response.setActivities(activities);
                response.setSuccessful(true);
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    private void prepareActivityDTO(ActivityDTO activityDTO, UserCommentDTO userCommentDTO) {
        activityDTO.setActivityType(ActivityTypeEnum.COMMENT.name());
        activityDTO.setLog(userCommentDTO.getComment());
        activityDTO.setCreated(userCommentDTO.getCreated());
        if (userCommentDTO.isSystemGenerated()) {
            activityDTO.setUsername("System");
        } else {
            activityDTO.setUsername(userCommentDTO.getUsername());
        }
    }

    private void prepareActivityDTO(ActivityDTO activityDTO, ActivityMetaVO activityMetaVO) {
        activityDTO.setActivityType(activityMetaVO.getActivityType());
        activityDTO.setLog(activityMetaVO.getLog());
        activityDTO.setCreated(activityMetaVO.getCreated());
        if (StringUtils.isNotBlank(activityMetaVO.getUsername())) {
            activityDTO.setUsername(activityMetaVO.getUsername());
        } else if (StringUtils.isNotBlank(activityMetaVO.getApiUsername())) {
            activityDTO.setUsername(activityMetaVO.getApiUsername());
        } else {
            activityDTO.setUsername("System");
        }
    }

    private void prepareShippingPackageFullDTO(ShippingPackageFullDTO shippingPackageDTO, ShippingPackage sp) {
        shippingPackageDTO.setCode(sp.getCode());
        shippingPackageDTO.setShippingProviderCode(sp.getShippingProviderCode());
        shippingPackageDTO.setShippingMethodCode(sp.getShippingMethod() != null ? sp.getShippingMethod().getShippingMethodCode() : null);
        shippingPackageDTO.setShippingPackageType(sp.getShippingPackageType().getCode());
        shippingPackageDTO.setShippingAddress(new AddressDTO(sp.getShippingAddress()));
        shippingPackageDTO.setTrackingStatus(sp.getShipmentTrackingStatusCode());
        shippingPackageDTO.setCourierStatus(sp.getProviderStatusCode());
        shippingPackageDTO.setDeliveryTime(sp.getDeliveryTime());
        shippingPackageDTO.setEstimatedWeight(new BigDecimal(sp.getEstimatedWeight()).divide(new BigDecimal(1000.0), 3, RoundingMode.HALF_EVEN));
        shippingPackageDTO.setActualWeight(new BigDecimal(sp.getActualWeight()).divide(new BigDecimal(1000.0), 3, RoundingMode.HALF_EVEN));
        shippingPackageDTO.setCreated(sp.getCreated());
        shippingPackageDTO.setReshipmentCode(sp.getReshipment() != null ? sp.getReshipment().getCode() : null);
        shippingPackageDTO.setParentPackageCode(sp.getParentShippingPackage() != null ? sp.getParentShippingPackage().getCode() : null);
        shippingPackageDTO.setNoOfItems(sp.getNoOfItems());
        shippingPackageDTO.setNoOfBoxes(sp.getNoOfBoxes());
        shippingPackageDTO.setInvoiceCode(sp.getInvoice() != null ? sp.getInvoice().getCode() : null);
        shippingPackageDTO.setInvoiceDisplayCode(sp.getInvoice() != null ? sp.getInvoice().getDisplayCode() : null);
        shippingPackageDTO.setReturnInvoiceDisplayCode(sp.getReturnInvoice() != null ? sp.getReturnInvoice().getDisplayCode() : null);
        Picklist picklist = pickerService.getPicklistForShippingPackage(sp.getCode());
        if (picklist != null) {
            shippingPackageDTO.setPicklistNumber(picklist.getCode());
            shippingPackageDTO.setZone(picklist.getPickSetName());
        }
        shippingPackageDTO.setLength(sp.getBoxLength());
        shippingPackageDTO.setWidth(sp.getBoxWidth());
        shippingPackageDTO.setHeight(sp.getBoxHeight());
        shippingPackageDTO.setTrackingNumber(sp.getTrackingNumber());
        shippingPackageDTO.setShippingLabelLink(sp.getShippingLabelLink());
        shippingPackageDTO.setStatusCode(sp.getStatusCode());
        shippingPackageDTO.setTotalPrice(sp.getTotalPrice());
        shippingPackageDTO.setCollectableAmount(sp.getCollectableAmount());
        shippingPackageDTO.setDispatchTime(sp.getDispatchTime());
        shippingPackageDTO.setRequiresCustomization(sp.isRequiresCustomization());
        if (sp.getSaleOrderItems().size() > 1 && !shippingPackageDTO.isRepackageable()
                && StringUtils.equalsAny(sp.getStatusCode(), ShippingPackage.StatusCode.READY_TO_SHIP.name(), ShippingPackage.StatusCode.PACKED.name())) {
            shippingPackageDTO.setSplittable(true);
        }
        shippingPackageDTO.setPodVerified(sp.isPodVerified());
        shippingPackageDTO.setCustomFieldValues(CustomFieldUtils.getCustomFieldValuesDTO(sp));
        shippingPackageDTO.setSaleOrderCustomFieldValues(CustomFieldUtils.getCustomFieldValuesDTO(sp.getSaleOrder()));
        List<RegulatoryFormDTO> stateRegulatoryForms = regulatoryService.getApplicableRegulatoryForms(
                new GetApplicableRegulatoryFormsRequest(sp.getCode())).getStateRegulatoryForms();
        List<StateRegulatoryForm> forms = new ArrayList<>(stateRegulatoryForms.size());
        for (RegulatoryFormDTO stateRegulatoryForm : stateRegulatoryForms) {
            StateRegulatoryForm form = new StateRegulatoryForm();
            form.setCode(stateRegulatoryForm.getCode());
            form.setName(stateRegulatoryForm.getName());
            forms.add(form);
        }
        shippingPackageDTO.setStateRegulatoryForms(forms);
        if (sp.getReshipment() != null && sp.getReshipment().getReshipmentSaleOrder() != null) {
            shippingPackageDTO.setReshipmentSaleOrderCode(sp.getReshipment().getReshipmentSaleOrder().getCode());
        }
        shippingPackageDTO.setShippingManifestCode(sp.getShippingManifest() != null ? sp.getShippingManifest().getCode() : null);
        ReturnManifestItem returnManifestItem = shippingService.getReturnManifestItemForShippingPackage(sp);
        shippingPackageDTO.setReturnManifestCode(returnManifestItem != null ? returnManifestItem.getReturnManifest().getCode() : null);
        shippingPackageDTO.setChannelName(sp.getSaleOrder().getChannel().getName());
        shippingPackageDTO.setChannelCode(sp.getSaleOrder().getChannel().getCode());
        shippingPackageDTO.setThirdPartyShipping(sp.isThirdPartyShipping());
        Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(sp.getSaleOrder().getChannel().getSourceCode());
        shippingPackageDTO.setRefetchShippingLabelAllowed(sp.isThirdPartyShipping() && source.isRefetchShippingLabelAllowed());
        shippingPackageDTO.setOrderRefreshEnabled(source.isOrderRefreshEnabled());
        shippingPackageDTO.setProductManagementSwitchedOff(sp.getSaleOrder().isProductManagementSwitchedOff());
        shippingPackageDTO.setShippingManager(sp.getShippingManager());
        Map<GetSaleOrderShippingPackagesResponse.LineItemKey, List<SaleOrderItem>> lineItemToSaleOrderItems = new HashMap<>();
        Map<String, Map<String, PicklistItem>> skuToSoiCodeToPicklistItem = new HashMap<>();
        List<PicklistItem> picklistItems = pickerService.getPicklistItemsByShippingPackageCode(sp.getCode(), Picklist.Destination.STAGING);
        picklistItems.forEach(picklistItem -> skuToSoiCodeToPicklistItem.computeIfAbsent(picklistItem.getSkuCode(), k -> new HashMap<>()).put(picklistItem.getSaleOrderItemCode(), picklistItem));
        for (SaleOrderItem soi : sp.getSaleOrderItems()) {
            String lineItemIdentifier = sp.getSaleOrder().isProductManagementSwitchedOff() ? soi.getChannelProductId() : soi.getItemType().getSkuCode();
            PicklistItem picklistItem = skuToSoiCodeToPicklistItem.get(lineItemIdentifier) != null ? skuToSoiCodeToPicklistItem.get(lineItemIdentifier).get(soi.getCode()) : null;
            GetSaleOrderShippingPackagesResponse.LineItemKey lineItemKey = null;
            if(picklistItem == null){
                lineItemKey = new GetSaleOrderShippingPackagesResponse.LineItemKey(lineItemIdentifier, null, null);
            } else {
                lineItemKey = new GetSaleOrderShippingPackagesResponse.LineItemKey(lineItemIdentifier, picklistItem.getPicklist().getPickSetName(), picklistItem.getPicklist().getCode());
            }
            lineItemToSaleOrderItems.computeIfAbsent(lineItemKey, k -> new ArrayList<>()).add(soi);
        }
        List<ShippingPackageLineItem> packageLineItems = new ArrayList<>(lineItemToSaleOrderItems.size());
        lineItemToSaleOrderItems.forEach(((lineItemKey, saleOrderItems) -> {
            ShippingPackageLineItem shippingPackageLineItem = new ShippingPackageLineItem();
            SaleOrderItem soi = saleOrderItems.iterator().next();
            shippingPackageLineItem.setLineItemIdentifier(lineItemKey.getIdentifier());
            shippingPackageLineItem.setPicksetName(lineItemKey.getPicksetName());
            shippingPackageLineItem.setPicklistCode(lineItemKey.getPicklistCode());
            shippingPackageLineItem.setItemName(sp.getSaleOrder().isProductManagementSwitchedOff() ? soi.getChannelProductName() : soi.getItemType().getName());
            shippingPackageLineItem.setQuantity(saleOrderItems.size());
            shippingPackageLineItem.setSaleOrderItems(new ArrayList<>(saleOrderItems.size()));
            for (SaleOrderItem saleOrderItem : saleOrderItems) {
                shippingPackageLineItem.getSaleOrderItems().add(new ShippingPackageLineItem.WsSaleOrderItem(saleOrderItem));
                if (SaleOrderItem.StatusCode.CANCELLED.name().equals(saleOrderItem.getStatusCode())) {
                    shippingPackageLineItem.setCancelled(shippingPackageLineItem.getCancelled() + 1);
                }
                if (StringUtils.equalsAny(sp.getStatusCode(), ShippingPackage.StatusCode.READY_TO_SHIP.name(), ShippingPackage.StatusCode.PACKED.name())
                        && SaleOrderItem.StatusCode.CANCELLED.name().equals(saleOrderItem.getStatusCode())) {
                    shippingPackageDTO.setRepackageable(true);
                }
                if (StringUtils.equalsAny(sp.getStatusCode(), ShippingPackage.StatusCode.READY_TO_SHIP.name(), ShippingPackage.StatusCode.PACKED.name())
                        && StringUtils.isBlank(saleOrderItem.getTrackingNumber()) && !sp.isThirdPartyShipping()) {
                    shippingPackageDTO.setProviderEditable(true);
                }
                if (SaleOrderItem.ItemDetailingStatus.PENDING.equals(saleOrderItem.getItemDetailingStatus())) {
                    shippingPackageDTO.setItemDetailingPending(shippingPackageDTO.getItemDetailingPending() + 1);
                }
            }
            if (sp.getSaleOrder().isProductManagementSwitchedOff()) {
                ChannelItemTypeTaxVO channelItemTypeTax = channelCatalogService.getChannelItemTypeTax(sp.getSaleOrder().getChannel().getCode(), lineItemKey.getIdentifier());
                Facility facility = CacheManager.getInstance().getCache(FacilityCache.class).getCurrentFacility();
                PartyAddress warehouseShippingAddress = facility.getPartyAddressByType(PartyAddressType.Code.SHIPPING.name());
                AddressDetail shippingAddress = sp.getShippingAddress();
                if (LocationCache.COUNTRY_CODE_INDIA.equalsIgnoreCase(shippingAddress.getCountryCode()) && warehouseShippingAddress.isIndiaAddress()) {
                    if (warehouseShippingAddress.getStateCode().equals(shippingAddress.getStateCode())) {
                        shippingPackageLineItem.setTaxType(TaxType.Type.VAT.name());
                        shippingPackageLineItem.setTaxPercentage(channelItemTypeTax != null ? channelItemTypeTax.getVatPercentage() : null);
                    } else {
                        shippingPackageLineItem.setTaxType(TaxType.Type.CST.name());
                        shippingPackageLineItem.setTaxPercentage(channelItemTypeTax != null ? channelItemTypeTax.getCstPercentage() : null);
                    }
                    shippingPackageLineItem.setHsnCode(channelItemTypeTax != null ? channelItemTypeTax.getHsnCode() : null);
                }
            }
            packageLineItems.add(shippingPackageLineItem);
        }));
        shippingPackageDTO.setShippingPackageLineItems(packageLineItems);
    }

    private void prepareSaleOrderItem(SaleOrderItemDTO saleOrderItemDTO, SaleOrderItem soi) {
        saleOrderItemDTO.setId(soi.getId());
        saleOrderItemDTO.setCode(soi.getCode());
        saleOrderItemDTO.setChannelProductId(soi.getChannelProductId());
        saleOrderItemDTO.setSellerSkuCode(soi.getSellerSkuCode());
        saleOrderItemDTO.setSkuCode(soi.getSaleOrder().isProductManagementSwitchedOff() ? soi.getSellerSkuCode() : soi.getItemType().getSkuCode());
        saleOrderItemDTO.setProductName(soi.getSaleOrder().isProductManagementSwitchedOff() ? soi.getChannelProductName() : soi.getItemType().getName());
        saleOrderItemDTO.setSellingPrice(soi.getSellingPrice());
        saleOrderItemDTO.setShippingCharges(soi.getShippingCharges());
        saleOrderItemDTO.setShippingMethodCharges(soi.getShippingMethodCharges());
        saleOrderItemDTO.setCashOnDeliveryCharges(soi.getCashOnDeliveryCharges());
        saleOrderItemDTO.setGiftWrapCharges(soi.getGiftWrapCharges());
        saleOrderItemDTO.setGiftMessage(soi.getGiftMessage());
        saleOrderItemDTO.setShippingCharges(soi.getShippingCharges());
        saleOrderItemDTO.setStatusCode(soi.getStatusCode());
        saleOrderItemDTO.setCancellable(soi.isCancellable());
        saleOrderItemDTO.setReversePickable(soi.isReversePickable());
        saleOrderItemDTO.setInventoryReleasable(soi.isInventoryReleasable());
        saleOrderItemDTO.setHoldable(!soi.isOnHold() && soi.isCancellable());
        saleOrderItemDTO.setUnholdable(soi.isOnHold());
        saleOrderItemDTO.setPackageModifiable(soi.isPackageModifiable());
        saleOrderItemDTO.setFacilitySwitchable(soi.isFacilitySwitchable());
        saleOrderItemDTO.setItemCode(soi.getItem() != null ? soi.getItem().getCode() : null);
        if (soi.getShippingPackage() != null) {
            ShippingPackage shippingPackage = soi.getShippingPackage();
            saleOrderItemDTO.setShippingPackageCode(shippingPackage.getCode());
            saleOrderItemDTO.setShippingPackageStatusCode(shippingPackage.getStatusCode());
            if (shippingPackage.getShippingProviderCode() != null) {
                saleOrderItemDTO.setShippingInfoAvailable(true);
                saleOrderItemDTO.setShippingProviderName(shippingPackage.getShippingProviderName());
                saleOrderItemDTO.setTrackingNumber(shippingPackage.getTrackingNumber());
                saleOrderItemDTO.setDispatchTime(shippingPackage.getDispatchTime());
            }
        }
        if (soi.getSaleOrderItemAlternate() != null) {
            saleOrderItemDTO.setSaleOrderItemAlternateId(soi.getSaleOrderItemAlternate().getId());
        }
        saleOrderItemDTO.setTotalPrice(soi.getTotalPrice());
        saleOrderItemDTO.setOnHold(soi.isOnHold());
        saleOrderItemDTO.setBundled(StringUtils.isNotBlank(soi.getCombinationIdentifier()));
        saleOrderItemDTO.setCombinationIdentifier(soi.getCombinationIdentifier());
        saleOrderItemDTO.setCombinationDescription(soi.getCombinationDescription());
        saleOrderItemDTO.setCancellationReason(soi.getCancellationReason());
        saleOrderItemDTO.setReturnReason(soi.getReversePickupReason());
    }

    private void prepareSaleOrderLineItems(GetSaleOrderLineItemsResponse response, SaleOrder saleOrder) {
        Facility currentFacility = CacheManager.getInstance().getCache(FacilityCache.class).getCurrentFacility();
        FacilitySaleOrderItems facilitySaleOrderItems = getFacilitySaleOrderItems(saleOrder, currentFacility);
        Map<String, LineItem> lineItemsMap;
        if (currentFacility != null && Facility.Type.DROPSHIP.name().equals(currentFacility.getType())) {
            if (facilitySaleOrderItems.getCurrentFacilitySaleOrderItems().isEmpty()) {
                response.setInvalidFacility(true);
                response.setValidFacilities(facilitySaleOrderItems.getValidFacilities());
            } else {
                lineItemsMap = getSaleOrderLineItems(facilitySaleOrderItems.getCurrentFacilitySaleOrderItems());
                prepareSaleOrderLineItemsInternal(response, currentFacility, lineItemsMap);
            }
        } else {
            lineItemsMap = getSaleOrderLineItems(facilitySaleOrderItems.getAllSaleOrderItems());
            prepareSaleOrderLineItemsInternal(response, currentFacility, lineItemsMap);
        }
    }

    private void prepareSaleOrderLineItemsInternal(GetSaleOrderLineItemsResponse response, Facility currentFacility, Map<String, LineItem> lineItemsMap) {
        List<SaleOrderLineItemDTO> lineItems = new ArrayList<>();
        for (Entry<String, LineItem> skuLineItems : lineItemsMap.entrySet()) {
            SaleOrderLineItemDTO lineItem = new SaleOrderLineItemDTO();
            List<SaleOrderItem> saleOrderItems = skuLineItems.getValue().getSaleOrderItems();
            SaleOrderItem saleOrderItem = saleOrderItems.get(0);
            lineItem.setLineItemIdentifier(skuLineItems.getKey());
            lineItem.setChannelProductId(saleOrderItem.getChannelProductId());
            lineItem.setChannelProductName(saleOrderItem.getChannelProductName());
            lineItem.setItemSku(skuLineItems.getKey());
            lineItem.setFacilityCode(saleOrderItem.getFacility() != null ? saleOrderItem.getFacility().getCode() : null);
            lineItem.setTotalItems(skuLineItems.getValue().getQuantity());
            lineItem.setLineItemName(skuLineItems.getValue().getProductName());
            for (SaleOrderItem soi : saleOrderItems) {
                lineItem.setSellingPrice(lineItem.getSellingPrice().add(soi.getSellingPrice()));
                lineItem.setTotalPrice(lineItem.getTotalPrice().add(soi.getTotalPrice()));
                lineItem.setShippingCharges(lineItem.getShippingCharges().add(soi.getShippingCharges()));
                lineItem.setCashOnDeliveryCharges(lineItem.getCashOnDeliveryCharges().add(soi.getCashOnDeliveryCharges()));
                lineItem.setGiftWrapCharges(lineItem.getGiftWrapCharges().add(soi.getGiftWrapCharges()));
                lineItem.setDiscount(lineItem.getDiscount().add(soi.getDiscount()));
                lineItem.setPrepaidAmount(lineItem.getPrepaidAmount().add(soi.getPrepaidAmount()));
                lineItem.setShippingMethodCharges(lineItem.getShippingMethodCharges().add(soi.getShippingMethodCharges()));
                lineItem.setTotalCharges(lineItem.getTotalCharges().add(soi.getShippingCharges()).add(soi.getShippingMethodCharges()).add(soi.getCashOnDeliveryCharges()).add(
                        soi.getGiftWrapCharges()));
                response.setCancellable((isCurrentFacilitySaleOrderItem(soi, currentFacility) && soi.isCancellable()) || response.isCancellable());
                response.setHoldable((isCurrentFacilitySaleOrderItem(soi, currentFacility) && !soi.isOnHold() && soi.isCancellable()) || response.isHoldable());
                response.setUnholdable((isCurrentFacilitySaleOrderItem(soi, currentFacility) && soi.isOnHold()) || response.isUnholdable());
                response.setInventoryReleasable((isCurrentFacilitySaleOrderItem(soi, currentFacility) && soi.isInventoryReleasable()) || response.isInventoryReleasable());
                response.setReversePickable(soi.isReversePickable() || response.isReversePickable()); // Allow reverse pickups in any facility
                response.setPackageModifiable((isCurrentFacilitySaleOrderItem(soi, currentFacility) && soi.isPackageModifiable()) || response.isPackageModifiable());
                response.setFacilitySwitchable((isCurrentFacilitySaleOrderItem(soi, currentFacility) && soi.isFacilitySwitchable()) || response.isFacilitySwitchable());
                response.setPackageCreatable((isCurrentFacilitySaleOrderItem(soi, currentFacility) && soi.isPackageCreatable()) || response.isPackageCreatable());
                response.setAlternateAcceptable(
                        (isCurrentFacilitySaleOrderItem(soi, currentFacility) && SaleOrderItem.StatusCode.ALTERNATE_SUGGESTED.name().equals(soi.getStatusCode()))
                                || response.isAlternateAcceptable());
                response.setAlternateSuggestible(SaleOrderItem.StatusCode.UNFULFILLABLE.name().equals(soi.getStatusCode()) || response.isAlternateSuggestible());
            }
            StatusSummary soiStatusSummary = getSaleOrderItemStatusSummary(saleOrderItems, currentFacility);
            lineItem.setInProcess(soiStatusSummary.getInProgress());
            lineItem.setCancellable(soiStatusSummary.getCancellable());
            lineItem.setUnfulfillable(soiStatusSummary.getUnfulfillable());
            lineItem.setOnHold(soiStatusSummary.getOnHold());
            lineItem.setDispatched(soiStatusSummary.getDispatched());
            lineItem.setCancelled(soiStatusSummary.getCancelled());
            lineItem.setReturned(soiStatusSummary.getReturned());
            lineItem.setReplaced(soiStatusSummary.getReplaced());
            lineItem.setReshipped(soiStatusSummary.getReshipped());
            lineItems.add(lineItem);
            response.setLineItems(lineItems);
        }
    }

    private boolean isCurrentFacilitySaleOrderItem(SaleOrderItem saleOrderItem, Facility currentFacility) {
        return saleOrderItem.getFacility() == null || (currentFacility != null && currentFacility.getCode().equals(saleOrderItem.getFacility().getCode()));
    }

    private Map<String, LineItem> getSaleOrderLineItems(List<SaleOrderItem> saleOrderItems) {
        Map<String, LineItem> channelSkuToLineItemsMap = new LinkedHashMap<>();
        for (SaleOrderItem saleOrderItem : saleOrderItems) {
            String lineItemIdentifier = saleOrderItem.getChannelProductId();
            if (saleOrderItem.getFacility() != null) {
                lineItemIdentifier += "-" + saleOrderItem.getFacility().getCode();
            }
            LineItem lineItem = channelSkuToLineItemsMap.get(lineItemIdentifier);
            if (lineItem == null) {
                lineItem = new LineItem();
                if (StringUtils.isNotBlank(saleOrderItem.getCombinationIdentifier())) {
                    lineItem.setType(ItemType.Type.BUNDLE.name());
                    lineItem.setProductName(saleOrderItem.getCombinationDescription());
                } else {
                    lineItem.setType(ItemType.Type.SIMPLE.name());
                    lineItem.setProductName(
                            saleOrderItem.getSaleOrder().isProductManagementSwitchedOff() ? saleOrderItem.getChannelProductName() : saleOrderItem.getItemType().getName());
                }
                channelSkuToLineItemsMap.put(lineItemIdentifier, lineItem);
            }
            lineItem.getSaleOrderItems().add(saleOrderItem);
        }
        for (LineItem lineItem : channelSkuToLineItemsMap.values()) {
            if (ItemType.Type.SIMPLE.name().equals(lineItem.getType())) {
                lineItem.setQuantity(lineItem.getSaleOrderItems().size());
            } else {
                Set<String> bundles = new HashSet<>();
                for (SaleOrderItem soi : lineItem.getSaleOrderItems()) {
                    bundles.add(soi.getCombinationIdentifier());
                }
                lineItem.setQuantity(bundles.size());
            }
        }
        return channelSkuToLineItemsMap;
    }

    private void prepareSaleOrderPriceSummary(SaleOrderPriceSummaryDTO saleOrderPriceSummary, SaleOrder saleOrder, ValidationContext context) {
        Facility currentFacility = CacheManager.getInstance().getCache(FacilityCache.class).getCurrentFacility();
        FacilitySaleOrderItems facilitySaleOrderItems = getFacilitySaleOrderItems(saleOrder, currentFacility);
        List<SaleOrderItem> saleOrderItems = null;
        if (currentFacility != null && Facility.Type.DROPSHIP.name().equals(currentFacility.getType())) {
            if (facilitySaleOrderItems.getCurrentFacilitySaleOrderItems().isEmpty()) {
                context.addError(WsResponseCode.INVALID_FACILITY_CODE);
            } else {
                saleOrderItems = facilitySaleOrderItems.getCurrentFacilitySaleOrderItems();
            }
        } else {
            saleOrderItems = facilitySaleOrderItems.getAllSaleOrderItems();
        }

        if (!context.hasErrors()) {
            Map<String, List<SaleOrderItem>> skuTolineItems = new HashMap<>();
            for (SaleOrderItem soi : saleOrderItems) {
                saleOrderPriceSummary.setSellingPrice(saleOrderPriceSummary.getSellingPrice().add(soi.getSellingPrice()));
                saleOrderPriceSummary.setTotalPrice(saleOrderPriceSummary.getTotalPrice().add(soi.getTotalPrice()));
                saleOrderPriceSummary.setShippingCharges(saleOrderPriceSummary.getShippingCharges().add(soi.getShippingCharges()));
                saleOrderPriceSummary.setCashOnDeliveryCharges(saleOrderPriceSummary.getCashOnDeliveryCharges().add(soi.getCashOnDeliveryCharges()));
                saleOrderPriceSummary.setGiftWrapCharges(saleOrderPriceSummary.getGiftWrapCharges().add(soi.getGiftWrapCharges()));
                saleOrderPriceSummary.setDiscount(saleOrderPriceSummary.getDiscount().add(soi.getDiscount()));
                saleOrderPriceSummary.setPrepaidAmount(saleOrderPriceSummary.getPrepaidAmount().add(soi.getPrepaidAmount()));
                saleOrderPriceSummary.setShippingMethodCharges(saleOrderPriceSummary.getShippingMethodCharges().add(soi.getShippingMethodCharges()));
                saleOrderPriceSummary.setStoreCredit(saleOrderPriceSummary.getStoreCredit().add(soi.getStoreCredit()));
                String key = soi.getItemType().getSkuCode() + "-" + soi.getSellingPrice();
                List<SaleOrderItem> lineItems = skuTolineItems.get(key);
                if (lineItems == null) {
                    lineItems = new ArrayList<SaleOrderItem>();
                    skuTolineItems.put(key, lineItems);
                }
                lineItems.add(soi);
            }
            GetTaxDetailsRequest taxDetailsRequest = new GetTaxDetailsRequest();
            taxDetailsRequest.setChannelCode(saleOrder.getChannel().getCode());
            taxDetailsRequest.setCustomerCode(saleOrder.getCustomer() != null ? saleOrder.getCustomer().getCode() : null);
            taxDetailsRequest.setShippingState(saleOrderItems.iterator().next().getShippingAddress().getStateCode());
            taxDetailsRequest.setShippingCountry(saleOrderItems.iterator().next().getShippingAddress().getCountryCode());
            List<WsChannelItemType> channelItemTypes = new ArrayList<>();
            List<WsItemType> itemTypes = new ArrayList<>();
            Set<InvoiceItem> invoiceItems = new HashSet<>();
            if (saleOrder.isProductManagementSwitchedOff()) {
                for (Entry<String, List<SaleOrderItem>> lineItem : skuTolineItems.entrySet()) {
                    WsChannelItemType wsChannelItemType = new WsChannelItemType();
                    SaleOrderItem soi = null;
                    Iterator<SaleOrderItem> soiIterator = lineItem.getValue().iterator();
                    while (soiIterator.hasNext()) {
                        soi = soiIterator.next();
                        if (soi.getInvoiceItem() == null) {
                            wsChannelItemType.setQuantity(wsChannelItemType.getQuantity() + 1);
                        } else {
                            invoiceItems.add(soi.getInvoiceItem());
                        }
                    }
                    if (wsChannelItemType.getQuantity() > 0) {
                        wsChannelItemType.setChannelProductId(soi.getChannelProductId());
                        wsChannelItemType.setSellingPrice(soi.getSellingPrice());
                        channelCatalogService.getChannelItemTypeTax(saleOrder.getChannel().getCode(), soi.getChannelProductId());
                        channelItemTypes.add(wsChannelItemType);
                    }
                }
                taxDetailsRequest.setChannelItemTypes(channelItemTypes);
            } else {
                for (Entry<String, List<SaleOrderItem>> lineItem : skuTolineItems.entrySet()) {
                    WsItemType wsItemType = new WsItemType();
                    SaleOrderItem soi = null;
                    Iterator<SaleOrderItem> soiIterator = lineItem.getValue().iterator();
                    while (soiIterator.hasNext()) {
                        soi = soiIterator.next();
                        if (soi.getInvoiceItem() == null) {
                            wsItemType.setQuantity(wsItemType.getQuantity() + 1);
                        } else {
                            invoiceItems.add(soi.getInvoiceItem());
                        }
                    }
                    if (wsItemType.getQuantity() > 0) {
                        wsItemType.setItemSku(soi.getItemType().getSkuCode());
                        wsItemType.setSellingPrice(soi.getSellingPrice());
                        itemTypes.add(wsItemType);
                    }
                }
                taxDetailsRequest.setItemTypes(itemTypes);
            }
            if (channelItemTypes.size() > 0 || itemTypes.size() > 0) {
                GetTaxDetailsResponse getTaxDetailsResponse = invoiceService.getTaxDetails(taxDetailsRequest);
                if (getTaxDetailsResponse.isSuccessful()) {
                    for (TaxDTO taxDTO : getTaxDetailsResponse.getTaxDetails()) {
                        saleOrderPriceSummary.setTax(saleOrderPriceSummary.getTax().add(taxDTO.getTax()));
                        saleOrderPriceSummary.setAdditionalTax(saleOrderPriceSummary.getAdditionalTax().add(taxDTO.getAdditionalTax()));
                    }
                }
            }
            setTaxesInPriceSummary(saleOrderPriceSummary, invoiceItems);
            setSellingPriceTaxInPriceSummary(saleOrderPriceSummary, invoiceItems);
            if (ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).isSellingPricesTaxExclusive()) {
                saleOrderPriceSummary.setSellingPrice(saleOrderPriceSummary.getSellingPrice().add(saleOrderPriceSummary.getTax()).add(saleOrderPriceSummary.getAdditionalTax()));
            }
        }
    }

    /**
     * sets tax which was calculated on selling price.
     */
    private void setSellingPriceTaxInPriceSummary(SaleOrderPriceSummaryDTO saleOrderPriceSummary, Set<InvoiceItem> invoiceItems) {
        saleOrderPriceSummary.setSellingPriceTax(
                BigDecimal.valueOf(invoiceItems.stream().mapToDouble(invoiceItem -> invoiceItem.getSellingPriceInvoiceItemTax().getTotalTaxAmount().doubleValue()).sum()));
    }

    /**
     * Fetches different types of taxes from each {@code invoiceItem} in {@code invoiceItems} and adds them to
     * corresponding taxes in {@code saleOrderPriceSummary}
     */
    private void setTaxesInPriceSummary(SaleOrderPriceSummaryDTO saleOrderPriceSummary, Set<InvoiceItem> invoiceItems) {
        for (InvoiceItem invoiceItem : invoiceItems) {
            Set<InvoiceItemTax> invoiceItemTaxes = invoiceItem.getInvoiceItemTaxes();
            saleOrderPriceSummary.setTax(saleOrderPriceSummary.getTax().add(invoiceItem.getTotalTaxAmount()));
            saleOrderPriceSummary.setCentralGst(
                    saleOrderPriceSummary.getCentralGst().add(invoiceItemTaxes.stream().map(InvoiceItemTax::getCentralGst).reduce(BigDecimal.ZERO, BigDecimal::add)));
            saleOrderPriceSummary.setStateGst(
                    saleOrderPriceSummary.getStateGst().add(invoiceItemTaxes.stream().map(InvoiceItemTax::getStateGst).reduce(BigDecimal.ZERO, BigDecimal::add)));
            saleOrderPriceSummary.setUnionTerritoryGst(
                    saleOrderPriceSummary.getUnionTerritoryGst().add(invoiceItemTaxes.stream().map(InvoiceItemTax::getUnionTerritoryGst).reduce(BigDecimal.ZERO, BigDecimal::add)));
            saleOrderPriceSummary.setIntegratedGst(
                    saleOrderPriceSummary.getIntegratedGst().add(invoiceItemTaxes.stream().map(InvoiceItemTax::getIntegratedGst).reduce(BigDecimal.ZERO, BigDecimal::add)));
            saleOrderPriceSummary.setCompensationCess(
                    saleOrderPriceSummary.getCompensationCess().add(invoiceItemTaxes.stream().map(InvoiceItemTax::getCompensationCess).reduce(BigDecimal.ZERO, BigDecimal::add)));
            saleOrderPriceSummary.setAdditionalTax(
                    saleOrderPriceSummary.getAdditionalTax().add(invoiceItemTaxes.stream().map(InvoiceItemTax::getAdditionalTax).reduce(BigDecimal.ZERO, BigDecimal::add)));

        }
    }

    private void prepareSaleOrderSummary(GetSaleOrderSummaryResponse response, SaleOrder saleOrder, int activityCount, int returnsCount, int invoiceCount, int shipmentCount,
            int saleOrderItemCount) {
        Facility currentFacility = CacheManager.getInstance().getCache(FacilityCache.class).getCurrentFacility();
        FacilitySaleOrderItems facilitySaleOrderItems = getFacilitySaleOrderItems(saleOrder, currentFacility);
        if (currentFacility != null && Facility.Type.DROPSHIP.name().equals(currentFacility.getType())) {
            if (facilitySaleOrderItems.getCurrentFacilitySaleOrderItems().isEmpty()) {
                SaleOrderSummaryDTO saleOrderSummary = response.getSaleOrderSummary();
                saleOrderSummary.setActivityCount(activityCount);
                saleOrderSummary.setReturnsCount(returnsCount);
                saleOrderSummary.setInvoiceCount(invoiceCount);
                saleOrderSummary.setShipmentCount(shipmentCount);
                saleOrderSummary.setSaleOrderItemCount(saleOrderItemCount);
                response.setSaleOrderSummary(saleOrderSummary);
                response.setInvalidFacility(true);
                response.setValidFacilities(facilitySaleOrderItems.getValidFacilities());
            } else {
                StatusSummary soiStatusSummary = getSaleOrderItemStatusSummary(facilitySaleOrderItems.getCurrentFacilitySaleOrderItems(), currentFacility);
                prepareSaleOrderSummaryInternal(response, saleOrder, soiStatusSummary, activityCount, returnsCount, invoiceCount, shipmentCount, saleOrderItemCount);
            }
        } else {
            StatusSummary soiStatusSummary = getSaleOrderItemStatusSummary(facilitySaleOrderItems.getAllSaleOrderItems(), currentFacility);
            prepareSaleOrderSummaryInternal(response, saleOrder, soiStatusSummary, activityCount, returnsCount, invoiceCount, shipmentCount, saleOrderItemCount);
        }
    }

    private void prepareSaleOrderSummaryInternal(GetSaleOrderSummaryResponse response, SaleOrder saleOrder, StatusSummary soiStatusSummary, int activityCount, int returnsCount,
            int invoiceCount, int shipmentCount, int saleOrderItemCount) {
        SaleOrderSummaryDTO saleOrderSummary = new SaleOrderSummaryDTO();
        ItemStatusDTO itemStatus = new ItemStatusDTO();
        itemStatus.setInProcess(soiStatusSummary.getInProgress());
        itemStatus.setCancellable(soiStatusSummary.getCancellable());
        itemStatus.setUnfulfillable(soiStatusSummary.getUnfulfillable());
        itemStatus.setDispatched(soiStatusSummary.getDispatched());
        itemStatus.setCancelled(soiStatusSummary.getCancelled());
        itemStatus.setReturned(soiStatusSummary.getReturned());
        itemStatus.setReplaced(soiStatusSummary.getReplaced());
        itemStatus.setReshipped(soiStatusSummary.getReshipped());
        itemStatus.setTotalItems(saleOrder.getSaleOrderItems().size());
        saleOrderSummary.setItemStatus(itemStatus);
        saleOrderSummary.setTotalPrice(soiStatusSummary.getTotalPrice());
        saleOrderSummary.setCode(saleOrder.getCode());
        saleOrderSummary.setDisplayOrderCode(saleOrder.getDisplayOrderCode());
        saleOrderSummary.setPriority(saleOrder.getPriority());
        saleOrderSummary.setStatus(saleOrder.getStatusCode());
        saleOrderSummary.setCustomerCode(saleOrder.getCustomer() != null ? saleOrder.getCustomer().getCode() : null);
        saleOrderSummary.setCustomerName(saleOrder.getCustomerName());
        saleOrderSummary.setChannel(saleOrder.getChannel().getName());
        saleOrderSummary.setThirdPartyShipping(saleOrder.isThirdPartyShipping());
        saleOrderSummary.setPaymentMethod(saleOrder.getPaymentMethod().getCode());
        saleOrderSummary.setPaymentInstrument(saleOrder.getPaymentInstrument() != null ? saleOrder.getPaymentInstrument().name() : null);
        saleOrderSummary.setCurrencyCode(saleOrder.getCurrencyCode());
        saleOrderSummary.setDisplayOrderDateTime(saleOrder.getDisplayOrderDateTime());
        saleOrderSummary.setFulfillmentTat(saleOrder.getFulfillmentTat());
        saleOrderSummary.setCreated(saleOrder.getCreated());
        saleOrderSummary.setUpdated(saleOrder.getUpdated());
        saleOrderSummary.setcFormProvided(saleOrder.isCformProvided());
        saleOrderSummary.setTaxExempted(saleOrder.isTaxExempted());
        saleOrderSummary.setBillingAddress(new WsAddressDetail(saleOrder.getBillingAddress()));
        saleOrderSummary.setShippingAddress(new WsAddressDetail(saleOrder.getSaleOrderItems().iterator().next().getShippingAddress()));
        saleOrderSummary.setCustomFieldValues(CustomFieldUtils.getCustomFieldValuesDTO(saleOrder));
        saleOrderSummary.setNotificationEmail(saleOrder.getNotificationEmail());
        saleOrderSummary.setNotificationMobile(saleOrder.getNotificationMobile());
        saleOrderSummary.setActivityCount(activityCount);
        saleOrderSummary.setReturnsCount(returnsCount);
        saleOrderSummary.setInvoiceCount(invoiceCount);
        saleOrderSummary.setShipmentCount(shipmentCount);
        saleOrderSummary.setSaleOrderItemCount(saleOrderItemCount);
        boolean addressEditable = true;
        for (SaleOrderItem saleOrderItem : saleOrder.getSaleOrderItems()) {
            if (StringUtils.equalsAny(saleOrderItem.getStatusCode(), SaleOrderItem.StatusCode.MANIFESTED.name(), SaleOrderItem.StatusCode.DISPATCHED.name(),
                    SaleOrderItem.StatusCode.DELIVERED.name(), SaleOrderItem.StatusCode.REPLACED.name(), SaleOrderItem.StatusCode.RESHIPPED.name())) {
                addressEditable = false;
                break;
            }
        }
        saleOrderSummary.setAddressEditable(addressEditable);
        Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(saleOrder.getChannel().getSourceCode());
        if (source.isPODRequired() && !ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).isRestrictOTPGeneration()) {
            saleOrderSummary.setPodRequired(true);
        }
        response.setSaleOrderSummary(saleOrderSummary);
    }

    private FacilitySaleOrderItems getFacilitySaleOrderItems(SaleOrder saleOrder, Facility currentFacility) {
        FacilitySaleOrderItems facilitySaleOrderItems = new FacilitySaleOrderItems();
        List<SaleOrderItem> allSaleOrderItems = new ArrayList<>();
        List<SaleOrderItem> currentFacilityItems = new ArrayList<>();
        List<SaleOrderItem> otherFacilityItems = new ArrayList<>();
        Set<String> validFacilities = new HashSet<>();
        for (SaleOrderItem soi : saleOrder.getSaleOrderItems()) {
            allSaleOrderItems.add(soi);
            if (soi.getFacility() != null) {
                validFacilities.add(soi.getFacility().getCode());
            }
            if (currentFacility != null && soi.getFacility() != null && currentFacility.getCode().equals(soi.getFacility().getCode())) {
                currentFacilityItems.add(soi);
            } else {
                otherFacilityItems.add(soi);
            }
        }
        facilitySaleOrderItems.setAllSaleOrderItems(allSaleOrderItems);
        facilitySaleOrderItems.setCurrentFacilitySaleOrderItems(currentFacilityItems);
        facilitySaleOrderItems.setOtherFacilitySaleOrderItems(otherFacilityItems);
        facilitySaleOrderItems.setValidFacilities(validFacilities);
        return facilitySaleOrderItems;
    }

    private StatusSummary getSaleOrderItemStatusSummary(List<SaleOrderItem> saleOrderItems, Facility currentFacility) {
        int inProgress = 0, cancellable = 0, unfulfillable = 0, onHold = 0, dispatched = 0, cancelled = 0, returned = 0, replaced = 0, reshipped = 0;
        BigDecimal totalPrice = BigDecimal.ZERO;
        for (SaleOrderItem soi : saleOrderItems) {
            totalPrice = totalPrice.add(soi.getTotalPrice());
            if (soi.isOnHold()) {
                onHold++;
            }
            SaleOrderItem.StatusCode statusCode = com.uniware.core.entity.SaleOrderItem.StatusCode.valueOf(soi.getStatusCode());
            switch (statusCode) {
                case CANCELLED:
                    cancelled++;
                    break;
                case UNFULFILLABLE:
                    unfulfillable++;
                    break;
                case REPLACED:
                    replaced++;
                    break;
                case RESHIPPED:
                    reshipped++;
                    break;
                case DISPATCHED:
                case DELIVERED:
                    dispatched++;
                    break;
                case CREATED:
                case FULFILLABLE:
                    inProgress++;
                    if (currentFacility != null && soi.getFacility() != null && soi.getFacility().getCode().equals(currentFacility.getCode())) {
                        cancellable++;
                    }
                    break;
                default:
                    break;
            }
        }
        return new StatusSummary(totalPrice, inProgress, cancellable, unfulfillable, onHold, dispatched, cancelled, returned, replaced, reshipped);
    }

    public static class LineItem {
        private List<SaleOrderItem> saleOrderItems = new ArrayList<>();
        private String              type;
        private int                 quantity;
        private String              productName;

        public List<SaleOrderItem> getSaleOrderItems() {
            return saleOrderItems;
        }

        public void setSaleOrderItems(List<SaleOrderItem> saleOrderItems) {
            this.saleOrderItems = saleOrderItems;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public int getQuantity() {
            return quantity;
        }

        public void setQuantity(int quantity) {
            this.quantity = quantity;
        }

        public String getProductName() {
            return productName;
        }

        public void setProductName(String productName) {
            this.productName = productName;
        }

    }

    public static class FacilitySaleOrderItems {
        private List<SaleOrderItem> allSaleOrderItems;
        private List<SaleOrderItem> currentFacilitySaleOrderItems;
        private List<SaleOrderItem> otherFacilitySaleOrderItems;
        private Set<String>         validFacilities;

        public List<SaleOrderItem> getAllSaleOrderItems() {
            return allSaleOrderItems;
        }

        public void setAllSaleOrderItems(List<SaleOrderItem> allSaleOrderItems) {
            this.allSaleOrderItems = allSaleOrderItems;
        }

        public List<SaleOrderItem> getCurrentFacilitySaleOrderItems() {
            return currentFacilitySaleOrderItems;
        }

        public void setCurrentFacilitySaleOrderItems(List<SaleOrderItem> currentFacilitySaleOrderItems) {
            this.currentFacilitySaleOrderItems = currentFacilitySaleOrderItems;
        }

        public List<SaleOrderItem> getOtherFacilitySaleOrderItems() {
            return otherFacilitySaleOrderItems;
        }

        public void setOtherFacilitySaleOrderItems(List<SaleOrderItem> otherFacilitySaleOrderItems) {
            this.otherFacilitySaleOrderItems = otherFacilitySaleOrderItems;
        }

        public Set<String> getValidFacilities() {
            return validFacilities;
        }

        public void setValidFacilities(Set<String> validFacilities) {
            this.validFacilities = validFacilities;
        }

    }

    private class StatusSummary {
        private int        inProgress = 0, cancellable = 0, unfulfillable = 0, dispatched = 0, onHold = 0, cancelled = 0, returned = 0, replaced = 0, reshipped = 0;
        private BigDecimal totalPrice;

        public StatusSummary(BigDecimal totalPrice, int inProgress, int cancellable, int unfulfillable, int onHold, int dispatched, int cancelled, int returned, int replaced,
                int reshipped) {
            this.totalPrice = totalPrice;
            this.inProgress = inProgress;
            this.cancellable = cancellable;
            this.unfulfillable = unfulfillable;
            this.onHold = onHold;
            this.dispatched = dispatched;
            this.cancelled = cancelled;
            this.returned = returned;
            this.replaced = replaced;
            this.reshipped = reshipped;
        }

        public BigDecimal getTotalPrice() {
            return totalPrice;
        }

        public int getInProgress() {
            return inProgress;
        }

        public int getCancellable() {
            return cancellable;
        }

        public int getUnfulfillable() {
            return unfulfillable;
        }

        public int getOnHold() {
            return onHold;
        }

        public int getDispatched() {
            return dispatched;
        }

        public int getCancelled() {
            return cancelled;
        }

        public int getReturned() {
            return returned;
        }

        public int getReplaced() {
            return replaced;
        }

        public int getReshipped() {
            return reshipped;
        }
    }

}
