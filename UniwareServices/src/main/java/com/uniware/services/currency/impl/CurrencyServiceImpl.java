/*
 *  Copyright 2013 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 21-May-2013
 *  @author karunsingla
 */
package com.uniware.services.currency.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.vo.CurrencyConversionRateVO;
import com.uniware.core.api.currency.GetCurrencyConversionRateRequest;
import com.uniware.core.api.currency.GetCurrencyConversionRateResponse;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.entity.Currency;
import com.uniware.dao.currency.ICurrencyDao;
import com.uniware.dao.currency.ICurrencyMao;
import com.uniware.services.configuration.TenantSystemConfiguration;
import com.uniware.services.currency.ICurrencyService;

@Service("currencyService")
public class CurrencyServiceImpl implements ICurrencyService {

    public static final String INR_CURRENCY_CODE = "INR";

    @Autowired
    private ICurrencyDao       currencyDao;

    @Autowired
    private ICurrencyMao       currencyMao;

    @Override
    @Transactional(readOnly = true)
    public List<Currency> getAllCurrencies() {
        return currencyDao.getAllCurrencies();
    }

    @Override
    public GetCurrencyConversionRateResponse getCurrencyConversionRate(GetCurrencyConversionRateRequest request) {
        GetCurrencyConversionRateResponse response = new GetCurrencyConversionRateResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            BigDecimal conversionRate = null;
            String baseCurrency = ConfigurationManager.getInstance().getConfiguration(TenantSystemConfiguration.class).getBaseCurrency();
            if (baseCurrency.equals(request.getCurrencyCode())) {
                conversionRate = BigDecimal.ONE;
            } else if (INR_CURRENCY_CODE.equals(baseCurrency)) {
                CurrencyConversionRateVO conversionRateVO = currencyMao.getCurrencyConversionRate(request.getCurrencyCode(), INR_CURRENCY_CODE);
                if (conversionRateVO != null) {
                    conversionRate = conversionRateVO.getRate();
                }
            } else {
                CurrencyConversionRateVO targetCurrencyToINR = currencyMao.getCurrencyConversionRate(request.getCurrencyCode(), INR_CURRENCY_CODE);
                if (targetCurrencyToINR != null) {
                    CurrencyConversionRateVO baseCurrencyToINR = currencyMao.getCurrencyConversionRate(baseCurrency, INR_CURRENCY_CODE);
                    if (baseCurrencyToINR != null) {
                        conversionRate = targetCurrencyToINR.getRate().divide(targetCurrencyToINR.getRate(), 5, RoundingMode.HALF_UP);
                    }
                }
            }
            if (conversionRate != null) {
                response.setConversionRate(conversionRate);
            } else {
                context.addError(WsResponseCode.CURRENCY_CONVERSION_RATE_NOT_AVAILABLE);
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        } else {
            response.setSuccessful(true);
        }
        return response;
    }
}
