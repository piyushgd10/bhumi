/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 *  @version     1.0, 24-May-2012
 *  @author praveeng
 */
package com.uniware.services.lookup;

import com.uniware.core.api.inventory.GetItemTypeInventoryRequest;
import com.uniware.core.api.inventory.GetItemTypeInventoryResponse;
import com.uniware.core.api.inventory.SearchInventoryRequest;
import com.uniware.core.api.inventory.SearchInventoryResponse;
import com.uniware.core.api.inventory.SearchInventorySnapshotRequest;
import com.uniware.core.api.inventory.SearchInventorySnapshotResponse;
import com.uniware.core.api.inventory.SearchItemTypeInventoryRequest;
import com.uniware.core.api.inventory.SearchItemTypeInventoryResponse;
import com.uniware.core.api.inventory.SearchItemTypeNotFoundQuantityRequest;
import com.uniware.core.api.inventory.SearchItemTypeNotFoundQuantityResponse;
import com.uniware.core.api.item.GetItemTypeDetailRequest;
import com.uniware.core.api.item.GetItemTypeDetailResponse;
import org.springframework.transaction.annotation.Transactional;

public interface ILookupService {

    SearchInventoryResponse searchInventory(SearchInventoryRequest request);

    GetItemTypeDetailResponse getItemTypeDetails(GetItemTypeDetailRequest request);

    SearchInventorySnapshotResponse searchInventorySnapshot(SearchInventorySnapshotRequest request);

    @Transactional SearchItemTypeNotFoundQuantityResponse itemQuantityNotFoundDetails(SearchItemTypeNotFoundQuantityRequest request);

    SearchItemTypeNotFoundQuantityResponse itemQuantityNotFoundDetails(String itemSkuCode);

    SearchItemTypeInventoryResponse searchItemtypeInventory(SearchItemTypeInventoryRequest request);

    GetItemTypeInventoryResponse getItemTypeInventory(GetItemTypeInventoryRequest request);

}
