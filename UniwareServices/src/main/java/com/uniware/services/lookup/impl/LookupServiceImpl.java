/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 24-May-2012
 *  @author praveeng
 */
package com.uniware.services.lookup.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.unifier.core.api.validation.ValidationContext;
import com.uniware.core.api.inventory.GetItemTypeInventoryRequest;
import com.uniware.core.api.inventory.GetItemTypeInventoryResponse;
import com.uniware.core.api.inventory.InventoryDTO;
import com.uniware.core.api.inventory.ItemTypeInventoryDTO;
import com.uniware.core.api.inventory.SearchInventoryRequest;
import com.uniware.core.api.inventory.SearchInventoryResponse;
import com.uniware.core.api.inventory.SearchInventorySnapshotRequest;
import com.uniware.core.api.inventory.SearchInventorySnapshotResponse;
import com.uniware.core.api.inventory.SearchInventorySnapshotResponse.InventorySnapshotDTO;
import com.uniware.core.api.inventory.SearchItemTypeInventoryRequest;
import com.uniware.core.api.inventory.SearchItemTypeInventoryResponse;
import com.uniware.core.api.inventory.SearchItemTypeNotFoundQuantityRequest;
import com.uniware.core.api.inventory.SearchItemTypeNotFoundQuantityResponse;
import com.uniware.core.api.item.GetItemTypeDetailRequest;
import com.uniware.core.api.item.GetItemTypeDetailResponse;
import com.uniware.core.api.item.GetItemTypeDetailResponse.ItemTypeDTO;
import com.uniware.core.api.item.ItemTypeQuantityNotFoundDTO;
import com.uniware.core.api.item.VendorItemTypeDTO;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.entity.ItemType;
import com.uniware.core.entity.ItemTypeInventory;
import com.uniware.core.entity.ItemTypeInventorySnapshot;
import com.uniware.core.entity.Vendor;
import com.uniware.core.entity.VendorItemType;
import com.uniware.dao.lookup.ILookupDao;
import com.uniware.dao.transition.ITransitionDao;
import com.uniware.services.catalog.ICatalogService;
import com.uniware.services.inventory.IInventoryService;
import com.uniware.services.lookup.ILookupService;

@Service
public class LookupServiceImpl implements ILookupService {

    @Autowired
    private ILookupDao        lookupDao;

    @Autowired
    private ICatalogService   catalogService;

    @Autowired
    private IInventoryService inventoryService;

    @Autowired
    private ITransitionDao    transitionDao;

    @Override
    @Transactional(readOnly = true)
    public SearchInventoryResponse searchInventory(SearchInventoryRequest request) {
        SearchInventoryResponse response = new SearchInventoryResponse();
        ValidationContext context = request.validate();
        if (context.hasErrors()) {
            response.setSuccessful(false);
            response.setErrors(context.getErrors());
        } else {
            for (ItemTypeInventory inventory : lookupDao.searchInventory(request)) {
                response.getElements().add(new ItemTypeInventoryDTO(inventory));
            }

            if (request.getSearchOptions() != null && request.getSearchOptions().isGetCount()) {
                Long count = lookupDao.getInventoryCount(request);
                response.setTotalRecords(count);
            }
            response.setSuccessful(true);
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    public GetItemTypeDetailResponse getItemTypeDetails(GetItemTypeDetailRequest request) {
        GetItemTypeDetailResponse response = new GetItemTypeDetailResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            ItemType itemType = catalogService.getItemTypeBySkuCode(request.getItemSKU());
            if (itemType == null) {
                context.addError(WsResponseCode.INVALID_ITEM_TYPE, "INVALID_ITEM_TYPE");
            } else {
                List<VendorItemType> vendorItemTypes = transitionDao.getVendorItemTypeByItemTypeId(itemType.getId());
                ItemTypeDTO itemTypeDTO = new ItemTypeDTO();
                for (VendorItemType vendorItemType : vendorItemTypes) {
                    VendorItemTypeDTO vendorItemTypeDTO = new VendorItemTypeDTO();
                    Vendor vendor = vendorItemType.getVendor();
                    vendorItemTypeDTO.setVendorEnabled(vendor.isEnabled());
                    vendorItemTypeDTO.setVendorCode(vendor.getCode());
                    vendorItemTypeDTO.setVendorName(vendor.getName());
                    vendorItemTypeDTO.setVendorId(vendor.getId());
                    vendorItemTypeDTO.setVendorSkuCode(vendorItemType.getVendorSkuCode());
                    itemTypeDTO.getVendorItemTypes().add(vendorItemTypeDTO);
                }
                if (itemTypeDTO.getVendorItemTypes().size() > 1) {
                    Iterator<VendorItemTypeDTO> itr = itemTypeDTO.getVendorItemTypes().iterator();
                    while (itr.hasNext()) {
                        VendorItemTypeDTO vendorItemTypeDTO = itr.next();
                        if (!vendorItemTypeDTO.isVendorEnabled()) {
                            itr.remove();
                        }
                    }
                }
                itemTypeDTO.setCategoryName(itemType.getCategory().getName());
                itemTypeDTO.setSkuCode(itemType.getSkuCode());
                itemTypeDTO.setName(itemType.getName());
                itemTypeDTO.setItemTypeImageUrl(itemType.getImageUrl());
                itemTypeDTO.setItemTypePageUrl(itemType.getProductPageUrl());
                itemTypeDTO.setEnabled(itemType.isEnabled());
                ItemTypeInventorySnapshot itemTypeSnapshot = inventoryService.getItemTypeInventorySnapshot(itemType.getId());
                if (itemTypeSnapshot != null) {
                    itemTypeDTO.setOpenPurchase(itemTypeSnapshot.getOpenPurchase());
                    itemTypeDTO.setInventory(itemTypeSnapshot.getInventory());
                    itemTypeDTO.setOpenSale(itemTypeSnapshot.getOpenSale());
                    itemTypeDTO.setPutawayPending(itemTypeSnapshot.getPutawayPending());
                    itemTypeDTO.setInPicking(itemTypeSnapshot.getInventoryBlocked());
                }

                List<ItemTypeInventory> itemTypeInventories = inventoryService.getItemTypeInventoryByItemType(itemType);
                for (ItemTypeInventory itemTypeInventory : itemTypeInventories) {
                    itemTypeDTO.setQuantityDamaged(itemTypeDTO.getQuantityDamaged() + itemTypeInventory.getQuantityDamaged());
                    itemTypeDTO.setQuantityNotFound(itemTypeDTO.getQuantityNotFound() + itemTypeInventory.getQuantityNotFound());
                }

                Long count = lookupDao.getPendingGRNCountByItemTypeId(itemType.getId());
                if (count != null) {
                    itemTypeDTO.setPendingGRN(count.intValue());
                    itemTypeDTO.setOpenPurchase(itemTypeDTO.getOpenPurchase() - itemTypeDTO.getPendingGRN());
                }
                response.setItemTypeDTO(itemTypeDTO);
                response.setSuccessful(true);
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    public SearchInventorySnapshotResponse searchInventorySnapshot(SearchInventorySnapshotRequest request) {
        SearchInventorySnapshotResponse response = new SearchInventorySnapshotResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            List<ItemTypeInventorySnapshot> itemTypeSnapShots = new ArrayList<ItemTypeInventorySnapshot>();
            itemTypeSnapShots = lookupDao.searchInventorySnapshot(request);
            for (ItemTypeInventorySnapshot itis : itemTypeSnapShots) {
                InventorySnapshotDTO inventorySnapshotDto = new InventorySnapshotDTO();
                inventorySnapshotDto.setItemName(itis.getItemType().getName());
                inventorySnapshotDto.setItemSku(itis.getItemType().getSkuCode());
                inventorySnapshotDto.setOpenSale(itis.getOpenSale());
                inventorySnapshotDto.setOpenPurchase(itis.getOpenPurchase());
                inventorySnapshotDto.setInventory(itis.getInventory());
                inventorySnapshotDto.setPutawayPending(itis.getPutawayPending());
                inventorySnapshotDto.setVirtualInventory(itis.getVirtualInventory());
                inventorySnapshotDto.setCategory(itis.getItemType().getCategory().getName());
                inventorySnapshotDto.setCreated(itis.getCreated());
                inventorySnapshotDto.setUpdated(itis.getUpdated());
                response.getElements().add(inventorySnapshotDto);
            }
            if (request.getSearchOptions() != null && request.getSearchOptions().isGetCount()) {
                Long count = lookupDao.getInventorySnapshotCount(request);
                response.setTotalRecords(count);
            }
            response.setSuccessful(true);
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    public SearchItemTypeNotFoundQuantityResponse itemQuantityNotFoundDetails(SearchItemTypeNotFoundQuantityRequest request) {
        SearchItemTypeNotFoundQuantityResponse response = new SearchItemTypeNotFoundQuantityResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            return itemQuantityNotFoundDetails(request.getItemSkuCode());
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    @Override
    @Transactional
    public SearchItemTypeNotFoundQuantityResponse itemQuantityNotFoundDetails(String itemSkuCode) {
        SearchItemTypeNotFoundQuantityResponse response = new SearchItemTypeNotFoundQuantityResponse();
        ItemType itemType = catalogService.getItemTypeBySkuCode(itemSkuCode);
        List<ItemTypeInventory> itemTypeInventories = inventoryService.getItemTypeInventoryByItemType(itemType);
        for (ItemTypeInventory itemTypeInventory : itemTypeInventories) {
            ItemTypeQuantityNotFoundDTO itemTypeQtyNotFoundDTO = new ItemTypeQuantityNotFoundDTO();
            itemTypeQtyNotFoundDTO.setItemSku(itemSkuCode);
            itemTypeQtyNotFoundDTO.setNotFoundQuantity(itemTypeInventory.getQuantityNotFound());
            itemTypeQtyNotFoundDTO.setShelfCode(itemTypeInventory.getShelf().getCode());
            itemTypeQtyNotFoundDTO.setType(itemTypeInventory.getType().name());
            response.getItemQuantityNotFoundDTO().add(itemTypeQtyNotFoundDTO);
        }
        return response;
    }

    @Override
    @Transactional
    public SearchItemTypeInventoryResponse searchItemtypeInventory(SearchItemTypeInventoryRequest request) {
        SearchItemTypeInventoryResponse response = new SearchItemTypeInventoryResponse();
        ItemType itemType = catalogService.getItemTypeBySkuCode(request.getSkuCode());
        List<ItemTypeInventory> itemTypeInventories = inventoryService.getItemTypeInventoryByItemType(itemType);
        for (ItemTypeInventory itemTypeInventory : itemTypeInventories) {
            ItemTypeInventoryDTO itemTypeInventoryDTO = new ItemTypeInventoryDTO(itemTypeInventory);
            response.getInventoryDTOs().add(itemTypeInventoryDTO);
        }
        response.setSuccessful(true);
        return response;
    }

    @Override
    @Transactional
    public GetItemTypeInventoryResponse getItemTypeInventory(GetItemTypeInventoryRequest request) {
        GetItemTypeInventoryResponse response = new GetItemTypeInventoryResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            ItemType itemType = catalogService.getItemTypeBySkuCode(request.getSkuCode());
            if (itemType == null) {
                context.addError(WsResponseCode.INVALID_ITEM_TYPE, "No item type exits with sku " + request.getSkuCode());
            } else {
                List<ItemTypeInventory> itemTypeInventories = inventoryService.getItemTypeInventoryByItemType(itemType);
                for (ItemTypeInventory itemTypeInventory : itemTypeInventories) {
                    InventoryDTO inventoryDTO = new InventoryDTO(itemTypeInventory);
                    response.getInventoryDTOs().add(inventoryDTO);
                }
            }
            response.setSuccessful(true);
        }
        response.setErrors(context.getErrors());
        return response;
    }

}
