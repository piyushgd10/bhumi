/*
 * Copyright 2017 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 02/11/17
 * @author piyush
 */
package com.uniware.services.imports;

import com.unifier.core.api.validation.ValidationContext;
import com.uniware.core.api.material.AddNonTraceableGatePassItemRequest;
import com.uniware.core.entity.ItemTypeInventory;
import org.springframework.beans.factory.annotation.Autowired;

import com.unifier.core.api.validation.WsError;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.entity.ImportJob;
import com.unifier.core.entity.ImportJobType;
import com.unifier.core.fileparser.Row;
import com.unifier.core.utils.StringUtils;
import com.unifier.services.imports.ImportJobHandler;
import com.unifier.services.imports.ImportJobHandlerRequest;
import com.unifier.services.imports.ImportJobHandlerResponse;
import com.uniware.core.api.material.AddItemToGatePassRequest;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.services.configuration.SystemConfiguration;
import com.uniware.services.material.IMaterialService;

import java.math.BigDecimal;

public class AddGatepassItemsImportJobHandler implements ImportJobHandler {

    private final String     GATEPASS_CODE    = "Gatepass Code";
    private final String     ITEM_OR_SKU_CODE = "Item Or Sku Code";
    private final String     QUANTITY         = "Qty";
    private final String     INVENTORY_TYPE   = "Inventory Type";
    private final String     SHELF_CODE       = "Shelf Code";
    private final String     UNIT_PRICE       = "Unit Price";

    @Autowired
    private IMaterialService materialService;

    @Override
    public ImportJobHandlerResponse handleRow(ImportJobHandlerRequest request, ImportJobType.ImportOptions importOption) throws Exception {
        ImportJobHandlerResponse response = new ImportJobHandlerResponse();
        SystemConfiguration.TraceabilityLevel traceabilityLevel = ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).getTraceabilityLevel();
        if (ImportJobType.ImportOptions.CREATE_NEW == importOption) {
            if (SystemConfiguration.TraceabilityLevel.ITEM.equals(traceabilityLevel)) {
                addTraceableGatepassItemToGatepass(request, response);
            } else {
                addNonTraceableGatepassItemToGatepass(request, response);
            }
        } else {
            response.addError(new WsError(WsResponseCode.FEATURE_NOT_SUPPORTED.code(), WsResponseCode.FEATURE_NOT_SUPPORTED.message()));
        }
        return response;
    }

    private void addTraceableGatepassItemToGatepass(ImportJobHandlerRequest request, ImportJobHandlerResponse response) {
        AddItemToGatePassRequest addItemToGatePassRequest = prepareAddItemToGatepassRequest(request);
        response.addResponse(materialService.addItemToGatePass(addItemToGatePassRequest));
    }

    private AddItemToGatePassRequest prepareAddItemToGatepassRequest(ImportJobHandlerRequest request) {
        Row row = request.getRow();
        AddItemToGatePassRequest addItemToGatePassRequest = new AddItemToGatePassRequest();
        addItemToGatePassRequest.setGatePassCode(row.getColumnValue(GATEPASS_CODE));
        addItemToGatePassRequest.setItemCode(row.getColumnValue(ITEM_OR_SKU_CODE));
        return addItemToGatePassRequest;
    }

    private void addNonTraceableGatepassItemToGatepass(ImportJobHandlerRequest request, ImportJobHandlerResponse response) {
        Row row = request.getRow();
        ValidationContext context = request.validate();
        Integer quantity = parseQuantity(row, context);
        ItemTypeInventory.Type inventoryType = parseInventoryType(row, context);
        BigDecimal unitPrice = parseUnitPrice(row, context);
        if (!context.hasErrors()){
            AddNonTraceableGatePassItemRequest addNonTraceableGatePassItemRequest = new AddNonTraceableGatePassItemRequest();
            addNonTraceableGatePassItemRequest.setGatePassCode(row.getColumnValue(GATEPASS_CODE));
            addNonTraceableGatePassItemRequest.setItemSKU(row.getColumnValue(ITEM_OR_SKU_CODE));
            addNonTraceableGatePassItemRequest.setQuantity(quantity);
            addNonTraceableGatePassItemRequest.setInventoryType(inventoryType);
            addNonTraceableGatePassItemRequest.setShelfCode(row.getColumnValue(SHELF_CODE));
            addNonTraceableGatePassItemRequest.setUnitPrice(unitPrice);
            response.addResponse(materialService.addNonTraceableGatePassItem(addNonTraceableGatePassItemRequest));
        } else {
            response.setErrors(context.getErrors());
        }
    }

    private BigDecimal parseUnitPrice(Row row, ValidationContext context) {
        BigDecimal unitPrice = null;
        if(StringUtils.isNotBlank(row.getColumnValue(UNIT_PRICE))){
            try{
                unitPrice = new BigDecimal(row.getColumnValue(UNIT_PRICE));
            } catch (Exception e){
                context.addError(WsResponseCode.INVALID_PRICES, "Invalid value for unit price : " + row.getColumnValue(UNIT_PRICE));
            }
        }
        return unitPrice;
    }

    private ItemTypeInventory.Type parseInventoryType(Row row, ValidationContext context) {
        ItemTypeInventory.Type inventoryType = null;
        if(StringUtils.isBlank(row.getColumnValue(INVENTORY_TYPE))){
            context.addError(WsResponseCode.INVALID_REQUEST,"Inventory Type cannot be empty");
        } else {
            try{
                inventoryType = ItemTypeInventory.Type.valueOf(row.getColumnValue(INVENTORY_TYPE));
            } catch (Exception e){
                context.addError(WsResponseCode.INVALID_REQUEST,"Invalid value for Inventory Type : " + row.getColumnValue(QUANTITY));
            }
        }
        return inventoryType;
    }

    private Integer parseQuantity(Row row, ValidationContext context) {
        Integer quantity = null;
        if(StringUtils.isBlank(row.getColumnValue(QUANTITY))){
            context.addError(WsResponseCode.INVALID_REQUEST,"Quantity cannot be empty");
        } else {
            try{
                quantity = Integer.parseInt(row.getColumnValue(QUANTITY));
            } catch (NumberFormatException e){
                context.addError(WsResponseCode.INVALID_REQUEST,"Invalid value for quantity : " + row.getColumnValue(QUANTITY));
            }
        }
        return quantity;
    }

    @Override
    public void preProcessor(ImportJob importJob) {

    }

    @Override
    public void postProcessor(ImportJob importJob) {

    }
}
