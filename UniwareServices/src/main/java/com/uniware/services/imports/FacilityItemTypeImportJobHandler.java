/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 23, 2012
 *  @author praveeng
 */
package com.uniware.services.imports;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;

import com.unifier.core.api.validation.WsError;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.entity.ImportJob;
import com.unifier.core.entity.ImportJobType;
import com.unifier.core.entity.ImportJobType.ImportOptions;
import com.unifier.core.fileparser.Row;
import com.unifier.core.utils.StringUtils;
import com.unifier.core.utils.ValidatorUtils;
import com.unifier.services.imports.ImportJobHandler;
import com.unifier.services.imports.ImportJobHandlerRequest;
import com.unifier.services.imports.ImportJobHandlerResponse;
import com.unifier.services.utils.CustomFieldUtils;
import com.uniware.core.api.catalog.CreateOrEditFacilityItemTypeRequest;
import com.uniware.core.api.catalog.EditFacilityItemTypeRequest;
import com.uniware.core.api.catalog.WsFacilityItemType;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.cache.FacilityCache;
import com.uniware.core.entity.Facility;
import com.uniware.core.entity.FacilityItemType;
import com.uniware.core.entity.ItemType;
import com.uniware.core.entity.Vendor;
import com.uniware.services.catalog.ICatalogService;

public class FacilityItemTypeImportJobHandler implements ImportJobHandler {
    private static final String FACILITY_CODE      = "facility code";
    private static final String ITEM_TYPE_SKU_CODE = "product code";
    private static final String INVENTORY          = "inventory";
    private static final String PRIORITY           = "priority";
    private static final String COMMISSION         = "commission";
    private static final String SHIPPING_CHARGES   = "shipping charges";
    private static final String ENABLED            = "enabled";

    @Autowired
    private ICatalogService     catalogService;

    @Override
    public ImportJobHandlerResponse handleRow(ImportJobHandlerRequest request, ImportOptions importOption) throws Exception {
        ImportJobHandlerResponse response = new ImportJobHandlerResponse();
        Row row = request.getRow();

        if (ImportJobType.ImportOptions.CREATE_NEW_AND_UPDATE_EXISTING == importOption) {
            WsFacilityItemType wsFacilityItemType = prepareWsFacilityItemType(response, row);
            if (!response.hasErrors()) {
                CreateOrEditFacilityItemTypeRequest createOrEditFacilityItemTypeRequest = new CreateOrEditFacilityItemTypeRequest();
                createOrEditFacilityItemTypeRequest.addFacilityItemType(wsFacilityItemType);
                response.addResponse(catalogService.createOrEditFacilityItemType(createOrEditFacilityItemTypeRequest));
            }
        } else if (ImportJobType.ImportOptions.UPDATE_EXISTING == importOption) {
            String facilityCode = row.getColumnValue(FACILITY_CODE);
            Facility facility = CacheManager.getInstance().getCache(FacilityCache.class).getFacilityByCode(facilityCode);
            if (facility == null || !Facility.Type.DROPSHIP.name().equals(facility.getType())) {
                response.addError(new WsError("Invalid facility code: " + facilityCode));
            }
            FacilityItemType facilityItemType = null;
            if (!response.hasErrors()) {
                String skuCode = row.getColumnValue(ITEM_TYPE_SKU_CODE);
                ItemType itemType = catalogService.getItemTypeBySkuCode(skuCode);
                if (itemType == null) {
                    response.addError(new WsError("Invalid item type skucode " + skuCode));
                }
                if (!response.hasErrors()) {
                    facilityItemType = catalogService.getFacilityItemType(itemType.getSkuCode(), facility.getId());
                    if (facilityItemType == null) {
                        response.addError(new WsError("Invalid facility item type"));
                    } 
                }
            }
            if (!response.hasErrors()) {
                WsFacilityItemType wsFacilityItemType = preparePartialWsFacilityItemType(response, row, facilityItemType);
                if (!response.hasErrors()) {
                    EditFacilityItemTypeRequest editFacilityItemTypeRequest = new EditFacilityItemTypeRequest();
                    editFacilityItemTypeRequest.setWsFacilityItemType(wsFacilityItemType);
                    response.addResponse(catalogService.editFacilityItemType(editFacilityItemTypeRequest));
                } 
            }
        }

        return response;
    }

    private WsFacilityItemType prepareWsFacilityItemType(ImportJobHandlerResponse response, Row row) {
        WsFacilityItemType wsFacilityItemType = new WsFacilityItemType();

        String priority = row.getColumnValue(PRIORITY);

        if (StringUtils.isNotBlank(priority)) {
            try {
                wsFacilityItemType.setPriority(Integer.parseInt(priority));
            } catch (NumberFormatException e) {
                response.addError(new WsError("Invalid value for 'priority'"));
            }
        }

        String inventory = row.getColumnValue(INVENTORY);
        if (StringUtils.isNotBlank(inventory)) {
            try {
                wsFacilityItemType.setInventory(Integer.parseInt(inventory));
            } catch (NumberFormatException e) {
                response.addError(new WsError("Invalid value for 'inventory'"));
            }
        }

        String enabled = row.getColumnValue(ENABLED);
        if (StringUtils.isNotBlank(enabled)) {
            wsFacilityItemType.setEnabled(StringUtils.parseBoolean(enabled));
        }

        if (StringUtils.isNotBlank(row.getColumnValue(COMMISSION))) {
            try {
                wsFacilityItemType.setCommission(new BigDecimal(row.getColumnValue(COMMISSION)));
            } catch (NumberFormatException e) {
                response.addError(new WsError("Invalid value for " + COMMISSION));
            }
        }

        if (StringUtils.isNotBlank(row.getColumnValue(SHIPPING_CHARGES))) {
            try {
                wsFacilityItemType.setShippingCharges(new BigDecimal(row.getColumnValue(SHIPPING_CHARGES)));
            } catch (NumberFormatException e) {
                response.addError(new WsError("Invalid value for " + SHIPPING_CHARGES));
            }
        }

        wsFacilityItemType.setFacilityCode(row.getColumnValue(FACILITY_CODE));
        wsFacilityItemType.setItemTypeSkuCode(row.getColumnValue(ITEM_TYPE_SKU_CODE));
        wsFacilityItemType.setCustomFieldValues(CustomFieldUtils.getCustomFieldValues(response, FacilityItemType.class.getName(), row));
        return wsFacilityItemType;
    }
    
    private WsFacilityItemType preparePartialWsFacilityItemType(ImportJobHandlerResponse response, Row row, FacilityItemType facilityItemType) {
        WsFacilityItemType wsFacilityItemType = new WsFacilityItemType();

        String priority = row.getColumnValue(PRIORITY);

        if (StringUtils.isNotBlank(priority)) {
            try {
                wsFacilityItemType.setPriority(Integer.parseInt(priority));
            } catch (NumberFormatException e) {
                response.addError(new WsError("Invalid value for 'priority'"));
            }
        } else {
            wsFacilityItemType.setPriority(facilityItemType.getPriority());
        }

        String inventory = row.getColumnValue(INVENTORY);
        if (StringUtils.isNotBlank(inventory)) {
            try {
                wsFacilityItemType.setInventory(Integer.parseInt(inventory));
            } catch (NumberFormatException e) {
                response.addError(new WsError("Invalid value for 'inventory'"));
            }
        } else {
            wsFacilityItemType.setInventory(facilityItemType.getInventory());
        }

        String enabled = row.getColumnValue(ENABLED);
        if (StringUtils.isNotBlank(enabled)) {
            wsFacilityItemType.setEnabled(StringUtils.parseBoolean(enabled));
        } else {
            wsFacilityItemType.setEnabled(facilityItemType.isEnabled());
        }

        if (StringUtils.isNotBlank(row.getColumnValue(COMMISSION))) {
            try {
                wsFacilityItemType.setCommission(new BigDecimal(row.getColumnValue(COMMISSION)));
            } catch (NumberFormatException e) {
                response.addError(new WsError("Invalid value for " + COMMISSION));
            }
        } else {
            wsFacilityItemType.setCommission(facilityItemType.getCommission());
        }

        if (StringUtils.isNotBlank(row.getColumnValue(SHIPPING_CHARGES))) {
            try {
                wsFacilityItemType.setShippingCharges(new BigDecimal(row.getColumnValue(SHIPPING_CHARGES)));
            } catch (NumberFormatException e) {
                response.addError(new WsError("Invalid value for " + SHIPPING_CHARGES));
            }
        } else {
            wsFacilityItemType.setShippingCharges(facilityItemType.getShippingCharges());
        }
        wsFacilityItemType.setFacilityCode(row.getColumnValue(FACILITY_CODE));
        wsFacilityItemType.setItemTypeSkuCode(row.getColumnValue(ITEM_TYPE_SKU_CODE));
        wsFacilityItemType.setCustomFieldValues(CustomFieldUtils.getCustomFieldValues(response, FacilityItemType.class.getName(), row));
        return wsFacilityItemType;
    }
    

    @Override
    public void preProcessor(ImportJob importJob) {
    }

    @Override
    public void postProcessor(ImportJob importJob) {
    }

}
