/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 31-Mar-2012
 *  @author ankit
 */
package com.uniware.services.imports;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.unifier.core.api.validation.WsError;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.entity.ImportJob;
import com.unifier.core.entity.ImportJobType.ImportOptions;
import com.unifier.core.fileparser.Row;
import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.NumberUtils;
import com.unifier.core.utils.StringUtils;
import com.unifier.services.imports.ImportJobHandler;
import com.unifier.services.imports.ImportJobHandlerRequest;
import com.unifier.services.imports.ImportJobHandlerResponse;
import com.unifier.services.utils.CustomFieldUtils;
import com.uniware.core.api.model.WsAddressDetail;
import com.uniware.core.api.model.WsAddressRef;
import com.uniware.core.api.model.WsSaleOrder;
import com.uniware.core.api.model.WsSaleOrderItem;
import com.uniware.core.api.model.WsSaleOrderItemCombination;
import com.uniware.core.api.model.WsShippingProvider;
import com.uniware.core.api.saleorder.CreateSaleOrderRequest;
import com.uniware.core.entity.Channel;
import com.uniware.core.entity.SaleOrder;
import com.uniware.core.entity.SaleOrderItem;
import com.uniware.core.entity.ShippingMethod;
import com.uniware.services.cache.ChannelCache;
import com.uniware.services.saleorder.ISaleOrderService;

public class SaleOrderImportJobHandler implements ImportJobHandler {

    private static final Logger  LOG                          = LoggerFactory.getLogger(SaleOrderImportJobHandler.class);

    @Autowired
    private ISaleOrderService    saleOrderService;

    //Grouped on SO_Code

    private static final String  SALE_ORDER_CODE              = "Sales Order Code";
    private static final String  DISPLAY_SALE_ORDER_CODE      = "Display Sales Order Code";
    private static final String  DISPLAY_SALE_ORDER_DATETIME  = "Order Date as dd/MM/yyyy hh:mm:ss";
    private static final String  NOTIFICATION_MAIL            = "Notification Email";
    private static final String  NOTIFICATION_MOBILE          = "Notification Mobile";
    private static final String  CUSTOMER_CODE                = "Customer Code";
    private static final String  CASH_ON_DELIVERY             = "COD";
    private static final String  CHANNEL                      = "Channel";
    private static final String  CURRENCY_CODE                = "Currency Code";
    private static final String  FULFILLMENT_TAT              = "Fulfillment Tat";

    //address details for each SOI List<WsAddressDetail> in WsSaleOrder
    private static final String  SHIPPING_ADDRESS_ID          = "Shipping Address Id";
    private static final String  SHIPPING_ADDRESS_NAME        = "Shipping Address Name";
    private static final String  SHIPPING_ADDRESS_LINE1       = "Shipping Address Line1";
    private static final String  SHIPPING_ADDRESS_LINE2       = "Shipping Address Line2";
    private static final String  SHIPPING_ADDRESS_CITY        = "Shipping Address City";
    private static final String  SHIPPING_ADDRESS_STATE       = "Shipping Address State";
    private static final String  SHIPPING_ADDRESS_COUNTRY     = "Shipping Address Country";
    private static final String  SHIPPING_ADDRESS_PINCODE     = "Shipping Address Pincode";
    private static final String  SHIPPING_ADDRESS_PHONE       = "Shipping Address Phone";

    //address details for each SOI List<WsAddressDetail> in WsSaleOrder
    private static final String  BILLING_ADDRESS_ID           = "Billing Address Id";
    private static final String  BILLING_ADDRESS_NAME         = "Billing Address Name";
    private static final String  BILLING_ADDRESS_LINE1        = "Billing Address Line1";
    private static final String  BILLING_ADDRESS_LINE2        = "Billing Address Line2";
    private static final String  BILLING_ADDRESS_CITY         = "Billing Address City";
    private static final String  BILLING_ADDRESS_STATE        = "Billing Address State";
    private static final String  BILLING_ADDRESS_COUNTRY      = "Billing Address Country";
    private static final String  BILLING_ADDRESS_PINCODE      = "Billing Address Pincode";
    private static final String  BILLING_ADDRESS_PHONE        = "Billing Address Phone";

    //WsSOI
    private static final String  ITEM_TYPE_NAME               = "Item Name";
    private static final String  ITEM_TYPE_SKU_CODE           = "Item SKU Code";
    private static final String  CHANNEL_PRODUCT_ID           = "Channel Product Id";
    private static final String  SHIPPING_METHOD_CODE         = "Shipping Method";
    private static final String  SALE_ORDER_ITEM_CODE         = "Sale Order Item Code";
    private static final String  GIFT_MESSAGE                 = "Gift Message";
    private static final String  GIFT_WARP                    = "Gift Wrap";
    private static final String  SELLING_PRICE                = "Selling Price";
    private static final String  SHIPPING_CHARGES             = "Shipping Charges";
    private static final String  SHIPPING_METHOD_CHARGES      = "Shipping Method Charges";
    private static final String  DISCOUNT                     = "Discount";
    private static final String  VOUCHER_CODE                 = "Voucher Code";
    private static final String  STORE_CREDIT                 = "Store Credit";
    private static final String  VOUCHER_VALUE                = "Voucher Value";
    private static final String  CASH_ON_DELIVERY_CHARGES     = "COD Service Charges";
    private static final String  GIFT_WRAP_CHARGES            = "Gift Wrap Charges";
    private static final String  PACKET_NUMBER                = "Packet Number";
    private static final String  ON_HOLD                      = "On Hold";
    private static final String  SHIPPING_PROVIDER            = "Shipping Provider";
    private static final String  TRACKING_NUMBER              = "Tracking Number";
    private static final String  QUANTITY                     = "Quantity";
    private static final String  FACILITY_CODE                = "Facility Code";
    private static final String  ADDITIONAL_INFO              = "Additional Info";
    private static final String  PREPAID_AMOUNT               = "Prepaid Amount";
    private static final String  ORDER_TOTAL_SHIPPING_CHARGES = "Order Total Shipping Charges";
    private static final String  COMBINATION_IDENTIFIER       = "Combination Identifier";
    private static final String  REQUIRES_CUSTOMIZATION       = "Requires Customization";

    //WsSaleOrderItemCombination
    private static final String  COMBINATION_DESCRIPTION      = "Combination Description";
    private static final String  QUANTITY_IN_COMBO            = "Quantity In Combo";

    private static final Pattern SALE_ORDER_CODE_PATTERN      = Pattern.compile("^[a-zA-Z0-9_/\\-]+$");
    private static final String  FULFILLMENT_TAT_DATE_FORMAT  = "dd-MM-yyyy HH:mm:ss:SSS";

    @Override
    public ImportJobHandlerResponse handleRow(ImportJobHandlerRequest request, ImportOptions importOption) throws Exception {
        ImportJobHandlerResponse response = new ImportJobHandlerResponse();
        List<Row> rows = request.getRows();

        CreateSaleOrderRequest createSaleOrderRequest = new CreateSaleOrderRequest();
        WsSaleOrder saleOrder = new WsSaleOrder();
        saleOrder.setSaleOrderItems(new ArrayList<WsSaleOrderItem>());
        saleOrder.setAddresses(new ArrayList<WsAddressDetail>());
        HashMap<String, WsAddressDetail> addresses = new HashMap<String, WsAddressDetail>();

        saleOrder.setSaleOrderItemCombinations(new ArrayList<WsSaleOrderItemCombination>());
        Map<String, WsSaleOrderItemCombination> combinations = new HashMap<String, WsSaleOrderItemCombination>();

        if (!SALE_ORDER_CODE_PATTERN.matcher(rows.get(0).getColumnValue(SALE_ORDER_CODE)).matches()) {
            response.addError(new WsError("Only alphabets, digits and underscores are allowed in " + SALE_ORDER_CODE));
        } else {
            saleOrder.setCode(rows.get(0).getColumnValue(SALE_ORDER_CODE));
        }

        saleOrder.setDisplayOrderCode(rows.get(0).getColumnValue(DISPLAY_SALE_ORDER_CODE));

        if (StringUtils.isNotBlank(rows.get(0).getColumnValue(DISPLAY_SALE_ORDER_DATETIME))) {
            Date orderDate = DateUtils.stringToDate(rows.get(0).getColumnValue(DISPLAY_SALE_ORDER_DATETIME), "dd/MM/yyyy hh:mm:ss");
            if (orderDate != null) {
                saleOrder.setDisplayOrderDate(orderDate);
            } else {
                response.addError(new WsError("Invalid value for " + DISPLAY_SALE_ORDER_DATETIME));
            }
        }

        if (StringUtils.isNotBlank(rows.get(0).getColumnValue(ADDITIONAL_INFO))) {
            saleOrder.setAdditionalInfo(rows.get(0).getColumnValue(ADDITIONAL_INFO));
        }

        if (StringUtils.isNotBlank(rows.get(0).getColumnValue(NOTIFICATION_MAIL))) {
            saleOrder.setNotificationEmail(rows.get(0).getColumnValue(NOTIFICATION_MAIL));
        }
        if (StringUtils.isNotBlank(rows.get(0).getColumnValue(NOTIFICATION_MOBILE))) {
            saleOrder.setNotificationMobile(rows.get(0).getColumnValue(NOTIFICATION_MOBILE));
        }

        if (StringUtils.isNotBlank(rows.get(0).getColumnValue(CUSTOMER_CODE))) {
            saleOrder.setCustomerCode(rows.get(0).getColumnValue(CUSTOMER_CODE));
        }

        if (StringUtils.isNotBlank(rows.get(0).getColumnValue(FULFILLMENT_TAT))) {
            try {
                saleOrder.setFulfillmentTat(DateUtils.stringToDate(rows.get(0).getColumnValue(FULFILLMENT_TAT), FULFILLMENT_TAT_DATE_FORMAT));
            }
            catch(Exception e) {
                response.addError(new WsError("Invalid Date Format. Please check description."));
            }
        }

        if (StringUtils.isNotBlank(rows.get(0).getColumnValue(CHANNEL))) {
            Channel c = CacheManager.getInstance().getCache(ChannelCache.class).getChannelByName(rows.get(0).getColumnValue(CHANNEL));
            if (c == null) {
                response.addError(new WsError("Invalid channel name"));
            }
            saleOrder.setChannel(c.getCode());
        }

        if (StringUtils.isNotBlank(rows.get(0).getColumnValue(CURRENCY_CODE))) {
            saleOrder.setCurrencyCode(rows.get(0).getColumnValue(CURRENCY_CODE));
        }

        if ("1".equals(rows.get(0).getColumnValue(CASH_ON_DELIVERY)) || "true".equalsIgnoreCase(rows.get(0).getColumnValue(CASH_ON_DELIVERY))) {
            saleOrder.setCashOnDelivery(true);
        } else if ("0".equals(rows.get(0).getColumnValue(CASH_ON_DELIVERY)) || "false".equalsIgnoreCase(rows.get(0).getColumnValue(CASH_ON_DELIVERY))) {
            saleOrder.setCashOnDelivery(false);
        } else {
            response.addError(new WsError("Invalid value for 'COD'"));
        }

        BigDecimal orderShippingCharges = null;
        if (StringUtils.isNotBlank(rows.get(0).getColumnValue(ORDER_TOTAL_SHIPPING_CHARGES))) {
            try {
                orderShippingCharges = NumberUtils.divide(new BigDecimal(rows.get(0).getColumnValue(ORDER_TOTAL_SHIPPING_CHARGES)), rows.size());
            } catch (NumberFormatException e) {
                response.addError(new WsError("Invalid value for " + ORDER_TOTAL_SHIPPING_CHARGES));
            }
        }

        String shippingAddressRef = StringUtils.EMPTY_STRING;
        String billingAddressRef = StringUtils.EMPTY_STRING;
        Set<Integer> packetShippingProviders = new HashSet<Integer>();
        int saleOrderItemNo = 0;
        for (Row row : rows) {
            if (!addresses.containsKey(row.getColumnValue(SHIPPING_ADDRESS_ID)) && StringUtils.isNotBlank(row.getColumnValue(SHIPPING_ADDRESS_ID))) {
                WsAddressDetail wsAddressDetail = prepareShippingAddress(row);
                addresses.put(row.getColumnValue(SHIPPING_ADDRESS_ID), wsAddressDetail);
                shippingAddressRef = row.getColumnValue(SHIPPING_ADDRESS_ID);
            }
            if (StringUtils.isNotBlank(row.getColumnValue(BILLING_ADDRESS_ID)) && !addresses.containsKey(row.getColumnValue(BILLING_ADDRESS_ID))) {
                WsAddressDetail wsAddressDetail = prepareBillingAddress(row);
                addresses.put(row.getColumnValue(BILLING_ADDRESS_ID), wsAddressDetail);
                billingAddressRef = row.getColumnValue(BILLING_ADDRESS_ID);
            }

            if (StringUtils.isNotBlank(row.getColumnValue(BILLING_ADDRESS_ID)) && addresses.containsKey(row.getColumnValue(BILLING_ADDRESS_ID))
                    && StringUtils.isNotBlank(row.getColumnValue(SHIPPING_ADDRESS_ID))) {
                billingAddressRef = row.getColumnValue(BILLING_ADDRESS_ID);
            }

            if (addresses.isEmpty() && StringUtils.isBlank(saleOrder.getCustomerCode())) {
                response.addError(new WsError("Atleast one of customer code or addresses (billing & shipping) should be provided"));
            }

            Integer quantity = null;
            if (StringUtils.isNotBlank(row.getColumnValue(QUANTITY))) {
                try {
                    quantity = Integer.parseInt(row.getColumnValue(QUANTITY));
                } catch (NumberFormatException e) {
                    response.addError(new WsError("Invalid value for " + QUANTITY));
                }
            }

            Integer quantityInCombo = null;
            if (StringUtils.isNotBlank(row.getColumnValue(QUANTITY_IN_COMBO))) {
                try {
                    quantityInCombo = Integer.parseInt(row.getColumnValue(QUANTITY_IN_COMBO));
                } catch (NumberFormatException e) {
                    response.addError(new WsError("Invalid value for " + QUANTITY_IN_COMBO));
                }
            }

            if (quantityInCombo != null && StringUtils.isBlank(row.getColumnValue(COMBINATION_IDENTIFIER))) {
                response.addError(new WsError(COMBINATION_IDENTIFIER + " may not be empty"));
            }

            if (StringUtils.isNotBlank(row.getColumnValue(COMBINATION_IDENTIFIER)) && StringUtils.isBlank(row.getColumnValue(COMBINATION_DESCRIPTION))) {
                response.addError(new WsError(COMBINATION_DESCRIPTION + " may not be empty"));
            }

            if (!response.hasErrors()) {
                String saleOrderItemCode = row.getColumnValue(SALE_ORDER_ITEM_CODE);
                if (StringUtils.isBlank(saleOrderItemCode)) {
                    saleOrderItemCode = saleOrder.getCode() + "-" + (++saleOrderItemNo);
                }
                String combinationIdentifier = row.getColumnValue(COMBINATION_IDENTIFIER);
                if (quantity == null || quantity.equals(1)) {
                    if (quantityInCombo == null || quantityInCombo.equals(1)) {
                        prepareWsSaleOrderItem(response, saleOrder, packetShippingProviders, row, saleOrderItemCode, orderShippingCharges, combinationIdentifier, combinations);
                    } else {
                        for (int j = 0; j < quantityInCombo; j++) {
                            prepareWsSaleOrderItem(response, saleOrder, packetShippingProviders, row, saleOrderItemCode + "-" + j, orderShippingCharges, combinationIdentifier,
                                    combinations);
                        }
                    }
                } else {
                    BigDecimal orderItemShippingCharges = null;
                    if (orderShippingCharges != null) {
                        orderItemShippingCharges = orderShippingCharges.divide(BigDecimal.valueOf(quantity));
                    }
                    for (int i = 0; i < quantity; i++) {
                        if (StringUtils.isNotBlank(row.getColumnValue(COMBINATION_IDENTIFIER))) {
                            combinationIdentifier = row.getColumnValue(COMBINATION_IDENTIFIER) + "-" + i;
                        }
                        if (quantityInCombo == null || quantityInCombo.equals(1)) {
                            prepareWsSaleOrderItem(response, saleOrder, packetShippingProviders, row, saleOrderItemCode + "-" + i, orderItemShippingCharges, combinationIdentifier,
                                    combinations);
                        } else {
                            for (int j = 0; j < quantityInCombo; j++) {
                                prepareWsSaleOrderItem(response, saleOrder, packetShippingProviders, row, saleOrderItemCode + "-" + i + "-" + j, orderItemShippingCharges,
                                        combinationIdentifier, combinations);
                            }
                        }
                    }
                }
            }

        }

        saleOrder.setCustomFieldValues(CustomFieldUtils.getCustomFieldValues(response, SaleOrder.class.getName(), rows.get(0)));

        //sale order changed
        if (!response.hasErrors()) {
            saleOrder.getAddresses().addAll(addresses.values());
            saleOrder.setBillingAddress(StringUtils.isNotBlank(billingAddressRef) ? new WsAddressRef(billingAddressRef) : null);
            saleOrder.getSaleOrderItemCombinations().addAll(combinations.values());
            saleOrder.setShippingAddress(StringUtils.isNotBlank(shippingAddressRef) ? new WsAddressRef(shippingAddressRef) : null);
            createSaleOrderRequest.setSaleOrder(saleOrder);
            long startTime = System.currentTimeMillis();
            response.addResponse(saleOrderService.createSaleOrder(createSaleOrderRequest));
            LOG.info("SaleOrder {} import took {} ms", saleOrder.getCode(), System.currentTimeMillis() - startTime);
        }
        return response;
    }

    private void prepareWsSaleOrderItem(ImportJobHandlerResponse response, WsSaleOrder saleOrder, Set<Integer> packetShippingProviders, Row row, String saleOrderItemCode,
            BigDecimal orderShippingCharges, String combinationIdentifier, Map<String, WsSaleOrderItemCombination> combinations) {
        WsSaleOrderItem wsSaleOrderItem = new WsSaleOrderItem();

        wsSaleOrderItem.setCode(saleOrderItemCode);
        try {
            wsSaleOrderItem.setShippingMethodCode(ShippingMethod.Code.valueOf(row.getColumnValue(SHIPPING_METHOD_CODE)).name());
        } catch (IllegalArgumentException e) {
            response.addError(new WsError("Invalid value for " + SHIPPING_METHOD_CODE));
        }
        wsSaleOrderItem.setCustomFieldValues(CustomFieldUtils.getCustomFieldValues(response, SaleOrderItem.class.getName(), row));
        if (StringUtils.isNotBlank(row.getColumnValue(PACKET_NUMBER))) {
            try {
                wsSaleOrderItem.setPacketNumber(Integer.parseInt(row.getColumnValue(PACKET_NUMBER)));
            } catch (NumberFormatException e) {
                response.addError(new WsError("Invalid value for " + PACKET_NUMBER));
            }
        }
        if (!packetShippingProviders.contains(wsSaleOrderItem.getPacketNumber())) {
            if (StringUtils.isNotBlank(row.getColumnValue(SHIPPING_PROVIDER)) && StringUtils.isNotBlank(row.getColumnValue(TRACKING_NUMBER))) {
                WsShippingProvider wsShippingProvider = new WsShippingProvider();
                wsShippingProvider.setCode(row.getColumnValue(SHIPPING_PROVIDER));
                wsShippingProvider.setTrackingNumber(row.getColumnValue(TRACKING_NUMBER));
                wsShippingProvider.setPacketNumber(wsSaleOrderItem.getPacketNumber());
                saleOrder.addShippingProvider(wsShippingProvider);
            }
        }

        wsSaleOrderItem.setItemSku(row.getColumnValue(ITEM_TYPE_SKU_CODE));
        wsSaleOrderItem.setChannelProductId(row.getColumnValue(CHANNEL_PRODUCT_ID));
        wsSaleOrderItem.setItemName(row.getColumnValue(ITEM_TYPE_NAME));
        if (StringUtils.isNotBlank(row.getColumnValue(SHIPPING_ADDRESS_ID))) {
            wsSaleOrderItem.setShippingAddress(new WsAddressRef(row.getColumnValue(SHIPPING_ADDRESS_ID)));
        }

        if (StringUtils.isNotBlank(row.getColumnValue(ON_HOLD))) {
            wsSaleOrderItem.setOnHold(StringUtils.parseBoolean(row.getColumnValue(ON_HOLD)));
        }

        if (StringUtils.isNotBlank(row.getColumnValue(FACILITY_CODE))) {
            wsSaleOrderItem.setFacilityCode(row.getColumnValue(FACILITY_CODE));
        }

        if (StringUtils.isBlank(combinationIdentifier) || !combinations.containsKey(combinationIdentifier)) {

            if (StringUtils.isNotBlank(row.getColumnValue(CASH_ON_DELIVERY_CHARGES))) {
                try {
                    wsSaleOrderItem.setCashOnDeliveryCharges(new BigDecimal(row.getColumnValue(CASH_ON_DELIVERY_CHARGES)));
                } catch (NumberFormatException e) {
                    response.addError(new WsError("Invalid value for " + CASH_ON_DELIVERY_CHARGES));
                }
            }

            if (StringUtils.isNotBlank(row.getColumnValue(DISCOUNT))) {
                try {
                    wsSaleOrderItem.setDiscount(new BigDecimal(row.getColumnValue(DISCOUNT)));
                } catch (NumberFormatException e) {
                    response.addError(new WsError("Invalid value for " + DISCOUNT));
                }
            }

            wsSaleOrderItem.setGiftMessage(row.getColumnValue(GIFT_MESSAGE));

            if (StringUtils.isNotBlank(row.getColumnValue(GIFT_WARP))) {
                wsSaleOrderItem.setGiftWrap(StringUtils.parseBoolean(row.getColumnValue(GIFT_WARP)));
            }

            if (StringUtils.isNotBlank(row.getColumnValue(GIFT_WRAP_CHARGES))) {
                try {
                    wsSaleOrderItem.setGiftWrapCharges(new BigDecimal(row.getColumnValue(GIFT_WRAP_CHARGES)));
                } catch (NumberFormatException e) {
                    response.addError(new WsError("Invalid value for " + GIFT_WRAP_CHARGES));
                }
            }

            if (StringUtils.isNotBlank(row.getColumnValue(VOUCHER_CODE))) {
                wsSaleOrderItem.setVoucherCode(row.getColumnValue(VOUCHER_CODE).trim());
            }

            if (StringUtils.isNotBlank(row.getColumnValue(VOUCHER_VALUE))) {
                try {
                    wsSaleOrderItem.setVoucherValue(new BigDecimal(row.getColumnValue(VOUCHER_VALUE)));
                } catch (NumberFormatException e) {
                    response.addError(new WsError("Invalid value for " + VOUCHER_VALUE));
                }
            }

            if (StringUtils.isNotBlank(row.getColumnValue(STORE_CREDIT))) {
                try {
                    wsSaleOrderItem.setStoreCredit(new BigDecimal(row.getColumnValue(STORE_CREDIT)));
                } catch (NumberFormatException e) {
                    response.addError(new WsError("Invalid value for " + STORE_CREDIT));
                }
            }

            try {
                wsSaleOrderItem.setSellingPrice(new BigDecimal(row.getColumnValue(SELLING_PRICE)));
            } catch (NumberFormatException e) {
                response.addError(new WsError("Invalid value for " + SELLING_PRICE));
            }

            if (StringUtils.isNotBlank(row.getColumnValue(SHIPPING_METHOD_CHARGES))) {
                try {
                    wsSaleOrderItem.setShippingMethodCharges(new BigDecimal(row.getColumnValue(SHIPPING_METHOD_CHARGES)));
                } catch (NumberFormatException e) {
                    response.addError(new WsError("Invalid value for " + SHIPPING_METHOD_CHARGES));
                }
            }

            if (StringUtils.isNotBlank(row.getColumnValue(PREPAID_AMOUNT))) {
                try {
                    wsSaleOrderItem.setPrepaidAmount(new BigDecimal(row.getColumnValue(PREPAID_AMOUNT)));
                } catch (NumberFormatException e) {
                    response.addError(new WsError("Invalid value for " + PREPAID_AMOUNT));
                }
            }

            if (orderShippingCharges == null) {
                if (StringUtils.isNotBlank(row.getColumnValue(SHIPPING_CHARGES))) {
                    try {
                        wsSaleOrderItem.setShippingCharges(new BigDecimal(row.getColumnValue(SHIPPING_CHARGES)));
                    } catch (NumberFormatException e) {
                        response.addError(new WsError("Invalid value for " + SHIPPING_CHARGES));
                    }
                }
            } else {
                wsSaleOrderItem.setShippingCharges(orderShippingCharges);
            }
            wsSaleOrderItem.setTotalPrice(wsSaleOrderItem.getSellingPrice().add(wsSaleOrderItem.getShippingCharges()).add(wsSaleOrderItem.getCashOnDeliveryCharges()).add(
                    wsSaleOrderItem.getShippingMethodCharges()));
        } else {
            wsSaleOrderItem.setTotalPrice(new BigDecimal(0));
            wsSaleOrderItem.setSellingPrice(new BigDecimal(0));
        }

        if (StringUtils.isNotBlank(combinationIdentifier) && !combinations.containsKey(combinationIdentifier)) {
            WsSaleOrderItemCombination wsSaleOrderItemCombination = new WsSaleOrderItemCombination();
            wsSaleOrderItemCombination.setCombinationIdentifier(combinationIdentifier);
            wsSaleOrderItemCombination.setCombinationDescription(row.getColumnValue(COMBINATION_DESCRIPTION));
            combinations.put(combinationIdentifier, wsSaleOrderItemCombination);
        }

        if (StringUtils.isNotBlank(REQUIRES_CUSTOMIZATION)) {
            wsSaleOrderItem.setRequiresCustomization(StringUtils.parseBoolean(row.getColumnValue(REQUIRES_CUSTOMIZATION)));
        }
        wsSaleOrderItem.setCombinationIdentifier(combinationIdentifier);
        saleOrder.getSaleOrderItems().add(wsSaleOrderItem);

    }

    private WsAddressDetail prepareShippingAddress(Row row) {
        WsAddressDetail wsAddressDetail = new WsAddressDetail();
        wsAddressDetail.setName(row.getColumnValue(SHIPPING_ADDRESS_NAME));
        wsAddressDetail.setId(row.getColumnValue(SHIPPING_ADDRESS_ID));
        wsAddressDetail.setAddressLine1(row.getColumnValue(SHIPPING_ADDRESS_LINE1));
        wsAddressDetail.setAddressLine2(row.getColumnValue(SHIPPING_ADDRESS_LINE2));
        wsAddressDetail.setPhone(row.getColumnValue(SHIPPING_ADDRESS_PHONE));
        wsAddressDetail.setPincode(row.getColumnValue(SHIPPING_ADDRESS_PINCODE));
        wsAddressDetail.setState(row.getColumnValue(SHIPPING_ADDRESS_STATE));
        if (StringUtils.isNotBlank(row.getColumnValue(SHIPPING_ADDRESS_COUNTRY))) {
            wsAddressDetail.setCountry(row.getColumnValue(SHIPPING_ADDRESS_COUNTRY));
        }
        wsAddressDetail.setCity(row.getColumnValue(SHIPPING_ADDRESS_CITY));
        return wsAddressDetail;
    }

    private WsAddressDetail prepareBillingAddress(Row row) {
        WsAddressDetail wsAddressDetail = new WsAddressDetail();
        wsAddressDetail.setName(row.getColumnValue(BILLING_ADDRESS_NAME));
        wsAddressDetail.setId(row.getColumnValue(BILLING_ADDRESS_ID));
        wsAddressDetail.setAddressLine1(row.getColumnValue(BILLING_ADDRESS_LINE1));
        wsAddressDetail.setAddressLine2(row.getColumnValue(BILLING_ADDRESS_LINE2));
        wsAddressDetail.setPhone(row.getColumnValue(BILLING_ADDRESS_PHONE));
        wsAddressDetail.setPincode(row.getColumnValue(BILLING_ADDRESS_PINCODE));
        wsAddressDetail.setState(row.getColumnValue(BILLING_ADDRESS_STATE));
        if (StringUtils.isNotBlank(row.getColumnValue(BILLING_ADDRESS_COUNTRY))) {
            wsAddressDetail.setCountry(row.getColumnValue(BILLING_ADDRESS_COUNTRY));
        }
        wsAddressDetail.setCity(row.getColumnValue(BILLING_ADDRESS_CITY));
        return wsAddressDetail;
    }

    @Override
    public void preProcessor(ImportJob importJob) {

    }

    @Override
    public void postProcessor(ImportJob importJob) {

    }
}
