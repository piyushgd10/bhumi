/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 23, 2012
 *  @author praveeng
 */
package com.uniware.services.imports;

import com.unifier.core.api.user.AddOrEditUserDetailRequest;
import com.unifier.core.api.user.AddOrEditUserDetailResponse;
import com.unifier.core.api.user.UserDTO;
import com.unifier.core.api.validation.WsError;
import com.unifier.core.entity.ImportJob;
import com.unifier.core.entity.ImportJobType.ImportOptions;
import com.unifier.core.entity.User;
import com.unifier.core.fileparser.Row;
import com.unifier.core.utils.StringUtils;
import com.unifier.services.imports.ImportJobHandler;
import com.unifier.services.imports.ImportJobHandlerRequest;
import com.unifier.services.imports.ImportJobHandlerResponse;
import com.unifier.services.users.IUsersService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class UsersImportJobHandler implements ImportJobHandler {
    private static final Logger LOG = LoggerFactory.getLogger(UsersImportJobHandler.class);

    private static final String FACILITY_CODE = "Facility Code";
    private static final String NAME          = "Name";
    private static final String USERNAME      = "Username";
    private static final String MOBILE        = "Mobile";
    private static final String ROLE          = "Role Code";

    @Autowired
    private IUsersService userService;

    @Override
    public ImportJobHandlerResponse handleRow(ImportJobHandlerRequest request, ImportOptions importOption) throws Exception {
        ImportJobHandlerResponse response = new ImportJobHandlerResponse();
        String username = request.getRows().get(0).getColumnValue(USERNAME);
        User user = userService.getUserByUsername(username);
        if (ImportOptions.CREATE_NEW_AND_UPDATE_EXISTING == importOption) {
            if (user == null) {
                response = createUser(request);
            } else {
                response = updateUser(request, user);
            }
        } else if (ImportOptions.CREATE_NEW == importOption) {
            if (user == null) {
                response = createUser(request);
            } else {
                response.addError(new WsError("User " + username + " already exists"));
            }
        } else if (ImportOptions.UPDATE_EXISTING == importOption) {
            if (user != null) {
                response = updateUser(request, user);
            } else {
                response.addError(new WsError("User " + username + " does not exist."));
            }
        }
        return response;
    }

    private ImportJobHandlerResponse updateUser(ImportJobHandlerRequest request, User user) {
        ImportJobHandlerResponse response = new ImportJobHandlerResponse();
        AddOrEditUserDetailRequest editUserRequest = new AddOrEditUserDetailRequest();
        editUserRequest.setUsername(user.getUsername());
        editUserRequest.setEnabled(true);
        editUserRequest.setMobile(request.getRows().get(0).getColumnValue(MOBILE));
        editUserRequest.setName(request.getRows().get(0).getColumnValue(NAME));
        Map<String, List<String>> facilityToRoles = new HashMap<>(request.getRows().size());
        List<UserDTO.FacilityRole> facilityRoles = new ArrayList<>(request.getRows().size());
        List<String> tenantRoles = new ArrayList<>(request.getRows().size());
        for (Row row : request.getRows()) {
            String facilityCode = row.getColumnValue(FACILITY_CODE);
            if (StringUtils.isNotBlank(facilityCode)) {
                if (facilityToRoles.get(facilityCode) == null) {
                    List<String> roles = new ArrayList<>();
                    facilityToRoles.put(facilityCode, roles);
                }
                facilityToRoles.get(facilityCode).add(row.getColumnValue(ROLE));
            } else {
                tenantRoles.add(row.getColumnValue(ROLE));
            }
        }
        Iterator<Map.Entry<String, List<String>>> iterator = facilityToRoles.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, List<String>> row = iterator.next();
            facilityRoles.add(new UserDTO.FacilityRole(row.getKey(), row.getValue()));
        }
        editUserRequest.setFacilityRoles(facilityRoles);
        editUserRequest.setTenantRoles(tenantRoles);
        AddOrEditUserDetailResponse editUserResponse = userService.editUserDetail(editUserRequest);
        response.addResponse(editUserResponse);
        return response;
    }

    private ImportJobHandlerResponse createUser(ImportJobHandlerRequest request) {
        ImportJobHandlerResponse response = new ImportJobHandlerResponse();
        AddOrEditUserDetailRequest addUserRequest = new AddOrEditUserDetailRequest();
        addUserRequest.setUsername(request.getRows().get(0).getColumnValue(USERNAME));
        addUserRequest.setEnabled(true);
        addUserRequest.setMobile(request.getRows().get(0).getColumnValue(MOBILE));
        addUserRequest.setName(request.getRows().get(0).getColumnValue(NAME));
        Map<String, List<String>> facilityToRoles = new HashMap<>(request.getRows().size());
        List<UserDTO.FacilityRole> facilityRoles = new ArrayList<>(request.getRows().size());
        List<String> tenantRoles = new ArrayList<>(request.getRows().size());
        for (Row row : request.getRows()) {
            String facilityCode = row.getColumnValue(FACILITY_CODE);
            if (StringUtils.isNotBlank(facilityCode)) {
                if (facilityToRoles.get(facilityCode) == null) {
                    List<String> roles = new ArrayList<>();
                    facilityToRoles.put(facilityCode, roles);
                }
                facilityToRoles.get(facilityCode).add(row.getColumnValue(ROLE));
            } else {
                tenantRoles.add(row.getColumnValue(ROLE));
            }
        }
        Iterator<Map.Entry<String, List<String>>> iterator = facilityToRoles.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, List<String>> row = iterator.next();
            facilityRoles.add(new UserDTO.FacilityRole(row.getKey(), row.getValue()));
        }
        addUserRequest.setFacilityRoles(facilityRoles);
        addUserRequest.setTenantRoles(tenantRoles);
        AddOrEditUserDetailResponse editUserResponse = userService.addUser(addUserRequest);
        response.addResponse(editUserResponse);
        return response;
    }

    @Override
    public void preProcessor(ImportJob importJob) {
    }

    @Override
    public void postProcessor(ImportJob importJob) {
    }

}
