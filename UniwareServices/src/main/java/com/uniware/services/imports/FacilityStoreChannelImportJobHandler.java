/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 26-Aug-2015
 *  @author parijat
 */
package com.uniware.services.imports;

import com.unifier.core.api.user.AddUserRolesRequest;
import com.unifier.core.api.user.CreateUserRequest;
import com.unifier.core.api.user.CreateUserResponse;
import com.unifier.core.api.validation.WsError;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.entity.ImportJob;
import com.unifier.core.entity.ImportJobType.ImportOptions;
import com.unifier.core.entity.User;
import com.unifier.core.fileparser.Row;
import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.StringUtils;
import com.unifier.services.imports.ImportJobHandler;
import com.unifier.services.imports.ImportJobHandlerRequest;
import com.unifier.services.imports.ImportJobHandlerResponse;
import com.unifier.services.users.IUsersService;
import com.unifier.services.utils.CustomFieldUtils;
import com.uniware.core.api.channel.AddChannelConnectorRequest;
import com.uniware.core.api.channel.AddChannelRequest;
import com.uniware.core.api.channel.ChannelDetailDTO;
import com.uniware.core.api.channel.EditChannelRequest;
import com.uniware.core.api.channel.WsChannel;
import com.uniware.core.api.channel.WsChannelConfigurationParameter;
import com.uniware.core.api.channel.WsChannelConnector;
import com.uniware.core.api.channel.WsChannelConnectorParameter;
import com.uniware.core.api.facility.CreateFacilityChannelRequest;
import com.uniware.core.api.facility.CreateFacilityChannelResponse;
import com.uniware.core.api.facility.CreateFacilityProfileRequest;
import com.uniware.core.api.facility.CreateFacilityRequest;
import com.uniware.core.api.facility.EditFacilityChannelRequest;
import com.uniware.core.api.facility.EditFacilityChannelResponse;
import com.uniware.core.api.party.WsFacility;
import com.uniware.core.api.party.WsFacilityProfile;
import com.uniware.core.api.party.WsPartyAddress;
import com.uniware.core.api.party.WsPartyContact;
import com.uniware.core.api.party.WsStoreTimings;
import com.uniware.core.api.warehouse.EditFacilityRequest;
import com.uniware.core.api.warehouse.FacilityDTO;
import com.uniware.core.api.warehouse.GetFacilityRequest;
import com.uniware.core.cache.FacilityCache;
import com.uniware.core.entity.Channel;
import com.uniware.core.entity.Facility;
import com.uniware.core.entity.FacilityProfile.DayTiming.DayOfWeek;
import com.uniware.core.entity.Location;
import com.uniware.core.entity.PartyAddressType;
import com.uniware.core.entity.PartyContactType;
import com.uniware.core.entity.Source;
import com.uniware.core.utils.UserContext;
import com.uniware.services.cache.ChannelCache;
import com.uniware.services.channel.IChannelService;
import com.uniware.services.configuration.SourceConfiguration;
import com.uniware.services.location.ILocationService;
import com.uniware.services.party.IPartyService;
import com.uniware.services.warehouse.IFacilityService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author parijat
 */
public class FacilityStoreChannelImportJobHandler implements ImportJobHandler {

    private static final Logger LOG                                   = LoggerFactory.getLogger(FacilityStoreChannelImportJobHandler.class);

    private static final String FACILITY_NAME                         = "Store Name";
    private static final String FACILITY_DISPLAY_NAME                 = "Store Display Name";
    private static final String FACILITY_CODE                         = "Store Code";
    private static final String FACILITY_ALTERNATE_CODE               = "Seller Store Code";
    private static final String FACILITY_ENABLED                      = "Enabled";
    private static final String FACILITY_PAN                          = "PAN";
    private static final String FACILITY_TIN                          = "TIN";
    private static final String FACILITY_CST                          = "CST Number";
    private static final String FACILITY_CIN                          = "CIN Number";
    private static final String FACILITY_ST                           = "Service Tax Number";
    private static final String FACILITY_WEBSITE                      = "Website URL";
    private static final String DEFAULT_OPENING_TIME                  = "08:00";
    private static final String DEFAULT_CLOSING_TIME                  = "20:00";
    private static final String STORE_CODE                            = "SNAPDEAL_O2O_STORE";

    //address details for shipping List<WsAddressDetail> in WsSaleOrder
    private static final String SHIPPING_ADDRESS_LINE1                = "Shipping Address Line1";
    private static final String SHIPPING_ADDRESS_LINE2                = "Shipping Address Line2";
    private static final String SHIPPING_ADDRESS_CITY                 = "Shipping Address City";
    private static final String SHIPPING_ADDRESS_STATE                = "Shipping Address State";
    private static final String SHIPPING_ADDRESS_COUNTRY              = "Shipping Address Country";
    private static final String SHIPPING_ADDRESS_PINCODE              = "Shipping Address Pincode";
    private static final String SHIPPING_ADDRESS_PHONE                = "Shipping Address Phone";

    //address details for  billing List<WsAddressDetail> in WsSaleOrder
    private static final String BILLING_SAME_AS_SHIPPING              = "Billing Same As Shipping";
    private static final String BILLING_ADDRESS_LINE1                 = "Billing Address Line1";
    private static final String BILLING_ADDRESS_LINE2                 = "Billing Address Line2";
    private static final String BILLING_ADDRESS_CITY                  = "Billing Address City";
    private static final String BILLING_ADDRESS_STATE                 = "Billing Address State";
    private static final String BILLING_ADDRESS_COUNTRY               = "Billing Address Country";
    private static final String BILLING_ADDRESS_PINCODE               = "Billing Address Pincode";
    private static final String BILLING_ADDRESS_PHONE                 = "Billing Address Phone";

    // USer details
    private static final String NAME                                  = "Name";
    private static final String USERNAME                              = "Username";
    private static final String PASSWORD                              = "Password";
    private static final String ROLE                                  = "User Role";
    private static final String USER_EMAIL                            = "Email";

    // Roles to preserve
    private static final String ROLES_TO_PRESERVE                     = "Roles To Preserve";

    // Facility Profile: Required for store type facility
    private static final String SELLER_NAME                           = "Store Manager Name";
    private static final String SELLER_MOBILE                         = "Store Manager Mobile";
    private static final String SELLER_EMAIL                          = "Store Manager Email";
    private static final String SELLER_ALTERNATE_PHONE                = "Alternate Phone";

    private static final String WEEKLY_CLOSED_DAYS                    = "Weekly Closed Days";
    private static final String STORE_OPENING_TIME                    = "Store Opening Time";
    private static final String STORE_CLOSING_TIME                    = "Store Closing Time";

    private static final String AUTO_SETUP_SERVICEABILITY             = "Auto Setup Serviceability";
    private static final String DELIVERY_RADIUS                       = "Delivery Radius";
    private static final String STORE_PICKUP_ENABLED                  = "Store pick Enabled";
    private static final String PICK_UP_SHIPPING_METHODS              = "Pick up Shipping Methods";
    private static final String STORE_SELF_DELIVERY_ENABLED           = "Store Self Delivery Enabled";
    private static final String STORE_SELF_SHIPPING_METHODS           = "Store Self Shipping Methods";
    private static final String DISPATCH_SLA                          = "Dispatch SLA";
    private static final String DELIVERY_SLA                          = "Delivery SLA";

    private static final String PANEL_USERNAME                        = "Panel Username";
    private static final String PANEL_PASSWORD                        = "Panel Password";
    private static final String API_KEY                               = "ApiKey";
    
    private static final String RESYNC_INVENTORY                      = "Resync Inventory";
    private static final String CUSTOMER_CODE                         = "Customer Code";
    private static final String BILLING_CODE                          = "Billing Party Code";
    private static final String ORDER_SYNC_STATUS                     = "Order Sync Status";
    private static final String INVENTORY_SYNC_STATUS                 = "Inventory Sync Status";
    private static final String PRICE_SYNC_STATUS                     = "Pricing Sync Status";
    private static final String RECONCILIATION_SYNC_STATUS            = "Reconciliation Sync Status";
    private static final String LEDGER_NAME                           = "Ledger Name";
    private static final String SCP_AUTO_VERIFY_ORDER                 = "Auto Verify Orders";                                               // true
                                                                                                                                             //    private static final String SCP_ALLOW_COMBINED_MANIFETS           = "Allow combined Manifest";          // false
    private static final String SCP_INVENTORY_ALLOCATION_PRIORITY     = "Inventory Allocation Priority";                                    // 0
    private static final String SCP_INVENTORY_UPDATE_FORMULA          = "Inventory Update Formula";                                         // null
                                                                                                                                             //    private static final String SCP_NOTIFY_CHANNEL_ON_DISPATCH        = "Notify Channel On Dispatch";       // false
    private static final String SCP_PACKAGE_TYPE                      = "Package Type";                                                     // FIXED
                                                                                                                                             //    private static final String SCP_PRE_SPLIT_ORDERS                  = "Pre Split Orders";                 // No
    private static final String SCP_PRODUCT_DELISTING                 = "Product Delisting";                                                // false
    private static final String SCP_PRODUCT_RELISTING                 = "Product Relisting";                                                // false
    private static final String SCP_SHIPMENT_LABEL_FORMAT             = "Shipment Label Format";                                            // PDF
    private static final String SCP_SHIPPING_LABEL_AGGREGATION_FORMAT = "Shipping Label Aggregation Format";                                // PDF
    private static final String SCP_TAT                               = "Tat (Hours)";                                                      // 48

    @Autowired
    private IChannelService     channelService;

    @Autowired
    private IFacilityService    facilityService;

    @Autowired
    private IUsersService       userService;

    @Autowired
    private IPartyService       partyService;

    @Autowired
    private ILocationService    locationService;

    /* (non-Javadoc)
     * @see com.uniware.services.imports.FacilityImportJobHandler#handleRow(com.unifier.services.imports.ImportJobHandlerRequest, com.unifier.core.entity.ImportJobType.ImportOptions)
     */
    @Override
    public ImportJobHandlerResponse handleRow(ImportJobHandlerRequest request, ImportOptions importOption) throws Exception {
        ImportJobHandlerResponse response = new ImportJobHandlerResponse();
        String facilityCode = request.getRow().getColumnValue(FACILITY_CODE);
        GetFacilityRequest req = new GetFacilityRequest();
        req.setCode(facilityCode);
        FacilityDTO facility = facilityService.getFacilityByCode(req).getFacilityDTO();
        String channelCode = constructChannelCodeByName("SNAPDEAL_O2O_" + request.getRow().getColumnValue(FACILITY_CODE));
        Channel channel = channelService.getChannelByCode(channelCode);
        Source source = ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getSourceByCode(STORE_CODE);
        boolean isAllowed = true;
        if (!source.isFacilityAssociationRequired()) {
            response.addError(new WsError("Import only allowed for source which have facility association enabled."));
            isAllowed = false;
        }
        if (channel != null) {
            ChannelDetailDTO channelDetail = CacheManager.getInstance().getCache(ChannelCache.class).getChannelDetailDTOByCode(channelCode);
            if (StringUtils.isNotBlank(channelDetail.getAssociatedFacility()) && !facilityCode.equals(channelDetail.getAssociatedFacility())) {
                response.addError(new WsError("Ambigous store code contact support."));
                isAllowed = false;
            }
        }
        if (!isAllowed) {
            return response;
        }


        if (ImportOptions.CREATE_NEW_AND_UPDATE_EXISTING == importOption) {
            if (facility == null) {
                response = createFacilityChannel(request);
            } else {
                response = editFacilityChannel(request, facility);
            }
        } else if (ImportOptions.CREATE_NEW == importOption) {
            if (facility == null) {
                response = createFacilityChannel(request);
            } else {
                response.addError(new WsError("Facility " + facility.getDisplayName() + " already exists"));
            }
        } else if (ImportOptions.UPDATE_EXISTING == importOption) {
            if (facility != null) {
                response = editFacilityChannel(request, facility);
            } else {
                response.addError(new WsError("Facility " + facility.getDisplayName() + " does not exist."));
            }
        }

        if (response.isSuccessful() && request.getRow().getColumnValue(USERNAME) != null) {
            Facility f = CacheManager.getInstance().getCache(FacilityCache.class).getFacilityById((Integer) response.getResultItems().get("facilityId"));
            UserContext.current().setFacility(f);
            User user = userService.getUserByUsername(request.getRow().getColumnValue(USERNAME));
            if (user == null) {
                response.addResponse(createUser(request));
            }
        }
        return response;
    }

    private ImportJobHandlerResponse editFacilityChannel(ImportJobHandlerRequest request, FacilityDTO facility) {
        ImportJobHandlerResponse response = new ImportJobHandlerResponse();
        Row row = request.getRow();

        WsFacility wsFacility = prepareWsFacility(row, response);
        WsFacilityProfile profile = null;
        profile = prepareWsFacilityProfile(row, response);
        WsChannel channel = prepareWsChannel(row, response);
        List<AddChannelConnectorRequest> channelConnectorRequests = prepareChannelConnectorsRequest(row, response);
        if(!response.hasErrors()){
            EditFacilityChannelRequest editFacilityChannelRequest = new EditFacilityChannelRequest();
            EditFacilityRequest editFacilityRequest = new EditFacilityRequest();
            editFacilityRequest.setFacility(wsFacility);
            EditChannelRequest editChannelRequest = new EditChannelRequest();
            editChannelRequest.setWsChannel(channel);
            CreateFacilityProfileRequest profileRequest = new CreateFacilityProfileRequest();
            profileRequest.setFacilityProfile(profile);
            editFacilityChannelRequest.setFacilityRequest(editFacilityRequest);
            editFacilityChannelRequest.setFacilityProfileRequest(profileRequest);
            editFacilityChannelRequest.setChannelRequest(editChannelRequest);
            editFacilityChannelRequest.setChannelConnectorRequests(channelConnectorRequests);

            EditFacilityChannelResponse editFacilityChannelResponse = null;
            try {
                editFacilityChannelResponse = facilityService.editFacilityAndChannel(editFacilityChannelRequest);
                response.addResponse(editFacilityChannelResponse);
            } catch (Exception e) {
                LOG.error("Error updating facility: ", e);
                response.addError(new WsError(e.getMessage()));
            }
            if (editFacilityChannelResponse != null && !editFacilityChannelResponse.hasErrors()) {
                response.addResultItem("facilityId", editFacilityChannelResponse.getFacilityResponse().getFacilityDTO().getId());
            }
        }
        return response;
    }

    private ImportJobHandlerResponse createFacilityChannel(ImportJobHandlerRequest request) {
        ImportJobHandlerResponse response = new ImportJobHandlerResponse();
        Row row = request.getRow();

        WsFacility wsFacility = prepareWsFacility(row, response);
        WsFacilityProfile profile = null;
        profile = prepareWsFacilityProfile(row, response);
        WsChannel channel = prepareWsChannel(row, response);
        List<AddChannelConnectorRequest> channelConnectorRequests = prepareChannelConnectorsRequest(row, response);
        if (!response.hasErrors()) {
            CreateFacilityChannelRequest createFacilityChannelRequest = new CreateFacilityChannelRequest();
            CreateFacilityRequest createFacilityRequest = new CreateFacilityRequest();
            createFacilityRequest.setFacility(wsFacility);
            createFacilityRequest.setUsername(userService.getUserWithDetailsById(request.getUserId()).getUsername());
            CreateFacilityChannelResponse createFacilityChannelResponse = null;
            AddChannelRequest channelRequest = new AddChannelRequest();
            channelRequest.setWsChannel(channel);
            
            CreateFacilityProfileRequest profileRequest = new CreateFacilityProfileRequest();
            profileRequest.setFacilityProfile(profile);
            createFacilityChannelRequest.setFacilityRequest(createFacilityRequest);
            createFacilityChannelRequest.setFacilityProfileRequest(profileRequest);
            createFacilityChannelRequest.setChannelRequest(channelRequest);
            createFacilityChannelRequest.setChannelConnectorRequests(channelConnectorRequests);

            try {
                createFacilityChannelResponse = facilityService.createFacilityAndChannel(createFacilityChannelRequest);
                response.addResponse(createFacilityChannelResponse);
            } catch (Exception e) {
                LOG.error("Error creating facility: ", e);
                response.addError(new WsError(e.getMessage()));
            }

            if (createFacilityChannelResponse != null && !createFacilityChannelResponse.hasErrors()) {
                response.addResultItem("facilityId", createFacilityChannelResponse.getFacilityResponse().getFacilityDTO().getId());

                // See if we need to preserve roles in this facility.
                if (StringUtils.isNotEmpty(row.getColumnValue(ROLES_TO_PRESERVE))) {
                    for (String roleCode : row.getColumnValue(ROLES_TO_PRESERVE).split(",")) {
                        List<String> usernames = userService.getUsersByRole(roleCode);
                        for (String username : usernames) {
                            AddUserRolesRequest addRoleRequest = new AddUserRolesRequest();
                            addRoleRequest.setUsername(username);
                            addRoleRequest.addFacilityCode(createFacilityChannelResponse.getFacilityResponse().getFacilityDTO().getCode());
                            addRoleRequest.addFacilityRoleCode(roleCode);
                            userService.addUserRoles(addRoleRequest);
                        }
                    }
                }

            }
        }
        return response;
    }

    private List<AddChannelConnectorRequest> prepareChannelConnectorsRequest(Row row, ImportJobHandlerResponse response) {
        List<AddChannelConnectorRequest> channelConnectorRequest = new ArrayList<>();
        String username = row.getColumnValue(PANEL_USERNAME);
        if (StringUtils.isBlank(username)) {
            response.addError(new WsError("Valid Panel username required to create store"));
        }
        String password = row.getColumnValue(PANEL_PASSWORD);
        if (StringUtils.isBlank(password)) {
            response.addError(new WsError("Valid Panel password required to create store"));
        }
        String apiKey = row.getColumnValue(API_KEY);
        if (StringUtils.isBlank(apiKey)) {
            response.addError(new WsError("Valid api key required to create store"));
        }
      
        // Channel connector parameter for seller panel
        
        AddChannelConnectorRequest request = new AddChannelConnectorRequest();
        WsChannelConnector connector = new WsChannelConnector();
        connector.setChannelCode(constructChannelCodeByName("SNAPDEAL_O2O_" + row.getColumnValue(FACILITY_CODE)));
        List<WsChannelConnectorParameter> channelConnectorParameters = new ArrayList<>();
        WsChannelConnectorParameter sellerPanelUsernameParameter = new WsChannelConnectorParameter();
        sellerPanelUsernameParameter.setName("username");
        sellerPanelUsernameParameter.setValue(username);
        channelConnectorParameters.add(sellerPanelUsernameParameter);
        WsChannelConnectorParameter sellerPanelPasswordParameter = new WsChannelConnectorParameter();
        sellerPanelPasswordParameter.setName("password");
        sellerPanelPasswordParameter.setValue(password);
        channelConnectorParameters.add(sellerPanelPasswordParameter);
        connector.setChannelConnectorParameters(channelConnectorParameters);
        connector.setName("SNAPDEAL_O2O_STORE_SELLER_PANEL");
        request.setChannelConnector(connector);
        channelConnectorRequest.add(request);
        
        // Channel connector parameter for api key
        
        AddChannelConnectorRequest apiKeyRequest = new AddChannelConnectorRequest();
        WsChannelConnector apiKeyConnector = new WsChannelConnector();
        apiKeyConnector.setChannelCode(constructChannelCodeByName("SNAPDEAL_O2O_" + row.getColumnValue(FACILITY_CODE)));
        List<WsChannelConnectorParameter> apiChannelConnectorParameters = new ArrayList<>();
        WsChannelConnectorParameter apiKeyParam = new WsChannelConnectorParameter();
        apiKeyParam.setName("apiKey");
        apiKeyParam.setValue(apiKey);
        apiChannelConnectorParameters.add(apiKeyParam);
        apiKeyConnector.setChannelConnectorParameters(apiChannelConnectorParameters);
        apiKeyConnector.setName("SNAPDEAL_O2O_API");
        apiKeyRequest.setChannelConnector(apiKeyConnector);
        channelConnectorRequest.add(apiKeyRequest);
        
        return channelConnectorRequest;
    }

    private String constructChannelCodeByName(String channelName) {
        return channelName.replaceAll("\\W", "_").toUpperCase();
    }

    private WsChannel prepareWsChannel(Row row, ImportJobHandlerResponse response) {
        WsChannel channel = new WsChannel();
        channel.setSourceCode(STORE_CODE);
        channel.setChannelName("SNAPDEAL_O2O_" + row.getColumnValue(FACILITY_CODE));
        channel.setEnabled(true);
        channel.setThirdPartyShipping(false);
        channel.setReSyncInventory("1".equals(row.getColumnValue(RESYNC_INVENTORY)) ? true : false);
        if (StringUtils.isNotBlank(row.getColumnValue(CUSTOMER_CODE))) {
            channel.setCustomerCode(row.getColumnValue(CUSTOMER_CODE));
        }
        if (StringUtils.isNotBlank(row.getColumnValue(BILLING_CODE))) {
            channel.setBillingPartyCode(row.getColumnValue(BILLING_CODE));
        }
        if (StringUtils.isNotBlank(row.getColumnValue(ORDER_SYNC_STATUS)) && Channel.SyncStatus.valueOf(row.getColumnValue(ORDER_SYNC_STATUS)) != null) {
            channel.setOrderSyncStatus(row.getColumnValue(ORDER_SYNC_STATUS));
        } else {
            channel.setOrderSyncStatus(Channel.SyncStatus.OFF.name());
        }
        if (StringUtils.isNotBlank(row.getColumnValue(INVENTORY_SYNC_STATUS)) && Channel.SyncStatus.valueOf(row.getColumnValue(INVENTORY_SYNC_STATUS)) != null) {
            channel.setInventorySyncStatus(row.getColumnValue(INVENTORY_SYNC_STATUS));
        } else {
            channel.setInventorySyncStatus(Channel.SyncStatus.OFF.name());
        }
        if (StringUtils.isNotBlank(row.getColumnValue(PRICE_SYNC_STATUS)) && Channel.SyncStatus.valueOf(row.getColumnValue(PRICE_SYNC_STATUS)) != null) {
            channel.setPricingSyncStatus(row.getColumnValue(PRICE_SYNC_STATUS));
        } else {
            channel.setPricingSyncStatus(Channel.SyncStatus.OFF.name());
        }
        if (StringUtils.isNotBlank(row.getColumnValue(RECONCILIATION_SYNC_STATUS)) && Channel.SyncStatus.valueOf(row.getColumnValue(RECONCILIATION_SYNC_STATUS)) != null) {
            channel.setOrderReconciliationSyncStatus(row.getColumnValue(RECONCILIATION_SYNC_STATUS));
        } else {
            channel.setOrderReconciliationSyncStatus(Channel.SyncStatus.OFF.name());
        }
        if (StringUtils.isNotBlank(row.getColumnValue(LEDGER_NAME))) {
            channel.setLedgerName(row.getColumnValue(LEDGER_NAME));
        }
        List<WsChannelConfigurationParameter> configurationParameters = new ArrayList<>();
        if (StringUtils.isNotBlank(row.getColumnValue(SCP_AUTO_VERIFY_ORDER))) {
            configurationParameters.add(new WsChannelConfigurationParameter(Channel.AUTO_VERIFY_ORDERS, row.getColumnValue(SCP_AUTO_VERIFY_ORDER)));
        }
        if (StringUtils.isNotBlank(row.getColumnValue(SCP_INVENTORY_ALLOCATION_PRIORITY))) {
            configurationParameters.add(new WsChannelConfigurationParameter(Channel.INVENTORY_ALLOCATION_PRIORITY, row.getColumnValue(SCP_INVENTORY_ALLOCATION_PRIORITY)));
        }
        if (StringUtils.isNotBlank(row.getColumnValue(SCP_INVENTORY_UPDATE_FORMULA))) {
            configurationParameters.add(new WsChannelConfigurationParameter(Channel.INVENTORY_UPDATE_FORMULA, row.getColumnValue(SCP_INVENTORY_UPDATE_FORMULA)));
        }
        if (StringUtils.isNotBlank(row.getColumnValue(SCP_PACKAGE_TYPE))) {
            configurationParameters.add(new WsChannelConfigurationParameter(Channel.PACKAGE_TYPE, row.getColumnValue(SCP_PACKAGE_TYPE)));
        }
        if (StringUtils.isNotBlank(row.getColumnValue(SCP_PRODUCT_DELISTING))) {
            configurationParameters.add(new WsChannelConfigurationParameter(Channel.PRODUCT_DELISTING_ENABLED, row.getColumnValue(SCP_PRODUCT_DELISTING)));
        }
        if (StringUtils.isNotBlank(row.getColumnValue(SCP_PRODUCT_RELISTING))) {
            configurationParameters.add(new WsChannelConfigurationParameter(Channel.PRODUCT_RELISTING_ENABLED, row.getColumnValue(SCP_PRODUCT_RELISTING)));
        }
        if (StringUtils.isNotBlank(row.getColumnValue(SCP_SHIPMENT_LABEL_FORMAT))) {
            configurationParameters.add(new WsChannelConfigurationParameter(Channel.SHIPMENT_LABEL_FORMAT, row.getColumnValue(SCP_SHIPMENT_LABEL_FORMAT)));
        }
        if (StringUtils.isNotBlank(row.getColumnValue(SCP_SHIPPING_LABEL_AGGREGATION_FORMAT))) {
            configurationParameters.add(new WsChannelConfigurationParameter(Channel.SHIPPING_LABEL_AGGREGATION_FORMAT, row.getColumnValue(SCP_SHIPPING_LABEL_AGGREGATION_FORMAT)));
        }
        if (StringUtils.isNotBlank(row.getColumnValue(SCP_TAT))) {
            configurationParameters.add(new WsChannelConfigurationParameter(Channel.TAT, row.getColumnValue(SCP_TAT)));
        }
        //configurationParameters.add(new WsChannelConfigurationParameter(Channel.ASSOCIATED_FACILITY, row.getColumnValue(FACILITY_CODE)));
        channel.setChannelConfigurationParameters(configurationParameters);
        return channel;
    }

    private WsFacility prepareWsFacility(Row row, ImportJobHandlerResponse response) {
        WsFacility wsFacility = new WsFacility();
        wsFacility.setType(Facility.Type.STORE.name());
        wsFacility.setAlternateCode(row.getColumnValue(FACILITY_ALTERNATE_CODE));
        wsFacility.setDisplayName(row.getColumnValue(FACILITY_DISPLAY_NAME));
        wsFacility.setCode(row.getColumnValue(FACILITY_CODE));
        wsFacility.setEnabled("0".equals(row.getColumnValue(FACILITY_ENABLED)) ? false : true);
        wsFacility.setCstNumber(row.getColumnValue(FACILITY_CST));
        wsFacility.setCinNumber(row.getColumnValue(FACILITY_CIN));
        wsFacility.setName(row.getColumnValue(FACILITY_NAME));
        wsFacility.setPan(row.getColumnValue(FACILITY_PAN));
        wsFacility.setStNumber(row.getColumnValue(FACILITY_ST));
        wsFacility.setTin(row.getColumnValue(FACILITY_TIN));
        wsFacility.setWebsite(row.getColumnValue(FACILITY_WEBSITE));
        wsFacility.setCustomFieldValues(CustomFieldUtils.getCustomFieldValues(response, Facility.class.getName(), row));
        wsFacility.getPartyContacts().add(preparePartyContact(row, response));
        wsFacility.setShippingAddress(preparePartyShippingAddress(row, response));
        wsFacility.setBillingAddress(preparePartyBillingAddress(row, response, wsFacility.getShippingAddress()));
        return wsFacility;
    }

    private WsPartyContact preparePartyContact(Row row, ImportJobHandlerResponse response) {
        WsPartyContact wsPartyContact = new WsPartyContact();
        wsPartyContact.setContactType(PartyContactType.Code.PRIMARY.name());
        wsPartyContact.setEmail(row.getColumnValue(SELLER_EMAIL));
        wsPartyContact.setName(row.getColumnValue(SELLER_NAME));
        wsPartyContact.setPartyCode(row.getColumnValue(FACILITY_CODE));
        wsPartyContact.setPhone(row.getColumnValue(SELLER_MOBILE));
        return wsPartyContact;
    }

    private WsPartyAddress preparePartyBillingAddress(Row row, ImportJobHandlerResponse response, WsPartyAddress wsPartyShippingAddress) {
        WsPartyAddress wsPartyBillingAddress = new WsPartyAddress();
        if (StringUtils.isNotBlank(row.getColumnValue(BILLING_SAME_AS_SHIPPING)) && !("1".equals(row.getColumnValue(BILLING_SAME_AS_SHIPPING)))) {
            wsPartyBillingAddress.setAddressLine1(row.getColumnValue(BILLING_ADDRESS_LINE1));
            wsPartyBillingAddress.setAddressLine2(row.getColumnValue(BILLING_ADDRESS_LINE2));
            wsPartyBillingAddress.setCity(row.getColumnValue(BILLING_ADDRESS_CITY));
            if (StringUtils.isNotBlank(row.getColumnValue(BILLING_ADDRESS_COUNTRY))) {
                wsPartyBillingAddress.setCountryCode(row.getColumnValue(BILLING_ADDRESS_COUNTRY));
            }
            wsPartyBillingAddress.setStateCode(row.getColumnValue(BILLING_ADDRESS_STATE));
            wsPartyBillingAddress.setPartyCode(row.getColumnValue(FACILITY_CODE));
            wsPartyBillingAddress.setAddressType(PartyAddressType.Code.BILLING.name());
            wsPartyBillingAddress.setPincode(row.getColumnValue(BILLING_ADDRESS_PINCODE));
            wsPartyBillingAddress.setPhone(row.getColumnValue(BILLING_ADDRESS_PHONE));
        } else {
            wsPartyBillingAddress = wsPartyShippingAddress;
        }
        return wsPartyBillingAddress;
    }

    private WsPartyAddress preparePartyShippingAddress(Row row, ImportJobHandlerResponse response) {
        WsPartyAddress wsPartyAddress = new WsPartyAddress();
        wsPartyAddress.setAddressLine1(row.getColumnValue(SHIPPING_ADDRESS_LINE1));
        wsPartyAddress.setAddressLine2(row.getColumnValue(SHIPPING_ADDRESS_LINE2));
        wsPartyAddress.setCity(row.getColumnValue(SHIPPING_ADDRESS_CITY));
        if (StringUtils.isNotBlank(row.getColumnValue(SHIPPING_ADDRESS_COUNTRY))) {
            wsPartyAddress.setCountryCode(row.getColumnValue(SHIPPING_ADDRESS_COUNTRY));
        }
        wsPartyAddress.setStateCode(row.getColumnValue(SHIPPING_ADDRESS_STATE));
        wsPartyAddress.setPartyCode(row.getColumnValue(FACILITY_CODE));
        wsPartyAddress.setAddressType(PartyAddressType.Code.SHIPPING.name());
        wsPartyAddress.setPincode(row.getColumnValue(SHIPPING_ADDRESS_PINCODE));
        wsPartyAddress.setPhone(row.getColumnValue(SHIPPING_ADDRESS_PHONE));
        return wsPartyAddress;
    }

    private WsFacilityProfile prepareWsFacilityProfile(Row row, ImportJobHandlerResponse response) {
        WsFacilityProfile wsProfile = new WsFacilityProfile();
        wsProfile.setFacilityCode(row.getColumnValue(FACILITY_CODE));
        wsProfile.setSellerName(row.getColumnValue(SELLER_NAME));
        wsProfile.setContactEmail(row.getColumnValue(SELLER_EMAIL));
        wsProfile.setMobile(row.getColumnValue(SELLER_MOBILE));
        wsProfile.setAlternatePhone(row.getColumnValue(SELLER_ALTERNATE_PHONE));
        Location location = locationService.getLocationByPincode(row.getColumnValue(SHIPPING_ADDRESS_PINCODE));
        if (location == null) {
            response.addError(new WsError("No position co-ordinates found for given pincode " + row.getColumnValue(SHIPPING_ADDRESS_PINCODE)));
        } else {
            List<Double> position = new ArrayList<>();
            for (double coordinate : location.getPosition()) {
                position.add(Double.valueOf(coordinate));
            }
            wsProfile.setPosition(position);
            wsProfile.setAutoSetupServiceability("1".equals(row.getColumnValue(AUTO_SETUP_SERVICEABILITY)) ? true : false);
            wsProfile.setStoreDeliveryEnabled("1".equals(row.getColumnValue(STORE_SELF_DELIVERY_ENABLED)) ? true : false);
            if (StringUtils.isNotBlank(row.getColumnValue(DELIVERY_RADIUS))) {
                try {
                    wsProfile.setDeliveryRadius(Double.valueOf(row.getColumnValue(DELIVERY_RADIUS)));
                } catch (Exception e) {
                    response.addError(new WsError("Invalid value for delivery radius"));
                }
            } else {
                wsProfile.setDeliveryRadius(new Double(10.0));
            }
        }
        wsProfile.setStorePickupEnabled("1".equals(row.getColumnValue(STORE_PICKUP_ENABLED)) ? true : false);
        if (!response.hasErrors()) {
            if (wsProfile.getStorePickupEnabled()) {
                if (StringUtils.isNotBlank(row.getColumnValue(PICK_UP_SHIPPING_METHODS))) {
                    List<String> methods = new ArrayList<>();
                    for (String method : row.getColumnValue(PICK_UP_SHIPPING_METHODS).split(",")) {
                        methods.add(method.trim());
                    }
                    wsProfile.setPickupShippingMethods(methods);
                } else {
                    response.addError(new WsError("Please fill valid values for pick up shipping methods. Pickup-COD,Pickup-Prepaid"));
                }
            } else {
                wsProfile.setPickupShippingMethods(new ArrayList<String>());
            }
        }

        if (!response.hasErrors()) {
            if (wsProfile.getStoreDeliveryEnabled()) {
                if (StringUtils.isNotBlank(row.getColumnValue(STORE_SELF_SHIPPING_METHODS))) {
                    List<String> methods = new ArrayList<>();
                    for (String method : row.getColumnValue(STORE_SELF_SHIPPING_METHODS).split(",")) {
                        methods.add(method.trim());
                    }
                    wsProfile.setSelfShippingMethods(methods);
                } else {
                    response.addError(new WsError("Please fill valid values for pick up shipping methods. Standard-COD,Standard-Prepaid"));
                }
            } else {
                wsProfile.setSelfShippingMethods(new ArrayList<String>());
            }
        }
        if (!response.hasErrors()) {
            if (StringUtils.isNotBlank(row.getColumnValue(DISPATCH_SLA))) {
                try {
                    wsProfile.setDispatchSLA(Integer.valueOf(row.getColumnValue(DISPATCH_SLA)).intValue());
                } catch (Exception e) {
                    response.addError(new WsError("Dispatch SLA invalid"));
                }
            } else {
                response.addError(new WsError("Dispatch SLA is mandatory"));
            }
        }
        if (!response.hasErrors()) {
            if (StringUtils.isNotBlank(row.getColumnValue(DELIVERY_SLA))) {
                try {
                    wsProfile.setDeliverySLA(Integer.valueOf(row.getColumnValue(DELIVERY_SLA)).intValue());
                } catch (Exception e) {
                    response.addError(new WsError("Delivery SLA invalid"));
                }
            } else {
                response.addError(new WsError("Delivery sla is mandatory"));
            }
        }

        if (!response.hasErrors()) {
            List<WsStoreTimings> timings = new ArrayList<>();
            List<String> weeklyOff = new ArrayList<>();
            if (StringUtils.isNotBlank(row.getColumnValue(WEEKLY_CLOSED_DAYS))) {
                for (String offDay : row.getColumnValue(WEEKLY_CLOSED_DAYS).split(",")) {
                    weeklyOff.add(offDay.trim().toUpperCase());
                }
            }
            String openingTime = DEFAULT_OPENING_TIME;
            String closingTime = DEFAULT_CLOSING_TIME;
            if (StringUtils.isNotBlank(row.getColumnValue(STORE_OPENING_TIME))) {
                try {
                    Date openingDate = DateUtils.stringToDate(row.getColumnValue(STORE_OPENING_TIME), "HH:mm");
                    openingTime = DateUtils.dateToString(openingDate, "HH:mm");
                } catch (Exception e) {
                    response.addError(new WsError("Invalid opening time format,it should be hh:mm"));
                }
            }
            if (StringUtils.isNotBlank(row.getColumnValue(STORE_CLOSING_TIME))) {
                try {
                    Date closingDate = DateUtils.stringToDate(row.getColumnValue(STORE_CLOSING_TIME), "HH:mm");
                    closingTime = DateUtils.dateToString(closingDate, "HH:mm");
                } catch (Exception e) {
                    response.addError(new WsError("Invalid closing time format,it should be hh:mm"));
                }
            }
            for (DayOfWeek day : DayOfWeek.values()) {
                if (!weeklyOff.contains(day.name())) {
                    WsStoreTimings storeTiming = new WsStoreTimings();
                    storeTiming.setDayOfWeek(day.name());
                    storeTiming.setOpeningTime(openingTime);
                    storeTiming.setClosingTime(closingTime);
                    timings.add(storeTiming);
                }
            }
            wsProfile.setStoreTimings(timings);
        }

        return wsProfile;
    }

    private CreateUserResponse createUser(ImportJobHandlerRequest request) {
        CreateUserRequest createUserRequest = new CreateUserRequest();
        createUserRequest.setName(request.getRow().getColumnValue(NAME));
        createUserRequest.setUsername(request.getRow().getColumnValue(USERNAME));
        createUserRequest.setPassword(request.getRow().getColumnValue(PASSWORD));
        createUserRequest.setConfirmPassword(request.getRow().getColumnValue(PASSWORD));
        createUserRequest.setEmail(request.getRow().getColumnValue(USER_EMAIL));
        createUserRequest.addRole(request.getRow().getColumnValue(ROLE));
        return userService.createUser(createUserRequest);
    }

    /* (non-Javadoc)
     * @see com.uniware.services.imports.FacilityImportJobHandler#preProcessor(com.unifier.core.entity.ImportJob)
     */
    @Override
    public void preProcessor(ImportJob importJob) {
    }

    /* (non-Javadoc)
     * 
     * @see com.uniware.services.imports.FacilityImportJobHandler#postProcessor(com.unifier.core.entity.ImportJob)
     */
    @Override
    public void postProcessor(ImportJob importJob) {
    }

}
