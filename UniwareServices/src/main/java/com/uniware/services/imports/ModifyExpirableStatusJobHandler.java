/**
 * Copyright 2017 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version 1.0, 25/09/17
 * @author aditya
 */
package com.uniware.services.imports;

import static com.uniware.core.utils.Constants.FROM_CATEGORY;

import org.springframework.beans.factory.annotation.Autowired;

import com.unifier.core.api.base.ServiceResponse;
import com.unifier.core.api.validation.WsError;
import com.unifier.core.entity.ImportJob;
import com.unifier.core.entity.ImportJobType;
import com.unifier.core.utils.StringUtils;
import com.unifier.services.imports.ImportJobHandler;
import com.unifier.services.imports.ImportJobHandlerRequest;
import com.unifier.services.imports.ImportJobHandlerResponse;
import com.uniware.core.api.catalog.EditItemTypeRequest;
import com.uniware.core.api.catalog.EditItemTypeResponse;
import com.uniware.core.api.catalog.WsItemType;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.services.catalog.ICatalogService;

public class ModifyExpirableStatusJobHandler implements ImportJobHandler {

    @SuppressWarnings("FieldCanBeLocal")
    private static String   SKU_CODE = "sku code", EXPIRABLE = "expirable", SHELF_LIFE = "shelf life";

    @Autowired
    private ICatalogService catalogService;

    @Override
    public ImportJobHandlerResponse handleRow(ImportJobHandlerRequest request, ImportJobType.ImportOptions importOption) throws Exception {
        ImportJobHandlerResponse response = new ImportJobHandlerResponse();
        if (ImportJobType.ImportOptions.UPDATE_EXISTING == importOption) {
            EditItemTypeRequest editItemTypeRequest = new EditItemTypeRequest();
            WsItemType wsItemType = new WsItemType();
            wsItemType.setSkuCode(request.getRow().getColumnValue(SKU_CODE));
            Boolean expirable = parseExpirable(request.getRow().getColumnValue(EXPIRABLE));
            if (expirable == null || expirable) {
                wsItemType.setExpirable(expirable);
            } else {
                response.addError(new WsError(WsResponseCode.INVALID_REQUEST.code(), "Invalid value for expirable."));
                response.setSuccessful(false);
                return response;
            }
            if (StringUtils.isNotBlank(request.getRow().getColumnValue(SHELF_LIFE))) {
                wsItemType.setShelfLife(Integer.parseInt(request.getRow().getColumnValue(SHELF_LIFE)));
            }
            if (Boolean.TRUE.equals(wsItemType.getExpirable()) && wsItemType.getShelfLife() == null) {
                response.addError(new WsError(WsResponseCode.INVALID_REQUEST.code(), "Shelf Life must not be empty for expirable items."));
                response.setSuccessful(false);
                return response;
            }
            editItemTypeRequest.setItemType(wsItemType);
            EditItemTypeResponse editItemTypeResponse = catalogService.editItemType(editItemTypeRequest);
            if (editItemTypeResponse.hasErrors()) {
                response.addErrors(editItemTypeResponse.getErrors());
                response.addWarnings(editItemTypeResponse.getWarnings());
            } else {
                response.setSuccessful(true);
            }
        } else {
            ServiceResponse response1 = new ServiceResponse();
            response1.addError(new WsError("Operation not allowed."));
            response.addResponse(response1);
        }
        return response;
    }

    public Boolean parseExpirable(String s) {
        return StringUtils.equalsIngoreCaseAny(s.trim(), FROM_CATEGORY) ? null : StringUtils.parseBoolean(s);
    }

    @Override
    public void preProcessor(ImportJob importJob) {
    }

    @Override
    public void postProcessor(ImportJob importJob) {
    }

}
