/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Apr 27, 2012
 *  @author praveeng
 */
package com.uniware.services.imports;

import com.unifier.core.api.validation.WsError;
import com.unifier.core.entity.ImportJob;
import com.unifier.core.entity.ImportJobType.ImportOptions;
import com.unifier.core.fileparser.Row;
import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.StringUtils;
import com.unifier.services.imports.ImportJobHandler;
import com.unifier.services.imports.ImportJobHandlerRequest;
import com.unifier.services.imports.ImportJobHandlerResponse;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.uniware.core.api.party.CreateVendorAgreementRequest;
import com.uniware.core.api.party.CreateVendorAgreementResponse;
import com.uniware.core.api.party.WsVendorAgreement;
import com.uniware.core.api.purchase.AddOrEditPurchaseOrderItemsRequest;
import com.uniware.core.api.purchase.CreatePurchaseOrderRequest;
import com.uniware.core.api.purchase.CreatePurchaseOrderResponse;
import com.uniware.core.api.purchase.WsPurchaseOrderItem;
import com.uniware.core.entity.Vendor;
import com.uniware.core.entity.VendorAgreement;
import com.uniware.services.purchase.IPurchaseService;
import com.uniware.services.vendor.IVendorService;

public class PurchaseOrderImportJobHandler implements ImportJobHandler {

    @Autowired
    IVendorService              vendorService;

    @Autowired
    IPurchaseService            purchaseService;

    private static final String VENDOR_CODE          = "vendor code";

    //create vendor agreement
    private static final String AGREEMENT_NAME       = "agreement name";
    private static final String AGREEMENT_START_TIME = "agreement start time as dd/MM/yyyy hh:mm:ss";
    private static final String AGREEMENT_END_TIME   = "agreement end time as dd/MM/yyyy hh:mm:ss";
    private static final String AGREEMENT_TEXT       = "agreement text";

    private static final String PRODUCT_CODE         = "product code";
    private static final String QUANTITY             = "quantity";

    private static final String UNIT_PRICE           = "unit price";
    private static final String MAX_RETAIL_PRICE     = "max retail price";
    private static final String DISCOUNT             = "discount";

    @Override
    public ImportJobHandlerResponse handleRow(ImportJobHandlerRequest request, ImportOptions importOption) throws Exception {
        ImportJobHandlerResponse response = new ImportJobHandlerResponse();
        List<Row> rows = request.getRows();
        String agreementName = getVendorAgreement(request, response);
        if (StringUtils.isNotBlank(agreementName)) {
            CreatePurchaseOrderRequest purchaseOrderRequest = new CreatePurchaseOrderRequest();
            purchaseOrderRequest.setUserId(request.getUserId());
            purchaseOrderRequest.setVendorCode(rows.get(0).getColumnValue(VENDOR_CODE));
            purchaseOrderRequest.setVendorAgreementName(agreementName);
            CreatePurchaseOrderResponse createPurchaseOrderResponse = purchaseService.createPurchaseOrder(purchaseOrderRequest);

            if (createPurchaseOrderResponse.isSuccessful()) {
                for (Row row : rows) {
                    AddOrEditPurchaseOrderItemsRequest purchaseOrderItemRequest = new AddOrEditPurchaseOrderItemsRequest();
                    purchaseOrderItemRequest.setPurchaseOrderCode(createPurchaseOrderResponse.getPurchaseOrderCode());
                    WsPurchaseOrderItem wsPurchaseOrderItem = new WsPurchaseOrderItem();
                    wsPurchaseOrderItem.setItemSKU(row.getColumnValue(PRODUCT_CODE));
                    try {
                        wsPurchaseOrderItem.setQuantity(Integer.parseInt(row.getColumnValue(QUANTITY)));
                    } catch (NumberFormatException e) {
                        response.addError(new WsError("Invalid value for " + QUANTITY));
                    }

                    String unitPrice = row.getColumnValue(UNIT_PRICE);
                    try {
                        wsPurchaseOrderItem.setUnitPrice(new BigDecimal(unitPrice));
                    } catch (NumberFormatException e) {
                        response.addError(new WsError("Invalid value for " + UNIT_PRICE));
                    }
                    String maxRetailPrice = row.getColumnValue(MAX_RETAIL_PRICE);
                    if (StringUtils.isNotBlank(maxRetailPrice)) {
                        try {
                            wsPurchaseOrderItem.setMaxRetailPrice(new BigDecimal(maxRetailPrice));
                        } catch (NumberFormatException e) {
                            response.addError(new WsError("Invalid value for " + MAX_RETAIL_PRICE));
                        }
                    }
                    String discount = row.getColumnValue(DISCOUNT);
                    if (StringUtils.isNotBlank(discount)) {
                        try {
                            wsPurchaseOrderItem.setDiscount(new BigDecimal(discount));
                        } catch (NumberFormatException e) {
                            response.addError(new WsError("Invalid value for " + DISCOUNT));
                        }
                    }
                    List<WsPurchaseOrderItem> wsPurchaseOrderItems = new ArrayList<WsPurchaseOrderItem>();
                    wsPurchaseOrderItems.add(wsPurchaseOrderItem);
                    purchaseOrderItemRequest.setPurchaseOrderItems(wsPurchaseOrderItems);
                    response.addResponse(purchaseService.addOrEditPurchaseOrderItems(purchaseOrderItemRequest));
                }
            } else {
                response.addResponse(createPurchaseOrderResponse);
            }

        }

        return response;
    }

    private String getVendorAgreement(ImportJobHandlerRequest request, ImportJobHandlerResponse response) {
        List<Row> rows = request.getRows();
        String agreementName = null;
        String vendorCode = rows.get(0).getColumnValue(VENDOR_CODE);
        if (StringUtils.isEmpty(vendorCode)) {
            response.addError(new WsError("Invalid value for " + VENDOR_CODE));
        } else {
            Vendor vendor = vendorService.getVendorByCode(vendorCode);
            if (vendor == null) {
                response.addError(new WsError("Invalid value for " + VENDOR_CODE));
            } else {
                agreementName = rows.get(0).getColumnValue(AGREEMENT_NAME);
                if (StringUtils.isBlank(agreementName)) {
                    response.addError(new WsError("Invalid value for " + AGREEMENT_NAME));
                } else {
                    VendorAgreement agreement = vendorService.getVendorAgreement(vendor.getId(), agreementName);
                    if (agreement == null) {
                        CreateVendorAgreementRequest agreementRequest = new CreateVendorAgreementRequest();
                        WsVendorAgreement wsVendorAgreement = agreementRequest.getVendorAgreement();
                        wsVendorAgreement.setVendorAgreementStatus(VendorAgreement.StatusCode.ACTIVE.name());
                        wsVendorAgreement.setName(rows.get(0).getColumnValue(AGREEMENT_NAME));
                        wsVendorAgreement.setAgreementText(rows.get(0).getColumnValue(AGREEMENT_TEXT));

                        String start = rows.get(0).getColumnValue(AGREEMENT_START_TIME);
                        if (StringUtils.isNotEmpty(start)) {
                            Date startTime = DateUtils.stringToDate(start, "dd/MM/yyyy hh:mm:ss");
                            if (startTime != null) {
                                wsVendorAgreement.setStartTime(startTime);
                            } else {
                                response.addError(new WsError("Invalid value for " + AGREEMENT_START_TIME));
                            }
                        }

                        String end = rows.get(0).getColumnValue(AGREEMENT_END_TIME);
                        if (StringUtils.isNotEmpty(end)) {
                            Date endTime = DateUtils.stringToDate(end, "dd/MM/yyyy hh:mm:ss");
                            if (endTime != null) {
                                wsVendorAgreement.setEndTime(endTime);
                            } else {
                                response.addError(new WsError("Invalid value for " + AGREEMENT_END_TIME));
                            }
                        }
                        CreateVendorAgreementResponse agreementResponse = vendorService.createAgreement(agreementRequest);
                        if (agreementResponse.isSuccessful()) {
                            return agreementResponse.getVendorAgreementDTO().getName();
                        }
                        response.addResponse(agreementResponse);
                    } else {
                        return agreement.getName();
                    }
                }
            }

        }
        return agreementName;
    }

    /* (non-Javadoc)
     * @see com.uniware.services.imports.ImportJobHandler#preProcessor(com.uniware.core.entity.ImportJob)
     */
    @Override
    public void preProcessor(ImportJob importJob) {

    }

    /* (non-Javadoc)
     * @see com.uniware.services.imports.ImportJobHandler#postProcessor(com.uniware.core.entity.ImportJob)
     */
    @Override
    public void postProcessor(ImportJob importJob) {

    }

}
