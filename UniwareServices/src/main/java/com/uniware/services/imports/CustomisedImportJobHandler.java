/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 23, 2012
 *  @author praveeng
 */
package com.uniware.services.imports;

import org.springframework.beans.factory.annotation.Autowired;

import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.entity.ImportJobType;
import com.uniware.services.configuration.ImportJobConfiguration;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.entity.ImportJob;
import com.unifier.core.entity.ImportJobType.ImportOptions;
import com.unifier.scraper.sl.runtime.ScraperScript;
import com.unifier.scraper.sl.runtime.ScriptExecutionContext;
import com.unifier.services.imports.ImportJobHandler;
import com.unifier.services.imports.ImportJobHandlerRequest;
import com.unifier.services.imports.ImportJobHandlerResponse;
import com.unifier.services.tenantprofile.service.ITenantProfileService;
import com.unifier.services.vo.TenantProfileVO;
import com.uniware.core.utils.UserContext;
import com.uniware.services.cache.ScriptVersionedCache;

public class CustomisedImportJobHandler implements ImportJobHandler {

    private ScraperScript processorScript;

    @Autowired
    private ITenantProfileService tenantProfileService;

    @Override
    public ImportJobHandlerResponse handleRow(ImportJobHandlerRequest request, ImportOptions importOption) {
        ImportJobHandlerResponse response = new ImportJobHandlerResponse();
        TenantProfileVO tenantProfile = tenantProfileService.getTenantProfileByCode(UserContext.current().getTenant().getCode());
        ScriptExecutionContext context = ScriptExecutionContext.current();
        context.addVariable("request", request);
        context.addVariable("tenantProfile", tenantProfile);
        try {
            processorScript.execute();
            response.setSuccessful(true);
        } finally {
            ScriptExecutionContext.destroy();
        }
        return response;
    }

    @Override
    public void preProcessor(ImportJob importJob) {
        ImportJobType importJobType = ConfigurationManager.getInstance().getConfiguration(ImportJobConfiguration.class).getImportJobTypeByName(importJob.getImportJobTypeName());
        processorScript = CacheManager.getInstance().getCache(ScriptVersionedCache.class).getScriptByName(importJobType.getProcessorScriptName(), true);
    }

    @Override
    public void postProcessor(ImportJob importJob) {
    }

}