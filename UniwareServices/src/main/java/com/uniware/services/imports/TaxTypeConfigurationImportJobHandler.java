/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 *  @version     1.0, 01-Jun-2012
 *  @author praveeng
 */
package com.uniware.services.imports;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.unifier.core.api.validation.WsError;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.entity.ImportJob;
import com.unifier.core.entity.ImportJobType.ImportOptions;
import com.unifier.core.fileparser.Row;
import com.unifier.core.utils.StringUtils;
import com.unifier.services.imports.ImportJobHandler;
import com.unifier.services.imports.ImportJobHandlerRequest;
import com.unifier.services.imports.ImportJobHandlerResponse;
import com.uniware.core.api.tax.CreateEditTaxTypesConfigurationRequest;
import com.uniware.core.api.tax.WsTaxTypeConfiguration;
import com.uniware.core.api.tax.WsTaxTypeConfiguration.WSTaxTypeConfigurationRange;
import com.uniware.core.configuration.TaxConfiguration;
import com.uniware.core.entity.TaxType;
import com.uniware.services.tax.ITaxTypeService;

public class TaxTypeConfigurationImportJobHandler implements ImportJobHandler {

    private static final String TAX_TYPE_CODE     = "tax type code";
    private static final String STATE_CODE        = "state code";
    private static final String VAT               = "vat";
    private static final String CST               = "cst";
    private static final String CST_WITH_CFORM    = "cst with cform";
    private static final String ADDITIONAL_TAX    = "additional tax";
    private static final String START_PRICE       = "start price";
    private static final String END_PRICE         = "end price";
    private static final String CENTRAL_GST       = "central gst";
    private static final String STATE_GST         = "state gst";
    private static final String UT_GST            = "union terriroty gst";
    private static final String INTEGRATED_GST    = "integrated gst";
    private static final String COMPENSATION_CESS = "compensation cess";
    private static final String IS_GST            = "is gst";

    @Autowired
    private ITaxTypeService     taxTypeService;

    @Override
    public ImportJobHandlerResponse handleRow(ImportJobHandlerRequest importRequest, ImportOptions importOption) throws Exception {
        ImportJobHandlerResponse response = new ImportJobHandlerResponse();
        Map<String, Map<String, WsTaxTypeConfiguration>> groupedTaxTypeConfigMap = new HashMap<>();
        Row firstRow = importRequest.getRows().get(0);
        boolean isGst = StringUtils.isNotBlank(firstRow.getColumnValue(IS_GST)) && StringUtils.parseBoolean(firstRow.getColumnValue(IS_GST));
        for (Row row : importRequest.getRows()) {
            String key = getKey(row);
            Map<String, WsTaxTypeConfiguration> taxTypeConfigs = groupedTaxTypeConfigMap.get(key);
            if (taxTypeConfigs == null) {
                taxTypeConfigs = new HashMap<>();
                groupedTaxTypeConfigMap.put(key, taxTypeConfigs);
            }
            WsTaxTypeConfiguration taxTypeConfig = prepareWsTaxTypeConfiguration(row, response);
            WsTaxTypeConfiguration wsTaxTypeConfig = taxTypeConfigs.get(taxTypeConfig.getStateCode());
            if (wsTaxTypeConfig == null) {
                taxTypeConfigs.put(taxTypeConfig.getStateCode(), taxTypeConfig);
            } else {
                wsTaxTypeConfig.getRanges().addAll(taxTypeConfig.getRanges());
            }
            /*else {
                appendRangesToConfiguration(configuration, row, response);
              }*/
        }
        if (!response.hasErrors()) {
            List<WsTaxTypeConfiguration> configurations = new ArrayList<>();
            groupedTaxTypeConfigMap.forEach((taxType, stateToConfig) -> {
                stateToConfig.forEach((state, config) -> {
                    configurations.add(config);
                });
                response.addResponse(createEditTaxTypeConfiguration(configurations, taxType, isGst));
            });
        }
        return response;
    }

    private String getKey(Row row) {
        String key = row.getColumnValue(TAX_TYPE_CODE);
        return key;
    }

    private ImportJobHandlerResponse createEditTaxTypeConfiguration(List<WsTaxTypeConfiguration> configurations, String taxTypeCode, boolean isGst) {

        ImportJobHandlerResponse response = new ImportJobHandlerResponse();
        CreateEditTaxTypesConfigurationRequest request = new CreateEditTaxTypesConfigurationRequest();
        request.setTaxTypeCode(taxTypeCode);
        request.setTaxTypeConfigurations(configurations);
        request.setGst(isGst);
        if (!response.hasErrors()) {
            TaxType taxType = taxTypeService.getTaxTypeByCode(request.getTaxTypeCode());
            if (taxType != null) {
                request.setEdit(true);
            }
            response.addResponse(taxTypeService.createTaxTypeConfiguration(request));
        }
        return response;
    }

    private void appendRangesToConfiguration(WsTaxTypeConfiguration configuration, Row row, ImportJobHandlerResponse response) {
        if (StringUtils.isBlank(row.getColumnValue(START_PRICE)) || StringUtils.isBlank(row.getColumnValue(END_PRICE))) {
            response.addError(new WsError("For price range configuration need to specify start price and end price. Row number [" + row.getLineNo() + "]"));
            return;
        }
        WSTaxTypeConfigurationRange wsTaxTypeConfigurationRange = prepareWsTaxTypeConfigurationRange(row, response);
        if (!response.hasErrors()) {
            configuration.getRanges().add(wsTaxTypeConfigurationRange);
        }
    }

    private WsTaxTypeConfiguration prepareWsTaxTypeConfiguration(Row row, ImportJobHandlerResponse response) {
        WsTaxTypeConfiguration wsTaxTypeConfiguration = new WsTaxTypeConfiguration();
        wsTaxTypeConfiguration.setTaxTypeCode(row.getColumnValue(TAX_TYPE_CODE));
        wsTaxTypeConfiguration.setStateCode(row.getColumnValue(STATE_CODE));
        List<WSTaxTypeConfigurationRange> ranges = new ArrayList<>();
        wsTaxTypeConfiguration.setRanges(ranges);
        WSTaxTypeConfigurationRange wsTaxTypeConfigurationRange = prepareWsTaxTypeConfigurationRange(row, response);
        ranges.add(wsTaxTypeConfigurationRange);
        return wsTaxTypeConfiguration;
    }

    private WSTaxTypeConfigurationRange prepareWsTaxTypeConfigurationRange(Row row, ImportJobHandlerResponse response) {
        WSTaxTypeConfigurationRange wsTaxTypeConfigurationRange = new WSTaxTypeConfigurationRange();
        if (StringUtils.isNotBlank(row.getColumnValue(CST))) {
            try {
                wsTaxTypeConfigurationRange.setCst(new BigDecimal(row.getColumnValue(CST)));
            } catch (NumberFormatException e) {
                response.addError(new WsError("Invalid value for " + CST + "Row number [" + row.getLineNo() + "]"));
            }
        }

        if (StringUtils.isNotBlank(row.getColumnValue(CST_WITH_CFORM))) {
            try {
                wsTaxTypeConfigurationRange.setCstWithCForm(new BigDecimal(row.getColumnValue(CST_WITH_CFORM)));
            } catch (NumberFormatException e) {
                response.addError(new WsError("Invalid value for " + CST_WITH_CFORM + "Row number [" + row.getLineNo() + "]"));
            }
        }

        if (StringUtils.isNotBlank(row.getColumnValue(VAT))) {
            try {
                wsTaxTypeConfigurationRange.setVat(new BigDecimal(row.getColumnValue(VAT)));
            } catch (NumberFormatException e) {
                response.addError(new WsError("Invalid value for " + VAT + "Row number [" + row.getLineNo() + "]"));
            }
        }

        if (StringUtils.isNotBlank(row.getColumnValue(ADDITIONAL_TAX))) {
            try {
                wsTaxTypeConfigurationRange.setAdditionalTax(new BigDecimal(row.getColumnValue(ADDITIONAL_TAX).trim()));
            } catch (NumberFormatException e) {
                response.addError(new WsError("Invalid value for " + ADDITIONAL_TAX + "Row number [" + row.getLineNo() + "]"));
            }
        }
        if (StringUtils.isNotBlank(row.getColumnValue(CENTRAL_GST))) {
            try {
                wsTaxTypeConfigurationRange.setCentralGst(new BigDecimal(row.getColumnValue(CENTRAL_GST)));
            } catch (NumberFormatException e) {
                response.addError(new WsError("Invalid value for " + CENTRAL_GST + "Row number [" + row.getLineNo() + "]"));
            }
        }
        if (StringUtils.isNotBlank(row.getColumnValue(STATE_GST))) {
            try {
                wsTaxTypeConfigurationRange.setStateGst(new BigDecimal(row.getColumnValue(STATE_GST)));
            } catch (NumberFormatException e) {
                response.addError(new WsError("Invalid value for " + STATE_GST + "Row number [" + row.getLineNo() + "]"));
            }
        }
        if (StringUtils.isNotBlank(row.getColumnValue(INTEGRATED_GST))) {
            try {
                wsTaxTypeConfigurationRange.setIntegratedGst(new BigDecimal(row.getColumnValue(INTEGRATED_GST)));
            } catch (NumberFormatException e) {
                response.addError(new WsError("Invalid value for " + INTEGRATED_GST + "Row number [" + row.getLineNo() + "]"));
            }
        }
        if (StringUtils.isNotBlank(row.getColumnValue(UT_GST))) {
            try {
                wsTaxTypeConfigurationRange.setUnionTerritoryGst(new BigDecimal(row.getColumnValue(UT_GST)));
            } catch (NumberFormatException e) {
                response.addError(new WsError("Invalid value for " + UT_GST + "Row number [" + row.getLineNo() + "]"));
            }
        }
        if (StringUtils.isNotBlank(row.getColumnValue(COMPENSATION_CESS))) {
            try {
                wsTaxTypeConfigurationRange.setCompensationCess(new BigDecimal(row.getColumnValue(COMPENSATION_CESS)));
            } catch (NumberFormatException e) {
                response.addError(new WsError("Invalid value for " + COMPENSATION_CESS + "Row number [" + row.getLineNo() + "]"));
            }
        }
        if (StringUtils.isNotBlank(row.getColumnValue(START_PRICE))) {
            try {
                wsTaxTypeConfigurationRange.setStartPrice(new BigDecimal(row.getColumnValue(START_PRICE).trim()));
            } catch (NumberFormatException e) {
                response.addError(new WsError("Invalid value for " + START_PRICE + "Row number [" + row.getLineNo() + "]"));
            }
        }
        if (StringUtils.isNotBlank(row.getColumnValue(END_PRICE))) {
            try {
                wsTaxTypeConfigurationRange.setEndPrice(new BigDecimal(row.getColumnValue(END_PRICE).trim()));
            } catch (NumberFormatException e) {
                response.addError(new WsError("Invalid value for " + END_PRICE + "Row number [" + row.getLineNo() + "]"));
            }
        }
        return wsTaxTypeConfigurationRange;
    }

    @Override
    public void preProcessor(ImportJob importJob) {

    }

    @Override
    public void postProcessor(ImportJob importJob) {
        ConfigurationManager.getInstance().markConfigurationDirty(TaxConfiguration.class);
    }
}
