/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 23, 2012
 *  @author praveeng
 */
package com.uniware.services.imports;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.unifier.core.api.validation.WsError;
import com.unifier.core.entity.ImportJob;
import com.unifier.core.entity.ImportJobType.ImportOptions;
import com.unifier.core.entity.User;
import com.unifier.core.fileparser.Row;
import com.unifier.services.imports.ImportJobHandler;
import com.unifier.services.imports.ImportJobHandlerRequest;
import com.unifier.services.imports.ImportJobHandlerResponse;
import com.unifier.services.users.IUsersService;
import com.uniware.core.api.putaway.GetPutawayRequest;
import com.uniware.core.api.putaway.PutawayDTO;
import com.uniware.core.api.returns.AddDirectReturnedSaleOrderItemsToPutawayRequest;
import com.uniware.core.api.reversepickup.AddReversePickupSaleOrderItemsToPutawayRequest.WsSaleOrderItem;
import com.uniware.core.api.reversepickup.AddReversePickupSaleOrderItemsToPutawayRequest.WsSaleOrderItem.StatusCode;
import com.uniware.core.entity.SaleOrder;
import com.uniware.core.entity.SaleOrderItem;
import com.uniware.services.putaway.IPutawayService;
import com.uniware.services.saleorder.ISaleOrderService;

public class SnapdealFCDirectReturnSaleOrderItemsImportJobHandler implements ImportJobHandler {
    private static final String PUTAWAY_CODE  = "Putaway Code";
    private static final String SUBORDER_CODE = "Suborder Code";
    private static final String IL_NUMBER     = "Il Number";
    private static final String STATUS        = "Status";
    private static final String REASON        = "Reason";

    @Autowired
    private IPutawayService     putawayService;

    @Autowired
    private ISaleOrderService   saleOrderService;

    @Autowired
    private IUsersService       usersService;

    @Override
    public ImportJobHandlerResponse handleRow(ImportJobHandlerRequest request, ImportOptions importOption) throws Exception {
        return handleReturnedOrderItems(request);
    }

    private ImportJobHandlerResponse handleReturnedOrderItems(ImportJobHandlerRequest request) {
        ImportJobHandlerResponse response = new ImportJobHandlerResponse();
        Row row = request.getRow();
        GetPutawayRequest getPutawayRequest = new GetPutawayRequest();
        getPutawayRequest.setCode(row.getColumnValue(PUTAWAY_CODE));
        PutawayDTO putawayDTO = putawayService.getPutaway(getPutawayRequest).getPutawayDTO();
        if (putawayDTO == null) {
            response.addError(new WsError("Invalid putaway code " + row.getColumnValue(PUTAWAY_CODE)));
        }

        SaleOrder saleOrder = null;
        if (!response.hasErrors()) {
            saleOrder = saleOrderService.getSaleOrderByAdditionalInfo(row.getColumnValue(IL_NUMBER));
            if (saleOrder == null) {
                response.addError(new WsError("Invalid Il Number " + row.getColumnValue(IL_NUMBER)));
            }
        }

        SaleOrderItem saleOrderItem = null;
        if (!response.hasErrors()) {
            saleOrderItem = saleOrderService.getSaleOrderItemByCode(saleOrder.getCode(), row.getColumnValue(SUBORDER_CODE));
            if (saleOrderItem == null) {
                response.addError(new WsError("Invalid Suborder Code and Il Number Combination" + row.getColumnValue(SUBORDER_CODE) + "," + row.getColumnValue(IL_NUMBER)));
            }
        }

        StatusCode statusCode = null;
        if (!response.hasErrors()) {
            try {
                statusCode = StatusCode.valueOf(row.getColumnValue(STATUS));
            } catch (Exception e) {
                response.addError(new WsError("Invalid Status Code " + row.getColumnValue(STATUS)));
            }
        }

        if (!response.hasErrors()) {
            User user = usersService.getUserByUsername(putawayDTO.getUsername());
            WsSaleOrderItem wsSaleOrderItem = new WsSaleOrderItem();
            wsSaleOrderItem.setCode(saleOrderItem.getCode());
            wsSaleOrderItem.setStatus(statusCode.name());
            List<WsSaleOrderItem> returnedSaleOrderItems = new ArrayList<WsSaleOrderItem>();
            returnedSaleOrderItems.add(wsSaleOrderItem);
            AddDirectReturnedSaleOrderItemsToPutawayRequest addDirectReturnedSaleOrderItemsToPutawayRequest = new AddDirectReturnedSaleOrderItemsToPutawayRequest();
            addDirectReturnedSaleOrderItemsToPutawayRequest.setPutawayCode(putawayDTO.getCode());
            addDirectReturnedSaleOrderItemsToPutawayRequest.setSaleOrderCode(saleOrder.getCode());
            addDirectReturnedSaleOrderItemsToPutawayRequest.setReturnedSaleOrderItems(returnedSaleOrderItems);
            addDirectReturnedSaleOrderItemsToPutawayRequest.setReason(row.getColumnValue(REASON));
            addDirectReturnedSaleOrderItemsToPutawayRequest.setUserId(user.getId());
            response.addResponse(putawayService.addDirectReturnsSaleOrderItemsToPutaway(
                    addDirectReturnedSaleOrderItemsToPutawayRequest));
        }
        return response;
    }

    @Override
    public void preProcessor(ImportJob importJob) {
    }

    @Override
    public void postProcessor(ImportJob importJob) {
    }

}
