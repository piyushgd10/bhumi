/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Nov 02, 2015
 *  @author akshay
 */
package com.uniware.services.imports;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.unifier.core.api.validation.WsError;
import com.unifier.core.entity.ImportJob;
import com.unifier.core.entity.ImportJobType.ImportOptions;
import com.unifier.core.fileparser.Row;
import com.unifier.core.utils.StringUtils;
import com.unifier.services.imports.ImportJobHandler;
import com.unifier.services.imports.ImportJobHandlerRequest;
import com.unifier.services.imports.ImportJobHandlerResponse;
import com.uniware.core.api.prices.EditChannelItemTypePriceRequest;
import com.uniware.core.api.prices.EditChannelItemTypePriceResponse;
import com.uniware.core.api.prices.PushAllPricesOnChannelsAsyncResponse;
import com.uniware.core.api.prices.PushAllPricesOnAllChannelsRequest;
import com.uniware.core.entity.Channel;
import com.uniware.services.channel.IChannelService;
import com.uniware.services.pricing.IPricingBridge;
import com.uniware.services.pricing.IPricingService;

/**
 * Handler for importing prices for products accros channels
 */

public class ChannelItemTypePriceImportJobHandler implements ImportJobHandler {

    private static final Logger LOG                = LoggerFactory.getLogger(ChannelItemTypePriceImportJobHandler.class);

    private static final String CHANNEL_NAME       = "Channel Name";
    private static final String CHANNEL_PRODUCT_ID = "Channel Product Id";
    private static final String SELLING_PRICE      = "Selling Price";
    private static final String MRP                = "Max Retail Price";
    private static final String MSP                = "Min Selling Price";
    private static final String CURRENCY_CODE      = "Currency Code";
    private static final String DEFAULT_CURRENCY   = "INR";
    private static final String DIRTY              = "Dirty";

    @Autowired
    private IChannelService     channelService;

    @Autowired
    @Qualifier("uniwarePricingService")
    private IPricingService     uniwarePricingService;

    @Autowired
    private IPricingBridge      pricingBridge;

    @Override
    public ImportJobHandlerResponse handleRow(ImportJobHandlerRequest request, ImportOptions importOption) throws Exception {
        ImportJobHandlerResponse response = new ImportJobHandlerResponse();
        Row row = request.getRow();
        Channel channel = channelService.getChannelByName(row.getColumnValue(CHANNEL_NAME));
        if (channel == null) {
            response.addError(new WsError("Invalid Channel Name " + row.getColumnValue(CHANNEL_NAME)));
            return response;
        }
        String sellingPrice = row.getColumnValue(SELLING_PRICE);
        String msp = row.getColumnValue(MSP);
        String mrp = row.getColumnValue(MRP);
        // Validate if price values are in valid format
        validatePrices(response, row, sellingPrice, msp, mrp);
        if (!response.hasErrors()) {
            EditChannelItemTypePriceRequest editPriceRequest = new EditChannelItemTypePriceRequest();
            editPriceRequest.setChannelCode(channel.getCode());
            editPriceRequest.setChannelProductId(row.getColumnValue(CHANNEL_PRODUCT_ID));
            editPriceRequest.setIsForceEdit(true);
            if (StringUtils.isNotBlank(sellingPrice)) {
                editPriceRequest.setSellingPrice(new BigDecimal(sellingPrice));
            }
            if (StringUtils.isNotBlank(mrp)) {
                editPriceRequest.setMrp(new BigDecimal(mrp));
            }
            if (StringUtils.isNotBlank(msp)) {
                editPriceRequest.setMsp(new BigDecimal(msp));
            }
            if (StringUtils.isNotBlank(row.getColumnValue(CURRENCY_CODE))) {
                editPriceRequest.setCurrencyCode(row.getColumnValue(CURRENCY_CODE));
            } else {
                editPriceRequest.setCurrencyCode(DEFAULT_CURRENCY);
            }
            if (StringUtils.isNotBlank(row.getColumnValue(DIRTY))) {
                editPriceRequest.setForceMarkDirty("1".equals(row.getColumnValue(DIRTY)));
            }
            EditChannelItemTypePriceResponse editResponse = uniwarePricingService.editChannelItemTypePrice(editPriceRequest);
            response.addResponse(editResponse);
        }
        return response;
    }

    private void validatePrices(ImportJobHandlerResponse response, Row row, String sellingPrice, String msp, String mrp) {
        if (StringUtils.isBlank(sellingPrice) && StringUtils.isBlank(msp) && StringUtils.isBlank(mrp)) {
            response.addError(new WsError("Either of Selling Price / MRP / MSP is mandatory"));
        }
        if (StringUtils.isNotBlank(row.getColumnValue(SELLING_PRICE))) {
            try {
                new BigDecimal(row.getColumnValue(SELLING_PRICE));
            } catch (Exception e) {
                response.addError(new WsError("Invalid value for " + SELLING_PRICE + " " + sellingPrice));
            }
        }
        if (StringUtils.isNotBlank(row.getColumnValue(MSP))) {
            try {
                new BigDecimal(row.getColumnValue(MSP));
            } catch (Exception e) {
                response.addError(new WsError("Invalid value for " + MSP + " " + msp));
            }
        }
        if (StringUtils.isNotBlank(row.getColumnValue(MRP))) {
            try {
                new BigDecimal(row.getColumnValue(MRP));
            } catch (Exception e) {
                response.addError(new WsError("Invalid value for " + MRP + " " + mrp));
            }
        }
    }

    @Override
    public void preProcessor(ImportJob importJob) {
    }

    @Override
    public void postProcessor(ImportJob importJob) {
        PushAllPricesOnAllChannelsRequest request = new PushAllPricesOnAllChannelsRequest();
        PushAllPricesOnChannelsAsyncResponse response = pricingBridge.pushAllPricesOnAllChannels(request);
        if (response.isSuccessful()) {
            LOG.info("Price sync scheduled after successful import all channels");
        } else {
            LOG.warn("Error while scheduling sync for all channels : error : {}", response.getErrors());
        }
    }
}
