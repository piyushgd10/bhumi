/*
 *  Copyright 2013 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jan 23, 2013
 *  @author praveeng
 */
package com.uniware.services.imports;

import org.springframework.beans.factory.annotation.Autowired;

import com.unifier.core.entity.ImportJob;
import com.unifier.core.entity.ImportJobType.ImportOptions;
import com.unifier.core.fileparser.Row;
import com.unifier.services.imports.ImportJobHandler;
import com.unifier.services.imports.ImportJobHandlerRequest;
import com.unifier.services.imports.ImportJobHandlerResponse;
import com.uniware.core.api.returns.AssignShippingProviderToReversePickupRequest;
import com.uniware.services.returns.IReturnsService;

public class AssignProviderToReversePickupImportJobHandler implements ImportJobHandler {

    private static final String REVERSE_PICKUP_CODE    = "reverse pickup code";
    private static final String SHIPPING_PROVIDER_CODE = "provider code";
    private static final String TRACKING_NUMBER        = "tracking number";

    @Autowired
    private IReturnsService     returnsService;

    @Override
    public ImportJobHandlerResponse handleRow(ImportJobHandlerRequest request, ImportOptions importOption) throws Exception {
        ImportJobHandlerResponse response = new ImportJobHandlerResponse();
        Row row = request.getRow();

        AssignShippingProviderToReversePickupRequest assignProviderRequest = new AssignShippingProviderToReversePickupRequest();
        assignProviderRequest.setReversePickupCode(row.getColumnValue(REVERSE_PICKUP_CODE));
        assignProviderRequest.setShippingProviderCode(row.getColumnValue(SHIPPING_PROVIDER_CODE));
        assignProviderRequest.setTrackingNumber(row.getColumnValue(TRACKING_NUMBER));
        response.addResponse(returnsService.assignShippingProviderToReversePickup(assignProviderRequest));
        return response;
    }

    @Override
    public void preProcessor(ImportJob importJob) {
        // TODO Auto-generated method stub

    }

    @Override
    public void postProcessor(ImportJob importJob) {
        // TODO Auto-generated method stub

    }

}
