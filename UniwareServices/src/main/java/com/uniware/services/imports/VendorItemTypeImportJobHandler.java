/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 23, 2012
 *  @author praveeng
 */
package com.uniware.services.imports;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;

import com.unifier.core.api.validation.WsError;
import com.unifier.core.entity.ImportJob;
import com.unifier.core.entity.ImportJobType;
import com.unifier.core.entity.ImportJobType.ImportOptions;
import com.unifier.core.entity.User;
import com.unifier.core.fileparser.Row;
import com.unifier.core.utils.StringUtils;
import com.unifier.services.imports.ImportJobHandler;
import com.unifier.services.imports.ImportJobHandlerRequest;
import com.unifier.services.imports.ImportJobHandlerResponse;
import com.unifier.services.users.IUsersService;
import com.unifier.services.utils.CustomFieldUtils;
import com.uniware.core.api.party.CreateVendorItemTypeRequest;
import com.uniware.core.api.party.EditVendorItemTypeRequest;
import com.uniware.core.api.party.WsVendorItemType;
import com.uniware.core.entity.VendorItemType;
import com.uniware.services.vendor.IVendorService;

public class VendorItemTypeImportJobHandler implements ImportJobHandler {
    private static final String VENDOR_CODE        = "vendor code";
    private static final String ITEM_TYPE_SKU_CODE = "product code";
    private static final String VENDOR_SKU_CODE    = "vendor skucode";
    private static final String INVENTORY          = "inventory";
    private static final String UNIT_PRICE         = "vendor price";
    private static final String PRIORITY           = "priority";
    private static final String ENABLED            = "enabled";

    @Autowired
    private IVendorService      vendorService;

    @Autowired
    private IUsersService       userService;

    @Override
    public ImportJobHandlerResponse handleRow(ImportJobHandlerRequest request, ImportOptions importOption) throws Exception {
        if (ImportJobType.ImportOptions.CREATE_NEW_AND_UPDATE_EXISTING == importOption) {
            String vendorCode = request.getRow().getColumnValue(VENDOR_CODE);
            String itemTypeSkuCode = request.getRow().getColumnValue(ITEM_TYPE_SKU_CODE);
            VendorItemType itemType = vendorService.getVendorItemType(vendorCode, itemTypeSkuCode);
            if (itemType == null) {
                return createVendorItemType(request);
            } else {
                return editVendorItemType(request);
            }
        } else if (ImportJobType.ImportOptions.CREATE_NEW == importOption) {
            return createVendorItemType(request);
        } else if (ImportJobType.ImportOptions.UPDATE_EXISTING == importOption) {
            return editVendorItemType(request);
        }
        return null;
    }

    private ImportJobHandlerResponse editVendorItemType(ImportJobHandlerRequest request) {
        ImportJobHandlerResponse response = new ImportJobHandlerResponse();
        Row row = request.getRow();

        WsVendorItemType wsVendorItemType = prepareWsVendorItemType(response, row);
        User user = userService.getUserWithDetailsById(request.getUserId());
        if (user != null && user.getVendor() != null) {
            wsVendorItemType.setVendorCode(user.getVendor().getCode());
        }
        if (!response.hasErrors()) {
            EditVendorItemTypeRequest editVendorItemTypeRequest = new EditVendorItemTypeRequest();
            editVendorItemTypeRequest.setVendorItemType(wsVendorItemType);
            editVendorItemTypeRequest.setUserId(request.getUserId());
            editVendorItemTypeRequest.setActionType("VendorItemTypeImport");
            response.addResponse(vendorService.editVendorItemType(editVendorItemTypeRequest));
        }
        return response;
    }

    private WsVendorItemType prepareWsVendorItemType(ImportJobHandlerResponse response, Row row) {
        WsVendorItemType wsVendorItemType = new WsVendorItemType();

        String unitPrice = row.getColumnValue(UNIT_PRICE);
        if (StringUtils.isNotEmpty(unitPrice)) {
            try {
                wsVendorItemType.setUnitPrice(new BigDecimal(unitPrice));
            } catch (NumberFormatException e) {
                response.addError(new WsError("Invalid value for 'unit price'"));
            }
        }

        String priority = row.getColumnValue(PRIORITY);

        if (StringUtils.isNotBlank(priority)) {
            try {
                wsVendorItemType.setPriority(Integer.parseInt(priority));
            } catch (NumberFormatException e) {
                response.addError(new WsError("Invalid value for 'priority'"));
            }
        }
        String strInventory = row.getColumnValue(INVENTORY);
        if (StringUtils.isNotBlank(strInventory)) {
            try {
                wsVendorItemType.setInventory(Integer.parseInt(strInventory));
            } catch (NumberFormatException e) {
                response.addError(new WsError("Invalid value for 'inventory'"));
            }
        }

        String enabled = row.getColumnValue(ENABLED);
        if (StringUtils.isNotBlank(enabled)) {
            wsVendorItemType.setEnabled(StringUtils.parseBoolean(enabled));
        }

        wsVendorItemType.setVendorCode(row.getColumnValue(VENDOR_CODE));
        wsVendorItemType.setVendorSkuCode(row.getColumnValue(VENDOR_SKU_CODE));
        wsVendorItemType.setItemTypeSkuCode(row.getColumnValue(ITEM_TYPE_SKU_CODE));
        wsVendorItemType.setCustomFieldValues(CustomFieldUtils.getCustomFieldValues(response, VendorItemType.class.getName(), row));
        return wsVendorItemType;
    }

    private ImportJobHandlerResponse createVendorItemType(ImportJobHandlerRequest request) {
        ImportJobHandlerResponse response = new ImportJobHandlerResponse();
        Row row = request.getRow();
        WsVendorItemType wsVendorItemType = prepareWsVendorItemType(response, row);
        if (!response.hasErrors()) {
            CreateVendorItemTypeRequest vendorItemTypeRequest = new CreateVendorItemTypeRequest();
            vendorItemTypeRequest.setVendorItemType(wsVendorItemType);
            response.addResponse(vendorService.createVendorItemType(vendorItemTypeRequest));
        }
        return response;
    }

    @Override
    public void preProcessor(ImportJob importJob) {
    }

    @Override
    public void postProcessor(ImportJob importJob) {
    }

}
