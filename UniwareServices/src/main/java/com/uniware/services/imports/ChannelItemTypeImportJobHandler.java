/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 23, 2012
 *  @author praveeng
 */
package com.uniware.services.imports;

import com.uniware.core.entity.ChannelItemType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.unifier.core.api.validation.WsError;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.entity.ImportJob;
import com.unifier.core.entity.ImportJobType.ImportOptions;
import com.unifier.core.fileparser.Row;
import com.unifier.core.utils.NumberUtils;
import com.unifier.core.utils.StringUtils;
import com.unifier.services.imports.ImportJobHandler;
import com.unifier.services.imports.ImportJobHandlerRequest;
import com.unifier.services.imports.ImportJobHandlerResponse;
import com.uniware.core.api.channel.CreateChannelItemTypeRequest;
import com.uniware.core.api.channel.CreateChannelItemTypeResponse;
import com.uniware.core.api.channel.IgnoreChannelItemTypeRequest;
import com.uniware.core.api.channel.WsChannelItemType;
import com.uniware.core.entity.Channel;
import com.uniware.services.channel.IChannelCatalogService;
import com.uniware.services.channel.IChannelService;
import com.uniware.services.configuration.ImportJobConfiguration;
import com.uniware.services.configuration.data.manager.ProductConfiguration;
import com.uniware.services.pricing.IPricingService;

public class ChannelItemTypeImportJobHandler implements ImportJobHandler {
    private static final String    CHANNEL_NAME           = "Channel Name";
    private static final String    ITEM_TYPE_SKU_CODE     = "Uniware SKU Code";
    private static final String    SELLER_SKU_CODE        = "Seller SKU Code";
    private static final String    CHANNEL_PRODUCT_ID     = "Channel Product Id";
    private static final String    IGNORED                = "IGNORE";
    private static final String    BLOCKED_INVENTORY      = "Blocked Inventory";
    private static final String    INVENTORY              = "Inventory";
    private static final String    LIVE                   = "live";
    private static final String    DISABLED               = "disabled";
    private static final String    SELLING_PRICE          = "Selling Price";
    private static final String    MRP                    = "Max Retail Price";
    private static final String    MSP                    = "Min Selling Price";
    private static final String    TRANSFER_PRICE         = "Transfer Price";

    @Autowired
    private IChannelService        channelService;

    @Autowired
    private IChannelCatalogService channelCatalogService;

    @Autowired
    @Qualifier("uniwarePricingService")
    private IPricingService        uniwarePricingService;

    private static final String    priceImportJobTypeName = "Price Import";

    @Override
    public ImportJobHandlerResponse handleRow(ImportJobHandlerRequest request, ImportOptions importOption) throws Exception {
        return addChannelItemType(request, importOption);
    }

    private ImportJobHandlerResponse addChannelItemType(ImportJobHandlerRequest request, ImportOptions importOption) throws Exception {
        ImportJobHandlerResponse response = new ImportJobHandlerResponse();
        Row row = request.getRow();
        Channel channel = channelService.getChannelByName(row.getColumnValue(CHANNEL_NAME));
        if (channel == null) {
            response.addError(new WsError("Invalid Channel Name " + row.getColumnValue(CHANNEL_NAME)));
        }
        if (StringUtils.isBlank(row.getColumnValue(IGNORED)) || !"1".equals(row.getColumnValue(IGNORED))) {
            boolean productMgmtSwitchedOff = ConfigurationManager.getInstance().getConfiguration(ProductConfiguration.class).isProductManagementSwitchedOff();
            if (!productMgmtSwitchedOff) {
                if (StringUtils.isNotEmpty(row.getColumnValue(INVENTORY))) {
                    response.addError(new WsError("Inventory update is not allowed for given system configuration"));
                } else if (StringUtils.isEmpty(row.getColumnValue(SELLER_SKU_CODE)) || StringUtils.isEmpty(row.getColumnValue(ITEM_TYPE_SKU_CODE))) {
                    response.addError(new WsError(SELLER_SKU_CODE + " and " + ITEM_TYPE_SKU_CODE + " are mandatory"));
                }
            } else {
                if (StringUtils.isEmpty(row.getColumnValue(INVENTORY))) {
                    response.addError(new WsError("Inventory cannot be left blank"));
                }
            }
        }

        if (!response.hasErrors()) {
            if (StringUtils.isNotBlank(row.getColumnValue(IGNORED)) && "1".equals(row.getColumnValue(IGNORED))) {
                IgnoreChannelItemTypeRequest ignoreChannelItemTypeRequest = new IgnoreChannelItemTypeRequest();
                ignoreChannelItemTypeRequest.setChannelCode(channel.getCode());
                ignoreChannelItemTypeRequest.setChannelProductId(row.getColumnValue(CHANNEL_PRODUCT_ID));
                response.addResponse(channelCatalogService.ignoreChannelItemType(ignoreChannelItemTypeRequest));
            } else {
                CreateChannelItemTypeRequest channelItemTypeRequest = new CreateChannelItemTypeRequest();
                WsChannelItemType channelItemType = new WsChannelItemType();
                channelItemType.setChannelCode(channel.getCode());
                channelItemType.setChannelProductId(row.getColumnValue(CHANNEL_PRODUCT_ID));
                channelItemType.setSkuCode(row.getColumnValue(ITEM_TYPE_SKU_CODE));
                channelItemType.setSellerSkuCode(row.getColumnValue(SELLER_SKU_CODE));
                if (StringUtils.isNotBlank(row.getColumnValue(LIVE))) {
                    channelItemType.setLive("1".equals(row.getColumnValue(LIVE)));
                }
                if (StringUtils.isNotBlank(row.getColumnValue(DISABLED))) {
                    channelItemType.setDisabled("1".equals(row.getColumnValue(DISABLED)));
                }
                if (StringUtils.isNotBlank(row.getColumnValue(BLOCKED_INVENTORY))) {
                    channelItemType.setBlockedInventory(Integer.parseInt(row.getColumnValue(BLOCKED_INVENTORY)));
                }
                if (StringUtils.isNotBlank(row.getColumnValue(INVENTORY))) {
                    channelItemType.setCurrentInventoryOnChannel(Integer.parseInt(row.getColumnValue(INVENTORY)));
                }
                if (StringUtils.isNotBlank(row.getColumnValue(TRANSFER_PRICE))) {
                    channelItemType.setTransferPrice(NumberUtils.stringToBigDecimal(row.getColumnValue(TRANSFER_PRICE)));
                }
                channelItemTypeRequest.setChannelItemType(channelItemType);
                channelItemTypeRequest.setSyncId(ChannelItemType.SyncedBy.MANUAL.toString());
                CreateChannelItemTypeResponse createChannelItemTypeResponse = channelCatalogService.createChannelItemType(channelItemTypeRequest);
                if (createChannelItemTypeResponse.isSuccessful()) {
                    String sellingPrice = row.getColumnValue(SELLING_PRICE);
                    String msp = row.getColumnValue(MSP);
                    String mrp = row.getColumnValue(MRP);
                    if (StringUtils.isNotBlank(priceImportJobTypeName) && (StringUtils.isNotBlank(sellingPrice) || StringUtils.isNotBlank(msp) || StringUtils.isNotBlank(mrp))) {
                        ImportJobHandler importHandler = ConfigurationManager.getInstance().getConfiguration(ImportJobConfiguration.class).getImportJobTypeConfigByName(
                                priceImportJobTypeName).getImportJobHandler();
                        response.addResponse(importHandler.handleRow(request, importOption));
                    } else {
                        response.addResponse(createChannelItemTypeResponse);
                    }
                } else {
                    response.addResponse(createChannelItemTypeResponse);
                }
            }
        }
        return response;
    }

    @Override
    public void preProcessor(ImportJob importJob) {
    }

    @Override
    public void postProcessor(ImportJob importJob) {
        if (StringUtils.isNotBlank(priceImportJobTypeName)) {
            ImportJobHandler importHandler = ConfigurationManager.getInstance().getConfiguration(ImportJobConfiguration.class).getImportJobTypeConfigByName(priceImportJobTypeName).getImportJobHandler();
            importHandler.postProcessor(importJob);
        }
    }

}
