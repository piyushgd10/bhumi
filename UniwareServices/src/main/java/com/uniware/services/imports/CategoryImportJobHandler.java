/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 23, 2012
 *  @author praveeng
 */
package com.uniware.services.imports;

import org.springframework.beans.factory.annotation.Autowired;

import com.unifier.core.api.validation.WsError;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.entity.ImportJob;
import com.unifier.core.entity.ImportJobType;
import com.unifier.core.entity.ImportJobType.ImportOptions;
import com.unifier.core.fileparser.Row;
import com.unifier.core.utils.StringUtils;
import com.unifier.services.imports.ImportJobHandler;
import com.unifier.services.imports.ImportJobHandlerRequest;
import com.unifier.services.imports.ImportJobHandlerResponse;
import com.uniware.core.api.catalog.CreateCategoryRequest;
import com.uniware.core.api.catalog.EditCategoryRequest;
import com.uniware.core.api.catalog.WsCategory;
import com.uniware.core.entity.Category;
import com.uniware.services.cache.CategoryCache;
import com.uniware.services.catalog.ICatalogService;
import com.uniware.services.configuration.SystemConfiguration;
import com.uniware.services.configuration.SystemConfiguration.TraceabilityLevel;

public class CategoryImportJobHandler implements ImportJobHandler {
    private static final String CODE                      = "category code";
    private static final String NAME                      = "category name";
    private static final String TAX_TYPE_CODE             = "tax type code";
    private static final String ITEM_DETAIL_FIELDS        = "item detail fields";
    private static final String HSN_CODE                  = "hsn code";
    private static final String GST_TAX_TYPE              = "gst tax type code";
    private static final String GRN_EXPIRY_TOLERANCE      = "grn expiry tolerance";
    private static final String DISPATCH_EXPIRY_TOLERANCE = "dispatch expiry tolerance";
    private static final String RETURN_EXPIRY_TOLERANCE   = "return expiry tolerance";
    private static final String EXPIRABLE                 = "expirable";
    private static final String SHELF_LIFE                = "shelf life";

    @Autowired
    private ICatalogService     catalogService;

    @Override
    public ImportJobHandlerResponse handleRow(ImportJobHandlerRequest request, ImportOptions importOption) throws Exception {
        if (ImportJobType.ImportOptions.CREATE_NEW_AND_UPDATE_EXISTING == importOption) {
            String code = request.getRow().getColumnValue(CODE);
            Category category = catalogService.getCategoryByCode(code);
            if (category == null) {
                return createCategory(request);
            } else {
                return editCategory(request);
            }
        } else if (ImportJobType.ImportOptions.CREATE_NEW == importOption) {
            return createCategory(request);
        } else if (ImportJobType.ImportOptions.UPDATE_EXISTING == importOption) {
            return editCategory(request);
        }
        return null;
    }

    private ImportJobHandlerResponse editCategory(ImportJobHandlerRequest request) {
        ImportJobHandlerResponse response = new ImportJobHandlerResponse();
        Row row = request.getRow();

        EditCategoryRequest categoryRequest = new EditCategoryRequest();
        WsCategory category = new WsCategory();
        prepareCategoryFromRow(row, category, response);
        if (!response.hasErrors()) {
            categoryRequest.setCategory(category);
            response.addResponse(catalogService.editCategory(categoryRequest));
        }
        return response;
    }

    private void prepareCategoryFromRow(Row row, WsCategory category, ImportJobHandlerResponse response) {
        TraceabilityLevel traceabilityLevel = ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).getTraceabilityLevel();
        category.setName(row.getColumnValue(NAME));
        category.setCode(row.getColumnValue(CODE));
        category.setGstTaxTypeCode(row.getColumnValue(GST_TAX_TYPE));
        category.setHsnCode(row.getColumnValue(HSN_CODE));
        category.setItemDetailFieldsText(row.getColumnValue(ITEM_DETAIL_FIELDS));
        category.setTaxTypeCode(row.getColumnValue(TAX_TYPE_CODE));
        if (StringUtils.isNotEmpty(row.getColumnValue(GRN_EXPIRY_TOLERANCE))) {
            try {
                category.setGrnExpiryTolerance(new Integer(row.getColumnValue(GRN_EXPIRY_TOLERANCE)));
            } catch (NumberFormatException e) {
                response.addError(new WsError("Invalid value for " + GRN_EXPIRY_TOLERANCE));
            }
        }
        if (StringUtils.isNotEmpty(row.getColumnValue(DISPATCH_EXPIRY_TOLERANCE))) {
            try {
                category.setDispatchExpiryTolerance(new Integer(row.getColumnValue(DISPATCH_EXPIRY_TOLERANCE)));
            } catch (NumberFormatException e) {
                response.addError(new WsError("Invalid value for " + DISPATCH_EXPIRY_TOLERANCE));
            }
        }
        if (StringUtils.isNotEmpty(row.getColumnValue(RETURN_EXPIRY_TOLERANCE))) {
            try {
                category.setReturnExpiryTolerance(new Integer(row.getColumnValue(RETURN_EXPIRY_TOLERANCE)));
            } catch (NumberFormatException e) {
                response.addError(new WsError("Invalid value for " + RETURN_EXPIRY_TOLERANCE));
            }
        }
        if (StringUtils.isNotEmpty(row.getColumnValue(EXPIRABLE))) {
            try {
                category.setExpirable("1".equals(row.getColumnValue(EXPIRABLE)));
            } catch (NumberFormatException e) {
                response.addError(new WsError("Invalid value for " + EXPIRABLE));
            }
        }
        if (StringUtils.isNotEmpty(row.getColumnValue(SHELF_LIFE))) {
            try {
                category.setShelfLife(new Integer(row.getColumnValue(SHELF_LIFE)));
            } catch (NumberFormatException e) {
                response.addError(new WsError("Invalid value for " + SHELF_LIFE));
            }
        }
    }

    private ImportJobHandlerResponse createCategory(ImportJobHandlerRequest request) {
        ImportJobHandlerResponse response = new ImportJobHandlerResponse();
        Row row = request.getRow();

        CreateCategoryRequest categoryRequest = new CreateCategoryRequest();
        WsCategory category = new WsCategory();
        prepareCategoryFromRow(row, category, response);
        if (!response.hasErrors()) {
            categoryRequest.setCategory(category);
            response.addResponse(catalogService.createCategory(categoryRequest));
        }
        return response;
    }

    @Override
    public void preProcessor(ImportJob importJob) {
    }

    @Override
    public void postProcessor(ImportJob importJob) {
        CacheManager.getInstance().markCacheDirty(CategoryCache.class);
    }

}
