/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 01-Jun-2012
 *  @author praveeng
 */
package com.uniware.services.imports;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;

import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.api.validation.WsError;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.entity.ImportJob;
import com.unifier.core.entity.ImportJobType.ImportOptions;
import com.unifier.core.fileparser.DelimitedFileParser;
import com.unifier.core.fileparser.DelimitedFileParser.RowIterator;
import com.unifier.core.fileparser.Row;
import com.unifier.core.utils.FileUtils;
import com.unifier.core.utils.StringUtils;
import com.unifier.services.imports.ImportJobHandler;
import com.unifier.services.imports.ImportJobHandlerRequest;
import com.unifier.services.imports.ImportJobHandlerResponse;
import com.unifier.services.imports.ImportJobTypeDTO;
import com.unifier.services.imports.impl.ImportServiceImpl;
import com.unifier.services.utils.CustomFieldUtils;
import com.uniware.core.api.catalog.CreateShippingProviderLocationRequest;
import com.uniware.core.api.catalog.EditShippingProviderLocationRequest;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.cache.EnvironmentPropertiesCache;
import com.uniware.core.cache.FacilityCache;
import com.uniware.core.entity.Facility;
import com.uniware.core.entity.FacilityProfile;
import com.uniware.core.entity.ShippingMethod;
import com.uniware.core.entity.ShippingProvider;
import com.uniware.core.entity.ShippingProviderLocation;
import com.uniware.core.utils.UserContext;
import com.uniware.services.configuration.ImportJobConfiguration;
import com.uniware.services.configuration.ShippingConfiguration;
import com.uniware.services.shipping.IShippingService;
import com.uniware.services.warehouse.IFacilityService;

public class ShippingProviderLocationImportJobHandler implements ImportJobHandler {

    private static final String NAME                   = "name";
    private static final String PINCODE                = "pincode";
    private static final String ROUTING_CODE           = "routing code";
    //    private static final String SHIPPING_METHOD_CODE     = "shipping provider method";
    //    private static final String CASH_ON_DELIVERY         = "COD";
    private static final String SHIPPING_PROVIDER_CODE = "shipping provider";
    private static final String SHIPPING_METHOD_NAME   = "shipping provider method";
    private static final String PRIORITY               = "priority";
    private static final String FACILITY_CODE          = "facility code";
    private static final String EXPRESSION             = "Expression";

    @Autowired
    private IShippingService    shippingService;

    @Autowired
    private IFacilityService    facilityService;

    @Override
    public ImportJobHandlerResponse handleRow(ImportJobHandlerRequest request, ImportOptions importOption) throws Exception {
        ImportJobHandlerResponse response = new ImportJobHandlerResponse();
        ValidationContext context = request.validate();
        FacilityProfile facilityProfile = facilityService.getFacilityProfileByCode(UserContext.current().getFacility().getCode());
        Row row = request.getRow();
        if (facilityProfile != null && facilityProfile.isAutoSetupServiceability()) {
            context.addError(WsResponseCode.INVALID_COLUMN_ENTRIES,
                    "Facility does not allow manual serviceability upload. Please uncheck Auto Setup Serviceability in facility for manual upload.");
        }
        if (!context.hasErrors()) {
            String pincode = row.getColumnValue(PINCODE);
            String shippingProviderCode = row.getColumnValue(SHIPPING_PROVIDER_CODE);
            String shippingMethodName = row.getColumnValue(SHIPPING_METHOD_NAME);
            String facilityCode = row.getColumnValue(FACILITY_CODE);
            Facility currentFacility = UserContext.current().getFacility();
            try {
                if (StringUtils.isNotBlank(facilityCode)) {
                    Facility facility = CacheManager.getInstance().getCache(FacilityCache.class).getFacilityByCode(facilityCode);
                    if (facility == null) {
                        context.addError(WsResponseCode.INVALID_FACILITY_CODE, "Invalid Facility Code");
                    } else {
                        UserContext.current().setFacility(facility);
                    }
                }
                if (!context.hasErrors()) {
                    ShippingProviderLocation location = shippingService.getShippingProviderLocation(shippingMethodName, shippingProviderCode, pincode);
                    if (location == null) {
                        context.addErrors(createShippingProviderLocation(request).getErrors());
                    } else {
                        context.addErrors(editShippingProviderLocation(request).getErrors());
                    }
                }
            } finally {
                UserContext.current().setFacility(currentFacility);
            }
        }
        response.addErrors(context.getErrors());
        response.setSuccessful(!context.hasErrors());
        return response;

    }

    private ImportJobHandlerResponse editShippingProviderLocation(ImportJobHandlerRequest request) {
        ImportJobHandlerResponse response = new ImportJobHandlerResponse();
        Row row = request.getRow();

        EditShippingProviderLocationRequest locationRequest = new EditShippingProviderLocationRequest();
        locationRequest.setName(row.getColumnValue(NAME));
        locationRequest.setPincode(row.getColumnValue(PINCODE));
        locationRequest.setRoutingCode(row.getColumnValue(ROUTING_CODE));
        locationRequest.setShippingProviderCode(row.getColumnValue(SHIPPING_PROVIDER_CODE));
        locationRequest.setShippingMethodName(row.getColumnValue(SHIPPING_METHOD_NAME));

        try {
            locationRequest.setPriority(Integer.parseInt(row.getColumnValue(PRIORITY)));
        } catch (NumberFormatException e) {
            response.addError(new WsError("Invalid value for 'priority'"));
        }

        locationRequest.setFilterExpression(row.getColumnValue(EXPRESSION));
        locationRequest.setCustomFieldValues(CustomFieldUtils.getCustomFieldValues(response, ShippingProviderLocation.class.getName(), row));
        if (!response.hasErrors()) {
            response.addResponse(shippingService.editShippingProviderLocation(locationRequest));
        }
        return response;
    }

    private ImportJobHandlerResponse createShippingProviderLocation(ImportJobHandlerRequest request) {
        ImportJobHandlerResponse response = new ImportJobHandlerResponse();
        Row row = request.getRow();

        CreateShippingProviderLocationRequest locationRequest = new CreateShippingProviderLocationRequest();
        locationRequest.setName(row.getColumnValue(NAME));
        locationRequest.setPincode(row.getColumnValue(PINCODE));
        locationRequest.setRoutingCode(row.getColumnValue(ROUTING_CODE));
        locationRequest.setShippingProviderCode(row.getColumnValue(SHIPPING_PROVIDER_CODE));
        locationRequest.setShippingMethodName(row.getColumnValue(SHIPPING_METHOD_NAME));

        try {
            locationRequest.setPriority(Integer.parseInt(row.getColumnValue(PRIORITY)));
        } catch (NumberFormatException e) {
            response.addError(new WsError("Invalid value for 'priority'"));
        }

        locationRequest.setFilterExpression(row.getColumnValue(EXPRESSION));
        locationRequest.setCustomFieldValues(CustomFieldUtils.getCustomFieldValues(response, ShippingProviderLocation.class.getName(), row));
        if (!response.hasErrors()) {
            response.addResponse(shippingService.createShippingProviderLocation(locationRequest));
        }
        return response;
    }

    @Override
    public void preProcessor(ImportJob importJob) {

    }

    @Override
    public void postProcessor(ImportJob importJob) {
        ImportJobTypeDTO importJobConfig = ConfigurationManager.getInstance().getConfiguration(ImportJobConfiguration.class).getImportJobTypeConfigByName(
                importJob.getImportJobTypeName());
        String importDirectory = CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class).getImportDirectoryPath();
        String importFilePath = FileUtils.normalizeFilePath(importDirectory, importJob.getFileName());
        DelimitedFileParser parser = new DelimitedFileParser(importFilePath, ',');
        RowIterator it = parser.parse();
        Map<String, Set<String>> serviceablePincodes = new HashMap<String, Set<String>>();
        while (it.hasNext()) {
            Row row = ImportServiceImpl.transformRow(importJob, importJobConfig, it.next());
            StringBuilder shippingProviderMethodBuilder = new StringBuilder().append(row.getColumnValue(SHIPPING_PROVIDER_CODE)).append('~').append(row.getColumnValue(SHIPPING_METHOD_NAME));
            if(StringUtils.isNotBlank(row.getColumnValue(FACILITY_CODE))) {
                shippingProviderMethodBuilder.append('~').append(row.getColumnValue(FACILITY_CODE));
            }
            String shippingProviderMethod = shippingProviderMethodBuilder.toString();
            Set<String> pincodes = serviceablePincodes.get(shippingProviderMethod.toUpperCase());
            if (pincodes == null) {
                pincodes = new HashSet<String>();
                serviceablePincodes.put(shippingProviderMethod.toUpperCase(), pincodes);
            }
            pincodes.add(row.getColumnValue(PINCODE));
        }
        ShippingConfiguration configuration = ConfigurationManager.getInstance().getConfiguration(ShippingConfiguration.class);
        for (Map.Entry<String, Set<String>> entry : serviceablePincodes.entrySet()) {
            String[] fields = entry.getKey().split("~");
            ShippingProvider shippingProvider = configuration.getShippingProviderByCode(fields[0]);
            ShippingMethod shippingMethod = configuration.getShippingMethod(fields[1]);
            if (shippingProvider != null && shippingMethod != null) {
                Set<String> pincodes;
                if (fields.length > 2) {
                    Facility facility = CacheManager.getInstance().getCache(FacilityCache.class).getFacilityByCode(fields[2]);
                    if (facility != null) {
                        pincodes = shippingService.getServiceablePincodesByShippingProviderMethod(shippingProvider, shippingMethod, facility.getCode());
                        pincodes.removeAll(entry.getValue());
                        if (!pincodes.isEmpty()) {
                            shippingService.updateNonServiceablePincodes(shippingProvider, shippingMethod, pincodes, facility.getId());
                        }
                    }
                } else {
                    pincodes = shippingService.getServiceablePincodesByShippingProviderMethod(shippingProvider, shippingMethod);
                    pincodes.removeAll(entry.getValue());
                    if (!pincodes.isEmpty()) {
                        shippingService.updateNonServiceablePincodes(shippingProvider, shippingMethod, pincodes);
                    }
                }
            }
        }

    }
}
