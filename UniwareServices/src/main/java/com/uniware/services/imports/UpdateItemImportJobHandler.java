/**
 * Copyright 2017 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version 1.0, 04/09/17
 * @author aditya
 */
package com.uniware.services.imports;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;

import com.unifier.core.api.validation.WsError;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.entity.ImportJob;
import com.unifier.core.entity.ImportJobType;
import com.unifier.core.utils.DateUtils;
import com.unifier.services.imports.ImportJobHandler;
import com.unifier.services.imports.ImportJobHandlerRequest;
import com.unifier.services.imports.ImportJobHandlerResponse;
import com.uniware.core.api.item.UpdateItemRequest;
import com.uniware.core.api.item.UpdateItemResponse;
import com.uniware.core.cache.FacilityCache;
import com.uniware.core.entity.Facility;
import com.uniware.services.inventory.IInventoryService;

public class UpdateItemImportJobHandler implements ImportJobHandler {
    private static final String ITEM_CODE          = "item code";
    private static final String FACILITY_CODE      = "facility code";
    private static final String MANUFACTURING_DATE = "manufacturing date";

    @Autowired
    private IInventoryService   inventoryService;

    @Override
    public ImportJobHandlerResponse handleRow(ImportJobHandlerRequest request, ImportJobType.ImportOptions importOption) throws Exception {
        ImportJobHandlerResponse response = new ImportJobHandlerResponse();
        if (ImportJobType.ImportOptions.UPDATE_EXISTING == importOption) {
            String itemCode = request.getRow().getColumnValue(ITEM_CODE);
            String facilityCode = request.getRow().getColumnValue(FACILITY_CODE);
            if (request.getRow().getColumnValue(MANUFACTURING_DATE) != null) {
                Date manufacturingDate = DateUtils.stringToDate(request.getRow().getColumnValue(MANUFACTURING_DATE), "dd/MM/yyyy");
                if (manufacturingDate != null) {
                    Facility facility = CacheManager.getInstance().getCache(FacilityCache.class).getFacilityByCode(facilityCode);
                    if (facility != null) {
                        UpdateItemRequest updateItemRequest = new UpdateItemRequest();
                        updateItemRequest.setItemCode(itemCode);
                        updateItemRequest.setFacilityCode(facilityCode);
                        updateItemRequest.setManufacturingDate(manufacturingDate);
                        UpdateItemResponse updateItemResponse = inventoryService.updateItemForFacility(updateItemRequest);
                        if (updateItemResponse.hasErrors()) {
                            response.addErrors(updateItemResponse.getErrors());
                        } else {
                            response.setSuccessful(true);
                        }
                    } else {
                        response.addError(new WsError("Invalid value for " + FACILITY_CODE));
                    }
                } else {
                    response.addError(new WsError("Invalid value for " + MANUFACTURING_DATE));
                }
            } else {
                response.addError(new WsError("Manufacturing date cannot be empty"));
            }

        } else {
            response.addError(new WsError("Operation Not Allowed"));
        }
        return response;
    }

    @Override
    public void preProcessor(ImportJob importJob) {

    }

    @Override
    public void postProcessor(ImportJob importJob) {

    }
}
