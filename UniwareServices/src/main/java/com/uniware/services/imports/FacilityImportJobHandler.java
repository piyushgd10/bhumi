/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 23, 2012
 *  @author praveeng
 */
package com.uniware.services.imports;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.unifier.core.api.user.AddUserRolesRequest;
import com.unifier.core.api.user.CreateUserRequest;
import com.unifier.core.api.user.CreateUserResponse;
import com.unifier.core.api.validation.WsError;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.entity.ImportJob;
import com.unifier.core.entity.ImportJobType;
import com.unifier.core.entity.ImportJobType.ImportOptions;
import com.unifier.core.entity.User;
import com.unifier.core.fileparser.Row;
import com.unifier.core.utils.StringUtils;
import com.unifier.core.utils.ValidatorUtils;
import com.unifier.services.imports.ImportJobHandler;
import com.unifier.services.imports.ImportJobHandlerRequest;
import com.unifier.services.imports.ImportJobHandlerResponse;
import com.unifier.services.users.IUsersService;
import com.unifier.services.utils.CustomFieldUtils;
import com.uniware.core.api.facility.CreateFacilityProfileRequest;
import com.uniware.core.api.facility.CreateFacilityRequest;
import com.uniware.core.api.facility.CreateFacilityResponse;
import com.uniware.core.api.party.WsFacility;
import com.uniware.core.api.party.WsFacilityProfile;
import com.uniware.core.api.party.WsPartyAddress;
import com.uniware.core.api.party.WsPartyContact;
import com.uniware.core.api.warehouse.EditFacilityRequest;
import com.uniware.core.api.warehouse.EditFacilityResponse;
import com.uniware.core.api.warehouse.FacilityDTO;
import com.uniware.core.api.warehouse.GetFacilityRequest;
import com.uniware.core.cache.FacilityCache;
import com.uniware.core.entity.Facility;
import com.uniware.core.entity.Location;
import com.uniware.core.entity.PartyAddressType;
import com.uniware.core.entity.PartyContactType;
import com.uniware.core.utils.UserContext;
import com.uniware.services.location.ILocationService;
import com.uniware.services.party.IPartyService;
import com.uniware.services.warehouse.IFacilityService;

public class FacilityImportJobHandler implements ImportJobHandler {
    private static final Logger LOG                         = LoggerFactory.getLogger(FacilityImportJobHandler.class);

    private static final String FACILITY_TYPE               = "Facility Type";
    private static final String FACILITY_NAME               = "Facility Name";
    private static final String FACILITY_DISPLAY_NAME       = "Facility Display Name";
    private static final String FACILITY_CODE               = "Facility Code";
    private static final String FACILITY_ENABLED            = "Enabled";
    private static final String SALES_TAX_ON_SHIPPING_CHARGES = "Sales Tax On Shipping Charges";
    private static final String FACILITY_PAN                = "PAN";
    private static final String FACILITY_TIN                = "TIN";
    private static final String FACILITY_CST                = "CST Number";
    private static final String FACILITY_CIN                = "CIN Number";
    private static final String FACILITY_GST                = "GST Number";
    private static final String FACILITY_ST                 = "Service Tax Number";
    private static final String FACILITY_WEBSITE            = "Website";

    //address details for shipping List<WsAddressDetail> in WsSaleOrder
    private static final String SHIPPING_ADDRESS_LINE1      = "Shipping Address Line1";
    private static final String SHIPPING_ADDRESS_LINE2      = "Shipping Address Line2";
    private static final String SHIPPING_ADDRESS_CITY       = "Shipping Address City";
    private static final String SHIPPING_ADDRESS_STATE      = "Shipping Address State";
    private static final String SHIPPING_ADDRESS_COUNTRY    = "Shipping Address Country";
    private static final String SHIPPING_ADDRESS_PINCODE    = "Shipping Address Pincode";
    private static final String SHIPPING_ADDRESS_PHONE      = "Shipping Address Phone";

    //address details for  billing List<WsAddressDetail> in WsSaleOrder
    private static final String BILLING_ADDRESS_LINE1       = "Billing Address Line1";
    private static final String BILLING_ADDRESS_LINE2       = "Billing Address Line2";
    private static final String BILLING_ADDRESS_CITY        = "Billing Address City";
    private static final String BILLING_ADDRESS_STATE       = "Billing Address State";
    private static final String BILLING_ADDRESS_COUNTRY     = "Billing Address Country";
    private static final String BILLING_ADDRESS_PINCODE     = "Billing Address Pincode";
    private static final String BILLING_ADDRESS_PHONE       = "Billing Address Phone";

    //contact details for primary contact
    private static final String PRIMARY_CONTACT_NAME        = "Primary Contact Name";
    private static final String PRIMARY_CONTACT_EMAIL       = "Primary Contact Email";
    private static final String PRIMARY_CONTACT_FAX         = "Primary Contact Fax";
    private static final String PRIMARY_CONTACT_PHONE       = "Primary Contact Phone";

    // USer details
    private static final String NAME                        = "Name";
    private static final String USERNAME                    = "Username";
    private static final String PASSWORD                    = "Password";
    private static final String ROLE                        = "User Role";
    private static final String USER_EMAIL                  = "Email";

    // Roles to preserve
    private static final String ROLES_TO_PRESERVE           = "Roles To Preserve";

    // Facility Profile: Required for store type facility
    private static final String SELLER_NAME                 = "Seller Name";
    private static final String SELLER_MOBILE               = "Seller Mobile";
    private static final String SELLER_EMAIL                = "Seller Email";
    private static final String SELLER_ALTERNATE_PHONE      = "Alternate Phone";

    private static final String AUTO_SETUP_SERVICEABILITY   = "Auto Setup Serviceability";
    private static final String DELIVERY_RADIUS             = "Delivery Radius";
    private static final String STORE_PICKUP_ENABLED        = "Store pick Enabled";
    private static final String PICK_UP_SHIPPING_METHODS    = "Pick up Shipping Methods";
    private static final String STORE_SELF_DELIVERY_ENABLED = "Store Self Delivery Enabled";
    private static final String STORE_SELF_SHIPPING_METHODS = "Store Self Shipping Methods";
    private static final String DISPATCH_SLA                = "Dispatch SLA";
    private static final String DELIVERY_SLA                = "Delivery SLA";

    @Autowired
    private IFacilityService    facilityService;

    @Autowired
    private IUsersService       userService;

    @Autowired
    private IPartyService       partyService;

    @Autowired
    private ILocationService    locationService;

    @Override
    public ImportJobHandlerResponse handleRow(ImportJobHandlerRequest request, ImportOptions importOption) throws Exception {
        ImportJobHandlerResponse response = new ImportJobHandlerResponse();
        String facilityCode = request.getRow().getColumnValue(FACILITY_CODE);
        GetFacilityRequest req = new GetFacilityRequest();
        req.setCode(facilityCode);
        FacilityDTO facility = facilityService.getFacilityByCode(req).getFacilityDTO();

        if (ImportJobType.ImportOptions.CREATE_NEW_AND_UPDATE_EXISTING == importOption) {
            if (facility == null) {
                response = createFacility(request);
            } else {
                response = updateFacility(request, facility);
            }
        } else if (ImportJobType.ImportOptions.CREATE_NEW == importOption) {
            if (facility == null) {
                response = createFacility(request);
            } else {
                response.addError(new WsError("Facility " + facilityCode + " already exists"));
            }
        } else if (ImportJobType.ImportOptions.UPDATE_EXISTING == importOption) {
            if (facility != null) {
                response = updateFacility(request, facility);
            } else {
                response.addError(new WsError("Facility" + facilityCode + " does not exist."));
            }
        }

        if (response.isSuccessful() && request.getRow().getColumnValue(USERNAME) != null) {
            Facility f = CacheManager.getInstance().getCache(FacilityCache.class).getFacilityById((Integer) response.getResultItems().get("facilityId"));
            UserContext.current().setFacility(f);
            User user = userService.getUserByUsername(request.getRow().getColumnValue(USERNAME));
            if (user == null) {
                response.addResponse(createUser(request));
            }
        }
        return response;
    }

    private CreateUserResponse createUser(ImportJobHandlerRequest request) {
        CreateUserRequest createUserRequest = new CreateUserRequest();
        createUserRequest.setName(request.getRow().getColumnValue(NAME));
        createUserRequest.setUsername(request.getRow().getColumnValue(USERNAME));
        createUserRequest.setPassword(request.getRow().getColumnValue(PASSWORD));
        createUserRequest.setConfirmPassword(request.getRow().getColumnValue(PASSWORD));
        createUserRequest.setEmail(request.getRow().getColumnValue(USER_EMAIL));
        createUserRequest.addRole(request.getRow().getColumnValue(ROLE));
        return userService.createUser(createUserRequest);
    }

    private ImportJobHandlerResponse updateFacility(ImportJobHandlerRequest request, FacilityDTO fac) {
        ImportJobHandlerResponse response = new ImportJobHandlerResponse();
        Row row = request.getRow();
        EditFacilityRequest editFacilityRequest = new EditFacilityRequest();
        WsFacility facility = new WsFacility();
        facility.setCode(row.getColumnValue(FACILITY_CODE));
        facility.setName(ValidatorUtils.getOrDefaultValue(row.getColumnValue(FACILITY_NAME), fac.getName()));
        facility.setDisplayName(ValidatorUtils.getOrDefaultValue(row.getColumnValue(FACILITY_DISPLAY_NAME), fac.getDisplayName()));
        facility.setPan(ValidatorUtils.getOrDefaultValue(row.getColumnValue(FACILITY_PAN), fac.getPan()));
        facility.setTin(ValidatorUtils.getOrDefaultValue(row.getColumnValue(FACILITY_TIN), fac.getTin()));
        facility.setCstNumber(ValidatorUtils.getOrDefaultValue(row.getColumnValue(FACILITY_CST), fac.getCstNumber()));
        facility.setCinNumber(ValidatorUtils.getOrDefaultValue(row.getColumnValue(FACILITY_CIN), fac.getCinNumber()));
        facility.setStNumber(ValidatorUtils.getOrDefaultValue(row.getColumnValue(FACILITY_ST), fac.getStNumber()));
        facility.setGstNumber(ValidatorUtils.getOrDefaultValue(row.getColumnValue(FACILITY_GST), fac.getGstNumber()));
        facility.setWebsite(ValidatorUtils.getOrDefaultValue(row.getColumnValue(FACILITY_WEBSITE), fac.getWebsite()));
        facility.setEnabled("0".equals(row.getColumnValue(FACILITY_ENABLED)) ? false : true);
        facility.setCustomFieldValues(CustomFieldUtils.getCustomFieldValues(response, Facility.class.getName(), row));
        if (StringUtils.isNotBlank(row.getColumnValue(SHIPPING_ADDRESS_LINE1))) {
            facility.setShippingAddress(preparePartyShippingAddress(row, response));
        }
        if (StringUtils.isNotBlank(row.getColumnValue(BILLING_ADDRESS_LINE1))) {
            facility.setBillingAddress(preparePartyBillingAddress(row, response));
        }
        if (StringUtils.isNotBlank(row.getColumnValue(PRIMARY_CONTACT_NAME))) {
            facility.getPartyContacts().add(preparePartyContact(row, response));
        }
        editFacilityRequest.setFacility(facility);
        EditFacilityResponse editFacilityResponse = facilityService.editFacility(editFacilityRequest);
        response.addResponse(editFacilityResponse);
        response.addResultItem("facilityId", editFacilityResponse.getFacilityDTO().getId());
        return response;
    }

    private ImportJobHandlerResponse createFacility(ImportJobHandlerRequest request) {
        ImportJobHandlerResponse response = new ImportJobHandlerResponse();
        Row row = request.getRow();

        WsFacility wsFacility = prepareWsFacility(row, response);
        WsFacilityProfile profile = null;
        if (Facility.Type.STORE.name().equalsIgnoreCase(wsFacility.getType())) {
            profile = prepareWsFacilityProfile(row, response);
        }
        if (!response.hasErrors()) {
            CreateFacilityRequest createFacilityRequest = new CreateFacilityRequest();
            createFacilityRequest.setFacility(wsFacility);
            createFacilityRequest.setUsername(userService.getUserWithDetailsById(request.getUserId()).getUsername());
            CreateFacilityResponse createFacilityResponse = null;
            try {
                createFacilityResponse = facilityService.createFacility(createFacilityRequest);
                response.addResponse(createFacilityResponse);
                if (createFacilityResponse.isSuccessful() && Facility.Type.STORE.name().equalsIgnoreCase(createFacilityResponse.getFacilityDTO().getType())) {
                    CreateFacilityProfileRequest profileRequest = new CreateFacilityProfileRequest();
                    profileRequest.setFacilityProfile(profile);
                    createFacilityResponse = facilityService.createFacilityProfile(profileRequest);
                    response.addResponse(createFacilityResponse);
                }
            } catch (Exception e) {
                LOG.error("Error creating facility: ", e);
                response.addError(new WsError(e.getMessage()));
            }

            if (createFacilityResponse != null && !createFacilityResponse.hasErrors()) {
                response.addResultItem("facilityId", createFacilityResponse.getFacilityDTO().getId());

                // See if we need to preserve roles in this facility.
                if (StringUtils.isNotEmpty(row.getColumnValue(ROLES_TO_PRESERVE))) {
                    for (String roleCode : row.getColumnValue(ROLES_TO_PRESERVE).split(",")) {
                        List<String> usernames = userService.getUsersByRole(roleCode);
                        for (String username : usernames) {
                            AddUserRolesRequest addRoleRequest = new AddUserRolesRequest();
                            addRoleRequest.setUsername(username);
                            addRoleRequest.addFacilityCode(createFacilityResponse.getFacilityDTO().getCode());
                            addRoleRequest.addFacilityRoleCode(roleCode);
                            userService.addUserRoles(addRoleRequest);
                        }
                    }
                }

            }
        }
        return response;
    }

    private WsFacilityProfile prepareWsFacilityProfile(Row row, ImportJobHandlerResponse response) {
        WsFacilityProfile wsProfile = new WsFacilityProfile();
        wsProfile.setFacilityCode(row.getColumnValue(FACILITY_CODE));
        wsProfile.setSellerName(row.getColumnValue(SELLER_NAME));
        wsProfile.setContactEmail(row.getColumnValue(SELLER_EMAIL));
        wsProfile.setMobile(row.getColumnValue(SELLER_MOBILE));
        wsProfile.setAlternatePhone(row.getColumnValue(SELLER_ALTERNATE_PHONE));
        Location location = locationService.getLocationByPincode(row.getColumnValue(SHIPPING_ADDRESS_PINCODE));
        if (location == null) {
            response.addError(new WsError("No position co-ordinates found for given pincode " + row.getColumnValue(SHIPPING_ADDRESS_PINCODE)));
        }
        List<Double> position = new ArrayList<>();
        for (double coordinate : location.getPosition()) {
            position.add(Double.valueOf(coordinate));
        }
        wsProfile.setPosition(position);
        wsProfile.setAutoSetupServiceability("1".equals(row.getColumnValue(AUTO_SETUP_SERVICEABILITY)) ? true : false);
        wsProfile.setStoreDeliveryEnabled("1".equals(row.getColumnValue(STORE_SELF_DELIVERY_ENABLED)) ? true : false);
        if (StringUtils.isNotBlank(row.getColumnValue(DELIVERY_RADIUS))) {
            wsProfile.setDeliveryRadius(Double.valueOf(row.getColumnValue(DELIVERY_RADIUS)));
        } else {
            wsProfile.setDeliveryRadius(new Double(10.0));
        }
        wsProfile.setStorePickupEnabled("1".equals(row.getColumnValue(STORE_PICKUP_ENABLED)) ? true : false);
        if (wsProfile.getStorePickupEnabled()) {
            if (StringUtils.isNotBlank(row.getColumnValue(PICK_UP_SHIPPING_METHODS))) {
                List<String> methods = new ArrayList<>();
                for (String method : row.getColumnValue(PICK_UP_SHIPPING_METHODS).split(",")) {
                    methods.add(method.trim());
                }
                wsProfile.setPickupShippingMethods(methods);
            } else {
                response.addError(new WsError("Please fill valid values for pick up shipping methods. Pickup-COD,Pickup-Prepaid"));
            }
        } else {
            wsProfile.setPickupShippingMethods(new ArrayList<String>());
        }
        
        if (wsProfile.getStoreDeliveryEnabled()) {
            if (StringUtils.isNotBlank(row.getColumnValue(STORE_SELF_SHIPPING_METHODS))) {
                List<String> methods = new ArrayList<>();
                for (String method : row.getColumnValue(STORE_SELF_SHIPPING_METHODS).split(",")) {
                    methods.add(method.trim());
                }
                wsProfile.setSelfShippingMethods(methods);
            } else {
                response.addError(new WsError("Please fill valid values for pick up shipping methods. Standard-COD,Standard-Prepaid"));
            }
        } else {
            wsProfile.setSelfShippingMethods(new ArrayList<String>());
        }
        if (!wsProfile.getStoreDeliveryEnabled() && !wsProfile.getStorePickupEnabled()) {
            response.addError(new WsError("Either delivery or pick needs to enabled for a store to function."));
        }
        if (wsProfile.getStorePickupEnabled()) {
            if (StringUtils.isNotBlank(row.getColumnValue(DISPATCH_SLA))) {
                wsProfile.setDispatchSLA(Integer.valueOf(row.getColumnValue(DISPATCH_SLA)).intValue());
            } else {
                response.addError(new WsError("Dispatch SLA required to enable pickup"));
            }
        }
        if (wsProfile.getStoreDeliveryEnabled()) {
            if (StringUtils.isNotBlank(row.getColumnValue(DELIVERY_SLA)) && StringUtils.isNotBlank(row.getColumnValue(DISPATCH_SLA))) {
                wsProfile.setDeliverySLA(Integer.valueOf(row.getColumnValue(DELIVERY_SLA)).intValue());
                wsProfile.setDispatchSLA(Integer.valueOf(row.getColumnValue(DISPATCH_SLA)).intValue());
            } else {
                response.addError(new WsError("Delivery & Dispatch SLA required to enable delivery"));
            }
        }
        return wsProfile;
    }

    private WsFacility prepareWsFacility(Row row, ImportJobHandlerResponse response) {
        WsFacility wsFacility = new WsFacility();
        wsFacility.setType(row.getColumnValue(FACILITY_TYPE));
        wsFacility.setDisplayName(row.getColumnValue(FACILITY_DISPLAY_NAME));
        wsFacility.setCode(row.getColumnValue(FACILITY_CODE));
        wsFacility.setEnabled("0".equals(row.getColumnValue(FACILITY_ENABLED)) ? false : true);
        wsFacility.setSalesTaxOnShippingCharges("1".equals(row.getColumnValue(SALES_TAX_ON_SHIPPING_CHARGES)) ? true : false);
        wsFacility.setCstNumber(row.getColumnValue(FACILITY_CST));
        wsFacility.setCinNumber(row.getColumnValue(FACILITY_CIN));
        wsFacility.setName(row.getColumnValue(FACILITY_NAME));
        wsFacility.setPan(row.getColumnValue(FACILITY_PAN));
        wsFacility.setStNumber(row.getColumnValue(FACILITY_ST));
        wsFacility.setTin(row.getColumnValue(FACILITY_TIN));
        wsFacility.setGstNumber(row.getColumnValue(FACILITY_GST));
        wsFacility.setWebsite(row.getColumnValue(FACILITY_WEBSITE));
        wsFacility.setCustomFieldValues(CustomFieldUtils.getCustomFieldValues(response, Facility.class.getName(), row));
        wsFacility.getPartyContacts().add(preparePartyContact(row, response));
        wsFacility.setBillingAddress(preparePartyBillingAddress(row, response));
        wsFacility.setShippingAddress(preparePartyShippingAddress(row, response));
        return wsFacility;
    }

    private WsPartyContact preparePartyContact(Row row, ImportJobHandlerResponse response) {
        WsPartyContact wsPartyContact = new WsPartyContact();
        wsPartyContact.setContactType(PartyContactType.Code.PRIMARY.name());
        wsPartyContact.setEmail(row.getColumnValue(PRIMARY_CONTACT_EMAIL));
        wsPartyContact.setFax(row.getColumnValue(PRIMARY_CONTACT_FAX));
        wsPartyContact.setName(row.getColumnValue(PRIMARY_CONTACT_NAME));
        wsPartyContact.setPartyCode(row.getColumnValue(FACILITY_CODE));
        wsPartyContact.setPhone(row.getColumnValue(PRIMARY_CONTACT_PHONE));
        return wsPartyContact;
    }

    private WsPartyAddress preparePartyBillingAddress(Row row, ImportJobHandlerResponse response) {
        WsPartyAddress wsPartyBillingAddress = new WsPartyAddress();
        wsPartyBillingAddress.setAddressLine1(row.getColumnValue(BILLING_ADDRESS_LINE1));
        wsPartyBillingAddress.setAddressLine2(row.getColumnValue(BILLING_ADDRESS_LINE2));
        wsPartyBillingAddress.setCity(row.getColumnValue(BILLING_ADDRESS_CITY));
        if (StringUtils.isNotBlank(row.getColumnValue(BILLING_ADDRESS_COUNTRY))) {
            wsPartyBillingAddress.setCountryCode(row.getColumnValue(BILLING_ADDRESS_COUNTRY));
        }
        wsPartyBillingAddress.setStateCode(row.getColumnValue(BILLING_ADDRESS_STATE));
        wsPartyBillingAddress.setPartyCode(row.getColumnValue(FACILITY_CODE));
        wsPartyBillingAddress.setAddressType(PartyAddressType.Code.BILLING.name());
        wsPartyBillingAddress.setPincode(row.getColumnValue(BILLING_ADDRESS_PINCODE));
        wsPartyBillingAddress.setPhone(row.getColumnValue(BILLING_ADDRESS_PHONE));
        return wsPartyBillingAddress;
    }

    private WsPartyAddress preparePartyShippingAddress(Row row, ImportJobHandlerResponse response) {
        WsPartyAddress wsPartyAddress = new WsPartyAddress();
        wsPartyAddress.setAddressLine1(row.getColumnValue(SHIPPING_ADDRESS_LINE1));
        wsPartyAddress.setAddressLine2(row.getColumnValue(SHIPPING_ADDRESS_LINE2));
        wsPartyAddress.setCity(row.getColumnValue(SHIPPING_ADDRESS_CITY));
        if (StringUtils.isNotBlank(row.getColumnValue(SHIPPING_ADDRESS_COUNTRY))) {
            wsPartyAddress.setCountryCode(row.getColumnValue(SHIPPING_ADDRESS_COUNTRY));
        }
        wsPartyAddress.setStateCode(row.getColumnValue(SHIPPING_ADDRESS_STATE));
        wsPartyAddress.setPartyCode(row.getColumnValue(FACILITY_CODE));
        wsPartyAddress.setAddressType(PartyAddressType.Code.SHIPPING.name());
        wsPartyAddress.setPincode(row.getColumnValue(SHIPPING_ADDRESS_PINCODE));
        wsPartyAddress.setPhone(row.getColumnValue(SHIPPING_ADDRESS_PHONE));
        return wsPartyAddress;
    }

    @Override
    public void preProcessor(ImportJob importJob) {
    }

    @Override
    public void postProcessor(ImportJob importJob) {
    }

}
