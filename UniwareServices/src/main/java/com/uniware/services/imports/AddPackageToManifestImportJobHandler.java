/*
 *  Copyright 2013 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 15-Jul-2013
 *  @author unicom
 */
package com.uniware.services.imports;

import com.unifier.core.utils.StringUtils;
import com.uniware.core.api.returns.AddShippingPackageToReturnManifestResponse;
import com.uniware.core.api.returns.EditReturnManifestItemRequest;
import org.springframework.beans.factory.annotation.Autowired;

import com.unifier.core.entity.ImportJob;
import com.unifier.core.entity.ImportJobType;
import com.unifier.core.entity.ImportJobType.ImportOptions;
import com.unifier.services.imports.ImportJobHandler;
import com.unifier.services.imports.ImportJobHandlerRequest;
import com.unifier.services.imports.ImportJobHandlerResponse;
import com.uniware.core.api.returns.AddShippingPackageToReturnManifestRequest;
import com.uniware.services.returns.IReturnsService;

public class AddPackageToManifestImportJobHandler implements ImportJobHandler {

    private final String RETURN_MANIFEST_CODE = "Return Manifest Code";
    private final String AWB_OR_SHIPMENT_Code = "Awb Or Shipment Code";
    private final String REASON               = "Reason";

    @Autowired
    private IReturnsService returnsService;

    @Override
    public ImportJobHandlerResponse handleRow(ImportJobHandlerRequest request, ImportOptions importOption) throws Exception {
        ImportJobHandlerResponse response = new ImportJobHandlerResponse();
        if (ImportJobType.ImportOptions.CREATE_NEW == importOption) {
            String code = request.getRow().getColumnValue(RETURN_MANIFEST_CODE);
            AddShippingPackageToReturnManifestRequest manifestRequest = new AddShippingPackageToReturnManifestRequest();
            manifestRequest.setReturnManifestCode(code);
            manifestRequest.setAwbOrShipmentCode(request.getRow().getColumnValue(AWB_OR_SHIPMENT_Code));
            //manifestRequest.setReason();
            AddShippingPackageToReturnManifestResponse addShippingPackageToReturnManifestResponse = returnsService.addShippingPackageToReturnManifest(manifestRequest);
            response.addResponse(addShippingPackageToReturnManifestResponse);
            if (StringUtils.isNotBlank(request.getRow().getColumnValue(REASON)) && addShippingPackageToReturnManifestResponse.isSuccessful()) {
                EditReturnManifestItemRequest editReturnManifestItemRequest = new EditReturnManifestItemRequest();
                editReturnManifestItemRequest.setPackageCode(addShippingPackageToReturnManifestResponse.getManifestItem().getShippingPackageCode());
                editReturnManifestItemRequest.setReturnManifestCode(code);
                editReturnManifestItemRequest.setReturnReason(request.getRow().getColumnValue(REASON));
                returnsService.editReturnManifestItem(editReturnManifestItemRequest);
            }
            return response;
        }
        return null;
    }

    @Override
    public void preProcessor(ImportJob importJob) {
        // TODO Auto-generated method stub

    }

    @Override
    public void postProcessor(ImportJob importJob) {
        // TODO Auto-generated method stub

    }

}
