/*
 * Copyright 2017 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 06/12/17
 * @author piyush
 */
package com.uniware.services.imports;

import org.springframework.beans.factory.annotation.Autowired;

import com.unifier.core.api.validation.WsError;
import com.unifier.core.entity.ImportJob;
import com.unifier.core.entity.ImportJobType;
import com.unifier.core.fileparser.Row;
import com.unifier.services.imports.ImportJobHandler;
import com.unifier.services.imports.ImportJobHandlerRequest;
import com.unifier.services.imports.ImportJobHandlerResponse;
import com.uniware.core.api.channel.ReenableCITInventorySyncStatusRequest;
import com.uniware.core.api.channel.ReenableInventorySyncRequest;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.services.channel.IChannelCatalogService;

public class ReEnableCITInventoryImportJobHandler implements ImportJobHandler {

    private final String           CHANNEL_CODE       = "Channel Code";
    private final String           CHANNEL_PRODUCT_ID = "Channel Product Id";

    @Autowired
    private IChannelCatalogService channelCatalogService;

    @Override
    public ImportJobHandlerResponse handleRow(ImportJobHandlerRequest request, ImportJobType.ImportOptions importOption) throws Exception {
        ImportJobHandlerResponse response = new ImportJobHandlerResponse();
        if (ImportJobType.ImportOptions.UPDATE_EXISTING == importOption) {
            reenableInventorySync(request, response);
        } else {
            response.addError(new WsError(WsResponseCode.FEATURE_NOT_SUPPORTED.code(), WsResponseCode.FEATURE_NOT_SUPPORTED.message()));
        }
        return response;
    }

    private void reenableInventorySync(ImportJobHandlerRequest request, ImportJobHandlerResponse response) {
        Row row = request.getRow();
        ReenableCITInventorySyncStatusRequest reenableInventorySyncStatusRequest = new ReenableCITInventorySyncStatusRequest();
        reenableInventorySyncStatusRequest.setChannelItemType(
                new ReenableInventorySyncRequest.WsReenableChannelItemType(row.getColumnValue(CHANNEL_CODE), row.getColumnValue(CHANNEL_PRODUCT_ID)));
        response.addResponse(channelCatalogService.reenableCITInventorySyncStatus(reenableInventorySyncStatusRequest));
    }

    @Override
    public void preProcessor(ImportJob importJob) {

    }

    @Override
    public void postProcessor(ImportJob importJob) {

    }
}
