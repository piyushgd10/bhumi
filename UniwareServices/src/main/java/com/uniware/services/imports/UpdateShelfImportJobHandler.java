/**
 * Copyright 2017 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version 1.0, 06/09/17
 * @author aditya
 */
package com.uniware.services.imports;

import org.springframework.beans.factory.annotation.Autowired;

import com.unifier.core.api.base.ServiceResponse;
import com.unifier.core.api.validation.WsError;
import com.unifier.core.entity.ImportJob;
import com.unifier.core.entity.ImportJobType.ImportOptions;
import com.unifier.core.utils.StringUtils;
import com.unifier.services.imports.ImportJobHandler;
import com.unifier.services.imports.ImportJobHandlerRequest;
import com.unifier.services.imports.ImportJobHandlerResponse;
import com.uniware.core.api.warehouse.CreateShelfRequest;
import com.uniware.core.api.warehouse.UpdateShelfRequest;
import com.uniware.services.warehouse.IShelfService;

public class UpdateShelfImportJobHandler implements ImportJobHandler {

    private static final String SHELF_CODE      = "shelf code";
    private static final String SHELF_TYPE_CODE = "shelf type code";
    private static final String SECTION_CODE    = "section code";
    private static final String ENABLED         = "enabled";

    @Autowired
    private IShelfService       shelfService;

    @Override
    public ImportJobHandlerResponse handleRow(ImportJobHandlerRequest request, ImportOptions importOption) throws Exception {
        ImportJobHandlerResponse response = new ImportJobHandlerResponse();
        if (ImportOptions.UPDATE_EXISTING == importOption) {
            UpdateShelfRequest updateShelfRequest = prepareUpdateShelfRequest(request);
            response.addResponse(shelfService.updateShelf(updateShelfRequest));

        } else if (ImportOptions.CREATE_NEW == importOption) {
            CreateShelfRequest createShelfRequest = prepareCreateShelfRequest(request);
            response.addResponse(shelfService.createShelf(createShelfRequest));
        } else if (ImportOptions.CREATE_NEW_AND_UPDATE_EXISTING == importOption) {
            if (shelfService.getShelfByCode(request.getRow().getColumnValue(SHELF_CODE)) == null) {
                CreateShelfRequest createShelfRequest = prepareCreateShelfRequest(request);
                response.addResponse(shelfService.createShelf(createShelfRequest));
            } else {
                UpdateShelfRequest updateShelfRequest = prepareUpdateShelfRequest(request);
                response.addResponse(shelfService.updateShelf(updateShelfRequest));
            }
        } else {
            ServiceResponse response1 = new ServiceResponse();
            response1.addError(new WsError("Operation Not Allowed"));
            response.addResponse(response1);
        }
        return response;
    }

    private UpdateShelfRequest prepareUpdateShelfRequest(ImportJobHandlerRequest request) {
        UpdateShelfRequest updateShelfRequest = new UpdateShelfRequest();
        updateShelfRequest.setShelfCode(request.getRow().getColumnValue(SHELF_CODE));
        updateShelfRequest.setShelfTypeCode(request.getRow().getColumnValue(SHELF_TYPE_CODE));
        if (StringUtils.isNotBlank(request.getRow().getColumnValue(ENABLED))) {
            updateShelfRequest.setEnabled(StringUtils.parseBoolean(request.getRow().getColumnValue(ENABLED)));
        }
        return updateShelfRequest;
    }

    private CreateShelfRequest prepareCreateShelfRequest(ImportJobHandlerRequest request) {
        CreateShelfRequest createShelfRequest = new CreateShelfRequest();
        createShelfRequest.setShelfCode(request.getRow().getColumnValue(SHELF_CODE));
        createShelfRequest.setShelfTypeCode(request.getRow().getColumnValue(SHELF_TYPE_CODE));
        createShelfRequest.setSectionCode(request.getRow().getColumnValue(SECTION_CODE));
        if (StringUtils.isNotBlank(request.getRow().getColumnValue(ENABLED))) {
            createShelfRequest.setEnabled(StringUtils.parseBoolean(request.getRow().getColumnValue(ENABLED)));
        }
        return createShelfRequest;
    }

    @Override
    public void preProcessor(ImportJob importJob) {

    }

    @Override
    public void postProcessor(ImportJob importJob) {

    }
}
