/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 12, 2012
 *  @author praveeng
 */
package com.uniware.services.imports;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;

import com.unifier.core.api.validation.WsError;
import com.unifier.core.entity.ImportJob;
import com.unifier.core.entity.ImportJobType.ImportOptions;
import com.unifier.core.fileparser.Row;
import com.unifier.core.utils.StringUtils;
import com.unifier.services.imports.ImportJobHandler;
import com.unifier.services.imports.ImportJobHandlerRequest;
import com.unifier.services.imports.ImportJobHandlerResponse;
import com.uniware.core.api.shipping.PaymentReconciliationRequest;
import com.uniware.dao.shipping.IPaymentReconciliationService;

public class PaymentReconciliationImportJobHandler implements ImportJobHandler {

    private static final String           SHIPPING_PACKAGE_CODE    = "Shipping Package Code";
    private static final String           TRACKING_NUMBER          = "Tracking Number";
    private static final String           SHIPPING_PROVIDER_CODE   = "Shipping Provider Code";
    private static final String           COLLECTED_AMOUNT         = "Collected Amount";
    private static final String           PAYMENT_REFERENCE_NUMBER = "Payment Reference Number";

    @Autowired
    private IPaymentReconciliationService paymentReconciliationService;

    @Override
    public ImportJobHandlerResponse handleRow(ImportJobHandlerRequest request, ImportOptions importOption) throws Exception {
        ImportJobHandlerResponse response = new ImportJobHandlerResponse();
        Row row = request.getRow();

        PaymentReconciliationRequest reconciliationRequest = new PaymentReconciliationRequest();
        reconciliationRequest.setShippingPackageCode(row.getColumnValue(SHIPPING_PACKAGE_CODE));
        reconciliationRequest.setShippingProviderCode(row.getColumnValue(SHIPPING_PROVIDER_CODE));
        reconciliationRequest.setTrackingNumber(row.getColumnValue(TRACKING_NUMBER));
        if (row.getColumnValue(PAYMENT_REFERENCE_NUMBER) != null) {
            reconciliationRequest.setPaymentReferenceNumber(row.getColumnValue(PAYMENT_REFERENCE_NUMBER));
        }

        String collectedAmount = row.getColumnValue(COLLECTED_AMOUNT);
        if (StringUtils.isNotBlank(collectedAmount)) {
            try {
                reconciliationRequest.setCollectedAmount(new BigDecimal(collectedAmount));
            } catch (NumberFormatException e) {
                response.addError(new WsError("Invalid value for 'collected amount"));
            }
        }

        if (!response.hasErrors()) {
            response.addResponse(paymentReconciliationService.reconcileShipmentPayment(reconciliationRequest));
        }
        return response;

    }

    @Override
    public void preProcessor(ImportJob importJob) {
    }

    @Override
    public void postProcessor(ImportJob importJob) {
    }

}
