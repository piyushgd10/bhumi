/*
 *  Copyright 2013 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 11-Oct-2013
 *  @author parijat
 */
package com.uniware.services.imports;

import org.springframework.beans.factory.annotation.Autowired;

import com.unifier.core.api.validation.WsError;
import com.unifier.core.entity.ImportJob;
import com.unifier.core.entity.ImportJobType.ImportOptions;
import com.unifier.services.imports.ImportJobHandler;
import com.unifier.services.imports.ImportJobHandlerRequest;
import com.unifier.services.imports.ImportJobHandlerResponse;
import com.uniware.core.api.purchase.AddUpdateReorderConfigRequest;
import com.uniware.services.catalog.ICatalogService;
import com.uniware.services.purchase.IPurchaseService;

public class ReorderConfigImportJobHandler implements ImportJobHandler {
    
    private static final String ITEM_SKU = "Product SKU";
    private static final String REORDER_QUANTITY = "Reorder Quantity";
    private static final String REORDER_THRESHOLD = "Reorder Threshold";
    
    @Autowired
    private ICatalogService catalogService;

    @Autowired
    private IPurchaseService purchaseService;
    
    @Override
    public ImportJobHandlerResponse handleRow(ImportJobHandlerRequest request, ImportOptions importOption) {
        ImportJobHandlerResponse response = new ImportJobHandlerResponse();
        AddUpdateReorderConfigRequest createUpdateRequest = new AddUpdateReorderConfigRequest();
        createUpdateRequest.setItemSkuCode(request.getRow().getColumnValue(ITEM_SKU));
        try {
            createUpdateRequest.setReorderQuantity(Integer.parseInt(request.getRow().getColumnValue(REORDER_QUANTITY)));
        } catch (NumberFormatException e) {
            response.addError(new WsError("Invalid value for " + REORDER_QUANTITY));
        }
        try {
            createUpdateRequest.setReorderThreshold(Integer.parseInt(request.getRow().getColumnValue(REORDER_THRESHOLD)));
        } catch (NumberFormatException e) {
            response.addError(new WsError("Invalid value for " + REORDER_THRESHOLD));
        }
        if(!response.hasErrors()) {
            response.addResponse(purchaseService.persistReorderConfig(createUpdateRequest ));
        }
        return response;
    }

    @Override
    public void preProcessor(ImportJob importJob) {
    }

    @Override
    public void postProcessor(ImportJob importJob) {
        // TODO Auto-generated method stub

    }

}
