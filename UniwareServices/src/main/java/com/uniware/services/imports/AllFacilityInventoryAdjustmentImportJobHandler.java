/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Nov 1, 2012
 *  @author singla
 */
package com.uniware.services.imports;

import com.unifier.core.entity.User;
import com.unifier.services.users.IUsersService;
import com.uniware.core.api.inventory.InventoryAdjustmentRequest;
import com.uniware.core.api.inventory.InventoryAdjustmentResponse;
import org.springframework.beans.factory.annotation.Autowired;

import com.unifier.core.api.validation.WsError;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.entity.ImportJob;
import com.unifier.core.entity.ImportJobType.ImportOptions;
import com.unifier.core.fileparser.Row;
import com.unifier.core.utils.StringUtils;
import com.unifier.scraper.sl.runtime.ScraperScript;
import com.unifier.scraper.sl.runtime.ScriptExecutionContext;
import com.unifier.services.imports.IImportService;
import com.unifier.services.imports.ImportJobHandler;
import com.unifier.services.imports.ImportJobHandlerRequest;
import com.unifier.services.imports.ImportJobHandlerResponse;
import com.unifier.services.tenantprofile.service.ITenantProfileService;
import com.unifier.services.vo.TenantProfileVO;
import com.uniware.core.api.inventory.WsInventoryAdjustment;
import com.uniware.core.cache.FacilityCache;
import com.uniware.core.entity.Facility;
import com.uniware.core.entity.InventoryAdjustment.AdjustmentType;
import com.uniware.core.entity.ItemTypeInventory;
import com.uniware.core.utils.UserContext;
import com.uniware.services.cache.ScriptVersionedCache;
import com.uniware.services.inventory.IInventoryService;

public class AllFacilityInventoryAdjustmentImportJobHandler implements ImportJobHandler {

    private static final String   ITEM_TYPE_SKU_CODE     = "Product Code";
    private static final String   QUANTITY               = "Quantity";
    private static final String   SHELF_CODE             = "Shelf Code";
    private static final String   FACILITY_CODE          = "Facility Code";
    private static final String   TRANSFER_TO_SHELF_CODE = "Transfer to Shelf Code";
    private static final String   ADJUSTMENT_TYPE        = "Adjustment Type";
    private static final String   INVENTORY_TYPE         = "Inventory Type";
    private static final String   SLA                    = "Sla";
    private static final String   REMARKS                = "Remarks";

    @Autowired
    private IInventoryService     inventoryService;

    @Autowired
    private ITenantProfileService tenantProfileService;

    @Autowired
    private IImportService        importService;

    @Autowired
    private IUsersService usersService;

    @Override
    public ImportJobHandlerResponse handleRow(ImportJobHandlerRequest request, ImportOptions importOption) throws Exception {
        ImportJobHandlerResponse response = new ImportJobHandlerResponse();
        Row row = request.getRow();
        WsInventoryAdjustment inventoryAdjustment = new WsInventoryAdjustment();
        inventoryAdjustment.setItemSKU(row.getColumnValue(ITEM_TYPE_SKU_CODE));
        inventoryAdjustment.setShelfCode(row.getColumnValue(SHELF_CODE));
        inventoryAdjustment.setTransferToShelfCode(row.getColumnValue(TRANSFER_TO_SHELF_CODE));
        try {
            inventoryAdjustment.setQuantity(Integer.parseInt(row.getColumnValue(QUANTITY)));
        } catch (NumberFormatException e) {
            response.addError(new WsError("Invalid value for 'quantity'"));
        }
        try {
            inventoryAdjustment.setAdjustmentType(AdjustmentType.valueOf(row.getColumnValue(ADJUSTMENT_TYPE).toUpperCase()));
        } catch (IllegalArgumentException e) {
            response.addError(new WsError("Invalid value for 'Adjustment Type'"));
        }
        if (StringUtils.isNotBlank(row.getColumnValue(SLA))) {
            try {
                inventoryAdjustment.setSla(Integer.parseInt(row.getColumnValue(SLA)));
            } catch (NumberFormatException e) {
                response.addError(new WsError("Invalid value for 'sla'"));
            }
        }
        if (StringUtils.isNotBlank(row.getColumnValue(INVENTORY_TYPE))) {
            try {
                inventoryAdjustment.setInventoryType(ItemTypeInventory.Type.valueOf(row.getColumnValue(INVENTORY_TYPE).toUpperCase()));
            } catch (IllegalArgumentException e) {
                response.addError(new WsError("Invalid value for 'Inventory Type'"));
            }
        }
        if (StringUtils.isNotBlank(row.getColumnValue(REMARKS))) {
            inventoryAdjustment.setRemarks(row.getColumnValue(REMARKS));
        }
        String facilityCode = row.getColumnValue(FACILITY_CODE);
        Facility facility = CacheManager.getInstance().getCache(FacilityCache.class).getFacilityByCode(facilityCode);
        if (facility == null) {
            response.addError(new WsError("Invalid value for 'Facility Code'"));
        }
        Facility currentFacility = UserContext.current().getFacility();
        try {
            UserContext.current().setFacility(facility);
            InventoryAdjustmentResponse adjustmentResponse = inventoryService.adjustInventory(new InventoryAdjustmentRequest(inventoryAdjustment , request.getUserId()));
            response.addErrors(adjustmentResponse.getErrors());
        } finally {
            UserContext.current().setFacility(currentFacility);
        }
        response.setSuccessful(!response.hasErrors());
        return response;
    }

    @Override
    public void preProcessor(ImportJob importJob) {
        // TODO Auto-generated method stub

    }

    @Override
    public void postProcessor(ImportJob importJob) {

    }

}
