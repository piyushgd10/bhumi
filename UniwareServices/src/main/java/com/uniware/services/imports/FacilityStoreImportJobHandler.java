/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 26-Aug-2015
 *  @author akshay
 */
package com.uniware.services.imports;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.unifier.core.cache.CacheManager;
import com.uniware.core.cache.FacilityCache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.unifier.core.api.user.AddUserRolesRequest;
import com.unifier.core.api.user.CreateUserRequest;
import com.unifier.core.api.user.CreateUserResponse;
import com.unifier.core.api.validation.WsError;
import com.unifier.core.entity.ImportJob;
import com.unifier.core.entity.ImportJobType;
import com.unifier.core.entity.ImportJobType.ImportOptions;
import com.unifier.core.entity.User;
import com.unifier.core.fileparser.Row;
import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.StringUtils;
import com.unifier.services.imports.ImportJobHandler;
import com.unifier.services.imports.ImportJobHandlerRequest;
import com.unifier.services.imports.ImportJobHandlerResponse;
import com.unifier.services.users.IUsersService;
import com.unifier.services.utils.CustomFieldUtils;
import com.uniware.core.api.facility.CreateFacilityProfileRequest;
import com.uniware.core.api.facility.CreateFacilityRequest;
import com.uniware.core.api.facility.CreateFacilityResponse;
import com.uniware.core.api.party.WsFacility;
import com.uniware.core.api.party.WsFacilityProfile;
import com.uniware.core.api.party.WsPartyAddress;
import com.uniware.core.api.party.WsPartyContact;
import com.uniware.core.api.party.WsStoreTimings;
import com.uniware.core.api.warehouse.FacilityDTO;
import com.uniware.core.api.warehouse.GetFacilityRequest;
import com.uniware.core.entity.Facility;
import com.uniware.core.entity.FacilityProfile.DayTiming.DayOfWeek;
import com.uniware.core.entity.Location;
import com.uniware.core.entity.PartyAddressType;
import com.uniware.core.entity.PartyContactType;
import com.uniware.core.utils.UserContext;
import com.uniware.services.location.ILocationService;
import com.uniware.services.party.IPartyService;
import com.uniware.services.warehouse.IFacilityService;

public class FacilityStoreImportJobHandler implements ImportJobHandler {

    private static final Logger LOG                         = LoggerFactory.getLogger(FacilityImportJobHandler.class);

    private static final String FACILITY_NAME               = "Store Name";
    private static final String FACILITY_DISPLAY_NAME       = "Store Display Name";
    private static final String FACILITY_CODE               = "Store Code";
    private static final String FACILITY_ALTERNATE_CODE     = "Seller Store Code";
    private static final String FACILITY_ENABLED            = "Enabled";
    private static final String FACILITY_PAN                = "PAN";
    private static final String FACILITY_TIN                = "TIN";
    private static final String FACILITY_CST                = "CST Number";
    private static final String FACILITY_CIN                = "CIN Number";
    private static final String FACILITY_ST                 = "Service Tax Number";
    private static final String FACILITY_WEBSITE            = "Website URL";
    private static final String DEFAULT_OPENING_TIME        = "08:00";
    private static final String DEFAULT_CLOSING_TIME        = "20:00";

    //address details for shipping List<WsAddressDetail> in WsSaleOrder
    private static final String SHIPPING_ADDRESS_LINE1      = "Shipping Address Line1";
    private static final String SHIPPING_ADDRESS_LINE2      = "Shipping Address Line2";
    private static final String SHIPPING_ADDRESS_CITY       = "Shipping Address City";
    private static final String SHIPPING_ADDRESS_STATE      = "Shipping Address State";
    private static final String SHIPPING_ADDRESS_COUNTRY    = "Shipping Address Country";
    private static final String SHIPPING_ADDRESS_PINCODE    = "Shipping Address Pincode";
    private static final String SHIPPING_ADDRESS_PHONE      = "Shipping Address Phone";

    //address details for  billing List<WsAddressDetail> in WsSaleOrder
    private static final String BILLING_SAME_AS_SHIPPING    = "Billing Same As Shipping";
    private static final String BILLING_ADDRESS_LINE1       = "Billing Address Line1";
    private static final String BILLING_ADDRESS_LINE2       = "Billing Address Line2";
    private static final String BILLING_ADDRESS_CITY        = "Billing Address City";
    private static final String BILLING_ADDRESS_STATE       = "Billing Address State";
    private static final String BILLING_ADDRESS_COUNTRY     = "Billing Address Country";
    private static final String BILLING_ADDRESS_PINCODE     = "Billing Address Pincode";
    private static final String BILLING_ADDRESS_PHONE       = "Billing Address Phone";

    // USer details
    private static final String NAME                        = "Name";
    private static final String USERNAME                    = "Username";
    private static final String PASSWORD                    = "Password";
    private static final String ROLE                        = "User Role";
    private static final String USER_EMAIL                  = "Email";

    // Roles to preserve
    private static final String ROLES_TO_PRESERVE           = "Roles To Preserve";

    // Facility Profile: Required for store type facility
    private static final String SELLER_NAME                 = "Store Manager Name";
    private static final String SELLER_MOBILE               = "Store Manager Mobile";
    private static final String SELLER_EMAIL                = "Store Manager Email";
    private static final String SELLER_ALTERNATE_PHONE      = "Alternate Phone";

    private static final String WEEKLY_CLOSED_DAYS          = "Weekly Closed Days";
    private static final String STORE_OPENING_TIME          = "Store Opening Time";
    private static final String STORE_CLOSING_TIME          = "Store Closing Time";

    private static final String AUTO_SETUP_SERVICEABILITY   = "Auto Setup Serviceability";
    private static final String DELIVERY_RADIUS             = "Delivery Radius";
    private static final String STORE_PICKUP_ENABLED        = "Store pick Enabled";
    private static final String PICK_UP_SHIPPING_METHODS    = "Pick up Shipping Methods";
    private static final String STORE_SELF_DELIVERY_ENABLED = "Store Self Delivery Enabled";
    private static final String STORE_SELF_SHIPPING_METHODS = "Store Self Shipping Methods";
    private static final String DISPATCH_SLA                = "Dispatch SLA";
    private static final String DELIVERY_SLA                = "Delivery SLA";

    @Autowired
    private IFacilityService    facilityService;

    @Autowired
    private IUsersService       userService;

    @Autowired
    private IPartyService       partyService;

    @Autowired
    private ILocationService    locationService;

    @Override
    public ImportJobHandlerResponse handleRow(ImportJobHandlerRequest request, ImportOptions importOption) throws Exception {
        ImportJobHandlerResponse response = new ImportJobHandlerResponse();
        String facilityCode = request.getRow().getColumnValue(FACILITY_CODE);
        GetFacilityRequest req = new GetFacilityRequest();
        req.setCode(facilityCode);
        FacilityDTO facility = facilityService.getFacilityByCode(req).getFacilityDTO();

        if (ImportJobType.ImportOptions.CREATE_NEW == importOption) {
            if (facility == null) {
                response = createFacility(request);
            } else {
                response.addError(new WsError("Facility " + facilityCode + " already exists"));
            }
        } 

        if (response.isSuccessful() && request.getRow().getColumnValue(USERNAME) != null) {
            Facility f = CacheManager.getInstance().getCache(FacilityCache.class).getFacilityById((Integer) response.getResultItems().get("facilityId"));
            UserContext.current().setFacility(f);
            User user = userService.getUserByUsername(request.getRow().getColumnValue(USERNAME));
            if (user == null) {
                response.addResponse(createUser(request));
            }
        }
        return response;
    }

    private CreateUserResponse createUser(ImportJobHandlerRequest request) {
        CreateUserRequest createUserRequest = new CreateUserRequest();
        createUserRequest.setName(request.getRow().getColumnValue(NAME));
        createUserRequest.setUsername(request.getRow().getColumnValue(USERNAME));
        createUserRequest.setPassword(request.getRow().getColumnValue(PASSWORD));
        createUserRequest.setConfirmPassword(request.getRow().getColumnValue(PASSWORD));
        createUserRequest.setEmail(request.getRow().getColumnValue(USER_EMAIL));
        createUserRequest.addRole(request.getRow().getColumnValue(ROLE));
        return userService.createUser(createUserRequest);
    }

    private ImportJobHandlerResponse createFacility(ImportJobHandlerRequest request) {
        ImportJobHandlerResponse response = new ImportJobHandlerResponse();
        Row row = request.getRow();

        WsFacility wsFacility = prepareWsFacility(row, response);
        WsFacilityProfile profile = prepareWsFacilityProfile(row, response);
        if (!response.hasErrors()) {
            CreateFacilityRequest createFacilityRequest = new CreateFacilityRequest();
            createFacilityRequest.setFacility(wsFacility);
            createFacilityRequest.setUsername(userService.getUserWithDetailsById(request.getUserId()).getUsername());
            CreateFacilityResponse createFacilityResponse = null;
            try {
                createFacilityResponse = facilityService.createFacility(createFacilityRequest);
                if (createFacilityResponse.isSuccessful()) {
                    CreateFacilityProfileRequest profileRequest = new CreateFacilityProfileRequest();
                    profileRequest.setFacilityProfile(profile);
                    createFacilityResponse = facilityService.createFacilityProfile(profileRequest);
                    response.addResponse(createFacilityResponse);
                } else {
                    response.addResponse(createFacilityResponse);
                }
            } catch (Exception e) {
                LOG.error("Error creating facility: ", e);
                response.addError(new WsError(e.getMessage()));
            }

            if (createFacilityResponse != null && !createFacilityResponse.hasErrors()) {
                response.addResultItem("facilityId", createFacilityResponse.getFacilityDTO().getId());

                // See if we need to preserve roles in this facility.
                if (StringUtils.isNotEmpty(row.getColumnValue(ROLES_TO_PRESERVE))) {
                    for (String roleCode : row.getColumnValue(ROLES_TO_PRESERVE).split(",")) {
                        List<String> usernames = userService.getUsersByRole(roleCode);
                        for (String username : usernames) {
                            AddUserRolesRequest addRoleRequest = new AddUserRolesRequest();
                            addRoleRequest.setUsername(username);
                            addRoleRequest.addFacilityCode(createFacilityResponse.getFacilityDTO().getCode());
                            addRoleRequest.addFacilityRoleCode(roleCode);
                            userService.addUserRoles(addRoleRequest);
                        }
                    }
                }

            }
        }
        return response;
    }

    private WsFacilityProfile prepareWsFacilityProfile(Row row, ImportJobHandlerResponse response) {
        WsFacilityProfile wsProfile = new WsFacilityProfile();
        wsProfile.setFacilityCode(row.getColumnValue(FACILITY_CODE));
        wsProfile.setSellerName(row.getColumnValue(SELLER_NAME));
        wsProfile.setContactEmail(row.getColumnValue(SELLER_EMAIL));
        wsProfile.setMobile(row.getColumnValue(SELLER_MOBILE));
        wsProfile.setAlternatePhone(row.getColumnValue(SELLER_ALTERNATE_PHONE));
        Location location = locationService.getLocationByPincode(row.getColumnValue(SHIPPING_ADDRESS_PINCODE));
        if (location == null) {
            response.addError(new WsError("No position co-ordinates found for given pincode " + row.getColumnValue(SHIPPING_ADDRESS_PINCODE)));
        } else {
            List<Double> position = new ArrayList<>();
            for (double coordinate : location.getPosition()) {
                position.add(Double.valueOf(coordinate));
            }
            wsProfile.setPosition(position);
            wsProfile.setAutoSetupServiceability("1".equals(row.getColumnValue(AUTO_SETUP_SERVICEABILITY)) ? true : false);
            wsProfile.setStoreDeliveryEnabled("1".equals(row.getColumnValue(STORE_SELF_DELIVERY_ENABLED)) ? true : false);
            if (StringUtils.isNotBlank(row.getColumnValue(DELIVERY_RADIUS))) {
                try {
                    wsProfile.setDeliveryRadius(Double.valueOf(row.getColumnValue(DELIVERY_RADIUS)));
                } catch (Exception e) {
                    response.addError(new WsError("Invalid value for delivery radius"));
                }
            } else {
                wsProfile.setDeliveryRadius(new Double(10.0));
            }
        }
        wsProfile.setStorePickupEnabled("1".equals(row.getColumnValue(STORE_PICKUP_ENABLED)) ? true : false);
        if (!response.hasErrors()) {
            if (wsProfile.getStorePickupEnabled()) {
                if (StringUtils.isNotBlank(row.getColumnValue(PICK_UP_SHIPPING_METHODS))) {
                    List<String> methods = new ArrayList<>();
                    for (String method : row.getColumnValue(PICK_UP_SHIPPING_METHODS).split(",")) {
                        methods.add(method.trim());
                    }
                    wsProfile.setPickupShippingMethods(methods);
                } else {
                    response.addError(new WsError("Please fill valid values for pick up shipping methods. Pickup-COD,Pickup-Prepaid"));
                }
            } else {
                wsProfile.setPickupShippingMethods(new ArrayList<String>());
            }
        }

        if (!response.hasErrors()) {
            if (wsProfile.getStoreDeliveryEnabled()) {
                if (StringUtils.isNotBlank(row.getColumnValue(STORE_SELF_SHIPPING_METHODS))) {
                    List<String> methods = new ArrayList<>();
                    for (String method : row.getColumnValue(STORE_SELF_SHIPPING_METHODS).split(",")) {
                        methods.add(method.trim());
                    }
                    wsProfile.setSelfShippingMethods(methods);
                } else {
                    response.addError(new WsError("Please fill valid values for pick up shipping methods. Standard-COD,Standard-Prepaid"));
                }
            } else {
                wsProfile.setSelfShippingMethods(new ArrayList<String>());
            }
        }
        if (!response.hasErrors()) {
            if (StringUtils.isNotBlank(row.getColumnValue(DISPATCH_SLA))) {
                try {
                    wsProfile.setDispatchSLA(Integer.valueOf(row.getColumnValue(DISPATCH_SLA)).intValue());
                } catch (Exception e) {
                    response.addError(new WsError("Dispatch SLA invalid"));
                }
            } else {
                response.addError(new WsError("Dispatch SLA is mandatory"));
            }
        }
        if (!response.hasErrors()) {
            if (StringUtils.isNotBlank(row.getColumnValue(DELIVERY_SLA))) {
                try {
                    wsProfile.setDeliverySLA(Integer.valueOf(row.getColumnValue(DELIVERY_SLA)).intValue());
                } catch (Exception e) {
                    response.addError(new WsError("Delivery SLA invalid"));
                }
            } else {
                response.addError(new WsError("Delivery sla is mandatory"));
            }
        }

        if (!response.hasErrors()) {
            List<WsStoreTimings> timings = new ArrayList<>();
            List<String> weeklyOff = new ArrayList<>();
            if (StringUtils.isNotBlank(row.getColumnValue(WEEKLY_CLOSED_DAYS))) {
                for (String offDay : row.getColumnValue(WEEKLY_CLOSED_DAYS).split(",")) {
                    weeklyOff.add(offDay.trim().toUpperCase());
                }
            }
            String openingTime = DEFAULT_OPENING_TIME;
            String closingTime = DEFAULT_CLOSING_TIME;
            if (StringUtils.isNotBlank(row.getColumnValue(STORE_OPENING_TIME))) {
                try {
                    Date openingDate = DateUtils.stringToDate(row.getColumnValue(STORE_OPENING_TIME), "HH:mm");
                    openingTime = DateUtils.dateToString(openingDate, "HH:mm");
                } catch (Exception e) {
                    response.addError(new WsError("Invalid opening time format,it should be hh:mm"));
                }
            }
            if (StringUtils.isNotBlank(row.getColumnValue(STORE_CLOSING_TIME))) {
                try {
                    Date closingDate = DateUtils.stringToDate(row.getColumnValue(STORE_CLOSING_TIME), "HH:mm");
                    closingTime = DateUtils.dateToString(closingDate, "HH:mm");
                } catch (Exception e) {
                    response.addError(new WsError("Invalid closing time format,it should be hh:mm"));
                }
            }
            for (DayOfWeek day : DayOfWeek.values()) {
                if (!weeklyOff.contains(day.name())) {
                    WsStoreTimings storeTiming = new WsStoreTimings();
                    storeTiming.setDayOfWeek(day.name());
                    storeTiming.setClosingTime(openingTime);
                    storeTiming.setOpeningTime(closingTime);
                    timings.add(storeTiming);
                }
            }
            wsProfile.setStoreTimings(timings);
        }

        return wsProfile;
    }

    private WsFacility prepareWsFacility(Row row, ImportJobHandlerResponse response) {
        WsFacility wsFacility = new WsFacility();
        wsFacility.setType(Facility.Type.STORE.name());
        wsFacility.setDisplayName(row.getColumnValue(FACILITY_DISPLAY_NAME));
        wsFacility.setCode(row.getColumnValue(FACILITY_CODE));
        wsFacility.setAlternateCode(row.getColumnValue(FACILITY_ALTERNATE_CODE));
        wsFacility.setEnabled("0".equals(row.getColumnValue(FACILITY_ENABLED)) ? false : true);
        wsFacility.setCstNumber(row.getColumnValue(FACILITY_CST));
        wsFacility.setCinNumber(row.getColumnValue(FACILITY_CIN));
        wsFacility.setName(row.getColumnValue(FACILITY_NAME));
        wsFacility.setPan(row.getColumnValue(FACILITY_PAN));
        wsFacility.setStNumber(row.getColumnValue(FACILITY_ST));
        wsFacility.setTin(row.getColumnValue(FACILITY_TIN));
        wsFacility.setWebsite(row.getColumnValue(FACILITY_WEBSITE));
        wsFacility.setCustomFieldValues(CustomFieldUtils.getCustomFieldValues(response, Facility.class.getName(), row));
        wsFacility.getPartyContacts().add(preparePartyContact(row, response));
        wsFacility.setShippingAddress(preparePartyShippingAddress(row, response));
        wsFacility.setBillingAddress(preparePartyBillingAddress(row, response, wsFacility.getShippingAddress()));
        return wsFacility;
    }

    private WsPartyContact preparePartyContact(Row row, ImportJobHandlerResponse response) {
        WsPartyContact wsPartyContact = new WsPartyContact();
        wsPartyContact.setContactType(PartyContactType.Code.PRIMARY.name());
        wsPartyContact.setEmail(row.getColumnValue(SELLER_EMAIL));
        wsPartyContact.setName(row.getColumnValue(SELLER_NAME));
        wsPartyContact.setPartyCode(row.getColumnValue(FACILITY_CODE));
        wsPartyContact.setPhone(row.getColumnValue(SELLER_MOBILE));
        return wsPartyContact;
    }

    private WsPartyAddress preparePartyBillingAddress(Row row, ImportJobHandlerResponse response, WsPartyAddress wsPartyShippingAddress) {
        WsPartyAddress wsPartyBillingAddress = new WsPartyAddress();
        if (StringUtils.isNotBlank(row.getColumnValue(BILLING_SAME_AS_SHIPPING)) && !("1".equals(row.getColumnValue(BILLING_SAME_AS_SHIPPING)))) {
            wsPartyBillingAddress.setAddressLine1(row.getColumnValue(BILLING_ADDRESS_LINE1));
            wsPartyBillingAddress.setAddressLine2(row.getColumnValue(BILLING_ADDRESS_LINE2));
            wsPartyBillingAddress.setCity(row.getColumnValue(BILLING_ADDRESS_CITY));
            if (StringUtils.isNotBlank(row.getColumnValue(BILLING_ADDRESS_COUNTRY))) {
                wsPartyBillingAddress.setCountryCode(row.getColumnValue(BILLING_ADDRESS_COUNTRY));
            }
            wsPartyBillingAddress.setStateCode(row.getColumnValue(BILLING_ADDRESS_STATE));
            wsPartyBillingAddress.setPartyCode(row.getColumnValue(FACILITY_CODE));
            wsPartyBillingAddress.setAddressType(PartyAddressType.Code.BILLING.name());
            wsPartyBillingAddress.setPincode(row.getColumnValue(BILLING_ADDRESS_PINCODE));
            wsPartyBillingAddress.setPhone(row.getColumnValue(BILLING_ADDRESS_PHONE));
        } else {
            wsPartyBillingAddress = wsPartyShippingAddress;
        }
        return wsPartyBillingAddress;
    }

    private WsPartyAddress preparePartyShippingAddress(Row row, ImportJobHandlerResponse response) {
        WsPartyAddress wsPartyAddress = new WsPartyAddress();
        wsPartyAddress.setAddressLine1(row.getColumnValue(SHIPPING_ADDRESS_LINE1));
        wsPartyAddress.setAddressLine2(row.getColumnValue(SHIPPING_ADDRESS_LINE2));
        wsPartyAddress.setCity(row.getColumnValue(SHIPPING_ADDRESS_CITY));
        if (StringUtils.isNotBlank(row.getColumnValue(SHIPPING_ADDRESS_COUNTRY))) {
            wsPartyAddress.setCountryCode(row.getColumnValue(SHIPPING_ADDRESS_COUNTRY));
        }
        wsPartyAddress.setStateCode(row.getColumnValue(SHIPPING_ADDRESS_STATE));
        wsPartyAddress.setPartyCode(row.getColumnValue(FACILITY_CODE));
        wsPartyAddress.setAddressType(PartyAddressType.Code.SHIPPING.name());
        wsPartyAddress.setPincode(row.getColumnValue(SHIPPING_ADDRESS_PINCODE));
        wsPartyAddress.setPhone(row.getColumnValue(SHIPPING_ADDRESS_PHONE));
        return wsPartyAddress;
    }

    @Override
    public void preProcessor(ImportJob importJob) {
    }

    @Override
    public void postProcessor(ImportJob importJob) {
    }

}
