/*
 *  Copyright 2013 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 4, 2013
 *  @author praveeng
 */
package com.uniware.services.imports;

import org.springframework.beans.factory.annotation.Autowired;

import com.unifier.core.api.validation.WsError;
import com.unifier.core.entity.ImportJob;
import com.unifier.core.entity.ImportJobType.ImportOptions;
import com.unifier.services.imports.ImportJobHandler;
import com.unifier.services.imports.ImportJobHandlerRequest;
import com.unifier.services.imports.ImportJobHandlerResponse;
import com.uniware.core.api.inventory.EditReorderItemLevelRequest;
import com.uniware.services.inventory.IInventoryService;

public class ReorderItemLevelImportJobHandler implements ImportJobHandler {

    private static final String SKUCODE          = "product code";
    private static final String MIN_BIN_QUANTITY = "Minimum Bin Quantity";

    @Autowired
    private IInventoryService   inventoryService;

    @Override
    public ImportJobHandlerResponse handleRow(ImportJobHandlerRequest request, ImportOptions importOption) throws Exception {
        ImportJobHandlerResponse response = new ImportJobHandlerResponse();
        String skuCode = request.getRow().getColumnValue(SKUCODE);

        EditReorderItemLevelRequest editReoderLevelRequest = new EditReorderItemLevelRequest();
        String minBinQuantity = request.getRow().getColumnValue(MIN_BIN_QUANTITY);
        try {
            editReoderLevelRequest.setMinBinSize(Integer.parseInt(minBinQuantity));
        } catch (NumberFormatException e) {
            response.addError(new WsError("Invalid Minimum Bin Quantity"));
        }
        editReoderLevelRequest.setSkuCode(skuCode);

        response.addResponse(inventoryService.editReorderItemLevel(editReoderLevelRequest));
        return response;
    }

    @Override
    public void preProcessor(ImportJob importJob) {

    }

    @Override
    public void postProcessor(ImportJob importJob) {
    }

}
