/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 22-Jun-2012
 *  @author praveeng
 */
package com.uniware.services.imports;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;

import com.unifier.core.api.validation.WsError;
import com.unifier.core.entity.ImportJob;
import com.unifier.core.entity.ImportJobType.ImportOptions;
import com.unifier.core.fileparser.Row;
import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.StringUtils;
import com.unifier.services.imports.ImportJobHandler;
import com.unifier.services.imports.ImportJobHandlerRequest;
import com.unifier.services.imports.ImportJobHandlerResponse;
import com.uniware.core.api.shipping.UpdateTrackingStatusRequest;
import com.uniware.core.api.shipping.UpdateTrackingStatusResponse;
import com.uniware.services.shipping.IShipmentTrackingService;
import com.uniware.services.shipping.IShippingService;

public class ShipmentTrackingStatusImportJobHandler implements ImportJobHandler {

    private static final String      SHIPPING_PROVIDER_CODE = "shipping provider code";
    private static final String      TRACKING_NUMBER        = "awb number";
    private static final String      STATUS                 = "tracking status";
    private static final String      STATUS_DATE            = "Status Date as dd/MM/yyyy HH:mm:ss";

    @Autowired
    private IShipmentTrackingService shipmentTrackingService;

    @Autowired
    private IShippingService         shippingService;

    @Override
    public ImportJobHandlerResponse handleRow(ImportJobHandlerRequest request, ImportOptions importOption) throws Exception {
        ImportJobHandlerResponse response = new ImportJobHandlerResponse();
        Row row = request.getRow();
        String shippingProviderCode = row.getColumnValue(SHIPPING_PROVIDER_CODE);
        if (StringUtils.isBlank(shippingProviderCode)) {
            response.addError(new WsError("'Shipping Provider Code' may not be empty"));
        }

        String trackingNumber = row.getColumnValue(TRACKING_NUMBER);
        if (StringUtils.isBlank(trackingNumber)) {
            response.addError(new WsError("'awb number' may not be empty"));
        }

        String trackingStatus = row.getColumnValue(STATUS);
        if (StringUtils.isBlank(trackingStatus)) {
            response.addError(new WsError("'tracking status' may not be empty"));
        }

        Date statusDate = DateUtils.stringToDate(row.getColumnValue(STATUS_DATE), "dd/MM/yyyy HH:mm:ss");
        if (statusDate == null) {
            response.addError(new WsError("Invalid value for 'status date'"));
        }

        if (!response.hasErrors()) {
            UpdateTrackingStatusRequest statusRequest = new UpdateTrackingStatusRequest();
            statusRequest.setProviderCode(shippingProviderCode);
            statusRequest.setStatusDate(statusDate);
            statusRequest.setTrackingNumber(trackingNumber);
            statusRequest.setTrackingStatus(trackingStatus);
            UpdateTrackingStatusResponse statusResponse = shippingService.updateTrackingStatus(statusRequest);
            response.setSuccessful(statusResponse.isSuccessful());
            response.addErrors(statusResponse.getErrors());
        }
        return response;
    }

    @Override
    public void preProcessor(ImportJob importJob) {

    }

    @Override
    public void postProcessor(ImportJob importJob) {

    }

}
