/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 *  @version     1.0, Mar 21, 2012
 *  @author praveeng
 */
package com.uniware.services.imports;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.unifier.core.api.validation.WsError;
import com.unifier.core.entity.ImportJob;
import com.unifier.core.entity.ImportJobType;
import com.unifier.core.entity.ImportJobType.ImportOptions;
import com.unifier.core.fileparser.Row;
import com.unifier.core.utils.StringUtils;
import com.unifier.services.imports.ImportJobHandler;
import com.unifier.services.imports.ImportJobHandlerRequest;
import com.unifier.services.imports.ImportJobHandlerResponse;
import com.unifier.services.utils.CustomFieldUtils;
import com.uniware.core.api.bundle.WsComponentItemType;
import com.uniware.core.api.catalog.CreateItemTypeRequest;
import com.uniware.core.api.catalog.EditItemTypeRequest;
import com.uniware.core.api.catalog.WsItemType;
import com.uniware.core.api.inventory.ReassessInventoryForItemTypeRequest;
import com.uniware.core.entity.ItemType;
import com.uniware.core.entity.ItemType.Type;
import com.uniware.services.catalog.ICatalogService;
import com.uniware.services.inventory.IInventoryService;

public class ItemTypeImportJobHandler implements ImportJobHandler {

    private static final String CATEGORY_CODE          = "category code";
    private static final String SKUCODE                = "product code";
    private static final String NAME                   = "name";
    private static final String DESCRIPTION            = "description";
    private static final String SCAN_IDENTIFIER        = "scan identifier";
    private static final String LENGTH                 = "length (mm)";
    private static final String WIDTH                  = "width (mm)";
    private static final String HEIGHT                 = "height (mm)";
    private static final String WEIGHT                 = "weight (gms)";
    private static final String EAN                    = "ean";
    private static final String UPC                    = "upc";
    private static final String HSN_CODE               = "HSN CODE";
    private static final String ISBN                   = "isbn";
    private static final String MIN_ORDER_SIZE         = "Min Order Size";
    private static final String COLOR                  = "color";
    private static final String BRAND                  = "brand";
    private static final String SIZE                   = "size";
    private static final String TAX_TYPE_CODE          = "tax type code";
    private static final String GST_TAX_TYPE           = "gst tax type code";
    private static final String TAGS                   = "tags";
    private static final String IMAGE_URL              = "image url";
    private static final String TAT                    = "tat";
    private static final String PRODUCT_PAGE_URL       = "product page url";
    private static final String ITEM_DETAIL_FIELDS     = "item detail fields";
    private static final String MRP                    = "MRP";
    private static final String BASE_PRICE             = "base price";
    private static final String COST_PRICE             = "cost price";
    private static final String REQUIRES_CUSTOMIZATION = "requires customization";
    private static final String RESYNC_INVENTORY       = "resync inventory";
    private static final String ENABLED                = "ENABLED";
    private static final String TYPE                   = "type";
    private static final String COMPONENT_SKU_CODE     = "component product code";
    private static final String COMPONENT_QUANTITY     = "component quantity";
    private static final String COMPONENT_PRICE        = "component price";

    @Autowired
    private ICatalogService     catalogService;

    @Autowired
    private IInventoryService   inventoryService;

    @Override
    public ImportJobHandlerResponse handleRow(ImportJobHandlerRequest request, ImportOptions importOption) throws Exception {
        String skuCode = request.getRows().get(0).getColumnValue(SKUCODE);
        ItemType itemType = catalogService.getAllItemTypeBySkuCode(skuCode);
        if (ImportJobType.ImportOptions.CREATE_NEW_AND_UPDATE_EXISTING == importOption) {
            if (itemType == null) {
                return createItemType(request);
            } else {
                return editItemType(request, itemType);
            }
        } else if (ImportJobType.ImportOptions.CREATE_NEW == importOption) {
            return createItemType(request);
        } else if (ImportJobType.ImportOptions.UPDATE_EXISTING == importOption) {
            return editItemType(request, itemType);
        }
        return null;
    }

    private ImportJobHandlerResponse createItemType(ImportJobHandlerRequest request) {
        ImportJobHandlerResponse response = new ImportJobHandlerResponse();
        List<Row> rows = request.getRows();

        CreateItemTypeRequest itemTypeRequest = new CreateItemTypeRequest();
        WsItemType itemType = prepareWsItemType(response, rows, null);
        if (!response.hasErrors()) {
            itemTypeRequest.setItemType(itemType);
            response.addResponse(catalogService.createItemType(itemTypeRequest));
        }
        return response;
    }

    private WsItemType prepareWsItemType(ImportJobHandlerResponse response, List<Row> rows, ItemType itemType) {
        WsItemType wsItemType = new WsItemType();
        Row row = rows.get(0);
        Type type = ItemType.Type.SIMPLE;
        if (StringUtils.isNotBlank(row.getColumnValue(TYPE))) {
            try {
                type = Type.valueOf(row.getColumnValue(TYPE));
                wsItemType.setType(type);
            } catch (Exception e) {
                response.addError(new WsError("Invalid value for 'type'"));
            }
        }
        if (!response.hasErrors()) {
            wsItemType.setType(type);
            if (!Type.BUNDLE.equals(type) && rows.size() > 1) {
                response.addError(new WsError("Multiple rows present for sku : " + row.getColumnValue(SKUCODE)));
            }
        }
        if (!response.hasErrors()) {
            wsItemType.setName(row.getColumnValue(NAME));
            wsItemType.setSkuCode(row.getColumnValue(SKUCODE));
            wsItemType.setDescription(row.getColumnValue(DESCRIPTION));
            wsItemType.setCategoryCode(row.getColumnValue(CATEGORY_CODE));
            wsItemType.setItemDetailFieldsText(row.getColumnValue(ITEM_DETAIL_FIELDS));
            wsItemType.setEan(row.getColumnValue(EAN));
            wsItemType.setUpc(row.getColumnValue(UPC));
            wsItemType.setIsbn(row.getColumnValue(ISBN));
            // TODO: 01/09/17 @ADITYA make changes in the import xml thingy
            wsItemType.setExpirable(itemType != null ? itemType.getExpirable() : null);
            wsItemType.setShelfLife(itemType != null ? itemType.getShelfLife() : null);
            wsItemType.setHsnCode(row.getColumnValue(HSN_CODE));
            String imageUrl = row.getColumnValue(IMAGE_URL);
            if (StringUtils.isNotEmpty(imageUrl)) {
                wsItemType.setImageUrl(imageUrl);
            }
            String tat = row.getColumnValue(TAT);
            if (StringUtils.isNotEmpty(tat)) {
                try {
                    wsItemType.setTat(Integer.parseInt(tat));
                } catch (NumberFormatException e) {
                    response.addError(new WsError("Invalid value for 'tat'"));
                }
            }

            String productPageUrl = row.getColumnValue(PRODUCT_PAGE_URL);
            if (StringUtils.isNotEmpty(productPageUrl)) {
                wsItemType.setProductPageUrl(productPageUrl);
            }
            wsItemType.setProductPageUrl(row.getColumnValue(PRODUCT_PAGE_URL));

            String height = row.getColumnValue(HEIGHT);
            if (StringUtils.isNotBlank(height)) {
                try {
                    wsItemType.setHeight(Integer.parseInt(height));
                } catch (NumberFormatException e) {
                    response.addError(new WsError("Invalid value for 'height'"));
                }
            }

            String length = row.getColumnValue(LENGTH);
            if (StringUtils.isNotBlank(length)) {
                try {
                    wsItemType.setLength(Integer.parseInt(length));
                } catch (NumberFormatException e) {
                    response.addError(new WsError("Invalid value for 'length'"));
                }
            }

            String weight = row.getColumnValue(WEIGHT);
            if (StringUtils.isNotBlank(weight)) {
                try {
                    wsItemType.setWeight(Integer.parseInt(weight));
                } catch (NumberFormatException e) {
                    response.addError(new WsError("Invalid value for 'weight'"));
                }
            }

            String width = row.getColumnValue(WIDTH);
            if (StringUtils.isNotBlank(width)) {
                try {
                    wsItemType.setWidth(Integer.parseInt(width));
                } catch (NumberFormatException e) {
                    response.addError(new WsError("Invalid value for 'width'"));
                }
            }

            String minOrderSize = row.getColumnValue(MIN_ORDER_SIZE);
            if (StringUtils.isNotBlank(minOrderSize)) {
                try {
                    wsItemType.setMinOrderSize(Integer.parseInt(minOrderSize));
                } catch (NumberFormatException e) {
                    response.addError(new WsError("Invalid value for 'Min Order Size'"));
                }
            }

            String color = row.getColumnValue(COLOR);
            if (StringUtils.isNotBlank(color)) {
                wsItemType.setColor(color);
            }

            String brand = row.getColumnValue(BRAND);
            if (StringUtils.isNotBlank(brand)) {
                wsItemType.setBrand(brand);
            }

            String size = row.getColumnValue(SIZE);
            if (StringUtils.isNotBlank(size)) {
                wsItemType.setSize(size);
            }

            if (row.getColumnValue(TAGS) != null) {
                if (StringUtils.isNotBlank(row.getColumnValue(TAGS))) {
                    wsItemType.setTags(StringUtils.split(row.getColumnValue(TAGS), ","));
                } else {
                    wsItemType.setTags(Collections.<String> emptyList());
                }
            }

            if (StringUtils.isNotEmpty(row.getColumnValue(TAX_TYPE_CODE))) {
                wsItemType.setTaxTypeCode(row.getColumnValue(TAX_TYPE_CODE));
            }
            if (StringUtils.isNotEmpty(row.getColumnValue(GST_TAX_TYPE))) {
                wsItemType.setGstTaxTypeCode(row.getColumnValue(GST_TAX_TYPE));
            }

            if (StringUtils.isNotEmpty(row.getColumnValue(MRP))) {
                try {
                    wsItemType.setMaxRetailPrice(new BigDecimal(row.getColumnValue(MRP)));
                } catch (NumberFormatException e) {
                    response.addError(new WsError("Invalid value for " + MRP));
                }
            }

            if (StringUtils.isNotEmpty(row.getColumnValue(BASE_PRICE))) {
                try {
                    wsItemType.setBasePrice(new BigDecimal(row.getColumnValue(BASE_PRICE)));
                } catch (NumberFormatException e) {
                    response.addError(new WsError("Invalid value for " + BASE_PRICE));
                }
            }
            if (StringUtils.isNotEmpty(row.getColumnValue(COST_PRICE))) {
                try {
                    wsItemType.setCostPrice(new BigDecimal(row.getColumnValue(COST_PRICE)));
                } catch (NumberFormatException e) {
                    response.addError(new WsError("Invalid value for " + COST_PRICE));
                }
            }

            if (StringUtils.isNotEmpty(row.getColumnValue(REQUIRES_CUSTOMIZATION))) {
                wsItemType.setRequiresCustomization(StringUtils.parseBoolean(row.getColumnValue(REQUIRES_CUSTOMIZATION)));
            }

            if (StringUtils.isNotBlank(row.getColumnValue(ENABLED))) {
                wsItemType.setEnabled(StringUtils.parseBoolean(row.getColumnValue(ENABLED)));
            }

            if (StringUtils.isNotBlank(row.getColumnValue(SCAN_IDENTIFIER))) {
                wsItemType.setScanIdentifier(row.getColumnValue(SCAN_IDENTIFIER));
            } else {
                wsItemType.setScanIdentifier(row.getColumnValue(SKUCODE));
            }

            wsItemType.setCustomFieldValues(CustomFieldUtils.getCustomFieldValues(response, ItemType.class.getName(), row));
        }
        if (Type.BUNDLE.equals(type)) {
            List<WsComponentItemType> componentItemTypes = new ArrayList<WsComponentItemType>(rows.size());
            for (Row itemTypeRow : rows) {
                WsComponentItemType componentItemType = new WsComponentItemType();
                if (StringUtils.isBlank(itemTypeRow.getColumnValue(COMPONENT_SKU_CODE))) {
                    response.addError(new WsError("Missing component sku code"));
                } else {
                    componentItemType.setItemSku(itemTypeRow.getColumnValue(COMPONENT_SKU_CODE));
                }
                if (StringUtils.isBlank(itemTypeRow.getColumnValue(COMPONENT_QUANTITY))) {
                    response.addError(new WsError("Missing component quantity in bundle"));
                } else {
                    try {
                        componentItemType.setQuantity(Integer.parseInt(itemTypeRow.getColumnValue(COMPONENT_QUANTITY)));
                    } catch (NumberFormatException nfe) {
                        response.addError(new WsError("Invalid value for field: '" + COMPONENT_QUANTITY + "'"));
                    }
                }
                if (StringUtils.isBlank(itemTypeRow.getColumnValue(COMPONENT_PRICE))) {
                    response.addError(new WsError("Missing component price in bundle"));
                } else {
                    try {
                        componentItemType.setPrice(new BigDecimal(itemTypeRow.getColumnValue(COMPONENT_PRICE)));
                    } catch (NumberFormatException nfe) {
                        response.addError(new WsError("Invalid value for field: '" + COMPONENT_PRICE + "'"));
                    }
                }
                if (!response.hasErrors()) {
                    componentItemTypes.add(componentItemType);
                }
            }
            if (!response.hasErrors()) {
                wsItemType.setComponentItemTypes(componentItemTypes);
            }
        }
        return wsItemType;
    }

    private ImportJobHandlerResponse editItemType(ImportJobHandlerRequest request, ItemType itemType) {
        ImportJobHandlerResponse response = new ImportJobHandlerResponse();
        Row row = request.getRows().get(0);
        EditItemTypeRequest itemTypeRequest = new EditItemTypeRequest();
        WsItemType wsItemType = prepareWsItemType(response, request.getRows(), itemType);

        if (!response.hasErrors()) {
            itemTypeRequest.setItemType(wsItemType);
            response.addResponse(catalogService.editItemType(itemTypeRequest));
        }
        if (StringUtils.isNotBlank(row.getColumnValue(RESYNC_INVENTORY)) && StringUtils.parseBoolean(row.getColumnValue(RESYNC_INVENTORY))) {
            ReassessInventoryForItemTypeRequest reassessInventoryForItemTypeRequest = new ReassessInventoryForItemTypeRequest();
            reassessInventoryForItemTypeRequest.setItemSku(wsItemType.getSkuCode());
            inventoryService.reassessInventoryForItemType(reassessInventoryForItemTypeRequest);
        }
        return response;
    }

    @Override
    public void preProcessor(ImportJob importJob) {

    }

    @Override
    public void postProcessor(ImportJob importJob) {
    }
}
