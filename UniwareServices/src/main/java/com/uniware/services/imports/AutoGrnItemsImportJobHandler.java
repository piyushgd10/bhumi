/*
 * Copyright 2018 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 31/01/18
 * @author piyush
 */
package com.uniware.services.imports;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.unifier.core.api.base.ServiceResponse;
import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.entity.ImportJob;
import com.unifier.core.entity.ImportJobType;
import com.unifier.core.fileparser.Row;
import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.StringUtils;
import com.unifier.services.aspect.RollbackOnFailure;
import com.unifier.services.imports.ImportJobHandler;
import com.unifier.services.imports.ImportJobHandlerRequest;
import com.unifier.services.imports.ImportJobHandlerResponse;
import com.uniware.core.api.inflow.AddOrEditInflowReceiptItemRequest;
import com.uniware.core.api.inflow.AddOrEditInflowReceiptItemResponse;
import com.uniware.core.api.inflow.CreateInflowReceiptRequest;
import com.uniware.core.api.inflow.CreateInflowReceiptResponse;
import com.uniware.core.api.inflow.SendInflowReceiptForQCRequest;
import com.uniware.core.api.inflow.WsGRN;
import com.uniware.core.api.inflow.WsInflowReceiptItem;
import com.uniware.core.api.purchase.CreateApprovedPurchaseOrderRequest;
import com.uniware.core.api.purchase.CreateApprovedPurchaseOrderResponse;
import com.uniware.core.api.purchase.WsPurchaseOrderItem;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.entity.InflowReceiptItem;
import com.uniware.core.entity.Item;
import com.uniware.core.entity.ItemType;
import com.uniware.core.entity.PurchaseOrder;
import com.uniware.core.entity.Vendor;
import com.uniware.core.entity.VendorItemType;
import com.uniware.services.catalog.ICatalogService;
import com.uniware.services.configuration.SystemConfiguration;
import com.uniware.services.inflow.IInflowService;
import com.uniware.services.inventory.IInventoryService;
import com.uniware.services.purchase.IPurchaseService;
import com.uniware.services.vendor.IVendorService;

public class AutoGrnItemsImportJobHandler implements ImportJobHandler {

    private final String      VENDOR_CODE           = "Vendor Code";
    private final String      VENDOR_INVOICE_NUMBER = "Vendor Invoice Number";
    private final String      VENDOR_INVOICE_DATE   = "Vendor Invoice Date";
    private final String      SKU_CODE              = "Sku Code";
    private final String      ITEM_CODE             = "Item Code";
    private final String      QUANTITY              = "Qty";
    private final String      UNIT_PRICE            = "Unit Price";
    private final String      MANUFACTURING_DATE    = "Manufacturing Date";
    private final String      ITEM_DETAILS          = "Item Details";

    @Autowired
    private IVendorService    vendorService;

    @Autowired
    private IInventoryService inventoryService;

    @Autowired
    private IPurchaseService  purchaseService;

    @Autowired
    private IInflowService    inflowService;

    @Autowired
    private ICatalogService   catalogService;

    @Override
    @Transactional
    public ImportJobHandlerResponse handleRow(ImportJobHandlerRequest request, ImportJobType.ImportOptions importOption) throws Exception {
        ImportJobHandlerResponse response = new ImportJobHandlerResponse();
        ValidationContext context = request.validate();
        if (ImportJobType.ImportOptions.CREATE_NEW == importOption) {
            List<Row> rows = request.getRows();
            String vendorCode = rows.get(0).getColumnValue(VENDOR_CODE);
            Vendor vendor = validateAndGetVendor(vendorCode);
            if (vendor == null) {
                context.addError(WsResponseCode.INVALID_VENDOR_CODE);
            } else {
                Map<String, Map<Date, List<ItemDTO>>> skuToManufacturingDateToItem = new HashMap<>();
                Map<String, Integer> skuToQty = new HashMap<>();
                Map<String, BigDecimal> skuToUnitPrice = new HashMap<>();
                validateRowsAndPreparePreprocessingData(rows, context, skuToManufacturingDateToItem, skuToQty, skuToUnitPrice);
                if (!context.hasErrors()) {
                    validateVendorItemTypes(vendor, skuToQty.keySet(), context, skuToUnitPrice);
                    if (SystemConfiguration.TraceabilityLevel.ITEM.equals(ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).getTraceabilityLevel())) {
                        validateItemCodes(skuToManufacturingDateToItem, context);
                    }
                }
                if (!context.hasErrors()) {
                    processAutoGrn(rows, context, vendor, skuToQty, skuToManufacturingDateToItem, skuToUnitPrice, request.getUserId());
                }
            }
        } else {
            context.addError(WsResponseCode.FEATURE_NOT_SUPPORTED);
        }
        if (context.hasErrors()) {
            response.addErrors(context.getErrors());
        } else {
            response.setSuccessful(true);
        }
        return response;
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @RollbackOnFailure
    private ServiceResponse processAutoGrn(List<Row> rows, ValidationContext context, Vendor vendor, Map<String, Integer> skuToQty,
            Map<String, Map<Date, List<ItemDTO>>> skuToManufacturingDateToItem, Map<String, BigDecimal> skuToUnitPrice, Integer userId) {
        ServiceResponse response = new ServiceResponse();
        CreateApprovedPurchaseOrderRequest createPORequest = prepareCreatePORequest(vendor, skuToQty, skuToUnitPrice, userId);
        CreateApprovedPurchaseOrderResponse createPOResponse = purchaseService.createApprovedPurchaseOrder(createPORequest);
        if (createPOResponse.isSuccessful()) {
            CreateInflowReceiptRequest inflowReceiptRequest = prepareInflowReceiptRequest(rows, createPOResponse, userId);
            CreateInflowReceiptResponse inflowReceiptResponse = inflowService.createInflowReceipt(inflowReceiptRequest);
            if (inflowReceiptResponse.isSuccessful()) {
                addInflowReceiptItemsToInflowReceipt(createPOResponse.getPurchaseOrderCode(), inflowReceiptResponse.getInflowReceiptCode(), context, skuToQty,
                        skuToManufacturingDateToItem, vendor);
                if (!context.hasErrors()) {
                    SendInflowReceiptForQCRequest sendForQCRequest = new SendInflowReceiptForQCRequest();
                    sendForQCRequest.setInflowReceiptCode(inflowReceiptResponse.getInflowReceiptCode());
                    context.addErrors(inflowService.sendInflowReceiptForQC(sendForQCRequest).getErrors());
                }
            } else {
                context.addErrors(inflowReceiptResponse.getErrors());
            }
        } else {
            context.addErrors(createPOResponse.getErrors());
        }
        if (context.hasErrors()) {
            response.addErrors(context.getErrors());
        } else {
            response.setSuccessful(true);
        }
        return response;
    }

    private void addInflowReceiptItemsToInflowReceipt(String purchaseOrderCode, String inflowReceiptCode, ValidationContext context, Map<String, Integer> skuToQty,
            Map<String, Map<Date, List<ItemDTO>>> skuToManufacturingDateToItem, Vendor vendor) {
        PurchaseOrder purchaseOrder = purchaseService.getPurchaseOrderByCode(purchaseOrderCode);
        purchaseOrder.getPurchaseOrderItems().stream().filter(purchaseOrderItem -> !context.hasErrors()).forEach(poItem -> {
            AddOrEditInflowReceiptItemRequest addInflowReceiptItemRequest = new AddOrEditInflowReceiptItemRequest();
            addInflowReceiptItemRequest.setInflowReceiptCode(inflowReceiptCode);
            addInflowReceiptItemRequest.setPurchaseOrderItemId(poItem.getId());
            String skuCode = poItem.getItemType().getSkuCode();
            if (skuToManufacturingDateToItem.size() > 0) {
                Map<Date, List<ItemDTO>> dateToItemDTOs = skuToManufacturingDateToItem.get(skuCode);
                dateToItemDTOs.forEach(((date, itemDTOS) -> {
                    WsInflowReceiptItem wsInflowReceiptItem = new WsInflowReceiptItem();
                    wsInflowReceiptItem.setManufacturingDate(date);
                    wsInflowReceiptItem.setQuantity(itemDTOS.size());
                    addInflowReceiptItemRequest.setInflowReceiptItem(wsInflowReceiptItem);
                    AddOrEditInflowReceiptItemResponse addInflowReceiptItemResponse = inflowService.addOrEditInflowReceiptItem(addInflowReceiptItemRequest);
                    if (addInflowReceiptItemResponse.isSuccessful()) {
                        InflowReceiptItem inflowReceiptItem = inflowService.getInflowReceiptItemById(addInflowReceiptItemResponse.getInflowReceiptItemDTO().getId());
                        ItemType itemType = catalogService.getItemTypeBySkuCode(skuCode);
                        List<Item> items = inventoryService.addItemsWithCodeAndDetails(itemType, vendor, itemDTOS, inflowReceiptItem, Item.StatusCode.CREATED, context);
                        if (!context.hasErrors()) {
                            inflowReceiptItem.setDetailedItemQuantity(items.size());
                            inflowReceiptItem.setItemsLabelled(true);
                        }
                    } else {
                        context.addErrors(addInflowReceiptItemResponse.getErrors());
                    }
                }));
            } else {
                WsInflowReceiptItem wsInflowReceiptItem = new WsInflowReceiptItem();
                wsInflowReceiptItem.setQuantity(skuToQty.get(skuCode));
                addInflowReceiptItemRequest.setInflowReceiptItem(wsInflowReceiptItem);
                AddOrEditInflowReceiptItemResponse addInflowReceiptItemResponse = inflowService.addOrEditInflowReceiptItem(addInflowReceiptItemRequest);
                if (!addInflowReceiptItemResponse.isSuccessful()) {
                    context.addErrors(addInflowReceiptItemResponse.getErrors());
                }
            }
        });
    }

    private CreateInflowReceiptRequest prepareInflowReceiptRequest(List<Row> rows, CreateApprovedPurchaseOrderResponse createPOResponse, Integer userId) {
        Row row = rows.get(0);
        CreateInflowReceiptRequest request = new CreateInflowReceiptRequest();
        request.setPurchaseOrderCode(createPOResponse.getPurchaseOrderCode());
        request.setVendorInvoiceDateCheckDisable(true);
        WsGRN wsGRN = new WsGRN();
        wsGRN.setVendorInvoiceNumber(row.getColumnValue(VENDOR_INVOICE_NUMBER));
        wsGRN.setVendorInvoiceDate(DateUtils.stringToDate(row.getColumnValue(VENDOR_INVOICE_DATE), "dd/MM/yyyy"));
        wsGRN.setUserId(userId);
        request.setWsGRN(wsGRN);
        return request;
    }

    private CreateApprovedPurchaseOrderRequest prepareCreatePORequest(Vendor vendor, Map<String, Integer> skuToQty, Map<String, BigDecimal> skuToUnitPrice, Integer userId) {
        CreateApprovedPurchaseOrderRequest request = new CreateApprovedPurchaseOrderRequest();
        request.setVendorCode(vendor.getCode());
        request.setUserId(userId);
        List<WsPurchaseOrderItem> purchaseOrderItems = new ArrayList<>();
        skuToQty.forEach((skuCode, qty) -> {
            WsPurchaseOrderItem poItem = new WsPurchaseOrderItem();
            poItem.setItemSKU(skuCode);
            poItem.setQuantity(qty);
            poItem.setUnitPrice(skuToUnitPrice.get(skuCode));
            purchaseOrderItems.add(poItem);
        });
        request.setPurchaseOrderItems(purchaseOrderItems);
        return request;
    }

    private void validateItemCodes(Map<String, Map<Date, List<ItemDTO>>> skuToDateToItemDTOs, ValidationContext context) {
        List<String> itemCodes = new ArrayList<>();
        skuToDateToItemDTOs.forEach((sku, dateToItemDTOs) -> dateToItemDTOs.forEach(((date, itemDTOs) -> itemDTOs.forEach(itemDTO -> itemCodes.add(itemDTO.getCode())))));
        List<Item> items = inventoryService.getItemsByCodes(itemCodes);
        if (items.size() > 0) {
            List<String> duplicateCodes = new ArrayList<>(items.size());
            items.forEach(item -> duplicateCodes.add(item.getCode()));
            context.addError(WsResponseCode.DUPLICATE_ITEM_CODE, "Item already present with codes : " + items.toString());
        }
    }

    @Transactional
    private void validateVendorItemTypes(Vendor vendor, Set<String> skuCodes, ValidationContext context, Map<String, BigDecimal> skuToUnitPrice) {
        List<VendorItemType> vendorItemTypes = vendorService.getVendorItemTypesBySkuCodes(vendor.getId(), skuCodes);
        if (vendorItemTypes.size() != skuCodes.size()) {
            context.addError(WsResponseCode.INVALID_VENDOR_ITEM_TYPE, "Not all vendor item types exist");
        } else {
            vendorItemTypes.forEach(vit -> skuToUnitPrice.computeIfAbsent(vit.getItemType().getSkuCode(), k -> vit.getUnitPrice()));
        }
    }

    private void validateRowsAndPreparePreprocessingData(List<Row> rows, ValidationContext context, Map<String, Map<Date, List<ItemDTO>>> skuToManufacturingDateToItem,
            Map<String, Integer> skuToQty, Map<String, BigDecimal> skuToUnitPrice) {
        SystemConfiguration.TraceabilityLevel traceabilityLevel = ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).getTraceabilityLevel();
        String vendorCode = rows.get(0).getColumnValue(VENDOR_CODE);
        rows.stream().filter(r -> !context.hasErrors()).forEach(row -> {
            String skuCode = row.getColumnValue(SKU_CODE);
            String rowVendorCode = row.getColumnValue(VENDOR_CODE);
            if (StringUtils.isBlank(skuCode)) {
                context.addError(WsResponseCode.INVALID_ITEM_SKU_CODE, "Missing Sku Code");
            } else if (!rowVendorCode.equals(vendorCode)) {
                context.addError(WsResponseCode.INVALID_VENDOR_CODE, "Multiple Vendor Codes present for same vendor invoice number : " + vendorCode + ", " + rowVendorCode);
            } else if (SystemConfiguration.TraceabilityLevel.ITEM.equals(traceabilityLevel)) {
                if (StringUtils.isBlank(row.getColumnValue(ITEM_CODE))) {
                    context.addError(WsResponseCode.INVALID_ITEM_CODE, "Missing Item Code");
                } else {
                    Date manufacturingDate = null;
                    if (StringUtils.isNotBlank(row.getColumnValue(MANUFACTURING_DATE))) {
                        manufacturingDate = DateUtils.stringToDate(row.getColumnValue(MANUFACTURING_DATE), "dd/MM/yyyy");
                        if(manufacturingDate == null){
                            context.addError(WsResponseCode.INVALID_DATE, "Manufacturing Date should be in format dd/MM/yyyy");
                        }
                    }
                    if(!context.hasErrors()){
                        String itemDetails = StringUtils.isNotBlank(row.getColumnValue(ITEM_DETAILS)) ? row.getColumnValue(ITEM_DETAILS) : null;
                        skuToManufacturingDateToItem.computeIfAbsent(skuCode, k -> new HashMap<>()).computeIfAbsent(manufacturingDate, k -> new ArrayList<>()).add(
                                new ItemDTO(row.getColumnValue(ITEM_CODE), itemDetails));
                    }
                }
                skuToQty.compute(skuCode, (k, v) -> v == null ? 1 : ++v);
            } else {
                Integer qty = parseQuantity(row, context);
                if (!context.hasErrors()) {
                    skuToQty.compute(skuCode, (k, v) -> v == null ? qty : v + qty);
                }
            }
            if (!context.hasErrors()) {
                BigDecimal price = parseUnitPrice(row, context);
                if (price != null && !context.hasErrors()) {
                    if (skuToUnitPrice.get(skuCode) != null && !skuToUnitPrice.get(skuCode).equals(price)) {
                        context.addError(WsResponseCode.INVALID_PRICES, "Sku : {} present with different prices", skuCode);
                    } else {
                        skuToUnitPrice.put(skuCode, price);
                    }
                }
            }
        });
    }

    private BigDecimal parseUnitPrice(Row row, ValidationContext context) {
        BigDecimal unitPrice = null;
        if (StringUtils.isNotBlank(row.getColumnValue(UNIT_PRICE))) {
            try {
                unitPrice = new BigDecimal(row.getColumnValue(UNIT_PRICE));
            } catch (Exception e) {
                context.addError(WsResponseCode.INVALID_PRICES, "Invalid value for unit price : " + row.getColumnValue(UNIT_PRICE));
            }
        }
        return unitPrice;
    }

    private Integer parseQuantity(Row row, ValidationContext context) {
        Integer quantity = null;
        if (StringUtils.isBlank(row.getColumnValue(QUANTITY))) {
            context.addError(WsResponseCode.INVALID_REQUEST, "Quantity cannot be empty");
        } else {
            try {
                quantity = Integer.parseInt(row.getColumnValue(QUANTITY));
            } catch (NumberFormatException e) {
                context.addError(WsResponseCode.INVALID_REQUEST, "Invalid value for quantity : " + row.getColumnValue(QUANTITY));
            }
        }
        return quantity;
    }

    private Vendor validateAndGetVendor(String vendorCode) {
        if (StringUtils.isNotBlank(vendorCode)) {
            return vendorService.getVendorByCode(vendorCode);
        }
        return null;
    }

    public static class ItemDTO {

        private String code;

        private String itemDetails;

        public ItemDTO(String code, String itemDetails) {
            this.code = code;
            this.itemDetails = itemDetails;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getItemDetails() {
            return itemDetails;
        }

        public void setItemDetails(String itemDetails) {
            this.itemDetails = itemDetails;
        }
    }

    @Override
    public void preProcessor(ImportJob importJob) {

    }

    @Override
    public void postProcessor(ImportJob importJob) {

    }
}
