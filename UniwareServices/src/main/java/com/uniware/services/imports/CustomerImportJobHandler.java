/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 23, 2012
 *  @author praveeng
 */
package com.uniware.services.imports;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.unifier.core.cache.CacheManager;
import com.unifier.core.entity.ImportJob;
import com.unifier.core.entity.ImportJobType;
import com.unifier.core.entity.ImportJobType.ImportOptions;
import com.unifier.core.fileparser.Row;
import com.unifier.core.utils.StringUtils;
import com.unifier.services.imports.ImportJobHandler;
import com.unifier.services.imports.ImportJobHandlerRequest;
import com.unifier.services.imports.ImportJobHandlerResponse;
import com.uniware.core.api.customer.CreateCustomerRequest;
import com.uniware.core.api.customer.CreateCustomerResponse;
import com.uniware.core.api.customer.EditCustomerRequest;
import com.uniware.core.api.customer.EditCustomerResponse;
import com.uniware.core.api.customer.WsCustomer;
import com.uniware.core.api.party.WsPartyAddress;
import com.uniware.core.api.party.WsPartyContact;
import com.uniware.core.entity.Customer;
import com.uniware.core.entity.PartyAddressType;
import com.uniware.core.entity.PartyContactType;
import com.uniware.services.cache.VendorCache;
import com.uniware.services.customer.ICustomerService;

public class CustomerImportJobHandler implements ImportJobHandler {
    private static final String CUSTOMER_CODE                = "customer code";
    private static final String CUSTOMER_NAME                = "customer name";
    private static final String CUSTOMER_PAN                 = "PAN";
    private static final String CUSTOMER_TIN                 = "TIN";
    private static final String CUSTOMER_CST                 = "CST Number";
    private static final String CUSTOMER_ST                  = "Service Tax Number";
    private static final String CUSTOMER_GST                 = "GST Number";
    private static final String CUSTOMER_WEBSITE             = "website";
    private static final String CUSTOMER_PROVIDES_C_FORM     = "Provides C Form";
    private static final String CUSTOMER_DUAL_COMPANY_RETAIL = "Dual Company Retail";
    private static final String REGISTERED_DEALER            = "Registered Dealer";
    private static final String TAX_EXEMPTED                 = "Tax Exempted";

    //address details for Customer shipping 
    private static final String SHIPPING_ADDRESS_LINE1       = "Shipping Address Line1";
    private static final String SHIPPING_ADDRESS_LINE2       = "Shipping Address Line2";
    private static final String SHIPPING_ADDRESS_CITY        = "Shipping Address City";
    private static final String SHIPPING_ADDRESS_STATE       = "Shipping Address State";
    private static final String SHIPPING_ADDRESS_COUNTRY     = "Shipping Address Country";
    private static final String SHIPPING_ADDRESS_PINCODE     = "Shipping Address Pincode";
    private static final String SHIPPING_ADDRESS_PHONE       = "Shipping Address Phone";

    //address details for Customer billing 
    private static final String BILLING_ADDRESS_LINE1        = "Billing Address Line1";
    private static final String BILLING_ADDRESS_LINE2        = "Billing Address Line2";
    private static final String BILLING_ADDRESS_CITY         = "Billing Address City";
    private static final String BILLING_ADDRESS_STATE        = "Billing Address State";
    private static final String BILLING_ADDRESS_COUNTRY      = "Billing Address Country";
    private static final String BILLING_ADDRESS_PINCODE      = "Billing Address Pincode";
    private static final String BILLING_ADDRESS_PHONE        = "Billing Address Phone";

    //contact details for primary contact
    private static final String PRIMARY_CONTACT_NAME         = "Primary Contact Name";
    private static final String PRIMARY_CONTACT_EMAIL        = "Primary Contact Email";
    private static final String PRIMARY_CONTACT_FAX          = "Primary Contact Fax";
    private static final String PRIMARY_CONTACT_PHONE        = "Primary Contact Phone";

    @Autowired
    private ICustomerService    customerService;

    @Override
    public ImportJobHandlerResponse handleRow(ImportJobHandlerRequest request, ImportOptions importOption) {
        if (ImportJobType.ImportOptions.CREATE_NEW_AND_UPDATE_EXISTING == importOption) {
            String customerCode = request.getRow().getColumnValue(CUSTOMER_CODE);
            Customer customer = customerService.getCustomerByCode(customerCode);
            if (customer == null) {
                return createCustomer(request);
            } else {
                return editCustomer(request);
            }
        } else if (ImportJobType.ImportOptions.CREATE_NEW == importOption) {
            return createCustomer(request);
        }
        return null;
    }

    private ImportJobHandlerResponse editCustomer(ImportJobHandlerRequest request) {
        ImportJobHandlerResponse response = new ImportJobHandlerResponse();
        Row row = request.getRow();
        WsCustomer customer = prepareWsCustomer(response, row);
        if (!response.hasErrors()) {
            EditCustomerRequest editCustomerRequest = new EditCustomerRequest();
            editCustomerRequest.setCustomer(customer);
            EditCustomerResponse editCustomerResponse = customerService.editCustomer(editCustomerRequest);
            response.addResponse(editCustomerResponse);
        }
        return response;
    }

    private WsCustomer prepareWsCustomer(ImportJobHandlerResponse response, Row row) {
        WsCustomer wsCustomer = new WsCustomer();
        wsCustomer.setCode(row.getColumnValue(CUSTOMER_CODE));
        wsCustomer.setCstNumber(row.getColumnValue(CUSTOMER_CST));
        wsCustomer.setName(row.getColumnValue(CUSTOMER_NAME));
        wsCustomer.setPan(row.getColumnValue(CUSTOMER_PAN));
        wsCustomer.setStNumber(row.getColumnValue(CUSTOMER_ST));
        wsCustomer.setTin(row.getColumnValue(CUSTOMER_TIN));
        wsCustomer.setGstNumber(row.getColumnValue(CUSTOMER_GST));
        wsCustomer.setWebsite(row.getColumnValue(CUSTOMER_WEBSITE));

        String providesCForm = row.getColumnValue(CUSTOMER_PROVIDES_C_FORM);
        if (StringUtils.isNotBlank(providesCForm)) {
            wsCustomer.setProvidesCform(StringUtils.parseBoolean(providesCForm));
        }
        String dualCompanyRetail = row.getColumnValue(CUSTOMER_DUAL_COMPANY_RETAIL);
        if (StringUtils.isNotBlank(dualCompanyRetail)) {
            wsCustomer.setDualCompanyRetail(StringUtils.parseBoolean(dualCompanyRetail));
        }
        String registeredCustomer = row.getColumnValue(REGISTERED_DEALER);
        if (StringUtils.isNotBlank(registeredCustomer)) {
            wsCustomer.setRegisteredDealer(StringUtils.parseBoolean(registeredCustomer));
        }
        String taxExempted = row.getColumnValue(TAX_EXEMPTED);
        if (StringUtils.isNotBlank(taxExempted)) {
            wsCustomer.setTaxExempted(StringUtils.parseBoolean(taxExempted));
        }
        List<WsPartyContact> partyContacts = new ArrayList<WsPartyContact>();
        partyContacts.add(preparePartyContact(row));
        wsCustomer.setPartyContacts(partyContacts);
        wsCustomer.setShippingAddress(preparePartyShippingAddress(response, row));
        wsCustomer.setBillingAddress(preparePartyBillingAddress(response, row));
        return wsCustomer;
    }

    private ImportJobHandlerResponse createCustomer(ImportJobHandlerRequest request) throws RuntimeException {
        ImportJobHandlerResponse response = new ImportJobHandlerResponse();
        Row row = request.getRow();
        WsCustomer wsCustomer = prepareWsCustomer(response, row);
        if (!response.hasErrors()) {
            CreateCustomerRequest createCustomerRequest = new CreateCustomerRequest();
            createCustomerRequest.setCustomer(wsCustomer);
            CreateCustomerResponse createCustomerResponse = customerService.createCustomer(createCustomerRequest);
            response.addResponse(createCustomerResponse);
        }
        return response;
    }

    private WsPartyContact preparePartyContact(Row row) {
        WsPartyContact wsPartyContact = new WsPartyContact();
        wsPartyContact.setContactType(PartyContactType.Code.PRIMARY.name());
        wsPartyContact.setEmail(row.getColumnValue(PRIMARY_CONTACT_EMAIL));
        wsPartyContact.setFax(row.getColumnValue(PRIMARY_CONTACT_FAX));
        wsPartyContact.setName(row.getColumnValue(PRIMARY_CONTACT_NAME));
        wsPartyContact.setPartyCode(row.getColumnValue(CUSTOMER_CODE));
        wsPartyContact.setPhone(row.getColumnValue(PRIMARY_CONTACT_PHONE));
        return wsPartyContact;
    }

    private WsPartyAddress preparePartyBillingAddress(ImportJobHandlerResponse response, Row row) {
        WsPartyAddress wsPartyBillingAddress = new WsPartyAddress();
        wsPartyBillingAddress.setAddressLine1(row.getColumnValue(BILLING_ADDRESS_LINE1));
        wsPartyBillingAddress.setAddressLine2(row.getColumnValue(BILLING_ADDRESS_LINE2));
        wsPartyBillingAddress.setCity(row.getColumnValue(BILLING_ADDRESS_CITY));
        if (StringUtils.isNotBlank(row.getColumnValue(BILLING_ADDRESS_COUNTRY))) {
            wsPartyBillingAddress.setCountryCode(row.getColumnValue(BILLING_ADDRESS_COUNTRY));
        }
        wsPartyBillingAddress.setStateCode(row.getColumnValue(BILLING_ADDRESS_STATE));
        wsPartyBillingAddress.setPartyCode(row.getColumnValue(CUSTOMER_CODE));
        wsPartyBillingAddress.setAddressType(PartyAddressType.Code.BILLING.name());
        wsPartyBillingAddress.setPincode(row.getColumnValue(BILLING_ADDRESS_PINCODE));
        wsPartyBillingAddress.setPhone(row.getColumnValue(BILLING_ADDRESS_PHONE));
        return wsPartyBillingAddress;
    }

    private WsPartyAddress preparePartyShippingAddress(ImportJobHandlerResponse response, Row row) {
        WsPartyAddress wsPartyAddress = new WsPartyAddress();
        wsPartyAddress.setAddressLine1(row.getColumnValue(SHIPPING_ADDRESS_LINE1));
        wsPartyAddress.setAddressLine2(row.getColumnValue(SHIPPING_ADDRESS_LINE2));
        wsPartyAddress.setCity(row.getColumnValue(SHIPPING_ADDRESS_CITY));
        if (StringUtils.isNotBlank(row.getColumnValue(SHIPPING_ADDRESS_COUNTRY))) {
            wsPartyAddress.setCountryCode(row.getColumnValue(SHIPPING_ADDRESS_COUNTRY));
        }
        wsPartyAddress.setStateCode(row.getColumnValue(SHIPPING_ADDRESS_STATE));
        wsPartyAddress.setPartyCode(row.getColumnValue(CUSTOMER_CODE));
        wsPartyAddress.setAddressType(PartyAddressType.Code.SHIPPING.name());
        wsPartyAddress.setPincode(row.getColumnValue(SHIPPING_ADDRESS_PINCODE));
        wsPartyAddress.setPhone(row.getColumnValue(SHIPPING_ADDRESS_PHONE));
        return wsPartyAddress;
    }

    @Override
    public void preProcessor(ImportJob importJob) {
    }

    @Override
    public void postProcessor(ImportJob importJob) {
        CacheManager.getInstance().markCacheDirty(VendorCache.class);
    }

}