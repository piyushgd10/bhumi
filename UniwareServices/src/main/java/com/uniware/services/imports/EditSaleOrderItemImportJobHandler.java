/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 31-Mar-2012
 *  @author ankit
 */
package com.uniware.services.imports;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;

import com.unifier.core.api.validation.WsError;
import com.unifier.core.entity.ImportJob;
import com.unifier.core.entity.ImportJobType.ImportOptions;
import com.unifier.core.fileparser.Row;
import com.unifier.core.utils.StringUtils;
import com.unifier.services.imports.ImportJobHandler;
import com.unifier.services.imports.ImportJobHandlerRequest;
import com.unifier.services.imports.ImportJobHandlerResponse;
import com.uniware.core.api.saleorder.EditSaleOrderItemRequest;
import com.uniware.services.saleorder.ISaleOrderService;

public class EditSaleOrderItemImportJobHandler implements ImportJobHandler {

    @Autowired
    private ISaleOrderService   saleOrderService;

    private static final String SALE_ORDER_CODE          = "Sales Order Code";
    private static final String SALE_ORDER_ITEM_CODE     = "Sale Order Item Code";
    private static final String TOTAL_PRICE              = "Total Price";
    private static final String SELLING_PRICE            = "Selling Price";
    private static final String SHIPPING_CHARGES         = "Shipping Charges";
    private static final String SHIPPING_METHOD_CHARGES  = "Shipping Method Charges";
    private static final String DISCOUNT                 = "Discount";
    private static final String VOUCHER_CODE             = "Voucher Code";
    private static final String STORE_CREDIT             = "Store Credit";
    private static final String VOUCHER_VALUE            = "Voucher Value";
    private static final String CASH_ON_DELIVERY_CHARGES = "COD Service Charges";
    private static final String GIFT_WRAP_CHARGES        = "Gift Wrap Charges";

    @Override
    public ImportJobHandlerResponse handleRow(ImportJobHandlerRequest request, ImportOptions importOption) throws Exception {
        ImportJobHandlerResponse response = new ImportJobHandlerResponse();
        Row row = request.getRow();
        EditSaleOrderItemRequest editSaleOrderItemRequest = new EditSaleOrderItemRequest();
        editSaleOrderItemRequest.setSaleOrderCode(row.getColumnValue(SALE_ORDER_CODE));

        if (StringUtils.isNotBlank(row.getColumnValue(CASH_ON_DELIVERY_CHARGES))) {
            try {
                editSaleOrderItemRequest.setCashOnDeliveryCharges(new BigDecimal(row.getColumnValue(CASH_ON_DELIVERY_CHARGES)));
            } catch (NumberFormatException e) {
                response.addError(new WsError("Invalid value for " + CASH_ON_DELIVERY_CHARGES));
            }
        }

        editSaleOrderItemRequest.setSaleOrderItemCode(row.getColumnValue(SALE_ORDER_ITEM_CODE));

        if (StringUtils.isNotBlank(row.getColumnValue(DISCOUNT))) {
            try {
                editSaleOrderItemRequest.setDiscount(new BigDecimal(row.getColumnValue(DISCOUNT)));
            } catch (NumberFormatException e) {
                response.addError(new WsError("Invalid value for " + DISCOUNT));
            }
        }

        if (StringUtils.isNotBlank(row.getColumnValue(GIFT_WRAP_CHARGES))) {
            try {
                editSaleOrderItemRequest.setGiftWrapCharges(new BigDecimal(row.getColumnValue(GIFT_WRAP_CHARGES)));
            } catch (NumberFormatException e) {
                response.addError(new WsError("Invalid value for " + GIFT_WRAP_CHARGES));
            }
        }

        if (StringUtils.isNotBlank(row.getColumnValue(VOUCHER_CODE))) {
            editSaleOrderItemRequest.setVoucherCode(row.getColumnValue(VOUCHER_CODE).trim());
        }

        if (StringUtils.isNotBlank(row.getColumnValue(VOUCHER_VALUE))) {
            try {
                editSaleOrderItemRequest.setVoucherValue(new BigDecimal(row.getColumnValue(VOUCHER_VALUE)));
            } catch (NumberFormatException e) {
                response.addError(new WsError("Invalid value for " + VOUCHER_VALUE));
            }
        }

        if (StringUtils.isNotBlank(row.getColumnValue(STORE_CREDIT))) {
            try {
                editSaleOrderItemRequest.setStoreCredit(new BigDecimal(row.getColumnValue(STORE_CREDIT)));
            } catch (NumberFormatException e) {
                response.addError(new WsError("Invalid value for " + STORE_CREDIT));
            }
        }

        try {
            editSaleOrderItemRequest.setSellingPrice(new BigDecimal(row.getColumnValue(SELLING_PRICE)));
        } catch (NumberFormatException e) {
            response.addError(new WsError("Invalid value for " + SELLING_PRICE));
        }

        if (StringUtils.isNotBlank(row.getColumnValue(SHIPPING_CHARGES))) {
            try {
                editSaleOrderItemRequest.setShippingCharges(new BigDecimal(row.getColumnValue(SHIPPING_CHARGES)));
            } catch (NumberFormatException e) {
                response.addError(new WsError("Invalid value for " + SHIPPING_CHARGES));
            }
        }

        if (StringUtils.isNotBlank(row.getColumnValue(SHIPPING_METHOD_CHARGES))) {
            try {
                editSaleOrderItemRequest.setShippingMethodCharges(new BigDecimal(row.getColumnValue(SHIPPING_METHOD_CHARGES)));
            } catch (NumberFormatException e) {
                response.addError(new WsError("Invalid value for " + SHIPPING_METHOD_CHARGES));
            }
        }

        try {
            editSaleOrderItemRequest.setTotalPrice(new BigDecimal(row.getColumnValue(TOTAL_PRICE)));
        } catch (NumberFormatException e) {
            response.addError(new WsError("Invalid value for " + TOTAL_PRICE));
        }

        //sale order item changed
        if (!response.hasErrors()) {
            response.addResponse(saleOrderService.editSaleOrderItem(editSaleOrderItemRequest));
        }
        return response;
    }

    @Override
    public void preProcessor(ImportJob importJob) {

    }

    @Override
    public void postProcessor(ImportJob importJob) {

    }
}
