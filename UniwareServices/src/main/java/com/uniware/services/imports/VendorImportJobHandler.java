/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 23, 2012
 *  @author praveeng
 */
package com.uniware.services.imports;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.unifier.core.api.validation.WsError;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.entity.ImportJob;
import com.unifier.core.entity.ImportJobType;
import com.unifier.core.entity.ImportJobType.ImportOptions;
import com.unifier.core.fileparser.Row;
import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.StringUtils;
import com.unifier.core.utils.ValidatorUtils;
import com.unifier.services.imports.ImportJobHandler;
import com.unifier.services.imports.ImportJobHandlerRequest;
import com.unifier.services.imports.ImportJobHandlerResponse;
import com.unifier.services.utils.CustomFieldUtils;
import com.uniware.core.api.party.CreateVendorRequest;
import com.uniware.core.api.party.CreateVendorResponse;
import com.uniware.core.api.party.EditVendorRequest;
import com.uniware.core.api.party.EditVendorResponse;
import com.uniware.core.api.party.WsGenericVendor;
import com.uniware.core.api.party.WsPartyAddress;
import com.uniware.core.api.party.WsPartyContact;
import com.uniware.core.api.party.WsVendorAgreement;
import com.uniware.core.entity.PartyAddress;
import com.uniware.core.entity.PartyAddressType;
import com.uniware.core.entity.PartyContact;
import com.uniware.core.entity.PartyContactType;
import com.uniware.core.entity.Vendor;
import com.uniware.core.entity.VendorAgreement;
import com.uniware.services.cache.VendorCache;
import com.uniware.services.party.IPartyService;
import com.uniware.services.vendor.IVendorService;

public class VendorImportJobHandler implements ImportJobHandler {
    private static final String VENDOR_CODE                   = "vendor code";
    private static final String VENDOR_NAME                   = "vendor name";
    private static final String REGISTERED_DEALER             = "Registered Dealer";
    private static final String VENDOR_PAN                    = "PAN";
    private static final String VENDOR_TIN                    = "TIN";
    private static final String VENDOR_CST                    = "CST Number";
    private static final String VENDOR_GST                    = "GST Number";
    private static final String VENDOR_ST                     = "Service Tax Number";
    private static final String VENDOR_WEBSITE                = "website";
    private static final String VENDOR_ENABLED                = "Enabled";
    private static final String VENDOR_PURCHASE_EXPIRY_PERIOD = "Purchase Expiry Period";
    private static final String VENDOR_ACCEPTS_C_FORM         = "Accepts C Form";
    private static final String VENDOR_TAX_EXEMPTED           = "Tax Exempted";

    //address details for Vendor shipping List<WsAddressDetail> in WsSaleOrder
    private static final String SHIPPING_ADDRESS_LINE1        = "Shipping Address Line1";
    private static final String SHIPPING_ADDRESS_LINE2        = "Shipping Address Line2";
    private static final String SHIPPING_ADDRESS_CITY         = "Shipping Address City";
    private static final String SHIPPING_ADDRESS_STATE        = "Shipping Address State";
    private static final String SHIPPING_ADDRESS_COUNTRY      = "Shipping Address Country";
    private static final String SHIPPING_ADDRESS_PINCODE      = "Shipping Address Pincode";
    private static final String SHIPPING_ADDRESS_PHONE        = "Shipping Address Phone";

    //address details for each Vendor billing List<WsAddressDetail> in WsSaleOrder
    private static final String BILLING_ADDRESS_LINE1         = "Billing Address Line1";
    private static final String BILLING_ADDRESS_LINE2         = "Billing Address Line2";
    private static final String BILLING_ADDRESS_CITY          = "Billing Address City";
    private static final String BILLING_ADDRESS_STATE         = "Billing Address State";
    private static final String BILLING_ADDRESS_COUNTRY       = "Billing Address Country";
    private static final String BILLING_ADDRESS_PINCODE       = "Billing Address Pincode";
    private static final String BILLING_ADDRESS_PHONE         = "Billing Address Phone";

    //contact details for primary contact
    private static final String PRIMARY_CONTACT_NAME          = "Primary Contact Name";
    private static final String PRIMARY_CONTACT_EMAIL         = "Primary Contact Email";
    private static final String PRIMARY_CONTACT_FAX           = "Primary Contact Fax";
    private static final String PRIMARY_CONTACT_PHONE         = "Primary Contact Phone";

    //create vendor agreement
    private static final String AGREEMENT_NAME                = "agreement name";
    private static final String AGREEMENT_START_TIME          = "agreement start time as dd/MM/yyyy hh:mm:ss";
    private static final String AGREEMENT_END_TIME            = "agreement end time as dd/MM/yyyy hh:mm:ss";
    private static final String AGREEMENT_TEXT                = "agreement text";

    @Autowired
    private IVendorService      vendorService;

    @Autowired
    private IPartyService       partyService;

    @Override
    public ImportJobHandlerResponse handleRow(ImportJobHandlerRequest request, ImportOptions importOption) {
        if (ImportJobType.ImportOptions.CREATE_NEW_AND_UPDATE_EXISTING == importOption) {
            String vendorCode = request.getRow().getColumnValue(VENDOR_CODE);
            Vendor vendor = vendorService.getVendorByCode(vendorCode);
            if (vendor == null) {
                return createVendor(request);
            } else {
                return editVendor(request);
            }
        } else if (ImportJobType.ImportOptions.CREATE_NEW == importOption) {
            return createVendor(request);
        } else if (ImportJobType.ImportOptions.UPDATE_EXISTING == importOption) {
            return editPartialVendor(request);
        }
        return null;
    }

    private ImportJobHandlerResponse editVendor(ImportJobHandlerRequest request) {
        ImportJobHandlerResponse response = new ImportJobHandlerResponse();
        Row row = request.getRow();

        WsGenericVendor wsVendor = prepareWsVendor(response, row);

        if (!response.hasErrors()) {
            WsPartyAddress wsPartyShippingAddress = preparePartyShippingAddress(response, row);
            if (!response.hasErrors()) {
                wsVendor.setShippingAddress(wsPartyShippingAddress);
            } else {
                throw new RuntimeException(response.getMessage());
            }

            WsPartyAddress wsPartyBillingAddress = preparePartyBillingAddress(response, row);
            if (!response.hasErrors()) {
                wsVendor.setBillingAddress(wsPartyBillingAddress);
            } else {
                throw new RuntimeException(response.getMessage());
            }

            if (!response.hasErrors()) {
                WsPartyContact wsPartyContact = preparePartyContact(row);
                if (!response.hasErrors()) {
                    List<WsPartyContact> partyContacts = new ArrayList<>();
                    partyContacts.add(wsPartyContact);
                    wsVendor.setPartyContacts(partyContacts);
                }
            } else {
                throw new RuntimeException(response.getMessage());
            }
            if (!response.hasErrors() && StringUtils.isNotBlank(row.getColumnValue(AGREEMENT_NAME))) {
                List<WsVendorAgreement> vendorAgreements = new ArrayList<>();
                vendorAgreements.add(prepareVendorAgreement(row, response));
                wsVendor.setVendorAgreements(vendorAgreements);
            }
            if (!response.hasErrors()) {
                EditVendorRequest editVendorRequest = new EditVendorRequest();
                editVendorRequest.setVendor(wsVendor);
                EditVendorResponse editVendorResponse = vendorService.editVendor(editVendorRequest);
                response.addResponse(editVendorResponse);
            }
        } else {
            throw new RuntimeException(response.getMessage());
        }
        return response;
    }

    public ImportJobHandlerResponse editPartialVendor(ImportJobHandlerRequest request) {
        ImportJobHandlerResponse response = new ImportJobHandlerResponse();
        Row row = request.getRow();
        Vendor vendor = vendorService.getVendorByCode(request.getRow().getColumnValue(VENDOR_CODE));
        if (vendor != null) {
            WsGenericVendor wsVendor = preparePartialWsVendor(response, row, vendor);
            if (!response.hasErrors()) {
                WsPartyAddress wsPartyShippingAddress = preparePartialPartyShippingAddress(response, row, vendor.getPartyAddressByType(PartyAddressType.Code.SHIPPING.name()),
                        vendor);
                wsVendor.setShippingAddress(wsPartyShippingAddress);
                WsPartyAddress wsPartyBillingAddress = preparePartialPartyBillingAddress(response, row, vendor.getPartyAddressByType(PartyAddressType.Code.BILLING.name()), vendor);
                wsVendor.setBillingAddress(wsPartyBillingAddress);
                WsPartyContact wsPartialPartyContact = preparePartialPartyContact(row, vendor.getPartyContactByType(PartyContactType.Code.PRIMARY.name()), vendor);
                List<WsPartyContact> partyContacts = new ArrayList<>();
                partyContacts.add(wsPartialPartyContact);
                wsVendor.setPartyContacts(partyContacts);
                EditVendorRequest editVendorRequest = new EditVendorRequest();
                editVendorRequest.setVendor(wsVendor);
                EditVendorResponse editVendorResponse = vendorService.editVendor(editVendorRequest);
                response.addResponse(editVendorResponse);
            } else {
                throw new RuntimeException(response.getMessage());
            }
        } else {
            response.addError(new WsError("Invalid Vendor Code"));
        }
        return response;
    }

    private WsGenericVendor prepareWsVendor(ImportJobHandlerResponse response, Row row) {
        WsGenericVendor wsVendor = new WsGenericVendor();
        wsVendor.setCode(row.getColumnValue(VENDOR_CODE));
        wsVendor.setCstNumber(row.getColumnValue(VENDOR_CST));
        wsVendor.setName(row.getColumnValue(VENDOR_NAME));
        wsVendor.setPan(row.getColumnValue(VENDOR_PAN));
        wsVendor.setStNumber(row.getColumnValue(VENDOR_ST));
        wsVendor.setTin(row.getColumnValue(VENDOR_TIN));
        wsVendor.setGstNumber(row.getColumnValue(VENDOR_GST));
        wsVendor.setWebsite(row.getColumnValue(VENDOR_WEBSITE));
        if (StringUtils.isNotBlank(row.getColumnValue(VENDOR_PURCHASE_EXPIRY_PERIOD))) {
            try {
                wsVendor.setPurchaseExpiryPeriod(Integer.parseInt(row.getColumnValue(VENDOR_PURCHASE_EXPIRY_PERIOD).trim()));
            } catch (NumberFormatException e) {
                response.addError(new WsError("Invalid format for Vendor Purchase Expiry Period"));
            }
        }
        String enabled = row.getColumnValue(VENDOR_ENABLED);
        if (StringUtils.isNotBlank(enabled)) {
            wsVendor.setEnabled(StringUtils.parseBoolean(enabled));
        }

        String acceptsCForm = row.getColumnValue(VENDOR_ACCEPTS_C_FORM);
        if (StringUtils.isNotBlank(acceptsCForm)) {
            wsVendor.setAcceptsCForm(StringUtils.parseBoolean(acceptsCForm));
        }
        String taxExempted = row.getColumnValue(VENDOR_TAX_EXEMPTED);
        if (StringUtils.isNotBlank(taxExempted)) {
            wsVendor.setTaxExempted(StringUtils.parseBoolean(taxExempted));
        }

        String registeredDealer = row.getColumnValue(REGISTERED_DEALER);
        if (StringUtils.isNotBlank(registeredDealer)) {
            wsVendor.setRegisteredDealer(StringUtils.parseBoolean(registeredDealer));
        }

        wsVendor.setCustomFieldValues(CustomFieldUtils.getCustomFieldValues(response, Vendor.class.getName(), row));

        return wsVendor;
    }

    private WsGenericVendor preparePartialWsVendor(ImportJobHandlerResponse response, Row row, Vendor vendor) {
        WsGenericVendor wsVendor = new WsGenericVendor();
        wsVendor.setName(ValidatorUtils.getOrDefaultValue(row.getColumnValue(VENDOR_NAME), vendor.getName()));
        wsVendor.setCode(ValidatorUtils.getOrDefaultValue(row.getColumnValue(VENDOR_CODE), vendor.getCode()));
        wsVendor.setPan(ValidatorUtils.getOrDefaultValue(row.getColumnValue(VENDOR_PAN), vendor.getPan()));
        wsVendor.setTin(ValidatorUtils.getOrDefaultValue(row.getColumnValue(VENDOR_TIN), vendor.getTin()));
        wsVendor.setGstNumber(ValidatorUtils.getOrDefaultValue(row.getColumnValue(VENDOR_GST), vendor.getGstNumber()));
        wsVendor.setCstNumber(ValidatorUtils.getOrDefaultValue(row.getColumnValue(VENDOR_CST), vendor.getCstNumber()));
        wsVendor.setStNumber(ValidatorUtils.getOrDefaultValue(row.getColumnValue(VENDOR_ST), vendor.getStNumber()));
        wsVendor.setWebsite(ValidatorUtils.getOrDefaultValue(row.getColumnValue(VENDOR_WEBSITE), vendor.getWebsite()));

        if (StringUtils.isNotBlank(row.getColumnValue(VENDOR_PURCHASE_EXPIRY_PERIOD))) {
            try {
                wsVendor.setPurchaseExpiryPeriod(Integer.parseInt(row.getColumnValue(VENDOR_PURCHASE_EXPIRY_PERIOD).trim()));
            } catch (NumberFormatException e) {
                response.addError(new WsError("Invalid format for Vendor Purchase Expiry Period"));
            }
        } else {
            wsVendor.setPurchaseExpiryPeriod(vendor.getPurchaseExpiryPeriod());
        }
        String enabled = row.getColumnValue(VENDOR_ENABLED);
        if (StringUtils.isNotBlank(enabled)) {
            wsVendor.setEnabled(StringUtils.parseBoolean(enabled));
        } else {
            wsVendor.setEnabled(vendor.isEnabled());
        }

        String acceptsCForm = row.getColumnValue(VENDOR_ACCEPTS_C_FORM);
        if (StringUtils.isNotBlank(acceptsCForm)) {
            wsVendor.setAcceptsCForm(StringUtils.parseBoolean(acceptsCForm));
        } else {
            wsVendor.setAcceptsCForm(vendor.isAcceptsCForm());
        }
        String taxExempted = row.getColumnValue(VENDOR_TAX_EXEMPTED);
        if (StringUtils.isNotBlank(taxExempted)) {
            wsVendor.setTaxExempted(StringUtils.parseBoolean(taxExempted));
        } else {
            wsVendor.setTaxExempted(vendor.isTaxExempted());
        }

        String registeredDealer = row.getColumnValue(REGISTERED_DEALER);
        if (StringUtils.isNotBlank(registeredDealer)) {
            wsVendor.setRegisteredDealer(StringUtils.parseBoolean(registeredDealer));
        } else {
            wsVendor.setRegisteredDealer(vendor.isRegisteredDealer());
        }
        wsVendor.setCustomFieldValues(CustomFieldUtils.getCustomFieldValues(response, Vendor.class.getName(), row));
        return wsVendor;
    }

    private ImportJobHandlerResponse createVendor(ImportJobHandlerRequest request) throws RuntimeException {
        ImportJobHandlerResponse response = new ImportJobHandlerResponse();
        Row row = request.getRow();
        WsGenericVendor wsVendor = prepareWsVendor(response, row);
        if (!response.hasErrors()) {
            CreateVendorRequest createVendorRequest = new CreateVendorRequest();

            WsPartyAddress wsPartyShippingAddress = preparePartyShippingAddress(response, row);
            if (!response.hasErrors()) {
                wsVendor.setShippingAddress(wsPartyShippingAddress);
            } else {
                throw new RuntimeException(response.getMessage());
            }

            WsPartyAddress wsPartyBillingAddress = preparePartyBillingAddress(response, row);
            if (!response.hasErrors()) {
                wsVendor.setBillingAddress(wsPartyBillingAddress);
            } else {
                throw new RuntimeException(response.getMessage());
            }

            if (!response.hasErrors()) {
                WsPartyContact wsPartyContact = preparePartyContact(row);
                if (!response.hasErrors()) {
                    List<WsPartyContact> partyContacts = new ArrayList<>();
                    partyContacts.add(wsPartyContact);
                    wsVendor.setPartyContacts(partyContacts);
                }
            } else {
                throw new RuntimeException(response.getMessage());
            }

            if (!response.hasErrors() && StringUtils.isNotBlank(row.getColumnValue(AGREEMENT_NAME))) {
                List<WsVendorAgreement> vendorAgreements = new ArrayList<>();
                vendorAgreements.add(prepareVendorAgreement(row, response));
                wsVendor.setVendorAgreements(vendorAgreements);
            }

            if (!response.hasErrors()) {
                createVendorRequest.setVendor(wsVendor);
                CreateVendorResponse createVendorResponse = vendorService.createVendor(createVendorRequest);
                response.addResponse(createVendorResponse);
            }
        } else {
            throw new RuntimeException(response.getMessage());
        }
        return response;
    }

    private WsPartyContact preparePartyContact(Row row) {
        WsPartyContact wsPartyContact = new WsPartyContact();
        wsPartyContact.setContactType(PartyContactType.Code.PRIMARY.name());
        wsPartyContact.setEmail(row.getColumnValue(PRIMARY_CONTACT_EMAIL));
        wsPartyContact.setFax(row.getColumnValue(PRIMARY_CONTACT_FAX));
        wsPartyContact.setName(row.getColumnValue(PRIMARY_CONTACT_NAME));
        wsPartyContact.setPartyCode(row.getColumnValue(VENDOR_CODE));
        wsPartyContact.setPhone(row.getColumnValue(PRIMARY_CONTACT_PHONE));
        return wsPartyContact;
    }

    private WsPartyContact preparePartialPartyContact(Row row, PartyContact partyContact, Vendor vendor) {
        WsPartyContact wsPartyContact = new WsPartyContact();
        wsPartyContact.setContactType(PartyContactType.Code.PRIMARY.name());
        wsPartyContact.setPartyCode(ValidatorUtils.getOrDefaultValue(row.getColumnValue(VENDOR_CODE), vendor.getCode()));
        wsPartyContact.setEmail(ValidatorUtils.getOrDefaultValue(row.getColumnValue(PRIMARY_CONTACT_EMAIL), partyContact.getEmail()));
        wsPartyContact.setFax(ValidatorUtils.getOrDefaultValue(row.getColumnValue(PRIMARY_CONTACT_FAX), partyContact.getFax()));
        wsPartyContact.setName(ValidatorUtils.getOrDefaultValue(row.getColumnValue(PRIMARY_CONTACT_NAME), partyContact.getName()));
        wsPartyContact.setPhone(ValidatorUtils.getOrDefaultValue(row.getColumnValue(PRIMARY_CONTACT_PHONE), partyContact.getPhone()));
        return wsPartyContact;
    }

    private WsPartyAddress preparePartyBillingAddress(ImportJobHandlerResponse response, Row row) {
        WsPartyAddress wsPartyBillingAddress = new WsPartyAddress();
        wsPartyBillingAddress.setAddressLine1(row.getColumnValue(BILLING_ADDRESS_LINE1));
        wsPartyBillingAddress.setAddressLine2(row.getColumnValue(BILLING_ADDRESS_LINE2));
        wsPartyBillingAddress.setCity(row.getColumnValue(BILLING_ADDRESS_CITY));
        if (StringUtils.isNotBlank(row.getColumnValue(BILLING_ADDRESS_COUNTRY))) {
            wsPartyBillingAddress.setCountryCode(row.getColumnValue(BILLING_ADDRESS_COUNTRY));
        }
        wsPartyBillingAddress.setStateCode(row.getColumnValue(BILLING_ADDRESS_STATE));
        wsPartyBillingAddress.setPartyCode(row.getColumnValue(VENDOR_CODE));
        wsPartyBillingAddress.setAddressType(PartyAddressType.Code.BILLING.name());
        wsPartyBillingAddress.setPincode(row.getColumnValue(BILLING_ADDRESS_PINCODE));
        wsPartyBillingAddress.setPhone(row.getColumnValue(BILLING_ADDRESS_PHONE));
        return wsPartyBillingAddress;
    }

    private WsPartyAddress preparePartialPartyBillingAddress(ImportJobHandlerResponse response, Row row, PartyAddress partyAddress, Vendor vendor) {
        WsPartyAddress wsPartyAddress = new WsPartyAddress();
        wsPartyAddress.setAddressLine1(ValidatorUtils.getOrDefaultValue(row.getColumnValue(BILLING_ADDRESS_LINE1), partyAddress.getAddressLine1()));
        wsPartyAddress.setAddressLine2(ValidatorUtils.getOrDefaultValue(row.getColumnValue(BILLING_ADDRESS_LINE2), partyAddress.getAddressLine2()));
        wsPartyAddress.setCity(ValidatorUtils.getOrDefaultValue(row.getColumnValue(BILLING_ADDRESS_CITY), partyAddress.getCity()));
        if (StringUtils.isNotBlank(row.getColumnValue(BILLING_ADDRESS_COUNTRY))) {
            wsPartyAddress.setCountryCode(ValidatorUtils.getOrDefaultValue(row.getColumnValue(BILLING_ADDRESS_COUNTRY), partyAddress.getCountryCode()));
        }
        wsPartyAddress.setStateCode(ValidatorUtils.getOrDefaultValue(row.getColumnValue(BILLING_ADDRESS_STATE), partyAddress.getStateCode()));
        wsPartyAddress.setPartyCode(ValidatorUtils.getOrDefaultValue(row.getColumnValue(VENDOR_CODE), vendor.getCode()));
        wsPartyAddress.setAddressType(PartyAddressType.Code.BILLING.name());
        wsPartyAddress.setPincode(ValidatorUtils.getOrDefaultValue(row.getColumnValue(BILLING_ADDRESS_PINCODE), partyAddress.getPincode()));
        wsPartyAddress.setPhone(ValidatorUtils.getOrDefaultValue(row.getColumnValue(BILLING_ADDRESS_PHONE), partyAddress.getPhone()));
        return wsPartyAddress;
    }

    private WsPartyAddress preparePartyShippingAddress(ImportJobHandlerResponse response, Row row) {
        WsPartyAddress wsPartyAddress = new WsPartyAddress();
        wsPartyAddress.setAddressLine1(row.getColumnValue(SHIPPING_ADDRESS_LINE1));
        wsPartyAddress.setAddressLine2(row.getColumnValue(SHIPPING_ADDRESS_LINE2));
        wsPartyAddress.setCity(row.getColumnValue(SHIPPING_ADDRESS_CITY));
        if (StringUtils.isNotBlank(row.getColumnValue(SHIPPING_ADDRESS_COUNTRY))) {
            wsPartyAddress.setCountryCode(row.getColumnValue(SHIPPING_ADDRESS_COUNTRY));
        }
        wsPartyAddress.setStateCode(row.getColumnValue(SHIPPING_ADDRESS_STATE));
        wsPartyAddress.setPartyCode(row.getColumnValue(VENDOR_CODE));
        wsPartyAddress.setAddressType(PartyAddressType.Code.SHIPPING.name());
        wsPartyAddress.setPincode(row.getColumnValue(SHIPPING_ADDRESS_PINCODE));
        wsPartyAddress.setPhone(row.getColumnValue(SHIPPING_ADDRESS_PHONE));
        return wsPartyAddress;
    }

    private WsPartyAddress preparePartialPartyShippingAddress(ImportJobHandlerResponse response, Row row, PartyAddress partyAddress, Vendor vendor) {
        WsPartyAddress wsPartyAddress = new WsPartyAddress();
        wsPartyAddress.setAddressLine1(ValidatorUtils.getOrDefaultValue(row.getColumnValue(SHIPPING_ADDRESS_LINE1), partyAddress.getAddressLine1()));
        wsPartyAddress.setAddressLine2(ValidatorUtils.getOrDefaultValue(row.getColumnValue(SHIPPING_ADDRESS_LINE2), partyAddress.getAddressLine2()));
        wsPartyAddress.setCity(ValidatorUtils.getOrDefaultValue(row.getColumnValue(SHIPPING_ADDRESS_CITY), partyAddress.getCity()));
        if (StringUtils.isNotBlank(row.getColumnValue(SHIPPING_ADDRESS_COUNTRY))) {
            wsPartyAddress.setCountryCode(ValidatorUtils.getOrDefaultValue(row.getColumnValue(SHIPPING_ADDRESS_COUNTRY), partyAddress.getCountryCode()));
        }
        wsPartyAddress.setStateCode(ValidatorUtils.getOrDefaultValue(row.getColumnValue(SHIPPING_ADDRESS_STATE), partyAddress.getStateCode()));
        wsPartyAddress.setPartyCode(ValidatorUtils.getOrDefaultValue(row.getColumnValue(VENDOR_CODE), vendor.getCode()));
        wsPartyAddress.setAddressType(PartyAddressType.Code.SHIPPING.name());
        wsPartyAddress.setPincode(ValidatorUtils.getOrDefaultValue(row.getColumnValue(SHIPPING_ADDRESS_PINCODE), partyAddress.getPincode()));
        wsPartyAddress.setPhone(ValidatorUtils.getOrDefaultValue(row.getColumnValue(SHIPPING_ADDRESS_PHONE), partyAddress.getPhone()));
        return wsPartyAddress;
    }

    private WsVendorAgreement prepareVendorAgreement(Row row, ImportJobHandlerResponse response) {
        WsVendorAgreement wsVendorAgreement = new WsVendorAgreement();
        wsVendorAgreement.setVendorAgreementStatus(VendorAgreement.StatusCode.ACTIVE.name());
        wsVendorAgreement.setName(row.getColumnValue(AGREEMENT_NAME));
        wsVendorAgreement.setAgreementText(row.getColumnValue(AGREEMENT_TEXT));
        String start = row.getColumnValue(AGREEMENT_START_TIME);
        if (StringUtils.isNotEmpty(start)) {
            Date startTime = DateUtils.stringToDate(start, "dd/MM/yyyy hh:mm:ss");
            if (startTime != null) {
                wsVendorAgreement.setStartTime(startTime);
            } else {
                response.addError(new WsError("Invalid value for " + AGREEMENT_START_TIME));
            }
        }
        String end = row.getColumnValue(AGREEMENT_END_TIME);
        if (StringUtils.isNotEmpty(end)) {
            Date endTime = DateUtils.stringToDate(end, "dd/MM/yyyy hh:mm:ss");
            if (endTime != null) {
                wsVendorAgreement.setEndTime(endTime);
            } else {
                response.addError(new WsError("Invalid value for " + AGREEMENT_END_TIME));
            }
        }
        return wsVendorAgreement;
    }

    @Override
    public void preProcessor(ImportJob importJob) {
    }

    @Override
    public void postProcessor(ImportJob importJob) {
        CacheManager.getInstance().markCacheDirty(VendorCache.class);
    }

}