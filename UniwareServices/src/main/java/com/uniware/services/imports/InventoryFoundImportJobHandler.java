/**
 * Copyright 2017 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version 1.0, 23/08/17
 * @author aditya
 */
package com.uniware.services.imports;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.unifier.core.api.validation.WsError;
import com.unifier.core.entity.ImportJob;
import com.unifier.core.entity.ImportJobType;
import com.unifier.core.fileparser.Row;
import com.unifier.services.aspect.RollbackOnFailure;
import com.unifier.services.imports.ImportJobHandler;
import com.unifier.services.imports.ImportJobHandlerRequest;
import com.unifier.services.imports.ImportJobHandlerResponse;
import com.uniware.core.api.inventory.MarkItemTypeQuantityFoundRequest;
import com.uniware.core.api.inventory.MarkItemTypeQuantityFoundResponse;
import com.uniware.core.entity.Item;
import com.uniware.core.entity.Shelf;
import com.uniware.services.inventory.IInventoryService;
import com.uniware.services.warehouse.IShelfService;

/**
 * Created by harshpal on 26/07/16.
 */
public class InventoryFoundImportJobHandler implements ImportJobHandler {
    private static final String SHELF_CODE = "Shelf Code";
    private static final String ITEM_CODE  = "Item Barcode";

    @Autowired
    private IInventoryService   inventoryService;

    @Autowired
    private IShelfService       shelfService;

    @Override
    @Transactional
    @RollbackOnFailure
    public ImportJobHandlerResponse handleRow(ImportJobHandlerRequest request, ImportJobType.ImportOptions importOption) throws Exception {
        ImportJobHandlerResponse response = new ImportJobHandlerResponse();
        Row row = request.getRow();
        String shelfCode = row.getColumnValue(SHELF_CODE);
        Shelf shelf = shelfService.getShelfByCode(shelfCode);
        if (shelf == null) {
            response.addError(new WsError("Invalid Shelf code"));
        }
        if (!response.hasErrors()) {
            Item item = inventoryService.getItemByCode(row.getColumnValue(ITEM_CODE));
            if (item == null) {
                response.addError(new WsError("Invalid Item Barcode"));
            } else if (!item.getStatusCode().equals(Item.StatusCode.GOOD_INVENTORY.name())) {
                response.addError(new WsError("Item : " + item.getCode() + " not GOOD_INVENTORY"));
            } else {
                MarkItemTypeQuantityFoundRequest foundRequest = new MarkItemTypeQuantityFoundRequest();
                foundRequest.setItemSku(item.getItemType().getSkuCode());
                foundRequest.setShelfCode(shelf.getCode());
                foundRequest.setAgeingStartDate(item.getAgeingStartDate());
                foundRequest.setQuantityFound(1);
                MarkItemTypeQuantityFoundResponse foundResponse = inventoryService.markItemTypeQuantityFound(foundRequest);
                if (!foundResponse.isSuccessful()) {
                    response.addErrors(foundResponse.getErrors());
                }
            }
        }
        response.setSuccessful(!response.hasErrors());
        return response;
    }

    @Override
    public void preProcessor(ImportJob importJob) {

    }

    @Override
    public void postProcessor(ImportJob importJob) {

    }
}
