/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 23, 2012
 *  @author praveeng
 */
package com.uniware.services.imports;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.unifier.core.api.validation.WsError;
import com.unifier.core.entity.ImportJob;
import com.unifier.core.entity.ImportJobType.ImportOptions;
import com.unifier.core.fileparser.Row;
import com.unifier.services.imports.ImportJobHandler;
import com.unifier.services.imports.ImportJobHandlerRequest;
import com.unifier.services.imports.ImportJobHandlerResponse;
import com.uniware.core.api.purchase.AddItemsToPurchaseCartRequest;
import com.uniware.core.api.purchase.AddItemsToPurchaseCartRequest.WsPurchaseCartItem;
import com.uniware.services.purchase.IPurchaseService;

public class AddToCartImportJobHandler implements ImportJobHandler {
    private static final String VENDOR_CODE        = "vendor code";
    private static final String ITEM_TYPE_SKU_CODE = "product code";
    private static final String UNIT_PRICE         = "Unit Price";
    private static final String QUANTITY_TO_ADD    = "Quantity To Add";

    @Autowired
    private IPurchaseService    purchaseService;

    @Override
    public ImportJobHandlerResponse handleRow(ImportJobHandlerRequest request, ImportOptions importOption) throws Exception {
        return addItemsToCart(request);
    }

    private ImportJobHandlerResponse addItemsToCart(ImportJobHandlerRequest request) {
        ImportJobHandlerResponse response = new ImportJobHandlerResponse();
        Row row = request.getRow();

        if (!response.hasErrors()) {
            AddItemsToPurchaseCartRequest addItemsToPurchaseCartRequest = new AddItemsToPurchaseCartRequest();
            addItemsToPurchaseCartRequest.setUserId(request.getUserId());
            List<WsPurchaseCartItem> purchaseCartItems = new ArrayList<AddItemsToPurchaseCartRequest.WsPurchaseCartItem>();
            purchaseCartItems.add(prepareWsPurchaseCartItem(response, row));
            addItemsToPurchaseCartRequest.setPurchaseCartItems(purchaseCartItems);
            if (!response.hasErrors()) {
                response.addResponse(purchaseService.addItemsToPurchaseCart(addItemsToPurchaseCartRequest));
            }
        }
        return response;
    }

    private WsPurchaseCartItem prepareWsPurchaseCartItem(ImportJobHandlerResponse response, Row row) {
        WsPurchaseCartItem purchaseCartItem = new WsPurchaseCartItem();
        purchaseCartItem.setItemSku(row.getColumnValue(ITEM_TYPE_SKU_CODE));
        purchaseCartItem.setVendorCode(row.getColumnValue(VENDOR_CODE));
        try {
            purchaseCartItem.setQuantity(Integer.parseInt(row.getColumnValue(QUANTITY_TO_ADD)));
        } catch (NumberFormatException e) {
            response.addError(new WsError("Invalid value for 'quantity to add'"));
        }
        if (StringUtils.isNotBlank(row.getColumnValue(UNIT_PRICE))) {
            try {
                purchaseCartItem.setUnitPrice(new BigDecimal(row.getColumnValue(UNIT_PRICE)));
            } catch (NumberFormatException e) {
                response.addError(new WsError("Invalid value for 'unit price'"));
            }
        }
        return purchaseCartItem;
    }

    @Override
    public void preProcessor(ImportJob importJob) {
    }

    @Override
    public void postProcessor(ImportJob importJob) {
    }

}
