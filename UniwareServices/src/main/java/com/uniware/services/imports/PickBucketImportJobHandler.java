/*
 * Copyright 2016 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 19/9/16 5:50 PM
 * @author bhuvneshwarkumar
 */

package com.uniware.services.imports;

import org.springframework.beans.factory.annotation.Autowired;

import com.unifier.core.api.base.ServiceResponse;
import com.unifier.core.api.validation.WsError;
import com.unifier.core.entity.ImportJob;
import com.unifier.core.entity.ImportJobType;
import com.unifier.core.utils.StringUtils;
import com.unifier.services.imports.ImportJobHandler;
import com.unifier.services.imports.ImportJobHandlerRequest;
import com.unifier.services.imports.ImportJobHandlerResponse;
import com.uniware.core.api.picker.CreatePickBucketRequest;
import com.uniware.core.api.picker.UpdatePickBucketRequest;
import com.uniware.services.picker.IPickerService;

/**
 * Created by bhuvneshwarkumar on 19/09/16.
 */
public class PickBucketImportJobHandler implements ImportJobHandler {

    private static final String CODE   = "code";
    private static final String LENGTH = "length";
    private static final String WIDTH  = "width";
    private static final String HEIGHT = "height";

    @Autowired
    private IPickerService pickerService;

    @Override
    public ImportJobHandlerResponse handleRow(ImportJobHandlerRequest request, ImportJobType.ImportOptions importOption) throws Exception {
        ImportJobHandlerResponse response = new ImportJobHandlerResponse();
        if (ImportJobType.ImportOptions.UPDATE_EXISTING == importOption) {
            UpdatePickBucketRequest updatePickBucketRequest = prepareUpdatePickBucketRequest(request);
            response.addResponse(pickerService.updatePickBucket(updatePickBucketRequest));
        } else if (ImportJobType.ImportOptions.CREATE_NEW == importOption) {
            CreatePickBucketRequest createPickBucketRequest = createCreatePickBucketRequest(request);
            response.addResponse(pickerService.createPickBucket(createPickBucketRequest));
        } else if (ImportJobType.ImportOptions.CREATE_NEW_AND_UPDATE_EXISTING == importOption) {
            if (pickerService.getPickBucketByCode(request.getRow().getColumnValue(CODE)) == null) {
                CreatePickBucketRequest createPickBucketRequest = createCreatePickBucketRequest(request);
                response.addResponse(pickerService.createPickBucket(createPickBucketRequest));
            } else {
                UpdatePickBucketRequest updatePickBucketRequest = prepareUpdatePickBucketRequest(request);
                response.addResponse(pickerService.updatePickBucket(updatePickBucketRequest));
            }
        } else {
            ServiceResponse response1 = new ServiceResponse();
            response1.addError(new WsError("Operation Not Allowed"));
            response.addResponse(response1);
        }
        return response;
    }

    private CreatePickBucketRequest createCreatePickBucketRequest(ImportJobHandlerRequest request) {
        CreatePickBucketRequest createPickBucketRequest = new CreatePickBucketRequest();
        createPickBucketRequest.setCode(request.getRow().getColumnValue(CODE));
        if (StringUtils.isNotBlank(request.getRow().getColumnValue(LENGTH))) {
            createPickBucketRequest.setLength(Integer.parseInt(request.getRow().getColumnValue(LENGTH)));
        }
        if (StringUtils.isNotBlank(request.getRow().getColumnValue(WIDTH))) {
            createPickBucketRequest.setWidth(Integer.parseInt(request.getRow().getColumnValue(WIDTH)));
        }
        if (StringUtils.isNotBlank(request.getRow().getColumnValue(HEIGHT))) {
            createPickBucketRequest.setHeight(Integer.parseInt(request.getRow().getColumnValue(HEIGHT)));
        }
        return createPickBucketRequest;
    }

    private UpdatePickBucketRequest prepareUpdatePickBucketRequest(ImportJobHandlerRequest request) {
        UpdatePickBucketRequest updatePickBucketRequest = new UpdatePickBucketRequest();
        updatePickBucketRequest.setCode(request.getRow().getColumnValue(CODE));
        if (StringUtils.isNotBlank(request.getRow().getColumnValue(LENGTH))) {
            updatePickBucketRequest.setLength(Integer.parseInt(request.getRow().getColumnValue(LENGTH)));
        }
        if (StringUtils.isNotBlank(request.getRow().getColumnValue(WIDTH))) {
            updatePickBucketRequest.setWidth(Integer.parseInt(request.getRow().getColumnValue(WIDTH)));
        }
        if (StringUtils.isNotBlank(request.getRow().getColumnValue(HEIGHT))) {
            updatePickBucketRequest.setHeight(Integer.parseInt(request.getRow().getColumnValue(HEIGHT)));
        }
        return updatePickBucketRequest;
    }

    @Override
    public void preProcessor(ImportJob importJob) {
    }

    @Override
    public void postProcessor(ImportJob importJob) {

    }
}
