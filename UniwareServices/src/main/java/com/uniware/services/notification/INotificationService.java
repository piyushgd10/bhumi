/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Apr 29, 2012
 *  @author singla
 */
package com.uniware.services.notification;

import java.util.List;

import com.unifier.core.api.validation.ValidationContext;
import com.uniware.core.api.notification.ProcessPendingNotificationsRequest;
import com.uniware.core.api.notification.ProcessPendingNotificationsResponse;
import com.uniware.core.api.notification.UpdateNotificationRequest;
import com.uniware.core.api.notification.UpdateNotificationResponse;
import com.uniware.core.entity.Channel;
import com.uniware.core.entity.Notification;
import com.uniware.services.notification.impl.NotificationServiceImpl.NotificationDetail;
import com.uniware.services.tasks.notifications.NotificationsProcessor.PendingNotificationStatus;

/**
 * @author singla
 */
public interface INotificationService {

    List<Notification> getPendingNotifications(int batchSize, int maxId);

    Notification updateNotification(Notification notification);

    ProcessPendingNotificationsResponse processPendingNotifications(ProcessPendingNotificationsRequest request);

    UpdateNotificationResponse retryOrSkipNotification(UpdateNotificationRequest request);

    NotificationDetail getNotificationDetail(Notification notification, Channel channel);

    Channel getNotificationChannel(Notification notification);

    boolean processGroupNotifications(ValidationContext context, Channel channel, List<List<Notification>> notifications);

    boolean processNotifications(ValidationContext context, Channel channel, List<Notification> groupNotifications);

    PendingNotificationStatus getPendingNotificationsStatus();

}
